using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modmarketing.subcampanhas.acoesmarketingEx.serverside
{
	public partial class associlarlistagemfinanceiros : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(FinanceiroAcoesMarketing());
		}

		protected Integer empresaID = Constants.INT_ZERO;
		protected Integer EmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		protected Integer pageSize = Constants.INT_ZERO;
		protected Integer PageSize
		{
			get { return pageSize; }
			set { if(value != null) pageSize = value; }
		}

		protected java.lang.Boolean pendentes = Constants.FALSE;
		protected java.lang.Boolean Pendentes
		{
			get { return pendentes; }
			set { if(value != null) pendentes = value; }
		}

		protected string tipoFinanceiroID = Constants.STR_EMPTY;
		protected string TipoFinanceiroID
		{
			get { return tipoFinanceiroID; }
			set { if (value != null) tipoFinanceiroID = value; }
		}

		protected string historicoPadraoID = Constants.STR_EMPTY;
		protected string HistoricoPadraoID
		{
			get { return historicoPadraoID; }
			set { if (value != null) historicoPadraoID = value; }
		}

		protected string inicio = Constants.STR_EMPTY;
		protected string dtInicio
		{
			get { return inicio; }
			set { if(value != null) inicio = value; }
		}

		protected string fim = Constants.STR_EMPTY;
		protected string dtFim
		{
			get { return fim; }
			set { if(value != null) fim = value; }
		}

		protected Integer pesquisa = Constants.INT_ZERO;
		protected Integer Pesquisa
		{
			get { return pesquisa; }
			set { if(value != null) pesquisa = value; }
		}

		protected DataSet FinanceiroAcoesMarketing()
		{
			ProcedureParameters[] param = new ProcedureParameters[8];
			
			param[0] = new ProcedureParameters("@EmpresaID", SqlDbType.Int, empresaID.intValue());
			param[1] = new ProcedureParameters("@Top", SqlDbType.Int, pageSize.intValue());
			param[2] = new ProcedureParameters("@Pendentes", SqlDbType.Bit, pendentes.booleanValue() ? 1 : 0);
			param[3] = new ProcedureParameters("@TipoFinanceiroID", SqlDbType.Int, 
				tipoFinanceiroID.Equals("") ? System.DBNull.Value : (Object)tipoFinanceiroID);
			param[4] = new ProcedureParameters("@HistoricoPadraoID", SqlDbType.Int, 
				historicoPadraoID.Equals("") ? System.DBNull.Value : (Object)historicoPadraoID);
			param[5] = new ProcedureParameters("@dtInicio", SqlDbType.DateTime, 
				inicio.Equals("") ? System.DBNull.Value : (Object)inicio);
			param[6] = new ProcedureParameters("@dtFim", SqlDbType.DateTime, 
				fim.Equals("") ? System.DBNull.Value : (Object)fim);
			param[7] = new ProcedureParameters("@Pesquisa", SqlDbType.VarChar, 
				pesquisa.Equals("") ? System.DBNull.Value : (Object)pesquisa.ToString());
			param[7].Length = 255;

			return DataInterfaceObj.execQueryProcedure("sp_Financeiro_AcoesMarketing", param);
		}
		
	}
}
