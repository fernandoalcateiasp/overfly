﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_acoesmarketing : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int RelatorioID;
        private int mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sFiltroEmpresas = Convert.ToString(HttpContext.Current.Request.Params["FiltroEmpresas"]);
        private string sFiltroEstados = Convert.ToString(HttpContext.Current.Request.Params["FiltroEstados"]);
        private string sFiltroMarcas = Convert.ToString(HttpContext.Current.Request.Params["sFiltroMarcas"]);
        private string sFiltroProprietarios = Convert.ToString(HttpContext.Current.Request.Params["sFiltroProprietarios"]);
        private string ndtInicio = Convert.ToString(HttpContext.Current.Request.Params["ndtInicio"]);
        private string ndtFim = Convert.ToString(HttpContext.Current.Request.Params["ndtFim"]);
        private int bPatrociniosPendentes = Convert.ToInt32(HttpContext.Current.Request.Params["bPatrociniosPendentes"]);
        private int bSaldoPatEfetivo = Convert.ToInt32(HttpContext.Current.Request.Params["bSaldoPatEfetivo"]);
        private string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string nomeRel = Request.Params["relatorioID"].ToString();
                RelatorioID = Convert.ToInt32(Request.Params["relatorioID"].ToString());

                switch (nomeRel)
                {
                    // Homologacao de produtos
                    case "40130":
                        acoesMarketing();
                        break;
                }
            }
        }

        public void acoesMarketing()
        {
            // Excel
            int Formato = 2;
            string Title = "Ações de Marketing";
            //mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["EmpresaID"]);
            string strSQL = "EXEC sp_AcoesMarketing_Relatorio " + sFiltroEmpresas + "," + sFiltroEstados + "," + sFiltroMarcas + "," + sFiltroProprietarios + "," + ndtInicio + "," +
                                ndtFim + "," + bPatrociniosPendentes + "," + bSaldoPatEfetivo + "," + sFiltro;
            var Header_Body = (Formato == 1 ? "Header" : "Body");
            //var posY = (Formato == 1 ? "0" : "0.004");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                //Relatorio.CriarObjLabel("Fixo", "", Title, "0.0", "0.0", "11", "#0113a0", "B", Header_Body, "100.0", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "10.13834", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.013834", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "10.0", "0.0", "11", "black", "B", Header_Body, "50.0", "Horas");
                Relatorio.CriarObjLabelData("Now", "", "", "16.795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.6795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");


                //Relatorio.CriarObjLabel("Fixo", "", Title, "1", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "80", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                //Relatorio.CriarObjTabela("0.00", "0.00001", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
