using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modmarketing.subcampanhas.acoesmarketingEx.serverside
{
    public partial class insertresponsavel : System.Web.UI.OverflyPage
    {
        private Integer registroID;
        private Integer responsavelID;
        private int rowsAffected = 0;

        protected Integer nRegistroID
        {
            set { registroID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nResponsavelID
        {
            set { responsavelID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSQL = "";

            strSQL = "UPDATE AcoesMarketing_Patrocinios " +
                "SET ResponsavelID = " + responsavelID.ToString() +
		        " WHERE AcaPatrocinioID = " + registroID.ToString();

            rowsAffected = DataInterfaceObj.ExecuteSQLCommand(strSQL);

            // Gera o resultado para o usuario.
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "SELECT " + rowsAffected + " as Resultado"
                    )
            );
        }
    }
}