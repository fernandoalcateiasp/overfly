using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modmarketing.subcampanhas.acoesmarketingEx.serverside
{
	public partial class gravaacoesmarketing : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(FinAcaoMarketingAssociar());
		}

		protected Integer[] acaoID = Constants.INT_EMPTY_SET;
		protected Integer[] AcaoID
		{
			get { return acaoID; }
			set { if (value != null) acaoID = value; }
		}

		protected Integer[] acaPatrocinioID = Constants.INT_EMPTY_SET;
		protected Integer[] AcaPatrocinioID
		{
			get { return acaPatrocinioID; }
			set { if (value != null) acaPatrocinioID = value; }
		}

		protected Integer[] finAcaoID = Constants.INT_EMPTY_SET;
		protected Integer[] FinAcaoID
		{
			get { return finAcaoID; }
			set { if (value != null) finAcaoID = value; }
		}

		protected Integer[] financeiroID = Constants.INT_EMPTY_SET;
		protected Integer[] FinanceiroID
		{
			get { return financeiroID; }
			set { if (value != null) financeiroID = value; }
		}

		protected java.lang.Double[] valor = Constants.DBL_EMPTY_SET;
		protected java.lang.Double[] Valor
		{
			get { return valor; }
			set { if (value != null) valor = value; }
		}

		protected string[] observacao = Constants.STR_EMPTY_SET;
		protected string[] Observacao
		{
			get { return observacao; }
			set { if (value != null) observacao = value; }
		}

		protected java.lang.Boolean dissociar = Constants.FALSE;
		protected java.lang.Boolean Dissociar
		{
			get { return dissociar; }
			set { if (value != null) dissociar = value; }
		}

        protected Integer[] usuarioID = Constants.INT_EMPTY_SET;
        protected Integer[] UsuarioID
        {
            get { return usuarioID; }
            set { if (value != null) usuarioID = value; }
        }

		protected DataSet FinAcaoMarketingAssociar()
		{
			string acumulado = "";
			ProcedureParameters[] param = new ProcedureParameters[9];

			param[0] = new ProcedureParameters("@AcaoID", SqlDbType.Int, System.DBNull.Value);
			param[1] = new ProcedureParameters("@AcaPatrocinioID", SqlDbType.Int, System.DBNull.Value);
			param[2] = new ProcedureParameters("@FinAcaoID", SqlDbType.Int, System.DBNull.Value);
			param[3] = new ProcedureParameters("@FinanceiroID", SqlDbType.Int, System.DBNull.Value);
			param[4] = new ProcedureParameters("@Valor", SqlDbType.Decimal, System.DBNull.Value);
			param[5] = new ProcedureParameters("@Observacao", SqlDbType.VarChar, System.DBNull.Value);
			param[5].Length = 30;
			param[6] = new ProcedureParameters("@Dissociar", SqlDbType.Bit, System.DBNull.Value);
            param[7] = new ProcedureParameters("@UsuarioID", SqlDbType.Int, System.DBNull.Value);
			param[8] = new ProcedureParameters("@Resultado", SqlDbType.VarChar, System.DBNull.Value, ParameterDirection.InputOutput);
			param[8].Length = 8000;
			
			for(int i = 0; i < acaoID.Length; i++)
			{
				param[0].Data = acaoID[i].intValue();
				
				param[1].Data = acaPatrocinioID == Constants.INT_EMPTY_SET || 
					acaPatrocinioID[i] == null ? 
						System.DBNull.Value :
						(Object)acaPatrocinioID[i].intValue();
						
				param[2].Data = finAcaoID == Constants.INT_EMPTY_SET || 
					finAcaoID[i] == null ?
						System.DBNull.Value :
						(Object)finAcaoID[i].intValue();
						
				param[3].Data = financeiroID == Constants.INT_EMPTY_SET || 
					financeiroID[i] == null ? 
						System.DBNull.Value : 
						(Object)financeiroID[i].intValue();
						
				param[4].Data = valor == Constants.DBL_EMPTY_SET || 
					valor[i] == null ?
						(Object)System.DBNull.Value : 
						valor[i].doubleValue();
						
				param[5].Data = observacao == Constants.STR_EMPTY_SET || 
					observacao[i] == null || observacao[i] == "" ?
						(Object)System.DBNull.Value : 
						observacao;
						
				param[6].Data = dissociar.booleanValue() ? 1 : 0;

                param[7].Data = usuarioID == Constants.INT_EMPTY_SET ||
                    usuarioID[i] == null ?
                        System.DBNull.Value :
                        (Object)usuarioID[i].intValue();
						
				param[8].Data = "";

				DataInterfaceObj.execNonQueryProcedure("sp_FinAcaoMarketing_Associar", param);
				
				acumulado += param[8].Data.ToString();
			}

			return DataInterfaceObj.getRemoteData("select '" + acumulado + "' as Resultado");
		}
	}
}
