using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modmarketing.subcampanhas.acoesmarketingEx.serverside
{
	public partial class verificacaoacoesmarketing : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(AcoesMarketingVerifica());
		}

		protected Integer acaoID = Constants.INT_ZERO;
		protected Integer nAcaoID
		{
			get { return acaoID; }
			set { if (value != null) acaoID = value; }
		}

		protected Integer currEstadoID = Constants.INT_ZERO;
		protected Integer nCurrEstadoID
		{
			get { return currEstadoID; }
			set { if (value != null) currEstadoID = value; }
		}

		protected Integer newEstadoID = Constants.INT_ZERO;
		protected Integer nNewEstadoID
		{
			get { return newEstadoID; }
			set { if (value != null) newEstadoID = value; }
		}

		protected Integer userID = Constants.INT_ZERO;
		protected Integer nUserID
		{
			get { return userID; }
			set { if (value != null) userID = value; }
		}


		protected DataSet AcoesMarketingVerifica()
		{
			ProcedureParameters[] param = new ProcedureParameters[] {
			    new ProcedureParameters("@AcaoID", SqlDbType.Int, acaoID.intValue()),
                new ProcedureParameters("@EstadoDeID", SqlDbType.Int,currEstadoID.intValue()),
                new ProcedureParameters("@EstadoParaID", SqlDbType.Int,newEstadoID.intValue()),
                new ProcedureParameters("@Data", SqlDbType.DateTime,System.DBNull.Value),
                new ProcedureParameters("@UsuarioID", SqlDbType.Int,userID.intValue()),
				new ProcedureParameters("@Resultado", SqlDbType.VarChar,System.DBNull.Value, ParameterDirection.InputOutput, 8000)
			};

			// Roda a procedure.
			DataInterfaceObj.execNonQueryProcedure("sp_AcoesMarketing_Verifica", param);
				
			// Gera o resultado.
			return DataInterfaceObj.getRemoteData("select " +
				(param[5].Data != DBNull.Value ? ("'" + param[5].Data.ToString() + "'") : " NULL ") + " as Resultado"
			);
		}
	}
}
