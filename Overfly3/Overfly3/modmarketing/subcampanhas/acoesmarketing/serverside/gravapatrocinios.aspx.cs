using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modmarketing.subcampanhas.acoesmarketingEx.serverside
{
	public partial class gravapatrocinios : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(Resultado());
		}

		protected Integer[] acaPatrocinioID = Constants.INT_EMPTY_SET;
		protected Integer[] AcaPatrocinioID
		{
			get { return acaPatrocinioID; }
			set { if (value != null) acaPatrocinioID = value; }
		}

		protected Integer[] acaoID = Constants.INT_EMPTY_SET;
		protected Integer[] AcaoID
		{
			get { return acaoID; }
			set { if (value != null) acaoID = value; }
		}

		protected Integer[] marca = Constants.INT_EMPTY_SET;
		protected Integer[] Marca
		{
			get { return marca; }
			set { if (value != null) marca = value; }
		}

		protected Integer[] iniciativa = Constants.INT_EMPTY_SET;
		protected Integer[] Iniciativa
		{
			get { return iniciativa; }
			set { if (value != null) iniciativa = value; }
		}

		protected Integer[] prazo = Constants.INT_EMPTY_SET;
		protected Integer[] Prazo
		{
			get { return prazo; }
			set { if (value != null) prazo = value; }
		}

		protected java.lang.Double[] valorPatrocinio = Constants.DBL_EMPTY_SET;
		protected java.lang.Double[] ValorPatrocinio
		{
			get { return valorPatrocinio; }
			set { if (value != null) valorPatrocinio = value; }
		}

		protected string[] observacao = Constants.STR_EMPTY_SET;
		protected string[] Observacao
		{
			get { return observacao; }
			set { if (value != null) observacao = value; }
		}

		protected Integer[] tipo = Constants.INT_EMPTY_SET;
		protected Integer[] Tipo
		{
			get { return tipo; }
			set { if (value != null) tipo = value; }
		}

        protected Integer[] empresaID = Constants.INT_EMPTY_SET;
        protected Integer[] EmpresaID
        {
            get { return empresaID; }
            set { if (value != null) empresaID = value; }
        }

		protected DataSet Resultado()
		{
			string sql = "";
			int resultado = 0;
			
			for(int i = 0; i < acaoID.Length; i++)
			{
				// Monta o SQL.
				if(prazo[i] == null) 
				{
					prazo[i] = Constants.INT_ZERO;
				}
				
				if(iniciativa[i] == null) 
				{
					iniciativa[i] = Constants.INT_ZERO;
				}

				if(observacao[i] == null || observacao[i].Equals(""))
				{
					observacao[i] = "NULL";
				}
				else
				{
					observacao[i] = "'" + observacao[i] + "'";
				}
				
				if(tipo[i].intValue() == 1)
				{
					sql += " DELETE AcoesMarketing_Patrocinios " +
						"WHERE AcaPatrocinioID =" + acaPatrocinioID[i].intValue();
				}
				else if(tipo[i].intValue() == 2)
				{
					sql += " INSERT INTO AcoesMarketing_Patrocinios " +
						"SELECT " + acaoID[i].intValue() + "," +
							marca[i].intValue() + "," +
                            "dbo.fn_Marca_Responsavel(" + marca[i].intValue() + ", " + empresaID[i].intValue() + ", 1, 0), " +
							iniciativa[i].intValue() + "," +
							valorPatrocinio[i].doubleValue() + "," +
							prazo[i].intValue() + "," +
							observacao[i];
				}
				else if (tipo[i].intValue() == 3)
				{
					sql += " UPDATE AcoesMarketing_Patrocinios SET " +
						"MarcaID = " + marca[i].intValue() + ", " +
						"Iniciativa = " + iniciativa[i].intValue() + ", " +
						"ValorPatrocinio = " + valorPatrocinio[i].doubleValue() + ", " + 
						"Prazo = " + prazo[i].intValue() + ", " +
						"Observacao = " + observacao[i] + " " +
					"WHERE AcaPatrocinioID = " + acaPatrocinioID[i].intValue();
				}
			}

			resultado = DataInterfaceObj.ExecuteSQLCommand(sql);

			return DataInterfaceObj.getRemoteData("select " + resultado + " as Resultado");
		}
	}
}
