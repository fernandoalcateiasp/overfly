using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modmarketing.subcampanhas.acoesmarketingEx.serverside
{
	public partial class associlarlistagemacoes : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(AcoesMarketingPesquisa());
		}

		protected Integer empresaID = Constants.INT_ZERO;
		protected Integer EmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		protected Integer pageSize = Constants.INT_ZERO;
		protected Integer PageSize
		{
			get { return pageSize; }
			set { if(value != null) pageSize = value; }
		}

		protected java.lang.Boolean pendentes = Constants.FALSE;
		protected java.lang.Boolean Pendentes
		{
			get { return pendentes; }
			set { if(value != null) pendentes = value; }
		}

		protected string tipoAcaoID = Constants.STR_EMPTY;
		protected string TipoAcaoID
		{
			get { return tipoAcaoID; }
			set { if(value != null) tipoAcaoID = value; }
		}

		protected Integer marcaID = Constants.INT_ZERO;
		protected Integer MarcaID
		{
			get { return marcaID; }
			set { if(value != null) marcaID = value; }
		}

		protected string inicio = Constants.STR_EMPTY;
		protected string dtInicio
		{
			get { return inicio; }
			set { if(value != null) inicio = value; }
		}

		protected string fim = Constants.STR_EMPTY;
		protected string dtFim
		{
			get { return fim; }
			set { if(value != null) fim = value; }
		}

		protected string pesquisa = Constants.STR_EMPTY;
		protected string Pesquisa
		{
			get { return pesquisa; }
			set { if(value != null) pesquisa = value; }
		}

		protected string tipoFinanceiroID = Constants.STR_EMPTY;
		protected string TipoFinanceiroID
		{
			get { return tipoFinanceiroID; }
			set { if(value != null) tipoFinanceiroID = value; }
		}

		protected java.lang.Boolean dissociar = Constants.FALSE;
		protected java.lang.Boolean Dissociar
		{
			get { return dissociar; }
			set { if(value != null) dissociar = value; }
		}
		
		protected DataSet AcoesMarketingPesquisa()
		{
			ProcedureParameters[] param = new ProcedureParameters[10];

			param[0] = new ProcedureParameters("@EmpresaID", SqlDbType.Int, empresaID.intValue());
			param[1] = new ProcedureParameters("@Top", SqlDbType.Int, pageSize.intValue());
			param[2] = new ProcedureParameters("@Pendentes", SqlDbType.Bit, pendentes.booleanValue() ? 1 : 0);
			param[3] = new ProcedureParameters("@TipoAcaoID", SqlDbType.Int, 
				tipoAcaoID.Equals("") ? System.DBNull.Value : (Object)tipoAcaoID);
			param[4] = new ProcedureParameters("@MarcaID", SqlDbType.Int,
				marcaID.ToString() == "0" ? System.DBNull.Value : (Object)marcaID.intValue());
			param[5] = new ProcedureParameters("@dtInicio", SqlDbType.DateTime,
				inicio.Equals("") ? System.DBNull.Value : (Object)inicio.ToString());
			param[6] = new ProcedureParameters("@dtFim", SqlDbType.DateTime,
				fim.Equals("") ? System.DBNull.Value : (Object)fim.ToString());
			param[7] = new ProcedureParameters("@Pesquisa", SqlDbType.VarChar,
				pesquisa.Equals("") ? System.DBNull.Value : (Object)pesquisa);
			param[7].Length = 255;
			param[8] = new ProcedureParameters("@TipoFinanceiroID", SqlDbType.Int,
				tipoFinanceiroID.Equals("") ? System.DBNull.Value : (Object)tipoFinanceiroID);
			param[9] = new ProcedureParameters("@Dissociar", SqlDbType.Bit, dissociar.booleanValue() ? 1 : 0);

			return DataInterfaceObj.execQueryProcedure("sp_AcoesMarketing_Pesquisa", param);
		}
		
	}
}
