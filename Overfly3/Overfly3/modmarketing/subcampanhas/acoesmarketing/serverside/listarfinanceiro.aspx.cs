using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.modmarketing.subcampanhas.acoesmarketingEx.serverside
{
	public partial class listarfinanceiro : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(FinanceiroDespesas());
		}

		protected Integer empresaID = Constants.INT_ZERO;
        private java.lang.Boolean False = new java.lang.Boolean(false);

		protected Integer EmpresaID
		{
			get { return empresaID; }
			set { if (value != null) empresaID = value; }
		}

		protected Integer tipoFinanceiroID = Constants.INT_ZERO;
		protected Integer TipoFinanceiroID
		{
			get { return tipoFinanceiroID; }
			set { if (value != null) tipoFinanceiroID = value; }
		}

        protected java.lang.Boolean baixaCampanhas;
        protected java.lang.Boolean BaixaCampanhas
        {
            get { return baixaCampanhas; }
            set { baixaCampanhas = value != null ? value : False; }
        }

		protected string inicio = Constants.STR_EMPTY;
		protected string dtInicio
		{
			get { return inicio; }
			set { if (value != null) inicio = value; }
		}

		protected string fim = Constants.STR_EMPTY;
		protected string dtFim
		{
			get { return fim; }
			set { if (value != null) fim = value; }
		}

		protected string tiposDespesa = Constants.STR_EMPTY;
		protected string TiposDespesa
		{
			get { return tiposDespesa; }
			set { if (value != null) tiposDespesa = value; }
		}

		protected string origens = Constants.STR_EMPTY;
		protected string Origens
		{
			get { return origens; }
			set { if (value != null) origens = value; }
		}

		protected string familias = Constants.STR_EMPTY;
		protected string Familias
		{
			get { return familias; }
			set { if (value != null) familias = value; }
		}

		protected string marcas = Constants.STR_EMPTY;
		protected string Marcas
		{
			get { return marcas; }
			set { if (value != null) marcas = value; }
		}

        protected string classPessoas = Constants.STR_EMPTY;
        protected string ClassPessoas
        {
            get { return classPessoas; }
            set { if (value != null) classPessoas = value; }
        }

        protected string produtos = Constants.STR_EMPTY;
        protected string Produtos
        {
            get { return produtos; }
            set { if (value != null) produtos = value; }
        }

		protected string excecoes = Constants.STR_EMPTY;
		protected string Excecoes
		{
			get { return excecoes; }
			set { if (value != null) excecoes = value; }
		}

		protected Integer tipoResultadoID = Constants.INT_ZERO;
		protected Integer TipoResultadoID
		{
			get { return tipoResultadoID; }
			set { if (value != null) tipoResultadoID = value; }
		}

		protected java.lang.Double valorMaximo = Constants.DBL_ZERO;
		protected java.lang.Double ValorMaximo
		{
			get { return valorMaximo; }
			set { if (value != null) valorMaximo = value; }
		}

		protected string observacao = Constants.STR_EMPTY;
		protected string Observacao
		{
			get { return observacao; }
			set { if (value != null) observacao = value; }
		}

		protected Integer proprietarioID = Constants.INT_ZERO;
		protected Integer ProprietarioID
		{
			get { return proprietarioID; }
			set { if (value != null) proprietarioID = value; }
		}

		protected Integer financeiroID = Constants.INT_ZERO;
		protected Integer FinanceiroID
		{
			get { return financeiroID; }
			set { if (value != null) financeiroID = value; }
		}

        private string resultado = Constants.STR_EMPTY;
        private string financeiro = Constants.STR_EMPTY;

        protected DataSet FinanceiroDespesas()
		{
			ProcedureParameters[] param = new ProcedureParameters[18];

			param[0] = new ProcedureParameters("@EmpresaID", SqlDbType.Int, 
				empresaID.intValue());
				
			param[1] = new ProcedureParameters("@TipoFinanceiroID", SqlDbType.Int, 
				tipoFinanceiroID.intValue());

            param[2] = new ProcedureParameters("@BaixaCampanhas", System.Data.SqlDbType.Bit,
                baixaCampanhas.booleanValue()); // ? "1" : "0");
				
			param[3] = new ProcedureParameters("@dtInicio", SqlDbType.DateTime, null);
			try
			{
				param[3].Data = (Object)DateTime.Parse(
					inicio, 
					System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat);
			}
			catch(System.Exception)
			{
				param[3].Data = DBNull.Value;
			}
			
			param[4] = new ProcedureParameters("@dtFim", SqlDbType.DateTime, null);
			try
			{
				param[4].Data = (Object)DateTime.Parse(
					fim,
					System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat);
			}
			catch (System.Exception)
			{
				param[4].Data = DBNull.Value;
			}
            
			param[5] = new ProcedureParameters("@TiposDespesa", SqlDbType.VarChar,
				tiposDespesa != null ? (Object)tiposDespesa : System.DBNull.Value);
			param[5].Length = 8000;

			param[6] = new ProcedureParameters("@Origem", SqlDbType.VarChar,
				!origens.Equals(Constants.STR_EMPTY) ? (Object)origens : System.DBNull.Value);
			param[6].Length = 8000;

			param[7] = new ProcedureParameters("@Familias", SqlDbType.VarChar,
				!familias.Equals(Constants.STR_EMPTY) ? (Object)familias : System.DBNull.Value);
			param[7].Length = 8000;

			param[8] = new ProcedureParameters("@Marcas", SqlDbType.VarChar,
				!marcas.Equals(Constants.STR_EMPTY) ? (Object)marcas : System.DBNull.Value);
			param[8].Length = 8000;

            param[9] = new ProcedureParameters("@Produtos", SqlDbType.VarChar,
                !produtos.Equals(Constants.STR_EMPTY) ? (Object)produtos : System.DBNull.Value);
            param[9].Length = 8000;

            param[10] = new ProcedureParameters("@ClassPessoas", SqlDbType.VarChar,
                !classPessoas.Equals(Constants.STR_EMPTY) ? (Object)classPessoas : System.DBNull.Value);
            param[10].Length = 8000;

			param[11] = new ProcedureParameters("@TiposExcecao", SqlDbType.VarChar,
				!excecoes.Equals(Constants.STR_EMPTY) ? (Object)excecoes : System.DBNull.Value);
			param[11].Length = 8000;

			param[12] = new ProcedureParameters("@TipoResultadoID", SqlDbType.Int,
				!tipoResultadoID.Equals(Constants.STR_EMPTY) ? (Object)tipoResultadoID.intValue() : System.DBNull.Value);

			param[13] = new ProcedureParameters("@ValorMaximo", SqlDbType.Decimal,
                ((valorMaximo != null) && (valorMaximo.longValue() != 0.0)) ? (Object)valorMaximo.doubleValue() : System.DBNull.Value);

			param[14] = new ProcedureParameters("@Observacao", SqlDbType.VarChar,
				!observacao.Equals(Constants.STR_EMPTY) ? (Object)observacao : System.DBNull.Value);
			param[14].Length = 30;

			param[15] = new ProcedureParameters("@ProprietarioID", SqlDbType.Int,
				proprietarioID != null ? (Object)proprietarioID.intValue() : System.DBNull.Value);

			param[16] = new ProcedureParameters("@FinanceiroID", SqlDbType.Int, 
				System.DBNull.Value, ParameterDirection.InputOutput);
				
			param[17] = new ProcedureParameters("@Resultado", SqlDbType.VarChar,
				System.DBNull.Value, ParameterDirection.InputOutput);
			param[17].Length = 8000;
            
            
            //if (((resultado != "") && (resultado != null)) || ((financeiro != "0") && (financeiro != "") && (financeiro != null)))
            if (tipoResultadoID.intValue() == 2)
            {
                DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_Despesas", param);

                financeiro = param[15].Data != DBNull.Value ? param[15].Data.ToString() : "0";
                resultado = param[16].Data != DBNull.Value ? param[16].Data.ToString() : "";

                // Gera o resultado.
                return DataInterfaceObj.getRemoteData("select " +
                    financeiro + " as FinanceiroID, " +
                    "'" + resultado + "'" + " as Resultado"
                );
            }
            else
                return DataInterfaceObj.execQueryProcedure("sp_Financeiro_Despesas", param);
		}
	}
}