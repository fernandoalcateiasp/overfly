/********************************************************************
superioresp.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
			   '(SELECT a.Observacao FROM TiposAuxiliares_Itens a WITH(NOLOCK) WHERE a.ITemID = TipoAcaoID) AS GrupoAcao, ' +
			   'CONVERT(VARCHAR, dtInicio, '+DATE_SQL_PARAM+') as V_dtInicio,' +
			   'CONVERT(VARCHAR, dtFim, '+DATE_SQL_PARAM+') as V_dtFim,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 1, GETDATE()) AS TotalPatrocinio,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 2, GETDATE()) AS Diferenca,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 3, GETDATE()) AS TotalPago,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 4, GETDATE()) AS TotalRecebido,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 5, GETDATE()) AS SaldoPlanejado,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 6, GETDATE()) AS SaldoPatrocinio,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 7, GETDATE()) AS SaldoEfetivo,' +			   
			   'CONVERT(INT, dbo.fn_AcaoMarketing_Totais(AcaoID, 11, GETDATE())) AS Atraso,' +
			   'CONVERT(VARCHAR, dbo.fn_AcaoMarketing_Datas(AcaoID, 1), '+DATE_SQL_PARAM+') AS dtVencimento,' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=AcoesMarketing.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM AcoesMarketing WITH(NOLOCK) ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY AcaoID DESC';
    else
        sSQL = 'SELECT *, ' + 
			   '(SELECT a.Observacao FROM TiposAuxiliares_Itens a WITH(NOLOCK) WHERE a.ITemID = TipoAcaoID) AS GrupoAcao, ' +
			   'CONVERT(VARCHAR, dtInicio, '+DATE_SQL_PARAM+') as V_dtInicio,' +
			   'CONVERT(VARCHAR, dtFim, '+DATE_SQL_PARAM+') as V_dtFim,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 1, GETDATE()) AS TotalPatrocinio,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 2, GETDATE()) AS Diferenca,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 3, GETDATE()) AS TotalPago,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 4, GETDATE()) AS TotalRecebido,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 5, GETDATE()) AS SaldoPlanejado,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 6, GETDATE()) AS SaldoPatrocinio,' +
			   'dbo.fn_AcaoMarketing_Totais(AcaoID, 7, GETDATE()) AS SaldoEfetivo,' +
			   'CONVERT(INT, dbo.fn_AcaoMarketing_Totais(AcaoID, 11, GETDATE())) AS Atraso,' +
			   'CONVERT(VARCHAR, dbo.fn_AcaoMarketing_Datas(AcaoID, 1), '+DATE_SQL_PARAM+') AS dtVencimento,' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=AcoesMarketing.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM AcoesMarketing WITH(NOLOCK) WHERE AcaoID = ' + nID + ' ORDER BY AcaoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);

    var sql;

    sql = 'SELECT *, ' +
		  '(SELECT a.Observacao FROM TiposAuxiliares_Itens a WITH(NOLOCK) WHERE a.ITemID = TipoAcaoID) AS GrupoAcao, ' +
   		  'CONVERT(VARCHAR, dtInicio, '+DATE_SQL_PARAM+') as V_dtInicio,' +
		  'CONVERT(VARCHAR, dtFim, '+DATE_SQL_PARAM+') as V_dtFim,' +
		  'dbo.fn_AcaoMarketing_Totais(AcaoID, 1, GETDATE()) AS TotalPatrocinio,' +
		  'dbo.fn_AcaoMarketing_Totais(AcaoID, 2, GETDATE()) AS Diferenca,' +
		  'dbo.fn_AcaoMarketing_Totais(AcaoID, 3, GETDATE()) AS TotalPago,' +
		  'dbo.fn_AcaoMarketing_Totais(AcaoID, 4, GETDATE()) AS TotalRecebido,' +
		  'dbo.fn_AcaoMarketing_Totais(AcaoID, 5, GETDATE()) AS SaldoPlanejado,' +
		  'dbo.fn_AcaoMarketing_Totais(AcaoID, 6, GETDATE()) AS SaldoPatrocinio,' +
		  'dbo.fn_AcaoMarketing_Totais(AcaoID, 7, GETDATE()) AS SaldoEfetivo,' +
		  'CONVERT(INT, dbo.fn_AcaoMarketing_Totais(AcaoID, 11, GETDATE())) AS Atraso,' +
		  'CONVERT(VARCHAR, dbo.fn_AcaoMarketing_Datas(AcaoID, 1), '+DATE_SQL_PARAM+') AS dtVencimento,' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM AcoesMarketing WITH(NOLOCK) WHERE AcaoID = 0';

    dso.SQL = sql;
}
