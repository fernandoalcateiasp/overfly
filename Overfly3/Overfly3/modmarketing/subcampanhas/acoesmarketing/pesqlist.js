/********************************************************************
pesqlist.js

Library javascript para o pesquisa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Dados da listagem da pesquisa 
var dsoListData01 = new CDatatransport("dsoListData01");
// Dados dos combos de contexto e filtros 
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
// Dados dos proprietarios para o pesqlist .URL 
var dsoPropsPL = new CDatatransport("dsoPropsPL");
var dsoEMailParticipante = new CDatatransport("dsoEMailParticipante");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Grupo de A��o', 'Tipo de A��o', 'A��o', 'Detalhe', 'Per�odo','In�cio','Fim','Vencimento','Atraso',
		'Total Planejado','Total Patroc�nio','Diferen�a', 'Total Pago', 'Total Recebido', 'Saldo Planejado', 'Saldo Patroc�nio', 'Saldo Efetivo', 'Observa��o');

    var dTFormat = '';

    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '',dTFormat,dTFormat,dTFormat, '###', 
		'###,###,###.00','###,###,###.00','###,###,###.00','###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00','');
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	var nCurrRETID = 0;
	
	especBtnIsPrintBtn('sup', 1);
	showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1]);
	tipsBtnsEspecControlBar('sup', ['Documentos','Relat�rios', 'Procedimento', 'Gerar Financeiros', 'Associar Financeiros','Resumo da A��o de Marketing']);
	setupEspecBtnsControlBar('sup', 'HHHHHH');

    alignColsInGrid(fg,[0,10,11,12,13,14,15,16]);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_pesqlist.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
	else if ( btnClicked == 2 )
        openModalPrint(); 
         
	else if (btnClicked == 4)
		openModalGerarFinanceiros();
	
	else if (btnClicked == 5)
		openModalAssociarAcoes();
		
	else if (btnClicked == 6)
	{
		if (fg.Row > 0)
		{
			var nCurrAcaoID = getCellValueByColKey(fg, 'AcaoID', fg.Row);
			window.top.openModalHTML(nCurrAcaoID, 'A��o ' + nCurrAcaoID + ' - Resumo', 'PL', 10, 1);
		}
	}	
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em dois pontos do js_pesqlist.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
	if (idElement.toUpperCase() == 'MODALPRINTHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
	else if (idElement.toUpperCase() == 'MODALASSOCIARACOESHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERARFINANCEIROSHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOPENHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao goExecPesqShowList(formID) do js_pesqlist.js

Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{
    //@@ da automacao -> retorno padrao
    return null;
}

function openModalPrint()
{
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var empresaCidadeID = empresaData[2];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nEmpresaCidadeID=' + escape(empresaCidadeID);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&nIdiomaID=' + escape(getDicCurrLang());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modmarketing/subcampanhas/acoesmarketing/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(/*370*/465,390));
}

/********************************************************************
Criada pelo programador
Esta funcao abre a janela modal de conciliacao de extratos bancarios
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function openModalAssociarAcoes()
{
	var htmlPath;
    var strPars = new String();
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
	var nPageSize = selRegistros.value;   
    
    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('PL');   
    strPars += '&nEmpresaID=' + escape(nEmpresaID);   
    strPars += '&nPageSize=' + escape(nPageSize);   
               
    htmlPath = SYS_PAGESURLROOT + '/modmarketing/subcampanhas/acoesmarketing/modalpages/modalassociaracoes.asp'+strPars;
    showModalWin(htmlPath, new Array(771,462));
        
    // Prossegue a automacao
    return null;
}

function openModalGerarFinanceiros()
{
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    
    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modmarketing/subcampanhas/acoesmarketing/modalpages/modalgerarfinanceiros.asp'+strPars;
    showModalWin(htmlPath, new Array(775,460));
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************