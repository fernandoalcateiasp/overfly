
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalassociaracoesHtml" name="modalassociaracoesHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/acoesmarketing/modalpages/modalassociaracoes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais
Dim i, sCaller, nEmpresaID, nAcaoID, nTipoFinanceiroID, nFinanceiroID, nPageSize

sCaller = ""
nEmpresaID = 0
nAcaoID = 0
nFinanceiroID = 0
nTipoFinanceiroID = 0
nPageSize = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nAcaoID").Count    
    nAcaoID = Request.QueryString("nAcaoID")(i)
Next

For i = 1 To Request.QueryString("nTipoFinanceiroID").Count    
    nTipoFinanceiroID = Request.QueryString("nTipoFinanceiroID")(i)
Next

For i = 1 To Request.QueryString("nFinanceiroID").Count    
    nFinanceiroID = Request.QueryString("nFinanceiroID")(i)
Next

For i = 1 To Request.QueryString("nPageSize").Count    
    nPageSize = Request.QueryString("nPageSize")(i)
Next


Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " &  nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_nAcaoID = " &  nAcaoID & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoFinanceiroID = " &  nTipoFinanceiroID & ";"
Response.Write vbcrlf

Response.Write "var glb_nFinanceiroID = " &  nFinanceiroID & ";"
Response.Write vbcrlf

Response.Write "var glb_nPageSize = " &  nPageSize & ";"
Response.Write vbcrlf

'Necessario para compatibilidade da automacao
Response.Write "var glb_USERID = 0;"

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fgAcoesMarketing -->
<script LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=BeforeSort>
<!--
fg_ModConcExtrBeforeSort(fgAcoesMarketing, arguments[0]);
//-->
</script>
<script LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=AfterSort>
<!--
fg_ModConcExtrAfterSort(fgAcoesMarketing, arguments[0]);
//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_ModConcExtrAfterRowColChangePesqList (fgAcoesMarketing, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=BeforeRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_BeforeRowColChange (fgAcoesMarketing, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
js_fg_ModConcExtrBeforeRowColChange (fgAcoesMarketing, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=DblClick>
<!--
js_fg_ModConcExtrDblClick (fgAcoesMarketing, fgAcoesMarketing.Row, fgAcoesMarketing.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=EnterCell>
<!--
 js_fg_EnterCell (fgAcoesMarketing);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=BeforeEdit>
<!--
 js_modalassociaracoes_BeforeEdit(fgAcoesMarketing, fgAcoesMarketing.Row, fgAcoesMarketing.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=AfterEdit>
<!--
	fgAcoesMarketing_AfterEdit(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgAcoesMarketing EVENT=ValidateEdit>
<!--
	fgAcoesMarketing_ValidateEdit();
//-->
</SCRIPT>

<!-- fgFinanceiros -->
<script LANGUAGE=javascript FOR=fgFinanceiros EVENT=BeforeSort>
<!--
fg_ModConcExtrBeforeSort(fgFinanceiros, arguments[0]);
//-->
</script>

<script LANGUAGE=javascript FOR=fgFinanceiros EVENT=AfterSort>
<!--
fg_ModConcExtrAfterSort(fgFinanceiros, arguments[0]);
//-->
</script>
<SCRIPT LANGUAGE=javascript FOR=fgFinanceiros EVENT=AfterRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_ModConcExtrAfterRowColChangePesqList (fgFinanceiros, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgFinanceiros EVENT=BeforeRowColChange>
<!--
glb_HasGridButIsNotFormPage = true;
js_fg_BeforeRowColChange (fgFinanceiros, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
js_fg_ModConcExtrBeforeRowColChange (fgFinanceiros, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgFinanceiros EVENT=DblClick>
<!--
js_fg_ModConcExtrDblClick (fgFinanceiros, fgFinanceiros.Row, fgFinanceiros.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fgFinanceiros EVENT=EnterCell>
<!--
 js_fg_EnterCell (fgFinanceiros);
//-->
</SCRIPT>

</head>

<body id="modalassociaracoesBody" name="modalassociaracoesBody" LANGUAGE="javascript" onload="return window_onload()">
	<div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

	<div id="divPesquisaAcao" name="divPesquisaAcao" class="divGeneral">
		<p id="lblSoAcaoPendente" name="lblSoAcaoPendente" class="lblGeneral">P</p>
		<input type="checkbox" id="chkSoAcaoPendente" name="chkSoAcaoPendente" class="fldGeneral" title="S� a��es pendentes?"></input>
		<p id="lblEmpresa1" name="lblEmpresa1" class="lblGeneral">Empresa</p>
		<select id="selEmpresa1" name="selEmpresa1" class="fldGeneral">
<%
	Dim strSQL, rsData

	Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT b.PessoaID AS EmpresaID, b.Fantasia AS Empresa " & _
        "FROM RelacoesPesRec a WITH (NOLOCK) INNER JOIN Pessoas b WITH (NOLOCK) ON a.SujeitoID=b.PessoaID " & _
        "WHERE a.ObjetoID=999 AND a.TipoRelacaoID=12 AND a.EstadoID=2 AND b.EstadoID=2 AND a.EstaEmOperacao=1 " & _
        "ORDER BY EmpresaID"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("EmpresaID").Value & "'" )
			
            If (CLng(nEmpresaID) = CLng(rsData.Fields("EmpresaID").Value)) Then
                Response.Write(" SELECTED ")
            End If
            
            Response.Write(">" & rsData.Fields("Empresa").Value & "</option>")
			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>	
		</select>
		<p id="lblTipoAcao" name="lblTipoAcao" class="lblGeneral">Tipo de a��o</p>
		<select id="selTipoAcao" name="selTipoAcao" class="fldGeneral">
<%
    strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem " & _ 
			 "UNION ALL " & _
			 "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
			 "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			 "WHERE (EstadoID=2 AND TipoID=304) " & _
			 "ORDER BY Ordem"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>				
		</select>
		<p id="lblPesquisaAcao" name="lblPesquisaAcao" class="lblGeneral">Pesquisa</p>
		<input type="text" id="txtPesquisaAcao" name="txtPesquisaAcao" class="fldGeneral"></input>
		<p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
		<select id="selMarca" name="selMarca" class="fldGeneral">
<%
    strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _ 
			 "UNION ALL " & _
			 "SELECT DISTINCT Marcas.ConceitoID AS fldID, Marcas.Conceito AS fldName " & _
			 "FROM Conceitos Marcas WITH(NOLOCK) " & _
			 "INNER JOIN AcoesMarketing_Patrocinios AMP WITH(NOLOCK) ON Marcas.ConceitoID = AMP.MarcaID  " & _			 
			 "INNER JOIN AcoesMarketing AM WITH(NOLOCK) ON AM.AcaoID = AMP.AcaoID  " & _			 
			 "WHERE (Marcas.EstadoID = 2 AND Marcas.TipoConceitoID = 304) " & _
			 "ORDER BY fldName"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>					
		</select>		
		<p id="lbldtAcaoInicio" name="lbldtAcaoInicio" class="lblGeneral">Data in�cio</p>
		<input type="text" id="txtdtAcaoInicio" name="txtdtAcaoInicio" class="fldGeneral"></input>
		<p id="lbldtAcaoFim" name="lbldtAcaoFim" class="lblGeneral">Data fim</p>
		<input type="text" id="txtdtAcaoFim" name="txtdtAcaoFim" class="fldGeneral"></input>
		<p id="lblEmpresa2" name="lblEmpresa2" class="lblGeneral">Empresa</p>
		<select id="selEmpresa2" name="selEmpresa2" class="fldGeneral"></select>
		<p id="lblTipoFinanceiro" name="lblTipoFinanceiro" class="lblGeneral">Tipo financeiro</p>
		<select id="selTipoFinanceiro" name="selTipoFinanceiro" class="fldGeneral">
<%
    strSQL = "SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName " & _
			 "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _			 
			 "WHERE a.TipoID = 801 " & _
			 "ORDER BY a.Ordem "

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>							
		</select>
    </div>    
	
	<div id="divPesquisaFinanceiro" name="divPesquisaFinanceiro" class="divGeneral">
		<p  id="lblSoFinanceiroPendente" name="lblSoFinanceiroPendente" class="lblGeneral">P</p>
		<input type="checkbox" id="chkSoFinanceiroPendente" name="chkSoFinanceiroPendente" class="fldGeneral" title="S� financeiros pendentes?"></input>
		<p id="lblHistoricoPadrao" name="lblHistoricoPadrao" class="lblGeneral">Hist�rico padr�o</p>
		<select id="selHistoricoPadrao" name="selHistoricoPadrao" class="fldGeneral">
<%
    strSQL ="SELECT 0 AS fldID, SPACE(0) AS fldName " & _ 
			"UNION ALL " & _
			"SELECT DISTINCT a.HistoricoPadraoID AS fldID, a.HistoricoPadrao AS fldName " & _
			 "FROM HistoricosPadrao a WITH(NOLOCK) " & _			 
			 "WHERE a.Marketing = 1 " & _
			 "ORDER BY fldName "

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	Set rsData = Nothing
%>					
		</select>
		<p id="lblPesquisaFinanceiro" name="lblPesquisaFinanceiro" class="lblGeneral">Pesquisa</p>
		<input type="text" id="txtPesquisaFinanceiro" name="txtPesquisaFinanceiro" class="fldGeneral"></input>
		<p id="lbldtFinanceiroInicio" name="lbldtFinanceiroInicio" class="lblGeneral">Data in�cio</p>
		<input type="text" id="txtdtFinanceiroInicio" name="txtdtFinanceiroInicio" class="fldGeneral"></input>
		<p id="lbldtFinanceiroFim" name="lbldtFinanceiroFim" class="lblGeneral">Data fim</p>
		<input type="text" id="txtdtFinanceiroFim" name="txtdtFinanceiroFim" class="fldGeneral"></input>
	</div>    
	
	
	<div id="divAcaoMarketing" name="divAcaoMarketing" class="divGeneral">
		<p id="lblAcaoMarketing" name="lblAcaoMarketing" class="lblGeneral">A��es de Marketing</p>
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgAcoesMarketing" name="fgAcoesMarketing" class="fldGeneral" VIEWASTEXT>
        </object>
    </div>    
    
    <div id="divFinanceiro" name="divFinanceiro" class="divGeneral">
		<p id="lblFinanceiro" name="lblFinanceiro" class="lblGeneral">Financeiros</p>
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgFinanceiros" name="fgFinanceiros" class="fldGeneral" VIEWASTEXT>
        </object>
    </div>    
    
    <div id="divControles" name="divControles" class="divGeneral">
   		<p  id="lblDissociar" name="lblDissociar" class="lblGeneral">Dissociar</p>
		<input type="checkbox" id="chkDissociar" name="chkDissociar" class="fldGeneral" title="Dissociar?"></input>
		<input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
		<input type="button" id="btnAssociar" name="btnAssociar" value="Associar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
	</div>	
	<input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	<input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
</body>

</html>
