/********************************************************************
modalassociaracoes.js

Library javascript para o modalassociaracoes.asp
Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_CounterCmbsDynamics = 0;
var glb_FirstTime = false;
var glb_LASTLINESELID = null;
var glb_PassedOneInDblClick = false;
var glb_nA1 = 0;
var glb_nA2 = 0;

var dsoAcaoMarketing = new CDatatransport("dsoAcaoMarketing");
var dsoFinanceiro = new CDatatransport("dsoFinanceiro");
var dsoGravar = new CDatatransport("dsoGravar");
var dsoEmpresas = new CDatatransport("dsoEmpresas");
	

/********************************************************************
Objeto controlador do carregamento dos dsos de grids e
preenchimento destes dsos 

Instanciado pela variavel glb_gridControlFill
********************************************************************/
function __gridControlFill()
{
	this.bFollowOrder = false;
    this.nDsosToArrive = 0;
    this.dso1_Ref = null;
    this.dso2_Ref = null;
    this.grid1_Ref = null;
    this.grid2_Ref = null;
}

var glb_gridControlFill = new __gridControlFill();

/********************************************************************
Objeto controlador de execucao dos eventos dos grids e de grid com
linha de totais

Nota:
nColDataType = 0 (String), 1 (Numerico), 2 (Data), 3 (Boolean)
ncolUsedToSort = -1 nenhuma coluna
ncolOrderUsedToSort = 1 ascendente, -1 descendente

Instanciado pela variavel glb_gridsEvents
********************************************************************/

function __resetOrderParams()
{
	this.ncolUsedToSort_fgAcoesMarketing = -1;
	this.ncolOrderUsedToSort_fgAcoesMarketing = 0;
	this.ncolUsedToSort_fgLacContas = -1;
	this.ncolOrderUsedToSort_fgFinanceiros = 0;
}

function __gridsEvents()
{
	this.bTotalLine_fgAcoesMarketing = false;
	this.bTotalLine_fgFinanceiros = false;
	this.bBlockEvents_fgAcoesMarketing = false;
	this.bBlockEvents_fgFinanceiros = false;
	this.ncolUsedToSort_fgAcoesMarketing = -1;
	this.ncolOrderUsedToSort_fgAcoesMarketing = 0;
	this.ncolUsedToSort_fgLacContas = -1;
	this.ncolOrderUsedToSort_fgFinanceiros = 0;
	
	this.resetOrderParams = __resetOrderParams;
}

var glb_gridsEvents = new __gridsEvents();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
   
window_onload()
btn_onclick(ctl)
setupPage()
adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder, mode)
startDynamicGrids()
startDynamicGrids_DSC()
showModalAfterDataLoad()

cmbOnChange(cmb)
chkOnClick()

js_fg_ModConcExtrBeforeRowColChange(grid, oldRow, oldCol, newRow, newCol)
fg_ModConcExtrBeforeSort(grid, col)
js_modalassociaracoes_BeforeEdit(grid, row, col)
fg_ModConcExtrAfterSort(grid, col)
js_fg_ModConcExtrDblClick(grid, row, col)

lineChangedInGrid_Extrato(grid, nRow)

gridSortParams(grid, col)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    glb_nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B4A1' + '\'' + ')') );
    
    glb_nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B4A2' + '\'' + ')') );

    // configuracao inicial do html
    setupPage();
    
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = 
		(getFrameInHtmlTop( 'frameSup01')).offsetTop;

	// ajusta o body do html
    with (modalassociaracoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    adjustLabelsCombos();
    chkDissociar_onclick();
    showExtFrame(window, true);
    
    window.focus();
    
    txtdtAcaoInicio.value = '01/09/2007';
    
    if (glb_nAcaoID != 0)
	{
		txtPesquisaAcao.value = glb_nAcaoID;
		//startDynamicGrids();
	}
	if (glb_nFinanceiroID != 0)
	{
		if (glb_nTipoFinanceiroID != 0)
			selTipoFinanceiro.value = glb_nTipoFinanceiroID;
			
		txtPesquisaFinanceiro.value = glb_nFinanceiroID;		
		//startDynamicGrids();
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    
    if (ctl.id == btnOK.id )
        btnOK.focus();
    //else if (ctl.id == btnCanc.id )
    //    btnCanc.focus();
    
    if (ctl.id == btnListar.id )
    {
		startDynamicGrids();
    }
    
    else if (ctl.id == btnAssociar.id )
    {
		gravar();
    }
    
    else if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
    	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
}

function txtPesquisaAcao_onkeypress()
{
	 if ( event.keyCode == 13 )
    {
        startDynamicGrids();
    }
}

function txtPesquisaFinanceiro_onkeypress()
{
	 if ( event.keyCode == 13 )
    {
        startDynamicGrids();
    }
}
/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	// btnOK nao e usado nesta modal
	with (btnOK)
	{
		disabled = true;
		style.visibility = 'hidden';
	}
	
	// btnCanc nao e usado nesta modal
	with (btnCanc)
	{
		style.visibility = 'hidden';
	}
	
	// texto da secao01
	secText('Associar A��es de Marketing' + (glb_nAcaoID != 0 ? ' (' + glb_nAcaoID + ')' : ''), 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 5;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    // heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    // altura livre da janela modal (descontando apenas a barra azul)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - (2 * frameBorder);
    
    adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder);
}

/********************************************************************
Posiciona os campos na interface, define labels e hints,
controla digitacao em campos num�ricos, define eventos associados
a controles, etc

Parametros:
	topFree	- inicio da altura da janela (posicao onde termina o div do
	          titulo da janela )
	heightFree - altura livre da janela (do top free ate o botao OK,
	          supondo o btnOK em baixo, que e o padrao de modal)
	widthFree - largura livre da janela
	frameBorder - largura da borda da janela
		
Retorno:
	nenhum
********************************************************************/
function adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder)
{
	var elem;
	var x_leftWidth = 0;
	var y_topHeight = 0;
	
	var elemLbl;
	var elemGrid;
	var lbl_height = 16;
	var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
	var btn_height = parseInt(btnOK.currentStyle.height, 10);
	var nDivControlsHeight = 0;

	// Posiciona os controles do divPesquisaAcao
	adjustElementsInForm([['lblSoAcaoPendente','chkSoAcaoPendente',3,1,-10],
	                      ['lblEmpresa1','selEmpresa1',13,1,-9],
						  ['lblTipoAcao','selTipoAcao',13,1,-4],
						  ['lblMarca','selMarca',16,1,-5],
						  ['lblEmpresa2','selEmpresa2',13,1,15],
						  ['lblTipoFinanceiro','selTipoFinanceiro',10,1,-2],
						  ['lbldtAcaoInicio','txtdtAcaoInicio',10,2,-10],
						  ['lbldtAcaoFim','txtdtAcaoFim',10,2],	  						  
						  ['lblPesquisaAcao','txtPesquisaAcao',24,2,-2]], null, null, true);

	// Posiciona os controles do divPesquisaFinanceiro						  
	adjustElementsInForm([['lblSoFinanceiroPendente','chkSoFinanceiroPendente',3,1,-10],
						  ['lblHistoricoPadrao','selHistoricoPadrao',19,1,184],		  
						  ['lbldtFinanceiroInicio','txtdtFinanceiroInicio',10,2,-10],
						  ['lbldtFinanceiroFim','txtdtFinanceiroFim',10,2],
						  ['lblPesquisaFinanceiro','txtPesquisaFinanceiro',24,2,-2]], null, null, true);

	// Posiciona os controles do divControles
	adjustElementsInForm([['lblDissociar','chkDissociar',3,1],
						  ['btnListar','btn',btn_width,1,230],
						  ['btnAssociar','btn',btn_width,1,10]], null, null, true);

    fillCmbEmpresas();
	//DireitoEspecifico
    //Acoes de Marketing->Acoes de Marketing->Modal Associar Acoes(B4)
    //40004 B4-> A1&&A2-> Habilita o botao alterar.
	btnAssociar.disabled = ((!glb_nA1) || (!glb_nA2));

    selEmpresa1.onchange = selEmpresa1_onchange;
	selTipoAcao.onchange = selTipoAcao_onchange;
	selMarca.onchange = selMarca_onchange;
	selHistoricoPadrao.onchange = selHistoricoPadrao_onchange;
	selTipoFinanceiro.onchange = selTipoFinanceiro_onchange;
	chkDissociar.onclick = chkDissociar_onclick;
	txtPesquisaAcao.maxLength = 255;
	txtPesquisaFinanceiro.maxLength = 255;
	
	txtPesquisaAcao.onkeypress = txtPesquisaAcao_onkeypress;
	txtPesquisaFinanceiro.onkeypress = txtPesquisaFinanceiro_onkeypress;
	
	btnListar.style.height = btn_height;
	btnAssociar.style.height = btn_height;
	btnCanc.style.height = btn_height;
	nDivControlsHeight = parseInt(btnAssociar.currentStyle.top, 10) +
	                     parseInt(btnAssociar.currentStyle.height, 10);
	
	chkSoAcaoPendente.checked = true;
	chkSoFinanceiroPendente.checked = true;
	
	// Dimensiona os divs em funcao dos parametros
	
	// o div de PesquisaAcao
	elem = divPesquisaAcao;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		//width = widthFree - (3 * ELEM_GAP / 2);
		width = (widthFree - (3 * ELEM_GAP / 2)) / 2 - (ELEM_GAP / 2);
		height = 100;
	}
	
	elem = divPesquisaFinanceiro;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		//width = widthFree - (3 * ELEM_GAP / 2);
		width = (widthFree - (3 * ELEM_GAP / 2)) / 2 - (ELEM_GAP / 2);
		height = 100;
	}

	// o div de controles
	elem = divControles;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		width = widthFree - (3 * ELEM_GAP / 2);
		height = nDivControlsHeight;
	}
	
	elem = divAcaoMarketing;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		//width = (widthFree - (3 * ELEM_GAP / 2)) / 2 - (ELEM_GAP / 2);
		width = 368;
		height = 280;
		//height = heightFree - 2 * ELEM_GAP - nDivControlsHeight;
	}

	elem = divFinanceiro;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		//width = (widthFree - (3 * ELEM_GAP / 2)) / 2 - (ELEM_GAP / 2);
		width = 368;
		height = 280;
		//height = heightFree - 2 * ELEM_GAP - nDivControlsHeight;
	}	
	
	// Dimensiona e posiciona os grids e seus labels
	elemLbl = lblAcaoMarketing;
	elemGrid = divAcaoMarketing;
	with (elemLbl.style)
	{
		backgroundColor = 'transparent';
		left = 0;
		top = ELEM_GAP / 2;
		width = parseInt(elemGrid.currentStyle.width, 10) - 2;
		height = lbl_height;
	}
	
	grid = fgAcoesMarketing;
	elem = divAcaoMarketing;
	with (grid.style)
	{
		left = 0;
		top = parseInt(elemLbl.currentStyle.top, 10) +
		      parseInt(elemLbl.currentStyle.height, 10);
		width = parseInt(elem.currentStyle.width, 10);
		height = parseInt(elem.currentStyle.height, 10) -
		         (parseInt(elemLbl.currentStyle.top, 10) +
		         parseInt(elemLbl.currentStyle.height, 10));
	}
	
	startGridInterface(grid , 1, null);
	grid.FontSize = '8';
	grid.Cols = 1;
    grid.ColWidth(0) = parseInt(elem.currentStyle.width, 10) * 18;
	
	elemLbl = lblFinanceiro;
	elemGrid = divFinanceiro;
	with (elemLbl.style)
	{
		backgroundColor = 'transparent';
		left = 0;
		top = ELEM_GAP / 2;
		width = parseInt(elemGrid.currentStyle.width, 10) - 2;
		height = lbl_height;
	}
	
	grid = fgFinanceiros;
	elem = divFinanceiro;
	with (grid.style)
	{
		left = 0;
		top = parseInt(elemLbl.currentStyle.top, 10) +
		      parseInt(elemLbl.currentStyle.height, 10);
		width = parseInt(elem.currentStyle.width, 10);
		height = parseInt(elem.currentStyle.height, 10) -
		         (parseInt(elemLbl.currentStyle.top, 10) +
		         parseInt(elemLbl.currentStyle.height, 10));
	}

	startGridInterface(grid , 1, null);
	grid.FontSize = '8';
	grid.Cols = 1;
    grid.ColWidth(0) = parseInt(elem.currentStyle.width, 10) * 18;
	
	// Posiciona os divs na horizontal e vertical
	elem = divPesquisaAcao;
	elem.style.left = ELEM_GAP;
	elem.style.top = topFree;
	y_topHeight = parseInt(elem.currentStyle.top, 10) +
			            parseInt(elem.currentStyle.height, 10);
	x_leftWidth = parseInt(elem.currentStyle.left, 10) +
			            parseInt(elem.currentStyle.width, 10);

	elem = divPesquisaFinanceiro;
	elem.style.left = ELEM_GAP + x_leftWidth;
	elem.style.top = topFree;
	y_topHeight = parseInt(elem.currentStyle.top, 10) +
			            parseInt(elem.currentStyle.height, 10);

	elem = divAcaoMarketing;
	elem.style.left = ELEM_GAP;
	//elem.style.top = topFree;
	elem.style.top = y_topHeight;
	y_topHeight = parseInt(elem.currentStyle.top, 10) +
			            parseInt(elem.currentStyle.height, 10);
	x_leftWidth = parseInt(elem.currentStyle.left, 10) +
			            parseInt(elem.currentStyle.width, 10);

	elem = divFinanceiro;
	elem.style.left = ELEM_GAP + x_leftWidth;
	elem.style.top = divAcaoMarketing.offsetTop;
	y_topHeight = parseInt(elem.currentStyle.top, 10) +
			            parseInt(elem.currentStyle.height, 10);

	elem = divControles;
	elem.style.left = ELEM_GAP;
	elem.style.top = y_topHeight;
}

/********************************************************************
Funcao do programador
Vai ao servidor buscar dados dos grids
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicGrids()
{
	if (!verificaData(txtdtFinanceiroInicio.value))
		return null;
	if (!verificaData(txtdtFinanceiroFim.value))
		return null;
	if (!verificaData(txtdtAcaoInicio.value))
		return null;		
	if (!verificaData(txtdtAcaoFim.value))
		return null;
		
    lockControlsInModalWin(true);
		
	var dso; 
	var dso2;
	var sFiltro = '';
	var sDataInicioFinanceiro = dateFormatToSearch(txtdtFinanceiroInicio.value);
    var sDataFimFinanceiro = dateFormatToSearch(txtdtFinanceiroFim.value);
	var sPesquisaFinanceiro = trimStr(txtPesquisaFinanceiro.value);
	var sHistoricoPadrao = (selHistoricoPadrao.value == 0 ? '' : selHistoricoPadrao.value);
	var sSQLDatasFinanceiro = '';
	var sSQLPesquisaFinanceiro = '';
	var sFinanceirosPendentes = (chkSoFinanceiroPendente.checked ? '1' : '0');
	var sPendentes = (chkSoAcaoPendente.checked ? '1' : '0');
	var sDissociar = (chkDissociar.checked ? '1' : '0');
	var sdtInicio = dateFormatToSearch(txtdtAcaoInicio.value);
    var sdtFim = dateFormatToSearch(txtdtAcaoFim.value);
	var sPesquisa = trimStr(txtPesquisaAcao.value);
	var sTipoAcaoID= (selTipoAcao.value == 0 ? '' : selTipoAcao.value);
	var sMarcaID = (selMarca.value == 0 ? '' : selMarca.value);
	
	glb_gridControlFill.bFollowOrder = false;
	glb_gridControlFill.nDsosToArrive = (chkDissociar.checked ? 1 : 2);
	glb_gridControlFill.dso1_Ref = dsoAcaoMarketing;
	glb_gridControlFill.dso2_Ref = (chkDissociar.checked ? null : dsoFinanceiro);
	glb_gridControlFill.grid1_Ref = fgAcoesMarketing;
	glb_gridControlFill.grid2_Ref = (chkDissociar.checked ? null : fgFinanceiros);    

	if ( glb_gridControlFill.dso1_Ref != null )
	{
		var strPars1 = '?EmpresaID=' + escape(selEmpresa1.value) +
			'&PageSize=' + escape(glb_nPageSize) +
			'&Pendentes=' + escape(sPendentes) +
			'&TipoAcaoID=' + escape(sTipoAcaoID) +
			'&MarcaID=' + escape(sMarcaID) +
			'&dtInicio=' + escape(sdtInicio) +
			'&dtFim=' + escape(sdtFim) +
			'&Pesquisa=' + escape(sPesquisa) +
			'&TipoFinanceiroID=' + escape(selTipoFinanceiro.value) +
			'&Dissociar=' + escape(sDissociar);

		glb_gridControlFill.dso1_Ref.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/acoesmarketing/serverside/associlarlistagemacoes.aspx' + strPars1;
		glb_gridControlFill.dso1_Ref.ondatasetcomplete = startDynamicGrids_DSC;
		glb_gridControlFill.dso1_Ref.Refresh();
    }
    
    if (glb_gridControlFill.dso2_Ref != null)
	{
		var strPars1 = '?EmpresaID=' + escape(selEmpresa2.value) +
			'&PageSize=' + escape(glb_nPageSize) +
			'&Pendentes=' + escape(sFinanceirosPendentes) +
			'&TipoFinanceiroID=' + escape(selTipoFinanceiro.value) +
			'&HistoricoPadraoID=' + escape(sHistoricoPadrao) +
			'&dtInicio=' + escape(sDataInicioFinanceiro) +
			'&dtFim=' + escape(sDataFimFinanceiro) +
			'&Pesquisa=' + escape(sPesquisaFinanceiro);

		glb_gridControlFill.dso2_Ref.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/acoesmarketing/serverside/associlarlistagemfinanceiros.aspx' + strPars1;
		glb_gridControlFill.dso2_Ref.ondatasetcomplete = startDynamicGrids_DSC;
		glb_gridControlFill.dso2_Ref.Refresh();

    }
}

/********************************************************************
Preenche os grids e mostra a janela
********************************************************************/
function startDynamicGrids_DSC() {
    glb_gridControlFill.nDsosToArrive--;
	
	if (glb_gridControlFill.nDsosToArrive > 0)
		return null;

	var i;
	var dso, grid;
	var dTFormat = '';
	var nTipoID, sColor;
	    
	if ( DATE_FORMAT == "DD/MM/YYYY" )
	    dTFormat = 'dd/mm/yyyy';
	else if ( DATE_FORMAT == "MM/DD/YYYY" )
	    dTFormat = 'mm/dd';
	    
	// Eventos dos grids - bloqueia/desbloqueia
	glb_gridsEvents.bBlockEvents_fgAcoesMarketing = true;
	glb_gridsEvents.bBlockEvents_fgFinanceiros = true;

	if (glb_gridControlFill.dso1_Ref != null)
	{
		glb_nSaldoExtrato = 0;
		
		dso = glb_gridControlFill.dso1_Ref;
		grid = glb_gridControlFill.grid1_Ref;
		
		glb_gridsEvents.bTotalLine_fgAcoesMarketing = true;
		
		grid.ExplorerBar = 0;
		startGridInterface(grid , 1, null);
		grid.FontSize = '8';
		grid.Editable = false;
		grid.BorderStyle = 1;
		
		headerGrid(grid,['ID',
		               'Tipo',
		               'A��o',
		               'Detalhe',
		               'Total Planejado',
		               'Marca',
		               'OK',
		               'Financeiro',
		               'Vencimento',
		               'Valor',
		               'Saldo',
		               'Aplicar',
		               'Observa��o',
		               'FinAcaoID',
					   'AcaPatrocinioID',
		               'MarcaID'], [13, 14, 15]);
		               
		var sAplicarReadOnly = (chkDissociar.checked ? '*' : '');
	
		fillGridMask(grid,dsoAcaoMarketing,['AcaoID*',
		                    'TipoAcao*',
		                    'Acao*',
		                    'Detalhe*',
		                    'TotalPlanejado*',
		                    'Marca*',
		                    'OK',
		                    'FinanceiroID*',
		                    'dtVencimento*',
		                    'Valor*',
		                    'Saldo*',
		                    'ValorAplicar' + sAplicarReadOnly,
		                    'Observacao' + sAplicarReadOnly,
		                    'FinAcaoID*',
		                    'AcaPatrocinioID*',
		                    'MarcaID*'],
		                    ['','','','','','','','','','','','','','','',''],
		                    ['','','','','(###,###,##0.00)','','','',dTFormat,'(###,###,##0.00)','(###,###,##0.00)','(###,###,##0.00)','','','','']);

		alignColsInGrid(grid,[4, 7, 9, 10, 11]);
		
		// Linha de Totais
		fgTotalizaGride(fgAcoesMarketing);
		
		grid.FrozenCols = 1;
		
		grid.MergeCells = 4;
		grid.MergeCol(0) = true;
		grid.MergeCol(1) = true;
		
		grid.AutoSizeMode = 0;
		grid.AutoSize(0,grid.Cols-1);
		
		grid.ExplorerBar = 5;
		
		if ( glb_gridsEvents.bTotalLine_fgAcoesMarketing )
		{
			if (grid.Rows > 2)
			{
				grid.Col = 1;
				grid.Col = 6;
				grid.Row = 2;
			}
		}	
		else
		{
			if (grid.Rows > 1)
				grid.Col = 1;
				grid.Col = 6;
				grid.Row = 1;
		}

		if ( (grid.Rows > 0) && grid.Row > 0 )
			grid.TopRow = grid.Row;
		
		grid.Editable = true;
	}
	
	if (glb_gridControlFill.dso2_Ref != null)
	{
		dso = glb_gridControlFill.dso2_Ref;
		grid = glb_gridControlFill.grid2_Ref;

		glb_gridsEvents.bTotalLine_fgFinanceiros = true;
		
		grid.ExplorerBar = 0;
		startGridInterface(grid , 1, null);
		grid.FontSize = '8';
		grid.Editable = false;
		grid.BorderStyle = 1;
		
		headerGrid(grid,['ID',
		                 'Pessoa',
		                 'Hist�rico Padr�o',
		                 'Vencimento',
		                 'Valor',
		                 'Saldo',
		                 'Observa��o'], []);
		                   
		fillGridMask(grid,dso,['FinanceiroID',
							   'Pessoa',
							   'HistoricoPadrao',
							   'dtVencimento',
							   'Valor',
							   'Saldo',
							   'Observacao'],
		                       ['','','','99/99/9999','999999999.99','999999999.99',''],
		                       ['','','',dTFormat,'###,###,##0.00','###,###,##0.00','']);
		                       

		alignColsInGrid(grid,[4, 5]);
		
		//Linha de Totais
        gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(grid, 'Valor'),'###,###,###,##0.00','S'],
                         [getColIndexByColKey(grid, 'Pessoa'),'###,##0','C'],
                         [getColIndexByColKey(grid, 'Saldo'),'###,###,###,##0.00','S']]);
				
		grid.FrozenCols = 1;
		//grid.MergeCells = 4;
		//grid.MergeCol(0) = true;
		
		grid.ExplorerBar = 5;
		
		if ( glb_gridsEvents.bTotalLine_fgFinanceiros )
		{
			if (grid.Rows > 2)
				grid.Row = 2;
		}	
		else
		{
			if (grid.Rows > 1)
				grid.Row = 1;
		}
		
		if ( (grid.Rows > 0) && grid.Row > 0 )
			grid.TopRow = grid.Row;

		grid.Editable = false;
		
	}	
	
	// Reinicializa objeto que controla dsos e grids
	glb_gridControlFill.bFollowOrder = false;
	glb_gridControlFill.nDsosToArrive = 0;
	glb_gridControlFill.dso1_Ref = null;
	glb_gridControlFill.dso2_Ref = null;
	glb_gridControlFill.grid1_Ref = null;
	glb_gridControlFill.grid2_Ref = null;
	
	// Eventos dos grids - bloqueia/desbloqueia
	glb_gridsEvents.bBlockEvents_fgAcoesMarketing = false;
	glb_gridsEvents.bBlockEvents_fgFinanceiros = false;
	
	lockControlsInModalWin(false);    
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalassociaracoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
}

/********************************************************************
Inverte o check de um checkbox associado a um label
********************************************************************/
function invertChkBox(ctl)
{
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    return true;
}    


/********************************************************************
Evento onchange dos combos
********************************************************************/
function cmbOnChange(cmb)
{
	;
	
}

/********************************************************************
Onclick dos checkbox
********************************************************************/
function chkOnClick()
{
	glb_gridControlFill.bFollowOrder = false;
	glb_gridControlFill.nDsosToArrive = 2;
	glb_gridControlFill.dso1_Ref = dsoAcaoMarketing;
	glb_gridControlFill.dso2_Ref = dsoFinanceiro;
	glb_gridControlFill.grid1_Ref = fgAcoesMarketing;
	glb_gridControlFill.grid2_Ref = fgFinanceiros;    
    
	startDynamicGrids();
	
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrBeforeRowColChange(grid, oldRow, oldCol, newRow, newCol)
{
	if (glb_gridsEvents.bBlockEvents_fgAcoesMarketing)
		return true;
		
	if (glb_gridsEvents.bBlockEvents_fgFinanceiros)
		return true;
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function fg_ModConcExtrBeforeSort(grid, col)
{
	glb_LASTLINESELID = '';
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function fg_ModConcExtrAfterSort(grid, col)
{
    var i, keyToSelectRow, bBlockEvents;
    
    keyToSelectRow = '';
    
    if ( (glb_LASTLINESELID != '') && (grid.Rows > 1) )
    {
        for (i=1; i<grid.Rows; i++)
        {
            if ( grid == fgAcoesMarketing )
				keyToSelectRow = grid.TextMatrix(i, getColIndexByColKey(grid, 'ExtLancamentoID*'));
			else if ( grid == fgFinanceiros )
				keyToSelectRow = grid.TextMatrix(i, getColIndexByColKey(grid, 'LanContaID*'));
            
            if (keyToSelectRow == glb_LASTLINESELID)
            {
				if ( grid == fgAcoesMarketing )
				{
					bBlockEvents = glb_gridsEvents.bBlockEvents_fgAcoesMarketing;
					glb_gridsEvents.bBlockEvents_fgAcoesMarketing = true;
				}	
				else if ( grid == fgFinanceiros )
				{
					bBlockEvents = glb_gridsEvents.bBlockEvents_fgFinanceiros;
					glb_gridsEvents.bBlockEvents_fgFinanceiros = true;
				}	
				
                grid.TopRow = i;
                grid.Row = i;        
                
   				if ( grid == fgAcoesMarketing )
					glb_gridsEvents.bBlockEvents_fgAcoesMarketing = bBlockEvents;
				else if ( grid == fgFinanceiros )
					glb_gridsEvents.bBlockEvents_fgFinanceiros = bBlockEvents;
                
                break;
            }    
        }
    }
    
    gridSortParams(grid, col);
}

/********************************************************************
Evento dos grids, particular desta pagina
********************************************************************/
function js_fg_ModConcExtrDblClick(grid, row, col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }

	glb_PassedOneInDblClick = true;
}

function fgAcoesMarketing_ValidateEdit()
{
}

function fgTotalizaGride(grid)
{
	var sAplicarReadOnly = (chkDissociar.checked ? '*' : '');
    var nOldStateGrid = grid.Editable;
    
    grid.Editable = false;
    
	gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true,
	[[getColIndexByColKey(grid, 'TotalPlanejado*'),'###,###,###,##0.00','S'],
		[getColIndexByColKey(grid, 'Valor*'),'###,###,###,##0.00','S'],
		[getColIndexByColKey(grid, 'Saldo*'),'###,###,###,##0.00','S'],
		[getColIndexByColKey(grid, 'ValorAplicar' + sAplicarReadOnly),'###,###,###,##0.00','S']], true);
		
	if (grid.Rows > 2)
		grid.TextMatrix(1,getColIndexByColKey(grid, 'TipoAcao*')) = (grid.Rows - 2);
		
    grid.Editable = nOldStateGrid;	
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalassociaracoes_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoAcaoMarketing, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;
}

function fgAcoesMarketing_AfterEdit(row)
{
	if (!chkDissociar.checked)
	{
		var nColSaldoAcoes = getColIndexByColKey(fgAcoesMarketing, 'Saldo*');
		var nColAplicar = getColIndexByColKey(fgAcoesMarketing, 'ValorAplicar');
		var nColOK = getColIndexByColKey(fgAcoesMarketing, 'OK');
		
		if (fgAcoesMarketing.Col != nColOK)
			fgAcoesMarketing.TextMatrix(fgAcoesMarketing.Row, nColOK) = 1;
			
		if (fgAcoesMarketing.ValueMatrix(row, nColOK) == 0)
		{
			fgAcoesMarketing.TextMatrix(row, nColAplicar) = 0;
			fgTotalizaGride(fgAcoesMarketing);
			return;
		}
		
		if (fgFinanceiros.Rows <= 1)
		{
			fgAcoesMarketing.TextMatrix(row, nColAplicar) = 0;
			fgTotalizaGride(fgAcoesMarketing);
			return;
		}
		
		if (fgAcoesMarketing.Col == nColOK)	
		{
			var nValorSaldoFinanceiro = fgFinanceiros.ValueMatrix(fgFinanceiros.Row, getColIndexByColKey(fgFinanceiros, 'Saldo'));
			var nValorAplicar = fgAcoesMarketing.ValueMatrix(row, nColSaldoAcoes);
			
			if ( (!isNaN(nValorSaldoFinanceiro)) && (!isNaN(nValorAplicar)) )
			{
				fgAcoesMarketing.TextMatrix(row, nColAplicar) = Math.min(nValorSaldoFinanceiro, nValorAplicar);
			}
			else
			{
				fgAcoesMarketing.TextMatrix(row, nColAplicar) = 0;
			}
		}
		fgTotalizaGride(fgAcoesMarketing);
	}
}	

function fgFinanceiros_AfterEdit(grid, row, col)
{
	;
}	

/********************************************************************
Examina sort order dos grids e guarda parametros
********************************************************************/
function gridSortParams(grid, col)
{
	// dtLancamento
	// dtBalancete
	// window.top.daysBetween(date1, date2);
	
	// nColDataType = 0 (String), 1 (Numerico), 2 (Data), 3 (Boolean)
	
	var i_init;
	
	var dataFirstRow = 0;
	var dataLastRow = 0;
	var dataDif = 0;
	var dataType = 0;
	var ncolOrderUsedToSort = -1;

	if ( grid == fgAcoesMarketing )
	{
		if ( glb_gridsEvents.bTotalLine_fgAcoesMarketing )
			i_init = 2;
		else
			i_init = 1;
			
		glb_gridsEvents.ncolUsedToSort_fgAcoesMarketing = col;	
		
		// inicialmente considera sempre sort ascendente
		glb_gridsEvents.ncolOrderUsedToSort_fgAcoesMarketing = 1;
			
	}		

	if ( grid == fgFinanceiros )
	{
		if ( glb_gridsEvents.bTotalLine_fgFinanceiros )
			i_init = 2;
		else
			i_init = 1;
			
		glb_gridsEvents.ncolUsedToSort_fgFinanceiros = col;	
		
		// inicialmente considera sempre sort ascendente
		glb_gridsEvents.ncolOrderUsedToSort_fgFinanceiros = 1;
	}
	
	dataType = grid.ColDataType(col);		
	
	if ( grid.Rows > i_init )
	{
		// data ou string
		if ( (dataType == 7) ||
		     (dataType == 8) || 
		     (dataType == 30) || 
		     (dataType == 31) )
		{
			dataFirstRow = '';
			dataLastRow = '';
			dataFirstRow = grid.TextMatrix(i_init, col);
			dataLastRow = grid.TextMatrix((grid.Rows - 1), col);
		}
		// numericos
		else
		{
			dataFirstRow = 0;
			dataLastRow = 0;
			dataFirstRow = grid.ValueMatrix(i_init, col);
			dataLastRow = grid.ValueMatrix((grid.Rows - 1), col);
		}
	}

	if ( grid.Rows > (i_init + 1) )
	{
		// String 8, 30, 31
		if ( (dataType == 8) || (dataType == 30) || (dataType == 31) )
		{
			if ( dataFirstRow == null )
				dataFirstRow = '';
			if ( dataLastRow == null )
				dataLastRow = '';	
				
			if ( (dataFirstRow == '') && (dataLastRow == '') )
				ncolOrderUsedToSort = 1;
			else if ( (dataFirstRow != '') && (dataLastRow == '') )
				ncolOrderUsedToSort = -1;
			else if ( (dataFirstRow == '') && (dataLastRow != '') )
				ncolOrderUsedToSort = 1;	
			else
			{
				if ( dataFirstRow < dataLastRow )
					ncolOrderUsedToSort = 1;	
				else
					ncolOrderUsedToSort = -1;	
			}
		}
		// Numero 2, 3, 4, 5, 6, 14, 20
		else if ( (dataType == 2) || (dataType == 3) ||
		          (dataType == 4) || (dataType == 5) ||
		          (dataType == 6) || (dataType == 14) || 
		          (dataType == 20) )
		{
			if ( dataFirstRow == null )
				dataFirstRow = 0;
			if ( dataLastRow == null )
				dataLastRow = 0;	

			dataDif = dataLastRow - dataFirstRow;
					
			if ( dataDif >= 0 )
				ncolOrderUsedToSort = 1;	
			else
				ncolOrderUsedToSort = -1;	
		}
		// Data 7
		else if ( dataType == 7 )
		{
			if ( dataFirstRow == null )
				dataFirstRow = '';
			if ( dataLastRow == null )
				dataLastRow = '';	
				
			if ( (dataFirstRow == '') && (dataLastRow == '') )
				ncolOrderUsedToSort = 1;
			else if ( (dataFirstRow != '') && (dataLastRow == '') )
				ncolOrderUsedToSort = -1;
			else if ( (dataFirstRow == '') && (dataLastRow != '') )
				ncolOrderUsedToSort = 1;	
			else
			{
				dataDif = window.top.daysBetween(dataFirstRow, dataLastRow);
					
				if ( dataDif >= 0 )
					ncolOrderUsedToSort = 1;	
				else
					ncolOrderUsedToSort = -1;	
			}
		}
	}

	if ( grid == fgAcoesMarketing )
		glb_gridsEvents.ncolOrderUsedToSort_fgAcoesMarketing = ncolOrderUsedToSort;
	else if ( grid == fgFinanceiros )	
		glb_gridsEvents.ncolOrderUsedToSort_fgFinanceiros = ncolOrderUsedToSort;
		
}

/********************************************************************
Usuario mudou a linha do grid de extratos
********************************************************************/
function lineChangedInGrid_Extrato(grid, nRow)
{
	var i, i_init;
	
	var nLanContaID_Extrato = 0;
	var nLanContaID_Lancamentos = 0;
	
	nLanContaID_Extrato = trimStr(fgAcoesMarketing.TextMatrix(fgAcoesMarketing.Row, getColIndexByColKey(fgAcoesMarketing, 'LanContaID*')));
	
	if ( (isNaN(nLanContaID_Extrato)) || (nLanContaID_Extrato == '') )
		nLanContaID_Extrato = 0;
	else
		nLanContaID_Extrato = parseInt(nLanContaID_Extrato, 10);
	
	if ( nLanContaID_Extrato != 0 )	
	{
		if ( glb_gridsEvents.bTotalLine_fgFinanceiros )
		{
			i_init = 2;
			if ( fgFinanceiros.Rows <= 2 )	
				return true;
		}
		else
			i_init = 1;

		for (i=i_init; i<fgFinanceiros.Rows; i++)
		{
			nLanContaID_Lancamentos = trimStr(fgFinanceiros.TextMatrix(i, getColIndexByColKey(fgFinanceiros, 'LanContaID*')));
	
			if ( (isNaN(nLanContaID_Lancamentos)) || (nLanContaID_Lancamentos == '') )
				nLanContaID_Lancamentos = 0;
			else
				nLanContaID_Lancamentos = parseInt(nLanContaID_Lancamentos, 10);
				
			if ( nLanContaID_Extrato == nLanContaID_Lancamentos )
			{
				glb_gridsEvents.bBlockEvents_fgFinanceiros = true;
				
				fgFinanceiros.Row = i;
				fgFinanceiros.TopRow = i;
				
				glb_gridsEvents.bBlockEvents_fgFinanceiros = false;
				break;
			}
		}
	}	
	else	
	{
		if ( glb_gridsEvents.bTotalLine_fgFinanceiros )
		{
			if ( fgFinanceiros.Row > 1 )	
				nLanContaID_Lancamentos = trimStr(fgFinanceiros.TextMatrix(fgFinanceiros.Row, getColIndexByColKey(fgFinanceiros, 'LanContaID*')));
		}
		else
		{
			if ( fgFinanceiros.Row > 0 )	
				nLanContaID_Lancamentos = trimStr(fgFinanceiros.TextMatrix(fgFinanceiros.Row, getColIndexByColKey(fgFinanceiros, 'LanContaID*')));			
		}
	
		if ( (isNaN(nLanContaID_Lancamentos)) || (nLanContaID_Lancamentos == '') )
			nLanContaID_Lancamentos = 0;
		else
			nLanContaID_Lancamentos = parseInt(nLanContaID_Lancamentos, 10);
			
		if ( nLanContaID_Lancamentos != 0)			
		{
			if ( glb_gridsEvents.bTotalLine_fgAcoesMarketing )
			{
				i_init = 2;
				if ( fgAcoesMarketing.Rows <= 2 )	
					return true;
			}
			else
				i_init = 1;

			for (i=i_init; i<fgAcoesMarketing.Rows; i++)
			{
				nLanContaID_Extrato = trimStr(fgAcoesMarketing.TextMatrix(i, getColIndexByColKey(fgAcoesMarketing, 'LanContaID*')));
	
				if ( (isNaN(nLanContaID_Extrato)) || (nLanContaID_Extrato == '') )
					nLanContaID_Extrato = 0;
				else
					nLanContaID_Extrato = parseInt(nLanContaID_Extrato, 10);
					
				if ( nLanContaID_Lancamentos == nLanContaID_Extrato )
				{
					glb_gridsEvents.bBlockEvents_fgFinanceiros = true;
					
					fgFinanceiros.Row = 0;
					fgFinanceiros.TopRow = 0;
					
					glb_gridsEvents.bBlockEvents_fgFinanceiros = false;
					break;
				}
			}
		}
	}
}

function adjustLabelsCombos()
{
	setLabelOfControl(lblHistoricoPadrao, selHistoricoPadrao.value);
	setLabelOfControl(lblMarca, selMarca.value);
	setLabelOfControl(lblTipoAcao, selTipoAcao.value);
}
	
function selTipoAcao_onchange()
{
	adjustLabelsCombos();
}
function selMarca_onchange()
{
	adjustLabelsCombos();
}
function selHistoricoPadrao_onchange()
{
	adjustLabelsCombos();
}
function selTipoFinanceiro_onchange()
{
	adjustLabelsCombos();
}
function chkDissociar_onclick()
{
	if (chkDissociar.checked)
	{
		divFinanceiro.style.visibility = 'hidden';
		divPesquisaFinanceiro.style.visibility = 'hidden';
		divAcaoMarketing.style.width = 746;
		btnAssociar.value = 'Dissociar';
	}
	else
	{
		divFinanceiro.style.visibility = 'inherit';
		divPesquisaFinanceiro.style.visibility = 'inherit';
		divAcaoMarketing.style.width = 368;
		btnAssociar.value = 'Associar';
	}
	
	fgAcoesMarketing.style.width = parseInt(divAcaoMarketing.currentStyle.width, 10);	
	fgAcoesMarketing.Cols = 1;
	fgAcoesMarketing.Rows = 1;
    fgAcoesMarketing.ColWidth(0) = parseInt(divAcaoMarketing.currentStyle.width, 10) * 18;
}

function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
		return false;
	}
	return true;
}
function js_fg_ModConcExtrAfterRowColChangePesqList(grid, oldRow, oldCol, newRow, newCol)
{
	// grid executa evento
	if ( (grid == fgAcoesMarketing) && (glb_gridsEvents.bBlockEvents_fgAcoesMarketing) )
		return true;

	if ( (grid == fgFinanceiros) && (glb_gridsEvents.bBlockEvents_fgFinanceiros) )
		return true;
		
	if (grid == fgFinanceiros)
	{
		limpaGridAcoesMarketing();
	}
	if ((grid.Row <= 1) && (grid.Rows >= 3))
		grid.Row = 2;
}

function limpaGridAcoesMarketing()
{
	var i = 0;
	var nColAplicar = getColIndexByColKey(fgAcoesMarketing, 'ValorAplicar');
	var nColOK = getColIndexByColKey(fgAcoesMarketing, 'OK');
	
	for (i=1; i<fgAcoesMarketing.Rows; i++)
	{
		fgAcoesMarketing.TextMatrix(i, nColOK) = 0;
		fgAcoesMarketing.TextMatrix(i, nColAplicar) = 0;
	}
}

function gravar()
{
	var strPars = '';
	var i=0;
	var nColOK = getColIndexByColKey(fgAcoesMarketing, 'OK');
	
	if ((!chkDissociar.checked) && (fgFinanceiros.Row < 2))
	{
		if ( window.top.overflyGen.Alert('Selecione um financeiro.') == 0 )
			return null;
		
		return false;
	}
	
	for (i=2; i<fgAcoesMarketing.Rows; i++)
	{
		if (fgAcoesMarketing.ValueMatrix(i, nColOK) != 0)
		{
			strPars += (strPars == '' ? '?' : '&');
			strPars += 'AcaoID=' + escape(fgAcoesMarketing.TextMatrix(i, getColIndexByColKey(fgAcoesMarketing, 'AcaoID*')));
			strPars += '&AcaPatrocinioID=' + escape(fgAcoesMarketing.TextMatrix(i, getColIndexByColKey(fgAcoesMarketing, 'AcaPatrocinioID*')));
			strPars += '&FinAcaoID=' + escape(fgAcoesMarketing.TextMatrix(i, getColIndexByColKey(fgAcoesMarketing, 'FinAcaoID*')));
			
			if (chkDissociar.checked)
			{
				strPars += '&FinanceiroID=' + escape(fgAcoesMarketing.TextMatrix(i, getColIndexByColKey(fgAcoesMarketing, 'FinanceiroID*')));
				strPars += '&Valor=' + escape(0);
				strPars += '&Observacao=';
			}
			else
			{
				strPars += '&FinanceiroID=' + escape(fgFinanceiros.ValueMatrix(fgFinanceiros.Row, getColIndexByColKey(fgFinanceiros, 'FinanceiroID')));
				strPars += '&Valor=' + escape(fgAcoesMarketing.ValueMatrix(i, getColIndexByColKey(fgAcoesMarketing, 'ValorAplicar')));
				strPars += '&Observacao=' + escape(fgAcoesMarketing.TextMatrix(i, getColIndexByColKey(fgAcoesMarketing, 'Observacao')));
			}
			
			strPars += '&Dissociar=' + escape(chkDissociar.checked ? 1 : 0);
			strPars += '&UsuarioID=' + getCurrUserID();
			 
		}
	}
	
	if (strPars == '')
	{
        if ( window.top.overflyGen.Alert('Selecione pelo menos uma a��o de marketing para gravar.') == 0 )
            return null;
        
		return false;
	}
	
	lockControlsInModalWin(true);
	
	dsoGravar.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/acoesmarketing/serverside/gravaacoesmarketing.aspx' + strPars;
    dsoGravar.ondatasetcomplete = dsoGravar_DSC;
    dsoGravar.refresh();
}

/********************************************************************
Preenche o grid de produtos
********************************************************************/
function dsoGravar_DSC()
{
	if (!((dsoGravar.recordset.BOF)&&(dsoGravar.recordset.BOF)))
	{
		if (dsoGravar.recordset['Resultado'].value != '')
		{
			if ( window.top.overflyGen.Alert(dsoGravar.recordset['Resultado'].value) == 0 )
				return null;
			
			lockControlsInModalWin(false);	
			return null;
		}
 	}
	
	startDynamicGrids();	
}

function fillCmbEmpresas()
{
    lockControlsInModalWin(true);
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    
    setConnection(dsoEmpresas);

    dsoEmpresas.SQL = 'SELECT ' + selEmpresa1.value + ' AS EmpresaID, ' + '\'' + selEmpresa1.options.item(selEmpresa1.selectedIndex).innerText + '\'' + ' AS Empresa ' +
			'UNION ' +
        'SELECT b.PessoaID AS EmpresaID, b.Fantasia AS Empresa ' +			
		'FROM FopagEmpresas a WITH (NOLOCK) INNER JOIN Pessoas b WITH (NOLOCK) ON a.EmpresaAlternativaID=b.PessoaID ' +
		'WHERE a.EmpresaID = ' + selEmpresa1.value + ' AND a.FuncionarioID IS NULL ' +
		'ORDER BY EmpresaID';

    dsoEmpresas.ondatasetcomplete = fillCmbEmpresas_DSC;
    dsoEmpresas.Refresh();
}

function fillCmbEmpresas_DSC() {
    clearComboEx(['selEmpresa2']);

    if (!((dsoEmpresas.recordset.BOF) || (dsoEmpresas.recordset.EOF)))
    {
        while (!dsoEmpresas.recordset.EOF)
        {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoEmpresas.recordset['Empresa'].value;
            oOption.value = dsoEmpresas.recordset['EmpresaID'].value;
            
            if (dsoEmpresas.recordset['EmpresaID'].value == selEmpresa1.value)
                oOption.selected = true;

            selEmpresa2.add(oOption);
            dsoEmpresas.recordset.MoveNext();
        }
    }
    
    lockControlsInModalWin(false);
}

function selEmpresa1_onchange()
{
    fillCmbEmpresas();
}
