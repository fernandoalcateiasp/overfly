/********************************************************************
modalresponsavel.js

Library javascript para o modalresponsavel.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var modPesqresponsavel_Timer = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoPesq = new CDatatransport('dsoPesq');
var dsoInsertResp = new CDatatransport('dsoInsertResp');

/********************************************************************
RELACAO DAS FUNCOES

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_ModPesqresponsavelDblClick()
fg_ModPesqresponsavelKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalresponsavelBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPesquisa').disabled == false )
        txtPesquisa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Selecionar responsável', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    // ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        height = 40;
    }
    
    // txtPesquisa
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtPesquisa.style)
    {
        left = 0;
        top = 16;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtPesquisa.onkeypress = txtPesquisa_onKeyPress;
    
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPesquisa.style.top, 10);
        left = parseInt(txtPesquisa.style.left, 10) + parseInt(txtPesquisa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
    
    btnCanc.disabled = false;
}

/********************************************************************
Usuario pressionou tecla no campo txtPesquisa
********************************************************************/
function txtPesquisa_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPesqresponsavel_Timer = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }
}

/********************************************************************
Caracter digitado no campo txtPesquisa
********************************************************************/
function txtPesquisa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

/********************************************************************
Usuario clicou o botao btnFindPesquisa
********************************************************************/
function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqresponsavel_Timer != null )
    {
        window.clearInterval(modPesqresponsavel_Timer);
        modPesqresponsavel_Timer = null;
    }
    
    txtPesquisa.value = trimStr(txtPesquisa.value);
    
    changeBtnState(txtPesquisa.value);

    if (btnFindPesquisa.disabled)
    {
        lockControlsInModalWin(false);
        return;
    }    
    
    startPesq(txtPesquisa.value);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqresponsavelDblClick()
{
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqresponsavelKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
    {
        var nResponsavelID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'PessoaID'));
        insertResponsavel(nResponsavelID);
    }        

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
}

/**************************************************
Select de responsaveis
**************************************************/
function startPesq(strPesquisa)
{
    var nEmpresaID = getCurrEmpresaData();

    setConnection(dsoPesq);

    dsoPesq.SQL = 'SELECT DISTINCT a.PessoaID, a.Nome, a.Fantasia ' +
                    'FROM Pessoas a WITH (NOLOCK) ' +
                        'INNER JOIN RelacoesPessoas b WITH (NOLOCK) ON (b.SujeitoID = a.PessoaID) ' +
                    'WHERE (a.ClassificacaoID = 57) AND ' +
                        '(a.EstadoID = 2) AND ' +
                        '(b.TipoRelacaoID = 31) AND ' +
                        '(b.EstadoID = 2) AND ' +
                        //'(b.ObjetoID = ' + nEmpresaID[0] + ') AND ' +                        
                        '((a.Nome LIKE \'%' + strPesquisa + '%\') OR ' +
                        '(a.Fantasia LIKE \'%' + strPesquisa + '%\'))';
	
	dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}


function dsopesq_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg,['ID',
                   'Nome',
                   'Fantasia'], []);

    fillGridMask(fg,dsoPesq,['PessoaID',
                             'Nome',
                             'Fantasia'],
                             ['','','']);

    alignColsInGrid(fg,[]);

    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        btnOK.disabled = false;
    }
    else
        btnOK.disabled = true;
}

function insertResponsavel(nResponsavelID)
{
    setConnection(dsoInsertResp);
    
    if ((nResponsavelID.length <= 0) || (glb_nRegistroID == 0))
    {
        window.top.overflyGen.Alert('Selecione um responsável');
        return null;
    }

    var strPars = '?nRegistroID=' + escape(glb_nRegistroID.toString());
    strPars += '&nResponsavelID=' + escape(nResponsavelID.toString());
    
    dsoInsertResp.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/acoesmarketing/serverside/insertresponsavel.aspx' + strPars;
    dsoInsertResp.ondatasetcomplete = insertResponsavel_DSC;
    dsoInsertResp.refresh();
}

function insertResponsavel_DSC()
{
    ;
}