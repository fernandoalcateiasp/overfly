
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalgerarfinanceirosHtml" name="modalgerarfinanceirosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/acoesmarketing/modalpages/modalgerarfinanceiros.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/acoesmarketing/modalpages/modalgerarfinanceiros.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat, nEmpresaID

sCaller = ""
nEmpresaID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write vbcrlf

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalgerarfinanceiros_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalgerarfinanceirosKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
fg_DblClick();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalgerarfinanceirosBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
//Atencao programador:
//Setimo parametro abaixo = true, permite ao usuario mudar de linha em grid
//clicando em celula read only.
js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalgerarfinanceirosBody" name="modalgerarfinanceirosBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">	
		<p id="lblTipo" name="lblTipo" class="lblGeneral">Tipo</p>
		<select id="selTipo" name="selTipo" class="fldGeneral" MULTIPLE>

<%
	Dim strSQL, rsData

	Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL =  "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
			 "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			 "WHERE (EstadoID=2 AND TipoID=109) " & _
			 "ORDER BY Ordem"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>		
		</select>
		<p id="lblOrigem" name="lblOrigem" class="lblGeneral" title="">Origem</p>
		<select id="selOrigem" name="selOrigem" title="" class="fldGeneral" MULTIPLE>
<%
    strSQL = "SELECT ItemID AS fldID, ItemAbreviado AS fldName, a.Ordem AS Ordem " & _
             "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
             "WHERE a.TipoID = 114 " & _
             "UNION ALL " & _
             "SELECT 0 AS fldID, '' AS fldName, -2 AS Ordem " & _
             "ORDER BY a.Ordem "
             

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>						
		</select>	
		<p id="lblFamilia" name="lblFamilia" class="lblGeneral">Fam�lia</p>
		<select id="selFamilia" name="selFamilia" class="fldGeneral" MULTIPLE>		
<%
    strSQL = "SELECT DISTINCT a.ConceitoID AS fldID, a.Conceito AS fldName " & _
			 "FROM Conceitos a WITH(NOLOCK) " & _
			 "INNER JOIN Conceitos b WITH(NOLOCK) ON a.ConceitoID = b.ProdutoID " & _
			 "INNER JOIN RelacoesPesCon c WITH(NOLOCK) ON b.ConceitoID = c.ObjetoID  " & _
			 "WHERE a.EstadoID = 2 AND c.TipoRelacaoID = 61 AND c.EstadoID NOT IN (4,5) AND c.SujeitoID = " & nEmpresaID & " " & _
             "UNION ALL " & _
             "SELECT 0 AS fldID, '' AS fldName " & _
			 "ORDER BY a.Conceito"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>						
		</select>		
		<p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
		<select id="selMarca" name="selMarca" class="fldGeneral" MULTIPLE>
<%
    strSQL = "SELECT DISTINCT a.ConceitoID AS fldID, a.Conceito AS fldName " & _
			 "FROM Conceitos a WITH(NOLOCK) " & _
			 "INNER JOIN Conceitos b WITH(NOLOCK) ON a.ConceitoID = b.MarcaID " & _
			 "INNER JOIN RelacoesPesCon c WITH(NOLOCK) ON b.ConceitoID = c.ObjetoID  " & _
			 "WHERE a.EstadoID = 2 AND c.TipoRelacaoID = 61 AND c.EstadoID NOT IN (1,4,5) " & _
			 "UNION ALL " & _
             "SELECT 0 AS fldID, '' AS fldName " & _
			 "ORDER BY a.Conceito"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>								
		</select>

        <p id="lblClasspessoa" name="lblClasspessoa" class="lblGeneral">Class. Pessoa</p>
		<select id="selClasspessoa" name="selClasspessoa" class="fldGeneral" MULTIPLE>
<%
    strSQL = "SELECT ItemID as fldID, ItemMasculino as fldName " & _
		     "FROM TiposAuxiliares_itens WITH(NOLOCK) " & _
		     "WHERE EstadoID=2 AND TipoID=13 AND (ItemID NOT IN (57, 58, 66, 67, 68)) " & _
		     "UNION ALL " & _
             "SELECT 0 AS fldID, '' AS fldName " & _
		     "ORDER BY ItemID"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>								
		</select>

		<p id="lblProduto" name="lblProduto" class="lblGeneral">Produto</p>
		<select id="selProduto" name="selProduto" class="fldGeneral" MULTIPLE>
<%
    strSQL = "IF ((SELECT COUNT(1) " & _
                    "FROM Pedidos_Itens_Despesas a WITH(NOLOCK) " & _
                    "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.PedItemID = a.PedItemID) " & _
                    "INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " & _
                    "INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = a.ProdutoID) " & _
                    "WHERE a.ProdutoID IS NOT NULL AND a.FinanceiroID IS NULL " & _
                    "AND c.EmpresaID = " & nEmpresaID &  " ) > 0) " & _
                "SELECT DISTINCT d.ConceitoID as fldID, (CONVERT(VARCHAR(10), d.ConceitoID) + ' - ' + d.Conceito) as fldName " & _
                "FROM Pedidos_Itens_Despesas a WITH(NOLOCK) " & _
                "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.PedItemID = a.PedItemID) " & _
                "INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " & _
                "INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = a.ProdutoID) " & _
                "WHERE a.ProdutoID IS NOT NULL AND a.FinanceiroID IS NULL " & _
                "AND c.EmpresaID = " & nEmpresaID &  " " & _
                "UNION ALL " & _
                "SELECT 0 AS fldID, '' AS fldName " & _
                "ORDER BY d.ConceitoID " & _
             "ELSE " & _
                "SELECT DISTINCT d.ConceitoID as fldID, (CONVERT(VARCHAR(10), d.ConceitoID) + ' - ' + d.Conceito) as fldName " & _
                "FROM Pedidos_Itens_Despesas a WITH(NOLOCK) " & _
                "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.PedItemID = a.PedItemID) " & _
                "INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " & _
                "INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = a.ProdutoID) " & _
                "WHERE a.ProdutoID IS NOT NULL AND a.FinanceiroID IS NULL " & _
                "AND c.EmpresaID = " & nEmpresaID &  " " & _
                "ORDER BY d.ConceitoID"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>								
		</select>
				
		<p id="lblExcecoes" name="lblExcecoes" class="lblGeneral">Exce��es</p>
		<select id="selExcecoes" name="selExcecoes" class="fldGeneral" MULTIPLE>
<%
    strSQL = "SELECT ItemID as fldID, ItemMasculino as fldName " & _
		"FROM TiposAuxiliares_itens WITH(NOLOCK) " & _
		"WHERE EstadoID=2 AND TipoID=111 " & _
        "UNION ALL " & _
        "SELECT 0 AS fldID, '' AS fldName " & _
		"ORDER BY ItemID"

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
%>								
		</select>

		<p id="lblTipoFinanceiro" name="lblTipoFinanceiro" class="lblGeneral">Tipo Financeiro</p>
		<select id="selTipoFinanceiro" name="selTipoFinanceiro" class="fldGeneral" >
<%
    strSQL = "SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName " & _
			 "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _			 
			 "WHERE a.TipoID = 801 " & _
			 "ORDER BY a.Ordem "

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	If (Not (rsData.BOF AND rsData.EOF) ) Then

		While Not (rsData.EOF)

			Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			Response.Write( rsData.Fields("fldName").Value & "</option>" )

			rsData.MoveNext()

		WEnd

	End If

	rsData.Close
	Set rsData = Nothing
%>									
		</select>        
        <p id="lblValorMaximo" name="lblValorMaximo" class="lblGeneral">Valor M�ximo</p>
        <input type="text" id="txtValorMaximo" name="txtValorMaximo" class="fldGeneral"></input>
        <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">Data In�cio</p>
        <input type="text" id="txtdtInicio" name="txtdtInicio" class="fldGeneral"></input>         
        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Data Fim</p>
        <input type="text" id="txtdtFim" name="txtdtFim" class="fldGeneral"></input>         

        <p id="lblCampanhas" name="lblCampanhas" class="lblGeneral" LANGUAGE=javascript title="Financeiro para baixar Campanhas?">C</p>
        <input type="checkbox" id="chkCampanhas" name="chkCampanhas" class="fldGeneral" title="Financeiro para baixar Campanhas?"></input>

        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral"></input>
        <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnGerar" name="btnGerar" value="Gerar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>