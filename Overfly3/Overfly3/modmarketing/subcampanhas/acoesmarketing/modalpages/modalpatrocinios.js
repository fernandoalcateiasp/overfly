/********************************************************************
modalpatrocinios.js

Library javascript para o modalpatrocinios.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_localTimerRefresh = null;
var glb_nMaxStringSize = 1024*1;
// Controla se algum dado do grid foi alterado
var glb_nDSO = 0;
var glb_nDSO2 = 0;
var glb_aDeleteds = new Array();
var glb_nI = 0;
var glb_nA1 = 0;
var glb_nA2 = 0;
var glb_nE1 = 0;
var glb_nE2 = 0;

// Dados do grid .RDS
var dsoMarca = new CDatatransport("dsoMarca");
var dsoGravar = new CDatatransport("dsoGravar");
var dsoPedido = new CDatatransport("dsoPedido");
var dsoGrid = new CDatatransport("dsoGrid");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalpatrocinios.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalpatrocinios.ASP

js_fg_modalpatrociniosBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalpatrociniosDblClick( grid, Row, Col)
js_modalpatrociniosKeyPress(KeyAscii)
js_modalpatrocinios_AfterRowColChange
js_modalpatrocinios_ValidateEdit()
js_modalpatrocinios_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    dealWithObjects_Load();
    dealWithGrid_Load();
    
    window_onload_1stPart();
    
    glb_nI = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'I' + '\'' + ')') );

    glb_nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );

    glb_nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );

    glb_nE1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'E1' + '\'' + ')') );

    glb_nE2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'E2' + '\'' + ')') );
    
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // ajusta o body do html
    with (modalpatrociniosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    setConnection(dsoMarca);
	dsoMarca.SQL = 'SELECT DISTINCT a.ConceitoID AS fldID, a.Conceito AS fldName ' +
				'FROM Conceitos a WITH(NOLOCK) ' +
					'INNER JOIN Conceitos b WITH(NOLOCK) ON a.ConceitoID = b.MarcaID ' +
					'INNER JOIN RelacoesPesCon c WITH(NOLOCK) ON b.ConceitoID = c.ObjetoID ' +
				'WHERE a.EstadoID=2 AND a.TipoConceitoID=304 AND c.EstadoID NOT IN (1,4,5) AND ' + 
				'c.TipoRelacaoID = 61 ' +
				'ORDER BY fldName';

	dsoMarca.ondatasetcomplete = window_onload_DSC;
	dsoMarca.Refresh();
}

function window_onload_DSC() {
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Alteração de Patrocínios', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var topFree, widthFree, heightFree; 
    var frameBorder = 5;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    // heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    // altura livre da janela modal (descontando apenas a barra azul)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - (2 * frameBorder);
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder);
}

/********************************************************************
Posiciona os campos na interface, define labels e hints,
controla digitacao em campos numéricos, define eventos associados
a controles, etc

Parametros:
	topFree	- inicio da altura da janela (posicao onde termina o div do
	          titulo da janela )
	heightFree - altura livre da janela (do top free ate o botao OK,
	          supondo o btnOK em baixo, que e o padrao de modal)
	widthFree - largura livre da janela
	frameBorder - largura da borda da janela
		
Retorno:
	nenhum
********************************************************************/
function adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder)
{
	var elem;
	var x_leftWidth = 0;
	var y_topHeight = 0;
	var nEstado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['EstadoID'].value");
	
	var lbl_height = 16;
	var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
	
	// Posiciona os controles do divText
	adjustElementsInForm([['lblTotalPlanejado','txtTotalPlanejado',14,1],
						  ['lblTotalRateado','txtTotalRateado',14,1,2],
						  ['lblSaldoRatear','txtSaldoRatear',14,1],
						  ['lblMarcasNaoRateadas','txtMarcasNaoRateadas',14,1],						  
						  ['lblValorRateio','txtValorRateio',14,1,-10],
						  ['btnRatear','btn',btn_width,1,5]], null, null, true);

	// Posiciona os controles do divControles
	adjustElementsInForm([['btnIncluir','btn',btn_width,1,5,-10],
						  ['btnExcluir','btn',btn_width,1,5],
						  ['btnExcluirTodos','btn',btn_width,1,5],
						  ['btnRefresh','btn',btn_width,1,5],
						  ['btnGravar','btn',btn_width,1,5]], null, null, true);

	// Posiciona os controles do divMarca						  
	adjustElementsInForm([['lblMarca','selMarca',15,1,0,-22]], null, null, true);
	
	txtTotalPlanejado.readOnly = true;
	txtTotalRateado.readOnly = true;
	txtSaldoRatear.readOnly = true;
	txtMarcasNaoRateadas.readOnly = true;
	txtValorRateio.readOnly = true;
	
	if (nEstado != 1)
	{
        //DireitoEspecifico
        //Acoes de Marketing->Acoes de Marketing->Patrocinios->Modal Patrocinios
        //23201 SFI-Grupo Patrocinio-> I==0 -> Desabilita o botao Incluir.
		btnIncluir.disabled = (glb_nI == 0);
		//DireitoEspecifico
        //Acoes de Marketing->Acoes de Marketing->Patrocinios->Modal Patrocinios
        //23200 SFS-Grupo Acoes de Marketing-> E1&&E2 -> Habilita oa botoes: 'Excluir','ExcluirTodos'.
        btnExcluir.disabled = ((!glb_nE1) || (!glb_nE2));
		btnExcluirTodos.disabled = ((!glb_nE1) || (!glb_nE2));
		//DireitoEspecifico
        //Acoes de Marketing->Acoes de Marketing->Patrocinios->Modal Patrocinios
        //23200 SFS-Grupo Acoes de Marketing-> A1&&A2 -> Habilita botao Gravar.
		btnGravar.disabled = ((!glb_nA1) || (!glb_nA2));
	}
	
	btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;	

	// Dimensiona os divs em funcao dos parametros
	
	elem = divText;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		top = topFree;
		left = 0;
		width = widthFree ;
		height = 70;
	}
	
	elem = divCombo;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		top = divText.offsetHeight + divText.offsetTop + 1;
		left = 0;
		width = 130;
		height = 320;
	}
	
	//Combo Marca
	selMarca.style.height = 328;
	
	elem = divFG;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		top = divText.offsetHeight + divText.offsetTop + 1;
		width = widthFree - divCombo.offsetWidth - ELEM_GAP;
		left = divCombo.offsetWidth + 5;
		height = divCombo.offsetHeight + 5;
	}
	elem = fg;
	with (elem.style)
	{
		border = 'none';
		top = 0;
		width = divFG.offsetWidth;
		left = 0;
		height = divFG.offsetHeight;
	}
	
	
	// o div de controles
	elem = divControles;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		top = divFG.offsetHeight + divFG.offsetTop + 1;
		width = widthFree;
		height = 50;
	}
	
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    var iLinhas = 0;
    var nValor = 0;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;   
    }
    // codigo privado desta janela
    else if (controlID == 'btnCanc')
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }
    else if (controlID == 'btnRefresh')
    {
		fillGridData();
    }
    else if (controlID == 'btnGravar')
    {
		if (fg.Rows > 2)
		{
			iLinhas = fg.Rows;
			nValor = 0;
			
			for (i=2; i<iLinhas; i++)
			{
				if ( fg.ValueMatrix((i), getColIndexByColKey(fg, 'Marca'))==0 )
				{
					if ( window.top.overflyGen.Alert('Selecionar Marca.') == 0 )
						return null;	
											
					fg.Row = i;	
					return null;						
				}
				if ( fg.ValueMatrix((i), getColIndexByColKey(fg, 'ValorPatrocinio'))==0 )
				{
					if ( window.top.overflyGen.Alert('Valor Patrocínio deve ser maior que 0.') == 0 )
						return null;					
					
					fg.Row = i;	
					return null;							
				}
			}
			js_modalpatrocinios_AfterEdit(0, 0);	
		}			
		saveDataInGrid();
    }
    else if (controlID == 'btnIncluir')
    {
		incluirLinha();
    }
    else if (controlID == 'btnRatear')
    {
		ratear();
    }
    else if (controlID == 'btnExcluir')
    {
		excluirLinha();
    }
    else if (controlID == 'btnExcluirTodos')
    {
		excluirTodos();
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
    
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    if (txtValorRateio.value == 0 || txtValorRateio.value == '')
		btnRatear.disabled = true;		
	else
		btnRatear.disabled = false;	
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    lockControlsInModalWin(true);
    
    var idToFind = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
    // zera o grid
    fg.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT a.AcaPatrocinioID, a.MarcaID AS Marca, a.Iniciativa, a.Prazo, a.ValorPatrocinio, a.Observacao, Convert(bit,0) As Alterado ' + 
                  'FROM AcoesMarketing_Patrocinios a WITH(NOLOCK) ' +
                  'INNER JOIN Conceitos b WITH(NOLOCK) ON a.MarcaID = b.ConceitoID ' +
                  'WHERE a.AcaoID = '+ idToFind + ' ' +
                  'ORDER BY b.Conceito';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    showExtFrame(window, true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    headerGrid(fg,['Marca',
                    'Inic',
                    'Prazo',
                    'Valor Patrocínio',
                    'Observacao',
                    'AcaPatrocinioID',
                    'Alterado'],[5,6]);                
    
    fillGridMask(fg,dsoGrid,['Marca',
                             'Iniciativa',
                             'Prazo',
                             'ValorPatrocinio',
                             'Observacao',
                             'AcaPatrocinioID',
                             'Alterado'],
                             ['','','999','999999999.99','','',''],
                             ['','','###','###,###,##0.00','','','']);
                             
	insertcomboData(fg, getColIndexByColKey(fg, 'Marca'), dsoMarca, 'fldName', 'fldID');
	alignColsInGrid(fg,[2,3]);
	
    //for (i=1; i<fg.Rows; i++)
		
	autoSize();
    
    fg.Redraw = 2;
        
    if (fg.Rows > 1)
    {
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [
                            [getColIndexByColKey(fg, 'ValorPatrocinio'),'###,###,###.00','S'],
                            [getColIndexByColKey(fg, 'Prazo'),'###,###,###.00','C']]);
        fg.Editable = true;
    }
       
    
    //Preenche os campos
    
    lockControlsInModalWin(false);
    
    fillEditBox();

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	var nAcaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
	var	iAcaPatrocinioID;	
	
	lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
    
    if (glb_aDeleteds.length>0)
	{
        for (i=0; i<glb_aDeleteds.length; i++)
        {       
		    nBytesAcum=strPars.length;
    		
		    if ( (nBytesAcum >= glb_nMaxStringSize) && (strPars != '') )
		    {
			    nBytesAcum = 0;
			    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
    		    strPars = '';
			    nDataLen = 0;
		    }

		    nDataLen++;
    			    
            iAcaPatrocinioID = glb_aDeleteds[i];
			strPars += (strPars == '' ? '?' : '&');
			strPars += 'AcaPatrocinioID=' + escape(iAcaPatrocinioID);
			strPars += '&AcaoID=' + escape(nAcaoID);
			strPars += '&Marca=' ;
			strPars += '&Iniciativa=' ;
			strPars += '&Prazo=' ;
			strPars += '&ValorPatrocinio=' ;
			strPars += '&Observacao=' ;
			strPars += '&Tipo=' + escape(1);
			strPars += '&EmpresaID=' + escape(glb_aEmpresaData[0]);
	    }
	    
	    if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
    }
    
    nDataLen = 0;
	nBytesAcum = 0;
	strPars = '';
        
    for (i=2; i<fg.Rows; i++)
    {       
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Alterado')) != 0)
        {
            nBytesAcum=strPars.length;
        	
            if ( (nBytesAcum >= glb_nMaxStringSize) && (strPars != '') )
            {
	            nBytesAcum = 0;
	            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
	            strPars = '';
	            nDataLen = 0;
            }

            nDataLen++;
        		    
            iAcaPatrocinioID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'AcaPatrocinioID'));
			strPars += (strPars == '' ? '?' : '&');
			strPars += 'AcaPatrocinioID=' + escape(iAcaPatrocinioID);
			strPars += '&AcaoID=' + escape(nAcaoID);
			strPars += '&Marca=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'Marca')));
			strPars += '&Iniciativa=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'Iniciativa')));
			strPars += '&Prazo=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'Prazo')));
			strPars += '&ValorPatrocinio=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorPatrocinio')));
			strPars += '&Observacao=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao')));
			strPars += '&Tipo=' + escape(iAcaPatrocinioID > 0 ? 3 : 2);  
			strPars += '&EmpresaID=' + escape(glb_aEmpresaData[0]);
		}
    }
    
    if (nDataLen > 0)
	    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
		    dsoGravar.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/acoesmarketing/serverside/gravapatrocinios.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGravar.ondatasetcomplete = sendDataToServer_DSC;
            dsoGravar.refresh();
		}
		else
		{	
	        glb_localTimerRefresh = window.setInterval('saveDataInGrid_DSC()', 30, 'JavaScript');
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Erro. Tente novamente.') == 0 )
			return null;
		
	    glb_localTimerRefresh = window.setInterval('saveDataInGrid_DSC()', 30, 'JavaScript');
	}
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    if (glb_localTimerRefresh != null)
    {
        window.clearInterval(glb_localTimerRefresh);
        glb_localTimerRefresh = null;
    }
    
	lockControlsInModalWin(false);	
	fillGridData();
	return null;
}

function incluirLinha()
{
	var iLinha = 0;
	var iLinhaOld = fg.Rows;
	var iLinhaNew = 0;
	var OldStateGrid;
	
	glb_GridIsBuilding = true;
		
	OldStateGrid = fg.Editable;
	fg.Editable = true;
	
	for (i=0; i<selMarca.length; i++)
		{
			if ( selMarca.options[i].selected == true )
			{
				fg.Rows = fg.Rows + 1;
				iLinha = fg.Rows - 1;
				fg.TopRow = iLinha;
				fg.Row = iLinha;
				fg.TextMatrix((iLinha), getColIndexByColKey(fg, 'Marca')) = selMarca.options[i].value;
				fg.TextMatrix((iLinha), getColIndexByColKey(fg, 'ValorPatrocinio')) = 0.00;
				fg.TextMatrix((iLinha), getColIndexByColKey(fg, 'Alterado')) = 1;
				selMarca.options[i].selected = false;
			}
		}
		
	iLinhaNew = fg.Rows;
	
	if (iLinhaNew == iLinhaOld)
	{
			fg.Rows = fg.Rows + 1;
			fg.TopRow = fg.Rows - 1;
			fg.Row = fg.Rows - 1;
	}
	
	fg.Editable = OldStateGrid;
	
	if (fg.Rows > 1)
    {
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [
                            [getColIndexByColKey(fg, 'ValorPatrocinio'),'###,###,###.00','S'],
                            [getColIndexByColKey(fg, 'Prazo'),'###,###,###.00','C']]);
        fg.Editable = true;
    }
    
	js_modalpatrocinios_AfterEdit(0, 0);
	
	glb_GridIsBuilding = false;
}

function excluirLinha()
{
	var nOldStateGrid;
	var iAcaPatrocinioID;

	if (fg.Row > 1)
	{
		_retMsg = window.top.overflyGen.Confirm("Deseja excluir a linha?");
		if (_retMsg == 1)
		{
			iAcaPatrocinioID = 0;
			iAcaPatrocinioID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'AcaPatrocinioID'));

			if (iAcaPatrocinioID != 0)
				glb_aDeleteds[glb_aDeleteds.length] = iAcaPatrocinioID;

			nOldStateGrid = fg.Editable;
			
			fg.Editable = false;
			fg.RemoveItem(fg.Row);
			
			if (fg.Rows < 3 )
				btnIncluir.focus();
			else
				fg.Row = fg.Rows - 1;
				
			fg.Editable = nOldStateGrid;
			
			if (fg.Rows == 2)
				fg.Rows = 1;
			else	
				totalizaGrid();				
			fillEditBox();
		}
	}									
	else			
		return null;
	
}

function excluirTodos()
{
	var nOldStateGrid;
	var iAcaPatrocinioID = 0;
	
	if (fg.Rows > 2)
	{
		_retMsg = window.top.overflyGen.Confirm("Deseja excluir tadas as linhas?");
		if (_retMsg == 1)
		{
			iLinhas = fg.Rows;
					
			for (i=2; i<iLinhas; i++)
			{
				iAcaPatrocinioID = fg.ValueMatrix((i), getColIndexByColKey(fg, 'AcaPatrocinioID'));
				if (iAcaPatrocinioID > 0)
					glb_aDeleteds[glb_aDeleteds.length] = iAcaPatrocinioID;
			}
			
			nOldStateGrid = fg.Editable;
			fg.Editable = false;
			fg.Rows = 1;
			fg.Editable =nOldStateGrid ;
			btnIncluir.focus();			
			fillEditBox();	
		}									
		else			
		    return null;
	}
	else			
		return null;
	
}

function fillEditBox()
{
	var iQuant = 0;
	var iLinhas = 0;
	var nValor = 0;
	var nTotalPlanejado =  roundNumber(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['TotalPlanejado'].value"),2);
	var nTotalRateado = 0;
	var nSaldoRatear = 0;
	var sVerifica = "";
	
	if (fg.Rows > 2)
	  nTotalRateado = fg.ValueMatrix(1, getColIndexByColKey(fg, 'ValorPatrocinio'));
		
	nSaldoRatear = roundNumber((nTotalPlanejado - nTotalRateado),2);
		
	txtTotalPlanejado.value = nTotalPlanejado;	
	txtTotalRateado.value = nTotalRateado;
	txtSaldoRatear.value = nSaldoRatear;
	if (fg.Rows > 2)
	{
		iLinhas = fg.Rows;
		nValor = 0;
		
		for (i=2; i<iLinhas; i++)
		{
			nValor = fg.ValueMatrix((i), getColIndexByColKey(fg, 'ValorPatrocinio'));
			if (nValor == 0)
				iQuant++;
		}
	}	
	txtMarcasNaoRateadas.value = iQuant;
	
	if (iQuant == 0)
		txtValorRateio.value  = 0;
	else
		txtValorRateio.value = roundNumber((nSaldoRatear / iQuant),2);

	sVerifica = txtTotalPlanejado.value;	
	if (sVerifica.indexOf(".") < 0)    
		txtTotalPlanejado.value += '.00';	
		
	sVerifica = txtTotalRateado.value;	
	if (sVerifica.indexOf(".") < 0)    
		txtTotalRateado.value += '.00';			

	sVerifica = txtSaldoRatear.value;			
	if (sVerifica.indexOf(".") < 0)    
		txtSaldoRatear.value += '.00';			
	
	sVerifica = txtValorRateio.value;	
	if (sVerifica.indexOf(".") < 0)    
		txtValorRateio.value += '.00';			

	setupBtnsFromGridState();
	fg.Editable = true;
}


// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalpatrociniosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalpatrociniosDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpatrociniosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpatrocinios_ValidateEdit()
{
    //totalizaGrid();		
	//fillEditBox();	
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpatrocinios_AfterEdit(Row, Col)
{

 if (Row > 1)
	fg.TextMatrix(Row, getColIndexByColKey(fg, 'Alterado')) = 1;
 
 if (Col == getColIndexByColKey(fg, 'ValorPatrocinio') ) 
		fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
 
 totalizaGrid();	
 autoSize();	
 fillEditBox();	
 fg.Row = Row;
 fg.Col = Col;

}

function totalizaGrid(iTipo)
{
	
	var nOldStateGrid;
	var nValorPatrocinio = 0;
	
	if ( ((fg.Rows > 1) && (iTipo = 1)) || (fg.Rows > 2))
	{	
		nOldStateGrid = fg.Editable;
		fg.Editable = false;		
		
		for (i=2; i<fg.Rows; i++)
		{		   
		   nValorPatrocinio += fg.ValueMatrix(i,getColIndexByColKey(fg, 'ValorPatrocinio'));
		}
		fg.TextMatrix(1,getColIndexByColKey(fg, 'ValorPatrocinio')) = nValorPatrocinio;
		fg.TextMatrix(1,getColIndexByColKey(fg, 'Prazo')) = fg.Rows - 2;
		fg.Editable = nOldStateGrid;
	}	
}

function autoSize()
{
    fg.autoSizeMode = 0;
    fg.autoSize(0,fg.Cols-1);
}

function ratear()
{
	var nRatear = parseFloat(txtValorRateio.value);
	var ColValorPatrocinio = getColIndexByColKey(fg, 'ValorPatrocinio');
	if (fg.Rows > 2)
	{
		iLinhas = fg.Rows;
		nValor = 0;
		
		for (i=2; i<iLinhas; i++)
			{
				nValor = fg.ValueMatrix((i), ColValorPatrocinio);
				if (nValor == 0)
				{
					fg.TextMatrix((i), ColValorPatrocinio) = nRatear;
					
				}
			}
		js_modalpatrocinios_AfterEdit(0, 0);	
	}	
	
}

// FINAL DE EVENTOS DE GRID *****************************************

function window_onunload()
{
    dealWithObjects_Unload();
}
