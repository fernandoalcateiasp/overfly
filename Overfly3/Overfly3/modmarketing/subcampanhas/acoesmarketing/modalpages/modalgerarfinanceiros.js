/********************************************************************
modalgerarfinanceiros.js

Library javascript para o modalgerarfinanceiros.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_nMaxStringSize = 1024*1;
var glb_nA1 = 0;
var glb_nA2 = 0;

// Dados do grid .RDS  
var dsoGrid = new CDatatransport("dsoGrid");
// Gravacao de dados do grid .RDS  
var dsoGrava = new CDatatransport("dsoGrava");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
fillGridData()
fillGridData_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalgerarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_modalgerarfinanceirosKeyPress(KeyAscii)
js_modalgerarfinanceiros_ValidateEdit()
js_fg_AfterRowColmodalgerarfinanceiros (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalgerarfinanceirosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // mostra a modal
    //refreshParamsAndDataAndShowModalWin(true);
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Gerar Financeiro', 1);
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    
    glb_nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B3A1' + '\'' + ')') );
    
    glb_nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B3A2' + '\'' + ')') );
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnCanc.disabled = true;
	btnOK.style.visibility = 'hidden';
    	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    
    adjustElementsInForm([['lblTipo','selTipo',26,1,-10,-10],
                          ['lblOrigem', 'selOrigem',5,1,-4],
		    			  ['lblFamilia','selFamilia',24,1,-4],
						  ['lblMarca','selMarca',14,1,-5],
						  ['lblTipoFinanceiro','selTipoFinanceiro',10,1,1],
						  ['lblValorMaximo','txtValorMaximo',10,1,-8],
						  ['lbldtInicio','txtdtInicio',10,2,571],
						  ['lbldtFim','txtdtFim',10,2],
						  ['lblClasspessoa', 'selClasspessoa',18,3,-10],
						  ['lblProduto', 'selProduto',30,3,-5],
                          ['lblExcecoes','selExcecoes',22,3,-5],
                          ['lblCampanhas', 'chkCampanhas', 3, 3, 2],
                          ['lblObservacao', 'txtObservacao', 18, 3, -10],
                          ['btnListar','btn',btn_width,4,588],						  
                          ['btnGerar','btn',btn_width,4,10]], null, null, true);

	selTipo.style.height = 70;
	selOrigem.style.height = 70;
	selFamilia.style.height = 70;
	selMarca.style.height = 70;
	selExcecoes.style.height = 70;
	selClasspessoa.style.height = 70;
	selProduto.style.height = 70;
	btnGerar.disabled = true;
	
	selTipo.onchange = selTipo_onchange;
	selOrigem.onchange = selOrigem_onchange;
	selFamilia.onchange = selFamilia_onchange;	
	selMarca.onchange = selMarca_onchange;
	selTipoFinanceiro.onchange = selTipoFinanceiro_onchange;
	txtdtInicio.onkeypress = txtdtInicio_onkeypress;
	txtdtFim.onkeypress = txtdtFim_onkeypress;
	txtValorMaximo.onkeypress = txtValorMaximo_onkeypress;
	txtObservacao.onkeypress = txtObservacao_onkeypress;
	
	selOptByValueInSelect(getHtmlId(), 'selTipoFinanceiro', 1002);
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = btnGerar.offsetLeft + btnGerar.offsetWidth + ELEM_GAP;    
        height = btnGerar.offsetTop + btnGerar.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 15;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10) - 20;
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + btnGerar.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Confirmar';
		title = 'Confirmar Financeiros';
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // codigo privado desta janela
    var _retMsg;
    var sMsg = '';
    var iTipoQuantSelected = 0;
        
    if ( sMsg != '' )
    {
        _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
    }    
    
    if (controlID == btnListar.id)
		fillGridData(false);
	else if (controlID == btnGerar.id)
	{
		for (i=0; i<selTipo.length; i++)
		{
			if ( selTipo.options[i].selected == true )
			{
				iTipoQuantSelected++;
			}
		}
		if (iTipoQuantSelected != 1)
		{
			if ( window.top.overflyGen.Alert('N�o � poss�vel gerar um financeiro com mais de um tipo de despesa.') == 0 )
				return null;
			
			return null;
		}
		fillGridData(true);
	}	
    
    //restoreInterfaceFromModal();
    if (controlID == btnCanc.id)
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{   
    // @@ atualizar parametros e dados aqui
    lockControlsInModalWin(true);
    
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(gravacao)
{
	lockControlsInModalWin(true);
	
	var iGrava = 0;
	glb_nDSOs = 1;
	
	if (gravacao)
		iGrava = 2;
	else 		
	    iGrava = 1;
	    
	if (!verificaData(txtdtInicio.value))
		return null;
	    
	if (!verificaData(txtdtFim.value))
		return null;   
	  
	var sFiltroOrigem = '';
	var sFiltroFamilia = '';
	var sFiltroMarca = '';
	var sFiltroProduto = '';
	var sFiltroClassPessoa = '';
	var sFiltroExcecoes = '';
	var sFiltroTipo = '';
	var sdtInicio = normalizeDate_DateTime(txtdtInicio.value, true);
	var sdtFim = normalizeDate_DateTime(txtdtFim.value, true);
	var sValorMaximo = trimStr(txtValorMaximo.value);
	var sObservacao = trimStr(txtObservacao.value);
	var strPars = '';

	if (iGrava != 2) {
	    for (i = 0; i < selOrigem.length; i++) {
	        if (selOrigem.options[i].selected == true && selOrigem.options[i].value != "") {
	            sFiltroOrigem += '/' + selOrigem.options[i].value;
	        }
	    }

	    sFiltroOrigem = (sFiltroOrigem == '' ? '' : sFiltroOrigem + '/');

	    for (i = 0; i < selFamilia.length; i++) {
	        if (selFamilia.options[i].selected == true && selFamilia.options[i].value != "") {
	            sFiltroFamilia += '/' + selFamilia.options[i].value;
	        }
	    }

	    sFiltroFamilia = (sFiltroFamilia == '' ? '' : sFiltroFamilia + '/');

	    for (i = 0; i < selMarca.length; i++) {
	        if (selMarca.options[i].selected == true && selMarca.options[i].value != "") {
	            sFiltroMarca += '/' + selMarca.options[i].value;
	        }
	    }
	    sFiltroMarca = (sFiltroMarca == '' ? '' : sFiltroMarca + '/');

	    for (i = 0; i < selProduto.length; i++) {
	        if (selProduto.options[i].selected == true && selProduto.options[i].value != "") {
	            sFiltroProduto += '/' + selProduto.options[i].value;
	        }
	    }

	    sFiltroProduto = (sFiltroProduto == '' ? '' : sFiltroProduto + '/');

	    for (i = 0; i < selClasspessoa.length; i++) {
	        if (selClasspessoa.options[i].selected == true && selClasspessoa.options[i].value != "") {
	            sFiltroClassPessoa += '/' + selClasspessoa.options[i].value;
	        }
	    }

	    sFiltroClassPessoa = (sFiltroClassPessoa == '' ? '' : sFiltroClassPessoa + '/');

	    for (i = 0; i < selExcecoes.length; i++) {
	        if (selExcecoes.options[i].selected == true && selExcecoes.options[i].value != "") {
	            sFiltroExcecoes += '/' + selExcecoes.options[i].value;
	        }
	    }
	    sFiltroExcecoes = (sFiltroExcecoes == '' ? '' : sFiltroExcecoes + '/');

	    for (i = 0; i < selTipo.length; i++) {
	        if (selTipo.options[i].selected == true && selTipo.options[i].value != "") {
	            sFiltroTipo += '/' + selTipo.options[i].value;
	        }
	    }
	    sFiltroTipo = (sFiltroTipo == '' ? '' : sFiltroTipo + '/');
	}
	else 
	{
	    if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Origem')) != '') 
	    {
	        sFiltroOrigem = '/' + fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Origem')) + '/';
	    }
	    if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FamiliaID')) != '') 
	    {
	        sFiltroFamilia = '/' + fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FamiliaID')) + '/';
	    }
	    if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'MarcaID')) != '')
	    {
            sFiltroMarca = '/' + fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'MarcaID')) + '/';
        }
        if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ClassPessoaID')) != '')
        {
            sFiltroClassPessoa = '/' + fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ClassPessoaID')) + '/';
        }
        if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ExcecaoID')) != '')
        {
            sFiltroExcecoes = '/' + fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ExcecaoID')) + '/';
        }
        if (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID')) != '')
        {
            sFiltroTipo = '/' + fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID')) + '/';
        }
        
        for (i = 0; i < selProduto.length; i++) {
            if (selProduto.options[i].selected == true) {
                sFiltroProduto += '/' + selProduto.options[i].value;
            }
        }

        sFiltroProduto = (sFiltroProduto == '' ? '' : sFiltroProduto + '/');
	}

	strPars = '?EmpresaID=' + escape(glb_aEmpresaData[0]);
	strPars += '&TipoFinanceiroID=' + escape(selTipoFinanceiro.value);
	strPars += '&BaixaCampanhas=' + escape((chkCampanhas.checked ? 1 : 0));
	strPars += '&dtInicio=' + escape(sdtInicio);
	strPars += '&dtFim=' + escape(sdtFim);
	strPars += '&TiposDespesa=' + escape(sFiltroTipo);
	strPars += '&Origens=' + escape(sFiltroOrigem);
	strPars += '&Familias=' + escape(sFiltroFamilia);
	strPars += '&Marcas=' + escape(sFiltroMarca);
	strPars += '&Produtos=' + escape(sFiltroProduto);
	strPars += '&ClassPessoas=' + escape(sFiltroClassPessoa);
	strPars += '&Excecoes=' + escape(sFiltroExcecoes);
	strPars += '&TipoResultadoID=' + escape(iGrava);
	strPars += '&ValorMaximo=' + escape(sValorMaximo);
	strPars += '&Observacao=' + escape(sObservacao);
	strPars += '&ProprietarioID=' + escape(glb_nUserID);

	dsoGrid.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/acoesmarketing/serverside/listarfinanceiro.aspx' + strPars;
	
	if (iGrava == 2 )
		dsoGrid.ondatasetcomplete = gerarfinanceiro_DSC;
	else 		
		dsoGrid.ondatasetcomplete = fillGridData_DSC;
		
    dsoGrid.Refresh();

}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	window.focus();
	
	if ((dsoGrid.recordset.fields.count == 2) || (dsoGrid.recordset.fields.count == 1))
	{
		if (dsoGrid.recordset['Resultado'].value != '')
		{
			if ( window.top.overflyGen.Alert(dsoGrid.recordset['Resultado'].value) == 0 )
			{
			    fg.Rows = 1;
				return null;
			}
		
				lockControlsInModalWin(false);	
				return null;
		}
	}


    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '10';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	var aHoldCols = new Array();

    headerGrid(fg,['Tipo',
                   'Origem',
   				   'Fam�lia',
				   'Marca',
				   'ClassPessoa',
				   'Valor',
				   'Percentual',
				   'Ordem',
				   'FamiliaID',
				   'MarcaID',
				   'ClassPessoaID',
				   'TipoID',
				   'Excecao',
				   'ExcecaoID'],[7,8,9,10,11,13]);

    fillGridMask(fg,dsoGrid,['Tipo',
                             'Origem',
   							 'Familia',
							 'Marca',
				             'ClassPessoa',
							 'Valor',
							 'Percentual',
							 'Ordem',
							 'FamiliaID',
							 'MarcaID',
							 'ClassPessoaID',
							 'TipoID',
							 'Excecao',
							 'ExcecaoID'],
							 ['','','','','','999999999.99','999.99','','','','','','',''],
							 ['','','','','','###,###,##0.00','##0.00','','','','','','','']);
    fg.Redraw = 0;

    alignColsInGrid(fg,[5,6]);

	gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Valor'),'###,###,###.00','S']]);

	fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

	paintCellsSpecialyReadOnly();

	fg.ExplorerBar = 5;

	lockControlsInModalWin(false);

	btnGerar_status(fg);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fg.Redraw = 2;
}

function gerarfinanceiro_DSC() {
    if (dsoGrid.recordset.fields.count == 2)
	{
		if (dsoGrid.recordset['Resultado'].value != '')
		{
			if ( window.top.overflyGen.Alert(dsoGrid.recordset['Resultado'].value) == 0 )
				return null;
		}
		else if (dsoGrid.recordset['FinanceiroID'].value != 0)
		{
			var _retConf = window.top.overflyGen.Confirm('Criado financeiro: ' + dsoGrid.recordset['FinanceiroID'].value + '.\nDeseja detalhar?', 1);

			if ( _retConf == 1)
					sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_aEmpresaData[0], dsoGrid.recordset['FinanceiroID'].value));
		}
		lockControlsInModalWin(false);
		fg.Rows = 1;
		btnGerar_status(fg);	
		return null;
    }
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

function btnGerar_status(fg)
{
	if (fg.Rows > 2)
		btnGerar.disabled = false;
	else
		btnGerar.disabled = true;
		
	//DireitoEspecifico
    //Acoes de Marketing->Acoes de Marketing->Modal Gerar Financeiros(B3)
    //40003 B3-> A1&&A2-> Habilita o botao Gerar.
	if ((!glb_nA1) || (!glb_nA2))
		btnGerar.disabled = true;
}
function fg_DblClick()
{
	;
}
// EVENTOS DE GRID **************************************************
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerarfinanceirosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarfinanceirosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerarfinanceiros_ValidateEdit()
{
    ;
}
function gerarfinanceiro()
{
 ;
}

function selTipo_onchange()
{
	fg.Rows =1;
	btnGerar_status(fg);
}
function selOrigem_onchange()
{
    fg.Rows =1;
	btnGerar_status(fg);
}
function selFamilia_onchange()
{
	fg.Rows =1;
	btnGerar_status(fg);
}
function selMarca_onchange()
{
	fg.Rows =1;
	btnGerar_status(fg);
}
function selTipoFinanceiro_onchange()
{
    fg.Rows = 1;

    //Habilita combo de campanhas somente para fin Rec. BJBN 09/03/2015.
    if (selTipoFinanceiro.value == 1002)
        chkCampanhas.disabled = false;
    else
    {
        chkCampanhas.checked = false;
        chkCampanhas.disabled = true;
    }

	btnGerar_status(fg);
}
function txtdtInicio_onkeypress()
{
	fg.Rows =1;
	btnGerar_status(fg);
}
function txtdtFim_onkeypress()
{
	fg.Rows =1;
	btnGerar_status(fg);
}
function txtValorMaximo_onkeypress()
{
	fg.Rows =1;
	btnGerar_status(fg);
}
function txtObservacao_onkeypress()
{
	fg.Rows =1;
	btnGerar_status(fg);
}

function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        	lockControlsInModalWin(false);
		return false;
	}
	return true;
}
// FINAL DE EVENTOS DE GRID *****************************************
