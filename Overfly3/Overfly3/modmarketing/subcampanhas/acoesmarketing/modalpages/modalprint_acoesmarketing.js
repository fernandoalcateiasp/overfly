/********************************************************************
modalprint_acoesmarketing.js

Library javascript para o modalprint.asp
Conceitos 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var nDsosToArrive;

// dso usado para envia e-mails 
var dsoEMail = new CDatatransport("dsoEMail");
// dso usado para como de empresas 
var dsoEmpresas = new CDatatransport("dsoEmpresas");
var dsoEstados = new CDatatransport("dsoEstados");
var dsoMarcas = new CDatatransport("dsoMarcas");
var dsoProprietarios = new CDatatransport("dsoProprietarios");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()
    
	
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    if ( selReports.disabled == false )
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
        
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
	}        
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Relat�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style)
    {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta combo de relatorios
    with (selReports.style)
    {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
	// ajusta o divAcoesMarketing
    with (divAcoesMarketing.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    
    // O estado do botao btnOK
    btnOK_Status();
    
    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // linka os divs nos relatorios
    linkDivsAndReports();

    // ajusta o divRelatorioVendas
    adjustdivAcoesMarketing();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

function adjustdivAcoesMarketing()
{
	nDsosToArrive = 4;
	
	setConnection(dsoEmpresas);

	dsoEmpresas.SQL = 'SELECT DISTINCT RelPesCon.SujeitoID AS EmpresaID, Empresas.Fantasia AS Empresa ' +
						'FROM Pessoas Empresas WITH(NOLOCK) ' +
								'INNER JOIN RelacoesPesCon RelPesCon WITH(NOLOCK) ON RelPesCon.SujeitoID=Empresas.PessoaID ' +
						'WHERE RelPesCon.TipoRelacaoID=61 AND RelPesCon.EstadoID <> 5 ' +
						'ORDER BY Empresa';

    dsoEmpresas.ondatasetcomplete = adjustdivAcoesMarketing_DSC;
    dsoEmpresas.refresh();	
    
    setConnection(dsoEstados);

	dsoEstados.SQL = 'SELECT b.RecursoID AS EstadoID, b.RecursoFantasia AS Estado ' +
					 'FROM RelacoesRecursos a WITH(NOLOCK) ' +
							 'INNER JOIN Recursos b WITH(NOLOCK) ON a.SujeitoID = b.RecursoID ' +
					 'WHERE (a.ObjetoID = 364 AND a.TipoRelacaoID = 3 AND b.RecursoID NOT IN (5)) ' +
					 'ORDER BY Estado ';


    dsoEstados.ondatasetcomplete = adjustdivAcoesMarketing_DSC;
    dsoEstados.refresh();	
    
    setConnection(dsoMarcas);

	dsoMarcas.SQL = 'SELECT DISTINCT a.ConceitoID AS MarcaID, a.Conceito AS Marca ' +
					'FROM Conceitos a WITH(NOLOCK) ' +
					'INNER JOIN Conceitos b WITH(NOLOCK) ON a.ConceitoID = b.MarcaID ' +
					'INNER JOIN RelacoesPesCon c WITH(NOLOCK) ON b.ConceitoID = c.ObjetoID  ' +
					'WHERE a.EstadoID = 2 AND c.TipoRelacaoID = 61 AND c.EstadoID NOT IN (1,4,5) ' +
					'ORDER BY Marca ';

    dsoMarcas.ondatasetcomplete = adjustdivAcoesMarketing_DSC;
    dsoMarcas.refresh();	
    
    setConnection(dsoProprietarios);

	dsoProprietarios.SQL = 'SELECT DISTINCT b.PessoaID, b.Fantasia ' +
					'FROM RelacoesPesCon a WITH(NOLOCK) ' +
					'INNER JOIN Pessoas b WITH(NOLOCK) ON a.ProprietarioID = b.PessoaID ' +					
					'WHERE a.TipoRelacaoID = 61 AND a.EstadoID NOT IN (1,5) ' +
					'ORDER BY Fantasia ';

    dsoProprietarios.ondatasetcomplete = adjustdivAcoesMarketing_DSC;
    dsoProprietarios.refresh();	
}

function adjustdivAcoesMarketing_DSC() {
    nDsosToArrive--;
	if (nDsosToArrive > 0)
		return null;
	
	clearComboEx(['selEmpresas']);
	
	while (! dsoEmpresas.recordset.EOF )
	{
		optionStr = dsoEmpresas.recordset['Empresa'].value;
		optionValue = dsoEmpresas.recordset['EmpresaID'].value;
		oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selEmpresas.add(oOption);
		dsoEmpresas.recordset.MoveNext();
	}
	
	while (! dsoEstados.recordset.EOF )
	{
		optionStr = dsoEstados.recordset['Estado'].value;
		optionValue = dsoEstados.recordset['EstadoID'].value;
		oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selEstados.add(oOption);
		dsoEstados.recordset.MoveNext();
	}
	
	while (! dsoMarcas.recordset.EOF )
	{
		optionStr = dsoMarcas.recordset['Marca'].value;
		optionValue = dsoMarcas.recordset['MarcaID'].value;
		oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selMarcas.add(oOption);
		dsoMarcas.recordset.MoveNext();
	}
	
	while (! dsoProprietarios.recordset.EOF )
	{
		optionStr = dsoProprietarios.recordset['Fantasia'].value;
		optionValue = dsoProprietarios.recordset['PessoaID'].value;
		oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selProprietarios.add(oOption);
		dsoProprietarios.recordset.MoveNext();
	}

	adjustElementsInForm([['lblEmpresas','selEmpresas',13,1,-10,-10],
						['lblEstados','selEstados',10,1,-5],
						['lblMarcas','selMarcas',13,1,-5],
						['lblProprietarios','selProprietarios',17,1,-5],
						['lbldtInicio','txtdtInicio',10,5,-10,-15],
						['lbldtFim','txtdtFim',10,5],
						['lblSoPatrociniosPendentes','chkSoPatrociniosPendentes',3,5],
						['lblSaldoParEfetivo','chkSaldoParEfetivo',3,5,-5],
						['lblFiltro','txtFiltro',55,6,-10]],null,null,true);

	selEmpresas.style.height = 140;
	selEstados.style.height = 140;
	selMarcas.style.height = 140;
	selProprietarios.style.height = 140;
	
	chkSoPatrociniosPendentes.onclick = chkSoPatrociniosPendentes_onclick; 
	chkSaldoParEfetivo.disabled = true;
	
	txtdtInicio.maxLength = 10;
	
	if (DATE_SQL_PARAM == 103)
		txtdtInicio.value = '01/09/2007';
	else
		txtdtInicio.value = '09/01/2007';

	txtdtFim.maxLength = 10;
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    if ( glb_arrayReports != null )
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
    divAcoesMarketing.setAttribute('report', 40130 , 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if ( this.disabled == false )
        this.focus();
    
    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    var i, elem;
    
    if ( selReports.selectedIndex != -1 )
    {
        btnOKStatus = false;                
    }
    
    // 0 div visivel deve ser coerente com o relatorio selecionado
    if ( selReports.value != getCurrDivReportAttr() )
        btnOKStatus = true;

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    if ( (selReports.value == 40130) && (glb_sCaller == 'PL') )
        relatorioAcoesMarketing();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';    
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
    if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}

function chkSoPatrociniosPendentes_onclick()
{
	if (chkSoPatrociniosPendentes.checked)
	{
		chkSaldoParEfetivo.disabled = false;		
	}
	else
	{
		chkSaldoParEfetivo.disabled = true;		
		chkSaldoParEfetivo.checked = false;		
	}
}	

function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        lockControlsInModalWin(false);
		return false;
	}
	return true;
}

function relatorioAcoesMarketing()
{
    
    /*
    var dirA1;
    var dirA2;
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    */
    
    var nEmpresaID = 0;
    var sInformation = '';
    var nTopHeader = 0;
    var sFiltroEmpresas = '';
    var sNomeEmpresas = '';
    var sFiltroEstados = '';
    var sFiltroMarcas = '';
    var sFiltroProprietarios = '';
    var bPatrociniosPendentes = (chkSoPatrociniosPendentes.checked ? 1 : 0);
    var bSaldoPatEfetivo = (chkSaldoParEfetivo.checked ? 1 : 0);
    var sFiltro = (txtFiltro.value == '' ? 'NULL' : '\'' + txtFiltro.value + '\'');
    var bFirstTime = 1;
    
    if (!verificaData(txtdtInicio.value))
    	return null;
	   
    if (!verificaData(txtdtFim.value))
    	return null;
	    
    var ndtInicio = '\'' + normalizeDate_DateTime(txtdtInicio.value, true) + '\'';
    var ndtFim = '\'' + normalizeDate_DateTime(txtdtFim.value, true) + '\'';
    
	
	for (i=0; i<selEmpresas.length; i++)
    {
        if ( selEmpresas.options[i].selected == true )
        {
            sFiltroEmpresas += (bFirstTime == 1 ? '' : ',') + selEmpresas.options[i].value;
            sNomeEmpresas += (bFirstTime == 1 ? '' : ',') + selEmpresas.options[i].innerText;
            bFirstTime = 0;
        }
    }
    sFiltroEmpresas = (sFiltroEmpresas == '' ? 'NULL' : '\'' + sFiltroEmpresas + '\'');
    sNomeEmpresas = (sNomeEmpresas == '' ? 'Todas' : sNomeEmpresas);
    
    bFirstTime = 1;
    for (i=0; i<selEstados.length; i++)
    {
        if ( selEstados.options[i].selected == true )
        {
            sFiltroEstados += (bFirstTime == 1 ? '' : ',') + selEstados.options[i].value;
            bFirstTime = 0;
        }
    }
    sFiltroEstados = (sFiltroEstados == '' ? 'NULL' : '\'' + sFiltroEstados + '\'');
    
    bFirstTime = 1;
    for (i=0; i<selMarcas.length; i++)
    {
        if ( selMarcas.options[i].selected == true )
        {
            sFiltroMarcas += (bFirstTime == 1 ? '' : ',') + selMarcas.options[i].value;
            bFirstTime = 0;
        }
    }
    sFiltroMarcas = (sFiltroMarcas == '' ? 'NULL' : '\'' + sFiltroMarcas + '\'');
    
    for (i=0; i<selProprietarios.length; i++)
    {
        if ( selProprietarios.options[i].selected == true )
        {
            sFiltroProprietarios += (sFiltroProprietarios == '' ? '' : ',') + selProprietarios.options[i].value;            
        }
    }
    sFiltroProprietarios = (sFiltroProprietarios == '' ? 'NULL' : '\'' + sFiltroProprietarios + '\'');

    if ((ndtInicio == null) || (ndtInicio == '\'null\''))
		ndtInicio = 'NULL';
	
	if ((ndtFim == null) || (ndtFim == '\'null\''))
		ndtFim = 'NULL';

	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

	var formato = 2;

	var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&Formato=" + formato + "&FiltroEmpresas=" + sFiltroEmpresas + "&FiltroEstados=" + sFiltroEstados +
	                    "&sFiltroMarcas=" + sFiltroMarcas + "&sFiltroProprietarios=" + sFiltroProprietarios + "&ndtInicio=" + ndtInicio + "&ndtFim=" + ndtFim +
                        "&bPatrociniosPendentes=" + bPatrociniosPendentes + "&bSaldoPatEfetivo=" +  bSaldoPatEfetivo + "&sFiltro=" + sFiltro;

	pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
	lockControlsInModalWin(true);
	window.document.onreadystatechange = reports_onreadystatechange;
	window.document.location = SYS_PAGESURLROOT + '/modmarketing/subcampanhas/acoesmarketing/serverside/Reports_acoesmarketing.aspx?' + strParameters;
}
