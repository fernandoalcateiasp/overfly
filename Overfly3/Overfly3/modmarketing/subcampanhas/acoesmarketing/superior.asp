<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="acoesmarketingsup01Html" name="acoesmarketingsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modqualidade/commonqualidade/commonqualidade.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/acoesmarketing/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/acoesmarketing/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="acoesmarketingsup01Body" name="acoesmarketingsup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblGrupoAcao" name="lblGrupoAcao" class="lblGeneral">Grupo de A��o</p>
        <input type="text" id="txtGrupoAcao" name="txtGrupoAcao" DATASRC="#dsoSup01" DATAFLD="GrupoAcao" class="fldGeneral"></input>
        <p id="lblTipoAcaoID" name="lblTipoAcaoID" class="lblGeneral">Tipo de A��o</p>		
        <select id="selTipoAcaoID" name="selTipoAcaoID" DATASRC="#dsoSup01" DATAFLD="TipoAcaoID" class="fldGeneral"></select>        
		<p id="lblAcao" name="lblAcao" class="lblGeneral">A��o</p>
        <input type="text" id="txtAcao" name="txtAcao" DATASRC="#dsoSup01" DATAFLD="Acao" class="fldGeneral"></input>
		<p id="lblDetalhe" name="lblDetalhe" class="lblGeneral">Detalhe</p>
        <input type="text" id="txtDetalhe" name="txtDetalhe" DATASRC="#dsoSup01" DATAFLD="Detalhe" class="fldGeneral"></input>
		<p id="lblPeriodo" name="lblPeriodo" class="lblGeneral">Per�odo</p>
        <input type="text" id="txtPeriodo" name="txtPeriodo" DATASRC="#dsoSup01" DATAFLD="Periodo" class="fldGeneral"></input>       
		<p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtdtInicio" name="txtdtInicio" DATASRC="#dsoSup01" DATAFLD="V_dtInicio" class="fldGeneral"></input>
        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtdtFim" name="txtdtFim" DATASRC="#dsoSup01" DATAFLD="V_dtFim" class="fldGeneral"></input>
		<p id="lbldtVencimento" name="lbldtVencimento" class="lblGeneral">Vencimento</p>
        <input type="text" id="txtdtVencimento" name="txtdtVencimento" DATASRC="#dsoSup01" DATAFLD="dtVencimento" class="fldGeneral"></input>
		<p id="lblAtraso" name="lblAtraso" class="lblGeneral">Atraso</p>
        <input type="text" id="txtAtraso" name="txtAtraso" DATASRC="#dsoSup01" DATAFLD="Atraso" class="fldGeneral"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
        <p id="lblTotalPlanejado" name="lblTotalPlanejado" class="lblGeneral">Total Planejado</p>
        <input type="text" id="txtTotalPlanejado" name="txtTotalPlanejado" DATASRC="#dsoSup01" DATAFLD="TotalPlanejado" class="fldGeneral"></input>
        <p id="lblTotalPatrocinio" name="lblTotalPatrocinio" class="lblGeneral">Total Patroc�nio</p>
        <input type="text" id="txtTotalPatrocinio" name="txtTotalPatrocinio" DATASRC="#dsoSup01" DATAFLD="TotalPatrocinio" class="fldGeneral"></input>
        <p id="lblDiferenca" name="lblDiferenca" class="lblGeneral">Diferen�a</p>
        <input type="text" id="txtDiferenca" name="txtDiferenca" DATASRC="#dsoSup01" DATAFLD="Diferenca" class="fldGeneral"></input>
        <p id="lblTotalPago" name="lblTotalPago" class="lblGeneral">Total Pago</p>
        <input type="text" id="txtTotalPago" name="txtTotalPago" DATASRC="#dsoSup01" DATAFLD="TotalPago" class="fldGeneral"></input>
		<p id="lblTotalRecebido" name="lblTotalRecebido" class="lblGeneral">Total Recebido</p>
        <input type="text" id="txtTotalRecebido" name="txtTotalRecebido" DATASRC="#dsoSup01" DATAFLD="TotalRecebido" class="fldGeneral"></input>        
        <p id="lblSaldoPlanejado" name="lblSaldoPlanejado" class="lblGeneral">Saldo Planejado</p>
        <input type="text" id="txtSaldoPlanejado" name="txtSaldoPlanejado" DATASRC="#dsoSup01" DATAFLD="SaldoPlanejado" class="fldGeneral"></input>
        <p id="lblSaldoEfetivo" name="lblSaldoEfetivo" class="lblGeneral">Saldo Efetivo</p>
        <input type="text" id="txtSaldoEfetivo" name="txtSaldoEfetivo" DATASRC="#dsoSup01" DATAFLD="SaldoEfetivo" class="fldGeneral"></input>
        <p id="lblSaldoPatrocinio" name="lblSaldoPatrocinio" class="lblGeneral">Saldo Patroc�nio</p>
        <input type="text" id="txtSaldoPatrocinio" name="txtSaldoPatrocinio" DATASRC="#dsoSup01" DATAFLD="SaldoPatrocinio" class="fldGeneral"></input>
    </div>
</body>

</html>
