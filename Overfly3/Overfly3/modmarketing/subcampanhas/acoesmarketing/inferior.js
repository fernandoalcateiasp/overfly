/********************************************************************
inferior.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var iempresa = 0;
// variavel de window setInterval
var __winTimerHandler = null;

// Dados do registro corrente .SQL (no minimo: regID, observacoes, propID e alternativoID) 
var dso01JoinSup = new CDatatransport("dso01JoinSup");
// Dados de combos estaticos do form .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Dados de combos dinamicos do form .URL 
var dsoDynamicCmbs = new CDatatransport("dsoDynamicCmbs");
// Descricao dos estados atuais dos registros do grid corrente .SQL         
var dsoStateMachineLkp = new CDatatransport("dsoStateMachineLkp");        
// Dados do combo de filtros da barra inferior .SQL         
var dsoFiltroInf = new CDatatransport("dsoFiltroInf");
// Dados dos combos de proprietario e alternativo         
var dsoCmbsPropAlt = new CDatatransport("dsoCmbsPropAlt");
// Dados de um grid .SQL 
var dso01Grid = new CDatatransport("dso01Grid");
var dsoCmb01Grid_01 = new CDatatransport("dsoCmb01Grid_01");
var dsoCmb02Grid_01 = new CDatatransport("dsoCmb02Grid_01");
var dsoCmb03Grid_01 = new CDatatransport("dsoCmb03Grid_01");
var dso01GridLkp = new CDatatransport("dso01GridLkp");
var dso02GridLkp = new CDatatransport("dso02GridLkp");
var dso03GridLkp = new CDatatransport("dso03GridLkp");
var dso04GridLkp = new CDatatransport("dso04GridLkp");
var dsoDeleteDocumento = new CDatatransport("dsoDeleteDocumento");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
	setupPage()
	adjustElementsInDivsNonSTD()
	putSpecialAttributesInControls()
	optChangedInCmb(cmb)
	frameIsAboutToApplyRights(folderID)
	prgServerInf(btnClicked)
	finalOfInfCascade(btnClicked)
	prgInterfaceInf(btnClicked, pastaID, dso)
	treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
	btnLupaClicked(btnClicked)
	btnBarClicked(controlBar, btnClicked)
	btnBarNotEspecClicked(controlBar, btnClicked)
	btnAltPressedInGrid( folderID )
	btnAltPressedInNotGrid( folderID )
	addedLineInGrid(folderID, grid, nLineInGrid, dso)
	selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
	fillCmbFiltsInfAutomatic(folderID)
	modalInformForm(idElement, param1, param2)
    
FUNCOES DA MAQUINA DE ESTADO:
	stateMachOpened( currEstadoID )
	stateMachClosed( oldEstadoID, currEstadoID )
    
EVENTOS DO GRID FG
	fg_AfterRowColChange_Prg()
	fg_DblClick_Prg()
	fg_ChangeEdit_Prg()
	fg_ValidateEdit_Prg()
	fg_BeforeRowColChange_Prg()
	fg_EnterCell_Prg()
	fg_MouseUp_Prg()
	fg_MouseDown_Prg()
	fg_BeforeEdit_Prg()
	
FUNCOES DO PROGRAMADOR (PARTICULARES DO FORM):	
	    
********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 

    glb_aGridStaticCombos = [ [20010, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dso02GridLkp'   , ''             , ''],
                                       [0, 'dso03GridLkp'   , ''             , ''],
                                       [0, 'dso04GridLkp'   , ''             , '']]],
							  [23201, [[0, 'dsoCmb01Grid_01', 'fldName', 'fldID'],
									   [0, 'dso01GridLkp'   , ''             , ''],
									   [0, 'dso02GridLkp'   , ''             , '']]] ];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
        
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03'],
                               [20008, 20009, [20010, 23201, 23202]] );
                               
     iempresa = getCurrEmpresaData();

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailinf.js

Usar para ajustes graficos da interface.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupPage()
{
    //@@ Ajustar os divs
    adjustDivs('inf',[[1,'divInf01_01'],
                      [1,'divInf01_02'],
                      [1,'divInf01_03']]);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao after_SetupPage() do js_detailinf.js

Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailinf.js

Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Usar para definir a pasta default pela variavel da automacao
glb_pastaDefault

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    glb_pastaDefault = 23201;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao selChanged() do js_interfaceex.js

Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
cmb - referencia ao combo que disparou o evento

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;

    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao finalDSOsCascade() do js_detailinf.js

O frame work esta prestes a setar os botoes da barra inferior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    // Pasta de LOG / Metas Vendas / Vendas / Lancamentos
    if ((folderID == 20010) || (folderID == 23202))
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailinf.js

Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chamar aqui funcoes particulares do form que forem necessarias
    // antes de trocar o div

	// Se houver operacoes de banco de dados particulares do form,
	// mover esta funcao para os finais retornos destas operacoes no
	// banco de dados
	finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada ao final da funcao prgServerInf(btnClicked) que e
disparada pelo frame work,

Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
	    
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailinf.js

Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

	showBtnsEspecControlBar('inf', false, [1,1,1,1]);
	tipsBtnsEspecControlBar('inf', ['','','','']);
	
	// remove botao de impressao
	especBtnIsPrintBtn('inf', 0);
	
	if(pastaID == 23201)
	{
		showBtnsEspecControlBar('inf', true, [1,1,0,0]);
		tipsBtnsEspecControlBar('inf', ['Inserir patroc�nios','Alterar respons�vel','','']);
	}
	else if(pastaID == 23202)
	{
		showBtnsEspecControlBar('inf', true, [1,0,0,0]);
		tipsBtnsEspecControlBar('inf', ['Detalha financeiro','','','']);
	}
    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{
	var nCurrEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['EstadoID'].value");
	
	var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,('getCurrRightValue(\'SUP\',\'A1\')') );

	var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,('getCurrRightValue(\'SUP\',\'A2\')') );

    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC')
          || (btnClicked == 'SUPEST') )
        setupEspecBtnsControlBar('sup', 'HHHDHH');
    else    
        setupEspecBtnsControlBar('sup', 'HDDDDD');
	
	if (folderID == 23201)
		setupEspecBtnsControlBar('inf', 'HHDD');
	
	else if (folderID == 23202)
		setupEspecBtnsControlBar('inf', 'HDDD');
	
	else
		setupEspecBtnsControlBar('inf', 'DDDD');

    //DireitoEspecifico
    //Acoes de Marketing->Acoes de Marketing->SUP
    //23200 SFS-Grupo Acoes de Marketing-> !A1||!A2 && Estado!=1 -> Desabilita o botao alterar.
	if ( (nCurrEstadoID != 1) && ((!nA1) || (!nA2)))
	{
	    var strBtns = currentBtnsCtrlBarString('sup');
        var aStrBtns = strBtns.split('');
        aStrBtns[3] = 'D';
        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window,'sup', strBtns);
    }
}

/********************************************************************
Funcao disparada pelo frame work.

Chamada pelo evento onClickBtnLupa() do js_detailinf.js

Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
	;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	
    
    if ( (controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 23201) )
    {
	   openModalPatrocinios();
	}	   
	
	if ( (controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 23201) )
    {
        openModalResponsavel();
    }
	
	if ( (controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 23202) )
    {
		if (fg.Row > 0)
			sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(iempresa[0],getCellValueByColKey(fg, 'FinanceiroID', fg.Row)));;
	}	   

}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editCurrFolder() do js_detailinf.js

Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{
	setupEspecBtnsControlBar('inf', 'DDDD');
	
	// Merge de Colunas
	fg.MergeCells = 0;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editCurrFolder() do js_detailinf.js

Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{
	;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao addLineInGrid() do js_detailinf.js

Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{
	setupEspecBtnsControlBar('inf', 'DDDD');
	// Merge de Colunas
	fg.MergeCells = 0;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo js_detailinf.js

Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{
    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em dois pontos do js_detailinf.js

Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados particulares do form ou deixa o frame work preencher o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diveros pontos do js_detailinf.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
	if (idElement.toUpperCase() == 'MODALPATROCINIOSHTML')
    {
        if ( param1 == 'OK' )
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    
    if (idElement.toUpperCase() == 'MODALRESPONSAVELHTML')
    {
        if ( param1 == 'OK' )
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    

            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{
}

function fg_DblClick_Prg()
{
	if (btnIsEnableInCtrlBar('inf', 12) && (keepCurrFolder() == 23202) && (!fg.Editable) )
              sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(iempresa[0], getCellValueByColKey(fg, 'FinanceiroID', fg.Row)));
}

function fg_ChangeEdit_Prg()
{
}

function fg_ValidateEdit_Prg()
{
}

function fg_BeforeRowColChange_Prg()
{
}

function fg_EnterCell_Prg()
{
}

function fg_MouseUp_Prg()
{
}

function fg_MouseDown_Prg()
{
}

function fg_BeforeEdit_Prg()
{
}

// FINAL DE EVENTOS DO GRID FG **************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailinf.js

Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelas funcoes submitChanges_DSC() e submitChangesParallel_DSC()
do js_detailinf.js

Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
;
}
function openModalPatrocinios()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 770;
    var nHeight = 470;
	var strBtns = currentBtnsCtrlBarString('inf');
    var aStrBtns = strBtns.split('');
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('I');
    strPars += '&nEmpresaID=' + escape(iempresa[0]);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modmarketing/subcampanhas/acoesmarketing/modalpages/modalpatrocinios.asp' + strPars;
    
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

function openModalResponsavel()
{
    var nRegistroID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'AcaPatrocinioID'));
    var nMarcaID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'MarcaID'));
    var strPars = '?nRegistroID=' + escape(nRegistroID.toString());

    if (nMarcaID = 3012)
    {
        htmlPath = SYS_PAGESURLROOT + '/modmarketing/subcampanhas/acoesmarketing/modalpages/modalresponsavel.asp' + strPars;
        showModalWin(htmlPath, new Array(571,284));
    }
    else
    {
        window.top.overflyGen.Alert('Somente na marca Alcat�ia � permitido que o respons�vel seja alterado.');
        return null;
    }
}

function refreshAfterModalClose()
{
    if ( __winTimerHandler != null )
    {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }
    
    __btn_REFR('inf');
}
// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************
