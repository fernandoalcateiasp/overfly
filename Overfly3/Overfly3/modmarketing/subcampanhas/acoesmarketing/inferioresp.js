/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.AcaoID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM AcoesMarketing a WITH(NOLOCK) WHERE '+
        sFiltro + 'a.AcaoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Patrocinio
    else if (folderID == 23201)
    {
        dso.SQL = 'SELECT a.* ' + 
                  'FROM AcoesMarketing_Patrocinios a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.AcaoID = '+ idToFind + ' ' +
                  'ORDER BY a.AcaoID';
        return 'dso01Grid_DSC';
    }
    // Financeiros
    else if (folderID == 23202)
    {
        dso.SQL = 'SELECT c.dtVencimento, c.dtEmissao, c.FinanceiroID, d.ItemMasculino AS TipoFinanceiro, ' +
			'f.HistoricoPadrao AS HistoricoPadrao, g.Conceito AS Marca, e.Fantasia AS Pessoa, b.Valor, b.Observacao ' +
                  'FROM Financeiro_AcoesMarketingPatrocinios b WITH(NOLOCK) ' +  
							'INNER JOIN Financeiro c WITH(NOLOCK) ON b.FinanceiroID=c.FinanceiroID ' + 
							'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON c.TipoFinanceiroID=d.ItemID ' +
							'INNER JOIN Pessoas e WITH(NOLOCK) ON c.PessoaID=e.PessoaID ' + 
							'INNER JOIN HistoricosPadrao f WITH(NOLOCK) ON c.HistoricoPadraoID=f.HistoricoPadraoID ' + 
							'LEFT JOIN Conceitos g WITH(NOLOCK) ON b.MarcaID=g.ConceitoID ' +
                  'WHERE ' + sFiltro + ' b.AcaoID = '+ idToFind + ' ' +
                  'ORDER BY b.AcaoID';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
	var aEmpresaID = getCurrEmpresaData();
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Patrocinio
    else if (pastaID == 23201)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT a.ConceitoID AS fldID, a.Conceito AS fldName ' +
				'FROM Conceitos a WITH(NOLOCK) ' +
					'INNER JOIN Conceitos b WITH(NOLOCK) ON a.ConceitoID = b.MarcaID ' +
					'INNER JOIN RelacoesPesCon c WITH(NOLOCK) ON b.ConceitoID = c.ObjetoID ' +
				'WHERE a.EstadoID=2 AND a.TipoConceitoID=304 AND c.EstadoID NOT IN (1,4,5) AND ' + 
				'c.TipoRelacaoID = 61 ' +
				'ORDER BY fldName';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT DISTINCT a.AcaPatrocinioID, b.Fantasia AS Responsavel ' +
				'FROM AcoesMarketing_Patrocinios a WITH(NOLOCK) ' +
				    'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ResponsavelID) ' +
				'WHERE a.AcaoID=' + nRegistroID + ' ' +
				'ORDER BY a.AcaPatrocinioID';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT a.AcaPatrocinioID, ' +
				'dbo.fn_AcaoMarketingPatrocinio_Datas(a.AcaPatrocinioID, 1) AS dtVencimento, ' +
				'dbo.fn_AcaoMarketingPatrocinio_Totais(a.AcaPatrocinioID, 11, GETDATE()) AS Atraso,' +
				'dbo.fn_AcaoMarketingPatrocinio_Totais(a.AcaPatrocinioID, 2, GETDATE()) AS Participacao,' +
				'dbo.fn_AcaoMarketingPatrocinio_Totais(a.AcaPatrocinioID, 3, GETDATE()) AS TotalPago,' +
				'dbo.fn_AcaoMarketingPatrocinio_Totais(a.AcaPatrocinioID, 4, GETDATE()) AS TotalRecebido,' +
				'dbo.fn_AcaoMarketingPatrocinio_Totais(a.AcaPatrocinioID, 6, GETDATE()) AS SaldoPatrocinio,' +
				'dbo.fn_AcaoMarketingPatrocinio_Totais(a.AcaPatrocinioID, 7, GETDATE()) AS SaldoEfetivo ' +
				'FROM AcoesMarketing_Patrocinios a WITH(NOLOCK) ' +
				'WHERE a.AcaoID=' + nRegistroID + ' ' +
				'ORDER BY a.AcaPatrocinioID';
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(23201);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 23201); //Patrocinio

    vPastaName = window.top.getPastaName(23202);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 23202); //Financeiros

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 23201)
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,3,7], ['AcaoID', registroID]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    var i=0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);
    }
    // Patrocinio
    else if (folderID == 23201)
    {
        headerGrid(fg,['Marca',
					   'Inic',
					   'Respons�vel',
					   'Prazo',
                       'Vencimento',
					   'Atraso',
					   '% Partic',
					   'Valor Patroc�nio',
                       'Total Pago',
                       'Total Recebido',
                       'Saldo Patroc�nio',                       
                       'Saldo Efetivo',
                       'Observa��o',
                       'AcaPatrocinioID'], [13]);

        glb_aCelHint = [[0,1,'A marca teve a iniciativa da a��o?']];

        fillGridMask(fg,currDSO,['MarcaID',
								 'Iniciativa',
								 '^AcaPatrocinioID^dso01GridLkp^AcaPatrocinioID^Responsavel*',
								 'Prazo',
								 '^AcaPatrocinioID^dso02GridLkp^AcaPatrocinioID^dtVencimento*',
								 '^AcaPatrocinioID^dso02GridLkp^AcaPatrocinioID^Atraso*',
								 '^AcaPatrocinioID^dso02GridLkp^AcaPatrocinioID^Participacao*',
								 'ValorPatrocinio',
								 '^AcaPatrocinioID^dso02GridLkp^AcaPatrocinioID^TotalPago*',
								 '^AcaPatrocinioID^dso02GridLkp^AcaPatrocinioID^TotalRecebido*',
								 '^AcaPatrocinioID^dso02GridLkp^AcaPatrocinioID^SaldoPatrocinio*',
								 '^AcaPatrocinioID^dso02GridLkp^AcaPatrocinioID^SaldoEfetivo*',								 
								 'Observacao',
								 'AcaPatrocinioID'],
								 ['','','','999','99/99/9999','999','999.99999','999999999.99','999999999.99','999999999.99','999999999.99','999999999.99','',''],
								 ['','','','###',dTFormat,'###','##0.00000','###,###,##0.00','###,###,##0.00','###,###,##0.00','###,###,##0.00','###,###,##0.00','','']);


	alignColsInGrid(fg,[3,4,5,6,7,8,9,11]);
		
		fg.FrozenCols = 1 ;
    }
    // Financeiro
    else if (folderID == 23202)
    {
        headerGrid(fg,['Vencimento',
					   'Emiss�o',
					   'Financeiro',
                       'Tipo',
                       'Hist�rico Padr�o',
                       'Marca',
                       'Pessoa',
                       'Valor',
                       'Observa��o'], []);

        fillGridMask(fg,currDSO,['dtVencimento',
								 'dtEmissao',
								 'FinanceiroID',
								 'TipoFinanceiro',
								 'HistoricoPadrao',
								 'Marca',
								 'Pessoa',
								 'Valor',
								 'Observacao'],
								 ['99/99/9999','99/99/9999','','','','','','999999999.99'  ,''],
								 [dTFormat    ,dTFormat    ,'','','','','','###,###,##0.00','']);
    }
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************