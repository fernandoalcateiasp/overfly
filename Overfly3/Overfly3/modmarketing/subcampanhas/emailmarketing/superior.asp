<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="emailmarketingsup01Html" name="emailmarketingsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/emailmarketing/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/emailmarketing/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="emailmarketingsup01Body" name="emailmarketingsup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->

        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p  id="lblProcessoIniciado" name="lblProcessoIniciado" class="lblGeneral">PI</p>
        <input type="checkbox" id="chkProcessoIniciado" name="chkProcessoIniciado" DATASRC="#dsoSup01" DATAFLD="ProcessoIniciado" class="fldGeneral" title="Processo j� foi iniciado?"></input>
        <p id="lblAssunto" name="lblAssunto" class="lblGeneral">Assunto</p>
        <input type="text" id="txtAssunto" name="txtAssunto" DATASRC="#dsoSup01" DATAFLD="Assunto" class="fldGeneral">
        <p id="lbldtEnvio" name="lbldtEnvio" class="lblGeneral">Data Envio</p>
		<input type="text" id="txtdtEnvio" name="txtdtEnvio" DATASRC="#dsoSup01" DATAFLD="V_dtEnvio" class="fldGeneral"></input>
        <p id="lbldtValidade" name="lbldtValidade" class="lblGeneral">Data Validade</p>
		<input type="text" id="txtdtValidade" name="txtdtValidade" DATASRC="#dsoSup01" DATAFLD="V_dtValidade" class="fldGeneral"></input>
		<p id="lblVitrineID" name="lblVitrineID" class="lblGeneral">Vitrine</p>
		<select id="selVitrineID" name="selVitrineID" DATASRC="#dsoSup01" DATAFLD="VitrineID" class="fldGeneral"></select>
		<p id="lblTemplateID" name="lblTemplateID" class="lblGeneral">Template</p>
		<select id="selTemplateID" name="selTemplateID" DATASRC="#dsoSup01" DATAFLD="TemplateID" class="fldGeneral"></select>
		<p id="lblAtributo" name="lblAtributo" class="lblGeneral">Atributo</p>
		<select id="selAtributoID" name="selAtributoID" DATASRC="#dsoSup01" DATAFLD="AtributoID" class="fldGeneral" title="Percentual de acr�scimo na margem m�nima (com base na diferen�a MP menos MM)"></select>
		<p id="lblPercentualPreco" name="lblPercentualPreco" class="lblGeneral">Percentual</p>
		<select id="selPercentualPreco" name="selPercentualPreco" DATASRC="#dsoSup01" DATAFLD="PercentualPreco" class="fldGeneral" title="Percentual de acr�scimo na margem m�nima (com base na diferen�a MP menos MM)">
		    <option value = "-50" >-50%</option>
		    <option value = "-25">-25%</option>
		    <option value = "0">0%</option>
		    <option value = "25">25%</option>
		    <option value = "50">50%</option>
		    <option value = "75">75%</option>
		    <option value = "100">100%</option>		    
		</select>
		
		<p id="lblPercentualProdutosCompativeis" name="lblPercentualProdutosCompativeis" class="lblGeneral">% Vit</p>
        <input type="text" id="txtPercentualProdutosCompativeis" name="txtPercentualProdutosCompativeis" DATASRC="#dsoSup01" DATAFLD="PercentualProdutosCompativeis" class="fldGeneral" title="Percentual m�nimo de produtos compat�veis">
        
        <p id="lblTamanhoArgumentoVenda" name="lblTamanhoArgumentoVenda" class="lblGeneral">Arg</p>
        <input type="text" id="txtTamanhoArgumentoVenda" name="txtTamanhoArgumentoVenda" DATASRC="#dsoSup01" DATAFLD="TamanhoArgumentoVenda" class="fldGeneral" title="Tamanho m�ximo dos argumentos de venda">		
        
        <p id="lblProdutos" name="lblProdutos" class="lblGeneral">Produtos</p>
        <input type="text" id="txtProdutos" name="txtProdutos" class="fldGeneral">
        <p id="lblClientes" name="lblClientes" class="lblGeneral">Clientes</p>
        <input type="text" id="txtClientes" name="txtClientes" class="fldGeneral">
        <p id="lblContatos" name="lblContatos" class="lblGeneral">Contatos</p>
        <input type="text" id="txtContatos" name="txtContatos" class="fldGeneral">
        <p id="lblEmails" name="lblEmails" class="lblGeneral">Emails</p>
        <input type="text" id="txtEmails" name="txtEmails" class="fldGeneral">
        <p id="lblPercentualProcessamento" name="lblPercentualProcessamento" class="lblGeneral">% Perc</p>
        <input type="text" id="txtPercentualProcessamento" name="txtPercentualProcessamento" class="fldGeneral">
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
		<input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
    </div>
    
</body>

</html>
