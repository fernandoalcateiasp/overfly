
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalemailsmarketingHtml" name="modalemailsmarketingHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/emailmarketing/modalpages/modalemailsmarketing.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf

Dim i, nEmailID, nEstadoID, nEmpresaID, nDireito, nVitrineID, sVitrine

nEmailID = 0
nEstadoID = 0
nEmpresaID = 0
nDireito = 0
nVitrineID = 0
sVitrine = ""

For i = 1 To Request.QueryString("nEmailID").Count    
    nEmailID = Request.QueryString("nEmailID")(i)
Next

For i = 1 To Request.QueryString("nEstadoID").Count    
    nEstadoID = Request.QueryString("nEstadoID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nDireito").Count    
    nDireito = Request.QueryString("nDireito")(i)
Next

For i = 1 To Request.QueryString("nVitrineID").Count    
    nVitrineID = Request.QueryString("nVitrineID")(i)
Next

For i = 1 To Request.QueryString("sVitrine").Count    
    sVitrine = Request.QueryString("sVitrine")(i)
Next

Response.Write "var glb_nEmailID = " & CStr(nEmailID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEstadoID = " & CStr(nEstadoID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nDireito = " & CStr(nDireito) & ";"
Response.Write vbcrlf

Response.Write "var glb_nVitrineID = " & CStr(nVitrineID) & ";"
Response.Write vbcrlf

Response.Write "var glb_sVitrine = '" & CStr(sVitrine) & "';"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//js_modalemailsmarketing_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
// js_modalemailsmarketing_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_modalemailsmarketing_DblClick();
//-->
</SCRIPT>

</head>

<body id="modalemailsmarketingBody" name="modalemailsmarketingBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divFields" name="divFields" class="divGeneral">
        <p id="lblCanais" name="lblCanais" class="lblGeneral">Canais</p>
        <select id="selCanais" name="selCanais" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " & _
		 "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=13 and Filtro Like '%{1221}%' AND Filtro Like '%<CLI>%') " & _
		 "ORDER BY Ordem"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	
	'If ( (CLng(rsData.Fields("fldID").Value) = 59) OR _
	'	 (CLng(rsData.Fields("fldID").Value) = 60) OR _
	'	 (CLng(rsData.Fields("fldID").Value) = 61) ) Then
	'	Response.Write " SELECTED "
	'End If

	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblEstados" name="lblEstados" class="lblGeneral">Est</p>
        <select id="selEstados" name="selEstados" class="fldGeneral" MULTIPLE>
<%
Dim rsData, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT b.RecursoID AS fldID, b.RecursoAbreviado AS fldName " & _
		 "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK) WHERE(a.EstadoID=2 and a.TipoRelacaoID=3 AND a.ObjetoID=301 AND " & _
			"a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.RecursoID NOT IN (1,5)) " & _
		 "ORDER BY a.Ordem"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	
	If (CLng(rsData.Fields("fldID").Value) = 2) Then
		Response.Write " SELECTED "
	End If
	
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblClassificacao" name="lblClassificacao" class="lblGeneral">Class</p>
        <select id="selClassificacao" name="selClassificacao" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " & _
		 "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=29) " & _
		 "ORDER BY Ordem"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
    
    If ((CDbl(rsData.Fields("fldID").Value) = 145) OR _
        (CDbl(rsData.Fields("fldID").Value) = 146)) Then
        Response.Write " SELECTED"
    End If
    
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
		<input type="button" id="btnOK" name="btnOK" value="Excluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnSincronizar" name="btnSincronizar" value="Sincronizar" title="Sincronizar os clientes" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnVisualizar" name="btnVisualizar" value="Visualizar" title="Visualizar Email" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnTeste" name="btnTeste" value="Teste" title="Enviar e-mail de teste para voc�" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    </div>

    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" class="fldGeneral" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
