/********************************************************************
modalemailsmarketing.js

Library javascript para o modalemailsmarketing.asp
********************************************************************/

/* INDICE DE FUNCOES ************************************************

window_onload()
btn_onclick(ctl)
setupPage()
loadDataAndTreatInterface()
cmbs_ondblclick()
buildStrParsEmails(nTipoResultadoID)
sendMails_DSC()

*********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_modEmailsProm_Timer = null;
var glb_nOK = 0;

//  Dsos genericos para banco de dados 
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoGrid = new CDatatransport("dsoGrid");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalemailsmarketingBody)
    {
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    configuraGrid();

    fillGridData(0,1);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    if (ctl.id == btnOK.id )
    {
        ;
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null );
    else if (ctl.id == btnSincronizar.id)
        sincronizaClientes();
    else if (ctl.id == btnVisualizar.id)
        visualizarEmail();
	else if (ctl.id == btnTeste.id)
		emailTeste();
}

/********************************************************************
Vai ao banco buscar a lista de e-mails que sera inserida no outlook
********************************************************************/
function openEMails()
{
    var strPars = buildStrParsEmails(2);
    var nUserID = getCurrUserID();
    
    if (strPars == '')
		return null;			

    strPars += (strPars == '' ? '?' : '&');
    strPars += 'nUsuarioID=' + escape(nUserID) + 
        '&nTipoResultadoID=' + escape(2) +
        '&nVitrineID=' + escape(glb_nVitrineID) +
        '&bEspecifico=' + escape(0);
    
    lockControlsInModalWin(true);

    dsoGen01.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/sendemailsmarketing.asp' + strPars;
    dsoGen01.ondatasetcomplete = openEMails_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Insere a lista de e-mails no outlook
********************************************************************/
function openEMails_DSC()
{
	var sMails = '';
	var nCounter = 0;
	while (!dsoGen01.recordset.EOF)
	{
		sMails += dsoGen01.recordset['Resultado'].value;
		dsoGen01.recordset.MoveNext();
		
		if (!dsoGen01.recordset.EOF)
			sMails += ';';
		nCounter++;
	}
	
	lockControlsInModalWin(false);
	if (nCounter == 0)
	{
		if ( window.top.overflyGen.Alert('Nenhum e-mail selecionado.') == 0 )
			return null;

		return null;
	}

	window.location = 'mailto:?body=' + escape('Use Ctrl-V para colar emails selecionados!');
	window.clipboardData.setData('Text', sMails);

    if ( window.top.overflyGen.Alert (nCounter + ' e-mails selecionados.') == 0 )
		return null;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Email Marketing - ' + glb_sVitrine , 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;
    var nCmbsHeight = 75;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

	// ajusta elementos no divFields
    adjustElementsInForm([['lblCanais','selCanais',16,1,-10,-10],
						  ['lblEstados','selEstados',3,1],
						  ['lblClassificacao','selClassificacao',4,1]],null,null,true);

    selEstados.style.height = nCmbsHeight;
    selCanais.style.height = nCmbsHeight;
    selClassificacao.style.height = nCmbsHeight;
	
    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);
        // altura e fixa para tres linhas de campos
        height = parseInt(selClassificacao.currentStyle.top, 10) + 
				 parseInt(selClassificacao.currentStyle.height, 10) + ELEM_GAP;
        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = modHeight - parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Editable = false;
    
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

	btnSincronizar.style.top = selClassificacao.offsetTop;
    btnSincronizar.style.left = parseInt(selClassificacao.currentStyle.left, 10) +
		selClassificacao.offsetWidth + ELEM_GAP - 4;
	btnSincronizar.style.width = btnOK.offsetWidth;
    
    if ((glb_nEstadoID == 1) || (glb_nEstadoID == 24))
        btnSincronizar.disabled = false;
    else        
        btnSincronizar.disabled = true;

	btnVisualizar.style.top = btnSincronizar.offsetTop;
    btnVisualizar.style.left = parseInt(btnSincronizar.currentStyle.left, 10) +
		btnSincronizar.offsetWidth + ELEM_GAP - 4;
	btnVisualizar.style.width = btnOK.offsetWidth;

	btnTeste.style.top = btnVisualizar.offsetTop;
    btnTeste.style.left = parseInt(btnVisualizar.currentStyle.left, 10) +
		btnVisualizar.offsetWidth + (ELEM_GAP / 3);
	btnTeste.style.width = btnOK.offsetWidth;

    btnOK.style.top = 0;
    btnOK.style.left = 0;
		
	btnCanc.style.top = 0;
    btnCanc.style.left = 0;
    btnCanc.style.width = 0;
    btnCanc.style.height = 0;
    btnCanc.style.visibility = 'hidden';
}

/********************************************************************
Evento de dblclick dos combos que atuam como listbox.
	Dblclick no combo de paises preenche o combo de UFs.
	Dblclick no combo de UFS preenche o combo de Cidades.
********************************************************************/
function cmbs_ondblclick()
{

}

/********************************************************************
Constroi a string de parametros que eh enviada para a pagina 
/serversidegen/emails/sendemailsmarketing.asp
	nTipoResultadoID: == 2 -> Somente listagem de e-mails
	nTipoResultadoID: == 3 -> Testar o email
	nTipoResultadoID: == 4 -> Enviar os emails
********************************************************************/
function buildStrParsEmails(nTipoResultadoID)
{
	var aCmbs = [[selEstados, 'sEstados'],
		[selCanais,'sCanais'],
		[selClassificacao,'sClassificacoes']];

	var i, j;
	var nItensSelected=0;
	var sStrPars='';
	var sSelecteds='';
	var nUserID = getCurrUserID();
	var sError;
	var nEstadosSelected = 0;
	
    // Percorre o array de combos
    for (i=0; i<aCmbs.length; i++)
    {
		for (j=0; j<(aCmbs[i][0]).length; j++)
		{
		    // Verifica se o item esta selecionado
		    if ( (aCmbs[i][0]).options[j].selected == true )
		    {
				if (i==0)
					nEstadosSelected++;
					
                if (aCmbs[i][0] == selClassificacao)
		            sSelecteds += (sSelecteds == '' ? '/' : '') + (aCmbs[i][0]).options[j].innerText + '/';
                else
		            sSelecteds += (sSelecteds == '' ? '/' : '') + (aCmbs[i][0]).options[j].value + '/';
                
		        nItensSelected++;
		    }
		}
		
		if (sSelecteds != '')
		{
			sStrPars += (sStrPars == '' ? '?' : '&');
			sStrPars += aCmbs[i][1] + '=' + escape(sSelecteds);
		}

		sSelecteds = '';
    }


	if (nTipoResultadoID != 2)
	{
	
		if (nEstadosSelected == 0)
		{
			if ( window.top.overflyGen.Alert ('Selecione pelo menos um estado.') == 0 )
				return null;
			
			return '';				
		}
	}

	sStrPars += (sStrPars == '' ? '?' : '&');
	
	return sStrPars + 'nEmpresaID=' + escape(glb_nEmpresaID);
}

/********************************************************************
Envia os e-mails ou e-mail de teste.
********************************************************************/
function emailTeste()
{
	if ( glb_modEmailsProm_Timer != null )    
	{
		window.clearInterval(glb_modEmailsProm_Timer);
		
		glb_modEmailsProm_Timer = null;
	}

    if (fg.Row <= 0)
        return null;

    var strPars = '';
    var nCanalID = getCellValueByColKey(fg, 'CanalID', fg.Row);
    var nUsuarioID = getCurrUserID();
    
    strPars += '?nEmailID=' + escape(glb_nEmailID);
    strPars += '&nCanalID=' + escape(nCanalID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
    
    lockControlsInModalWin(true);

    dsoGen01.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/emailmarketing/serverside/emailteste.aspx' + strPars;
    dsoGen01.ondatasetcomplete = emailTeste_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Retorno do envio de e-mail
********************************************************************/
function emailTeste_DSC()
{
    lockControlsInModalWin(false);

    if ( !(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF) )
    {
        dsoGen01.recordset.MoveFirst();
        glb_modEmailsProm_Timer = window.setInterval('sendEmail(' + dsoGen01.recordset['EmailID'].value + ')', 100, 'JavaScript');
    }
    else
    {
	    if ( window.top.overflyGen.Alert ('Erro ao enviar o e-mail.') == 0 )
		    return null;
    }
}

/********************************************************************
Informa que o e-mail deve ser enviado
********************************************************************/
function sendEmail(nEmailID)
{
	if ( glb_modEmailsProm_Timer != null )    
	{
		window.clearInterval(glb_modEmailsProm_Timer);
		
		glb_modEmailsProm_Timer = null;
	}

    if (nEmailID == 0)
    {
	    if ( window.top.overflyGen.Alert ('Erro ao enviar o e-mail.') == 0 )
		    return null;

	    return null;
    }
    else
    {       
        lockControlsInModalWin(true);

        var strPars = '?nMailtoSendID=' + escape(nEmailID);
        
        strPars += '&bHTML=1';

        dsoGen01.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/sendemail.aspx' + strPars;
        dsoGen01.ondatasetcomplete = sendEmail_DSC;
        dsoGen01.Refresh();
    }
}

function sendEmail_DSC()
{
    lockControlsInModalWin(false);

	if ( window.top.overflyGen.Alert ( dsoGen01.recordset['Msg'].value ) == 0 )
		return null;
}

/********************************************************************
Sincorniza os clientes
********************************************************************/
function sincronizaClientes()
{
	if ( glb_modEmailsProm_Timer != null )    
	{
		window.clearInterval(glb_modEmailsProm_Timer);
		
		glb_modEmailsProm_Timer = null;
	}

    var strPars = buildStrParsEmails(3);
    var nUserID = getCurrUserID();
    
    if (strPars == '')
		return null;			
    
    strPars += '&nEmailID=' + escape(glb_nEmailID);
    strPars += '&nTipoResultadoID=' + escape(1);
    
    lockControlsInModalWin(true);

    dsoGen01.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/emailmarketing/serverside/sincronizaclientes.aspx' + strPars;
    dsoGen01.ondatasetcomplete = sincronizaClientes_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Retorno da sincroniza��o dos clientes
********************************************************************/
function sincronizaClientes_DSC()
{
    lockControlsInModalWin(false);

    glb_modEmailsProm_Timer = window.setInterval('fillGridData(0,1)', 100, 'JavaScript');
}

function configuraGrid()
{
    startGridInterface(fg);
    
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    
    with (fg)
    {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '8';
   		Editable = false;
		AllowUserResizing = 1;
		VirtualData = true;
		Ellipsis = 1;
		SelectionMode = 1;
		AllowSelection = false;
		Rows = 1;
		Cols = 7;
		FixedRows = 1;
		FixedCols = 1;
		ScrollBars = 3;
		OutLineBar = 5;
		GridLines = 0;
		FormatString = 'Registro' + '\t' + 'Tipo' + '\t' + 'Nivel' + '\t' + 'CanalID' + '\t' + 'RegistroID' + '\t' + 'Quantidade' + '\t' + 'EmaClienteID';
		OutLineBar = 1;
        GridLinesFixed = 13;
		GridLines = 1;
		GridColor = 0X000000;		
		ColKey(0) = 'Registro';
		ColKey(1) = 'Tipo';
		ColKey(2) = 'Nivel';
		ColKey(3) = 'CanalID';
		ColKey(4) = 'RegistroID';
		ColKey(5) = 'Quantidade';
		ColKey(6) = 'EmaClienteID';
		ColHidden(1) = true;
		ColHidden(2) = true;		
		ColHidden(3) = true;		
		ColHidden(4) = true;		
		ColHidden(6) = true;		
    }
    
    fg.Redraw = 2;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(nCanalID, nTipoResultado)
{
	if ( glb_modEmailsProm_Timer != null )    
	{
		window.clearInterval(glb_modEmailsProm_Timer);
		
		glb_modEmailsProm_Timer = null;
	}

	lockControlsInModalWin(true);
	
	var strPars = '';
	    
	strPars = '?nEmailID=' + escape(glb_nEmailID);
	strPars += '&nCanalID=' + escape(nCanalID);
	strPars += '&nTipoResultado=' + escape(nTipoResultado);

	dsoGrid.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/emailmarketing/serverside/listartreeview.aspx' + strPars;
	dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    var dTFormat = '';
    var i, j;
    var nNivel = 0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Rows = 1;
    
    if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
        dsoGrid.recordset.MoveFirst();
    
	if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
	{
		glb_GridIsBuilding = true;
		while (!dsoGrid.recordset.EOF)
		{
			fg.Row = fg.Rows - 1;
			
		    fg.AddItem('', fg.Row+1);
			
		    if (fg.Row < (fg.Rows-1))
			    fg.Row++;
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Registro')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Registro')) = (dsoGrid.recordset['Registro'].value == null ? '' : dsoGrid.recordset['Registro'].value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Tipo')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Tipo')) = (dsoGrid.recordset['Tipo'].value == null ? '' : dsoGrid.recordset['Tipo'].value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) = (dsoGrid.recordset['Nivel'].value == null ? '' : dsoGrid.recordset['Nivel'].value);	
			
		    fg.ColDataType(getColIndexByColKey(fg, 'CanalID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CanalID')) = (dsoGrid.recordset['CanalID'].value == null ? '' : dsoGrid.recordset['CanalID'].value);	
			
		    fg.ColDataType(getColIndexByColKey(fg, 'RegistroID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RegistroID')) = (dsoGrid.recordset['RegistroID'].value == null ? '' : dsoGrid.recordset['RegistroID'].value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Quantidade')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Quantidade')) = (dsoGrid.recordset['Quantidade'].value == null ? '' : dsoGrid.recordset['Quantidade'].value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'EmaClienteID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EmaClienteID')) = (dsoGrid.recordset['EmaClienteID'].value == null ? '' : dsoGrid.recordset['EmaClienteID'].value);	

		    fg.IsSubTotal(fg.Row) = true;
		    fg.RowOutlineLevel(fg.Row) = dsoGrid.recordset['Nivel'].value;
            
            dsoGrid.recordset.MoveNext();
		}
	}

	glb_GridIsBuilding = false;
    
    alignColsInGrid(fg,[1,2,3,4,5,6]);    
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    for (i=0; i<fg.Rows; i++ )
    {
        nNivel = getCellValueByColKey(fg, 'Nivel', i);

        if ( (fg.IsSubTotal(i) == true) && (nNivel > 0) )
            fg.IsCollapsed(i) = 2;
    }

    fg.Redraw = 2;
    
    lockControlsInModalWin(false);
	    
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }            
}

function visualizarEmail()
{
    var nCanalID = 0;
    var nTipo = 0;
    var nEmaClienteID = 0;
    var strPars = '';

    if (fg.Row <= 0)
        return null;

    nCanalID = getCellValueByColKey(fg, 'CanalID', fg.Row);
    nTipo = getCellValueByColKey(fg, 'Tipo', fg.Row);

    if ((nTipo == 6) || (nTipo == 7))
    {
        if ((glb_nEstadoID == 35) ||
            (glb_nEstadoID == 36) ||
            (glb_nEstadoID == 63))
            nEmaClienteID = getCellValueByColKey(fg, 'EmaClienteID', fg.Row);
        else
        {
		    if ( window.top.overflyGen.Alert('Email ainda n�o preparado.') == 0 )
			    return null;

		    return null;
        
        }            
    }

    strPars += '?nEmailID=' + escape(glb_nEmailID) + 
        '&nCanalID=' + escape(nCanalID) +
        '&nEmaClienteID=' + escape(nEmaClienteID);

    window.open(SYS_PAGESURLROOT + '/modmarketing/subcampanhas/emailmarketing/serverside/template.aspx' + strPars);
}
function js_modalemailsmarketing_DblClick()
{
    visualizarEmail();
}