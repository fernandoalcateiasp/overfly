/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de emailmarketing
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=EmailMarketing.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'CONVERT(VARCHAR, dtEnvio, '+DATE_SQL_PARAM+') AS V_dtEnvio, ' +
               'CONVERT(VARCHAR, dtValidade, '+DATE_SQL_PARAM+') AS V_dtValidade, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 1) AS Produtos, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 2) AS Clientes, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 3) AS Contatos, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 4) AS Emails, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 6) AS PercentualProcessamento ' +
               'FROM EmailMarketing WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY EmailID DESC';
    else
        sSQL = 'SELECT *, ' + 
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=EmailMarketing.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               '(SELECT CONVERT(VARCHAR, a.dtEnvio, ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, a.dtEnvio, 108) ' +
					'FROM EmailMarketing a WITH(NOLOCK) ' +
					'WHERE (a.EmailID=EmailMarketing.EmailID)) AS V_dtEnvio, ' +
               'CONVERT(VARCHAR, dtValidade, '+DATE_SQL_PARAM+') AS V_dtValidade, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 1) AS Produtos, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 2) AS Clientes, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 3) AS Contatos, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 4) AS Emails, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 6) AS PercentualProcessamento ' +
               'FROM EmailMarketing WITH(NOLOCK) WHERE EmailID = ' + nID + ' ORDER BY EmailID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
          '0 AS Prop1, 0 AS Prop2, ' +
               'CONVERT(VARCHAR, dtEnvio, '+DATE_SQL_PARAM+') AS V_dtEnvio, ' +
               'CONVERT(VARCHAR, dtValidade, '+DATE_SQL_PARAM+') AS V_dtValidade, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 1) AS Produtos, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 2) AS Clientes, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 3) AS Contatos, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 4) AS Emails, ' +
               'dbo.fn_EmailMarketing_Totais(EmailID, NULL, 6) AS PercentualProcessamento ' +
          'FROM EmailMarketing WITH(NOLOCK) WHERE EmailID = 0';
    
    dso.SQL = sql;          
}              
