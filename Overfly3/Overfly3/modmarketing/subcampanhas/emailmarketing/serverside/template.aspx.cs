using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.emailmarketing.serverside
{
	public partial class template : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			string ini = Content;
		}

		protected Integer emailID = Constants.INT_ZERO;
		protected Integer nEmailID
		{
			get { return emailID; }
			set { if (value != null) emailID = value; }
		}

		protected Integer canalID = Constants.INT_ZERO;
		protected Integer nCanalID
		{
			get { return canalID; }
			set { if (value != null) canalID = value; }
		}

		protected Integer emaClienteID = Constants.INT_ZERO;
		protected Integer nEmaClienteID
		{
			get { return emaClienteID; }
			set { if (value != null) emaClienteID = value; }
		}

        protected Integer ChamadaModalCRM = Constants.INT_ZERO;
        protected Integer bChamadaModalCRM
        {
            get { return ChamadaModalCRM; }
            set { if (value != null) ChamadaModalCRM = value; }
        }

		private string content = "";
		protected string Content
		{
			get
			{
				if (content == "" && EmailMarketingClientes.Tables[1].Rows.Count > 0)
				{
					content = EmailMarketingClientes.Tables[1].Rows[0]["Body"].ToString();
				}

				return content;
			}
		}

		protected DataSet emailMarketingClientes = null;
		protected DataSet EmailMarketingClientes
		{
			get
			{
				string sql = "";

				if (emailMarketingClientes == null)
				{
                    sql += "SELECT TOP 1 REPLACE(CONVERT(VARCHAR(MAX),a.Body),'[[endif]]','[endif]') AS Body " +
						"FROM EmailMarketing_Clientes a WITH(NOLOCK) " +
						"WHERE (a.EmailID = " + nEmailID + 
						((nCanalID != null && nCanalID.intValue() > 0) ? (" AND a.CanalID = " + nCanalID) : ("")) +
                        ((Convert.ToInt32(ChamadaModalCRM) == 1) ? " " : " AND a.Body IS NOT NULL ");

					if (nEmaClienteID.intValue() > 0)
					{
                        if (Convert.ToInt32(ChamadaModalCRM) == 1)
                            sql = sql.Replace("CONVERT(VARCHAR(MAX),a.Body)", "CONVERT(VARCHAR(MAX),dbo.fn_Pessoa_EmailMarketing_Body(a.PessoaID, a.EmailID))");
	                    
                        sql += " AND a.EmaClienteID = " + nEmaClienteID;
					}

					sql += ") ORDER BY a.EmaClienteID ";

					emailMarketingClientes = DataInterfaceObj.getRemoteData(sql);
				}

				return emailMarketingClientes;
			}
		}
	}
}
