using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.emailmarketing.serverside
{
	public partial class listartreeview : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
            DataSet ds = DataInterfaceObj.getRemoteData(
                "EXEC sp_EmailMarketing_Dados " +
                    emailID.intValue() + ", " +
                    ((canalID != null && canalID.intValue() != 0) ? canalID.ToString() : " NULL ") + ", " +
                    tipoResultado.ToString()
            );

            WriteResultXML(ds);
        }

		protected Integer emailID = Constants.INT_ZERO;
		protected Integer nEmailID
		{
			get { return emailID; }
			set { if (value != null) emailID = value; }
		}

		protected Integer canalID = Constants.INT_ZERO;
		protected Integer nCanalID
		{
			get { return canalID; }
			set { if (value != null) canalID = value; }
		}

		protected Integer tipoResultado = Constants.INT_ZERO;
		protected Integer nTipoResultado
		{
			get { return tipoResultado; }
			set { if (value != null) tipoResultado = value; }
		}
	}
}
