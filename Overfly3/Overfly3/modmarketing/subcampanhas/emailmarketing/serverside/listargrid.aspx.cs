using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.emailmarketing.serverside
{
	public partial class listargrid : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT a.EmailID, a.CanalID AS CanalID, b.ItemMasculino AS Canal, " + 
					"dbo.fn_EmailMarketing_Totais(a.EmailID, a.CanalID, 2) AS Clientes, " + 
					"dbo.fn_EmailMarketing_Totais(a.EmailID, a.CanalID, 3) AS Contatos, " + 
					"c.Ordem, c.ProdutoID, " + 
					"e.Conceito AS Familia, f.Conceito AS Marca, d.Modelo AS Modelo, d.Descricao AS Descricao, " + 
					"dbo.fn_EmailMarketing_Totais(a.EmailID, NULL, 2) AS TotalClientes, " + 
					"dbo.fn_EmailMarketing_Totais(a.EmailID, NULL, 3) AS TotalContatos " + 
				 "FROM EmailMarketing_Clientes a WITH(NOLOCK) " + 
					"INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.CanalID) " + 
					"INNER JOIN EmailMarketing_Clientes_Produtos c WITH(NOLOCK) ON (c.EmaClienteID = a.EmaClienteID) " + 
					"INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) " + 
					"INNER JOIN Conceitos e WITH(NOLOCK) ON (e.ConceitoID = d.ProdutoID) " + 
					"INNER JOIN Conceitos f WITH(NOLOCK) ON (f.ConceitoID = d.MarcaID) " + 
				  "WHERE (a.EmailID=" + nEmailID + ") " +   
				  "GROUP BY a.EmailID, a.CanalID, b.ItemMasculino, c.Ordem, c.ProdutoID, " + 
						"e.Conceito, f.Conceito, d.Modelo, d.Descricao " + 
				  "ORDER BY a.CanalID, c.Ordem, c.ProdutoID "
			));
		}

		protected Integer emailID = Constants.INT_ZERO;
		protected Integer nEmailID
		{
			get { return emailID; }
			set { if (value != null) emailID = value; }
		}
	}
}
