using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.emailmarketing.serverside
{
	public partial class emailteste : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(
				"INSERT INTO EmailsTemp (RelatorioID, Data, UserID, MailFrom, MailTo, MailCc, MailBcc, Subject, MailBody) " + 
				"SELECT TOP 1 1, GETDATE(), " + nUsuarioID + ", " + 
					"dbo.fn_Pessoa_URL(b.EmpresaID, 124, NULL), " + 
					"dbo.fn_Pessoa_URL(" + nUsuarioID + ", 124, NULL), SPACE(0), SPACE(0), " + 
					"a.Assunto, a.Body " + 
				"FROM EmailMarketing_Clientes a WITH(NOLOCK) " + 
					"INNER JOIN EmailMarketing b WITH(NOLOCK) ON (b.EmailID = a.EmailID) " + 
				"WHERE (a.EmailID = " + nEmailID + " AND a.CanalID = " + nCanalID + " AND " + 
					"a.Body IS NOT NULL) " + 
				"ORDER BY a.EmaClienteID "
			);

			WriteResultXML(DataInterfaceObj.getRemoteData(
                "SELECT TOP 1 EmailID FROM EmailsTemp WITH(NOLOCK) WHERE (RelatorioID = 1 AND UserID = " +
					nUsuarioID +
				") ORDER BY EmailID DESC "
			));
		}

		protected Integer emailID = Constants.INT_ZERO;
		protected Integer nEmailID
		{
			get { return emailID; }
			set { if(value != null) emailID = value; }
		}

		protected Integer canalID = Constants.INT_ZERO;
		protected Integer nCanalID
		{
			get { return canalID; }
			set { if(value != null) canalID = value; }
		}

		protected Integer usuarioID = Constants.INT_ZERO;
		protected Integer nUsuarioID
		{
			get { return usuarioID; }
			set { if(value != null) usuarioID = value; }
		}
	}
}
