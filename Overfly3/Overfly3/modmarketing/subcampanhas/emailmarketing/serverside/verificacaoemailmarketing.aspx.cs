using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.emailmarketing.serverside
{
	public partial class verificacaoemailmarketing : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			ProcedureParameters[] param = new ProcedureParameters[] {
				new ProcedureParameters("@EmailID", SqlDbType.Int, emailID.intValue()),
				new ProcedureParameters("@EstadoDeID", SqlDbType.Int, estadoDeID.intValue()),
				new ProcedureParameters("@EstadoParaID", SqlDbType.Int, estadoParaID.intValue()),
				new ProcedureParameters("@Data", SqlDbType.DateTime, DBNull.Value),
				new ProcedureParameters("@UsuarioID", SqlDbType.Int, userID.intValue()),
				new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value,ParameterDirection.InputOutput, 8000)
			};

			DataInterfaceObj.execNonQueryProcedure(
				"sp_EmailMarketing_Verifica",
				param
			);

			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select " + 
				((param[5].Data != DBNull.Value) ? ("'" + param[5].Data + "'") : (object) "NULL") + 
				" as Resultado"));
		}

		protected Integer emailID = Constants.INT_ZERO;
		protected Integer nEmailID
		{
			get { return emailID; }
			set { if (value != null) emailID = value; }
		}

		protected Integer estadoDeID = Constants.INT_ZERO;
		protected Integer nEstadoDeID
		{
			get { return estadoDeID; }
			set { if (value != null) estadoDeID = value; }
		}

		protected Integer estadoParaID = Constants.INT_ZERO;
		protected Integer nEstadoParaID
		{
			get { return estadoParaID; }
			set { if (value != null) estadoParaID = value; }
		}

		protected Integer userID = Constants.INT_ZERO;
		protected Integer nUserID
		{
			get { return userID; }
			set { if (value != null) userID = value; }
		}
	}
}
