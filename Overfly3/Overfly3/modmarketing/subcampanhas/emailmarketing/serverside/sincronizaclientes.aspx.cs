using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.emailmarketing.serverside
{
	public partial class sincronizaclientes : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			ProcedureParameters[] param = new ProcedureParameters[] {
				new ProcedureParameters("@EmailID", SqlDbType.Int, emailID.intValue()),
				new ProcedureParameters("@Canais", SqlDbType.VarChar, canais != null ? (object) canais : DBNull.Value,100),
				new ProcedureParameters("@Estados", SqlDbType.VarChar, estados != null ? (object) estados : DBNull.Value, 100),
				new ProcedureParameters("@Classificacoes", SqlDbType.VarChar, classificacoes != null ? (object) classificacoes : DBNull.Value, 100),
				new ProcedureParameters("@TipoResultado", SqlDbType.Int, tipoResultadoID.intValue()),
			};

			DataInterfaceObj.execNonQueryProcedure(
				"sp_EmailMarketing_Sincroniza",
				param
			);

			WriteResultXML(DataInterfaceObj.getRemoteData("select 'Sincronizacao OK' as fldResponse"));
		}

		protected Integer emailID = Constants.INT_ZERO;
		protected Integer nEmailID
		{
			get { return emailID; }
			set { if (value != null) emailID = value; }
		}

		protected Integer empresaID = null;
		protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { if (value != null) empresaID = value; }
		}
	    
		protected string estados = null;
		protected string sEstados
		{
			get { return estados; }
			set { if (value != null) estados = value; }
		}

		protected string canais = null;
		protected string sCanais
		{
			get { return canais; }
			set { if (value != null) canais = value; }
		}

		protected string classificacoes = null;
		protected string sClassificacoes
		{
			get { return classificacoes; }
			set { if (value != null) classificacoes = value; }
		}

		protected Integer usuarioID = Constants.INT_ZERO;
		protected Integer nUsuarioID
		{
			get { return usuarioID; }
			set { if (value != null) usuarioID = value; }
		}

		protected Integer vitrineID = Constants.INT_ZERO;
		protected Integer nVitrineID
		{
			get { return vitrineID; }
			set { if (value != null) vitrineID = value; }
		}

		protected Integer tipoResultadoID = Constants.INT_ZERO;
		protected Integer nTipoResultadoID
		{
			get { return tipoResultadoID; }
			set { if (value != null) tipoResultadoID = value; }
		}
	}
}
