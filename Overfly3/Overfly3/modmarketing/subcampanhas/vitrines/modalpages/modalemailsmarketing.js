/********************************************************************
modalemailsmarketing.js

Library javascript para o modalemailsmarketing.asp
********************************************************************/

/* INDICE DE FUNCOES ************************************************

window_onload()
btn_onclick(ctl)
openEMails()
openEMails_DSC()
pesqProdutos()
setupPage()
loadDataAndTreatInterface()
cmbs_ondblclick()
fillCmbUFsCidades(elem)
fillCmbUFsCidades_DSC()
buildStrParsEmails(nTipoResultadoID)
sendMails(nTipoResultadoID)
sendMails_DSC()

*********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_modEmailsProm_Timer = null;
var glb_nOK = 0;

//  Dsos genericos para banco de dados 
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoFillCmbs = new CDatatransport("dsoFillCmbs");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    glb_modEmailsProm_Timer = window.setInterval('show_This_Modal()', 100, 'JavaScript');
}

/********************************************************************
Mostra a janela pela primeira vez
********************************************************************/
function show_This_Modal()
{
	if ( glb_modEmailsProm_Timer != null )    
	{
		window.clearInterval(glb_modEmailsProm_Timer);
		
		glb_modEmailsProm_Timer = null;
	}
	
    // ajusta o body do html
    with (modalemailsmarketingBody)
    {
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    if (ctl.id == btnOK.id )
    {
        ;
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null );
        
	else if (ctl.id == btnEnviar.id)
		sendMails(4);

	else if (ctl.id == btnTeste.id)
		sendMails(3);
	else if (ctl.id == btnEmails.id)
		openEMails();
}

/********************************************************************
Vai ao banco buscar a lista de e-mails que sera inserida no outlook
********************************************************************/
function openEMails()
{
    var strPars = buildStrParsEmails(2);
    var nUserID = getCurrUserID();
    
    if (strPars == '')
		return null;			

    strPars += (strPars == '' ? '?' : '&');
    strPars += 'nUsuarioID=' + escape(nUserID) + 
        '&nTipoResultadoID=' + escape(2) +
        '&nVitrineID=' + escape(selVitrine.value) +
        '&bEspecifico=' + escape(chkEMailEspecifico.checked ? 1 : 0);
    
    lockControlsInModalWin(true);

    dsoGen01.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/sendemailsmarketing.aspx' + strPars;
    dsoGen01.ondatasetcomplete = openEMails_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Insere a lista de e-mails no outlook
********************************************************************/
function openEMails_DSC() {
    var sMails = '';
	var nCounter = 0;
	while (!dsoGen01.recordset.EOF)
	{
		sMails += dsoGen01.recordset['Resultado'].value;
		dsoGen01.recordset.MoveNext();
		
		if (!dsoGen01.recordset.EOF)
			sMails += ';';
		nCounter++;
	}
	
	lockControlsInModalWin(false);
	if (nCounter == 0)
	{
		if ( window.top.overflyGen.Alert('Nenhum e-mail selecionado.') == 0 )
			return null;

		return null;
	}

	window.location = 'mailto:?body=' + escape('Use Ctrl-V para colar emails selecionados!');
	window.clipboardData.setData('Text', sMails);

    if ( window.top.overflyGen.Alert (nCounter + ' e-mails selecionados.') == 0 )
		return null;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('E-mails de marketing', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;
    var nCmbsHeight = 150;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

	// ajusta elementos no divFields
    adjustElementsInForm([['lblCanais','selCanais',16,1,-10,-10],
						  ['lblEstados','selEstados',3,1],
						  ['lblClassificacao','selClassificacao',4,1],
						  ['lblPaises','selPaises',6,1],
						  ['lblUFs','selUFs',6,1],
						  ['lblCidades','selCidades',18,1],
						  ['lblFamilias','selFamilias',19,1],
						  ['lblMarcas','selMarcas',13,1],
						  ['lblVitrine','selVitrine',16,2,-10],
						  ['lblAssuntoEmail','txtAssuntoEmail',47, 3, -10, 80],
						  ['lblValidade','txtValidade',10, 3],
						  ['lblEMailEspecifico','chkEMailEspecifico',3,3]],null,null,true);

    selEstados.style.height = nCmbsHeight;
    selCanais.style.height = nCmbsHeight;
    selClassificacao.style.height = nCmbsHeight;
    selPaises.style.height = nCmbsHeight;
    selUFs.style.height = nCmbsHeight;
    selCidades.style.height = nCmbsHeight;
    selFamilias.style.height = nCmbsHeight;
    selMarcas.style.height = nCmbsHeight;
	txtAssuntoEmail.maxLength = 100;
    selPaises.ondblclick = cmbs_ondblclick;
	selUFs.ondblclick = cmbs_ondblclick;
	txtValidade.maxLength = 10;
	chkEMailEspecifico.checked = true;
	selVitrine.onchange = selVitrine_onchange;
	
	selVitrine_onchange();

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);
        // altura e fixa para tres linhas de campos
        height = parseInt(txtValidade.currentStyle.top, 10) + 
				 parseInt(txtValidade.currentStyle.height, 10) + ELEM_GAP;
        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }
    
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    btnEnviar.disabled = true;
    
	btnEmails.style.top = txtValidade.offsetTop;
    btnEmails.style.left = parseInt(chkEMailEspecifico.currentStyle.left, 10) +
		chkEMailEspecifico.offsetWidth + ELEM_GAP - 4;
	btnEmails.style.width = btnOK.offsetWidth;
	
	btnTeste.style.top = btnEmails.offsetTop;
    btnTeste.style.left = parseInt(btnEmails.currentStyle.left, 10) +
		btnEmails.offsetWidth + (ELEM_GAP / 3);
	btnTeste.style.width = btnOK.offsetWidth;

	btnEnviar.style.top = btnTeste.offsetTop;
    btnEnviar.style.left = parseInt(btnTeste.currentStyle.left, 10) +
		btnTeste.offsetWidth + (ELEM_GAP / 3);
	btnEnviar.style.width = btnOK.offsetWidth;

    btnOK.style.top = 0;
    btnOK.style.left = 0;
		
	btnCanc.style.top = 0;
    btnCanc.style.left = 0;
    btnCanc.style.width = 0;
    btnCanc.style.height = 0;
    btnCanc.style.visibility = 'hidden';
}

function selVitrine_onchange()
{
	var sEmpresa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()[3]');;
	
	if (selVitrine.selectedIndex < 0)
	    txtAssuntoEmail.value = '';
    else
	    txtAssuntoEmail.value = sEmpresa + ' - Ofertas da loja ' + selVitrine.options[selVitrine.selectedIndex].innerText;
}

/********************************************************************
Evento de dblclick dos combos que atuam como listbox.
	Dblclick no combo de paises preenche o combo de UFs.
	Dblclick no combo de UFS preenche o combo de Cidades.
********************************************************************/
function cmbs_ondblclick()
{
	if (this.id == 'selPaises')
		clearComboEx(['selUFs','selCidades']);
	else if (this.id == 'selUFs')
		clearComboEx(['selCidades']);

	fillCmbUFsCidades(this);
}

/********************************************************************
Vai ao banco buscar os dados que serao usado para preencher os combos
	de UFs e Cidades.
********************************************************************/
function fillCmbUFsCidades(elem)
{
	var i=0;
	var nItensSelected=0;
	var sSelecteds='';

    for (i=0; i<elem.length; i++)
    {
        if ( elem.options[i].selected == true )
        {
            nItensSelected++;
            sSelecteds += (sSelecteds == '' ? '' : ',') + elem.options[i].value;
        }
    }
    
    if (nItensSelected == 0)
		return null;

	lockControlsInModalWin(true);
	setConnection(dsoFillCmbs);

	if (elem.id.toUpperCase() == 'SELPAISES')
	{
		dsoFillCmbs.SQL = 'SELECT 1 AS Indice, LocalidadeID AS fldID, CodigoLocalidade2 AS fldName ' +
			'FROM Localidades WITH(NOLOCK) ' +
			'WHERE EstadoID=2 AND TipoLocalidadeID = 204 AND LocalizacaoID IN (' + sSelecteds + ') ' +
			'ORDER BY CodigoLocalidade2';
	}
	else if (elem.id.toUpperCase() == 'SELUFS')
	{
		dsoFillCmbs.SQL = 'SELECT 2 AS Indice, LocalidadeID AS fldID, Localidade AS fldName ' +
			'FROM Localidades WITH(NOLOCK) ' +
			'WHERE EstadoID=2 AND TipoLocalidadeID = 205 AND LocalizacaoID IN (' + sSelecteds + ') ' +
			'ORDER BY Localidade';
	}

	dsoFillCmbs.ondatasetcomplete = fillCmbUFsCidades_DSC;
	dsoFillCmbs.refresh();
}

/********************************************************************
Preenche os combos de UFs e Cidades.
********************************************************************/
function fillCmbUFsCidades_DSC() {
    var oCmb = null;
    var optionStr, optionValue;
    var oOption;
    var i;
	
	if (dsoFillCmbs.recordset.BOF && dsoFillCmbs.recordset.EOF)
	{
		lockControlsInModalWin(false);

		return null;
	}
	
	if (dsoFillCmbs.recordset['Indice'].value == 1)
		oCmb = selUFs;
	else if (dsoFillCmbs.recordset['Indice'].value == 2)
		oCmb = selCidades;
	else	
	{
		lockControlsInModalWin(false);
		return null;
	}

    while (! dsoFillCmbs.recordset.EOF )
    {
        optionStr = dsoFillCmbs.recordset['fldName'].Value;
		optionValue = dsoFillCmbs.recordset['fldID'].Value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oCmb.add(oOption);
        dsoFillCmbs.recordset.MoveNext();
    }
    
    lockControlsInModalWin(false);
    window.focus();
}

/********************************************************************
Constroi a string de parametros que eh enviada para a pagina 
/serversidegenEx/emails/sendemailsmarketing.asp
	nTipoResultadoID: == 2 -> Somente listagem de e-mails
	nTipoResultadoID: == 3 -> Testar o email
	nTipoResultadoID: == 4 -> Enviar os emails
********************************************************************/
function buildStrParsEmails(nTipoResultadoID)
{
	var aCmbs = [[selEstados, 'sEstados'],
		[selCanais,'sCanais'],
		[selClassificacao,'sClassificacoes'],
		[selPaises,'sPaises'],
		[selUFs,'sUFs'],
		[selCidades,'sCidades'],
		[selFamilias,'sFamilias'],
		[selMarcas,'sMarcas']];

	var i, j;
	var nItensSelected=0;
	var sStrPars='';
	var sSelecteds='';
	var sProdutos='';
	var nUserID = getCurrUserID();
	var sError;
	var nEstadosSelected = 0;
	
	if ((trimStr(txtValidade.value) == '') || (chkDataEx(txtValidade.value) == false))
	{	
		if ( window.top.overflyGen.Alert ('Data de validade inv�lida.') == 0 )
			return null;
		return '';
	}

    // Percorre o array de combos
    for (i=0; i<aCmbs.length; i++)
    {
		for (j=0; j<(aCmbs[i][0]).length; j++)
		{
		    // Verifica se o item esta selecionado
		    if ( (aCmbs[i][0]).options[j].selected == true )
		    {
				if (i==0)
					nEstadosSelected++;
					
		        sSelecteds += (sSelecteds == '' ? '' : ',') + (aCmbs[i][0]).options[j].value;
		        nItensSelected++;
		    }
		}
		
		if (sSelecteds != '')
		{
			sStrPars += (sStrPars == '' ? '?' : '&');
			sStrPars += aCmbs[i][1] + '=' + escape(sSelecteds);
		}

		sSelecteds = '';
    }


	if (nTipoResultadoID != 2)
	{
	
		if (nEstadosSelected == 0)
		{
			if ( window.top.overflyGen.Alert ('Selecione pelo menos um estado.') == 0 )
				return null;
			
			return '';				
		}
	}

	sStrPars += (sStrPars == '' ? '?' : '&');
	
	return sStrPars + 'nEmpresaID=' + escape(glb_nEmpresaID) +
		'&sProdutos=' + escape(sProdutos) + 
		'&sAssunto=' + escape(trimStr(txtAssuntoEmail.value)) +
		'&dtValidade=' + escape(putDateInMMDDYYYY2(trimStr(txtValidade.value)));
}

/********************************************************************
Envia os e-mails ou e-mail de teste.
	nTipoResultadoID == 3 -> Envia os e-mails
	nTipoResultadoID == 4 -> Envia o e-mail de teste
********************************************************************/
function sendMails(nTipoResultadoID)
{
	if ( glb_modEmailsProm_Timer != null )    
	{
		window.clearInterval(glb_modEmailsProm_Timer);
		
		glb_modEmailsProm_Timer = null;
	}

    var strPars = buildStrParsEmails(nTipoResultadoID);
    var nUserID = getCurrUserID();
    
    if (strPars == '')
		return null;			
    
    strPars += '&nUsuarioID=' + escape(nUserID);
    strPars += '&nTipoResultadoID=' + escape(nTipoResultadoID);
    strPars += '&nOK=' + escape(glb_nOK);
    strPars += '&nVitrineID=' + escape(selVitrine.value);
    strPars += '&bEspecifico=' + escape(chkEMailEspecifico.checked ? 1 : 0);
    
	if (nTipoResultadoID == 3)
		btnEnviar.disabled = !glb_nDireito;

    lockControlsInModalWin(true);

    dsoGen01.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/sendemailsmarketing.aspx' + strPars;
    dsoGen01.ondatasetcomplete = sendMails_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Retorno do envio de e-mail
********************************************************************/
function sendMails_DSC() {
    lockControlsInModalWin(false);

    if ( !(dsoGen01.BOF && dsoGen01.EOF) )
    {
        dsoGen01.recordset.MoveFirst();
		
		if ((dsoGen01.recordset['nOK'].value == null) || (dsoGen01.recordset['nOK'].value == ''))
		{
			if ( window.top.overflyGen.Alert ( dsoGen01.recordset['fldResponse'].value ) == 0 )
				return null;
		}
		else
		{
			if (!dsoGen01.recordset['nOK'].Value)
			{
				if ( window.top.overflyGen.Alert ( dsoGen01.recordset['fldResponse'].Value ) == 0 )
					return null;
			}
			else
			{
				if (glb_nOK == 1)
				{
					glb_nOK = 0;

					if ( window.top.overflyGen.Alert ( dsoGen01.recordset['fldResponse'].value ) == 0 )
						return null;
				}
				else
				{
					var _retMsg;
					var sMsg = dsoGen01.recordset['fldResponse'].value;
			            
					if ( sMsg != '' )
					{
						_retMsg = window.top.overflyGen.Confirm(sMsg);
				        	    
						if ( (_retMsg == 0) || (_retMsg == 2) )
							return null;
						else
						{
							glb_nOK = 1;
							glb_modEmailsProm_Timer = window.setInterval('sendMails(4)', 100, 'JavaScript');
						}							
					}    
				}
			}
		}
    }
}
