/********************************************************************
modalagendamento.js

Library javascript para o modalagendamento.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_nMaxStringSize = 1024*1;
var glb_nA1 = 0;
var glb_nA2 = 0;
var glb_apu = 0;

// Variaveis para envio de dados ao aspx.
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;

//  Dados das lojas .RDS  
var dsoComboVitrines = new CDatatransport("dsoComboVitrines");
//  Dados do grid .RDS  
var dsoGrid = new CDatatransport("dsoGrid");
//  Gravacao de dados do grid .RDS  
var dsoGrava = new CDatatransport("dsoGrava");
//  Gravacao de dados do grid .RDS  
var dsoSincroniza = new CDatatransport("dsoSincroniza");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
fillGridData()
fillGridData_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalagendamentoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_modalagendamentoKeyPress(KeyAscii)
js_modalagendamento_ValidateEdit()
js_fg_AfterRowColmodalagendamento (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalagendamentoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    
	lockControlsInModalWin(true);
	
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
    showExtFrame(window, true);
}


/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Agendamento das vitrines', 1);
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    
    glb_nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B3A1' + '\'' + ')') );
    
    glb_nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B3A2' + '\'' + ')') );
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnCanc.disabled = true;
	btnOK.style.visibility = 'hidden';
    	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

	var arrayDeCampos = new Array(['lblCanal','selCanal',22,1,-10,-10],
    					 ['lblSegunda','chkSegunda',3,1],
						 ['lblTerca','chkTerca',3,1],
						 ['lblQuarta','chkQuarta',3,1],
						 ['lblQuinta','chkQuinta',3,1],
						 ['lblSexta','chkSexta',3,1],
						 ['lblSabado','chkSabado',3,1],
						 ['lblDomingo','chkDomingo',3,1],
						 ['lblVitrine','selVitrine',21,1],
						 ['btnAplicar','btn',btn_width,1, 7],
						 ['btnLimpar','btn',btn_width,1, 5]);
						  
    // Aqui ajusta os controles do divControls
    coll = window.document.getElementsByTagName('INPUT');
    var primeiro = true;
    for ( i=0; i<coll.length; i++ )
    {
        if (coll.item(i).type.toUpperCase() == 'CHECKBOX')
        {
			if(coll.item(i).id.substr(0,10).toUpperCase() == 'CHKHORARIO')
			{
				nome = coll.item(i).id.substr(10, coll.item(i).id.length);
				
				arrayDeCampos[arrayDeCampos.length] = [
					'lblHorario' + nome,
					'chkHorario' + nome,
					3,
					2,
					primeiro ? 175 : 0];

				primeiro = false;
			}
        }    
    }
	
    adjustElementsInForm(arrayDeCampos, null, null, true);
    
    btnLer.style.width = btn_width;
    btnLer.style.height = btnAplicar.offsetHeight;
    btnLer.style.top = selVitrine.offsetTop + 45;
    btnLer.style.left = btnAplicar.offsetLeft;
    btnGravar.style.width = btn_width;
    btnGravar.style.height = btnAplicar.offsetHeight;
    btnGravar.style.top = btnLer.offsetTop;
    btnGravar.style.left = btnLimpar.offsetLeft;
    
    lblDados.style.top = selVitrine.offsetTop + 30;
    lblDados.style.left = btnLer.offsetLeft - 40;

    chkDados.style.top = lblDados.offsetTop + 16;
    chkDados.style.width = FONT_WIDTH * 3;
    chkDados.style.left = lblDados.offsetLeft - 3;
    chkDados.onclick = chkDados_onclick;

	selCanal.style.height = 70;
	//btnLimpar.disabled = true;
	
	//lblOrigem.title = '0-Nacional\n1-Importado\n2-Importado adquirido no mercado interno';
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = btnLimpar.offsetLeft + btnLimpar.offsetWidth + ELEM_GAP;    
        height = selCanal.offsetTop + selCanal.offsetHeight;    
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + btnLimpar.offsetTop;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Confirmar';
		title = 'Confirmar Financeiros';
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // codigo privado desta janela
    var _retMsg;
    var sMsg = '';
    var iTipoQuantSelected = 0;
        
    if ( sMsg != '' )
    {
        _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
    }    
    
    if (controlID == btnAplicar.id)
    {
		aplicarGridData();
	}
	else if (controlID == btnLimpar.id)
	{
		limparGrid();
	}	
	else if (controlID == btnGravar.id)
	{
	    saveDataInGrid();
	}
	else if (controlID == btnLer.id)
	{
		fillGridData();
	}
    
    //restoreInterfaceFromModal();
    if (controlID == btnCanc.id)
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
}

/********************************************************************
********************************************************************/
function aplicarGridData()
{
	var selVitrine = window.document.getElementById('selVitrine');
	
	var arrayDia = new Array(window.document.getElementById('chkDomingo'),
		window.document.getElementById('chkSegunda'),
		window.document.getElementById('chkTerca'),
		window.document.getElementById('chkQuarta'),
		window.document.getElementById('chkQuinta'),
		window.document.getElementById('chkSexta'),
		window.document.getElementById('chkSabado'));
	
	var coll = window.document.getElementsByTagName('INPUT');
	
	for(row = 1; row < fg.Rows; row++)
	{
	    var j = 1;
	    while(j < 8)
		{
			if(!arrayDia[j - 1].checked)
			{
			    j++;

			    continue;
			}

			var canalID = fg.ValueMatrix(row, getColIndexByColKey(fg, 'CanalID'));
			var horaDia = fg.ValueMatrix(row, getColIndexByColKey(fg, 'HoraDia'));
			var achei = false;
			
			for(i = 0; i < coll.length; i++)
			{
				if((coll.item(i).type.toUpperCase() == 'CHECKBOX')
					&& (coll.item(i).id.substr(0,10).toUpperCase() == 'CHKHORARIO')
					&& (horaDia == coll.item(i).horaDia)
					&& coll.item(i).checked)
				{
					achei = true;
					break;
				}
			}
			
			if(!achei)
			{
			    j++;

			    continue;
			}
			
			for(k = 0; k < selCanal.options.length; k++)
			{
				if((selCanal.options[k].value == canalID) && (selCanal.options[k].selected))
				{
					fg.TextMatrix(row, getColIndexByColKey(fg, 'DiaSemana' + j)) = selVitrine.value;
					fg.TextMatrix(row, getColIndexByColKey(fg, 'OK')) = 1;
				}
			}

			j++;
		}
	}
}

function chkDados_onclick()
{
	for(row = 1; row < fg.Rows; row++)
		fg.TextMatrix(row, getColIndexByColKey(fg, 'OK')) = 1;
}

/********************************************************************
********************************************************************/
function limparGrid()
{
	for(row = 1; row < fg.Rows; row++)
	{
	    var j = 1;
	    var sDiaSemana;

	    while(j < 8)
		{
		    sDiaSemana = 'DiaSemana' + j;

		    fg.TextMatrix(row, getColIndexByColKey(fg, 'DiaSemana' + j)) = '';
			fg.TextMatrix(row, getColIndexByColKey(fg, 'OK')) = 1;

			j++;
	    }
	}
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{   
    // @@ atualizar parametros e dados aqui
    lockControlsInModalWin(true);
    
    // TODO - Voltar WHERE de EstadoID in (1,2) para EstadoID=2
    setConnection(dsoComboVitrines);
	dsoComboVitrines.SQL = 'SELECT a.VitrineID, a.Vitrine, a.TipoVitrineID ' +
		'FROM Vitrines a WITH(NOLOCK) INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.TipoVitrineID) ' + 
		'WHERE ((a.EstadoID IN (1,2)) AND (a.EmpresaID = ' + glb_aEmpresaData[0] + ')) ' + 
		'ORDER BY b.Ordem, a.TipoVitrineID, a.VitrineID	';
		
    dsoComboVitrines.ondatasetcomplete = comboLoja_DSC;
    dsoComboVitrines.Refresh();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function comboLoja_DSC() {
    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	lockControlsInModalWin(true);
	var sDadoNormal = (chkDados.checked ? '1' : '0');
	var strPars = '';
	    
    setConnection(dsoGrid);
	dsoGrid.SQL = 'SELECT a.ItemID AS CanalID, a.ItemMasculino AS Canal, b.ItemMasculino AS Hora, b.Numero AS HoraDia,' +
			'(SELECT VitrineID FROM Vitrines_Agendamento aa WITH(NOLOCK) ' +
				'WHERE (aa.EmpresaID = ' + glb_aEmpresaData[0] + ') AND (aa.CanalID = a.ItemID) AND (aa.EhDadoNormal = ' + sDadoNormal + ') AND ' +
					'(aa.DiaSemana = 2) AND (aa.HoraDia = b.Numero)) AS DiaSemana2, ' +
			'(SELECT VitrineID FROM Vitrines_Agendamento aa WITH(NOLOCK) ' +
						'WHERE (aa.EmpresaID = ' + glb_aEmpresaData[0] + ') AND (aa.CanalID = a.ItemID) AND (aa.EhDadoNormal = ' + sDadoNormal + ') AND ' +
							'(aa.DiaSemana = 3) AND (aa.HoraDia = b.Numero)) AS DiaSemana3, ' +
			'(SELECT VitrineID FROM Vitrines_Agendamento aa WITH(NOLOCK) ' +
						'WHERE (aa.EmpresaID = ' + glb_aEmpresaData[0] + ') AND (aa.CanalID = a.ItemID) AND (aa.EhDadoNormal = ' + sDadoNormal + ') AND ' +
							'(aa.DiaSemana = 4) AND (aa.HoraDia = b.Numero)) AS DiaSemana4, ' +
			'(SELECT VitrineID FROM Vitrines_Agendamento aa WITH(NOLOCK) ' +
						'WHERE (aa.EmpresaID = ' + glb_aEmpresaData[0] + ') AND (aa.CanalID = a.ItemID) AND (aa.EhDadoNormal = ' + sDadoNormal + ') AND ' +
							'(aa.DiaSemana = 5) AND (aa.HoraDia = b.Numero)) AS DiaSemana5, ' +
			'(SELECT VitrineID FROM Vitrines_Agendamento aa WITH(NOLOCK) ' + 
						'WHERE (aa.EmpresaID = ' + glb_aEmpresaData[0] + ') AND (aa.CanalID = a.ItemID) AND (aa.EhDadoNormal = ' + sDadoNormal + ') AND ' +
							'(aa.DiaSemana = 6) AND (aa.HoraDia = b.Numero)) AS DiaSemana6, ' +
			'(SELECT VitrineID FROM Vitrines_Agendamento aa WITH(NOLOCK) ' +
						'WHERE (aa.EmpresaID = ' + glb_aEmpresaData[0] + ') AND (aa.CanalID = a.ItemID) AND (aa.EhDadoNormal = ' + sDadoNormal + ') AND ' +
							'(aa.DiaSemana = 7) AND (aa.HoraDia = b.Numero)) AS DiaSemana7, ' +
			'(SELECT VitrineID FROM Vitrines_Agendamento aa WITH(NOLOCK) ' + 
						'WHERE (aa.EmpresaID = ' + glb_aEmpresaData[0] + ') AND (aa.CanalID = a.ItemID) AND (aa.EhDadoNormal = ' + sDadoNormal + ') AND ' +
							'(aa.DiaSemana = 1) AND (aa.HoraDia = b.Numero)) AS DiaSemana1, 0 AS OK ' +
		'FROM TiposAuxiliares_Itens a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
		'WHERE ((a.TipoID = 13) and (a.EstadoID = 2) AND (a.Filtro LIKE ' + '\'' + '%CLI%' + '\'' + ') AND ' +
			'(b.TipoID = 305) AND (b.EstadoID = 2)) ' +
		'ORDER BY a.Ordem, b.Ordem';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	window.focus();
	
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = true;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	var aHoldCols = new Array();

    headerGrid(fg,['Canal',
                   'Hora',
   				   'Segunda',
				   'Ter�a',
				   'Quarta',
				   'Quinta',
				   'Sexta',
				   'S�bado',
                   'Domingo',
				   'CanalID',
				   'HoraDia',
				   'OK'], [9,10,11]);

    fillGridMask(fg,dsoGrid,['Canal*',
                             'Hora*',
							 'DiaSemana2',
							 'DiaSemana3',
							 'DiaSemana4',
							 'DiaSemana5',
							 'DiaSemana6',
							 'DiaSemana7',
							 'DiaSemana1',
						     'CanalID',
						     'HoraDia',
						     'OK'],
							 ['','','','','','','','','','','',''],
							 ['','','','','','','','','','','','']);
	
	insertcomboData(fg, getColIndexByColKey(fg, 'DiaSemana2'), dsoComboVitrines, 'Vitrine', 'VitrineID');
	insertcomboData(fg, getColIndexByColKey(fg, 'DiaSemana3'), dsoComboVitrines, 'Vitrine', 'VitrineID');
	insertcomboData(fg, getColIndexByColKey(fg, 'DiaSemana4'), dsoComboVitrines, 'Vitrine', 'VitrineID');
	insertcomboData(fg, getColIndexByColKey(fg, 'DiaSemana5'), dsoComboVitrines, 'Vitrine', 'VitrineID');
	insertcomboData(fg, getColIndexByColKey(fg, 'DiaSemana6'), dsoComboVitrines, 'Vitrine', 'VitrineID');
	insertcomboData(fg, getColIndexByColKey(fg, 'DiaSemana7'), dsoComboVitrines, 'Vitrine', 'VitrineID');
	insertcomboData(fg, getColIndexByColKey(fg, 'DiaSemana1'), dsoComboVitrines, 'Vitrine', 'VitrineID');
	
    fg.MergeCells = 4;
	fg.MergeCol(0) = true;

    fg.Redraw = 0;
    
	fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	paintCellsSpecialyReadOnly();
	    
	fg.FrozenCols = 2;
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);
	
	btnGravar_status(fg);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    fg.Redraw = 2;    
    
    fg.Editable = true;
}
function gerarfinanceiro_DSC()
{
	
	if (dsoGrid.recordset.Fields.Count == 2)
	{
		if (dsoGrid.recordset['Resultado'].value != '')
		{
			if ( window.top.overflyGen.Alert(dsoGrid.recordset['Resultado'].value) == 0 )
				return null;
		}
		else if (dsoGrid.recordset['FinanceiroID'].value != 0)
		{
			var _retConf = window.top.overflyGen.Confirm('Criado financeiro: ' + dsoGrid.recordset['FinanceiroID'].value + '.\nDeseja detalhar?', 1);

			if ( _retConf == 1)
					sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(glb_aEmpresaData[0], dsoGrid.recordset['FinanceiroID'].value));
		}
		lockControlsInModalWin(false);
		fg.Rows = 1;
		btnGravar_status(fg);	
		return null;
    }
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

function btnGravar_status(fg)
{
	//if (fg.Rows > 2)
	//	btnGravar.disabled = false;
	//else
	//	btnGravar.disabled = true;
		
	//DireitoEspecifico
    //Acoes de Marketing->Acoes de Marketing->Modal Gerar Financeiros(B3)
    //40003 B3-> A1&&A2-> Habilita o botao Gerar.
	//if ((!glb_nA1) || (!glb_nA2))
	//	btnGravar.disabled = true;
}
function fg_DblClick()
{
	;
}
// EVENTOS DE GRID **************************************************
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalagendamentoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalagendamentoKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalagendamento_ValidateEdit(grid, row, col)
{
    fg.TextMatrix(row, getColIndexByColKey(fg, 'OK')) = 1;
}
function gerarfinanceiro()
{
 ;
}

function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        	lockControlsInModalWin(false);
		return false;
	}
	return true;
}
// FINAL DE EVENTOS DE GRID *****************************************

function saveDataInGrid() {
    var strPars = '';
    glb_apu = 0;
    
    strPars = '?EmpresaID=' + escape(glb_aEmpresaData[0]) + '&EhDefault=' + escape(1) +
		"&EhDadoNormal=" + escape(chkDados.checked ? 1 : 0);

    var nDataLen = 0;
    var nBytesAcum = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    for (i = 1; i < fg.Rows; i++)
     {
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 1)
            continue;

        nBytesAcum = strPars.length;

        if ((nBytesAcum >= glb_nMaxStringSize) && (strPars != '')) {
            nBytesAcum = 0;
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
            strPars = '';
            nDataLen = 0;
        }

        nDataLen++;

        strPars = ((strPars == '') ? ('?EmpresaID=' + escape(glb_aEmpresaData[0]) + '&EhDefault=' + escape(1) +
		                                "&EhDadoNormal=" + escape(chkDados.checked ? 1 : 0) + '&') : (strPars + '&')) + 
		            "CanalID=" + fg.ValueMatrix(i, getColIndexByColKey(fg, 'CanalID')) + "&HoraDia=" + fg.ValueMatrix(i, getColIndexByColKey(fg, 'HoraDia'));

        var j = 1;

        while (j < 8) {
            strPars += "&DiaSemana" + j.toString() + "=" + fg.ValueMatrix(i, getColIndexByColKey(fg, 'DiaSemana' + j.toString()));

            j++;
         }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/vitrines/serverside/gravaragendamento.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else 
        {
            if (window.top.overflyGen.Alert('Vitrine agendada com sucesso') == 0)
                return true; 
        }
            
        
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Erro. Tente novamente.') == 0)
            return null;
    }
}

function sendDataToServer_DSC() {

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

