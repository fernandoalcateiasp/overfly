/********************************************************************
modalprodutos.js

Library javascript para o modalprodutos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;

var glb_timerUnloadWin = null;

// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_refreshGrid = null;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_callFillGridTimerInt = null;
var glb_firstColLoja = 0;
var glb_nItensCancelados = 0;
var glb_nItensAtualizados = 0;
var glb_aDeletedElements = new Array();

// Dados do grid .RDS  
var dsoGrid = new CDatatransport("dsoGrid");
var dsoGrid2 = new CDatatransport("dsoGrid2");
// Combos da interface .RDS  
var dsoCmbsInterface = new CDatatransport("dsoCmbsInterface");
// Gravacao .RDS  
var dsoGrava = new CDatatransport("dsoGrava");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoAlternativoID = new CDatatransport("dsoAlternativoID");

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalprodutos.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalprodutos.ASP

js_fg_modalprodutosBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalprodutosDblClick( grid, Row, Col)
js_modalprodutosKeyPress(KeyAscii)
js_modalprodutos_AfterRowColChange
js_modalprodutos_ValidateEdit()
js_modalprodutos_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // ajusta o body do html
    with (modalprodutosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
    
	// carrega combos e mostra a janela modal
    fillCmbsData();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Lojas Web', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    
    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';

    // ajusta o divPesquisa
    elem = window.document.getElementById('divPesquisa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 90;
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.currentStyle.top, 10) +
			parseInt(divPesquisa.currentStyle.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = 155;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // ajusta o divFG
    with (divFG2.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFG.offsetTop + divFG.offsetHeight + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = 155;
    }
    
    with (fg2.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }
    
    adjustElementsInForm([['lblFiltroID','selFiltroID',15,1,-10,-10],
						  ['lblFamiliaID','selFamiliaID',20,1],
						  ['lblMarcaID','selMarcaID',20,1],
						  ['lblProprietarioID', 'selProprietarioID', 19, 1],
                          ['lblAlternativoID', 'selAlternativoID', 19, 1],
						  ['lblPesquisa','txtPesquisa',47,2,-10],
						  ['lblEstoque','chkEstoque',3,2],
						  ['btnListar','btn',btnOK.offsetWidth,2,0,-4],
						  ['btnAdicionar','btn',btnOK.offsetWidth,2],
						  ['btnRemover','btn',btnOK.offsetWidth,2],
						  ['btnGravar','btn',btnOK.offsetWidth,2,2]], null, null, true);

	btnListar.style.height = btnOK.offsetHeight;
	btnGravar.style.height = btnOK.offsetHeight;
	btnAdicionar.style.height = btnOK.offsetHeight;
	btnRemover.style.height = btnOK.offsetHeight;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;    
    fg2.Redraw = 2;

    selProprietarioID.onchange = selProprietarioID_onchange;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnGravar')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnListar')
    {
		fillGridData();    
    }
    else if (controlID == 'btnAdicionar')
    {
		adicionarProdutos();
    }
    else if (controlID == 'btnRemover')
    {
        removerProdutos();
        editaOrdem();
    }
    
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    if (glb_refreshGrid != null)
    {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState(bPar)
{
	var i;
	
	if ( bPar == false)
		btnGravar.disabled = bPar;
	else
		btnGravar.disabled = true;	
		
	var nTipoVitrineID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['TipoVitrineID'].value");
	if (nTipoVitrineID == 513)
	{
		var nMarcaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['MarcaID'].value");
		selOptByValueInSelect(getHtmlId(), 'selMarcaID', nMarcaID);
		selMarcaID.disabled = true;
	}
}

function fillCmbsData()
{
	var strSQL = '';
	var nEmpresaID = glb_aEmpresaData[0];
	var sEmpresasAlternativas;
	var sEmpresas = '';
	
	if (nEmpresaID == 2)
		sEmpresasAlternativas = '4,10, 21';
	else
		sEmpresasAlternativas = nEmpresaID;
		
	sEmpresas = '(' + nEmpresaID + ',' + sEmpresasAlternativas + ')';

    setConnection(dsoCmbsInterface);

    strSQL = 'SELECT 1 AS Indice1, 1 AS Indice2, -1 AS fldID, SPACE(0) AS fldName, NULL AS Atributo1, NULL AS Atributo2 ' +
		'UNION ALL ' +
			'SELECT 1 AS Indice1, 2 AS Indice2, 0 AS fldID, ' + '\'' + 'Sem Loja' + '\'' + ' AS fldName, NULL AS Atributo1, NULL AS Atributo2 ' +
		'UNION ALL ' +
			'SELECT 1 AS Indice1, (2 + a.Ordem) AS Indice2, a.ItemID AS fldID, a.ItemMasculino AS fldName, ' +
				'a.ItemMasculino AS Atributo1, ' +
				'CONVERT(VARCHAR(8000), a.Observacoes) AS Atributo2 ' +
			'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
			'WHERE (a.EstadoID = 2 AND a.TipoID = 108) ' +
		'UNION ALL ' +
			'SELECT 2 AS Indice1, a.Ordem AS Indice2, b.RecursoID AS fldID, b.RecursoFantasia AS fldName, ' +
				'ISNULL(b.Filtro, SPACE(0)) AS Atributo1, ' +
				'NULL AS Atributo2 ' +
			'FROM RelacoesRecursos a WITH(NOLOCK) ' +
				'INNER JOIN Recursos b WITH(NOLOCK) ON (a.SujeitoID = b.RecursoID) ' +
			'WHERE (a.ObjetoID = 24230 AND a.TipoRelacaoID = 2 AND ' +
				'b.EstadoID = 2 AND b.ClassificacaoID = 12) ' +
		'UNION ALL ' +
			'SELECT DISTINCT 3 AS Indice1, 1 AS Indice2, 0 AS fldID, SPACE(0) AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2  ' +
		'UNION ALL ' +
			'SELECT DISTINCT 3 AS Indice1, 2 AS Indice2, c.ConceitoID AS fldID, c.Conceito AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2  ' +
			'FROM RelacoesPesCon a WITH(NOLOCK) ' +
				'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) ' +
				'INNER JOIN Conceitos c WITH(NOLOCK) ON (b.ProdutoID = c.ConceitoID) ' +
			'WHERE (a.SujeitoID IN ' + sEmpresas + ' AND a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) ' +
		'UNION ALL ' +
			'SELECT DISTINCT 4 AS Indice1, 1 AS Indice2, 0 AS fldID, SPACE(0) AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2  ' +
		'UNION ALL ' +
			'SELECT DISTINCT 4 AS Indice1, 2 AS Indice2, c.ConceitoID AS fldID, c.Conceito AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2 ' +
			'FROM RelacoesPesCon a WITH(NOLOCK) ' +
				'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) ' +
				'INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MarcaID = c.ConceitoID) ' +
			'WHERE (a.SujeitoID IN ' + sEmpresas + ' AND a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) ' +
		'UNION ALL ' +
			'SELECT DISTINCT 5 AS Indice1, 1 AS Indice2, 0 AS fldID, SPACE(0) AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2  ' +
		'UNION ALL ' +
			'SELECT DISTINCT 5 AS Indice1, 2 AS Indice2, b.PessoaID AS fldID, b.Fantasia AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2 ' +
			'FROM RelacoesPesCon a WITH(NOLOCK) ' +
				'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +
			'WHERE (a.SujeitoID IN ' + sEmpresas + ' AND a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) ' +
			'ORDER BY Indice1, Indice2, fldName, fldID';

    if (strSQL != null)
    {
        dsoCmbsInterface.SQL = strSQL;
        dsoCmbsInterface.ondatasetcomplete = dsoCmbsInterface_DSC;
        dsoCmbsInterface.refresh();
    }
}

function dsoCmbsInterface_DSC() {
    var optionStr;
	var optionValue;
	var atributo1;
	var atributo2;
	
    while (! dsoCmbsInterface.recordset.EOF )
    {
        optionStr = dsoCmbsInterface.recordset['fldName'].value;
		optionValue = dsoCmbsInterface.recordset['fldID'].value;
		atributo1 = dsoCmbsInterface.recordset['Atributo1'].value;
		atributo2 = dsoCmbsInterface.recordset['Atributo2'].value;

		var oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		oOption.setAttribute('Atributo1', atributo1, 1);
		oOption.setAttribute('Atributo2', atributo2, 1);

		if (dsoCmbsInterface.recordset['Indice1'].value == 2)
			selFiltroID.add(oOption);
		if (dsoCmbsInterface.recordset['Indice1'].value == 3)
			selFamiliaID.add(oOption);
		if (dsoCmbsInterface.recordset['Indice1'].value == 4)
			selMarcaID.add(oOption);
		if (dsoCmbsInterface.recordset['Indice1'].value == 5)
			selProprietarioID.add(oOption);

        dsoCmbsInterface.recordset.MoveNext();
    }

    selProprietarioID_onchange();

	selFiltroID.disabled = (selFiltroID.options.length == 0);
	selFamiliaID.disabled = (selFamiliaID.options.length == 0);
	selMarcaID.disabled = (selMarcaID.options.length == 0);
	selProprietarioID.disabled = (selProprietarioID.options.length == 0);
	selOptByValueInSelect(getHtmlId(), 'selProprietarioID', glb_nUserID);
	
	fillGridData2();
	
	showExtFrame(window, true);
	
	setupBtnsFromGridState();
	
	window.focus();
}

function findCmbIndexByOptionID(oCmb, nOptionID)
{
	var retVal = -1;
	var i = 0;
	
	for (i=0; i<oCmb.options.length; i++)
	{
		if (oCmb.options.item(i).value == nOptionID)
		{
			retVal = i;
			break;
		}
	}
		
	return retVal;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}

    lockControlsInModalWin(true);
    
	var strSQL = '';
	var strPars = '';
	
    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    
    var nTipoPesquisa = 1;
	var sLojaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['LojaID'].value");
	var sFiltro = selFiltroID.options.item(selFiltroID.selectedIndex).getAttribute('Atributo1', 1);
	var nFamiliaID = selFamiliaID.value;
	var sFamiliaID = '';
	var nMarcaID = selMarcaID.value;
	var sMarcaID = '';
	var nProprietarioID = selProprietarioID.value;
	var sProprietarioID = '';
	var nAlternativoID = selAlternativoID.value;
	var sAlternativoID = '';
	var sPesquisa = trimStr(txtPesquisa.value);
	
	if (sFiltro ==  null || sFiltro == '') {
		sFiltro = 'NULL';
	}
		
	if (nFamiliaID == 0)
		sFamiliaID = 'NULL';
	else		
		sFamiliaID = nFamiliaID;

	if (nMarcaID == 0)
		sMarcaID = 'NULL';
	else		
		sMarcaID = nMarcaID;

	if (nProprietarioID == 0)
		sProprietarioID = 'NULL';
	else		
		sProprietarioID = nProprietarioID;

	if (nAlternativoID == 0)
	    sAlternativoID = 'NULL';
	else
	    sAlternativoID = nAlternativoID;

	if (sPesquisa == null || sPesquisa == '') {
		sPesquisa = 'NULL';
	}

	strPars = "?EmpresaID=" + glb_aEmpresaData[0] + 
		"&UsuarioID=" + glb_nUserID + 
		"&Registros=500" + 
		"&TipoPesquisa=" + nTipoPesquisa + 
		"&LojaID=" + sLojaID + 
		"&ClassificacaoID=NULL" +
		"&Filtro=" + sFiltro + 
		"&FamiliaID=" + sFamiliaID + 
		"&MarcaID=" + sMarcaID +
		"&ProprietarioID=" + sProprietarioID +
        "&AlternativoID=" + sAlternativoID +
		"&Pesquisa=" + sPesquisa + 
		"&Estoque=" + (chkEstoque.checked ? 1 : 0);

    if (strPars != null)
    {
		// parametrizacao do dso dsoGrid
		setConnection(dsoGrid);
		
        dsoGrid.URL = SYS_ASPURLROOT + "/modmarketing/subcampanhas/vitrines/serverside/LojasProdutos.aspx" + strPars;
        dsoGrid.ondatasetcomplete = fillGridData_DSC;
        dsoGrid.refresh();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
        
    fg.Redraw = 0;
    fg.Rows = 1;

    fg.ExplorerBar = 0;
    
    // Dados de Gerencia de Produto
    var aColsHeaderGrid = new Array();
    var aColsFillGridMask_1 = new Array();
    var aColsFillGridMask_2 = new Array();
    var aColsFillGridMask_3 = new Array();
    var aColsTotais = new Array();
    
    var i, j;
    var nomeLoja;
    var nPos = 0;
    var nQuantFamilia, nQuantMarca;
    var aFamilia = new Array();
    var aMarca = new Array();
    
    aColsHeaderGrid = ['Familia', 
                   'Marca',
                   'Modelo',
                   'Descri��o',
                   'Estq',
				   'GP',
				   'E',
                   'ID',
                   'OK',
                   'RelacaoID',
                   'EhProprietario',
                   'EhAlternativo'];
                   
	aColsFillGridMask_1 = ['Familia*', 
                   'Marca*',
                   'Modelo*',
                   'Descricao*',
                   'Estoque*',
				   'IniciaisGP*',
				   'Estado*',
                   'ProdutoID*',
                   '_calc_OK_1',
                   'RelacaoID*',
                   'EhProprietario*',
                   'EhAlternativo*'];
   
	glb_firstColLoja = aColsFillGridMask_1.length;
                   
    aColsFillGridMask_2 = ['','','','','','','','','','','',''];
	aColsFillGridMask_3 = ['','','','','','','','','','','',''];
                  
	aColsHidden = [9,10,11];

    aColsTotais = [[0,'####','C'], [1,'####','C'], [2,'####','C']];
    
    // Grid nao tem dados
    if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
    {
		fg.AutoSizeMode = 0;
		fg.AutoSize(0,fg.Cols-1);
		fg.Redraw = 2;
	    
		// Controla se o grid esta em construcao
		glb_GridIsBuilding = false;    
	    
		if (fg.Rows > 2)
			fg.Editable = true;
		
		lockControlsInModalWin(false);

		// coloca foco no grid
		window.focus();
		fg.focus();

		// ajusta estado dos botoes
		setupBtnsFromGridState();
		
		return;
    }
    
    headerGrid(fg, aColsHeaderGrid, aColsHidden);

	fg.ColDataType(getColIndexByColKey(fg, '_calc_OK_1')) = 11; // format boolean (checkbox)

    fillGridMask(fg,dsoGrid,
		aColsFillGridMask_1,
		aColsFillGridMask_2,
		aColsFillGridMask_3);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, aColsTotais);

    if (fg.Rows > 2)
    {
		nQuantFamilia = 0;
		nQuantMarca = 0;
		
        for (i = 2; i < fg.Rows; i++)
        {
			if ( ascan(aFamilia, fg.TextMatrix(i, 0), false) == -1 )
			{
				aFamilia[aFamilia.length] = fg.TextMatrix(i, 0);
				nQuantFamilia += 1;
			}

			if ( ascan(aMarca, fg.TextMatrix(i, 1), false) == -1 )
			{
				aMarca[aMarca.length] = fg.TextMatrix(i, 1);
				nQuantMarca += 1;
			}            
        }
        
        fg.TextMatrix(1, 0) = nQuantFamilia;
        fg.TextMatrix(1, 1) = nQuantMarca;
    }

    alignColsInGrid(fg,[4,7]);

    fg.FrozenCols = 3;
    
    fg.MergeCells = 4;
	fg.MergeCol(0) = true;
	fg.MergeCol(1) = true;
	
	fg.Redraw = 0;
    paintReadOnlyCols(fg);
    fg.Redraw = 2;
    
    fg.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg.Rows > 2 )
        fg.Row = 2;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    if (fg.Rows > 2)
        fg.Editable = true;
	
    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 2)
    {
        window.focus();
        fg.focus();
    }
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData2()
{
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}
	
	var vitrineID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['VitrineID'].value");
	var nEmpresaAlternativaID = 0;
    lockControlsInModalWin(true);
    
	if (glb_aEmpresaData[0] == 2)
		nEmpresaAlternativaID = 4;
	else
		nEmpresaAlternativaID = glb_aEmpresaData[0];
    
	var strSQL = 'SELECT Familias.Conceito AS Familia, Marcas.Conceito AS Marca, Produtos.Modelo, Produtos.Descricao, ' +
		        'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, ProdutosVitrine.ProdutoID, -341, NULL, GETDATE(), NULL, 375, NULL) AS Estoque, ' +
		        'GP.Iniciais AS IniciaisGP, EstadoProduto.RecursoAbreviado AS Estado, ' +
		        'ProdutosVitrine.ProdutoID, ProdutosVitrine.Ordem, ProdutosVitrine.dtVencimento, ProdutosVitrine.VitProdutoID ' +
	        'FROM Vitrines_Produtos ProdutosVitrine WITH(NOLOCK) ' +
			        'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON ProdutosVitrine.ProdutoID = Produtos.ConceitoID ' +
			        'INNER JOIN Conceitos Familias WITH(NOLOCK) ON Produtos.ProdutoID = Familias.ConceitoID ' +
			        'INNER JOIN Conceitos Marcas WITH(NOLOCK) ON Produtos.MarcaID=Marcas.ConceitoID ' +
			        'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON ProdutosEmpresa.ObjetoID=ProdutosVitrine.ProdutoID ' +
			        'INNER JOIN ((SELECT ' + glb_aEmpresaData[0] + ' AS EmpID ' +
							        'UNION ALL ' +
						        'SELECT EmpresaAlternativaID ' +
							        'FROM FopagEmpresas WITH(NOLOCK) ' +
							        'WHERE EmpresaID = ' + glb_aEmpresaData[0] + ' AND FuncionarioID IS NULL)) Emps ON ProdutosEmpresa.SujeitoID = Emps.EmpID ' +
			        'INNER JOIN RelacoesPesRec GP WITH(NOLOCK) ON GP.SujeitoID=ProdutosEmpresa.ProprietarioID ' +
			        'INNER JOIN Recursos EstadoProduto WITH(NOLOCK) ON EstadoProduto.RecursoID=ProdutosEmpresa.EstadoID ' +
	        'WHERE ProdutosVitrine.VitrineID = ' + vitrineID + '  ' +
			        'AND GP.TipoRelacaoID = 11 ' +
	        'ORDER BY ProdutosVitrine.Ordem';		
		
    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    

	// parametrizacao do dso dsoGrid
	setConnection(dsoGrid2);
    dsoGrid2.SQL = strSQL;
    dsoGrid2.ondatasetcomplete = fillGridData2_DSC;
    dsoGrid2.refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData2_DSC() 
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2);
    fg2.FontSize = '8';
    fg2.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
        
    fg2.Redraw = 0;
    fg2.Rows = 1;

    fg2.ExplorerBar = 0;
    
    // Dados de Gerencia de Produto
    var aColsHeaderGrid = new Array();
    var aColsFillGridMask_1 = new Array();
    var aColsFillGridMask_2 = new Array();
    var aColsFillGridMask_3 = new Array();
    var aColsTotais = new Array();
    
    var i, j;
    var nomeLoja;
    var nPos = 0;
    var nQuantFamilia, nQuantMarca;
    var aFamilia = new Array();
    var aMarca = new Array();
    
    aColsHeaderGrid = ['Familia', 
                   'Marca',
                   'Modelo',
                   'Descri��o',
                   'Estq',
				   'GP',
				   'E',
                   'ID',
                   'Ordem',
                   'Vencimento',
                   'OK',
                   'VitProdutoID'];
                   
	aColsFillGridMask_1 = ['Familia*', 
                   'Marca*',
                   'Modelo*',
                   'Descricao*',
                   'Estoque*',
				   'IniciaisGP*',
				   'Estado*',
                   'ProdutoID*',
                   'Ordem*',
                   'dtVencimento',
                   '_calc_OK_1',
                   'VitProdutoID'];

	
	//glb_firstColLoja = aColsFillGridMask_1.length;

    aColsFillGridMask_2 = ['','','','','','','','','99','99/99/9999','',''];
	aColsFillGridMask_3 = ['','','','','','','','','##', dTFormat,'',''];

	aColsHidden = [11];
    aColsTotais = [[0,'####','C'], [1,'####','C'], [2,'####','C']];
	headerGrid(fg2, aColsHeaderGrid, aColsHidden);

    fillGridMask(fg2,dsoGrid2,
		aColsFillGridMask_1,
		aColsFillGridMask_2,
		aColsFillGridMask_3);
	
	fg2.ColDataType(getColIndexByColKey(fg2, '_calc_OK_1')) = 11; // format boolean (checkbox)

    alignColsInGrid(fg2,[4,7]);

    fg2.FrozenCols = 3;
    
    fg2.MergeCells = 4;
	fg2.MergeCol(0) = true;
	fg2.MergeCol(1) = true;
	
	fg2.Redraw = 0;
    paintReadOnlyCols(fg2);
    fg2.Redraw = 2;
    
    fg2.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg2.Rows > 1 )
        fg2.Row = 1;
    
    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0,fg2.Cols-1);
    fg2.Redraw = 2;
    
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    //if (fg2.Rows > 2)
	fg2.Editable = true;
	
    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg2.Rows > 2)
    {
        window.focus();
        fg2.focus();
    }
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}


function saveDataInGrid()
{
	for (i=fg2.Rows-1; i>=1; i--)
	{
	    if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Ordem*')) == '')
		{
			if ( window.top.overflyGen.Alert ('Definir a ordem do produto ' + fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'ProdutoID*'))) == 0 )
				return null;
			
			return null;		
		}
	}
	

	var strPars = '';
	var nBytesAcum = 0;
	var nDataLen = 0;
	var i, j;
	
	lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
	var vitrineID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['VitrineID'].value");

	var ordem;
	
    for (i = 1; i < fg2.Rows; i++)
    {
        try {
            ordem = parseInt(getCellValueByColKey(fg2, 'Ordem*', i));
		}
		catch (e) {
		    ordem = 0;
        }
		
		nBytesAcum=strPars.length;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
				strPars = '';
				nDataLen = 0;
			}
			
  			nDataLen++;
			strPars += '?VitrineID=' + escape(vitrineID);
			
			strPars += '&ProdutoID=' + escape(getCellValueByColKey(fg2, 'ProdutoID*', i));
			strPars += '&Ordem=' + escape(ordem);
		    strPars += '&dtVencimento=' + escape(putDateInMMDDYYYY2(getCellValueByColKey(fg2, 'dtVencimento', i)));

			if (getCellValueByColKey(fg2, 'VitProdutoID', i) != '')
				strPars += '&VitProdutoID=' + escape(getCellValueByColKey(fg2, 'VitProdutoID', i));
			else
			    strPars += '&VitProdutoID=' + escape(0);
				
			continue;
		}

		nDataLen++;
		strPars += '&ProdutoID=' + escape(getCellValueByColKey(fg2, 'ProdutoID*', i));
		strPars += '&Ordem=' + escape(ordem);
	    strPars += '&dtVencimento=' + escape(putDateInMMDDYYYY2(getCellValueByColKey(fg2, 'dtVencimento', i)));

		if (getCellValueByColKey(fg2, 'VitProdutoID', i) != '')
			strPars += '&VitProdutoID=' + escape(getCellValueByColKey(fg2, 'VitProdutoID', i));
		else
			strPars += '&VitProdutoID=' + escape(0);
    }

    if (strPars != '')
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    strPars = '';

	for (i = 0; i < glb_aDeletedElements.length; i++) 
	{
	    nBytesAcum = strPars.length;

	    if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) 
	    {
	        nBytesAcum = 0;
	        if (strPars != '') 
	        {
	            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
	            strPars = '';
	            nDataLen = 0;
	        }

	        nDataLen++;
	        strPars += '?Deletar=' + escape(glb_aDeletedElements[i]);

	        continue;
	    }

	    nDataLen++;
	    strPars += '&Deletar=' + escape(glb_aDeletedElements[i]);	    
	}

	if (strPars != '')
	    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;	


	sendDataToServer();
}

function sendDataToServer()
{
	var sMensagem = '';

	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/vitrines/serverside/gravaproduto.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			closeWindow();
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		return null;
	}
}

function closeWindow()
{
	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalprodutosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalprodutosDblClick(grid, Row, Col)
{
    var bCheck;

    if (grid.Rows > 1) 
    {
        bCheck = grid.ValueMatrix(Row, Col);

        for (i = 2; i < grid.Rows; i++) 
        {
            grid.TextMatrix(i, Col) = bCheck;
        }
    }         
}

function js_fg2_modalprodutosDblClick(grid, Row, Col) 
{
    var bCheck;

    if ((grid.Rows > 1) && Col == getColIndexByColKey(fg2, '_calc_OK_1'))
    {
        bCheck = grid.ValueMatrix(Row, Col);

        for (i = 1; i < grid.Rows; i++) 
        {
            grid.TextMatrix(i, Col) = bCheck;
        }
    }         
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalprodutosKeyPress(KeyAscii)
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalprodutos_ValidateEdit()
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalprodutos_AfterEdit(Row, Col)
{
    var nType;
    
    if (fg.Editable)
    {
		if (fg.Row >= 2)
			setupBtnsFromGridState(false);
    }
}

function js_fg2_modalprodutos_AfterEdit(Row, Col)
{
    if (Col == getColIndexByColKey(fg2, '_calc_OK_1')) 
    {
        var nProdutoID = fg2.ValueMatrix(Row, getColIndexByColKey(fg2, 'ProdutoID*'));        
        var bCheck = fg2.ValueMatrix(Row, getColIndexByColKey(fg2, '_calc_OK_1'));

        for (var i = 1; i < fg2.Rows; i++) 
        {
            if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'ProdutoID*')) == nProdutoID)
                fg2.TextMatrix(i, getColIndexByColKey(fg2, '_calc_OK_1')) = bCheck;
        }
    }
}

// FINAL DE EVENTOS DE GRID *****************************************

function adicionarProdutos_2()
{
	var i=0;
	
	for (i=2; i<fg.Rows; i++)
	{
		if (fg.ValueMatrix(i, getColIndexByColKey(fg, '_calc_OK_1')) != 0)
		{
			insereProduto(i);
			fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_OK_1')) = 0;
		}
	}
}

function  adicionarProdutos()
{
    var nProdutoID;
    var sValidade = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "txtdtFim.value");
    var bExists = false;
    var nOrdem;
    var nOrdem2 = 1;
    var j = 0;
    var i = 0;

    fg2.Editable = false;
    glb_GridIsBuilding = true;  

    for (i = 2; i < fg.Rows; i++) 
    {
        nOrdem = 1;
        bExists = false;
        
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, '_calc_OK_1')) != 0) 
        {        
            nProdutoID = fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID*'));

            fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_OK_1')) = 0;

            for (j = 1; j < fg2.Rows; j++) 
            {
                if (fg2.TextMatrix(j, getColIndexByColKey(fg2, 'ProdutoID*')) == nProdutoID) 
                {
                    bExists = true;
                    break;
                }

                nOrdem2 = fg2.ValueMatrix(j, getColIndexByColKey(fg2, 'Ordem*'));

                if (nOrdem2 >= nOrdem)
                    nOrdem = nOrdem2 + 1;
            }

            if (bExists)
                continue;
                  
            fg2.Rows = fg2.Rows + 1;
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'Familia*')) = fg.TextMatrix(i, getColIndexByColKey(fg, 'Familia*'));
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'Marca*')) = fg.TextMatrix(i, getColIndexByColKey(fg, 'Marca*'));
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'Modelo*')) = fg.TextMatrix(i, getColIndexByColKey(fg, 'Modelo*'));
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'Descricao*')) = fg.TextMatrix(i, getColIndexByColKey(fg, 'Descricao*'));
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'Estoque*')) = fg.TextMatrix(i, getColIndexByColKey(fg, 'Estoque*'));
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'IniciaisGP*')) = fg.TextMatrix(i, getColIndexByColKey(fg, 'IniciaisGP*'));
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'Estado*')) = fg.TextMatrix(i, getColIndexByColKey(fg, 'Estado*'));
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'ProdutoID*')) = nProdutoID;
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'Ordem*')) = nOrdem;
            fg2.TextMatrix(fg2.Rows - 1, getColIndexByColKey(fg2, 'dtVencimento')) = sValidade;		                                    
        }
    }	
	
	fg2.Redraw = 0;
    paintReadOnlyCols(fg2);
    fg2.Redraw = 2;
    
    fg2.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg2.Rows > 1 )
        fg2.Row = 1;
    
    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0,fg2.Cols-1);
    fg2.Redraw = 2;
    glb_GridIsBuilding = false;   
    fg2.Editable = true;
	
    
}

function removerProdutos()
{
	btnGravar.disabled = false;
	var i=0;
	
	for (i=fg2.Rows-1; i>=1; i--)
	{
		if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, '_calc_OK_1')) != 0)
		{
			if (fg2.TextMatrix(i, getColIndexByColKey(fg2, 'VitProdutoID')) != '')
				glb_aDeletedElements[glb_aDeletedElements.length] = fg2.TextMatrix(i, getColIndexByColKey(fg2, 'VitProdutoID'));
			fg2.RemoveItem(i);
		}
	}
}

function editaOrdem() 
{
    var i = 0;
    var nProdutoID = 0;
    var nProdutoID2 = 0;
    var nOrdem = 0;
    
    for (i = 1;i < fg2.Rows; i++)    
    {
        nProdutoID = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'ProdutoID*'));

        if (nProdutoID != nProdutoID2)
            nOrdem += 1;

        fg2.TextMatrix(i, getColIndexByColKey(fg2, 'Ordem*')) = nOrdem;

        nProdutoID2 = nProdutoID;
    }

}

function selProprietarioID_onchange() {

    var nProprietarioID = selProprietarioID.value;
    var strSQL = '';
    var nEmpresaID = glb_aEmpresaData[0];
    var sEmpresasAlternativas;
    var sEmpresas = '';

    if (nEmpresaID == 2)
        sEmpresasAlternativas = '4,10, 21';
    else
        sEmpresasAlternativas = nEmpresaID;

    sEmpresas = '(' + nEmpresaID + ',' + sEmpresasAlternativas + ')';

    setConnection(dsoAlternativoID);

    strSQL = "SELECT DISTINCT 0 AS fldID, SPACE(0) AS fldName UNION ALL ";

    if (nProprietarioID == 0) {
        strSQL += "SELECT DISTINCT " +
	                            "b.PessoaID AS fldID, " +
	                            "b.Fantasia AS fldName " +
	                            "FROM RelacoesPesCon a WITH(NOLOCK) " +
	                            	"INNER JOIN Pessoas b WITH(NOLOCK) ON (a.AlternativoID = b.PessoaID) " +
	                            "WHERE (a.SujeitoID IN " + sEmpresas + " AND a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) " +
                                "ORDER BY fldName";
    } else {
        strSQL += "SELECT DISTINCT " +
	                            "b.PessoaID AS fldID, " +
	                            "b.Fantasia AS fldName " +
	                            "FROM RelacoesPesCon a WITH(NOLOCK) " +
	                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.AlternativoID = b.PessoaID) " +
	                            "WHERE (a.SujeitoID IN " + sEmpresas + " AND a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) AND (a.ProprietarioID = " + nProprietarioID.toString() + ") " +
                                 "ORDER BY fldName";
    }

    dsoAlternativoID.SQL = strSQL;

    dsoAlternativoID.ondatasetcomplete = dsoAlternativoID_DSC;
    dsoAlternativoID.Refresh();
}

function dsoAlternativoID_DSC() {

    var optionStr;
    var optionValue;

    selAlternativoID.disabled = false;

    clearComboEx(['selAlternativoID']);

    while (!dsoAlternativoID.recordset.EOF) {
        optionStr = dsoAlternativoID.recordset['fldName'].value;
        optionValue = dsoAlternativoID.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        selAlternativoID.add(oOption);

        dsoAlternativoID.recordset.MoveNext();
    }

    selAlternativoID.disabled = (selAlternativoID.options.length == 0);
}