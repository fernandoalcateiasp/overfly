
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalemailsmarketingHtml" name="modalemailsmarketingHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/vitrines/modalpages/modalemailsmarketing.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf

Dim i, nEmpresaID, nDireito, nFiltrarProdutos

nEmpresaID = 0
nDireito = 0
nFiltrarProdutos = 0

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nDireito").Count    
    nDireito = Request.QueryString("nDireito")(i)
Next

For i = 1 To Request.QueryString("nFiltrarProdutos").Count    
    nFiltrarProdutos = Request.QueryString("nFiltrarProdutos")(i)
Next

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nDireito = " & CStr(nDireito) & ";"
Response.Write vbcrlf

Response.Write "var glb_nFiltrarProdutos = " & CStr(nFiltrarProdutos) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<body id="modalemailsmarketingBody" name="modalemailsmarketingBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divFields" name="divFields" class="divGeneral">
        <p id="lblCanais" name="lblCanais" class="lblGeneral">Canal</p>
        <select id="selCanais" name="selCanais" class="fldGeneral">
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " & _
		 "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=13 and Filtro Like '%{1221}%' AND Filtro Like '%<CLI>%') " & _
		 "ORDER BY Ordem"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf

    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        
        <p id="lblVitrine" name="lblVitrine" class="lblGeneral">Vitrine</p>
        <select id="selVitrine" name="selVitrine" class="fldGeneral">
<%
Set rsData = Server.CreateObject("ADODB.Recordset")
strSQL = "SELECT VitrineID AS fldID, Vitrine AS fldName " & _
		 "FROM Vitrines WITH(NOLOCK) WHERE(EstadoID=2 and EmpresaID=" & CStr(nEmpresaID) & ") " & _
		 "ORDER BY TipoVitrineID, Vitrine"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	
	'If ( (CLng(rsData.Fields("fldID").Value) = 59) OR _
	'	 (CLng(rsData.Fields("fldID").Value) = 60) OR _
	'	 (CLng(rsData.Fields("fldID").Value) = 61) ) Then
	'	Response.Write " SELECTED "
	'End If

	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
    
        <p id="lblEstados" name="lblEstados" class="lblGeneral">Est</p>
        <select id="selEstados" name="selEstados" class="fldGeneral" MULTIPLE>
<%
Dim rsData, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT b.RecursoID AS fldID, b.RecursoAbreviado AS fldName " & _
		 "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK) WHERE(a.EstadoID=2 and a.TipoRelacaoID=3 AND a.ObjetoID=301 AND " & _
			"a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.RecursoID NOT IN (1,5)) " & _
		 "ORDER BY a.Ordem"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	
	If (CLng(rsData.Fields("fldID").Value) = 2) Then
		Response.Write " SELECTED "
	End If
	
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblClassificacao" name="lblClassificacao" class="lblGeneral">Class</p>
        <select id="selClassificacao" name="selClassificacao" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " & _
		 "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=29) " & _
		 "ORDER BY Ordem"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
    
    If ((CDbl(rsData.Fields("fldID").Value) = 145) OR _
        (CDbl(rsData.Fields("fldID").Value) = 146)) Then
        Response.Write " SELECTED"
    End If
    
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblPaises" name="lblPaises" class="lblGeneral">Pa�s</p>
        <select id="selPaises" name="selPaises" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT LocalidadeID AS fldID, CodigoLocalidade2 AS fldName " & _
		 "FROM Localidades WITH(NOLOCK) " & _
		 "WHERE TipolocalidadeID=203 and Estadoid=2" & _
		 "ORDER BY CodigoLocalidade3"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblUFs" name="lblUFs" class="lblGeneral">UF</p>
        <select id="selUFs" name="selUFs" class="fldGeneral" MULTIPLE></select>
        <p id="lblCidades" name="lblCidades" class="lblGeneral">Cidade</p>
        <select id="selCidades" name="selCidades" class="fldGeneral" MULTIPLE></select>
        <p id="lblFamilias" name="lblFamilias" class="lblGeneral">Produto</p>
        <select id="selFamilias" name="selFamilias" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT c.ConceitoID AS fldID, c.Conceito AS fldName " & _
		 "FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
		 "WHERE a.SujeitoID =" & CStr(nEmpresaID) & " AND a.TipoRelacaoID=61 AND a.EstadoID NOT IN (1, 4, 5) AND a.ObjetoID=b.ConceitoID AND " & _
		 "b.ProdutoID=c.ConceitoID " & _
		 "ORDER BY c.Conceito"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblMarcas" name="lblMarcas" class="lblGeneral">Marca</p>
        <select id="selMarcas" name="selMarcas" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT c.ConceitoID AS fldID, c.Conceito AS fldName " & _
		 "FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
		 "WHERE a.SujeitoID = " & CStr(nEmpresaID) & " AND a.TipoRelacaoID=61 AND a.EstadoID NOT IN (1, 4, 5) AND a.ObjetoID=b.ConceitoID AND " & _
		 "b.MarcaID=c.ConceitoID " & _
		 "ORDER BY c.Conceito"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>

        <p id="lblAssuntoEmail" name="lblAssuntoEmail" class="lblGeneral">Assunto</p>
        <input type="text" id="txtAssuntoEmail" name="txtAssuntoEmail" class="fldGeneral">

        <p id="lblValidade" name="lblValidade" class="lblGeneral">Validade</p>
        <input type="text" id="txtValidade" name="txtValidade" class="fldGeneral">
	    <p id="lblEMailEspecifico" name="lblEMailEspecifico" class="lblGeneral">Esp</p>
	    <input type="checkbox" id="chkEMailEspecifico" name="chkEMailEspecifico" class="fldGeneral" Title="E-mail espec�fico ou gen�rico?">
        
        
		<input type="button" id="btnOK" name="btnOK" value="Excluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnEmails" name="btnEmails" value="E-mails" title="Selecionar e-mails" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">		
		<input type="button" id="btnTeste" name="btnTeste" value="Teste" title="Enviar e-mail de teste para voc�" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnEnviar" name="btnEnviar" value="Enviar" title="Enviar e-mails para os clientes selecionados" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    </div>

    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
