using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;
using OVERFLYSVRCFGLib;

namespace Overfly3.modmarketing.subcampanhas.vitrinesEx.serverside
{
	public partial class template : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{

		}
		
		protected int argumentosVendaLen = 80;

		protected Integer vitrineID = Constants.INT_ZERO;
		protected Integer VitrineID
		{
			get { return vitrineID; }
			set { if(value != null) vitrineID = value; }
		}

		protected Integer empresaID = Constants.INT_ZERO;
		protected Integer EmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		protected Integer empresaAlternativaID = Constants.INT_ZERO;
		protected Integer EmpresaAlternativaID
		{
			get { return empresaAlternativaID; }
			set { if(value != null) empresaAlternativaID = value; }
		}
		
		protected string templatePage = Constants.STR_EMPTY;
		protected string TemplatePage
		{
			get
			{
				DataSet dsResult = null;
				
				if (templatePage == null || templatePage.Length == 0)
				{
					dsResult = DataInterfaceObj.getRemoteData(
                            "SELECT Template FROM Vitrines WITH(NOLOCK) WHERE VitrineID = " + vitrineID
						);
					
					if(dsResult.Tables[1].Rows.Count > 0)
					{
						templatePage = (string) dsResult.Tables[1].Rows[0]["Template"];
					}
				}

				return templatePage;
			}
			
			set
			{
				templatePage = value;
			}
		}

		protected ProdutoVitrine[] dataProdutos = null;
		protected ProdutoVitrine[] DataProdutos
		{
			get
			{
				if(dataProdutos == null)
				{
					DataSet ds = DataInterfaceObj.getRemoteData(
						"SELECT  " +
							"VitrineProdutos.Ordem, " +
							"VitrineProdutos.ProdutoID, " +
							"Familias.Conceito AS Produto, " +
							"Marcas.Conceito AS Marca, " +
							"Produtos.Modelo, " +
							"Produtos.Descricao, " +
							"Produtos.Descricao, (CASE WHEN LEN(CONVERT(VARCHAR(8000),dbo.fn_Produto_ArgumentosVenda(ProdutosEmpresa.ObjetoID, -ProdutosEmpresa.SujeitoID))) > " + argumentosVendaLen + " THEN LEFT(CONVERT(VARCHAR(8000), ProdutosEmpresa.ArgumentosVenda)," + argumentosVendaLen + ") + '...' " +
								"ELSE dbo.fn_Produto_ArgumentosVenda(ProdutosEmpresa.ObjetoID, -ProdutosEmpresa.SujeitoID) END) " +
							"AS ArgumentosVenda " +
						"FROM " +
                            "Vitrines_Produtos VitrineProdutos WITH(NOLOCK), " +
                            "Vitrines WITH(NOLOCK), " +
                            "RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), " +
                            "Conceitos Produtos WITH(NOLOCK), " +
                            "Conceitos Familias WITH(NOLOCK), " +
                            "Conceitos Marcas WITH(NOLOCK) " +
						"WHERE " +
							"Vitrines.VitrineID = " + vitrineID + " AND " +
							"Vitrines.VitrineID = VitrineProdutos.VitrineID AND " +
							"ProdutosEmpresa.SujeitoID IN (" + empresaID + ", " + empresaAlternativaID + ") AND " +
							"ProdutosEmpresa.ObjetoID = VitrineProdutos.ProdutoID AND " +
							"ProdutosEmpresa.TipoRelacaoID = 61 AND " +
							"VitrineProdutos.ProdutoID = Produtos.ConceitoID AND " +
							"Produtos.ProdutoID = Familias.ConceitoID AND " +
							"Produtos.MarcaID=Marcas.ConceitoID " +
						"ORDER BY " +
							"VitrineProdutos.Ordem"
					);
					
					dataProdutos = new ProdutoVitrine[ds.Tables[1].Rows.Count];
					
					for(int i = 0; i < ds.Tables[1].Rows.Count; i++)
					{
						dataProdutos[i] = new ProdutoVitrine();
						
						dataProdutos[i].Produto = (int) ds.Tables[1].Rows[i]["Ordem"];
						dataProdutos[i].ProdutoID = (int)ds.Tables[1].Rows[i]["ProdutoID"];
						dataProdutos[i].Descricao = "#" + (int)ds.Tables[1].Rows[i]["ProdutoID"] + "<br/>" + 
							(string)ds.Tables[1].Rows[i]["Produto"] + " " +
							(string)ds.Tables[1].Rows[i]["Marca"] + " " +
							(string)ds.Tables[1].Rows[i]["Modelo"] + " " +
							(string)ds.Tables[1].Rows[i]["Descricao"];
						dataProdutos[i].ArgumentoVenda = (string) (ds.Tables[1].Rows[i]["ArgumentosVenda"] != null ?
							ds.Tables[1].Rows[i]["ArgumentosVenda"] : "");
					}
				}
				
				return dataProdutos;
			}
		}

		protected ProdutoVitrine[] dataMelhores = null;
		protected ProdutoVitrine[] DataMelhores
		{
			get
			{
				if (dataMelhores == null)
				{
					DataSet ds = DataInterfaceObj.getRemoteData(
						"EXEC sp_Vitrine_MelhoresProdutos " + vitrineID + ", NULL, NULL, NULL, NULL, NULL, NULL, " + argumentosVendaLen
					);

					dataMelhores = new ProdutoVitrine[ds.Tables[1].Rows.Count];
					 
					for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
					{
						dataMelhores[i] = new ProdutoVitrine();

						dataMelhores[i].Produto = (int)ds.Tables[1].Rows[i]["Ordem"];
						dataMelhores[i].ProdutoID = (int)ds.Tables[1].Rows[i]["ProdutoID"];
						dataMelhores[i].Descricao = "#" + (int)ds.Tables[1].Rows[i]["ProdutoID"] + "<br/>" +
							(string)ds.Tables[1].Rows[i]["Produto"] + " " +
							(string)ds.Tables[1].Rows[i]["Marca"] + " " +
							(string)ds.Tables[1].Rows[i]["Modelo"] + " " +
							(string)ds.Tables[1].Rows[i]["Descricao"];
						dataMelhores[i].ArgumentoVenda = (string)(ds.Tables[1].Rows[i]["ArgumentosVenda"] != null ?
							ds.Tables[1].Rows[i]["ArgumentosVenda"] : "");
					}
				}

				return dataMelhores;
			}
		}

		public string HtmlProduto(int id, int produtoOrdem, int tipoLayout)
		{
			string strProduto = "";
			string sProdutoDescricao = "";

			int tamanhoImagem = tipoLayout;
			string imageSourceString;
			string strRoot;
			string srvName;

			foreach(ProdutoVitrine produtoVitrine in (id == 1) ? DataProdutos : DataMelhores)
			{
				if(produtoVitrine.Produto == produtoOrdem)
				{
					// Imagem
					srvName = Request.ServerVariables[0];// ("SERVER_NAME");
					
					if(srvName.ToUpper().IndexOf(".COM") > 0)
					{
						strRoot = "http://localhost/overfly3";
					}
					else
					{
						strRoot = Util.PagesURLRoot;
					}
				
					imageSourceString = strRoot + 
						"/serversidegenEx/imageblob.aspx?nFormID=2110&nSubFormID=21000&nRegistroID=" + 
						produtoVitrine.ProdutoID +
						"&nTamanho=" +
						tamanhoImagem +
						"&nOrdem=1";
					
					strProduto = "<p class=centro>\n" +
						"<IMG src='" + imageSourceString + "'>\n" +
						"</p>\n";
					
					sProdutoDescricao = produtoVitrine.Descricao;
						
					if(tipoLayout == 2)
					{
						sProdutoDescricao = "<B>" + sProdutoDescricao + "</B>";
					}
						
					// Descricao
					strProduto += "<p class=centro>\n" +
						"<a href='#' title='Produto em destaque 1' ";
					
					if(tipoLayout == 2)
					{
						strProduto += " class=titulo_principal ";
					}
					
					strProduto += ">" + sProdutoDescricao + "</a>\n";
						
					if(produtoVitrine.ArgumentoVenda != null)
					{
						strProduto += "<br/>\n" + 
							produtoVitrine.ArgumentoVenda + "\n";
					}
					
					if(tipoLayout == 3 || tipoLayout == 4)
					{
						strProduto += 
							"<br /><br /><p class='centro'>\n" +
								"Veja mais em:\n" +
							"<br />" +
							"<a href='#' title='Categoria 1' class='categoria'>XXXXXXXXXXXXXXXXXXXX</a>" +
							"</p><br />";
					}
						
					strProduto += "</p>\n\n";
				}
			}
			
			return strProduto;
		}

		protected string ColunaCentral()
		{
			string sProdutoOrdem = "";
			string sProdutoHTML = "";
			int nCounter = 0;
			int nCounter2 = 0;
			int nLexemaLen = 0;
			int nIdendifier = 0;
			int nTipoLayout = 0;
			int pointer;
				
			if (TemplatePage.Length > 0)
			{
				while (nCounter < TemplatePage.Length)
				{
					nLexemaLen = 0;

					pointer = TemplatePage.Substring(nCounter, TemplatePage.Length - nCounter).ToUpper().IndexOf("[PRODUTO_");
					
					if (pointer == -1)
					{
						pointer = TemplatePage.Substring(nCounter, TemplatePage.Length - nCounter).ToUpper().IndexOf("[MELHOR_");

						if (pointer == -1)
						{
							nCounter = TemplatePage.Length;
							continue;
						}
						else
						{
							nIdendifier = 2;
							nLexemaLen = 8;
						}
					}
					else
					{
						nIdendifier = 1;
						nLexemaLen = 9;
					}

					nCounter += pointer;
					
					if(nLexemaLen > 0)
					{
						nCounter2 = nCounter + nLexemaLen;
						sProdutoOrdem = "";

						while (Util.IsNumeric(TemplatePage.Substring(nCounter2, 1)))
						{
							sProdutoOrdem += TemplatePage.Substring(nCounter2, 1);
							nCounter2++;
						}

						if (TemplatePage.Substring(nCounter2, 1).Equals("|"))
						{
							nCounter2++;

							if (Util.IsNumeric(TemplatePage.Substring(nCounter2, 1)))
							{
								nTipoLayout = int.Parse(TemplatePage.Substring(nCounter2, 1));
								
								// Troca pelo codigo HTML do produto
								if(Util.IsNumeric(sProdutoOrdem))
								{
									nCounter2++;
									
									// Gera o html do produto ou dos melhores, dependendo do valor de nIdentifier.
									sProdutoHTML = HtmlProduto(nIdendifier, int.Parse(sProdutoOrdem), nTipoLayout);
									
									if(!sProdutoHTML.Equals(""))
									{
										nCounter2++;
										
										TemplatePage = TemplatePage.Substring(0, nCounter) + 
											sProdutoHTML +
											TemplatePage.Substring(nCounter2, TemplatePage.Length - nCounter2);

										nCounter += sProdutoHTML.Length;
									}
								}
							}
						}
					}
				}

				TemplatePage += " ";
			}

			return TemplatePage;
		}
	}
}
