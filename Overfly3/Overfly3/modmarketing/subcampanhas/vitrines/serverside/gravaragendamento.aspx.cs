using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.vitrinesEx.serverside
{
	public partial class gravaragendamento : System.Web.UI.OverflyPage
	{
		protected static Integer ZERO = new Integer(0);
		protected static Integer[] EMPTY = new Integer[0];
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			VitrineAgendamentoSincroniza();
			
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select 0 as fldresp"
			));
		}
		
		protected Integer empresaID = ZERO;
		protected Integer EmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		protected Integer dadoNormal = ZERO;
		protected Integer EhDadoNormal
		{
			get { return dadoNormal; }
			set { if(value != null) dadoNormal = value; }
		}

		protected Integer[] canalID = EMPTY;
		protected Integer[] CanalID
		{
			get { return canalID; }
			set { if(value != null) canalID = value; }
		}

		protected Integer[] horaDia = EMPTY;
		protected Integer[] HoraDia
		{
			get { return horaDia; }
			set { if(value != null) horaDia = value; }
		}

		protected Integer[] diaSemana1 = EMPTY;
		protected Integer[] DiaSemana1
		{
			get { return diaSemana1; }
			set { if(value != null) diaSemana1 = value; }
		}

		protected Integer[] diaSemana2 = EMPTY;
		protected Integer[] DiaSemana2
		{
			get { return diaSemana2; }
			set { if(value != null) diaSemana2 = value; }
		}

		protected Integer[] diaSemana3 = EMPTY;
		protected Integer[] DiaSemana3
		{
			get { return diaSemana3; }
			set { if(value != null) diaSemana3 = value; }
		}

		protected Integer[] diaSemana4 = EMPTY;
		protected Integer[] DiaSemana4
		{
			get { return diaSemana4; }
			set { if(value != null) diaSemana4 = value; }
		}

		protected Integer[] diaSemana5 = EMPTY;
		protected Integer[] DiaSemana5
		{
			get { return diaSemana5; }
			set { if(value != null) diaSemana5 = value; }
		}

		protected Integer[] diaSemana6 = EMPTY;
		protected Integer[] DiaSemana6
		{
			get { return diaSemana6; }
			set { if (value != null) diaSemana6 = value; }
		}

		protected Integer[] diaSemana7 = EMPTY;
		protected Integer[] DiaSemana7
		{
			get { return diaSemana7; }
			set { if (value != null) diaSemana7 = value; }
		}
		
		protected void VitrineAgendamentoSincroniza()
		{
			int vitrine = 0;

			ProcedureParameters[] param = new ProcedureParameters[6];

			param[0] = new ProcedureParameters("@EmpresaID", SqlDbType.Int, System.DBNull.Value);
			param[1] = new ProcedureParameters("@CanalID", SqlDbType.Int, System.DBNull.Value);
			param[2] = new ProcedureParameters("@EhDadoNormal", SqlDbType.Binary, System.DBNull.Value);
			param[3] = new ProcedureParameters("@DiaSemana", SqlDbType.Int, System.DBNull.Value);
			param[4] = new ProcedureParameters("@HoraDia", SqlDbType.Int, System.DBNull.Value);
			param[5] = new ProcedureParameters("@VitrineID", SqlDbType.Int, System.DBNull.Value);
			
			for(int i = 0; i < canalID.Length; i++)
			{
				for(int j = 1; j <= 7; j++)
				{
					if (j == 1) vitrine = diaSemana1[i].intValue();
					else if (j == 2) vitrine = diaSemana2[i].intValue();
					else if (j == 3) vitrine = diaSemana3[i].intValue();
					else if (j == 4) vitrine = diaSemana4[i].intValue();
					else if (j == 5) vitrine = diaSemana5[i].intValue();
					else if (j == 6) vitrine = diaSemana6[i].intValue();
					else if (j == 7) vitrine = diaSemana7[i].intValue();

					param[0].Data = empresaID.intValue();
					param[1].Data = canalID[i].intValue();
					param[2].Data = new byte[] {(byte) dadoNormal.intValue()};
					param[3].Data = j;
					param[4].Data = horaDia[i].intValue();
					param[5].Data = vitrine;
					
					DataInterfaceObj.execNonQueryProcedure(
                        "sp_Vitrine_AgendamentoSincroniza", 
						param);
				}
			}
		}
	}
}
