using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.vitrinesEx.serverside
{
	public partial class gravaproduto : System.Web.UI.OverflyPage
	{
		private static Integer ZERO = new Integer(0);
		private static Integer[] EMPTY = new Integer[0];
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			int fldresp = DataInterfaceObj.ExecuteSQLCommand(SQL);
			
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select " + fldresp + " as fldresp"
			));
				
		}
		
		protected Integer vitrineID = ZERO;
		protected Integer VitrineID
		{
			get { return vitrineID; }
			set { if(value != null) vitrineID = value; }
		}

		protected Integer[] produtoID = EMPTY;
		protected Integer[] ProdutoID
		{
			get { return produtoID; }
			set { if(value != null) produtoID = value; }
		}

		protected Integer[] vitProdutoID = EMPTY;
		protected Integer[] VitProdutoID
		{
			get { return vitProdutoID; }
			set { if(value != null) vitProdutoID = value; }
		}

		protected Integer[] ordem = EMPTY;
		protected Integer[] Ordem
		{
			get { return ordem; }
			set { if(value != null) ordem = value; }
		}

		protected string[] vencimento;
		protected string[] dtVencimento
		{
			get { return vencimento; }
			set { if(value != null) vencimento = value; }
		}

		protected Integer[] deletar = EMPTY;
		protected Integer[] Deletar
		{
			get { return deletar; }
			set { if(value != null) deletar = value; }
		}
		
		protected string SQL
		{
			get
			{
				string sql = "";
				int i;
				
				for(i = 0; i < deletar.Length; i++)
				{
					sql += " DELETE Vitrines_Produtos WHERE VitProdutoID=" + Deletar[i] + " ";
				}

				for (i = 0; i < produtoID.Length; i++)
				{
					if(vitProdutoID[i].intValue() != 0)
					{
						sql += 
							"UPDATE Vitrines_Produtos SET " + 
								"Ordem=" + ordem[i] + ", " +
								"dtVencimento='" + vencimento[i] + "' " + 
							"WHERE VitProdutoID=" + vitProdutoID[i] + " "; 
					}
					else
					{
						sql += "INSERT INTO Vitrines_Produtos (VitrineID, Ordem, dtVencimento, ProdutoID) " +
							"VALUES (" + vitrineID + ", " + 
								ordem[i] + ", '" + 
								vencimento[i] + "', " + 
								produtoID[i] + ") ";
					}
				}
				
				return sql;
			}
		}
	}
}
