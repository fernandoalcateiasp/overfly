using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;

namespace Overfly3.modmarketing.subcampanhas.vitrinesEx.serverside
{
	public partial class pesqproduto : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}
		
		protected string toFind = "";
		protected string strToFind
		{
			get { return toFind; }
			set { if (value != null) toFind = "'%" + value + "%'"; }
		}
		
		protected Integer empresaID = new Integer(0);
		protected Integer EmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}
		
		protected string SQL
		{
			get 
			{
				return 
					"SELECT DISTINCT TOP 100 a.Conceito AS fldName, a.ConceitoID AS fldID, " +
						"a.Modelo AS Modelo, a.PartNumber AS PartNumber, a.Descricao AS Descricao " +
                    "FROM Conceitos a WITH(NOLOCK), RelacoesPesCon b WITH(NOLOCK), RelacoesPesCon_Fornecedores c WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), " +
                        "RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), " +
                        "RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos1 WITH(NOLOCK) " +
					"WHERE (a.EstadoID = 2 AND a.TipoConceitoID = 303 AND b.SujeitoID = " + empresaID + " AND " +
						"a.ConceitoID = b.ObjetoID AND b.TipoRelacaoID = 61 AND b.EstadoID NOT IN(1,4,5) AND " +
						"b.RelacaoID = c.RelacaoID AND a.MarcaID = Marcas.ConceitoID AND a.ProdutoID = Familias.ConceitoID AND " +
						"Familias.EstadoID = 2 AND Familias.ConceitoID = RelConceitos2.SujeitoID AND " +
						"RelConceitos2.TipoRelacaoID = 41 AND RelConceitos2.EstadoID = 2 AND " +
						"RelConceitos2.ObjetoID = Conceitos2.ConceitoID AND Conceitos2.EstadoID = 2 AND " +
						"Conceitos2.ConceitoID = RelConceitos1.SujeitoID AND RelConceitos1.TipoRelacaoID = 41 AND " +
						"RelConceitos1.EstadoID = 2 AND RelConceitos1.ObjetoID = Conceitos1.ConceitoID AND " +
						"Conceitos1.EstadoID = 2 AND " +
						"((a.Conceito LIKE " + toFind + " ) OR " +
						"(Familias.Conceito LIKE " + toFind + " ) OR " +
						"(Conceitos1.Conceito LIKE " + toFind + " ) OR " +
						"(Conceitos2.Conceito LIKE " + toFind + " ) OR " +
						"(Marcas.Conceito LIKE " + toFind + " ) OR " +
						"(a.Modelo LIKE " + toFind + " ) OR " +
						"(a.Descricao LIKE " + toFind + " ) OR " +
						"(a.PartNumber LIKE " + toFind + " ) OR " +
						"(b.ObjetoID LIKE " + toFind + " ))) " +
					"ORDER BY fldName ";
			}
		}
	}
}
