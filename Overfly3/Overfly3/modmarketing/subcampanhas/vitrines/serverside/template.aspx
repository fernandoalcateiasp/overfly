<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="template.aspx.cs" Inherits="Overfly3.modmarketing.subcampanhas.vitrinesEx.serverside.template" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
		<title>Preview de vitrine</title>
		<meta http-equiv="pragma" content="no-cache" />
		<link href="style.css" rel="stylesheet" type="text/css" />
		<link href="vitrine.css" rel="stylesheet" type="text/css" />
		<link rel="shortcut icon" href="favicon.ico" />
	</head>
	
	<body>
		<!-- in�cio da coluna central -->
		<div id="coluna_central">
			<% Response.Write(ColunaCentral()); %>
		</div>
	</body>
</html>
