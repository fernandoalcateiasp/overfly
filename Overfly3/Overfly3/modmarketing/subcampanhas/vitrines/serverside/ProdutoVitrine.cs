using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Overfly3.modmarketing.subcampanhas.vitrinesEx.serverside
{
	public class ProdutoVitrine
	{
		private int produto;
		public int Produto
		{
			get { return produto; }
			set { produto = value; }
		}

		private int produtoID;
		public int ProdutoID
		{
			get { return produtoID; }
			set { produtoID = value; }
		}

		private string descricao;
		public string Descricao
		{
			get { return descricao; }
			set { descricao = value; }
		}

		private string argumentoVenda;
		public string ArgumentoVenda
		{
			get { return argumentoVenda; }
			set { argumentoVenda = value; }
		}
	}
}
