using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.modmarketing.subcampanhas.vitrinesEx.serverside
{
	public partial class LojasProdutos : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(SPLojasProdutos());
		}
		
		protected Integer empresaID;
		protected Integer EmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}
		
		protected Integer usuarioID;
		protected Integer UsuarioID
		{
			get { return usuarioID; }
			set { if(value != null) usuarioID = value; }
		}
		
		protected Integer registros;
		protected Integer Registros
		{
			get { return registros; }
			set { if(value != null) registros = value; }
		}
		
		protected Integer tipoPesquisa;
		protected Integer TipoPesquisa
		{
			get { return tipoPesquisa; }
			set { if(value != null) tipoPesquisa = value; }
		}
		
		protected Integer lojaID;
		protected Integer LojaID
		{
			get { return lojaID; }
			set { if(value != null) lojaID = value; }
		}
		
		protected Integer classificacaoID;
		protected Integer ClassificacaoID
		{
			get { return classificacaoID; }
			set { if(value != null) classificacaoID = value; }
		}
		
		protected string filtro;
		protected string Filtro
		{
			get { return filtro; }
			set { if(value != null) filtro = value; }
		}
		
		protected Integer familiaID;
		protected Integer FamiliaID
		{
			get { return familiaID; }
			set { if(value != null) familiaID = value; }
		}
		
		protected Integer marcaID;
		protected Integer MarcaID
		{
			get { return marcaID; }
			set { if(value != null) marcaID = value; }
		}
		
		protected Integer proprietarioID;
		protected Integer ProprietarioID
		{
			get { return proprietarioID; }
			set { if(value != null) proprietarioID = value; }
		}

        protected Integer alternativoID;
        protected Integer AlternativoID
        {
            get { return alternativoID; }
            set { if (value != null) alternativoID = value; }
        }
		
		protected string pesquisa;
		protected string Pesquisa
		{
			get { return pesquisa; }
			set { if(value != null) pesquisa = value; }
		}
		
		protected java.lang.Boolean estoque;
		protected java.lang.Boolean Estoque
		{
			get { return estoque; }
			set { if(value != null) estoque = value; }
		}
		
		protected DataSet SPLojasProdutos()
		{
			ProcedureParameters[] param = new ProcedureParameters[14];

			param[0] = new ProcedureParameters(
				"@EmpresaID", 
				SqlDbType.Int, 
				empresaID != null ? (Object)empresaID.ToString() : System.DBNull.Value);
			
			param[1] = new ProcedureParameters(
				"@UsuarioID", 
				SqlDbType.Int, 
				usuarioID != null ? (Object)usuarioID.ToString() : System.DBNull.Value);
			
			param[2] = new ProcedureParameters(
				"@Registros", 
				SqlDbType.Int, 
				registros != null ? (Object)registros.ToString() : System.DBNull.Value);
			
			param[3] = new ProcedureParameters(
				"@TipoPesquisa", 
				SqlDbType.Int, 
				tipoPesquisa != null ? (Object)tipoPesquisa.ToString() : System.DBNull.Value);
			
			param[4] = new ProcedureParameters(
				"@LojaID", 
				SqlDbType.Int, 
				lojaID != null ? (Object)lojaID.ToString() : System.DBNull.Value);
			
			param[5] = new ProcedureParameters(
				"@ClassificacaoID", 
				SqlDbType.Int, 
				classificacaoID != null ? (Object)classificacaoID.ToString() : System.DBNull.Value);
			
			param[6] = new ProcedureParameters(
				"@Filtro", 
				SqlDbType.VarChar, 
				(filtro != null && filtro.Length > 0 && !filtro.ToUpper().Equals("NULL")) ?
					(Object)filtro : System.DBNull.Value);
			param[6].Length = filtro != null ? filtro.Length : 0;
			
			param[7] = new ProcedureParameters(
				"@FamiliaID", 
				SqlDbType.Int, 
				familiaID != null ? (Object)familiaID.ToString() : System.DBNull.Value);
			
			param[8] = new ProcedureParameters(
				"@MarcaID", 
				SqlDbType.Int, 
				marcaID != null ? (Object)marcaID.ToString() : System.DBNull.Value);
			
			param[9] = new ProcedureParameters(
				"@ProprietarioID", 
				SqlDbType.Int, 
				proprietarioID != null ? (Object)proprietarioID.ToString() : System.DBNull.Value);

            param[10] = new ProcedureParameters(
                "@AlternativoID",
                SqlDbType.Int,
                alternativoID != null ? (Object)alternativoID.ToString() : System.DBNull.Value);

            param[11] = new ProcedureParameters(
				"@Pesquisa", 
				SqlDbType.VarChar,
				(pesquisa != null && pesquisa.Length > 0 && !pesquisa.ToUpper().Equals("NULL")) ?
					(Object)pesquisa : System.DBNull.Value);
			param[11].Length = pesquisa != null ? pesquisa.Length : 0;
			
			param[12] = new ProcedureParameters(
				"@Estoque", 
				SqlDbType.Bit, 
				estoque.booleanValue() ? 1 : 0);

            param[13] = new ProcedureParameters(
                "@LojaCanal",
                SqlDbType.Int,
                System.DBNull.Value);
			
			return DataInterfaceObj.execQueryProcedure("sp_Lojas_Produtos", param);
		}
	}
}
