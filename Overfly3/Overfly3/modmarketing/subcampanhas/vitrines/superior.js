/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
//  Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
//  Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
//  Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
//  Busca data corrente no servidor .URL 
var dsoCurrData = new CDatatransport("dsoCurrData");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
    					  ['selLojaEspecialID', '2'],
						  ['selLojaID', '3'],
						  ['selMarcaID', '4']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modmarketing/subcampanhas/vitrines/inferior.asp',
                              SYS_PAGESURLROOT + '/modmarketing/subcampanhas/vitrines/pesquisa.asp');                          
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'VitrineID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoVitrineID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);
    
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblVitrine','txtVitrine',40,1],
                          ['lblTipoRegistroID','selTipoRegistroID',15,1],
                          ['lblLojaEspecialID','selLojaEspecialID',23,1],                          
                          ['lblLojaID','selLojaID',23,1,-selLojaEspecialID.offsetWidth-34],                                                    
                          ['lblMarcaID','selMarcaID',23,1,-selLojaID.offsetWidth-34],
                          ['lblQuantidadeProdutos', 'txtQuantidadeProdutos', 5, 2],
                          ['lblEhRandomica','chkEhRandomica',3,2],
                          ['lbldtInicio','txtdtInicio',10,2],
                          ['lbldtFim','txtdtFim',10,2],
                          ['lblObservacao','txtObservacao',40,2]], null, null, true);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    // Da automacao, deve constar de todos os ifs do carrierArrived
    if ( __currFormState() != 0 )
            return null;
            
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    
	showHideCombos(true);

	// Mover esta funcao para os finais de retornos de operacoes no
	// banco de dados
	finalOfSupCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
    {
        adjustSupInterface();
        showHideCombos(false);
    }
    else
		adjustLabelsCombos(false);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }
    //else if (btnClicked == 4)
	//{
	//	loadModalImagem();
	//}
}

function loadModalImagem()
{
	var strPars = new String();
	
	var nVitrineID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['VitrineID'].value");
 
	var nTipoVitrineID = dsoSup01.recordset["TipoVitrineID"].value;
	
	var sCaller = 'S';
 
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );

	strPars = '?nRegistroID=' + escape(nVitrineID);
	strPars += '&nTipoRegistroID=' + escape(nTipoVitrineID);
	strPars += '&sCaller=' + escape(sCaller);
	
	//DireitoEspecifico
    //Pessoas->F�sicas/jur�dicas->Modal Imagens
    //20100 SFS-Grupo Pessoas -> A1&&A2 -> Permite abrir a modal para gerenciar imagens, caso contrario apenas mostra a imagem cadastrada.
    if ( (nA1 == 1) && (nA2 == 1) )
    {
        htmlPath = SYS_ASPURLROOT + '/modalgen/modalgerimagens.asp' + strPars;
    	
		showModalWin(htmlPath, new Array(724, 488));
    }
    else
    {
        htmlPath = SYS_ASPURLROOT + '/modalgen/modalshowimagens.asp' + strPars;
    
		showModalWin(htmlPath, new Array(0,0));
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var nEmpresaID = getCurrEmpresaData();
    
    if (btnClicked == 'SUPOK')
    {
        // Se e um novo registro
        //if (dsoSup01.recordset[glb_sFldIDName] != null && dsoSup01.recordset[glb_sFldIDName].value != null)
        if (dsoSup01.recordset[glb_sFldIDName].value == null)
			dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Imagens
    if ( (idElement.toUpperCase() == 'MODALGERIMAGENSHTML')||
          (idElement.toUpperCase() == 'MODALSHOWIMAGENSHTML'))
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    showBtnsEspecControlBar('sup', true, [1,1,1,0,0]);
    tipsBtnsEspecControlBar('sup', ['Documentos','Relat�rios','Procedimentos','','']);

    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);
}
function dsoCurrDataComplete_DSC() {
    // Prossegue a automacao interrompida no botao de inclusao
    lockAndOrSvrSup_SYS();
}

function showHideCombos(bPaging)
{
	var tipoRegistroID;
	
	if (bPaging)
		tipoRegistroID = dsoSup01.recordset['TipoVitrineID'].value;
	else
		tipoRegistroID = selTipoRegistroID.value;
		
	// Home		
	if (tipoRegistroID == 511)
	{
	    lblLojaEspecialID.style.visibility = 'hidden';
		selLojaEspecialID.style.visibility = 'hidden';
		lblLojaID.style.visibility = 'hidden';
		selLojaID.style.visibility = 'hidden';
		lblMarcaID.style.visibility = 'hidden';
		selMarcaID.style.visibility = 'hidden';
	}
	
	// Loja especial
	else if (tipoRegistroID == 514)
	{
	    lblLojaEspecialID.style.visibility = 'inherit';
		selLojaEspecialID.style.visibility = 'inherit';
		lblLojaID.style.visibility = 'hidden';
		selLojaID.style.visibility = 'hidden';
		lblMarcaID.style.visibility = 'hidden';
		selMarcaID.style.visibility = 'hidden';
	}
	
	// Loja
	else if (tipoRegistroID == 512)
	{
	    lblLojaEspecialID.style.visibility = 'hidden';
		selLojaEspecialID.style.visibility = 'hidden';
		lblLojaID.style.visibility = 'inherit';
		selLojaID.style.visibility = 'inherit';
		lblMarcaID.style.visibility = 'hidden';
		selMarcaID.style.visibility = 'hidden';
	}
	
	// Marca
	else if (tipoRegistroID == 513)
	{
	    lblLojaEspecialID.style.visibility = 'hidden';
		selLojaEspecialID.style.visibility = 'hidden';
		lblLojaID.style.visibility = 'hidden';
		selLojaID.style.visibility = 'hidden';
		lblMarcaID.style.visibility = 'inherit';
		selMarcaID.style.visibility = 'inherit';
	}
	
	adjustLabelsCombos(bPaging);
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
        setLabelOfControl(lblTipoRegistroID, dsoSup01.recordset['TipoVitrineID'].value);
        setLabelOfControl(lblLojaID, dsoSup01.recordset['LojaID'].value);
        setLabelOfControl(lblMarcaID, dsoSup01.recordset['MarcaID'].value);
	}        
    else
    {
        setLabelOfControl(lblTipoRegistroID, selTipoRegistroID.value);
        setLabelOfControl(lblLojaID, selLojaID.value);
        setLabelOfControl(lblMarcaID, selMarcaID.value);
    }
}
