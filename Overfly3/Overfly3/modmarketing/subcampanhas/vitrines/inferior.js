/********************************************************************
recursosinf01.js

Library javascript para o recursosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var __inFTimer_Conceitos = null;

//  Dados do registro corrente .SQL (no minimo: regID, observacoes, propID e alternativoID) 
var dso01JoinSup = new CDatatransport("dso01JoinSup");
//  Dados de combos estaticos do form .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
//  Dados de combos dinamicos do form .URL 
var dsoDynamicCmbs = new CDatatransport("dsoDynamicCmbs");
//  Descricao dos estados atuais dos registros do grid corrente .SQL         
var dsoStateMachineLkp = new CDatatransport("dsoStateMachineLkp");        
//  Dados do combo de filtros da barra inferior .SQL         
var dsoFiltroInf = new CDatatransport("dsoFiltroInf");
//  Dados dos combos de proprietario e alternativo         
var dsoCmbsPropAlt = new CDatatransport("dsoCmbsPropAlt");
//  Dados de um grid .SQL 
var dso01Grid = new CDatatransport("dso01Grid");
var dso01GridLkp = new CDatatransport("dso01GridLkp");
var dso02GridLkp = new CDatatransport("dso02GridLkp");
var dso03GridLkp = new CDatatransport("dso03GridLkp");
var dso04GridLkp = new CDatatransport("dso04GridLkp");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();
    
    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 
    glb_aGridStaticCombos = [ [20010, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dso02GridLkp'   , ''             , ''],
                                       [0, 'dso03GridLkp'   , ''             , ''],
                                       [0, 'dso04GridLkp'   , ''             , '']]],
                              [23102, [[0, 'dso01GridLkp'   , ''             , '']]] ];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
        
    linkDivsAndSubForms(null, ['divInf01_01',
							   'divInf01_02',
                               'divInf01_03',
                               'divInf01_04'],
                               [20008, 20009, 23101, [20010, 23102]] );

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
    
}

function setupPage()
{
    // txtTexto
    elem = window.document.getElementById('txtTemplate');
    //elem.disabled = false;
    with (elem.style)
    {
        left = 10;
        top = 10;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 2);
        height = (MAX_FRAMEHEIGHT / 2) - (ELEM_GAP * 7);
    }
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{

}

function fg_DblClick_Prg()
{

}

function fg_ChangeEdit_Prg()
{

}
function fg_ValidateEdit_Prg()
{

}
function fg_BeforeRowColChange_Prg()
{

}

function fg_EnterCell_Prg()
{

}

function fg_MouseUp_Prg()
{

}

function fg_MouseDown_Prg()
{

}

function fg_BeforeEdit_Prg()
{

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
 
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

     
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

	// Mover esta funcao para os finais retornos de operacoes no
	// banco de dados
	finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    // esconde botoes especificos
    showBtnsEspecControlBar('inf', false, [1,1,1,1,1,1]);
    tipsBtnsEspecControlBar('inf', ['','','','','','']);

    // Template
    if (pastaID == 23101)
    {
        // mostra dois botoes especificos desabilitados
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Preview', '', '', '']);
    }
    // Produtos
    else if (pastaID == 23102)
    {
        // mostra dois botoes especificos desabilitados
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Incluir produto', 'Detalhar produto', '', '']);
    }
    
    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
	    
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{
    //@@
    
    setupEspecBtnsControlBar('inf', 'HDDDDDDD');

    if (folderID == 23101)
    {
		if (btnClicked != 'INFALT')
			setupEspecBtnsControlBar('inf', 'HDDDDDD');
		else
			setupEspecBtnsControlBar('inf', 'DDDDDDD');
	}
    else if (folderID == 23102)
    {
		setupEspecBtnsControlBar('inf', 'HDDDDDD');
		if (fg.Row > 0)
			setupEspecBtnsControlBar('inf', 'HHDDDDD');
	}
	
	setupEspecBtnsControlBar('sup', 'HDDHDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{
    if (folderID == 23102)
		setupEspecBtnsControlBar('inf', 'HDDDDDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
    
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var nEmpresaData = getCurrEmpresaData();
	var nEmpresaAlternativaID = 0;
	var nEmpresaVitrineID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL , "dsoSup01.recordset['EmpresaID'].value");
	
	if (nEmpresaVitrineID == 2)
		nEmpresaAlternativaID = 4;
	else
		nEmpresaAlternativaID = nEmpresaVitrineID;
	
	if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 23102))
		openModalProdutos();
	else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 23102))
	{
		var relacaoID = getCellValueByColKey(fg, '^ProdutoID^dso01GridLkp^ProdutoID^RelacaoID', fg.Row);
		sendJSCarrier(getHtmlId(), 'SHOWRELPESCON', new Array(nEmpresaData[0], relacaoID));
	}
	else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 23101))
	{
        nVitrineID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL , "dsoSup01.recordset['VitrineID'].value");
		window.open(SYS_PAGESURLROOT + '/modmarketing/subcampanhas/vitrines/serverside/template.aspx?VitrineID=' + escape(nVitrineID) +
			'&EmpresaID=' + escape(nEmpresaVitrineID) + '&EmpresaAlternativaID=' + escape(nEmpresaAlternativaID));
	}
	
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Produtos
    if ( keepCurrFolder() == 23102 )
    {
        if (idElement.toUpperCase() == 'MODALPRODUTOSHTML')
        {
            if ( param1 == 'OK' )                
            {
                //writeProdutoInGrid(param2);
                // esta funcao fecha a janela modal e destrava a interface
                // movida para a funcao acima
                //restoreInterfaceFromModal();
                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Altera��o');
            }
            else if (param1 == 'CANCEL')
            {
                // esta funcao fecha a janela modal e destrava a interface
                restoreInterfaceFromModal();    
                            
                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Altera��o');
            }
            
            __inFTimer_Conceitos = window.setInterval('forceInfRefr()', 50, 'JavaScript');
             return null;
        }    
    }    
}

function forceInfRefr()
{
    if ( __inFTimer_Conceitos != null )
    {
        window.clearInterval(__inFTimer_Conceitos);
        __inFTimer_Conceitos = null;
    }
    
   __btn_REFR('inf');
}

/********************************************************************
Funcao criada pelo programador.
Escreve Produto e ID do Produto vindo da janela modal de Produtos.
Pasta de produtos - 24152

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeProdutoInGrid(aProduto)
{
    // o array aProduto contem: Conceito, ConceitoID
    
    fg.Redraw = 0;
    // Escreve nos campos
    fg.TextMatrix(keepCurrLineInGrid(),1) = aProduto[0];
    fg.TextMatrix(keepCurrLineInGrid(),2) = aProduto[1];
    fg.TextMatrix(keepCurrLineInGrid(),3) = aProduto[2];
    
    fg.Redraw = 2;
    
    // esta funcao fecha a janela modal e destrava a interface
    restoreInterfaceFromModal();
    
    // ajusta barra de botoes
    setupEspecBtnsControlBar('inf', 'HDDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{
	var nVitrineID = 0;
	//setupEspecBtnsControlBar('inf', 'HDDDDDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{

    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    // Pasta de LOG Read Only
    if (folderID == 20010)
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // Pasta Produtos
    else if (folderID == 23102)
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de produtos para a pasta de produtos - 24152

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalProdutos()
{
	var htmlPath;
    var strPars = new String();
    var nWidth = 810;
    var nHeight = 460;
    var i;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('I');
    //strPars += '&nA1=' + escape(nA1);
    //strPars += '&nA2=' + escape(nA2);
    strPars += '&nA1=' + escape(1);
    strPars += '&nA2=' + escape(1);
    
    // o combo de numeros de registros
    for (i = 0; i < 2; i++)
    {
		strPars += '&sCmbText=' + escape(i);
		strPars += '&nCmbValue=' + escape(i);
		
		if ( i == 0 )
			strPars += '&nCmbOptionSel=' + escape(1);
		else
			strPars += '&nCmbOptionSel=' + escape(0);
    }
    
    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modmarketing/subcampanhas/vitrines/modalpages/modalprodutos.asp' + strPars;
    
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}
