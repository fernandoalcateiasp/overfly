/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de publicacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.PublicacaoID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Publicacoes a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.PublicacaoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 23328) // Marcas
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Publicacoes_Marcas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.PublicacaoID = ' + idToFind + ' ' +
                  'ORDER BY a.MarcaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 23321) // Destinos
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Publicacoes_Destinos a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.PublicacaoID = ' + idToFind + ' ' +
                  'ORDER BY a.DestinoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 23322) // Ocultas
    {
        dso.SQL = 'SELECT a.*, b.Fantasia AS Usuario ' +
                  'FROM Publicacoes_Ocultas a WITH(NOLOCK) ' +
                  'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.UsuarioID ' +
                  'WHERE ' + sFiltro + ' a.PublicacaoID = ' + idToFind + ' ' +
                  'ORDER BY a.dtData DESC, b.Fantasia';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 23323) // Notificacoes
    {
        dso.SQL = 'SELECT a.*, b.Fantasia AS Usuario ' +
                  'FROM Publicacoes_Notificacoes a WITH(NOLOCK) ' +
                  'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.UsuarioID ' +
                  'WHERE ' + sFiltro + ' a.PublicacaoID = ' + idToFind + ' ' +
                 'ORDER BY a.dtData DESC, b.Fantasia';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 23324) // Leituras
    {
        dso.SQL = 'SELECT a.*, b.Fantasia AS Usuario, b.PessoaID AS UsuarioID, e.Fantasia AS Revenda, e.PessoaID AS RevendaID ' +
            'FROM Publicacoes_Leituras a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.UsuarioID ' +
            'INNER JOIN RelacoesPessoas_Contatos c WITH(NOLOCK) ON c.ContatoID = b.PessoaID ' +
            'INNER JOIN RelacoesPessoas d WITH(NOLOCK) ON d.RelacaoID = c.RelacaoID ' +
            'INNER JOIN Pessoas e WITH(NOLOCK) ON e.PessoaID = d.SujeitoID ' +
            'WHERE ' + sFiltro + ' a.PublicacaoID = ' + idToFind + ' AND d.ObjetoID = ' + nEmpresaData[0] +
            ' ORDER BY a.dtData DESC, b.Fantasia';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 23325) // Curtidas
    {
        dso.SQL = 'SELECT a.*, b.Fantasia AS Usuario, c.Comentario, b.PessoaID AS UsuarioID, f.Fantasia AS Revenda, f.PessoaID AS RevendaID ' +
            'FROM Publicacoes_Curtidas a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.UsuarioID ' +
            'LEFT JOIN Publicacoes_Comentarios c WITH(NOLOCK) ON c.PubComentarioID = a.PubComentarioID ' +
            'INNER JOIN RelacoesPessoas_Contatos d WITH(NOLOCK) ON d.ContatoID = b.PessoaID ' +
            'INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON e.RelacaoID = d.RelacaoID ' +
            'INNER JOIN Pessoas f WITH(NOLOCK) ON f.PessoaID = e.SujeitoID ' +
            'WHERE ' + sFiltro + ' a.PublicacaoID = ' + idToFind + ' AND e.ObjetoID = ' + nEmpresaData[0] +
            ' ORDER BY a.dtData DESC, b.Fantasia';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 23326) // Coment�rios
    {
        dso.SQL = 'SELECT a.*, b.Fantasia AS Usuario, c.Comentario AS Comentario2, d.Fantasia AS Moderador, b.PessoaID AS UsuarioID, g.Fantasia AS Revenda, g.PessoaID AS RevendaID ' +
            'FROM Publicacoes_Comentarios a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.UsuarioID ' +
            'LEFT JOIN Publicacoes_Comentarios c WITH(NOLOCK) ON c.PubComentarioID2 = a.PubComentarioID ' +
            'LEFT JOIN Pessoas d WITH(NOLOCK) ON d.PessoaID = a.ModeradorID ' +
            'INNER JOIN RelacoesPessoas_Contatos e WITH(NOLOCK) ON e.ContatoID = b.PessoaID ' +
            'INNER JOIN RelacoesPessoas f WITH(NOLOCK) ON f.RelacaoID = e.RelacaoID ' +
            'INNER JOIN Pessoas g WITH(NOLOCK) ON g.PessoaID = f.SujeitoID ' +
            'WHERE ' + sFiltro + ' a.PublicacaoID = ' + idToFind + ' AND f.ObjetoID = ' + nEmpresaData[0] +
            ' ORDER BY a.dtData DESC, b.Fantasia';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 23327) // Compartilhamentos
    {
        dso.SQL = 'SELECT a.*, b.Fantasia AS Usuario, b.PessoaID AS UsuarioID, e.Fantasia AS Revenda, e.PessoaID AS RevendaID ' +
            'FROM Publicacoes_Compartilhamentos a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.UsuarioID ' +
            'INNER JOIN RelacoesPessoas_Contatos c WITH(NOLOCK) ON c.ContatoID = b.PessoaID ' +
            'INNER JOIN RelacoesPessoas d WITH(NOLOCK) ON d.RelacaoID = c.RelacaoID ' +
            'INNER JOIN Pessoas e WITH(NOLOCK) ON e.PessoaID = d.SujeitoID ' +
            'WHERE ' + sFiltro + ' a.PublicacaoID = ' + idToFind + ' AND d.ObjetoID = ' + nEmpresaData[0] +
            ' ORDER BY a.dtData DESC, b.Fantasia';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
	var nEmpresaData = getCurrEmpresaData();
    setConnection(dso);
    var nEmpresaAlternativaID = 0;
    
    if (nEmpresaData[0] == 2)
		nEmpresaAlternativaID = 4;
	else
		nEmpresaAlternativaID = nEmpresaData[0];
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    else if (pastaID == 23328) // Marcas
    {
        if (dso.id == 'dsoCmb01Grid_01')
        {
            dso.SQL = 'SELECT NULL AS ConceitoID, SPACE(0) AS Conceito ' +
                      'UNION ' +
                      'SELECT ConceitoID, Conceito ' +
                          'FROM Conceitos ' +
                          'WHERE ((EstadoID = 2) AND (TipoConceitoID = 304)) ' +
                          'ORDER BY Conceito';
        }
    }
    else if (pastaID == 23321)
    {
        if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemMasculino ' +
                     'FROM Publicacoes_Destinos a WITH(NOLOCK) ' +
                     'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON b.ItemID = a.TipoDestinoID ' +
                     'WHERE (a.PublicacaoID =' + nRegistroID + ')';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(23328);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 23328); //Marcas

    vPastaName = window.top.getPastaName(23321);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 23321); //Destinos

    vPastaName = window.top.getPastaName(23322);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 23322); //Ocultas

    vPastaName = window.top.getPastaName(23323);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 23323); //Notifica��es

    vPastaName = window.top.getPastaName(23324);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 23324); //Leituras

    vPastaName = window.top.getPastaName(23325);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 23325); //Curtidas

    vPastaName = window.top.getPastaName(23326);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 23326); //Coment�rios

    vPastaName = window.top.getPastaName(23327);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 23327); //Compartilhamentos

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
    if (folderID == 23328) // Marcas
    {
        currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['PublicacaoID', registroID], []);

        if (currLine < 0)
        {
            if (fg.disabled == false)
            {
                try
                {
                    fg.Editable = true;
                    window.focus();
                    fg.focus();
                }
                catch (e)
                {
                    ;
                }
            }

            return false;
        }
    }

    if (folderID == 23321) // Destinos
	{
		currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1], ['PublicacaoID', registroID], [4]);

		if (currLine < 0)
		{
			if (fg.disabled == false)
			{
				try
				{
					fg.Editable = true;
					window.focus();
					fg.focus();
				}	
				catch(e)
				{
					;
				}
			}    
	    
			return false;
		}    
    }
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 23328) // Marcas
    {
        headerGrid(fg, ['Marca',
                       'PubMarcaID'], [1]);

        fillGridMask(fg, currDSO, ['MarcaID',
								 'PubMarcaID'],
                                 ['', ''],
                                 ['', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

    else if (folderID == 23321) // Destinos
    {
        headerGrid(fg, ['Tipo Destino',
                       'DestinoID',
                       'Notifica��o',
                       'Auto',
                       'TipoDestinoID',
                       'PubDestinoID'], [4, 5]);

        fillGridMask(fg, currDSO, ['^TipoDestinoID^dso01GridLkp^ItemID^ItemMasculino*',
								 'DestinoID*',
                                 'Notificacao',
                                 'Automatico*',
                                 'TipoDestinoID',
								 'PubDestinoID'],
                                 ['','','','', '', ''],
                                 ['','','','', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 23322) // Ocultas
    {
        headerGrid(fg, ['Data',
                       'Usu�rio',
                       'PubOcultaID'], [2]);

        fillGridMask(fg, currDSO, ['dtData*',
								 'Usuario*',
								 'PubOcultaID*'],
                                 ['', '', ''],
                                 ['', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 23323) // Notifica��es
    {
        headerGrid(fg, ['Data',
                       'Usu�rio',
                       'PubNotificacaoID'], [2]);

        fillGridMask(fg, currDSO, ['dtData*',
								 'Usuario*',
								 'PubNotificacaoID*'],
                                 ['', '', ''],
                                 ['', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 23324) // Leituras
    {
        headerGrid(fg, ['Data',    
                       'Usu�rio',
                       'Usu�rioID',
                       'Revenda',
                       'RevendaID',
                       'PubLeituraID'], [5]);

        fillGridMask(fg, currDSO, ['dtData*',
								 'Usuario*',
                                 'UsuarioID*',
                                 'Revenda*',
                                 'RevendaID*',
								 'PubLeituraID*'],
                                 ['', '', '', '', '', ''],
                                 [dTFormat + ' hh:mm:ss', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 23325) // Curtidas
    {
        headerGrid(fg, ['Data',
                       'Usu�rio',
                       'Usu�rioID',
                       'Revenda',
                       'RevendaID',
                       'Comentario',
                       'PubCurtidaID'], [6]);

        fillGridMask(fg, currDSO, ['dtData*',
								 'Usuario*',
                                 'UsuarioID*',
                                 'Revenda*',
                                 'RevendaID*',
                                 'Comentario*',
								 'PubCurtidaID*'],
                                 ['', '', '', '', '', '', ''],
                                 [dTFormat + ' hh:mm:ss', '', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 23326) // Coment�rios
    {
        headerGrid(fg, ['Data',
                       'Usu�rio',
                       'Usu�rioID',
                       'Revenda',
                       'RevendaID',
                       'Comentario',
                       'PubComentarioID'], [6]);

        fillGridMask(fg, currDSO, ['dtData*',
								 'Usuario*',
                                 'UsuarioID*',
                                 'Revenda*',
                                 'RevendaID*',
                                 'Comentario*',
								 'PubComentarioID*'],
                                 ['', '', '', '', '', '', ''],
                                 [dTFormat + ' hh:mm:ss', '', '', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 23327) // Compartilhamentos
    {
        headerGrid(fg, ['Data',
                       'Usu�rio',
                       'Usu�rioID',
                       'Revenda',
                       'RevendaID',
                       'PubCompartilhamentoID'], [5]);

        fillGridMask(fg, currDSO, ['dtData*',
								 'Usuario*',
                                 'UsuarioID*',
                                 'Revenda*',
                                 'RevendaID*',
								 'PubCompartilhamentoID*'],
                                 ['', '', '', '', '', ''],
                                 [dTFormat + ' hh:mm:ss', '', '', '', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
}
