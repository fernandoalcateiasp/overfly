/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form publicacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
//  Dados da listagem da pesquisa 
var dsoListData01 = new CDatatransport("dsoListData01");
//  Dados dos combos de contexto e filtros 
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
//  Dados dos proprietarios para o pesqlist .URL 
var dsoPropsPL = new CDatatransport("dsoPropsPL");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Tipo', 'PublicadoID', 'T�tulo', 'Recorr�ncia', 'In�cio', 'Fim', 'Data', 'Not', 'IO', 'IR', 'Observa��o');

    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui
    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY HH:MM")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY HH:MM")
        dTFormat = 'mm/dd/yyyy hh:mm';
    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', dTFormat, dTFormat, dTFormat, '', '', '', '');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Imagem', 'Criar Publica��es', 'Gerenciar Publica��es', 'Moderar Coment�rios']);
 
	setupEspecBtnsControlBar('sup', 'DHHDHHH');

	glb_aCelHint = [[0, 9, 'Envia notifica��es para os clientes?'],
                    [0, 10, 'Ignorar publica��es ocultas?'],
                    [0, 11, 'Ignorar repercuss�o da publica��o e n�o atualizar a data da publica��o?']];


	if ((fg.Cols >= 4) && (fg.Rows > 1))
	    fg.FrozenCols = 4;

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // usuario clicou botao documentos
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
    else if (btnClicked == 5)
        openModalCriarPublicacoes();
    else if (btnClicked == 6)
        openModalGerenciarPublicacoes();
    else if (btnClicked == 7)
        openModalModerarComentarios();
}

function openModalCriarPublicacoes() {
    
    var htmlPath;
    var nWidth = 760;
    var nHeight = 560;


    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                              ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));


    if ((nA1 == 1) && (nA2 == 1)) {
        // carregar a modal
        htmlPath = SYS_ASPURLROOT + '/modmarketing/subcampanhas/publicacoes/modalpages/modalcriarpublicacoes.asp';
        showModalWin(htmlPath, new Array(nWidth, nHeight));
    }
}


function openModalGerenciarPublicacoes() {

    var htmlPath;
    var strPars = new String();
    var nWidth = 970;
    var nHeight = 547;

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modmarketing/subcampanhas/publicacoes/modalpages/modalgerenciarpublicacoes.asp';
        showModalWin(htmlPath, new Array(nWidth, nHeight));
}

function openModalModerarComentarios() {

    var htmlPath;
    var strPars = new String();
    var nWidth = 650;
    var nHeight = 547;

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modmarketing/subcampanhas/publicacoes/modalpages/modalmoderarcomentarios.asp';
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{

    // Modal de documentos
    if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCRIARPUBLICACOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            if (param2 == 'NOREG')
                lockInterface(false);

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERENCIARPUBLICACOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            if (param2 == 'NOREG')
                lockInterface(false);

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALMODERARCOMENTARIOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            if (param2 == 'NOREG')
                lockInterface(false);

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}
