﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using Overfly3.systemEx.serverside;
using WSData;

namespace Overfly3.modmarketing.subcampanhas.publicacoes.serverside
{
    public partial class gerarpublicacoes : System.Web.UI.OverflyPage
    {
        string Resultado = "";

        protected static Integer ZERO = new Integer(0);
        protected static Integer[] EMPTY = new Integer[0];
        protected static string VAZIO = "";

        protected Integer DataLen = ZERO;
        protected Integer nDataLen
        {
            get { return DataLen; }
            set { if (value != null) DataLen = value; }
        }

        private Integer EmpresaID;
        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = (value != null ? value : ZERO); }
        }

        private Integer TipoPublicacaoID;
        public Integer nTipoPublicacaoID
        {
            get { return TipoPublicacaoID; }
            set { TipoPublicacaoID = (value != null ? value : ZERO); }
        }

        private Integer[] PublicadoID ;
        public Integer[] nPublicadoID
        {
            get { return PublicadoID; }
            set { PublicadoID = value; }
        }

        private Integer Recorrencia;
        public Integer nRecorrencia
        {
            get { return Recorrencia; }
            set { Recorrencia = value; }
        }

        private string DInicio ;
        public string dInicio
        {
            get { return DInicio; }
            set { DInicio = value; }
        }

        private string DFim ;
        public string dFim
        {
            get { return DFim; }
            set { DFim = value; }
        }

        private string Notificacao ;
        public string bNotificacao
        {
            get { return Notificacao; }
            set { Notificacao = value; }
        }

        private string IgnorarOcultos ;
        public string bIgnorarOcultos
        {
            get { return IgnorarOcultos; }
            set { IgnorarOcultos = value; }
        }

        private string IgnorarRepercucao ;

        public string bIgnorarRepercucao
        {
            get { return IgnorarRepercucao; }
            set { IgnorarRepercucao = value; }
        }

        private Integer UsuarioID ;
        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = (value != null ? value : ZERO); }
        }
      
      
        protected override void PageLoad(object sender, EventArgs e)
        {

            publicacaogerador();
 
           WriteResultXML(
                DataInterfaceObj.getRemoteData("select '" + Resultado + "' as Resultado "));
        }

        private void publicacaogerador()
        {
            for (int i = 0; i < DataLen.intValue(); i++)
            {
                ProcedureParameters[] procParams = new ProcedureParameters[11];

                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                EmpresaID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(EmpresaID));

                procParams[1] = new ProcedureParameters(
                    "@TipoPublicacaoID",
                    System.Data.SqlDbType.Int,
                TipoPublicacaoID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(TipoPublicacaoID.ToString()));

                procParams[2] = new ProcedureParameters(
                   "@PublicadoID",
                   System.Data.SqlDbType.Int,
               PublicadoID[i].intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(PublicadoID[i].ToString()));

                procParams[3] = new ProcedureParameters(
                  "@Recorrencia",
                  System.Data.SqlDbType.Int,
               Recorrencia.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(Recorrencia.ToString()));

                procParams[4] = new ProcedureParameters(
                    "@dtInicio",
                    System.Data.SqlDbType.DateTime,
                dInicio == string.Empty ? DBNull.Value : (Object)dInicio.ToString());

                procParams[5] = new ProcedureParameters(
                   "@dtFim",
                   System.Data.SqlDbType.DateTime,
               dFim == string.Empty ? DBNull.Value : (Object)dFim.ToString());

                procParams[6] = new ProcedureParameters(
                    "@Notificacao",
                    System.Data.SqlDbType.Bit,
                Notificacao.ToString() == "0" ? false : true);

                procParams[7] = new ProcedureParameters(
                    "@IgnorarOcultos",
                    System.Data.SqlDbType.Bit,
                IgnorarOcultos.ToString() == "0" ? false : true);

                procParams[8] = new ProcedureParameters(
                    "@IgnorarRepercucao",
                    System.Data.SqlDbType.Bit,
                IgnorarRepercucao.ToString() == "0" ? false : true);

                procParams[9] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                UsuarioID.ToString() == null ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

                procParams[10] = new ProcedureParameters(
                   "@Resultado",
                   System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.InputOutput);
                procParams[10].Length = 8000;

                DataInterfaceObj.execNonQueryProcedure("sp_Publicacao_Gerador", procParams);

                if (procParams[10].Data != DBNull.Value)
                    Resultado += procParams[10].Data.ToString() + "\r \n";
            }
        }
    }
}