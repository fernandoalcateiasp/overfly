﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using Overfly3.systemEx.serverside;
using WSData;

namespace Overfly3.modmarketing.subcampanhas.publicacoes.serverside
{
    public partial class moderarcomentarios : System.Web.UI.OverflyPage
    {
        string Resultado = "";

        protected static Integer ZERO = new Integer(0);
        protected static Integer[] EMPTY = new Integer[0];
        protected static string VAZIO = "";

        protected Integer DataLen = ZERO;
        protected Integer nDataLen
        {
            get { return DataLen; }
            set { if (value != null) DataLen = value; }
        }

        private Integer[] ID;
        public Integer[] nID
        {
            get { return ID; }
            set { ID = (value != null ? value : EMPTY); }
        }

        private Integer UsuarioID ;
        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = (value != null ? value : ZERO); }
        }

        private string[] Publica;
        public string[] bPublica
        {
            get { return Publica; }
            set { Publica = value ; }
        }

        protected string SQL
        {
            get
            {
                string sql = "";
                int i;

                for (i = 0; i < DataLen.intValue(); i++)
                {
                    sql += " UPDATE Publicacoes_Comentarios SET ModeradorID = " + UsuarioID + ", Publica = " + Publica[i] + " WHERE PubComentarioID = " + ID[i];
                }
                return sql;
            }
        }
      
      
        protected override void PageLoad(object sender, EventArgs e)
        {

         int fldresp = DataInterfaceObj.ExecuteSQLCommand(SQL);

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + fldresp + " as fldresp"
            ));
        }
    }
}