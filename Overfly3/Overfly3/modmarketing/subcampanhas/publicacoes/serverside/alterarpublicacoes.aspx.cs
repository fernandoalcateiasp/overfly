﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using Overfly3.systemEx.serverside;
using WSData;

namespace Overfly3.modmarketing.subcampanhas.publicacoes.serverside
{
    public partial class alterarpublicacoes : System.Web.UI.OverflyPage
    {
        string Resultado = "";

        protected static Integer ZERO = new Integer(0);
        protected static Integer[] EMPTY = new Integer[0];
        protected static string VAZIO = "";

        protected Integer DataLen = ZERO;
        protected Integer nDataLen
        {
            get { return DataLen; }
            set { if (value != null) DataLen = value; }
        }

        private Integer[] PublicacaoID;
        public Integer[] nID
        {
            get { return PublicacaoID; }
            set { PublicacaoID = (value != null ? value : EMPTY); }
        }

        private string[] Titulo ;
        public string[] sTitulo
        {
            get { return Titulo; }
            set { Titulo = value; }
        }

        private Integer[] Recorrencia;
        public Integer[] nRecorrencia
        {
            get { return Recorrencia; }
            set { Recorrencia = (value != null ? value : EMPTY); }
        }

        private string[] DInicio;
        public string[] dInicio
        {
            get { return DInicio; }
            set { DInicio = value; }
        }

        private string[] DFim ;
        public string[] dFim
        {
            get { return DFim; }
            set { DFim = value; }
        }

        private string[] Notificacao ;
        public string[] bNotificacao
        {
            get { return Notificacao; }
            set { Notificacao = value; }
        }

        private string[] IgnorarOcultos ;
        public string[] bIgnorarOcultos
        {
            get { return IgnorarOcultos; }
            set { IgnorarOcultos = value; }
        }

        private string[] IgnorarRepercucao ;
        public string[]bIgnorarRepercucao
        {
            get { return IgnorarRepercucao; }
            set { IgnorarRepercucao = value; }
        }

        private string[] Observacao;
        public string[] sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }

        private string[] Observacoes;
        public string[] sObservacoes
        {
            get { return Observacoes; }
            set { Observacoes = value ; }
        }

        private Integer UsuarioID ;
        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = (value != null ? value : ZERO); }
        }

        protected string SQL
        {
            get
            {
                string sql = "";
                int i;

                for (i = 0; i < DataLen.intValue(); i++)
                {
                    sql += " UPDATE Publicacoes SET Titulo = '" + Titulo[i].Replace("***virgula***", ",") + "', Recorrencia = '" + Recorrencia[i] + "', dtInicio = " + ((DInicio[i] == "") ? "NULL" : "'" + DInicio[i] + "'") + ", " +
                         "dtFim = " + ((DFim[i] == "") ? "NULL" : "'" + DFim[i] + "'") + ", Notificacao = '" + Notificacao[i] + "', IgnorarOcultos = '" + IgnorarOcultos[i] + "', " +
                         "IgnorarRepercucao = '" + IgnorarRepercucao[i] + "', Observacao = '" + Observacao[i].Replace("***virgula***", ",") + "', Observacoes = '" + Observacoes[i].Replace("***virgula***", ",") + "', " +
                    "UsuarioID = " + UsuarioID + " WHERE PublicacaoID = " + PublicacaoID[i];
                }
                //((DescricaoComplementar[i] == "null") ? "NULL" : "'" + DescricaoComplementar[i] + "'") 
                return sql;
            }
        }
      
      
        protected override void PageLoad(object sender, EventArgs e)
        {

         int fldresp = DataInterfaceObj.ExecuteSQLCommand(SQL);

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " + fldresp + " as fldresp"
            ));
        }
    }
}