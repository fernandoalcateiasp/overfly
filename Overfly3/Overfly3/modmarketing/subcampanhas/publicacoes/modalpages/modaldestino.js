/********************************************************************
modalpessoa.js

Library javascript para o modalDestino.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport('dsoPesq');
var dsoTipoDestino = new CDatatransport('dsoTipoDestino');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modaldestinoBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if (document.getElementById('txtDestino').disabled == false)
        txtDestino.focus();

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Destino', 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divDestino
    elem = window.document.getElementById('divDestino');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
        
    }

    selTipoDestino.onchange = selTipoDestino_onchange;

    useBtnListar(true, divDestino);

    adjustElementsInForm([['lblTipoDestino', 'selTipoDestino', 12, 1, -10],
                          ['lblDestino', 'txtDestino', 34, 1]],
                          null,null,true);
                          
    with (btnListar)
    {
        style.top = parseInt(txtDestino.currentStyle.top, 10);
        style.left = parseInt(txtDestino.currentStyle.left, 10) +
                     parseInt(txtDestino.currentStyle.width, 10) + ELEM_GAP;
    }                          

        
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divDestino').style.top) + parseInt(document.getElementById('divDestino').style.height) + ELEM_GAP;
        // width = temp + 221;
        width = temp + 321;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);
    
    headerGrid(fg,['ID',
                   'Destino'], []);
    
    fg.Redraw = 2;
    //selTipoDestino = dsoSup01.recordset['TipoDestinoID'].value;
    txtDestino.onkeypress = txtDestino_onKeyPress;

    setConnection(dsoTipoDestino);
    dsoTipoDestino.SQL = "SELECT ItemID AS TipoDestinoID, ItemMasculino AS TipoDestino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ((EstadoID = 2) AND (TipoID = 312))";
    dsoTipoDestino.ondatasetcomplete = dsoTipoDestino_DSC;
    dsoTipoDestino.Refresh();

}

function selTipoDestino_onchange() {
    
    if ((selTipoDestino.value == 581) || (selTipoDestino.value == 582))
    {
        startPesq(txtDestino.value);
    }
    else {
        fg.Rows = 1;
        btnOK.disabled = true;
    }

    
}

function dsoTipoDestino_DSC() {

    clearComboEx(['selTipoDestino']);

    while (!dsoTipoDestino.recordset.EOF) {
        optionStr = dsoTipoDestino.recordset['TipoDestino'].value;
        optionValue = dsoTipoDestino.recordset['TipoDestinoID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selTipoDestino.add(oOption);
        dsoTipoDestino.recordset.MoveNext();
    }
}

function txtDestino_onKeyPress()
{
    if ( event.keyCode == 13 )
        btnListar_onclick();
}


function btnListar_onclick()
{
    txtDestino.value = trimStr(txtDestino.value);
    
    startPesq(txtDestino.value);
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', new Array(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ID')), selTipoDestino.value,
                selTipoDestino.options[selTipoDestino.selectedIndex].text));
    }

    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    
    //Altera��o: Remo��o da obrigatoriedade da pessoa e a rela��o da pessoa estar ativa, utilizado para devolu��o e rma LFS:05/05/2011
    
    var strSQL = '';


    if (selTipoDestino.value == 581)
	{
        strSQL = " SELECT ItemID AS ID, ItemMasculino AS Segmento " +
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                    "WHERE ((EstadoID = 2) AND (TipoID = 313)) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), ItemID) + SPACE(1) + ItemMasculino) LIKE '%" + strPesquisa + "%')";

        strSQL += " ORDER BY ItemMasculino ";
	}
    else if (selTipoDestino.value == 582)
	{
        strSQL = " SELECT ItemID AS ID, ItemMasculino AS Classificacao " +
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                   "WHERE ((EstadoID = 2) AND (TipoID = 13) AND (Filtro LIKE '%<CLI>%')) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), ItemID) + SPACE(1) + ItemMasculino) LIKE '%" + strPesquisa + "%')";

        strSQL += " ORDER BY ItemMasculino ";

    }
    else if (selTipoDestino.value == 583)
    {
        strSQL = " SELECT DISTINCT TOP 200 a.PessoaID AS ID, a.Nome AS Nome " +
                  "FROM Pessoas a WITH(NOLOCK) " +
                  "INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON b.ItemID = a.ClassificacaoID " +
                  "WHERE ((a.EstadoID = 2) AND (a.TipoPessoaID = 52) AND (b.Filtro LIKE '%<CLI>%')) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.PessoaID) + SPACE(1) + a.Nome) LIKE '%" + strPesquisa + "%')";

        strSQL += " ORDER BY a.Nome ";
    }
    else if (selTipoDestino.value == 584) // Localiza��o
    {
        strSQL = " SELECT DISTINCT TOP 200 a.LocalidadeID AS ID, a.Localidade AS Nome " +
                  "FROM Localidades a WITH(NOLOCK) " +
                  "WHERE ((a.EstadoID = 2) AND (a.TipoLocalidadeID IN (203, 204, 205))) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.LocalidadeID) + SPACE(1) + a.Localidade + SPACE(1) + a.CodigoLocalidade2) LIKE '%" + strPesquisa + "%')";

        strSQL += " ORDER BY Nome ";

    }

    setConnection(dsoPesq);
    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    var nTipoDestino = selTipoDestino.value;

    if (nTipoDestino == 581) {
        var aTitleCols = new Array('ID', 'Segmento');
        var aFieldsCols = new Array('ID', 'Segmento');
        var aMaskCols = new Array('', '');
    }
    else if (nTipoDestino == 582) {
        var aTitleCols = new Array('ID', 'Classificacao');
        var aFieldsCols = new Array('ID', 'Classificacao');
        var aMaskCols = new Array('', '');
    }
    else if (nTipoDestino == 583) {
        var aTitleCols = new Array('ID', 'Nome');
        var aFieldsCols = new Array('ID', 'Nome');
        var aMaskCols = new Array('', '');
    }
    else if (nTipoDestino == 584) {
        var aTitleCols = new Array('ID', 'Nome');
        var aFieldsCols = new Array('ID', 'Nome');
        var aMaskCols = new Array('', '');
    }

    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg, aTitleCols, []);
    
    fillGridMask(fg, dsoPesq, aFieldsCols, aMaskCols);
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
    {
        btnOK.disabled = true;    
        txtDestino.focus();
    }    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
    
}

function fg_KeyPress(KeyAscii)
{
    // Enter
    if ( (KeyAscii == 13) && (fg.Row > 0) )
    {
        btn_onclick(btnOK);
    }
}
