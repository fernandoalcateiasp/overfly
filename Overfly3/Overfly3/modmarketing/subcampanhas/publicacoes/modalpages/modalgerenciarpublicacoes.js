/********************************************************************
modalgerenciarpublicacoes.js

Library javascript para o modalgerenciarpublicacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport("dsoPesq");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoperfilUsuario = new CDatatransport("dsoperfilUsuario");
var dsoTipoPublicacao = new CDatatransport('dsoTipoPublicacao');
var dsoEstados = new CDatatransport("dsoEstados");
var dsoRecorrencia = new CDatatransport("dsoRecorrencia");

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_DescricaoAutomatica;
var glb_sTxtFiltro;
var glb_nCurrUserID;
var glb_empresa;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {

    glb_nCurrUserID = getCurrUserID();

    glb_empresa = getCurrEmpresaData();

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();
    btnCanc.style.visibility = 'hidden';
    btnOK.style.visibility = 'hidden';
    // ajusta o body do html
    var elem = document.getElementById('modalgerenciarpublicacoesBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
        btnGravar.disabled = false;
    }
    else {
        btnGravar.disabled = true;
        txtBusca.focus();
    }

    setConnection(dsoRecorrencia);

    dsoRecorrencia.SQL = " SELECT '' AS Recorrencia UNION ALL  SELECT '1' UNION ALL  SELECT '2' UNION ALL  SELECT '3' UNION ALL  SELECT '6' UNION ALL  SELECT '12' UNION ALL SELECT '24' UNION ALL  SELECT '48' UNION ALL SELECT '168' ";

    dsoRecorrencia.ondatasetcomplete = dsoRecorrencia_DSC;
    dsoRecorrencia.Refresh();


    // coloca foco no campo apropriado
    if (document.getElementById('txtBusca').disabled == false)
        txtBusca.focus();
}

function dsoRecorrencia_DSC() {
        return null;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    
    // texto da secao01
    secText('Gerenciar Publica��es', 1);

    // ajusta elementos da janela
    var elem;
    var temp;
    
    elem = window.document.getElementById('divControls');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP / 2;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
    }


    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblTipoPublicacaoID', 'selTipoPublicacaoID', 15, 1],
                          ['lblBusca', 'txtBusca', 70, 1],
                          ['btnListar', 'btn', 50, 1],
                          ['btnGravar', 'btn', 50, 1],
                          ['btnAtivar', 'btn', 50, 1]], null, null, true);


    //@@ *** Div Observa��es ***/
    adjustElementsInForm(['lblObservacoes', 'txtObservacoes', 10, 2], null, null, true);

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {

        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP / 2;
        top = parseInt(document.getElementById('divControls').style.top) + parseInt(document.getElementById('divControls').style.height) + ELEM_GAP;
        width = 945;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 225;
    }
    
    elem = document.getElementById('fg');
    with (elem.style) {

        left = ELEM_GAP  / 2;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);

    }

    elem = document.getElementById('txtObservacoes');
    with (elem.style) {

        left = ELEM_GAP - 2 ;
        top = parseInt(document.getElementById('fg').style.height) + parseInt(document.getElementById('divControls').style.height) + (ELEM_GAP * 4);
        width = parseInt(document.getElementById('fg').style.width) + parseInt(document.getElementById('fg').style.left) -2;
        height = parseInt(document.getElementById('fg').style.height) / 4;

    }

    txtBusca.onkeypress = txtBusca_onKeyPress;
    selTipoPublicacaoID.onchange = selTipoPublicacaoID_onchange;
    txtObservacoes.onchange = txtObservacoes_onchange;

    startGridInterface(fg);

    headerGrid(fg, [''], []);

    //fg.ColWidth(getColIndexByColKey(fg, 'NotaFiscalID')) = 1430;
    fg.Redraw = 2;

    setConnection(dsoTipoPublicacao);
    dsoTipoPublicacao.SQL = "SELECT ItemID AS TipoPublicacaoID, ItemMasculino AS TipoPublicacao FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ((EstadoID = 2) AND (TipoID = 311))";
    dsoTipoPublicacao.ondatasetcomplete = dsoTipoPublicacao_DSC;
    dsoTipoPublicacao.Refresh();

}

function dsoTipoPublicacao_DSC() {

    clearComboEx(['selTipoPublicacaoID']);

    var oOption = document.createElement("OPTION");
        oOption.value = 0;
        oOption.text = "";
        selTipoPublicacaoID.add(oOption);

    while (!dsoTipoPublicacao.recordset.EOF) {
        optionStr = dsoTipoPublicacao.recordset['TipoPublicacao'].value;
        optionValue = dsoTipoPublicacao.recordset['TipoPublicacaoID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selTipoPublicacaoID.add(oOption);
        dsoTipoPublicacao.recordset.MoveNext();

        selTipoPublicacaoID.selectedIndex = 1;
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    lockControlsInModalWin(true);

    if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);
    else if (ctl.id == 'btnListar') {
        txtBusca.value = trimStr(txtBusca.value);
        startPesq(txtBusca.value);
    }
    else if (ctl.id == 'btnGravar') {
        saveDataInGrid();
    }
    else if (ctl.id == 'btnAtivar') {
        ativarPublicacoes();
    }

}

function startPesq(strPesquisa) {

    lockControlsInModalWin(true);

    //Altera��o: Remo��o da obrigatoriedade da pessoa e a rela��o da pessoa estar ativa, utilizado para devolu��o e rma LFS:05/05/2011

    var strSQL = '';
    var PaisID = getCurrEmpresaData()[1];

    //Produto

    strSQL = "SELECT DISTINCT TOP 50 a.PublicacaoID AS ID, b.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK,  " +
                " c.ItemMasculino AS Tipo, a.PublicadoID, a.Titulo AS Titulo, a.Recorrencia, CONVERT(VARCHAR, dtInicio, 103) + SPACE(1) + CONVERT(VARCHAR, dtInicio,108) AS Inicio, " +
                " CONVERT(VARCHAR, dtFim, 103) + SPACE(1) + CONVERT(VARCHAR, dtFim,108) AS Fim, CONVERT(BIT, a.Notificacao) AS Notificacao, CONVERT(BIT, a.IgnorarOcultos) AS IgnorarOcultos, "+
                " CONVERT(BIT, a.IgnorarRepercucao) AS IgnorarRepercucao, a.Observacao, a.Observacoes " +
                 "FROM Publicacoes a WITH(NOLOCK) " +
                    "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) " +
                    "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON c.ItemID = a.TipoPublicacaoID " + 
                  "WHERE ((a.EstadoID = 1) AND (dbo.fn_Pessoa_Pais(a.EmpresaID, " + PaisID + ") = 1)) ";

        if (strPesquisa != '') {
            strSQL += " AND (a.Titulo LIKE ISNULL('%" + strPesquisa + "%', SPACE(0))) ";
        }

        if (selTipoPublicacaoID.value != 0) {
            strSQL += " AND (a.TipoPublicacaoID = " + selTipoPublicacaoID.value + ") ";
        }


        strSQL += " ORDER BY  c.ItemMasculino, a.Titulo ";
    

    setConnection(dsoPesq);
    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();

   
}

function dsopesq_DSC() {

    var aTitleCols = new Array('ID', 'Est', 'OK', 'Tipo', 'PublicadoID', 'T�tulo', 'Recorr�ncia', 'Inicio', 'Fim', 'Not', 'IO', 'IR',
                        'Observa��o', 'Observa��es');
    var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Tipo*', 'PublicadoID*', 'Titulo', 'Recorrencia', 'Inicio', 'Fim', 'Notificacao',
                    'IgnorarOcultos', 'IgnorarRepercucao', 'Observacao', 'Observacoes');

    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY HH:MM")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY HH:MM")
        dTFormat = 'mm/dd/yyyy hh:mm';

    var aMaskCols = new Array('', '', '', '', '', '', '', '99/99/9999 99:99:99', '99/99/9999 99:99:99', '', '', '', '', '');
    var aMaskVCols = new Array('', '', '', '', '', '', '', dTFormat, dTFormat, '', '', '', '', '');
        

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '9';
    fg.FrozenCols = 0;

    headerGrid(fg, aTitleCols, [13]);

    glb_aCelHint = [[0, 9, 'Envia notifica��es para os clientes?'],
        [0, 10, 'Ignorar publica��es ocultas?'],
        [0, 11, 'Ignorar repercuss�o da publica��o e n�o atualizar a data da publica��o?']];

    fillGridMask(fg, dsoPesq, aFieldsCols, aMaskCols, aMaskVCols);

    fg.FrozenCols = 1;

    fg.Redraw = 0;

    //Alinhas as colunas do grid
    alignColsInGrid(fg);

    insertcomboData(fg, getColIndexByColKey(fg, 'Recorrencia'), dsoRecorrencia, 'Recorrencia', 'Recorrencia');

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.ExplorerBar = 5;


    if (fg.Rows > 1)
        fg.FrozenCols = 5;

    lockControlsInModalWin(false);
    fg.Editable = true;

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
        btnGravar.disabled = false;
    }
    else {
        btnGravar.disabled = true;
        txtBusca.focus();
    }

    fg.Redraw = 2;
}

//Apenas perfil de desenvolvedor, administrador e homologa��o poder�o alterar alguns dados do coneito em A.
function perfilUsuario() {
    setConnection(dsoperfilUsuario);

    dsoperfilUsuario.SQL = 'SELECT COUNT(DISTINCT a.PerfilID) AS PermiteSuporte, ' +
                                    '(SELECT COUNT(DISTINCT aa.PerfilID) ' +
							            'FROM RelacoesPesRec_Perfis aa WITH(NOLOCK) ' +
									        'INNER JOIN RelacoesPesRec bb WITH(NOLOCK) ON (bb.RelacaoID = aa.RelacaoID) ' +
		                                'WHERE (bb.objetoID = 999 AND bb.EstadoID = 2 AND  bb.SujeitoID =  ' + glb_nCurrUserID + ' AND aa.PerfilID IN (525, 619)))  AS PermiteHomologacao ' +
	                            'FROM RelacoesPesRec_Perfis a WITH(NOLOCK) ' +
		                            'INNER JOIN RelacoesPesRec b ON (b.RelacaoID = a.RelacaoID) ' +
	                            'WHERE (b.objetoID = 999 AND b.EstadoID = 2 AND b.SujeitoID =  ' + glb_nCurrUserID + '  AND a.PerfilID IN (500, 501)) ';

    dsoperfilUsuario.ondatasetcomplete = perfilUsuario_DSC;
    dsoperfilUsuario.Refresh();
}

function perfilUsuario_DSC() {
    glb_PermiteSuporte = dsoperfilUsuario.recordset['PermiteSuporte'].value;
    glb_PermiteHomologacao = dsoperfilUsuario.recordset['PermiteHomologacao'].value;

    return null;
}

function btnListar_onclick() {
    txtBusca.value = trimStr(txtBusca.value);

    if (btnListar.disabled)
        return;

    startPesq(txtBusca.value);
}
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var sDtInicio = '';
    var sDtFim = '';

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    lockControlsInModalWin(true);

    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + encodeURIComponent(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?nUsuarioID=' + encodeURIComponent(glb_nCurrUserID);
            }

            sDtInicio = normalizeDate_DateTime(getCellValueByColKey(fg, 'Inicio', i), true);
            sDtInicio = (sDtInicio == null ? '' : sDtInicio);

            sDtFim = normalizeDate_DateTime(getCellValueByColKey(fg, 'Fim', i), true);
            sDtFim = (sDtFim == null ? '' : sDtFim);

            //Alterado do encodeURIComponent pelo escape pois o encode n�o estava corrigindo caracteres especiais. FDS. 25/09/2018
            nDataLen++;
            strPars += '&nID=' + escape(getCellValueByColKey(fg, 'ID*', i));
            strPars += '&sTitulo=' + escape(getCellValueByColKey(fg, 'Titulo', i));
            strPars += '&nRecorrencia=' + escape(getCellValueByColKey(fg, 'Recorrencia', i));
            strPars += '&dInicio=' + escape(sDtInicio);
            strPars += '&dFim=' + escape(sDtFim);
            strPars += '&bNotificacao=' + escape(getCellValueByColKey(fg, 'Notificacao', i) == 0 ? 0 : 1);
            strPars += '&bIgnorarOcultos=' + escape(getCellValueByColKey(fg, 'IgnorarOcultos', i) == 0 ? 0 : 1);
            strPars += '&bIgnorarRepercucao=' + escape(getCellValueByColKey(fg, 'IgnorarRepercucao', i) == 0 ? 0 : 1);
            strPars += '&sObservacao=' + escape(getCellValueByColKey(fg, 'Observacao', i));
            strPars += '&sObservacoes=' + escape(getCellValueByColKey(fg,'Observacoes', i));
        }

    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars.replace(/%2C/g, "***virgula***") + '&nDataLen=' + encodeURIComponent(nDataLen);
    else {
        if (window.top.overflyGen.Alert('Selecione um registro') == 0)
            return null;
    }
    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/publicacoes/serverside/alterarpublicacoes.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            startPesq(txtBusca.value);
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_DSC() {

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
    
    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if (dsoGrava.recordset['fldresp'].value = 0) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['fldresp'].value) == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Publica��es alteradas.') == 0)
                return null;
        }

    }

}
function selTipoPublicacaoID_onchange() {

        fg.Rows = 1;
        btnGravar.disabled = true;

}

function js_fg_modalgerenciarpublicacoesDblClick(grid, Row, Col) {

    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'Ok')))
        return true;

    var i;
    var bFill = true;

    fg.Editable = false;
    fg.Redraw = 0;
    lockControlsInModalWin(true);


    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    fg.Editable = true;
    fg.Redraw = 2;
    window.focus();
    fg.focus();
}
function js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {

     txtObservacoes.value = fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'Observacoes'));
}

function js_ModalGerenciarPublicacoes_AfterEdit(Row, Col)
{
    if (Col != getColIndexByColKey(fg, 'OK'))
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
}

function txtObservacoes_onchange() {
    
    if (fg.Row > 0) {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacoes')) = txtObservacoes.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'OK')) = 1;
    }

}
function ativarPublicacoes() {

    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;


    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    lockControlsInModalWin(true);

    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + encodeURIComponent(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?nUsuarioID=' + encodeURIComponent(glb_nCurrUserID);
            }

            nDataLen++;
            strPars += '&nID=' + encodeURIComponent(getCellValueByColKey(fg, 'ID*', i));
        }

    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + encodeURIComponent(nDataLen);
    else {
        if (window.top.overflyGen.Alert('Selecione um registro') == 0)
            return null;
    }

    sendDataToServer_Ativar();
    
}


function sendDataToServer_Ativar() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/publicacoes/serverside/ativarpublicacoes.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_Ativar_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            startPesq(txtBusca.value);
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}
function sendDataToServer_Ativar_DSC() {

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer_Ativar()', INTERVAL_BATCH_SAVE, 'JavaScript');

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if (dsoGrava.recordset['fldresp'].value = 0) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['fldresp'].value) == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Publica��es ativadas.') == 0)
                return null;
        }

    }

}
function txtBusca_onKeyPress() {
    if (event.keyCode == 13)
        btnListar_onclick();
}
