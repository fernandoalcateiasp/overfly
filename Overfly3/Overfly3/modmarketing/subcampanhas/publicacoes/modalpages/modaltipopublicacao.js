/********************************************************************
modalpessoa.js

Library javascript para o modalpessoa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport('dsoPesq');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() 
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modaltipopublicacaoBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPublicado').disabled == false )
        txtPublicado.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Publicado', 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    modWidth = frameRect[2];
    modHeight = frameRect[3];
    
    // ajusta o divTipoPublicacao
    elem = window.document.getElementById('divTipoPublicacao');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = 0;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
        
    }

    useBtnListar(true, divTipoPublicacao);

    var nTipoPublicacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selTipoRegistroID.value');

    if (nTipoPublicacaoID == 579)
        btnListar_onclick();

    adjustElementsInForm([['lblPublicado', 'txtPublicado', 34, 1]], null, null, true);

    with (btnListar) {
        style.top = parseInt(txtPublicado.currentStyle.top, 10);
        style.left = parseInt(txtPublicado.currentStyle.left, 10) +
                     parseInt(txtPublicado.currentStyle.width, 10) + ELEM_GAP;
        style.width = 60;
    }

    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divTipoPublicacao').style.top) + parseInt(document.getElementById('divTipoPublicacao').style.height) + ELEM_GAP;
        width = modWidth - (ELEM_GAP * 2.5);
        height = modHeight - 135;    
      
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    txtPublicado.onkeypress = txtPublicado_onKeyPress;

}

function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', fg.TextMatrix(fg.Row, 0));
    }

    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);

}

function fg_KeyPress(KeyAscii) {
    // Enter
    if ((KeyAscii == 13) && (fg.Row > 0)) {
        btn_onclick(btnOK);
    }
}

function btnListar_onclick() {
    txtPublicado.value = trimStr(txtPublicado.value);

    if (btnListar.disabled)
        return;

    startPesq(txtPublicado.value);
}

function startPesq(strPesquisa)
{

    lockControlsInModalWin(true);

    var nTipoPublicacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selTipoRegistroID.value');
    var strSQL = '';
    var PaisID = getCurrEmpresaData()[1];
    var sPesquisa = txtPublicado.value;
    sPesquisa = trimStr(sPesquisa);

    //Produto
    if (nTipoPublicacaoID == 571) {
        strSQL = "SELECT DISTINCT TOP 500 b.ConceitoID AS ProdutoID, LEFT(dbo.fn_Produto_Descricao2(b.ConceitoID, NULL, 11), 100) AS Produto " +
                 "FROM RelacoesPesCon a WITH(NOLOCK) " +
                    "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) " +
                    "INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) " + 
                    "INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = b.MarcaID)  " +
                  "WHERE ((a.TipoRelacaoID = 61) AND (dbo.fn_Pessoa_Pais(a.SujeitoID, " + PaisID + ") = 1) AND (a.EstadoID IN (2, 12, 13, 14, 15)) AND (a.Publica = 1)) ";

        if (sPesquisa != '')
        {
            strSQL += " AND ((b.Conceito LIKE ISNULL('%" + sPesquisa + "%', SPACE(0))) " +
                            "OR (c.Conceito LIKE ISNULL('%" + sPesquisa + "%', SPACE(0))) " +
                            "OR (d.Conceito LIKE ISNULL('%" + sPesquisa + "%', SPACE(0))) " +
                            "OR (b.Modelo LIKE ISNULL('%" + sPesquisa + "%', SPACE(0))) " +
                            "OR (b.Descricao LIKE ISNULL('%" + sPesquisa + "%', SPACE(0))) " +
                            "OR (b.PartNumber LIKE ISNULL('%" + sPesquisa + "%', SPACE(0))) " +
                            "OR (ISNULL('"+sPesquisa+"', b.ConceitoID) LIKE  '%' + CONVERT(VARCHAR(10), b.ConceitoID)+'%')) ";
        }

        strSQL += " ORDER BY Produto ";
    }
    //Loja
    else if (nTipoPublicacaoID == 572) {
        strSQL = " SELECT a.ItemID AS LojaID, a.ItemMasculino AS Loja " +
		            "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
                "WHERE ((a.TipoID = 108) AND (a.EstadoID = 2)) ";

        if (sPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.ItemID) + SPACE(1) + a.ItemMasculino) LIKE '%" + sPesquisa + "%')";

        strSQL += " ORDER BY a.ItemMasculino ";
    }
    //Fam�lia
    else if (nTipoPublicacaoID == 573) {
        strSQL = "SELECT c.ConceitoID AS FamiliaID, c.Conceito AS Familia, COUNT(1) AS Quant " +
                    "FROM RelacoesPesCon a WITH(NOLOCK) " +
                        "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) " +
                        "INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) " +
                    " WHERE ((a.TipoRelacaoID = 61) AND (dbo.fn_Pessoa_Pais(a.SujeitoID, " + PaisID + ") = 1) " +
                           "AND (a.EstadoID IN (2, 12, 13, 14, 15)) AND (a.Publica = 1)) ";

        if (sPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), c.ConceitoID) + SPACE(1) + c.conceito) LIKE '%" + sPesquisa + "%')";

        strSQL += "GROUP BY c.ConceitoID, c.Conceito " +
                  "ORDER BY Familia" ;
    }
    //Marca
    else if (nTipoPublicacaoID == 574) {
        strSQL = "SELECT c.ConceitoID AS MarcaID, c.Conceito AS Marca, COUNT(1) AS Quant " +
                     "FROM RelacoesPesCon a WITH(NOLOCK) " +
                        "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) " +
                        "INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.MarcaID) " +
                     " WHERE ((a.TipoRelacaoID = 61) AND (dbo.fn_Pessoa_Pais(a.SujeitoID, " + PaisID + ") = 1) AND " +
                         "(a.EstadoID IN (2, 12, 13, 14, 15)) AND (a.Publica = 1)) ";

        if (sPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), c.ConceitoID) + SPACE(1) + c.conceito) LIKE '%" + sPesquisa + "%')";

        strSQL += "GROUP BY c.ConceitoID, c.Conceito " + 
                  " ORDER BY Marca";
    }
    //Ofertas
    else if (nTipoPublicacaoID == 575)
    {
        strSQL = "SELECT a.RecursoID, a.RecursoFantasia AS Oferta " +
                    "FROM Recursos a WITH(NOLOCK) " +
                    "WHERE ((a.Publica = 1) AND (a.TipoRecursoID = 7) AND (a.EstadoID = 2)) ";

        if (sPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.RecursoID) + SPACE(1) + a.RecursoFantasia) LIKE '%" + sPesquisa + "%')";

        strSQL += "ORDER BY a.RecursoID ";
    }
    //Press-release (Sacola?)
    else if (nTipoPublicacaoID == 576)
    {
        strSQL = "SELECT a.NovidadeID AS PressReleaseID, a.Titulo AS PressRelease, Data, Fonte, Observacoes " +
                    "FROM Novidades a WITH(NOLOCK) " +
                    "WHERE ((a.TipoNovidadeID = 501) AND (a.EstadoID = 2)) ";

        if (sPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.NovidadeID) + SPACE(1) + a.Titulo + SPACE(1) + CONVERT(VARCHAR(MAX), a.Observacoes)) LIKE '%" + sPesquisa + "%')";

        strSQL += "ORDER BY PressRelease ";
    }
    //Evento
    else if (nTipoPublicacaoID == 577) {
        strSQL = "SELECT a.EventoID, a.Evento, Data, Horario, Local, Endereco, PublicoAlvo  " +
                    "FROM Eventos a WITH(NOLOCK)  " +
                    "WHERE ((a.TipoEventoID = 503) AND (a.EstadoID = 2)) ";

        if (sPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.EventoID) + SPACE(1) + a.Evento) LIKE '%" + sPesquisa + "%')";

        strSQL += "ORDER BY Evento ";
    }
    //Noticia
    else if (nTipoPublicacaoID == 578)
    {
        strSQL = "SELECT a.NovidadeID AS NoticiaID, a.Titulo AS Noticia, Data, Fonte, Observacoes " +
                    "FROM Novidades a WITH(NOLOCK) " +
                    " WHERE ((a.TipoNovidadeID = 502) AND (a.EstadoID = 2)) ";

        if (sPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.NovidadeID) + SPACE(1) + a.Titulo + SPACE(1) + CONVERT(VARCHAR(MAX), a.Observacoes)) LIKE '%" + sPesquisa + "%')";

        strSQL += "ORDER BY Noticia ";
    }
    //Enquetes (Banners?)
    else if (nTipoPublicacaoID == 579)
    {
        strSQL = "SELECT a.PesquisaID, a.Pesquisa, a.Data, ISNULL(a.Observacao, '') AS Observacao " +
                    "FROM Pesquisas a WITH(NOLOCK) " +
                    "WHERE (a.EstadoID = 2) ";

        if (sPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.PesquisaID) + SPACE(1) + a.Pesquisa) LIKE '%" + sPesquisa + "%')";

        strSQL += "ORDER BY a.PesquisaID ";
    }



    setConnection(dsoPesq);
    //dsoPesq.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/listarpessoa.aspx' + strPars;
    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() {
 
 
    var nTipoPublicacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selTipoRegistroID.value');

    //Produto
    if (nTipoPublicacaoID == 571)
    {
        var aTitleCols = new Array('ID', 'Produto');
        var aFieldsCols = new Array('ProdutoID', 'Produto');
        var aMaskCols = new Array('', '');
    }
    //Loja
    if (nTipoPublicacaoID == 572) {
        var aTitleCols = new Array('ID', 'Loja');
        var aFieldsCols = new Array('LojaID', 'Loja');
        var aMaskCols = new Array('', '');
    }
    //Fam�lia
    if (nTipoPublicacaoID == 573) {
        var aTitleCols = new Array('ID', 'Familia', 'Quant');
        var aFieldsCols = new Array('FamiliaID', 'Familia', 'Quant');
        var aMaskCols = new Array('', '', '');
    }
    //Marca
    if (nTipoPublicacaoID == 574) {
        var aTitleCols = new Array('ID', 'Marca', 'Quant');
        var aFieldsCols = new Array('MarcaID', 'Marca', 'Quant');
        var aMaskCols = new Array('', '', '');
    }
    //Ofertas
    if (nTipoPublicacaoID == 575)
    {
        var aTitleCols = new Array('ID', 'Oferta');
        var aFieldsCols = new Array('RecursoID', 'Oferta');
        var aMaskCols = new Array('', '');
    }
    //Press-release (Sacola?)
    if (nTipoPublicacaoID == 576)
    {
        var aTitleCols = new Array('ID', 'PressRelease', 'Data', 'Fonte', 'Observacoes');
        var aFieldsCols = new Array('PressReleaseID', 'PressRelease', 'Data', 'Fonte', 'Observacoes');
        var aMaskCols = new Array('', '', '', '', '');
    }
    //Evento
    if (nTipoPublicacaoID == 577)
    {
        var aTitleCols = new Array('ID', 'Evento', 'Data', 'Hor�rio', 'Local', 'Endereco', 'P�blico Alvo');
        var aFieldsCols = new Array('EventoID', 'Evento', 'Data', 'Horario', 'Local', 'Endereco', 'PublicoAlvo');
        var aMaskCols = new Array('', '', '', '', '', '', '');
    }
    //Noticia
    if (nTipoPublicacaoID == 578)
    {
        var aTitleCols = new Array('ID', 'Noticia', 'Data', 'Fonte', 'Observacoes');
        var aFieldsCols = new Array('NoticiaID', 'Noticia', 'Data', 'Fonte', 'Observacoes');
        var aMaskCols = new Array('', '', '', '', '');
    }
    //Enquetes
    if (nTipoPublicacaoID == 579)
    {
        var aTitleCols = new Array('ID', 'Pesquisa', 'Data', 'Observacao');
        var aFieldsCols = new Array('PesquisaID', 'Pesquisa', 'Data', 'Observacao');
        var aMaskCols = new Array('', '', '', '');
    }


    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg, aTitleCols, []);

    fillGridMask(fg, dsoPesq, aFieldsCols, aMaskCols);
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.Redraw = 2;

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();

        btnOK.disabled = false;
    }
    else {
        btnOK.disabled = true;
        txtPublicado.focus();
    }

    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    

}
function fg_DblClick() {
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK

    if ((fg.Row >= 1) && (btnOK.disabled == false))
        btn_onclick(btnOK);
}


function txtPublicado_onKeyPress() {
    if (event.keyCode == 13)
        btnListar_onclick();
}
