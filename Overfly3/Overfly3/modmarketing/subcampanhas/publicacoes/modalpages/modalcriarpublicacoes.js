/********************************************************************
modalcriarpublicacoes.js

Library javascript para o modalcriarpublicacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport("dsoPesq");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoperfilUsuario = new CDatatransport("dsoperfilUsuario");
var dsoTipoPublicacao = new CDatatransport('dsoTipoPublicacao');

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_DescricaoAutomatica;
var glb_sTxtFiltro;
var glb_nCurrUserID;
var glb_empresa;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {

    glb_nCurrUserID = getCurrUserID();

    glb_empresa = getCurrEmpresaData();

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    btnCanc.style.visibility = 'hidden';
    btnOK.style.visibility = 'hidden';
    // ajusta o body do html
    var elem = document.getElementById('modalcriarpublicacoesBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);


    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
        btnGerar.disabled = false;
    }
    else {
        btnGerar.disabled = true;
        txtBusca.focus();
    }


    // coloca foco no campo apropriado
    if (document.getElementById('txtBusca').disabled == false)
        txtBusca.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    
    // texto da secao01
    secText('Criar Publica��es', 1);

    // ajusta elementos da janela
    var elem;
    var temp;
    
    elem = window.document.getElementById('divControls');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP / 2;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
    }


    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblTipoPublicacaoID', 'selTipoPublicacaoID', 15, 1],
                          ['lblBusca', 'txtBusca', 50, 1],
                          ['btnListar', 'btn', 50, 1],
                          ['lblRecorrencia', 'selRecorrencia', 15, 2],
                          ['lbldtInicio', 'txtdtInicio', 19, 2],
                          ['lbldtFim', 'txtdtFim', 19, 2],
                          ['lblNotificacao', 'chkNotificacao', 3, 2],
                          ['lblIgnorarOcultos', 'chkIgnorarOcultos', 3, 2],
                          ['lblIgnorarRepercucao', 'chkIgnorarRepercucao', 3, 2],
                          ['btnGerar', 'btn', 50, 2]], null, null, true);


    elem = window.document.getElementById('txtBusca');
    with (elem.style) {
        left = 140;
        width = 530;
    }

    elem = window.document.getElementById('lblBusca');
    with (elem.style) {
        left = 140;
    }

    elem = window.document.getElementById('btnListar');
    with (elem.style) {
        left = parseInt(document.getElementById('txtBusca').style.left) + parseInt(document.getElementById('txtBusca').style.width) + ELEM_GAP;
        width = 55;
    }

    elem = window.document.getElementById('txtDtInicio');
    with (elem.style) {
        left = document.getElementById('selRecorrencia').offsetLeft + document.getElementById('selRecorrencia').offsetWidth + ELEM_GAP;
    }
    
    elem = window.document.getElementById('lblDtInicio');
    with (elem.style) {
        left = document.getElementById('selRecorrencia').offsetLeft + document.getElementById('selRecorrencia').offsetWidth + ELEM_GAP;
    }

    elem = window.document.getElementById('txtDtFim');
    with (elem.style) {
        left = parseInt(document.getElementById('txtDtInicio').style.left) + parseInt(document.getElementById('txtDtInicio').style.width) + ELEM_GAP;
    }

    elem = window.document.getElementById('txtDtFim');
    with (elem.style) {
        left = parseInt(document.getElementById('txtDtInicio').style.left) + parseInt(document.getElementById('txtDtInicio').style.width) + ELEM_GAP;
    }

    elem = window.document.getElementById('lblDtFim');
    with (elem.style) {
        left = parseInt(document.getElementById('txtDtInicio').style.left) + parseInt(document.getElementById('txtDtInicio').style.width) + ELEM_GAP;
    }

    elem = window.document.getElementById('chkNotificacao');
    with (elem.style) {
        left = parseInt(document.getElementById('txtDtFim').style.left) + parseInt(document.getElementById('txtDtFim').style.width) + 5;
    }

    elem = window.document.getElementById('lblNotificacao');
    with (elem.style) {
        left = parseInt(document.getElementById('txtDtFim').style.left) + parseInt(document.getElementById('txtDtFim').style.width) + ELEM_GAP;
    }

    elem = window.document.getElementById('chkIgnorarOcultos');
    with (elem.style) {
        left = parseInt(document.getElementById('chkNotificacao').style.left) + parseInt(document.getElementById('chkNotificacao').style.width) + 2;
    }

    elem = window.document.getElementById('lblIgnorarOcultos');
    with (elem.style) {
        left = parseInt(document.getElementById('chkNotificacao').style.left) + parseInt(document.getElementById('chkNotificacao').style.width) + ELEM_GAP - 2;
    }

    elem = window.document.getElementById('chkIgnorarRepercucao');
    with (elem.style) {
        left = parseInt(document.getElementById('chkIgnorarOcultos').style.left) + parseInt(document.getElementById('chkIgnorarOcultos').style.width) + 2;
    }

    elem = window.document.getElementById('lblIgnorarRepercucao');
    with (elem.style) {
        left = parseInt(document.getElementById('chkIgnorarOcultos').style.left) + parseInt(document.getElementById('chkIgnorarOcultos').style.width) + ELEM_GAP -2;
    }

    elem = window.document.getElementById('btnGerar');
    with (elem.style) {
        //left = parseInt(document.getElementById('chkIgnorarRepercucao').style.left) + parseInt(document.getElementById('chkIgnorarRepercucao').style.width) + 3;
        left = parseInt(document.getElementById('txtBusca').style.left) + parseInt(document.getElementById('txtBusca').style.width) + ELEM_GAP;
        width = 55;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {

        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP / 2;
        top = parseInt(document.getElementById('divControls').style.top) + parseInt(document.getElementById('divControls').style.height) + ELEM_GAP + 80;
        width = temp + 410;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 253 ;
    }
    
    elem = document.getElementById('fg');
    with (elem.style) {

        left = ELEM_GAP  / 2;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);

    }

    txtBusca.onkeypress = txtBusca_onKeyPress;
    selTipoPublicacaoID.onchange = selTipoPublicacaoID_onchange;

    txtdtInicio.maxLength = 19;
    txtdtFim.maxLength = 19;

    // e so aceita caracteres coerentes com data
    txtdtFim.onkeypress = verifyDateTimeNotLinked;
    txtdtInicio.onkeypress = verifyDateTimeNotLinked;

    startGridInterface(fg);

    headerGrid(fg, [''], []);

    //fg.ColWidth(getColIndexByColKey(fg, 'NotaFiscalID')) = 1430;
    fg.Redraw = 2;

    setConnection(dsoTipoPublicacao);
    dsoTipoPublicacao.SQL = "SELECT ItemID AS TipoPublicacaoID, ItemMasculino AS TipoPublicacao FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ((EstadoID = 2) AND (TipoID = 311))";
    dsoTipoPublicacao.ondatasetcomplete = dsoTipoPublicacao_DSC;
    dsoTipoPublicacao.Refresh();
}


function dsoTipoPublicacao_DSC() {

    clearComboEx(['selTipoPublicacaoID']);

    while (!dsoTipoPublicacao.recordset.EOF) {
        optionStr = dsoTipoPublicacao.recordset['TipoPublicacao'].value;
        optionValue = dsoTipoPublicacao.recordset['TipoPublicacaoID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selTipoPublicacaoID.add(oOption);
        dsoTipoPublicacao.recordset.MoveNext();
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    lockControlsInModalWin(true);

    if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);
    else if (ctl.id == 'btnListar') {
        txtBusca.value = trimStr(txtBusca.value);
        startPesq(txtBusca.value);
    }
    else if (ctl.id == 'btnGerar') {
        saveDataInGrid();
    }
}

function startPesq(strPesquisa) {

    lockControlsInModalWin(true);

    //Altera��o: Remo��o da obrigatoriedade da pessoa e a rela��o da pessoa estar ativa, utilizado para devolu��o e rma LFS:05/05/2011

    var strSQL = '';
    var PaisID = getCurrEmpresaData()[1];

    //Produto
    if (selTipoPublicacaoID.value == 571) {
        strSQL = "SELECT DISTINCT TOP 50 b.ConceitoID AS ID, e.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, LEFT(dbo.fn_Produto_Descricao2(b.ConceitoID, NULL, 11), 100) AS Produto " +
                 "FROM RelacoesPesCon a WITH(NOLOCK) " +
                    "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) " +
                    "INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) " +
                    "INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = b.MarcaID)  " +
                    "INNER JOIN Recursos e WITH(NOLOCK) ON (e.RecursoID = b.EstadoID)  " +
                  "WHERE ((a.TipoRelacaoID = 61) AND (dbo.fn_Pessoa_Pais(a.SujeitoID, " + PaisID + ") = 1) AND (a.Publica = 1) AND (a.EstadoID IN (2,12,13,14)))";


        if (strPesquisa != '') {
            strSQL += " AND ((b.Conceito LIKE ISNULL('%" + strPesquisa + "%', SPACE(0))) " +
                            "OR (c.Conceito LIKE ISNULL('%" + strPesquisa + "%', SPACE(0))) " +
                            "OR (d.Conceito LIKE ISNULL('%" + strPesquisa + "%', SPACE(0))) " +
                            "OR (b.Modelo LIKE ISNULL('%" + strPesquisa + "%', SPACE(0))) " +
                            "OR (b.Descricao LIKE ISNULL('%" + strPesquisa + "%', SPACE(0))) " +
                            "OR (b.PartNumber LIKE ISNULL('%" + strPesquisa + "%', SPACE(0))) " +
                            "OR (ISNULL('" + strPesquisa + "', b.ConceitoID) LIKE  '%' + CONVERT(VARCHAR(10), b.ConceitoID)+'%')) ";
        }

        strSQL += " ORDER BY Produto ";
    }
    //Loja
    else if (selTipoPublicacaoID.value == 572) {
        strSQL = " SELECT DISTINCT TOP 50  a.ItemID AS ID, b.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, a.ItemMasculino AS Loja " +
		            "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
                    "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID)  " +
        "WHERE ((a.TipoID = 108) AND (a.EstadoID = 2)) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.ItemID) + SPACE(1) + a.ItemMasculino) LIKE '%" + strPesquisa + "%')";

        strSQL += " ORDER BY a.ItemMasculino ";
    }
    //Fam�lia
    else if (selTipoPublicacaoID.value == 573) {
        strSQL = "SELECT DISTINCT TOP 50  c.ConceitoID AS ID, d.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, c.Conceito AS Familia " +
                    "FROM RelacoesPesCon a WITH(NOLOCK) " +
                        "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) " +
                        "INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) " +
                        "INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID)  " +
                    " WHERE ((a.TipoRelacaoID = 61) AND (dbo.fn_Pessoa_Pais(a.SujeitoID, " + PaisID + ") = 1) " +
                           " AND (a.Publica = 1) AND (a.EstadoID = 2)) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), c.ConceitoID) + SPACE(1) + c.conceito) LIKE '%" + strPesquisa + "%')";

        strSQL += "ORDER BY Familia";
    }
    //Marca
    else if (selTipoPublicacaoID.value == 574) {
        strSQL = "SELECT DISTINCT TOP 50  c.ConceitoID AS ID,  d.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, c.Conceito AS Marca " +
                     "FROM RelacoesPesCon a WITH(NOLOCK) " +
                        "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) " +
                        "INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.MarcaID) " +
                        "INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID)  " +
                     " WHERE ((a.TipoRelacaoID = 61) AND (dbo.fn_Pessoa_Pais(a.SujeitoID, " + PaisID + ") = 1) AND " +
                         "(a.Publica = 1) AND (a.EstadoID = 2)) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), c.ConceitoID) + SPACE(1) + c.conceito) LIKE '%" + strPesquisa + "%')";

        strSQL += " ORDER BY Marca";
    }
    //Evento
    else if (selTipoPublicacaoID.value == 575) {
        strSQL = "SELECT DISTINCT TOP 50  a.EventoID AS ID, b.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, a.Evento, Data, Horario, Local, Endereco, PublicoAlvo  " +
                    "FROM Eventos a WITH(NOLOCK)  " +
                    "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID)  " +
                    "WHERE ((a.TipoEventoID = 503) AND (a.EstadoID = 2)) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.EventoID) + SPACE(1) + a.Evento) LIKE '%" + strPesquisa + "%')";

        strSQL += "ORDER BY Evento ";
    }
    //Press-release
    else if (selTipoPublicacaoID.value == 576) {
        strSQL = "SELECT TOP 50  a.NovidadeID AS ID, b.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, a.Titulo AS PressRelease, Data, Fonte, a.Observacoes " +
                    "FROM Novidades a WITH(NOLOCK) " +
                     "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID)  " +
                    "WHERE ((a.TipoNovidadeID = 501) AND (a.EstadoID = 2))";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.NovidadeID) + SPACE(1) + a.Titulo + SPACE(1) + CONVERT(VARCHAR(MAX), a.Observacoes)) LIKE '%" + strPesquisa + "%')";

        strSQL += "ORDER BY PressRelease ";
    }
    //Noticia
    else if (selTipoPublicacaoID.value == 577) {
        strSQL = "SELECT TOP 50  a.NovidadeID AS ID, b.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, a.Titulo AS Noticia, Data, Fonte, a.Observacoes " +
                    "FROM Novidades a WITH(NOLOCK) " +
                    "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID)  " +
                    " WHERE ((a.TipoNovidadeID = 502) AND (a.EstadoID = 2))";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.NovidadeID) + SPACE(1) + a.Titulo + SPACE(1) + CONVERT(VARCHAR(MAX), a.Observacoes)) LIKE '%" + strPesquisa + "%')";

        strSQL += "ORDER BY Noticia ";
    }
    //Enquetes
    else if (selTipoPublicacaoID.value == 578) {
        strSQL = "SELECT DISTINCT TOP 50 a.PesquisaID AS ID, b.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, a.Pesquisa, a.Data, ISNULL(a.Observacao, '') AS Observacao " +
                    "FROM Pesquisas a WITH(NOLOCK) " +
                    "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID)  " +
                    "WHERE (a.EstadoID = 2) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.PesquisaID) + SPACE(1) + a.Pesquisa) LIKE '%" + strPesquisa + "%')";

        strSQL += "ORDER BY a.PesquisaID ";
    }
    //Ofertas
    else if (selTipoPublicacaoID.value == 579) {
        strSQL = "SELECT DISTINCT TOP 50 a.RecursoID AS ID, b.RecursoAbreviado AS Est, CONVERT(BIT, 0) AS OK, a.RecursoFantasia AS Oferta " +
                    "FROM Recursos a WITH(NOLOCK) " +
                    "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID)  " +
                    "WHERE ((a.Publica = 1) AND (a.TipoRecursoID = 7) AND (a.EstadoID = 2)) ";

        if (strPesquisa != '')
            strSQL += " AND ((CONVERT(VARCHAR(10), a.RecursoID) + SPACE(1) + a.RecursoFantasia) LIKE '%" + strPesquisa + "%')";

        strSQL += "ORDER BY a.RecursoID ";
    }



    setConnection(dsoPesq);
    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();

   
}

function dsopesq_DSC() {

    if (selTipoPublicacaoID.value == 571) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'Produto');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Produto*');
        var aMaskCols = new Array('', '', '', '');
    }
    if (selTipoPublicacaoID.value == 572) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'Loja');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Loja*');
        var aMaskCols = new Array('', '', '', '');
    }
    if (selTipoPublicacaoID.value == 573) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'Familia');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Familia*');
        var aMaskCols = new Array('', '', '', '');
    }
    if (selTipoPublicacaoID.value == 574) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'Marca');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Marca*');
        var aMaskCols = new Array('', '', '', '');
    }
    if (selTipoPublicacaoID.value == 575) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'Evento', 'Data', 'Hor�rio', 'Local');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Evento*', 'Data*', 'Horario*', 'Local*');
        var aMaskCols = new Array('', '', '', '', '', '', '');
    }
    if (selTipoPublicacaoID.value == 576) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'PressRelease', 'Data', 'Fonte');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'PressRelease*', 'Data*', 'Fonte*');
        var aMaskCols = new Array('', '', '', '', '', '');
    }
    if (selTipoPublicacaoID.value == 577) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'Noticia', 'Data', 'Fonte');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Noticia*', 'Data*', 'Fonte*');
        var aMaskCols = new Array('', '', '', '', '', '');
    }
    if (selTipoPublicacaoID.value == 578) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'Pesquisa', 'Data', 'Observacao');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Pesquisa*', 'Data*', 'Observacao*');
        var aMaskCols = new Array('', '', '', '', '', '');
    }
    if (selTipoPublicacaoID.value == 579) {
        var aTitleCols = new Array('ID', 'Est', 'OK', 'Oferta');
        var aFieldsCols = new Array('ID*', 'Est*', 'OK', 'Oferta*');
        var aMaskCols = new Array('', '', '', '');
    }

    startGridInterface(fg);
    fg.FrozenCols = 0;
    fg.Editable = false;

    headerGrid(fg, aTitleCols, []);

    fillGridMask(fg, dsoPesq, aFieldsCols, aMaskCols);
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    fg.Redraw = 0;
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.Redraw = 2;

    if (fg.Rows > 1)
        fg.FrozenCols = 3;

    fg.Editable = true;
   
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
        btnGerar.disabled = false;
    }
    else {
        btnGerar.disabled = true;
        txtBusca.focus();
    }

}

//Apenas perfil de desenvolvedor, administrador e homologa��o poder�o alterar alguns dados do coneito em A.
function perfilUsuario() {
    setConnection(dsoperfilUsuario);

    dsoperfilUsuario.SQL = 'SELECT COUNT(DISTINCT a.PerfilID) AS PermiteSuporte, ' +
                                    '(SELECT COUNT(DISTINCT aa.PerfilID) ' +
							            'FROM RelacoesPesRec_Perfis aa WITH(NOLOCK) ' +
									        'INNER JOIN RelacoesPesRec bb WITH(NOLOCK) ON (bb.RelacaoID = aa.RelacaoID) ' +
		                                'WHERE (bb.objetoID = 999 AND bb.EstadoID = 2 AND  bb.SujeitoID =  ' + glb_nCurrUserID + ' AND aa.PerfilID IN (525, 619)))  AS PermiteHomologacao ' +
	                            'FROM RelacoesPesRec_Perfis a WITH(NOLOCK) ' +
		                            'INNER JOIN RelacoesPesRec b ON (b.RelacaoID = a.RelacaoID) ' +
	                            'WHERE (b.objetoID = 999 AND b.EstadoID = 2 AND b.SujeitoID =  ' + glb_nCurrUserID + '  AND a.PerfilID IN (500, 501)) ';

    dsoperfilUsuario.ondatasetcomplete = perfilUsuario_DSC;
    dsoperfilUsuario.Refresh();
}

function perfilUsuario_DSC() {
    glb_PermiteSuporte = dsoperfilUsuario.recordset['PermiteSuporte'].value;
    glb_PermiteHomologacao = dsoperfilUsuario.recordset['PermiteHomologacao'].value;

    return null;
}

function btnListar_onclick() {
    txtBusca.value = trimStr(txtBusca.value);

    if (btnListar.disabled)
        return;

    startPesq(txtBusca.value);
}
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var glb_nUserID = glb_USERID;

    var sDtInicio = '';
    var sDtFim = '';


    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';
    glb_nEmpresaID = getCurrEmpresaData()[0];
    lockControlsInModalWin(true);

    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                    strPars = '';
                    nDataLen = 0;
                }

                sDtInicio = normalizeDate_DateTime(txtdtInicio.value, true);
                sDtInicio = (sDtInicio == null ? '' : sDtInicio);

                sDtFim = normalizeDate_DateTime(txtdtFim.value, true);
                sDtFim = (sDtFim == null ? '' : sDtFim);


                strPars += '?nEmpresaID=' + escape(glb_nEmpresaID);
                strPars += '&nTipoPublicacaoID=' + escape(selTipoPublicacaoID.value);
                strPars += '&nRecorrencia=' + escape(selRecorrencia.value);
                strPars += '&dInicio=' + escape(sDtInicio);
                strPars += '&dFim=' + escape(sDtFim);
                strPars += '&bNotificacao=' + escape(chkNotificacao.checked ? 1 : 0);
                strPars += '&bIgnorarOcultos=' + escape(chkIgnorarOcultos.checked ? 1 : 0);
                strPars += '&bIgnorarRepercucao=' + escape(chkIgnorarRepercucao.checked ? 1 : 0);
                strPars += '&nUsuarioID=' + escape(glb_nCurrUserID);
            }

            nDataLen++;
            
            strPars += '&nPublicadoID=' + escape(getCellValueByColKey(fg, 'ID*', i));
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
    else {
        if (window.top.overflyGen.Alert('Selecione um registro') == 0)
            return null;
    }
    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/publicacoes/serverside/gerarpublicacoes.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            startPesq(txtBusca.value);
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_DSC() {

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
    
    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if ((dsoGrava.recordset['Resultado'].value != null) && (dsoGrava.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Publica��es geradas.') == 0)
                return null;
        }

    }

}
function selTipoPublicacaoID_onchange() {

        fg.Rows = 1;
        btnGerar.disabled = true;

}
function js_fg_modalcriarpublicacoesDblClick(grid, Row, Col) {

    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'Ok')))
        return true;

    var i;
    var bFill = true;

    fg.Editable = false;
    fg.Redraw = 0;
    lockControlsInModalWin(true);


    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    fg.Editable = true;
    fg.Redraw = 2;
    window.focus();
    fg.focus();
}

function txtBusca_onKeyPress() {
    if (event.keyCode == 13)
        btnListar_onclick();
}
