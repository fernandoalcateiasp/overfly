/********************************************************************
modalgerenciarpublicacoes.js

Library javascript para o modalgerenciarpublicacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport("dsoPesq");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoModerador = new CDatatransport('dsoModerador');
var dsoEstados = new CDatatransport("dsoEstados");

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_DescricaoAutomatica;
var glb_sTxtFiltro;
var glb_nCurrUserID;
var glb_empresa;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {

    glb_nCurrUserID = getCurrUserID();

    glb_empresa = getCurrEmpresaData();

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();
    btnCanc.style.visibility = 'hidden';
    btnOK.style.visibility = 'hidden';
    // ajusta o body do html
    var elem = document.getElementById('modalmoderarcomentariosBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
        btnGravar.disabled = false;
    }
    else {
        btnGravar.disabled = true;
        txtdtInicio.focus();
    }


    // coloca foco no campo apropriado
    if (document.getElementById('txtdtInicio').disabled == false)
        txtdtInicio.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    
    // texto da secao01
    secText('Moderar Coment�rios', 1);

    // ajusta elementos da janela
    var elem;
    var temp;
    
    elem = window.document.getElementById('divControls');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP / 2;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;
    }


    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblModeradorID', 'selModeradorID', 18, 1],
                          ['lblPublicaOK1', 'chkPublicaOK1', 3, 1],
                          ['lblPublicaOK0', 'chkPublicaOK0', 3, 1],
                          ['lbldtInicio', 'txtdtInicio', 19, 1],
                          ['lbldtFim', 'txtdtFim', 19, 1],
                          ['btnListar', 'btn', 50, 1],
                          ['btnGravar', 'btn', 50, 1]], null, null, true);


    //@@ *** Div Observa��es ***/
    adjustElementsInForm(['lblComentario', 'txtComentario', 10, 2], null, null, true);

    elem = window.document.getElementById('chkPublicaOK0');
    with (elem.style) {
      
        left = 177;
    }

    elem = window.document.getElementById('txtDtInicio');
    with (elem.style) {
        left = document.getElementById('chkPublicaOK0').offsetLeft + document.getElementById('chkPublicaOK0').offsetWidth + 5;
        //left = parseInt(document.getElementById('chkPublicaOK0').style.left) + parseInt(document.getElementById('chkNotificacao').style.width) + ELEM_GAP;
    }

    elem = window.document.getElementById('lblDtInicio');
    with (elem.style) {
        left = document.getElementById('chkPublicaOK0').offsetLeft + document.getElementById('chkPublicaOK0').offsetWidth + 5;
    }

    elem = window.document.getElementById('txtDtFim');
    with (elem.style) {
        left = parseInt(document.getElementById('txtDtInicio').style.left) + parseInt(document.getElementById('txtDtInicio').style.width) + 5;
    }

    elem = window.document.getElementById('lblDtFim');
    with (elem.style) {
        left = parseInt(document.getElementById('txtDtInicio').style.left) + parseInt(document.getElementById('txtDtInicio').style.width) + 5;
    }

    elem = window.document.getElementById('btnListar');
    with (elem.style) {
        //left = parseInt(document.getElementById('chkIgnorarRepercucao').style.left) + parseInt(document.getElementById('chkIgnorarRepercucao').style.width) + 3;
        left = parseInt(document.getElementById('txtDtFim').style.left) + parseInt(document.getElementById('txtDtFim').style.width) + 5;
    }

    elem = window.document.getElementById('btnGravar');
    with (elem.style) {
        //left = parseInt(document.getElementById('chkIgnorarRepercucao').style.left) + parseInt(document.getElementById('chkIgnorarRepercucao').style.width) + 3;
        left = parseInt(document.getElementById('btnListar').style.left) + parseInt(document.getElementById('btnListar').style.width) + 5;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {

        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP / 2;
        top = parseInt(document.getElementById('divControls').style.top) + parseInt(document.getElementById('divControls').style.height) + ELEM_GAP + 5;
        width = 625;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 225;
    }
    
    elem = document.getElementById('fg');
    with (elem.style) {

        left = ELEM_GAP  / 2;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);

    }

    elem = document.getElementById('txtComentario');
    with (elem.style) {

        left = ELEM_GAP ;
        top = parseInt(document.getElementById('fg').style.height) + parseInt(document.getElementById('divControls').style.height) + (ELEM_GAP * 4);
        width = parseInt(document.getElementById('fg').style.width) + parseInt(document.getElementById('fg').style.left) -2;
        height = parseInt(document.getElementById('fg').style.height) / 4;

    }


    txtdtInicio.maxLength = 19;
    txtdtFim.maxLength = 19;

    // e so aceita caracteres coerentes com data
    txtdtFim.onkeypress = txtdtFim_onKeyPress; // verifyDateTimeNotLinked;
    txtdtInicio.onkeypress = txtdtInicio_onKeyPress; //verifyDateTimeNotLinked;
    
    selModeradorID.onchange = selModeradorID_onchange;
    chkPublicaOK1.onclick = chkPublicaOK1_onchange;
    chkPublicaOK0.onclick = chkPublicaOK0_onchange;

    txtComentario.readOnly = true;
    startGridInterface(fg);

    headerGrid(fg, [''], []);

    fg.Redraw = 2;

    setConnection(dsoModerador);
    dsoModerador.SQL = " SELECT DISTINCT a.ModeradorID, b.Fantasia AS Moderador FROM Publicacoes_Comentarios a INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.ModeradorID ORDER BY b.Fantasia ";
    dsoModerador.ondatasetcomplete = dsoModerador_DSC;
    dsoModerador.Refresh();
}

function dsoModerador_DSC() {

    clearComboEx(['selModeradorID']);

    var oOption = document.createElement("OPTION");
        oOption.value = 0;
        oOption.text = "";
        selModeradorID.add(oOption);

        while (!dsoModerador.recordset.EOF) {
            optionStr = dsoModerador.recordset['Moderador'].value;
            optionValue = dsoModerador.recordset['ModeradorID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        
        if (optionStr == 'Overfly')
            oOption.selected = true;

        selModeradorID.add(oOption);
        dsoModerador.recordset.MoveNext();
       
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {

    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    lockControlsInModalWin(true);

    if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);
    else if (ctl.id == 'btnListar') {
        startPesq();
    }
    else if (ctl.id == 'btnGravar') {
        saveDataInGrid();
    }
}

function startPesq() {

    lockControlsInModalWin(true);

    //Altera��o: Remo��o da obrigatoriedade da pessoa e a rela��o da pessoa estar ativa, utilizado para devolu��o e rma LFS:05/05/2011

    var strSQL = '';
    var PaisID = getCurrEmpresaData()[1];
    
    strSQL = "SELECT DISTINCT TOP 100 a.PubComentarioID AS ID, a.dtData AS Data, b.Fantasia AS Usuario, c.Fantasia AS Moderador, " +
             " CONVERT(BIT, a.Publica) AS Publica, CONVERT(BIT, 0) AS OK, a.Comentario " +
                 "FROM Publicacoes_Comentarios a WITH(NOLOCK) " +
                    "INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.UsuarioID) " +
                    "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.ModeradorID) " +
                "WHERE a.ModeradorID IS NOT NULL ";

    if ((chkPublicaOK1.checked == false) && (chkPublicaOK0.checked == true))
        strSQL += " AND a.Publica = 0 ";

    if ((chkPublicaOK1.checked == true) && (chkPublicaOK0.checked == false))
        strSQL += " AND a.Publica = 1 ";

    if (selModeradorID.value != 0) 
        strSQL += " AND (a.ModeradorID = " + selModeradorID.value + ") ";

    if (txtdtFim.value != "")
        strSQL += " AND (a.dtData <= '" + normalizeDate_DateTime(txtdtFim.value, true) + "') ";

    if (txtdtInicio.value != "") 
        strSQL += " AND (a.dtData >= '" + normalizeDate_DateTime(txtdtInicio.value, true) + "') ";
    
    strSQL += " ORDER BY a.dtData ";

    setConnection(dsoPesq);
    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();

   
}

function dsopesq_DSC() {

    var aTitleCols = new Array('ID', 'Data', 'Usu�rio', 'Moderador', 'Publica', 'OK', 'Coment�rio');
    var aFieldsCols = new Array('ID*', 'Data*', 'Usuario*', 'Moderador*', 'Publica', 'OK', 'Comentario');

    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY HH:MM")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY HH:MM")
        dTFormat = 'mm/dd/yyyy hh:mm';

    var aMaskCols = new Array('', '99/99/9999 99:99:99', '', '', '', '', '');
    var aMaskVCols = new Array('', '99/99/9999 99:99:99', '', '', '', '', '');
        

    startGridInterface(fg);
    fg.FontSize = '10';
    fg.FrozenCols = 0;

    fg.Editable = false;

    headerGrid(fg, aTitleCols, [0, 6]);

    fillGridMask(fg, dsoPesq, aFieldsCols, aMaskCols, aMaskVCols);

    fg.FrozenCols = 1;


    fg.Redraw = 0;
    alignColsInGrid(fg);
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    if (fg.Rows > 1)
        fg.FrozenCols = 4;

    fg.Editable = true;
    txtComentario.readOnly = true;
   
    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1) {
        window.focus();
        fg.focus();
        btnGravar.disabled = false;
    }
    else {
        btnGravar.disabled = true;
        txtdtInicio.focus();
    }

}


function btnListar_onclick() {
    if (btnListar.disabled)
        return;

    startPesq();
}
function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    lockControlsInModalWin(true);

    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + encodeURIComponent(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?nUsuarioID=' + encodeURIComponent(glb_nCurrUserID);
            }


            nDataLen++;
            strPars += '&nID=' + encodeURIComponent(getCellValueByColKey(fg, 'ID*', i));
            strPars += '&bPublica=' + encodeURIComponent(getCellValueByColKey(fg, 'Publica', i) == 0 ? 0 : 1);
        }

    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + encodeURIComponent(nDataLen);
    else {
        if (window.top.overflyGen.Alert('Selecione um registro') == 0)
            return null;
    }
    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subcampanhas/publicacoes/serverside/moderarcomentarios.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            startPesq();
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_DSC() {

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
    
    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if (dsoGrava.recordset['fldresp'].value = 0) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['fldresp'].value) == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Registros moderados com sucesso.') == 0)
                return null;
        }

    }

}
function selModeradorID_onchange() {

    fg.Rows = 1;
    btnGravar.disabled = true;
    txtComentario.value = "";
}

function chkPublicaOK1_onchange() {

    if (chkPublicaOK1.checked == false && chkPublicaOK0.checked == false)
        chkPublicaOK0.checked = true;
            
}

function chkPublicaOK0_onchange() {

    if (chkPublicaOK1.checked == false && chkPublicaOK0.checked == false)
        chkPublicaOK1.checked = true;

}

function js_fg_modalmoderarcomentariosDblClick(grid, Row, Col) {

    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if ((Col != getColIndexByColKey(grid, 'Ok')))
        return true;

    var i;
    var bFill = true;

    fg.Editable = false;
    fg.Redraw = 0;
    lockControlsInModalWin(true);


    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    lockControlsInModalWin(false);
    fg.Editable = true;
    fg.Redraw = 2;
    window.focus();
    fg.focus();
}
function js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {

     txtComentario.value = fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'Comentario'));
}

function js_ModalModerarComentarios_AfterEdit(Row, Col) {
 
    fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
}
function txtdtInicio_onKeyPress() {
    
    txtdtInicio.onkeypress = verifyDateTimeNotLinked;

    if (event.keyCode == 13)
        txtdtFim.focus();
      
}

function txtdtFim_onKeyPress() {

    txtdtFim.onkeypress = verifyDateTimeNotLinked;

    if (event.keyCode == 13)
        btnListar_onclick();

}