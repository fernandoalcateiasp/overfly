<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="publicacoessup01Html" name="publicacoessup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/publicacoes/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subcampanhas/publicacoes/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="publicacoessup01Body" name="publicacoessup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
		<select id="selTipoRegistroID" name="selTipoRegistroID" DATASRC="#dsoSup01" DATAFLD="TipoPublicacaoID" class="fldGeneral"></select>
        <p id="lblPublicadoID" name="lblPublicadoID" class="lblGeneral">PublicadoID</p>
        <input type="text" id="txtPublicadoID" name="txtPublicadoID" DATASRC="#dsoSup01" DATAFLD="PublicadoID"  class="fldGeneral">
        <input type="image" id="btnFindTipoRegistro" name="btnFindTipoRegistro" class="fldGeneral" title="Preencher Combo" width="24" height="23">
        <p id="lblTitulo" name="lblTitulo" class="lblGeneral">T�tulo</p>
        <input type="text" id="txtTitulo" name="txtTitulo" DATASRC="#dsoSup01" DATAFLD="Titulo" class="fldGeneral">

        <p id="lblNotificacao" name="lblNotificacao" class="lblGeneral">Not</p>
        <input type="checkbox" id="chkNotificacao" name="chkNotificacao" DATASRC="#dsoSup01" DATAFLD="Notificacao" class="fldGeneral" title="Envia notifica��es para os clientes?"></input>
        <p id="lblNotificacaoRepetir" name="lblNotificacaoRepetir" class="lblGeneral">Rep</p>
        <input type="checkbox" id="chkNotificacaoRepetir" name="chkNotificacaoRepetir" DATASRC="#dsoSup01" DATAFLD="NotificacaoRepetir" class="fldGeneral" title="Repetir notifica��es na recorr�ncia at� Data Fim?"></input>
        <p id="lblEmailMarketing" name="lblEmailMarketing" class="lblGeneral">Email</p>
        <input type="checkbox" id="chkEmailMarketing" name="chkEmailMarketing" DATASRC="#dsoSup01" DATAFLD="Email" class="fldGeneral" title="Tamb�m enviar e-mail marketing desta publica��o?"></input>
        <p id="lblIgnorarOcultos" name="lblIgnorarOcultos" class="lblGeneral">IO</p>
        <input type="checkbox" id="chkIgnorarOcultos" name="chkIgnorarOcultos" DATASRC="#dsoSup01" DATAFLD="IgnorarOcultos" class="fldGeneral" title="Ignorar publica��es ocultas?"></input>
        <p id="lblIgnorarRepercucao" name="lblIgnorarRepercucao" class="lblGeneral">IR</p>
        <input type="checkbox" id="chkIgnorarRepercucao" name="chkIgnorarRepercucao" DATASRC="#dsoSup01" DATAFLD="IgnorarRepercucao" class="fldGeneral" title="Ignorar repercuss�o da publica��o e n�o atualizar a data da publica��o?"></input>
		<p id="lblRecorrencia" name="lblRecorrencia" class="lblGeneral">Recorr�ncia</p>
		<select id="selRecorrencia" name="selRecorrencia" DATASRC="#dsoSup01" DATAFLD="Recorrencia" class="fldGeneral" title="N�mero de horas para refrescar a data da publica��o">
            <option value=0></option>
            <option value=1>1</option>
            <option value=2>2</option>
            <option value=3>3</option>
            <option value=6>6</option>
            <option value=12>12</option>
            <option value=24>24</option>
            <option value=48>48</option>
            <option value=168>168</option>
        </select>
        <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">In�cio</p>
		<input type="text" id="txtdtInicio" name="txtdtInicio" DATASRC="#dsoSup01" DATAFLD="V_dtInicio" class="fldGeneral"></input>
        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Fim</p>
		<input type="text" id="txtdtFim" name="txtdtFim" DATASRC="#dsoSup01" DATAFLD="V_dtFim" class="fldGeneral"></input>
        <p id="lbldtData" name="lbldtData" class="lblGeneral">Data</p>
		<input type="text" id="txtdtData" name="txtdtData" DATASRC="#dsoSup01" DATAFLD="V_dtData" class="fldGeneral"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
		<input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
		<p id="lblSegmentacaoID" name="lblSegmentacaoID" class="lblGeneral">Segmenta��o</p>
		<select id="selSegmentacaoID" name="selSegmentacaoID" DATASRC="#dsoSup01" DATAFLD="SegmentacaoID" class="fldGeneral"></select>
        <p id="lblOR" name="lblOR" class="lblGeneral">OR</p>
        <input type="checkbox" id="chkOR" name="chkOR" DATASRC="#dsoSup01" DATAFLD="EhOR" class="fldGeneral" title="Clientes especificados em Destinos OU que manifestaram interesse na oferta da publica��o"></input>
        <p id="lblExcluir" name="lblExcluir" class="lblGeneral">Excluir</p>
        <input type="checkbox" id="chkExcluir" name="chkExcluir" DATASRC="#dsoSup01" DATAFLD="Excluir" class="fldGeneral" title="Excluir clientes que compraram no �ltimo m�s?"></input>
		<p id="lblRecencia" name="lblRecencia" class="lblGeneral">Rec�ncia</p>
		<select id="selRecenciaOperador" name="selRecenciaOperador" class="fldGeneral">
            <option value="<="><=</option>
            <option value="=">=</option>
            <option value=">=">>=</option>
		</select>
        <select id="selRecencia" name="selRecencia" class="fldGeneral">
            <option value=1>1</option>
            <option value=2>2</option>
            <option value=3>3</option>
            <option value=4>4</option>
            <option value=5>5</option>
        </select>

		<p id="lblFrequencia" name="lblFrequencia" class="lblGeneral">Frequ�ncia</p>
		<select id="selFrequenciaOperador" name="selFrequenciaOperador" class="fldGeneral">
            <option value="<="><=</option>
            <option value="=">=</option>
            <option value=">=">>=</option>
		</select>
        <select id="selFrequencia" name="selFrequencia" class="fldGeneral">
            <option value=1>1</option>
            <option value=2>2</option>
            <option value=3>3</option>
            <option value=4>4</option>
            <option value=5>5</option>
        </select>
        
		<p id="lblValor" name="lblValor" class="lblGeneral">Valor</p>
		<select id="selValorOperador" name="selValorOperador" class="fldGeneral">
            <option value="<="><=</option>
            <option value="=">=</option>
            <option value=">=">>=</option>
		</select>
        <select id="selValor" name="selValor" class="fldGeneral">
            <option value=1>1</option>
            <option value=2>2</option>
            <option value=3>3</option>
            <option value=4>4</option>
            <option value=5>5</option>
        </select>
    </div>
    
</body>

</html>
