/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
//  Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
//  Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
//  Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
//  Busca data corrente no servidor .URL 
var dsoCurrData = new CDatatransport("dsoCurrData");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");


var glb_BtnFromFramWork;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTipoRegistroID', '1']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modmarketing/subcampanhas/publicacoes/inferior.asp',
                              SYS_PAGESURLROOT + '/modmarketing/subcampanhas/publicacoes/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'PublicacaoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    // glb_sCtlTipoRegistroID = 'selTipoPublicacaoID';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoPublicacaoID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);
    
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1],
                          ['lblTipoRegistroID', 'selTipoRegistroID', 10, 1],
                          ['lblPublicadoID', 'txtPublicadoID', 10, 1],
                          ['btnFindTipoRegistro', 'btn', 21, 1],
                          ['lblTitulo', 'txtTitulo', 60, 2],
                          ['lblNotificacao', 'chkNotificacao', 3, 2],
                          ['lblNotificacaoRepetir', 'chkNotificacaoRepetir', 3, 2],
                          ['lblEmailMarketing', 'chkEmailMarketing', 3, 2],
                          ['lblIgnorarOcultos', 'chkIgnorarOcultos', 3, 2],
                          ['lblRecorrencia', 'selRecorrencia', 10, 3],
                          ['lbldtInicio','txtdtInicio', 19, 3],
                          ['lbldtFim', 'txtdtFim', 19, 3],
                          ['lbldtData', 'txtdtData', 19, 3],                          
                          ['lblIgnorarRepercucao', 'chkIgnorarRepercucao', 3, 3],
                          ['lblObservacao', 'txtObservacao', 40, 3],
                          ['lblSegmentacaoID', 'selSegmentacaoID', 15, 4],
                          ['lblOR', 'chkOR', 3, 4],
                          ['lblExcluir', 'chkExcluir', 3, 4],
                          ['lblRecencia', 'selRecenciaOperador', 7, 4, -12],
                          ['', 'selRecencia', 5, 4],
                          ['lblFrequencia', 'selFrequenciaOperador', 7, 4, -12],
                          ['', 'selFrequencia', 5, 4],
                          ['lblValor', 'selValorOperador', 7, 4, -12],
                          ['', 'selValor', 5, 4]], null, null, true);


    // Estes campos tem dd/mm/yyyy hh:mm:ss
    txtdtInicio.maxLength = 19;
    txtdtFim.maxLength = 19;
    txtdtData.maxLength = 19;

    selRecencia.style.left = (selRecencia.offsetLeft - 10);
    selFrequencia.style.left = (selFrequencia.offsetLeft - 10);
    selValor.style.left = (selValor.offsetLeft - 10);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    // Da automacao, deve constar de todos os ifs do carrierArrived
    if ( __currFormState() != 0 )
            return null;
            
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    lblTipoRegistroID.style.color = 'blue';
    lblPublicadoID.style.color = 'blue';
    lblTitulo.style.color = 'green';
    lblRecorrencia.style.color = 'green';
    lbldtInicio.style.color = 'green';
    lbldtFim.style.color = 'green';
    lblNotificacao.style.color = 'green';
    lblIgnorarOcultos.style.color = 'green';
    lblIgnorarRepercucao.style.color = 'green';
    lblObservacao.style.color = 'green';
    lbldtData.style.color = 'green';
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    glb_BtnFromFramWork = btnClicked;

    //@@
    
	showHideCombos(true);

	// Mover esta funcao para os finais de retornos de operacoes no
	// banco de dados
	if (!(dsoSup01.recordset.BOF || dsoSup01.recordset.EOF))
	{
	    if (dsoSup01.recordset['EstadoID'].value == 2)
	    {
	        lbldtInicio.style.color = 'black';
	        txtdtInicio.readOnly = true;
	    }
	    else if (dsoSup01.recordset['EstadoID'].value == 1)
	    {
	        lbldtInicio.style.color = 'green';
	        txtdtInicio.readOnly = false;
	    }
	}
	else
	{
	    lbldtInicio.style.color = 'green';
	    txtdtInicio.readOnly = false;
	}

    // Estes campos tem dd/mm/yyyy hh:mm:ss
	txtdtInicio.maxLength = 19;
	txtdtFim.maxLength = 19;
	txtdtData.maxLength = 19;

    // e seleciona conteudo quando em foco
	txtdtInicio.onfocus = selFieldContent;
	txtdtFim.onfocus = selFieldContent;
	txtdtData.onfocus = selFieldContent;

    // e so aceita caracteres coerentes com data
	txtdtInicio.onkeypress = verifyDateTimeNotLinked;
	txtdtFim.onkeypress = verifyDateTimeNotLinked;
	txtdtData.onkeypress = verifyDateTimeNotLinked;

	if ((btnClicked == 'SUPDET') ||
     (btnClicked == 'SUPREFR') ||
     (btnClicked == 'SUPANT') ||
     (btnClicked == 'SUPSEG')) {
	    startDynamicCmbs();
	}
	else
	    finalOfSupCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    if (btnClicked == 'SUPINCL') {
        controlReadOnlyFields();
    }
    
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

function controlReadOnlyFields()
{
    txtPublicadoID.readOnly = true;
    txtdtInicio.readOnly = false;
    lbldtInicio.style.color = 'green';

    if (!(dsoSup01.recordset.BOF || dsoSup01.recordset.EOF))
    {
        if (dsoSup01.recordset['EstadoID'].value == 2)
        {
            txtdtInicio.readOnly = true;
            lbldtInicio.style.color = 'black';
        }
            
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
    {
        adjustSupInterface();
        txtPublicadoID.value = "";
        //showHideCombos(false);
        adjustLabelsCombos(false);

        if ((selTipoRegistroID.value == 571) || (selTipoRegistroID.value == 573) || (selTipoRegistroID.value == 574))
        {
            selSegmentacaoID.style.visibility = 'inherit';
            lblSegmentacaoID.style.visibility = 'inherit';
        }
        else
        {
            selSegmentacaoID.style.visibility = 'hidden';
            lblSegmentacaoID.style.visibility = 'hidden';
        }
    }
    else
		adjustLabelsCombos(false);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }
    //else if (btnClicked == 4) {
    //    loadModalImagem();
    //}
    else if (btnClicked == 5) {
        openModalCriarPublicacoes();
    }
}


function loadModalImagem() {
    var strPars = new String();

    var nPublicacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PublicacaoID'].value");

    var nTipoPublicacaoID = dsoSup01.recordset["TipoPublicacaoID"].value;

    var sCaller = 'S';

    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));

    strPars = '?nRegistroID=' + escape(nPublicacaoID);
    strPars += '&nTipoRegistroID=' + escape(nTipoPublicacaoID);
    strPars += '&sCaller=' + escape(sCaller);

    //DireitoEspecifico
    //Pessoas->F�sicas/jur�dicas->Modal Imagens
    //20100 SFS-Grupo Pessoas -> A1&&A2 -> Permite abrir a modal para gerenciar imagens, caso contrario apenas mostra a imagem cadastrada.
    if ((nA1 == 1) && (nA2 == 1)) {
        htmlPath = SYS_ASPURLROOT + '/modalgen/modalgerimagens.asp' + strPars;

        showModalWin(htmlPath, new Array(724, 488));
    }
    else {
        htmlPath = SYS_ASPURLROOT + '/modalgen/modalshowimagens.asp' + strPars;

        showModalWin(htmlPath, new Array(0, 0));
    }
}

function openModalCriarPublicacoes() {
    /*
    var htmlPath;

    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                              ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));


    if ((nA1 == 1) && (nA2 == 1)) {
        htmlPath = SYS_ASPURLROOT + '/modalgen/modalgerimagens.asp' + strPars;

        showModalWin(htmlPath, new Array(724, 488));
*/

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var nEmpresaID = getCurrEmpresaData();
    
    if (btnClicked == 'SUPOK')
    {
        // Se e um novo registro
        //if (dsoSup01.recordset[glb_sFldIDName] != null && dsoSup01.recordset[glb_sFldIDName].value != null)
        if (dsoSup01.recordset[glb_sFldIDName].value == null)
			dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
     if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
     }
    // Imagens
     if ((idElement.toUpperCase() == 'MODALGERIMAGENSHTML') || (idElement.toUpperCase() == 'MODALSHOWIMAGENSHTML')) {
         if (param1 == 'OK') {
             // esta funcao fecha a janela modal e destrava a interface
             restoreInterfaceFromModal();
             // escreve na barra de status
             writeInStatusBar('child', 'cellMode', 'Detalhe');
             return 0;
         }
         else if (param1 == 'CANCEL') {
             // esta funcao fecha a janela modal e destrava a interface
             restoreInterfaceFromModal();
             // escreve na barra de status
             writeInStatusBar('child', 'cellMode', 'Detalhe');
             return 0;
         }
     }
    else if (idElement.toUpperCase() == 'MODALTIPOPUBLICACAOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            txtPublicadoID.value = param2;
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{

    controlReadOnlyFields();
    // chamada abaixo necessaria para prosseguir a automacao

    // Estes campos tem dd/mm/yyyy hh:mm:ss
    txtdtInicio.maxLength = 19;
    txtdtFim.maxLength = 19;
    txtdtData.maxLength = 19;

    // e seleciona conteudo quando em foco
    txtdtInicio.onfocus = selFieldContent;
    txtdtFim.onfocus = selFieldContent;
    txtdtData.onfocus = selFieldContent;

    // e so aceita caracteres coerentes com data
    txtdtInicio.onkeypress = verifyDateTimeNotLinked;
    txtdtFim.onkeypress = verifyDateTimeNotLinked;
    txtdtData.onkeypress = verifyDateTimeNotLinked;

    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimentos', 'Imagem', 'Criar publica��es', 'Alterar/Ativar Publica��es', 'Modera��o de Coment�rios']);

    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);
}
function dsoCurrDataComplete_DSC() {
    // Prossegue a automacao interrompida no botao de inclusao
    lockAndOrSvrSup_SYS();
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
        setLabelOfControl(lblTipoRegistroID, dsoSup01.recordset['TipoPublicacaoID'].value);
	}        
    else
    {
        setLabelOfControl(lblTipoRegistroID, selTipoRegistroID.value);
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    if (btnClicked.id == 'btnFindTipoRegistro')
        openModalTipoPublicacao();
}
/********************************************************************
Funcao criada pelo programador.
Abre janela modal de parceiro 

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalTipoPublicacao() {
    var htmlPath;
    var strPars = new String();
    var aEmpresa = getCurrEmpresaData();
    var nCurrEmpresa = aEmpresa[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/modmarketing/subcampanhas/publicacoes/modalpages/modaltipopublicacao.asp' + strPars;
    showModalWin(htmlPath, new Array(900, 440));

}

function showHideCombos(bPaging) {

    adjustLabelsCombos(bPaging);
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    glb_CounterCmbsDynamics = 1;

    // parametrizacao do dso dsoCmbDynamic03 (designado para selImportadorID)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL =   "SELECT 0 AS fldID, \'\' AS fldName UNION ALL " +
                            "SELECT ItemID AS fldID, ItemMasculino AS fldName " +
		                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
		                        "WHERE TipoID = 314";

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.refresh();
}

/**********************************************************************
 *
 **********************************************************************/
function dsoCmbDynamic_DSC()
{
    var i;
    var optionStr, optionValue;
    var aCmbsDynamics = [selSegmentacaoID];
    var aDSOsDunamics = [dsoCmbDynamic01];

    clearComboEx(['selSegmentacaoID']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < aCmbsDynamics.length; i++) {
            while (!aDSOsDunamics[i].recordset.EOF) {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
                optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }
        }

        setLabelOfControl(lblSegmentacaoID, selSegmentacaoID.value);
        selSegmentacaoID.onchange = selSegmentacaoID_onChange;

        // volta para a automacao
        finalOfSupCascade(glb_BtnFromFramWork);

        if ((selTipoRegistroID.value == 571) || (selTipoRegistroID.value == 573) || (selTipoRegistroID.value == 574))
        {
            selSegmentacaoID.style.visibility = 'inherit';
            lblSegmentacaoID.style.visibility = 'inherit';
        }
        else
        {
            selSegmentacaoID.style.visibility = 'hidden';
            lblSegmentacaoID.style.visibility = 'hidden';
        }
        
        selSegmentacaoID_onChange();
    }

    return null;
}

function selSegmentacaoID_onChange()
{
    if ((selSegmentacaoID.value == 1197) && (selSegmentacaoID.style.visibility == 'inherit'))
    {
        lblOR.style.visibility = 'inherit';
        chkOR.style.visibility = 'inherit';
        lblExcluir.style.visibility = 'inherit';
        chkExcluir.style.visibility = 'inherit';
        lblRecencia.style.visibility = 'inherit';
        selRecenciaOperador.style.visibility = 'inherit';
        selRecencia.style.visibility = 'inherit';
        lblFrequencia.style.visibility = 'inherit';
        selFrequenciaOperador.style.visibility = 'inherit';
        selFrequencia.style.visibility = 'inherit';
        lblValor.style.visibility = 'inherit';
        selValorOperador.style.visibility = 'inherit';
        selValor.style.visibility = 'inherit';
    }
    else
    {
        lblOR.style.visibility = 'hidden';
        chkOR.style.visibility = 'hidden';
        lblExcluir.style.visibility = 'hidden';
        chkExcluir.style.visibility = 'hidden';
        lblRecencia.style.visibility = 'hidden';
        selRecenciaOperador.style.visibility = 'hidden';
        selRecencia.style.visibility = 'hidden';
        lblFrequencia.style.visibility = 'hidden';
        selFrequenciaOperador.style.visibility = 'hidden';
        selFrequencia.style.visibility = 'hidden';
        lblValor.style.visibility = 'hidden';
        selValorOperador.style.visibility = 'hidden';
        selValor.style.visibility = 'hidden';
    }
}