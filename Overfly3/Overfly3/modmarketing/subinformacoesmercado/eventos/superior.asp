<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="eventossup01Html" name="eventossup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subinformacoesmercado/eventos/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subinformacoesmercado/eventos/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="eventossup01Body" name="eventossup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoEventoID"></select>    
        <p id="lblInscricoes" name="lblInscricoes" class="lblGeneral">Inscri��es</p>
        <input type="checkbox" id="chkInscricoes" name="chkInscricoes" DATASRC="#dsoSup01" DATAFLD="Inscricoes" class="fldGeneral" title="Permite inscri��o na Web?"></input>
        <p id="lblMobile" name="lblMobile" class="lblGeneral">Mobile</p>
        <input type="checkbox" id="chkMobile" name="chkMobile" DATASRC="#dsoSup01" DATAFLD="Mobile" class="fldGeneral" title="Pesquisa Mobile ?"></input>
        <p id="lblCadastrados" name="lblCadastrados" class="lblGeneral">Cadastrados</p>
        <input type="checkbox" id="chkCadastrados" name="chkCadastrados" DATASRC="#dsoSup01" DATAFLD="Cadastrados" class="fldGeneral" title="Evento s� para clientes?"></input>
        <p id="lblEspeciais" name="lblEspeciais" class="lblGeneral">Especiais</p>
        <input type="checkbox" id="chkEspeciais" name="chkEspeciais" DATASRC="#dsoSup01" DATAFLD="Especiais" class="fldGeneral" title="Evento s� para clientes especiais?"></input>
        <p id="lblIPI" name="lblIPI" class="lblGeneral">IPI</p>
        <input type="checkbox" id="chkIPI" name="chkIPI" DATASRC="#dsoSup01" DATAFLD="IPI" class="fldGeneral" title="Evento s� para clientes IPI?"></input>
        <p id="lblResultado" name="lblResultado" class="lblGeneral">Resultado</p>
        <input type="checkbox" id="chkResultado" name="chkResultado" DATASRC="#dsoSup01" DATAFLD="Resultado" class="fldGeneral" title="Tem resultado?"></input>
        <p id="lblFotos" name="lblFotos" class="lblGeneral">Fotos</p>
        <input type="checkbox" id="chkFotos" name="chkFotos" DATASRC="#dsoSup01" DATAFLD="Fotos" class="fldGeneral" title="Evento tem fotos?"></input>
		<p id="lblEvento" name="lblEvento" class="lblGeneral">Evento</p>
		<input type="text" id="txtEvento" name="txtEvento" DATASRC="#dsoSup01" DATAFLD="Evento" class="fldGeneral"></input>
		<p id="lblData" name="lblData" class="lblGeneral" >Data</p>
		<input type="text" id="txtData" name="txtData" DATASRC="#dsoSup01" DATAFLD="V_Data" class="fldGeneral"></input>
		<p id="lblHorario" name="lblHorario" class="lblGeneral" >Hor�rio</p>
		<input type="text" id="txtHorario" name="txtHorario" DATASRC="#dsoSup01" DATAFLD="Horario" class="fldGeneral"></input>
		<p id="lblLocal" name="lblLocal" class="lblGeneral">Local</p>
		<input type="text" id="txtLocal" name="txtLocal" DATASRC="#dsoSup01" DATAFLD="Local" class="fldGeneral"></input>
		<p id="lblEndereco" name="lblEndereco" class="lblGeneral" >Endere�o</p>
		<input type="text" id="txtEndereco" name="txtEndereco" DATASRC="#dsoSup01" DATAFLD="Endereco" class="fldGeneral"></input>
		<p id="lblPublicoAlvo" name="lblPublicoAlvo" class="lblGeneral" >P�blico Alvo</p>
		<input type="text" id="txtPublicoAlvo" name="txtPublicoAlvo" DATASRC="#dsoSup01" DATAFLD="PublicoAlvo" class="fldGeneral"></input>
		<p id="lblConteudo" name="lblConteudo" class="lblGeneral">Conte�do</p>
		<input type="text" id="txtConteudo" name="txtConteudo" DATASRC="#dsoSup01" DATAFLD="Conteudo" class="fldGeneral"></input>
		<p id="lblPreRequisito" name="lblPreRequisito" class="lblGeneral">Pr� Requisito</p>
		<input type="text" id="txtPreRequisito" name="txtPreRequisito" DATASRC="#dsoSup01" DATAFLD="PreRequisito" class="fldGeneral"></input>
		<p id="lblPreco" name="lblPreco" class="lblGeneral">Pre�o</p>
		<input type="text" id="txtPreco" name="txtPreco" DATASRC="#dsoSup01" DATAFLD="Preco" class="fldGeneral"></input>
    </div>
    
</body>

</html>
