/********************************************************************
modalinscricoes.js

Library javascript para o modalinscricoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_InscricoesTimerInt = null;

//  Dados dos combos Vendedores, Parceiros, Pessoas .URL  
var dsoCombos = new CDatatransport("dsoCombos");
//  Compra de Item .URL  
var dsoPurchaseItem = new CDatatransport("dsoPurchaseItem");
//  Dados do grid .RDS  
var dsoGrid = new CDatatransport("dsoGrid");
//  Dados dos combos CFOP .RDS  
var dsoCFOP = new CDatatransport("dsoCFOP");
//  Dados dos combos Prazo .RDS  
var dsoPrazo = new CDatatransport("dsoPrazo");
//  Gerar Pedido .URL  
var dsoGerarPedido = new CDatatransport("dsoGerarPedido");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btns_onclick()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalinscricoes.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalinscricoes.ASP

js_fg_modalinscricoesBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalinscricoesDblClick( grid, Row, Col)
js_modalinscricoesKeyPress(KeyAscii)
js_modalinscricoes_AfterRowColChange
js_modalinscricoes_ValidateEdit()
js_modalinscricoes_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalinscricoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Participantes', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // ajusta o divCtlBar2
    with (divCtlBar2.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        width = modWidth - 5;
        height = 30;
        // height = 40;
        top = modHeight - parseInt(height, 10) - 5;
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita e esconde o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    btnOK.style.top = 20;
    // centra e troca label do botao cancel
    // divCtlBar2.appendChild(btnCanc);
    btnCanc.style.left = (modWidth - parseInt(btnCanc.currentStyle.width, 10)) / 2;
    btnCanc.value = 'Cancelar';
    btnCanc.style.top = 20;
    
    // btnGravar
    with (btnGravar.style)
    {
        left = (parseInt(divCtlBar2.currentStyle.width, 10) / 2) - (btnWidth) + 5;
        top = eTop;
        width = btnWidth;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    btnGravar.onclick = btns_onclick;
    
    // btnCanc, todo a direita
    with (btnCanc.style)
    {
        top = eTop;
        width = btnWidth;
        left =  parseInt(btnGravar.currentStyle.width, 10) + 
                parseInt(btnGravar.currentStyle.left, 10) + 10;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    
    // linha horizontal de arremate superior topLine
    // do divCtlBar2
    with ( topLine.style )
    {
        // color = 'gray';
        color = 'transparent';
        left = 0;
        top = 0;
        height = 0;
        width = parseInt(divCtlBar2.currentStyle.width, 10);
    }   
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(divCtlBar2.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
}

/********************************************************************
Usuario clicou botao que nao e OK ou Cancel
********************************************************************/
function btns_onclick()
{
    if ( this.disabled )
        return;
    
    var _retMsg;
    var sMsg = '';
        
    if (this == btnGravar)
        saveDataInGrid();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        lockControlsInModalWin(true);
        sendJSMessage( getHtmlId(), JS_DATAINFORM, glb_sCaller, null );
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( !btnGravar.disabled )
            sMsg = 'Fechar sem gravar?';

        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    var nCurrLineInGrid = -1;

    btnGravar.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    if (glb_InscricoesTimerInt != null)
    {
        window.clearInterval(glb_InscricoesTimerInt);
        glb_InscricoesTimerInt = null;
    }
    
    var nEventoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 
        'SELECT a.EventoInscricaoID, (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.EmpresaID) AS Empresa, ' + 
            'a.Nome, a.Compareceu, a.Resultado1, a.Resultado2, a.Resultado3, a.Resultado4 ' +
        'FROM Eventos_Inscricoes a WITH(NOLOCK) ' +
        'WHERE a.EventoID = '+ nEventoID + ' AND (a.Convite IS NULL OR a.Convite=0) ' +
        'ORDER BY (SELECT TOP 1 Fantasia FROM Pessoas WHERE a.EmpresaID = PessoaID), a.Nome ';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    var bResultado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['Resultado'].value");
    
    if (bResultado)
        aHoldCols = [8];
    else
        aHoldCols = [4,5,6,7,8]; 

    headerGrid(fg,['ID',
                   'Empresa',
                   'Nome',
                   'Compareceu',
                   'Resultado1',
                   'Resultado2',
                   'Resultado3',
                   'Resultado4',
                   'EventoInscricaoID'],aHoldCols);
                       
    fillGridMask(fg,dsoGrid,['EventoInscricaoID*',
                             'Empresa*', 
                             'Nome*',
                             'Compareceu',
                             'Resultado1',
                             'Resultado2',
                             'Resultado3',
                             'Resultado4',
							 'EventoInscricaoID'],
                             ['','','','','9999999999','9999999999','9999999999','9999999999','']);
                              
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.FrozenCols = 3;
    
    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = 3;
    
    if (fg.Rows > 1)
        fg.Editable = true;

    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    glb_dataGridWasChanged = false;
    
    lockControlsInModalWin(true);
    
    try
    {
        dsoGrid.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg.Rows = 1;
        lockControlsInModalWin(false);
        
        setupBtnsFromGridState();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
        return null;
    }    

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);
    
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalinscricoesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalinscricoesDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalinscricoesKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalinscricoes_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalinscricoes_AfterEdit(Row, Col)
{
    var nType;
    
    if (fg.Editable)
    {
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();
        dsoGrid.recordset.Find(fg.ColKey(8), fg.TextMatrix(Row, 8));

        if ( !(dsoGrid.recordset.EOF) )
        {
            nType = dsoGrid.recordset[fg.ColKey(Col)].type;
            
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
                dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
            else    
                dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);
                
            glb_dataGridWasChanged = true;
            setupBtnsFromGridState();
        }    
    }    
}
// FINAL DE EVENTOS DE GRID *****************************************
