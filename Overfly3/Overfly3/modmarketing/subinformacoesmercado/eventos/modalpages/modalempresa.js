/********************************************************************
modalEmpresa.js

Library javascript para o modalEmpresa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var modPesqRel_TimerHandler = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoPesq = new CDatatransport("dsoPesq");
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalEmpresaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPesquisa').disabled == false )
        txtPesquisa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Empresa', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    // ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        height = 40;
        
    }
    
    // txtPesquisa
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtPesquisa.style)
    {
        left = 0;
        top = 16;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtPesquisa.onkeypress = txtPesquisa_onKeyPress;
    
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;

    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPesquisa.style.top, 10);
        left = parseInt(txtPesquisa.style.left, 10) + parseInt(txtPesquisa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = frameRect[3] - (14 * ELEM_GAP);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    headerGrid(fg,['ID',
                   'Nome',
                   'Fantasia'], []);

    fg.Redraw = 2;
    
    lblPesquisa.innerText = 'Empresa';
}

function txtPesquisa_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPesqRel_TimerHandler = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }
}


function txtPesquisa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqRel_TimerHandler != null )
    {
        window.clearInterval(modPesqRel_TimerHandler);
        modPesqRel_TimerHandler = null;
    }
    
    txtPesquisa.value = trimStr(txtPesquisa.value);
    
    changeBtnState(txtPesquisa.value);

    if (btnFindPesquisa.disabled)
    {
        lockControlsInModalWin(false);
        return;
    }    
    
    startPesq(txtPesquisa.value);
}


function fg_ModPesqRelDblClick()
{
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqRelKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
 
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id)
    {
		var aSendBack = new Array((glb_Convidado == 0 ? 2 : 8));

		aSendBack[0] = fg.TextMatrix(fg.Row, 2);
		aSendBack[1] = fg.TextMatrix(fg.Row, 1);
		
		if (glb_Convidado == 1)
		{
			aSendBack[2] = fg.TextMatrix(fg.Row, 3);
			aSendBack[3] = fg.TextMatrix(fg.Row, 4);
			aSendBack[4] = fg.TextMatrix(fg.Row, 5);
			aSendBack[5] = fg.TextMatrix(fg.Row, 6);
			aSendBack[6] = fg.TextMatrix(fg.Row, 7);
			aSendBack[7] = fg.TextMatrix(fg.Row, 8);
		}
		
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , aSendBack );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
        
    var strPas = '?';
    strPas += 'strToFind='+escape(strPesquisa);
    strPas += '&Convidado='+escape(glb_Convidado);

    dsoPesq.URL = SYS_ASPURLROOT + '/modmarketing/subinformacoesmercado/eventos/serverside/pesqempresa.aspx'+strPas;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    var aHeaderToGrid = new Array((glb_Convidado == 0 ? 3 : 9));
    var aFieldsToGrid = new Array((glb_Convidado == 0 ? 3 : 9));
    var aMaskToGrid = new Array((glb_Convidado == 0 ? 3 : 9));
    var aHoldCols = new Array((glb_Convidado == 0 ? 0 : 1));
    
    aHeaderToGrid[0] = 'Nome';
    aHeaderToGrid[1] = 'ID';
    aHeaderToGrid[2] = 'Fantasia';
    
    aFieldsToGrid[0] = 'Nome';
    aFieldsToGrid[1] = 'PessoaID';
    aFieldsToGrid[2] = 'Fantasia';
    
    aMaskToGrid[0] = '';
    aMaskToGrid[1] = '';
    aMaskToGrid[2] = '';
    
    if (glb_Convidado)
    {
		aHeaderToGrid[3] = 'Contato';
		aHeaderToGrid[4] = 'Cargo';
		aHeaderToGrid[5] = 'Documento';
		aHeaderToGrid[6] = 'Telefone';
		aHeaderToGrid[7] = 'EMail';
		aHeaderToGrid[8] = 'ContatoID';
		
		aFieldsToGrid[3] = 'Contato';
		aFieldsToGrid[4] = 'ContatoCargo';
		aFieldsToGrid[5] = 'ContatoDocumento';
		aFieldsToGrid[6] = 'ContatoTelefone';
		aFieldsToGrid[7] = 'ContatoEMail';
		aFieldsToGrid[8] = 'ContatoID';
		
		aMaskToGrid[3] = '';
		aMaskToGrid[4] = '';
		aMaskToGrid[5] = '';
		aMaskToGrid[6] = '';
		aMaskToGrid[7] = '';
		aMaskToGrid[8] = '';
		
		aHoldCols[0] = 8;
	}

    headerGrid(fg, aHeaderToGrid, aHoldCols);

    fillGridMask(fg,dsoPesq, aFieldsToGrid, aMaskToGrid);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        btnOK.disabled = false;
    }    
    else
        btnOK.disabled = true;    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
    
}
