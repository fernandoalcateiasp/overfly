﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_Inferior : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        private int empresaID = Convert.ToInt32(HttpContext.Current.Request.Params["empresaID"]);
        private int nLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nLinguaLogada"]);
        private string idToFind = Convert.ToString(HttpContext.Current.Request.Params["idToFind"]);
        private string DATE_SQL_PARAM = Convert.ToString(HttpContext.Current.Request.Params["DATE_SQL_PARAM"]);
        private string glb_sFiltroInsricoes = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltroInsricoes"]);
        private string aDataCmbFiltro = Convert.ToString(HttpContext.Current.Request.Params["aDataCmbFiltro"]);
        private string controlID = Convert.ToString(HttpContext.Current.Request.Params["controlID"]);

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controlID)
                {
                    case "Inscricoes":
                        ReportGrid_Inscricoes();
                        break;
                }
            }
        }

        public void ReportGrid_Inscricoes()
        {
            DateTime DataHoje = DateTime.Now;
            string param = "<Format>dd/mm/yyy</Format>";
            string _data = DataHoje.ToString("dd_MM_yyyy");

            if (nLinguaLogada != 246)
            {
                param = "<Format>mm/dd/yyy</Format>";
                _data = DataHoje.ToString("MM_dd_yyyy");
            }
                
            string Title = "Inscrições_" + sEmpresaFantasia + "_" + _data;

            string strSQL = "";

            if (aDataCmbFiltro == "41094")
            {
                strSQL = "SELECT a.EventoInscricaoID AS [ID], b.Fantasia AS Empresa, a.Nome, a.Cargo, a.Documento," +
                    "a.Telefone, a.Email, a.Observacao ";
            }
            else
            {
                strSQL = "SELECT a.EventoInscricaoID AS [ID], b.Fantasia AS Empresa, a.Nome, a.Cargo, a.Documento," +
                    "a.Telefone, a.Email, CONVERT(VARCHAR(10), a.Data, " + DATE_SQL_PARAM + ") AS Data, a.Observacao, (CASE a.Compareceu WHEN 1 THEN 'Sim' ELSE 'Não' END) AS Compareceu ";
            }

            strSQL += "FROM Eventos_Inscricoes a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " +
                        "WHERE " + glb_sFiltroInsricoes + " ( (a.EventoID = " + idToFind + ") AND (a.EmpresaID=b.PessoaID) ) " +
                        "ORDER BY (SELECT TOP 1 Fantasia FROM Pessoas WITH(NOLOCK) WHERE a.EmpresaID = PessoaID), a.Nome ";

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

            int Datateste = 0;
            bool nulo = false;

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            if (dsFin.Tables["Query1"].Rows.Count == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, false);

                Relatorio.CriarObjColunaNaTabela("ID", "ID", true, "ID");
                Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "Empresa");
                Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Nome", "Nome", true, "Nome");
                Relatorio.CriarObjCelulaInColuna("Query", "Nome", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Cargo", "Cargo", true, "Cargo");
                Relatorio.CriarObjCelulaInColuna("Query", "Cargo", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Documento", "Documento", true, "Documento");
                Relatorio.CriarObjCelulaInColuna("Query", "Documento", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Telefone", "Telefone", true, "Telefone");
                Relatorio.CriarObjCelulaInColuna("Query", "Telefone", "DetailGroup");

                Relatorio.CriarObjColunaNaTabela("Email", "Email", true, "Email");
                Relatorio.CriarObjCelulaInColuna("Query", "Email", "DetailGroup");

                if (aDataCmbFiltro != "41094")
                {
                    Relatorio.CriarObjColunaNaTabela("Data", "Data", true, "Data");
                    Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", Decimais: param);
                }

                Relatorio.CriarObjColunaNaTabela("Observacao", "Observacao", false, "8");
                Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup");


                if (aDataCmbFiltro != "41094")
                {
                    Relatorio.CriarObjColunaNaTabela("Compareceu", "Compareceu", true, "Compareceu");
                    Relatorio.CriarObjCelulaInColuna("Query", "Compareceu", "DetailGroup");
                }
                                
                Relatorio.TabelaEnd();

                Relatorio.CriarPDF_Excel(Title, 2);
            }        
        }
    }
}
