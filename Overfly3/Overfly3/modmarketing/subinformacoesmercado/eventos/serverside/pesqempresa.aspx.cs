using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subinformacoesmercado.eventos.serverside
{
	public partial class pesqempresa : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
		}

		protected string strOperator = " >= ";
	    
		protected string toFind = Constants.STR_EMPTY;
		protected string strToFind
		{
			get { return toFind; }
			set
			{ 
				if(value != null) toFind = value.Trim();

				if(toFind.StartsWith("%") || toFind.EndsWith("%"))
				{
					strOperator = " LIKE ";
				}
			}
		}
	    
		protected Integer convidado = Constants.INT_ZERO;
		protected Integer Convidado
		{
			get { return convidado; }
			set { if(value != null) convidado = value; }
		}

		protected string SQL
		{
			get
			{
				string sql = "";

				// Somente Confirmados
				if(convidado.intValue() == 0)
				{
					sql += "SELECT TOP 100 PessoaID, Fantasia, Nome " +
                             "FROM Pessoas WITH(NOLOCK)  " +
							 "WHERE (Nome " + strOperator + "'" + strToFind + "' ";

							 if(strOperator.Equals(" LIKE "))
								sql += "OR Fantasia " + strOperator + "'" + strToFind + "') ";
							 else
								sql += ") ";
							 
					
					sql += "ORDER BY Nome ";
				}
				// Somente Convidados
				else
				{
					sql += "SELECT DISTINCT TOP 100 Pessoas.PessoaID, Pessoas.Fantasia, Pessoas.Nome, " + 
							 "Contatos.ContatoID AS ContatoID, PessoasContatos.Nome AS Contato, " +
                             "Contatos.Cargo AS ContatoCargo, dbo.fn_Pessoa_Documento(Contatos.ContatoID, NULL, NULL, 101, 102, 0) AS ContatoDocumento, " +
                             "dbo.fn_Pessoa_Telefone(Contatos.ContatoID, 119, 121,1,0,NULL) AS ContatoTelefone, " + 
							 "dbo.fn_Pessoa_URL(Contatos.ContatoID, 124, NULL) AS ContatoEMail " +
                             "FROM Pessoas Pessoas WITH(NOLOCK), RelacoesPessoas RelPessoas WITH(NOLOCK), RelacoesPessoas_Contatos Contatos WITH(NOLOCK), " +
                             "Pessoas PessoasContatos WITH(NOLOCK) " + 
							 "WHERE (Pessoas.Nome " + strOperator + "'" + strToFind + "' ";

					if(strOperator.Equals(" LIKE "))
					   sql += "OR Pessoas.Fantasia " + strOperator + "'" + strToFind + "') ";
					else
					   sql += ") ";
					         
					sql += " AND (Pessoas.PessoaID=RelPessoas.SujeitoID AND RelPessoas.TipoRelacaoID=21) AND " + 
						"(Contatos.RelacaoID=RelPessoas.RelacaoID) AND (PessoasContatos.PessoaID=Contatos.ContatoID) " +
						"ORDER BY Pessoas.Nome ";
				}

				return sql;
			}
		}
	}
}
