using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subinformacoesmercado.pesquisas.serverside
{
	public partial class incluirpesquisas : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(SQL);

			WriteResultXML(DataInterfaceObj.getRemoteData("select '' as fldresp"));
		}

		protected Integer pesquisaID = Constants.INT_ZERO;
		protected Integer nPesquisaID
		{
			get { return pesquisaID; }
			set { if(value != null) pesquisaID = value; }
		}

		protected Integer pesqPessoaID = Constants.INT_ZERO;
		protected Integer PesqPessoaID
		{
			get { return pesqPessoaID; }
			set { if(value != null) pesqPessoaID = value; }
		}

		protected java.lang.Boolean incluir = Constants.FALSE;
		protected java.lang.Boolean bIncluir
		{
			get { return incluir; }
			set { if(value != null) incluir = value; }
		}

		protected Integer[] pessoaID = Constants.INT_EMPTY_SET;
		protected Integer[] nPessoaID
		{
			get { return pessoaID; }
			set { if(value != null) pessoaID = value; }
		}

		protected Integer[] pesqPerAlternativaID = Constants.INT_EMPTY_SET;
		protected Integer[] PesqPerAlternativaID
		{
			get { return pesqPerAlternativaID; }
			set { if(value != null) pesqPerAlternativaID = value; }
		}

		protected string[] contatoID = Constants.STR_EMPTY_SET;
		protected string[] nContatoID
		{
			get { return contatoID; }
			set
			{
				if(value != null) contatoID = value;
				
				for(int i = 0; i < contatoID.Length; i++)
				{
					if(contatoID[i].Equals("")) contatoID[i] = " NULL ";
				}
			}
		}

		protected Integer[] proprietarioID = Constants.INT_EMPTY_SET;
		protected Integer[] nProprietarioID
		{
			get { return proprietarioID; }
			set { if(value != null) proprietarioID = value; }
		}

		protected Integer[] pesqPessoaID2 = Constants.INT_EMPTY_SET;
		protected Integer[] nPesqPessoaID
		{
			get { return pesqPessoaID2; }
			set { if (value != null) pesqPessoaID2 = value; }
		}

		protected string SQL
		{
			get
			{
				string sql = "";

				for(int i = 0; i < nContatoID.Length;i++)
				{
					if(bIncluir.booleanValue())
					{
						sql += "EXEC sp_Pesquisa_PessoasIncluir " +
							pesquisaID + ", " +
							pessoaID[i] + ", " +
							contatoID[i] + ", " +
							nProprietarioID[i];
					}
					else
					{
						sql += "DELETE Pesquisas_Pessoas WHERE PesqPessoaID=" + nPesqPessoaID[i];
					}
				}

				return sql;
			}
		}
	}
}
