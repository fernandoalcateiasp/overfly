using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subinformacoesmercado.pesquisas.serverside
{
	public partial class pesquisapesquisas : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PesquisaPessoasPesquisarExternal());
		}

		protected Integer pesquisaID = Constants.INT_ZERO;
		protected Integer PesquisaID
		{
			get { return pesquisaID; }
			set { if(value != null) pesquisaID = value; }
		}

		protected Integer incluir = Constants.INT_ZERO;
		protected Integer Incluir
		{
			get { return incluir; }
			set { if(value != null) incluir = value; }
		}

		protected Integer empresaID = null;
		protected Integer EmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		protected Integer finalizado1 = Constants.INT_ZERO;
		protected Integer Finalizado1
		{
			get { return finalizado1; }
			set { if(value != null) finalizado1 = value; }
		}

		protected Integer finalizado2 = Constants.INT_ZERO;
		protected Integer Finalizado2
		{
			get { return finalizado2; }
			set { if(value != null) finalizado2 = value; }
		}
	    
		protected Integer paisID = null;
		protected Integer PaisID
		{
			get { return paisID; }
			set { if(value != null) paisID = value; }
		}

		protected Integer ufID = null;
		protected Integer UFID
		{
			get { return ufID; }
			set { if(value != null) ufID = value; }
		}
	    
		protected Integer proprietarioID = null;
		protected Integer ProprietarioID
		{
			get { return proprietarioID; }
			set { if(value != null) proprietarioID = value; }
		}
	    
		protected Integer classificacaoID = null;
		protected Integer ClassificacaoID
		{
			get { return classificacaoID; }
			set { if(value != null) classificacaoID = value; }
		}

		protected Integer top = Constants.INT_ZERO;
		protected Integer Top
		{
			get { return top; }
			set { if(value != null) top = value; }
		}
	    
		protected string pesquisa = Constants.STR_EMPTY;
		protected string Pesquisa
		{
			get { return pesquisa; }
			set { if (value != null) pesquisa = value; }
		}

		protected DataSet PesquisaPessoasPesquisarExternal()
		{
			ProcedureParameters[] param = new ProcedureParameters[11];

			param[0] = new ProcedureParameters(
				"@PesquisaID",
				SqlDbType.Int,
				pesquisaID.intValue());

			param[1] = new ProcedureParameters(
				"@Incluir",
				SqlDbType.Bit,
				incluir.intValue() == 1 ? 1 : 0);

			param[2] = new ProcedureParameters(
				"@Finalizada1",
				SqlDbType.Bit,
				finalizado1.intValue() == 1 ? 1 : 0);

			param[3] = new ProcedureParameters(
				"@Finalizada2",
				SqlDbType.Bit,
				finalizado2.intValue() == 1 ? 1 : 0);

			param[4] = new ProcedureParameters(
				"@EmpresaID",
				SqlDbType.Int,
				empresaID != null ? (object) empresaID.intValue() : DBNull.Value);

			param[5] = new ProcedureParameters(
				"@PaisID",
				SqlDbType.Int,
				paisID != null ? (object) paisID.intValue() : DBNull.Value);

			param[6] = new ProcedureParameters(
				"@UFID",
				SqlDbType.Int,
				ufID != null ? (object) ufID.intValue() : DBNull.Value);

			param[7] = new ProcedureParameters(
				"@ProprietarioID",
				SqlDbType.Int,
				proprietarioID != null ? (object) proprietarioID.intValue() : DBNull.Value);

			param[8] = new ProcedureParameters(
				"@ClassificacaoID",
				SqlDbType.Int,
				classificacaoID != null ? (object) classificacaoID.intValue() : DBNull.Value);

			param[9] = new ProcedureParameters(
				"@Top",
				SqlDbType.Int,
				top.intValue());

			param[10] = new ProcedureParameters(
				"@Pesquisa",
				SqlDbType.VarChar,
				pesquisa);
			param[10].Length = 100;

			return DataInterfaceObj.execQueryProcedure("sp_Pesquisa_PessoasPesquisar_External", param);
		}
	}
}
