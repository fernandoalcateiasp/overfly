using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subinformacoesmercado.pesquisas.serverside
{
	public partial class verificacaopesquisa : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT dbo.fn_Pesquisa_Verifica(" + 
					nPesquisaID + "," + 
					estadoDe + "," +
					estadoPara + 
				") AS Verificacao"
			));
		}

		protected Integer pesquisaID = Constants.INT_ZERO;
		protected Integer nPesquisaID
		{
			get { return pesquisaID; }
			set { if(value != null) pesquisaID = value; }
		}

		protected Integer estadode = Constants.INT_ZERO;
		protected Integer estadoDe
		{
			get { return estadode; }
			set { if(value != null) estadode = value; }
		}

		protected Integer estadopara = Constants.INT_ZERO;
		protected Integer estadoPara
		{
			get { return estadopara; }
			set { if(value != null) estadopara = value; }
		}
	}
}
