using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;
using java.lang;

namespace Overfly3.modmarketing.subinformacoesmercado.pesquisas.serverside
{
	public partial class gravaralternativas : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(PesquisaPessoasRespostas());
		}

		protected Integer pesqPessoaID = Constants.INT_ZERO;
		protected Integer PesqPessoaID
		{
			get { return pesqPessoaID; }
			set { if(value != null) pesqPessoaID = value; }
		}

		protected Integer[] pesqPerAlternativaID = Constants.INT_EMPTY_SET;
		protected Integer[] PesqPerAlternativaID
		{
			get { return pesqPerAlternativaID; }
			set { if(value != null) pesqPerAlternativaID = value; }
		}

		protected string[] resposta = Constants.STR_EMPTY_SET;
		protected string[] Resposta
		{
			get { return resposta; }
			set { if(value != null) resposta = value; }
		}

		protected string resultado = Constants.STR_EMPTY;

		protected DataSet PesquisaPessoasRespostas()
		{
			ProcedureParameters[] param = new ProcedureParameters[4];

			param[0] = new ProcedureParameters("@PesqPessoaID", SqlDbType.Int, PesqPessoaID.intValue());
			param[1] = new ProcedureParameters("@PesqPerAlternativaID", SqlDbType.Int, DBNull.Value);
			param[2] = new ProcedureParameters("@Resposta", SqlDbType.Int, DBNull.Value);
			param[3] = new ProcedureParameters("@Resultado", SqlDbType.Int, DBNull.Value);
			param[3].Length = 100;

			for(int i = 0; i < pesqPerAlternativaID.Length; i++)
			{
				param[1].Data = PesqPerAlternativaID[i];
				param[2].Data = Resposta[i];

				DataInterfaceObj.execNonQueryProcedure("sp_Pesquisa_PessoasRespostas", param);

				resultado += param[3].Data + "\n";
			}

			return DataInterfaceObj.getRemoteData("select " + resultado + " as fldresp");
		}
	}
}
