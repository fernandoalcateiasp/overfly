using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;

namespace Overfly3.modmarketing.subinformacoesmercado.pesquisas.serverside
{
	public partial class gravapesquisapessoa : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(
				"UPDATE Pesquisas_Pessoas SET " +
					"dtData='" + dtInicio + "', " +
					"Observacao='" + Observacao + "', " +
					"ContatoID=" + ContatoID + "," +
					"ProprietarioID=" + ProprietarioID + " " +
				"WHERE PesqPessoaID=" + PesqPessoaID
			);

			WriteResultXML(DataInterfaceObj.getRemoteData("select NULL as fldresp"));
		}

		protected string pesqPessoaID = Constants.STR_EMPTY;
		protected string PesqPessoaID
		{
			get { return pesqPessoaID; }
			set { if(value != null) pesqPessoaID = value; }
		}

		protected string observacao = Constants.STR_EMPTY;
		protected string Observacao
		{
			get { return observacao; }
			set { if(value != null) observacao = value; }
		}

		protected string inicio = Constants.STR_EMPTY;
		protected string dtInicio
		{
			get { return inicio; }
			set { if(value != null) inicio = value; }
		}

		protected string contatoID = Constants.STR_EMPTY;
		protected string ContatoID
		{
			get { return contatoID; }
			set
			{
				if(value != null) contatoID = value;

				if (int.Parse(contatoID) == 0) contatoID = " NULL ";
			}
		}

		protected string proprietarioID = Constants.STR_EMPTY;
		protected string ProprietarioID
		{
			get { return proprietarioID; }
			set
			{
				if (value != null) proprietarioID = value;

				if (int.Parse(proprietarioID) == 0) proprietarioID = " NULL ";
			}
		}
	}
}
