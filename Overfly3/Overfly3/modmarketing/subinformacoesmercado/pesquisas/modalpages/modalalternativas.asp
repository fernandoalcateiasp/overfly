<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalalternativasHtml" name="modalalternativasHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subinformacoesmercado/pesquisas/modalpages/modalalternativas.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nPesqPerguntaID, strSQL, PesquisaID, rsData

nPesqPerguntaID = 0
PesquisaID = 0
strSQL = ""

For i = 1 To Request.QueryString("nPesqPerguntaID").Count    
    nPesqPerguntaID = Request.QueryString("nPesqPerguntaID")(i)
Next

For i = 1 To Request.QueryString("PesquisaID").Count    
    PesquisaID = Request.QueryString("PesquisaID")(i)
Next

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=CellChanged>
<!--
js_modalalternativas_CellChanged (fg, arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalalternativas_ValidateEdit();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalalternativas_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalalternativasKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalalternativasDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalalternativasBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
if (fg_ExecEvents == true)
        js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);    
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 if (fg_ExecEvents == true)
		js_fg_EnterCell (fg);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 if (fg_ExecEvents == true)
		js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR="fg" EVENT="AfterSort">
<!--
 fg_AfterSort();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR="fg" EVENT="BeforeSort">
<!--
 fg_BeforeSort();
//-->
</SCRIPT>

</head>

<body id="modalalternativasBody" name="modalalternativasBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>

    <div id="divControls" name="divControls" class="divGeneral">
	    <p id="lblPerguntaID" name="lblPerguntaID" class="lblGeneral">Perguntas</p>
	    <select id="selPerguntaID" name="selPerguntaID" class="fldGeneral">
<%
    strSQL = "SELECT a.PesqperguntaID AS fldID, a.Pergunta AS fldName " & _
			 "FROM Pesquisas_Perguntas a WITH(NOLOCK) " & _
             "WHERE a.PesquisaID=" & CStr(PesquisaID) & " " & _
             "ORDER BY a.Ordem"
	
	Set rsData = Server.CreateObject("ADODB.Recordset")
    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	While (Not rsData.EOF)

	 	Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "

		If (CLng(rsData.Fields("fldID").Value) = CLng(nPesqPerguntaID)) Then
			Response.Write " SELECTED "
		End If

		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf

	    rsData.MoveNext
	Wend

	rsData.Close
	Set rsData = Nothing
%>
		</select>
    </div>
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnIncluir" name="btnIncluir" value="Incluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	<input type="button" id="btnExcluir" name="btnExcluir" value="Excluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	<input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	<input type="button" id="btnRefresh" name="btnRefresh" value="Refresh" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
            
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
</body>

</html>
