/********************************************************************
modalpesquisa.js

Library javascript para o modalpesquisa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_sResultado = '';
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_bFirstFill = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_nInterfaceMode = 1; // 1->Pesquisar, 2->Preencher pesquisa
var glb_nPerguntas = 0;
var glb_nCurrentQuestion = 1;
var glb_PesquisaID = 0;
var glb_PesqPessaoReposicionaID = -1;
var glb_RepostarReadOnly = true;

// Dados do grid .RDS  
var dsoGrid = new CDatatransport("dsoGrid");
// Gravacao de dados do grid .RDS  
var dsoGrava = new CDatatransport("dsoGrava");
var dsoCombo = new CDatatransport("dsoCombo");
var dsoCmbUF = new CDatatransport("dsoCmbUF");
var dsoCmbPais = new CDatatransport("dsoCmbPais");
var dsoCmbProprietario = new CDatatransport("dsoCmbProprietario");
var dsoPesquisa = new CDatatransport("dsoPesquisa");
var dsoContatos = new CDatatransport("dsoContatos");
var dsoPerguntas = new CDatatransport("dsoPerguntas");
var dsoAlternativas = new CDatatransport("dsoAlternativas");
var dsoDataInicio = new CDatatransport("dsoDataInicio");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalpesquisaBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalpesquisaDblClick(grid, Row, Col)
js_modalpesquisaKeyPress(KeyAscii)
js_modalpesquisa_ValidateEdit()
js_modalpesquisa_BeforeEdit(grid, row, col)
js_modalpesquisa_AfterEdit(Row, Col)
js_fg_AfterRowColmodalpesquisa (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpesquisaKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpesquisa_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpesquisa_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalpesquisa_AfterEdit(Row, Col)
{
	setupBtnsFromGridState();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalpesquisa(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return true;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

	glb_PesquisaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'txtRegistroID.value');
    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalpesquisaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_bFirstFill = true;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	//secText(translateTerm('Ativar Financeiros', null), 1);
	nRegistroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'txtRegistroID.value');
	sEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'txtEstadoID.value');
	sData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'txtData.value');
	sPesquisa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'txtPesquisa.value');
	
	var sCaption = 'ID: ' + nRegistroID + '   Est: ' + sEstadoID + '   Data: ' + sData +
		'   Pesquisa: ' + sPesquisa;
	
	secText(sCaption, 1);
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK
    btnOK.disabled = true;
	//btnFillGrid.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

	adjustElementsInForm([['lblIncluir','chkIncluir',3,1],
						  ['lblFinalizado1','chkFinalizado1',3,1,-7],
						  ['lblFinalizado2','chkFinalizado2',3,1,-17],
						  ['lblEmpresaID','selEmpresaID',14,1,-7],
						  ['lblPaisID','selPaisID',10,1],
						  ['lblUFID','selUFID',6,1],
						  ['lblProprietarioID','selProprietarioID',14,1],
						  ['lblClassificacaoID','selClassificacaoID',9,1],
						  ['lblTop','selTop',6,1],
						  ['lblPesquisaDetail','txtPesquisaDetail',10,1],
						  ['btnListar','btn',FONT_WIDTH * 8,1, 5],
						  ['lblTotalPessoas','txtTotalPessoas',10,2],
						  ['lblTotalFinalizadas','txtTotalFinalizadas',10,2],
						  ['btnIncluirDetalhar','btn',FONT_WIDTH * 8,2,5],
						  ['btnExcluir','btn',FONT_WIDTH * 8,2,5]], null, null, true);

	txtTotalPessoas.readOnly = true;
	txtTotalFinalizadas.readOnly = true;

	selPaisID.onchange = selPaisID_onchange;
	selEmpresaID.onchange = selEmpresaID_onchange;
	chkIncluir.onclick = chkIncluir_onclick;
	btnIncluirDetalhar.onclick = btnIncluirDetalhar_onclick;
	btnExcluir.onclick = btnExcluir_onclick;
	btnListar.onclick = btnListar_onclick;
	selContatoID.onchange = selContatoID_onchange;
	txtPesquisaDetail.onkeypress = txtFields_onKeyPress;

	adjustElementsInForm([['lblPessoa','txtPessoa',18,1],
		['lblContatoID','selContatoID',19,1],
		['lblObservacao2','txtObservacao2',17,1],
		['lblTelefone','txtTelefone',14,1],
		['lblEMail','txtEMail',21,1],
		['lblProprietario2ID','selProprietario2ID',19,2],
		['lbldtInicio','txtdtInicio',10,2],
		['lbldtFim','txtdtFim',10,2],
		['btnGravarPesquisa','btn',FONT_WIDTH * 8,2],
		['btnVoltar','btn',FONT_WIDTH * 8,2,5],
		['lblNumeroPerguntas','txtNumeroPerguntas',10,3],
		['lblPergunta','txtPergunta',82,3],
		['lblInstrucao','txtInstrucao',93,4]], null, null, true);

	adjustElementsInForm([['btnPessoaAnterior', 'btn', FONT_WIDTH * 12, 1, 10,-10],
						  ['btnPerguntaAnterior', 'btn', FONT_WIDTH * 12, 1, 185],
						  ['btnPerguntaProxima', 'btn', FONT_WIDTH * 12, 1, 5],
						  ['btnPessoaProxima', 'btn', FONT_WIDTH * 12, 1, 166]], null, null, true);

	btnPessoaAnterior.style.height = 26;
	btnPerguntaAnterior.style.height = 26;
	btnPerguntaProxima.style.height = 26;
	btnPessoaProxima.style.height = 26;
	
	txtPessoa.readOnly = true;
	txtdtFim.readOnly = true;
	txtTelefone.readOnly = true;
	txtEMail.readOnly = true;
	txtNumeroPerguntas.readOnly = true;
	txtPergunta.readOnly = true;
	txtInstrucao.readOnly = true;

	btnVoltar.onclick = btnVoltar_onclick;
	
	btnPessoaAnterior.onclick = btnPessoaAnterior_onclick;
	btnPessoaProxima.onclick = btnPessoaProxima_onclick;
	btnPerguntaAnterior.onclick = btnPerguntaAnterior_onclick;
	btnPerguntaProxima.onclick = btnPerguntaProxima_onclick;
	btnGravarPesquisa.onclick = btnGravarPesquisa_onclick;
	
	adjustLabelsCombos();

    
    // Ajusta o divControls
    with (divDetail01.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 20;
        width = 790;    
        height = 95; 
    }

    // Ajusta o divControls
    with (divDetail02.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 20;
        width = 790;    
        height = 185; 
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divDetail01.currentStyle.top, 10) + parseInt(divDetail01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // ajusta o divFG
    with (divFG2.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divDetail02.currentStyle.top, 10) + parseInt(divDetail02.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = 190;
    }
    
    with (fg2.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }
    
    // Ajusta o divControls
    with (divDetail02Buttons.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = divFG2.offsetTop + divFG2.offsetHeight;
        width = 790;    
        height = 45; 
    }

    // Reposiciona botao OK
    with (btnOK)
    {
		style.visibility = 'hidden';
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;    
    fg2.Redraw = 2;
}

function chkIncluir_onclick()
{
	fg.Rows = 1;
	if (chkIncluir.checked)
	{
		btnIncluirDetalhar.value = 'Incluir';
		chkFinalizado1.checked = false;
		chkFinalizado2.checked = false;
		lblFinalizado1.style.visibility = 'hidden';
		chkFinalizado1.style.visibility = 'hidden';
		lblFinalizado2.style.visibility = 'hidden';
		chkFinalizado2.style.visibility = 'hidden';
		btnExcluir.style.visibility = 'hidden';
	}
	else
	{
		btnIncluirDetalhar.value = 'Detalhar';
		lblFinalizado1.style.visibility = 'inherit';
		chkFinalizado1.style.visibility = 'inherit';
		lblFinalizado2.style.visibility = 'inherit';
		chkFinalizado2.style.visibility = 'inherit';
		btnExcluir.style.visibility = 'inherit';
	}
	
	setBtnsStateByRight();
}

function txtFields_onKeyPress()
{
    if ( event.keyCode == 13 )
	{
		btnListar_onclick();
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        ;
    }
    else if (controlID == 'btnFillGrid')
    {
		listar();
    }
    else if (controlID == 'btnImprimir')
    {
		imprimeGrid();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    loadQuestions();
}

function loadQuestions()
{
	setConnection(dsoPerguntas);
    dsoPerguntas.SQL = 'SELECT a.* ' +
		'FROM Pesquisas_Perguntas a WITH(NOLOCK) ' +
		'WHERE a.PesquisaID = ' + glb_PesquisaID + ' ' +
		'ORDER BY a.Ordem';

    dsoPerguntas.ondatasetcomplete = dsoPerguntas_DSC;
    dsoPerguntas.Refresh();
}

function dsoPerguntas_DSC()
{
	glb_nPerguntas = dsoPerguntas.recordset.RecordCount();
	changeInterfaceMode(1);
	showExtFrame(window, true);
	selEmpresaID_onchange();
	chkIncluir_onclick();
	setBtnsStateByRight();
	window.focus();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    
	btnOK.disabled = true;

	return null;
	if ((bHasRowsInGrid) && ((selEstadoParaID.value > 0) || (selSituacaoCobrancaParaID.value > 0)))
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				btnOK.disabled = false;
				break;
			}	
		}
	}
	
	//btnFillGrid.disabled = false;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
	{
		window.clearInterval(glb_OcorrenciasTimerInt);
		glb_OcorrenciasTimerInt = null;
	}

	var strPars = '?PesquisaID=' + escape(glb_PesquisaID);
	strPars += '&Incluir=' + escape(chkIncluir.checked ? 1 : 0);
	strPars += '&EmpresaID=' + escape(selEmpresaID.value);
	strPars += '&Finalizado1=' + escape(chkFinalizado1.checked ? 1 : 0);
	strPars += '&Finalizado2=' + escape(chkFinalizado2.checked ? 1 : 0);
	strPars += '&PaisID=' + escape(selPaisID.value);
	strPars += '&UFID=' + escape(selUFID.value);
	strPars += '&ProprietarioID=' + escape(selProprietarioID.value);
	strPars += '&ClassificacaoID=' + escape(selClassificacaoID.value);
	strPars += '&Top=' + escape(selTop.value);
	strPars += '&Pesquisa=' + escape(txtPesquisaDetail.value);

    dsoPesquisa.URL = SYS_ASPURLROOT + '/modmarketing/subinformacoesmercado/pesquisas/serverside/pesquisapesquisas.aspx' + strPars;
    dsoPesquisa.ondatasetcomplete = fillGridData_DSC;
    dsoPesquisa.refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    
    if (!(dsoPesquisa.recordset.BOF && dsoPesquisa.recordset.EOF))
    {
		txtTotalPessoas.value = dsoPesquisa.recordset['TotalPessoas'].value;
		txtTotalFinalizadas.value = dsoPesquisa.recordset['TotalFinalizadas'].value;
    }
    else
    {
		txtTotalPessoas.value = '';
		txtTotalFinalizadas.value = '';
    }

    headerGrid(fg,['Pessoa', 'ID', 'Contato', 'ProprietarioID', 'Proprietario', 'In�cio', 'Finaliza��o', 'Observa��o', 'OK', 'ContatoID', 'PesqPessoaID'], [3,9,10]);

    fillGridMask(fg,dsoPesquisa,['Pessoa*', 'PessoaID*', 'Contato*', 'ProprietarioID*', 'Proprietario*', 'dtInicio*', 'dtFinalizacao*', 'Observacao*', 'OK', 'ContatoID*', 'PesqPessoaID*'],
							 ['', '', '', '', '', '', '', '', '', '', ''],
							 ['', '', '', '', '', '', '', '', '', '', '']);
    fg.Redraw = 0;
    alignColsInGrid(fg,[1]);
    
	//fg.FrozenCols = 4;

	paintCellsSpecialyReadOnly();
	fg.ExplorerBar = 5;
	
	dsoPesquisa.recordset.moveFirst();
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
    
	fg.Redraw = 2;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.LeftCol = 0;

	setBtnsStateByRight();

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
        
		if (glb_PesqPessaoReposicionaID != -1)
		{
			for (i=1; i<fg.Rows; i++)
			{
				if (glb_PesqPessaoReposicionaID == getCellValueByColKey(fg, 'PesqPessoaID*', i))
				{
					fg.Row = i;
					break;
				}
			}
		}
    }
    
    glb_PesqPessaoReposicionaID = -1;
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid(bIncluir)
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
	glb_sResultado = '';

    lockControlsInModalWin(true);
    
    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?nPesquisaID=' + escape(glb_PesquisaID);
				strPars += '&bIncluir=' + escape(bIncluir ? 1 : 0);
			}
		
			nDataLen++;
			strPars += '&nPessoaID=' + escape(getCellValueByColKey(fg, 'PessoaID*', i));
			strPars += '&nContatoID=' + escape(getCellValueByColKey(fg, 'ContatoID*', i));
			strPars += '&nProprietarioID=' + escape(getCellValueByColKey(fg, 'ProprietarioID*', i));
			strPars += '&nPesqPessoaID=' + escape(getCellValueByColKey(fg, 'PesqPessoaID*', i));
		}	
	}

	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subinformacoesmercado/pesquisas/serverside/incluirpesquisas.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		if ((dsoGrava.recordset['fldresp'].value != null) &&
			(dsoGrava.recordset['fldresp'].value != ''))
		{
			if ( window.top.overflyGen.Alert(dsoGrava.recordset['fldresp'].value) == 0 )
				return null;
				
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
			return null;
		}
	}
	
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function selFormaPagamentoID_onchange()
{
	adjustLabelsCombos();
	fg.Rows = 1;
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function adjustLabelsCombos()
{
	return null;
    setLabelOfControl(lblFormaPagamentoID, selFormaPagamentoID.value);
    setLabelOfControl(lblEstadoDeID, selEstadoDeID.value);
    setLabelOfControl(lblSituacaoCobrancaDeID, selSituacaoCobrancaDeID.value);
    setLabelOfControl(lblEstadoParaID, selEstadoParaID.value);
    setLabelOfControl(lblSituacaoCobrancaParaID, selSituacaoCobrancaParaID.value);
}

function listar()
{
	if (!verificaData())
		return null;
		
	fillGridData();
}

function verificaData()
{
	var sDataInicio = trimStr(txtPesquisaID.value);
	var sDataFim = trimStr(txtdtFim.value);

	var bDataIsValid = true;

	if (sDataInicio != '')
		bDataIsValid = chkDataEx(sDataInicio);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data in�cio inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sDataInicio != '' )    
			txtPesquisaID.focus();
		
		return false;
	}

	bDataIsValid = true;

	if (sDataFim != '')
		bDataIsValid = chkDataEx(sDataFim);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data fim inv�lida.') == 0 )
            return null;
        
        window.focus();
        
        if ( sDataFim != '' )    
			txtdtFim.focus();
		
		return false;
	}

	return true;
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

function selPaisID_onchange()
{
	fillCmbUF();
}

function selEmpresaID_onchange()
{
	fillCmbPais();
	fillCmbProprietario();
}

function selContatoID_onchange()
{
	txtEMail.value = selContatoID.options[selContatoID.selectedIndex].getAttribute('Email', 1);
	txtTelefone.value = selContatoID.options[selContatoID.selectedIndex].getAttribute('Telefone', 1);
}

function fillCmbPais()
{
	var nEmpresaID = selEmpresaID.value;
	
	setConnection(dsoCmbPais);
    dsoCmbPais.SQL = 'SELECT  0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ' +
			'SELECT DISTINCT d.LocalidadeID AS fldID, d.Localidade AS fldName ' +
		'FROM RelacoesPessoas a WITH(NOLOCK) ' +
			'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) ' +
			'INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON (b.PessoaID = c.PessoaID) ' +
			'INNER JOIN Localidades d WITH(NOLOCK) ON (c.PaisID = d.LocalidadeID) ' +
		'WHERE (a.ObjetoID = ' + nEmpresaID + ' AND a.TipoRelacaoID = 21 AND a.EstadoID = 2 AND ' +
			'b.EstadoID = 2 AND c.Ordem = 1 AND c.EndFaturamento = 1) ' +
		'ORDER BY fldName';

    dsoCmbPais.ondatasetcomplete = dsoCmbPais_DSC;
    dsoCmbPais.Refresh();
}

function dsoCmbPais_DSC()
{
	fillCmb(selPaisID, dsoCmbPais);
	selPaisID_onchange();
	//fillCmbUF();
}

function fillCmbProprietario()
{
	var nEmpresaID = selEmpresaID.value;
	
	setConnection(dsoCmbProprietario);
    dsoCmbProprietario.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ' +
		'SELECT DISTINCT c.PessoaID AS fldID, c.Fantasia AS fldName ' +
		'FROM RelacoesPessoas a WITH(NOLOCK) ' +
			'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) ' +
			'INNER JOIN Pessoas c WITH(NOLOCK) ON (a.ProprietarioID = c.PessoaID) ' +
		'WHERE (a.ObjetoID = ' + nEmpresaID + ' AND a.TipoRelacaoID = 21 AND a.EstadoID IN (2) AND ' +
			'b.EstadoID IN (2)) ' +
		'ORDER BY fldName';

    dsoCmbProprietario.ondatasetcomplete = dsoCmbProprietario_DSC;
    dsoCmbProprietario.Refresh();
}

function dsoCmbProprietario_DSC()
{
	fillCmb(selProprietarioID, dsoCmbProprietario);
	
	//DireitoEspecifico
    //Pesquisas->Pesquisas->SUP->Modal Pesquisa (B1)
    //23460 SFS-Grupo Pesquisa -> A2=0 -> Seta o combo Proprietario com o id do usuario logado caso o mesmo seja uma op��o do combo. Desabilita o como Proprietario. Caso o combo n�o esteja com o usuario logado o bot�o Listar ficar� desabilitado.
	if (glb_nA2 == 0)
	{
		selOptByValueInSelect(getHtmlId(), 'selProprietarioID', glb_nUserID);
		selProprietarioID.disabled = true;
		
		if (selProprietarioID.value != glb_nUserID)
			btnListar.disabled = true;	
	}
}

function fillCmbUF()
{
	var nPaisID = selPaisID.value;
	
	if (nPaisID == '')
		return null;
	
	setConnection(dsoCmbUF);
    dsoCmbUF.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				  'UNION ' +
				  'SELECT a.LocalidadeID AS fldID, a.CodigoLocalidade2 AS fldName ' +
                  'FROM Localidades a WITH(NOLOCK) ' +
                  'WHERE a.TipoLocalidadeID = 204 AND a.LocalizacaoID = ' + nPaisID + ' ' +
                  'ORDER BY fldName';
    
    dsoCmbUF.ondatasetcomplete = fillCmbUF_DSC;
    dsoCmbUF.Refresh();
}

function fillCmbUF_DSC()
{
	fillCmb(selUFID, dsoCmbUF);
}

function fillCmb(cmb, dso, At1, At2)
{
	clearComboEx([cmb.id]);
	if (! (dso.recordset.BOF && dso.recordset.EOF))
	{
		dso.recordset.moveFirst();
		while (! dso.recordset.EOF )
		{
			optionStr = dso.recordset['fldName'].value;
			optionValue = dso.recordset['fldID'].value;
			var oOption = document.createElement("OPTION");
			oOption.text = optionStr;
			oOption.value = optionValue;
			
			if (At1 != null)
				oOption.setAttribute(At1, dso.recordset[At1].value);

			if (At2 != null)
				oOption.setAttribute(At2, dso.recordset[At2].value);
			
			cmb.add(oOption);
			dso.recordset.MoveNext();
		}
    }
    
    cmb.disabled = (cmb.options.length == 0);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalpesquisaBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalpesquisaDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

function btnIncluirDetalhar_onclick()
{
	if (chkIncluir.checked)
		saveDataInGrid(true);
	else
		changeInterfaceMode(2, true);
}

function btnExcluir_onclick()
{
    var _retMsg = window.top.overflyGen.Confirm('Deseja realmente excluir este(s) registro(s)?');
	        
	if ( (_retMsg == 0) || (_retMsg == 2) )
	    return null;

	saveDataInGrid(false);
}

function btnListar_onclick()
{
	fillGridData();
}

function changeInterfaceMode(nNewMode, posicionaPointer)
{
	var i=0;
	var nProprietarioID = 0;

	// Pesquisa
	if (nNewMode == 1)
	{
		divDetail01.style.visibility = 'inherit';
		divFG.style.visibility = 'inherit';
		divDetail02.style.visibility = 'hidden';
		divFG2.style.visibility = 'hidden';
		divDetail02Buttons.style.visibility = 'hidden';
	}
	else if (nNewMode == 2)
	{
		// Posiciona o dso
		if (posicionaPointer)
		{
			dsoPesquisa.recordset.moveFist();
			
			while (!dsoPesquisa.recordset.EOF)
			{
				if (dsoPesquisa.recordset['PesqPessoaID'].value == getCellValueByColKey(fg, 'PesqPessoaID*', fg.Row))
					break;
					
				dsoPesquisa.recordset.moveNext();
			}
		}
		
		setConnection(dsoDataInicio);
		dsoDataInicio.SQL = 'SELECT CONVERT(VARCHAR(10), a.dtData, 103) AS V_dtInicio ' +
			'FROM Pesquisas_Pessoas a WITH(NOLOCK) ' +
			'WHERE a.PesqPessoaID = ' +  dsoPesquisa.recordset['PesqPessoaID'].value;

		dsoDataInicio.ondatasetcomplete = NewMode2_DSC;
		dsoDataInicio.Refresh();
	}
}

function NewMode2_DSC()
{
	// Vai p/ primeira pergunta
	glb_nCurrentQuestion = 1;
	dsoPerguntas.recordset.moveFirst();
	txtPessoa.value = dsoPesquisa.recordset['Pessoa'].value;
	txtPessoa.value = (txtPessoa.value == 'null' ? '' : txtPessoa.value);
	
	txtObservacao2.value = dsoPesquisa.recordset['Observacao'].value;
	txtObservacao2.value = (txtObservacao2.value == 'null' ? '' : txtObservacao2.value);
	
	txtdtInicio.value = dsoDataInicio.recordset['V_dtInicio'].value;
	txtdtInicio.value = (txtdtInicio.value == 'null' ? '' : txtdtInicio.value);
	
	txtdtFim.value = dsoPesquisa.recordset['dtFinalizacao'].value;
	txtdtFim.value = (txtdtFim.value == 'null' ? '' : txtdtFim.value);
	
	glb_RepostarReadOnly = (!dsoPesquisa.recordset['PermiteGravacao'].value);
	
	nProprietarioID = dsoPesquisa.recordset['ProprietarioID'].value;
	
	clearComboEx([selProprietario2ID.id]);
	for (i=0; i<selProprietarioID.options.length; i++)
	{
		var oOption = document.createElement("OPTION");
		oOption.text = selProprietarioID.options[i].text;
		oOption.value = selProprietarioID.options[i].value;
		
		if (oOption.value == nProprietarioID)
			oOption.selected = true;
		
		selProprietario2ID.add(oOption);
	}
	
	//DireitoEspecifico
    //Pesquisas->Pesquisas->SUP->Modal Pesquisa (B1)
    //23460 SFS-Grupo Pesquisa -> A2=0 -> Desabilita o como Proprietario. 
	if (glb_nA2 == 0)
		selProprietario2ID.disabled = true;

	divDetail01.style.visibility = 'hidden';
	divFG.style.visibility = 'hidden';
	divDetail02.style.visibility = 'inherit';
	divFG2.style.visibility = 'inherit';
	divDetail02Buttons.style.visibility = 'inherit';
	fillQuestionFields();
	fillContato();
}

function btnVoltar_onclick()
{
	glb_PesqPessaoReposicionaID = dsoPesquisa.recordset['PesqPessoaID'].value;
	changeInterfaceMode(1, true);
	btnListar_onclick();
}

function fillContato()
{
	lockControlsInModalWin(true);
	var nEmpresaID = selEmpresaID.value;
	var sFiltroEmpresa = '';
	var nPessoaID = dsoPesquisa.recordset['PessoaID'].value;

	if (selEmpresaID.selectedIndex == 0)
		sFiltroEmpresa = ' AND dbo.fn_Empresa_Sistema(a.ObjetoID)=1 ';
	else
		sFiltroEmpresa = ' AND a.ObjetoID =' + selEmpresaID.value + ' ';

	setConnection(dsoContatos);

    dsoContatos.SQL = 'SELECT DISTINCT 0 AS fldID, SPACE(0) AS fldName, ' +
		'dbo.fn_Pessoa_Telefone(' + nPessoaID + ', 119, 121,1,0,NULL) AS Telefone, dbo.fn_Pessoa_URL(' + nPessoaID + ', 124, NULL) AS Email ' +
    'UNION ALL SELECT DISTINCT c.PessoaID AS fldID, c.Fantasia AS fldName, ' +
		'dbo.fn_Pessoa_Telefone(c.PessoaID, 119, 121,1,0,NULL) AS Telefone, dbo.fn_Pessoa_URL(c.PessoaID, 124, NULL) AS Email ' +
		'FROM RelacoesPessoas a WITH(NOLOCK) INNER JOIN RelacoesPessoas_Contatos b WITH(NOLOCK) ON a.RelacaoID=b.RelacaoID ' +
			'INNER JOIN Pessoas c WITH(NOLOCK) ON b.ContatoID=c.PessoaID ' +
		'WHERE a.SujeitoID=' + nPessoaID + ' AND a.TipoRelacaoID=21 ' + sFiltroEmpresa + ' ' +
		'ORDER BY fldName';

    dsoContatos.ondatasetcomplete = dsoContatos_DSC;
    dsoContatos.Refresh();
}

function dsoContatos_DSC()
{
	lockControlsInModalWin(false);
	var nContatoID = dsoPesquisa.recordset['ContatoID'].value;
	fillCmb(selContatoID, dsoContatos, 'Email', 'Telefone');

	if (nContatoID != '')
		selOptByValueInSelect(getHtmlId(), 'selContatoID', nContatoID);

	selContatoID_onchange();
}

function fillQuestionFields()
{
	txtNumeroPerguntas.value = glb_nCurrentQuestion + '/' + glb_nPerguntas;
	txtPergunta.value = dsoPerguntas.recordset['Pergunta'].value;
	txtInstrucao.value = dsoPerguntas.recordset['Instrucao'].value;
	txtInstrucao.value = (txtInstrucao.value.toUpperCase() == 'NULL' ? '' : txtInstrucao.value);
	loadAlternativas();
}

function btnPerguntaAnterior_onclick()
{
	skipQuestion(-1);
}

function btnPerguntaProxima_onclick()
{
	if (!validateCurrentAlternativa())
	{
		if ( window.top.overflyGen.Alert('Responda todas alternativas para prosseguir.') == 0 )
			return null;
		return null;
	}
	else if (glb_RepostarReadOnly)
		skipQuestion(1);
	else
		saveDataInGrid2();
		
}

function skipQuestion(way)
{
	btnPerguntaProxima.value = 'Pr�xima pergunta';
	// Proxima
	if (way == 1)
	{
		if (glb_nCurrentQuestion == glb_nPerguntas)
		{
			btnPerguntaProxima.value = 'Concluir';

			//if ( window.top.overflyGen.Alert('Pesquisa conclu�da.') == 0 )
				//return null;
				
			var _retMsg_ = window.top.overflyGen.Confirm('Pesquisa conclu�da.\nDeseja ir para a pr�xima pessoa?');				
			
			if (_retMsg_ == 1)
				btnPessoaProxima_onclick();

			return null;
		}

		glb_nCurrentQuestion++;
		
		if (glb_nCurrentQuestion == glb_nPerguntas)
			btnPerguntaProxima.value = 'Concluir';
		
		dsoPerguntas.recordset.moveNext();
		fillQuestionFields();
	}
	else if (way == -1)
	{
		if (glb_nCurrentQuestion == 1)
		{
			if ( window.top.overflyGen.Alert('Voc� ja est� na primeira pergunta.') == 0 )
				return null;

			return null;
		}

		glb_nCurrentQuestion--;
		dsoPerguntas.recordset.movePrevious();
		fillQuestionFields();
	}
}

function loadAlternativas()
{
	lockControlsInModalWin(true);
	var nPesqPerguntaID = dsoPerguntas.recordset['PesqPerguntaID'].value;
	var nAlternativas = dsoPerguntas.recordset['QuantidadeRespostasAlternativa'].value;
	
	if ((nAlternativas == null) || (nAlternativas <= 1))
		nAlternativas = 2;

	setConnection(dsoAlternativas);
    dsoAlternativas.SQL = 'SELECT a.*, ISNULL(b.Resposta, 0) AS Resposta ';
    
    for (i=1; i<=nAlternativas; i++)
		dsoAlternativas.SQL += ', 0 AS Op' + i.toString();

dsoAlternativas.SQL += ' FROM Pesquisas_Perguntas_Alternativas a WITH(NOLOCK) LEFT OUTER JOIN Pesquisas_Pessoas_Respostas b WITH(NOLOCK) ' +
			'ON a.PesqPerAlternativaID=b.PesqPerAlternativaID AND b.PesqPessoaID=' + dsoPesquisa.recordset['PesqPessoaID'].value + ' ' +
		'WHERE a.PesqPerguntaID=' + nPesqPerguntaID + ' ' +
		'ORDER BY a.Ordem';

    dsoAlternativas.ondatasetcomplete = dsoAlternativas_DSC;
    dsoAlternativas.Refresh();
}

function dsoAlternativas_DSC()
{
	var nAlternativas = dsoPerguntas.recordset['QuantidadeRespostasAlternativa'].value;
	var aHeader = new Array();
	var aFields = new Array();
	
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;
	
    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2);
    
    fg2.FontSize = '8';
    fg2.FrozenCols = 0;
    
	var aHeader = new Array();
	var aFields = new Array();
	var aHiddenCols = new Array();
	
	aHeader[0] = '#';
	aHeader[1] = 'Alternativas';
	
	aFields[0] = 'Ordem*';
	aFields[1] = 'Alternativa*';

    for (i=2; i<nAlternativas+2; i++)
    {
		aHeader[i] = (i-1).toString();
		aFields[i] = 'Op' + (i-1);
    }

	aHeader[i] = 'PesqPerAlternativaID';
	aFields[i] = 'PesqPerAlternativaID';

	aHeader[i+1] = 'Resposta';
	aFields[i+1] = 'Resposta';
	
	aHiddenCols[0] = i;
	aHiddenCols[1] = i+1;
    
    headerGrid(fg2,aHeader, aHiddenCols);

    fillGridMask(fg2,dsoAlternativas,aFields,[''],['']);

	for (i=2; i<nAlternativas+2; i++)
	{
		fg2.ColDataType(i) = 11;
	}
	
	var nResposta = 0;
	for (i=1; i<fg2.Rows; i++)
	{
		nResposta = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Resposta'));
		if (nResposta != 0)
			fg2.TextMatrix(i, getColIndexByColKey(fg2, 'Op' + nResposta.toString())) = true;
	}
	
    fg2.Redraw = 0;
    alignColsInGrid(fg2,[0]);
    
	//fg2.FrozenCols = 4;

	paintCellsSpecialyReadOnly();
	fg2.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // Merge de Colunas
    fg2.MergeCells = 4;
    fg2.MergeCol(0) = true;
    fg2.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
    
	fg2.Redraw = 2;
    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0,fg2.Cols-1);
    
	for (i=2; i<nAlternativas+2; i++)
	{
		fg2.ColWidth(i) = 360;
	}

    fg2.Editable = (!glb_RepostarReadOnly);
    // se tem linhas no grid, coloca foco no grid
    if (fg2.Rows > 1)
    {
        window.focus();
        if (fg2.Editable)
			fg2.focus();
    }                
}

function btnPessoaAnterior_onclick()
{
	skipPerson(-1);
}

function btnPessoaProxima_onclick()
{
	skipPerson(1);
}

function skipPerson(way)
{
	// Move para proxima pessoa
	if (way == 1)
	{
		if (!dsoPesquisa.recordset.EOF)
			dsoPesquisa.recordset.moveNext();

		if (dsoPesquisa.recordset.EOF)
		{
			dsoPesquisa.recordset.moveFirst();
			dsoPesquisa.recordset.moveLast();
			if ( window.top.overflyGen.Alert('Voc� ja est� na �ltima pessoa.') == 0 )
				return null;
			
			return null;
		}
	}
	// Move para pessoa anterior
	else if (way == -1)
	{
		if (!dsoPesquisa.recordset.BOF)
			dsoPesquisa.recordset.movePrevious();

		if (dsoPesquisa.recordset.BOF)
		{
			dsoPesquisa.recordset.moveLast();
			dsoPesquisa.recordset.moveFirst();
		
			if ( window.top.overflyGen.Alert('Voc� ja est� na primeira pessoa.') == 0 )
				return null;
			
			return null;
		}
	}

	skipQuestion(0);
	changeInterfaceMode(2, false);
}

function validateCurrentAlternativa()
{
	var bRetVal = false;
	var i=0;
	
	if (fg2.Rows == 0)
		return bRetVal;

	var nAlternativas = dsoPerguntas.recordset['QuantidadeRespostasAlternativa'].value;
	
	// Trap nas colunas
	if (nAlternativas == 1)
	{
		for (i=1; i<fg2.Rows; i++)
		{
			if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Op1')) != 0)
				bRetVal = true;
		}
	}
	else if (nAlternativas > 1)
	{
		for (i=1; i<fg2.Rows; i++)
		{
			bRetVal = true;
			bOK = false;

			for (j=2; j<nAlternativas+2; j++)
			{
				if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Op' + (j-1).toString())) != 0)
				{
					bOK = true;
					break;
				}
			}
			
			if (!bOK)
			{
				bRetVal = false;
				break;
			}
		}
	}
	
	return bRetVal;
}


/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid2()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
	glb_sResultado = '';

    lockControlsInModalWin(true);
    
    for (i=1; i<fg2.Rows; i++)
    {
		nBytesAcum=strPars.length;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
				strPars = '';
				nDataLen = 0;
			}

			strPars += '?PesqPessoaID=' + escape(dsoPesquisa.recordset['PesqPessoaID'].value);
		}

		nDataLen++;
		strPars += '&PesqPerAlternativaID=' + escape(getCellValueByColKey(fg2, 'PesqPerAlternativaID', i));
		strPars += '&Resposta=' + escape(getResposta(i));
	}

	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendData2ToServer();
}

function sendData2ToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subinformacoesmercado/pesquisas/serverside/gravaralternativas.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer2_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			skipQuestion(1);
			//glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		//glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer2_DSC()
{
	if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		if ((dsoGrava.recordset['fldresp'].value != null) &&
			(dsoGrava.recordset['fldresp'].value != null))
		{
			if ( window.top.overflyGen.Alert(dsoGrava.recordset['fldresp'].value) == 0 )
				return null;
				
			lockControlsInModalWin(false);
			skipQuestion(1);
			return null;
		}
	}
	
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendData2ToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}


function getResposta(rowAlternativa)
{
	var nAlternativas = dsoPerguntas.recordset['QuantidadeRespostasAlternativa'].value;
	retVal = 0;

	for (j=2; j<nAlternativas+2; j++)
	{
		if (fg2.ValueMatrix(rowAlternativa, getColIndexByColKey(fg2, 'Op' + (j-1).toString())) != 0)
		{
			retVal = j-1;
			break;
		}
	}

	return retVal;
}

function js_fg2_AfterEdit(row, col)
{
	var nAlternativas = dsoPerguntas.recordset['QuantidadeRespostasAlternativa'].value;
	
	// Trap nas colunas
	if (nAlternativas == 1)
	{
		for (i=1; i<fg2.Rows; i++)
		{
			if (row != i)
				fg2.TextMatrix(i, getColIndexByColKey(fg2, 'Op1')) = false;
		}
	}
	else if (nAlternativas > 1)
	{
		for (j=2; j<nAlternativas+2; j++)
		{
			if (j != col)
				fg2.TextMatrix(row, getColIndexByColKey(fg2, 'Op' + (j-1).toString())) = false;
		}
	}
}

function btnGravarPesquisa_onclick()
{
	if (chkDataEx(txtdtInicio.value) != true)
    {
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;

        window.focus();    
        txtdtInicio.focus();
        return null;    
    }
    
    var strPars = '?PesqPessoaID=' + escape(dsoPesquisa.recordset['PesqPessoaID'].value);
    strPars += '&Observacao=' + escape(txtObservacao2.value);
    strPars += '&dtInicio=' + escape(normalizeDate_DateTime(txtdtInicio.value, true));
    strPars += '&ContatoID=' + escape(selContatoID.value);
    strPars += '&ProprietarioID=' + escape(selProprietario2ID.value);
    
    dsoPesquisa.recordset['Observacao'].value = txtObservacao2.value;
    dsoPesquisa.recordset['ContatoID'].value = selContatoID.value;
    dsoPesquisa.recordset['ProprietarioID'].value = selProprietario2ID.value;
    
	lockControlsInModalWin(true);
    dsoGrava.URL = SYS_ASPURLROOT + '/modmarketing/subinformacoesmercado/pesquisas/serverside/gravapesquisapessoa.aspx' + strPars;
    dsoGrava.ondatasetcomplete = gravaPesquisaPessoa_DSC;
    dsoGrava.refresh();
}

function gravaPesquisaPessoa_DSC()
{
	lockControlsInModalWin(false);	
}

function setBtnsStateByRight()
{
	btnExcluir.disabled = true;
	btnIncluirDetalhar.disabled = true;
	chkIncluir.disabled = false;

	if (fg.Rows > 1)
	{
		btnExcluir.disabled = false;
		btnIncluirDetalhar.disabled = false;
	}

    //DireitoEspecifico
    //Pesquisas->Pesquisas->SUP->Modal Pesquisa (B1)
    //23460 SFS-Grupo Pesquisa -> A2=0 || A2=0 -> Desabilita o bot�o Excluir e o check box Incluir. 
	if ((glb_nA1 == 0) || (glb_nA2 == 0))
	{
		btnExcluir.disabled = true;
		chkIncluir.disabled = true;
	}
}
