
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalpesquisaHtml" name="modalpesquisaHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modmarketing/subinformacoesmercado/pesquisas/modalpages/modalpesquisa.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmarketing/subinformacoesmercado/pesquisas/modalpages/modalpesquisa.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat, nContextoID, A1, A2
Dim rsData, strSQL
Dim nEmpresaID, nPaisID

Set rsData = Server.CreateObject("ADODB.Recordset")

sCaller = ""
nContextoID = 0
nEmpresaID = 0
nPaisID = 0
A1 = 0
A2 = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

For i = 1 To Request.QueryString("EmpresaID").Count    
    nEmpresaID = Request.QueryString("EmpresaID")(i)
Next

For i = 1 To Request.QueryString("PaisID").Count    
    nPaisID = Request.QueryString("PaisID")(i)
Next

For i = 1 To Request.QueryString("A1").Count    
    A1 = Request.QueryString("A1")(i)
Next

For i = 1 To Request.QueryString("A2").Count    
    A2 = Request.QueryString("A2")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nA1 = " & CStr(A1) & ";"
Response.Write vbcrlf

Response.Write "var glb_nA2 = " & CStr(A2) & ";"
Response.Write vbcrlf

If (nContextoID = 9111) Then
	Response.Write "var glb_nTipoFinanceiroID = 1001;"
Else
	Response.Write "var glb_nTipoFinanceiroID = 1002;"
End If

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalpesquisa_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalpesquisa_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalpesquisaKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalpesquisa_BeforeEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalpesquisaDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalpesquisaBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalpesquisa (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<!-- fg2 -->
<SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=AfterEdit>
<!--
	js_fg2_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalpesquisaBody" name="modalpesquisaBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <div id="divDetail01" name="divDetail01" class="divGeneral">
        <p id="lblIncluir" name="lblIncluir" class="lblGeneral">I</p>
        <input type="checkbox" id="chkIncluir" name="chkIncluir" class="fldGeneral" title="Incluir?"></input>
        <p id="lblFinalizado1" name="lblFinalizado1" class="lblGeneral"></p>
        <input type="checkbox" id="chkFinalizado1" name="chkFinalizado1" class="fldGeneral" title="Finalizado?"></input>
        <p id="lblFinalizado2" name="lblFinalizado2" class="lblGeneral">F</p>
        <input type="checkbox" id="chkFinalizado2" name="chkFinalizado2" class="fldGeneral" title="Finalizado?"></input>
	    <p id="lblEmpresaID" name="lblEmpresaID" class="lblGeneral">Empresa</p>
	    <select id="selEmpresaID" name="selEmpresaID" class="fldGeneral">
<%
    strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
			"UNION " & _
			"SELECT a.PessoaID AS fldID, a.Fantasia AS fldName " & _
			 "FROM Pessoas a WITH(NOLOCK) " & _
             "WHERE a.EstadoID = 2 AND a.PessoaID <= 100 AND dbo.fn_Empresa_Sistema(a.PessoaID) = 1 " & _
             "ORDER BY fldName"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "
	    
		If (CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaID)) Then
			Response.Write " SELECTED "
		End If
	    
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	'Set rsData = Nothing
%>
	    </select>
	    <p id="lblPaisID" name="lblPaisID" class="lblGeneral">Pa�s</p>
	    <select id="selPaisID" name="selPaisID" class="fldGeneral"></select>
	    <p id="lblUFID" name="lblUFID" class="lblGeneral">UF</p>
	    <select id="selUFID" name="selUFID" class="fldGeneral"></select>
	    <p id="lblProprietarioID" name="lblProprietarioID" class="lblGeneral">Propriet�rio</p>
	    <select id="selProprietarioID" name="selProprietarioID" class="fldGeneral"></select>
	    <p id="lblClassificacaoID" name="lblClassificacaoID" class="lblGeneral">Classifica��o</p>
	    <select id="selClassificacaoID" name="selClassificacaoID" class="fldGeneral">
<%
    strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Indice, 0 AS Ordem1 " & _
			 "UNION " & _
			 "SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName, 1 AS Indice, Ordem AS Ordem1 " & _
			 "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
             "WHERE a.EstadoID=2 AND a.TipoID=29 " & _
             "ORDER BY Indice, Ordem1"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34) & " "
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData.MoveNext
	Wend
	
	rsData.Close
	Set rsData = Nothing
%>
	    </select>
	    <p id="lblTop" name="lblTop" class="lblGeneral">Top</p>
	    <select id="selTop" name="selTop" class="fldGeneral">
			<option value="25">25</option>
			<option value="50">50</option>
			<option value="100">100</option>
			<option value="200">200</option>
			<option value="300">300</option>
	    </select>
		<p id="lblPesquisaDetail" name="lblPesquisaDetail" class="lblGeneral">Pesquisa</p>
		<input type="text" id="txtPesquisaDetail" name="txtPesquisaDetail" class="fldGeneral"></input>
		<input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		
		<p id="lblTotalPessoas" name="lblTotalPessoas" class="lblGeneral">Total Pessoas</p>
		<input type="text" id="txtTotalPessoas" name="txtTotalPessoas" class="fldGeneral"></input>
		<p id="lblTotalFinalizadas" name="lblTotalFinalizadas" class="lblGeneral">Total Finalizadas</p>
		<input type="text" id="txtTotalFinalizadas" name="txtTotalFinalizadas" class="fldGeneral"></input>
		<input type="button" id="btnIncluirDetalhar" name="btnIncluirDetalhar" value="Incluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnExcluir" name="btnExcluir" value="Excluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    </div>    

	<div id="divDetail02" name="divDetail02" class="divGeneral">
	    <p id="lblPessoa" name="lblPessoa" class="lblGeneral">Pessoa</p>
	    <input type="text" id="txtPessoa" name="txtPessoa" class="fldGeneral"></input>
	    <p id="lblContatoID" name="lblContatoID" class="lblGeneral">Contato</p>
	    <select id="selContatoID" name="selContatoID" class="fldGeneral"></select>
	    <p id="lblObservacao2" name="lblObservacao2" class="lblGeneral">Observa��o</p>
	    <input type="text" id="txtObservacao2" name="txtObservacao2" class="fldGeneral"></input>
	    <p id="lblTelefone" name="lblTelefone" class="lblGeneral">Telefone</p>
	    <input type="text" id="txtTelefone" name="txtTelefone" class="fldGeneral"></input>
	    <p id="lblEMail" name="lblEMail" class="lblGeneral">E-Mail</p>
	    <input type="text" id="txtEMail" name="txtEMail" class="fldGeneral"></input>
	    <p id="lblProprietario2ID" name="lblProprietario2ID" class="lblGeneral">Propriet�rio</p>
	    <select id="selProprietario2ID" name="selProprietario2ID" class="fldGeneral"></select>
	    <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">In�cio</p>
	    <input type="text" id="txtdtInicio" name="txtdtInicio" class="fldGeneral"></input>
	    <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Finaliza��o</p>
	    <input type="text" id="txtdtFim" name="txtdtFim" class="fldGeneral"></input>
	    <input type="button" id="btnGravarPesquisa" name="btnGravarPesquisa" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	    <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	    <p id="lblNumeroPerguntas" name="lblNumeroPerguntas" class="lblGeneral">N�mero</p>
	    <input type="text" id="txtNumeroPerguntas" name="txtNumeroPerguntas" class="fldGeneral"></input>
	    <p id="lblPergunta" name="lblPergunta" class="lblGeneral">Pergunta</p>
	    <input type="text" id="txtPergunta" name="txtPergunta" class="fldGeneral"></input>
	    <p id="lblInstrucao" name="lblInstrucao" class="lblGeneral">Instru��o</p>
	    <input type="text" id="txtInstrucao" name="txtInstrucao" class="fldGeneral"></input>
	</div>

	<div id="divDetail02Buttons" name="divDetail02Buttons" class="divGeneral">
		<input type="button" id="btnPessoaAnterior" name="btnPessoaAnterior" value="Pessoa anterior" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnPerguntaAnterior" name="btnPerguntaAnterior" value="Pergunta anterior" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnPerguntaProxima" name="btnPerguntaProxima" value="Pr�xima Pergunta" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnPessoaProxima" name="btnPessoaProxima" value="Pr�xima Pessoa" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
	</div>

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <!--<img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>-->
        <!--<img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>-->
        <!--<img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>-->
    </div>    
    <!-- Div do grid -->

    <div id="divFG2" name="divFG2" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg2" name="fg2" VIEWASTEXT>
        </object>
        <!--<img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>-->
        <!--<img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>-->
        <!--<img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>-->
    </div>    
            
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
