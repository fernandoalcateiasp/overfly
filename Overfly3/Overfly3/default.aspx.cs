﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;
using System.Net.NetworkInformation;

namespace Overfly3
{
    public partial class _default : System.Web.UI.OverflyPage
    {
        protected DataSet rsDataUso;
        protected DataSet rsDataDicTerms;
        protected DataSet Aplicacao;
        protected DataSet Login;
        protected DataSet SQLAplicacoes;

        protected Integer sredirecionado = new Integer(0);

        public Integer Redirecionado
        {
            get { return sredirecionado; }
            set { sredirecionado = value; }
        }
        protected int redirecionado
        {
            get { return sredirecionado.intValue(); }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

        }
    }
}