﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Configuration;
using System.Data.SqlClient;
using Microsoft.Reporting.WebForms;
using System.IO;
using System.Text;
using System.Drawing;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.ComponentModel;
using System.Xml.Serialization;
using System.Xml;
using System.Net.Mail;
//using Outlook = Microsoft.Office.Interop.Outlook;
using System.Security;
using OVERFLYSVRCFGLib;
using System.Diagnostics;
using System.Collections;
using System.Web.Security;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using iTextSharp.text;
using iTextSharp.text.pdf;
using System.Runtime.InteropServices;

//using Office = Microsoft.Office;

namespace Overfly3.PrintJet
{

    public class ClsReport
    {
        //Variáveis ----------------------------------------------------------------------------------------------------------------------------------------------------

        // string sLocalPath = appPath();
        string sLocalPath = "";
        string _NomeRel = string.Empty;
        string[,] _sqlQuery;
        string[,] _arrIndexKey;
        string _TipoSaida = string.Empty;
        DataTable _DT = new DataTable();
        //DataSet _ds = new DataSet();
        DataSet ds = new DataSet();
        DataSet dataset = new DataSet();
        DataSet DsTable = new DataSet();
        int Sort = 0;
        int Index = 0;
        int SetNodeBody;
        int SetNodeHeader;
        int SetNodeFooter;
        int getRelModel = 0;
        int NewTab = 0;
        int NewFields = 0;
        int contTotalFild = 0;
        string listColum = "";
        string _TipoObjeto = string.Empty;
        string _ValorObjeto = string.Empty;
        string _NomeQuery = string.Empty;
        string _tPositX = "0";
        string _tPositY = "0";
        string _Tam = string.Empty;
        string _Cor = string.Empty;
        string _Format = string.Empty;
        string _data = string.Empty;
        string _LandScape = string.Empty;
        string _Language = string.Empty;
        string Alinhamento = string.Empty;
        string ContColumHier = string.Empty;
        int CountTablixMembers = 0;
        int CountColumnTablixMembers = 0;
        int PaddingUltiCol = 0;
        int ContTab = 0;
        int ContImg = 0;
        double contTamColFinal = 0;
        int ContRow = 0;
        Object oOverflySvrCfg = null;
        //string xmlModel = "\\PrintJet\\RelModelo.xml";
        private int currentPageIndex;
        private string tmpFileName = string.Empty;
        private List<Stream> streamList;

        private MemoryStream xmlModeloRel;
        string strConn = "";
        string smtpServer = "";
        int relCont = 0;
        bool TabCopy = false;
        bool _HeaderColor;
        string _TamFonte = "8";
        string _LargColum = "";
        string _AddCell = "";
        string _RowBold = "";
        string _ColorRow = "";
        bool RowGroup1 = false;
        bool RowGroup2 = false;
        bool RowGroup3 = false;
        bool RowGroup4_Sub1 = false;
        bool RowGroup4_Sub2 = false;
        bool RowGroup4_Sub3 = false;
        int ContRowGroup = 0;
        int contLinhas = 0;
        int contMergeCels = 0;
        string[] arrGroups;

        List<string> ListGroups = new List<string>();

        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);

        public ClsReport()
        {
            oOverflySvrCfg = comCreateObject("OverflySvrCfg.OverflyMTS");
            strConn = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).DotNetDataBaseStrConn(DataInterfaceObj.ApplicationName);

            smtpServer = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).EmailServerIP(DataInterfaceObj.ApplicationName);

            sLocalPath = appPath();
        }

        //Prepara Relatório Vazio e seu respectivo dataset -------------------------------------------------------------------------------------------------------------------------------------------
        public void CriarRelatório(string NomeRel, string[,] QueryString, string Format, string Landscape, string TamHeader = "Default", string MargX = "Default", string MargY = "Default", bool Header_Color = false, string TamFooter = "Default", bool Footer_Color = false, string MargYBottom = "Default") // QueryString [Nome,SqlQuery]
        {
            //if (HttpContext.Current.Session["ContRel"] != null)
            //relCont = (int)HttpContext.Current.Session["ContRel"];

            //relCont ++;

            //HttpContext.Current.Session["ContRel"] = relCont;

            _NomeRel = NomeRel;
            _sqlQuery = QueryString;
            // _TipoSaida = TipoSaida;
            _Format = Format;
            _LandScape = Landscape;

            if (_Format == "BRA")
            {
                _Language = "pt-BR";
            }
            else
            {
                _Language = "en-US";
            }

            SetNodeBody = 0;
            SetNodeHeader = 0;
            SetNodeFooter = 0;

            dataset = MontaDataSet(_sqlQuery);
            dataset.DataSetName = "DataSetTemp1";

            for (int aux = 0; aux < dataset.Tables.Count; aux++)
            {
                ds.Tables.Add(dataset.Tables[aux].Copy());
            }

            HttpContext.Current.Session["Query"] = ds;

            if (_NomeRel == "Invoice")
            {
                AjustaRelatório(_Format, _LandScape, "0", "0.7", "0");
                _TamFonte = "7";
            }
            else
            {
                AjustaRelatório(_Format, _LandScape, TamHeader, MargX, MargY, Header_Color, TamFooter, Footer_Color, MargYBottom);
            }

        }

        private void AjustaRelatório(string Format, string Land_Port, string HeaderHeight, string Margem_X, string Margem_Y, bool Header_Color = false, string FooterHeight = "Default", bool Footer_Color = false, string Margem_Y_Bottom = "Default")
        {

            string _MargemLeft = (Margem_X == "Default" ? "0" : Margem_X);
            string _MargemTop = (Margem_Y == "Default" ? "0" : Margem_Y);
            string _HeaderHeight = (HeaderHeight == "Default" ? "2" : HeaderHeight);
            string _FooterHeight = (FooterHeight == "Default" ? "NotShow" : FooterHeight);
            string _MargemBottom = (Margem_Y_Bottom == "Default" ? "0" : Margem_Y_Bottom);

            string _Footer_Color;
            if (Footer_Color)
            {
                _Footer_Color = "#bdbdbd";
            }
            else
            {
                _Footer_Color = "White";
            }

            if (_FooterHeight != "NotShow")
            {
                _FooterHeight = "    <PageFooter>" +
                                    "      <Height>" + _FooterHeight + "cm</Height>" +
                                    "      <PrintOnFirstPage>true</PrintOnFirstPage>" +
                                    "      <PrintOnLastPage>true</PrintOnLastPage>" +
                                    "      <Style>" +
                                    "        <BackgroundColor>" + _Footer_Color + "</BackgroundColor>" +
                                    "      </Style>" +
                                    "    </PageFooter>";
            }


            string _RelWidth = "";
            string _RelHeight = "";
            string _Header_Color = "";

            if (Format == "BRA")
            {
                if (Land_Port != "Landscape")
                {
                    _RelWidth = "21";
                    _RelHeight = "29.7";
                }
                else
                {
                    _RelWidth = "29.7";
                    _RelHeight = "21";
                }
            }
            else
            {
                if (Land_Port != "Landscape")
                {
                    _RelWidth = "21.59";
                    _RelHeight = "27.94";
                }
                else
                {
                    _RelWidth = "27.94";
                    _RelHeight = "21.59";
                }

            }

            if (Header_Color)
            {
                _Header_Color = "#bdbdbd";
            }
            else
            {
                _Header_Color = "White";
            }

            XmlDocument xmlDoc = new XmlDocument();

            XmlNodeList node;

            XmlNode node2;

            if (Format == "Excel")
            {
                _RelWidth = (contTamColFinal.ToString() == "0" ? "5" : contTamColFinal.ToString());
                _RelHeight = "29.7";

                xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
                xmlDoc.Load(xmlModeloRel);
                xmlModeloRel.Dispose();
            }
            else
            {
                string xmlModel = "\\PrintJet\\RelModelo.xml";

                if (Land_Port == "Landscape")
                {
                    xmlModel = "\\PrintJet\\RelModeloLand.xml";
                }

                if (ds.Tables[0].Rows.Count == 0)
                {
                    xmlModel = "\\PrintJet\\RelModeloVazio.xml";
                }

                xmlDoc.Load(sLocalPath + xmlModel);
            }

            node = xmlDoc.GetElementsByTagName("Page");

            node2 = node.Item(0);



            node2.InnerXml = "    <PageHeader>" +
                                "      <Height>" + _HeaderHeight + "cm</Height>" +
                                "      <PrintOnFirstPage>true</PrintOnFirstPage>" +
                                "      <PrintOnLastPage>true</PrintOnLastPage>" +

                                /*" <ReportItems>" +
                                " <Textbox Name='Paginas'>" +
                                " <CanGrow>true</CanGrow>" +
                                " <KeepTogether>true</KeepTogether>" +
                                "<Paragraphs>" +
                                " <Paragraph>" +
                                "  <TextRuns>" +
                                "    <TextRun>" +
                                "       <Value>=Globals.PageNumber &amp; </Value>" + // ' / ' &amp; Globals.TotalPages</Value>" +
                                "        <Style />" +
                                "       </TextRun>" +
                                "      </TextRuns>" +
                                "       <Style />" +
                                "  </Paragraph>" +
                                "  </Paragraphs>" +
                                "  <Left>29.32434cm</Left>" +
                                " <Height>0.6cm</Height>" +
                                "<Width>2.5cm</Width>" +
                                "<Style>" +
                                "<Border>" +
                                "  <Style>None</Style>" +
                                " </Border>" +
                                "  <PaddingLeft>2pt</PaddingLeft>" +
                                "   <PaddingRight>2pt</PaddingRight>" +
                                "    <PaddingTop>2pt</PaddingTop>" +
                                "     <PaddingBottom>2pt</PaddingBottom>" +
                                "    </Style>" +
                                "   </Textbox>" +
                                "  </ReportItems>" +
                                */
                                "      <Style>" +
                                "        <BackgroundColor>" + _Header_Color + "</BackgroundColor>" +
                                "      </Style>" +
                                "    </PageHeader>" +
                                _FooterHeight +
                                "    <PageHeight>" + _RelHeight + "cm</PageHeight>" +
                                "    <PageWidth>" + _RelWidth + "cm</PageWidth>" +
                                "    <LeftMargin>" + _MargemLeft + "cm</LeftMargin>" +
                                "    <RightMargin>0cm</RightMargin>" +
                                "    <TopMargin>" + _MargemTop + "cm</TopMargin>" +
                                "    <BottomMargin>" + _MargemBottom + "cm</BottomMargin>" +
                                "    <ColumnSpacing>0cm</ColumnSpacing>" +
                                "    <Style />";


            /*if (_FooterHeight != "NotShow")
            {
                node2.InnerXml +=   "    <PageFooter>" +
                                    "      <Height>" + _FooterHeight + "cm</Height>" +
                                    "      <PrintOnFirstPage>false</PrintOnFirstPage>" +
                                    "      <PrintOnLastPage>true</PrintOnLastPage>" +
                                    "    </PageFooter>";
            }*/

            xmlDoc.Save(xmlModeloRel = new MemoryStream());

        }

        private string Convert_Mm_Cm(string Medida)
        {
            decimal retornoInt;
            string retornoStr = Medida;

            retornoInt = Convert.ToDecimal(retornoStr);
            //retornoInt = retornoInt * 10;
            retornoStr = Convert.ToString(retornoInt);
            retornoStr = retornoStr + "cm";

            retornoStr = retornoStr.Replace(",", ".");

            return retornoStr;
        }
        public void CriarObjTabela(string posX, string posY, string[,] NomesQuerys_IndexKey_IndexKeyAnt, bool HeaderColor = true, bool Reset = false)
        {

            NewTab = NewTab + 1;
            NewFields = NewFields + 1;

            CountTablixMembers = 0;

            ContTab = 0;

            contTamColFinal = 0;

            contMergeCels = 0;

            contLinhas = 0;

            CountColumnTablixMembers = 0;

            RowGroup1 = false;
            RowGroup2 = false;
            RowGroup3 = false;
            RowGroup4_Sub1 = false;
            RowGroup4_Sub2 = false;
            RowGroup4_Sub3 = false;

            ListGroups.Clear();

            TabCopy = false;

            ContRowGroup++;

            contLinhas++;


            _tPositX = Convert_Mm_Cm(posX);
            _tPositY = Convert_Mm_Cm(CompareXX_YY(posY, 2));

            _arrIndexKey = NomesQuerys_IndexKey_IndexKeyAnt;

            DsTable = PreparaTabelaTeste(ds, _arrIndexKey, Reset);
            //if (ds.Tables[0].Rows.Count == 0)
            //{
            //    return;
            //}

            int Datateste = 0;
            bool nulo = false;

            Datateste = DsTable.Tables.Count;
            for (int i = 0; i < Datateste; i++)
            {
                if ((DsTable.Tables[i].Rows.Count == 0) && (_NomeRel != "Invoice"))
                    nulo = true;
            }

            if (nulo)
            {
                return;
            }

            _HeaderColor = HeaderColor;
            _NomeQuery = "Query1";

            HttpContext.Current.Session["NewTabCount"] = NewTab;
            HttpContext.Current.Session["Query"] = DsTable;
        }
        public void CriarObjTabela(string posX, string posY, string NomeQuery, bool HeaderColor = true, bool Full = false, bool Reset = false, string LargCol = "Default")
        {

            NewTab = NewTab + 1;
            NewFields = NewFields + 1;

            CountTablixMembers = 0;

            ContTab = 0;

            contTamColFinal = 0;

            contMergeCels = 0;

            contLinhas = 0;

            CountColumnTablixMembers = 0;

            RowGroup1 = false;
            RowGroup2 = false;
            RowGroup3 = false;
            RowGroup4_Sub1 = false;
            RowGroup4_Sub2 = false;
            RowGroup4_Sub3 = false;

            ListGroups.Clear();

            TabCopy = false;

            ContRowGroup++;

            contLinhas++;

            _tPositX = Convert_Mm_Cm(posX);
            _tPositY = Convert_Mm_Cm(CompareXX_YY(posY, 2));

            DsTable = ds;

            int Datateste = 0;
            bool nulo = false;

            Datateste = DsTable.Tables.Count;
            for (int i = 0; i < Datateste; i++)
            {
                if ((DsTable.Tables[i].Rows.Count == 0) && (_NomeRel != "Invoice"))
                    nulo = true;
            }

            if (nulo)
            {
                return;
            }


            if (_NomeQuery == NomeQuery)
            {
                TabCopy = true;
            }

            _NomeQuery = NomeQuery;


            _HeaderColor = HeaderColor;

            HttpContext.Current.Session["NewTabCount"] = NewTab;
            HttpContext.Current.Session["Query"] = DsTable;

            bool _LargAutoAjust = false;

            if (Full)
            {
                for (int x = 0; x < DsTable.Tables[_NomeQuery].Columns.Count; x++)
                {
                    string NomeColuna = DsTable.Tables[_NomeQuery].Columns[x].ColumnName;

                    if (LargCol == "Default")
                    {
                        _LargAutoAjust = true;
                        LargCol = NomeColuna;
                    }

                    if (NomeColuna != "TesteID" && NomeColuna != "_Registro" && ((NomeColuna.ToString().Substring((NomeColuna.Length - 1), 1)) != "_"))
                    {
                        CriarObjColunaNaTabela(NomeColuna, NomeColuna, _LargAutoAjust, LargCol);
                        CriarObjCelulaInColuna("Query", NomeColuna, "DetailGroup");
                    }


                }
            }
        }

        public void TabelaEnd()
        {
            /*
            if (DsTable.Tables[0].Rows.Count == 0)
            {
                return;
            }*/
            int Datateste = 0;
            bool nulo = false;

            Datateste = DsTable.Tables.Count;
            for (int i = 0; i < Datateste; i++)
            {
                if ((DsTable.Tables[i].Rows.Count == 0) && (_NomeRel != "Invoice"))
                    nulo = true;
            }

            if (nulo)
            {
                return;
            }

            if (_NomeRel == "TotalizaVendas")
            {
                CriarObjColunaNaTabela("N", "N", false, "1");

                CriarObjCelulaInColuna("Query", "N", "DetailGroup");
                CriarObjCelulaInColuna("Fixo", "N", "TotalGroup", "", "DetailGroup");
            }


            if (_LandScape != "Excel")
            {
                CriarObjColunaNaTabela("P#", "P#", false, "0");

                foreach (string Group in ListGroups)
                {

                    if (Group == "TotalGroup_Parent")
                    {
                        CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "ParentGroup");
                    }
                    else if (Group == "TotalGroup_Detail")
                    {
                        CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "", "DetailGroup");
                    }
                    else
                    {
                        CriarObjCelulaInColuna("Fixo", "", Group);
                    }
                }
            }
            TablixMembers();
        }

        private void TablixMembers()
        {
            XmlDocument xmlTeste = new XmlDocument();

            XmlNodeList node;

            XmlNode node2;

            DataSet ds = new DataSet();

            ds = (DataSet)HttpContext.Current.Session["Query"];

            string Part1 = string.Empty;
            string Part2 = "<TablixMember />";

            CountTablixMembers = (Int32)HttpContext.Current.Session["TablixMemberCount"];

            NewTab = (Int32)HttpContext.Current.Session["NewTabCount"];


            if (CountTablixMembers != 0)
            {
                for (int i = 0; i < CountTablixMembers; i++)
                {
                    Part1 = Part1 + Part2;
                }

                ContColumHier = "<TablixMembers>" + Part1 + "</TablixMembers>";

                xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
                xmlTeste.Load(xmlModeloRel);
                xmlModeloRel.Dispose();

                node = xmlTeste.GetElementsByTagName("TablixColumnHierarchy");

                node2 = node.Item(NewTab - 1);

                string teste4 = node2.InnerXml;

                node2.InnerXml = @teste4 + @ContColumHier;

                xmlTeste.Save(xmlModeloRel = new MemoryStream());
            }

        }

        public void CriarObjLinha(string posX, string posY, string Tam, string Cor, string Header_Body_Footer)
        {
            XmlDocument xmlTeste = new XmlDocument();
            xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
            xmlTeste.Load(xmlModeloRel);
            xmlModeloRel.Dispose();
            XmlNodeList node;

            _tPositX = Convert_Mm_Cm(posX);
            _tPositY = Convert_Mm_Cm(CompareXX_YY(posY, 2));

            XmlNode node2;
            string Field = string.Empty;
            Sort++;


            if (Header_Body_Footer == "Header")
            {
                node = xmlTeste.GetElementsByTagName("ReportItems");

                if (SetNodeHeader == 0)
                {
                    node = xmlTeste.GetElementsByTagName("PageHeader");


                    node2 = node.Item(0);

                    string teste3 = node2.InnerXml;

                    node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                               "  </ReportItems> ";

                    xmlTeste.Save(xmlModeloRel = new MemoryStream());

                    SetNodeHeader = 1;
                }

                node = xmlTeste.GetElementsByTagName("ReportItems");

                if (node.Count > 1)
                { node2 = node.Item((1 + NewTab)); }
                else
                { node2 = node.Item(0); }

                string teste = node2.InnerXml;

                node2.InnerXml = @teste + "  <Line Name=\"Line" + Sort + "\"> " +
                                             " <Top>" + _tPositY + "</Top> " +
                                             " <Left>" + _tPositX + "</Left>" +
                                             " <Height>0cm</Height>" +
                                             " <Width>" + Tam + "cm</Width>" +
                                             " <Style>" +
                                             "   <Border>" +
                                             "     <Color>Black</Color> " +
                                             "     <Style>Solid</Style>" +
                                             "   </Border>" +
                                             " </Style>" +
                                           " </Line>";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());

            }
            else if (Header_Body_Footer == "Footer")
            {
                node = xmlTeste.GetElementsByTagName("ReportItems");

                if (SetNodeFooter == 0)
                {
                    node = xmlTeste.GetElementsByTagName("PageFooter");


                    node2 = node.Item(0);

                    string teste3 = node2.InnerXml;

                    node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                               "  </ReportItems> ";

                    xmlTeste.Save(xmlModeloRel = new MemoryStream());

                    SetNodeFooter = 1;
                }

                node = xmlTeste.GetElementsByTagName("ReportItems");

                if (node.Count > 1)
                { node2 = node.Item((2 + NewTab)); }
                else
                { node2 = node.Item(0); }

                string teste = node2.InnerXml;

                node2.InnerXml = @teste + "  <Line Name=\"Line" + Sort + "\"> " +
                                             " <Top>" + _tPositY + "</Top> " +
                                             " <Left>" + _tPositX + "</Left>" +
                                             " <Height>0cm</Height>" +
                                             " <Width>" + Tam + "cm</Width>" +
                                             " <Style>" +
                                             "   <Border>" +
                                             "     <Color>Black</Color> " +
                                             "     <Style>Solid</Style>" +
                                             "   </Border>" +
                                             " </Style>" +
                                           " </Line>";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());

            }
            else
            {
                if (SetNodeBody == 0)
                {
                    node = xmlTeste.GetElementsByTagName("Body");

                    node2 = node.Item(0);

                    string teste3 = node2.InnerXml;

                    node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                               "  </ReportItems> ";

                    xmlTeste.Save(xmlModeloRel = new MemoryStream());

                    SetNodeBody = 1;
                }

                node = xmlTeste.GetElementsByTagName("ReportItems");

                node2 = node.Item(0);

                string teste = node2.InnerXml;

                node2.InnerXml = @teste + "  <Line Name=\"Line" + Sort + "\"> " +
                                             " <Top>" + _tPositY + "</Top> " +
                                             " <Left>" + _tPositX + "</Left>" +
                                             " <Height>0cm</Height>" +
                                             " <Width>" + Tam + "cm</Width>" +
                                             " <Style>" +
                                             "   <Border>" +
                                             "     <Style>Solid</Style>" +
                                             "   </Border>" +
                                             " </Style>" +
                                           " </Line>";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());
            }
        }

        private string appPath()
        {
            string sRetVal = AppDomain.CurrentDomain.BaseDirectory;
            //String strFullPathToMyFile = AppDomain.CurrentDomain.BaseDirectory;

            string sRetVal2 = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().GetName().CodeBase);

            //sRetVal2 = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).PagesURLRoot("Overfly3");

            sRetVal2 = ((OVERFLYSVRCFGLib.OverflyMTS)oOverflySvrCfg).RootLogicAppPath(DataInterfaceObj.ApplicationName);

            return sRetVal2;
        }

        public void CriarObjLabel(string Query_Fixo, string NomeQuery, string ValorObjeto, string posX, string posY, string TamFont, string Cor,
                                  string Negrito, string Header_Body_Footer, string TamLabel, string PosAlinhamento = "Left", bool Sublinhado = false, bool backgroundColor = false)
        {
            string _backgroundColor = "";

            if (backgroundColor == true)
            {
                _backgroundColor = " <BackgroundColor>Silver</BackgroundColor> ";
            }

            if (PosAlinhamento != "Left" && PosAlinhamento != "Right")
            {
                PosAlinhamento = "Left";
            }

            string canGrow = " <CanGrow>true</CanGrow> ";
            if (_NomeRel == "Cheques")
            {
                canGrow = " <CanGrow>false</CanGrow> ";
            }

            _tPositX = Convert_Mm_Cm(posX);
            _tPositY = Convert_Mm_Cm(CompareXX_YY(posY, 2));

            if (Query_Fixo == "Fixo" || ds.Tables[NomeQuery].Rows.Count != 0)
            {

                if (Query_Fixo == "Fixo" || ds.Tables[NomeQuery].Rows[0][ValorObjeto].ToString() != "")
                {

                    XmlDocument xmlTeste = new XmlDocument();

                    xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
                    xmlTeste.Load(xmlModeloRel);
                    xmlModeloRel.Dispose();
                    //}
                    XmlNodeList node;

                    XmlNode node2;

                    string Field = string.Empty;
                    string Field2 = string.Empty;
                    Sort++;
                    Index++;

                    if (Query_Fixo == "Fixo")
                    {
                        Field = ValorObjeto;

                        if (Field.Contains("&"))
                            Field = Field.Replace("&", "&amp;");

                    }
                    else
                    {
                        Field = ds.Tables[NomeQuery].Rows[0][ValorObjeto].ToString();

                        if (Field.Contains("&"))
                            Field = Field.Replace("&", "&amp;");
                        else if (Field.Contains("<") && Field.Contains(">"))
                        {
                            Field = Field.Replace("<", " ");
                            Field = Field.Replace(">", " ");
                        }

                        if (ds.Tables[NomeQuery].Columns[ValorObjeto].DataType.ToString() == "System.Decimal")
                        {
                            Field = string.Format("{0:N}", ds.Tables[NomeQuery].Rows[0][ValorObjeto]);

                        }

                        if (_data == "Horas_BRA")
                        {
                            Field = string.Format("{0:dd/MM/yyy HH:mm:ss}", ds.Tables[NomeQuery].Rows[0][ValorObjeto]);
                            _data = string.Empty;
                        }
                        else if (_data == "_BRA")
                        {
                            Field = string.Format("{0:dd/MM/yyy}", ds.Tables[NomeQuery].Rows[0][ValorObjeto]);
                            _data = string.Empty;
                        }
                    }

                    decimal _tamFonte = Convert.ToDecimal(TamFont);

                    System.Drawing.Font stringFont = new System.Drawing.Font("Arial", Convert.ToInt32(Math.Ceiling(_tamFonte)));
                    System.Drawing.Image image = new Bitmap(1, 1);
                    Graphics graphics = Graphics.FromImage(image);
                    SizeF size = new SizeF();

                    size = graphics.MeasureString(ValorObjeto, stringFont);

                    int WidthLabelTemp = (int)Math.Round(size.Width);
                    int HeigthLabelTemp = (int)Math.Round(size.Height);

                    string WidthLabel = TamLabel;
                    string HeigthLabel2 = Convert.ToString(HeigthLabelTemp * 2.54 / 96);

                    string HeigthLabel = HeigthLabel2.Replace(",", ".");
                    string FontFamily = "";

                    if (_NomeRel == "Cheques")
                    {
                        FontFamily = "              <FontFamily>Fixedsys</FontFamily> ";
                    }

                    if (Header_Body_Footer == "Header")
                    {

                        if (SetNodeHeader == 0)
                        {

                            node = xmlTeste.GetElementsByTagName("PageHeader");


                            node2 = node.Item(0);

                            string teste3 = node2.InnerXml;

                            node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                                       "  </ReportItems> ";

                            xmlTeste.Save(xmlModeloRel = new MemoryStream());
                            SetNodeHeader = 1;
                        }

                        if (Negrito.Trim() == "")
                        { Negrito = "Normal"; }
                        else if (Negrito == "B")
                        { Negrito = "Bold"; }
                        else if (Negrito == "L")
                        { Negrito = "Light"; }

                        if (Cor.Trim() == "")
                        { Cor = "Black"; }

                        string sublinhado = "None";

                        if (Sublinhado == true)
                        {
                            sublinhado = "Underline";
                        }

                        node = xmlTeste.GetElementsByTagName("ReportItems");

                        if (node.Count > 1)
                        { node2 = node.Item((1 + NewTab)); }
                        else
                        { node2 = node.Item(0); }

                        string teste = node2.InnerXml;

                        Sort = Sort + 500;

                        node2.InnerXml = @teste + " <Textbox Name=\"Textbox" + Sort + "\"> " +
                                                            canGrow +
                                                            " <KeepTogether>true</KeepTogether> " +
                                                            " <Paragraphs> " +
                                                            "   <Paragraph> " +
                                                            "     <TextRuns> " +
                                                            "       <TextRun> " +
                                                            "         <Value>" + Field + "</Value> " +
                                                            "          <Style> " +
                                                            "              <FontSize>" + TamFont + "pt</FontSize> " +
                                                            "              <Color>" + Cor + "</Color> " +
                                                            "              <FontWeight>" + Negrito + "</FontWeight>" +
                                                            "              <TextDecoration>" + sublinhado + "</TextDecoration>" +
                                                                            FontFamily +
                                                            "          </Style> " +
                                                            "       </TextRun> " +
                                                            "     </TextRuns> " +
                                                            "     <Style>" +
                                                            "       <TextAlign>" + PosAlinhamento + "</TextAlign>" +
                                                            "     </Style>" +
                                                            "   </Paragraph> " +
                                                            " </Paragraphs> " +
                                                            " <rd:DefaultName>Textbox" + Sort + "</rd:DefaultName> " +
                                                            " <Top>" + _tPositY + "</Top> " +
                                                            " <Left>" + _tPositX + "</Left> " +
                                                            " <Height>" + HeigthLabel + "cm</Height> " +
                                                            " <Width>" + WidthLabel + "cm</Width> " +
                                                            " <ZIndex>" + Index + "</ZIndex> " +
                                                        " </Textbox> ";

                        xmlTeste.Save(xmlModeloRel = new MemoryStream());

                    }
                    else if (Header_Body_Footer == "Footer")
                    {

                        if (SetNodeFooter == 0)
                        {

                            node = xmlTeste.GetElementsByTagName("PageFooter");


                            node2 = node.Item(0);

                            string teste3 = node2.InnerXml;

                            node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                                       "  </ReportItems> ";

                            xmlTeste.Save(xmlModeloRel = new MemoryStream());
                            SetNodeFooter = 1;
                        }

                        if (Negrito.Trim() == "")
                        { Negrito = "Normal"; }
                        else if (Negrito == "B")
                        { Negrito = "Bold"; }
                        else if (Negrito == "L")
                        { Negrito = "Light"; }

                        if (Cor.Trim() == "")
                        { Cor = "Black"; }

                        string sublinhado = "None";

                        if (Sublinhado == true)
                        {
                            sublinhado = "Underline";
                        }

                        node = xmlTeste.GetElementsByTagName("ReportItems");

                        if (node.Count > 1)
                        { node2 = node.Item((2 + NewTab)); }
                        else
                        { node2 = node.Item(0); }


                        string teste = node2.InnerXml;

                        Sort = Sort + 500;

                        node2.InnerXml = @teste + " <Textbox Name=\"Textbox" + Sort + "\"> " +
                                                            canGrow +
                                                            " <KeepTogether>true</KeepTogether> " +
                                                            " <Paragraphs> " +
                                                            "   <Paragraph> " +
                                                            "     <TextRuns> " +
                                                            "       <TextRun> " +
                                                            "         <Value>" + Field + "</Value> " +
                                                            "          <Style> " +
                                                            "              <FontSize>" + TamFont + "pt</FontSize> " +
                                                            "              <Color>" + Cor + "</Color> " +
                                                            "              <FontWeight>" + Negrito + "</FontWeight>" +
                                                            "              <TextDecoration>" + sublinhado + "</TextDecoration>" +
                                                                            FontFamily +
                                                            "          </Style> " +
                                                            "       </TextRun> " +
                                                            "     </TextRuns> " +
                                                            "     <Style>" +
                                                            "       <TextAlign>" + PosAlinhamento + "</TextAlign>" +
                                                            "     </Style>" +
                                                            "   </Paragraph> " +
                                                            " </Paragraphs> " +
                                                            " <rd:DefaultName>Textbox" + Sort + "</rd:DefaultName> " +
                                                            " <Top>" + _tPositY + "</Top> " +
                                                            " <Left>" + _tPositX + "</Left> " +
                                                            " <Height>" + HeigthLabel + "cm</Height> " +
                                                            " <Width>" + WidthLabel + "cm</Width> " +
                                                            " <ZIndex>" + Index + "</ZIndex> " +
                                                        " </Textbox> ";

                        xmlTeste.Save(xmlModeloRel = new MemoryStream());

                    }
                    else
                    {
                        if (SetNodeBody == 0)
                        {
                            node = xmlTeste.GetElementsByTagName("Body");

                            node2 = node.Item(0);

                            string teste3 = node2.InnerXml;

                            node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                                       "  </ReportItems> ";

                            xmlTeste.Save(xmlModeloRel = new MemoryStream());

                            SetNodeBody = 1;
                        }

                        if (Negrito.Trim() == "")
                        { Negrito = "Normal"; }
                        else if (Negrito == "B")
                        { Negrito = "Bold"; }
                        else if (Negrito == "L")
                        { Negrito = "Light"; }

                        if (Cor.Trim() == "")
                        { Cor = "Black"; }

                        string sublinhado = "None";

                        if (Sublinhado == true)
                        {
                            sublinhado = "Underline";
                        }

                        node = xmlTeste.GetElementsByTagName("ReportItems");

                        node2 = node.Item(0);

                        string teste = node2.InnerXml;

                        node2.InnerXml = @teste + " <Textbox Name=\"Textbox" + Sort + "\"> " +
                                                             canGrow +
                                                             " <KeepTogether>true</KeepTogether> " +
                                                             " <Paragraphs> " +
                                                             "   <Paragraph> " +
                                                             "     <TextRuns> " +
                                                             "       <TextRun> " +
                                                             "         <Value>" + Field + "</Value> " +
                                                             "          <Style> " +
                                                             "              <FontSize>" + TamFont + "pt</FontSize> " +
                                                             "              <Color>" + Cor + "</Color> " +
                                                             "              <FontWeight>" + Negrito + "</FontWeight>" +
                                                             "              <TextDecoration>" + sublinhado + "</TextDecoration>" +
                                                                            FontFamily +
                                                             "          </Style> " +
                                                             "       </TextRun> " +
                                                             "     </TextRuns> " +
                                                             "     <Style>" +
                                                             "       <TextAlign>" + PosAlinhamento + "</TextAlign>" +
                                                             "     </Style>" +
                                                             "   </Paragraph> " +
                                                             " </Paragraphs> " +
                                                             " <rd:DefaultName>Textbox" + Sort + "</rd:DefaultName> " +
                                                             " <Top>" + _tPositY + "</Top> " +
                                                             " <Left>" + _tPositX + "</Left> " +
                                                             " <Height>" + HeigthLabel + "cm</Height> " +
                                                             " <Width>" + WidthLabel + "cm</Width> " +
                                                             " <ZIndex>" + Index + "</ZIndex> " +
                                                             " <Style> " +
                                                             "   <Border> " +
                                                             "     <Style>None</Style> " +
                                                             "   </Border> " +
                                                             _backgroundColor +
                                                             " </Style> " +
                                                           " </Textbox> ";

                        xmlTeste.Save(xmlModeloRel = new MemoryStream());
                    }
                }
            }
        }

        public void CriarObjImage(string Query_Fixo, string NomeQuery, string ValorObjeto, string posX, string posY, string Header_Body_Footer, string tamX, string tamY)
        {
            _tPositX = Convert_Mm_Cm(posX);
            _tPositY = Convert_Mm_Cm(CompareXX_YY(posY, 2));
            string DataType = ds.Tables[NomeQuery].Columns[ValorObjeto].DataType.ToString();

            XmlDocument xmlTeste = new XmlDocument();

            if (getRelModel == 0)
                xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
            xmlTeste.Load(xmlModeloRel);
            xmlModeloRel.Dispose();
            //}


            XmlNodeList node;

            XmlNode node2;

            string Field = string.Empty;
            string Field2 = string.Empty;
            Sort++;
            Index++;

            if (Header_Body_Footer == "Header")
            {

                if (SetNodeHeader == 0)
                {

                    node = xmlTeste.GetElementsByTagName("PageHeader");


                    node2 = node.Item(0);

                    string teste3 = node2.InnerXml;

                    node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                               "  </ReportItems> ";

                    xmlTeste.Save(xmlModeloRel = new MemoryStream());

                    SetNodeHeader = 1;
                }

                node = xmlTeste.GetElementsByTagName("ReportItems");

                if (node.Count > 1)
                { node2 = node.Item((1 + NewTab)); }
                else
                { node2 = node.Item(0); }

                string teste = node2.InnerXml;

                Sort = Sort + 500;

                node2.InnerXml = @teste + "<Image Name=\"Image1" + Sort + "\"> " +
                                          "  <Source>Database</Source>" +
                                          "  <Value>=First(Fields!" + ValorObjeto + ".Value, \"" + NomeQuery + "\")</Value>" +
                                          "  <MIMEType>image/jpeg</MIMEType>" +
                                          "  <Sizing>FitProportional</Sizing>" +
                                          "  <Top>" + _tPositY + "</Top>" +
                                          "  <Left>" + _tPositX + "</Left>" +
                                          "  <Height>" + tamY + "cm</Height>" +
                                          "  <Width>" + tamX + "cm</Width>" +
                                          " <ZIndex>" + Index + "</ZIndex> " +
                                          "  <Style>" +
                                          "      <Border>" +
                                          "      <Style>None</Style>" +
                                          "      </Border>" +
                                          "  </Style>" +
                                          "  </Image>";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());

            }
            else if (Header_Body_Footer == "Footer")
            {

                if (SetNodeFooter == 0)
                {

                    node = xmlTeste.GetElementsByTagName("PageFooter");


                    node2 = node.Item(0);

                    string teste3 = node2.InnerXml;

                    node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                               "  </ReportItems> ";

                    xmlTeste.Save(xmlModeloRel = new MemoryStream());

                    SetNodeFooter = 1;
                }

                node = xmlTeste.GetElementsByTagName("ReportItems");

                if (node.Count > 1)
                { node2 = node.Item((2 + NewTab)); }
                else
                { node2 = node.Item(0); }

                string teste = node2.InnerXml;

                Sort = Sort + 500;

                node2.InnerXml = @teste + "<Image Name=\"Image1" + Sort + "\"> " +
                                          "  <Source>Database</Source>" +
                                          "  <Value>=First(Fields!" + ValorObjeto + ".Value, \"" + NomeQuery + "\")</Value>" +
                                          "  <MIMEType>image/jpeg</MIMEType>" +
                                          "  <Sizing>FitProportional</Sizing>" +
                                          "  <Top>" + _tPositY + "</Top>" +
                                          "  <Left>" + _tPositX + "</Left>" +
                                          "  <Height>" + tamY + "cm</Height>" +
                                          "  <Width>" + tamX + "cm</Width>" +
                                          " <ZIndex>" + Index + "</ZIndex> " +
                                          "  <Style>" +
                                          "      <Border>" +
                                          "      <Style>None</Style>" +
                                          "      </Border>" +
                                          "  </Style>" +
                                          "  </Image>";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());

            }
            else
            {
                if (SetNodeBody == 0)
                {
                    node = xmlTeste.GetElementsByTagName("Body");

                    node2 = node.Item(0);

                    string teste3 = node2.InnerXml;

                    node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                               "  </ReportItems> ";

                    xmlTeste.Save(xmlModeloRel = new MemoryStream());

                    SetNodeBody = 1;
                }
                node = xmlTeste.GetElementsByTagName("ReportItems");

                node2 = node.Item(0);

                string teste = node2.InnerXml;

                node2.InnerXml = @teste + "<Image Name=\"Image1" + Sort + "\"> " +
                                          "  <Source>Database</Source>" +
                                          "  <Value>=First(Fields!" + ValorObjeto + ".Value, \"" + NomeQuery + "\")</Value>" +
                                          "  <MIMEType>image/jpeg</MIMEType>" +
                                          "  <Sizing>FitProportional</Sizing>" +
                                          "  <Top>" + _tPositY + "</Top>" +
                                          "  <Left>" + _tPositX + "</Left>" +
                                          "  <Height>" + tamY + "cm</Height>" +
                                          "  <Width>" + tamX + "cm</Width>" +
                                          " <ZIndex>" + Index + "</ZIndex> " +
                                          "  <Style>" +
                                          "      <Border>" +
                                          "      <Style>None</Style>" +
                                          "      </Border>" +
                                          "  </Style>" +
                                          "  </Image>";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());
            }

            if (ContImg == 0)
            {
                ContImg++;

                NewFields = NewFields + 1;

                node = xmlTeste.GetElementsByTagName("DataSets");

                node2 = node.Item(0);

                string testeDT = node2.InnerXml;

                node2.InnerXml = @testeDT +
                                            "     <DataSet Name=\"" + NomeQuery + "\"> " +
                                            "          <Fields> " +
                                            "          </Fields> " +
                                            "          <Query> " +
                                            "            <DataSourceName>DataSource</DataSourceName> " +
                                            "            <CommandText>/* Local Query */</CommandText> " +
                                            "          </Query> " +
                                            "          <rd:DataSetInfo> " +
                                            "            <rd:DataSetName>DataSetTemp" + NewFields + "</rd:DataSetName> " +
                                            "            <rd:TableName>" + NomeQuery + "</rd:TableName> " +
                                            "            <rd:TableAdapterFillMethod /> " +
                                            "            <rd:TableAdapterGetDataMethod /> " +
                                            "            <rd:TableAdapterName /> " +
                                            "          </rd:DataSetInfo> " +
                                            "        </DataSet> ";


                xmlTeste.Save(xmlModeloRel = new MemoryStream());

                node = xmlTeste.GetElementsByTagName("Fields");

                node2 = node.Item(NewFields - 1);

                string testeDs = node2.InnerXml;

                node2.InnerXml = @testeDs + "    <Field Name=\"" + HeadersColumn(ds.Tables[NomeQuery].Columns[ValorObjeto].ColumnName.ToString().Trim()) + "\"> " +
                                                 " <DataField>" + HeadersColumn(ds.Tables[NomeQuery].Columns[ValorObjeto].ColumnName.ToString().Trim()) + "</DataField> " +
                                                 " <rd:TypeName>" + DataType + "</rd:TypeName> " +
                                               " </Field> ";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());

                if (_NomeRel != "Invoice")
                    ContImg = 0;

            }

        }

        private static Object comCreateObject(string sProgID)
        {
            Type oType = Type.GetTypeFromProgID(sProgID);
            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }
            return null;
        }

        //Monta DataSet com o resultado da query 
        private DataSet MontaDataSet(string[,] sqlQuery)
        {

            DataSet DsTeste1 = new DataSet();
            DataTable DtTeste1 = new DataTable();

            try
            {
                for (int aux = 0; aux < sqlQuery.GetLength(0); aux++)
                {

                    SqlCommand cmd = new SqlCommand(sqlQuery[aux, 1].ToString());
                    using (SqlConnection con = new SqlConnection(strConn))
                    {
                        using (SqlDataAdapter sda = new SqlDataAdapter())
                        {
                            cmd.Connection = con;
                            cmd.CommandTimeout = 360000;
                            sda.SelectCommand = cmd;

                            sda.Fill(DsTeste1, sqlQuery[aux, 0].ToString());
                        }
                    }
                }
            }
            catch (Exception e)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert(" + e.Message.ToString() + "); </script>");
                // HttpContext.Current.Response.Write("<script type=\"text/javascript\">alert('" + e.InnerException.InnerException.Message.ToString() + "');</script>");
                HttpContext.Current.Response.Flush(); // send it to the client to download
                HttpContext.Current.Response.Clear();
                //HttpContext.Current.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
            // Tratamento de Nome das colunas
            for (int x = 0; x < DsTeste1.Tables[0].Columns.Count; x++)
            {
                string NomeColuna = DsTeste1.Tables[0].Columns[x].ColumnName;
                DsTeste1.Tables[0].Columns[x].ColumnName = HeadersColumn(NomeColuna);
            }

            return DsTeste1;
        }

        public void CriarObjLabelData(string Query_Now, string NomeQuery, string ValorObjeto, string posX, string posY, string TamFont, string Cor, string Negrito, string Header_Body, string TamLabel, string Horas, string PosAlinhamento = "Left", bool Sublinhado = false)
        {
            string DataNF = "";
            if ((NomeQuery != "") && (ValorObjeto != ""))
                DataNF = ds.Tables[NomeQuery].Rows[0][ValorObjeto].ToString();

            if (PosAlinhamento != "Left" && PosAlinhamento != "Right")
            {
                PosAlinhamento = "Left";
            }

            if (_Format == "BRA")
            {
                if (Query_Now == "Now")
                {
                    DateTime DataHoje = DateTime.Now;
                    _data = DataHoje.ToString("dd/MM/yyy ");

                    if (Horas == "Horas")
                    {
                        _data = DataHoje.ToString("dd/MM/yyy ") + DataHoje.ToString("HH:mm:ss");
                    }

                    CriarObjLabel("Fixo", "", ValorObjeto = _data, posX, posY, TamFont, Cor, Negrito, Header_Body, TamLabel, PosAlinhamento);
                }
                else
                {
                    if (Horas == "Horas")
                    {
                        _data = "Horas_BRA";
                        CriarObjLabel(Query_Now, NomeQuery, ValorObjeto, posX, posY, TamFont, Cor, Negrito, Header_Body, TamLabel, PosAlinhamento);
                    }
                    else
                    {
                        _data = "_BRA";
                        CriarObjLabel(Query_Now, NomeQuery, ValorObjeto, posX, posY, TamFont, Cor, Negrito, Header_Body, TamLabel, PosAlinhamento);
                    }
                }
            }
            else
            {
                DateTime DataHoje = DateTime.Now;

                if (DataNF == null)
                    _data = DataHoje.ToString("MM/dd/yyy");
                else
                    _data = DataNF;


                if (Horas == "Horas")
                {
                    _data = DataHoje.ToString("MM/dd/yyy ") + DataHoje.ToString("hh:mm:ss");
                }

                CriarObjLabel("Fixo", "", ValorObjeto = _data, posX, posY, TamFont, Cor, Negrito, Header_Body, TamLabel, PosAlinhamento);
            }
        }

        public void CriarPDF_Excel(string fileName, int tipo) // tipo = 1 ("PDF") / 2 ("EXCEL")
        {
            // Se Retorno do DataTable estiver vazio finaliza o processo
            int Datateste = 0;
            bool nulo = false;

            //Função para substituir os caracteres especiais
            if (string.IsNullOrEmpty(fileName))
                fileName = "";
            else
            {
                byte[] bytesTwo = System.Text.Encoding.GetEncoding("iso-8859-8").GetBytes(fileName);
                fileName = System.Text.Encoding.UTF8.GetString(bytesTwo);
            }

            Datateste = DsTable.Tables.Count;

            for (int i = 0; i < Datateste; i++)
            {
                if (DsTable.Tables[i].Rows.Count == 0)
                    nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há nada para ser Impresso'); </script>");
                //HttpContext.Current.Response.Flush(); // send it to the client to download
                //HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.End();
                return;

            }

            if (tipo == 2)
            {
                AjustaRelatório("Excel", "", "0", "0", "0");
            }
            XmlDocument xmlTeste = new XmlDocument();

            DataSet ds = new DataSet();

            ds = (DataSet)HttpContext.Current.Session["Query"];

            string Formato;

            MemoryStream stream_RDLC = new MemoryStream();

            stream_RDLC = xmlModeloRel;

            stream_RDLC.Seek(0, System.IO.SeekOrigin.Begin);

            //xmlTeste.Load(xmlModeloRel);
            //xmlModeloRel.Dispose();

            //xmlTeste.Save(sLocalPath + "\\PrintJet\\RelFinal.rdlc");

            if (tipo == 1)
            {
                //TipoView = "attachment;";
                Formato = "PDF";
            }
            else
            {
                //TipoView = "attachment;";
                Formato = "Excel";
            }

            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.DataSources.Clear();
            //viewer.LocalReport.ReportPath = sLocalPath + "\\PrintJet\\RelFinal.rdlc"; // Link Report
            viewer.LocalReport.LoadReportDefinition(stream_RDLC);

            for (int Count = 0; Count < ds.Tables.Count; Count++)
            {
                viewer.LocalReport.DataSources.Add(new ReportDataSource(ds.Tables[Count].TableName, ds.Tables[Count])); // Add datasource 
            }

            //Imprimir(viewer);

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;
            byte[] bytes;

            try
            {

                bytes = viewer.LocalReport.Render(Formato, null, out mimeType, out encoding, out extension, out streamids, out warnings);

                HttpContext.Current.Response.Buffer = true;
                HttpContext.Current.Response.Clear();
                //HttpContext.Current.Response.Redirect("../modcomercial/submodpedidos/frmpedidos/modalpages/modalprint.asp");
                HttpContext.Current.Response.ContentType = mimeType;
                HttpContext.Current.Response.AddHeader("content-disposition", "attachment; filename=" + fileName + "." + extension);
                //HttpContext.Current.Response.AddHeader("content-disposition", "inline; filename=" + fileName + "." + extension);
                //HttpContext.Current.Response.BinaryWrite(bytes); // create the file
                HttpContext.Current.Response.OutputStream.Write(bytes, 0, bytes.Length); // create the file
                //HttpContext.Current.Response.WriteFile(fileName + "." + extension);
                //HttpContext.Current.Response.Redirect("../modcomercial/submodpedidos/frmpedidos/modalpages/modalprint.asp");
                HttpContext.Current.Response.Flush(); // send it to the client to download
                HttpContext.Current.Response.Clear();
                //ttpContext.Current.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();

            }
            catch (Exception e)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Problema na renderização do relatório.'); </script>");
                // HttpContext.Current.Response.Write("<script type=\"text/javascript\">alert('" + e.InnerException.InnerException.Message.ToString() + "');</script>");
                HttpContext.Current.Response.Flush(); // send it to the client to download
                HttpContext.Current.Response.Clear();
                //HttpContext.Current.Response.End();
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }

        public void Imprimir(string fileName) // 
        {

            XmlDocument xmlTeste = new XmlDocument();

            DataSet ds = new DataSet();

            ds = (DataSet)HttpContext.Current.Session["Query"];

            string Formato;

            MemoryStream stream_RDLC = new MemoryStream();

            stream_RDLC = xmlModeloRel;

            stream_RDLC.Seek(0, System.IO.SeekOrigin.Begin);

            //xmlTeste.Load(xmlModeloRel);
            //xmlModeloRel.Dispose();

            //xmlTeste.Save(sLocalPath + "\\PrintJet\\RelFinal.rdlc");

            Formato = "PDF";

            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.DataSources.Clear();
            //viewer.LocalReport.ReportPath = sLocalPath + "\\PrintJet\\RelFinal.rdlc"; // Link Report
            viewer.LocalReport.LoadReportDefinition(stream_RDLC);

            for (int Count = 0; Count < ds.Tables.Count; Count++)
            {
                viewer.LocalReport.DataSources.Add(new ReportDataSource(ds.Tables[Count].TableName, ds.Tables[Count])); // Add datasource 
            }

            //Imprimir(viewer);

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            byte[] bytes = viewer.LocalReport.Render(Formato, null, out mimeType, out encoding, out extension, out streamids, out warnings);

            var streamDoc = new MemoryStream();

            //FileStream fs = new FileStream(HttpContext.Current.Server.MapPath("output.pdf"), FileMode.Create);
            //fs.Write(bytes, 0, bytes.Length);
            //fs.Close();

            //Open existing PDF
            //Document document = new Document(PageSize.A4);
            Document document = new Document(PageSize.LETTER);
            PdfReader reader = new PdfReader(bytes);//(HttpContext.Current.Server.MapPath("output.pdf"));
            //Getting a instance of new PDF writer
            PdfWriter writer = PdfWriter.GetInstance(document, streamDoc);//new FileStream(HttpContext.Current.Server.MapPath("Invoice.pdf"), FileMode.Create));
            document.Open();
            PdfContentByte cb = writer.DirectContent;

            int i = 0;
            int p = 0;
            int n = reader.NumberOfPages;
            iTextSharp.text.Rectangle psize = reader.GetPageSize(1);

            float width = psize.Width;
            float height = psize.Height;

            if (_NomeRel == "Cheques")
            {
                n = 1;
            }

            //Add Page to new document
            while (i < n)
            {
                document.NewPage();
                p++;
                i++;

                PdfImportedPage page1 = writer.GetImportedPage(reader, i);
                cb.AddTemplate(page1, 0, 0);
            }

            //Attach javascript to the document

            string JsPrintParam = "var pp = getPrintParams();" +
                                  "pp.interactive = pp.constants.interactionLevel.automatic;" +
                                  "pp.pageHandling = pp.constants.handling.none;" +
                                  "this.print(pp);\r";

            //PdfAction jAction = PdfAction.JavaScript("var pp = getPrintParams();pp.interactive = pp.constants.interactionLevel.automatic; this.print(pp);\r", writer);
            PdfAction jAction = PdfAction.JavaScript(JsPrintParam, writer);
            writer.AddJavaScript(jAction);
            document.Close();

            byte[] content = streamDoc.ToArray();

            //HttpContext.Current.Response.Write("<script type=\"text/javascript\"> var janela = window.open('Invoice.pdf'); </script>");

            HttpContext.Current.Response.ContentType = mimeType;
            HttpContext.Current.Response.AddHeader("content-disposition", "inline; filename=" + fileName + "." + extension);
            HttpContext.Current.Response.BinaryWrite(content); // create the file
            //HttpContext.Current.Response.Write("<script type=\"text/javascript\"> var pp = getPrintParams(); pp.interactive = pp.constants.interactionLevel.automatic; this.print(pp);\r </script>");
            HttpContext.Current.Response.Flush(); // send it to the client to download
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.End();

            //var impressoras = PrinterSettings.InstalledPrinters;

            //habilitPrinter.Assert();
            //habilitPrinter.Level = PrintingPermissionLevel.AllPrinting;

            //Print(viewer.LocalReport);
        }

        public void Email(string Nome_do_Arquivo, int tipo)
        {
            if (tipo == 2)
            {
                AjustaRelatório("Excel", "", "0", "0", "0");
            }
            XmlDocument xmlTeste = new XmlDocument();

            DataSet ds = new DataSet();

            ds = (DataSet)HttpContext.Current.Session["Query"];

            string Formato;

            MemoryStream stream_RDLC = new MemoryStream();

            stream_RDLC = xmlModeloRel;

            stream_RDLC.Seek(0, System.IO.SeekOrigin.Begin);

            //xmlTeste.Load(xmlModeloRel);
            //xmlModeloRel.Dispose();

            //xmlTeste.Save(sLocalPath + "\\PrintJet\\RelFinal.rdlc");

            if (tipo == 1)
            {
                //TipoView = "attachment;";
                Formato = "PDF";
            }
            else
            {
                //TipoView = "attachment;";
                Formato = "Excel";
            }

            ReportViewer viewer = new ReportViewer();
            viewer.ProcessingMode = ProcessingMode.Local;
            viewer.LocalReport.DataSources.Clear();
            //viewer.LocalReport.ReportPath = sLocalPath + "\\PrintJet\\RelFinal.rdlc"; // Link Report
            viewer.LocalReport.LoadReportDefinition(stream_RDLC);

            for (int Count = 0; Count < ds.Tables.Count; Count++)
            {
                viewer.LocalReport.DataSources.Add(new ReportDataSource(ds.Tables[Count].TableName, ds.Tables[Count])); // Add datasource 
            }

            //Imprimir(viewer);

            Warning[] warnings;
            string[] streamids;
            string mimeType;
            string encoding;
            string extension;

            byte[] bytes = viewer.LocalReport.Render(Formato, null, out mimeType, out encoding, out extension, out streamids, out warnings);


            MemoryStream memoryStream = new MemoryStream(bytes);
            memoryStream.Seek(0, SeekOrigin.Begin);

            MailMessage message = new MailMessage();
            Attachment attachment = new Attachment(stream_RDLC, Nome_do_Arquivo + ".pdf");
            message.Attachments.Add(attachment);

            message.From = new MailAddress("victor.matos@alcateia.com.br");
            message.To.Add("victor.matos@alcateia.com.br");
            message.CC.Add("victor.matos@alcateia.com.br");
            message.Subject = "Business Report";
            message.IsBodyHtml = true;
            message.Body = "Please find Attached Report herewith.";

            message.BodyEncoding = System.Text.Encoding.UTF8;

            try
            {
                //SmtpClient smtp = new SmtpClient(smtpServer);
                SmtpClient smtp = new SmtpClient("172.16.10.242", 25);
                smtp.UseDefaultCredentials = false;
                smtp.Port = 25;
                smtp.Send(message);
            }
            catch (System.Exception e)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\">alert('" + e.InnerException.InnerException.Message.ToString() + "');</script>");
                HttpContext.Current.Response.Clear();
                HttpContext.Current.Response.End();
            }
            /* }
         else
         {
         //This is for testing.
         SmtpClient smtp = new SmtpClient();        
                 smtp.Send(message);
         }*/

            memoryStream.Close();
            memoryStream.Dispose();

            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.End();



        }



        private Stream CreateStream(string name, string fileNameExtension, Encoding encoding, string mimeType, bool willSeek)
        {
            tmpFileName = Path.GetTempFileName();
            FileStream s = new FileStream(tmpFileName, FileMode.Create);

            if (streamList.Count == 0)
            {
                streamList = new List<Stream>();
            }

            streamList.Add(s);
            return s;

        }

        private void ExportToStream(LocalReport report)
        {
            StringBuilder deviceInfo = new StringBuilder()
                ;
            {
                deviceInfo.Append("<DeviceInfo>");
                deviceInfo.Append(" <OutputFormat>EMF</OutputFormat>");
                deviceInfo.Append(" <PageWidth>11.6929133739in</PageWidth>");
                deviceInfo.Append(" <PageHeight>7.480314953in</PageHeight>");
                deviceInfo.Append(" <MarginTop>0.0in</MarginTop>");
                deviceInfo.Append(" <MarginLeft>0.0in</MarginLeft>");
                deviceInfo.Append(" <MarginRight>0.0in</MarginRight>");
                deviceInfo.Append(" <MarginBottom>0.0in</MarginBottom>");
                deviceInfo.Append("</DeviceInfo>");
            }

            Warning[] warnings = null;
            report.Render("image", deviceInfo.ToString(), CreateStream, out warnings);

            foreach (Stream s in streamList)
            {
                s.Position = 0;
            }

            deviceInfo = null;
        }

        private void PrintPage(object sender, PrintPageEventArgs ev)
        {

            using (Metafile pageImage = new Metafile(streamList[currentPageIndex]))
            {
                currentPageIndex += 1;

                ev.Graphics.DrawImage(pageImage, ev.PageBounds);
                ev.HasMorePages = (currentPageIndex < streamList.Count);
            }
        }

        private void Print(LocalReport report)
        {
            streamList = new List<Stream>();
            ExportToStream(report);
            if (streamList != null && streamList.Count > 0)
            {
                String username = System.Security.Principal.WindowsIdentity.GetCurrent().Name;

                HttpContext.Current.Response.Write("<script type=\"text/javascript\">alert('" + username + "');</script>");

                HttpContext.Current.Response.End();

                var userToken = System.Security.Principal.WindowsIdentity.GetCurrent().Token;

                using (System.Security.Principal.WindowsImpersonationContext wic = System.Security.Principal.WindowsIdentity.Impersonate(IntPtr.Zero))
                {
                    //code to send printdocument to the printer


                    using (PrintDocument printDoc = new PrintDocument())
                    {
                        //printDoc.PrinterSettings.PrinterName = "CutePDF Writer";
                        if (!printDoc.PrinterSettings.IsValid)
                        {
                            string msg = "Impressora não disponível ou não válida!";
                            throw new ArgumentException(msg);
                        }
                        if (printDoc.PrinterSettings.CanDuplex == true)
                        {
                            printDoc.PrinterSettings.Duplex = Duplex.Horizontal;
                        }
                        printDoc.DefaultPageSettings.Landscape = false;

                        PaperSize pkSize;

                        pkSize = printDoc.PrinterSettings.PaperSizes[0];
                        printDoc.DefaultPageSettings.PaperSize = pkSize;
                        printDoc.DefaultPageSettings.Margins.Top = 0;
                        printDoc.DefaultPageSettings.Margins.Bottom = 0;
                        printDoc.DefaultPageSettings.Margins.Left = 0;
                        printDoc.DefaultPageSettings.Margins.Right = 0;

                        printDoc.PrintPage += PrintPage;

                        printDoc.Print();
                    }
                }
            }
        }

        private DataSet TrataExcecao(string NomeRel, DataSet ds)
        {
            if (_NomeRel == "RelPedido")
            {
                for (int aux2 = 0; aux2 < ds.Tables["Query2"].Columns.Count - 1; aux2++)
                {
                    switch (ds.Tables["Query2"].Columns[aux2].ColumnName.ToString().Trim())
                    {
                        case "PedidoID":
                            ds.Tables["Query2"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "Ordem":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "C";
                            break;

                        case "ProdutoID":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "ID";
                            break;

                        case "Quantidade":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "Quant";
                            break;

                        case "ValorUnitario":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "Unitario";
                            break;

                        case "ValorTotal":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "Total";
                            break;

                        case "ICMSISS":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "ICMS";
                            break;
                    }
                }

                for (int aux2 = 0; aux2 < ds.Tables["Query3"].Columns.Count - 1; aux2++)
                {
                    switch (ds.Tables["Query3"].Columns[aux2].ColumnName.ToString().Trim())
                    {
                        case "PedidoID":
                            ds.Tables["Query3"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "dtVencimento":
                            ds.Tables["Query3"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "Ordem":
                            ds.Tables["Query3"].Columns[aux2].ColumnName = "O";
                            break;

                        case "FinanceiroID":
                            ds.Tables["Query3"].Columns[aux2].ColumnName = "Fin";
                            break;

                        case "FormaPagamento":
                            ds.Tables["Query3"].Columns[aux2].ColumnName = "Forma";
                            break;

                        case "PrazoPagamento":
                            ds.Tables["Query3"].Columns[aux2].ColumnName = "Prazo";
                            break;

                        case "ValorParcela":
                            ds.Tables["Query3"].Columns[aux2].ColumnName = "Valor";
                            break;

                    }
                }
            }

            if (_NomeRel == "RelAsstec1")
            {
                for (int aux2 = 0; aux2 < ds.Tables["Query1"].Columns.Count; aux2++)
                {
                    if (ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "AsstecID" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Estado" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Proprio" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Defeito" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Cli" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Forn" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Data" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Produto" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "ProdutoID" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "NumeroSerie" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Cliente" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Vendedor")
                    {
                        ds.Tables["Query1"].Columns.RemoveAt(aux2);

                        if (aux2 != 0)
                        {
                            aux2 = aux2 - 1;
                        }
                    }
                }
            }
            else if (_NomeRel == "RelatorioCompras")
            {
                for (int aux2 = 0; aux2 < ds.Tables["Query1"].Columns.Count; aux2++)
                {
                    if (ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Data" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Pedido" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "NF" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "DataNF" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "CFOP" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Vendedor" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "ID" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Parceiro" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "ID" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Produto" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Modelo" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "PartNumber" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Quantidade" &&
                        ds.Tables["Query1"].Columns[aux2].ToString().Trim() != "Valor")
                    {
                        ds.Tables["Query1"].Columns.RemoveAt(aux2);

                        if (aux2 != 0)
                        {
                            aux2 = aux2 - 1;
                        }
                    }
                }
            }

            else if (_NomeRel == "DCICMS-ST")
            {
                for (int aux2 = 0; aux2 < ds.Tables["Query3"].Columns.Count; aux2++)
                {
                    if (ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "ID" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "Produto" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "Finalidade" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "NCM" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "IVAST" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "BaseCalculo" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "ALQInter" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "ALQIntra" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "IVASTAj" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "CFOP" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "Quantidade" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "ValorTotal" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "ValorTotalComIPI" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "BCICMS" &&
                        ds.Tables["Query3"].Columns[aux2].ToString().Trim() != "ICMS")
                    {
                        ds.Tables["Query3"].Columns.RemoveAt(aux2);

                        if (aux2 != 0)
                        {
                            aux2 = aux2 - 1;
                        }
                    }
                }
            }

            else if (_NomeRel == "SolicitDeComp")
            {
                for (int aux2 = 0; aux2 < ds.Tables["Query2"].Columns.Count - 1; aux2++)
                {
                    switch (ds.Tables["Query2"].Columns[aux2].ColumnName.ToString().Trim())
                    {
                        case "PedidoID":
                            ds.Tables["Query2"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "Moeda":
                            ds.Tables["Query2"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "CodigoFornecedor":
                            ds.Tables["Query2"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "ValorConvertido":
                            ds.Tables["Query2"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "TaxaMoeda":
                            ds.Tables["Query2"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "Ordem":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "C";
                            break;

                        case "ProdutoID":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "ID";
                            break;

                        case "Quantidade":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "Quant";
                            break;

                        case "ValorUnitario":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "Unitario";
                            break;

                        case "ValorTotal":
                            ds.Tables["Query2"].Columns[aux2].ColumnName = "Total";
                            break;
                    }
                }

                for (int aux2 = 0; aux2 < ds.Tables["Query3"].Columns.Count - 1; aux2++)
                {
                    switch (ds.Tables["Query3"].Columns[aux2].ColumnName.ToString().Trim())
                    {
                        case "PedidoID":
                            ds.Tables["Query3"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "Tipo":
                            ds.Tables["Query3"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "Observacao":
                            ds.Tables["Query3"].Columns.RemoveAt(aux2);
                            aux2 = aux2 - 1;
                            break;

                        case "Ordem":
                            ds.Tables["Query3"].Columns[aux2].ColumnName = "O";
                            break;
                    }
                }
            }

            else if (_NomeRel == "TotalizaVendas")
            {
                string[] ListColum = (string[])(HttpContext.Current.Session["ListColum"]);

                string[] indexKey2 = (string[])(HttpContext.Current.Session["indexKey2"]);
                string[] indexKey3 = (string[])(HttpContext.Current.Session["indexKey3"]);
                string[] indexKey4 = (string[])(HttpContext.Current.Session["indexKey4"]);
                string[] indexKey5 = (string[])(HttpContext.Current.Session["indexKey5"]);

                string[] FieldsMae3 = (string[])(HttpContext.Current.Session["FieldsMae3"]);
                string[] FieldsMae4 = (string[])(HttpContext.Current.Session["FieldsMae4"]);
                string[] FieldsMae5 = (string[])(HttpContext.Current.Session["FieldsMae5"]);

                string keyAnt = (string)(HttpContext.Current.Session["keyAnt"]);

                DataTable Dt = new DataTable();

                //# Prepara Nível 1

                DataColumn ColumnCount1 = new DataColumn();
                DataColumn ColumnNivel1 = new DataColumn();

                ColumnCount1.ColumnName = "C";
                ColumnNivel1.ColumnName = "N";

                ds.Tables["Query1"].Columns.Add(ColumnCount1);
                ds.Tables["Query1"].Columns.Add(ColumnNivel1);

                int Niv = 1;

                Dt = ds.Tables["Query1"].Clone();

                //# Prepara Nível 2 se existir

                if (ds.Tables["Query2"] != null)
                {

                    DataColumn ColumnCount2 = new DataColumn();
                    DataColumn ColumnNivel2 = new DataColumn();
                    DataColumn ColumnCap = new DataColumn();

                    ColumnCount2.ColumnName = "C";
                    ColumnNivel2.ColumnName = "N";
                    //ColumnCap.ColumnName = "CAP";

                    ds.Tables["Query2"].Columns.Add(ColumnCount2);
                    ds.Tables["Query2"].Columns.Add(ColumnNivel2);

                    //ds.Tables["Query2"].Columns.Remove("CAP");

                    //ColumnCap.DataType = typeof(int);

                    //ds.Tables["Query2"].Columns.Add(ColumnCap);

                    Dt = ds.Tables["Query2"].Clone();
                    ds.Tables["Query2"].Columns.Remove("CAP");
                    Niv = 2;

                    //# Prepara Nível 3 se existir

                    if (ds.Tables["Query3"] != null)
                    {
                        DataColumn ColumnCount3 = new DataColumn();
                        DataColumn ColumnNivel3 = new DataColumn();

                        ColumnCount3.ColumnName = "C";
                        ColumnNivel3.ColumnName = "N";

                        ds.Tables["Query3"].Columns.Add(ColumnCount3);
                        ds.Tables["Query3"].Columns.Add(ColumnNivel3);
                        Dt = ds.Tables["Query3"].Clone();
                        ds.Tables["Query3"].Columns.Remove("CAP");
                        Niv = 3;

                        //# Prepara Nível 4 se existir

                        if (ds.Tables["Query4"] != null)
                        {
                            DataColumn ColumnCount4 = new DataColumn();
                            DataColumn ColumnNivel4 = new DataColumn();

                            ColumnCount4.ColumnName = "C";
                            ColumnNivel4.ColumnName = "N";

                            ds.Tables["Query4"].Columns.Add(ColumnCount4);
                            ds.Tables["Query4"].Columns.Add(ColumnNivel4);

                            Dt = ds.Tables["Query4"].Clone();
                            ds.Tables["Query4"].Columns.Remove("CAP");
                            Niv = 4;

                            //# Prepara Nível 5 se existir

                            if (ds.Tables["Query5"] != null)
                            {
                                DataColumn ColumnCount5 = new DataColumn();
                                DataColumn ColumnNivel5 = new DataColumn();

                                ColumnCount5.ColumnName = "C";
                                ColumnNivel5.ColumnName = "N";

                                ds.Tables["Query5"].Columns.Add(ColumnCount5);
                                ds.Tables["Query5"].Columns.Add(ColumnNivel5);

                                Dt = ds.Tables["Query5"].Clone();
                                ds.Tables["Query5"].Columns.Remove("CAP");
                                Niv = 5;
                            }
                        }
                    }
                }

                Dt.Columns["Nome"].DataType = typeof(string);

                // Nivel 1
                for (int cont1 = 0; cont1 < ds.Tables["Query1"].Rows.Count; cont1++)
                {

                    ds.Tables["Query1"].Rows[cont1]["C"] = (cont1 + 1);

                    if (Niv > 1)
                    {
                        ds.Tables["Query1"].Rows[cont1]["N"] = "N";
                    }

                    Dt.ImportRow(ds.Tables["Query1"].Rows[cont1]);

                    // Nivel 2
                    if (ds.Tables["Query2"] != null)
                    {

                        for (int cont2 = 0; cont2 < ds.Tables["Query2"].Rows.Count; cont2++)
                        {

                            if (ds.Tables["Query2"].Rows[cont2]["Nome"].ToString().Contains("12:00:00") == true)
                            {
                                //string dataStr = Convert.ToDateTime(ds.Tables["Query2"].Rows[cont2]["Nome"].ToString()).ToShortDateString();

                                //DateTime data = Convert.ToDateTime(dataStr);

                                //ds.Tables["Query2"].Rows[cont2]["Nome"] = data; 
                            }

                            ds.Tables["Query2"].Rows[cont2]["C"] = (cont2 + 1);

                            if (Niv > 2)
                            {
                                ds.Tables["Query2"].Rows[cont2]["N"] = "N";
                            }

                            if (ds.Tables["Query1"].Rows[cont1][keyAnt/*"ID"/*indexKey2[1]*/].ToString().Trim() == ds.Tables["Query2"].Rows[cont2][indexKey2[0].Trim()].ToString().Trim())
                            {

                                Dt.ImportRow(ds.Tables["Query2"].Rows[cont2]);

                                //Nivel3
                                if (ds.Tables["Query3"] != null)
                                {
                                    for (int cont3 = 0; cont3 < ds.Tables["Query3"].Rows.Count; cont3++)
                                    {

                                        if (ds.Tables["Query3"].Rows[cont3]["Nome"].ToString().Contains("12:00:00") == true)
                                        {
                                            ds.Tables["Query3"].Rows[cont3]["Nome"] = ds.Tables["Query3"].Rows[cont3]["Nome"].ToString().Substring(0, 9);
                                        }

                                        ds.Tables["Query3"].Rows[cont3]["C"] = (cont3 + 1);

                                        if (Niv > 3)
                                        {
                                            ds.Tables["Query3"].Rows[cont3]["N"] = "N";
                                        }

                                        if (ds.Tables["Query2"].Rows[cont2][FieldsMae3[0].Trim()].ToString().Trim() == ds.Tables["Query3"].Rows[cont3][indexKey3[0].Trim()].ToString().Trim() &&
                                            ds.Tables["Query2"].Rows[cont2][FieldsMae3[1].Trim()].ToString().Trim() == ds.Tables["Query3"].Rows[cont3][indexKey3[1].Trim()].ToString().Trim())
                                        {

                                            Dt.ImportRow(ds.Tables["Query3"].Rows[cont3]);

                                            //Nivel4
                                            if (ds.Tables["Query4"] != null)
                                            {
                                                for (int cont4 = 0; cont4 < ds.Tables["Query4"].Rows.Count; cont4++)
                                                {

                                                    if (ds.Tables["Query4"].Rows[cont4]["Nome"].ToString().Contains("12:00:00") == true)
                                                    {
                                                        ds.Tables["Query4"].Rows[cont4]["Nome"] = ds.Tables["Query4"].Rows[cont4]["Nome"].ToString().Substring(0, 9);
                                                    }

                                                    ds.Tables["Query4"].Rows[cont4]["C"] = (cont4 + 1);

                                                    if (Niv > 4)
                                                    {
                                                        ds.Tables["Query4"].Rows[cont4]["N"] = "N";
                                                    }

                                                    if (ds.Tables["Query3"].Rows[cont3][FieldsMae4[0].Trim()].ToString().Trim() == ds.Tables["Query4"].Rows[cont4][indexKey4[0].Trim()].ToString().Trim() &&
                                                        ds.Tables["Query3"].Rows[cont3][FieldsMae4[1].Trim()].ToString().Trim() == ds.Tables["Query4"].Rows[cont4][indexKey4[1].Trim()].ToString().Trim() &&
                                                        ds.Tables["Query3"].Rows[cont3][FieldsMae4[2].Trim()].ToString().Trim() == ds.Tables["Query4"].Rows[cont4][indexKey4[2].Trim()].ToString().Trim())
                                                    {
                                                        Dt.ImportRow(ds.Tables["Query4"].Rows[cont4]);

                                                        //Nivel5
                                                        if (ds.Tables["Query5"] != null)
                                                        {
                                                            for (int cont5 = 0; cont5 < ds.Tables["Query5"].Rows.Count; cont5++)
                                                            {

                                                                if (ds.Tables["Query5"].Rows[cont5]["Nome"].ToString().Contains("12:00:00") == true)
                                                                {
                                                                    ds.Tables["Query5"].Rows[cont5]["Nome"] = ds.Tables["Query5"].Rows[cont5]["Nome"].ToString().Substring(0, 9);
                                                                }

                                                                ds.Tables["Query5"].Rows[cont5]["C"] = (cont5 + 1);

                                                                if (Niv > 5)
                                                                {
                                                                    ds.Tables["Query5"].Rows[cont5]["N"] = "N";
                                                                }


                                                                if (ds.Tables["Query4"].Rows[cont4][FieldsMae5[0].Trim()].ToString().Trim() == ds.Tables["Query5"].Rows[cont5][indexKey5[0].Trim()].ToString().Trim() &&
                                                                    ds.Tables["Query4"].Rows[cont4][FieldsMae5[1].Trim()].ToString().Trim() == ds.Tables["Query5"].Rows[cont5][indexKey5[1].Trim()].ToString().Trim() &&
                                                                    ds.Tables["Query4"].Rows[cont4][FieldsMae5[2].Trim()].ToString().Trim() == ds.Tables["Query5"].Rows[cont5][indexKey5[2].Trim()].ToString().Trim() &&
                                                                    ds.Tables["Query4"].Rows[cont4][FieldsMae5[3].Trim()].ToString().Trim() == ds.Tables["Query5"].Rows[cont5][indexKey5[3].Trim()].ToString().Trim())
                                                                {
                                                                    Dt.ImportRow(ds.Tables["Query5"].Rows[cont5]);

                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }

                for (int contD = 0; contD < Dt.Rows.Count; contD++)
                {
                    if (Dt.Rows[contD]["Nome"].ToString().Contains("12:00:00") == true)
                    {
                        DateTime data = Convert.ToDateTime(Dt.Rows[contD]["Nome"].ToString());

                        Dt.Rows[contD]["Nome"] = data.Day + "/" + data.Month + "/" + data.Year;
                    }
                }

                Dt.TableName = "Query1";

                ds.Reset();

                ds.Tables.Add(Dt);

                ds.Tables["Query1"].Columns["C"].SetOrdinal(0);
                ds.Tables["Query1"].Columns["N"].SetOrdinal(3);

                for (int aux2 = 0; aux2 < ds.Tables["Query1"].Columns.Count; aux2++)
                {
                    for (int aux3 = 0; aux3 < ListColum.Length; aux3++)
                    {
                        if (ListColum[aux3].ToString().Trim() == ds.Tables["Query1"].Columns[aux2].ColumnName.ToString().Trim() ||
                            ds.Tables["Query1"].Columns[aux2].ColumnName.ToString().Trim() == "C" ||
                            ds.Tables["Query1"].Columns[aux2].ColumnName.ToString().Trim() == "N")
                        {
                            switch (ds.Tables["Query1"].Columns[aux2].ColumnName)
                            {
                                //case "Desp_Ger":
                                //    ds.Tables["Query1"].Columns[aux2].ColumnName = "Desp Ger";
                                //    break;
                                //case "Preco_Base":
                                //    ds.Tables["Query1"].Columns[aux2].ColumnName = "Preço Base";
                                //    break;
                                //case "Custos_Ger":
                                //    ds.Tables["Query1"].Columns[aux2].ColumnName = "Custos Ger";
                                //    break;
                                //case "Custo_Base_Cont":
                                //    ds.Tables["Query1"].Columns[aux2].ColumnName = "Custo Base";
                                //    break;
                                case "Contr_Ger":
                                    ds.Tables["Query1"].Columns[aux2].ColumnName = "Contribuição";
                                    break;
                            }
                            break;
                        }


                        if (aux3 == ListColum.Length - 1)
                        {
                            ds.Tables["Query1"].Columns.RemoveAt(aux2);

                            aux2 = aux2 - 1;
                        }
                    }

                }

                object soma1;
                object soma2;
                object soma3;
                object soma4;
                object soma5;
                object soma6;
                object soma7;
                object soma8;
                object soma9;
                object soma10;
                object soma11;

                DataRow dr = ds.Tables["Query1"].NewRow();

                decimal vl1;
                decimal vl2;

                DBNull Null;

                //System.Globalization.CultureInfo Culture;

                //Culture = System.Globalization.CultureInfo.CreateSpecificCulture("pt-BR");

                if (ListColum[0].ToString() == "Ger")
                {

                    if (ds.Tables["Query1"].Rows[0]["N"].ToString().Trim() == "")
                    {
                        soma1 = ds.Tables["Query1"].Compute("Sum(Pedidos)", "");
                        soma2 = ds.Tables["Query1"].Compute("Sum(Quantidade)", "");
                        soma3 = ds.Tables["Query1"].Compute("Sum(Faturamento)", "");
                        soma4 = ds.Tables["Query1"].Compute("Sum(Impostos)", "");
                        soma5 = ds.Tables["Query1"].Compute("Sum(Desp_Ger)", "");
                        soma6 = ds.Tables["Query1"].Compute("Sum(Preco_Base)", "");
                        soma7 = ds.Tables["Query1"].Compute("Sum(Custos_Ger)", "");

                        //if (ds.Tables["Query1"].Columns["Custo_Base"] != null)
                        //{
                        soma8 = ds.Tables["Query1"].Compute("Sum(Custo_Base)", "");
                        //}
                        //else
                        //{
                        //    soma8 = ds.Tables["Query1"].Compute("Sum(Custo_Base_Cont)", "N=N");
                        //}

                        soma9 = ds.Tables["Query1"].Compute("Sum(Contribuição)", "");

                        vl1 = Convert.ToDecimal(soma9);
                        vl2 = Convert.ToDecimal(soma6);

                        soma10 = (vl1 / vl2) * 100;
                        vl1 = Convert.ToDecimal(soma10);

                        soma11 = ds.Tables["Query1"].Compute("Sum(CAP)", "");

                        Null = DBNull.Value;

                        dr["C"] = Null;
                        dr["N"] = "N";
                        dr["Nome"] = "Totais";
                        dr["Pedidos"] = soma1;
                        dr["Quantidade"] = soma2;
                        dr["Faturamento"] = soma3;
                        dr["Impostos"] = soma4;
                        dr["Desp_Ger"] = soma5;
                        dr["Preco_Base"] = soma6;
                        dr["Custos_Ger"] = soma7;
                        dr["Custo_Base"] = soma8;
                        dr["Contribuição"] = soma9;
                        dr["MC"] = vl1.ToString("N");
                        dr["CAP"] = soma11;
                    }
                    else
                    {
                        soma1 = ds.Tables["Query1"].Compute("Sum(Pedidos)", "N=N");
                        soma2 = ds.Tables["Query1"].Compute("Sum(Quantidade)", "N=N");
                        soma3 = ds.Tables["Query1"].Compute("Sum(Faturamento)", "N=N");
                        soma4 = ds.Tables["Query1"].Compute("Sum(Impostos)", "N=N");
                        soma5 = ds.Tables["Query1"].Compute("Sum(Desp_Ger)", "N=N");
                        soma6 = ds.Tables["Query1"].Compute("Sum(Preco_Base)", "N=N");
                        soma7 = ds.Tables["Query1"].Compute("Sum(Custos_Ger)", "N=N");
                        soma8 = ds.Tables["Query1"].Compute("Sum(Custo_Base)", "N=N");
                        soma9 = ds.Tables["Query1"].Compute("Sum(Contribuição)", "N=N");

                        vl1 = Convert.ToDecimal(soma9);
                        vl2 = Convert.ToDecimal(soma6);

                        soma10 = (vl1 / vl2) * 100;
                        vl1 = Convert.ToDecimal(soma10);

                        soma11 = ds.Tables["Query1"].Compute("Sum(CAP)", "N=N");

                        Null = DBNull.Value;

                        dr["C"] = Null;
                        dr["N"] = "N";
                        dr["Nome"] = "Totais";
                        dr["Pedidos"] = soma1;
                        dr["Quantidade"] = soma2;
                        dr["Faturamento"] = soma3;
                        dr["Impostos"] = soma4;
                        dr["Desp_Ger"] = soma5;
                        dr["Preco_Base"] = soma6;
                        dr["Custos_Ger"] = soma7;
                        dr["Custo_Base"] = soma8;
                        dr["Contribuição"] = soma9;
                        dr["MC"] = vl1.ToString("N");
                        dr["CAP"] = soma11;
                    }


                }
                else
                {
                    soma1 = ds.Tables["Query1"].Compute("Sum(Pedidos)", "N=N");
                    soma2 = ds.Tables["Query1"].Compute("Sum(Quantidade)", "N=N");
                    soma3 = ds.Tables["Query1"].Compute("Sum(Faturamento)", "N=N");
                    soma4 = ds.Tables["Query1"].Compute("Sum(Impostos)", "N=N");
                    soma5 = ds.Tables["Query1"].Compute("Sum(Preco_Base_Cont)", "N=N");
                    soma6 = ds.Tables["Query1"].Compute("Sum(Custo_Base_Cont)", "N=N");
                    soma9 = ds.Tables["Query1"].Compute("Sum(Contr_Cont)", "N=N");

                    vl1 = Convert.ToDecimal(soma9);
                    vl2 = Convert.ToDecimal(soma5);

                    soma10 = (vl1 / vl2) * 100;
                    vl1 = Convert.ToDecimal(soma10);

                    soma11 = ds.Tables["Query1"].Compute("Sum(CAP)", "");

                    Null = DBNull.Value;

                    dr["C"] = Null;
                    dr["N"] = "N";
                    dr["Nome"] = "Totais";
                    dr["Pedidos"] = soma1;
                    dr["Quantidade"] = soma2;
                    dr["Faturamento"] = soma3;
                    dr["Impostos"] = soma4;
                    dr["Preco_Base_Cont"] = soma5;
                    dr["Custo_Base_Cont"] = soma6;
                    dr["Contr_Cont"] = soma9;
                    dr["MC_Cont"] = vl1.ToString("#,0.00");
                    dr["CAP"] = soma11;
                }
                ds.Tables["Query1"].Rows.Add(dr);
            }
            return ds;
        }

        private DataSet PreparaTabela(DataSet Ds, string[,] IndexKeys)
        {

            char[] DelimitChar = { ';' };

            DataTable Dt = new DataTable();

            DataSet ds = new DataSet();

            ds = Ds;

            //# Prepara Nível 1

            DataColumn ColumnCount1 = new DataColumn();
            DataColumn ColumnNivel1 = new DataColumn();

            ColumnCount1.ColumnName = "C";
            ColumnNivel1.ColumnName = "N";

            ds.Tables[_sqlQuery[0, 0]].Columns.Add(ColumnCount1);
            ds.Tables[_sqlQuery[0, 0]].Columns.Add(ColumnNivel1);

            int Niv = 1;

            Dt = ds.Tables[_sqlQuery[0, 0]].Clone();

            //# Prepara Nível 2 se existir

            if (IndexKeys.GetLength(1) >= 1)
            {
                DataColumn ColumnCount2 = new DataColumn();
                DataColumn ColumnNivel2 = new DataColumn();
                DataColumn ColumnCap = new DataColumn();

                ColumnCount2.ColumnName = "C";
                ColumnNivel2.ColumnName = "N";
                //ColumnCap.ColumnName = "CAP";

                ds.Tables[_sqlQuery[1, 0]].Columns.Add(ColumnCount2);
                ds.Tables[_sqlQuery[1, 0]].Columns.Add(ColumnNivel2);
                Dt = ds.Tables[_sqlQuery[1, 0]].Clone();
                //ds.Tables[_sqlQuery[1, 0]].Columns.Remove("CAP");
                Niv = 2;


                //# Prepara Nível 3 se existir
                if (IndexKeys.GetLength(0) >= 2)
                {
                    DataColumn ColumnCount3 = new DataColumn();
                    DataColumn ColumnNivel3 = new DataColumn();

                    ColumnCount3.ColumnName = "C";
                    ColumnNivel3.ColumnName = "N";

                    ds.Tables[_sqlQuery[2, 0]].Columns.Add(ColumnCount3);
                    ds.Tables[_sqlQuery[2, 0]].Columns.Add(ColumnNivel3);
                    Dt = ds.Tables[_sqlQuery[2, 0]].Clone();
                    //ds.Tables[_sqlQuery[2, 0]].Columns.Remove("CAP");
                    Niv = 3;

                    //# Prepara Nível 4 se existir
                    if (IndexKeys.GetLength(0) >= 3)
                    {
                        DataColumn ColumnCount4 = new DataColumn();
                        DataColumn ColumnNivel4 = new DataColumn();

                        ColumnCount4.ColumnName = "C";
                        ColumnNivel4.ColumnName = "N";

                        ds.Tables[_sqlQuery[3, 0]].Columns.Add(ColumnCount4);
                        ds.Tables[_sqlQuery[3, 0]].Columns.Add(ColumnNivel4);

                        Dt = ds.Tables[_sqlQuery[3, 0]].Clone();
                        //ds.Tables[_sqlQuery[3, 0]].Columns.Remove("CAP");
                        Niv = 4;

                        //# Prepara Nível 5 se existir

                        if (IndexKeys.GetLength(0) >= 4)
                        {
                            DataColumn ColumnCount5 = new DataColumn();
                            DataColumn ColumnNivel5 = new DataColumn();

                            ColumnCount5.ColumnName = "C";
                            ColumnNivel5.ColumnName = "N";

                            ds.Tables[_sqlQuery[4, 0]].Columns.Add(ColumnCount5);
                            ds.Tables[_sqlQuery[4, 0]].Columns.Add(ColumnNivel5);

                            Dt = ds.Tables[_sqlQuery[4, 0]].Clone();
                            //ds.Tables[_sqlQuery[4, 0]].Columns.Remove("CAP");
                            Niv = 5;
                        }
                    }
                }
            }
            Dt.Columns["Nome"].DataType = typeof(string);

            // Nivel 1
            for (int cont1 = 0; cont1 < ds.Tables[_sqlQuery[0, 0]].Rows.Count; cont1++)
            {

                ds.Tables[_sqlQuery[0, 0]].Rows[cont1]["C"] = (cont1 + 1);

                if (Niv > 1)
                {
                    ds.Tables[_sqlQuery[0, 0]].Rows[cont1]["N"] = "N";
                }

                Dt.ImportRow(ds.Tables[_sqlQuery[0, 0]].Rows[cont1]);

                // Nivel 2
                if (_sqlQuery.GetLength(0) >= 2)
                {

                    for (int cont2 = 0; cont2 < ds.Tables[_sqlQuery[1, 0]].Rows.Count; cont2++)
                    {
                        ds.Tables[_sqlQuery[1, 0]].Rows[cont2]["C"] = (cont2 + 1);

                        if (Niv > 2)
                        {
                            ds.Tables[_sqlQuery[1, 0]].Rows[cont2]["N"] = "N";
                        }

                        string[] indexKey2 = IndexKeys[0, 1].Split(DelimitChar);
                        string[] FieldsMae2 = IndexKeys[0, 2].Split(DelimitChar);

                        if (ds.Tables[_sqlQuery[0, 0]].Rows[cont1][FieldsMae2[0]].ToString().Trim() == ds.Tables[_sqlQuery[1, 0]].Rows[cont2][indexKey2[0].Trim()].ToString().Trim())
                        {

                            Dt.ImportRow(ds.Tables[_sqlQuery[1, 0]].Rows[cont2]);

                            //Nivel3
                            if (_sqlQuery.GetLength(0) >= 3)
                            {
                                for (int cont3 = 0; cont3 < ds.Tables[_sqlQuery[2, 0]].Rows.Count; cont3++)
                                {

                                    if (ds.Tables[_sqlQuery[2, 0]].Rows[cont3]["Nome"].ToString().Contains("12:00:00") == true)
                                    {
                                        ds.Tables[_sqlQuery[2, 0]].Rows[cont3]["Nome"] = ds.Tables[_sqlQuery[2, 0]].Rows[cont3]["Nome"].ToString().Substring(0, 9);
                                    }

                                    ds.Tables[_sqlQuery[2, 0]].Rows[cont3]["C"] = (cont3 + 1);

                                    if (Niv > 3)
                                    {
                                        ds.Tables[_sqlQuery[2, 0]].Rows[cont3]["N"] = "N";
                                    }

                                    string[] indexKey3 = IndexKeys[1, 1].Split(DelimitChar);
                                    string[] FieldsMae3 = IndexKeys[1, 2].Split(DelimitChar);

                                    if (ds.Tables[_sqlQuery[1, 0]].Rows[cont2][FieldsMae3[0].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[2, 0]].Rows[cont3][indexKey3[0].Trim()].ToString().Trim() &&
                                        ds.Tables[_sqlQuery[1, 0]].Rows[cont2][FieldsMae3[1].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[2, 0]].Rows[cont3][indexKey3[1].Trim()].ToString().Trim())
                                    {

                                        Dt.ImportRow(ds.Tables[_sqlQuery[2, 0]].Rows[cont3]);

                                        //Nivel4
                                        if (_sqlQuery.GetLength(0) >= 4)
                                        {
                                            for (int cont4 = 0; cont4 < ds.Tables[_sqlQuery[3, 0]].Rows.Count; cont4++)
                                            {

                                                if (ds.Tables[_sqlQuery[3, 0]].Rows[cont4]["Nome"].ToString().Contains("12:00:00") == true)
                                                {
                                                    ds.Tables[_sqlQuery[3, 0]].Rows[cont4]["Nome"] = ds.Tables[_sqlQuery[3, 0]].Rows[cont4]["Nome"].ToString().Substring(0, 9);
                                                }

                                                ds.Tables[_sqlQuery[3, 0]].Rows[cont4]["C"] = (cont4 + 1);

                                                if (Niv > 4)
                                                {
                                                    ds.Tables[_sqlQuery[3, 0]].Rows[cont4]["N"] = "N";
                                                }

                                                string[] indexKey4 = IndexKeys[2, 1].Split(DelimitChar);
                                                string[] FieldsMae4 = IndexKeys[2, 2].Split(DelimitChar);

                                                if (ds.Tables[_sqlQuery[2, 0]].Rows[cont3][FieldsMae4[0].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[3, 0]].Rows[cont4][indexKey4[0].Trim()].ToString().Trim() &&
                                                    ds.Tables[_sqlQuery[2, 0]].Rows[cont3][FieldsMae4[1].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[3, 0]].Rows[cont4][indexKey4[1].Trim()].ToString().Trim() &&
                                                    ds.Tables[_sqlQuery[2, 0]].Rows[cont3][FieldsMae4[2].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[3, 0]].Rows[cont4][indexKey4[2].Trim()].ToString().Trim())
                                                {
                                                    Dt.ImportRow(ds.Tables[_sqlQuery[3, 0]].Rows[cont4]);

                                                    //Nivel5
                                                    if (_sqlQuery.GetLength(0) >= 5)
                                                    {
                                                        for (int cont5 = 0; cont5 < ds.Tables[_sqlQuery[4, 0]].Rows.Count; cont5++)
                                                        {

                                                            if (ds.Tables[_sqlQuery[4, 0]].Rows[cont5]["Nome"].ToString().Contains("12:00:00") == true)
                                                            {
                                                                ds.Tables[_sqlQuery[4, 0]].Rows[cont5]["Nome"] = ds.Tables[_sqlQuery[4, 0]].Rows[cont5]["Nome"].ToString().Substring(0, 9);
                                                            }

                                                            ds.Tables[_sqlQuery[4, 0]].Rows[cont5]["C"] = (cont5 + 1);

                                                            if (Niv > 5)
                                                            {
                                                                ds.Tables[_sqlQuery[4, 0]].Rows[cont5]["N"] = "N";
                                                            }

                                                            string[] indexKey5 = IndexKeys[3, 1].Split(DelimitChar);
                                                            string[] FieldsMae5 = IndexKeys[3, 2].Split(DelimitChar);

                                                            if (ds.Tables[_sqlQuery[3, 0]].Rows[cont4][FieldsMae5[0].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[4, 0]].Rows[cont5][indexKey5[0].Trim()].ToString().Trim() &&
                                                                ds.Tables[_sqlQuery[3, 0]].Rows[cont4][FieldsMae5[1].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[4, 0]].Rows[cont5][indexKey5[1].Trim()].ToString().Trim() &&
                                                                ds.Tables[_sqlQuery[3, 0]].Rows[cont4][FieldsMae5[2].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[4, 0]].Rows[cont5][indexKey5[2].Trim()].ToString().Trim() &&
                                                                ds.Tables[_sqlQuery[3, 0]].Rows[cont4][FieldsMae5[3].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[4, 0]].Rows[cont5][indexKey5[3].Trim()].ToString().Trim())
                                                            {
                                                                Dt.ImportRow(ds.Tables[_sqlQuery[4, 0]].Rows[cont5]);

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (int contD = 0; contD < Dt.Rows.Count; contD++)
            {
                if (Dt.Rows[contD]["Nome"].ToString().Contains("12:00:00") == true)
                {
                    DateTime data = Convert.ToDateTime(Dt.Rows[contD]["Nome"].ToString());

                    Dt.Rows[contD]["Nome"] = data.Day + "/" + data.Month + "/" + data.Year;
                }
            }

            Dt.TableName = "Query1";

            ds.Reset();

            ds.Tables.Add(Dt);

            ds.Tables["Query1"].Columns["C"].SetOrdinal(0);
            ds.Tables["Query1"].Columns["N"].SetOrdinal(3);

            return ds;
        }

        private DataSet PreparaTabelaTeste(DataSet Ds, string[,] IndexKeys, bool Reset)
        {
            char[] DelimitChar = { ';' };
            int posicao;
            string[] Query;
            string[] indexKey;
            string[] FieldsMae;

            DataTable Dt = new DataTable();

            DataSet ds = new DataSet();
            ds = Ds;

            //# Prepara Nível 1

            DataColumn ColumnCount1 = new DataColumn();
            DataColumn ColumnNivel1 = new DataColumn();

            if (IndexKeys.Length > 1)
            {
                Query = IndexKeys[0, 0].Split(DelimitChar);
                indexKey = IndexKeys[0, 1].Split(DelimitChar);
                FieldsMae = IndexKeys[0, 2].Split(DelimitChar);

                posicao = (Assek(_sqlQuery, Query[0].Trim()) - 1);
            }
            else
                posicao = (Assek(_sqlQuery, "Query1"));

            ColumnCount1.ColumnName = "C";
            ColumnNivel1.ColumnName = "N";

            ds.Tables[_sqlQuery[posicao, 0]].Columns.Add(ColumnCount1);
            ds.Tables[_sqlQuery[posicao, 0]].Columns.Add(ColumnNivel1);

            int Niv = 1;

            Dt = ds.Tables[_sqlQuery[posicao, 0]].Clone();

            //# Prepara Nível 2 se existir

            if (IndexKeys.GetLength(1) > 1)
            {

                DataColumn ColumnCount2 = new DataColumn();
                DataColumn ColumnNivel2 = new DataColumn();
                DataColumn ColumnCap = new DataColumn();

                DataColumn C1_1 = new DataColumn();
                DataColumn C2_1 = new DataColumn();

                if (IndexKeys.Length > 1)
                {
                    Query = IndexKeys[0, 0].Split(DelimitChar);
                    posicao = Assek(_sqlQuery, Query[0].Trim());
                }
                else
                    posicao = (Assek(_sqlQuery, "Query1"));


                ColumnCount2.ColumnName = "C";
                ColumnNivel2.ColumnName = "N";

                ds.Tables[_sqlQuery[posicao, 0]].Columns.Add(ColumnCount2);
                ds.Tables[_sqlQuery[posicao, 0]].Columns.Add(ColumnNivel2);

                //if (_NomeRel == "TotalizaVendas")
                //{
                C1_1.ColumnName = "C1";
                C2_1.ColumnName = "C2";

                ds.Tables[_sqlQuery[posicao, 0]].Columns.Add(C1_1);
                ds.Tables[_sqlQuery[posicao, 0]].Columns.Add(C2_1);
                //}

                foreach (DataColumn DC in ds.Tables[_sqlQuery[posicao, 0]].Columns)
                {
                    bool colExiste = false;

                    foreach (DataColumn DC2 in Dt.Columns)
                    {
                        if (DC.ColumnName != DC2.ColumnName)
                        {
                            colExiste = false;
                        }
                        else
                        {
                            colExiste = true;
                            break;
                        }
                    }
                    if (!colExiste)
                    {
                        Dt.Columns.Add(DC.ColumnName, DC.DataType);
                    }
                }

                Niv = 2;

                //# Prepara Nível 3 se existir
                if (IndexKeys.GetLength(0) >= 2)
                {
                    DataColumn ColumnCount3 = new DataColumn();
                    DataColumn ColumnNivel3 = new DataColumn();

                    DataColumn C1_2 = new DataColumn();
                    DataColumn C2_2 = new DataColumn();
                    DataColumn C3_2 = new DataColumn();
                    DataColumn C4_2 = new DataColumn();

                    string[] Query2 = IndexKeys[1, 0].Split(DelimitChar);
                    int posicao2 = Assek(_sqlQuery, Query2[0].Trim());

                    ColumnCount3.ColumnName = "C";
                    ColumnNivel3.ColumnName = "N";

                    ds.Tables[_sqlQuery[posicao2, 0]].Columns.Add(ColumnCount3);
                    ds.Tables[_sqlQuery[posicao2, 0]].Columns.Add(ColumnNivel3);
                    Dt = ds.Tables[_sqlQuery[posicao2, 0]].Clone();

                    //ds.Tables[_sqlQuery[2, 0]].Columns.Remove("CAP");

                    //  if (_NomeRel == "TotalizaVendas")
                    //{
                    C1_2.ColumnName = "C1";
                    C2_2.ColumnName = "C2";
                    C3_2.ColumnName = "C3";
                    C4_2.ColumnName = "C4";

                    ds.Tables[_sqlQuery[posicao2, 0]].Columns.Add(C1_2);
                    ds.Tables[_sqlQuery[posicao2, 0]].Columns.Add(C2_2);
                    ds.Tables[_sqlQuery[posicao2, 0]].Columns.Add(C3_2);
                    ds.Tables[_sqlQuery[posicao2, 0]].Columns.Add(C4_2);
                    //}
                    foreach (DataColumn DC in ds.Tables[_sqlQuery[posicao2, 0]].Columns)
                    {

                        bool colExiste = false;

                        foreach (DataColumn DC2 in Dt.Columns)
                        {

                            if (DC.ColumnName != DC2.ColumnName)
                            {
                                colExiste = false;
                            }
                            else
                            {
                                colExiste = true;
                                break;
                            }
                        }
                        if (!colExiste)
                        {
                            Dt.Columns.Add(DC.ColumnName, DC.DataType);
                        }
                    }

                    Niv = 3;
                    //# Prepara Nível 4 se existir
                    if (IndexKeys.GetLength(0) >= 3)
                    {
                        DataColumn ColumnCount4 = new DataColumn();
                        DataColumn ColumnNivel4 = new DataColumn();

                        string[] Query3 = IndexKeys[2, 0].Split(DelimitChar);
                        int posicao3 = Assek(_sqlQuery, Query3[0].Trim());


                        DataColumn C1_3 = new DataColumn();
                        DataColumn C2_3 = new DataColumn();
                        DataColumn C3_3 = new DataColumn();
                        DataColumn C4_3 = new DataColumn();
                        DataColumn C5_3 = new DataColumn();
                        DataColumn C6_3 = new DataColumn();


                        ColumnCount4.ColumnName = "C";
                        ColumnNivel4.ColumnName = "N";

                        ds.Tables[_sqlQuery[posicao3, 0]].Columns.Add(ColumnCount4);
                        ds.Tables[_sqlQuery[posicao3, 0]].Columns.Add(ColumnNivel4);

                        Dt = ds.Tables[_sqlQuery[3, 0]].Clone();
                        //ds.Tables[_sqlQuery[3, 0]].Columns.Remove("CAP");

                        // (_NomeRel == "TotalizaVendas")
                        //{
                        C1_3.ColumnName = "C1";
                        C2_3.ColumnName = "C2";
                        C3_3.ColumnName = "C3";
                        C4_3.ColumnName = "C4";
                        C5_3.ColumnName = "C5";
                        C6_3.ColumnName = "C6";

                        ds.Tables[_sqlQuery[posicao3, 0]].Columns.Add(C1_3);
                        ds.Tables[_sqlQuery[posicao3, 0]].Columns.Add(C2_3);
                        ds.Tables[_sqlQuery[posicao3, 0]].Columns.Add(C3_3);
                        ds.Tables[_sqlQuery[posicao3, 0]].Columns.Add(C4_3);
                        ds.Tables[_sqlQuery[posicao3, 0]].Columns.Add(C5_3);
                        ds.Tables[_sqlQuery[posicao3, 0]].Columns.Add(C6_3);
                        //}
                        foreach (DataColumn DC in ds.Tables[_sqlQuery[3, 0]].Columns)
                        {

                            bool colExiste = false;

                            foreach (DataColumn DC2 in Dt.Columns)
                            {

                                if (DC.ColumnName != DC2.ColumnName)
                                {
                                    colExiste = false;
                                }
                                else
                                {
                                    colExiste = true;
                                    break;
                                }

                            }

                            if (!colExiste)
                            {
                                Dt.Columns.Add(DC.ColumnName, DC.DataType);
                            }

                        }

                        Niv = 4;

                        //# Prepara Nível 5 se existir
                        if (IndexKeys.GetLength(0) >= 4)
                        {
                            DataColumn ColumnCount5 = new DataColumn();
                            DataColumn ColumnNivel5 = new DataColumn();

                            DataColumn C1_4 = new DataColumn();
                            DataColumn C2_4 = new DataColumn();
                            DataColumn C3_4 = new DataColumn();
                            DataColumn C4_4 = new DataColumn();
                            DataColumn C5_4 = new DataColumn();
                            DataColumn C6_4 = new DataColumn();
                            DataColumn C7_4 = new DataColumn();
                            DataColumn C8_4 = new DataColumn();

                            ColumnCount5.ColumnName = "C";
                            ColumnNivel5.ColumnName = "N";

                            ds.Tables[_sqlQuery[4, 0]].Columns.Add(ColumnCount5);
                            ds.Tables[_sqlQuery[4, 0]].Columns.Add(ColumnNivel5);

                            Dt = ds.Tables[_sqlQuery[4, 0]].Clone();

                            if (_NomeRel == "TotalizaVendas")
                            {
                                C1_4.ColumnName = "C1";
                                C2_4.ColumnName = "C2";
                                C3_4.ColumnName = "C3";
                                C4_4.ColumnName = "C4";
                                C5_4.ColumnName = "C5";
                                C6_4.ColumnName = "C6";
                                C7_4.ColumnName = "C7";
                                C8_4.ColumnName = "C8";

                                ds.Tables[_sqlQuery[4, 0]].Columns.Add(C1_4);
                                ds.Tables[_sqlQuery[4, 0]].Columns.Add(C2_4);
                                ds.Tables[_sqlQuery[4, 0]].Columns.Add(C3_4);
                                ds.Tables[_sqlQuery[4, 0]].Columns.Add(C4_4);
                                ds.Tables[_sqlQuery[4, 0]].Columns.Add(C5_4);
                                ds.Tables[_sqlQuery[4, 0]].Columns.Add(C6_4);
                                ds.Tables[_sqlQuery[4, 0]].Columns.Add(C7_4);
                                ds.Tables[_sqlQuery[4, 0]].Columns.Add(C8_4);
                            }
                            foreach (DataColumn DC in ds.Tables[_sqlQuery[4, 0]].Columns)
                            {

                                bool colExiste = false;

                                foreach (DataColumn DC2 in Dt.Columns)
                                {

                                    if (DC.ColumnName != DC2.ColumnName)
                                    {
                                        colExiste = false;
                                    }
                                    else
                                    {
                                        colExiste = true;
                                        break;
                                    }

                                }

                                if (!colExiste)
                                {
                                    Dt.Columns.Add(DC.ColumnName, DC.DataType);
                                }

                            }

                            Niv = 5;
                        }
                    }
                }
            }

            if (Dt.Columns.Contains("Nome"))
                Dt.Columns["Nome"].DataType = typeof(string);

            // Nivel 1
            //for (int cont1 = 0; cont1 < ds.Tables[_sqlQuery[posicao, 0]].Rows.Count; cont1++)
            //  {


            if (IndexKeys.Length > 1)
            {
                Query = IndexKeys[0, 0].Split(DelimitChar);
                indexKey = IndexKeys[0, 1].Split(DelimitChar);
                FieldsMae = IndexKeys[0, 2].Split(DelimitChar);

                posicao = (Assek(_sqlQuery, Query[0].Trim()) - 1);
            }
            else
            {
                FieldsMae = new string[] { "0" };

                posicao = (Assek(_sqlQuery, "Query1"));
            }


            for (int cont1 = 0; cont1 < ds.Tables[_sqlQuery[posicao, 0]].Rows.Count; cont1++)
            {
                ds.Tables[_sqlQuery[posicao, 0]].Rows[cont1]["C"] = (cont1 + 1);

                if (Niv > 1)
                {
                    ds.Tables[_sqlQuery[posicao, 0]].Rows[cont1]["N"] = "N";
                }

                Dt.ImportRow(ds.Tables[_sqlQuery[posicao, 0]].Rows[cont1]);

                // Nivel 2
                if (IndexKeys.GetLength(1) > 1)
                {
                    //for (int cont2 = 0; cont2 < ds.Tables[_sqlQuery[posicao, 0]].Rows.Count; cont2++)
                    //{
                    string[] Query2 = IndexKeys[0, 0].Split(DelimitChar);
                    string[] indexKey2 = IndexKeys[0, 1].Split(DelimitChar);
                    string[] FieldsMae2 = IndexKeys[0, 2].Split(DelimitChar);
                    int posicao2 = Assek(_sqlQuery, Query2[0].Trim());

                    for (int cont2 = 0; cont2 < ds.Tables[_sqlQuery[posicao2, 0]].Rows.Count; cont2++)
                    {
                        ds.Tables[_sqlQuery[posicao2, 0]].Rows[cont2]["C"] = (cont2 + 1);

                        if (Niv > 2)
                        {
                            ds.Tables[_sqlQuery[posicao2, 0]].Rows[cont2]["N"] = "N";
                        }
                        if (ds.Tables[_sqlQuery[posicao2, 0]].Columns.Contains("C1"))
                        {
                            for (int i = 0; i < indexKey2.Length; i++)
                            {
                                ds.Tables[_sqlQuery[posicao2, 0]].Rows[cont2]["C" + (i + 1)] = ds.Tables[_sqlQuery[posicao2, 0]].Rows[cont2][indexKey2[i].Trim()];
                            }
                        }

                        if (ds.Tables[_sqlQuery[posicao, 0]].Rows[cont1][FieldsMae[0].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao2, 0]].Rows[cont2][indexKey2[0].Trim()].ToString().Trim())
                        {
                            Dt.ImportRow(ds.Tables[_sqlQuery[posicao2, 0]].Rows[cont2]);

                            //Nivel3
                            if (IndexKeys.GetLength(0) >= 2)
                            {

                                //for (int cont3 = 0; cont3 < ds.Tables[_sqlQuery[posicao2, 0]].Rows.Count; cont3++)
                                //{
                                string[] Query3 = IndexKeys[1, 0].Split(DelimitChar);
                                string[] indexKey3 = IndexKeys[1, 1].Split(DelimitChar);
                                string[] FieldsMae3 = IndexKeys[1, 2].Split(DelimitChar);
                                int posicao3 = Assek(_sqlQuery, Query3[0].Trim());

                                for (int cont3 = 0; cont3 < ds.Tables[_sqlQuery[posicao3, 0]].Rows.Count; cont3++)
                                {
                                    if (ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3]["Nome"].ToString().Contains("12:00:00") == true)
                                    {
                                        ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3]["Nome"] = ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3]["Nome"].ToString().Substring(0, 9);
                                    }

                                    ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3]["C"] = (cont3 + 1);

                                    if (Niv > 3)
                                    {
                                        ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3]["N"] = "N";
                                    }

                                    if (ds.Tables[_sqlQuery[posicao3, 0]].Columns.Contains("C1"))
                                    {
                                        for (int i = 0; i < indexKey3.Length; i++)
                                        {
                                            ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3]["C" + (i + 1)] = ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3][indexKey3[i].Trim()];
                                        }
                                    }

                                    if (ds.Tables[_sqlQuery[posicao2, 0]].Rows[cont2][FieldsMae3[0].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3][indexKey3[0].Trim()].ToString().Trim() &&
                                        ds.Tables[_sqlQuery[posicao2, 0]].Rows[cont2][FieldsMae3[1].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3][indexKey3[1].Trim()].ToString().Trim())
                                    {

                                        Dt.ImportRow(ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3]);

                                        //Nivel4
                                        if (IndexKeys.GetLength(0) >= 3)
                                        {
                                            //for (int cont4 = 0; cont4 < ds.Tables[_sqlQuery[posicao3, 0]].Rows.Count; cont4++)
                                            //{
                                            string[] Query4 = IndexKeys[2, 0].Split(DelimitChar);
                                            string[] indexKey4 = IndexKeys[2, 1].Split(DelimitChar);
                                            string[] FieldsMae4 = IndexKeys[2, 2].Split(DelimitChar);
                                            int posicao4 = Assek(_sqlQuery, Query4[0].Trim());

                                            for (int cont4 = 0; cont4 < ds.Tables[_sqlQuery[posicao4, 0]].Rows.Count; cont4++)
                                            {
                                                if (ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4]["Nome"].ToString().Contains("12:00:00") == true)
                                                {
                                                    ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4]["Nome"] = ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4]["Nome"].ToString().Substring(0, 9);
                                                }

                                                ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4]["C"] = (cont4 + 1);

                                                if (Niv > 4)
                                                {
                                                    ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4]["N"] = "N";
                                                }

                                                if (ds.Tables[_sqlQuery[posicao4, 0]].Columns.Contains("C1"))
                                                {
                                                    for (int i = 0; i < indexKey4.Length; i++)
                                                    {
                                                        ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4]["C" + (i + 1)] = ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4][indexKey4[i].Trim()];
                                                    }
                                                }

                                                if (ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3][FieldsMae4[0].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4][indexKey4[0].Trim()].ToString().Trim() &&
                                                    ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3][FieldsMae4[1].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4][indexKey4[1].Trim()].ToString().Trim() &&
                                                    ds.Tables[_sqlQuery[posicao3, 0]].Rows[cont3][FieldsMae4[2].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4][indexKey4[2].Trim()].ToString().Trim())
                                                {
                                                    Dt.ImportRow(ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4]);

                                                    //Nivel5
                                                    if (IndexKeys.GetLength(0) >= 4)
                                                    {
                                                        //for (int cont5 = 0; cont5 < ds.Tables[_sqlQuery[posicao4, 0]].Rows.Count; cont5++)
                                                        //{

                                                        string[] Query5 = IndexKeys[3, 0].Split(DelimitChar);
                                                        string[] indexKey5 = IndexKeys[3, 1].Split(DelimitChar);
                                                        string[] FieldsMae5 = IndexKeys[3, 2].Split(DelimitChar);
                                                        int posicao5 = Assek(_sqlQuery, Query5[0].Trim());

                                                        for (int cont5 = 0; cont5 < ds.Tables[_sqlQuery[posicao5, 0]].Rows.Count; cont5++)
                                                        {

                                                            if (ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5]["Nome"].ToString().Contains("12:00:00") == true)
                                                            {
                                                                ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5]["Nome"] = ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5]["Nome"].ToString().Substring(0, 9);
                                                            }

                                                            ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5]["C"] = (cont5 + 1);

                                                            if (Niv > 5)
                                                            {
                                                                ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5]["N"] = "N";
                                                            }

                                                            if (ds.Tables[_sqlQuery[posicao5, 0]].Columns.Contains("C1"))
                                                            {
                                                                for (int i = 0; i < indexKey5.Length; i++)
                                                                {
                                                                    ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5]["C" + (i + 1)] = ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5][indexKey5[i].Trim()];
                                                                }
                                                            }

                                                            if (ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4][FieldsMae5[0].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5][indexKey5[0].Trim()].ToString().Trim() &&
                                                                ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4][FieldsMae5[1].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5][indexKey5[1].Trim()].ToString().Trim() &&
                                                                ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4][FieldsMae5[2].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5][indexKey5[2].Trim()].ToString().Trim() &&
                                                                ds.Tables[_sqlQuery[posicao4, 0]].Rows[cont4][FieldsMae5[3].Trim()].ToString().Trim() == ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5][indexKey5[3].Trim()].ToString().Trim())
                                                            {
                                                                Dt.ImportRow(ds.Tables[_sqlQuery[posicao5, 0]].Rows[cont5]);

                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }

            for (int contD = 0; contD < Dt.Rows.Count; contD++)
            {
                if (Dt.Columns.Contains("Nome"))
                {
                    if (Dt.Rows[contD]["Nome"].ToString().Contains("12:00:00") == true)
                    {
                        DateTime data = Convert.ToDateTime(Dt.Rows[contD]["Nome"].ToString());

                        Dt.Rows[contD]["Nome"] = data.Day + "/" + data.Month + "/" + data.Year;
                    }
                }
            }

            Dt.TableName = "Query1";

            //if ((_NomeRel == "TotalizaVendas") || (_NomeRel == "RelatorioDeVendas"))
            if (Reset)
            {
                ds.Reset();
            }

            ds.Tables.Add(Dt);

            ds.Tables["Query1"].Columns["C"].SetOrdinal(0);
            ds.Tables["Query1"].Columns["N"].SetOrdinal(3);

            return ds;
        }
        private DataSet TotalFild(DataSet ds, string NomeColuna)
        {
            DataRow dr;

            if (contTotalFild == 0)
            {
                dr = ds.Tables["Query1"].NewRow();
            }
            else
            {
                dr = ds.Tables["Query1"].Rows[ds.Tables["Query1"].Rows.Count - 1];
            }
            object soma1;
            decimal vl1;
            decimal vl2;

            DBNull Null;

            if (ds.Tables["Query1"].Rows[0]["N"].ToString().Trim() == "")
            {
                soma1 = ds.Tables["Query1"].Compute("Sum(" + NomeColuna + ")", "");

                if (NomeColuna == "MC_Cont" || NomeColuna == "MC")
                {
                    if (ds.Tables["Query1"].Columns["Contr_Ger"] != null)
                    {
                        vl1 = Convert.ToDecimal(ds.Tables["Query1"].Compute("Sum(Contr_Ger)", ""));
                    }
                    else
                    {
                        vl1 = Convert.ToDecimal(ds.Tables["Query1"].Compute("Sum(Contr_Cont)", ""));
                    }

                    vl2 = Convert.ToDecimal(ds.Tables["Query1"].Compute("Sum(Preco_Base)", ""));
                    vl1 = Convert.ToDecimal((vl1 / vl2) * 100);

                    dr[NomeColuna] = vl1.ToString("N");
                }
                else
                {

                    dr[NomeColuna] = soma1;
                }

                Null = DBNull.Value;

                dr[0] = Null;
                dr["N"] = "N";

                if (ds.Tables["Query1"].Columns.Contains("Nome"))
                    dr["Nome"] = "Totais";
            }
            else
            {
                soma1 = ds.Tables["Query1"].Compute("Sum(" + NomeColuna + ")", "N=N");

                if (NomeColuna == "MC_Cont" || NomeColuna == "MC")
                {

                    if (ds.Tables["Query1"].Columns["Contr_Ger"] != null)
                    {
                        vl1 = Convert.ToDecimal(ds.Tables["Query1"].Compute("Sum(Contr_Ger)", ""));
                    }
                    else
                    {
                        vl1 = Convert.ToDecimal(ds.Tables["Query1"].Compute("Sum(Contr_Cont)", ""));
                    }

                    vl2 = Convert.ToDecimal(ds.Tables["Query1"].Compute("Sum(Preco_Base)", ""));
                    vl1 = Convert.ToDecimal((vl1 / vl2) * 100);

                    dr[NomeColuna] = vl1.ToString("N");
                }
                else
                {

                    dr[NomeColuna] = soma1;
                }
                Null = DBNull.Value;
                dr[0] = Null;
                dr["N"] = "N";
                if (ds.Tables["Query1"].Columns.Contains("Nome"))
                    dr["Nome"] = "Totais";
            }
            if (contTotalFild == 0)
            {
                ds.Tables["Query1"].Rows.Add(dr);
                contTotalFild = 1;
            }
            return ds;
        }
        public void CriarObjColunaNaTabela(string NomeHeader, string NomeCampo, bool LarguraAutoAjust, string Largura_NomeCampo_Larguracm, string _Alinhamento = "Default", string TamFonte = "Default", string AlturaLinha = "Default")
        {

            // Se Retorno do DataTable estiver vazio finaliza o processo
            int Datateste = 0;
            bool nulo = false;

            Datateste = DsTable.Tables.Count;

            for (int i = 0; i < Datateste; i++)
            {
                if ((DsTable.Tables[i].Rows.Count == 0) && (_NomeRel != "Invoice"))
                    nulo = true;
            }


            if (nulo)
            {
                return;
            }

            string _PaddingRight = "4";
            string _PaddingLeft = "0";
            string _ColumnHide = "";

            string _ColorLColumn = "";

            string centimeters;
            string container;
            string _headColor = "Silver";

            if (TamFonte == "Default")
            {
                _TamFonte = "8";
            }
            else
            {
                _TamFonte = TamFonte;
            }


            Sort++;

            _LargColum = "";

            Alinhamento = _Alinhamento;

            // Contador de Colunas na Tabela 

            CountColumnTablixMembers = CountColumnTablixMembers + 1;

            HttpContext.Current.Session["TablixMemberCount"] = CountColumnTablixMembers;

            string NomeQuery = _NomeQuery;

            DataSet ds = new DataSet();
            ds = DsTable;

            string nomeHeader;

            if (NomeHeader != "")
            {
                nomeHeader = NomeHeader;
            }
            else
            {
                nomeHeader = "Null";
            }

            if (!LarguraAutoAjust)
            {
                _LargColum = Largura_NomeCampo_Larguracm;

                contTamColFinal = contTamColFinal + (Convert.ToDouble(_LargColum));

                _PaddingRight = _PaddingRight + "pt";
            }
            else
            {
                //   if (ds.Tables[NomeQuery].Rows.Count > 0)
                //   {

                _LargColum = ds.Tables[NomeQuery].Rows[0][NomeCampo].ToString();
                System.Drawing.Font stringFont = new System.Drawing.Font("Arial", Convert.ToInt32(_TamFonte));
                System.Drawing.Image image = new Bitmap(1, 1);
                Graphics graphics = Graphics.FromImage(image);
                SizeF size = new SizeF();
                SizeF size2 = new SizeF();
                SizeF size3 = new SizeF();


                if (ds.Tables[NomeQuery].Rows.Count == 0)
                {
                    size = graphics.MeasureString("1", stringFont);
                }

                else if (ds.Tables[NomeQuery].Rows.Count < 2)
                {

                    size = graphics.MeasureString(ds.Tables[NomeQuery].Rows[0][NomeCampo].ToString(), stringFont);

                }
                else
                {
                    for (int cont2 = 0; cont2 < ds.Tables[NomeQuery].Rows.Count - 1; cont2++)
                    {
                        size2 = graphics.MeasureString(ds.Tables[NomeQuery].Rows[cont2 + 1][NomeCampo].ToString(), stringFont);
                        size3 = graphics.MeasureString(ds.Tables[NomeQuery].Rows[cont2][NomeCampo].ToString(), stringFont);

                        if (ContRow == 0)
                        {
                            if (size2.Width > size3.Width)
                            {
                                size = size2;
                            }
                            else
                            {
                                size = size3;
                            }

                            ContRow = 1;
                        }
                        else
                        {
                            if (size2.Width > size.Width)
                            {
                                size = size2;
                            }
                        }
                    }
                }

                size2 = graphics.MeasureString(nomeHeader, stringFont);

                if (size2.Width > size.Width)
                {
                    size = size2;
                }

                int WidthColum = (int)Math.Round(size.Width);

                double centimeters2 = WidthColum * 2.54 / 96;

                centimeters2 = centimeters2 + 0.2;

                contTamColFinal = contTamColFinal + centimeters2;


                if (_LandScape == "Excel")
                {
                    centimeters2 = centimeters2 + 0.8;
                }

                container = Convert.ToString(centimeters2);

                _PaddingRight = _PaddingRight + "pt";
                _LargColum = container.Replace(",", ".");
            }


            if (NomeCampo.ToString().Trim() == "P#9")
            {
                string Padding;
                double centimeters3;

                PaddingUltiCol = 1;

                if (_LandScape == "Landscape")
                {
                    centimeters3 = (29 - contTamColFinal) + 0.7;
                }
                else
                {
                    centimeters3 = 21 - contTamColFinal;
                }
                _PaddingLeft = "0";
                _PaddingLeft = _PaddingLeft + "pt";

                _PaddingRight = "0";
                _PaddingRight = _PaddingRight + "pt";

                container = Convert.ToString((1 + centimeters3) - 0.4);

                _LargColum = container.Replace(",", ".");
            }

            XmlDocument xmlTeste = new XmlDocument();

            XmlNodeList node;

            XmlNode node2;



            if (_HeaderColor != true)
            {
                _headColor = "White";

            }

            if (_Alinhamento != "Default" && _Alinhamento != "")
            {
                Alinhamento = _Alinhamento;
            }
            else if (_Alinhamento == "Default")
            {
                Alinhamento = "Left";
            }
            else if (_Alinhamento == "Right")
            {
                Alinhamento = "Right";
            }
            else if (_Alinhamento == "")
            {
                Alinhamento = "Left";
            }
            else if (_Alinhamento == "Left")
            {
                Alinhamento = "Left";
            }
            else if (_Alinhamento == "Center")
            {
                Alinhamento = "Center";
            }
            string DataType;

            if (NomeCampo != "P#")
            {
                DataType = ds.Tables[NomeQuery].Columns[NomeCampo].DataType.ToString();
            }
            else
            {
                DataType = "System.String";
            }

            if (SetNodeBody == 0)
            {

                xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
                xmlTeste.Load(xmlModeloRel);
                xmlModeloRel.Dispose();
                //}


                node = xmlTeste.GetElementsByTagName("Language");

                node2 = node.Item(0);

                node2.InnerXml = _Language;

                xmlTeste.Save(xmlModeloRel = new MemoryStream());


                node = xmlTeste.GetElementsByTagName("Body");

                node2 = node.Item(0);

                string teste3 = node2.InnerXml;

                node2.InnerXml = @teste3 + @"  <ReportItems>  " +
                                           "  </ReportItems> ";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());

                SetNodeBody = 1;
            }

            //if (ds.Tables[NomeQuery].Columns[NomeCampo].ColumnName.ToString().Trim() == "P#")
            if (NomeCampo.ToString().Trim() == "P#")
            {
                if (_headColor == "White")
                {
                    _ColorLColumn = "<Color>Black</Color>";
                    _ColorRow = "<Color>Black</Color>";
                }
                else
                {
                    _ColorLColumn = "<Color>Silver</Color>";
                    //_ColorRow = "<Color>Silver</Color>";
                }
            }
            //}

            if (_NomeRel == "TotalizaVendas")
            {
                if (ds.Tables[_NomeQuery].Columns["N"] != null)
                {

                    //_RowBold = " <FontWeight>=IIf(Fields!N.Value = \"N\",\"Bold\",\"Default\")</FontWeight> ";

                    //if (ds.Tables[_NomeQuery].Columns[NomeCampo].ColumnName.ToString().Trim() == "N")
                    if (NomeCampo.ToString().Trim() == "N")
                    {
                        _LargColum = "0.07938";
                        _ColorLColumn = "<Color>Silver</Color>";
                        //_ColorRow = "<Color>White</Color>";
                    }
                    else
                    {
                        _ColumnHide = "";

                    }
                }
                else
                {
                    _RowBold = "<FontWeight>Normal</FontWeight>";
                }
            }
            if (ContTab == 0)
            {
                xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
                xmlTeste.Load(xmlModeloRel);
                xmlModeloRel.Dispose();
                //}


                if (TabCopy != true)
                {
                    node = xmlTeste.GetElementsByTagName("DataSets");

                    node2 = node.Item(0);

                    string testeDT = node2.InnerXml;

                    node2.InnerXml = @testeDT + "     <DataSet Name=\"" + NomeQuery + "\"> " +
                                                "          <Fields> " +
                                                "          </Fields> " +
                                                "          <Query> " +
                                                "            <DataSourceName>DataSource</DataSourceName> " +
                                                "            <CommandText>/* Local Query */</CommandText> " +
                                                "          </Query> " +
                                                "          <rd:DataSetInfo> " +
                                                "            <rd:DataSetName>DataSetTemp" + NewFields + "</rd:DataSetName> " +
                                                "            <rd:TableName>" + NomeQuery + "</rd:TableName> " +
                                                "            <rd:TableAdapterFillMethod /> " +
                                                "            <rd:TableAdapterGetDataMethod /> " +
                                                "            <rd:TableAdapterName /> " +
                                                "          </rd:DataSetInfo> " +
                                                "        </DataSet> ";

                }

                node = xmlTeste.GetElementsByTagName("ReportItems");

                node2 = node.Item(0);

                string testeDs2 = node2.InnerXml;

                string _type = "";
                string _textAlign = Alinhamento;
                string _FormatData = "";
                string _AlturaLinha = "";

                if (AlturaLinha == "Default")
                {
                    _AlturaLinha = "0.4233418";
                }
                else
                {
                    _AlturaLinha = AlturaLinha;
                }




                node2.InnerXml = @testeDs2 + "  <Rectangle Name=\"Rectangle" + Sort + "\"> " +
                                             "   <ReportItems>" +
                                             "       <Tablix Name=\"Tablix" + Sort + "\">" +
                                                    "   <TablixBody> " +
                                                    "     <TablixColumns> " +
                                                    "       <TablixColumn> " +
                                                    "         <Width>" + _LargColum + "cm</Width> " +
                                                    "       </TablixColumn> " +
                                                    "     </TablixColumns> " +
                                                    "     <TablixRows>" +
                                                    "       <TablixRow> " +
                                                    "         <Height>" + _AlturaLinha + "cm</Height> " +
                                                    "         <TablixCells> " +
                                                    "           <TablixCell> " +
                                                    "             <CellContents> " +
                                                    "               <Textbox Name=\"Textbox" + Sort + 8 + "\"> " +
                                                    " <CanGrow>true</CanGrow> " +
                                                    "                 <KeepTogether>true</KeepTogether> " +
                                                    "                 <Paragraphs> " +
                                                    "                   <Paragraph> " +
                                                    "                     <TextRuns> " +
                                                    "                       <TextRun> " +
                                                    "                         <Value>" + nomeHeader + "</Value> " +
                                                    "                           <Style> " +
                                                    "                             <FontWeight>Bold</FontWeight> " +
                                                    "                             <FontSize>" + _TamFonte + "pt</FontSize> " +
                                                    "                           </Style> " +
                                                    "                       </TextRun> " +
                                                    "                     </TextRuns> " +
                                                    "                     <Style> " +
                                                    "                       <TextAlign>" + _textAlign + "</TextAlign> " +
                                                    "                     </Style> " +
                                                    "                   </Paragraph> " +
                                                    "                 </Paragraphs> " +
                                                    "                 <rd:DefaultName>Textbox" + Sort + 2 + "</rd:DefaultName> " +
                                                    "               " + _ColumnHide +
                                                    "                 <Style> " +
                                                    "                   <Border> " +
                                                    "                     <Color>LightGrey</Color> " +
                                                    "                     <Style>None</Style> " +
                                                    "                   </Border> " +
                                                    "                   <BackgroundColor>" + _headColor + "</BackgroundColor> " +
                                                    "                 </Style> " +
                                                    "               </Textbox> " +
                                                    "             </CellContents> " +
                                                    "           </TablixCell> " +
                                                    "         </TablixCells> " +
                                                    "       </TablixRow> " +
                                                    "    </TablixRows> " +
                                                    "  </TablixBody> " +
                                                    "  <TablixColumnHierarchy> " +
                                                    "  </TablixColumnHierarchy> " +
                                                    "  <TablixRowHierarchy> " +
                                                    "  </TablixRowHierarchy> " +
                                                    "  <RepeatColumnHeaders>true</RepeatColumnHeaders> " +
                                                    "  <RepeatRowHeaders>true</RepeatRowHeaders>" +
                                                    "  <DataSetName>" + NomeQuery + "</DataSetName> " +
                                                    "  <Height>0.33333in</Height> " +
                                                    "  <Width>3.69365in</Width> " +
                                                    "  <Style> " +
                                                    "    <Border> " +
                                                    "      <Style>None</Style> " +
                                                    "    </Border> " +
                                                    "  </Style> " +
                                                    "</Tablix> " +
                                                    " </ReportItems> " +
                                                    "  <Top>" + _tPositY + "</Top> " +
                                                    "  <Left>" + _tPositX + "</Left> " +
                                                    "  <Height>" + (_NomeRel == "Invoice" ? "9.01cm" : "0.01 cm") + " </Height> " +
                                                    "  <Width>20.6cm</Width> " +
                                                    " <ZIndex>" + Index + "</ZIndex> " +
                                                    //"  <Style> " +
                                                    //  " <Border> " +
                                                    //  "  <Style>Dotted</Style> " +
                                                    // " </Border> " +
                                                    // " <BackgroundColor>LightBlue</BackgroundColor> " +
                                                    //"</Style> " +
                                                    " </Rectangle>";
                xmlTeste.Save(xmlModeloRel = new MemoryStream());

                SetNodeBody = 1;
                ContTab = 1;

            }
            else
            {
                ////##################################################################################################################
                xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
                xmlTeste.Load(xmlModeloRel);
                xmlModeloRel.Dispose();
                node = xmlTeste.GetElementsByTagName("TablixColumns");

                node2 = node.Item(NewTab - 1);

                string teste = node2.InnerXml;

                node2.InnerXml = @teste + @"  <TablixColumn> " +
                                          "     <Width>" + _LargColum + "cm</Width> " +
                                          "  </TablixColumn>";

                node = xmlTeste.GetElementsByTagName("TablixCells");

                var cont = (RowGroup3 ? 2 : 1);

                cont = (RowGroup1 ? 3 : cont);

                cont = (RowGroup4_Sub3 && RowGroup1 ? 4 : RowGroup4_Sub3 ? 3 : cont);

                cont = (RowGroup4_Sub1 && RowGroup4_Sub3 ? 5 : RowGroup4_Sub1 ? 4 : cont);

                node2 = node.Item(ContRowGroup - cont);
                string _type = "";
                string _textAlign = Alinhamento;
                string _FormatData = "";

                string teste2 = node2.InnerXml;

                node2.InnerXml = teste2 + @" <TablixCell> " +
                                             " <CellContents> " +
                                             "   <Textbox Name=\"Textbox" + Sort + 1 + "\"> " +
                                             "     <CanGrow>true</CanGrow> " +
                                             "     <KeepTogether>true</KeepTogether> " +
                                             "     <Paragraphs> " +
                                             "       <Paragraph> " +
                                             "         <TextRuns> " +
                                             "           <TextRun> " +
                                             "             <Value>" + nomeHeader + "</Value> " +
                                             "              <Style> " +
                                             "               <FontSize>" + _TamFonte + "pt</FontSize> " +
                                             "               <FontWeight>Bold</FontWeight> " +
                                             "             " + _ColorLColumn +
                                             "              </Style> " +
                                             "           </TextRun> " +
                                             "         </TextRuns> " +
                                             "         <Style> " +
                                             "          <TextAlign>" + _textAlign + "</TextAlign> " +
                                             "         </Style> " +
                                             "       </Paragraph> " +
                                             "     </Paragraphs> " +
                                             "     <rd:DefaultName>Textbox" + Sort + 1 + "</rd:DefaultName> " +
                                             " " + _ColumnHide +
                                             "     <Style> " +
                                             "       <Border> " +
                                             "         <Color>LightGrey</Color> " +
                                             "         <Style>None</Style> " +
                                             "       </Border> " +
                                             "      <PaddingRight>" + _PaddingRight + "</PaddingRight> " +
                                             "      <PaddingLeft>" + _PaddingLeft + "</PaddingLeft> " +
                                             "       <BackgroundColor>" + _headColor + "</BackgroundColor> " +
                                             "     </Style> " +
                                             "   </Textbox> " +
                                             " </CellContents> " +
                                           " </TablixCell>";

                xmlTeste.Save(xmlModeloRel = new MemoryStream());
            }
        }

        public void CriarObjCelulaInColuna(string Query_Fixo, string NomeCampo, string TipoGrupo, string GroupBy = "Null", string Total_TipoGrupo = "Null",
                                           int ColSpanCont = 0, bool Negrito = false, string Fonte = "0", string Decimais = "", bool borderBottom = false, string alturaLinha = "Default", bool avg = false)
        {

            XmlDocument xmlTeste = new XmlDocument();
            XmlNodeList node;
            XmlNode node2;

            DataSet ds = new DataSet();

            ds = DsTable;
            /*
            //if ((ds.Tables[0].Rows.Count == 0) || (ds.Tables[0].Rows.Count > 1))
            if (ds.Tables[0].Rows.Count == 0)
            {
                return;
            }*/

            // Se Retorno do DataTable estiver vazio finaliza o processo
            int Datateste = 0;
            bool nulo = false;

            Datateste = DsTable.Tables.Count;
            for (int i = 0; i < Datateste; i++)
            {
                if ((DsTable.Tables[i].Rows.Count == 0) && (_NomeRel != "Invoice"))
                    nulo = true;
            }

            if (nulo)
            {
                return;
            }

            Sort++;

            int RowGroup = 0;

            int numeroRowGroup = 0;

            string _PaddingRight = "4";
            string _PaddingLeft = "0";
            string _ColumnHide = "";

            string _ColorLColumn = "";

            string centimeters;
            string container;
            string _headColor = "Black";


            string borderBottom_ = "";
            if (borderBottom)
            {
                borderBottom_ = " <BottomBorder> <Color>LightGrey</Color> <Style>Solid</Style> <Width>1pt</Width> </BottomBorder> ";
            }

            string alturaLinha_ = "";
            if (alturaLinha != "Default")
            {
                alturaLinha_ = " <PaddingTop> " + alturaLinha + "pt </PaddingTop> " +
                               " <PaddingBottom> " + alturaLinha + "pt </PaddingBottom> ";
            }

            _PaddingRight = _PaddingRight + "pt";

            xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
            xmlTeste.Load(xmlModeloRel);
            xmlModeloRel.Dispose();

            XmlNode RowNode;

            node = xmlTeste.GetElementsByTagName("TablixRows");

            if (TipoGrupo == "ParentGroup")
            {
                if (RowGroup1 == false)
                {
                    RowNode = node.Item(NewTab - 1);

                    string strContainer = RowNode.InnerXml;

                    RowNode.InnerXml = strContainer + @"<TablixRow> <Height>0.16667in</Height> <TablixCells> </TablixCells> </TablixRow>";

                    //-----------------------

                    node = xmlTeste.GetElementsByTagName("TablixRowHierarchy");

                    RowNode = node.Item(NewTab - 1);
                    RowNode.InnerXml = @"			  <TablixMembers> " +
                                        "               <TablixMember> " +
                                        //"                 <FixedData>true</FixedData> " +
                                        "                  <KeepWithGroup>After</KeepWithGroup> " +
                                        //"                  <RepeatOnNewPage>true</RepeatOnNewPage> " +
                                        "               </TablixMember> " +
                                        "               <TablixMember> " +
                                        "                 <Group Name=\"Group1\"> " +
                                        "                   <GroupExpressions> " +
                                        "                     <GroupExpression>=Fields!" + GroupBy.ToString().Trim() + ".Value</GroupExpression> " +
                                        "                   </GroupExpressions> " +
                                        "                 </Group> " +
                                        "                 <TablixMembers> " +
                                        "                   <TablixMember> " +
                                        "                     <KeepWithGroup>After</KeepWithGroup> " +
                                        "                   </TablixMember> " +
                                        "                   <TablixMember> " +
                                        "                     <Group  Name=\"Details5" + Sort + "\" />" +
                                        "                   </TablixMember> " +
                                        "                 </TablixMembers> " +
                                        "               </TablixMember> " +
                                        "             </TablixMembers> ";


                    string DataType = ds.Tables[_NomeQuery].Columns[GroupBy].DataType.ToString();

                    node = xmlTeste.GetElementsByTagName("Fields");

                    node2 = node.Item(NewFields - 1);

                    string testeDs = node2.InnerXml;

                    node2.InnerXml = @testeDs + "    <Field Name=\"" + HeadersColumn(GroupBy.ToString().Trim()) + "\"> " +
                                                     " <DataField>" + HeadersColumn(GroupBy.ToString().Trim()) + "</DataField> " +
                                                     " <rd:TypeName>" + DataType + "</rd:TypeName> " +
                                                   " </Field> ";
                    RowGroup1 = true;
                    ContRowGroup++;
                    contLinhas++;
                    ListGroups.Add(TipoGrupo);
                }
            }
            else if (TipoGrupo == "ChildGroup")
            {
                if (RowGroup2 == false)
                {
                    RowNode = node.Item(NewTab - 1);

                    string strContainer = RowNode.InnerXml;

                    RowNode.InnerXml = strContainer + @"<TablixRow>  <Height>0.16667in</Height> <TablixCells> </TablixCells> </TablixRow>";

                    //-----------------------

                    node = xmlTeste.GetElementsByTagName("TablixRowHierarchy");

                    RowNode = node.Item(NewTab - 1);
                    RowNode.InnerXml = @"              <TablixMembers> " +
                                        "                <TablixMember> " +
                                        //"                  <FixedData>true</FixedData> " +
                                        "                  <KeepWithGroup>After</KeepWithGroup> " +
                                        //"                  <RepeatOnNewPage>true</RepeatOnNewPage> " +
                                        "                </TablixMember> " +
                                        "                <TablixMember> " +
                                        "                  <Group Name=\"Details5" + Sort + "\" />" +
                                        "                </TablixMember> " +
                                        "              </TablixMembers> ";

                    RowGroup2 = true;
                    ContRowGroup++;
                    contLinhas++;
                    ListGroups.Add(TipoGrupo);
                }
            }
            else if (TipoGrupo == "DetailGroup")
            {
                if (RowGroup3 == false)
                {
                    RowNode = node.Item(NewTab - 1);

                    string strContainer = RowNode.InnerXml;

                    RowNode.InnerXml = strContainer + @"<TablixRow> <Height>0.16667in</Height> <TablixCells> </TablixCells> </TablixRow>";

                    //-----------------------

                    if (!RowGroup1)
                    {
                        node = xmlTeste.GetElementsByTagName("TablixRowHierarchy");

                        RowNode = node.Item(NewTab - 1);

                        //strContainer = RowNode.InnerXml;

                        RowNode.InnerXml = @"              <TablixMembers> " +
                                            "                <TablixMember> " +
                                            //"                  <FixedData>true</FixedData> " +
                                            "                  <KeepWithGroup>After</KeepWithGroup> " +
                                            //"                  <RepeatOnNewPage>true</RepeatOnNewPage> " +
                                            "                </TablixMember> " +
                                            "                <TablixMember> " +
                                            "                  <Group Name=\"Details5" + Sort + "\" />" +
                                            "                </TablixMember> " +
                                            "              </TablixMembers> ";
                    }

                    RowGroup3 = true;

                    ContRowGroup++;
                    contLinhas++;

                    ListGroups.Add(TipoGrupo);
                }
            }
            else if (TipoGrupo == "TotalGroup")
            {

                if (Total_TipoGrupo == "ParentGroup")
                {

                    if (RowGroup4_Sub1 == false)
                    {
                        RowNode = node.Item(NewTab - 1);

                        string strContainer = RowNode.InnerXml;

                        RowNode.InnerXml = strContainer + @"<TablixRow> <Height>0.16667in</Height> <TablixCells> </TablixCells> </TablixRow>";


                        //-----------------------

                        node = xmlTeste.GetElementsByTagName("TablixRowHierarchy");

                        RowNode = node.Item(NewTab - 1);

                        //strContainer = RowNode.InnerXml;

                        RowNode.InnerXml = @"              <TablixMembers> " +
                                            "                <TablixMember> " +
                                            //"                  <FixedData>true</FixedData> " +
                                            "                  <KeepWithGroup>After</KeepWithGroup> " +
                                            //"                  <RepeatOnNewPage>true</RepeatOnNewPage> " +
                                            "                </TablixMember> " +
                                            "                <TablixMember> " +
                                            "                  <Group Name=\"Group1\"> " +
                                            "                    <GroupExpressions> " +
                                            "                      <GroupExpression>=Fields!" + GroupBy.ToString().Trim() + ".Value</GroupExpression> " +
                                            "                    </GroupExpressions> " +
                                            "                  </Group> " +
                                            "                  <TablixMembers> " +
                                            "                    <TablixMember> " +
                                            "                      <KeepWithGroup>After</KeepWithGroup> " +
                                            "                    </TablixMember> " +
                                            "                    <TablixMember> " +
                                            "                      <Group Name=\"Details5" + Sort + "\" />" +
                                            "                    </TablixMember> " +
                                            "                    <TablixMember> " +
                                            "                      <KeepWithGroup>Before</KeepWithGroup> " +
                                            "                    </TablixMember> " +
                                            "                  </TablixMembers> " +
                                            "                </TablixMember> " +
                                            "                <TablixMember> " +
                                            "                  <KeepWithGroup>Before</KeepWithGroup> " +
                                            "                </TablixMember> " +
                                            "              </TablixMembers> ";

                        RowGroup4_Sub1 = true;

                        ContRowGroup++;
                        contLinhas++;

                        ListGroups.Add("TotalGroup_Parent");
                    }
                }
                else if (Total_TipoGrupo == "DetailGroup")
                {

                    if (RowGroup4_Sub3 == false)
                    {
                        RowNode = node.Item(NewTab - 1);

                        string strContainer = RowNode.InnerXml;

                        RowNode.InnerXml = strContainer + @"<TablixRow> <Height>0.16667in</Height> <TablixCells> </TablixCells> </TablixRow>";


                        //-----------------------

                        if (!RowGroup1)
                        {
                            node = xmlTeste.GetElementsByTagName("TablixRowHierarchy");

                            RowNode = node.Item(NewTab - 1);
                            RowNode.InnerXml = @"              <TablixMembers> " +
                                                "                <TablixMember> " +
                                                //"                  <FixedData>true</FixedData> " +
                                                "                  <KeepWithGroup>After</KeepWithGroup> " +
                                                //"                  <RepeatOnNewPage>true</RepeatOnNewPage> " +
                                                "                </TablixMember> " +
                                                "                    <TablixMember> " +
                                                "                      <Group Name=\"Details5" + Sort + "\" />" +
                                                "                    </TablixMember> " +
                                                "                    <TablixMember> " +
                                                "                      <KeepWithGroup>Before</KeepWithGroup> " +
                                                "                    </TablixMember> " +
                                                "              </TablixMembers> ";
                            //}
                        }
                        else
                        {
                            node = xmlTeste.GetElementsByTagName("TablixRowHierarchy");

                            RowNode = node.Item(NewTab - 1);

                            //strContainer = RowNode.InnerXml;

                            RowNode.InnerXml = @"              <TablixMembers> " +
                                                "                <TablixMember> " +
                                                //"                  <FixedData>true</FixedData> " +
                                                "                  <KeepWithGroup>After</KeepWithGroup> " +
                                                //"                  <RepeatOnNewPage>true</RepeatOnNewPage> " +
                                                "                </TablixMember> " +
                                                "                <TablixMember> " +
                                                "                  <Group Name=\"Group1\"> " +
                                                "                    <GroupExpressions> " +
                                                "                     <GroupExpression>=Fields!" + GroupBy.ToString().Trim() + ".Value</GroupExpression> " +
                                                "                    </GroupExpressions> " +
                                                "                  </Group> " +
                                                "                  <TablixMembers> " +
                                                "                    <TablixMember> " +
                                                "                      <KeepWithGroup>After</KeepWithGroup> " +
                                                "                    </TablixMember> " +
                                                "                    <TablixMember> " +
                                                "                      <Group Name=\"Details5" + Sort + "\" />" +
                                                "                    </TablixMember> " +
                                                "                    <TablixMember> " +
                                                "                      <KeepWithGroup>Before</KeepWithGroup> " +
                                                "                    </TablixMember> " +
                                                "                  </TablixMembers> " +
                                                "                </TablixMember> " +
                                                "              </TablixMembers> ";
                        }

                        RowGroup4_Sub3 = true;

                        ContRowGroup++;
                        contLinhas++;

                        ListGroups.Add("TotalGroup_Detail");
                    }
                }
            }

            if (TipoGrupo == "ParentGroup")
            {
                node = xmlTeste.GetElementsByTagName("TablixCells");

                var cont = (RowGroup3 ? 2 : 1);

                cont = (RowGroup4_Sub3 ? 3 : cont);

                cont = (RowGroup4_Sub1 ? 4 : cont);

                node2 = node.Item(ContRowGroup - cont);
            }
            else if (TipoGrupo == "ChildGroup")
            {
                node = xmlTeste.GetElementsByTagName("TablixCells");
                node2 = node.Item(ContRowGroup - 1);
            }
            else if (TipoGrupo == "DetailGroup")
            {
                node = xmlTeste.GetElementsByTagName("TablixCells");

                var cont = 1;

                cont = (RowGroup4_Sub3 ? 2 : cont);

                cont = (RowGroup4_Sub1 ? 3 : cont);

                node2 = node.Item(ContRowGroup - cont);
            }
            else if (TipoGrupo == "TotalGroup")
            {
                node = xmlTeste.GetElementsByTagName("TablixCells");

                if (Total_TipoGrupo == "DetailGroup")
                {
                    var cont = 1;

                    cont = (RowGroup4_Sub1 ? 2 : cont);

                    node2 = node.Item(ContRowGroup - cont);
                }
                else // (Total_TipoGrupo == "ParentGroup")
                {
                    var cont = 1;

                    //cont = (RowGroup4_Sub1 ? 2 : cont);

                    node2 = node.Item(ContRowGroup - cont);
                }
            }
            else
            {
                node2 = null;
            }

            string _type = "";
            string _textAlign = Alinhamento;
            string _FormatData = "";
            string _Value = "";

            if (Query_Fixo == "Query")
            {
                if (NomeCampo != "")
                {
                    if (TipoGrupo == "TotalGroup" && avg == false)
                    {
                        _Value = "<Value>=Sum(Fields!" + NomeCampo.ToString().Trim() + ".Value)</Value>";
                    }
                    else if (TipoGrupo == "TotalGroup" && avg == true)
                    {
                        _Value = "<Value>=Avg(Fields!" + NomeCampo.ToString().Trim() + ".Value)</Value>";
                    }
                    else
                    {
                        _Value = "<Value>=Fields!" + NomeCampo.ToString().Trim() + ".Value</Value> ";
                    }
                }
                else
                {
                    _Value = "<Value />";
                }
            }
            else
            {
                if (NomeCampo != "")
                {
                    _Value = "<Value>" + NomeCampo + "</Value> ";
                }
                else
                {
                    _Value = "<Value />";
                }
            }

            string teste2 = node2.InnerXml;

            string ColSpan = "";

            if (Fonte != "0")
            {
                _TamFonte = Fonte;
            }
            else
            {
                _TamFonte = "8";
            }
            if (Negrito)
            {
                _RowBold = "<FontWeight>Bold</FontWeight>";
            }
            else
            {
                _RowBold = "<FontWeight>Normal</FontWeight>";
            }


            if (_NomeRel == "TotalizaVendas")
            {
                if (ds.Tables[_NomeQuery].Columns["N"] != null)
                {

                    _RowBold = " <FontWeight>=IIf(Fields!N.Value = \"N\",\"Bold\",\"Default\")</FontWeight> ";

                    //if (ds.Tables[_NomeQuery].Columns[NomeCampo].ColumnName.ToString().Trim() == "N")
                    if (NomeCampo.ToString().Trim() == "N")
                    {
                        centimeters = "0.07938";
                        _ColorLColumn = "<Color>Silver</Color>";
                        _ColorRow = "<Color>White</Color>";
                    }
                    else
                    {
                        _ColumnHide = "";
                        _ColorLColumn = "";


                    }
                }
                else
                {
                    _RowBold = "<FontWeight>Normal</FontWeight>";
                }
            }


            if (ColSpanCont != 0)
            {
                ColSpan = "<ColSpan>" + ColSpanCont + "</ColSpan>";
            }

            if (NomeCampo == "Merge")
            {
                node2.InnerXml = teste2 + @" <TablixCell /> ";
            }
            else
            {
                node2.InnerXml = teste2 + @" <TablixCell> " +
                                             " <CellContents> " +
                                                 "   <Textbox Name=\"Textbox" + Sort + 2 + "\"> " +
                                                 "     <CanGrow>true</CanGrow> " +
                                                 "     <KeepTogether>true</KeepTogether> " +
                                                 "     <Paragraphs> " +
                                                 "       <Paragraph> " +
                                                 "         <TextRuns> " +
                                                 "           <TextRun> " +
                                                 "            " + _Value +
                                                 "             <Style> " +
                                                 "               <FontStyle>Normal</FontStyle> " +
                                                 "               <FontSize>" + _TamFonte + "pt</FontSize> " +
                                                 "               " + _RowBold +
                                                 "               " + _ColorRow +
                                                 "               " + _type +
                                                 "               " + _FormatData +
                                                   "      " + Decimais +
                                                 "               <TextDecoration>None</TextDecoration> " +
                                                 "             </Style> " +
                                                 "           </TextRun> " +
                                                 "         </TextRuns> " +
                                                 "         <Style> " +
                                                 "          <TextAlign>" + _textAlign + "</TextAlign> " +
                                                 "         </Style> " +
                                                 "       </Paragraph> " +
                                                         _AddCell +
                                                 "     </Paragraphs> " +
                                                 "     <rd:DefaultName>Textbox" + Sort + 2 + "</rd:DefaultName> " +
                                                 " " + _ColumnHide +
                                                 "     <Style> " +
                                                 "       <Border> " +
                                                 "         <Color>LightGrey</Color> " +
                                                 "         <Style>None</Style> " +
                                                 "       </Border> " +
                                                 " " + borderBottom_ +
                                                 "      <PaddingRight>" + _PaddingRight + "</PaddingRight> " +
                                                 "      <PaddingLeft>" + _PaddingLeft + "</PaddingLeft> " +
                                                 " " + alturaLinha_ +
                                                 "     </Style> " +
                                                 "   </Textbox> " +
                                                 " " + ColSpan +
                                                 " </CellContents> " +
                                               " </TablixCell>";

            }

            if (NomeCampo != "" && Query_Fixo == "Query" && TipoGrupo != "TotalGroup" && TabCopy != true)
            {
                string DataType = ds.Tables[_NomeQuery].Columns[NomeCampo].DataType.ToString();

                node = xmlTeste.GetElementsByTagName("Fields");

                node2 = node.Item(NewFields - 1);

                string testeDs = node2.InnerXml;
                string NomeCampoReplace = NomeCampo.ToString().Replace(" ", "_").Trim();

                node2.InnerXml = @testeDs + "    <Field Name=\"" + HeadersColumn(NomeCampo.ToString().Trim()) + "\"> " +
                                                 " <DataField>" + HeadersColumn(NomeCampo.ToString().Trim()) + "</DataField> " +
                                                 " <rd:TypeName>" + DataType + "</rd:TypeName> " +
                                               " </Field> ";
            }
            xmlTeste.Save(xmlModeloRel = new MemoryStream());
        }
        private string CompareXX_YY(string Posicao, int XY)// x-1 ou y - 2
        {
            decimal Posicao1, Posicao2, PosicaoRetorno;
            string retornoStr;
            PosicaoRetorno = Convert.ToDecimal("0.00");
            string posX = _tPositX.Replace("cm", "");
            string posY = _tPositY.Replace("cm", "");

            Posicao1 = Convert.ToDecimal(Posicao);

            if (XY == 1)
            {
                Posicao2 = Convert.ToDecimal(posX);

                if (Posicao1 <= Posicao2)
                {
                    PosicaoRetorno = Posicao1 + Convert.ToDecimal("2.00");

                }
                else
                    PosicaoRetorno = Posicao2;
            }
            else if (XY == 2)
            {
                Posicao2 = Convert.ToDecimal(posY);

                if (Posicao1 <= Posicao2)
                {
                    PosicaoRetorno = Posicao1;
                }
                else
                {
                    PosicaoRetorno = Posicao1;
                }
            }
            retornoStr = Convert.ToString(PosicaoRetorno);

            return retornoStr;
        }

        private int Assek(string[,] aArray, string strToSeek)
        {
            int nFinal = (((aArray.Length) / 2) - 1);
            int retVal = -1;

            for (int i = 0; i <= nFinal; i++)
            {
                if (aArray[i, 0].CompareTo(strToSeek) == 0)
                {
                    retVal = i;
                    break;
                }
            }
            return retVal;
        }

        private string HeadersColumn(string Nome)
        {

            if ((Nome.ToString().Substring((Nome.Length - 1), 1)) == " ")
            {
                Nome = Nome.ToString().Replace(" ", "").Trim();
            }

            if ((Nome.ToString().Substring(0, 1)) == " ")
            {
                Nome = Nome.ToString().Replace(" ", "").Trim();
            }

            if (Nome.Contains(" "))
            {
                Nome = Nome.ToString().Replace(" ", "_").Trim();
            }

            if (Nome.Contains("-"))
            {
                Nome = Nome.ToString().Replace("-", "_").Trim();
            }

            if (Nome.Contains("/"))
            {
                Nome = Nome.ToString().Replace("/", "_").Trim();
            }

            if ((Nome.Contains("(")) || (Nome.Contains(")")))
            {
                Nome = Nome.ToString().Replace("(", "").Trim();

                Nome = Nome.ToString().Replace(")", "").Trim();
            }
            return Nome;

        }


        //Método criado para inserir numeração da página. MSO 27/10/2017
        public void CriarNumeroPagina(string posX, string posY, string TamFonte, string Cor, string Negrito, bool Sublinhado, string PosAlinhamento = "Right")
        {
            string _Sublinhado = "None";
            XmlDocument xmlTeste = new XmlDocument();
            xmlModeloRel.Seek(0, System.IO.SeekOrigin.Begin);
            xmlTeste.Load(xmlModeloRel);
            xmlModeloRel.Dispose();
            XmlNodeList node;

            if (PosAlinhamento != "Left" && PosAlinhamento != "Right")
            {
                PosAlinhamento = "Right";
            }

            _tPositX = Convert_Mm_Cm(posX);
            _tPositY = Convert_Mm_Cm(CompareXX_YY(posY, 2));

            if (TamFonte == "")
            {
                _TamFonte = "8";
            }
            else
            {
                _TamFonte = TamFonte;
            }


            if (Cor.Trim() == "")
            {
                Cor = "Black";
            }


            if (Negrito.Trim() == "")
            {
                Negrito = "Normal";
            }
            else if (Negrito == "B")
            {
                Negrito = "Bold";
            }
            else if (Negrito == "L")
            {
                Negrito = "Light";
            }


            if (Sublinhado == true)
            {
                _Sublinhado = "Underline";
            }



            node = xmlTeste.GetElementsByTagName("ReportItems");
            XmlNode node2;

            node2 = node.Item(0);

            string testeDs = node2.InnerXml;


            node2.InnerXml = testeDs + " <Textbox Name='Paginas'>" +
                                        " <CanGrow>true</CanGrow>" +
                                        " <KeepTogether>true</KeepTogether>" +
                                        " <Paragraphs>" +
                                        "   <Paragraph>" +
                                        "       <TextRuns>" +
                                        "           <TextRun>" +
                                        "               <Value>=Globals!PageNumber </Value>" +
                                        "               <Style>" +
                                        "                   <FontSize>" + _TamFonte + "pt </FontSize>" +
                                        "                   <Color>" + Cor + "</Color>" +
                                        "                   <FontWeight>" + Negrito + "</FontWeight>" +
                                        "                   <TextDecoration>" + _Sublinhado + "</TextDecoration>" +
                                        "                   <TextAlign>" + PosAlinhamento + "</TextAlign>" +
                                        "               </Style >" +
                                        "           </TextRun>" +
                                        "           <TextRun>" +
                                        "               <Value>/</Value>" +
                                        "               <Style>" +
                                        "                   <FontSize>" + _TamFonte + "pt </FontSize>" +
                                        "                   <Color>" + Cor + "</Color>" +
                                        "                   <FontWeight>" + Negrito + "</FontWeight>" +
                                        "                   <TextDecoration>" + _Sublinhado + "</TextDecoration>" +
                                        "                   <TextAlign>" + PosAlinhamento + "</TextAlign>" +
                                        "               </Style >" +
                                        "           </TextRun>" +
                                        "           <TextRun>" +
                                        "               <Value>=Globals!TotalPages </Value>" +
                                        "               <Style>" +
                                        "                   <FontSize>" + _TamFonte + "pt </FontSize>" +
                                        "                   <Color>" + Cor + "</Color>" +
                                        "                   <FontWeight>" + Negrito + "</FontWeight>" +
                                        "                   <TextDecoration>" + _Sublinhado + "</TextDecoration>" +
                                        "                   <TextAlign>" + PosAlinhamento + "</TextAlign>" +
                                        "               </Style >" +
                                        "           </TextRun>" +
                                        "       </TextRuns>" +
                                        "       <Style>" +
                                        "           <TextAlign>" + PosAlinhamento + "</TextAlign>" +
                                        "       </Style>" +
                                        "   </Paragraph>" +
                                        " </Paragraphs>" +
                                        " <Left>" + _tPositX + "</Left>" +
                                        " <Top>" + _tPositY + "</Top>" +
                                        " <Height>1cm</Height>" +
                                        " <Width>2cm</Width>" +
                                        " <Style>" +
                                        "   <Border>" +
                                        "       <Style>None</Style>" +
                                        "   </Border>" +
                                        "   <PaddingLeft>0pt</PaddingLeft>" +
                                        "   <PaddingRight>0pt</PaddingRight>" +
                                        "   <PaddingTop>0pt</PaddingTop>" +
                                        "   <PaddingBottom>0pt</PaddingBottom>" +
                                        " </Style>" +
                                        " </Textbox>";

            xmlTeste.Save(xmlModeloRel = new MemoryStream());
        }
    }
}