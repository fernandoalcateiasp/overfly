/********************************************************************
toolbarimages.js

Library javascript de funcoes para as imagens dos botoes das barras
********************************************************************/

// As constantes abaixo sao de uso exclusivo desta library

// CONSTANTES *******************************************************

// Diretorio de imagens
var imgPath = SYS_PAGESURLROOT + '/images/btnstoolbar1/';

// Variaveis de imagens =============================================
var overImg = new Array();
overImg[0] = new String();
overImg[1] = new String();
overImg[2] = new String();
overImg[3] = new String();
overImg[4] = new String();
overImg[5] = new String();
overImg[6] = new String();
overImg[7] = new String();
overImg[8] = new String();
overImg[9] = new String();
overImg[10] = new String();
overImg[11] = new String();
overImg[12] = new String();
overImg[13] = new String();
overImg[14] = new String();
overImg[15] = new String();
overImg[16] = new String();
overImg[17] = new String();
overImg[18] = new String();
// ovr1024
overImg[19] = new Image();
overImg[20] = new Image();
overImg[21] = new Image();
overImg[22] = new Image();
overImg[23] = new Image();
overImg[24] = new Image();
overImg[25] = new Image();
overImg[26] = new Image();

var overImg_I = new Array(26);
overImg_I[0] = new String();
overImg_I[1] = new String();

var downImg = new Array();
downImg[0] = new String();
downImg[1] = new String();
downImg[2] = new String();
downImg[3] = new String();
downImg[4] = new String();
downImg[5] = new String();
downImg[6] = new String();
downImg[7] = new String();
downImg[8] = new String();
downImg[9] = new String();
downImg[10] = new String();
downImg[11] = new String();
downImg[12] = new String();
downImg[13] = new String();
downImg[14] = new String();
downImg[15] = new String();
downImg[16] = new String();
downImg[17] = new String();
downImg[18] = new String();
// ovr1024
downImg[19] = new Image();
downImg[20] = new Image();
downImg[21] = new Image();
downImg[22] = new Image();
downImg[23] = new Image();
downImg[24] = new Image();
downImg[25] = new Image();
downImg[26] = new Image();

var downImg_I = new Array(26);
downImg_I[0] = new String();
downImg_I[1] = new String();

var defaultImg = new Array();
defaultImg[0] = new String();
defaultImg[1] = new String();
defaultImg[2] = new String();
defaultImg[3] = new String();
defaultImg[4] = new String();
defaultImg[5] = new String();
defaultImg[6] = new String();
defaultImg[7] = new String();
defaultImg[8] = new String();
defaultImg[9] = new String();
defaultImg[10] = new String();
defaultImg[11] = new String();
defaultImg[12] = new String();
defaultImg[13] = new String();
defaultImg[14] = new String();
defaultImg[15] = new String();
defaultImg[16] = new String();
defaultImg[17] = new String();
defaultImg[18] = new String();
// ovr1024
defaultImg[19] = new Image();
defaultImg[20] = new Image();
defaultImg[21] = new Image();
defaultImg[22] = new Image();
defaultImg[23] = new Image();
defaultImg[24] = new Image();
defaultImg[25] = new Image();
defaultImg[26] = new Image();

var defaultImg_I = new Array(26);
defaultImg_I[0] = new String();
defaultImg_I[1] = new String();

var colorImg = new Array();
colorImg[0] = new String();
colorImg[1] = new String();
colorImg[2] = new String();
colorImg[3] = new String();
colorImg[4] = new String();
colorImg[5] = new String();
colorImg[6] = new String();
colorImg[7] = new String();
colorImg[8] = new String();
colorImg[9] = new String();
colorImg[10] = new String();
colorImg[11] = new String();
colorImg[12] = new String();
colorImg[13] = new String();
colorImg[14] = new String();
colorImg[15] = new String();
colorImg[16] = new String();
colorImg[17] = new String();
colorImg[18] = new String();
// ovr1024
colorImg[19] = new Image();
colorImg[20] = new Image();
colorImg[21] = new Image();
colorImg[22] = new Image();
colorImg[23] = new Image();
colorImg[24] = new Image();
colorImg[25] = new Image();
colorImg[26] = new Image();

var colorImg_I = new Array(26);
colorImg_I[0] = new String();
colorImg_I[1] = new String();

// Variaveis de imagens de botao de impressao =======================
var overImgP = new String();
var downImgP = new String();
var defaultImgP = new String();
var colorImgP = new String();

// Final de variáveis de imagens ====================================

// Preload imagens no array de imagens ==============================

// Imagens particulares da barra superior

defaultImg[0] = imgPath + 'btn_list4.gif';
defaultImg[1] = imgPath + 'btn_reg4.gif';

overImg[0] = imgPath + 'btn_list2.gif';
overImg[1] = imgPath + 'btn_reg2.gif';

downImg[0] = imgPath + 'btn_list3.gif';
downImg[1] = imgPath + 'btn_reg3.gif';

colorImg[0] = imgPath + 'btn_list1.gif';
colorImg[1] = imgPath + 'btn_reg1.gif';

// Imagens particulares da barra inferior

defaultImg_I[0] = imgPath + 'btn_backpg4.gif';
defaultImg_I[1] = imgPath + 'btn_aheadpg4.gif';

overImg_I[0] = imgPath + 'btn_backpg2.gif';
overImg_I[1] = imgPath + 'btn_aheadpg2.gif';

downImg_I[0] = imgPath + 'btn_backpg3.gif';
downImg_I[1] = imgPath + 'btn_aheadpg3.gif';

colorImg_I[0] = imgPath + 'btn_backpg1.gif';
colorImg_I[1] = imgPath + 'btn_aheadpg1.gif';

// botoes especificos da barra inferior

colorImg_I[11] = imgPath + 'btn_1_1.gif';
overImg_I[11] = imgPath + 'btn_1_2.gif';
downImg_I[11] = imgPath + 'btn_1_3.gif';
defaultImg_I[11] = imgPath + 'btn_1_4.gif';

colorImg_I[12] = imgPath + 'btn_2_1.gif';
overImg_I[12] = imgPath + 'btn_2_2.gif';
downImg_I[12] = imgPath + 'btn_2_3.gif';
defaultImg_I[12] = imgPath + 'btn_2_4.gif';

colorImg_I[13] = imgPath + 'btn_3_1.gif';
overImg_I[13] = imgPath + 'btn_3_2.gif';
downImg_I[13] = imgPath + 'btn_3_3.gif';
defaultImg_I[13] = imgPath + 'btn_3_4.gif';
// Imagens comuns as duas barras

defaultImg[2] = imgPath + 'btn_inc4.gif';
defaultImg[3] = imgPath + 'btn_alt4.gif';
defaultImg[4] = imgPath + 'btn_status4.gif';
defaultImg[5] = imgPath + 'btn_del4.gif';
defaultImg[6] = imgPath + 'btn_save4.gif';
defaultImg[7] = imgPath + 'btn_cancel4.gif';
defaultImg[8] = imgPath + 'btn_refresh4.gif';
defaultImg[9] = imgPath + 'btn_back4.gif';
defaultImg[10] = imgPath + 'btn_ahead4.gif';
defaultImg[11] = imgPath + 'btn_docs4.gif';
defaultImg[12] = imgPath + 'btn_print_4.gif';
defaultImg[13] = imgPath + 'btn_Procedimentos4.gif';
defaultImg[14] = imgPath + 'btn_4_4.gif';
defaultImg[15] = imgPath + 'btn_5_4.gif';
defaultImg[16] = imgPath + 'btn_6_4.gif';
defaultImg[17] = imgPath + 'btn_7_4.gif';
defaultImg[18] = imgPath + 'btn_8_4.gif';
// ovr1024
defaultImg[19] = imgPath + 'btn_9_4.gif';
defaultImg[20] = imgPath + 'btn_10_4.gif';
defaultImg[21] = imgPath + 'btn_11_4.gif';
defaultImg[22] = imgPath + 'btn_12_4.gif';
defaultImg[23] = imgPath + 'btn_13_4.gif';
defaultImg[24] = imgPath + 'btn_14_4.gif';
defaultImg[25] = imgPath + 'btn_15_4.gif';
defaultImg[26] = imgPath + 'btn_16_4.gif';

overImg[2] = imgPath + 'btn_inc2.gif';
overImg[3] = imgPath + 'btn_alt2.gif';
overImg[4] = imgPath + 'btn_status2.gif';
overImg[5] = imgPath + 'btn_del2.gif';
overImg[6] = imgPath + 'btn_save2.gif';
overImg[7] = imgPath + 'btn_cancel2.gif';
overImg[8] = imgPath + 'btn_refresh2.gif';
overImg[9] = imgPath + 'btn_back2.gif';
overImg[10] = imgPath + 'btn_ahead2.gif';
overImg[11] = imgPath + 'btn_docs2.gif';
overImg[12] = imgPath + 'btn_print_2.gif';
overImg[13] = imgPath + 'btn_Procedimentos2.gif';
overImg[14] = imgPath + 'btn_4_2.gif';
overImg[15] = imgPath + 'btn_5_2.gif';
overImg[16] = imgPath + 'btn_6_2.gif';
overImg[17] = imgPath + 'btn_7_2.gif';
overImg[18] = imgPath + 'btn_8_2.gif';
// ovr1024
overImg[19] = imgPath + 'btn_9_2.gif';
overImg[20] = imgPath + 'btn_10_2.gif';
overImg[21] = imgPath + 'btn_11_2.gif';
overImg[22] = imgPath + 'btn_12_2.gif';
overImg[23] = imgPath + 'btn_13_2.gif';
overImg[24] = imgPath + 'btn_14_2.gif';
overImg[25] = imgPath + 'btn_15_2.gif';
overImg[26] = imgPath + 'btn_16_2.gif';

downImg[2] = imgPath + 'btn_inc3.gif';
downImg[3] = imgPath + 'btn_alt3.gif';
downImg[4] = imgPath + 'btn_status3.gif';
downImg[5] = imgPath + 'btn_del3.gif';
downImg[6] = imgPath + 'btn_save3.gif';
downImg[7] = imgPath + 'btn_cancel3.gif';
downImg[8] = imgPath + 'btn_refresh3.gif';
downImg[9] = imgPath + 'btn_back3.gif';
downImg[10] = imgPath + 'btn_ahead3.gif';
downImg[11] = imgPath + 'btn_docs3.gif';
downImg[12] = imgPath + 'btn_print_3.gif';
downImg[13] = imgPath + 'btn_Procedimentos3.gif';
downImg[14] = imgPath + 'btn_4_3.gif';
downImg[15] = imgPath + 'btn_5_3.gif';
downImg[16] = imgPath + 'btn_6_3.gif';
downImg[17] = imgPath + 'btn_7_3.gif';
downImg[18] = imgPath + 'btn_8_3.gif';
// ovr1024
downImg[19] = imgPath + 'btn_9_3.gif';
downImg[20] = imgPath + 'btn_10_3.gif';
downImg[21] = imgPath + 'btn_11_3.gif';
downImg[22] = imgPath + 'btn_12_3.gif';
downImg[23] = imgPath + 'btn_13_3.gif';
downImg[24] = imgPath + 'btn_14_3.gif';
downImg[25] = imgPath + 'btn_15_3.gif';
downImg[26] = imgPath + 'btn_16_3.gif';

colorImg[2] = imgPath + 'btn_inc1.gif';
colorImg[3] = imgPath + 'btn_alt1.gif';
colorImg[4] = imgPath + 'btn_status1.gif';
colorImg[5] = imgPath + 'btn_del1.gif';
colorImg[6] = imgPath + 'btn_save1.gif';
colorImg[7] = imgPath + 'btn_cancel1.gif';
colorImg[8] = imgPath + 'btn_refresh1.gif';
colorImg[9] = imgPath + 'btn_back1.gif';
colorImg[10] = imgPath + 'btn_ahead1.gif';
colorImg[11] = imgPath + 'btn_docs1.gif';
colorImg[12] = imgPath + 'btn_print_1.gif';
colorImg[13] = imgPath + 'btn_Procedimentos1.gif';
colorImg[14] = imgPath + 'btn_4_1.gif';
colorImg[15] = imgPath + 'btn_5_1.gif';
colorImg[16] = imgPath + 'btn_6_1.gif';
colorImg[17] = imgPath + 'btn_7_1.gif';
colorImg[18] = imgPath + 'btn_8_1.gif';
// ovr1024
colorImg[19] = imgPath + 'btn_9_1.gif';
colorImg[20] = imgPath + 'btn_10_1.gif';
colorImg[21] = imgPath + 'btn_11_1.gif';
colorImg[22] = imgPath + 'btn_12_1.gif';
colorImg[23] = imgPath + 'btn_13_1.gif';
colorImg[24] = imgPath + 'btn_14_1.gif';
colorImg[25] = imgPath + 'btn_15_1.gif';
colorImg[26] = imgPath + 'btn_16_1.gif';

// Imagens de botao de impressao
overImgP = imgPath + 'btn_print_2.gif';
downImgP = imgPath + 'btn_print_3.gif';
defaultImgP = imgPath + 'btn_print_4.gif';
colorImgP = imgPath + 'btn_print_1.gif';
