/********************************************************************
tiposrelsup01.js

Library javascript para o tiposrelsup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_CounterCmbsDynamics = 0;
var glb_BtnFromFramWork = null;

// Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
// Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
// Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
var dsoCmbDynamic03 = new CDatatransport("dsoCmbDynamic03");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modmateriais/submodestoques/localizacoes/inferior.asp',
                              SYS_PAGESURLROOT + '/modmateriais/submodestoques/localizacoes/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'LocalizacaoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblLocalizacao','txtLocalizacao',20,1],
						  ['lblCodigo','txtCodigo',3,1],
						  ['lblLocalizacaoCodificada','txtLocalizacaoCodificada',9,1,-10],
                          ['lblLocalizacaoMaeID','selLocalizacaoMaeID',11,1,-10],
						  ['lblNivel','txtNivel',3,1,-5],
						  ['lblTamanhoMinimo','selTamanhoMinimo',5,1,-5],
						  ['lblTamanhoMaximo','selTamanhoMaximo',5,1,-5],
						  ['lblEstoqueSeguro','chkEstoqueSeguro',3,1],
						  ['lblLocalizacaoUtil','chkLocalizacaoUtil',3,1],
						  ['lblPalete','chkPalete',3,1],
						  ['lblReposicaoDiaria','chkReposicaoDiaria',3,1,-3],
						  ['lblProdutosDistintos','txtProdutosDistintos',4,2],
						  ['lblPaletesAdicionais','txtPaletesAdicionais',4,2],
						  ['lblLargura','txtLargura',5,2],
						  ['lblAltura','txtAltura',5,2],
						  ['lblProfundidade','txtProfundidade',5,2],
						  ['lblLarguraTolerancia','txtLarguraTolerancia',5,2],
						  ['lblAlturaTolerancia','txtAlturaTolerancia',5,2],
						  ['lblProfundidadeTolerancia','txtProfundidadeTolerancia',5,2],
						  ['lblCubagem','txtCubagem',12,2],
						  ['lblObservacao','txtObservacao',30,3],
						  ['lblDescricao','txtDescricao',96,4]], null, null, true);

	startDynamicCmbs();
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    // campos read-only
    txtLocalizacaoCodificada.disabled = true;
    txtNivel.disabled = true;
    chkLocalizacaoUtil.disabled = true;
    txtCubagem.disabled = true;
    txtDescricao.disabled = true;

    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
	{
		chkLocalizacaoUtil.checked = dsoSup01.recordset["LocalizacaoUtil"].value;
		
		adjustLabelsCombos(true);
	}

    glb_BtnFromFramWork = btnClicked;
    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') || (btnClicked == 'SUPINCL'))
        startDynamicCmbs();
    else    
        // volta para a automacao    
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selSujeitoID, selObjetoID, 
    //  selGrupoImpostoEntradaID,selGrupoImpostoSaidaID,
    //  selMoedaEntradaID, selMoedaSaidaID)

    glb_CounterCmbsDynamics = 3;

    // Dados da Empresa Logada (EmpresaID,EmpresaPaisID,EmpresaCidadeID)  = (isNaN(nTipoRegraFiscalID) ? -1 : nTipoRegraFiscalID);
    var nEmpresaData = getCurrEmpresaData();
    var nRegistro = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    var nTops = (!(nRegistro) ? 50 : nRegistro);

    setConnection(dsoCmbDynamic01);
    
    dsoCmbDynamic01.SQL = 'SELECT (ISNULL(MAX(TamanhoMinimo), 0) + 1) AS TipoMinimo ' +
            'FROM LocalizacoesEstoque a ' +
            'WHERE (a.EstadoID = 2 AND a.EmpresaID = ' + nEmpresaData[0] + ')';

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.refresh();

    setConnection(dsoCmbDynamic02);
    
    dsoCmbDynamic02.SQL = 'SELECT (ISNULL(MAX(TamanhoMaximo), 0) + 1) AS TipoMaximo ' +
            'FROM LocalizacoesEstoque a ' +
            'WHERE (a.EstadoID = 2 AND a.EmpresaID = ' + nEmpresaData[0] + ')';

    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.refresh();


    dsoCmbDynamic03.SQL = "SELECT '1' as Indice, LocalizacaoID as fldID, dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1) as fldName, " +
		"dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1) AS Ordem " +
		"FROM LocalizacoesEstoque WITH(NOLOCK) " +
		"WHERE EstadoID = 2 AND EmpresaID=" + nEmpresaData[0] + " " +
		"ORDER BY Indice, Ordem ";

    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic03.refresh();
   }

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    // Se nem todos os DSOs voltaram, espera pelos demais.
    glb_CounterCmbsDynamics--;
    if (glb_CounterCmbsDynamics != 0)
        return null;

    var optionStr, optionValue;
    var aCmbsDynamics = [selTamanhoMinimo, selTamanhoMaximo, selLocalizacaoMaeID];
    var aDSOsDunamics = [dsoCmbDynamic01, dsoCmbDynamic02, dsoCmbDynamic03];
    var nTipoMaximo = 0;
    var nTipoMinimo = 0;
    var i = 0;
    var j = 0;

    clearComboEx(['selTamanhoMinimo', 'selTamanhoMaximo', 'selLocalizacaoMaeID']);
    
    // atualiza a data corrente e calcula os campos calculados
    for (i = 0; i <= aCmbsDynamics.length - 1; i++) {
        if (i == 0) 
        {
            if (!((aDSOsDunamics[i].recordset.BOF) && (aDSOsDunamics[i].recordset.EOF))) 
            {
                aDSOsDunamics[i].recordset.MoveFirst();
                nTipoMinimo = aDSOsDunamics[i].recordset['TipoMinimo'].value;
            }
            
            for (j = 1; j <= nTipoMinimo; j++) {
                optionStr = j;
                optionValue = j;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
            }
        }
        else if (i == 1) {
            if (!((aDSOsDunamics[i].recordset.BOF) && (aDSOsDunamics[i].recordset.EOF))) {
                aDSOsDunamics[i].recordset.MoveFirst();
                nTipoMaximo = aDSOsDunamics[i].recordset['TipoMaximo'].value;
            }

            for (j = 1; j <= nTipoMaximo; j++) {
                optionStr = j;
                optionValue = j;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
            }
        }
        else {
            for (aDSOsDunamics[i].recordset.moveFirst(); !aDSOsDunamics[i].recordset.EOF; aDSOsDunamics[i].recordset.moveNext()) {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
                optionValue = aDSOsDunamics[i].recordset['fldID'].value;

                var oOption = document.createElement("OPTION");

                oOption.text = optionStr;
                oOption.value = optionValue;

                aCmbsDynamics[i].add(oOption);
            }
        }
    }

    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);

    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();

	adjustLabelsCombos(false);

    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // Documentos
    if ((controlBar == 'SUP') && (btnClicked == 1)) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }
    else if ((controlBar == 'SUP') && (btnClicked == 2)) 
    	openModalPrint();
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
	var nEmpresaID = getCurrEmpresaData();
	
	if (btnClicked == 'SUPOK') {
		// Se e um novo registro
		//if(dsoSup01.recordset[glb_sFldIDName] != null && dsoSup01.recordset[glb_sFldIDName].value != null) {
			dsoSup01.recordset['EmpresaID'].value = nEmpresaID[0];
		//}
	}
	
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
	if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // campos read-only
    txtLocalizacaoCodificada.disabled = true;
    txtNivel.disabled = true;
    chkLocalizacaoUtil.disabled = true;
    txtCubagem.disabled = true;
    txtDescricao.disabled = true;
    
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra tres botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Localiza��es']);
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
        setLabelOfControl(lblLocalizacaoMaeID, dsoSup01.recordset['LocalizacaoMaeID'].value);
	}        
    else
    {
        setLabelOfControl(lblLocalizacaoMaeID, selLocalizacaoMaeID.value);
    }
}
/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    var nLocalizacaoID = dsoSup01.recordset['LocalizacaoID'].value;
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&nLocalizacaoID=' + escape(nLocalizacaoID);       
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modmateriais/submodestoques/localizacoes/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(440,270));
}

