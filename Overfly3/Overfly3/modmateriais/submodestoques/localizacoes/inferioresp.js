/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = "SELECT a.LocalizacaoID, a.ProprietarioID, a.AlternativoID, a.Observacoes " + 
			"FROM LocalizacoesEstoque a WHERE " +
			sFiltro + "a.LocalizacaoID = " + idToFind;
        
        return "dso01JoinSup_DSC";
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21201) // Produtos
    {
        dso.SQL = 'SELECT dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) AS Localizacao, ' +
			'd.ConceitoID AS ProdutoID, d.Conceito AS Produto, d.Modelo AS Modelo, b.ReposicaoDiaria AS ReposicaoDiaria, ' +
			'dbo.fn_Produto_PesosMedidas(d.ConceitoID, NULL, 10) AS QuantidadePorCaixa, ' +
			'dbo.fn_LocalizacaoEstoque_ProdutosTotais(a.LocalizacaoID, c.ObjetoID, NULL, NULL, NULL, 0, 1) AS QuantidadeCaixasTotal,' +
			'dbo.fn_LocalizacaoEstoque_ProdutosTotais(a.LocalizacaoID, c.ObjetoID, 1, NULL, NULL, 0, 1) AS QuantidadeCaixasFila, ' +
			'dbo.fn_LocalizacaoEstoque_ProdutosTotais(a.LocalizacaoID, c.ObjetoID, 1, NULL,    1, 0, 1) AS QuantidadeCaixasPilha, ' +
			'a.Ordem AS Ordem ' +
        'FROM RelacoesPesCon_LocalizacoesEstoque a, LocalizacoesEstoque b, RelacoesPesCon c, Conceitos d ' +
        'WHERE ( ' + sFiltro + 'dbo.fn_LocalizacaoEstoque_EhFilha(a.LocalizacaoID, ' + idToFind + ', 1) = 1 AND ' +
			'a.LocalizacaoID = b.LocalizacaoID AND a.RelacaoID = c.RelacaoID AND c.ObjetoID = d.ConceitoID) ' +
		'ORDER BY dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1), d.ConceitoID ';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
       
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a, Recursos b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a, Pessoas b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a, TiposAuxiliares_Itens b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a, Recursos b ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(21201);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 21201); // Produtos

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Produtos
    else if (folderID == 21201)
    {
        headerGrid(fg,['Localiza��o',
						'RD',
                        'Ordem',
                        'ID',
                        'Produto',
                        'Modelo',
                        'Q/Cx',
					    'Total',
					    'Fila',
					    'Pilha'], []);
                       
        // array de celulas com hint
        // glb_aCelHint = [[Row,Col,'Hint'], [Row,Col,'Hint'], ...]
        glb_aCelHint = [[0,1,'Reposi��o di�ria?'],
						[0,6,'Quantidade por caixa'],
						[0,7,'Quantidade de caixas total'],
						[0,8,'Quantidade de caixas por fila'],
						[0,9,'Quantidade de caixas por pilha']];

        fillGridMask(fg,currDSO,['Localizacao',
								 'ReposicaoDiaria',
                                 'Ordem',
                                 'ProdutoID',
                                 'Produto',
                                 'Modelo',
                                 'QuantidadePorCaixa',
								 'QuantidadeCaixasTotal',
								 'QuantidadeCaixasFila',
								 'QuantidadeCaixasPilha']
                                 ,['','','','','','','','','','']);
        
        alignColsInGrid(fg,[2, 3, 6, 7, 8, 9]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }    
}
