/********************************************************************
modalprint_planocontas.js

Library javascript para o modalprint.asp
Valores a Localizar
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_currDate = '';

// dso usado para envia e-mails 
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoEMail = new CDatatransport("dsoEMail");
// dso usado para pegar a data atual 
var dsoCurrData = new CDatatransport("dsoCurrData");
var dsoCombo = new CDatatransport("dsoCombo");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()
   

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    glb_bUseProgressBar = true;
    asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modalprintBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (selReports.disabled == false)
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id)
        btnCanc.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

function oPrinter_OnPrintFinish() {
    winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Relat�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;

    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style) {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta combo de relatorios
    with (selReports.style) {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta o divBarCode
    with (divBarCode.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divLocalizacaoEstoque
    with (divLocalizacaoEstoque.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divInventario
    with (divInventario.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divInconsistencia
    with (divInconsistencia.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // O estado do botao btnOK
    btnOK_Status();

    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;

    selReports.onchange = selReports_Change;

    // carrega a lista de relatorios
    loadReportsInList();

    // linka os divs nos relatorios
    linkDivsAndReports();

    // ajusta os divs

    // ajusta o divBarCode
    adjustDivBarCode();

    adjustdivLocalizacaoEstoque();

    adjustdivInventario();

    adjustdivInconsistencia();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList() {
    var i;

    if (glb_arrayReports != null) {
        for (i = 0; i < glb_arrayReports.length; i++) {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);

            if (glb_arrayReports[i][2])
                selReports.selectedIndex = i;
        }

        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports() {
    divBarCode.setAttribute('report', 40126, 1);
    divLocalizacaoEstoque.setAttribute('report', 40127, 1);
    divInventario.setAttribute('report', 40129, 1);
    divInconsistencia.setAttribute('report', 40128, 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    // O estado do botao btnOK
    btnOK_Status();

    window.focus();
    if (ctl.disabled == false)
        ctl.focus();

    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change() {
    // mostra o div correspondente ao relatorio
    showDivByReportID();

    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if (this.disabled == false)
        this.focus();

    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = true;
    var i, elem;

    if (selReports.selectedIndex != -1) {
        // Relatorio de Plano de Contas
        if (selReports.value == 40126)
            btnOKStatus = false;
        else if (selReports.value == 40127)
            btnOKStatus = false;
        else if (selReports.value == 40128)
            btnOKStatus = false;
        else if (selReports.value == 40129)
            btnOKStatus = false;
    }

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {
    // Relatorio de Plano de Contas
    if ((selReports.value == 40126) && (glb_sCaller == 'S'))
        etiquetaLocalizacao();
    else if ((selReports.value == 40127) && (glb_sCaller == 'PL'))
        localizacaoEstoque();
    else if ((selReports.value == 40128) && (glb_sCaller == 'PL'))
        relatorioInconsistencia();
    else if ((selReports.value == 40129) && (glb_sCaller == 'PL'))
        inventarioEstoque();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID() {
    var i, coll, attr, currRep;

    attr = null;
    currRep = 0;

    currRep = selReports.value;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (attr == currRep)
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr() {
    var i, coll, attr, retVal;

    attr = null;
    retVal = 0;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (coll[i].style.visibility == 'visible') {
                retVal = attr;
                break;
            }
        }
    }
    return retVal;
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights() {
    if (selReports.options.length != 0)
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;

    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else
            coll[i].style.visibility = 'hidden';
    }

    // desabilita o combo de relatorios
    selReports.disabled = true;

    // desabilita o botao OK
    btnOK.disabled = true;

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];

    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);

    elem = document.getElementById('selReports');

    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);

    // a altura livre    
    modHeight -= topFree;

    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt';
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';

    // acrescenta o elemento
    window.document.body.appendChild(elem);

    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);

    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;

    return null;
}

function adjustdivInventario() {
    adjustElementsInForm([['lblProduto', 'chkProduto', 3, 1, -10, -10],
						  ['lblDiferenca', 'chkDiferenca', 3, 1],
						  ['lblData', 'txtData', 17.5, 1],
                          ['lblDeposito', 'selDeposito', 18, 1],
						  ['lblFiltro3', 'txtFiltro3', 35, 3, -10, 103],
                          ['lblFormatoSolicitacao2', 'selFormatoSolicitacao2', 9.3, 3]], null, null, true);

    txtData.maxLength = 16;
    txtData.value = glb_dCurrDateTime;
    lblFamilia2.style.left = 0;
    lblFamilia2.style.top = chkDiferenca.offsetTop + chkDiferenca.offsetHeight + ELEM_GAP;
    selFamilia2.style.left = 0;
    selFamilia2.style.top = lblFamilia2.offsetTop + 16;
    selFamilia2.style.width = 26 * FONT_WIDTH;
    selFamilia2.style.height = 118;


    lblMarca2.style.left = selFamilia2.offsetLeft + selFamilia2.offsetWidth + ELEM_GAP;
    lblMarca2.style.top = chkDiferenca.offsetTop + chkDiferenca.offsetHeight + ELEM_GAP;
    selMarca2.style.left = selFamilia2.offsetLeft + selFamilia2.offsetWidth + ELEM_GAP;
    selMarca2.style.top = lblFamilia2.offsetTop + 16;
    selMarca2.style.width = 18 * FONT_WIDTH;
    selMarca2.style.height = 118;
}

function adjustdivLocalizacaoEstoque() {
    adjustElementsInForm([['lblEstoque', 'chkEstoque', 3, 1, -10, -10],
                            ['lblReposicaoDiaria', 'chkReposicaoDiaria', 3, 1],
                            ['lblProdutosLocalizacao', 'chkProdutosLocalizacao', 3, 1],
                            ['lblInicio', 'txtInicio', 10, 1],
                            ['lblFim', 'txtFim', 10, 1],
                            ['lblOrdemRelLoc', 'selOrdemRelLoc', 9, 1],
                            ['lblFiltro2', 'txtFiltro2', 34, 3, -10, 103],
                            ['lblFormatoSolicitacao', 'selFormatoSolicitacao', 10, 3]], null, null, true);

    lblFamilia.style.left = 0;
    lblFamilia.style.top = txtFim.offsetTop + txtFim.offsetHeight + ELEM_GAP;
    selFamilia.style.left = 0;
    selFamilia.style.top = lblFamilia.offsetTop + 16;
    selFamilia.style.width = 26 * FONT_WIDTH;
    selFamilia.style.height = 118;

    lblMarca.style.left = selFamilia.offsetLeft + selFamilia.offsetWidth + ELEM_GAP;
    lblMarca.style.top = txtFim.offsetTop + txtFim.offsetHeight + ELEM_GAP;
    selMarca.style.left = selFamilia.offsetLeft + selFamilia.offsetWidth + ELEM_GAP;
    selMarca.style.top = lblFamilia.offsetTop + 16;
    selMarca.style.width = 18 * FONT_WIDTH;
    selMarca.style.height = 118;

    chkProdutosLocalizacao.checked = true;

    txtInicio.onfocus = selFieldContent;
    txtInicio.maxLength = 10;

    txtFim.onfocus = selFieldContent;
    txtFim.maxLength = 10;
    chkEstoque.checked = true;
}

function adjustDivBarCode() {
    selEtiquetas.onchange = selEtiquetas_onChange;
    txtVias.onkeypress = verifyNumericEnterNotLinked;
    txtVias.setAttribute('thePrecision', 2, 1);
    txtVias.setAttribute('theScale', 0, 1);
    txtVias.setAttribute('minMax', new Array(0, 99), 1);
    txtVias.setAttribute('verifyNumPaste', 1);
    txtVias.onfocus = selFieldContent;
    txtInicio3.onfocus = selFieldContent;
    txtFim3.onfocus = selFieldContent;

    txtLabelHeight.onkeypress = verifyNumericEnterNotLinked;
    txtLabelGap.onkeypress = verifyNumericEnterNotLinked;
    txtLabelHeight.onfocus = selFieldContent;
    txtLabelGap.onfocus = selFieldContent;
    txtLabelHeight.setAttribute('verifyNumPaste', 1);
    txtLabelGap.setAttribute('verifyNumPaste', 1);
    txtLabelHeight.setAttribute('thePrecision', 3, 1);
    txtLabelGap.setAttribute('thePrecision', 3, 1);
    txtLabelHeight.setAttribute('theScale', 0, 1);
    txtLabelGap.setAttribute('theScale', 0, 1);
    txtLabelHeight.setAttribute('minMax', new Array(0, 999), 1);
    txtLabelGap.setAttribute('minMax', new Array(0, 999), 1);

    var aBarCode;
    var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');

    txtInicio3.maxLength = 25;
    txtFim3.maxLength = 25;

    aBarCode = [[0, 'S� esta'],
                [1, 'Faixa'],
                [2, 'Todas']];

    lblInicio3.style.visibility = 'hidden';
    txtInicio3.style.visibility = 'hidden';
    lblFim3.style.visibility = 'hidden';
    txtFim3.style.visibility = 'hidden';

    txtVias.value = '1';

    // Impressora Yanco
    if (nPrintBarcode == 1) {
        txtLabelHeight.disabled = true;
        lblLabelHeight.style.visibility = 'hidden';
        txtLabelHeight.style.visibility = 'hidden';
        txtLabelGap.disabled = true;
        lblLabelGap.style.visibility = 'hidden';
        txtLabelGap.style.visibility = 'hidden';
    }
    else {
        txtLabelHeight.disabled = false;
        lblLabelHeight.style.visibility = 'inherit';
        txtLabelHeight.style.visibility = 'inherit';
        txtLabelGap.disabled = false;
        lblLabelGap.style.visibility = 'inherit';
        txtLabelGap.style.visibility = 'inherit';
    }

    txtLabelHeight.value = 53;
    txtLabelGap.value = sendJSMessage(getHtmlId(), JS_WIDEMSG, 'LABELBARCODE_GAP', null);

    var i, j, optionStr, optionValue;
    var aCmbsDynamics = [selEtiquetas];
    var aaCombos = [aBarCode];

    clearComboEx(['selEtiquetas']);

    for (i = 0; i <= aCmbsDynamics.length - 1; i++) {
        for (j = 0; j <= aaCombos[i].length - 1; j++) {
            optionValue = aaCombos[i][j][0];
            optionStr = aaCombos[i][j][1];
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
        }
    }

    adjustElementsInForm([['lblEtiquetas', 'selEtiquetas', 13, 1, -10, -10],
						  ['lblLocalizacaoUtil', 'chkLocalizacaoUtil', 3, 1],
                          ['lblVias', 'txtVias', 3, 1],
                          ['lblLabelHeight', 'txtLabelHeight', 4, 1],
                          ['lblLabelGap', 'txtLabelGap', 4, 1],
                          ['lblInicio3', 'txtInicio3', 25, 2, -10],
                          ['lblFim3', 'txtFim3', 25, 2],
                          ['lblFiltro', 'txtFiltro', 51, 3, -10]], null, null, true);

    chkLocalizacaoUtil.checked = true;
}

function adjustdivInconsistencia() {
    adjustElementsInForm([['lblInconsistencia1', 'chkInconsistencia1', 3, 1, -10, -10],
		['lblInconsistencia2', 'chkInconsistencia2', 3, 1],
		['lblInconsistencia3', 'chkInconsistencia3', 3, 1],
		['lblInconsistencia4', 'chkInconsistencia4', 3, 1],
		['lblInconsistencia5', 'chkInconsistencia5', 3, 1],
        ['lblFormatoSolicitacao3', 'selFormatoSolicitacao3', 10, 2, -8, 0]], null, null, true);
}

function selEtiquetas_onChange() {
    // 0 - so esta etiqueta
    // 1 - faixa
    // 2 - todas

    if (selEtiquetas.value == 1) {
        lblInicio3.style.visibility = 'inherit';
        txtInicio3.style.visibility = 'inherit';
        lblFim3.style.visibility = 'inherit';
        txtFim3.style.visibility = 'inherit';
    }
    else {
        lblInicio3.style.visibility = 'hidden';
        txtInicio3.style.visibility = 'hidden';
        lblFim3.style.visibility = 'hidden';
        txtFim3.style.visibility = 'hidden';
    }
}

function dsoCurrDataComplete_DSC() {
    if (!(dsoCurrData.recordset.BOF && dsoCurrData.recordset.EOF))
        glb_currDate = dsoCurrData.recordset['currDate'].value;
}

function etiquetaLocalizacao() {
    lockControlsInModalWin(true);

    setConnection(dsoGen01);

    var empresaData = getCurrEmpresaData();
    var sFiltro = '';
    var sInicio = '';
    var sFim = '';

    if (chkLocalizacaoUtil.checked)
        sFiltro += ' AND dbo.fn_LocalizacaoEstoque_Util(LocalizacaoID) = 1 ';

    if (trimStr(txtFiltro.value) != '')
        sFiltro += ' AND ' + trimStr(txtFiltro.value) + ' ';

    if (selEtiquetas.value == 0)
        sFiltro += ' AND LocalizacaoID = ' + glb_nLocalizacaoID + ' ';
    else if (selEtiquetas.value == 1) {
        sInicio = trimStr(txtInicio3.value);
        sFim = trimStr(txtFim3.value);

        if (sInicio != '')
            sFiltro += ' AND dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1) >= ' + '\'' + sInicio + '\'' + ' ';

        if (sFim != '')
            sFiltro += ' AND dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1) <= ' + '\'' + sFim + '\'' + ' ';
    }

    dsoGen01.SQL = 'SELECT dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1) AS Codigo, ' +
			'dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 2) AS Descricao ' +
		'FROM LocalizacoesEstoque WITH(NOLOCK) ' +
		'WHERE (EstadoID = 2 AND EmpresaID = ' + empresaData[0] + sFiltro + ') ' +
		'ORDER BY dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1)';

    dsoGen01.ondatasetcomplete = etiquetaLocalizacao_DSC;

    try {
        dsoGen01.refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
        txtFiltro.focus();
    }

}

function etiquetaLocalizacao_DSC() {
    lockControlsInModalWin(false);

    var numRecs, i, retVal;
    var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');

    numRecs = 0;

    if (!(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF)) {
        dsoGen01.recordset.MoveLast();
        numRecs = dsoGen01.recordset.RecordCount();
        dsoGen01.recordset.MoveFirst();
    }

    numRecs = numRecs * parseInt(trimStr(txtVias.value), 10);

    if (numRecs == 0) {
        if (window.top.overflyGen.Alert('Nenhuma etiqueta a ser impressa!') == 0)
            return null;
    }
    else {
        if (selEtiquetas.value != 0) {
            if (numRecs == 1)
                retVal = window.top.overflyGen.Confirm('Imprime ' + numRecs.toString() + ' etiqueta?');
            else
                retVal = window.top.overflyGen.Confirm('Imprime ' + numRecs.toString() + ' etiquetas?');
        }
        else {
            retVal = window.top.overflyGen.Confirm('Imprime a etiqueta ' + dsoGen01.recordset['Codigo'].value + ' ?');
        }
    }

    if (retVal == 0)
        return null;
    else if (retVal == 1) {
        var nVias;
        nVias = parseInt((txtVias.value), 10);
        if (nVias <= 0)
            nVias = 1;

        oPrinter.ResetPrinterBarLabel(1, 5, 9);

        dsoGen01.recordset.MoveFirst();

        while (!dsoGen01.recordset.EOF) {
            for (i = 1; i <= nVias; i++) {
                oPrinter.InsertLabelLocalizacaoData(removeDiatricsOnJava(removeCharFromString(dsoGen01.recordset['Descricao'].value, '"')),
													removeDiatricsOnJava(removeCharFromString(dsoGen01.recordset['Codigo'].value, '"')));
            }
            dsoGen01.recordset.MoveNext();
        }

        oPrinter.PrintBarLabels(nPrintBarcode, true, parseInt(txtLabelHeight.value, 10),
                                parseInt(txtLabelGap.value, 10), 80, 1);

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
    }

}

function localizacaoEstoque() {
    var formato = 2;
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');




    var sFiltroFamilia = '';
    nItensSelected = 0;

    for (i = 0; i < selFamilia.length; i++) {
        if (selFamilia.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND d.ProdutoID IN(' : ', ') + selFamilia.options[i].value;
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ') ' : '');


    var sFiltroMarca = '';
    nItensSelected = 0;

    for (i = 0; i < selMarca.length; i++) {
        if (selMarca.options[i].selected == true) {
            nItensSelected++;
            sFiltroMarca += (nItensSelected == 1 ? ' AND d.MarcaID IN (' : ', ') + selMarca.options[i].value;
        }
    }

    sFiltroMarca += (nItensSelected > 0 ? ') ' : '');

    sSelOrdemRelLoc = selOrdemRelLoc.value;
    if (sSelOrdemRelLoc == '')
        sSelOrdemRelLoc = "null";

    sSFiltroMarca = sFiltroMarca;
    if (sSFiltroMarca == '')
        sSFiltroMarca = "null";

    sSFiltroFamilia = sFiltroFamilia;
    if (sSFiltroFamilia == '')
        sSFiltroFamilia = "null";

    stxtFim = txtFim.value;
    if (stxtFim == '')
        stxtFim = "null";

    stxtInicio = txtInicio.value;
    if (stxtInicio == '')
        stxtInicio = "null";

    stxtFiltro2 = txtFiltro2.value;
    if (stxtFiltro2 == '')
        stxtFiltro2 = "null";


    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + selFormatoSolicitacao.value + "&nEmpresaLogadaIdioma=" + aEmpresaData[8] +
                        "&txtFiltro2=" + stxtFiltro2 + "&txtInicio=" + stxtInicio + "&txtFim=" + stxtFim + "&sFiltroFamilia=" + sSFiltroFamilia + "&sFiltroMarca=" + sSFiltroMarca +
                        "&chkReposicaoDiaria=" + (chkReposicaoDiaria.checked ? 1 : 0) + "&chkProdutosLocalizacao=" + (chkProdutosLocalizacao.checked ? 1 : 0) + "&selOrdemRelLoc=" + sSelOrdemRelLoc +
                        "&chkEstoque=" + (chkEstoque.checked ? 1 : 0);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modmateriais/submodestoques/localizacoes/serverside/Reports_localizacoes.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}

function inventarioEstoque() {
    if (trimStr(txtData.value) != '') {
        if (!chkDataEx(txtData.value)) {
            if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
                return null;

            lockControlsInModalWin(false);
            return null;
        }
    }

    var dirA1;
    var dirA2;
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nEmpresaID = 0;
    var sInformation = '';
    var nTopHeader = 0;
    var sData;
    var nDepositoID = 0;

    nDepositoID = selDeposito.value;

    if (nDepositoID == '') {
        nDepositoID = null;
    }

    var sFiltro = (trimStr(txtFiltro3.value) == '' ? '' : ' AND ' + txtFiltro3.value + ' ');

    var sFiltroFamilia = '';
    nItensSelected = 0;

    for (i = 0; i < selFamilia2.length; i++) {
        if (selFamilia2.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' b.ProdutoID IN(' : ', ') + selFamilia2.options[i].value;
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ') AND ' : '');

    var sFiltroMarca = '';
    nItensSelected = 0;

    for (i = 0; i < selMarca2.length; i++) {
        if (selMarca2.options[i].selected == true) {
            nItensSelected++;
            sFiltroMarca += (nItensSelected == 1 ? ' b.MarcaID IN (' : ', ') + selMarca2.options[i].value;
        }
    }

    sFiltroMarca += (nItensSelected > 0 ? ') AND ' : '');

    sFiltro = (trimStr(sFiltro + sFiltroFamilia + sFiltroMarca) == '' ? 'NULL' : '\'' + trimStr(sFiltro + sFiltroFamilia + sFiltroMarca) + '\'');

    if (trimStr(txtData.value) != '')
        sData = '\'' + normalizeDate_DateTime(txtData.value, true) + '\'';
    else
        sData = 'NULL';


    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    // var formato = selFormato.value;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + selFormatoSolicitacao2.value +
                        "&nDepositoID=" + nDepositoID + "&chkProduto=" + (chkProduto.checked ? "1" : "0") + "&chkDiferenca=" + (chkDiferenca.checked ? '1' : '0') + "&sData=" + sData +
                        "&sFiltro=" + sFiltro + "&DataHora=" + glb_dCurrDateTime;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/modmateriais/submodestoques/localizacoes/serverside/Reports_localizacoes.aspx?' + strParameters;

    glbContRel = 1;
}

function relatorioInconsistencia() {
    var sLinguaLogada = getDicCurrLang();
    var formato = selFormatoSolicitacao3.value;

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var sInformation = 'Inconsist�ncias: ';
    var nTopHeader = 0;
    var bHasOneChecked = false;

    for (i = 1; i <= 5; i++) {
        if (eval('chkInconsistencia' + i.toString() + '.checked')) {
            bHasOneChecked = true;
            sInformation += i.toString() + '  ';
        }
    }

    if (!bHasOneChecked) {
        if (window.top.overflyGen.Alert('Selecione pelo menos uma inconsist�ncia') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&nEmpresaLogadaIdioma=" + aEmpresaData[8] + "&chkInconsistencia1=" + (chkInconsistencia1.checked ? 1 : 0) + "&chkInconsistencia2=" + (chkInconsistencia2.checked ? 1 : 0) +
                        "&chkInconsistencia3=" + (chkInconsistencia3.checked ? 1 : 0) + "&chkInconsistencia4=" + (chkInconsistencia4.checked ? 1 : 0) +
                        "&chkInconsistencia5=" + (chkInconsistencia5.checked ? 1 : 0) + "&sInformation=" + sInformation;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/modmateriais/submodestoques/localizacoes/serverside/Reports_localizacoes.aspx?" + strParameters;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
}
