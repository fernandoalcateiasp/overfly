/********************************************************************
modalLocalizacoes.js

Library javascript para o modalLocalizacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_refreshGrid = null;
var glb_timerUnloadWin = null;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_callFillGridTimerInt = null;
var glb_bInclusao = false;

var glb_bFirstTime = true;
var glb_nBindDSOs = 0;

// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
// Combo de moeda .RDS
var dsoCmbs = new CDatatransport("dsoCmbs");
var dsoCmb01Grid = new CDatatransport("dsoCmb01Grid");
// Gravacao .RDS
var dsoGrava = new CDatatransport("dsoGrava");
// Direitos de ususario .RDS
var dsoDireitos = new CDatatransport("dsoDireitos");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
fieldExists(dso, fldName)

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalLocalizacoes.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalLocalizacoes.ASP

js_fg_modalLocalizacoesBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalLocalizacoesDblClick( grid, Row, Col)
js_modalLocalizacoesKeyPress(KeyAscii)
js_modalLocalizacoes_AfterRowColChange
js_modalLocalizacoes_ValidateEdit()
js_modalLocalizacoes_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // ajusta o body do html
    with (modalLocalizacoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Localizações dos produtos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    
    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';

    // ajusta o divCidade
    elem = window.document.getElementById('divPesquisa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 110;
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.currentStyle.top, 10) +
			parseInt(divPesquisa.currentStyle.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    chkProduto.checked = true;
    chkProduto.onclick = chkProduto_onclick;
    chkIncluir.onclick = chkIncluir_onclick;
    adjustInterface();
	txtProduto.onfocus = setColorAndselFieldContent;
	txtProduto.onkeyup = setonkeyup;
    txtProduto.maxLength = 20;
    selProdutoID.onchange = selProdutoID_onchange;
    selProdutoID.onfocus = setColorAndselFieldContent;
	txtLocalizacao.onfocus = setColorAndselFieldContent;
	txtLocalizacao.onkeyup = setonkeyup;
	chkReposicaoDiaria.onclick = chkReposicaoDiaria_onclick;
    txtLocalizacao.maxLength = 20;
    selLocalizacaoID.onchange = selLocalizacaoID_onchange;
    selLocalizacaoID.onfocus = setColorAndselFieldContent;
    chkInventario.onclick = chkInventario_onclick;

	btnExcluir.style.height = btnOK.offsetHeight;
	btnGravar.style.height = btnOK.offsetHeight;
	btnEtiqueta.style.height = btnOK.offsetHeight;
	btnEtiquetaTodas.style.height = btnOK.offsetHeight;
	
	setupBtnsFromGridState();
	
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

function chkProduto_onclick()
{
	fg.Rows = 1;
	adjustInterface();
}

function chkIncluir_onclick()
{
	adjustInterface();
}

function chkInventario_onclick()
{
	if (chkInventario.checked)
		chkIncluir.checked = false;

	fg.Rows = 1;
	adjustInterface();
}

function adjustInterface()
{
	if (!chkIncluir.checked)
	{
		if (chkProduto.checked)
		{
			lblLocalizacao.style.visibility = 'hidden';
			txtLocalizacao.style.visibility = 'hidden';
			lblReposicaoDiaria.style.visibility = 'hidden';
			chkReposicaoDiaria.style.visibility = 'hidden';
			lblLocalizacaoID.style.visibility = 'hidden';
			selLocalizacaoID.style.visibility = 'hidden';
			lblProduto.style.visibility = 'inherit';
			txtProduto.style.visibility = 'inherit';
			lblProdutoID.style.visibility = 'inherit';
			selProdutoID.style.visibility = 'inherit';
			chkInventario.checked = false;
			lblInventario.style.visibility = 'hidden';
			chkInventario.style.visibility = 'hidden';
			btnEtiqueta.style.visibility = 'hidden';
			btnEtiquetaTodas.style.visibility = 'hidden';
			
		}
		else
		{
			lblLocalizacao.style.visibility = 'inherit';
			txtLocalizacao.style.visibility = 'inherit';
			lblReposicaoDiaria.style.visibility = 'inherit';
			chkReposicaoDiaria.style.visibility = 'inherit';
			lblLocalizacaoID.style.visibility = 'inherit';
			selLocalizacaoID.style.visibility = 'inherit';
			lblProduto.style.visibility = 'hidden';
			txtProduto.style.visibility = 'hidden';
			lblProdutoID.style.visibility = 'hidden';
			selProdutoID.style.visibility = 'hidden';
			lblInventario.style.visibility = 'inherit';
			chkInventario.style.visibility = 'inherit';
			btnEtiqueta.style.visibility = 'inherit';
			btnEtiquetaTodas.style.visibility = 'inherit';
		}
	}
	else
	{
		lblLocalizacao.style.visibility = 'inherit';
		txtLocalizacao.style.visibility = 'inherit';
		lblReposicaoDiaria.style.visibility = 'inherit';
		chkReposicaoDiaria.style.visibility = 'inherit';
		lblLocalizacaoID.style.visibility = 'inherit';
		selLocalizacaoID.style.visibility = 'inherit';
		lblProduto.style.visibility = 'inherit';
		txtProduto.style.visibility = 'inherit';
		lblProdutoID.style.visibility = 'inherit';
		selProdutoID.style.visibility = 'inherit';
		chkInventario.checked = false;
		lblInventario.style.visibility = 'hidden';
		chkInventario.style.visibility = 'hidden';
		btnEtiqueta.style.visibility = 'hidden';
		btnEtiquetaTodas.style.visibility = 'hidden';
	}

	if (chkInventario.checked)
	{
		lblIncluir.style.visibility = 'hidden';
		chkIncluir.style.visibility = 'hidden';
		btnExcluir.value = 'Zerar Inv';
		btnExcluir.title = 'Zerar Inventário';
	}
	else
	{
		lblIncluir.style.visibility = 'inherit';
		chkIncluir.style.visibility = 'inherit';
		btnExcluir.value = 'Excluir';
		btnExcluir.title = '';
	}

	window.focus();
	try
	{
		if (chkProduto.checked)
		{
			if (!txtProduto.disabled)
				txtProduto.focus();
		}
		else
		{
			if (!txtLocalizacao.disabled)
				txtLocalizacao.focus();
		}
    }
    catch(e)
    {
		;
    }
    
    setupBtnsFromGridState();
    
	if (chkProduto.checked)
	{
		adjustElementsInForm([['lblProduto1','chkProduto',3,1,-10,-10],
							  ['lblProduto','txtProduto',49,1,-5],
							  ['lblLocalizacao','txtLocalizacao',35,1],
							  ['lblReposicaoDiaria','chkReposicaoDiaria',3,1],
							  ['lblIncluir','chkIncluir',3,2,-10,-8],
							  ['lblProdutoID','selProdutoID',49,2,-5],
							  ['lblLocalizacaoID','selLocalizacaoID',39,2],
							  ['lblInventario','chkInventario',3, 3, -10, -13],
							  ['lblMensagem','txtMensagem', 68, 3, -4],
							  ['btnGravar','btn',btnOK.offsetWidth,3,4,-2],
							  ['btnExcluir','btn',btnOK.offsetWidth,3,2]], null, null, true);
	}
	else
	{
		adjustElementsInForm([['lblProduto1','chkProduto',3,1,-10,-10],
							  ['lblLocalizacao','txtLocalizacao',35,1,-5],
							  ['lblReposicaoDiaria','chkReposicaoDiaria',3,1],
							  ['lblProduto','txtProduto',49,1],
							  ['lblIncluir','chkIncluir',3,2,-10,-8],
							  ['lblLocalizacaoID','selLocalizacaoID',39,2,-5],
							  ['lblProdutoID','selProdutoID',49,2],
							  ['lblInventario','chkInventario',3, 3, -10, -13],
							  ['lblMensagem','txtMensagem', 68, 3, -4],
							  ['btnGravar','btn',btnOK.offsetWidth,3,4,-2],
							  ['btnExcluir','btn',btnOK.offsetWidth,3,2]], null, null, true);
	}

	
	btnExcluir.style.height = btnOK.offsetHeight;
	btnGravar.style.height = btnOK.offsetHeight;
	btnEtiqueta.style.height = btnOK.offsetHeight;
	btnEtiqueta.style.top = selLocalizacaoID.offsetTop;
	btnEtiqueta.style.left = btnGravar.offsetLeft;

	btnEtiquetaTodas.style.height = btnOK.offsetHeight;
	btnEtiquetaTodas.style.top = selLocalizacaoID.offsetTop;
	btnEtiquetaTodas.style.left = btnEtiqueta.offsetLeft + btnEtiqueta.offsetWidth + 3;

	txtMensagem.readOnly = true;
	txtMensagem.style.fontWeight = 'bold';
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnGravar')
    {
		txtMensagem.value = '';
        saveDataInGrid();
    }
    else if (controlID == 'btnExcluir')
    {
		var bReposicaoDiaria;
        var _retMsg = '';

		if (chkInventario.checked)
		{
			_retMsg = window.top.overflyGen.Confirm('Deseja zerar a contagem do inventário?');

			if ( _retMsg == 0 )
				return null;
			else if ( _retMsg == 1 )
			{
				txtMensagem.value = '';
				zerarInventario();
			}
			else
				return null;
		}
		else
		{
			bReposicaoDiaria = ((fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ReposicaoDiaria*')) != 0) ? true : false);

			if (bReposicaoDiaria)
			{
				_retMsg = window.top.overflyGen.Confirm('Deseja excluir esta localização?');

				if ( _retMsg == 0 )
					return null;
				else if ( _retMsg == 1 )
				{
					txtMensagem.value = '';
					excluirLocalizacaoProduto();
				}
				else
					return null;
			}
			else
			{
				txtMensagem.value = '';
				excluirLocalizacaoProduto();
			}
		}
    }
    else if (controlID == 'btnEtiqueta')
    {
		printEtiquetaProduto(false);
	}
    else if (controlID == 'btnEtiquetaTodas')
    {
		printEtiquetaProduto(true);
	}
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    if (glb_refreshGrid != null)
    {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }

    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    showExtFrame(window, true);
    
    window.focus();
    
    if (chkProduto.checked)
		txtProduto.focus();
	else		
		txtLocalizacao.focus();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
	var i;
	
	//DireitoEspecifico
    //Localozacoes Estoque->Localozacoes->Modal Localizacoes (B2)
    //40002 B2-> A2=0 -> Desabilita o botao Incluir.
	if (glb_nA2 == 1)
		chkIncluir.disabled = false;
	else		
		chkIncluir.disabled = true;

	//DireitoEspecifico
    //Localozacoes Estoque->Localozacoes->Modal Localizacoes (B2)
    //40002 B2-> A1&&A2&&chkInventario -> Habilita o botao Excluir.
    //40002 B2-> A2 && !chkInventario && fg.Rows>2 -> Habilita o botao Excluir.
	if ((chkInventario.checked) && (glb_nA1 == 1) && (glb_nA2 == 1))
		btnExcluir.disabled = false;
	else if ((!chkInventario.checked) && (glb_nA2 == 1))
		btnExcluir.disabled = (fg.Rows > 2 ? false : true);
	else		
		btnExcluir.disabled = true;

	if ((glb_nA2 == 1) || (chkInventario.checked))
		btnGravar.disabled = (fg.Rows > 2 ? false : true);
	else		
		btnGravar.disabled = true;
		
	btnEtiqueta.disabled = fg.Row <= 0;
	btnEtiquetaTodas.disabled = fg.Rows <= 2;

	selProdutoID.disabled = (selProdutoID.options.length == 0);
	selLocalizacaoID.disabled = (selLocalizacaoID.options.length == 0);
}

function fillCmbLocalizacao()
{
	lockControlsInModalWin(true);
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbs);

	var sLocalizacao =  txtLocalizacao.value;

    strSQL = 'SELECT a.LocalizacaoID AS fldID, dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) AS fldName ' +
		'FROM LocalizacoesEstoque a WITH(NOLOCK) ' +
		'WHERE (a.EmpresaID = ' + glb_aEmpresaData[0] + ' AND ' +
			'a.EstadoID = 2 AND dbo.fn_LocalizacaoEstoque_Util(a.LocalizacaoID) = 1 AND ' +
			'dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) LIKE ' + '\'' + sLocalizacao + '%' + '\'' + ') ';

	if (chkReposicaoDiaria.checked)
		strSQL += ' AND a.ReposicaoDiaria = 1 ';

	strSQL += 'ORDER BY dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) ';

    dsoCmbs.SQL = strSQL;
    dsoCmbs.ondatasetcomplete = dsoCmbLocalizacao_DSC;
    dsoCmbs.refresh();
}

function dsoCmbLocalizacao_DSC() {
    var optionStr;
	var optionValue;
	
	txtMensagem.value = '';

	clearComboEx(['selLocalizacaoID']);

    while (! dsoCmbs.recordset.EOF )
    {
        optionStr = dsoCmbs.recordset['fldName'].value;
		optionValue = dsoCmbs.recordset['fldID'].value;

		var oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selLocalizacaoID.add(oOption);

        dsoCmbs.recordset.MoveNext();
    }
	
	lockControlsInModalWin(false);
	
	if (!chkProduto.checked)
	{
		txtProduto.value = '';
		clearComboEx(['selProdutoID']);
		fg.Rows = 1;
	}

	if (selLocalizacaoID.options.length == 1)
	{
		selLocalizacaoID.selectedIndex = 0;
		glb_callFillGridTimerInt = window.setInterval('selLocalizacaoID_onchange()', 10, 'JavaScript');
	}
	else
	{
		if (selLocalizacaoID.options.length == 0)
			txtMensagem.value = 'Localização inválida';
		else				
			txtMensagem.value = 'Selecione uma localização';

		selLocalizacaoID.selectedIndex = -1;
		setupBtnsFromGridState();
		setNextFocus(selLocalizacaoID, 1);
	}
}

function fillCmbProduto()
{
	lockControlsInModalWin(true);
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbs);

    strSQL = 'SET NOCOUNT ON CREATE TABLE #Produtos (ProdutoID INT) ' +
		'INSERT INTO #Produtos ' +
			'EXEC sp_Identificador_Produtos ' + glb_aEmpresaData[0] + ', ' + '\'' + txtProduto.value + '\'' + ', NULL, 2 ' +
		'SELECT b.ConceitoID, b.Conceito, b.Modelo, dbo.fn_Produto_Estoque(' + glb_aEmpresaData[0] + ', b.ConceitoID, 341, NULL, NULL, NULL, 375, -1) AS Estoque ' +
			'FROM #Produtos a, Conceitos b WITH(NOLOCK) ' +
			'WHERE (a.ProdutoID = b.ConceitoID) ' +
		'DROP TABLE #Produtos SET NOCOUNT OFF';

    dsoCmbs.SQL = strSQL;
    dsoCmbs.ondatasetcomplete = dsoCmbProduto_DSC;
    dsoCmbs.refresh();
}

function dsoCmbProduto_DSC() {
    var optionStr;
	var optionValue;
	
	txtMensagem.value = '';

	clearComboEx(['selProdutoID']);

    while (! dsoCmbs.recordset.EOF )
    {
        optionStr = dsoCmbs.recordset['ConceitoID'].value + ' ' + dsoCmbs.recordset['Conceito'].value + ' ' + dsoCmbs.recordset['Modelo'].value + ' (' + dsoCmbs.recordset['Estoque'].value + ')';
		optionValue = dsoCmbs.recordset['ConceitoID'].value;

		var oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		selProdutoID.add(oOption);

        dsoCmbs.recordset.MoveNext();
    }
	
	lockControlsInModalWin(false);
	
	if (chkProduto.checked)
	{		
		txtLocalizacao.value = '';
		clearComboEx(['selLocalizacaoID']);
		fg.Rows = 1;
	}

	if (selProdutoID.options.length == 1)
	{
		selProdutoID.selectedIndex = 0;
		glb_callFillGridTimerInt = window.setInterval('selProdutoID_onchange()', 10, 'JavaScript');
	}
	else
	{
		if (selProdutoID.options.length == 0)
			txtMensagem.value = 'Produto inválido';
		else				
			txtMensagem.value = 'Selecione um produto';

		selProdutoID.selectedIndex = -1;
		setupBtnsFromGridState();
		setNextFocus(selProdutoID, 1);
	}
}

function findCmbIndexByOptionID(oCmb, nOptionID)
{
	var retVal = -1;
	var i = 0;
	
	for (i=0; i<oCmb.options.length; i++)
	{
		if (oCmb.options.item(i).value == nOptionID)
		{
			retVal = i;
			break;
		}
	}
		
	return retVal;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}

    lockControlsInModalWin(true);

	var strSQL = '';
	var nEmpresaID = 0;
	var nProdutoID = '';
	var sFiltroLocalizacao = '';
	
    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	if (chkProduto.checked)
	{
		nProdutoID = selProdutoID.value;

		strSQL = 'SELECT b.RelPesConLocalizacaoID, b.Ordem, c.LocalizacaoID, c.ReposicaoDiaria, ' +
			'dbo.fn_LocalizacaoEstoque_ProdutosTotais(c.LocalizacaoID, a.ObjetoID, NULL, NULL, NULL, 0, 1) AS QuantidadeCaixasTotal,' +
			'dbo.fn_LocalizacaoEstoque_ProdutosTotais(c.LocalizacaoID, a.ObjetoID, 1,    NULL, NULL, 0, 1) AS QuantidadeCaixasFila, ' +
			'dbo.fn_LocalizacaoEstoque_ProdutosTotais(c.LocalizacaoID, a.ObjetoID, 1,    NULL,    1, 0, 1) AS QuantidadeCaixasPilha ' +
			'FROM RelacoesPesCon a WITH(NOLOCK), RelacoesPesCon_LocalizacoesEstoque b WITH(NOLOCK), LocalizacoesEstoque c WITH(NOLOCK) ' +
			'WHERE (a.ObjetoID = ' + nProdutoID + ' AND a.SujeitoID = ' + glb_aEmpresaData[0] + ' AND ' +
				'a.TipoRelacaoID = 61 AND a.RelacaoID = b.RelacaoID AND ' +
				'b.LocalizacaoID = c.LocalizacaoID) ' +
			'ORDER BY b.Ordem ';
	}
	else		
	{
		if (selLocalizacaoID.value > 0)
			sFiltroLocalizacao = 'a.LocalizacaoID = ' + selLocalizacaoID.value + ' AND ';

		strSQL = 'SELECT a.LocalizacaoID, a.ReposicaoDiaria AS ReposicaoDiaria, b.RelPesConLocalizacaoID, d.ConceitoID AS ProdutoID, d.Conceito AS Produto, ' +
				'd.Modelo AS Modelo, d.PartNumber AS PartNumber, ' +
				'dbo.fn_Produto_Estoque(' + glb_aEmpresaData[0] + ', d.ConceitoID, 341, NULL, NULL, NULL, 375, -1) AS Estoque, ' +			
				'dbo.fn_Produto_PesosMedidas(c.ObjetoID, NULL, 10) AS QuantidadePorCaixa, ' +
				'dbo.fn_LocalizacaoEstoque_ProdutosTotais(a.LocalizacaoID, c.ObjetoID, NULL, NULL, NULL, 0, 1) AS QuantidadeCaixasTotal,' +
				'dbo.fn_LocalizacaoEstoque_ProdutosTotais(a.LocalizacaoID, c.ObjetoID,    1, NULL, NULL, 0, 1) AS QuantidadeCaixasFila, ' +
				'dbo.fn_LocalizacaoEstoque_ProdutosTotais(a.LocalizacaoID, c.ObjetoID,    1, NULL,    1, 0, 1) AS QuantidadeCaixasPilha, ' +
				'b.Ordem, b.InventarioCaixa, b.InventarioUnidade ' +
			'FROM LocalizacoesEstoque a WITH(NOLOCK), RelacoesPesCon_LocalizacoesEstoque b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK), Conceitos d WITH(NOLOCK) ' +
			'WHERE (' + sFiltroLocalizacao + ' a.LocalizacaoID = b.LocalizacaoID AND ' +
				'b.RelacaoID = c.RelacaoID AND c.SujeitoID = ' + glb_aEmpresaData[0] + ' AND c.TipoRelacaoID = 61 AND ' +
				'c.ObjetoID = d.ConceitoID) ';
				
		if (chkReposicaoDiaria.checked)
			strSQL += ' AND a.ReposicaoDiaria = 1 ';
			
		strSQL += 'ORDER BY d.ConceitoID';
	}
	
	if (glb_bFirstTime)
		glb_nBindDSOs = 2;
	else
		glb_nBindDSOs = 1;

    dsoGrid.SQL = strSQL;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.refresh();
    
    if (glb_bFirstTime)
    {
		setConnection(dsoCmb01Grid);
		dsoCmb01Grid.SQL = 'SELECT a.LocalizacaoID AS fldID, dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) AS fldName ' +
			'FROM LocalizacoesEstoque a WITH(NOLOCK) ' +
			'WHERE (a.EstadoID = 2 AND a.EmpresaID = ' + glb_aEmpresaData[0] + ' AND ' +
			'dbo.fn_LocalizacaoEstoque_Util(a.LocalizacaoID) = 1) ' +
			'ORDER BY dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1)';        

		dsoCmb01Grid.ondatasetcomplete = fillGridData_DSC;
		dsoCmb01Grid.refresh();
		glb_bFirstTime = false;
    }
}

/********************************************************************
Fecha a janela se mostra-la
********************************************************************/
function unloadThisWin()
{
    if (glb_timerUnloadWin != null)
    {
        window.clearInterval(glb_timerUnloadWin);
        glb_timerUnloadWin = null;
    }
   
    if ( window.top.overflyGen.Alert('Nenhum registro disponível.') == 0 )
        return null;
            
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, 'NOREG' );
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nBindDSOs--;
	
	if (glb_nBindDSOs > 0)
		return null;

    var dTFormat = '';
    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');
    var i, j;
    var bgColorGreen, bgColorYellow, bgColorRed;
	var nTipoChamada;
    var aHoldCols = new Array();
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    fg.Editable = false;
    fg.Redraw = 0;
    fg.Rows = 1;

    fg.ExplorerBar = 0;
	
	removeCombo(fg);
	if (chkProduto.checked)
	{
		headerGrid(fg,['Ord', 
					   'Localização',
					   'RD',
					   'Total',
					   'Fila',
					   'Pilha',
					   'RelPesConLocalizacaoID'],[6]);

		fillGridMask(fg,dsoGrid,['Ordem',
						'LocalizacaoID',
						'ReposicaoDiaria*',
						'QuantidadeCaixasTotal*',
						'QuantidadeCaixasFila*',
						'QuantidadeCaixasPilha*',
						'RelPesConLocalizacaoID'],
						['999','','','','','',''],
						['','','','','','','']);

		insertcomboData(fg, getColIndexByColKey(fg, 'LocalizacaoID'), dsoCmb01Grid, 'fldName', 'fldID');

        gridHasTotalLine(fg, '', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'QuantidadeCaixasTotal*'),'######','S'], 
														[getColIndexByColKey(fg, 'QuantidadeCaixasFila*'),'######','S'],
														[getColIndexByColKey(fg, 'QuantidadeCaixasPilha*'),'######','S']]);

		if (fg.Rows > 1)
			fg.TextMatrix(1, getColIndexByColKey(fg, 'Ordem')) = (fg.Rows - 2);
	
		alignColsInGrid(fg,[0,3,4,5]);
		
		// Merge de Colunas
		fg.MergeCells = 4;
		fg.MergeCol(-1) = true;

		fg.FrozenCols = 2;
		fg.Col = 0;
	}
	else		
	{
		if (chkInventario.checked)
			aHoldCols = [6, 8, 9, 10, 13];
		else
			aHoldCols = [11, 12, 13];
			
		headerGrid(fg,['Local',
					   'RD',
					   'Ord',
					   'ID', 
					   'Produto',
					   'Modelo',
					   'Estoque',
					   'Q/Cx',
					   'Total',
					   'Fila',
					   'Pilha',
					   'Inv Cx',
					   'Inv Un',
					   'RelPesConLocalizacaoID'],aHoldCols);

		fillGridMask(fg,dsoGrid,['LocalizacaoID',
						'ReposicaoDiaria*',
						'Ordem*',
						'ProdutoID*',
						'Produto*',
						'Modelo*',
						'Estoque*',
					    'QuantidadePorCaixa*',
						'QuantidadeCaixasTotal*',
						'QuantidadeCaixasFila*',
						'QuantidadeCaixasPilha*',
						'InventarioCaixa',
						'InventarioUnidade',
						'RelPesConLocalizacaoID'],
						['','','','','','','','','','','','99999','99999',''],
						['','','','','','','','','','','','#####','#####','']);

		insertcomboData(fg, getColIndexByColKey(fg, 'LocalizacaoID'), dsoCmb01Grid, 'fldName', 'fldID');

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Produto*'),'###','C'], 
															  [getColIndexByColKey(fg, 'QuantidadeCaixasTotal*'),'######','S'], 
															  [getColIndexByColKey(fg, 'QuantidadeCaixasFila*'),'######','S'], 
															  [getColIndexByColKey(fg, 'QuantidadeCaixasPilha*'),'######','S'], 
															  [getColIndexByColKey(fg, 'InventarioCaixa'),'######','S'], 
															  [getColIndexByColKey(fg, 'InventarioUnidade'),'######','S'], 
															  [getColIndexByColKey(fg, 'Estoque*'),'######','S']]);

		alignColsInGrid(fg,[2,3,6,7,8,9,10,11,12]);

		fg.FrozenCols = 2;
		fg.Col = 0;
	}

	fg.Redraw = 0;
    paintReadOnlyCols(fg);
    fg.Redraw = 2;
    fg.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg.Rows > 2 )
        fg.Row = 1;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    if (fg.Rows > 2)
        fg.Editable = true;
	
    lockControlsInModalWin(false);

	if (!glb_bInclusao)
		nTipoChamada = 1;
	else
	{
		nTipoChamada = 2;
		glb_bInclusao = false;
	}

	if (chkProduto.checked)
		setNextFocus(selProdutoID, nTipoChamada);
	else		
		setNextFocus(selLocalizacaoID, nTipoChamada);

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function saveDataInGrid()
{
	var strPars = '';
	var nBytesAcum = 0;
	var nDataLen = 0;
	
	lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);
    
    for (i=2; i<fg.Rows; i++)
    {
		nBytesAcum=strPars.length;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
				strPars = '';
				nDataLen = 0;
			}
		}

		nDataLen++;
		strPars += (strPars == '' ? '?' : '&');
		strPars += 'nRelPesConLocalizacaoID=' + escape(getCellValueByColKey(fg, 'RelPesConLocalizacaoID', i));

		if (!chkInventario.checked)
		{
			strPars += '&nInventario=' + escape(0);

			if (!chkProduto.checked)
				strPars += '&nOrdem=' + escape(trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Ordem*'))));
			else
				strPars += '&nOrdem=' + escape(trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Ordem'))));

			strPars += '&nLocalizacaoID=' + escape(trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'LocalizacaoID'))));
			strPars += '&nInventarioCaixa=' + escape(0);
			strPars += '&nInventarioUnidade=' + escape(0);
		}
		else
		{
			strPars += '&nInventario=' + escape(1);
			strPars += '&nOrdem=' + escape(0);
			strPars += '&nLocalizacaoID=' + escape(0);
			strPars += '&nInventarioCaixa=' + escape(trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'InventarioCaixa'))));
			strPars += '&nInventarioUnidade=' + escape(trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'InventarioUnidade'))));
		}
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/modmateriais/submodestoques/localizacoes/serverside/gravarlocalizacoes.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_callFillGridTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Erro na gravação') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_callFillGridTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
		return null;
	}
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}


/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);
	glb_callFillGridTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

/********************************************************************
Verifica se um dado campo existe em um dado dso

Parametros:
dso         - referencia ao dso
fldName     - nome do campo a verificar a existencia
                      
Retorno:
true se o campo existe, caso contrario false
********************************************************************/
function fieldExists(dso, fldName)
{
    var retVal = false;
    var i;
    
    for ( i=0; i< dso.recordset.fields.count; i++ )
    {
        if ( dso.recordset.fields[i].name == fldName )
            retVal = true;
    }
    
    return retVal;
}

function selProdutoID_onchange()
{
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}

	if (chkProduto.checked)
		fillGridData();
	else
	{
		setNextFocus(selProdutoID, 1);
		setupBtnsFromGridState();
	}
}

function chkReposicaoDiaria_onclick()
{
	fillCmbLocalizacao();
}

function selLocalizacaoID_onchange()
{
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}

	if (!chkProduto.checked)
		fillGridData();
	else
	{
		setNextFocus(selLocalizacaoID, 1);
		setupBtnsFromGridState();
	}
}
/********************************************************************
Configuracao do proximo focus
********************************************************************/
function setonkeyup()
{ 
    if ( event.keyCode == 13 )
		setNextFocus(this, 1);
}

/********************************************************************
Configuracao do proximo focus
nTipoChamada
1-Enter
2-Retorno do banco
********************************************************************/
function setNextFocus(othis, nTipoChamada)
{
	var txtMaster = '';
	var selMaster = '';
	var txtSlave = '';
	var selSlave = '';

	if (othis.id == 'txtProduto')
		fillCmbProduto();
	else if (othis.id == 'txtLocalizacao')
		fillCmbLocalizacao();
	else
	{
		if (chkProduto.checked)
		{
			txtMaster = txtProduto;
			selMaster = selProdutoID;
			txtSlave = txtLocalizacao;
			selSlave = selLocalizacaoID;
		}
		else
		{
			txtMaster = txtLocalizacao;
			selMaster = selLocalizacaoID;
			txtSlave = txtProduto;
			selSlave = selProdutoID;
		}

		if (selMaster.options.length == 0)
			txtMaster.focus();
		else
		{
			if (selMaster.selectedIndex == -1)
				selMaster.focus();
			else if (!chkIncluir.checked)
			{
				if ((txtMaster.id == txtLocalizacao.id) && (selLocalizacaoID.options.length > 1))
					selLocalizacaoID.focus();
				else
					txtMaster.focus();
			}
			else
			{
				if (othis.id == selMaster.id)
				{
					if (nTipoChamada == 1)
						txtSlave.focus();
					else
					{
						txtProduto.focus();
					}						
				}
				else if (othis.id == selSlave.id)
				{
					if (selSlave.options.length == 0)
						txtSlave.focus();
					else
					{
						if (selSlave.selectedIndex == -1)
							selSlave.focus();
						else
						{
							// fazer a inclusão
							incluirLocalizacaoProduto();
						}								
					}							
				}						
			}
		}				
	}
}

/********************************************************************
Seleciona conteudo de um controle e muda a cor quando o mesmo recebe foco
********************************************************************/
function setColorAndselFieldContent()
{
	txtProduto.style.fontWeight = '';
	txtProduto.style.backgroundColor = 'transparent';
	selProdutoID.style.fontWeight = '';
	selProdutoID.style.backgroundColor = 'transparent';
	txtLocalizacao.style.fontWeight = '';
	txtLocalizacao.style.backgroundColor = 'transparent';
	selLocalizacaoID.style.fontWeight = '';
	selLocalizacaoID.style.backgroundColor = 'transparent';

	this.style.fontWeight = 'bold';
	this.style.backgroundColor = '#FF7F50';

	if ((this.id == 'txtProduto') || (this.id == 'txtLocalizacao'))
		this.select();
}

function incluirLocalizacaoProduto()
{
	glb_bInclusao = true;

	var strPars = '?nEmpresaID=' + escape(glb_aEmpresaData[0]) +
		'&nProdutoID=' + escape(selProdutoID.value) +
		'&nLocalizacaoID=' + escape(selLocalizacaoID.value);

	lockControlsInModalWin(true);

	dsoGrava.URL = SYS_ASPURLROOT + '/modmateriais/submodestoques/localizacoes/serverside/incluir.aspx' + strPars;
	dsoGrava.ondatasetcomplete = incluirLocalizacaoProduto_DSC;
	dsoGrava.refresh();
}

function incluirLocalizacaoProduto_DSC() {
    lockControlsInModalWin(false);
	
	txtMensagem.value = '';

	if (chkProduto.checked)
	{
		txtLocalizacao.value = '';
		clearComboEx(['selLocalizacaoID']);
	}
	else		
	{
		txtProduto.value = '';
		clearComboEx(['selProdutoID']);
	}
	
	if (dsoGrava.recordset.fields.count > 0)
	{
		if ( !((dsoGrava.recordset.BOF)&&(dsoGrava.recordset.EOF)) )
		{
			if ((dsoGrava.recordset['Resultado'].value != null) &&
				(dsoGrava.recordset['Resultado'].value != ''))
				txtMensagem.value = dsoGrava.recordset['Resultado'].value;
		}
	}

	glb_callFillGridTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

function excluirLocalizacaoProduto()
{
	if (fg.Rows > 2)
	{
		var strPars = '?nRelPesConLocalizacaoID=' + escape(getCellValueByColKey(fg, 'RelPesConLocalizacaoID', fg.Row));

		lockControlsInModalWin(true);

		dsoGrava.URL = SYS_ASPURLROOT + '/modmateriais/submodestoques/localizacoes/serverside/excluir.aspx' + strPars;
		dsoGrava.ondatasetcomplete = excluirLocalizacaoProduto_DSC;
		dsoGrava.refresh();
	}
}

function excluirLocalizacaoProduto_DSC() {
    lockControlsInModalWin(false);
	glb_callFillGridTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

function zerarInventario()
{
	var strPars = '?nEmpresaID=' + escape(glb_aEmpresaData[0]);

	lockControlsInModalWin(true);

	dsoGrava.URL = SYS_ASPURLROOT + '/modmateriais/submodestoques/localizacoes/serverside/zerarinventario.aspx' + strPars;
	dsoGrava.ondatasetcomplete = zerarInventario_DSC;
	dsoGrava.refresh();
}

function zerarInventario_DSC() {
    lockControlsInModalWin(false);
	glb_callFillGridTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalLocalizacoesBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalLocalizacoesDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalLocalizacoesKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalLocalizacoes_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalLocalizacoes_AfterEdit(Row, Col)
{
	;
}

// FINAL DE EVENTOS DE GRID *****************************************
function oPrinter_OnPrintFinish()
{
    winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
}

function printEtiquetaProduto(bTodas)
{
	if (bTodas)
	{
		if (fg.Rows <= 2)
			return null;
	}
	else if (fg.Row <= 1)
		return null;

	var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');
	var i;
	var nID = 0;
	var sDescricao = '';
	
	oPrinter.ResetPrinterBarLabel(1, 5, 9);
	if (!bTodas)
	{
		nID = getCellValueByColKey(fg, 'ProdutoID*', fg.Row);
		sDescricao = getCellValueByColKey(fg, 'Produto*', fg.Row);
		oPrinter.InsertLabelLocalizacaoData(nID, sDescricao);
	}
	else
	{
		for (i=2; i<fg.Rows; i++)
		{
			nID = getCellValueByColKey(fg, 'ProdutoID*', i);
			sDescricao = getCellValueByColKey(fg, 'Produto*', i);
			oPrinter.InsertLabelLocalizacaoData(nID, sDescricao);
		}
	}

    oPrinter.PrintBarLabels(nPrintBarcode, true, 15, 3, 220, 2);
}