
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalprintHtml" name="modalprintHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modmateriais/submodestoques/localizacoes/modalpages/modalprint.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_progressbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodalprint.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmateriais/submodestoques/localizacoes/modalpages/modalprint_localizacoes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, sEmpresaFantasia, nContextoID, nUserID, nLocalizacaoID
Dim relatorioID, relatorio, EhDefault, relatorioFantasia, dirA1, dirA2, dirA1A2True
Dim currDateFormat

sCaller = ""
nEmpresaID = 0
sEmpresaFantasia = ""
nContextoID = 0
nUserID = 0
nLocalizacaoID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'sEmpresaFantasia - fantasia da empresa logada
For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

'nContextoID - ID do contexto
For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

'nUserID - ID do usuario logado
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("nLocalizacaoID").Count    
    nLocalizacaoID = Request.QueryString("nLocalizacaoID")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("currDateFormat").Count
    currDateFormat = Request.QueryString("currDateFormat")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & sEmpresaFantasia & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nLocalizacaoID = " & nLocalizacaoID & ";"
Response.Write vbcrlf

Dim sHour, sMinute
sHour = Hour(Now())
sMinute = Minute(Now())

If (Len(CStr(sHour)) = 1) Then
	sHour = "0" & CStr(sHour)
End If

If (Len(CStr(sMinute)) = 1) Then
	sMinute = "0" & CStr(sMinute)
End If

If (currDateFormat = "DD/MM/YYYY") Then
	Response.Write "var glb_dCurrDateTime = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & " " & sHour & ":" & sMinute & "';"
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';"
    Response.Write "var glb_dCurrDateIni = '" & Day(DateAdd("d", -3, Date)) & "/" & Month(DateAdd("d", -3, Date))& "/" & Year(DateAdd("d", -3, Date)) & "';"
ElseIf (currDateFormat = "MM/DD/YYYY") Then
	Response.Write "var glb_dCurrDateTime = '" & Month(Date) & "/" & Day(Date) & "/" & Year(Date) & " " & sHour & ":" & sMinute & "';"
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';"
    Response.Write "var glb_dCurrDateIni = '" & Month(DateAdd("d", -3, Date)) & "/" & Day(DateAdd("d", -3, Date))& "/" & Year(DateAdd("d", -3, Date)) & "';"
ElseIf (currDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';"
    Response.Write "var glb_dCurrDateIni = '" & Year(DateAdd("d", -3, Date)) & "/" & Month(DateAdd("d", -3, Date)) & "/" & Day(DateAdd("d", -3, Date)) & "';"
Else
    Response.Write "var glb_dCurrDate = '';"
End If

Response.Write vbcrlf

'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

Dim rsData, strSQL
Dim rsData1

If ( (sCaller) = "PL" ) Then
    i = 0
Else
    i = 1
End If        

strSQL = "SELECT DISTINCT d.RecursoID AS RelatorioID, d.Recurso AS Relatorio, " & _
         "c.EhDefault, d.RecursoFantasia AS RelatorioFantasia, c.Ordem, " & _
         "g.Alterar1 AS A1, g.Alterar2 AS A2 " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _
         "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK) " & _
         "WHERE a.ObjetoID= " & CStr(nContextoID) & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 " & _
         "AND a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.TipoRecursoID=4 " & _ 
         "AND b.Principal=1 AND b.RecursoID=c.ObjetoID AND c.EstadoID=2 " & _
         "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID " & _
         "AND d.EstadoID=2 AND d.TipoRecursoID=5 AND d.ClassificacaoID=14 AND d.EhDetalhe= " & CStr(i) & " " & _
         "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & CStr(nContextoID) & " )) " & _
         "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
         "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
         "AND e.SujeitoID IN ((SELECT " & CStr(nUserID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(nUserID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND e.RelacaoID=f.RelacaoID " & _
         "AND f.EmpresaID= " & CStr(nEmpresaID) & " AND g.ContextoID= " & CStr(nContextoID) & " " & _
         "AND c.ObjetoID=g.RecursoMaeID AND d.RecursoID=g.RecursoID " & _
         "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND f.PerfilID=g.PerfilID " & _
         "ORDER BY c.Ordem"

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
Response.Write "var glb_arrayReports = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf
'------------------
i = 0
Do While (Not rsData.EOF)
        
    relatorioID = rsData.Fields("RelatorioID").Value
    relatorio = rsData.Fields("Relatorio").Value
    EhDefault = rsData.Fields("EhDefault").Value
    relatorioFantasia = rsData.Fields("RelatorioFantasia").Value
    dirA1 = rsData.Fields("A1").Value
    dirA2 = rsData.Fields("A2").Value
    
    If ( dirA1 AND dirA2 ) Then
        dirA1A2True = TRUE        
    Else
        dirA1A2True = FALSE    
    End If
        
    rsData.MoveNext
        
    Do While (Not rsData.EOF)
        If ( CLng(relatorioID) = CLng(rsData.Fields("RelatorioID").Value) ) Then
            'dirX1 e dirX2 e o anterior sempre
            If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
                dirA1A2True = TRUE        
            End If

            dirA1 = dirA1 OR rsData.Fields("A1").Value
            dirA2 = dirA2 OR rsData.Fields("A2").Value
            rsData.MoveNext
        Else
            Exit Do
        End If    
    Loop    
        
    If ( (dirA1) AND (dirA2) ) Then
        If ( NOT (dirA1A2True)  ) Then
            dirA1 = FALSE
            dirA2 = TRUE
        End If
    End If    

    Response.Write "glb_arrayReports[" & CStr(i) & "] = new Array("
    Response.Write Chr(39)
    Response.Write CStr(relatorio)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write CStr(relatorioID)
    Response.Write ","
    Response.Write LCase(CStr(EhDefault))
    Response.Write ","
    Response.Write Chr(39)
    Response.Write CStr(relatorioFantasia)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write LCase(CStr(dirA1))
    Response.Write ","
    Response.Write LCase(CStr(dirA2))
    Response.Write ");"
    i = i + 1

Loop

rsData.Close
Set rsData = Nothing
Response.Write vbcrlf

Response.Write "</script>"

Response.Write vbcrlf

'--- Interface dos relatorios do form Relacoes entre Pessoas 
    
'strSQL = ""

'Set rsData1 = Server.CreateObject("ADODB.Recordset")         

'rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

'--- Final de Interface do Relatorios do form Relacoes entre Pessoas  

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=oPrinter EVENT=OnPrintFinish>
<!--
 oPrinter_OnPrintFinish();
//-->
</SCRIPT>

    
</head>

<body id="modalprintBody" name="modalprintBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Controle de impressao matricial -->
    <object classid="clsid:1DF93CFF-D5C8-4A6D-983F-19DBAAC698D9" id = "oPrinter" name = "oPrinter" width=0 height=0 VIEWASTEXT></object>
    
    <div id="divBarCode" name="divBarCode" class="divGeneral">
        <p id="lblEtiquetas" name="lblEtiquetas" class="lblGeneral">Etiquetas</p>
        <select id="selEtiquetas" name="selEtiquetas" class="fldGeneral"></select>
        <p id="lblLocalizacaoUtil" name="lblLocalizacaoUtil" class="lblGeneral">LU</p>
        <input type="checkbox" id="chkLocalizacaoUtil" name="chkLocalizacaoUtil" class="fldGeneral" title="S� localiza��es �teis?"></input>
        <p id="lblVias" name="lblVias" class="lblGeneral">Vias</p>
        <input type="text" id="txtVias" name="txtVias" class="fldGeneral"></input>
        <p id="lblLabelHeight" name="lblLabelHeight" class="lblGeneral">Alt</p>
        <input type="text" id="txtLabelHeight" name="txtLabelHeight" class="fldGeneral" title="Altura (mm)"></input>
        <p id="lblLabelGap" name="lblLabelGap" class="lblGeneral">Gap</p>
        <input type="text" id="txtLabelGap" name="txtLabelGap" class="fldGeneral" title="Espa�amento entre etiquetas"></input>
        <p id="lblInicio3" name="lblInicio3" class="lblGeneral">In�cio</p>
        <input type="text" id="txtInicio3" name="txtInicio3" class="fldGeneral"></input>
        <p id="lblFim3" name="lblFim3" class="lblGeneral">Fim</p>
        <input type="text" id="txtFim3" name="txtFim3" class="fldGeneral"></input>
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral"></input>
    </div>
    
    <div id="divInventario" name="divInventario" class="divGeneral">
        <p id="lblProduto" name="lblProduto" class="lblGeneral">Prod</p>
        <input type="checkbox" id="chkProduto" name="chkProduto" class="fldGeneral" title="S� produtos inventariados ou todos?"></input>
        <p id="lblDiferenca" name="lblDiferenca" class="lblGeneral">Dif</p>
        <input type="checkbox" id="chkDiferenca" name="chkDiferenca" class="fldGeneral" title="Diferen�a?"></input>
        <p id="lblData" name="lblData" class="lblGeneral">Data</p>
        <input type="text" id="txtData" name="txtData" class="fldGeneral"></input>
		<p id="lblFamilia2" name="lblFamilia2" class="lblGeneral">Fam�lias</p>
		<select id="selFamilia2" name="selFamilia2" class="fldGeneral" MULTIPLE>
		<%
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT c.ConceitoID AS fldID, c.Conceito AS fldName " & _
                    "FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & CStr(nEmpresaID) & " AND " & _
						"a.TipoRelacaoID = 61 AND a.ObjetoID = b.ConceitoID AND b.ProdutoID = c.ConceitoID) " & _
                    "GROUP BY c.ConceitoID, c.Conceito " & _
                    "ORDER BY c.Conceito"

			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>
		</select>

		<p id="lblMarca2" name="lblMarca2" class="lblGeneral">Marcas</p>
		<select id="selMarca2" name="selMarca2" class="fldGeneral" MULTIPLE>
		<%
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT c.ConceitoID AS fldID, c.Conceito AS fldName " & _
                    "FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & CStr(nEmpresaID) & " AND " & _
						"a.TipoRelacaoID = 61 AND a.ObjetoID = b.ConceitoID AND b.MarcaID = c.ConceitoID) " & _
                    "GROUP BY c.ConceitoID, c.Conceito " & _
                    "ORDER BY c.Conceito"

			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>
		</select>
        
        <p id="lblDeposito" name="lblDeposito" class="lblGeneral">Dep�sito</p>
		<select id="selDeposito" name="selDeposito" class="fldGeneral">
		<%
			Set rsData = Server.CreateObject("ADODB.Recordset")
            
            strSQL = " SELECT NULL AS fldID, 'Todos' AS fldName UNION ALL SELECT a.ObjetoID AS fldID, dbo.fn_Pessoa_Fantasia(a.ObjetoID,0) AS fldName " & _
	                      "FROM RelacoesPessoas a WITH(NOLOCK) " & _
	                      "WHERE a.SujeitoID = " & CStr(nEmpresaID) & " AND a.TipoRelacaoID = 28 " 
            
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)

            	If (rsData.Fields("fldID").Value = CLng(nEmpresaID) ) Then
			        Response.Write " selected"
		        End If

				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>
		</select>
      
          <p id="lblFiltro3" name="lblFiltro3" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro3" name="txtFiltro3" class="fldGeneral"></input>

          <p id="lblFormatoSolicitacao2" name="lblFormatoSolicitacao2" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao2" name="selFormatoSolicitacao2" class="fldGeneral">
            <option value="1">PDF</option>		
            <option value="2">Excel</option>	
        </select>
    </div>
    
    <div id="divLocalizacaoEstoque" name="divLocalizacaoEstoque" class="divGeneral">
        <p id="lblEstoque" name="lblEstoque" class="lblGeneral">Est</p>
        <input type="checkbox" id="chkEstoque" name="chkEstoque" class="fldGeneral" title="Mostra estoque?"></input>
        <p id="lblReposicaoDiaria" name="lblReposicaoDiaria" class="lblGeneral">RD</p>
        <input type="checkbox" id="chkReposicaoDiaria" name="chkReposicaoDiaria" class="fldGeneral" title="S� reposi��o di�ria?"></input>
        <p id="lblProdutosLocalizacao" name="lblProdutosLocalizacao" class="lblGeneral">PL</p>
        <input type="checkbox" id="chkProdutosLocalizacao" name="chkProdutosLocalizacao" class="fldGeneral" title="S� produtos com localiza��o?"></input>
        <p id="lblInicio" name="lblInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="Text1" name="txtInicio" class="fldGeneral"></input>
        <p id="lblFim" name="lblFim" class="lblGeneral" >Fim</p>
        <input type="text" id="Text2" name="txtFim" class="fldGeneral"></input>
        
		<p id="lblOrdemRelLoc" name="lblOrdemRelLoc" class="lblGeneral">Ordem</p>
		<select id="selOrdemRelLoc" name="selOrdemRelLoc" class="fldGeneral">
			<option value="ID">ID</option>
			<option value="PRODUTO">Produto</option>
			<option value="MODELO">Modelo</option>
		</select>

		<p id="lblFamilia" name="lblFamilia" class="lblGeneral">Fam�lias</p>
		<select id="selFamilia" name="selFamilia" class="fldGeneral" MULTIPLE>
		<%
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT c.ConceitoID AS fldID, c.Conceito AS fldName " & _
                    "FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & CStr(nEmpresaID) & " AND " & _
						"a.TipoRelacaoID = 61 AND a.ObjetoID = b.ConceitoID AND b.ProdutoID = c.ConceitoID) " & _
                    "GROUP BY c.ConceitoID, c.Conceito " & _
                    "ORDER BY c.Conceito"

			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>
		</select>

		<p id="lblMarca" name="lblMarca" class="lblGeneral">Marcas</p>
		<select id="selMarca" name="selMarca" class="fldGeneral" MULTIPLE>
		<%
			Set rsData = Server.CreateObject("ADODB.Recordset")

			strSQL = "SELECT c.ConceitoID AS fldID, c.Conceito AS fldName " & _
                    "FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & CStr(nEmpresaID) & " AND " & _
						"a.TipoRelacaoID = 61 AND a.ObjetoID = b.ConceitoID AND b.MarcaID = c.ConceitoID) " & _
                    "GROUP BY c.ConceitoID, c.Conceito " & _
                    "ORDER BY c.Conceito"

			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend

			rsData.Close
			Set rsData = Nothing
		%>
		</select>
        
        <p id="lblFiltro2" name="lblFiltro2" class="lblGeneral">Filtro</p>
        <input type="text" id="Text3" name="txtFiltro2" class="fldGeneral"></input>

        <p id="lblFormatoSolicitacao" name="lblFormatoSolicitacao" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao" name="selFormatoSolicitacao" class="fldGeneral">
            <option value="2">Excel</option>		
            <option value="1">PDF</option>	
        </select>
    </div>
    
    <div id="divInconsistencia" name="divInconsistencia" class="divGeneral">
        <p id="lblInconsistencia1" name="lblInconsistencia1" class="lblGeneral">1</p>
        <input type="checkbox" id="chkInconsistencia1" name="chkInconsistencia1" class="fldGeneral" title="Verifica inconsist�ncia 1?" checked></input>
        <p id="lblInconsistencia2" name="lblInconsistencia2" class="lblGeneral">2</p>
        <input type="checkbox" id="chkInconsistencia2" name="chkInconsistencia2" class="fldGeneral" title="Verifica inconsist�ncia 2?"></input>
        <p id="lblInconsistencia3" name="lblInconsistencia3" class="lblGeneral">3</p>
        <input type="checkbox" id="chkInconsistencia3" name="chkInconsistencia3" class="fldGeneral" title="Verifica inconsist�ncia 3?" checked></input>
        <p id="lblInconsistencia4" name="lblInconsistencia4" class="lblGeneral">4</p>
        <input type="checkbox" id="chkInconsistencia4" name="chkInconsistencia4" class="fldGeneral" title="Verifica inconsist�ncia 4?" checked></input>
        <p id="lblInconsistencia5" name="lblInconsistencia5" class="lblGeneral">5</p>
        <input type="checkbox" id="chkInconsistencia5" name="chkInconsistencia5" class="fldGeneral" title="Verifica inconsist�ncia 5?" checked></input>

        <p id="lblFormatoSolicitacao3" name="lblFormatoSolicitacao3" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao3" name="selFormatoSolicitacao3" class="fldGeneral">
            <option value="2">Excel</option>
            <option value="1">PDF</option>
        </select>
    </div>

    <p id="lblReports" name="lblReports" class="paraNormal"></p>
    <select id="selReports" name="selReports" class="fldGeneral">
    </select>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>

</body>

</html>
