<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="localizacoessup01Html" name="localizacoessup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmateriais/submodestoques/localizacoes/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modmateriais/submodestoques/localizacoes/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="localizacoessup01Body" name="localizacoessup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">
        <p id="lblLocalizacao" name="lblLocalizacao" class="lblGeneral">Localiza��o</p>
        <input type="text" id="txtLocalizacao" name="txtLocalizacao" DATASRC="#dsoSup01" DATAFLD="Localizacao" class="fldGeneral">
        <p id="lblCodigo" name="lblCodigo" class="lblGeneral">C�digo</p>
        <input type="text" id="txtCodigo" name="txtCodigo" DATASRC="#dsoSup01" DATAFLD="Codigo" class="fldGeneral">
        <p id="lblLocalizacaoCodificada" name="lblLocalizacaoCodificada" class="lblGeneral">Loc Codificada</p>
        <input type="text" id="txtLocalizacaoCodificada" name="txtLocalizacaoCodificada" DATASRC="#dsoSup01" DATAFLD="LocalizacaoCodificada" class="fldGeneral">
        <p id="lblLocalizacaoMaeID" name="lblLocalizacaoMaeID" class="lblGeneral">Loc M�e</p>
        <select id="selLocalizacaoMaeID" name="selLocalizacaoMaeID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="LocalizacaoMaeID"></select>
        <p id="lblNivel" name="lblNivel" class="lblGeneral">N�vel</p>
        <input type="text" id="txtNivel" name="txtNivel" DATASRC="#dsoSup01" DATAFLD="Nivel" class="fldGeneral">
        <p id="lblTamanhoMinimo" name="lblTamanhoMinimo" class="lblGeneral" title="Tamanho m�nimo de produto que a localiza��o comporta">Tam M�n</p>
        <select id="selTamanhoMinimo" name="selTamanhoMinimo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TamanhoMinimo"></select>
        <p id="lblTamanhoMaximo" name="lblTamanhoMaximo" class="lblGeneral" title="Tamanho m�ximo de produto que a localiza��o comporta">Tam M�x</p>
        <select id="selTamanhoMaximo" name="selTamanhoMaximo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TamanhoMaximo"></select>
        <p id="lblEstoqueSeguro" name="lblEstoqueSeguro" class="lblGeneral">ES</p>
        <input type="checkbox" id="chkEstoqueSeguro" name="chkEstoqueSeguro" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EstoqueSeguro" title="Estoque seguro?">
        <p id="lblLocalizacaoUtil" name="lblLocalizacaoUtil" class="lblGeneral">LU</p>
        <input type="checkbox" id="chkLocalizacaoUtil" name="chkLocalizacaoUtil" class="fldGeneral" title="Localiza��o �til?">
        <p id="lblPalete" name="lblPalete" class="lblGeneral">Palete</p>
        <input type="checkbox" id="chkPalete" name="chkPalete" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Palete" title="� palete?">
        <p id="lblReposicaoDiaria" name="lblReposicaoDiaria" class="lblGeneral">RD</p>
        <input type="checkbox" id="chkReposicaoDiaria" name="chkReposicaoDiaria" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ReposicaoDiaria" title="Reposi��o di�ria?">
        <p id="lblProdutosDistintos" name="lblProdutosDistintos" class="lblGeneral">Produtos</p>
        <input type="text" id="txtProdutosDistintos" name="txtProdutosDistintos" DATASRC="#dsoSup01" DATAFLD="ProdutosDistintos" class="fldGeneral" title="N�mero de produtos distintos">
        <p id="lblPaletesAdicionais" name="lblPaletesAdicionais" class="lblGeneral">Pal Adic</p>
        <input type="text" id="txtPaletesAdicionais" name="txtPaletesAdicionais" DATASRC="#dsoSup01" DATAFLD="PaletesAdicionais" class="fldGeneral" title="N�mero de paletes adicionais">
        <p id="lblLargura" name="lblLargura" class="lblGeneral">Largura</p>
        <input type="text" id="txtLargura" name="txtLargura" DATASRC="#dsoSup01" DATAFLD="Largura" class="fldGeneral" title="Largura (mm)">
        <p id="lblAltura" name="lblAltura" class="lblGeneral">Altura</p>
        <input type="text" id="txtAltura" name="txtAltura" DATASRC="#dsoSup01" DATAFLD="Altura" class="fldGeneral" title="Altura (mm)">
        <p id="lblProfundidade" name="lblProfundidade" class="lblGeneral">Profund</p>
        <input type="text" id="txtProfundidade" name="txtProfundidade" DATASRC="#dsoSup01" DATAFLD="Profundidade" class="fldGeneral" title="Profundidade (mm)">
        <p id="lblLarguraTolerancia" name="lblLarguraTolerancia" class="lblGeneral">Largura*</p>
        <input type="text" id="txtLarguraTolerancia" name="txtLarguraTolerancia" DATASRC="#dsoSup01" DATAFLD="LarguraTolerancia" class="fldGeneral" title="Toler�ncia na largura (mm)">
        <p id="lblAlturaTolerancia" name="lblAlturaTolerancia" class="lblGeneral">Altura*</p>
        <input type="text" id="txtAlturaTolerancia" name="txtAlturaTolerancia" DATASRC="#dsoSup01" DATAFLD="AlturaTolerancia" class="fldGeneral" title="Toler�ncia na altura (mm)">
        <p id="lblProfundidadeTolerancia" name="lblProfundidadeTolerancia" class="lblGeneral">Profund*</p>
        <input type="text" id="txtProfundidadeTolerancia" name="txtProfundidadeTolerancia" DATASRC="#dsoSup01" DATAFLD="ProfundidadeTolerancia" class="fldGeneral" title="Toler�ncia na profundidade (mm)">
        <p id="lblCubagem" name="lblCubagem" class="lblGeneral">Cubagem</p>
        <input type="text" id="txtCubagem" name="txtCubagem" DATASRC="#dsoSup01" DATAFLD="Cubagem" class="fldGeneral" title="Cubagem (m3)">
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral">
        <p id="lblDescricao" name="lblDescricao" class="lblGeneral">Descri��o</p>
        <input type="text" id="txtDescricao" name="txtDescricao" DATASRC="#dsoSup01" DATAFLD="Descricao" class="fldGeneral">
    </div>
</body>

</html>
