/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form tiposrel
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// Dados da listagem da pesquisa
var dsoListData01 = new CDatatransport("dsoListData01");
// Dados dos combos de contexto e filtros
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");

// Dados dos proprietarios para o pesqlist .URL
var dsoPropsPL = new CDatatransport("dsoPropsPL");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Localiza��o', 'C�digo', 'Loc Codificada', 'Localiza��o M�e', 'N�vel', 
		'LU', 'Tam M�n', 'Tam M�x', 'ES', 'Palete', 'RD', 'Produtos', 'Pal Adic', 'Observa��o');
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
    alignColsInGrid(fg,[0,6,8,9,13,14]);
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Localiza��es']);	
	setupEspecBtnsControlBar('sup', 'HHDH');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // Usuario clicou botao documentos
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
    else if (btnClicked == 2) 
    	openModalPrint();
    else if ( btnClicked == 4 ) 
    	openModalLocalizacoes();
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALLOCALIZACOESHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    // por questoes esteticas, coloca foco no grid de pesquisa
    if ( fg.disabled == false )
        fg.focus();
    
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modmateriais/submodestoques/localizacoes/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(440,373));
}

function openModalLocalizacoes()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 770;
    var nHeight = 440;

    var nA1 = getCurrRightValue('SUP','B2A1');

    var nA2 = getCurrRightValue('SUP','B2A2');

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nA1=' + escape(nA1);
    strPars += '&nA2=' + escape(nA2);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modmateriais/submodestoques/localizacoes/modalpages/modalLocalizacoes.asp' + strPars;
    
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}
