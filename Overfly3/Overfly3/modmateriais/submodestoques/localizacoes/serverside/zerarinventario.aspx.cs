using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modmateriais.submodestoques.localizacoesEx.serverside
{
	public partial class zerarinventario : System.Web.UI.OverflyPage
	{
		private static Integer ZERO = new Integer(0);
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + RelPesConLocalizacoesEstoqueAtualiza() + " as fldresp"
				)
			);
		}
		
		private Integer empresaID = ZERO;
		protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		protected int RelPesConLocalizacoesEstoqueAtualiza()
		{
			int result = DataInterfaceObj.ExecuteSQLCommand(
				"exec sp_RelPesConLocalizacoesEstoque_Atualiza " + empresaID + ", 2"
			);

			return result;
		}
	}
}
