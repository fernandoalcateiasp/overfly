using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modmateriais.submodestoques.localizacoesEx.serverside
{
	public partial class incluir : System.Web.UI.OverflyPage
	{
		private static Integer ZERO = new Integer(0);
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select '" + ProdutoLocalizacaoEstoqueIncluir() + "' as Resultado"
				)
			);
		}
		
		private Integer empresaID = ZERO;
		protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { if(value != null) empresaID = value; }
		}

		private Integer produtoID = ZERO;
		protected Integer nProdutoID
		{
			get { return produtoID; }
			set { if(value != null) produtoID = value; }
		}

		private Integer localizacaoID = ZERO;
		protected Integer nLocalizacaoID
		{
			get { return localizacaoID; }
			set { if(value != null) localizacaoID = value; }
		}

		protected string ProdutoLocalizacaoEstoqueIncluir()
        {
            string volta = "";

			ProcedureParameters[] procparam = new ProcedureParameters[4];

			procparam[0] = new ProcedureParameters("@EmpresaID", SqlDbType.Int, empresaID.ToString(), ParameterDirection.Input);
			procparam[1] = new ProcedureParameters("@ProdutoID", SqlDbType.Int, produtoID.ToString(), ParameterDirection.Input);
			procparam[2] = new ProcedureParameters("@LocalizacaoID", SqlDbType.Int, localizacaoID.ToString(), ParameterDirection.Input);
			procparam[3] = new ProcedureParameters("@Resultado", SqlDbType.VarChar, System.DBNull.Value, ParameterDirection.InputOutput);
            procparam[3].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure("sp_Produto_LocalizacaoEstoqueIncluir", procparam);
            

            if (procparam[3].Data != DBNull.Value)
                volta = procparam[3].Data.ToString();

            return volta;
		}
	}
}
