using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;

namespace Overfly3.modmateriais.submodestoques.localizacoesEx.serverside
{
	public partial class excluir : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			int fldresp = DataInterfaceObj.ExecuteSQLCommand(
					"DELETE RelacoesPesCon_LocalizacoesEstoque WHERE (RelPesConLocalizacaoID = " + relPesConLocalizacaoID + ") "
				);

			WriteResultXML(DataInterfaceObj.getRemoteData("select " + fldresp + " as fldresp"));
		}

		private Integer relPesConLocalizacaoID = new Integer(0);
		protected Integer nRelPesConLocalizacaoID
		{
			get { return relPesConLocalizacaoID; }
			set { if(value != null) relPesConLocalizacaoID = value; }
		}
	}
}
