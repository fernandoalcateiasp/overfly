using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;

namespace Overfly3.modmateriais.submodestoques.localizacoesEx.serverside
{
	public partial class gravarlocalizacoes : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
        {
            execUpdates();

			WriteResultXML(DataInterfaceObj.getRemoteData("select '" + fldresp + "' as fldresp"));
		}

		private int fldresp = 0;
		
		private void execUpdates()
		{
			string sql = "";
			
			for(int i = 0; i < nRelPesConLocalizacaoID.Length; i++)
			{
				if(nInventario[i].intValue() == 0)
				{
					if(nOrdem[i].Equals(""))
					{
						nOrdem[i] = " NULL ";
					}

					sql += "UPDATE RelacoesPesCon_LocalizacoesEstoque SET Ordem=" + nOrdem[i] + ", " +
							"LocalizacaoID=" + nLocalizacaoID[i] + " " +
						"WHERE (RelPesConLocalizacaoID = " + nRelPesConLocalizacaoID[i] + ") ";
				}
				else
				{
					if(nInventarioCaixa[i].Equals(""))
					{
						nInventarioCaixa[i] = " NULL ";
					}

					if(nInventarioUnidade[i].Equals(""))
					{
						nInventarioUnidade[i] = " NULL ";
					}

					sql += "UPDATE RelacoesPesCon_LocalizacoesEstoque SET InventarioCaixa=" + nInventarioCaixa[i] + ", " +
							"InventarioUnidade=" + nInventarioUnidade[i] + " " +
						"WHERE (RelPesConLocalizacaoID = " + nRelPesConLocalizacaoID[i] + ") ";
				}

				fldresp += DataInterfaceObj.ExecuteSQLCommand(sql);
			}
		}
		
		private Integer[] relPesConLocalizacaoID;
		protected Integer[] nRelPesConLocalizacaoID
		{
			get { return relPesConLocalizacaoID; }
			set { relPesConLocalizacaoID = value; }
		}

		private Integer[] inventario;
		protected Integer[] nInventario
		{
			get { return inventario; }
			set { inventario = value; }
		}

		private string[] ordem;
		protected string[] nOrdem
		{
			get { return ordem; }
			set { ordem = value; }
		}

		private Integer[] localizacaoID;
		protected Integer[] nLocalizacaoID
		{
			get { return localizacaoID; }
			set { localizacaoID = value; }
		}

		private string[] inventarioCaixa;
		protected string[] nInventarioCaixa
		{
			get { return inventarioCaixa; }
			set { inventarioCaixa = value; }
		}

		private string[] inventarioUnidade;
		protected string[] nInventarioUnidade
		{
			get { return inventarioUnidade; }
			set { inventarioUnidade = value; }
		}
	}
}
