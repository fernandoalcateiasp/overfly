﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_localizacoes : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        private string nEmpresaLogadaIdioma = Convert.ToString(HttpContext.Current.Request.Params["nEmpresaLogadaIdioma"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string nDepositoID = Convert.ToString(HttpContext.Current.Request.Params["nDepositoID"]);
        private string chkProduto = Convert.ToString(HttpContext.Current.Request.Params["chkProduto"]);
        private string chkDiferenca = Convert.ToString(HttpContext.Current.Request.Params["chkDiferenca"]);
        private string sData = Convert.ToString(HttpContext.Current.Request.Params["sData"]);
        private string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);
        private string DataHora = Convert.ToString(HttpContext.Current.Request.Params["DataHora"]);
        int Datateste = 0;
        bool nulo = false;

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID) {
                    case "40129":
                        relatorioInventarioEstoque();
                        break;

                    case "40127":
                        localizacaoEstoque();
                        break;

                    case "40128":
                        relatorioInconsistencia();
                        break;
                }
            }
        }

        public void relatorioInventarioEstoque()
        {
            string Title = "Relatório de Inventário";

            string strSQL = "EXEC sp_Inventario " + nEmpresaID + ", " + nDepositoID + ", " + chkProduto + "," +
                            chkDiferenca + ", " + sData + ", " + sFiltro + ", 1";

            var Header_Body = (formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (formato == 1)
            {
                double PosYHeader = 0;

                Relatorio.CriarRelatório("Inventário", arrSqlQuery, "BRA", "Landscape", "1.5", "Default", "Default", true);

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {

                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, "0.2", PosYHeader.ToString(), "12", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Inventário", "13.5", PosYHeader.ToString(), "12", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabelData("Now", "", "", "23", PosYHeader.ToString(), "11", "black", "B", "Header", "3.72187", "Horas");

                    Relatorio.CriarObjLabel("Fixo", "", "Data:", "0.2", (PosYHeader += 0.5).ToString(), "9", "Black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", DataHora, "1.5", PosYHeader.ToString(), "9", "Black", "B", "Header", "4", "");

                    Relatorio.CriarObjLabel("Fixo", "", "ID", "0.2", (PosYHeader += 0.5).ToString(), "9", "Black", "B", "Header", "1", "Left");
                    Relatorio.CriarObjLabel("Fixo", "", "Produto", "1.2", PosYHeader.ToString(), "9", "Black", "B", "Header", "4.5", "Left");
                    Relatorio.CriarObjLabel("Fixo", "", "Modelo", "5.7", PosYHeader.ToString(), "9", "Black", "B", "Header", "4.5", "Left");
                    Relatorio.CriarObjLabel("Fixo", "", "Localizações", "10.2", PosYHeader.ToString(), "9", "Black", "B", "Header", "10", "Left");
                    Relatorio.CriarObjLabel("Fixo", "", "Qtd/Cx", "20.2", PosYHeader.ToString(), "9", "Black", "B", "Header", "1.3", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Inv Cx", "21.5", PosYHeader.ToString(), "9", "Black", "B", "Header", "1.2", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Inv Un", "22.7", PosYHeader.ToString(), "9", "Black", "B", "Header", "1.2", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Inv", "23.9", PosYHeader.ToString(), "9", "Black", "B", "Header", "1", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Est", "24.9", PosYHeader.ToString(), "9", "Black", "B", "Header", "1", "Right");
                    if (chkDiferenca == "1")
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "Dif", "25.9", PosYHeader.ToString(), "9", "Black", "B", "Header", "1", "Right");
                    }

                    Relatorio.CriarNumeroPagina("27", "0", "10", "Black", "B", false);

                    Relatorio.CriarObjLinha("0.2", "1", "29", "", "Header");

                    Relatorio.CriarObjTabela("0.2", "0", arrIndexKey, false, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "ProdutoID", false, "1", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Total", "TotalGroup", "", "DetailGroup", 4, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Produto", false, "4.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Modelo", false, "4.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Localizacoes", false, "10", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Localizacoes", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "QuantidadePorCaixa", false, "1.3", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadePorCaixa", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadePorCaixa", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "TotalInventarioCaixa", false, "1.2", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalInventarioCaixa", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalInventarioCaixa", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "TotalInventarioUnidade", false, "1.2", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalInventarioUnidade", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalInventarioUnidade", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "TotalInventarioQuantidade", false, "1", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalInventarioQuantidade", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalInventarioQuantidade", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Estoque", false, "1", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Estoque", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "Estoque", "TotalGroup", "", "DetailGroup", 0, true);

                    if (chkDiferenca == "1")
                    {
                        Relatorio.CriarObjColunaNaTabela(" ", "DiferencaInventario", false, "1", "Right", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "DiferencaInventario", "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Query", "DiferencaInventario", "TotalGroup", "", "DetailGroup", 0, true);
                    }

                    Relatorio.TabelaEnd();

                }

            }

            else if (formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {

                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                    Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.92187", "Horas");

                    //Cria Tabela           
                    Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                    Relatorio.TabelaEnd();
                }
            }

            Relatorio.CriarPDF_Excel(Title, formato);
        }

        public void localizacaoEstoque()
        {
            string txtFiltro2 = Convert.ToString(HttpContext.Current.Request.Params["txtFiltro2"]);
            string txtInicio = Convert.ToString(HttpContext.Current.Request.Params["txtInicio"]);
            string txtFim = Convert.ToString(HttpContext.Current.Request.Params["txtFim"]);
            string sFiltroFamilia = Convert.ToString(HttpContext.Current.Request.Params["sFiltroFamilia"]);
            string sFiltroMarca = Convert.ToString(HttpContext.Current.Request.Params["sFiltroMarca"]);
            int chkReposicaoDiaria = Convert.ToInt32(HttpContext.Current.Request.Params["chkReposicaoDiaria"]);
            int chkProdutosLocalizacao = Convert.ToInt32(HttpContext.Current.Request.Params["chkProdutosLocalizacao"]);
            string selOrdemRelLoc = Convert.ToString(HttpContext.Current.Request.Params["selOrdemRelLoc"]);
            int chkEstoque = Convert.ToInt32(HttpContext.Current.Request.Params["chkEstoque"]);
            int iQLinhas = 0;
            
            string sFiltro = ((txtFiltro2) == "null" ? "" : " AND " + txtFiltro2 + " ");

            string sInicio = txtInicio;
            sInicio = (sInicio != "null" ? " AND dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) >= " + "'" + sInicio + "'" : "");

            string sFim = txtFim;
            sFim = (sFim != "null" ? " AND dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) <= " + "'" + sFim + "'" : "");

            string sFiltroRD = "";
            if (chkReposicaoDiaria == 1)
                sFiltroRD = " AND b.ReposicaoDiaria=1 ";

            if (txtFiltro2 == "null")
                txtFiltro2 = " ";

            if (txtInicio == "null")
                txtInicio = " ";

            if (txtFim == "null")
                txtFim = " ";

            if (sFiltroFamilia == "null")
                sFiltroFamilia = " ";

            if (sFiltroMarca == "null")
                sFiltroMarca = " ";



            string strMasterQuerySQL = " SELECT " +
                                            " 1 AS Indice, a.LocalizacaoID AS LocalizacaoID_, dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) AS Localizacao, " +
                                            " (CASE b.ReposicaoDiaria WHEN 1 THEN CHAR(88) ELSE SPACE(0) END) AS RD, " +
                                            " '' AS 'Produto', '' AS ProdutoID,  '' AS 'Modelo', SPACE(0) AS Linha_, SPACE(0) AS Linha2_, NULL AS Ordem,  '' AS Localizacoes, COUNT(*) AS Qnt," + //ATENÇÃO
                                            " SUM(dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, NULL, NULL, NULL, 0, 1)) AS QuantidadeCaixasTotals, " +
                                            " NULL AS QuantidadeCaixasTotal, " +
                                            " SUM(dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, 1, NULL, NULL, 0, 1)) AS QuantidadeCaixasFilas, " +
                                            " NULL AS QuantidadeCaixasFila, " +
                                            " SUM(dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, 1, NULL, 1, 0, 1)) AS QuantidadeCaixasPilhas, " +
                                            " NULL AS QuantidadeCaixasPilha " +
                                        " FROM " +
                                            " RelacoesPesCon_LocalizacoesEstoque a WITH(NOLOCK) " +
                                                " INNER JOIN LocalizacoesEstoque b WITH(NOLOCK) ON(b.LocalizacaoID = a.LocalizacaoID) " +
                                                " INNER JOIN RelacoesPesCon c WITH(NOLOCK) ON(c.RelacaoID = a.RelacaoID) " +
                                                " INNER JOIN Conceitos d WITH(NOLOCK) ON(d.ConceitoID = c.ObjetoID) " +
                                                " INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON(e.SujeitoID = b.EmpresaID) " +
                                        " WHERE " +
                                            " e.TipoRelacaoID = 28 AND b.EmpresaID = " +
                                                nEmpresaID + sFiltro + sFiltroFamilia + sFiltroMarca + sInicio + sFim + sFiltroRD +
                                        " GROUP BY a.LocalizacaoID, b.ReposicaoDiaria ";

            if (chkProdutosLocalizacao == 0)
            {
                strMasterQuerySQL = strMasterQuerySQL +
                    " UNION ALL " +
                        " SELECT " +
                            " 2 AS Indice, 0 AS LocalizacaoID_, 'Sem Localização' AS Localizacao, SPACE(0) AS RD, " +
                            " '' AS 'Produto', '' AS ProdutoID,  '' AS 'Modelo', SPACE(0) AS Linha_, SPACE(0) AS Linha2_, NULL AS Ordem,  '' AS Localizacoes, COUNT(*) AS Qnt," + //ATENÇÃO
                            " NULL AS QuantidadeCaixasTotals, NULL AS QuantidadeCaixasFilas, NULL AS QuantidadeCaixasPilhas, " +
                            " NULL AS QuantidadeCaixasTotal, NULL AS QuantidadeCaixasFila, NULL AS QuantidadeCaixasPilha " +
                        " FROM " +
                            " RelacoesPesCon c WITH(NOLOCK) " +
                        " WHERE " +
                                " c.SujeitoID = " + nEmpresaID +
                            " AND c.TipoRelacaoID = 61 " +
                            " AND dbo.fn_Produto_Estoque(c.SujeitoID, c.ObjetoID, 341, NULL, NULL, NULL, 375, -1) > 0 " +
                            " AND(SELECT COUNT(*) FROM RelacoesPesCon_LocalizacoesEstoque aa WHERE(aa.RelacaoID = c.RelacaoID)) = 0 " +
                        " GROUP BY c.SujeitoID ";
            }

            strMasterQuerySQL = strMasterQuerySQL + "ORDER BY Indice, dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1)";


            string sOrderBy = "";
            if (selOrdemRelLoc == "ID")
                sOrderBy = " ORDER BY d.ConceitoID";
            else if (selOrdemRelLoc == "PRODUTO")
                sOrderBy = " ORDER BY d.Conceito, d.ConceitoID";
            else if (selOrdemRelLoc == "MODELO")
                sOrderBy = " ORDER BY d.Modelo, d.Conceito, d.ConceitoID";


            string strDetailQuerySQL01 = " SELECT " +
                                            " a.LocalizacaoID AS LocalizacaoID_, d.ConceitoID AS ProdutoID, d.Conceito AS Produto, d.Modelo AS Modelo, " +
                                            " NULL AS QuantidadeCaixasTotals, " +
                                            " dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, NULL, NULL, NULL, 0, 1) AS QuantidadeCaixasTotal, " +
                                            " NULL AS QuantidadeCaixasFilas, " +
                                            " dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, 1, NULL, NULL, 0, 1) AS QuantidadeCaixasFila, " +
                                            " NULL AS QuantidadeCaixasPilhas, " +
                                            " dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, 1, NULL, 1, 0, 1) AS QuantidadeCaixasPilha, " +
                                            " a.Ordem AS Ordem, dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) AS Localizacoes, " +
                                            " dbo.fn_Produto_Estoque(c.SujeitoID, c.ObjetoID, 341, e.ObjetoID, NULL, NULL, 375, -1) AS Estoque, " +
                                            " dbo.fn_Pad(SPACE(0), 6, CHAR(95), 'L') AS [Linha_], " +
                                            " dbo.fn_Pad(SPACE(0), 6, CHAR(95), 'L') AS [Linha2_], " +
                                            " '' AS 'Localizacao', '' AS 'RD', NULL AS 'Qnt'" + //ATENÇÃO
                                        " FROM " +
                                            " RelacoesPesCon_LocalizacoesEstoque a WITH(NOLOCK) " +
                                                " INNER JOIN LocalizacoesEstoque b WITH(NOLOCK) ON(b.LocalizacaoID = a.LocalizacaoID) " +
                                                " INNER JOIN RelacoesPesCon c WITH(NOLOCK) ON(c.RelacaoID = a.RelacaoID) " +
                                                " INNER JOIN Conceitos d WITH(NOLOCK) ON(d.ConceitoID = c.ObjetoID) " +
                                                " INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON(e.SujeitoID = b.EmpresaID) " +
                                        " WHERE " +
                                            " e.TipoRelacaoID = 28 AND b.EmpresaID = " + nEmpresaID + " AND e.EstadoID = 2 " +
                                            sFiltro + sFiltroFamilia + sFiltroMarca + sInicio + sFim;

            if (chkProdutosLocalizacao == 0)
            {
                strDetailQuerySQL01 = strDetailQuerySQL01 +
                    " UNION ALL " +
                        " SELECT " +
                            " 0 AS LocalizacaoID_, d.ConceitoID AS ProdutoID, d.Conceito AS Produto, d.Modelo AS Modelo, " +
                            " NULL AS QuantidadeCaixasTotals, NULL AS QuantidadeCaixasTotal, NULL AS QuantidadeCaixasFilas, NULL AS QuantidadeCaixasFila, NULL AS QuantidadeCaixasPilhas," +
                            " NULL AS QuantidadeCaixasPilha, NULL AS Ordem, NULL AS Localizacoes, dbo.fn_Produto_Estoque(c.SujeitoID, c.ObjetoID, 341, NULL, NULL, NULL, 375, -1) AS Estoque, " +
                            " dbo.fn_Pad(SPACE(0), 6, CHAR(95), 'L') AS [Linha_], " +
                            " dbo.fn_Pad(SPACE(0), 6, CHAR(95), 'L') AS [Linha2_], " +
                            " '' AS 'Localizacao', '' AS 'RD', NULL AS 'Qnt'" + //ATENÇÃO
                        " FROM " +
                            " RelacoesPesCon c WITH(NOLOCK) " +
                                " INNER JOIN Conceitos d WITH(NOLOCK) ON(d.ConceitoID = c.ObjetoID) " +
                        " WHERE " +
                                " c.SujeitoID = " + nEmpresaID +
                            " AND c.TipoRelacaoID = 61 AND dbo.fn_Produto_Estoque(c.SujeitoID, c.ObjetoID, 341, NULL, NULL, NULL, 375, -1) > 0 " +
                            " AND(SELECT COUNT(*) FROM RelacoesPesCon_LocalizacoesEstoque aa WHERE(aa.RelacaoID = c.RelacaoID)) = 0 " +
                            sFiltro + sFiltroFamilia + sFiltroMarca;
            }

            strDetailQuerySQL01 = strDetailQuerySQL01 + sOrderBy;



            string Title = "Localização de Estoque";

            var Header_Body = (formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "strMasterQuerySQL", strMasterQuerySQL },
                                          { "strDetailQuerySQL01", strDetailQuerySQL01 }};

            string[,] arrIndexKey = new string[,] { { "strDetailQuerySQL01", "LocalizacaoID_", "LocalizacaoID_" } };

            if (formato == 1)
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "1.2", "Default", "Default", true);
            }
            else
            {
                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape");
            }

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            iQLinhas = dsFin.Tables["strMasterQuerySQL"].Rows.Count;
            

            if (iQLinhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {                
                if (formato == 1) // Se PDF
                {

                    double headerX = 0.2;
                    double headerY = 0.2;
                    double tabelaTitulo = 0.8;

                    double tabelaX = 0.2;
                    double tabelaY = 0;

                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 12).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "5", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 23).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");

                    Relatorio.CriarNumeroPagina((headerX + 27).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                    //Linha
                    Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.5).ToString(), "29", "", "Header");

                    //Cabeçalho da tabela
                    Relatorio.CriarObjLabel("Fixo", "", "Localização", (headerX).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "2", "");
                    Relatorio.CriarObjLabel("Fixo", "", "RD", (headerX + 2.5).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "0.78", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX + 4.43).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "0.91", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Produto", (headerX + 6.55).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "1.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Modelo", (headerX + 12.55).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "1.5", "");

                    if (chkEstoque == 1)
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "Est", (headerX + 17).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "3.5", "");

                        headerX = 17.5;
                    }
                    else
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "CX", (headerX + 17).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "3.5", "");
                        Relatorio.CriarObjLabel("Fixo", "", "UN", (headerX + 18.6).ToString(), (tabelaTitulo).ToString(), "10", "black", "B", "Header", "1.42", "");

                        headerX = 18.5;
                    }

                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), arrIndexKey, false, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Localizacao", false, "2.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Localizacao", "DetailGroup","Null","Null",0,false,"10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Total Geral", "TotalGroup", "", "DetailGroup", 1, true, "10");

                    Relatorio.CriarObjColunaNaTabela(" ", "RD", false, "1.25", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "RD", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "ProdutoID", false, "1.2", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Qnt", false, "1.6", "Left", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Qnt", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Query", "Qnt", "TotalGroup", "", "DetailGroup", 0, true , "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Produto", false, "6", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Modelo", false, "4.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup", "Null", "Null", 0, false, "10");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    if (chkEstoque == 1)
                    {
                        Relatorio.CriarObjColunaNaTabela(" ", "Estoque", false, "1.5", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Estoque", "DetailGroup", "Null", "Null", 0, false, "10");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);
                    }
                    else
	                {
                        Relatorio.CriarObjColunaNaTabela(" ", "Linha_", false, "1.5", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Linha_", "DetailGroup", "Null", "Null", 0, false, "10");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                        Relatorio.CriarObjColunaNaTabela(" ", "Linha2_", false, "1.5", "Default", "1", "0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Linha2_", "DetailGroup", "Null", "Null", 0, false, "10");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);
                    }
                    
                    Relatorio.TabelaEnd();

                }

                else if (formato == 2) // Se Excel
                {

                    double headerX = 0;
                    double headerY = 0;

                    double tabelaX = 0;
                    double tabelaY = 0.3;

                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "8", "black", "B", "Body", "3", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 4.25).ToString(), headerY.ToString(), "8", "#0113a0", "B", "Body", "4", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 13.25).ToString(), headerY.ToString(), "8", "black", "B", "Body", "4", "Horas");


                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), arrIndexKey, false, true);
                    
                    Relatorio.CriarObjColunaNaTabela("Localização", "Localizacao", false, "3", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Localizacao", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "Total Geral", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela("RD", "RD", false, "1.25", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "RD", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela("ID", "ProdutoID", false, "1.05", "Right", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "Qnt", false, "1.05", "Left", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Qnt", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "Qnt", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Produto", "Produto", false, "5", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela("Modelo", "Modelo", false, "4", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    if (chkEstoque == 1)
                    {
                        Relatorio.CriarObjColunaNaTabela("Est", "Estoque", false, "1.1", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Estoque", "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);
                    }
                    else
                    {
                        Relatorio.CriarObjColunaNaTabela("CX", "Linha_", false, "1.25", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Linha_", "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                        Relatorio.CriarObjColunaNaTabela("UN", "Linha2_", false, "1.4", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Linha2_", "DetailGroup");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);
                    }

                    Relatorio.CriarObjColunaNaTabela("Cx Tot", "QuantidadeCaixasTotal", false, "2", "Right", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasTotal", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "QuantidadeCaixasTotals", false, "1.1", "Left", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasTotals", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasTotals", "TotalGroup", "", "DetailGroup", 0, true);


                    Relatorio.CriarObjColunaNaTabela("Cx Fila", "QuantidadeCaixasFila", false, "2", "Right", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasFila", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "QuantidadeCaixasFilas", false, "1.15", "Left", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasFilas", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasFilas", "TotalGroup", "", "DetailGroup", 0, true);


                    Relatorio.CriarObjColunaNaTabela("Cx Pilha", "QuantidadeCaixasPilha", false, "2", "Right", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasPilha", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela(" ", "QuantidadeCaixasPilhas", false, "1.3", "Left", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasPilhas", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeCaixasPilhas", "TotalGroup", "", "DetailGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Ord", "Ordem", false, "0.8", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Ordem", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);

                    Relatorio.CriarObjColunaNaTabela("Localizacoes", "Localizacoes", false, "3", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Localizacoes", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 1, true);


                    Relatorio.TabelaEnd();
                }

                Relatorio.CriarPDF_Excel(Title, formato);
            }
        }

        public void relatorioInconsistencia()
        {
            int chkInconsistencia1 = Convert.ToInt32(HttpContext.Current.Request.Params["chkInconsistencia1"]);
            int chkInconsistencia2 = Convert.ToInt32(HttpContext.Current.Request.Params["chkInconsistencia2"]);
            int chkInconsistencia3 = Convert.ToInt32(HttpContext.Current.Request.Params["chkInconsistencia3"]);
            int chkInconsistencia4 = Convert.ToInt32(HttpContext.Current.Request.Params["chkInconsistencia4"]);
            int chkInconsistencia5 = Convert.ToInt32(HttpContext.Current.Request.Params["chkInconsistencia5"]);
            string sInformation    = Convert.ToString(HttpContext.Current.Request.Params["sInformation"]);


            string Title = "Inconsistência de Localização";


            string strSQL = "EXEC sp_LocalizacoesEstoque_Produtos_Inconsistencias " + nEmpresaID + ", " + chkInconsistencia1 + ", " + chkInconsistencia2 + ", " + chkInconsistencia3 + ", " +
                            chkInconsistencia4 + ", " + chkInconsistencia5;


            var Header_Body = (formato == 1 ? "Header" : "Body");


            arrSqlQuery = new string[,] { { "strSQL", strSQL } };


            Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Landscape", "1.5", "Default", "Default", true);


            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;

            int linhas = dsFin.Tables["strSQL"].Rows.Count;

            if (linhas == 0)
            {
                nulo = true;
            }

            if (nulo)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                HttpContext.Current.Response.End();
                return;
            }
            else
            {
                if (formato == 1) //SE PDF
                {
                    double headerX = 0.2;
                    double headerY = 0.2;
                    double tabelaTitulo = 1.2;

                    double tabelaX = 0.2;
                    double tabelaY = 0;

                    double bodyX = 0.2;
                    double bodyY = 0.0;

                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "12", "black", "B", "Header", "5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 10).ToString(), headerY.ToString(), "12", "#0113a0", "B", "Header", "9", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 24).ToString(), headerY.ToString(), "10", "black", "B", "Header", "3.72187", "Horas");
                    Relatorio.CriarNumeroPagina((headerX + 27).ToString(), (headerY).ToString(), "10", "Black", "B", false);

                    //Header Parametros
                    Relatorio.CriarObjLabel("Fixo", "", sInformation, (headerX).ToString(), (headerY + 0.5).ToString(), "8", "black", "B", "Header", "29", "");

                    Relatorio.CriarObjLinha((headerX).ToString(), (headerY + 0.9).ToString(), "29", "", "Header");
                    
                    //Header titulo da tabela
                    Relatorio.CriarObjLabel("Fixo", "", "Local", (headerX).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Tam", (headerX + 1.85).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ES", (headerX + 3.1).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "PD", (headerX + 4.36).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Família", (headerX + 4.9).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ID", (headerX + 8.99).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "E", (headerX + 9.69).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Produto", (headerX + 10.3).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Tam", (headerX + 16.06).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "ES", (headerX + 16.99).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Est", (headerX + 18.6).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Localizações", (headerX + 19.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Inconsistência", (headerX + 25.2).ToString(), (tabelaTitulo).ToString(), "8", "black", "B", "Header", "15", "");
                    
                    //Tabela
                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "strSQL", false, false, false);

                    Relatorio.CriarObjColunaNaTabela(" ", "Localizacao", false, "1.3", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Localizacao", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "LocalizacaoTamanhoMinimo", false, "0.9", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "LocalizacaoTamanhoMinimo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "LocalizacaoTamanhoMaximo", false, "0.9", "Left", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "LocalizacaoTamanhoMaximo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "LocalizacaoEstoqueSeguro_", false, "0.9", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "LocalizacaoEstoqueSeguro_", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "LocalizacaoProdutosDistintos", false, "0.9", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "LocalizacaoProdutosDistintos", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Familia", false, "3", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Familia", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "ProdutoID", false, "1.5", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Estado", false, "0.9", "Center", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "Produto", false, "5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "ProdutoTamanho", false, "1.5", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoTamanho", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "ProdutoEstoqueSeguro_", false, "0.9", "Center", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoEstoqueSeguro_", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "ProdutoEstoque", false, "1.5", "Right", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoEstoque", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "ProdutoLocalizacoes", false, "6", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoLocalizacoes", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela(" ", "inconsistencia", false, "3.5", "Default", "1", "0");
                    Relatorio.CriarObjCelulaInColuna("Query", "inconsistencia", "DetailGroup", "Null", "Null", 0, false, "8");
                    
                    Relatorio.TabelaEnd();

                    //Separador totais
                    Relatorio.CriarObjLinha((bodyX).ToString(), (bodyY + 0.5).ToString(), "29", "", "Body");

                    Relatorio.CriarObjLabel("Fixo", "", "Total de Registros: " + linhas, (bodyX).ToString(), (bodyY + 0.8).ToString(), "8", "black", "B", "Body", "15", "");

                }
                else if (formato == 2) // Se Excel
                {
                    double headerX = 0;
                    double headerY = 0;

                    double tabelaX = 0;
                    double tabelaY = 0.3;

                    //Header
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, (headerX).ToString(), headerY.ToString(), "8", "black", "B", "Body", "3.5", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, (headerX + 8.5).ToString(), headerY.ToString(), "8", "#0113a0", "B", "Body", "5", "");
                    Relatorio.CriarObjLabelData("Now", "", "", (headerX + 18.5).ToString(), headerY.ToString(), "8", "black", "B", "Body", "5", "Horas");

                    Relatorio.CriarObjTabela(tabelaX.ToString(), tabelaY.ToString(), "strSQL", false, false, false);

                    Relatorio.CriarObjColunaNaTabela("Localizacao", "Localizacao", false, "3.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Localizacao", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("LocalizacaoTamanhoMinimo", "LocalizacaoTamanhoMinimo", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "LocalizacaoTamanhoMinimo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("LocalizacaoTamanhoMaximo", "LocalizacaoTamanhoMaximo", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "LocalizacaoTamanhoMaximo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("LocalizacaoEstoqueSeguro", "LocalizacaoEstoqueSeguro", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "LocalizacaoEstoqueSeguro", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("LocalizacaoProdutosDistintos", "LocalizacaoProdutosDistintos", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "LocalizacaoProdutosDistintos", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Familia", "Familia", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Familia", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("ProdutoID", "ProdutoID", false, "2.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoID", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Estado", "Estado", false, "2", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Produto", "Produto", false, "5.5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Modelo", "Modelo", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("ProdutoTamanho", "ProdutoTamanho", false, "2.7", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoTamanho", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("ProdutoEstoqueSeguro", "ProdutoEstoqueSeguro", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoEstoqueSeguro", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("ProdutoEstoque", "ProdutoEstoque", false, "2.6", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoEstoque", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("ProdutoLocalizacoes", "ProdutoLocalizacoes", false, "3.3", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "ProdutoLocalizacoes", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("inconsistencia", "inconsistencia", false, "5", "Default");
                    Relatorio.CriarObjCelulaInColuna("Query", "inconsistencia", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.TabelaEnd();
                }
            }
            Relatorio.CriarPDF_Excel(Title, formato);
        }
    }
}
