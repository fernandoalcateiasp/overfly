using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class insertimage : System.Web.UI.OverflyPage
    {
		private string formId = "0";
		private string subFormId = "0";
		private string registroId = "0";
        private string tamanho = "0";
        private string ordem = "1";
        private string versao = "";

		public string nFormID
		{
			set { formId = value; }
		}
		public string nSubFormID
		{
			set { subFormId = value; }
		}
		public string nRegistroID
		{
			set { registroId = value; }
		}
        public string sVersao
        {
            set 
            { 
                versao = value;
                
                if (versao == "")
                    versao = "NULL";
                else
                    versao = "'" + versao + "'";
            }
        }
		public string nTamanho
		{
			set
			{
				tamanho = value;

				if (tamanho == "0")
				{
					// Documentos, QAF, RET
					if (formId == "7110" || formId == "7180" || formId == "12220")
					{
						tamanho = "4";
					}
					else
					{
						tamanho = "2";
					}
				}
			}
		}
		public string nOrdem
		{
			set { ordem = value != null ? value : "1"; }
		}

        private void insertImagem()
        {
            string sql = string.Empty;

            sql = "INSERT INTO Imagens (FormID, SubFormID, RegistroID, Tamanho, Ordem, Imagem, Versao)" +
                    "SELECT " + formId.ToString() + ", " + subFormId.ToString() + ", " + registroId.ToString() + ", " + tamanho.ToString() + ", " + ordem.ToString() + ", NULL," + versao.ToString();

            DataInterfaceObj.ExecuteSQLCommand(sql);
        }

        protected override void PageLoad(object sender, EventArgs e)
		{
            string strVercao = "a.Versao = " + versao + " AND ";

            if (versao == "NULL")
                strVercao = "a.Versao IS NULL AND ";
            
            DataSet result = DataInterfaceObj.getRemoteData(
                "SELECT TOP 1 * " +
                    "FROM Imagens a WITH(NOLOCK) " +
                    "WHERE a.FormID = " + formId.ToString() + " AND " +
                        "a.SubFormID = " + subFormId.ToString() + " AND " +
                        "a.RegistroID = " + registroId.ToString() + " AND " + strVercao +
                        "a.Tamanho = " + tamanho.ToString() + " AND " +
                        "a.Ordem = " + ordem.ToString() + " " +
                        "ORDER BY a.Tamanho, a.Ordem, a.ImagemID"
            );

            if (result.Tables[1].Rows.Count <= 0)
                insertImagem();

            WriteResultXML(DataInterfaceObj.getRemoteData("select NULL as Resultado"));
        }
    }
}
