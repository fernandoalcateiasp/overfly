using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class listaprecosdata : System.Web.UI.OverflyPage
    {
		private Integer clienteId;
		private Integer empresaId;

		public Integer nClienteID
		{
			set { clienteId = value; }
		}
		public Integer nEmpresaID
		{
			set { empresaId = value; }
		}
		
        protected override void PageLoad(object sender, EventArgs e)
        {
			// Gera o resultado.
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT TOP 1 Pessoas.PessoaID, Pessoas.Fantasia, Enderecos.UFID, 1 AS Parceiro, " +
					"dbo.fn_Empresa_Sistema(Pessoas.PessoaID) AS EmpresaSistema, " +
					"RelPessoas.ListaPreco AS ListaPreco, dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL) AS PMP, " +
					"c.NumeroParcelas AS NumeroParcelas, " +
					"dbo.fn_RelacoesPessoas_FinanciamentoPadrao(Pessoas.PessoaID, RelPessoas.ObjetoID, 1) AS FinanciamentoPadrao, " +
					"ISNULL(dbo.fn_RelPessoa_Classificacao(" + empresaId + ", " + clienteId + ", 0, GETDATE(),1), SPACE(0)) AS ClassificacaoInterna," +
					"ISNULL(dbo.fn_RelPessoa_Classificacao(" + empresaId + ", " + clienteId + ", 1, GETDATE(),1), SPACE(0)) AS ClassificacaoExterna " +
					"FROM Pessoas Empresas WITH(NOLOCK) " +
					"INNER JOIN RelacoesPesRec EmpresasSistema WITH(NOLOCK) ON (Empresas.PessoaID = EmpresasSistema.SujeitoID) " +
					"INNER JOIN RelacoesPessoas RelPessoas WITH(NOLOCK) ON (RelPessoas.ObjetoID = Empresas.PessoaID) " +
					"INNER JOIN Pessoas WITH(NOLOCK) ON (Pessoas.PessoaID = RelPessoas.SujeitoID) " +
                    "INNER JOIN Pessoas_Creditos PesCredito WITH(NOLOCK) ON (Pessoas.PessoaID = PesCredito.PessoaID) " +
                    "LEFT OUTER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID AND Enderecos.EndFaturamento = 1 AND Enderecos.Ordem = 1) " +
					"INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON (ISNULL(PesCredito.FinanciamentoLimiteID, 2) = c.FinanciamentoID) " +
					"WHERE (Empresas.PessoaID = " + empresaId + " AND PesCredito.PaisID = Enderecos.PaisID AND " +
						"EmpresasSistema.TipoRelacaoID = 12 AND EmpresasSistema.ObjetoID = 999 AND " +
						"Pessoas.PessoaID = " + clienteId + " AND Pessoas.EstadoID = 2 AND " +
						"RelPessoas.EstadoID = 2 AND RelPessoas.TipoRelacaoID = 21)"
				)
			);
        }
    }
}
