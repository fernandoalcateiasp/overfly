using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
	public partial class gravaragendamento : System.Web.UI.OverflyPage
	{
        private string[] pessoaId;
        private string[] status;
        private string[] empresaId;
        private string[] usuarioId;
        private string[] servicoAtendimentoID;
        private string[] tipoAtendimentoId;
        private string[] meioAtendimentoId;
        private string[] atendimentoOk;
        private string[] observacoes;
        private string[] produtos;
        private string[] dtAgendamento;
        private string[] pesAtendimentoId;
        private string[] erro;

        private IFormatProvider formatProvider =
            new System.Globalization.CultureInfo("en-US", true);

        protected string[] nPessoaID
        {
            set
            {
                pessoaId = value;

                for (int i = 0; i < pessoaId.Length; i++)
                {
                    if (pessoaId[i] == null || pessoaId[i].Length == 0)
                        pessoaId[i] = "";
                }
            }
        }

        protected string[] nStatus
        {
            set
            {
                status = value;

                for (int i = 0; i < status.Length; i++)
                {
                    if (status[i] == null || status[i].Length == 0)
                        status[i] = "";
                }
            }
        }

        protected string[] nEmpresaID
        {
            set
            {
                empresaId = value;

                for (int i = 0; i < empresaId.Length; i++)
                {
                    if (empresaId[i] == null || empresaId[i].Length == 0)
                        empresaId[i] = "";
                }
            }
        }

        protected string[] nUserID
        {
            set
            {
                usuarioId = value;

                for (int i = 0; i < usuarioId.Length; i++)
                {
                    if (usuarioId[i] == null || usuarioId[i].Length == 0)
                        usuarioId[i] = "";
                }
            }
        }

        protected string[] nServicoAtendimentoID
        {
            set
            {
                servicoAtendimentoID = value;

                for (int i = 0; i < servicoAtendimentoID.Length; i++)
                {
                    if (servicoAtendimentoID[i] == null || servicoAtendimentoID[i].Length == 0)
                        servicoAtendimentoID[i] = "";
                }
            }
        }

        protected string[] nTipoAtendimentoID
        {
            set
            {
                tipoAtendimentoId = value;

                for (int i = 0; i < tipoAtendimentoId.Length; i++)
                {
                    if (tipoAtendimentoId[i] == null || tipoAtendimentoId[i].Length == 0)
                        tipoAtendimentoId[i] = "";
                }
            }
        }

        protected string[] nMeioAtendimentoID
        {
            set
            {
                meioAtendimentoId = value;

                if (meioAtendimentoId.Length > 0) {
                    for (int i = 0; i < meioAtendimentoId.Length; i++)
                    {
                        if (meioAtendimentoId[i] == null || meioAtendimentoId[i].Length == 0)
                            meioAtendimentoId[i] = "";
                    }
                }
                else
                    meioAtendimentoId.SetValue("", 0);
            }
        }

        protected string[] sAtendimentoOK
        {
            set
            {
                atendimentoOk = value;

                for (int i = 0; i < atendimentoOk.Length; i++)
                {
                    if (atendimentoOk[i] == null || atendimentoOk[i].Length == 0)
                        atendimentoOk[i] = "0";
                }
            }
        }

        protected string[] sObservacoes
        {
            set
            {
                observacoes = value;

                for (int i = 0; i < observacoes.Length; i++)
                {
                    if (observacoes[i] == null || observacoes[i].Length == 0)
                        observacoes[i] = "";
                }
            }
        }

        protected string[] sProdutos
        {
            set
            {
                produtos = value;

                for (int i = 0; i < produtos.Length; i++)
                {
                    if (produtos[i] == null || produtos[i].Length == 0)
                        produtos[i] = "";
                }
            }
        }

        protected string[] sDtAgendamento
        {
            set
            {
                dtAgendamento = value;

                for (int i = 0; i < dtAgendamento.Length; i++)
                {
                    if (dtAgendamento[i] == null || dtAgendamento[i].Length == 0)
                        dtAgendamento[i] = "";
                }
            }
        }

        protected string[] nPesAtendimentoID
        {
            set
            {
                pesAtendimentoId = value;

                for (int i = 0; i < pesAtendimentoId.Length; i++)
                {
                    if (pesAtendimentoId[i] == null || pesAtendimentoId[i].Length == 0)
                        pesAtendimentoId[i] = "";
                }
            }
        }

		protected bool GravaAtendimento(int i) {
		    return (status[i] != null && status[i].Length > 0 && int.Parse(status[i]) == 1) &&
                    (pesAtendimentoId[i] != null && pesAtendimentoId[i].Length > 0 && int.Parse(pesAtendimentoId[i]) != 0) && 
                    (produtos[i] != null && produtos[i].Length > 0);
		}

		protected void AtendimentoRealiza(int i)
		{
			// Executa procedure sp_Atendimento_Realiza
			ProcedureParameters[] atendimentoRealizaParams = new ProcedureParameters[11];

			atendimentoRealizaParams[0] = new ProcedureParameters(
				"@PessoaID",
				System.Data.SqlDbType.Int,
                (pessoaId == null) ? System.DBNull.Value : 
                    ((pessoaId[i] == null || pessoaId[i].Length == 0) ?
                        System.DBNull.Value :
                        (Object)int.Parse(pessoaId[i])));

			atendimentoRealizaParams[1] = new ProcedureParameters(
				"@EmpresaID",
				System.Data.SqlDbType.Int,
                ((empresaId == null) ? System.DBNull.Value :
                    empresaId[i] == null || empresaId[i].Length == 0 ?
					    System.DBNull.Value :
                        (Object)int.Parse(empresaId[i])));

			atendimentoRealizaParams[2] = new ProcedureParameters(
				"@UsuarioID",
				System.Data.SqlDbType.Int,
                ((usuarioId == null) ? System.DBNull.Value :
                    usuarioId[i] == null || usuarioId[i].Length == 0 ?
					    System.DBNull.Value :
                        (Object)int.Parse(usuarioId[i])));

            atendimentoRealizaParams[3] = new ProcedureParameters(
                "@ServicoAtendimentoID",
                System.Data.SqlDbType.Int,
                ((servicoAtendimentoID == null) ? System.DBNull.Value :
                    servicoAtendimentoID[i] == null || servicoAtendimentoID[i].Length == 0 ?
                        System.DBNull.Value :
                        (Object)int.Parse(servicoAtendimentoID[i])));

			atendimentoRealizaParams[4] = new ProcedureParameters(
				"@TipoAtendimentoID",
				System.Data.SqlDbType.Int,
                ((tipoAtendimentoId == null) ? System.DBNull.Value :
                    tipoAtendimentoId[i] == null || tipoAtendimentoId[i].Length == 0 ?
					    System.DBNull.Value :
                        (Object)int.Parse(tipoAtendimentoId[i])));

			atendimentoRealizaParams[5] = new ProcedureParameters(
				"@MeioAtendimentoID",
				System.Data.SqlDbType.Int,
                ((meioAtendimentoId == null) ? System.DBNull.Value :
                    meioAtendimentoId[i] == null || meioAtendimentoId[i].Length == 0 ?
					    System.DBNull.Value :
                        (Object)int.Parse(meioAtendimentoId[i])));

			atendimentoRealizaParams[6] = new ProcedureParameters(
				"@AtendimentoOK",
				System.Data.SqlDbType.Bit,
                ((atendimentoOk == null) ? 0 : (atendimentoOk[i] == '1'.ToString() ? 1 : 0)));

            atendimentoRealizaParams[7] = new ProcedureParameters(
                "@dtAgendamento",
                System.Data.SqlDbType.DateTime,
                ((dtAgendamento == null) ? System.DBNull.Value :
                    dtAgendamento[i] == null || dtAgendamento[i].Length == 0 ?
                        System.DBNull.Value :
                        (Object)DateTime.Parse(
                            dtAgendamento[i],
                            formatProvider,
                            System.Globalization.DateTimeStyles.NoCurrentDateDefault)));
            
            atendimentoRealizaParams[8] = new ProcedureParameters(
				"@Observacoes",
				System.Data.SqlDbType.VarChar,
                ((observacoes == null) ? System.DBNull.Value :
                    observacoes[i] == null ?
					    System.DBNull.Value :
                        (Object)observacoes[i]),
                8000);

            atendimentoRealizaParams[9] = new ProcedureParameters(
                "@TipoResultadoID",
                System.Data.SqlDbType.Int,
                ((status == null) ? System.DBNull.Value :
                    status[i] == null || status[i].Length == 0 ?
                        System.DBNull.Value :
                        (Object)int.Parse(status[i])));

			atendimentoRealizaParams[10] = new ProcedureParameters(
				"@PesAtendimentoID",
				System.Data.SqlDbType.Int,
                ((pesAtendimentoId == null) ? System.DBNull.Value :
                    pesAtendimentoId[i] == null || pesAtendimentoId[i].Length == 0 ?
					    System.DBNull.Value :
                        (Object)int.Parse(pesAtendimentoId[i])),
				ParameterDirection.InputOutput);

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Atendimento_Realiza", 
				atendimentoRealizaParams);

            if (pesAtendimentoId == null)
            {
                pesAtendimentoId = new string[1];
                pesAtendimentoId[0] = atendimentoRealizaParams[10].Data.ToString();
            }
            else
                pesAtendimentoId[i] = atendimentoRealizaParams[10].Data.ToString();
        }
               
		protected void AtendimentoGravaProdutos(int i)
		{
			// Executa procedure sp_Atendimento_GravaProdutos
			ProcedureParameters[] atendimentoGravaProdutosParams = new ProcedureParameters[3];

			atendimentoGravaProdutosParams[0] = new ProcedureParameters(
				"@PesAtendimentoID",
				System.Data.SqlDbType.Int,
                ((pesAtendimentoId == null) ? System.DBNull.Value :
                    pesAtendimentoId[i] == null || pesAtendimentoId[i].Length == 0 ?
					    System.DBNull.Value :
                        (Object)int.Parse(pesAtendimentoId[i])));

			atendimentoGravaProdutosParams[1] = new ProcedureParameters(
				"@DadosCotacao",
				System.Data.SqlDbType.VarChar,
                ((produtos == null) ? System.DBNull.Value :
                    produtos[i] == null || produtos[i].Length == 0 ?
					    System.DBNull.Value :
                        (Object)produtos[i]));
			atendimentoGravaProdutosParams[1].Length = 8000;

			atendimentoGravaProdutosParams[2] = new ProcedureParameters(
				"@Erro",
				System.Data.SqlDbType.VarChar,
                ((erro == null) ? System.DBNull.Value : 
                    erro[i] == null || erro[i].Length == 0 ? 
                        System.DBNull.Value : 
                        (Object)erro[i]),
				ParameterDirection.InputOutput);
			atendimentoGravaProdutosParams[2].Length = 100;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Atendimento_GravaProdutos", 
				atendimentoGravaProdutosParams);

            if (erro == null)
            {
                erro = new string[1];
                erro[0] = atendimentoGravaProdutosParams[2].Data.ToString();
            }
            else
                erro[i] = atendimentoGravaProdutosParams[2].Data.ToString();
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
            string sqlRetorno = "";
            
            for (int i = 0; i < status.Length; i++)
            {
                AtendimentoRealiza(i);

                if (GravaAtendimento(i))
                {
                    AtendimentoGravaProdutos(i);
                }

                if (erro == null)
                    sqlRetorno = "select " + pesAtendimentoId[i] + " as PesAtendimentoID";
                else if (erro[i] == null || erro[i].Length == 0)
                    sqlRetorno = "select " + pesAtendimentoId[i] + " as PesAtendimentoID";
                else
                {
                    throw new Exception(erro[i]);
                }

                erro = null;
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(sqlRetorno));
		}
	}
}
