using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class currdate : System.Web.UI.OverflyPage
    {
        private string dateFormat;

		public string currDateFormat
		{
			set { dateFormat = value; }
		}
		
        protected override void PageLoad(object sender, EventArgs e)
        {
            System.Data.DataSet resultDataSet = null;

            if (dateFormat == "DD/MM/YYYY")
            {
				resultDataSet = DataInterfaceObj.getRemoteData(
                    "select " +
	                    "convert(varchar(2), day(getdate())) + " +
	                    "'/' + " +
	                    "convert(varchar(2), month(getdate())) + " +
	                    "'/' + " +
	                    "convert(varchar(4), year(getdate())) as currDate"
                );
            }
            else if (dateFormat == "MM/DD/YYYY")
            {
				resultDataSet = DataInterfaceObj.getRemoteData(
                    "select " +
	                    "convert(varchar(2), month(getdate())) + " +
	                    "'/' + " +
	                    "convert(varchar(2), day(getdate())) + " +
	                    "'/' + " +
	                    "convert(varchar(4), year(getdate())) as currDate"
                );
            }
            else if (dateFormat == "YYYY/MM/DD/")
            {
				resultDataSet = DataInterfaceObj.getRemoteData(
                    "select " +
                        "convert(varchar(4), year(getdate())) + " +
                        "'/' + " +
                        "convert(varchar(2), month(getdate())) + " +
                        "'/' + " +
                        "convert(varchar(2), day(getdate())) as currDate"
                );
            }

            WriteResultXML(resultDataSet);
        }
    }
}
