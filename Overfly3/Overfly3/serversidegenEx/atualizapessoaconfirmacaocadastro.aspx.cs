using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSOverflyRDS;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class atualizapessoaconfirmacaocadastro : System.Web.UI.OverflyPage
    {
        private Integer validacaoDadoId;
        private Integer paisId;

		private ProcedureParameters[] gravaValorProcParams = new ProcedureParameters[2];
		private ProcedureParameters[] atualizaPessoaProcParams = new ProcedureParameters[1];

		public Integer nValidacaoDadoID
		{
			set { validacaoDadoId = value; }
		}
		
		public Integer nPaisID
		{
			set { paisId = value; }
		}

		protected void ValidacoesDadosGravaValorID()
		{
            gravaValorProcParams[0] = new ProcedureParameters(
                "@ValidacaoDadoID", 
                System.Data.SqlDbType.Int, 
                validacaoDadoId.intValue());
            gravaValorProcParams[1] = new ProcedureParameters(
                "@PaisID", 
                System.Data.SqlDbType.Int,
				paisId.intValue());

			DataInterfaceObj.execNonQueryProcedure(
				"sp_ValidacoesDados_GravaValorID",
				gravaValorProcParams);
		}
		
		protected void ValidacoesDadosAtualizaPessoa()
        {
            atualizaPessoaProcParams[0] = gravaValorProcParams[0];

			DataInterfaceObj.execNonQueryProcedure(
                "sp_ValidacoesDados_AtualizaPessoa", 
                atualizaPessoaProcParams);
		}
		
		protected override void PageLoad(object sender, EventArgs e)
        {
			ValidacoesDadosGravaValorID();
			ValidacoesDadosAtualizaPessoa();
			
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT dbo.fn_ValidacoesDados_EhValida(" +
					validacaoDadoId +
					") AS Resultado"
				)
			);
        }
    }
}
