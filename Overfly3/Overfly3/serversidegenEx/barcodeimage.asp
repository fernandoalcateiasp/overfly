
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
   Function GetColor(sColor)
    dim icolor
    if(sColor="Black") then
		icolor=1 '&#000000
	elseif (sColor="White") then
		icolor=255+255*256+255*256*256 '&HFFFFFF
	elseif (sColor="Blue") then
	    icolor=255*256*256   '&HFF0000
	elseif (sColor="Green") then
	    icolor=255*256 '&H00FF00
	elseif (sColor="Red") then
	    icolor=255 '&HFF
	else
	    icolor=&HFF
	end if
	GetColor=icolor
   End Function
%>

<%
	Dim i
	Dim nBarCodeType
	Dim nBarCodeNumber	
	Dim sTopComment
	Dim sBottomComment
	Dim sSupplement
	Dim nNarrowBarsWidth
	Dim nNarrowBarsHeight
	Dim nOrientation
	Dim bGenerateBorder
	Dim nBorderType
	Dim nZonaSilencio
	Dim bCheckEnable
	Dim sBackGroundColor
	Dim sBarsColor
	Dim nRatio
	Dim sTopCommentFontName
	Dim nTopCommentFontSize
	Dim bTopCommentFontItalic
	Dim bTopCommentFontBold
	Dim sBottomCommentFontName
	Dim nBottomCommentFontSize
	Dim bBottomCommentFontItalic
	Dim bBottomCommentFontBold
	Dim sMessageCommentFontName
	Dim nMessageCommentFontSize
	Dim bMessageCommentFontItalic
	Dim bMessageCommentFontBold

	' Clear out the existing HTTP header information
	Response.Expires = 0
	Response.Buffer = TRUE
	Response.Clear

	' Change the HTTP header to reflect that an image is being passed.
	Response.ContentType = "image/png"

	'Tipo do Codigo de Barras, default 18 (Interleaved 2 of 5)
	nBarCodeType = 18
	'Numero do codigo de barras
	nBarCodeNumber = 0
	'Texto do comenterio acima do codigo de barras
	sTopComment = ""
	'Texto do comenterio abaixo do codigo de barras
	sBottomComment = ""
	'Suplemento
	sSupplement = ""
	'Largura das barras (medida em pixels)
	nNarrowBarsWidth = 1
	'Altura das barras (medida em pixels)
	nNarrowBarsHeight = 49
	'Orientacao , em graus
	nOrientation = 0
	'Gera bordar p/ o codigo de barras
	bGenerateBorder = 0
	'Tipo de Borda, 0 = no border
	nBorderType = 0
	'Zona de silencio
	nZonaSilencio = 1
	'Check Enable
	bCheckEnable = 0
	'Cor de fundo do codigo de barras
	sBackGroundColor = "White"
	'Cor das barras do codigo de barras
	sBarsColor = "Black"
	'Espacamento entre as barras
	nRatio = 3
	'Nome da fonte do comentario acima do codigo de barras
	sTopCommentFontName = "Arial"
	'Tamanho da fonte do comentario acima do codigo de barras
	nTopCommentFontSize = 10
	'Gera fonte italica no comentario acima do codigo de barras
	bTopCommentFontItalic = 0
	'Gera fonte bold no comentario acima do codigo de barras
	bTopCommentFontBold = 0
	'Nome da fonte do comentario acima do codigo de barras
	sBottomCommentFontName = "Arial"
	'Tamanho da fonte do comentario abaixo do codigo de barras
	nBottomCommentFontSize = 10
	'Gera fonte italica no comentario abaixo do codigo de barras
	bBottomCommentFontItalic = 0
	'Gera fonte bold no comentario abaixo do codigo de barras
	bBottomCommentFontBold = 0
	'Nome da fonte da linha de mensagem
	sMessageCommentFontName = "Arial"
	'Tamanho da fonte da linha de mensagem
	nMessageCommentFontSize = 10
	'Gera fonte italica na linha de mensagem
	bMessageCommentFontItalic = 0
	'Gera fonte bold na linha de mensagem
	bMessageCommentFontBold = 0

	For i = 1 To Request.QueryString("nBarCodeType").Count    
	  nBarCodeType = Request.QueryString("nBarCodeType")(i)
	Next

	For i = 1 To Request.QueryString("nBarCodeNumber").Count    
	  nBarCodeNumber = Request.QueryString("nBarCodeNumber")(i)
	Next

	For i = 1 To Request.QueryString("sTopComment").Count    
	  sTopComment = Request.QueryString("sTopComment")(i)
	Next

	For i = 1 To Request.QueryString("sBottomComment").Count    
	  sBottomComment = Request.QueryString("sBottomComment")(i)
	Next

	For i = 1 To Request.QueryString("sSupplement").Count    
	  sSupplement = Request.QueryString("sSupplement")(i)
	Next

	For i = 1 To Request.QueryString("nNarrowBarsWidth").Count    
	  nNarrowBarsWidth = Request.QueryString("nNarrowBarsWidth")(i)
	Next

	For i = 1 To Request.QueryString("nNarrowBarsHeight").Count    
	  nNarrowBarsHeight = Request.QueryString("nNarrowBarsHeight")(i)
	Next

	For i = 1 To Request.QueryString("nOrientation").Count    
	  nOrientation = Request.QueryString("nOrientation")(i)
	Next

	For i = 1 To Request.QueryString("bGenerateBorder").Count    
	  bGenerateBorder = CInt(Request.QueryString("bGenerateBorder")(i))
	Next

	For i = 1 To Request.QueryString("nBorderType").Count    
	  nBorderType = Request.QueryString("nBorderType")(i)
	Next

	For i = 1 To Request.QueryString("nZonaSilencio").Count    
	  nZonaSilencio = Request.QueryString("nZonaSilencio")(i)
	Next

	For i = 1 To Request.QueryString("bCheckEnable").Count    
	  bCheckEnable = CInt(Request.QueryString("bCheckEnable")(i))
	Next

	For i = 1 To Request.QueryString("sBackGroundColor").Count
	  sBackGroundColor = Request.QueryString("sBackGroundColor")(i)
	Next

	For i = 1 To Request.QueryString("sBarsColor").Count
	  sBarsColor = Request.QueryString("sBarsColor")(i)
	Next

	For i = 1 To Request.QueryString("nRatio").Count
	  nRatio = Request.QueryString("nRatio")(i)
	Next

	For i = 1 To Request.QueryString("sTopCommentFontName").Count
	  sTopCommentFontName = Request.QueryString("sTopCommentFontName")(i)
	Next

	For i = 1 To Request.QueryString("nTopCommentFontSize").Count
	  nTopCommentFontSize = Request.QueryString("nTopCommentFontSize")(i)
	Next

	For i = 1 To Request.QueryString("bTopCommentFontItalic").Count
	  bTopCommentFontItalic = CInt(Request.QueryString("bTopCommentFontItalic")(i))
	Next

	For i = 1 To Request.QueryString("bTopCommentFontBold").Count
	  bTopCommentFontBold = CInt(Request.QueryString("bTopCommentFontBold")(i))
	Next
	
	For i = 1 To Request.QueryString("sBottomCommentFontName").Count
	  sBottomCommentFontName = Request.QueryString("sBottomCommentFontName")(i)
	Next

	For i = 1 To Request.QueryString("nBottomCommentFontSize").Count
	  nBottomCommentFontSize = Request.QueryString("nBottomCommentFontSize")(i)
	Next

	For i = 1 To Request.QueryString("bBottomCommentFontItalic").Count
	  bBottomCommentFontItalic = CInt(Request.QueryString("bBottomCommentFontItalic")(i))
	Next

	For i = 1 To Request.QueryString("bBottomCommentFontBold").Count
	  bBottomCommentFontBold = CInt(Request.QueryString("bBottomCommentFontBold")(i))
	Next

	For i = 1 To Request.QueryString("sMessageCommentFontName").Count
	  sMessageCommentFontName = Request.QueryString("sMessageCommentFontName")(i)
	Next

	For i = 1 To Request.QueryString("nMessageCommentFontSize").Count
	  nMessageCommentFontSize = Request.QueryString("nMessageCommentFontSize")(i)
	Next

	For i = 1 To Request.QueryString("bMessageCommentFontItalic").Count
	  bMessageCommentFontItalic = CInt(Request.QueryString("bMessageCommentFontItalic")(i))
	Next

	For i = 1 To Request.QueryString("bMessageCommentFontBold").Count
	  bMessageCommentFontBold = CInt(Request.QueryString("bMessageCommentFontBold")(i))
	Next

	Set barcode = Server.CreateObject("ASPXBarCode.ASPBarCode.1")

	barcode.barSymbology=CInt(nBarCodeType)
	barcode.barMessage=nBarCodeNumber
	barcode.barHeight=CInt(nNarrowBarsHeight)
	barcode.barNarrowWidth=CInt(nNarrowBarsWidth)
	barcode.BackColor=GetColor(sBackGroundColor)
	barcode.barColor=GetColor(sBarsColor)
	barcode.barQuietZone=CInt(nZonaSilencio)
	barcode.barRatio=CInt(nRatio)
	barcode.barSupplement=sSupplement
	barcode.barCDEnable=CBool(bCheckEnable=1)
	barcode.barBearerMode=CBool(bGenerateBorder=1)
	barcode.barBorder=CLng(nBorderType)
	barcode.barHRDisplay=CBool(0)

	Set fn=barcode.barMessageFont
	fn.Name=sMessageCommentFontName
	fn.Size=CInt(nMessageCommentFontSize)
	fn.Italic=CBool(bMessageCommentFontItalic=1)
	fn.Bold= CBool(bMessageCommentFontBold=1)
	barcode.barMessageFont=fn
	Set fn = Nothing

	Set fn=barcode.barTopFont
	fn.Name=sTopCommentFontName
	fn.Size=CInt(nTopCommentFontSize)
	fn.Italic=CBool(bTopCommentFontItalic=1)
	fn.Bold= CBool(bTopCommentFontBold=1)
	barcode.barTopFont=fn   
	Set fn = Nothing

	Set fn=barcode.barBottomFont
	fn.Name=sBottomCommentFontName
	fn.Size=CInt(nBottomCommentFontSize)
	fn.Italic=CBool(bBottomCommentFontItalic=1)
	fn.Bold= CBool(bBottomCommentFontBold=1)
	barcode.barBottomFont=fn   
	Set fn = Nothing

	barcode.barTopComment=sTopComment
	barcode.barBottomComment=sBottomComment
	barcode.barOrientation=CInt(nOrientation)

	Dim bi
	bi=barcode.ReadBinary()
	Response.BinaryWrite(bi)
	Set barcode = Nothing
	Response.End
	   
%>
