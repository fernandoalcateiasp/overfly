﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class fornecedorExportaPrecos : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;

        private string nRelacoes = Convert.ToString(HttpContext.Current.Request.Params["sRelacoes"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
                relatorioFornecedores();
        }

        public void relatorioFornecedores()
        {
            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";

            if (sLinguaLogada != 246)
                param = "MM/dd/yyy";

            string Title = "Preços de Produtos - " + DateTime.Now.ToString(param);

            string strSQL = "SELECT a.ObjetoID AS ProdutoID," +
                                    "dbo.fn_Localidade_Dado(dbo.fn_Pessoa_Localidade(a.SujeitoID, 1, NULL, NULL), 2) AS Pais, " +
                                    "dbo.fn_Localidade_Dado(dbo.fn_Pessoa_Localidade(a.SujeitoID, 2, NULL, NULL), 2) AS UF, " +
                                    "(CASE WHEN(a.OrigemID IN(2701, 2706)) THEN 'I' ELSE 'N' END) AS Origem, " +
                                    "c.PartNumber, " +
                                    "d.SimboloMoeda AS Moeda, " +
                                    "b.CustoFOB, " +
                                    "(CASE WHEN((a.OrigemID IN(2701, 2706)) AND(b.ViaTransporteID = 634) AND(c.ClassificacaoID = 315)) " +
                                    "THEN ISNULL(CONVERT(VARCHAR(10), b.FatorInternacaoEntrada), '') ELSE '' END) AS FIE, " +
                                    "'' AS dtValidade, " +
                                    "'' AS dtProcessamento, " +
                                    "'' AS Processamento " +
                                "FROM RelacoesPesCon a WITH(NOLOCK) " +
                                    "INNER JOIN RelacoesPesCon_Fornecedores b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) AND(b.Ordem = 1) " +
                                    "INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.ObjetoID) " +
                                    "INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = b.MoedaID) " +
                                "WHERE a.RelacaoID IN (" + nRelacoes + ")";

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

            int Datateste = 0;

            DataSet dsFin = new DataSet();
            dsFin = (DataSet)HttpContext.Current.Session["Query"];
            Datateste = dsFin.Tables.Count;

            //Verifica se a Query está vazia e retorna mensagem de erro
            if (dsFin.Tables["Query1"].Rows.Count == 0)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                return;
            }
            else
            {
                Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, true, true);

                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, 2);
        }
    }
}