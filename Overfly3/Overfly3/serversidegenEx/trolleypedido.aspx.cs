using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class trolleypedido : System.Web.UI.OverflyPage
    {
        private Integer carrinhoId;
        
        private Integer pedidoId;
        private string resultado;

		public Integer nCarrinhoID
		{
			set { carrinhoId = value != null ? value : new Integer(0); }
		}

		// Roda a procedure sp_Pedido_Gerador
		protected void PedidoGerador()
		{
            // Roda a procedure sp_Pedido_Gerador
            ProcedureParameters[] procParams = new ProcedureParameters[23];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                -1*carrinhoId.intValue());

            procParams[1] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@ParceiroID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[3] = new ProcedureParameters(
                "@EhCliente",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[4] = new ProcedureParameters(
                "@TransacaoID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@Observacao",
                System.Data.SqlDbType.VarChar,
				System.DBNull.Value);

            procParams[6] = new ProcedureParameters(
                "@SeuPedido",
                System.Data.SqlDbType.VarChar,
				System.DBNull.Value);

            procParams[7] = new ProcedureParameters(
                "@OrigemPedidoID",
                System.Data.SqlDbType.Int,
				605);

            procParams[8] = new ProcedureParameters(
                "@FinanciamentoID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[9] = new ProcedureParameters(
                "@dtPrevisaoEntrega",
                System.Data.SqlDbType.DateTime,
				System.DBNull.Value);

            procParams[10] = new ProcedureParameters(
                "@TransportadoraID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[11] = new ProcedureParameters(
                "@MeioTransporteID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[12] = new ProcedureParameters(
                "@ModalidadeTransporteID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[13] = new ProcedureParameters(
                "@Frete",
                System.Data.SqlDbType.Bit,
				System.DBNull.Value);

            procParams[14] = new ProcedureParameters(
                "@DepositoID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[15] = new ProcedureParameters(
                "@ProprietarioID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[16] = new ProcedureParameters(
                "@AlternativoID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[17] = new ProcedureParameters(
                "@Observacoes",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);
            procParams[17].Length = 8000;

            procParams[18] = new ProcedureParameters(
                "@NumProjeto",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value);

            procParams[19] = new ProcedureParameters(
                "@ProgramaMarketingID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[20] = new ProcedureParameters(
                "@URLEtiqueta",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value); 

            procParams[21] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[22] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
				System.DBNull.Value,
                ParameterDirection.Output);
            procParams[22].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pedido_Gerador", 
				procParams);

			pedidoId = (procParams[21].Data != DBNull.Value && procParams[21].Data.ToString().Length > 0) ?
				new Integer(int.Parse(procParams[21].Data.ToString())) : null;
				
			resultado = procParams[22].Data.ToString();
		}

		// Roda a procedure sp_PedidoItemCarrinho_Gerador
		protected void PedidoItemCarrinhoGerador()
		{
            ProcedureParameters[] procParams2 = new ProcedureParameters[1];

            procParams2[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                pedidoId != null ? (Object)pedidoId.ToString() : DBNull.Value);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_PedidoItemCarrinho_Gerador",
                procParams2);
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
            PedidoGerador();

            if (pedidoId != null)
                PedidoItemCarrinhoGerador();

            // Gera o resultado para o usuario.
            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select " +
                ((pedidoId != null) ? pedidoId.ToString() : " NULL ") + " as PedidoID, " +
                ((resultado != null) ? (resultado.Length > 0 ? ("'" + resultado + "'") : "NULL") : " NULL ") + " as Resultado"
            ));
        }
    }
}
