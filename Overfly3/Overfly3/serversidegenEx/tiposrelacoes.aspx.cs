using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class tiposrelacoes : System.Web.UI.OverflyPage
    {
        private Integer formId;

		public Integer formID
		{
			set { formId = value; }
		}

        protected string Sql
        {
			get
			{
				string sql = "";

				if (formId == null) return sql;

				switch (formId.intValue())
				{
					case 1110:   //Form de Recursos
						sql = "SELECT 'SUJEITO' AS Tipo,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " +
							"a.Ordem,a.EhDefault,a.ObjetoOutros AS NomePasta,a.Sujeito AS Header,b.TipoObjetoID AS TipoPossivel " +
							"FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
							"WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " +
							"AND (a.RelacaoEntreID =  131 OR a.RelacaoEntreID =  132) " +
							"UNION ALL (SELECT 'OBJETO' AS Tipo,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " +
							"a.Ordem,a.EhDefault,a.SujeitoOutros AS NomePasta,a.Objeto AS Header,b.TipoSujeitoID AS TipoPossivel " +
							"FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
							"WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " +
							"AND (a.RelacaoEntreID =  131 OR a.RelacaoEntreID =  132)) " +
							"ORDER BY NomePasta";
						break;
					case 1210:
						sql = "SELECT 'SUJEITO' AS Tipo,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " +
							"a.Ordem,a.EhDefault,a.ObjetoOutros AS NomePasta,a.Sujeito AS Header,b.TipoObjetoID AS TipoPossivel " +
							"FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
							"WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " +
							"AND (a.RelacaoEntreID =  132 OR a.RelacaoEntreID =  133 " +
							"OR a.RelacaoEntreID =  135) " +
							"UNION ALL (SELECT 'OBJETO' AS Tipo,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " +
							"a.Ordem,a.EhDefault,a.SujeitoOutros AS NomePasta,a.Objeto AS Header,b.TipoSujeitoID AS TipoPossivel " +
							"FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
							"WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " +
							"AND (a.RelacaoEntreID =  132 OR a.RelacaoEntreID =  133 " +
							"OR a.RelacaoEntreID =  135)) " +
							"ORDER BY NomePasta";
						break;
					case 2110:
						sql = "SELECT 'SUJEITO' AS Tipo,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " +
							"a.Ordem,a.EhDefault,a.ObjetoOutros AS NomePasta,a.Sujeito AS Header,b.TipoObjetoID AS TipoPossivel " +
							"FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
							"WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " +
							"AND (a.RelacaoEntreID =  134 OR a.RelacaoEntreID =  135) " +
							"UNION ALL (SELECT 'OBJETO' AS Tipo,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " +
							"a.Ordem,a.EhDefault,a.SujeitoOutros AS NomePasta,a.Objeto AS Header,b.TipoSujeitoID AS TipoPossivel " +
							"FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
							"WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " +
							"AND (a.RelacaoEntreID =  134 OR a.RelacaoEntreID =  135)) " +
							"ORDER BY NomePasta";
						break;
					default:
						sql = "SELECT 'SUJEITO' AS Tipo,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " +
							"a.Ordem,a.EhDefault,a.ObjetoOutros AS NomePasta,a.Sujeito AS Header,b.TipoObjetoID AS TipoPossivel " +
							"FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
							"WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.TipoRelacaoID = -999";
						break;
				}

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(Sql));
		}
	}
}
