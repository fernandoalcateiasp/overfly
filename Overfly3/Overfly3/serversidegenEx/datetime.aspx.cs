using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class datetime : System.Web.UI.OverflyPage
    {
        protected override void PageLoad(object sender, EventArgs e)
        {
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select  " +
						"convert(varchar(2), day(getdate())) + " +
						"'/' +  " +
						"convert(varchar(2), month(getdate())) + " +
						"'/' +  " +
						"convert(varchar(4), year(getdate())) as [DateTime], " +
						"DATEPART ( hh , getdate()) as [Hora], " +
						"DATEPART ( mi , getdate()) as [Minuto]"
				)
            );
        }
    }
}
