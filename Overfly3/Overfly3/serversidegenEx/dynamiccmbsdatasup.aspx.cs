using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class dynamiccmbsdatasup : System.Web.UI.OverflyPage
    {
        private java.lang.Integer formId;
        private java.lang.Integer userId;
        private java.lang.Integer empresaId;
        private java.lang.Integer _param1, _param2, _param3, _param4, _param5;
        private java.lang.Integer _param6, _param7, _param8, _param9, _param10;

        private java.lang.Integer Zero = new java.lang.Integer(0);

        public java.lang.Integer nFormID
        {
            get { return formId; }
            set { formId = (value != null ? value : Zero); }
        }

        public java.lang.Integer nUserID
        {
            get { return userId; }
            set { userId = (value != null ? value : Zero); }
        }

        public java.lang.Integer nEmpresaID
        {
            get { return empresaId; }
            set { empresaId = (value != null ? value : Zero); }
        }

        public java.lang.Integer param1
        {
            get { return _param1; }
            set { _param1 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param2
        {
            get { return _param2; }
            set { _param2 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param3
        {
            get { return _param3; }
            set { _param3 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param4
        {
            get { return _param4; }
            set { _param4 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param5
        {
            get { return _param5; }
            set { _param5 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param6
        {
            get { return _param6; }
            set { _param6 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param7
        {
            get { return _param7; }
            set { _param7 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param8
        {
            get { return _param8; }
            set { _param8 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param9
        {
            get { return _param9; }
            set { _param9 = (value != null ? value : Zero); }
        }

        public java.lang.Integer param10
        {
            get { return _param10; }
            set { _param10 = (value != null ? value : Zero); }
        }

        protected string Sql
        {
            get
            {
                string sql = null;

                switch (nFormID.intValue())
                {
                    case 1110:   // Form de Recursos
                    case 1120:   // Form de Rela��es entre Recursos
                    case 1210:   // Form de Pessoas
                    case 1130:   // Form de Rela��es Pessoas e Recursos
                    case 1220:   // Form de Relacoes Entre Pessoas
                    case 1320:   // Form de Tipos de Rela��es
                    case 1330:   // Form de Localidades
                    case 2110:   // Form de Conceitos
                    case 2120:   // Form de Rela��es entre Conceitos
                    case 2130:   // Form de Rela��es entre Pessoas e Conceitos
                    case 5110:   // Form de Pedidos

                        sql =
                                    // parametrizacao do dso dsoCmbDynamic01 (designado para selPessoaID)
                                "SELECT 1 AS Indice, PessoaID AS fldID, Fantasia AS fldName, 0 AS Ordem, dbo.fn_Pessoa_Telefone(PessoaID, 119, 122,1,0,NULL) AS Telefone " +
                                "FROM Pessoas WITH(NOLOCK) " +
                                "WHERE PessoaID = " + (param1) + " " +

                                // parametrizacao do dso dsoCmbDynamic02 (designado para selParceiroID)
                                "UNION ALL " +
                                "SELECT 2 AS Indice, PessoaID as fldID,Fantasia as fldName, 0 AS Ordem, dbo.fn_Pessoa_Telefone(PessoaID, 119, 122,1,0,NULL) AS Telefone " +
                                "FROM Pessoas WITH(NOLOCK) " +
                                "WHERE PessoaID = " + (param2) + " " +

                                // parametrizacao do dso dsoCmbDynamic03 (designado para selTransacaoID)
                                "UNION ALL " +
                                "SELECT 3 AS Indice, Transacoes.OperacaoID AS fldID, CONVERT(VARCHAR(10), Transacoes.OperacaoID) AS fldName, 0 AS Ordem, SPACE(0) AS Telefone " +
                                "FROM Operacoes Transacoes WITH(NOLOCK) " +
                                "WHERE Transacoes.OperacaoID = " + (param3) + " " +

                                // parametrizacao do dso dsoCmbDynamic04 (designado para selNotaFiscalID)
                                "UNION ALL " +
                                "SELECT 4 AS Indice, NotaFiscalID AS fldID, LTRIM(RTRIM(STR(NotaFiscal))) AS fldName, 0 AS Ordem, SPACE(0) AS Telefone " +
                                "FROM NotasFiscais WITH(NOLOCK) " +
                                "WHERE NotaFiscalID = " + (param4) + " " +

                                // parametrizacao do dso dsoCmbDynamic05 (designado para selTransportadoraID)
                                "UNION ALL " +
                                "SELECT 5 AS Indice, a.PessoaID as fldID, a.Fantasia as fldName, 0 AS Ordem, dbo.fn_Pessoa_Telefone(a.PessoaID, 119, 122,1,0,NULL) AS Telefone " +
                                "FROM Pessoas a WITH(NOLOCK) " +
                                "WHERE a.PessoaID=" + (param7) + " AND a.PessoaID<>" + (param5) + " " +
                                "UNION ALL " +
                                "SELECT 5 AS Indice, " + (param5) + " AS fldID, 'O mesmo'  AS fldName, 1000 AS Ordem, dbo.fn_Pessoa_Telefone(" + (param5) + ", 119, 122,1,0,NULL) AS Telefone " + " " +

                                // parametrizacao do dso dsoCmbDynamic07 (designado para selMeioTransporteID)
                                "UNION ALL SELECT 6 AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem, SPACE(0) AS Telefone " +
                                "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                                "WHERE ItemID =" + (param8) + " " +

                                // parametrizacao do dso dsoCmbDynamic08 (designado para selModalidadeTransporteID)
                                "UNION ALL SELECT 7 AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem AS Ordem, SPACE(0) AS Telefone " +
                                "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                                "WHERE ItemID =" + (param9) + " " +

                                // parametrizacao do dso dsoCmbDynamic09 (designado para selShipTo)
                                "UNION ALL SELECT 8 AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem, SPACE(0) AS Telefone " +
                                "UNION ALL SELECT 8 AS Indice, PessoaID AS fldID, Fantasia AS fldName, 1 AS Ordem, SPACE(0) AS Telefone " +
                                "FROM Pessoas WITH(NOLOCK) " +
                                "WHERE PessoaID =" + (param10) + " " +
                                "ORDER BY Indice, Ordem";

                        break;

                    case 5140:   // Form de Opera��es                
                    case 5220:   // Form de Lista de Preco
                    case 9110:   // Form Financeiro
                    case 9130:   // Form Valores a Localizar
                    case 9210:   // Form Cobranca
                    case 9220:   // Form Bancos
                    default:
                        break;
                }

                return sql;
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(
                DataInterfaceObj.getRemoteData(Sql));
        }
    }
}
