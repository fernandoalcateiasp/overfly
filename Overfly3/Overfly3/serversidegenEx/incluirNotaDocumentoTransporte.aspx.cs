using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using Overfly3.systemEx.serverside;

namespace Overfly3.serversidegenEx
{
    public partial class incluirNotaDocumentoTransporte : System.Web.UI.OverflyPage
	{
        private string DocumentoTransporteID;
        private string NotaFiscalID;
        //private string observacao;
        private bool Automatico;
        private string ChaveAcesso;
        private string Protocolo;

        public string nDocumentoTransporteID
		{
            set { DocumentoTransporteID = value != null ? value : "0"; }
		}

        public string nNotaFiscalID
        {
            set { NotaFiscalID = value != null ? value : "0"; }
        }

        public string bAutomatico
        {
            set { Automatico = value == "1" ? true : false; }
        }

        public string sChaveAcesso
        {
            set { ChaveAcesso = value != null ? value : ""; }
        }

        public string sProtocolo
        {
            set { Protocolo =  value != null ? value : ""; }
        }
        
        /* Roda a procedure sp_DocumentoTransporteNotaFiscal_Gerador */
        private void DocumentoTransporteTransporteNotaFiscal()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[5];

            parameters[0] = new ProcedureParameters("@DocumentoTransporteID", System.Data.SqlDbType.Int, DocumentoTransporteID);

            parameters[1] = new ProcedureParameters("@NotaFiscalLocalizar", System.Data.SqlDbType.Int, DBNull.Value);

            parameters[2] = new ProcedureParameters("@Protocolo", System.Data.SqlDbType.Int, DBNull.Value);

            parameters[3] = new ProcedureParameters("@NotaFiscalID", System.Data.SqlDbType.Int, NotaFiscalID);

            parameters[4] = new ProcedureParameters("@Observacao", System.Data.SqlDbType.VarChar, DBNull.Value); parameters[2].Length = 20;

            DataInterfaceObj.execNonQueryProcedure("sp_DocumentoTransporteNotaFiscal_Gerador", parameters);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
            string resultserv = "null";
            
            if (Automatico == true)
            {
                if (Protocolo != "" && Protocolo != "null")
                {
                    DataSet rowsAffected = null;

                    rowsAffected = DataInterfaceObj.getRemoteData(
                        "SELECT CONVERT(VARCHAR, COUNT(1)) [Existe] FROM Pedidos WITH(NOLOCK) WHERE NotaFiscalID = " + NotaFiscalID + " AND (RNTC LIKE '%" + Protocolo + "%' OR Observacao LIKE '%" + Protocolo + "%')");

                    string Existe = rowsAffected.Tables[1].Rows[0]["Existe"].ToString();

                    if (Existe != "0")
                    {
                        DataInterfaceObj.ExecuteSQLCommand(
                            "UPDATE DocumentosTransporte_NotasFiscais SET NotaFiscalID = " + NotaFiscalID + " WHERE Protocolo = '" + Protocolo + "'");
                    }
                    else
                        resultserv = "'Pedido com esta nota fiscal n�o possui mesmo Protocolo'";
                }
                else if (ChaveAcesso != "")
                {
                    DataSet rowsAffected = null;

                    rowsAffected = DataInterfaceObj.getRemoteData(
                        "SELECT CONVERT(VARCHAR, COUNT(1)) [Existe] FROM DocumentosTransporte_NotasFiscais WITH(NOLOCK) WHERE ChaveAcessoLocalizar = '" + ChaveAcesso + "'");

                    string Existe = rowsAffected.Tables[1].Rows[0]["Existe"].ToString();

                    if (Existe != "0")
                    {
                        DataInterfaceObj.ExecuteSQLCommand(
                            "UPDATE DocumentosTransporte_NotasFiscais SET NotaFiscalID = " + NotaFiscalID + " WHERE ChaveAcessoLocalizar = '" + ChaveAcesso + "'");
                    }
                    else
                        DataInterfaceObj.ExecuteSQLCommand(
                            "INSERT INTO DocumentosTransporte_NotasFiscais (DocumentoTransporteID, NotaFiscalID, ChaveAcessoLocalizar) SELECT " + DocumentoTransporteID + ", " + NotaFiscalID + ", '" + ChaveAcesso + "'");
                }
            }
            else
                DocumentoTransporteTransporteNotaFiscal();

			WriteResultXML(DataInterfaceObj.getRemoteData("select " + resultserv + " as resultServ"));
		}
	}
}
