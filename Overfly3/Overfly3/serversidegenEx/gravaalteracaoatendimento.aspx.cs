using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class gravaalteracaoatendimento : System.Web.UI.OverflyPage
	{
        private string[] empresaid;
		private string[] servico;
        private string[] usuarioId;
        private string[] classificacaoId;
        private string[] registroId;
        private string[] tipoId;
        private string[] dataValidade;
        private string[] proprietarioId;
        private string[] tipoPessoaId;
        private string[] ddiTelefone;
        private string[] dddComercial;
        private string[] telComercial;
        private string[] dddFaxCelular;
        private string[] faxCelular;
        private string[] email;
        private string[] site;
        private string[] recenciaMeta;
        private string[] frequenciaMeta;
        private string[] valorMeta;
        private string[] vendasMeta;
        private string[] pesRFVID;
        private string[] aprovacao;

		private Integer dataLen;

        protected Integer DataLen
        {
			set { dataLen = value; }
		}
        
        protected string[] nEmpresaid
        {
			set
			{
                empresaid = value;

                for (int i = 0; i < empresaid.Length; i++)
				{
                    if (empresaid[i] == null || empresaid[i].Length == 0)
                        empresaid[i] = "NULL";
				}
			}
		}

        protected string[] nServico
        {
			set
			{
				servico = value;

				for (int i = 0; i < servico.Length; i++)
				{
					if (servico[i] == null || servico[i].Length == 0)
						servico[i] = "NULL";
				}
			}
		}

        protected string[] nUsuarioID
        {
			set
			{
				usuarioId = value;
			}
		}

        protected string[] nClassificacaoID
        {
			set
			{
				classificacaoId = value; 
				
				for (int i = 0; i < classificacaoId.Length; i++)
				{
					if (classificacaoId[i] == null || classificacaoId[i].Length == 0)
						classificacaoId[i] = "NULL";
				}
			}
		}
       
        protected string[] nRegistroID
        {
			set
			{
				registroId = value;

                for (int i = 0; i < registroId.Length; i++)
				{
					if (registroId[i] == null || registroId[i].Length == 0)
						registroId[i] = "NULL";
				}
			}
		}
        
        protected string[] nTipoID
        {
			set
			{
				tipoId = value; 
				
				for (int i = 0; i < tipoId.Length; i++)
				{
					if (tipoId[i] == null || tipoId[i].Length == 0)
						tipoId[i] = "NULL";
				}
			}
		}

        protected string[] dtValidade
        {
			set
			{
				dataValidade = value;

				for (int i = 0; i < dataValidade.Length; i++)
				{
					if (dataValidade[i] == null || dataValidade[i].Length == 0)
						dataValidade[i] = "NULL";
					else
						dataValidade[i] = dataValidade[i];
				}
			}
		}

        protected string[] nProprietarioID
        {
			set
			{
				proprietarioId = value; 
				
				for (int i = 0; i < proprietarioId.Length; i++)
				{
					if (proprietarioId[i] == null || proprietarioId[i].Length == 0)
						proprietarioId[i] = "NULL";
				}
			}
		}

        protected string[] sTipoPessoaID
        {
			set
			{
				tipoPessoaId = value; 
			
				for (int i = 0; i < tipoPessoaId.Length; i++)
				{
					if (tipoPessoaId[i] == null || tipoPessoaId[i].Length == 0)
						tipoPessoaId[i] = "NULL";
				}
			}
		}

        protected string[] sDDITelefone
        {
			set
			{
				ddiTelefone = value; 
				
				for (int i = 0; i < ddiTelefone.Length; i++)
				{
					if (ddiTelefone[i] == null || ddiTelefone[i].Length == 0)
						ddiTelefone[i] = "NULL";
					else
						ddiTelefone[i] = "'" + ddiTelefone[i] + "'";
				}
			}
		}

        protected string[] sDDDComercial
        {
			set
			{
				dddComercial = value; 
				
				for (int i = 0; i < dddComercial.Length; i++)
				{
					if (dddComercial[i] == null || dddComercial[i].Length == 0)
						dddComercial[i] = "NULL";
					else
						dddComercial[i] = "'" + dddComercial[i] + "'";
				}
			}
		}        

        protected string[] sTelComercial
        {
			set
			{
				telComercial = value; 
				
				for (int i = 0; i < telComercial.Length; i++)
				{
					if (telComercial[i] == null || telComercial[i].Length == 0)
						telComercial[i] = "NULL";
					else
						telComercial[i] = "'" + telComercial[i] + "'";
				}
			}
		}

        protected string[] sDDDFaxCelular
        {
			set
			{
				dddFaxCelular = value; 
				
				for (int i = 0; i < dddFaxCelular.Length; i++)
				{
					if (dddFaxCelular[i] == null || dddFaxCelular[i].Length == 0)
						dddFaxCelular[i] = "NULL";
					else
						dddFaxCelular[i] = "'" + dddFaxCelular[i] + "'";
				}
			}
		}

        protected string[] sFaxCelular
        {
			set
			{
				faxCelular = value; 
				
				for (int i = 0; i < faxCelular.Length; i++)
				{
					if (faxCelular[i] == null || faxCelular[i].Length == 0)
						faxCelular[i] = "NULL";
					else
						faxCelular[i] = "'" + faxCelular[i] + "'";
				}
			}
		}

        protected string[] sEmail
        {
			set
			{
				email = value; 
				
				for (int i = 0; i < faxCelular.Length; i++)
				{
					if (email[i] == null || email[i].Length == 0)
						email[i] = "NULL";
					else
						email[i] = "'" + email[i] + "'";
				}
			}
		}

        protected string[] sSite
        {
			set
			{
				site = value; 
				
				for (int i = 0; i < site.Length; i++)
				{
					if (site[i] == null || site[i].Length == 0)
						site[i] = "NULL";
					else
						site[i] = "'" + site[i] + "'";
				}
			}
		}

        protected string[] nRecenciaMeta
        {
			set
			{
				recenciaMeta = value; 
				
				for (int i = 0; i < recenciaMeta.Length; i++)
				{
					if (recenciaMeta[i] == null || recenciaMeta[i].Length == 0)
						recenciaMeta[i] = "NULL";
				}
			}
		}

        protected string[] nFrequenciaMeta
        {
			set
			{
				frequenciaMeta = value; 
				
				for (int i = 0; i < frequenciaMeta.Length; i++)
				{
					if (frequenciaMeta[i] == null || frequenciaMeta[i].Length == 0)
						frequenciaMeta[i] = "NULL";
				}
			}
		}

        protected string[] nValorMeta
        {
			set
			{
				valorMeta = value; 
				
				for (int i = 0; i < valorMeta.Length; i++)
				{
					if (valorMeta[i] == null || valorMeta[i].Length == 0)
						valorMeta[i] = "NULL";
				}
			}
		}

        protected string[] nVendasMeta
        {
			set
			{
				vendasMeta = value; 
				
				for (int i = 0; i < vendasMeta.Length; i++)
				{
					if (vendasMeta[i] == null || vendasMeta[i].Length == 0)
						vendasMeta[i] = "NULL";
				}
			}
		}

        protected string[] nPesRFVID
        {
            set
            {
                pesRFVID = value;

                for (int i = 0; i < pesRFVID.Length; i++)
                {
                    if (pesRFVID[i] == null || pesRFVID[i].Length == 0)
                        pesRFVID[i] = "NULL";
                }
            }
        }

        protected string[] bAprovacao
        {
            set
            {
                aprovacao = value;

                for (int i = 0; i < aprovacao.Length; i++)
                {
                    if (aprovacao[i] == null || aprovacao[i].Length == 0)
                        aprovacao[i] = "NULL";
                }
            }
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
			string sql = "";
            int currTipoID = 0;
            int rowsAffected = 0;
            string erros = "";

            for (int i = 0; i < dataLen.intValue(); i++)
            {
                currTipoID = int.Parse(tipoId[i]);

                if (currTipoID == 1)
                {
                    //Pessoas
                    if (int.Parse(servico[i]) == 1)
                    {
                        sql = sql + "UPDATE Pessoas SET ClassificacaoID=" + classificacaoId[i] + 
                            ", dtValidadeCredito=" +
                            dataValidade[i] + ", " +
                            "UsuarioID=" + usuarioId[0] + ", " +
                            "ProprietarioID= " + proprietarioId[i] + " " +
                            "WHERE PessoaID=" + registroId[i] + " " +
                            "EXEC sp_Pessoa_Cadastro2 3, NULL, NULL, " + tipoPessoaId[i] + ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, " +
                            "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " + ddiTelefone[i] + ", " + dddComercial[i] + ", " +
                            telComercial[i] + " " + ", " + dddFaxCelular[i] + ", " + faxCelular[i] + " " + ", " +
                            email[i] + ", NULL, " + site[i] + ", " + usuarioId + ", NULL, NULL, NULL, NULL, NULL, " + registroId[i] + " ";
                    }
                    else
                    {
                        sql = sql + "UPDATE Pessoas SET ClassificacaoID=" + classificacaoId[i] + ", " +
                            "UsuarioID=" + usuarioId[0] + ", " +                            
                            "ProprietarioID= " + proprietarioId[i] + " " +
                            "WHERE PessoaID=" + registroId[i] + " " +
                            "UPDATE Pessoas_RFV SET RecenciaMeta = " + recenciaMeta[i] + ", FrequenciaMeta = " + frequenciaMeta[i] + ", " +
                            "ValorMeta =  " + valorMeta[i] + ", VendasMeta = " + vendasMeta[i] + ", AprovadorID = " + (Convert.ToBoolean(aprovacao[i]) ? usuarioId[0] : "NULL") +
                                " WHERE  PesRFVID = " + pesRFVID[i] + " " +
                            "EXEC sp_Pessoa_Cadastro2 3, NULL, NULL, " + tipoPessoaId[i] + ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, " +
                                "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " + ddiTelefone[i] + ", " + dddComercial[i] + ", "
                                + telComercial[i] + " " +
                            ", " + dddFaxCelular[i] + ", " + faxCelular[i] + " " +
                            ", " + email[i] + ", NULL, " + site[i] + ", " + usuarioId[0] + ", NULL, NULL, NULL, NULL, NULL, " + registroId[i] + " ";
                    }
                }
                //RelPessoas
                else if (currTipoID == 2)
                {
                    if (int.Parse(servico[i]) == 1)
                        sql = "UPDATE RelacoesPessoas SET " +                            
                            "UsuarioID=" + usuarioId[0] + ", " +
                            "ProprietarioID= " + proprietarioId[i] + " " +
                            "WHERE RelacaoID=" + registroId[i];
                    else
                        sql = "UPDATE RelacoesPessoas SET UsuarioID=" + usuarioId[0] + ", " +                            
                            "ProprietarioID= " + proprietarioId[i] + " " +
                            "WHERE RelacaoID=" + registroId[i];
                }
                //Contatos
                else if (currTipoID == 4)
                {
                    sql = "UPDATE Pessoas SET UsuarioID=" + usuarioId[0] + ", " +                        
                        "ProprietarioID= " + proprietarioId[i] + " " +
                        "WHERE PessoaID=" + registroId[i] + " " +
                        "EXEC sp_Pessoa_Cadastro2 3, NULL, NULL, " + tipoPessoaId[i] + ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " + ddiTelefone[i] + ", " + dddComercial[i] + ", " + telComercial[i] + " " +
                        ", " + dddFaxCelular[i] + ", " + faxCelular[i] + " " +
                        ", " + email[i] + ", NULL, " + site[i] + ", " + usuarioId[0] + ", NULL, NULL, NULL, NULL, NULL, " + registroId[i];
                }

                // Executa o pacote
                try
                {
                    rowsAffected = DataInterfaceObj.ExecuteSQLCommand(sql);
                }
                catch (System.Exception ex)
                {
                    if (!(ex.Message.Contains("Telefone j� existe")))
                        erros += "Erro no ID: " + registroId[i] + " - " + ex.Message + "\n";
                }

                sql = "";
            }

            var retorno = "";
            
            if (erros.Length > 0)
                retorno = "SELECT '" + erros + "' as Resultado";
            else
                retorno = "SELECT '" + rowsAffected + "' as Resultado";

            // Gera o resultado para o usuario.
			WriteResultXML(DataInterfaceObj.getRemoteData(retorno));
		}
	}
}
