using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class reportslog : System.Web.UI.OverflyPage
    {
        private string reportId;
        private string userId;
        private string empresaId;

		public string ReportID
		{
			set { reportId = value; }
		}
		public string UserID
		{
			set { userId = value; }
		}
		public string EmpresaID
		{
			set { empresaId = value; }
		}

        protected override void PageLoad(object sender, EventArgs e)
        {
			int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(
                "INSERT INTO _reportLog (EmpresaID, ReportID, UserID, DateStart) " +
                    "SELECT " + empresaId + "," + reportId + "," + userId + ", GETDATE()"
            );

            // Gera o resultado para o usuario.
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + rowsAffected + " as fldresp"
				)
            );
        }
    }
}
