using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class incluirDocumentoTransporteFatura : System.Web.UI.OverflyPage
	{

        private string FaturaID;
        private string DocumentoTransporteID;
        //private string observacao;


        public string nFaturaID
		{
            set { FaturaID = value != null ? value : "0"; }
		}

        public string nDocumentoTransporteID
        {
            set { DocumentoTransporteID = value != null ? value : "0"; }
        }


        /** Roda a procedure sp_FaturaDocumentoTransporte_Gerador */
        private void FaturaTransporteDocumentoTransporte()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[3];

            parameters[0] = new ProcedureParameters("@FaturaID", System.Data.SqlDbType.Int, FaturaID);

            parameters[1] = new ProcedureParameters("@DocumentoTransporteID", System.Data.SqlDbType.Int, DocumentoTransporteID);

            parameters[2] = new ProcedureParameters("@Observacao", System.Data.SqlDbType.VarChar, DBNull.Value); parameters[2].Length = 20;


            DataInterfaceObj.execNonQueryProcedure("sp_FaturaDocumentoTransporte_Gerador", parameters);

		}

		protected override void PageLoad(object sender, EventArgs e)
		{
            FaturaTransporteDocumentoTransporte();

			WriteResultXML(DataInterfaceObj.getRemoteData("select null as resultServ"));
		}
	}
}
