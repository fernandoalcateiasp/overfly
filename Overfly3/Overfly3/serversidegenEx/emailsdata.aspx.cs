using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class emailsdata : System.Web.UI.OverflyPage
    {
        private Integer userId;
        private Integer empresaId;
        private string nomePessoa;
        private Integer tipoPessoaId;
        private Integer soEmpLog;       

        public Integer nUserID
        {
			set { userId = value; }
		}
        public Integer nEmpresaID
        {
			set { empresaId = value; }
		}
        public string sNomePessoa
		{
			set { nomePessoa = value.Trim(); }
		}	
		public Integer nSoEmpLog
		{
			set { soEmpLog = value; }
		}

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sOperator;
            string sEmpresa;

            // Define qual o operador.
            sOperator = " >= ";

            if (nomePessoa.StartsWith("'%") || nomePessoa.EndsWith("%'"))
                sOperator = " LIKE ";


            if (soEmpLog.intValue() == 0)
                sEmpresa = "";
            else
                sEmpresa = "(b.ObjetoID IN (" + empresaId + ")) AND ";
            
            // Monta a consulta.
            string strSQL =
               "SELECT DISTINCT TOP 1000 a.Fantasia, " +
                "dbo.fn_Pessoa_Telefone(a.PessoaID, 119, 119, 1, 1, NULL) AS Telefone, " +
                "dbo.fn_Pessoa_Telefone(a.PessoaID, 121, 121, 1, 0, NULL) AS Celular, dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS Email, " +
                "0 AS [To], 0 AS [Cc], 0 AS [Bcc] " +
            "FROM Pessoas a WITH(NOLOCK) " +
                "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.SujeitoID = a.PessoaID) " +
            "WHERE ((a.EstadoID = 2) AND " +
                    "(b.EstadoID = 2) AND " +
                    "(b.TipoRelacaoID = 31) AND " +
                    sEmpresa +
                    "(a.Fantasia " + sOperator +"'"+ nomePessoa + "')) " +
            "ORDER BY a.Fantasia ";

			WriteResultXML(DataInterfaceObj.getRemoteData(strSQL));
        }
    }
}
