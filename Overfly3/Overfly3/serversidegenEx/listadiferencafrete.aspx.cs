using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class listadiferencafrete : System.Web.UI.OverflyPage
    {
        private string empresaId;
        private string dtInicio;
        private string dtFim;
        private string saldo;

        protected string nEmpresaID { set { empresaId = value != null ? value : ""; } }
        protected string ndtInicio { set { dtInicio = value != null ? value : ""; } }
        protected string ndtFim { set { dtFim = value != null ? value : ""; } }
        protected string nSaldo { set { saldo = value != null ? value : ""; } }

        protected DataSet DiferencaFrete()
        {
            // Executa procedure sp_DocumentoTransporte_Verifica
            ProcedureParameters[] DiferencaFreteParams = new ProcedureParameters[4];

            DiferencaFreteParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int, (Object)int.Parse(empresaId));

            DiferencaFreteParams[1] = new ProcedureParameters(
                "@dtInicio", System.Data.SqlDbType.DateTime, ((dtInicio == "") ? DBNull.Value : (Object)DateTime.Parse(dtInicio)));

            DiferencaFreteParams[2] = new ProcedureParameters(
                "@dtFim", System.Data.SqlDbType.DateTime, ((dtFim == "") ? DBNull.Value : (Object)DateTime.Parse(dtFim))); 

            DiferencaFreteParams[3] = new ProcedureParameters(
                "@Saldo",
                System.Data.SqlDbType.Int, ((saldo == "") ? DBNull.Value : (Object)int.Parse(saldo)));
            
            return DataInterfaceObj.execQueryProcedure(
                    "sp_DocumentoTransporte_DiferencaFrete",
                    DiferencaFreteParams);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {   
			// Gera o resultado.
			WriteResultXML(DiferencaFrete());
        }
    }
}

