using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class selecionaSacola : System.Web.UI.OverflyPage
    {
		private Integer sacolaID;
		private Integer vendedorID;
        private Integer tornarAtual;
        private Integer duplicarSacola;
        private int sacolaNovaID;

        public Integer nSacolaID
        {
			set { sacolaID = value; }
		}
		public Integer nVendedorID
        {
			set { vendedorID = value; }
		}

        public Integer nTornarAtual
        {
            set { tornarAtual = value; }
        }

        public Integer nDuplicarSacola
        {
            set { duplicarSacola = value; }
        }

        // Roda a procedure sp_SacolasCompras_Atual
        protected string SacolasComprasAtual()
        {
            string result = "";

            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
                "@SacolaID",
                System.Data.SqlDbType.Int,
                sacolaID.ToString());

            procParams[1] = new ProcedureParameters(
                "@VendedorID",
                System.Data.SqlDbType.Int,
                vendedorID.ToString());

            procParams[2] = new ProcedureParameters(
                "@TornarAtual",
                System.Data.SqlDbType.Bit,
                ((tornarAtual.intValue() == 1) ? true : false));

            procParams[3] = new ProcedureParameters(
                "@DuplicarSacola",
                System.Data.SqlDbType.Bit,
                ((duplicarSacola.intValue() == 1) ? true : false));

            procParams[4] = new ProcedureParameters(
                "@SacolaNovaID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[5] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParams[5].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_SacolasCompras_Atual", 
				procParams);

            sacolaNovaID += (procParams[4].Data != DBNull.Value) ? int.Parse(procParams[4].Data.ToString()) : 0; 
            result = (procParams[5].Data != DBNull.Value) ? (string)procParams[5].Data : "";

            return result;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string result = SacolasComprasAtual();

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT '" + result + "' AS Resultado, " + sacolaNovaID.ToString() + " AS SacolaNovaID"));
        }
    }
}
