using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Overfly3.systemEx.serverside;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class pesqlistsvr : System.Web.UI.OverflyPage
    {
        private static java.lang.Boolean Falso = new java.lang.Boolean(false);
        private static Integer MenosUm = new Integer(-1);
        private static Integer Zero = Constants.INT_ZERO;
        private ProcedureParameters[] parametrosSQL;

        // Atributos dos parametros da pagina
        private Integer formId;
        private Integer subformId;
        private string fldKey;
        private string order;
        private string metPesq;
        private string spcClause;
        private string textToSearch;
        private string condition;
        private Integer pageNumber;
        private Integer pageSize;
        private Integer filtroContextoId;
        private Integer filtroId;
        private Integer empresaId;
        private Integer idiomaSistemaId;
        private Integer idiomaEmpresaId;
        private Integer idiomaId;
        private Integer argumentoIsEmpty;
        private Integer userId;
        private Long proprietarioId; // Selecionar apenas os registros do usuario corrente
        private java.lang.Boolean ModoComboProprietario;
        private java.lang.Boolean c1;
        private java.lang.Boolean c2;
        private java.lang.Boolean a1;
        private java.lang.Boolean a2;
        private java.lang.Boolean RegistrosVencidos;
        


        // Dim fldF - Tratar no esquema novo (fldError).
        private string sql = "";
        private string sql2;
        private string sqlExecAfterCreateTable = "";
        private string sOperator;
        private int startID;
        private string sFiltro;
        private string sFiltroContexto;
        private string sAdditionalFrom;
        private string sAdditionalFrom_fn;
        private string sAdditionalWhere;
        private string sStrTop;

        private System.Data.DataSet rsContextoFiltro;

        // Parameto do SELECT para aplicar o direito
        // Parametro do WHERE para aplicar o direito
        private string sAccessAllowed = "";

        private string strAdditionalFrom1;
        private string strAdditionalFrom2;
        private string strAdditionalCondition1;
        private string strAdditionalCondition2;

        private string sClienteOperator;
        private string sClienteOperator2;
        private string sSQLPrecoZero;

        // Variaveis para logar o desempenho do lista de precos
        private int _Estoque;
        private int _ChavePessoaID;
        private int _ParceiroID;
        private int _PessoaID;
        private int _FinalidadeID;

        public Integer nFormID
        {
            set { formId = value != null ? value : Zero; }
        }
        public Integer nSubFormID
        {
            set { subformId = value != null ? value : Zero; }
        }
        public string sFldKey
        {
            set
            {
                if (value != null)
                    fldKey = value.Substring(0, 1) + "." + value.Substring(1);
                else
                    fldKey = "";
            }
        }
        public string nOrder
        {
            set { order = value != null ? value : ""; }
        }
        public java.lang.Boolean bRegistrosVencidos
        {
            set
            {
                if (value != null)
                {
                    RegistrosVencidos = value;
                }
                else
                    RegistrosVencidos = Falso;

            }
        }
        public java.lang.Boolean bModoComboProprietario
        {
            set
            {
                if (value != null)
                {
                    ModoComboProprietario = value;
                }
                else
                    ModoComboProprietario = Falso;

            }
        }
        public string sMetPesq
        {
            set { metPesq = value != null ? value : ""; }
        }
        // Clausula especial de pesquisa de peslist nao padroes
        public string sSpcClause
        {
            set { spcClause = value != null ? value : ""; }
        }
        public string sTextToSearch
        {
            set
            {
                if (value != null)
                {
                    textToSearch = value;

                    if (metPesq != null && metPesq.ToUpper().Equals("DEF") && textToSearch.Trim().Equals(""))
                    {
                        textToSearch = "0";
                    }
                }
                else
                {
                    textToSearch = "";
                }
            }
        }
        public string sCondition
        {
            set { condition = value != null ? value : ""; }
        }
        public Integer nPageNumber
        {
            set { pageNumber = value; }
        }
        public Integer nPageSize
        {
            set { pageSize = value; }
        }
        public Integer nFiltroContextoID
        {
            set { filtroContextoId = value; }
        }
        public Integer nFiltroID
        {
            set { filtroId = value; }
        }
        public Integer nEmpresaID
        {
            set { empresaId = value != null ? value : Zero; }
        }
        public Integer nIdiomaSistemaID
        {
            set { idiomaSistemaId = value != null ? value : Zero; }
        }
        public Integer nIdiomaEmpresaID
        {
            set { idiomaEmpresaId = value != null ? value : Zero; }
        }
        public Integer nIdiomaID
        {
            set { idiomaId = value; }
        }
        public Integer nArgumentoIsEmpty
        {
            set { argumentoIsEmpty = value != null ? value : MenosUm; }
        }
        public Integer userID
        {
            set { userId = value != null ? value : Zero; }
        }
        public Long nProprietarioID
        {
            set { proprietarioId = value; }
        }       
        public java.lang.Boolean C1
        {
            set { c1 = value != null ? value : Falso; }
        }
        public java.lang.Boolean C2
        {
            set { c2 = value != null ? value : Falso; }
        }
        public java.lang.Boolean A1
        {
            set { a1 = value != null ? value : Falso; }
        }
        public java.lang.Boolean A2
        {
            set { a2 = value != null ? value : Falso; }
        }

        private String[] CotadorPartNumber;

        public String[] sCotadorPartNumber
        {
            get { return CotadorPartNumber; }
            set { CotadorPartNumber = value; }
        }

        public int returnFldType(String tableName, String fieldName)
        {
            System.Data.DataSet rsTable = DataInterfaceObj.getRemoteData("SELECT TOP 1 * FROM " + tableName + " WITH(NOLOCK) ");



            /*
            rsTable.Tables[0].Rows.Count	11	int
		    rsTable.Tables[0].Rows[4].ItemArray[0]	"dtVigencia"	object {string}
		    rsTable.Tables[0].Rows[4].ItemArray[24]	"datetime"	object {string}
		    rsTable.Tables[0].Rows[4].ItemArray[2]	8	object {int}             
             */

            if (fieldName.IndexOf(".") >= 0)
            {
                fieldName = fieldName.Substring(fieldName.IndexOf(".") + 1);
            }


            if (rsTable.Tables[1].Rows.Count > 0)
            {

                try
                {
                    return Defines.dataTypesCode(rsTable.Tables[1].Rows[0][fieldName].GetType().Name);
                }
                catch (System.Exception)
                {
                    return 0;
                }
            }
            else
            {
                foreach (DataRow line in rsTable.Tables[0].Rows)
                {
                    if (line.ItemArray[0].ToString().Equals(fieldName))
                    {
                        return Defines.dataTypesCode(line.ItemArray[24].ToString());

                    }
                }
                return 0;
            }
        }
        protected void MakeSQLs()
        {
            int i;

            if ((proprietarioId.intValue() != 0) && (proprietarioId.intValue() == userId.intValue()))
            {
                if (Convert.ToBoolean(ModoComboProprietario) != false)
                    sAccessAllowed = " " + " a.ProprietarioID=" + proprietarioId.ToString() + " AND ";
                else
                    sAccessAllowed = " " + " a.AlternativoID=" + proprietarioId.ToString() + " AND ";
            }
            else if ((proprietarioId.intValue() != 0) && (proprietarioId.intValue() != userId.intValue()))
            {
                if (Convert.ToBoolean(ModoComboProprietario) != false)
                {
                    sAccessAllowed = " " + " a.ProprietarioID IN (SELECT " + proprietarioId.ToString() + " UNION ALL " +
                                                      "SELECT a.SujeitoID " +
                                                         "FROM RelacoesPessoas a WITH(NOLOCK) " +
                                                         "WHERE (a.TipoRelacaoID = 34) AND (a.ObjetoID = " + proprietarioId.ToString() + ") AND " +
                                                               "(a.EstadoID = 2)) AND ";
                }
                else
                {
                    sAccessAllowed = " " + " a.AlternativoID IN (SELECT " + proprietarioId.ToString() + " UNION ALL " +
                                                   "SELECT a.SujeitoID " +
                                                      "FROM RelacoesPessoas a WITH(NOLOCK) " +
                                                      "WHERE (a.TipoRelacaoID = 34) AND (a.ObjetoID = " + proprietarioId.ToString() + ") AND " +
                                                            "(a.EstadoID = 2)) AND ";
                }
            }

            if (!c2.booleanValue() && c1.booleanValue())
            {
                if (Convert.ToBoolean(ModoComboProprietario) == false)
                    sAccessAllowed = sAccessAllowed + " a.ProprietarioID= " + userId + " AND ";
                else 
                    sAccessAllowed = sAccessAllowed + " a.AlternativoID= " + userId + " AND ";
            }
            else if (c2.booleanValue() && !c1.booleanValue())
            {
                sAccessAllowed = sAccessAllowed + " "
                    + userId
                    + " =(CASE WHEN a.ProprietarioID="
                    + userId
                    + " THEN a.ProprietarioID "
                    + "WHEN a.AlternativoID=" + userId + " THEN a.AlternativoID "
                    + "WHEN a.AlternativoID=(SELECT TOP 1 grupo.ObjetoID FROM RelacoesPessoas grupo WITH(NOLOCK) "
                    + "WHERE grupo.TipoRelacaoID=34 AND grupo.EstadoID=2 "
                    + "AND grupo.SujeitoID=" + userId + " "
                    + "AND grupo.ObjetoID=a.AlternativoID) THEN " + userId + " "
                    + "ELSE 0 END) AND ";
            }
            else if (!c2.booleanValue() && c1.booleanValue())
            {
                pageSize = Zero;
            }


            if (condition != null && condition != "")
            {
                condition = "AND (" + condition + ")";
            }

            startID = (pageNumber.intValue() * pageSize.intValue()) - pageSize.intValue();

            if (order != null && order == "DESC" && textToSearch != null && textToSearch != "0")
            {
                sOperator = " <= ";
            }
            else
            {
                sOperator = " >= ";
            }

            // default - DEF
            // comeca com - COM
            // termina com TERM
            // contem - CONT

            if (textToSearch != null
                && textToSearch.Length > 1
               && ((textToSearch[0] == '%') || (textToSearch[textToSearch.Length - 1] == '%'))
                )
            {
                sOperator = " LIKE ";
            }
            else if (metPesq != null && metPesq.ToUpper() == "COM")
            {
                sOperator = " LIKE ";
                textToSearch = textToSearch + "%";
            }
            else if (metPesq != null && metPesq.ToUpper() == "CONT")
            {
                sOperator = " LIKE ";
                textToSearch = "%" + textToSearch + "%";
            }
            else if (metPesq != null && metPesq.ToUpper() == "TERM")
            {
                sOperator = " LIKE ";
                textToSearch = "%" + textToSearch;
            }

            if (formId.intValue() != 5220 && formId.intValue() != 5110 && formId.intValue() != 8110 && formId.intValue() != 1210)
            {
                textToSearch = "'" + textToSearch + "'";
            }
            ///////////////////////////// Filtro de Contexto -- INICIO ////////////////////////////
            sql = "SELECT 1 AS Ordem, Filtro From Recursos WITH(NOLOCK) WHERE RecursoID = " + filtroContextoId +
                  " UNION ALL " +
                  "SELECT 2 AS Ordem, Filtro From Recursos WITH(NOLOCK) WHERE RecursoID = " + filtroId;

            rsContextoFiltro = DataInterfaceObj.getRemoteData(sql);

            sFiltroContexto = " ";
            sFiltro = " ";

            for (i = 0; i < rsContextoFiltro.Tables[1].Rows.Count; i++)
            {
                if (rsContextoFiltro.Tables[1].Rows[i]["Ordem"].ToString() == "1")
                {
                    if ((rsContextoFiltro.Tables[1].Rows[i]["Filtro"] != DBNull.Value)
                            && ((rsContextoFiltro.Tables[1].Rows[i]["Filtro"].ToString() != "")))
                    {
                        sFiltroContexto = " AND (" + rsContextoFiltro.Tables[1].Rows[i]["Filtro"].ToString() + ") ";
                    }
                }

                if (rsContextoFiltro.Tables[1].Rows[i]["Ordem"].ToString() == "2")
                {
                    if ((rsContextoFiltro.Tables[1].Rows[i]["Filtro"] != DBNull.Value)
                            && ((rsContextoFiltro.Tables[1].Rows[i]["Filtro"].ToString() != "")))
                    {
                        sFiltro = " AND (" + rsContextoFiltro.Tables[1].Rows[1]["Filtro"] + ") ";
                    }
                }
            }

            //////////////////////////// Filtro (COMBO 1 e 2 SUPERIOR) -- FIM ////////////////////////

            sqlExecAfterCreateTable = "";


            switch (formId.intValue())
            {
                case 1110:   //formID /basico/Recursos formID

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP "
                        + (pageSize.intValue() * pageNumber.intValue()) + " a.RecursoID "
                        + "FROM Recursos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK) WHERE " + sAccessAllowed
                        + "a.EstadoID = c.RecursoID AND a.TipoRecursoID = b.ItemID AND "
                        + fldKey + sOperator + textToSearch + " "
                        + condition + sFiltroContexto + sFiltro;              
                    
                    if (fldKey == "a.RecursoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RecursoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " a.RecursoID,c.RecursoAbreviado as Estado,a.Recurso,a.RecursoFantasia,b.ItemMasculino, CONVERT(VARCHAR(50), SPACE(0)) AS fldError "
                             + "FROM Recursos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK), @TempTable z WHERE "
                             + "a.EstadoID = c.RecursoID AND a.TipoRecursoID = b.ItemID AND "
                             + fldKey + sOperator + textToSearch + " "
                             + condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RecursoID "
                             + "AND z.IDTMP > " + startID
                             + " ORDER BY z.IDTMP";

                    break;
                //////////////////////////// As strings de pesquisa -- INICIO ////////////////////////

                case 1120:   //formID  /basico/relacoes/relentrerecs

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP "
                        + (pageSize.intValue() * pageNumber.intValue()) + " a.RelacaoID "
                        + "FROM RelacoesRecursos a WITH(NOLOCK), TiposRelacoes b WITH(NOLOCK), Recursos c WITH(NOLOCK), Recursos d WITH(NOLOCK),Recursos e WITH(NOLOCK) WHERE " + sAccessAllowed
                        + "a.TipoRelacaoID=b.TipoRelacaoID AND a.SujeitoID=c.RecursoID AND "
                        + "a.ObjetoID=d.RecursoID AND a.EstadoID=e.RecursoID AND "
                        + fldKey + sOperator + textToSearch + " "
                        + condition + sFiltroContexto + sFiltro;

                    
                    if (fldKey == "a.RelacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " "
                             + "a.RelacaoID, e.RecursoAbreviado AS Estado, b.TipoRelacao AS Relacao, a.SujeitoID, c.RecursoFantasia AS Sujeito, a.ObjetoID, d.RecursoFantasia AS Objeto, CONVERT(VARCHAR(50), SPACE(0)) AS fldError "
                             + "FROM RelacoesRecursos a WITH(NOLOCK), TiposRelacoes b WITH(NOLOCK), Recursos c WITH(NOLOCK),Recursos d WITH(NOLOCK), Recursos e WITH(NOLOCK), @TempTable z "
                             + "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.SujeitoID=c.RecursoID AND a.ObjetoID=d.RecursoID AND a.EstadoID=e.RecursoID AND "
                             + fldKey + sOperator + textToSearch + " "
                             + condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RelacaoID "
                             + "AND z.IDTMP >= " + startID
                             + " ORDER BY z.IDTMP";

                    break;

                case 1130:   //formID  /basico/relacoes/relpessoaserecursos

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP "
                        + (pageSize.intValue() * pageNumber.intValue()) + " a.RelacaoID "
                        + "FROM RelacoesPesRec a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Recursos e WITH(NOLOCK) "
                        + "WHERE " + sAccessAllowed
                        + "a.TipoRelacaoID = c.TipoRelacaoID AND "
                        + "a.EstadoID = b.RecursoID AND "
                        + "a.SujeitoID = d.PessoaID AND "
                        + "a.ObjetoID = e.RecursoID AND ";

                    //Contexto de Uso Juridico
                    if ((filtroContextoId.intValue() == Integer.decode("1132").intValue()) && !(c2.booleanValue() && c1.booleanValue()))
                    {
                        sql = sql + "a.SujeitoID=" + (empresaId) + " AND ";
                    }

                    sql = sql + fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;
                    
                    if (fldKey == "a.RelacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize
                             + " a.RelacaoID, "
                             + "b.RecursoAbreviado as Estado, "
                             + "c.TipoRelacao, a.SujeitoID, d.Fantasia as Sujeito, a.ObjetoID, e.RecursoFantasia as Objeto, CONVERT(VARCHAR(50), SPACE(0)) AS fldError "
                             + "FROM RelacoesPesRec a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Recursos e WITH(NOLOCK), @TempTable z "
                             + "WHERE a.TipoRelacaoID = c.TipoRelacaoID AND a.EstadoID = b.RecursoID AND a.SujeitoID = d.PessoaID AND a.ObjetoID = e.RecursoID AND "
                             + fldKey + sOperator + textToSearch + " "
                             + condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RelacaoID "
                             + "AND z.IDTMP > " + startID
                             + " ORDER BY z.IDTMP";

                    break;

                case 1210:   //formID  /basico/Pessoas

                    string sDefinicaoParametros;
                    sAdditionalFrom_fn = "";
                    sAdditionalFrom = "";
                    sAdditionalWhere = "";
                    sStrTop = " TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " ";
                    sDefinicaoParametros = (fldKey == "a.Fantasia" ? "@TTS VARCHAR(128)" : "@TTS NVARCHAR(128)") + ", @UserID INT, @StartID INT, @PS INT, @EmpID INT ";

                    if (fldKey == "d.Numero")
                    {
                        sAdditionalFrom = " INNER JOIN Pessoas_Documentos d WITH(NOLOCK) ON ( a.PessoaID = d.PessoaID ) AND d.TipoDocumentoID IN(101,111) ";
                    }

                    if (filtroContextoId.intValue() == 1211)
                    {
                        sAdditionalWhere = " AND ((ISNULL(a.ClassificacaoID, 0) = 0) OR (a.ClassificacaoID IN (SELECT aa.ClassificacaoID FROM dbo.fn_Direitos_TiposAuxiliares_tbl (@UserID, @EmpID, GETDATE()) aa))) ";
                    }

                    condition = condition.Replace(">>", "<");

                    //Determinar tamanho dos parametros
                    parametrosSQL = new ProcedureParameters[7];
                    parametrosSQL[1] = new ProcedureParameters("@DefinicaoParametros", SqlDbType.NVarChar, sDefinicaoParametros, ParameterDirection.Input, 4000);
                    parametrosSQL[2] = new ProcedureParameters("@TTS", SqlDbType.VarChar, textToSearch.ToString(), ParameterDirection.Input, 128);
                    parametrosSQL[3] = new ProcedureParameters("@UserID", SqlDbType.Int, userId.intValue(), ParameterDirection.Input);
                    parametrosSQL[4] = new ProcedureParameters("@StartID", SqlDbType.Int, startID, ParameterDirection.Input);
                    parametrosSQL[5] = new ProcedureParameters("@PS", SqlDbType.Int, pageSize.intValue(), ParameterDirection.Input);
                    parametrosSQL[6] = new ProcedureParameters("@EmpID", SqlDbType.Int, empresaId.intValue(), ParameterDirection.Input);

                    sql = " SET NOCOUNT ON DECLARE @TempTable TABLE (IDTMP INT IDENTITY,IDFromPesq INT NOT NULL) ";

                    sql += " INSERT INTO @TempTable (IDFromPesq) SELECT " + sStrTop + " a.PessoaID "
                             + "FROM Pessoas a WITH(NOLOCK) "
                        //  + "LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON ( a.ClassificacaoID = e.ItemID ) " 
                             + sAdditionalFrom_fn + sAdditionalFrom
                             + "WHERE " + sAccessAllowed
                             + fldKey + sOperator + " @TTS "
                             + condition + sFiltroContexto + sFiltro + sAdditionalWhere;


                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.PessoaID, a.EstadoID ) = 1)";
                    }
                    
                    if (filtroId.intValue() == 41006)
                    {
                        sql = sql + " ORDER BY dtCadastro";
                    }
                    else if (fldKey == "a.PessoaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.PessoaID";
                    }

                    sql2 = " SELECT TOP ( @PS ) "
                              + "a.PessoaID,b.RecursoAbreviado as Estado,a.Nome,a.Fantasia,c.ItemAbreviado AS TipoPessoa, e.ItemMasculino AS Classificacao, MIN(f.dtFim) AS CredVenc, "
                              + "dbo.fn_Pessoa_Observacao(a.PessoaID) AS Observacao, dbo.fn_Pessoa_Localizacao(a.PessoaID) AS Localizacao, a.TipoPessoaID, "
                              + "(CASE WHEN @UserID = a.ProprietarioID THEN 1 WHEN @UserID = a.AlternativoID THEN 1 WHEN dbo.fn_Pessoa_Grupo(@UserID) = a.AlternativoID THEN 1 ELSE 0 END) AS Responsavel, "
                              + "dbo.fn_Pessoa_Cor(a.PessoaID, dbo.fn_Pessoa_Localidade(@EmpID, 1, NULL, NULL)) AS CorPessoa, CONVERT(VARCHAR(50), SPACE(0)) AS fldError "
                              + "FROM Pessoas a WITH(NOLOCK) "
                                + "LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON ( a.ClassificacaoID = e.ItemID ) "
                                + "INNER JOIN Recursos b WITH(NOLOCK) ON ( a.EstadoID = b.RecursoID ) "
                                + "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON ( a.TipoPessoaID = c.ItemID ) "
                                + "LEFT OUTER JOIN Creditos f WITH(NOLOCK) ON ( a.PessoaID = f.ParceiroID ) AND (f.EstadoID = 75) "
                                + "INNER JOIN @TempTable z ON ( z.IDFromPesq = a.PessoaID ) "
                              + sAdditionalFrom
                              + "WHERE " 
                              //+ fldKey + sOperator + " @TTS "
                              //+ condition + sFiltroContexto + sFiltro + sAdditionalWhere
                              //+ "AND z.IDTMP > @StartID "
                              + " z.IDTMP > @StartID "
                              + " GROUP BY a.PessoaID, b.RecursoAbreviado, a.Nome, a.Fantasia, c.ItemMasculino, e.ItemMasculino, a.TipoPessoaID, a.ProprietarioID, a.AlternativoID, c.ItemAbreviado, z.IDTMP "
                              + "ORDER BY z.IDTMP ";

                    sql += sql2;

                    parametrosSQL[0] = new ProcedureParameters("@sql", SqlDbType.NVarChar, sql, ParameterDirection.Input, 4000);
                    break;

                case 1220:   // formID  /basico/relacoes/relpessoas

                    sAdditionalFrom = "";
                    sAdditionalWhere = "";

                    if (fldKey == "h.Numero")
                    {
                        sAdditionalFrom = "INNER JOIN Pessoas_Documentos h WITH(NOLOCK) ON (a.SujeitoID = h.PessoaID) AND (h.TipoDocumentoID IN(101,111)) ";
                    }
                    else if (fldKey == "i.Numero")
                    {
                        sAdditionalFrom = "INNER JOIN Pessoas_Documentos i WITH(NOLOCK) ON (a.ObjetoID = i.PessoaID) AND (i.TipoDocumentoID IN(101,111)) ";
                    }

                    if (fldKey == "a.Faturamento")
                    {
                        fldKey = " CONVERT(NUMERIC(11,2), dbo.fn_Pessoa_Totaliza(a.SujeitoID, a.ObjetoID, 1, GETDATE()-10000, GETDATE(), 1)) ";
                        //    fldKey = "  ";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RelacaoID "
                             + "FROM RelacoesPessoas a WITH(NOLOCK) "
                                    + "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) "
                                    + "INNER JOIN TiposRelacoes c WITH(NOLOCK) ON (a.TipoRelacaoID = c.TipoRelacaoID) "
                                    + "INNER JOIN Pessoas d WITH(NOLOCK) ON (a.SujeitoID = d.PessoaID) "
                                    + "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ObjetoID = e.PessoaID) "
                                    + sAdditionalFrom
                             + "WHERE " + sAccessAllowed + " "
                             + fldKey + sOperator + textToSearch + " "
                             + condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.RelacaoID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.RelacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " "
                                    + "a.RelacaoID, b.RecursoAbreviado as Estado, c.TipoRelacao, "
                                    + "a.SujeitoID, d.Fantasia as Sujeito, a.ObjetoID, e.Fantasia as Objeto, "
                                    + "dbo.fn_RelPessoa_Classificacao(a.ObjetoID, a.SujeitoID, 2, GETDATE(), 1) AS C, "
                                    + "dbo.fn_RelPessoa_Classificacao(a.ObjetoID, a.SujeitoID, 0, GETDATE(), 1) AS I, "
                                    + "dbo.fn_RelPessoa_Classificacao(a.ObjetoID, a.SujeitoID, 1, GETDATE(), 1) AS E, "
                                    + "dbo.fn_Pessoa_Totaliza(a.SujeitoID, a.ObjetoID, 18, GETDATE()-10000, GETDATE(), 0) AS diasUltimaCompra, "
                                    + "dbo.fn_Pessoa_Credito(a.SujeitoID, 3, dbo.fn_Pessoa_Localidade(" + empresaId + ", 1, NULL, NULL)) AS LimiteCredito, "
                                    + "f.Fantasia AS Proprietario, g.Fantasia AS Alternativo, "
                                    + "dbo.fn_Empresa_Sistema(a.SujeitoID) AS nSujeitoEmpresaSistema, dbo.fn_Empresa_Sistema(a.ObjetoID) AS nObjetoEmpresaSistema, CONVERT(VARCHAR(50), SPACE(0)) AS fldError "
                                + "FROM RelacoesPessoas a WITH(NOLOCK) "
                                    + "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) "
                                    + "INNER JOIN TiposRelacoes c WITH(NOLOCK) ON (a.TipoRelacaoID = c.TipoRelacaoID) "
                                    + "INNER JOIN Pessoas d WITH(NOLOCK) ON (a.SujeitoID = d.PessoaID) "
                                    + "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ObjetoID = e.PessoaID) "
                                    + "INNER JOIN Pessoas f WITH(NOLOCK) ON (a.ProprietarioID = f.PessoaID) "
                                    + "INNER JOIN Pessoas g WITH(NOLOCK) ON (a.AlternativoID = g.PessoaID) "
                                    + "INNER JOIN @TempTable z ON (z.IDFromPesq = a.RelacaoID) "
                                    + sAdditionalFrom
                                + "WHERE "
                                    + fldKey + sOperator + textToSearch + " "
                                    + condition + sFiltroContexto + sFiltro + " "
                                    + "AND z.IDTMP > " + startID + " "
                                + "ORDER BY z.IDTMP";

                    break;

                case 5240:   //formID  /basico/visitas

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) + " a.VisitaID "
                             + "FROM Visitas a WITH(NOLOCK) LEFT JOIN Pessoas c WITH(NOLOCK) ON a.PessoaID=c.PessoaID "
                             + "LEFT JOIN Prospect g WITH(NOLOCK) ON a.ProspectID=g.ProspectID "
                             + "WHERE a.EmpresaID = " + empresaId + " AND " + sAccessAllowed
                             + fldKey + sOperator + textToSearch + " "
                             + condition + sFiltroContexto + sFiltro;


                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.VisitaID, a.EstadoID ) = 1)";
                    }
                    if (fldKey == "a.VisitaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.VisitaID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " a.VisitaID, b.RecursoAbreviado as Estado, d.ItemMasculino AS TipoVisita, e.ItemMasculino AS TipoVisitado, "
                        + "c.Fantasia AS Pessoa, g.Fantasia AS Prospect, a.dtVisita, a.dtVencimento, a.Cadastro, a.Pesquisa, f.Fantasia AS Vendedor,a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError "
                            + "FROM Visitas a WITH(NOLOCK) INNER JOIN Recursos b WITH(NOLOCK) ON a.EstadoID=b.RecursoID "
                                + "LEFT JOIN Pessoas c WITH(NOLOCK) ON a.PessoaID=c.PessoaID "
                                + "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.TipoVisitaID=d.ItemID "
                                + "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON a.TipoVisitadoID=e.ItemID "
                                + "INNER JOIN Pessoas f WITH(NOLOCK) ON a.ProprietarioID=f.PessoaID "
                                + "LEFT JOIN Prospect g WITH(NOLOCK) ON a.ProspectID=g.ProspectID "
                                + "INNER JOIN @TempTable z ON z.IDFromPesq=a.VisitaID "
                            + "WHERE " + fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro
                            + " AND z.IDTMP > " + startID
                            + " ORDER BY z.IDTMP";

                    break;

                case 1310: // formID  /basico/tabelasaux/tiposaux

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) + " a.TipoID "
                             + "FROM TiposAuxiliares a WITH(NOLOCK) WHERE " + sAccessAllowed
                             + fldKey + sOperator + textToSearch + " "
                             + condition + sFiltroContexto + sFiltro;


                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.TipoID, a.EstadoID ) = 1)";
                    }
 
                   if (fldKey == "a.TipoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.TipoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " a.TipoID,b.RecursoAbreviado as Estado,a.Tipo,a.Feminino,a.Abreviacao, "
                        + "a.Numero,a.Filtro,a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError "
                            + "FROM TiposAuxiliares a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z WHERE a.EstadoID=b.RecursoID AND "
                            + fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro
                            + " AND z.IDFromPesq=a.TipoID AND z.IDTMP > " + startID
                            + " ORDER BY z.IDTMP";

                    break;

                case 1320:   // formID  /basico/tabelasaux/tiposrel

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.TipoRelacaoID " +
                             "FROM TiposRelacoes a WITH(NOLOCK) WHERE " + sAccessAllowed +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.TipoRelacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.TipoRelacaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                              "a.TipoRelacaoID,b.RecursoAbreviado as Estado,a.TipoRelacao,c.ItemMasculino AS RelacaoEntre,d.ItemMasculino AS TipoInferencia, a.Sujeito, a.Objeto, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                              "FROM TiposRelacoes a WITH(NOLOCK),Recursos b WITH(NOLOCK),TiposAuxiliares_Itens c WITH(NOLOCK),TiposAuxiliares_Itens d WITH(NOLOCK), @TempTable z " +
                              "WHERE a.EstadoID=b.RecursoID AND a.RelacaoEntreID = c.ItemID AND a.TipoInferenciaID = d.ItemID AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.TipoRelacaoID " +
                              "AND z.IDTMP > " + startID +
                              " ORDER BY z.IDTMP";

                    break;


                case 1330:   //formID  /basico/Localidades

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.LocalidadeID " +
                             "FROM Localidades a  WITH(NOLOCK) WHERE " + sAccessAllowed +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.LocalidadeID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.LocalidadeID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " a.LocalidadeID,b.RecursoAbreviado as Estado, " +
                             "a.Localidade,c.ItemMasculino as TipoLocalidade, a.CodigoLocalidade2, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Localidades a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID=b.RecursoID AND " +
                             "a.TipoLocalidadeID = c.ItemID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.LocalidadeID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 1360:   //formID /modbasico/submodauxiliar/dicionario

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.TermoID " +
                             "FROM Dicionario a WITH(NOLOCK) WHERE " + sAccessAllowed +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.TermoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.TermoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " a.TermoID, b.RecursoAbreviado as Estado, a.Termo, c.ItemMasculino AS Idioma, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Dicionario a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z WHERE a.EstadoID=b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro + " " +
                             "AND a.EstadoID=b.RecursoID AND a.IdiomaID=c.ItemID AND z.IDFromPesq=a.TermoID AND z.IDTMP > " + startID + " " +
                             "ORDER BY z.IDTMP";

                    break;

                case 1340:   //formID /modbasico/submodauxiliar/termoacordo

                    if ((returnFldType("termoAcordo", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) " +
                            "SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.TermoAcordoID " +
                                "FROM TermoAcordo a WITH(NOLOCK) WHERE " + sAccessAllowed +
                                 fldKey + sOperator + textToSearch + " " +
                                 condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.TermoAcordoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.TermoAcordoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " a.TermoAcordoID, b.RecursoAbreviado as Estado, c.ItemId, c.ItemMasculino AS Tipo, " +
                                    " a.Observacao, c.EstadoID AS EstadoID, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                "FROM TermoAcordo a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                                "WHERE  a.EmpresaID IN " +
                                        "(SELECT " + (empresaId) + " " +
                                        " UNION ALL SELECT EmpresaAlternativaID FROM FopagEmpresas WITH(NOLOCK) WHERE (EmpresaID = " + (empresaId) + " AND " +
                                        "FuncionarioID IS NULL) AND (EmpresaAlternativaID = dbo.fn_Empresa_Matriz(EmpresaAlternativaID, 1, 1))) AND  " +
                                        " a.EstadoID=b.RecursoID AND " + fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro + " " +
                                        "AND a.EstadoID=b.RecursoID AND c.ItemID = a.TipoTermoAcordoID AND z.IDFromPesq = a.TermoAcordoID AND z.IDTMP > " + startID + " " +
                                        "ORDER BY z.IDTMP ";

                    break;

                case 1350:   //formID /modbasico/submodauxiliar/feriados

                    if ((returnFldType("feriados", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) " +
                            "SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.FeriadoID " +
                                "FROM Feriados a WITH(NOLOCK) WHERE " + sAccessAllowed +
                                 fldKey + sOperator + textToSearch + " " +
                                 condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.FeriadoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.FeriadoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " a.FeriadoID, b.RecursoAbreviado as Estado, a.Feriado, c.ItemFeminino AS Tipo, " +
                                    " a.Observacao, c.EstadoID AS EstadoID, CONVERT(VARCHAR(50), SPACE(0)) AS fldError, dbo.fn_Feriado_OK(a.FeriadoID, GETDATE()) AS FeriadoOK " +
                                "FROM Feriados a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                                "WHERE  a.EstadoID=b.RecursoID AND " + fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro + " " +
                                        "AND a.EstadoID=b.RecursoID AND c.ItemID = a.TipoFeriadoID AND z.IDFromPesq = a.FeriadoID AND z.IDTMP > " + startID + " " +
                                        "ORDER BY z.IDTMP ";

                    break;

                case 2110:   //formID  /pcm/conceitos                                     

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.ConceitoID " +
                             "FROM Conceitos a WITH(NOLOCK) " +
                             "WHERE " + sAccessAllowed +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.ConceitoID, a.EstadoID ) = 1)";
                    }         
                    if (fldKey == "a.ConceitoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.ConceitoID";
                    }
                    sql2 = "SELECT TOP " + pageSize + " " +
                                 "a.ConceitoID,b.RecursoAbreviado AS Estado," + "a.Conceito,c.ItemMasculino as TipoConceito, CONVERT(VARCHAR(50), SPACE(0)) AS fldError, " +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Descricao2(a.ConceitoID," + (idiomaEmpresaId) + ", 11) AS Descricao, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("a.Internacional, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("a.DescricaoAutomatica, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Conformidade(a.ConceitoID,0) AS Conf, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Conformidade(a.ConceitoID,1) AS Carac, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Conformidade(a.ConceitoID,2) AS AV, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Conformidade(a.ConceitoID,3) AS Imagens, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Conformidade(a.ConceitoID,4) AS PM, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Conformidade(a.ConceitoID,5) AS Ident, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Conformidade(a.ConceitoID,6) AS Espec, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2112) ? ("dbo.fn_Produto_Conformidade(a.ConceitoID,7) AS BF, ") : ("")) +
                                 ((filtroContextoId.intValue() == 2115) ? "a.Imposto," : "") +
                                 "LEFT(CONVERT(VARCHAR(MAX), a.Observacao), 100) AS Observacao, " +
                                 "LEFT(CONVERT(VARCHAR(MAX), a.Observacoes), 100) AS Observacoes, d.Fantasia AS Proprietario, e.Fantasia AS Alternativo " +
                             "FROM Conceitos a WITH(NOLOCK) " +
                                 " INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) "+
                                 " INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = a.TipoConceitoID) " +
                                 " INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.ProprietarioID) " +
                                 " INNER JOIN Pessoas e WITH(NOLOCK) ON (e.PessoaID = a.AlternativoID) " +
                                 " INNER JOIN @TempTable z ON (z.IDFromPesq = a.ConceitoID) " +
                             "WHERE "+ fldKey + sOperator + textToSearch + " " +
                                condition + sFiltroContexto + sFiltro + "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 2120:  //formID  /basico/relacoes/relconceitos

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RelacaoID " +
                             "FROM RelacoesConceitos a WITH(NOLOCK) ,TiposRelacoes b WITH(NOLOCK) ,Conceitos c WITH(NOLOCK) ,Conceitos d WITH(NOLOCK) ,Recursos e WITH(NOLOCK) WHERE " + sAccessAllowed +
                             "a.TipoRelacaoID=b.TipoRelacaoID AND a.SujeitoID=c.ConceitoID AND " +
                             "a.ObjetoID=d.ConceitoID AND a.EstadoID=e.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.RelacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.RelacaoID, e.RecursoAbreviado AS Estado, b.TipoRelacao AS Relacao, a.SujeitoID, c.Conceito AS Sujeito, a.ObjetoID, d.Conceito AS Objeto, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM RelacoesConceitos a WITH(NOLOCK),TiposRelacoes b WITH(NOLOCK),Conceitos c WITH(NOLOCK),Conceitos d WITH(NOLOCK),Recursos e WITH(NOLOCK), @TempTable z " +
                             "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.SujeitoID=c.ConceitoID AND a.ObjetoID=d.ConceitoID AND a.EstadoID=e.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RelacaoID " +
                             "AND z.IDTMP >= " + startID +
                             " ORDER BY z.IDTMP";

                    break;


                case 2130:  // formID  /basico/relacoes/relpessoaseconceitos
                    {
                        // Contexto de Compra e Venda tem filtro permanente
                        if (filtroContextoId.intValue() == 2131)
                        {
                            strAdditionalFrom1 = "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON Marcas.ConceitoID = e.MarcaID " +
                                                 "INNER JOIN Conceitos Familias WITH(NOLOCK) ON Familias.ConceitoID = e.ProdutoID " +
                                                 "INNER JOIN RelacoesConceitos RelConceitos2 WITH(NOLOCK) ON RelConceitos2.SujeitoID = Familias.ConceitoID " +
                                                 "INNER JOIN Conceitos Conceitos2 WITH(NOLOCK) ON Conceitos2.ConceitoID = RelConceitos2.ObjetoID " +
                                                 "INNER JOIN RelacoesConceitos RelConceitos1 WITH(NOLOCK) ON RelConceitos1.SujeitoID = Conceitos2.ConceitoID " +
                                                 "INNER JOIN Conceitos Conceitos1 WITH(NOLOCK) ON Conceitos1.ConceitoID = RelConceitos1.ObjetoID " +
                                                 "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(aa.RelacaoID, NULL, 1, GETDATE()), 1)) AS ProdutoInconsistencia, aa.RelacaoID AS _RelacaoID " +
				                                        "FROM  RelacoesPesCon aa WITH(NOLOCK)) AS ProdutosInconsistentes ON (ProdutosInconsistentes._RelacaoID = a.RelacaoID) " +
		                                        "INNER JOIN (SELECT dbo.fn_Produto_Estoque(bb.SujeitoID, bb.ObjetoID, 341, NULL, GETDATE(), NULL, NULL, NULL) AS Estoque, bb.RelacaoID AS _RelacaoID "+
				                                        "FROM RelacoesPesCon bb WITH(NOLOCK)) AS Estoque ON (Estoque._RelacaoID = a.RelacaoID) " +
		                                        "INNER JOIN (SELECT dbo.fn_Produto_Comprar(cc.SujeitoID, cc.ObjetoID, NULL, GETDATE(), 10) AS Compras, cc.RelacaoID AS _RelacaoID "+
				                                        "FROM  RelacoesPesCon cc WITH(NOLOCK)) AS ComprasPendentes ON (ComprasPendentes._RelacaoID = a.RelacaoID) " +
		                                        "INNER JOIN (SELECT dbo.fn_Produto_Comprar(dd.SujeitoID, dd.ObjetoID, NULL, GETDATE(), 20) AS Excesso, dd.RelacaoID AS _RelacaoID "+
				                                        "FROM  RelacoesPesCon dd WITH(NOLOCK)) AS EstoqueExcesso ON (EstoqueExcesso._RelacaoID = a.RelacaoID) " +
		                                        "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ee.RelacaoID, 10, 1, GETDATE()), 1)) AS EstadoInconsistencia, ee.RelacaoID AS _RelacaoID "+
				                                        "FROM  RelacoesPesCon ee WITH(NOLOCK)) AS EstadosInconsistentes ON (EstadosInconsistentes._RelacaoID = a.RelacaoID) "+
		                                        "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ff.RelacaoID, 20, 1, GETDATE()), 1)) AS CVPInconsistencia, ff.RelacaoID AS _RelacaoID "+
				                                        "FROM  RelacoesPesCon ff WITH(NOLOCK)) AS CVPInconsistentes ON (CVPInconsistentes._RelacaoID = a.RelacaoID) "+
		                                        "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(gg.RelacaoID, 30, 1, GETDATE()), 1)) AS PVInconsistencia, gg.RelacaoID AS _RelacaoID "+
				                                        "FROM  RelacoesPesCon gg WITH(NOLOCK)) AS PVInconsistentes ON (PVInconsistentes._RelacaoID = a.RelacaoID) "+
		                                        "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(hh.RelacaoID, 40, 1, GETDATE()), 1)) AS MCInconsistencia, hh.RelacaoID AS _RelacaoID "+
				                                        "FROM  RelacoesPesCon hh WITH(NOLOCK)) AS MCInconsistentes ON (MCInconsistentes._RelacaoID = a.RelacaoID) "+
		                                        "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ii.RelacaoID, 50, 1, GETDATE()), 1)) AS InternetInconsistencia, ii.RelacaoID AS _RelacaoID "+
				                                        "FROM  RelacoesPesCon ii WITH(NOLOCK)) AS InternetInconsistentes ON (InternetInconsistentes._RelacaoID = a.RelacaoID) "+
		                                        "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(jj.RelacaoID, 60, 1, GETDATE()), 1)) AS PagamentoInconsistencia, jj.RelacaoID AS _RelacaoID "+
				                                        "FROM  RelacoesPesCon jj WITH(NOLOCK)) AS PagamentosInconsistentes ON (PagamentosInconsistentes._RelacaoID = a.RelacaoID) "+
                                                "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(kk.RelacaoID, 70, 1, GETDATE()), 1)) AS CustosInconsistencia, kk.RelacaoID AS _RelacaoID " +
				                                        "FROM  RelacoesPesCon kk WITH(NOLOCK)) AS CustosInconsistentes ON (CustosInconsistentes._RelacaoID = a.RelacaoID) ";

                            strAdditionalCondition1 = " AND Familias.EstadoID = 2 AND " +
                                                        "RelConceitos2.TipoRelacaoID = 41 AND RelConceitos2.EstadoID = 2 AND " +
                                                        "Conceitos2.EstadoID = 2 AND " +
                                                        "RelConceitos1.TipoRelacaoID = 41 AND " +
                                                        "RelConceitos1.EstadoID = 2 AND " +
                                                        "Conceitos1.EstadoID = 2 ";

                            strAdditionalCondition2 = " AND ((e.Conceito LIKE " + textToSearch + " ) OR " +
                                                        "(dbo.fn_Tradutor(Familias.Conceito, " + idiomaSistemaId + ", " + idiomaEmpresaId + ", NULL) LIKE " + textToSearch + " ) OR " +
                                                        "(dbo.fn_Tradutor(Conceitos1.Conceito, " + idiomaSistemaId + ", " + idiomaEmpresaId + ", NULL) LIKE " + textToSearch + " ) OR " +
                                                        "(dbo.fn_Tradutor(conceitos2.Conceito, " + idiomaSistemaId + ", " + idiomaEmpresaId + ", NULL) LIKE " + textToSearch + " ) OR " +
                                                        "(Marcas.Conceito LIKE " + textToSearch + " ) OR " +
                                                        "(e.Modelo LIKE " + textToSearch + " ) OR " +
                                                        "(e.Descricao LIKE " + textToSearch + " ) OR " +
                                                        "(e.PartNumber LIKE " + textToSearch + " ) OR " +
                                                        "(e.ConceitoID LIKE " + textToSearch + " ) ) ";

                            sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) + " a.RelacaoID " +
                                "FROM RelacoesPesCon a WITH(NOLOCK) " +
                                    "INNER JOIN Recursos b WITH(NOLOCK) ON b.RecursoID = a.EstadoID " +
                                    "INNER JOIN TiposRelacoes c WITH(NOLOCK) ON c.TipoRelacaoID = a.TipoRelacaoID " +
                                    "INNER JOIN Pessoas d WITH(NOLOCK) ON d.PessoaID  = a.SujeitoID " +
                                    "INNER JOIN Conceitos e WITH(NOLOCK) ON e.ConceitoID = a.ObjetoID ";

                            sql = sql + strAdditionalFrom1;
                            
                            /*
                            if ((sOperator.Trim() == "LIKE") && (fldKey.Trim() == "e.Conceito") && (textToSearch.Replace("%", "").Replace("''", "") != string.Empty))
                            {
                                sql = sql + strAdditionalFrom1;
                            }
                            */

                            sql = sql + " WHERE " + sAccessAllowed + 
                                     "a.SujeitoID = " + empresaId + " ";

                            sql = sql + strAdditionalCondition1;

                            if (textToSearch.Replace("%", "").Replace("''", "") != string.Empty)
                            {
                                if ((sOperator.Trim() == "LIKE") && (fldKey.Trim() == "e.Conceito"))
                                    sql = sql + strAdditionalCondition2;
                                else
                                    sql = sql + " AND " + fldKey + sOperator + textToSearch + " ";
                            }

                            sql = sql + condition + sFiltroContexto + sFiltro;
                        }

                        else
                        {
                            sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) + " a.RelacaoID " +
                                "FROM RelacoesPesCon a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK) " +
                                "WHERE " + sAccessAllowed + "a.EstadoID = b.RecursoID AND a.TipoRelacaoID = c.TipoRelacaoID AND " +
                                "a.SujeitoID = d.PessoaID AND a.ObjetoID = e.ConceitoID AND " +
                                fldKey + sOperator + textToSearch + " " +
                                condition + sFiltroContexto + sFiltro;
                        }

                        //Registros Vencidos 
                        if (RegistrosVencidos.booleanValue() == true)
                        {
                            sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.RelacaoID, a.EstadoID ) = 1)";
                        }

                        if (fldKey == "a.RelacaoID")
                            sql = sql + " ORDER BY " + fldKey + " " + order;
                        else
                            sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";

                        //Contexto de Compra e Venda tem filtro permanente
                        if (filtroContextoId.intValue() == 2131)
                        {
                            sql2 = "SELECT TOP  " + pageSize +
                                     " a.RelacaoID,b.RecursoAbreviado AS Estado, c.TipoRelacao, a.SujeitoID, d.Fantasia AS Sujeito, a.ObjetoID, dbo.fn_Produto_Descricao2(a.ObjetoID, " + (idiomaEmpresaId) + ", 11) AS Objeto, "
                                     + " f.Fantasia as Prop, g.Fantasia as Alt , CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                     "FROM RelacoesPesCon a WITH(NOLOCK) " +
                                        "INNER JOIN Recursos b WITH(NOLOCK) ON b.RecursoID = a.EstadoID " +
                                        "INNER JOIN TiposRelacoes c WITH(NOLOCK) ON c.TipoRelacaoID = a.TipoRelacaoID " +
                                        "INNER JOIN Pessoas d WITH(NOLOCK) ON d.PessoaID = a.SujeitoID  " +
                                        "INNER JOIN Conceitos e WITH(NOLOCK) ON e.ConceitoID = a.ObjetoID  " +
                                        "INNER JOIN Pessoas f ON f.PessoaID =  a.ProprietarioID " +
                                        "INNER JOIN Pessoas g ON g.PessoaID = a.AlternativoID " +
                                        "INNER JOIN @TempTable z ON z.IDFromPesq=a.RelacaoID  ";
                            /*
                            if ((sOperator.Trim() == "LIKE") && (fldKey == "e.Conceito"))
                                sql2 = sql2 + strAdditionalFrom1;
                            */
                            sql2 = sql2 + " WHERE a.SujeitoID = " + empresaId;
                            /*
                            if ((sOperator.Trim() == "LIKE") && (fldKey == "e.Conceito"))
                                sql2 = sql2 + strAdditionalCondition1;
                            else
                                sql2 = sql2 + " AND " + fldKey + sOperator + textToSearch + " ";
                            */

                            sql2 = sql2 + condition + sFiltroContexto + "AND z.IDTMP > " + startID +
                                     " ORDER BY z.IDTMP";
                        }
                        else
                            sql2 = "SELECT TOP " + pageSize +
                                     " a.RelacaoID,b.RecursoAbreviado AS Estado, c.TipoRelacao, a.SujeitoID, d.Fantasia AS Sujeito, a.ObjetoID, e.Conceito AS Objeto, "+ 
                                     " f.Fantasia as Prop, g.Fantasia as Alt , "+ 
                                     " CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                     "FROM RelacoesPesCon a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK),  Pessoas f, Pessoas g,  @TempTable z " +
                                     "WHERE a.EstadoID = b.RecursoID AND a.TipoRelacaoID = c.TipoRelacaoID AND " +
                                     "a.SujeitoID = d.PessoaID AND a.ObjetoID = e.ConceitoID AND a.ProprietarioID = f.PessoaID AND a.AlternativoID = g.PessoaID AND " +
                                     fldKey + sOperator + textToSearch + " " +
                                     condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RelacaoID " +
                                     "AND z.IDTMP > " + startID +
                                     " ORDER BY z.IDTMP";
                    }
                    break;

                case 2210: // formID  /modmateriais/submodestoques/localizacoes

                    if (fldKey == "a.LocalizacaoCodificada")
                    {
                        fldKey = " dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) ";
                    }
                    else if (fldKey == "a.LocalizacaoMae")
                    {
                        fldKey = " dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoMaeID, 1) ";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) + " a.LocalizacaoID " +
                             "FROM LocalizacoesEstoque a  WITH(NOLOCK),Recursos b WITH(NOLOCK) WHERE " + sAccessAllowed +
                             "a.EmpresaID = " + empresaId + " AND " +
                             "a.EstadoID=b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.LocalizacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.LocalizacaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.LocalizacaoID AS [ID], b.RecursoAbreviado AS Est, a.Localizacao AS [Localiza��o], a.Codigo, dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) AS [Loc Codificada], " +
                             "dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoMaeID, 1) AS [Localiza��o M�e], " +
                             "dbo.fn_LocalizacaoEstoque_Nivel(a.LocalizacaoID) AS [N�vel], " +
                             "dbo.fn_LocalizacaoEstoque_Util(a.LocalizacaoID) AS LU, " +
                             "a.TamanhoMinimo, a.TamanhoMaximo, a.EstoqueSeguro, a.Palete AS Palete, a.ReposicaoDiaria AS RD, " +
                             "a.ProdutosDistintos AS ProdutosDistintos, a.PaletesAdicionais AS PaletesAdicionais, " +
                             "a.Observacao AS [Observa��o], CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM LocalizacoesEstoque a  WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EmpresaID = " + empresaId + " AND a.EstadoID=b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.LocalizacaoID " +
                             "AND z.IDTMP >= " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                //SSO.: 23/12/2010
                case 5410: // formID  /modcomercial/submodtransporte/DocumentosTransporte

                    if (fldKey == "a.dtEmissao" && textToSearch == "'0'")
                        textToSearch = "CONVERT(INT," + textToSearch + ")";

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.DocumentoTransporteID " +
                                "FROM DocumentosTransporte a WITH(NOLOCK) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) " +
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.TransportadoraID) " +
                                (fldKey == "d.CodigoErro" ? "INNER JOIN TransportadoraDados_Detalhes_Erros d WITH(NOLOCK) ON (d.RegistroID = a.DocumentoTransporteID) " : " ") +
                                (fldKey == "f.NotaFiscal" ? "INNER JOIN DocumentosTransporte_NotasFiscais e WITH(NOLOCK) ON (e.DocumentoTransporteID = a.DocumentoTransporteID) " +
                                        "INNER JOIN NotasFiscais f WITH(NOLOCK) ON (f.NotaFiscalID = e.NotaFiscalID) " : " ") +
                                "WHERE " + sAccessAllowed +
                                "c.PessoaID = a.TransportadoraID AND " +
                                "a.EmpresaID = " + empresaId + " AND " +
                                "a.EstadoID=b.RecursoID AND " +
                                fldKey + ((fldKey == "d.CodigoErro" || fldKey == "f.NotaFiscal") ? " = " : sOperator) + textToSearch + " " +
                                condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.DocumentoTransporteID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.DocumentoTransporteID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + ", a.DocumentoTransporteID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                                "a.DocumentoTransporteID AS [ID], " +
                                "b.RecursoAbreviado AS [Est], " +
                                "a.Numero, " +
                                "a.dtEmissao AS [Emissao], " +
                        //"a.TransportadoraID, " +
                                "c.Fantasia AS [Transportadora], " +
                                "a.ValorTotal AS [Valor Total], " +
                                "CONVERT(VARCHAR(50), SPACE(0)) AS [fldError] " +
                                "FROM DocumentosTransporte a WITH (NOLOCK) " +
                                    "INNER JOIN Recursos b WITH (NOLOCK) ON (b.RecursoID = a.EstadoID) " +
                                    "INNER JOIN Pessoas c WITH (NOLOCK) ON (c.PessoaID = a.TransportadoraID) " +
                                    (fldKey == "d.CodigoErro" ? "INNER JOIN TransportadoraDados_Detalhes_Erros d WITH(NOLOCK) ON (d.RegistroID = a.DocumentoTransporteID) " : " ") +
                                    (fldKey == "f.NotaFiscal" ? "INNER JOIN DocumentosTransporte_NotasFiscais e WITH(NOLOCK) ON (e.DocumentoTransporteID = a.DocumentoTransporteID) " +
                                        "INNER JOIN NotasFiscais f WITH(NOLOCK) ON (f.NotaFiscalID = e.NotaFiscalID) " : " ") +
                                    "INNER JOIN @TempTable z ON (z.IDFromPesq = a.DocumentoTransporteID) " +
                                "WHERE a.EmpresaID = " + empresaId + " AND " + fldKey + ((fldKey == "d.CodigoErro" || fldKey == "f.NotaFiscal") ? " = " : sOperator) +
                                textToSearch + " " + condition + sFiltroContexto + sFiltro + (fldKey == "d.CodigoErro" ? " AND d.TipoRegistroID = 1901 " : " ") +
                                "AND z.IDTMP >= " + startID +
                                " ORDER BY z.IDTMP";
                    break;

                //

                case 3140: // formID  /modestrategico/subplanejamentoestrategico/orcamentos

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.OrcamentoID " +
                             "FROM Orcamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK) WHERE " + sAccessAllowed +
                             "a.EmpresaID = " + empresaId + " AND " +
                             "a.EstadoID=b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.OrcamentoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + ", a.OrcamentoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.OrcamentoID AS [ID], b.RecursoAbreviado AS Est, a.Ano, " +
                             "a.Observacao AS [Observa��o], CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Orcamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EmpresaID = " + empresaId + " AND a.EstadoID=b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.OrcamentoID " +
                             "AND z.IDTMP >= " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 4210:   // formID  /modmarketing/subinformacoesmercado/vitrines

                    if ((returnFldType("Vitrines", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) + " a.VitrineID " +
                             "FROM Vitrines a WITH(NOLOCK) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.LojaID=d.ItemID) " +
                             "WHERE " + sAccessAllowed +
                             "a.EmpresaID = " + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.VitrineID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.VitrineID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                            "a.VitrineID, b.RecursoAbreviado AS Est, a.Vitrine, c.ItemMasculino AS Tipo, d.ItemFeminino AS LojaEspecial, e.ItemFeminino AS Loja, " +
                            "f.Conceito AS Marca, a.EhRandomica AS Rand, a.dtInicio, a.dtFim, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                        "FROM Vitrines a WITH(NOLOCK) " +
                            "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID=b.RecursoID) " +
                            "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.TipoVitrineID=c.ItemID) " +
                            "LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.LojaEspecialID=d.ItemID) " +
                            "LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.LojaID=e.ItemID) " +
                            "LEFT OUTER JOIN Conceitos f WITH(NOLOCK) ON (a.MarcaID=f.ConceitoID) " +
                            "INNER JOIN @TempTable z ON (z.IDFromPesq=a.VitrineID) " +
                        "WHERE a.EmpresaID = " + empresaId + " AND " +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " " +
                            "AND z.IDTMP > " + startID +
                        " ORDER BY z.IDTMP";

                    break;

                case 4220:   // Acoesmarketing

                    if (fldKey == "a.dtVencimento")
                    {
                        fldKey = "dbo.fn_AcaoMarketing_Datas(a.AcaoID,1)";

                        if (textToSearch == "'0'")
                        {
                            textToSearch = "0";
                        }

                    }
                    else if ((returnFldType("AcoesMarketing", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.AcaoID " +
                             "FROM AcoesMarketing a WITH(NOLOCK) INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON a.TipoAcaoID=c.ItemID WHERE " + sAccessAllowed +
                             "a.EmpresaID = " + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;


                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.AcaoID, a.EstadoID ) = 1)";
                    }
                    
                    if (fldKey == "a.AcaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.AcaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                              "a.AcaoID, b.RecursoAbreviado AS Est, c.Observacao AS [Grupo de Acao], c.ItemMasculino AS [Tipo de Acao], " +
                              "a.Acao, a.Detalhe, a.Periodo, a.dtInicio, a.dtFim, dbo.fn_Acaomarketing_Datas(a.AcaoID,1) AS dtVencimento, dbo.fn_AcaoMarketing_Totais(AcaoID, 11, GETDATE()) AS Atraso, " +
                              "a.TotalPlanejado, " +
                              "dbo.fn_AcaoMarketing_Totais(AcaoID, 1, GETDATE()) AS TotalPatrocinio, " +
                              "dbo.fn_AcaoMarketing_Totais(AcaoID, 2, GETDATE()) AS Diferenca, " +
                              "dbo.fn_AcaoMarketing_Totais(AcaoID, 3, GETDATE()) AS TotalPago, " +
                              "dbo.fn_AcaoMarketing_Totais(AcaoID, 4, GETDATE()) AS TotalRecebido, " +
                              "dbo.fn_AcaoMarketing_Totais(AcaoID, 5, GETDATE()) AS SaldoPlanejado, " +
                              "dbo.fn_AcaoMarketing_Totais(AcaoID, 6, GETDATE()) AS SaldoPatrocinio, " +
                              "dbo.fn_AcaoMarketing_Totais(AcaoID, 7, GETDATE()) AS SaldoEfetivo, a.Observacao AS Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                              "FROM AcoesMarketing a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                              "WHERE a.EmpresaID = " + empresaId + " AND a.EstadoID=b.RecursoID AND a.TipoAcaoID=c.ItemID AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.AcaoID " +
                              "AND z.IDTMP > " + startID +
                              " ORDER BY z.IDTMP";

                    break;

                case 4230:   // EmailMarketing

                    if ((returnFldType("EmailMarketing", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.EmailID " +
                             "FROM EmailMarketing a WITH(NOLOCK) INNER JOIN Vitrines b WITH(NOLOCK) ON (b.VitrineID = a.VitrineID) WHERE " + sAccessAllowed +
                             "a.EmpresaID = " + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.EmailID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.EmailID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + ", a.EmailID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                              "a.EmailID, b.RecursoAbreviado AS Est, a.ProcessoIniciado, a.Assunto, a.dtEnvio, a.dtValidade, c.Vitrine AS [Vitrine], " +
                              "d.ItemMasculino AS [Template], a.PercentualProdutosCompativeis, a.TamanhoArgumentoVenda, " +
                              "a.QuantidadeProdutos AS Produtos, " +
                              "dbo.fn_EmailMarketing_Totais(a.EmailID, NULL, 2) AS Clientes, " +
                              "dbo.fn_EmailMarketing_Totais(a.EmailID, NULL, 3) AS Contatos, " +
                              "a.QuantidadeEmail AS Emails, " +
                              "dbo.fn_EmailMarketing_Totais(a.EmailID, NULL, 6) AS PercentualProcessamento, " +
                              "a.Observacao AS Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                              "FROM EmailMarketing a WITH(NOLOCK) " +
                                    "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) " +
                                    "INNER JOIN Vitrines c WITH(NOLOCK) ON (c.VitrineID = a.VitrineID) " +
                                    "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (d.ItemID = a.TemplateID) " +
                                    "INNER JOIN @TempTable z ON (z.IDFromPesq = a.EmailID) " +
                              "WHERE a.EmpresaID = " + empresaId + " AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro + " AND z.IDTMP > " + startID +
                              " ORDER BY z.IDTMP";

                    break;

                case 4240: // formID  /modmarketing/subcampanhas/publicacoes

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.PublicacaoID " +
                             "FROM Publicacoes a WITH(NOLOCK), Recursos b WITH(NOLOCK) WHERE " + sAccessAllowed +
                             "a.EstadoID=b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.PublicacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + ", a.PublicacaoID";
                    }
                    
                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.PublicacaoID AS [ID], b.RecursoAbreviado AS Est, c.ItemMasculino AS Tipo, a.PublicadoID, a.Titulo AS [T�tulo], a.Recorrencia AS [Recorrr�ncia], " +
                             "CONVERT(VARCHAR(10), a.dtInicio, 103) + SPACE(1) + CONVERT(VARCHAR(10), a.dtInicio, 108) AS [In�cio], " +
                             "CONVERT(VARCHAR(10), a.DtFim, 103) + SPACE(1) + CONVERT(VARCHAR(10), a.DtFim, 108) AS Fim, " +
                             "CONVERT(VARCHAR(10), a.dtData, 103) + SPACE(1) + CONVERT(VARCHAR(10), a.dtData, 108) AS [Data], a.Notificacao AS [Not], " +
                             "a.IgnorarOcultos AS IO, a.IgnorarRepercucao AS IR, a.Observacao AS Observa��o, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Publicacoes a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c , @TempTable z " +
                             "WHERE a.EstadoID=b.RecursoID AND " +
                                    "c.ItemID = a.TipoPublicacaoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.PublicacaoID " +
                             "AND z.IDTMP >= " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 4410:   // formID  /modmarketing/subinformacoesmercado/novidades

                    if ((returnFldType("Novidades", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    // Contexto de Press-Release tem filtro permanente
                    if (filtroContextoId.intValue() == 4411)
                    {
                        sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.NovidadeID " +
                                 "FROM Novidades a WITH(NOLOCK) WHERE " + sAccessAllowed +
                                 "a.EmpresaID = " + empresaId + " AND " +
                                 fldKey + sOperator + textToSearch + " " +
                                 condition + sFiltroContexto + sFiltro;
                    }
                    else
                    {
                        sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.NovidadeID " +
                                 "FROM Novidades a WITH(NOLOCK) WHERE " + sAccessAllowed +
                                 fldKey + sOperator + textToSearch + " " +
                                 condition + sFiltroContexto + sFiltro;
                    }

                    if (fldKey == "a.NovidadeID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.NovidadeID";
                    }

                    // Contexto de Press-Release tem filtro permanente
                    if (filtroContextoId.intValue() == 4411)
                    {
                        sql2 = "SELECT TOP " + pageSize + " " +
                                  "a.NovidadeID,b.RecursoAbreviado as Estado,a.Data,a.Titulo, a.Fonte, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                  "FROM Novidades a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z " +
                                  "WHERE a.EmpresaID = " + empresaId + " AND " +
                                  "a.EstadoID=b.RecursoID AND " +
                                  fldKey + sOperator + textToSearch + " " +
                                  condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.NovidadeID " +
                                  "AND z.IDTMP > " + startID +
                                  " ORDER BY z.IDTMP";
                    }
                    else
                    {
                        sql2 = "SELECT TOP " + pageSize + " " +
                                  "a.NovidadeID,b.RecursoAbreviado as Estado,a.Data,a.Titulo, a.Fonte, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                  "FROM Novidades a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z " +
                                  "WHERE a.EstadoID=b.RecursoID AND " +
                                  fldKey + sOperator + textToSearch + " " +
                                  condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.NovidadeID " +
                                  "AND z.IDTMP > " + startID +
                                  " ORDER BY z.IDTMP";
                    }

                    break;

                case 4420:   // formID  /modmarketing/subinformacoesmercado/eventos

                    if ((returnFldType("Eventos", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.EventoID " +
                             "FROM Eventos a WITH(NOLOCK) WHERE " + sAccessAllowed +
                             "a.EmpresaID = " + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.EventoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.EventoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                              "a.EventoID,b.RecursoAbreviado as Estado, c.ItemMasculino AS Tipo, a.Evento, a.Data, a.Horario, a.Local, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                              "FROM Eventos a WITH(NOLOCK),Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                              "WHERE a.EmpresaID = " + empresaId + " AND " +
                              "a.EstadoID=b.RecursoID AND a.TipoEventoID=c.ItemID AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.EventoID " +
                              "AND z.IDTMP > " + startID +
                              " ORDER BY z.IDTMP";

                    break;

                case 4430:   // formID  /modmarketing/subinformacoesmercado/pesquisas

                    if ((returnFldType("Pesquisas", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.PesquisaID " +
                             "FROM Pesquisas a WITH(NOLOCK) WHERE " + sAccessAllowed + " " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.PesquisaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.PesquisaID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                              "a.PesquisaID,b.RecursoAbreviado as Estado, a.Pesquisa, a.Data, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                              "FROM Pesquisas a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z " +
                              "WHERE a.EstadoID=b.RecursoID AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.PesquisaID " +
                              "AND z.IDTMP > " + startID +
                              " ORDER BY z.IDTMP";

                    break;

                case 5110:   // formID  /modcomercial/subpedidos/frmpedidosEx

                    //if ((argumentoIsEmpty.intValue() == 1) && (spcClause != "aValorTotalPedido"))
                    //{
                    //    sTextToSearch = "";
                    //    sOperator = " >= ";
                    //}

                    string sOperatorNF;
                    string sOMesmoCondition;
                    string sJoinTransportadoraOperator;
                    string sTableNS;
                    string sTablePes_d;
                    string sTablePes_e;
                    string sConditionMain;
                    string sParametros;

                    sOMesmoCondition = "";
                    sTableNS = "";
                    sTablePes_d = "";
                    sTablePes_e = "";
                    sParametros = "@TTS NVARCHAR(128), @EmpID INT, @UserID INT, @StartID INT, @PS INT";

                    //Chave de pesquisa e Nota Fiscal
                    if (spcClause == "cNotaFiscal")
                        sOperatorNF = " INNER JOIN ";
                    else
                        sOperatorNF = " LEFT OUTER JOIN ";

                    sConditionMain = " AND " + fldKey + sOperator + " @TTS ";


                    if (fldKey == "a.dtChegadaPortador")
                        sOMesmoCondition = " AND a.TransportadoraID=a.ParceiroID ";

                    if (fldKey == "a.NumeroSerie")
                    {
                        sTableNS = "INNER JOIN dbo.fn_Pedido_NumeroSerie_tbl(@TTS) g ON (a.PedidoID = g.PedidoID) ";
                        sConditionMain = "";
                    }

                    if (fldKey == "d.Fantasia")
                        sTablePes_d = "INNER JOIN Pessoas d WITH(NOLOCK) ON (a.PessoaID = d.PessoaID) ";

                    if (fldKey == "e.Fantasia")
                        sTablePes_e = "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID=e.PessoaID) ";

                    if (fldKey == "l.Data")
                        sConditionMain = " AND dbo.Fn_pedido_datas(a.pedidoid, 5) " + sOperator + " @TTS ";

                    if ((filtroId.intValue() != 41000) && (filtroId.intValue() != 41037))
                        sFiltro = sFiltro + " AND ((a.EstadoID < 25) OR (a.EstadoID >= 25 AND a.EmpresaID = ISNULL(a.FilialID, a.EmpresaID))) ";

                    sql = "SET NOCOUNT ON DECLARE @DataZero DATETIME = dbo.fn_Data_Zero(GETDATE()) DECLARE @TempTable TABLE (IDTMP int IDENTITY, IDFromPesq INT NOT NULL) ";

                    // Transportadora 
                    if (spcClause == "fFantasia")
                    {
                        sJoinTransportadoraOperator = " INNER JOIN ";

                        sql = sql + "DECLARE @TempTable_Prov TABLE (PedidoID INT, Fantasia VARCHAR (20)) " +
                                      "INSERT INTO @TempTable_prov (PedidoID, Fantasia) " +
                                       "SELECT a.PedidoID, f.Fantasia " +
                                            "FROM Pedidos a WITH(NOLOCK) " +
                                             "LEFT OUTER JOIN NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID)  " +
                                              sJoinTransportadoraOperator + " Pessoas f WITH(NOLOCK) ON (a.TransportadoraID = f.PessoaID)  " +
                                       "WHERE a.EmpresaID = @EmpID AND (a.TransportadoraID <> a.ParceiroID AND a.TransportadoraID <> a.EmpresaID) " +
                                   sConditionMain + condition + sFiltroContexto + sFiltro + sOMesmoCondition;

                        sql = sql + "INSERT INTO @TempTable (IDFromPesq) SELECT TOP (" + ((pageSize.intValue() * pageNumber.intValue())) + ") PedidoID FROM @TempTable_Prov f ";

                    }
                    else
                    {
                        sJoinTransportadoraOperator = " LEFT OUTER JOIN ";

                        sql = sql + "INSERT INTO @TempTable (IDFromPesq) SELECT TOP (" + ((pageSize.intValue() * pageNumber.intValue())) + ") a.PedidoID " +
                                    "FROM Pedidos a WITH(NOLOCK) " +
                                    sOperatorNF + " NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) " +
                                    sJoinTransportadoraOperator + " Pessoas f WITH(NOLOCK) ON (a.TransportadoraID = f.PessoaID) " +
                                    sTablePes_d + sTablePes_e + sTableNS +
                                    "WHERE " + sAccessAllowed + " " +
                                    "a.EmpresaID = @EmpID " +
                                    sConditionMain + condition + sFiltroContexto + sFiltro + sOMesmoCondition;
                    }

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.PedidoID, a.EstadoID ) = 1)";
                    }


                    if (fldKey == "a.PedidoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else if (fldKey == "a.dtPrevisaoEntrega")
                    {
                        sql = sql + " ORDER BY (CONVERT(DATETIME, CONVERT(VARCHAR, a.dtPrevisaoEntrega, 103), 103)), ISNULL(a.dtChegadaPortador, @DataZero), a.dtPrevisaoEntrega " + order;
                    }
                    else if (fldKey == "f.Fantasia")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else if (fldKey == "a.dtChegadaPortador")
                    {
                        sql = sql + " ORDER BY ISNULL(a.dtChegadaPortador, dbo.fn_Data_Zero(GETDATE()+1)) " + order;
                    }
                    else if (fldKey == "a.NumeroSerie")
                    {
                        sql = sql + " ORDER BY a.PedidoID " + order;
                    }
                    else if (fldKey == "l.Data")
                    {
                        sql = sql + " ORDER BY a.PedidoID " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.PedidoID DESC";
                    }

                    sql2 = "SELECT TOP (" + pageSize + ") ";

                    if (((filtroId.intValue() == 41025) || (filtroId.intValue() == 41037)) && (empresaId.intValue() != 7))
                    {
                        sql2 = sql2 + "a.PedidoID AS PedidoID2, i.VinculadoID, ";
                    }

                    sql2 = sql2 +
                           "a.PedidoID, b.RecursoAbreviado AS Estado, a.dtPedido, d.Fantasia, g.OperacaoID, " +
                           "ISNULL(CONVERT(VARCHAR(50),c.NotaFiscal), dbo.fn_Pedido_Invoice(a.PedidoID, 2)) AS NotaFiscal, " + 
                           "ISNULL(c.dtNotaFiscal, dbo.fn_Pedido_dtInvoice(a.PedidoID)) AS dtNotaFiscal, " +
                           "a.ValorTotalPedido, " +
                           "(CASE a.TransportadoraID WHEN a.ParceiroID THEN 'O mesmo' " +
                           "WHEN a.EmpresaID THEN 'Nosso carro' " +
                           "ELSE (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.TransportadoraID) END) AS Transportadora, " +
                           "a.TotalPesoBruto, a.NumeroVolumes, a.Observacao, h.Fantasia AS Deposito, e.Fantasia AS Colaborador, " +
                           "CONVERT(VARCHAR(20), (SELECT dbo.fn_Pedido_PrevisaoEntrega(a.PedidoID))) AS PrevisaoEntrega, " +
                           "dbo.fn_Pedido_Datas(a.PedidoID, 5) AS LiberacaoPedido, " +
                           "a.dtChegadaPortador, dbo.fn_Pessoa_Localizacao(a.PessoaID) AS Localizacao, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 1) AS CorPedido, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 2) AS CorEstado, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 3) AS CorPessoa, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 4) AS CorValorTotal, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 5) AS CorTransportadora, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 6) AS CorPesoBruto, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 7) AS CorVolumes, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 8) AS CorColaborador, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 9) AS CorPrevisaoEntrega, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 10) AS CorDeposito, " +
                           "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 12) AS CorLiberacao, SPACE(0) AS fldError  ";

                    if (((filtroId.intValue() == 41025) || (filtroId.intValue() == 41037)) && (empresaId.intValue() != 7))
                    {
                        sql2 = sql2 + ", dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 11) AS CorVinculado ";
                    }

                    sql2 = sql2 +
                        "FROM @TempTable z " +
                            "INNER JOIN Pedidos a WITH(NOLOCK) ON (z.IDFromPesq=a.PedidoID) " +
                            "LEFT OUTER JOIN Pessoas h WITH(NOLOCK) ON (a.DepositoID=h.PessoaID) " +
                            sOperatorNF + " NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) " +
                            sJoinTransportadoraOperator + " Pessoas f WITH(NOLOCK) ON (a.TransportadoraID=f.PessoaID) " +
                            "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                            "INNER JOIN Pessoas d WITH(NOLOCK) ON (a.PessoaID=d.PessoaID) " +
                            "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID=e.PessoaID) " +
                            "INNER JOIN Operacoes g WITH(NOLOCK) ON (a.TransacaoID = g.OperacaoID) ";


                    if (((filtroId.intValue() == 41025) || (filtroId.intValue() == 41037)) && (empresaId.intValue() != 7))
                    {
                        sql2 = sql2 + "LEFT JOIN Pedidos_Vinculados i WITH(NOLOCK) ON (i.PedidoID = a.PedidoID) ";
                    }

                    sql2 = sql2 +
                         "WHERE z.IDTMP > " + startID + " " +
                         "ORDER BY z.IDTMP";

                    sql = sql + " " + sql2 + " SET NOCOUNT OFF";

                    //Determinar tamanho dos parametros
                    parametrosSQL = new ProcedureParameters[7];
                    parametrosSQL[0] = new ProcedureParameters("@sql", SqlDbType.NVarChar, sql, ParameterDirection.Input, 4000);
                    parametrosSQL[1] = new ProcedureParameters("@DefinicaoParametros", SqlDbType.NVarChar, sParametros, ParameterDirection.Input, 4000);
                    parametrosSQL[2] = new ProcedureParameters("@TTS", SqlDbType.VarChar, textToSearch.ToString(), ParameterDirection.Input, 128);
                    parametrosSQL[3] = new ProcedureParameters("@UserID", SqlDbType.Int, userId.intValue(), ParameterDirection.Input);
                    parametrosSQL[4] = new ProcedureParameters("@StartID", SqlDbType.Int, startID, ParameterDirection.Input);
                    parametrosSQL[5] = new ProcedureParameters("@PS", SqlDbType.Int, pageSize.intValue(), ParameterDirection.Input);
                    parametrosSQL[6] = new ProcedureParameters("@EmpID", SqlDbType.Int, empresaId.intValue(), ParameterDirection.Input);

                    break;
                case 5120:   // formID  /modcomercial/subpedidos/notasfiscais

                    if ((returnFldType("NotasFiscais", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.NotaFiscalID " +
                             "FROM NotasFiscais a WITH(NOLOCK) " +
                                "INNER JOIN NotasFiscais_Pessoas c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) " +
                                                   "AND ((a.Emissor = 1 AND c.TipoID = 791) OR (a.Emissor = 0 AND c.TipoID = 790)) " +
                             "LEFT JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (d.ItemID = a.DocumentoFiscalID) " +
                             "WHERE " + sAccessAllowed + " a.EmpresaID = " + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.NotaFiscalID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.NotaFiscalID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                           "a.NotaFiscalID, b.RecursoAbreviado AS Estado, dbo.fn_TipoAuxiliar_Item(a.DocumentoFiscalID, 2) AS DF, a.NotaFiscal, a.dtNotaFiscal, a.CFOPs, " +
                            "a.PedidoID, a.Vendedor, a.ValorTotalNota, c.Nome, a.NumeroRPS AS RPS, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                         "FROM NotasFiscais a WITH(NOLOCK) " +
                            "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                            "LEFT JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (d.ItemID = a.DocumentoFiscalID) " +
                            "INNER JOIN NotasFiscais_Pessoas c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) " +
                                                    "AND ((a.Emissor = 1 AND c.TipoID = 791) OR (a.Emissor = 0 AND c.TipoID = 790)) " +
                            "INNER JOIN @TempTable z ON (z.IDFromPesq=a.NotaFiscalID) " +
                             "WHERE a.EmpresaID = " + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";
                    break;

                case 5130:    // formID  /modcomercial//cartascorrecao

                    if ((returnFldType("CartasCorrecao", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) + " a.CartaCorrecaoID " +
                              "FROM CartasCorrecao a WITH(NOLOCK) " +
                                "INNER JOIN NotasFiscais b WITH(NOLOCK) ON a.NotaFiscalID = b.NotaFiscalID " +
                                "INNER JOIN Pedidos c WITH(NOLOCK) ON b.NotaFiscalID = c.NotaFiscalID " +
                                "INNER JOIN Pessoas d WITH(NOLOCK) ON c.PessoaID = d.PessoaID " +
                              "WHERE " + sAccessAllowed + " a.EmpresaID= " + empresaId + " AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.CartaCorrecaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.CartaCorrecaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                            " a.CartaCorrecaoID, e.RecursoAbreviado as Estado, a.dtCartaCorrecao, a.AjusteCartaPendente, a.SequencialEvento, b.NotaFiscal, b.dtNotaFiscal, " +
                            " c.PedidoID, d.Fantasia, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                            " FROM CartasCorrecao a WITH(NOLOCK) " +
                                "INNER JOIN NotasFiscais b WITH(NOLOCK) ON a.NotaFiscalID = b.NotaFiscalID " +
                                "INNER JOIN Pedidos c WITH(NOLOCK) ON b.NotaFiscalID = c.NotaFiscalID " +
                                "INNER JOIN Pessoas d WITH(NOLOCK) ON c.PessoaID = d.PessoaID " +
                                "INNER JOIN Recursos e WITH(NOLOCK) ON a.EstadoID = e.RecursoID " +
                                "INNER JOIN @TempTable z ON z.IDFromPesq=a.CartaCorrecaoID " +
                             "WHERE a.EmpresaID= " + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 5140:   //formID  /modcomercial/subpedidos/operacoes

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.OperacaoID " +
                              "FROM Operacoes a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.OperacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.OperacaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.OperacaoID,b.RecursoAbreviado AS Estado,a.Operacao,c.ItemMasculino, SPACE(0) AS fldError  " +
                            "FROM Operacoes a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                            "WHERE a.EstadoID = b.RecursoID AND a.TipoOperacaoID = c.ItemID AND " +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.OperacaoID " +
                            "AND z.IDTMP > " + startID +
                            " ORDER BY z.IDTMP";

                    break;

                case 5145:   //formID  /modcomercial/subpedidos/regrasfiscais

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RegraFiscalID " +
                              "FROM RegrasFiscais a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.RegraFiscalID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RegraFiscalID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             " a.RegraFiscalID,  b.RecursoAbreviado AS Estado, a.RegraFiscal, d.RegraFiscal as RegraFiscalMae, c.ItemMasculino, a.Nivel, a.Ordem, " +
                             "a.Continua ,a.Legislacao, a.dtData, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM RegrasFiscais a WITH(NOLOCK) " +
                             "INNER JOIN Recursos b WITH ( NOLOCK )ON a.EstadoID = b.RecursoID " +
                             "INNER JOIN TiposAuxiliares_Itens c WITH ( NOLOCK )on a.tiporegrafiscalid = c.itemid " +
                             "INNER JOIN @TempTable z ON z.IDFromPesq = a.RegraFiscalID " +
                             "LEFT JOIN dbo.RegrasFiscais d WITH ( NOLOCK ) ON a.RegraFiscalMaeID = d.RegraFiscalID " +
                             " WHERE " + fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 5150:   //formID  /modcomercial/subpedidos/DPA

                    if ((returnFldType("DPA", fldKey) == 135) && (textToSearch == "//0//"))
                    {
                        textToSearch = "0";
                    }

                    if (fldKey == "d.Fantasia")
                    {
                        sClienteOperator = " INNER JOIN ";
                    }
                    else
                    {
                        sClienteOperator = " LEFT OUTER JOIN ";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.DPAID " +
                             "FROM DPA a WITH(NOLOCK) " +
                                sClienteOperator + " Pessoas d WITH(NOLOCK) ON (a.ClienteID = d.PessoaID) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON (a.FornecedorID = c.PessoaID) " +
                                "INNER JOIN Conceito    s e WITH(NOLOCK) ON (a.MoedaID = e.ConceitoID) " +
                                "INNER JOIN Pessoas f WITH(NOLOCK) ON (a.ContatoID = f.PessoaID) " +
                             "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.DPAID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.DPAID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.DPAID, b.RecursoAbreviado AS Estado, a.DPA, c.Fantasia AS Fornecedor, d.Fantasia AS Cliente, " +
                             "dbo.fn_ProgramaMarketing_Identificador(a.FornecedorID, a.ClienteID) AS Identificador, a.dtEmissao, a.dtVencimento, " +
                             "e.SimboloMoeda AS Moeda, dbo.fn_DPA_Totais(a.DPAID, NULL, 1) AS Total, dbo.fn_DPA_Totais(a.DPAID, NULL, 2) AS Saldo, " +
                             "f.Fantasia AS Contato, a.Custo, a.SolicitaAprovacao, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM DPA a WITH(NOLOCK), Recursos b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK), Pessoas f WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND a.FornecedorID = c.PessoaID AND a.ClienteID " + sClienteOperator +
                             " d.PessoaID AND " +
                                "a.MoedaID = e.ConceitoID AND a.ContatoID = f.PessoaID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.DPAID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;


                case 5160:   //formID  /modcomercial/subpedidos/Campanhas
                    string sFornecedorOperador;

                    if ((returnFldType("Campanhas", fldKey) == 135) && (textToSearch == "//0//"))
                    {
                        textToSearch = "0";
                    }

                    sClienteOperator2 = "";
                    sFornecedorOperador = "";

                    if (fldKey.Trim() == "c.Fantasia")
                    {
                        sFornecedorOperador = " INNER JOIN ";
                    }
                    else
                    {
                        sFornecedorOperador = " LEFT OUTER JOIN ";
                    }

                    if (fldKey.Trim() == "d.Fantasia")
                    {
                        sClienteOperator2 = " INNER JOIN ";
                    }
                    else
                    {
                        sClienteOperator2 = " LEFT OUTER JOIN ";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.CampanhaID " +
                                     "FROM Campanhas a WITH(NOLOCK) " +
                                        "LEFT OUTER JOIN Pessoas f WITH(NOLOCK) ON (a.ContatoID = f.PessoaID) " +
                                        sFornecedorOperador + " Pessoas c WITH(NOLOCK) ON (a.FornecedorID = c.PessoaID) " +
                                        sClienteOperator2 + " Pessoas d WITH(NOLOCK) ON (a.ClienteID = d.PessoaID) " +
                                        "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                                        "INNER JOIN Conceitos e WITH(NOLOCK) ON (a.MoedaID = e.ConceitoID) " +
                                     "WHERE " + sAccessAllowed +
                                      fldKey + sOperator + textToSearch + " " +
                                      condition + sFiltroContexto + sFiltro;


                    if (fldKey.Trim() == "a.CampanhaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.CampanhaID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                                 "a.CampanhaID, b.RecursoAbreviado AS Estado, a.dtEmissao, a.Campanha, a.Codigo, " +
                                "c.Fantasia AS Fornecedor, f.Fantasia AS Contato, d.Fantasia AS Cliente, " +
                                "dbo.fn_ProgramaMarketing_Identificador(a.FornecedorID, a.ClienteID) AS Identificador, " +
                                "a.dtInicio, a.dtFim, e.SimboloMoeda AS Moeda, a.ValorTotal, dbo.fn_Campanha_Totais(a.CampanhaID, NULL, 2) AS Saldo, a.PedidosPorCliente, " +
                                "a.CampanhaInterna AS [Int], a.SolicitaAprovacao, a.Custo, a.RebatePermiteDPA, a.Observacao , CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                "FROM Campanhas a WITH(NOLOCK) " +
                                "LEFT OUTER JOIN Pessoas f WITH(NOLOCK) ON (a.ContatoID = f.PessoaID) " +
                                sFornecedorOperador + " Pessoas c WITH(NOLOCK) ON (a.FornecedorID = c.PessoaID) " +
                                sClienteOperator2 + " Pessoas d WITH(NOLOCK) ON (a.ClienteID = d.PessoaID) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                                "INNER JOIN Conceitos e WITH(NOLOCK) ON (a.MoedaID = e.ConceitoID) " +
                                "INNER JOIN @TempTable z  ON (z.IDFromPesq=a.CampanhaID) " +
                             "WHERE " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 5170:   //formID  /modcomercial/subpedidos/frmLotes

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.LoteID " +
                                                  "FROM Lotes a WITH(NOLOCK) " +
                                                  //"WHERE a.EmpresaID = " + empresaId + " AND " + sAccessAllowed +
                                                  "WHERE " + sAccessAllowed +
                                                  fldKey + sOperator + textToSearch + " " +
                                                  condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.LoteID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.LoteID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                        " a.LoteID AS Lote , b.RecursoAbreviado AS Estado, a.Descricao,CONVERT(VARCHAR (50), a.dtEmissao,103) AS DataEmissao," +
                        " CONVERT(VARCHAR (50), a.dtInicio,103) AS DataInicio, CONVERT(VARCHAR (50), a.dtFim,103) AS DataFim, a.Observacoes," +
                        " CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                        " FROM    Lotes a WITH ( NOLOCK ) " +
                        " INNER JOIN Recursos b WITH ( NOLOCK ) ON ( a.EstadoID = b.RecursoID ) " +
                        " INNER JOIN @TempTable z ON z.IDFromPesq = a.LoteID" +
                        //" WHERE  a.EmpresaID = " + empresaId + " AND " + fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro + " AND z.IDTMP > " + startID +
                        " WHERE " + fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro + " AND z.IDTMP > " + startID +
                          " ORDER BY z.IDTMP";


                    break;


                case 5220: //formID  /modcomercial/subatendimento/listaprecos
                    //Nota para Listas de Precos
                    //fldKey -> usado para Ordem
                    //textToSearch -> usado como palavra chave para LIKE

                    string lstPre_Cotador = "";
                    string lstPre_ParceiroID = "";
                    string lstPre_ListaPreco = "";
                    string lstPre_PessoaID = "";
                    string lstPre_PessoaID2 = "";
                    string lstPre_UFID = "";
                    string lstPre_ImpostosIncidencia = "";
                    string lstPre_Frete = "";
                    string lstPre_MeioTransporteID = "";
                    // Novos parametros, s� ser�o usados para listagem de pre�os sem Pessoa - DMC 04/02/2010
                    string lstPre_ChavePessoaID = "";
                    string lstPre_Classe = "";
                    string lstPre_TipoPessoa = "";
                    string lstPre_FaturamentoDireto = "";
                    string lstPre_Contribuinte = "";
                    string lstPre_SimplesNacional = "";
                    string lstPre_Suframa = "";
                    string lstPre_Cidade = "";
                    string lstPre_Classificacoes = "";
                    string lstPre_Cnae = "";
                    string lstPre_Isencoes = "";
                    string lstPre_Pais = "";
                    string lstPre_FinanciamentoID = "";
                    string lstPre_Quantidade = "";
                    string lstPre_MoedaConversaoID = "";
                    string lstPre_Estoque = "";
                    string lstPre_TransacaoID = "";
                    string lstPre_FinalidadeID = "";
                    string lstPre_Lote = "";
                    //Novos Parametros de Busca por Marca, Familia, LinhaProduto
                    string lstPre_FamiliaID = "";
                    string lstPre_MarcaID = "";
                    string lstPre_LinhaProdutoID = "";
                    string lstPre_TextBusca = "";
                    string lstPre_ProdutoReferenciaID = "";
                    string lstPre_TipoRelacaoID = "";
                    string lstPre_EmaClienteID = "";
                    string sStrProdAliq = "";
                    string lstPre_FldKey2 = "";
                    string sSQLMC = "";
                    string sSQLEstoqueDisp1 = "";
                    string sSQLEstoqueDisp2 = "";
                    string sSQLEstoqueReserva = "";
                    string sSQLEstoqueEquipe = "";
                    string sSQLDisponibilidade = "";
                    string sStrInsertEstoque = "";
                    string sStrDeleteEstoque = "";
                    string sStrSelectEstoque = "";
                    string sSQLPMP = "";
                    string sStrCFOP = "";
                    string sUltimoPreco = "";
                    string sStrInnerLote = "";
                    string sStrInnerLote2 = "";
                    string sStrInnerLote3 = "";
                    string sStrSelectLote = "";
                    string sStrInsertLote2 = "";
                    string sStrInsertLote3 = "";
                    bool bQuoteBOM = false;

                    if (Request.QueryString["lstPre_Cotador"] != null)
                        lstPre_Cotador = Request.QueryString["lstPre_Cotador"];
                    
                    if(Request.Form.Count > 0)
                    {
                        bQuoteBOM = true;
                    }
                    
                    if (int.Parse(lstPre_Cotador) == 0)
                    {
                        if (Request.QueryString["lstPre_ParceiroID"] != null)
                            lstPre_ParceiroID = Request.QueryString["lstPre_ParceiroID"];

                        if (Request.QueryString["lstPre_ListaPreco"] != null)
                            lstPre_ListaPreco = Request.QueryString["lstPre_ListaPreco"];

                        if (Request.QueryString["lstPre_PessoaID"] != null)
                            lstPre_PessoaID = Request.QueryString["lstPre_PessoaID"];

                        if (Request.QueryString["lstPre_PessoaID2"] != null)
                            lstPre_PessoaID2 = Request.QueryString["lstPre_PessoaID2"];

                        if (Request.QueryString["lstPre_ChavePessoaID"] != null)
                            lstPre_ChavePessoaID = Request.QueryString["lstPre_ChavePessoaID"];

                        if (Request.QueryString["lstPre_Classe"] != null)
                            lstPre_Classe = Request.QueryString["lstPre_Classe"];

                        if (Request.QueryString["lstPre_TipoPessoa"] != null)
                            lstPre_TipoPessoa = Request.QueryString["lstPre_TipoPessoa"];

                        if (Request.QueryString["lstPre_FaturamentoDireto"] != null)
                            lstPre_FaturamentoDireto = Request.QueryString["lstPre_FaturamentoDireto"];

                        if (Request.QueryString["lstPre_Contribuinte"] != null)
                            lstPre_Contribuinte = Request.QueryString["lstPre_Contribuinte"];

                        if (Request.QueryString["lstPre_SimplesNacional"] != null)
                            lstPre_SimplesNacional = Request.QueryString["lstPre_SimplesNacional"];

                        if (Request.QueryString["lstPre_Suframa"] != null)
                            lstPre_Suframa = Request.QueryString["lstPre_Suframa"];

                        if (Request.QueryString["lstPre_Cidade"] != null)
                            lstPre_Cidade = Request.QueryString["lstPre_Cidade"];

                        if (Request.QueryString["lstPre_Classificacoes"] != null)
                            lstPre_Classificacoes = Request.QueryString["lstPre_Classificacoes"];

                        if (Request.QueryString["lstPre_Cnae"] != null)
                            lstPre_Cnae = Request.QueryString["lstPre_Cnae"];

                        if (Request.QueryString["lstPre_Isencoes"] != null)
                            lstPre_Isencoes = Request.QueryString["lstPre_Isencoes"];

                        if (Request.QueryString["lstPre_UFID"] != null)
                            lstPre_UFID = Request.QueryString["lstPre_UFID"];

                        if (Request.QueryString["lstPre_Pais"] != null)
                            lstPre_Pais = Request.QueryString["lstPre_Pais"];

                        if (Request.QueryString["lstPre_Frete"] != null)
                            lstPre_Frete = Request.QueryString["lstPre_Frete"];

                        if (Request.QueryString["lstPre_MeioTransporteID"] != null)
                            lstPre_MeioTransporteID = Request.QueryString["lstPre_MeioTransporteID"];

                        if (Request.QueryString["lstPre_FinanciamentoID"] != null)
                            lstPre_FinanciamentoID = Request.QueryString["lstPre_FinanciamentoID"];

                        if (Request.QueryString["lstPre_Quantidade"] != null)
                            lstPre_Quantidade = Request.QueryString["lstPre_Quantidade"];

                        if (Request.QueryString["lstPre_MoedaConversaoID"] != null)
                            lstPre_MoedaConversaoID = Request.QueryString["lstPre_MoedaConversaoID"];

                        if (Request.QueryString["lstPre_Estoque"] != null)
                            lstPre_Estoque = Request.QueryString["lstPre_Estoque"];

                        if (Request.QueryString["lstPre_Lote"] != null)
                            lstPre_Lote = Request.QueryString["lstPre_Lote"];

                        if (Request.QueryString["lstPre_TransacaoID"] != null)
                            lstPre_TransacaoID = Request.QueryString["lstPre_TransacaoID"];

                        if (Request.QueryString["lstPre_FinalidadeID"] != null)
                            lstPre_FinalidadeID = Request.QueryString["lstPre_FinalidadeID"];

                        if (Request.QueryString["lstPre_ProdutoReferenciaID"] != null)
                            lstPre_ProdutoReferenciaID = Request.QueryString["lstPre_ProdutoReferenciaID"];

                        if (Request.QueryString["lstPre_TipoRelacaoID"] != null)
                            lstPre_TipoRelacaoID = Request.QueryString["lstPre_TipoRelacaoID"];

                        if ((Request.QueryString["lstPre_FamiliaID"] != null) && (Request.QueryString["lstPre_FamiliaID"] != ""))
                            lstPre_FamiliaID = Request.QueryString["lstPre_FamiliaID"];

                        if ((Request.QueryString["lstPre_MarcaID"] != null) && (Request.QueryString["lstPre_MarcaID"] != ""))
                            lstPre_MarcaID = Request.QueryString["lstPre_MarcaID"];

                        if ((Request.QueryString["lstPre_LinhaProdutoID"] != null) && (Request.QueryString["lstPre_LinhaProdutoID"] != ""))
                            lstPre_LinhaProdutoID = Request.QueryString["lstPre_LinhaProdutoID"];

                        if ((Request.QueryString["lstPre_EmaClienteID"] != null) && (Request.QueryString["lstPre_EmaClienteID"] != ""))
                            lstPre_EmaClienteID = Request.QueryString["lstPre_EmaClienteID"];

                        // Cotador corporativo: Estimate BOM ou Quote DID
                        /*
                            '&CotadorPartNumber='
                            '&CotadorLineNumber='
                            '&CotadorDescription='
                            '&CotadorListPrice='
                            '&CotadorQuantity='
                            '&CotadorDiscount='
                        */

                        lstPre_TextBusca = textToSearch.ToString();

                        sDefinicaoParametros = "@ParID INT,@LP INT,@PesID INT,@PesID2 INT,@ClaParID INT,@TipPesID INT,@FD BIT,@Con BIT,@SN BIT,@Suf BIT,@CidID INT,@ClaPesID INT," +
                                                "@Cnae NVARCHAR(8),@Isen NVARCHAR(128),@ImpInc BIT,@UFID INT,@PaisID INT,@Frete BIT,@MTID INT,@FinalID INT,@FinanID INT,@Quant INT,@MoedaID INT," +
                                                "@TranID INT,@ProdRefID INT,@TipRelID INT,@IdiSisID INT,@IdiEmpID INT,@TTS NVARCHAR(128),@IdiID INT,@EmpID INT,@StartID INT, " +
                                                "@PS INT, @FamiliaID INT, @MarcaID INT, @LinhaID INT, @EmaClienteID INT ";

                        // Determinar tamanho dos parametros
                        parametrosSQL = new ProcedureParameters[39];

                        parametrosSQL[1] = new ProcedureParameters("@DefinicaoParametros", SqlDbType.NVarChar, sDefinicaoParametros, ParameterDirection.Input, 4000);
                        parametrosSQL[2] = new ProcedureParameters("@ParID", SqlDbType.Int, ((int.Parse(lstPre_ParceiroID) > 0) ? (Object)int.Parse(lstPre_ParceiroID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[3] = new ProcedureParameters("@LP", SqlDbType.Int, ((int.Parse(lstPre_ListaPreco) >= 0) ? (Object)int.Parse(lstPre_ListaPreco) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[4] = new ProcedureParameters("@PesID", SqlDbType.Int, ((int.Parse(lstPre_PessoaID) >= 0) ? (Object)int.Parse(lstPre_PessoaID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[5] = new ProcedureParameters("@PesID2", SqlDbType.Int, (int.Parse(lstPre_PessoaID2) != 0) ? (Object)int.Parse(lstPre_PessoaID2) : DBNull.Value, ParameterDirection.Input);
                        parametrosSQL[6] = new ProcedureParameters("@ClaParID", SqlDbType.Int, (Object)int.Parse(lstPre_Classe), ParameterDirection.Input);
                        parametrosSQL[7] = new ProcedureParameters("@TipPesID", SqlDbType.Int, ((lstPre_TipoPessoa != "") ? (Object)int.Parse(lstPre_TipoPessoa) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[8] = new ProcedureParameters("@FD", SqlDbType.Bit, lstPre_FaturamentoDireto == "1" ? true : false, ParameterDirection.Input);
                        parametrosSQL[9] = new ProcedureParameters("@Con", SqlDbType.Bit, lstPre_Contribuinte == "1" ? true : false, ParameterDirection.Input);
                        parametrosSQL[10] = new ProcedureParameters("@SN", SqlDbType.Bit, lstPre_SimplesNacional == "1" ? true : false, ParameterDirection.Input);
                        parametrosSQL[11] = new ProcedureParameters("@Suf", SqlDbType.Bit, lstPre_Suframa == "1" ? true : false, ParameterDirection.Input);
                        parametrosSQL[12] = new ProcedureParameters("@CidID", SqlDbType.Int, ((int.Parse(lstPre_Cidade) > 0) ? (Object)int.Parse(lstPre_Cidade) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[13] = new ProcedureParameters("@ClaPesID", SqlDbType.Int, ((int.Parse(lstPre_Classificacoes) > 0) ? (Object)int.Parse(lstPre_Classificacoes) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[14] = new ProcedureParameters("@Cnae", SqlDbType.NVarChar, ((lstPre_Cnae != "") ? (Object)lstPre_Cnae : DBNull.Value), ParameterDirection.Input, 9);
                        parametrosSQL[15] = new ProcedureParameters("@Isen", SqlDbType.NVarChar, ((lstPre_Isencoes != "") ? (Object)lstPre_Isencoes : DBNull.Value), ParameterDirection.Input, 128);
                        parametrosSQL[16] = new ProcedureParameters("@ImpInc", SqlDbType.Bit, lstPre_ImpostosIncidencia == "1" ? true : false, ParameterDirection.Input);
                        parametrosSQL[17] = new ProcedureParameters("@UFID", SqlDbType.Int, ((int.Parse(lstPre_UFID) >= 0) ? (Object)int.Parse(lstPre_UFID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[18] = new ProcedureParameters("@PaisID", SqlDbType.Int, int.Parse(lstPre_Pais), ParameterDirection.Input);
                        parametrosSQL[19] = new ProcedureParameters("@Frete", SqlDbType.Bit, int.Parse(lstPre_Frete) >= 1 ? true : false, ParameterDirection.Input);
                        parametrosSQL[20] = new ProcedureParameters("@MTID", SqlDbType.Int, (int.Parse(lstPre_MeioTransporteID) != 0) ? (Object)int.Parse(lstPre_MeioTransporteID) : DBNull.Value, ParameterDirection.Input);
                        parametrosSQL[21] = new ProcedureParameters("@FinalID", SqlDbType.Int, ((int.Parse(lstPre_FinalidadeID) > 0) ? (Object)int.Parse(lstPre_FinalidadeID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[22] = new ProcedureParameters("@FinanID", SqlDbType.Int, int.Parse(lstPre_FinanciamentoID), ParameterDirection.Input);
                        parametrosSQL[23] = new ProcedureParameters("@Quant", SqlDbType.Int, int.Parse(lstPre_Quantidade), ParameterDirection.Input);
                        parametrosSQL[24] = new ProcedureParameters("@MoedaID", SqlDbType.Int, int.Parse(lstPre_MoedaConversaoID), ParameterDirection.Input);
                        parametrosSQL[25] = new ProcedureParameters("@TranID", SqlDbType.Int, int.Parse(lstPre_TransacaoID), ParameterDirection.Input);
                        parametrosSQL[26] = new ProcedureParameters("@ProdRefID", SqlDbType.Int, ((int.Parse(lstPre_ProdutoReferenciaID) > 0) ? (Object)int.Parse(lstPre_ProdutoReferenciaID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[27] = new ProcedureParameters("@TipRelID", SqlDbType.Int, ((int.Parse(lstPre_TipoRelacaoID) > 0) ? (Object)int.Parse(lstPre_TipoRelacaoID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[28] = new ProcedureParameters("@IdiSisID", SqlDbType.Int, idiomaSistemaId.intValue(), ParameterDirection.Input);
                        parametrosSQL[29] = new ProcedureParameters("@IdiEmpID", SqlDbType.Int, idiomaEmpresaId.intValue(), ParameterDirection.Input);
                        parametrosSQL[30] = new ProcedureParameters("@TTS", SqlDbType.NVarChar, textToSearch.ToString(), ParameterDirection.Input, 128);
                        parametrosSQL[31] = new ProcedureParameters("@IdiID", SqlDbType.Int, idiomaId.intValue(), ParameterDirection.Input);
                        parametrosSQL[32] = new ProcedureParameters("@EmpID", SqlDbType.Int, empresaId.intValue(), ParameterDirection.Input);
                        parametrosSQL[33] = new ProcedureParameters("@StartID", SqlDbType.Int, startID, ParameterDirection.Input);
                        parametrosSQL[34] = new ProcedureParameters("@PS", SqlDbType.Int, pageSize.intValue(), ParameterDirection.Input);
                        parametrosSQL[35] = new ProcedureParameters("@FamiliaID", SqlDbType.Int, ((int.Parse(lstPre_FamiliaID) > 0) ? (Object)int.Parse(lstPre_FamiliaID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[36] = new ProcedureParameters("@MarcaID", SqlDbType.Int, ((int.Parse(lstPre_MarcaID) > 0) ? (Object)int.Parse(lstPre_MarcaID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[37] = new ProcedureParameters("@LinhaID", SqlDbType.Int, ((int.Parse(lstPre_LinhaProdutoID) > 0) ? (Object)int.Parse(lstPre_LinhaProdutoID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[38] = new ProcedureParameters("@EmaClienteID", SqlDbType.Int, ((int.Parse(lstPre_EmaClienteID) > 0) ? (Object)int.Parse(lstPre_EmaClienteID) : DBNull.Value), ParameterDirection.Input);

                        sql = "SET NOCOUNT ON DECLARE @TempTable TABLE (IDTMP INT IDENTITY,IDFromPesq INT NOT NULL,OK BIT,RelacaoID INT,EstadoID INT,Lote INT ,EmpresaID INT,Conceito VARCHAR(200),Estoque INT,CFOPID INT,ProdAliq NUMERIC(11,2)," +
                                "Descricao VARCHAR(35),Modelo VARCHAR(18),FamiliaID INT,MarcaID INT,LinhaID INT,Observacao VARCHAR(25),ClaFiscalID INT,MargemMinima NUMERIC(5,2),PKID INT) ";

                        if (!(lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")))
                        {
                            sql += "DECLARE @Result_Tbl TABLE (PKID INT IDENTITY(1,1) UNIQUE CLUSTERED NOT NULL, ProdutoID INT) " +
                               "INSERT INTO @Result_Tbl " +
                                   "EXEC sp_Produto_Busca @TTS, @IdiEmpID, NULL, NULL, 1 ";
                        }

                        /*strAdditionalFrom1 = "INNER JOIN Conceitos e WITH(NOLOCK) ON b.MarcaID=e.ConceitoID " +
                                                "INNER JOIN Conceitos f WITH(NOLOCK) ON b.ProdutoID=f.ConceitoID AND f.EstadoID=2 " +
                                                "INNER JOIN RelacoesConceitos g WITH(NOLOCK) ON f.ConceitoID=g.SujeitoID AND g.TipoRelacaoID=41 AND g.EstadoID=2 " +
                                                "INNER JOIN Conceitos h WITH(NOLOCK) ON g.ObjetoID=h.ConceitoID AND h.EstadoID=2 " +
                                                "INNER JOIN RelacoesConceitos i WITH(NOLOCK) ON h.ConceitoID=i.SujeitoID AND i.TipoRelacaoID=41 AND i.EstadoID=2 " +
                                                "INNER JOIN Conceitos j WITH(NOLOCK) ON i.ObjetoID=j.ConceitoID AND j.EstadoID=2 ";*/
                        strAdditionalFrom1 = "";

                        strAdditionalCondition1 = " ";

                        if (lstPre_ProdutoReferenciaID == "0")
                        {
                            strAdditionalFrom2 = "";

                            /*strAdditionalCondition2 = " AND (b.Conceito+";

                            if (idiomaSistemaId.intValue() != idiomaEmpresaId.intValue())
                                strAdditionalCondition2 += "dbo.fn_Tradutor(f.Conceito,@IdiSisID,@IdiEmpID, NULL)+" +
                                                            "dbo.fn_Tradutor(j.Conceito,@IdiSisID,@IdiEmpID, NULL)+" +
                                                            "dbo.fn_Tradutor(h.Conceito,@IdiSisID,@IdiEmpID, NULL)+";
                            else
                                strAdditionalCondition2 += "f.Conceito+j.Conceito+h.Conceito+";

                            strAdditionalCondition2 += "e.Conceito+" +
                                                         "b.Modelo+" +
                                                         "b.Descricao+" +
                                                         "ISNULL(b.PartNumber,SPACE(0))+" +
                                                         "b.Conceito + SPACE(1) + ISNULL(b.Serie, SPACE(0))+" +
                                                         "CONVERT(VARCHAR,a.ObjetoID) LIKE @TTS) ";*/
                        }
                        else
                        {
                            strAdditionalFrom2 = "INNER JOIN RelacoesConceitos Referencias WITH(NOLOCK) ON Referencias.EstadoID = 2 AND ((Referencias.ObjetoID=@ProdRefID AND Referencias.SujeitoID=b.ConceitoID) OR (Referencias.SujeitoID=@ProdRefID AND Referencias.ObjetoID=b.ConceitoID))";

                            strAdditionalCondition2 = "";

                            if (lstPre_TipoRelacaoID != "0")
                            {
                                strAdditionalCondition2 = strAdditionalCondition2 + " AND(Referencias.TipoRelacaoID=@TipRelID) ";
                            }
                        }

                        //LOTE
                        sStrSelectLote = " ";
                        sStrInnerLote2 = " ";
                        sStrInnerLote3 = " ";
                        sStrInsertLote2 = " ";
                        sStrInsertLote3 = " ";

                        if (int.Parse(lstPre_Lote) == 1)
                        {

                            sStrInnerLote2 = " INNER JOIN dbo.Lotes_Produtos Lote WITH(NOLOCK) ON (Lote.ProdutoID = a.IDFromPesq) ";
                            sStrInnerLote3 = " INNER JOIN dbo.Lotes x WITH (NOLOCK) ON ((x.EmpresaID = f.PessoaID) AND (Lote.LoteID = x.LoteID) " +
                                                "AND (x.EstadoID = 41) AND (dbo.fn_Lote_Pessoa(x.LoteID, @PesID) = 1)) ";
                            sStrSelectLote = " Lote.LoteID ";

                            sStrInsertLote2 = " INNER JOIN Lotes_Produtos Lote WITH(NOLOCK) ON (Lote.ProdutoID = a.ObjetoID) ";
                            sStrInsertLote3 = " INNER JOIN dbo.Lotes x WITH (NOLOCK) ON ((x.EmpresaID = a.SujeitoID) AND (Lote.LoteID = x.LoteID) " +
                                                    "AND (x.EstadoID = 41) AND (dbo.fn_Lote_Pessoa(x.LoteID, @PesID) = 1)) ";
                        }

                        sStrInsertEstoque = "0 ";

                        //Altera��o: Caso o usu�rio utilize a busca por filtro de marca, aumenta o retorno da busca. SGP.
                        if ((int.Parse(lstPre_FamiliaID) > 0) || (int.Parse(lstPre_MarcaID) > 0) || (int.Parse(lstPre_LinhaProdutoID) > 0))
                        {
                            sStrTop = " TOP 300 ";
                        }
                        else
                        {
                            sStrTop = " TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " ";
                        }

                        sStrDeleteEstoque = "";
                        sStrSelectEstoque = " ";

                        if (int.Parse(lstPre_ChavePessoaID) == 2) //sem pessoa
                            sStrSelectEstoque = " dbo.fn_Produto_Estoque(a.EmpresaID,a.IDFromPesq,-356,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(a.IDFromPesq,a.EmpresaID,NULL,@TipPesID),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + ") ";
                        else if (int.Parse(lstPre_ChavePessoaID) == 1) // com pessoa
                            sStrSelectEstoque = " dbo.fn_Produto_Estoque(a.EmpresaID,a.IDFromPesq,-356,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(a.IDFromPesq,a.EmpresaID,@PesID,NULL),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + ") ";

                        if (int.Parse(lstPre_Estoque) == 1)
                        {
                            if (int.Parse(lstPre_ChavePessoaID) == 2) //sem pessoa
                                sStrInsertEstoque = " dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,-356,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(a.ObjetoID,a.SujeitoID,NULL,@TipPesID),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + ") ";
                            else if (int.Parse(lstPre_ChavePessoaID) == 1) // com pessoa
                                sStrInsertEstoque = " dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,-356,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(a.ObjetoID,a.SujeitoID,@PesID,NULL),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + ") ";

                            sStrTop = " ";
                            sStrDeleteEstoque = " DELETE FROM @TempTable WHERE Estoque<=0 ";
                            sStrSelectEstoque = "Estoque";
                        }


                        sql += "INSERT INTO @TempTable(IDFromPesq,OK,RelacaoID,EmpresaID,Lote,Conceito,Estoque, Descricao,Modelo,FamiliaID,MarcaID,LinhaID,Observacao,ClaFiscalID,EstadoID,MargemMinima" + ((lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")) ? ")" : ",PKID) ") +
                                    "SELECT DISTINCT" + sStrTop + "a.ObjetoID,0,a.RelacaoID,a.SujeitoID, NULL as Lote , b.Conceito," + sStrInsertEstoque + "," +
                                            "b.Descricao,b.Modelo,b.ProdutoID,b.MarcaID,b.LinhaProdutoID,a.Observacao,a.ClassificacaoFiscalID, " +
                                            "a.EstadoID,a.MargemMinima " + ((lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")) ? "" : ",l.PKID ") +
                                        "FROM RelacoesPesCon a WITH(NOLOCK)" +
                                                "INNER JOIN Conceitos b WITH(NOLOCK) ON a.ObjetoID=b.ConceitoID " +
                                                "INNER JOIN fn_FopagEmpresas_tbl(@EmpID, NULL) c ON a.SujeitoID=c.EmpresaID ";

                        if (!(lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")))
                        {
                            sql = sql + "INNER JOIN @Result_Tbl l ON (l.ProdutoID = a.ObjetoID) ";
                        }

                        if (int.Parse(lstPre_EmaClienteID) > 0)
                        {
                            sql = sql + "INNER JOIN EmailMarketing_Clientes_Produtos m WITH(NOLOCK) ON ((m.ProdutoID = a.ObjetoID) AND (m.EmpresaID = a.SujeitoID) AND (m.EmaClienteID = @EmaClienteID)) ";
                        }

                        sql = sql + sStrInsertLote2 + sStrInsertLote3;

                        if (sOperator.Trim() == "LIKE")
                        {
                            sql = sql + strAdditionalFrom1;
                        }

                        sql = sql + strAdditionalFrom2;

                        sql = sql + " WHERE " + sAccessAllowed + " 1=1 ";

                        //Where filtro por Familia, Marca, LinhaProduto. SGP
                        if (int.Parse(lstPre_FamiliaID) > 0)
                        {
                            sql = sql + " AND b.ProdutoID = " + int.Parse(lstPre_FamiliaID) + " ";
                        }

                        if (int.Parse(lstPre_MarcaID) > 0)
                        {
                            sql = sql + " AND b.MarcaID = " + int.Parse(lstPre_MarcaID) + " ";
                        }

                        if (int.Parse(lstPre_LinhaProdutoID) > 0)
                        {
                            sql = sql + " AND b.LinhaProdutoID = " + int.Parse(lstPre_LinhaProdutoID) + " ";
                        }

                        if (spcClause != null && spcClause != "")
                        {
                            sql = sql + spcClause + " ";
                        }

                        lstPre_FldKey2 = fldKey;
                        //if (lstPre_FldKey2 == "b.Conceito")
                        //lstPre_FldKey2 = "b.Conceito + SPACE(1) + ISNULL(b.Modelo, SPACE(0)) + SPACE(1) + ISNULL(b.PartNumber, SPACE(0)) + SPACE(1) + ISNULL(b.Serie, SPACE(0))";

                        /*if (sOperator.Trim() == "LIKE")
                        {
                            sql = sql + strAdditionalCondition1 + strAdditionalCondition2;
                        }
                        else if (lstPre_ProdutoReferenciaID != "0")
                        {
                            sql = sql + " AND " + lstPre_FldKey2 + sOperator + " @TTS " + strAdditionalCondition2 + " ";
                        }
                        else
                        {
                            sql = sql + " AND " + lstPre_FldKey2 + sOperator + " @TTS ";
                        }*/

                        sql = sql + condition + sFiltroContexto + sFiltro;

                        if (!(lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")))
                            sql = sql + " ORDER BY l.PKID ";

                        /*if (fldKey == "a.ObjetoID")
                        {
                            sql = sql + " ORDER BY l.PKID, " + fldKey + " " + order;
                        }
                        else
                        {
                            sql = sql + " ORDER BY l.PKID, " + fldKey + " " + order + " ,a.ObjetoID";
                        }*/

                        sql = sql + sStrDeleteEstoque;

                        if (int.Parse(lstPre_ChavePessoaID) == 2)
                        {
                            sStrCFOP = "dbo.fn_EmpresaPessoaTransacao_CFOP(EmpresaID,NULL,@PaisID,@Con,@Suf,@UFID,@Isen,@TranID,@FinalID,IDFromPesq,NULL) ";
                            sStrProdAliq = "dbo.fn_Produto_Aliquota(EmpresaID,IDFromPesq,NULL,@Con,@UFID,@CidID,NULL,CFOPID) ";
                        }
                        else
                        {
                            sStrCFOP = "dbo.fn_EmpresaPessoaTransacao_CFOP(EmpresaID,@PesID,NULL,NULL,NULL,NULL,NULL,@TranID,@FinalID,IDFromPesq,NULL) ";
                            sStrProdAliq = "dbo.fn_Produto_Aliquota(EmpresaID,IDFromPesq,@PesID2,NULL,NULL,NULL,NULL,CFOPID) ";
                        }

                        sql += " UPDATE @TempTable SET CFOPID = " + sStrCFOP;
                        sql += "UPDATE @TempTable SET ProdAliq = " + sStrProdAliq;
                        //sql += "UPDATE @TempTable SET conceito = dbo.fn_Produto_Descricao(idfrompesq,NULL,1,0) ";


                        if ((pageSize.intValue() <= 10) && (int.Parse(lstPre_PessoaID) > 0) && (int.Parse(lstPre_ParceiroID) > 0))
                        {
                            sUltimoPreco = "dbo.fn_Produto_PessoaUltimoPreco(a.EmpresaID,a.IDFromPesq,@PesID,@ParID,@IdiID)";
                        }
                        else
                        {
                            sUltimoPreco = "NULL";
                        }

                        if (empresaId.intValue() == 7)
                        {
                            sSQLPrecoZero = "ISNULL(dbo.fn_Preco_Preco(a.EmpresaID,a.IDFromPesq,NULL,NULL,0,NULL,NULL,GetDate(),NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL,NULL, NULL),0) PrecoZero,";
                            sSQLEstoqueEquipe = "STR(dbo.fn_Produto_EstoqueEquipe(a.EmpresaID,a.IDFromPesq,-" + (userId) + ",4),6,0) EstoqueEquipe,";
                        }
                        else
                        {
                            sSQLPrecoZero = "NULL PrecoZero,";
                            sSQLEstoqueEquipe = "NULL EstoqueEquipe,";
                        }

                        if (pageSize.intValue() <= 1)
                        {
                            if (int.Parse(lstPre_ChavePessoaID) == 2)
                            {
                                sSQLMC = "dbo.fn_Preco_MargemLista(a.EmpresaID,a.IDFromPesq, @Quant, dbo.fn_Preco_PrecoLista(a.EmpresaID,a.IDFromPesq,NULL,ProdAliq,@MoedaID,GETDATE(),@LP,@ImpInc,@Quant," +
                                         "0,@PesID,@ParID,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,@FinanID,@Frete,@MTID,NULL,@FinalID,a.CFOPID," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "), " +
                                         "NULL,NULL,ProdAliq,@MoedaID,GETDATE(),@ImpInc,@PesID,@ClaPesID,@FD,@Con,@SN,@UFID,@CidID,@Cnae,@TipPesID,@ParID,@FinanID," +
                                         "@Frete,@MTID,NULL,@FinalID,a.CFOPID,1, " + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + ", NULL) MC,";
                                sSQLEstoqueDisp1 = "STR(dbo.fn_Produto_Estoque(a.EmpresaID,a.IDFromPesq,-356,a.EmpresaID,NULL,dbo.fn_Produto_EstoqueGlobal(a.IDFromPesq,a.EmpresaID,NULL,@TipPesID),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "),6,0) Disponivel1,";
                                sSQLEstoqueDisp2 = "STR(dbo.fn_Produto_Estoque(a.EmpresaID,a.IDFromPesq,-356,-1,NULL,dbo.fn_Produto_EstoqueGlobal(a.IDFromPesq,a.EmpresaID,NULL,@TipPesID),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "),6,0) Disponivel2,";
                                sSQLEstoqueReserva = "STR(dbo.fn_Produto_Estoque(a.EmpresaID,a.IDFromPesq,353,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(a.IDFromPesq,a.EmpresaID,NULL,@TipPesID),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "),6,0) Reserva,";
                                sSQLDisponibilidade = "dbo.fn_Produto_PrevisaoEntregaContratual(a.IDFromPesq, a.EmpresaID, @Quant, " + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + ") AS PrevisaoEntrega, ";
                                // sSQLDisponibilidade = "dbo.fn_Produto_Disponibilidade(a.EmpresaID,a.IDFromPesq,NULL,GETDATE(),NULL,@IdiEmpID,1) dtPrevisaoDisponibilidade,";
                                sSQLPMP = "ISNULL(dbo.fn_Empresa_Financiamento(a.EmpresaID,@FinanID,@MoedaID,1),0) PMP,";

                            }
                            else
                            {
                                sSQLMC = "dbo.fn_Preco_MargemLista(a.EmpresaID,a.IDFromPesq, @Quant,dbo.fn_Preco_PrecoLista(a.EmpresaID,a.IDFromPesq,NULL,ProdAliq,@MoedaID,GETDATE(),@LP,@ImpInc,@Quant," +
                                                "0,@PesID,@ParID,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,@FinanID,@Frete,@MTID,NULL,@FinalID,a.CFOPID," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "), " +
                                         "NULL,NULL,ProdAliq,@MoedaID,GETDATE(),@ImpInc,@PesID,@ClaPesID,@FD,@Con,@SN,@UFID,@CidID,@Cnae,@TipPesID,@ParID,@FinanID," +
                                         "@Frete,@MTID,NULL,@FinalID,a.CFOPID,1, " + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + ", NULL) MC,";
                                sSQLEstoqueDisp1 = "STR(dbo.fn_Produto_Estoque(a.EmpresaID,a.IDFromPesq,-356,a.EmpresaID,NULL,dbo.fn_Produto_EstoqueGlobal(a.IDFromPesq,a.EmpresaID,@PesID,NULL),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "),6,0) Disponivel1,";
                                sSQLEstoqueDisp2 = "STR(dbo.fn_Produto_Estoque(a.EmpresaID,a.IDFromPesq,-356,-1,NULL,dbo.fn_Produto_EstoqueGlobal(a.IDFromPesq,a.EmpresaID,@PesID,NULL),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "),6,0) Disponivel2,";
                                sSQLEstoqueReserva = "STR(dbo.fn_Produto_Estoque(a.EmpresaID,a.IDFromPesq,353,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(a.IDFromPesq,a.EmpresaID,@PesID,NULL),NULL," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "),6,0) Reserva,";
                                sSQLDisponibilidade = "dbo.fn_Produto_PrevisaoEntregaContratual(a.IDFromPesq, a.EmpresaID, @Quant, " + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + ") AS PrevisaoEntrega, ";
                                // sSQLDisponibilidade = "dbo.fn_Produto_Disponibilidade(a.EmpresaID,a.IDFromPesq,NULL,GETDATE(),NULL,@IdiEmpID,1) dtPrevisaoDisponibilidade,";
                                sSQLPMP = "ISNULL(dbo.fn_Empresa_Financiamento(a.EmpresaID,@FinanID,@MoedaID,1),0) PMP,";

                            }
                        }
                        else
                        {
                            sSQLMC = "NULL MC,";
                            sSQLEstoqueDisp1 = "NULL Disponivel1,";
                            sSQLEstoqueDisp2 = "NULL Disponivel2,";
                            sSQLEstoqueReserva = "NULL Reserva,";
                            sSQLDisponibilidade = "NULL PrevisaoEntrega,";
                            sSQLPMP = "NULL PMP,";
                        }

                        sql2 = "SELECT DISTINCT " + sStrTop +
                                 " a.IDFromPesq ConceitoID, dbo.fn_Produto_Descricao2(a.IDFromPesq, " + idiomaEmpresaId.intValue() + ", 11) Conceito, a.OK, f.Fantasia Empresa," +
                                 (sStrSelectLote != " " ? sStrSelectLote : "NULL") + " Lote ," + " c.RecursoAbreviado Estado, ";

                        
                        
                        
                        //Caso chave de pesquisa for "2" (Cota��o sem pessoa)                                                                                         
                        if (int.Parse(lstPre_ChavePessoaID) == 2)
                        {
                            sql2 = sql2 + "ISNULL(dbo.fn_Preco_PrecoLista(a.EmpresaID,a.IDFromPesq,NULL,ProdAliq,@MoedaID,GETDATE(),@LP,@ImpInc,@Quant," +
                                                                            "0,NULL,@ParID,@FD,@Con,@SN,@Suf,@UFID,@CidID,@Cnae,@Isen,1,@ClaParID,@ClaPesID,@TipPesID,NULL,NULL,@FinanID," +
                                                                            "@Frete,@MTID,NULL,@FinalID,a.CFOPID," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "),0) Preco, " +
                                                                            "CONVERT(INT,0) QuantidadeComprar, " + sSQLMC + "ProdAliq Imp, NULL AS Guia, NULL AS ICMSST, ";
                        }
                        else
                        {
                            sql2 = sql2 + "ISNULL(dbo.fn_Preco_PrecoLista(a.EmpresaID,a.IDFromPesq,NULL,ProdAliq,@MoedaID,GETDATE(),@LP,@ImpInc,@Quant," +
                                                                            "0,@PesID,@ParID,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,@FinanID,@Frete,@MTID,NULL, " +
                                                                            "@FinalID,a.CFOPID," + (sStrSelectLote != " " ? sStrSelectLote : "NULL") + "),0) Preco, CONVERT(INT,0) QuantidadeComprar, " +
                                                                            sSQLMC + "ProdAliq Imp, NULL AS Guia, NULL AS ICMSST, ";
                        }

                        sql2 = sql2 + "STR(" + sStrSelectEstoque + ",6,0) Disponivel," + sSQLEstoqueDisp1 + sSQLEstoqueDisp2 + sSQLEstoqueReserva + sSQLEstoqueEquipe +
                                "dbo.fn_Produto_PesosMedidas(a.IDFromPesq,NULL,10) QCx, " + sSQLDisponibilidade;

                        // "d.Conceito+SPACE(1)+e.Conceito+SPACE(1)+a.Modelo+SPACE(1)+ISNULL(h.Conceito+SPACE(1),SPACE(0))+a.Descricao DadosProd,f.PessoaID EmpresaID," +
                        sql2 = sql2 + "h.Conceito LinhaProduto, " + sUltimoPreco + " [�ltima Compra], " +
                             "a.Observacao Observacao, a.CFOPID CFOP, g.ClassificacaoFiscal NCM, " +
                             "NULL AS PrecoDigitado, " +
                             "NULL AS MargemDigitada, " +
                             "ISNULL(a.MargemMinima, 0) AS MargemMinima, " +
                             "NULL AS ICMSST_Unitario, " +
                             sSQLPrecoZero +
                             "dbo.fn_Produto_Descricao2(a.IDFromPesq, " + (idiomaEmpresaId) + ", 11) DadosProd, " +
                             "dbo.fn_Produto_EmailMarketingValido(a.EmpresaID,a.IDFromPesq,@ParID,NULL,NULL,NULL,1) TemEmailMKT, " +
                             "(SELECT TOP 1 DATEDIFF(dd,GETDATE(),dtMediaPagamento) FROM RelacoesPesCon WITH(NOLOCK) WHERE ObjetoID = a.IDFromPesq) DiasPagamento, " +
                             "f.PessoaID EmpresaID, " +
                             "a.IDTMP IDTMP, " +
                             "SPACE(0) AS fldError " +
                             "FROM @TempTable a " +
                                "INNER JOIN Recursos c WITH(NOLOCK)ON(a.EstadoID=c.RecursoID)" +
                                "INNER JOIN Conceitos d WITH(NOLOCK)ON(a.FamiliaID=d.ConceitoID)" +
                                "INNER JOIN Conceitos e WITH(NOLOCK)ON(a.MarcaID=e.ConceitoID)" +
                                "INNER JOIN Pessoas f WITH(NOLOCK)ON(f.PessoaID=a.EmpresaID)" +
                                "LEFT OUTER JOIN Conceitos g WITH(NOLOCK)ON(g.ConceitoID=a.ClaFiscalID)" +
                                "LEFT OUTER JOIN Conceitos h WITH(NOLOCK)ON(h.ConceitoID=a.LinhaID)" +
                                (sStrInnerLote2 != " " ? sStrInnerLote2 : " ") +
                                (sStrInnerLote3 != " " ? sStrInnerLote3 : " ");


                        //Filtra as empresas a serem mostradas na Listagemd e acordo com a Pessoa ou o Tipo de Pessoa.                                                                                     
                        //Caso chave de pesquisa seja "2" (Cota��o sem pessoa)
                        if (int.Parse(lstPre_ChavePessoaID) == 2)
                        {
                            sFiltro = "AND dbo.fn_Produto_EmpresaMostrar(a.IDFromPesq,a.EmpresaID,NULL,@TipPesID,1)=a.EmpresaID ";
                        }
                        //Caso chave de pesquisa seja "1" (Cota��o com pessoa)
                        else
                        {
                            sFiltro = "AND dbo.fn_Produto_EmpresaMostrar(a.IDFromPesq,a.EmpresaID ,@PesID,NULL,1)=a.EmpresaID ";
                        }

                        sql2 = sql2 + " WHERE (1=1) ";

                        if (spcClause != "")
                        {
                            sql2 = sql2 + spcClause + " ";
                        }

                        sql2 = sql2 + sFiltro +
                                " /*AND dbo.fn_Produto_ESD(IDFromPesq) = 0*/ " +
                                 " AND a.IDTMP>@StartID " +
                                 "ORDER BY " + ((!(lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%"))) ? "a.IDTMP" : "Conceito") + " SET NOCOUNT OFF  ";

                        sql += sql2;

                        // Codigo para depuracao no SQL - NAO REMOVER!!!
                        /*
                        string strParameters = "SELECT ";
                        for (i=2; i<parametrosSQL.Length; i++)
                        {
	                        strParameters += parametrosSQL[i].Name + "=";

	                        if (parametrosSQL[i].Data.GetType().FullName == "System.DBNull")
		                        strParameters += "NULL";
	                        else if (parametrosSQL[i].Data.GetType().FullName == "System.String")
                                strParameters += "'" + parametrosSQL[i].Data.ToString() + "'";
	                        else if (parametrosSQL[i].Data.GetType().FullName == "System.Int32")
                                strParameters += parametrosSQL[i].Data.ToString();
	                        else if (parametrosSQL[i].Data.GetType().FullName == "System.Boolean")
                                strParameters += ((bool)parametrosSQL[i].Data ? "1" : "0");

                            strParameters += ", ";
                        }
                        */
                    }
                    
                    else if (bQuoteBOM)
                    {
                        parametrosSQL = new ProcedureParameters[1];

                        string[] CotadorPartNumber = Request.Form[0].Split(new string[] { ";," }, StringSplitOptions.None);
                        string[] CotadorLineNumber = Request.Form[1].Split(new string[] { ";," }, StringSplitOptions.None);
                        string[] CotadorDescription = Request.Form[2].Split(new string[] { ";," }, StringSplitOptions.None);
                        string[] CotadorListPrice = Request.Form[3].Split(new string[] { ";," }, StringSplitOptions.None);
                        string[] CotadorQuantity = Request.Form[4].Split(new string[] { ";," }, StringSplitOptions.None);
                        string[] CotadorDiscount = Request.Form[5].Split(new string[] { ";," }, StringSplitOptions.None);
                        string[] CotadorServiceDuration = Request.Form[6].Split(new string[] { ";," }, StringSplitOptions.None);
                        string sQuoteBOM = Request.Form[7].ToString();
                        string sQuoteCurrency = Request.Form[8].ToString();

                        if (Request.QueryString["lstPre_Pais"] != null)
                            lstPre_Pais = Request.QueryString["lstPre_Pais"];

                        //Tratativa para retirar a virgula que pode estar como separador de milhar
                        for (int j = 0; j < CotadorListPrice.Length; j++)
                        {
                            CotadorListPrice[j] = CotadorListPrice[j].Replace(",", "");
                        }
                        for (int j = 0; j < CotadorQuantity.Length; j++)
                        {
                            CotadorQuantity[j] = CotadorQuantity[j].Replace(",", "");
                        }
                        for (int j = 0; j < CotadorDiscount.Length; j++)
                        {
                            CotadorDiscount[j] = CotadorDiscount[j].Replace(",", "");
                        }
                                              
                        if (Request.QueryString["lstPre_ParceiroID"] != null)
                            lstPre_ParceiroID = Request.QueryString["lstPre_ParceiroID"];

                        if (Request.QueryString["lstPre_PessoaID"] != null)
                            lstPre_PessoaID = Request.QueryString["lstPre_PessoaID"];

                        if ((Request.QueryString["lstPre_MarcaID"] != null) && (Request.QueryString["lstPre_MarcaID"] != ""))
                            lstPre_MarcaID = Request.QueryString["lstPre_MarcaID"];

                        if (Request.QueryString["lstPre_FinalidadeID"] != null)
                            lstPre_FinalidadeID = Request.QueryString["lstPre_FinalidadeID"];

                        if (Request.QueryString["lstPre_FinanciamentoID"] != null)
                            lstPre_FinanciamentoID = Request.QueryString["lstPre_FinanciamentoID"];

                        sql =   "USE Overfly " +
                                "DECLARE @PesID INT =" + lstPre_PessoaID + ", @ParID INT = " + lstPre_ParceiroID + ", @MarcaID INT = "  + lstPre_MarcaID  + ", @FinalidadeID INT = " + lstPre_FinalidadeID + ", " +
                                        "@FinanciamentoID INT = " + lstPre_FinanciamentoID + ",@CurrLine INT, @LinesNumber INT, @LineNumber VARCHAR(40), @LineNumber2 VARCHAR(40), @LineNumberMae VARCHAR(40), " +
                                        "@LineNumberMae2 VARCHAR(40), @ProdutoID INT, @NCM VARCHAR(25), @NCMMae VARCHAR(25), @OrigemID INT, @MoedaIDProposta INT, @MoedaIDMae INT, @Quant INT, @TipoProdutoID INT, " +
                                        "@PrecoFob NUMERIC(20,2), @PrecoFobDesc NUMERIC(20, 2), @EmpresaID INT, @EmpresaIDMae INT, @ICMSST NUMERIC(11, 2), @Preco NUMERIC(11, 2), @TipoPagamentoST INT, " +
                                        "@Desconto NUMERIC(20, 2), @QuoteBOM VARCHAR(10) = '" + sQuoteBOM + "', @Margem NUMERIC(5,2) = dbo.fn_Cotador_Margem(" + lstPre_MarcaID  +",1), " +
                                        "@CFOPID INT, @PartNumber VARCHAR(200), @PartNumberMae VARCHAR(200), @OrigemMae VARCHAR(35), @LinhaProdutoMae VARCHAR(400), @OrigemMaeID INT, @TipoProdutoIDMae INT, " +
                                        "@PrazoMae INT, @DescontoMae NUMERIC(11,2), @PrevisaoEntrega VARCHAR(60), @FIE NUMERIC(10,4), @FIS NUMERIC(10,4), @CorpProdutoID INT, @CFOPIDMae INT, @MoedaIDInterface INT, " + 
                                        "@PaisID INT = " + lstPre_Pais + ", @Data DATETIME, @Vigencia INT, @Origem varchar(35), @ConceitoID INT, @TaxaConversaoID INT, @TagValor VARCHAR(100), @TaxaConversao NUMERIC(15,7), @DescontoLista numeric(20, 2) " +

                                //Tabela @Cotador recebe os dados como est�o na GPL/Quote
                                "DECLARE @Cotador TABLE(ID INT IDENTITY, PartNumber VARCHAR(200), LineNumber VARCHAR(40), CotDescription VARCHAR(400), ListPrice NUMERIC(20,2), Quantity INT, " +
                                                        "Discount NUMERIC(20,2), Vigencia INT)" +
                                
                                "INSERT INTO @Cotador(PartNumber, LineNumber, CotDescription, ListPrice, Quantity, Discount, Vigencia) VALUES ";

                        int cotLength = CotadorPartNumber.Length;

                        if((CotadorLineNumber.Length == cotLength) && (CotadorDescription.Length == cotLength) && (CotadorListPrice.Length == cotLength) && 
                           (CotadorQuantity.Length == cotLength) && (CotadorDiscount.Length == cotLength))
                        {
                            for(int j = 0; j < cotLength; j++)
                            {
                                if(CotadorPartNumber[j] != "")
                                {
                                    if (sql[sql.Length-1] == ')')
                                    {
                                        sql += ",";
                                    }

                                    sql += "('" + CotadorPartNumber[j] + "','" + CotadorLineNumber[j] + "','" + CotadorDescription[j] + "'," + CotadorListPrice[j] + "," + CotadorQuantity[j] + "," + CotadorDiscount[j] + "," + CotadorServiceDuration[j] + ")";
                                }
                            }
                        }
                        else
                        {
                            ;
                        }

                        sql += "SELECT @LinesNumber = @@ROWCOUNT " +
                                "SET @CurrLine = 1 " +

                                "SET @MoedaIDProposta = " + sQuoteCurrency + " " +

                                //Tabela @Conceito recebe os dados dos produtos quando cadastrados no sistema
                                "DECLARE @Conceitos TABLE(ConceitoID INT, PartNumber VARCHAR(200)) " +
                                "WHILE (@CurrLine <= @LinesNumber) " +
                                "BEGIN " +
                                    "SELECT " +
		                                "@ConceitoID = c.ConceitoID, @PartNumber = a.PartNumber " +
		                                "FROM @Cotador a " +
			                                "INNER JOIN CorpProdutos b WITH (NOLOCK) ON b.Produto = a.PartNumber " +
			                                "INNER JOIN Conceitos c WITH (NOLOCK) ON c.PartNumber = b.Produto " +
			                                "INNER JOIN RelacoesPesCon d WITH (NOLOCK) ON d.ObjetoID = c.ConceitoID " +
                                        "WHERE b.OrigemID = d.OrigemID AND d.SujeitoID = dbo.fn_Cotador_EmpresaCompativel(b.TipoProdutoID, b.OrigemID, b.MarcaID) " +
				                                "AND c.EstadoID NOT IN (1, 3, 4, 5, 11) AND d.EstadoID NOT IN (1, 3, 4, 5, 11) AND " +
                                                "b.MoedaID = (CASE WHEN (b.TipoProdutoID = 1134) THEN 647 ELSE @MoedaIDProposta END) AND a.ID = @CurrLine " +
		                                        "ORDER BY d.EstadoID ASC " +

	                                "SELECT @Quant = COUNT(1) " +
		                                "FROM @Conceitos a " +
		                                "WHERE a.PartNumber = @PartNumber AND a.ConceitoID = @ConceitoID " +

	                                "IF (@Quant = 0 AND @PartNumber IS NOT NULL AND @ConceitoID IS NOT NULL) " +
		                                "INSERT INTO @Conceitos (ConceitoID, PartNumber) VALUES (@ConceitoID,@PartNumber) " +

	                                "SET @CurrLine = @CurrLine + 1 " +
                                    "SELECT @ConceitoID = NULL, @PartNumber = NULL, @Quant = NULL " +
                                "END " +

                                 

                                //Tabela @Cotador2 recebe os dados trabalhados
                                "DECLARE @Cotador2 TABLE (IDCot INT IDENTITY, LineNumber VARCHAR(40), OK BIT, ProdutoID INT, PartNumber VARCHAR(200), ProdutoSubstituto varchar(200), Origem VARCHAR(35), " +
                                                            "NCM VARCHAR(25), Produto VARCHAR(400), Vigencia INT, EmpresaID INT, PrecoFob NUMERIC(20, 2), PrecoFobDesc NUMERIC(20,2), " +
                                                            "Desconto NUMERIC(20,2), Preco NUMERIC(20,2), Guia VARCHAR(400), ICMSST NUMERIC(11,2), MC NUMERIC(5,2), Quant INT, Disponivel NUMERIC(20,2), " +
                                                            "PrevisaoEntrega VARCHAR(400), LinhaProduto VARCHAR(400), OrigemID INT, TipoProdutoID INT, CorpProdutoID INT, ProdutoVerTambem varchar(200), " +
                                                            "CFOP INT, FIE NUMERIC(10,4), FIS NUMERIC(10,4)) " +

                                "INSERT INTO @Cotador2(LineNumber, ProdutoID, OK, PartNumber, ProdutoSubstituto, Origem,  NCM, Produto, Vigencia, EmpresaID, PrecoFob, PrecoFobDesc, Desconto, Preco, " +
                                                        "Guia, ICMSST, MC, Quant, Disponivel, PrevisaoEntrega, LinhaProduto, OrigemID, TipoProdutoID, CorpProdutoID, ProdutoVerTambem, CFOP, FIE, FIS) " +
                                    "SELECT	a.LineNumber, c.ConceitoID, 1, a.PartNumber, b.ProdutoSubstituto, " +
                                    "SUBSTRING(e.Observacao, CHARINDEX(\'-\', e.Observacao, 0) + 1, LEN(e.Observacao)), " +
                                    "d.Conceito as NCM, ISNULL(b.DescricaoProduto,a.CotDescription) AS Produto, a.Vigencia, " +
                                    "dbo.fn_Cotador_EmpresaCompativel(b.TipoProdutoID, b.OrigemID, b.MarcaID) AS EmpresaID, " +
                                    "a.ListPrice AS 'Pre�o Fob', 0 AS 'Pre�o Fob Desc', a.Discount AS Desconto, 0 AS Pre�o, '' AS Guia, 0.00 AS 'ICMS-ST', @Margem AS MC, " +
                                    "a.Quantity AS Quant, 0 AS Disponivel, '' AS 'Previs�o Entrega', ISNULL(dbo.fn_TipoAuxiliar_Item(b.TipoProdutoID, 0), NULL) AS 'Linha Produto', b.OrigemID, " +
                                    "b.TipoProdutoID, b.CorpProdutoID, b.ProdutoVerTambem, NULL AS CFOP, 0.00 AS FIE, 0.00 AS FIS " +
                                        "FROM @Cotador a " +
                                            "LEFT JOIN CorpProdutos b WITH(NOLOCK) ON b.Produto = a.PartNumber AND b.MoedaID = @MoedaIDProposta " +
                                            "LEFT JOIN @Conceitos c ON c.PartNumber = a.PartNumber " +
                                            "LEFT JOIN Conceitos d WITH(NOLOCK) ON d.ConceitoID = b.NCMID " +
                                            "LEFT JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON e.ItemID = b.OrigemID " +
                                        "ORDER BY a.ID " +

                                "SELECT @LinesNumber = @@ROWCOUNT " +

                                "SET @CurrLine = 1 " +

                                //Loop para tratar os dados de servi�o quando a GPL/Quote � importada. Todo servi�o no sistema � cadastrado como Nacional, contudo na GPL a cota��o vem em Dolar
                                "IF(@MoedaIDProposta = 541) " +
                                "BEGIN " + 
                                    "WHILE @CurrLine <= @LinesNumber " +
                                    "BEGIN " +
                                        "DECLARE @CorpServicoID INT, @TipoServicoID INT, @OrigemServicoID INT " +
                                        "SET @PartNumber = NULL " +

                                        "SELECT @CorpServicoID = b.CorpProdutoID, @TipoServicoID = b.TipoProdutoID, @OrigemServicoID = b.OrigemID, @PartNumber = a.PartNumber " +
                                            "FROM @Cotador2 a " +
                                                "INNER JOIN CorpProdutos b WITH(NOLOCK) ON b.Produto = a.PartNumber " +
                                            "WHERE IDCot = @CurrLine AND b.OrigemID NOT IN (2701,2706) " +
                                        "IF(@PartNumber LIKE '%CON-%') " +
                                        "BEGIN " +
                                            "UPDATE @Cotador2 SET TipoProdutoID = @TipoServicoID, CorpProdutoID = @CorpServicoID, OrigemID = @OrigemServicoID, " +
                                                    "EmpresaID = 2, LinhaProduto = ISNULL(dbo.fn_TipoAuxiliar_Item(@TipoServicoID, 0), NULL) " +
                                                "WHERE IDCot = @CurrLine " +
                                        "END " +

                                        "SET @CurrLine += 1 " +
                                    "END " +
                                "END " +

                                "SET @CurrLine = 1 " +

                                //Tabela @ProdutosMaes recebe os dados dos itens principais e replica para os subitens (Itens filhos)
                                "DECLARE @ProdutosMaes TABLE(ProdMaeID INT IDENTITY, LineNumber VARCHAR(40), Origem VARCHAR(35), NCM VARCHAR(25), EmpresaID INT, LinhaProduto VARCHAR(400), OrigemID INT, " +
                                                                "TipoProdutoID INT, PrevisaoEntrega VARCHAR(60), Desconto NUMERIC(11,2), CFOPID INT) " +

                               
                                "WHILE(@CurrLine <= @LinesNumber) " +
                                "BEGIN " +
                                    "SELECT	@LineNumberMae = LineNumber, @PartNumberMae = PartNumber, @OrigemMae = Origem, @NCMMae = NCM, @EmpresaIDMae = EmpresaID, @LinhaProdutoMae = LinhaProduto, " +
                                            "@OrigemMaeID = OrigemID,  @TipoProdutoIDMae = TipoProdutoID, @TipoProdutoIDMae = TipoProdutoID, @DescontoLista = Desconto, " +
                                            "@Desconto = dbo.fn_Cotador_ProdutoDesconto(CorpProdutoID, NULL) " +
                                    "FROM @Cotador2 " +
                                    "WHERE IDCot = @CurrLine " +

                                    //Preenchimento da tabela com os itens principais
                                    "IF(@TipoProdutoIDMae <> 1134 OR @PartNumberMae NOT LIKE '%CON-%') " +
                                    "BEGIN " +
                                        "SET @LineNumberMae2 = REPLACE(@LineNumberMae, '.', ',')" +
                                        "IF(CHARINDEX(',', @LineNumberMae2) = 0 OR (CHARINDEX(',0', @LineNumberMae2) <> 0 AND CHARINDEX(',0,',@LineNumberMae2) = 0)) " +
                                        "BEGIN " +

                                            "SELECT @PrazoMae = dbo.fn_Produto_PrevisaoEntregaAtual(NULL, NULL, CorpProdutoID, NULL, NULL, NULL, NULL) FROM @Cotador2 WHERE IDCot = @CurrLine " +
                                            "IF(@PrazoMae IS NULL) " +
                                            "BEGIN " +
                                                "SET @PrevisaoEntrega = 'N�o � possivel.' " +
                                            "END " +
                                            "ELSE " +
                                            "BEGIN " +
                                                "IF((@EmpresaIDMae = 2 AND @TipoProdutoIDMae = 1133 AND(CASE @OrigemMaeID WHEN 2701 THEN 1 WHEN 2706 THEN 1 ELSE 0 END) = 0)) " +
                                                "BEGIN " +
                                                    "SET @PrevisaoEntrega = 'At� 10 dias �teis ap�s o HW' " +
                                                "END " +
                                                "ELSE IF(@PrazoMae <> 0) " +
                                                "BEGIN " +
                                                    "SET @PrevisaoEntrega = 'At� ' + CONVERT(VARCHAR,@PrazoMae) + ' dias �teis' " +
                                                "END " +
                                                "ELSE IF(@PrazoMae = 0) " +
                                                "BEGIN " +
                                                    "SET @PrevisaoEntrega = '' " +
                                                "END " +
                                            "END " +

                                            "INSERT INTO @ProdutosMaes(LineNumber, Origem, NCM, EmpresaID, LinhaProduto, OrigemID, TipoProdutoID, PrevisaoEntrega) " +
                                                "VALUES(@LineNumberMae, @OrigemMae, @NCMMae, @EmpresaIDMae, @LinhaProdutoMae, @OrigemMaeID, @TipoProdutoIDMae, @PrevisaoEntrega) " +
                                        "END " +

                                        //Atualiza a previs�o de entrega do Item principal na tabela principal

                                        "IF (ISNULL(@DescontoLista, 0) > ISNULL(@Desconto, 0)) " +
			                                "SET @Desconto = @DescontoLista " +

                                        "UPDATE @Cotador2 SET PrevisaoEntrega = @PrevisaoEntrega, Desconto = @Desconto WHERE IDCot = @CurrLine " +

                                    "END " +
                                    "SET @CurrLine = @CurrLine + 1 " +
                                "END " +

                                "SET @CurrLine = 1 " +

                                //Preenchimento dos subitens (itens fihos)
                                "WHILE(@CurrLine <= @LinesNumber) " + 
                                "BEGIN " +

                                    "SET @LineNumber2 = REPLACE(@LineNumber, '.', ',') " +
                                    "SELECT @LineNumberMae = NULL, @OrigemMae = NULL, @NCMMae = NULL, @EmpresaIDMae = NULL, " +
                                        "@LinhaProdutoMae = NULL, @PrevisaoEntrega = NULL, @Origem = NULL " +

                                    "SELECT @ProdutoID = ProdutoID, @LineNumber = LineNumber, @PartNumber = PartNumber, @NCM = NCM, " +
                                    "@OrigemID = OrigemID, @DescontoLista = dbo.fn_Cotador_ProdutoDesconto(CorpProdutoID, NULL), @Desconto = Desconto, " +
                                    "@TipoProdutoID = TipoProdutoID, @PrecoFobDesc = PrecoFobDesc, @PrecoFob = ISNULL(PrecoFob,0), @Quant = Quant " + 
                                        "FROM @Cotador2 WHERE IDCot = @CurrLine " +

                                    "IF (ISNULL(@DescontoLista, 0) > ISNULL(@Desconto, 0)) " +
		                                "SET @Desconto = @DescontoLista " +

                                    "SELECT @LineNumberMae = LineNumber, @OrigemMae = Origem, @OrigemMaeID = OrigemID, @NCMMae = NCM, @EmpresaIDMae = EmpresaID, " +
                                            "@LinhaProdutoMae = LinhaProduto, @TipoProdutoIDMae = TipoProdutoID, @PrevisaoEntrega = PrevisaoEntrega " +
                                        "FROM @ProdutosMaes " +
                                        "WHERE LEFT(LineNumber, CHARINDEX('.', LineNumber)) = LEFT(@LineNumber, CHARINDEX('.', @LineNumber)) " +

                                    "IF(@TipoProdutoID = 1134 OR @PartNumber LIKE '%CON-%') " +
                                    "BEGIN " +
                                        "SELECT @PrazoMae = dbo.fn_Produto_PrevisaoEntregaAtual(NULL,NULL,CorpProdutoID,NULL,NULL,NULL,NULL) FROM @Cotador2 WHERE IDCot = @CurrLine " +

                                        "IF(@PrazoMae IS NULL) " +
                                        "BEGIN " +
                                        	"SET @PrevisaoEntrega = 'N�o � possivel.' " +
                                        "END " +
                                        "ELSE " +
                                        "BEGIN " +
                                        	"IF((@EmpresaIDMae=2 AND (CASE @OrigemMaeID WHEN 2701 THEN 1 WHEN 2706 THEN 1 ELSE 0 END) = 1)) " +
                                        	"BEGIN " +
                                        		"SET @PrevisaoEntrega = 'At� 10 dias �teis ap�s o HW' " +
                                        	"END " +
                                        	"ELSE IF(@PrazoMae <> 0) " +
                                        	"BEGIN " +
                                        		"SET @PrevisaoEntrega = 'At� ' + CONVERT(VARCHAR,@PrazoMae) + ' dias �teis' " +
                                        	"END " +
                                        	"ELSE IF(@PrazoMae = 0) "+ 
                                        	"BEGIN " +
                                        		"SET @PrevisaoEntrega = '' " +
                                        	"END "+
                                        "END " +

                                        "UPDATE @Cotador2 SET NCM = 'Servi�o', LinhaProduto = ISNULL(dbo.fn_TipoAuxiliar_Item(1134, 0), NULL), " +
                                            "TipoProdutoID = 1134, EmpresaID = 2, PrevisaoEntrega = @PrevisaoEntrega, Desconto = @Desconto " +
                                            "WHERE IDCot = @CurrLine " +

                                        "SELECT @Origem = SUBSTRING(b.Observacao,CHARINDEX('-',b.Observacao,0) + 1,LEN(b.Observacao)) " +
                                            "FROM @Cotador2 a " +
                                                "INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.OrigemID) " +
                                            "WHERE IDCot = @CurrLine " +

                                        "UPDATE @Cotador2 SET Origem = @Origem WHERE IDCot = @CurrLine " +

                                    "END " +
                                    "ELSE IF(((@PrecoFob = 0 AND @TipoProdutoID IS NULL) OR (@PrecoFob > 0 AND @TipoProdutoID = 1132)) AND CHARINDEX(',', @LineNumber2) <> 0 OR (CHARINDEX(',0', @LineNumber2) = 0 AND CHARINDEX(',0,',@LineNumber2) <> 0)) " +
                                    "BEGIN " +
                                        "UPDATE @Cotador2 SET Origem = @OrigemMae, OrigemID = @OrigemMaeID, NCM = @NCMMae, EmpresaID = @EmpresaIDMae, " +
                                            "LinhaProduto = @LinhaProdutoMae, TipoProdutoID = @TipoProdutoIDMae, PrevisaoEntrega = @PrevisaoEntrega, Desconto = @Desconto " +
                                            "WHERE IDCot = @CurrLine " +
                                    "END " +
                                    "ELSE IF (@TipoProdutoIDMae = 1132 AND @TipoProdutoID = 1133 AND @OrigemID IN (2701, 2706)) " +
                                    "BEGIN " +
                                        "UPDATE @Cotador2 SET NCM = @NCMMae, EmpresaID = @EmpresaIDMae WHERE IDCot = @CurrLine " +
                                    "END " +

                                    "SELECT @EmpresaID = EmpresaID, @NCM = NCM, " +
                                            "@OrigemID = OrigemID " +
                                        "FROM @Cotador2  WHERE IDCot = @CurrLine " +

                                    "SET @Data = dbo.fn_Data_Hora(GETDATE(), '01/01/2001 00:00:00.000') " +

                                    //Calculo do pre�o e impostos dos itens que possuem valor.
                                    "IF(@MoedaIDProposta IS NOT NULL AND @PrecoFob > 0) " +
                                    "BEGIN " +
                                        "SELECT @ICMSST = NULL, @TipoPagamentoST = NULL, @Preco = NULL, @FIE = NULL, @FIS = NULL, @CorpProdutoID = NULL " +

                                        "SET @MoedaIDInterface = (CASE WHEN @PaisID = 130 THEN 647 ELSE 541 END) " +
                     

                                        "UPDATE @Cotador2 SET PrecoFobDesc = dbo.fn_Cotador_CustoReposicao(EmpresaID, CorpProdutoID, Desconto, 0, NULL) WHERE IDCot = @CurrLine " +

                                        "SELECT @PrecoFobDesc = a.PrecoFobDesc, @TipoProdutoID = a.TipoProdutoID, @CorpProdutoID = a.CorpProdutoID, @Vigencia = a.Vigencia, @TaxaConversaoID = b.TaxaConversaoID " +
                                            "FROM @Cotador2 a "+
                                                "INNER JOIN CorpProdutos b WITH(NOLOCK) ON b.CorpProdutoID = a.CorpProdutoID " +
                                            "WHERE IDCot = @CurrLine " +

                                        //O servi�o � cadastrado com valor anual na CorpProdutos e no BOM/Quote vem multiplicado pela vig�ncia (Service Duration)
                                        "IF(@Vigencia>12 AND @TipoProdutoID = 1134) " +
                                        "BEGIN " +
                                            "SET @PrecoFobDesc = @PrecoFobDesc*(@Vigencia/12) " +
                                            "UPDATE @Cotador2 SET PrecoFobDesc = @PrecoFobDesc WHERE IDCot = @CurrLine " +
                                        "END " +
                                        "ELSE IF (@TipoProdutoIDMae = 1132 AND @TipoProdutoID = 1133 AND @OrigemID IN (2701, 2706)) " +
                                         "BEGIN " +
                                            "SET @TipoProdutoID = 1132 " +
                                        "END " +

                                        "EXEC sp_Preco_Corporativo @ProdutoID, @NCM, @MarcaID, @OrigemID, @MoedaIDInterface, @Quant, @ParID, @PesID, @FinalidadeID, 0 /*Frete*/, @PrecoFobDesc, @Margem, " +
                                                "@FinanciamentoID, @EmpresaID, @TipoProdutoID, 1 , NULL, NULL, @ICMSST OUTPUT, NULL, NULL, @TipoPagamentoST OUTPUT, NULL, @Preco OUTPUT, " +
                                                "NULL, @FIE OUTPUT, NULL, NULL, @CFOPID OUTPUT " +

                                        //Taxa do d�lar segue outra cota��o segundo Igor Oliveira para os casos de servi�o importado devido a um acordo comercial com a Cisco. MTH.: 13 / 09 / 2018
                                        "IF ((@MoedaIDProposta = 541) AND (@TipoProdutoID = 1134) AND (@TaxaConversaoID IS NOT NULL) AND (@TaxaConversaoID != 0)) " +
                                        "BEGIN " +
                                            "SELECT @TagValor = dbo.fn_TagValor(Observacoes, 'TaxaConversao' , '<', '|', @TaxaConversaoID) " +
                                                "FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = @MarcaID " +

                                            "SELECT @TaxaConversao = SUBSTRING(@TagValor,CHARINDEX(';',@TagValor,0)+1,LEN(@TagValor)) " +

                                            "UPDATE @Cotador2 SET PrecoFob = (PrecoFob * @TaxaConversao) WHERE LineNumber = @LineNumber  " +
                                        "END " +

                                        "SET @FIS = dbo.fn_DivideZero(@Preco, dbo.fn_Cotador_CustoReposicao(@EmpresaID, @CorpProdutoID, @Desconto, 0, @MoedaIDInterface)) " +

                                        "UPDATE @Cotador2 SET Preco = ISNULL(@Preco,0), ICMSST = ISNULL(@ICMSST,0.00), Guia = @TipoPagamentoST, CFOP = @CFOPID, FIE = @FIE " +
                                            "WHERE IDCot = @CurrLine " +

                                        "UPDATE @ProdutosMaes SET CFOPID = @CFOPID WHERE LineNumber = @LineNumber " +
                                    "END " +

                                    "SET @CurrLine += 1 " +
                                "END " +

                                "SELECT @LinesNumber = COUNT(1) FROM @Cotador2 " +
                                "SET @CurrLine = 1 " +

                                //Loop para preenchimento do CFOPID do item principal (item m�e) para os subitens (itens fihos)
                                "WHILE (@CurrLine < @LinesNumber) " +
                                "BEGIN " +
                                    "SELECT @CFOPID = NULL, @LineNumber = NULL, @CFOPIDMae = NULL, @PartNumber = NULL, @TipoProdutoID = NULL " +

                                    "SELECT @CFOPID = CFOP, @LineNumber = LineNumber, @PartNumber = PartNumber, @TipoProdutoID = TipoProdutoID FROM @Cotador2 WHERE IDCot = @CurrLine " +

                                    "IF(@CFOPID IS NULL AND @TipoProdutoID <> 1134) " +
                                    "BEGIN " +
                                        "SELECT @CFOPIDMae = CFOPID FROM @ProdutosMaes WHERE LEFT(LineNumber,1) = LEFT(@LineNumber,1) " +

                                        "UPDATE @Cotador2 SET CFOP = @CFOPIDMae WHERE IDCot = @CurrLine " +
                                    "END " +

                                    "SET @CurrLine = @CurrLine + 1 " +
                                "END " +

                                "UPDATE @Cotador2 SET Disponivel = dbo.fn_Produto_Estoque(EmpresaID,ProdutoID,-356,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(EmpresaID,ProdutoID,@PesID,NULL),NULL,NULL) " +

                                "UPDATE @Cotador2 SET Guia = 'GARE' WHERE Guia = '1' " +
                                "UPDATE @Cotador2 SET Guia = 'GNRE' WHERE Guia = '2' " +
                                "UPDATE @Cotador2 SET Guia = '' WHERE Guia = '0' " +

                                "SELECT a.LineNumber, a.ProdutoID AS ConceitoID, '[' + a.PartNumber + ']' + SPACE(1) + a.Produto AS Produto, a.ProdutoSubstituto, a.OK, b.Fantasia AS Empresa, a.Origem, " +
                                        "a.Preco, a.Quant AS QuantidadeComprar, a.MC, ISNULL(a.Desconto,0) AS Desconto, a.NCM, a.Guia, a.ICMSST, a.Vigencia, a.PrecoFobDesc, a.PrecoFob, a.FIS, a.FIE, " +
                                        "a.Disponivel, a.PrevisaoEntrega, a.TipoProdutoID, a.LinhaProduto, CFOP, a.ProdutoVerTambem, " +
                                        "dbo.fn_Cotador_Inconsistencias(a.PartNumber,a.Quant,(CASE WHEN ((a.Vigencia>12) AND (a.TipoProdutoID = 1134)) THEN CAST(a.PrecoFob/(a.Vigencia/12) AS DECIMAL (11,2)) ELSE a.PrecoFob END), " +
                                        "a.Preco, a.LineNumber,ISNULL(a.Desconto,0),@MoedaIDProposta,a.CorpProdutoID,a.EmpresaID,NULL,2,0) AS Inconsistencia, a.EmpresaID, NULL AS PrecoDigitado, " +
                                        "NULL AS MargemDigitada, a.ICMSST AS ICMSST_Unitario, a.CorpProdutoID, a.PartNumber, @MoedaIDInterface AS MoedaID, c.SimboloMoeda AS Moeda, a.OrigemID AS OrigemID, SPACE(0) AS fldError " +
                                    "FROM @Cotador2 a " +
                                        "LEFT JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.EmpresaID " +
                                        "LEFT JOIN Conceitos c WITH(NOLOCK) ON c.ConceitoID = @MoedaIDInterface " +
                                    "ORDER BY a.IDCot";
                    }
                    //Fonte Cotador
                    else
                    {
                        if (Request.QueryString["lstPre_ParceiroID"] != null)
                            lstPre_ParceiroID = Request.QueryString["lstPre_ParceiroID"];

                        if (Request.QueryString["lstPre_PessoaID"] != null)
                            lstPre_PessoaID = Request.QueryString["lstPre_PessoaID"];

                        if (Request.QueryString["lstPre_Pais"] != null)
                            lstPre_Pais = Request.QueryString["lstPre_Pais"];

                        if (Request.QueryString["lstPre_Frete"] != null)
                            lstPre_Frete = Request.QueryString["lstPre_Frete"];

                        if (Request.QueryString["lstPre_FinanciamentoID"] != null)
                            lstPre_FinanciamentoID = Request.QueryString["lstPre_FinanciamentoID"];

                        if (Request.QueryString["lstPre_FinalidadeID"] != null)
                            lstPre_FinalidadeID = Request.QueryString["lstPre_FinalidadeID"];

                        if (Request.QueryString["lstPre_Quantidade"] != null)
                            lstPre_Quantidade = Request.QueryString["lstPre_Quantidade"];

                        if (Request.QueryString["lstPre_Estoque"] != null)
                            lstPre_Estoque = Request.QueryString["lstPre_Estoque"];

                        if (Request.QueryString["lstPre_TransacaoID"] != null)
                            lstPre_TransacaoID = Request.QueryString["lstPre_TransacaoID"];

                        if ((Request.QueryString["lstPre_MarcaID"] != null) && (Request.QueryString["lstPre_MarcaID"] != ""))
                            lstPre_MarcaID = Request.QueryString["lstPre_MarcaID"];

                        if ((Request.QueryString["lstPre_LinhaProdutoID"] != null) && (Request.QueryString["lstPre_LinhaProdutoID"] != ""))
                            lstPre_LinhaProdutoID = Request.QueryString["lstPre_LinhaProdutoID"];

                        lstPre_TextBusca = textToSearch.ToString();

                        sDefinicaoParametros = "@ParID INT,@PesID INT, @PaisID INT,@Frete BIT,@FinanID INT, @FinalID INT, @Quant INT, @TranID INT, @TTS NVARCHAR(128), @IdiID INT,@EmpID INT, " +
                                                "@PS INT, @MarcaID INT, @LinhaID INT ";

                        // Determinar tamanho dos parametros
                        parametrosSQL = new ProcedureParameters[16];

                        parametrosSQL[1] = new ProcedureParameters("@DefinicaoParametros", SqlDbType.NVarChar, sDefinicaoParametros, ParameterDirection.Input, 4000);
                        parametrosSQL[2] = new ProcedureParameters("@ParID", SqlDbType.Int, ((int.Parse(lstPre_ParceiroID) > 0) ? (Object)int.Parse(lstPre_ParceiroID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[3] = new ProcedureParameters("@PesID", SqlDbType.Int, ((int.Parse(lstPre_PessoaID) >= 0) ? (Object)int.Parse(lstPre_PessoaID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[4] = new ProcedureParameters("@PaisID", SqlDbType.Int, int.Parse(lstPre_Pais), ParameterDirection.Input);
                        parametrosSQL[5] = new ProcedureParameters("@Frete", SqlDbType.Bit, int.Parse(lstPre_Frete) >= 1 ? true : false, ParameterDirection.Input);
                        parametrosSQL[6] = new ProcedureParameters("@FinanID", SqlDbType.Int, int.Parse(lstPre_FinanciamentoID), ParameterDirection.Input);
                        parametrosSQL[7] = new ProcedureParameters("@FinalID", SqlDbType.Int, ((int.Parse(lstPre_FinalidadeID) > 0) ? (Object)int.Parse(lstPre_FinalidadeID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[8] = new ProcedureParameters("@Quant", SqlDbType.Int, int.Parse(lstPre_Quantidade), ParameterDirection.Input);
                        parametrosSQL[9] = new ProcedureParameters("@TranID", SqlDbType.Int, int.Parse(lstPre_TransacaoID), ParameterDirection.Input);
                        parametrosSQL[10] = new ProcedureParameters("@TTS", SqlDbType.NVarChar, textToSearch.ToString(), ParameterDirection.Input, 128);
                        parametrosSQL[11] = new ProcedureParameters("@IdiID", SqlDbType.Int, idiomaId.intValue(), ParameterDirection.Input);
                        parametrosSQL[12] = new ProcedureParameters("@EmpID", SqlDbType.Int, empresaId.intValue(), ParameterDirection.Input);
                        parametrosSQL[13] = new ProcedureParameters("@PS", SqlDbType.Int, pageSize.intValue(), ParameterDirection.Input);
                        parametrosSQL[14] = new ProcedureParameters("@MarcaID", SqlDbType.Int, ((int.Parse(lstPre_MarcaID) > 0) ? (Object)int.Parse(lstPre_MarcaID) : DBNull.Value), ParameterDirection.Input);
                        parametrosSQL[15] = new ProcedureParameters("@LinhaID", SqlDbType.Int, ((int.Parse(lstPre_LinhaProdutoID) > 0) ? (Object)int.Parse(lstPre_LinhaProdutoID) : DBNull.Value), ParameterDirection.Input);

                        sStrTop = " TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " ";

                        sql = "SET NOCOUNT ON " +

                                "DECLARE @LinesNumber INT, @CurrLine INT, @ProdutoID INT, @NCM VARCHAR(25), @OrigemID INT, @PrecoFobDesc NUMERIC(13,2), @EmpresaID INT, @MoedaID INT, @TipoProdutoID INT, " +
                                            "@Preco NUMERIC(11,2), @Produtos INT, @FIE NUMERIC(10,4), @FIS NUMERIC(10,4), @CV NUMERIC(10,4), @DP NUMERIC(10,4), @IPI NUMERIC(11,2), @ICMS NUMERIC(11,2), " +
                                            "@ICMSST NUMERIC(11,2), @DIFAL NUMERIC(11,2), @ISS NUMERIC(11,2), @TipoPagamentoST INT, @ValorFrete NUMERIC(11,2), @CFOPID INT, @Entrega VARCHAR(80), " +
                                            "@ConceitoNCM INT, @CorpProdutoID INT, @Desconto NUMERIC(5,2), @ConceitoID int, @PartNumber varchar(200) " +

                                "DECLARE @TempTable TABLE (IDTMP INT IDENTITY, ProdutoID INT, OK BIT, PartNumber VARCHAR(200), ProdutoSubstituto varchar(200), Produto VARCHAR(400), EmpresaID INT, " +
                                                            "OrigemID INT, Origem VARCHAR(35), TipoProdutoID INT, TipoProduto VARCHAR(80), NCM VARCHAR(25), MoedaID INT, Moeda VARCHAR(3), " +
                                                            "PrecoFob NUMERIC(13,2), Desconto NUMERIC(5,2),  PrecoFobDesc NUMERIC(13,2), PrecoUnitario NUMERIC(11,2), ValorFrete NUMERIC(11,2), " +
                                                            "IPI NUMERIC(11,2), ICMS NUMERIC(11,2), ICMSST NUMERIC(11,2), ISS NUMERIC(11,2), FIE NUMERIC(10,4), FIS NUMERIC(10,4), TipoPagamentoST INT, " +
                                                            "Estoque INT, Entrega VARCHAR(80), CorpProdutoID INT, CFOPID INT, ProdutoVerTambem varchar(200)) " +

                                "INSERT INTO @TempTable(PartNumber, ProdutoSubstituto, OK, Produto, EmpresaID, OrigemID, Origem, TipoProdutoID, TipoProduto, NCM, MoedaID, PrecoFob, Desconto, " +
                                                        "PrecoFobDesc, CorpProdutoID, ProdutoVerTambem) " +
                                    "SELECT DISTINCT " + sStrTop + " Produto, ProdutoSubstituto, 0, a.DescricaoProduto, " +
                                            "dbo.fn_Cotador_EmpresaCompativel(a.TipoProdutoID, a.OrigemID, a.MarcaID) [EmpresaID], a.OrigemID, " +
                                            "SUBSTRING(d.Observacao, CHARINDEX(\'-\', d.Observacao, 0) + 1, LEN(d.Observacao)) [Origem], TipoProdutoID [TipoProdutoID], " +
                                            "dbo.fn_TipoAuxiliar_Item(TipoProdutoID, 0) [TipoProduto], c.Conceito [NCM], (CASE WHEN @PaisID = 130 THEN 647 ELSE 541 END) AS MoedaID, a.PrecoLista [PrecoFob], " +
                                            "dbo.fn_Cotador_ProdutoDesconto(a.CorpProdutoID, NULL) [Desconto], a.PrecoLista, a.CorpProdutoID, a.ProdutoVerTambem " +
                                        "FROM CorpProdutos a WITH(NOLOCK) " +
                                            "LEFT JOIN Conceitos b WITH(NOLOCK) ON ((b.PartNumber = a.Produto) AND (b.EstadoID NOT IN (1, 3, 4, 5, 11))) " +
                                            "LEFT JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.NCMID) " +
                                            "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (d.ItemID = a.OrigemID) " +
                                        "WHERE ((a.MarcaID = @MarcaID ) AND (a.EstadoID = 2 OR a.ProdutoSubstituto IS NOT NULL) AND ((LEN(a.NCMID) > 0) OR (a.TipoProdutoID IN (1133,1134)))) ";

                        if (int.Parse(lstPre_LinhaProdutoID) > 0)
                            sql += " AND a.TipoProdutoID = @LinhaID ";

                        if (!(lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")) && fldKey == "a.CorpProdutos")
                        {
                            sql += "AND " + "(a.Produto + a.DescricaoProduto LIKE '" + textToSearch.ToString() + "'" + " OR a.ProdutoSubstituto LIKE '" + textToSearch.ToString() + "') ";
                        }
                        else if (!(lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")) && fldKey == "b.Conceitos")
                        {
                            sql += "AND " + "((b.Conceito + SPACE(1) + b.Modelo + SPACE(1) + b.PartNumber + SPACE(1) + ISNULL(b.Serie, SPACE(0)) + SPACE(1) + ISNULL(b.Descricao, SPACE(0)) + " +
                                    "SPACE(1) + ISNULL(b.DescricaoComplementar, SPACE(0)) LIKE '" + lstPre_TextBusca + "') OR (CONVERT(VARCHAR(10), b.ConceitoID) = '" + lstPre_TextBusca.Replace("%", "") + "')) ";
                        }

                        sql += "SET @LinesNumber = @@ROWCOUNT " +
                                "SET @CurrLine = 1 " +

                                "WHILE (@CurrLine <= @LinesNumber) " +
                                "BEGIN " +
                                    "SELECT @ConceitoID = c.ConceitoID, @PartNumber = a.PartNumber " +
                                        "FROM @TempTable a " +
                                            "INNER JOIN CorpProdutos b WITH (NOLOCK) ON b.Produto = a.PartNumber " +
                                            "INNER JOIN Conceitos c WITH (NOLOCK) ON c.PartNumber = b.Produto " +
                                            "INNER JOIN RelacoesPesCon d WITH (NOLOCK) ON d.ObjetoID = c.ConceitoID " +
                                        "WHERE a.OrigemID = d.OrigemID " +
                                            "AND d.SujeitoID = dbo.fn_Cotador_EmpresaCompativel(b.TipoProdutoID, b.OrigemID, b.MarcaID) " +
                                            "AND c.EstadoID NOT IN (1, 3, 4, 5, 11) AND d.EstadoID NOT IN (1, 3, 4, 5, 11) AND a.OrigemID = d.OrigemID AND a.IDTMP = @CurrLine " +
                                        "ORDER BY d.EstadoID ASC " +

                                    "UPDATE @TempTable SET ProdutoID = @ConceitoID WHERE IDTMP = @CurrLine " +

                                    "SET @CurrLine = @CurrLine + 1 " +
                                    "SELECT @ConceitoID = NULL, @PartNumber = NULL, @Quant = NULL " +
                                "END " +

                                "SET @CurrLine = 1 " +

                            "SET @Entrega = 'At� 10 dias �teis' " +

                            //Calcula PrecoFobDesc 
                            "UPDATE @TempTable SET PrecoFobDesc = dbo.fn_Cotador_CustoReposicao(EmpresaID, CorpProdutoID, Desconto, 0, NULL)" +

                            //Calcula Estoque dispon�vel
                            "UPDATE @TempTable SET Estoque = dbo.fn_Produto_Estoque(EmpresaID,ProdutoID,-356,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(EmpresaID,ProdutoID,@PesID,NULL),NULL,NULL) " +

                            "WHILE(@CurrLine <= @LinesNumber) " +
                            "BEGIN " +
                                "SELECT @IPI = NULL, @ICMS = NULL, @ICMSST = NULL, @DIFAL = NULL, @ISS = NULL, @TipoPagamentoST = NULL, @ValorFrete = NULL, @Preco = NULL, @Produtos = NULL, " +
                                        "@FIE = NULL, @CV = NULL, @DP = NULL, @CFOPID = NULL " +

                                "SELECT @ProdutoID = ProdutoID, @NCM = NCM, @TipoProdutoID = TipoProdutoID, @OrigemID = OrigemID, " +
                                        "@MoedaID = MoedaID, @PrecoFobDesc = PrecoFobDesc, @EmpresaID = EmpresaID, @CorpProdutoID = CorpProdutoID, " +
                                        "@Desconto =  Desconto " +
                                    "FROM @TempTable " +
                                    "WHERE IDTMP = @CurrLine " +

                                "IF(@TipoProdutoID = 1134) " +
                                "BEGIN " +
                                    " UPDATE @TempTable SET MoedaID = 647 WHERE IDTMP = @CurrLine " +
                                "END " +

                                "SELECT @Entrega = dbo.fn_Produto_PrevisaoEntregaAtual(NULL,NULL,CorpProdutoID,NULL,NULL,NULL,NULL) FROM @TempTable  WHERE IDTMP = @CurrLine " +

                                "IF(@Entrega IS NULL OR @Entrega = 0) " +
                                "BEGIN " +
                                    "SET @Entrega = '' " +
                                "END " +
                                "ELSE " +
                                "BEGIN " +
                                    "SET @Entrega = 'At� ' + @Entrega + ' dias �teis' " +
                                "END " +

                                "EXEC sp_Preco_Corporativo @ProdutoID, @NCM, @MarcaID, @OrigemID, @MoedaID, @Quant, @ParID, @PesID, @FinalID, 0 /*Frete*/, @PrecoFobDesc, NULL, @FinanID, " +
                                                            "@EmpresaID, @TipoProdutoID, 1 , @IPI OUTPUT, @ICMS OUTPUT, @ICMSST OUTPUT, @DIFAL OUTPUT, @ISS OUTPUT, @TipoPagamentoST OUTPUT, @ValorFrete OUTPUT, " +
                                                            "@Preco OUTPUT, @Produtos OUTPUT, @FIE OUTPUT, @CV OUTPUT, @DP OUTPUT, @CFOPID OUTPUT " +

                                "SET @FIS = dbo.fn_DivideZero(@Preco, dbo.fn_Cotador_CustoReposicao(@EmpresaID, @CorpProdutoID, @Desconto, 0, @MoedaID)) " +

                                "UPDATE @TempTable " +
                                    "SET PrecoUnitario = @Preco, ValorFrete = @ValorFrete, IPI = @IPI,  ICMS = @ICMS, ICMSST = @ICMSST,  ISS = @ISS, FIE = @FIE, TipoPagamentoST = @TipoPagamentoST, " +
                                                        "CFOPID = @CFOPID, FIS = @FIS, Entrega = @Entrega " +
                                    "WHERE IDTMP = @CurrLine " +

                                "SET @CurrLine  += 1 " +
                            "END ";

                        sql2 = "SELECT '' AS LineNumber, a.ProdutoID AS ConceitoID, '[' + a.PartNumber + ']' + SPACE(1) + a.Produto AS Produto, a.ProdutoSubstituto, a.OK, " +
                                    "dbo.fn_Pessoa_Fantasia(a.EmpresaID, 0) AS Empresa, a.Origem, a.PrecoUnitario AS Preco, CONVERT(INT,0) AS QuantidadeComprar, NULL AS MC, a.Desconto, a.NCM, " +
                                    "(CASE TipoPagamentoST WHEN 1 THEN 'GARE' WHEN 2 THEN 'GNRE' ELSE NULL END) AS Guia, a.ICMSST, '' AS Vigencia, a.PrecoFobDesc, a.PrecoFob, FIS, FIE, " +
                                    "STR(Estoque,6,0) AS Disponivel, Entrega AS PrevisaoEntrega, a.TipoProdutoID, TipoProduto LinhaProduto, a.CFOPID AS CFOP, a.ProdutoVerTambem," +
                                    "dbo.fn_Cotador_Inconsistencias(a.PartNumber,0,a.PrecoUnitario,NULL,NULL,a.Desconto, NULL, a.CorpProdutoID, EmpresaID, NULL, 1, 0) AS 'Inconsistencia', " +
                                    "EmpresaID, NULL AS PrecoDigitado, NULL AS MargemDigitada, a.ICMSST AS ICMSST_Unitario, a.CorpProdutoID, a.PartNumber, a.MoedaID, b.SimboloMoeda AS Moeda, a.OrigemID AS OrigemID, " +
                                    "SPACE(0) AS fldError " +
                                "FROM @TempTable a " +
                                    "LEFT JOIN Conceitos b WITH(NOLOCK) ON b.ConceitoID = a.MoedaID ";

                        lstPre_FldKey2 = fldKey;

                        if (int.Parse(lstPre_Estoque) == 1)
                        {
                            sql2 += "WHERE  a.Estoque > 0 ";

                            if (!(lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")) && fldKey == "b.Conceitos")
                                sql2 += "AND a.ProdutoID IS NOT NULL ";
                        }
                        else if (!(lstPre_TextBusca.Equals("") || lstPre_TextBusca.Equals("%%")) && fldKey == "b.Conceitos")
                        {
                            sql2 += "WHERE  a.ProdutoID IS NOT NULL ";
                        }

                        sql += sql2;
                    }

                    parametrosSQL[0] = new ProcedureParameters("@sql", SqlDbType.NVarChar, sql, ParameterDirection.Input);

                break;

                case 5310: // modcomercial/submodimportacao/importacao
                    int NumeroPaginas = (pageSize.intValue() * pageNumber.intValue());

                    sql = "INSERT INTO @TempTable (IDFromPesq) " +
                        "SELECT TOP " + NumeroPaginas +
                            " a.ImportacaoID " +
                        "FROM Importacao a WITH(NOLOCK) " +
                        "WHERE " + sAccessAllowed +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " " +
                            "AND a.EmpresaID=" + (empresaId) + " " +
                        "ORDER BY " + fldKey + " " + order;

                    sql2 = "SELECT TOP " + pageSize.intValue() + " " +
                                "a.ImportacaoID, " +
                                "b.RecursoAbreviado AS Estado, " +
                                "dbo.fn_Importacao_SituacaoDescricao(a.ImportacaoID) AS Situacao, " +
                                "a.ProcessoImportacao as ProcessoImportacao, " +
                                "dbo.fn_Importacao_Marcas(a.ImportacaoID) AS Marcas, " +
                                "(SELECT COUNT(1) " +
	                                "FROM Invoices aa WITH(NOLOCK) " +
	                                "WHERE (aa.ImportacaoID = a.ImportacaoID)) AS Invoices, " +
                                "(SELECT COUNT(1) " +
	                                "FROM Invoices aa WITH(NOLOCK) " +
		                                "INNER JOIN Invoices_Itens bb WITH(NOLOCK) ON (bb.InvoiceID = aa.InvoiceID) " +
	                                "WHERE (aa.ImportacaoID = a.ImportacaoID)) AS Itens, " +
                                "(SELECT SUM(aa.ValorTotalInvoice) " +
	                                "FROM Invoices aa WITH(NOLOCK) " +
	                                "WHERE (aa.ImportacaoID = a.ImportacaoID)) AS TotalFOB, " +
                                "a.dtPrevisaoEntrega, " +
                                "a.AtrasoEntrega, " +
                                "a.DI, " +
                                "a.dtRegistro, " +
                                //"dbo.fn_Importacao_Pessoa(a.ImportacaoID) AS Exportador, " +
                                "dbo.fn_Importacao_NotasFiscais(a.ImportacaoID) AS NFs," +
                                "a.Observacao, " +
                                "a.Observacoes, " +
                                "CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                            "FROM " +
                                "Importacao a WITH(NOLOCK) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) " +
                                "INNER JOIN @TempTable z ON (z.IDFromPesq = a.ImportacaoID) " +
                            "WHERE " + fldKey + sOperator + textToSearch + " " +
                                "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 5320: // modcomercial/submodimportacao/siscoserv

                    sql = "INSERT INTO @TempTable (IDFromPesq) " +
                        "SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) +
                            " a.SiscoservID " +
                        "FROM Siscoserv a WITH(NOLOCK) " +
                        "WHERE " + sAccessAllowed +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " " +
                            "AND a.EmpresaID=" + (empresaId) + " " +
                        "ORDER BY " + fldKey + " " + order;

                    sql2 = "SELECT TOP " + pageSize + " " +
                                "a.SiscoServID, " +
                                "b.RecursoAbreviado AS Estado, " +
                                "a.DtInicio as V_DtInicio, " +
                                "a.DtFim as V_DtFim, " +
                                "CONVERT (VARCHAR, (FLOOR(dbo.fn_SiscoServ_Totais(a.SiscoservID, NULL, NULL, NULL, NULL, 4)) - " +
                                "FLOOR(dbo.fn_SiscoServ_Totais(a.SiscoservID, NULL, NULL, NULL, NULL, 6)))) + '/' +  " +
                                "CONVERT(VARCHAR, FLOOR(dbo.fn_SiscoServ_Totais(a.SiscoservID, NULL, NULL, NULL, NULL, 4))) AS RAS, " +
                                "CONVERT(VARCHAR, FLOOR(dbo.fn_SiscoServ_Totais(a.SiscoservID, NULL, NULL, NULL, NULL, 5)) -  " +
                                "FLOOR(dbo.fn_SiscoServ_Totais(a.SiscoservID, NULL, NULL, NULL, NULL, 7))) + '/' +  " +
                                "CONVERT(VARCHAR, FLOOR(dbo.fn_SiscoServ_Totais(a.SiscoservID, NULL, NULL, NULL, NULL, 5))) AS RP, " +
                                "CONVERT(VARCHAR, FLOOR(dbo.fn_SiscoServ_Totais(a.SiscoservID, NULL, NULL, NULL, NULL, 6))) AS RASProblema, " +
                                "CONVERT(VARCHAR, FLOOR(dbo.fn_SiscoServ_Totais(a.SiscoservID, NULL, NULL, NULL, NULL, 7))) AS RPProblema, " +
                                "a.Observacao, " +
                                "a.EstadoID, " +
                                "CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                            "FROM " +
                                "Siscoserv a WITH(NOLOCK) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) " +
                                "INNER JOIN @TempTable z ON (z.IDFromPesq = a.SiscoservID) " +
                            "WHERE " + fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.SiscoservID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";
                    break;

                case 5330: // modcomercial/submodimportacao/Invoice

                    sql = "INSERT INTO @TempTable (IDFromPesq) " +
                        "SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) +
                            " a.InvoiceID " +
                        "FROM Invoices a WITH(NOLOCK) " +
                        "WHERE " + sAccessAllowed +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " " +
                            "AND a.EmpresaID=" + (empresaId) + " " +
                        "ORDER BY " + fldKey + " " + order;

                    sql2 = "SELECT TOP " + pageSize + " " +
                                "a.InvoiceID, " +
                                "b.RecursoAbreviado AS Estado, " +
                                "a.Invoice, " +
                                "a.dtEmissao, " +
                                "d.Fantasia, " +
                                "a.ItensInvoice, " + 
                                "a.ValorTotalInvoice, " +
                                "CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                            "FROM " +
                                "Invoices a WITH(NOLOCK) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) " +
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.EmpresaID) " +
                                "INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.ExportadorID) " +
                                "INNER JOIN @TempTable z ON (z.IDFromPesq = a.InvoiceID) " +
                            "WHERE " + fldKey + sOperator + textToSearch + " " +
                                    "AND z.IDTMP > " + startID +
                            "ORDER BY z.IDTMP";

                    break;


                case 5420: // modcomercial/submodtransporte/faturatransporte

                    if (fldKey == "a.dtEmissao" && textToSearch == "'0'")
                        textToSearch = "CONVERT(INT," + textToSearch + ")";

                    if (fldKey == "a.dtVencimento" && textToSearch == "'0'")
                        textToSearch = "CONVERT(INT," + textToSearch + ")";

                    sql = "INSERT INTO @TempTable (IDFromPesq) " +
                        "SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) +
                            " a.FaturaID " +
                        "FROM FaturasTransporte a WITH(NOLOCK) " +
                        "INNER JOIN dbo.Pessoas c WITH ( NOLOCK ) ON ( a.TransportadoraID = c.PessoaID ) " +
                        "WHERE " + sAccessAllowed +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " " +
                            "AND a.EmpresaID=" + (empresaId) + " " +
                        "ORDER BY " + fldKey + " " + order;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.DocumentoID, a.EstadoID ) = 1)";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                                "a.FaturaID, " +
                                "b.RecursoAbreviado AS Estado, " +
                                "a.Numero, " +
                                "a.dtEmissao as Emissao, " +
                                "a.dtVencimento as Vencimento, " +
                                "a.ValorTotal, " +
                                "c.Fantasia as Transportadora, " +
                                "CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                            "FROM " +
                                "FaturasTransporte a WITH(NOLOCK) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) " +
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.TransportadoraID) " +
                                "INNER JOIN @TempTable z ON (Z.IDFromPesq = a.FaturaID) " +
                            "WHERE " + fldKey + sOperator + textToSearch + " " +
                                "AND z.IDTMP > " + startID +
                            "ORDER BY z.IDTMP";

                    break;
                case 6110:    //  modindustrial/submodproducao/ordensproducao

                    string sOperatorOP_NF;
                    sOperatorOP_NF = " LEFT OUTER JOIN ";

                    if ((returnFldType("OrdensProducao", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }
                    else if ((returnFldType("Pedidos", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    if (fldKey == "a.NumeroSerie")
                    {
                        fldKey = "dbo.fn_Pedido_NumeroSeriePesquisa(-1 * a.OrdemProducaoID, " + textToSearch + ")";
                        sOperator = "=";
                        textToSearch = "1";
                    }
                    else if (fldKey == "d.NotaFiscal")
                    {
                        sOperatorOP_NF = " INNER JOIN ";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + (pageSize.intValue() * pageNumber.intValue()) + " a.OrdemProducaoID " +
                              "FROM OrdensProducao a WITH(NOLOCK) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                                "INNER JOIN Pedidos c WITH(NOLOCK) ON (a.PedidoID = c.PedidoID) " +
                                sOperatorOP_NF + " NotasFiscais d WITH(NOLOCK) ON (c.NotaFiscalID = d.NotaFiscalID) " +
                                "INNER JOIN Pessoas f WITH(NOLOCK) ON (c.PessoaID=f.PessoaID) " +
                              "WHERE " + sAccessAllowed + " " +
                              "c.EmpresaID = " + (empresaId) + " AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.OrdemProducaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.OrdemProducaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                            "a.OrdemProducaoID, b.RecursoAbreviado as Estado, a.Suspenso, a.ProcessoIniciado, " +
                            "dbo.fn_OrdemProducao_Sincronizada(a.OrdemProducaoID) AS Sincronizada, a.dtOP, a.PedidoID, e.RecursoAbreviado AS EstadoPedido, f.Fantasia AS Parceiro, d.NotaFiscal AS Nota, " +
                            "c.dtPrevisaoEntrega, NULL as TempoUtilProducao, NULL AS dtPrevisaoSistema, " +
                            "g.Fantasia AS Colaborador, " +
                            "(case" +
                               "WHEN (a.EstadoID <> 124 AND c.dtPrevisaoEntrega < GETDATE()) { 1 " +
                               "WHEN (a.EstadoID <> 124 AND dbo.fn_Data_Zero(c.dtPrevisaoEntrega) = dbo.fn_Data_Zero(GETDATE())) { 2 " +
                               "ELSE 0 END) AS Vencida " +
                            "FROM OrdensProducao a WITH(NOLOCK) " +
                               "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                               "INNER JOIN Pedidos c WITH(NOLOCK) ON (a.PedidoID = c.PedidoID) " +
                               sOperatorOP_NF + " NotasFiscais d WITH(NOLOCK) ON (c.NotaFiscalID = d.NotaFiscalID) " +
                               "INNER JOIN Recursos e WITH(NOLOCK) ON (c.EstadoID = e.RecursoID) " +
                               "INNER JOIN Pessoas f WITH(NOLOCK) ON (c.PessoaID = f.PessoaID) " +
                               "INNER JOIN Pessoas g WITH(NOLOCK) ON (a.ProprietarioID=g.PessoaID) " +
                               "INNER JOIN @TempTable z ON (z.IDFromPesq=a.OrdemProducaoID), CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                            "WHERE " + sAccessAllowed +
                            "c.EmpresaID = " + (empresaId) + " AND " +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " " +
                            "AND z.IDTMP > " + startID +
                            " ORDER BY z.IDTMP";

                    break;

                case 7110:    //  modqualidade/submodiso/documentos      

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.DocumentoID " +
                              "FROM Documentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK) " +
                              "WHERE a.EstadoID = b.RecursoID AND a.ItemNormaID = c.ItemID AND a.TipoDocumentoID = d.ItemID AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.DocumentoID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.DocumentoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.DocumentoID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.DocumentoID, b.RecursoAbreviado as Estado, a.Documento, a.DocumentoAbreviado, d.ItemAbreviado AS TipoDocumento, c.ItemAbreviado AS Norma, " +
                             "REPLACE(a.Versao, CHAR(44), char(46)) AS Versao, a.dtEmissao, a.Observacao, e.Fantasia, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Documentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK), Pessoas e WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND a.ItemNormaID = c.ItemID AND a.TipoDocumentoID = d.ItemID AND a.ProprietarioID = e.PessoaID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.DocumentoID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 7120:    //  modqualidade/submodiso/riq      

                    if ((returnFldType("RIQ", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RIQID " +
                              "FROM RIQ a WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK) " +
                              "WHERE a.EmpresaID = " + (empresaId) + " AND " +
                              "a.IndicadorID=c.ItemID AND a.ItemNormaID=d.ItemID AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.RIQID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RIQID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.RIQID, b.RecursoAbreviado as Estado, c.ItemMasculino AS Indicador, c.ItemAbreviado AS IndicadorAbrev, " +
                             "d.ItemAbreviado AS Norma, a.Unidade AS Unidade, " +
                             "(case a.Frequencia WHEN 1 THEN 'Mensal'  " +
                                                "WHEN 2 THEN 'Bimestral'  " +
                                                "WHEN 3 THEN 'Trimestral'  " +
                                                "WHEN 6 THEN 'Semestral'  " +
                                                "WHEN 12 THEN 'Anual'  " +
                                                "ELSE SPACE(0) END) AS Frequencia, a.dtInicioCalculo, a.dtMeta, a.Meta, a.ResultadoMinimo, a.ResultadoMaximo, " +
                                                "e.Fantasia AS Proprietario, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM RIQ a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK), Pessoas e WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND a.IndicadorID=c.ItemID AND a.ItemNormaID=d.ItemID AND " +
                             "a.ProprietarioID=e.PessoaID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RIQID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 7130:    //  modqualidade/submodiso/rad      

                    if ((returnFldType("RAD", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RADID " +
                              "FROM RAD a WITH(NOLOCK) " +
                              "WHERE a.EmpresaID = " + (empresaId) + " AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.RADID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RADID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.RADID, b.RecursoAbreviado as Estado, a.dtAnalise, a.Local, a.dtVencimento, " +
                             "a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM RAD a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RADID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 7140:    //  modqualidade/submodiso/rrc

                    if ((returnFldType("RRC", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RRCID " +
                              "FROM RRC a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                              "WHERE a.TipoRealimentacaoID=b.ItemID AND a.ClienteID=c.PessoaID AND a.EmpresaID = " + (empresaId) + " AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.RRCID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RRCID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.RRCID, e.RecursoAbreviado as Estado, a.Realimentacao, b.ItemMasculino, a.dtEmissao, a.dtVencimento, " +
                             "a.Critico, a.Observacao, c.Fantasia, d.Fantasia, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM RRC a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Recursos e WITH(NOLOCK), @TempTable z " +
                             "WHERE a.TipoRealimentacaoID=b.ItemID AND a.ClienteID=c.PessoaID AND a.ProprietarioID=d.PessoaID AND " +
                             "a.EstadoID=e.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RRCID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 7150:    //  modqualidade/submodiso/psc

                    if ((returnFldType("PSC", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.PSCID " +
                              "FROM PSC a WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                              "WHERE a.ClienteID=c.PessoaID AND a.EmpresaID = " + (empresaId) + " AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.PSCID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.PSCID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.PSCID, b.RecursoAbreviado as Estado, a.SituacaoCliente, a.dtEmissao, " +
                             "a.Critico, a.Observacao, c.Fantasia AS Cliente, dbo.fn_PSC_GrauSatisfacao(a.PSCID) AS Satisfacao, d.Fantasia AS Proprietario, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM PSC a WITH(NOLOCK), Recursos b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID=b.RecursoID AND a.ClienteID=c.PessoaID AND a.ProprietarioID=d.PessoaID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.PSCID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 7160:    //  modqualidade/submodiso/rai      

                    if ((returnFldType("RAI", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RAIID " +
                              "FROM RAI a WITH(NOLOCK), Recursos c WITH(NOLOCK) " +
                              "WHERE a.EstadoID=c.RecursoID AND a.EmpresaID = " + (empresaId) + " AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.RAIID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RAIID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.RAIID, c.RecursoAbreviado as Estado, a.dtAuditoria, a.dtVencimento, " +
                             "dbo.fn_RAI_GrauConformidade(a.RAIID) AS GrauConformidade, a.SistemaOK, a.Observacao, d.Fantasia, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM RAI a WITH(NOLOCK), Recursos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = c.RecursoID AND a.ProprietarioID = d.PessoaID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RAIID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 7170:    //  modqualidade/submodiso/racp      

                    if ((returnFldType("RACP", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RACPID " +
                              "FROM RACP a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK) " +
                              "WHERE a.TipoAcaoID = b.ItemID AND a.EstadoID=c.RecursoID AND a.EmpresaID = " + (empresaId) + " AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.RACPID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RACPID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.RACPID, c.RecursoAbreviado as Estado, a.NaoConformidade, b.ItemMasculino, a.dtEmissao, a.dtVencimento, " +
                             "a.Critico, a.Observacao, d.Fantasia, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM RACP a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = c.RecursoID AND a.TipoAcaoID = b.ItemID AND a.ProprietarioID = d.PessoaID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.RACPID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 7180:    //  modqualidade/submodiso/qaf

                    if ((returnFldType("QAF", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.QAFID " +
                              "FROM QAF a WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                              "WHERE a.FornecedorID=c.PessoaID AND a.EmpresaID = " + (empresaId) + " AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.QAFID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.QAFID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.QAFID, b.RecursoAbreviado as Estado, a.dtEmissao, a.dtVencimento, " +
                             "c.Fantasia AS Fornecedor, a.Observacao, d.Fantasia AS Proprietario, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM QAF a WITH(NOLOCK), Recursos b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID=b.RecursoID AND a.FornecedorID=c.PessoaID AND a.ProprietarioID=d.PessoaID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.QAFID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 8110:    //  modservicos/subsuporte/assistenciatecnica      
                    string asstec_Priorizar, asstec_OrdemPriorizar;

                    asstec_Priorizar = "0";
                    if (Request.QueryString["asstec_Priorizar"] != null)
                        asstec_Priorizar = Request.QueryString["asstec_Priorizar"];

                    asstec_OrdemPriorizar = "";

                    sStrTop = " TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " ";

                    if ((returnFldType("Asstec", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    if ((fldKey == "a.dtAsstec") && (textToSearch == "0"))
                    {
                        textToSearch = "";
                    }

                    sDefinicaoParametros = "@TTS NVARCHAR(128), @EmpID INT, @StartID INT, @PS INT";

                    //Determinar tamanho dos parametros
                    parametrosSQL = new ProcedureParameters[6];
                    parametrosSQL[1] = new ProcedureParameters("@DefinicaoParametros", SqlDbType.NVarChar, sDefinicaoParametros, ParameterDirection.Input, 4000);
                    parametrosSQL[2] = new ProcedureParameters("@TTS", SqlDbType.NVarChar, textToSearch.ToString(), ParameterDirection.Input, 128);
                    parametrosSQL[3] = new ProcedureParameters("@EmpID", SqlDbType.Int, empresaId.intValue(), ParameterDirection.Input);
                    parametrosSQL[4] = new ProcedureParameters("@StartID", SqlDbType.Int, startID, ParameterDirection.Input);
                    parametrosSQL[5] = new ProcedureParameters("@PS", SqlDbType.Int, pageSize.intValue(), ParameterDirection.Input);

                    sql = " SET NOCOUNT ON DECLARE @TempTable TABLE (IDTMP INT IDENTITY,IDFromPesq INT NOT NULL) ";

                    // NOTA:
                    //  e.Fantasia -> Cliente
                    //  m.Fantasia -> Fornecedor

                    // Tabela temporaria para pesquisa por cliente
                    if (fldKey == "e.Fantasia")
                    {
                        sql += "INSERT INTO @TempTable (IDFromPesq) SELECT " + sStrTop + " a.AsstecID " +
                              "FROM Asstec a WITH(NOLOCK) " +
                                    "INNER JOIN Recursos b WITH(NOLOCK) ON ( a.EstadoID = b.RecursoID ) " +
                                    "INNER JOIN Conceitos c WITH(NOLOCK) ON ( a.ProdutoID=c.ConceitoID ) " +
                                    "INNER JOIN Asstec_Pessoas d WITH(NOLOCK) ON ( d.AsstecID = a.AsstecID ) " +
                                    "INNER JOIN Pessoas e WITH(NOLOCK) ON ( d.PessoaID = e.PessoaID ) " +
                              "WHERE " + sAccessAllowed + " a.EmpresaID=@EmpID AND " +
                                     "d.EhCliente = 1 AND " +
                                     fldKey + sOperator + " @TTS " +
                                     condition + sFiltroContexto + sFiltro;
                    }
                    // Tabela temporaria para pesquisa por fornecedor
                    else if (fldKey == "m.Fantasia")
                    {
                        sql += "INSERT INTO @TempTable (IDFromPesq) SELECT " + sStrTop + " a.AsstecID " +
                              "FROM Asstec a WITH(NOLOCK) " +
                                    "INNER JOIN Recursos b WITH(NOLOCK) ON ( a.EstadoID = b.RecursoID ) " +
                                    "INNER JOIN Conceitos c WITH(NOLOCK) ON ( a.ProdutoID=c.ConceitoID ) " +
                                    "INNER JOIN Asstec_Pessoas d WITH(NOLOCK) ON ( d.AsstecID = a.AsstecID ) " +
                                    "INNER JOIN Pessoas m WITH(NOLOCK) ON ( d.PessoaID = m.PessoaID ) " +
                              "WHERE " + sAccessAllowed + " a.EmpresaID=@EmpID AND " +
                                     "d.EhCliente = 0 AND " +
                                     fldKey + sOperator + " @TTS " +
                                     condition + sFiltroContexto + sFiltro;
                    }
                    // Tabela temporaria para demais pesquisas
                    else
                    {
                        sql += "INSERT INTO @TempTable (IDFromPesq) SELECT " + sStrTop + " a.AsstecID " +
                                  "FROM Asstec a WITH(NOLOCK) " +
                                    "INNER JOIN Recursos b WITH(NOLOCK) ON ( a.EstadoID = b.RecursoID ) " +
                                    "INNER JOIN Conceitos c WITH(NOLOCK) ON ( a.ProdutoID=c.ConceitoID ) " +
                                  "WHERE " + sAccessAllowed + " a.EmpresaID=@EmpID AND " +
                                  fldKey + sOperator + " @TTS " +
                                  condition + sFiltroContexto + sFiltro;
                    }

                    if (asstec_Priorizar == "1")
                    {

                        if ((filtroId.intValue() == 41000) || (filtroId.intValue() == 41058) || (filtroId.intValue() == 41081))
                            asstec_OrdemPriorizar = "a.PrioridadeGeral DESC,";
                        else if ((filtroId.intValue() == 41087) || (filtroId.intValue() == 41088))
                            asstec_OrdemPriorizar = "a.PrioridadeFornecedor DESC,";
                        else if (filtroId.intValue() == 41205)
                            asstec_OrdemPriorizar = "a.PrioridadeCliente DESC,";
                    }

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.AsstecID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.AsstecID")
                    {
                        sql = sql + " ORDER BY " + asstec_OrdemPriorizar + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + asstec_OrdemPriorizar + fldKey + " " + order + " ,a.AsstecID";
                    }

                    // Dados para pesquisa por cliente             
                    if (fldKey == "e.Fantasia")
                    {
                        sql2 = " SELECT TOP (@PS) " +
                                 " a.AsstecID, b.RecursoAbreviado as Estado, " +
                                   "a.Proprio as P,  a.Defeito,  a.Cliente as C, a.Fornecedor as F,  " +
                                 "(SELECT TOP 1 Clientes.Fantasia " +
                                 "FROM Asstec_Pessoas AsstecClientes WITH(NOLOCK), Pessoas Clientes WITH(NOLOCK) " +
                                 "WHERE (a.AsstecID = AsstecClientes.AsstecID AND AsstecClientes.EhCliente = 1 AND AsstecClientes.PessoaID = Clientes.PessoaID) " +
                                 "ORDER BY AsstecClientes.AstPessoaID DESC) AS Cliente, " +
                                 "(SELECT TOP 1 Fornecedores.Fantasia " +
                                 "FROM Asstec_Pessoas AsstecFornecedores WITH(NOLOCK), Pessoas Fornecedores WITH(NOLOCK) " +
                                 "WHERE (a.AsstecID = AsstecFornecedores.AsstecID AND AsstecFornecedores.EhCliente = 0 AND AsstecFornecedores.PessoaID = Fornecedores.PessoaID) " +
                                 "ORDER BY AsstecFornecedores.AstPessoaID DESC) AS Fornecedor, " +
                                 "a.dtAsstec, c.Conceito, c.ConceitoID, a.NumeroSerie, " +
                                 "a.OrdemProducaoID, a.CaseNumber, a.RMA, dbo.fn_Asstec_Cor (a.PrioridadeCliente)as CorCliente, dbo.fn_Asstec_Cor(a.PrioridadeFornecedor)as CorFornecedor, dbo.fn_Asstec_Cor(a.PrioridadeGeral)as CorGeral, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                 "FROM Asstec a WITH(NOLOCK) " +
                                       "INNER JOIN Recursos b WITH(NOLOCK) ON ( a.EstadoID=b.RecursoID ) " +
                                       "INNER JOIN Conceitos c WITH(NOLOCK) ON ( a.ProdutoID=c.ConceitoID ) " +
                                       "INNER JOIN Asstec_Pessoas d WITH(NOLOCK) ON ( d.AsstecID=a.AsstecID ) " +
                                       "INNER JOIN Pessoas e WITH(NOLOCK) ON ( d.PessoaID=e.PessoaID ) " +
                                       "INNER JOIN @TempTable z ON ( z.IDFromPesq=a.AsstecID ) " +
                                 "WHERE a.EmpresaID=@EmpID AND " +
                                        "z.IDTMP > @StartID AND " +
                                        "d.EhCliente=1 AND " +
                                        fldKey + sOperator + " @TTS " +
                                        condition + sFiltroContexto + sFiltro +
                                 " ORDER BY z.IDTMP";
                    }
                    // Dados para pesquisa por fornecedor
                    else if (fldKey == "m.Fantasia")
                    {
                        sql2 = " SELECT TOP (@PS) " +
                                 " a.AsstecID, b.RecursoAbreviado as Estado, " +
                                   "a.Proprio as P,  a.Defeito,  a.Cliente as C, a.Fornecedor as F,  " +
                                 "(SELECT TOP 1 Clientes.Fantasia " +
                                 "FROM Asstec_Pessoas AsstecClientes WITH(NOLOCK), Pessoas Clientes WITH(NOLOCK) " +
                                 "WHERE (a.AsstecID = AsstecClientes.AsstecID AND AsstecClientes.EhCliente = 1 AND AsstecClientes.PessoaID = Clientes.PessoaID) " +
                                 "ORDER BY AsstecClientes.AstPessoaID DESC) AS Cliente, " +
                                 "(SELECT TOP 1 Fornecedores.Fantasia " +
                                 "FROM Asstec_Pessoas AsstecFornecedores WITH(NOLOCK), Pessoas Fornecedores WITH(NOLOCK) " +
                                 "WHERE (a.AsstecID = AsstecFornecedores.AsstecID AND AsstecFornecedores.EhCliente = 0 AND AsstecFornecedores.PessoaID = Fornecedores.PessoaID) " +
                                 "ORDER BY AsstecFornecedores.AstPessoaID DESC) AS Fornecedor, " +
                                 "a.dtAsstec, c.Conceito, c.ConceitoID, a.NumeroSerie, " +
                                 "a.OrdemProducaoID, a.CaseNumber, a.RMA, dbo.fn_Asstec_Cor (a.PrioridadeCliente)as CorCliente, dbo.fn_Asstec_Cor(a.PrioridadeFornecedor)as CorFornecedor, dbo.fn_Asstec_Cor(a.PrioridadeGeral)as CorGeral, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                 "FROM Asstec a WITH(NOLOCK) " +
                                       "INNER JOIN Recursos b WITH(NOLOCK) ON ( a.EstadoID=b.RecursoID ) " +
                                       "INNER JOIN Conceitos c WITH(NOLOCK) ON ( a.ProdutoID=c.ConceitoID ) " +
                                       "INNER JOIN Asstec_Pessoas d WITH(NOLOCK) ON ( d.AsstecID=a.AsstecID ) " +
                                       "INNER JOIN Pessoas m WITH(NOLOCK) ON ( d.PessoaID=m.PessoaID ) " +
                                       "INNER JOIN @TempTable z ON ( z.IDFromPesq=d.AsstecID ) " +
                                 "WHERE a.EmpresaID=@EmpID AND " +
                                        "z.IDTMP > @StartID AND " +
                                        "d.EhCliente = 0 AND " +
                                        fldKey + sOperator + " @TTS " +
                                        condition + sFiltroContexto + sFiltro +
                                 " ORDER BY z.IDTMP";
                    }
                    // Dados para demais pesquisas
                    else
                    {
                        sql2 = " SELECT TOP (@PS) " +
                                 " a.AsstecID, b.RecursoAbreviado as Estado, " +
                                   "a.Proprio as P,  a.Defeito,  a.Cliente as C, a.Fornecedor as F,  " +
                                 "(SELECT TOP 1 Clientes.Fantasia " +
                                 "FROM Asstec_Pessoas AsstecClientes WITH(NOLOCK), Pessoas Clientes WITH(NOLOCK) " +
                                 "WHERE (a.AsstecID = AsstecClientes.AsstecID AND AsstecClientes.EhCliente = 1 AND AsstecClientes.PessoaID = Clientes.PessoaID) " +
                                 "ORDER BY AsstecClientes.AstPessoaID DESC) AS Cliente, " +
                                 "(SELECT TOP 1 Fornecedores.Fantasia " +
                                 "FROM Asstec_Pessoas AsstecFornecedores WITH(NOLOCK), Pessoas Fornecedores WITH(NOLOCK) " +
                                 "WHERE (a.AsstecID = AsstecFornecedores.AsstecID AND AsstecFornecedores.EhCliente = 0 AND AsstecFornecedores.PessoaID = Fornecedores.PessoaID) " +
                                 "ORDER BY AsstecFornecedores.AstPessoaID DESC) AS Fornecedor, " +
                                 "a.dtAsstec, c.Conceito, c.ConceitoID, a.NumeroSerie, " +
                                 "a.OrdemProducaoID, a.CaseNumber, a.RMA, dbo.fn_Asstec_Cor (a.PrioridadeCliente)as CorCliente, dbo.fn_Asstec_Cor(a.PrioridadeFornecedor)as CorFornecedor, dbo.fn_Asstec_Cor(a.PrioridadeGeral)as CorGeral, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                                 "FROM Asstec a WITH(NOLOCK) " +
                                        "INNER JOIN Recursos b WITH(NOLOCK) ON ( a.EstadoID=b.RecursoID ) " +
                                        "INNER JOIN Conceitos c WITH(NOLOCK) ON ( a.ProdutoID=c.ConceitoID ) " +
                                        "INNER JOIN @TempTable z ON ( z.IDFromPesq=a.AsstecID ) " +
                                 "WHERE a.EmpresaID=@EmpID AND " +
                                        "z.IDTMP > @StartID AND " +
                                         fldKey + sOperator + " @TTS " +
                                         condition + sFiltroContexto + sFiltro +
                                 " ORDER BY z.IDTMP";
                    }

                    sql += sql2;

                    parametrosSQL[0] = new ProcedureParameters("@sql", SqlDbType.NVarChar, sql, ParameterDirection.Input, 4000);
                    break;

                case 8120:    //  modservicos/subsuporte/glossario      

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.TermoID " +
                              "FROM Glossario a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.TermoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.TermoID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.TermoID, b.RecursoAbreviado as Estado, a.Termo, a.Significado, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Glossario a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.TermoID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 9110:    //  modfinanceiro/submodcontrolefinanceiro/financeiro      

                    string sOperatorSit = " LEFT OUTER JOIN ";

                    if (!a1.booleanValue() || !a2.booleanValue())
                    {
                        sFiltro = sFiltro + " AND (a.PedidoID IS NOT NULL OR a.HistoricoPadraoID = 1146) ";
                    }

                    if ((returnFldType("Financeiro", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    if (fldKey == "a.SaldoDevedor")
                    {
                        fldKey = "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) ";
                    }
                    else if (fldKey == "a.SaldoAtualizado")
                    {
                        fldKey = "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) ";
                    }
                    else if (fldKey == "g.ItemAbreviado")
                    {
                        sOperatorSit = " INNER JOIN ";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.FinanceiroID " +
                              "FROM Financeiro a WITH(NOLOCK) " +
                                sOperatorSit + " TiposAuxiliares_Itens g WITH(NOLOCK) ON (a.SituacaoCobrancaID = g.ItemID) " +
                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) " +
                              "WHERE " + sAccessAllowed + " a.EmpresaID=" + empresaId + " AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.FinanceiroID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.FinanceiroID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.FinanceiroID, c.RecursoAbreviado as Estado, g.ItemAbreviado AS Sit, a.PedidoID, a.Duplicata, b.Fantasia, " +
                             "d.ItemAbreviado, a.PrazoPagamento, a.dtVencimento, e.SimboloMoeda as Moeda, a.Valor, " +
                             "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, " +
                             "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, " +
                             "f.HistoricoPadrao, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError, " +
                             "dbo.fn_Financeiro_Cor(a.FinanceiroID," + userId + ",GETDATE(),1) AS CorObservacao " +
                             "FROM Financeiro a WITH(NOLOCK) " +
                                sOperatorSit + " TiposAuxiliares_Itens g WITH(NOLOCK) ON (a.SituacaoCobrancaID = g.ItemID) " +
                                "LEFT OUTER JOIN HistoricosPadrao f WITH(NOLOCK) ON (a.HistoricoPadraoID = f.HistoricoPadraoID) " +
                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) " +
                                "INNER JOIN Recursos c WITH(NOLOCK) ON (a.EstadoID = c.RecursoID) " +
                                "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.FormaPagamentoID = d.ItemID) " +
                                "INNER JOIN Conceitos e WITH(NOLOCK) ON (a.MoedaID = e.ConceitoID) " +
                                "INNER JOIN @TempTable z ON (z.IDFromPesq = a.FinanceiroID) " +
                             "WHERE a.EmpresaID=" + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 9130:    //  modfinanceiro/submodcontrolefinanceiro/valoresalocalizar

                    if ((returnFldType("ValoresLocalizar", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    if (fldKey == "a.SaldoFinanceiro")
                    {
                        fldKey = "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) ";
                    }
                    else if (fldKey == "a.SaldoOcorrencia")
                    {
                        fldKey = "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 2) ";
                    }

                    string sOperatorPessoa;

                    // Chave de pesquisa e Nota Fiscal
                    if (fldKey == "e.Fantasia")
                    {
                        sOperatorPessoa = " INNER JOIN ";
                    }
                    else
                    {
                        sOperatorPessoa = " LEFT OUTER JOIN ";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.ValorID " +
                              "FROM ValoresLocalizar a WITH(NOLOCK) " +
                              sOperatorPessoa + " Pessoas e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID) " +
                              "WHERE " + sAccessAllowed + " a.EmpresaID=" + empresaId + " AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.ValorID, a.EstadoID ) = 1)";
                    }


                    if (fldKey == "a.ValorID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.ValorID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.ValorID, b.RecursoAbreviado as Estado, f.ItemMasculino, c.ItemAbreviado as FormaPagamento, a.MotivoDevolucao, e.Fantasia, a.Emitente, a.dtEmissao, a.dtApropriacao, a.BancoAgencia, a.Conta, a.NumeroDocumento, d.SimboloMoeda as Moeda, a.Valor, " +
                             "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) AS SaldoFinanceiro, " +
                             "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 2) AS SaldoOcorrencia, " +
                             "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 8) AS SaldoConcilicao, a.Identificador, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS ContaBancaria, a.Observacao, a.Historico, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM ValoresLocalizar a WITH(NOLOCK) " +
                                sOperatorPessoa + " Pessoas e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                                "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.FormaPagamentoID = c.ItemID) " +
                                "INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID = d.ConceitoID) " +
                                "INNER JOIN @TempTable z ON (z.IDFromPesq = a.ValorID) " +
                                "INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (a.ProcessoID = f.ItemID) " +
                             "WHERE a.EmpresaID=" + empresaId + " AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 9140:    //  modfinanceiro/submodcontrolefinanceiro/financiamentospadrao      

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.FinanciamentoID " +
                              "FROM FinanciamentosPadrao a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.FinanciamentoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.FinanciamentoID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.FinanciamentoID, b.RecursoAbreviado as Estado, a.Financiamento, a.Ordem, a.NumeroParcelas, " +
                             "a.Prazo1, a.Incremento as Incr, dbo.fn_Financiamento_PMP(a.FinanciamentoID,NULL,NULL,NULL) as PMP, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM FinanciamentosPadrao a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.FinanciamentoID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 9150:    //  modfinanceiro/subcontrolefinanceiro/depositosbancarios

                    if ((returnFldType("DepositosBancarios", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.DepositoID " +
                              "FROM DepositosBancarios a WITH(NOLOCK) " +
                              "WHERE a.EmpresaID = " + empresaId + " AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.DepositoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.DepositoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.DepositoID, b.RecursoAbreviado as Estado, a.dtData, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS Conta, " +
                             "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 3) AS Quantidade, " +
                             "c.SimboloMoeda AS Moeda, " +
                             "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 6) AS ValorDinheiro, " +
                             "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 7) AS ValorCheques, " +
                             "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 8) AS Total, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM DepositosBancarios a WITH(NOLOCK), Recursos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EmpresaID = " + empresaId + " AND a.EstadoID=b.RecursoID AND a.MoedaID=c.ConceitoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.DepositoID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 9160:    //  modfinanceiro/submodcontrolefinanceiro/creditos   

                    if ((returnFldType("Creditos", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    if (!a1.booleanValue())
                    {
                        sFiltro = " AND ((b.ProprietarioID = " + userId.intValue().ToString()+ ") OR (b.AlternativoID = " + userId.intValue().ToString() + "))";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.CreditoID " +
                              "FROM Creditos a WITH(NOLOCK) " +
                                " INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ParceiroID) " +
                              "WHERE a.PaisID = dbo.fn_Pessoa_Localidade(" + empresaId + ", 1, NULL, NULL) AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.CreditoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.CreditoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.CreditoID AS [ID], c.RecursoAbreviado AS Estado, b.Fantasia AS Parceiro, f.CodigoLocalidade3 AS Pais, e.Fantasia AS Seguradora, " +
                             "a.dtSolicitacao, d.Fantasia AS Solicitante," +
                             "(CASE WHEN a.Prioridade = 1 THEN 'Sim' ELSE 'N�o' END) AS Pri, " +
                             " a.dtConcessao AS Concessao, a.dtFim, g.SimboloMoeda AS Moeda,  " +
                             "a.ValorSolicitado, a.PotencialCredito, a.ValorConcedido, a.Observacao, b.PessoaID, b.TipoPessoaID, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Creditos a WITH(NOLOCK) " +
                                 "INNER JOIN Pessoas b WITH(NOLOCK) ON(b.PessoaID = a.ParceiroID) " +
                                 "INNER JOIN Recursos c WITH(NOLOCK) ON(c.RecursoID = a.EstadoID) " +
                                 "INNER JOIN Pessoas d WITH(NOLOCK) ON(d.PessoaID = a.SolicitanteID) " +
                                 "LEFT JOIN Pessoas e WITH(NOLOCK) ON(e.PessoaID = a.SeguradoraID) " +
                                 "INNER JOIN Localidades f WITH(NOLOCK) ON (f.LocalidadeID = a.PaisID) " +
                                 "INNER JOIN Conceitos g WITH(NOLOCK) ON (g.ConceitoID = a.MoedaID) " +
                                 "INNER JOIN @TempTable z ON(z.IDFromPesq = a.CreditoID) " +
                             "WHERE " + fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.CreditoID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY a.Prioridade DESC, z.IDTMP";
                    break;

                case 9210:    //  modfinanceiro/subcobranca/cobranca      

                    if ((returnFldType("Cobranca", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.CobrancaID " +
                              "FROM Cobranca a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed + " a.EmpresaID=" + empresaId + " AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.CobrancaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.CobrancaID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.CobrancaID, b.RecursoAbreviado as Estado, c.Codigo, d.ItemMasculino, " +
                             "a.dtGravacao, a.Sequencial, a.Arquivo, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Cobranca a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesPessoas_Contas e WITH(NOLOCK), RelacoesPessoas f WITH(NOLOCK), Pessoas g WITH(NOLOCK), Bancos c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EmpresaID=" + empresaId + " AND a.EstadoID = b.RecursoID " +
                             "AND a.RelPesContaID = e.RelPesContaID " +
                             "AND e.RelacaoID = f.RelacaoID AND f.ObjetoID = g.PessoaID " +
                             "AND g.BancoID = c.BancoID AND a.TipoCobrancaID=d.ItemID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.CobrancaID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 9220:    //  modfinanceiro/subcobranca/bancos      

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.BancoID " +
                              "FROM Bancos a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.BancoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.BancoID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.BancoID, b.RecursoAbreviado as Estado, a.Banco, a.Codigo, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Bancos a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.BancoID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 9230:    //  modfinanceiro/subcobranca/cobrancasDiversas


                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.CobrancaDiversaID " +
                              "FROM CobrancaDiversas a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.CobrancaDiversaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.CobrancaDiversaID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.CobrancaDiversaID, c.RecursoAbreviado as Estado ,b.Fantasia as Prestador, a.dtGravacao, a.NomeArquivo, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             " FROM CobrancaDiversas a WITH(NOLOCK)"+
                             " INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.PrestadorID) " +
                             " INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = a.EstadoID) " +
                             " INNER JOIN @TempTable z ON (z.IDFromPesq = a.CobrancaDiversaID) " +                                                         
                             "WHERE " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    
                    break;

                case 10110:    //  modcontabil/submodcontabilidade/planocontas      

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.ContaID " +
                              "FROM PlanoContas a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.ContaID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.ContaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.ContaID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.ContaID, b.RecursoAbreviado as Estado, dbo.fn_Conta_Identada(a.ContaID, " + (idiomaId) + ", NULL, NULL) AS Conta, " +
                                "c.ItemMasculino AS Tipo, dbo.fn_Conta_Nivel(a.ContaID) AS Nivel, a.ContaMaeID, d.ItemMasculino AS UsoEspecial, " +
                                "e.ItemMasculino AS Detalhamento, a.Detalha, a.DetalhamentoObrigatorio, " +
                                "dbo.fn_Conta_AceitaLancamento(a.ContaID) AS AceitaLancamento, a.LancamentoManual, " +
                                "a.SaldoDevedor, a.SaldoCredor, a.UsoRestrito, a.USA, a.TemOrcamento, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM PlanoContas a WITH(NOLOCK) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.TipoContaID = c.ItemID) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.UsoEspecialID = d.ItemID) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " +
                                "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.TipoDetalhamentoID = e.ItemID) " +
                                "INNER JOIN @TempTable z ON (z.IDFromPesq=a.ContaID) " +
                             "WHERE " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 10120:    //  modcontabil/submodcontabilidade/historicospadrao      

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue()) * (pageNumber.intValue())) + " a.HistoricoPadraoID " +
                          "FROM HistoricosPadrao a WITH(NOLOCK) " +
                          "WHERE " + sAccessAllowed +
                          fldKey + sOperator + textToSearch + " " +
                          condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.HistoricoPadraoID")
                        sql += " ORDER BY " + fldKey + " " + order;
                    else
                        sql += " ORDER BY " + fldKey + " " + order + " ,a.HistoricoPadraoID";

                    sql2 = "SELECT TOP " + pageSize +
                            " a.HistoricoPadraoID, b.RecursoAbreviado as Estado, a.HistoricoPadrao, " +
                            "a.HistoricoComplementar, a.HistoricoAbreviado, c.ItemAbreviado AS TipoLancamento, a.UsoCotidiano, a.UsoSistema, a.EhRetorno, a.AnteciparLancamento, a.MultiplasEmpresas, " +
                            "(CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.Pedido = 1)))) AS Pedido, " +
                            "(CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.Financeiro = 1)))) AS Financeiro, " +
                            "dbo.fn_HistoricoPadrao_ConciliacaoBancaria(a.HistoricoPadraoID, NULL, 0) AS ConciliacaoBancaria, " +
                            "dbo.fn_HistoricoPadrao_ConciliacaoBancaria(a.HistoricoPadraoID, NULL, 1) AS GeraLancamento, a.Marketing, " +
                            "a.PesoContabilizacao, a.Grupo, a.RequerDocumentoFiscal, a.Observacao, SPACE(0) AS fldError " +
                            "FROM HistoricosPadrao a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                            "WHERE a.EstadoID = b.RecursoID AND a.TipoLancamentoID = c.ItemID AND " +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.HistoricoPadraoID " +
                            "AND z.IDTMP > " + startID +
                            " ORDER BY z.IDTMP";


                    break;

                case 10130:    //  modcontabil/submodcontabilidade/lancamentos      

                    if ((returnFldType("Lancamentos", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.LancamentoID " +
                              "FROM Lancamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), HistoricosPadrao d WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed + " a.EmpresaID=" + empresaId + " AND " +
                              "a.EstadoID = b.RecursoID AND a.TipoLancamentoID = c.ItemID AND a.HistoricoPadraoID = d.HistoricoPadraoID AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.LancamentoID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.LancamentoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.LancamentoID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                          " a.LancamentoID, b.RecursoAbreviado as Estado, a.dtLancamento, a.dtBalancete, " +
                         "dbo.fn_Lancamento_Conciliado(-a.LancamentoID) AS Conciliado, " +
                            "a.EhEstorno, c.ItemAbreviado, CONVERT(NUMERIC(11), dbo.fn_Lancamento_Totais(a.LancamentoID, 7)) AS Quantidade, d.HistoricoPadrao, a.HistoricoComplementar, " +
                            "dbo.fn_Lancamento_Detalhe(a.LancamentoID) AS Detalhe, " +
                            "a.ValorTotalLancamento AS ValorTotalLancamento, " +
                            "dbo.fn_Lancamento_Totais(a.LancamentoID, 6) AS ValorTotalConvertido, a.Observacao , CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                         "FROM Lancamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), HistoricosPadrao d WITH(NOLOCK), @TempTable z " +
                         "WHERE a.EmpresaID=" + empresaId + " AND a.EstadoID = b.RecursoID AND " +
                         "a.TipoLancamentoID = c.ItemID AND a.HistoricoPadraoID = d.HistoricoPadraoID AND " +
                         fldKey + sOperator + textToSearch + " " +
                         condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.LancamentoID " +
                         "AND z.IDTMP > " + startID +
                         " ORDER BY z.IDTMP";

                    break;

                case 10140:    //  modcontabil/submodcontabilidade/extratosbancarios      

                    if ((returnFldType("ExtratosBancarios", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    if (fldKey == "a.Conta")
                    {
                        fldKey = "dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) ";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.ExtratoID " +
                              "FROM ExtratosBancarios a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed + " a.EmpresaID=" + empresaId + " AND " +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.ExtratoID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.ExtratoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.ExtratoID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                            " a.ExtratoID, b.RecursoAbreviado as Estado, a.Concilia, a.dtInicial, a.dtFinal, a.dtSaldo, " +
                               "dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS Conta, " +
                               "dbo.fn_ExtratoBancario_Totais(a.ExtratoID, NULL, 1) AS NumeroLancamentos, " +
                               "dbo.fn_ExtratoBancario_Totais(a.ExtratoID, NULL, 2) AS TotalDebitos, " +
                               "dbo.fn_ExtratoBancario_Totais(a.ExtratoID, NULL, 3) AS TotalCreditos, " +
                               "a.SaldoFinal, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                            "FROM ExtratosBancarios a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " +
                            "WHERE a.EmpresaID=" + empresaId + " AND a.EstadoID = b.RecursoID AND " +
                            fldKey + sOperator + textToSearch + " " +
                            condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.ExtratoID " +
                            "AND z.IDTMP > " + startID +
                            " ORDER BY z.IDTMP";

                    break;

                case 12210:    // Competencias       

                    if ((returnFldType("Competencias", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.CompetenciaID " +
                              "FROM Competencias a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) " +
                              "WHERE a.EstadoID=b.RecursoID AND a.TipoCompetenciaID=c.ItemID AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.CompetenciaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.CompetenciaID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.CompetenciaID, b.RecursoAbreviado as Estado, a.Competencia, c.ItemMasculino AS Tipo, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Competencias a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND a.TipoCompetenciaID=c.ItemID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.CompetenciaID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 12220:    //  modrecursoshumanos/submodtreinamento/ret

                    if ((returnFldType("RET", fldKey) == 135) && (textToSearch == "'0'"))
                    {
                        textToSearch = "0";
                    }

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.RETID " +
                              "FROM RET a WITH(NOLOCK), Competencias b WITH(NOLOCK) " +
                              "WHERE a.TreinamentoID=b.CompetenciaID AND a.EmpresaID = " + (empresaId) + " AND " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.RETID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.RETID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.RETID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.RETID, c.RecursoAbreviado as Estado, b.Competencia AS Treinamento, a.dtData, a.dtVencimento, a.Observacao, " +
                             "a.AvaliacaoTreinandos, a.AvaliacaoTreinamento, a.AvaliacaoEficacia, d.Fantasia AS Fornecedor, " +
                             "f.SimboloMoeda AS Moeda, a.Valor, CONVERT(NUMERIC(5,1), dbo.fn_RET_CargaHoraria(a.RETID)) AS CH, " +
                             "a.FrequenciaMinima, a.NotaMinima, a.Aulas, a.Turmas," +
                             "(SELECT COUNT(*) FROM RET_Participantes WITH(NOLOCK) WHERE RETID=a.RETID) AS Participantes, " +
                             "e.Fantasia AS Proprietario, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM RET a WITH(NOLOCK) " +
                                "LEFT OUTER JOIN Pessoas d WITH(NOLOCK) ON (a.FornecedorID=d.PessoaID) " +
                                "LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID=e.PessoaID) " +
                                "LEFT OUTER JOIN Conceitos f WITH(NOLOCK) ON (a.MoedaID=f.ConceitoID) " +
                                "INNER JOIN Competencias b WITH(NOLOCK) ON (a.TreinamentoID=b.CompetenciaID) " +
                                "INNER JOIN Recursos c WITH(NOLOCK) ON (a.EstadoID=c.RecursoID) " +
                                "INNER JOIN @TempTable z ON (z.IDFromPesq=a.RETID) " +
                             "WHERE " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 1230:    //  modbasico/submodreno/prospect

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.prospectID " +
                              "FROM prospect a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    if (fldKey == "a.prospectID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.prospectID";
                    }

                    sql2 = "SELECT TOP " + pageSize +
                             " a.prospectID, b.RecursoAbreviado as Estado, a.Nome, a.Fantasia, c.ItemMasculino, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM prospect a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND a.TipoPessoaID = c.ItemID AND " +
                             fldKey + sOperator + textToSearch + " " +
                             condition + sFiltroContexto + sFiltro + " AND z.IDFromPesq=a.prospectID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 12320: //  modrecursoshumanos/submodavaliacao/avaliacao

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.AvaliacaoID " +
                              "FROM AvaliacoesDesempenho a WITH(NOLOCK), Pessoas d WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro + " AND a.FuncionarioID=d.PessoaID AND " +
                              "dbo.fn_Avaliacao_Participante(a.AvaliacaoID, " + (userId) + ") = 1 AND " +
                              "a.EmpresaID=" + (empresaId);

                    if (fldKey == "a.AvaliacaoID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.AvaliacaoID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.AvaliacaoID, b.RecursoAbreviado as Estado, c.ItemMasculino AS TipoAvaliacao, d.Fantasia AS Funcionario, " +
                             "e.RecursoFantasia AS Cargo, a.dtInicio, a.dtFim, a.Observacao, f.Fantasia AS GestorImediato, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM AvaliacoesDesempenho a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Recursos e WITH(NOLOCK), Pessoas f WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND a.TipoAvaliacaoID = c.ItemID AND " +
                             "a.FuncionarioID=d.PessoaID AND a.CargoID=e.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro + " AND " +
                             "dbo.fn_Avaliacao_Participante(a.AvaliacaoID, " + (userId) + ") = 1 AND " +
                             "a.EmpresaID=" + (empresaId) + " AND a.ProprietarioID=f.PessoaID AND z.IDFromPesq=a.AvaliacaoID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;

                case 12510: //  modrecursoshumanos/submodfopag/fopag

                      sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP "+((pageSize.intValue() * pageNumber.intValue())) +" a.FopagID " +
                              "FROM Fopag a WITH(NOLOCK) " +
                              "WHERE "+sAccessAllowed +
                              fldKey+sOperator+textToSearch+" " +
                              condition+sFiltroContexto+sFiltro+" AND a.EmpresaID="+(empresaId);

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.FopagID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.FopagID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                       sql= sql +" ORDER BY "+fldKey+" "+order+" ,a.FopagID";


                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.FopagID, b.RecursoAbreviado as Estado, c.ItemMasculino AS TipoFopag, a.ProcessamentoLiberado, a.IncluiVerbasNormais, " +
                             "a.IncluiVerbasVendas, a.PlanoSaudeFuncionario, a.SubtraiSalarioFixo, a.ArredondaSalarios, a.GeraFinanceirosDistintos, " +
                             "a.DiaAntecipacaoAviso, a.DiaAntecipacaoPagamento, a.DiasUteisPagamento, a.DiasDesconsiderarVendas, " +
                             "a.DiasRecuarVendas, a.dtFopag, a.dtFopagInicio, a.dtFopagFim, a.dtFopagPagamento AS dtFopagPagamento, " +
                             "dbo.fn_Fopag_Totais(a.FopagID, NULL, NULL, NULL, NULL, NULL, 1, 1) AS TotalFopag, a.Observacao, " +
                             "dbo.fn_Fopag_Data(FopagID, 4) AS dtApuracaoInicio, dbo.fn_Fopag_Data(FopagID, 5) AS dtApuracaoFim, a.EstadoID, a.dtUltimoFechamento, a.TipoFopagID " +
                             ", CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM Fopag a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND a.TipoFopagID = c.ItemID AND " +                             
                             fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro +
                             " AND a.EmpresaID=" + (empresaId) + " AND z.IDFromPesq=a.FopagID " +
                             "AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";
                    break;

                case 12530: //  modrecursoshumanos/submodfopag/verbas

                    sql = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " + ((pageSize.intValue() * pageNumber.intValue())) + " a.VerbaID " +
                              "FROM VerbasFopag a WITH(NOLOCK) " +
                              "WHERE " + sAccessAllowed +
                              fldKey + sOperator + textToSearch + " " +
                              condition + sFiltroContexto + sFiltro;

                    //Registros Vencidos 
                    if (RegistrosVencidos.booleanValue() == true)
                    {
                        sql = sql + " AND (dbo.fn_Registro_Vencido( " + formId + ", " + filtroContextoId + ", " + subformId + ", a.VerbaID, a.EstadoID ) = 1)";
                    }

                    if (fldKey == "a.VerbaID")
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order;
                    }
                    else
                    {
                        sql = sql + " ORDER BY " + fldKey + " " + order + " ,a.VerbaID";
                    }

                    sql2 = "SELECT TOP " + pageSize + " " +
                             "a.VerbaID, b.RecursoAbreviado as Estado, a.Verba, a.EhProvento, a.EhAutomatica, a.PermiteValorExterno, a.EhBeneficio, a.EhParametro, a.MetodoDataReferenciaID," +
                             "a.MetodoReferenciaID, a.MetodoValorID, a.TrataDataFopagInicio, a.TrataDataFopagFim, a.TrataDataReferencia, " +
                             "a.TrataReferencia, a.TrataValor, a.Observacao, CONVERT(VARCHAR(50), SPACE(0)) AS fldError " +
                             "FROM VerbasFopag a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " +
                             "WHERE a.EstadoID = b.RecursoID AND " +
                             fldKey + sOperator + textToSearch + " " + condition + sFiltroContexto + sFiltro +
                             " AND z.IDFromPesq=a.VerbaID AND z.IDTMP > " + startID +
                             " ORDER BY z.IDTMP";

                    break;



                default:
                    break;
            }
        }

        // Executa procedure sp_RelatorioEstoque
        public System.Data.DataSet execPesqlist()
        {

            System.Data.DataSet dsResult;
            DateTime t_inicio;
            DateTime t_fim;
            TimeSpan t_diferenca;

            t_inicio = DateTime.Now;

            ProcedureParameters[] procParams = new ProcedureParameters[4];
            procParams[0] = new ProcedureParameters(
                "@iPageSize",
                System.Data.SqlDbType.Int,
                pageSize.intValue());

            procParams[1] = new ProcedureParameters(
                "@sSQL",
                System.Data.SqlDbType.VarChar,
                sql);
            procParams[1].Length = 8000;

            procParams[2] = new ProcedureParameters(
                "@sSQL2",
                System.Data.SqlDbType.VarChar,
                sql2);
            procParams[2].Length = 8000;

            procParams[3] = new ProcedureParameters(
                "@sExecAfterCreateTable",
                System.Data.SqlDbType.VarChar,
                sqlExecAfterCreateTable);
            procParams[3].Length = 8000;

            dsResult = DataInterfaceObj.execQueryProcedure(
                "sp_pesqlist",
                procParams);

            /*return DataInterfaceObj.execQueryProcedure(
                "sp_pesqlist",
                procParams);*/

            t_fim = DateTime.Now;
            t_diferenca = t_fim.Subtract(t_inicio);


            /*
            empresaId.intValue()
            userId.intValue()
            pageSize.intValue()
            _Estoque
            textToSearch
            _ChavePessoaID -- ==1 c/ pessoa, ==2 s/ pessoa
            _ParceiroID
            _PessoaID
            _FinalidadeID,
             
            @EmpresaID INT,
            @ColaboradorID INT,
            @PageSize INT,
            @Estoque BIT,
            @Argumento VARCHAR(100),
            @ComPessoa BIT,
            @ParceiroID INT,
            @PessoaID INT,
            @FinalidadeID INT,
            @TempoResposta INT
             */

            ProcedureParameters[] procParams_Log = new ProcedureParameters[10];
            procParams_Log[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                empresaId.intValue());

            procParams_Log[1] = new ProcedureParameters(
                "@ColaboradorID",
                System.Data.SqlDbType.Int,
                userId.intValue());

            procParams_Log[2] = new ProcedureParameters(
                "@PageSize",
                System.Data.SqlDbType.Int,
                pageSize.intValue());

            procParams_Log[3] = new ProcedureParameters(
                "@Estoque",
                System.Data.SqlDbType.Int,
                (_Estoque == 1 ? 1 : 0));

            procParams_Log[4] = new ProcedureParameters(
                "@Argumento",
                System.Data.SqlDbType.Int,
                argumentoIsEmpty.intValue());

            procParams_Log[5] = new ProcedureParameters(
                "@ComPessoa",
                System.Data.SqlDbType.Bit,
                (_ChavePessoaID != 1 ? 0 : 1));

            procParams_Log[6] = new ProcedureParameters(
                "@ParceiroID",
                System.Data.SqlDbType.Int,
                _ParceiroID);

            procParams_Log[7] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                _PessoaID);

            procParams_Log[8] = new ProcedureParameters(
                "@FinalidadeID",
                System.Data.SqlDbType.Int,
                _FinalidadeID);

            procParams_Log[9] = new ProcedureParameters(
                "@TempoResposta",
                System.Data.SqlDbType.Float,
                t_diferenca.TotalSeconds);

            DataInterfaceObj.execNonQueryProcedure(
                            "sp_PesqList_Log",
                            procParams_Log);

            return dsResult;
        }

        public System.Data.DataSet execPesqlistSql()
        {
            System.Data.DataSet dsResult;

            dsResult = DataInterfaceObj.execQueryProcedure("dbo.sp_executesql", parametrosSQL);

            return dsResult;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            MakeSQLs();

            try
            {
                if ((formId.intValue() == 5220) || (formId.intValue() == 5110) || (formId.intValue() == 8110) || (formId.intValue() == 1210))
                    WriteResultXML(execPesqlistSql());
                else
                    WriteResultXML(execPesqlist());
            }
            catch (System.Exception exception)
            {
                
             WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select 'Filtro Inv�lido' as fldError"));               
            
            }
        }
    }
}