using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class purchaseitemtrolley : System.Web.UI.OverflyPage
    {
        private string userId;
        private string pessoaId;
        private string parceiroId;
        private string moedaId;
        private string financiamentoId;
        private string frete;
        private string meioTransporteId;
        private Integer dataLen;
        private string[] empresaId;
        private string[] produtoId;
        private string[] corpprodutoId;
        private string[] desconto;
        private string[] loteId;
        private string[] quantidade;
        private string[] precoInterno;
        private string[] precoRevenda;
        private string[] precoUnitario;
        private string[] cfopId;
        private string[] finalidadeID;
        private string[] guia;
        private string[] icmsst;
        private string[] sacolaobservacoes;


		public string nUserID
		{    
			set { userId = value; }
		}
        public string[] nEmpresaID
        {
            set { empresaId = value; }
        }
		public string nPessoaID
		{
			set { pessoaId = value; }
		}
		public string nParceiroID
		{
			set { parceiroId = value; }
		}
		public string nMoedaID
		{
			set { moedaId = value; }
		}
		public string nFinanciamentoID
		{
			set { financiamentoId = value; }
		}
		public string nFrete
		{
			set { frete = value; }
		}
		public string sMeioTransporteID
		{
			set { meioTransporteId = value; }
		}
		public Integer nDataLen
		{
			set { dataLen = value; }
		}		
		public string[] nProdutoID
		{
            set
            {
                produtoId = value;

                for (int i = 0; i < produtoId.Length; i++)
                {
                    if (produtoId[i] == "")
                    {
                        produtoId[i] = "NULL";
                    }
                }
            }
		}
        public string[] nCorpProdutoID
        {
            set
            {
                corpprodutoId = value;

                for (int i = 0; i < corpprodutoId.Length; i++)
                {
                    if (corpprodutoId[i] == "")
                    {
                        corpprodutoId[i] = "NULL";
                    }
                }
            }
        }
        public string[] nDesconto
        {
            set { desconto = value; }
        }
        public string[] nLoteID
        {
            set
            {
                loteId = value;

                for (int i = 0; i < loteId.Length; i++)
                {
                    if (loteId[i] == "")
                    {
                        loteId[i] = "NULL";
                    }
                }
            }
        }
		public string[] nQuantidade
		{
			set { quantidade = value; }
		}
        public string[] nPrecoInterno
		{
            set { precoInterno = value; }
		}
        public string[] nPrecoRevenda
		{
            set { precoRevenda = value; }
		}
        public string[] nPrecoUnitario
        {
            set { precoUnitario = value; }
        }
        public string[] sGuia
        {
            set { guia = value; }
        }
        public string[] nICMSST
        {
            set { icmsst = value; }
        }
		public string[] sCFOPID
		{
			set
			{
				cfopId = Request.QueryString["sCFOPID"].Split(Separator);

				for (int i = 0; i < cfopId.Length; i++)
				{
					if (cfopId[i] == "0")
					{
						cfopId[i] = "NULL";
					}
				}
			}
		}
        public string[] sFinalidade
        {
            set
            {
                finalidadeID = Request.QueryString["sFinalidade"].Split(Separator);

                for (int i = 0; i < finalidadeID.Length; i++)
                {
                    if (finalidadeID[i] == "0")
                    {
                        finalidadeID[i] = "NULL";
                    }
                }
            }
        }
        public string[] sSacolaObservacoes
        {
            set { sacolaobservacoes = value; }
        }
		// Executa procedure sp_ListaPrecos_Comprar e retorna a mensagem.
		protected string ListaPrecosComprar()
        {
            string result = "";
            ProcedureParameters[] procParams = new ProcedureParameters[30];

			for (int i = 0; i < procParams.Length; i++)
            {
				procParams[i] = new ProcedureParameters();
            }

			for (int i = 0; i < dataLen.intValue(); i++)
            {
                procParams[0].Name = "@EmpresaID";
				procParams[0].Type = System.Data.SqlDbType.Int;
				procParams[0].Data = int.Parse(empresaId[i]);

				procParams[1].Name = "@PessoaID";
				procParams[1].Type = System.Data.SqlDbType.Int;
                procParams[1].Data = int.Parse(pessoaId);

				procParams[2].Name = "@ParceiroID";
				procParams[2].Type = System.Data.SqlDbType.Int;
				procParams[2].Data = int.Parse(parceiroId);

				procParams[3].Name = "@ProdutoID";
				procParams[3].Type = System.Data.SqlDbType.Int;
                procParams[3].Data = int.Parse(produtoId[i]);

                procParams[4].Name = "@CorpProdutoID";
                procParams[4].Type = System.Data.SqlDbType.Int;
                procParams[4].Data = int.Parse(corpprodutoId[i]);

                procParams[5].Name = "@SacItemMaeID";
                procParams[5].Type = System.Data.SqlDbType.Int;
                procParams[5].Data = System.DBNull.Value;

                procParams[6].Name = "@SacItemCotMaeID";
                procParams[6].Type = System.Data.SqlDbType.Int;
                procParams[6].Data = System.DBNull.Value;

                procParams[7].Name = "@DescricaoProduto";
                procParams[7].Type = System.Data.SqlDbType.VarChar;
                procParams[7].Data = System.DBNull.Value;
                procParams[7].Length = 400;

                procParams[8].Name = "@PartNumber";
                procParams[8].Type = System.Data.SqlDbType.VarChar;
                procParams[8].Data = System.DBNull.Value;
                procParams[8].Length = 400;

                procParams[9].Name = "@Vigencia";
                procParams[9].Type = System.Data.SqlDbType.Int;
                procParams[9].Data = System.DBNull.Value;

                procParams[10].Name = "@Desconto";
                procParams[10].Type = System.Data.SqlDbType.Float;
                procParams[10].Data = (desconto[i] != null && double.Parse(desconto[i], CultureInfo.InvariantCulture.NumberFormat) > 0.00) ?
                                        (Object)double.Parse(desconto[i], CultureInfo.InvariantCulture.NumberFormat) : System.DBNull.Value;

				procParams[11].Name = "@Quantidade";
				procParams[11].Type = System.Data.SqlDbType.Int;
				procParams[11].Data = int.Parse(quantidade[i]);

				procParams[12].Name = "@MoedaID";
				procParams[12].Type = System.Data.SqlDbType.Int;
				procParams[12].Data = int.Parse(moedaId);

                procParams[13].Name = "@Guia";
                procParams[13].Type = System.Data.SqlDbType.VarChar;
                procParams[13].Data = guia[i].Length > 0 ? (Object)guia[i].ToString() : System.DBNull.Value;
                procParams[13].Length = 10;
                 
                procParams[14].Name = "@ICMSST";
                procParams[14].Type = System.Data.SqlDbType.Money;
                procParams[14].Data =
                    (icmsst[i] != null && double.Parse(icmsst[i], CultureInfo.InvariantCulture.NumberFormat) > 0.00) ?
                        (Object)double.Parse(icmsst[i], CultureInfo.InvariantCulture.NumberFormat) : System.DBNull.Value;                

                procParams[15].Name = "@PrecoInterno";
                procParams[15].Type = System.Data.SqlDbType.Money;
                procParams[15].Data =
                    (precoInterno[i] != null && double.Parse(precoInterno[i], CultureInfo.InvariantCulture.NumberFormat) > 0.00) ?
                        (Object)double.Parse(precoInterno[i], CultureInfo.InvariantCulture.NumberFormat) : System.DBNull.Value;

                procParams[16].Name = "@PrecoRevenda";
				procParams[16].Type = System.Data.SqlDbType.Money;
				procParams[16].Data =
                    (precoRevenda[i] != null && double.Parse(precoRevenda[i], CultureInfo.InvariantCulture.NumberFormat) > 0.00) ?
                        (Object)double.Parse(precoRevenda[i], CultureInfo.InvariantCulture.NumberFormat) : System.DBNull.Value;

                procParams[17].Name = "@PrecoUnitario";
				procParams[17].Type = System.Data.SqlDbType.Money;
				procParams[17].Data =
                    (precoUnitario[i] != null && double.Parse(precoUnitario[i], CultureInfo.InvariantCulture.NumberFormat) > 0.00) ?
                        (Object)double.Parse(precoUnitario[i], CultureInfo.InvariantCulture.NumberFormat) : System.DBNull.Value;

                procParams[18].Name = "@FinalidadeID";
                procParams[18].Type = System.Data.SqlDbType.Int;
                procParams[18].Data =
                    (finalidadeID[i] != null && finalidadeID[i] == "NULL") ?
                    System.DBNull.Value : (Object)long.Parse(finalidadeID[i]);


				procParams[19].Name = "@CFOPID";
				procParams[19].Type = System.Data.SqlDbType.Int;
				procParams[19].Data =
                    (cfopId[i] != null && cfopId[i] == "NULL") ?
					System.DBNull.Value : (Object)long.Parse(cfopId[i]);

				procParams[20].Name = "@FinanciamentoID";
				procParams[20].Type = System.Data.SqlDbType.Money;
				procParams[20].Data = financiamentoId;

				procParams[21].Name = "@IncluiFrete";
				procParams[21].Type = System.Data.SqlDbType.Int;
				procParams[21].Data = frete;

				procParams[22].Name = "@MeioTransporteID";
				procParams[22].Type = System.Data.SqlDbType.Int;
				procParams[22].Data = 
                    (meioTransporteId != null && long.Parse(meioTransporteId) == 0) ?
						System.DBNull.Value : (Object)long.Parse(meioTransporteId);

				procParams[23].Name = "@ColaboradorID";
				procParams[23].Type = System.Data.SqlDbType.Int;
				procParams[23].Data = userId;

				procParams[24].Name = "@UsuarioID";
				procParams[24].Type = System.Data.SqlDbType.Int;
				procParams[24].Data = userId;
                
                procParams[25].Name = "@LoteID";
                procParams[25].Type = System.Data.SqlDbType.Int;
                procParams[25].Data = int.Parse(loteId[i]);
                    /*(int.Parse(loteId[i]) != null) ?
                       System.DBNull.Value : (Object)int.Parse(loteId[i]); */

				procParams[26].Name = "@Web";
				procParams[26].Type = System.Data.SqlDbType.Bit;
				procParams[26].Data = 0;

                procParams[27].Name = "@SacolaObservacoes";
                procParams[27].Type = System.Data.SqlDbType.VarChar;
                procParams[27].Data = sacolaobservacoes[i].Length > 0 ? (Object)sacolaobservacoes[i].ToString() : System.DBNull.Value;
                procParams[27].Length = 400;

                procParams[28].Name = "@Resultado";
				procParams[28].Type = System.Data.SqlDbType.VarChar;
				procParams[28].Data = "";
				procParams[28].Direction = ParameterDirection.Output;
                procParams[28].Length = 100;

                procParams[29].Name = "@SacItemID";
                procParams[29].Type = System.Data.SqlDbType.Int;
                procParams[29].Data = "";
                procParams[29].Direction = ParameterDirection.Output;


                DataInterfaceObj.execNonQueryProcedure("sp_ListaPrecos_Comprar", procParams);

                // Gera o resultado para o usuario.
				result += (procParams[28].Data != DBNull.Value) ?
					(string)procParams[28].Data + (char)13 + (char)10 : "";
            }
            
            return (result != null || result.Length > 0) ? result : " NULL ";
		}

        public string ListaPrecosCotadorComprar()
        {
            string result = "";
            ProcedureParameters[] procParams = new ProcedureParameters[2];

            for (int i = 0; i < procParams.Length; i++)
            {
                procParams[i] = new ProcedureParameters();
            }

            procParams[0].Name = "@List";
            procParams[0].Type = System.Data.SqlDbType.VarChar;
            procParams[0].Data = Request.Form[0].ToString();

            procParams[1].Name = "@Result";
            procParams[1].Type = System.Data.SqlDbType.VarChar;
            procParams[1].Data = "";
            procParams[1].Direction = ParameterDirection.Output;
            procParams[1].Length = 100;

            DataInterfaceObj.execNonQueryProcedure("sp_ListaPrecos_Cotador_Comprar", procParams);

            result += (procParams[1].Data != DBNull.Value) ?
                    (string)procParams[1].Data + (char)13 + (char)10 : "";

            return (result != null || result.Length > 0) ? result : " NULL ";
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            string Mensagem = "";
            if (Request.Form.Count == 0)
            {
                Mensagem = ListaPrecosComprar();
            }
            else
            {
                Mensagem = ListaPrecosCotadorComprar();
            }
			
			// Gera o resultado para o usuario.
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select '" + Mensagem + "' as Mensagem"
				)
			);
		}
    }
}