using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class listaragendamento : System.Web.UI.OverflyPage
    {
        private string empresaId;
        private string registroId;
        private string estados;
        private string proprietario;
        private string classificacao;
        private string pesquisa;
        private string tiporesultado;
        private string tipoatendimento;
        private string dtdata;
        private string pessoaGravacaoId;
        private string userId;
        private string data;
        private string atender1;
        private string atender2;
        private string credito;
        private string emailID;

        protected string EmpresaID { set { empresaId = value != null ? value : ""; } }
        protected string RegistroID { set { registroId = value != null ? value : ""; } }
        protected string sEstados { set { estados = value != null ? value : ""; } }
        protected string nProprietario { set { proprietario = value != null ? value : ""; } }
        protected string sClassificacao { set { classificacao = value != null ? value : ""; } }
        protected string sPesquisa { set { pesquisa = value != null ? value : ""; } }
        protected string nTipoResultado { set { tiporesultado = value != null ? value : ""; } }
        protected string nTipoAtendimento { set { tipoatendimento = value != null ? value : ""; } }
        protected string ndtData { set { dtdata = value != null ? value : ""; } }
        protected string nPessoaGravacaoID { set { pessoaGravacaoId = value != null ? value : ""; } }
        protected string nUsuarioID { set { userId = value != null ? value : ""; } }
        protected string sData { set { data = value != null ? value : ""; } }
        protected string bAtender1 { set { atender1 = value != null ? value : ""; } }
        protected string bAtender2 { set { atender2 = value != null ? value : ""; } }
        protected string bCredito { set { credito = value != null ? value : ""; } }
        protected string nEmailID { set { emailID = value != null ? value : ""; } }

        protected DataSet PessoaAtendimento()
        {
            // Executa procedure sp_Atendimento_Realiza
            ProcedureParameters[] PessoaAtendimentoParams = new ProcedureParameters[16];

            PessoaAtendimentoParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                empresaId == null || empresaId.Length == 0 ?
                System.DBNull.Value :
                (Object)int.Parse(empresaId));

            PessoaAtendimentoParams[1] = new ProcedureParameters(
                "@Registros",
                System.Data.SqlDbType.Int,
                registroId == null || registroId.Length == 0 ?
                System.DBNull.Value :
                (Object)int.Parse(registroId));

            PessoaAtendimentoParams[2] = new ProcedureParameters(
                "@Estados",
                System.Data.SqlDbType.VarChar,
                estados == null || estados.Length == 0 ?
                    System.DBNull.Value : (Object)estados,
                8000);

            PessoaAtendimentoParams[3] = new ProcedureParameters(
                "@ProprietarioID",
                System.Data.SqlDbType.Int,
                proprietario == null || proprietario.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(proprietario));

            PessoaAtendimentoParams[4] = new ProcedureParameters(
                "@Classificacoes",
                System.Data.SqlDbType.VarChar,
                classificacao == null || classificacao.Length == 0 ?
                System.DBNull.Value : (Object)classificacao, 8000);

            PessoaAtendimentoParams[5] = new ProcedureParameters(
                "@Pesquisa",
                System.Data.SqlDbType.VarChar,
                pesquisa == null || pesquisa.Length == 0 ?
                    System.DBNull.Value : (Object)pesquisa,
                1000);

            PessoaAtendimentoParams[6] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                tiporesultado == null || tiporesultado.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(tiporesultado));

            PessoaAtendimentoParams[7] = new ProcedureParameters(
                "@TipoAtendimentoID",
                System.Data.SqlDbType.Int,
                tipoatendimento == null || tipoatendimento.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(tipoatendimento));

            PessoaAtendimentoParams[8] = new ProcedureParameters(
                "@dtData",
                System.Data.SqlDbType.DateTime,
                System.DBNull.Value);

            PessoaAtendimentoParams[9] = new ProcedureParameters(
                "@PessoaGravacaoID",
                System.Data.SqlDbType.Int,
                pessoaGravacaoId == null || pessoaGravacaoId.Length == 0 || pessoaGravacaoId == "0" ?
                System.DBNull.Value : (Object)int.Parse(pessoaGravacaoId));

            PessoaAtendimentoParams[10] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                userId == null || userId.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(userId));

            PessoaAtendimentoParams[11] = new ProcedureParameters(
                "@dtDataRFV",
                System.Data.SqlDbType.DateTime,
                data.ToString() == null ? DBNull.Value : (Object)(data.ToString()));

            PessoaAtendimentoParams[12] = new ProcedureParameters(
                "@Atender",
                System.Data.SqlDbType.Bit,
                atender1 != null ? Convert.ToBoolean(atender1) : false);

            PessoaAtendimentoParams[13] = new ProcedureParameters(
                "@AtendimentosPendentes",
                System.Data.SqlDbType.Bit,
                atender2 != null ? Convert.ToBoolean(atender2) : false);

            PessoaAtendimentoParams[14] = new ProcedureParameters(
                "@Credito",
                System.Data.SqlDbType.Bit,
                credito != null ? Convert.ToBoolean(credito) : false);

            PessoaAtendimentoParams[15] = new ProcedureParameters(
                "@EmailID",
                System.Data.SqlDbType.Int,
                emailID != null && emailID != "" ? (Object)int.Parse(emailID) : DBNull.Value);

            string strParams = "";

            for (int i = 0; i < PessoaAtendimentoParams.Length; i++)
                strParams += PessoaAtendimentoParams[i].Data.ToString() + ", ";

                return DataInterfaceObj.execQueryProcedure(
                    "sp_Pessoa_Atendimento",
                    PessoaAtendimentoParams);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(PessoaAtendimento());
        }
    }
}
