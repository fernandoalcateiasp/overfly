using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace Overfly3.serversidegenEx
{
    public partial class imageblob : System.Web.UI.OverflyPage
    {
        private string formId = "0";

        public string nFormID
        {
            get { return formId; }
            set { formId = value; }
        }
        private string subFormId = "0";

        public string nSubFormID
        {
            get { return subFormId; }
            set { subFormId = value; }
        }
        private string registroId = "0";

        public string nRegistroID
        {
            get { return registroId; }
            set { registroId = value; }
        }
        private string versao = "";

        public string Versao
        {
            get { return versao; }
            set { versao = value; }
        }

        private string fileID = "";
        public string nFileID
        {
            get { return fileID; }
            set { fileID = value; }
        }

        private string tamanho = "0";
           
		public string nTamanho
		{
			set
			{
				tamanho = value;

				if (tamanho == "0")
				{
					// Documentos, QAF, RET
					if (formId == "7110" || formId == "7180" || formId == "12220")
					{
						tamanho = "4";
					}
					else
					{
						tamanho = "2";
					}
				}
			}
		}
        private string ordem = "1";

        public string nOrdem
		{
			set { ordem = value != null ? value : "1"; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
            if ((fileID != null) && (fileID != ""))
            {
                // Gera o resultado.
                DataSet result = DataInterfaceObj.getRemoteData(
                    "SELECT TOP 1 Arquivo AS Imagem " +
                        "FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] a WITH(NOLOCK) " +
                        "WHERE a.DocumentoID = " + fileID + " AND a.TipoArquivoID = 1451");

                // Escreve a imagem.
                MemoryStream ms = new MemoryStream();
                byte[] dbData = (Byte[])result.Tables[1].Rows[0]["Imagem"];
                ms.Write(dbData, 0, dbData.Length);
                Bitmap bm = new Bitmap(ms);

                Response.ContentType = "image/jpeg";
                bm.Save(Response.OutputStream, ImageFormat.Jpeg);
            }
            else
            {
                string strVersao = "";

                if (versao == null || versao == "")
                    strVersao = "a.Versao IS NULL AND ";
                else
                    strVersao = "a.Versao = " + versao + " AND ";

                // Gera o resultado.
                DataSet result = DataInterfaceObj.getRemoteData(
                    "SELECT TOP 1 * FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] a WITH(NOLOCK) " +
                    "WHERE a.FormID = " + (formId) + " AND " +
                    "a.SubFormID = " + (subFormId) + " AND " +
                    "a.TipoArquivoID = 1451 AND " +
                    "a.RegistroID = " + (registroId) + " AND " +
                    strVersao + " a.TamanhoImagem = " + (tamanho) + " AND " +
                    "a.OrdemImagem = " + (ordem) + " " +
                    "ORDER BY a.TamanhoImagem, a.OrdemImagem, a.DocumentoID");

                // Escreve a imagem.
                MemoryStream ms = new MemoryStream();
                byte[] dbData = (Byte[])result.Tables[1].Rows[0]["Arquivo"];
                ms.Write(dbData, 0, dbData.Length);
                Bitmap bm = new Bitmap(ms);

                Response.ContentType = "image/jpeg";
                bm.Save(Response.OutputStream, ImageFormat.Jpeg);
            }
        }
    }
}
