using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Net.Mail;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx.emails
{
	public partial class emailpedidoatual : System.Web.UI.Page
	{
		private string html = "0";
		private string mandEmail = "0";
		private string vendedorID = "0";
		private string empresaID = "0";
		private string contatoID = "0";
		private string clienteID = "0";
		private string usuarioFinalID = "0";
		
		private string contatoEmail;

		/*
		private string mailTo = "";
		private string mailFrom = "";
		private string mailCc = "";
		private string mailBcc = "";
		private string mailSubject = "";
		private string sMailBody = "";
		*/
		private string webPath;
		private string sql;

		private dataInterface pageDataInterface = new dataInterface(
			System.Configuration.ConfigurationManager.AppSettings["application"]
		);
		
		private System.Data.DataSet emailsDataSet;
		MailMessage mailMessage = new MailMessage();

		private string strXML;
		public string StrXML
		{
			get { return strXML; }
		}

		protected void Page_Load(object sender, EventArgs e)
		{
			Response.Cache.SetNoStore();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);

			// Recupera os par�metros da p�gina.
			html = Request.QueryString["bHTML"] != null ?
				Request.QueryString["bHTML"] : "0";
				
			mandEmail = Request.QueryString["bMandEmail"] != null ?
				Request.QueryString["bMandEmail"] : "0";
				
			vendedorID = Request.QueryString["nVendedorID"] != null ?
				Request.QueryString["nVendedorID"] : "0";
				
			empresaID = Request.QueryString["nEmpresaID"] != null ?
				Request.QueryString["nEmpresaID"] : "0";
				
			clienteID = Request.QueryString["nClienteID"] != null ?
				Request.QueryString["nClienteID"] : "0";
				
			usuarioFinalID = Request.QueryString["nUsuarioFinalID"] != null ?
				Request.QueryString["nUsuarioFinalID"] : "0";
				
			contatoID = Request.QueryString["nContatoID"] != null ?
				Request.QueryString["nContatoID"] : "0";
			
			// Gera o resultado.
			makeResponse();
		}

		protected void makeResponse()
		{
			sql = null;
			
			queryEmails();
			
			if (mandEmail == "1" && emailsDataSet.Tables[1].Rows.Count > 0)
			{
				contatoID = emailsDataSet.Tables[1].Rows[0]["ContatoID"].ToString();
				contatoEmail = emailsDataSet.Tables[1].Rows[0]["ContatoEmail"].ToString();

				if (contatoEmail == null || contatoEmail == "")
				{
					sql = 
						"SELECT 'O contato n�o tem e-mail cadastrado.' AS msg, " +
						contatoID + 
						" AS ContatoID";
				}
				else
				{
					if(sendEmail())
					{
						sql = "SELECT 'E-Mail enviado com sucesso.' AS msg, " +
							contatoID + " AS ContatoID";
					}
				}
			}
			else if (mandEmail == "0" && emailsDataSet.Tables[1].Rows.Count > 0)
			{
				sql = "SELECT 'E-Mail enviado com sucesso.' AS msg, " +
					contatoID + " AS ContatoID";
			}
			else
			{
				sql = "SELECT 'N�o foi poss�vel enviar o e-mail.' AS msg, " +
					contatoID + " AS ContatoID";
			}

			// Gera o resultado.
			dataInterface resultDataInterface = new dataInterface(
				System.Configuration.ConfigurationManager.AppSettings["application"]
			);
			
			// Obtem o DSO.
			System.Data.DataSet resultDataSet =
				pageDataInterface.getRemoteData(sql);
			// Converte o DSO para XML
			System.Xml.XmlDocument xmlDataSet =
				pageDataInterface.datasetToXML(resultDataSet);
			// Concatena o cabecalho e o conteudo do rsultado.
			strXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
				xmlDataSet.InnerXml;

		}

		protected void queryEmails()
		{
			emailsDataSet = pageDataInterface.getRemoteData(
				"SELECT " +
					"(SELECT TOP 1 Fantasia FROM Pessoas WITH(NOLOCK) " +
						"WHERE PessoaID=" + vendedorID + ") AS Vendedor, " +
                    "dbo.fn_Pessoa_Telefone(" + vendedorID + ", 119, 120,1,0,NULL) AS VendedorTelefone, " +
					"(SELECT Fantasia FROM Pessoas WITH(NOLOCK) " +
						"WHERE PessoaID = " + contatoID + ") AS Contato, " +
					contatoID + " AS ContatoID," +
					"dbo.fn_Pessoa_URL(" + vendedorID + ", 124, NULL) AS VendedorEmail, " +
					"dbo.fn_Pessoa_URL(" + contatoID + ", 124, NULL) AS ContatoEmail, " +
					"(SELECT TOP 1 Fantasia FROM Pessoas WITH(NOLOCK) " +
						"WHERE PessoaID=" + empresaID + ") AS Empresa, " +
					"dbo.fn_Pessoa_URL(" + empresaID + ", 125, NULL) " +
				"AS EmpresaURL"
			);
		}
		
		protected bool sendEmail()
		{
			// Pega a linha de retorno
			System.Data.DataRow row = emailsDataSet.Tables[1].Rows[0];
			
			// Monta o link do click do email.
			webPath = emailsDataSet.Tables[1].Rows[0]["EmpresaURL"] +
				"/commonpages/serverside/imageblob.asp";
			
			// Monta os dados do email.

			mailMessage.From = 
				new MailAddress(row["VendedorEmail"].ToString());
			mailMessage.To.Add(row["ContatoEmail"].ToString());
			mailMessage.Subject = emailsDataSet.Tables[1].Rows[0]["Empresa"] +
				" - Pedido atual";
			mailMessage.Body =
				"<B>" + emailsDataSet.Tables[1].Rows[0]["Contato"] +
				",</B><BR><BR>" +
				"<A HREF='" + emailsDataSet.Tables[1].Rows[0]["EmpresaURL"] +
				"/default.aspx?nUserID=" + contatoID +
				"&nEmpresaID=" + empresaID +
				"&nClienteID=" + clienteID +
				"&nUsuarioFinalID=" + usuarioFinalID +
				"&nPageID=13'" + ">Clique aqui</A> " + 
				"para finalizar o seu pedido.<BR><BR><BR>" +
				"<B>" + emailsDataSet.Tables[1].Rows[0]["Vendedor"] +
				"</B><BR>" +
				emailsDataSet.Tables[1].Rows[0]["VendedorTelefone"] +
				"<BR>" +
				"<A href='" +
				emailsDataSet.Tables[1].Rows[0]["EmpresaURL"] + "'>" +
				"<IMG SRC='" + webPath + "?nFormID=1210&" +
				"nSubFormID=20100&nRegistroID=" + empresaID + 
				"' BORDER='0'></IMG>" +
				"</A><BR><BR>" +
				"<HTML><BODY><P><FONT FACE='Tahoma' SIZE='2'>" + 
				"</FONT></P></BODY></HTML>";
			
			// Manda o e mail
			try
			{
				SmtpClient smtpClient = new SmtpClient("172.25.100.160", 25);
				smtpClient.Send(mailMessage);
			}
			catch (System.Exception e)
			{
				sql = "SELECT 'N�o foi poss�vel enviar o e-mail.\r\n" +
					"Motivo: " + e.Message + "' AS msg, " +
					contatoID + " AS ContatoID";
				
				return false;
			}
						
			return true;
		}
	}
}
