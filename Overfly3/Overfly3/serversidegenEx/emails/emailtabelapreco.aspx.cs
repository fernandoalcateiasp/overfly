using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSOverflyRDS;
using WSData;
using java.lang;


namespace Overfly3.serversidegenEx.emails
{
    public partial class emailproposta : System.Web.UI.OverflyPage        
    {
        private Integer SacolaId;
        private String Resultado;

        public Integer nSacolaID
        {
            set { SacolaId = value; }
        }

        protected void SendEmail_1()
        {

            ProcedureParameters[] procParams = new ProcedureParameters[2];

            procParams[0] = new ProcedureParameters(
                "@SacolaID",
                System.Data.SqlDbType.Int,
                SacolaId.ToString());

            procParams[1] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                null,
                ParameterDirection.Output);
            procParams[1].Length = 100;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Email_Sacola",
                procParams);

            Resultado = procParams[1].Data.ToString();
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            SendEmail_1();

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT ' " + Resultado + " ' AS Resultado "));
        }
    }
}