using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.serversidegenEx.emails
{
	public partial class sendemailsmarketing : System.Web.UI.OverflyPage
	{
		//private Integer empresaID;
		protected Integer nEmpresaID
		{
			get { return nEmpresaID; }
			set { nEmpresaID = value; }
		}

		private string estados;
		protected string sEstados
		{
			get { return estados; }
			set { estados = value; }
		}

		private string canais;
		protected string sCanais
		{
			get { return canais; }
			set { canais = value; }
		}

		private string classificacoes;
		protected string sClassificacoes
		{
			get { return classificacoes; }
			set { classificacoes = value; }
		}

		private string paises;
		protected string sPaises
		{
			get { return paises; }
			set { paises = value; }
		}

		private string ufs;
		protected string sUFs
		{
			get { return ufs; }
			set { ufs = value; }
		}

		private string cidades;
		protected string sCidades
		{
			get { return cidades; }
			set { cidades = value; }
		}

		private string familias;
		protected string sFamilias
		{
			get { return familias; }
			set { familias = value; }
		}

		private string marcas;
		protected string sMarcas
		{
			get { return marcas; }
			set { marcas = value; }
		}

		private string produtos;
		protected string sProdutos
		{
			get { return produtos; }
			set { produtos = value; }
		}

		private Integer estadoProdutoID;
		protected Integer nEstadoProdutoID
		{
			get { return estadoProdutoID; }
			set { estadoProdutoID = value; }
		}

		private string assunto;
		protected string sAssunto
		{
			get { return assunto; }
			set { assunto = value; }
		}

		private Integer tipoEmailID;
		protected Integer nTipoEmailID
		{
			get { return tipoEmailID; }
			set { tipoEmailID = value; }
		}

		private Integer usuarioID;
		protected Integer nUsuarioID
		{
			get { return usuarioID; }
			set { usuarioID = value; }
		}

		private Integer vitrineID;
		protected Integer nVitrineID
		{
			get { return vitrineID; }
			set { vitrineID = value; }
		}

		private Integer tipoResultadoID;
		protected Integer nTipoResultadoID
		{
			get { return tipoResultadoID; }
			set { tipoResultadoID = value; }
		}

		private string validade;
		protected string dtValidade
		{
			get { return validade; }
			set { validade = value; }
		}

		private java.lang.Boolean ok;
		protected java.lang.Boolean nOK
		{
			get { return ok; }
			set { ok = value; }
		}

		private java.lang.Boolean especifico;
		protected java.lang.Boolean bEspecifico
		{
			get { return especifico; }
			set { especifico = value; }
		}

		private string resultado;
		protected string Resultado
		{
			get { return resultado; }
			set { resultado = value; }
		}

		private DataSet dsEmailMarketing = null;

		protected void EmailMarketing()
		{
			ProcedureParameters[] param = new ProcedureParameters[] {
				new ProcedureParameters("@EmpresaID", SqlDbType.Int, nEmpresaID.intValue()),
				new ProcedureParameters("@Estados", SqlDbType.VarChar, sEstados, 8000),
				new ProcedureParameters("@Atuacoes", SqlDbType.VarChar, sCanais, 8000),
				new ProcedureParameters("@Classificacoes", SqlDbType.VarChar, sClassificacoes, 8000),
				new ProcedureParameters("@Paises", SqlDbType.VarChar, sPaises, 8000),
				new ProcedureParameters("@UFs", SqlDbType.VarChar, sUFs, 8000),
				new ProcedureParameters("@Cidades", SqlDbType.VarChar, sCidades, 8000),
				new ProcedureParameters("@Familias", SqlDbType.VarChar, sFamilias, 8000),
				new ProcedureParameters("@Marcas", SqlDbType.VarChar, sMarcas, 8000),
				new ProcedureParameters("@Produtos", SqlDbType.VarChar, sProdutos, 1000),
				new ProcedureParameters("@EstadoProdutoID", SqlDbType.Int, nEstadoProdutoID.intValue()),
				new ProcedureParameters("@TipoEmailID", SqlDbType.Int, nTipoEmailID.intValue()),
				new ProcedureParameters("@dtValidade", SqlDbType.DateTime, dtValidade),
				new ProcedureParameters("@Assunto", SqlDbType.VarChar, sAssunto),
				new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUsuarioID.intValue()),
				new ProcedureParameters("@VitrineID", SqlDbType.Int, nVitrineID.intValue()),
				new ProcedureParameters("@TipoResultadoID", SqlDbType.Int, nTipoResultadoID.intValue()),
				new ProcedureParameters("@EmailEspecifico", SqlDbType.Bit, bEspecifico.booleanValue() ? 1 : 0),
				new ProcedureParameters("@OK", SqlDbType.Bit, nOK.booleanValue() ? 1 : 0, ParameterDirection.InputOutput),
				new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, ParameterDirection.InputOutput, 8000)
			};

			if(nTipoResultadoID.intValue() != 2) {
				DataInterfaceObj.execNonQueryProcedure("sp_Email_Marketing", param);
			} else {
				dsEmailMarketing = DataInterfaceObj.execQueryProcedure("sp_Email_Marketing", param);
			}

			Resultado = param[19].Data.ToString();
			nOK = new java.lang.Boolean(int.Parse(param[18].Data.ToString()) != 0);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			EmailMarketing();

			string sql = "";

			if (nTipoResultadoID.intValue() != 2)
			{
				if(Resultado == null) Resultado = "E-mails enviados com sucesso";

				if (nTipoResultadoID.intValue() != 4)
				{
					sql += "select NULL as nOK, '" + Resultado + "' as fldResponse";
				}
				else
				{
					sql += "select " + (nOK.booleanValue() ? 1 : 0) + " as nOK, '" + Resultado + "' as fldResponse";
				}

				dsEmailMarketing = DataInterfaceObj.getRemoteData(sql);
			}

			WriteResultXML(dsEmailMarketing);
		}
	}
}
