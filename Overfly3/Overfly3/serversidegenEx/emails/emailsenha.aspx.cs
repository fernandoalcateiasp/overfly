using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.serversidegenEx.emails
{
	public partial class emailsenha : System.Web.UI.OverflyPage
	{
		private Integer fromID = Constants.INT_ZERO;
		protected Integer nFromID
		{
			get { return fromID; }
			set { fromID = value; }
		}

		private Integer contatoID = Constants.INT_ZERO;
		protected Integer nContatoID
		{
			get { return contatoID; }
			set { contatoID = value; }
		}

		private Integer relPesContatoID = Constants.INT_ZERO;
		protected Integer nRelPesContatoID
		{
			get { return relPesContatoID; }
			set { relPesContatoID = value; }
		}

		private java.lang.Boolean changeSenha = Constants.FALSE;
		protected java.lang.Boolean bChangeSenha
		{
			get { return changeSenha; }
			set { changeSenha = value; }
		}

		private string resultado1 = Constants.STR_EMPTY;
		protected string sResultado1
		{
			get { return resultado1; }
			set { resultado1 = value; }
		}

		private string resultado = Constants.STR_EMPTY;
		protected string sResultado
		{
			get { return resultado; }
			set { resultado = value; }
		}

        private Integer pesUrlID = Constants.INT_ZERO;
        protected Integer nPesURLID
        {
            get { return pesUrlID; }
            set { pesUrlID = value; }
        }

		protected void SenhaInsert()
		{
			ProcedureParameters[] param = new ProcedureParameters[] {
				new ProcedureParameters("@EmailLogin", SqlDbType.VarChar, nContatoID, 80),
				new ProcedureParameters("@Senha", SqlDbType.VarChar, DBNull.Value, 20),
				new ProcedureParameters("@GerarSenha", SqlDbType.Bit, 1),
				new ProcedureParameters("@ForcarTroca", SqlDbType.Bit, 1),
                new ProcedureParameters("@CaminhoFTP",System.Data.SqlDbType.VarChar,DBNull.Value,800),
				new ProcedureParameters("@Tipo", SqlDbType.Int, 2),
                new ProcedureParameters("@PesUrlID", SqlDbType.VarChar, nPesURLID, 20),
				new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, 8000)
			};

			DataInterfaceObj.execNonQueryProcedure("sp_Senha_Insert", param);

			sResultado1 = param[5].Data.ToString();
		}

		protected void EmailConfirmacaoSenha()
		{
			ProcedureParameters[] param = new ProcedureParameters[] {
				new ProcedureParameters("@RelPesContatoID", SqlDbType.Int, nRelPesContatoID),
				new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, 100)
			};

			DataInterfaceObj.execNonQueryProcedure("sp_Email_ConfirmacaoSenha", param);

			sResultado = param[1].Data.ToString();

            
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			string msg = "";

            if (changeSenha.booleanValue() == true)
            {
                SenhaInsert();
            }

			EmailConfirmacaoSenha();

			if(sResultado == null || sResultado == "") {
				if (bChangeSenha.booleanValue()) {
					msg = "Senha alterada e enviada para o e-mail do contato.";
				} else {
					msg = "Senha enviada para o e-mail do contato.";
				}
			} else {
				msg = sResultado;
			}

			WriteResultXML(DataInterfaceObj.getRemoteData(
				"select '" + msg + "' as fldResponse"
			));
		}
	}
}
