using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.serversidegenEx.emails
{
	public partial class emailquestionario : System.Web.UI.OverflyPage
	{
		private string procedure;
		private string alert;

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DS);
		}

		protected DataSet DS
		{
			get
			{
				return DataInterfaceObj.getRemoteData(
						"select '" + FldResponse + "' as fldResponse"
					);
			}
		}

		protected string fldResponse = null;
		protected string FldResponse
		{
			get
			{
				if (fldResponse == null)
				{
					ProcedureParameters[] param;

                    if (tipo.intValue() == 2)
                    {
                        param = new ProcedureParameters[] 
					    {
						    new ProcedureParameters("@EmpresaID", SqlDbType.Int, empresaID.intValue()),
						    new ProcedureParameters("@ParceiroID", SqlDbType.Int, parceiroID.intValue()),
						    new ProcedureParameters("@ContatoID", SqlDbType.Int, contatoID.intValue()),
						    new ProcedureParameters("@EMail", SqlDbType.VarChar, eMail),
						    new ProcedureParameters("@Resultado", SqlDbType.VarChar, eMail, ParameterDirection.InputOutput, 100)
					    };
                    }
                    else
                    {
                        param = new ProcedureParameters[] 
					    {
						    new ProcedureParameters("@EmpresaID", SqlDbType.Int, empresaID.intValue()),
						    new ProcedureParameters("@ParceiroID", SqlDbType.Int, parceiroID.intValue()),
						    new ProcedureParameters("@ContatoID", SqlDbType.Int, contatoID.intValue()),
						    new ProcedureParameters("@Resultado", SqlDbType.VarChar, eMail, ParameterDirection.InputOutput, 100)
					    };
                    }

                        DataInterfaceObj.execNonQueryProcedure(procedure, param);

					fldResponse = (string)param[3].Data;
				}

				return (fldResponse == null || fldResponse == "null" || fldResponse == "") ? alert : fldResponse;
			}
		}
		
		protected Integer empresaID = Constants.INT_ZERO;
		protected Integer nEmpresaID
		{
			get { return empresaID; }
			set { if (value != null) empresaID = value; }
		}

		protected Integer parceiroID = Constants.INT_ZERO;
		protected Integer nParceiroID
		{
			get { return parceiroID; }
			set { if (value != null) parceiroID = value; }
		}

		protected Integer contatoID = Constants.INT_ZERO;
		protected Integer nContatoID
		{
			get { return contatoID; }
			set { if (value != null) contatoID = value; }
		}

		protected Integer tipo = Constants.INT_ZERO;
		protected Integer nTipo
		{
			get { return tipo; }
			set
			{
				if (value != null) tipo = value;
				
				if(tipo.intValue() == 0)
				{
					procedure = "sp_Email_PSC";
					alert = "PSC enviada para o e-mail do contato.";
				}
				else if(tipo.intValue() == 1)
				{
					procedure = "sp_Email_QAF";
					alert = "QAF enviado para o e-mail do contato.";
				}
				else if(tipo.intValue() == 2)
				{
					procedure = "sp_Email_RRC";
					alert = "RRC enviado para o e-mail do contato.";
				}
			}
		}

		protected string eMail = Constants.STR_EMPTY;
		protected string sEMail
		{
			get { return eMail; }
			set { if (value != null) eMail = value; }
		}
	}
}
