using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx.emails
{
    public partial class sendemail : System.Web.UI.OverflyPage
    {
        protected Integer mailtoSendID;
        protected Integer nMailtoSendID
        {
            get { return mailtoSendID; }
            set { mailtoSendID = value; }
        }

        protected java.lang.Boolean html;
        protected java.lang.Boolean bHTML
        {
            get { return html; }
            set { html = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string mailTo = "";
            string mailFrom = "";
            string mailCc = "";
            string mailBcc = "";
            int TipoEmail = 0;
            string mailSubject = "";
            string sMailBody = "";
            string mensagem = "";

            OVERFLYSVRCFGLib.OverflyMTS oOverflySvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();
            string smtpServer = oOverflySvrCfg.EmailServerIP(DataInterfaceObj.ApplicationName);

            DataSet eMailsTemp = DataInterfaceObj.getRemoteData(
                "SELECT *, ISNULL(TipoEmail,0) AS TipoEmailID FROM EMailsTemp WITH(NOLOCK) WHERE EMailID = " + mailtoSendID
            );

            DataSet result = null;

            if (eMailsTemp.Tables[1].Rows.Count > 0)
            {
                mailTo = (string)eMailsTemp.Tables[1].Rows[0]["MailTo"];
                mailFrom = (string)eMailsTemp.Tables[1].Rows[0]["MailFrom"];
                mailCc = (string)eMailsTemp.Tables[1].Rows[0]["MailCc"];
                mailBcc = (string)eMailsTemp.Tables[1].Rows[0]["MailBcc"];
                mailSubject = (string)eMailsTemp.Tables[1].Rows[0]["Subject"];
                sMailBody = (string)eMailsTemp.Tables[1].Rows[0]["MailBody"];
                TipoEmail = (int)eMailsTemp.Tables[1].Rows[0]["TipoEmailID"];

                System.Net.Mail.MailMessage message = new System.Net.Mail.MailMessage();

                if (bHTML.booleanValue())
                {
                    sMailBody.Replace(
                        new string(new char[] { (char)13, (char)10 }),
                        "<BR>");
                    sMailBody.Replace(
                        new string(new char[] { (char)32 }),
                        "&nbsp");

                    sMailBody = "<HTML><BODY><P><FONT FACE='Courier New' SIZE='2'>" + sMailBody + "</FONT></P></BODY></HTML>";
                }
                /*
                message.From = new System.Net.Mail.MailAddress(mailFrom);
                message.To.Add(FormatMultipleEmailAddresses(mailTo));
                if (mailCc.Length > 0) message.CC.Add(mailCc);
                if (mailBcc.Length > 0) message.Bcc.Add(mailBcc);
                message.Subject = mailSubject;
                message.IsBodyHtml = bHTML.booleanValue();
                message.Body = sMailBody;
                message.BodyEncoding = System.Text.Encoding.UTF8;
                System.Net.Mail.SmtpClient smtp = new System.Net.Mail.SmtpClient(smtpServer);
                smtp.Send(message);*/
                try
                {
                    ProcedureParameters[] param = new ProcedureParameters[] 
                    {
				        new ProcedureParameters("@From", SqlDbType.VarChar, mailFrom,8000),
				        new ProcedureParameters("@To", SqlDbType.VarChar, mailTo, 8000),
				        new ProcedureParameters("@Cc", SqlDbType.VarChar, mailCc, 8000),
				        new ProcedureParameters("@BCc", SqlDbType.VarChar, mailBcc, 8000),
				        new ProcedureParameters("@Subject", SqlDbType.VarChar, mailSubject, 8000),
				        new ProcedureParameters("@BodyFormat", SqlDbType.Int, 0),
				        new ProcedureParameters("@MailFormat", SqlDbType.Int, 0),
				        new ProcedureParameters("@Importance", SqlDbType.Int, 1),
				        new ProcedureParameters("@Body", SqlDbType.VarChar,sMailBody.ToString()),
				        new ProcedureParameters("@FullPath", SqlDbType.VarChar, DBNull.Value),
				        new ProcedureParameters("@Arquivo", SqlDbType.VarChar, DBNull.Value),
				        new ProcedureParameters("@EmailID", SqlDbType.Int, 0),
				        new ProcedureParameters("@TipoEmail", SqlDbType.Int, TipoEmail),
				        new ProcedureParameters("@UsuarioID", SqlDbType.VarChar, 999),
				        new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, ParameterDirection.InputOutput, 8000)
                    };
                    DataInterfaceObj.execNonQueryProcedure("sp_Email_Gerador", param);
                    if (param[14].Data != DBNull.Value)
                    {
                        mensagem = param[14].Data.ToString();
                    }
                }
                /*
            // Monta o resultado;
            result = DataInterfaceObj.getRemoteData(
                "SELECT 'E-Mail enviado com sucesso.' AS Msg "
            );
        }
        else
        {
            // Monta o resultado;
            result = DataInterfaceObj.getRemoteData(
                "SELECT 'N�o foi poss�vel enviar o e-mail.' AS Msg "
            );
        }*/
                catch (System.Exception exception)
                {

                    //erros += "#ERROR#OrflyPage (" + this.GetType().Name + "):" + exception.Message + " \r\n ";
                    mensagem += exception.Message;
                }
                WriteResultXML(DataInterfaceObj.getRemoteData("SELECT '" + mensagem + "' AS Msg"));
            }
        }
        private string FormatMultipleEmailAddresses(string emailAddresses)
        {
            var delimiters = new[] { ',', ';' };

            var addresses = emailAddresses.Split(delimiters, StringSplitOptions.RemoveEmptyEntries);

            return string.Join(",", addresses);


        }
    }
}