using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class pesqrelacoes : System.Web.UI.OverflyPage
    {
        private Integer formId;
        private string comboId;
        private Integer empresaId;
        private string quemÉNaRel;
        private Integer tipoRelacaoId;
        private Integer regExcluidoId;
        private string toFind;
        private string key;

        public Integer nFormID
        {
            set { formId = value; }
        }
        public string sComboID
        {
            set { comboId = value; }
        }
        public Integer nEmpresaID
        {
            set { empresaId = value; }
        }
        public string sQuemENaRel
        {
            set { quemÉNaRel = value; }
        }
        public Integer nTipoRelacaoID
        {
            set { tipoRelacaoId = value; }
        }
        public Integer nRegExcluidoID
        {
            set { regExcluidoId = value; }
        }
        public string strToFind
        {
            set { toFind = value; }
        }
        public string nKey
        {
            set { key = value; }
        }

        public string Operator
        {
            get
            {
                if (toFind.Length > 1 && toFind.StartsWith("'%") || toFind.EndsWith("%'"))
                    return " LIKE ";
                else
                    return " = ";
            }
        }

        protected string Sql
        {
            get
            {
                string sql = "";
                string programaMarketing = "";



                switch (formId.intValue())
                {
                    case 1220:   //Relacao Entre Pessoas 

                        //Sujeito
                        if (quemÉNaRel.Equals("SUJ"))
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                     "a.Fantasia AS fldName, a.Sexo " +
                                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " +
                                     "AND b.TipoRelacaoID=" + tipoRelacaoId + " " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }
                        //Clientelia Asstec
                        else if ((quemÉNaRel.Equals("OBJ") && (tipoRelacaoId.intValue() == 22)))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                     "a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK) " +
                                            "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoObjetoID " +
                                            "INNER JOIN Pessoas c WITH(NOLOCK) ON c.TipoPessoaID = b.TipoSujeitoID " +
                                            "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.ClassificacaoID = d.ItemID " +
                                     "WHERE a.EstadoID = 2 " +
                                     "AND b.TipoRelacaoID=" + tipoRelacaoId + " " +
                                     "AND c.PessoaID = " + regExcluidoId + " " +
                                     "AND d.Filtro LIKE '%<ASS>%' " +
                                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " +
                                     "WHERE a.SujeitoID = " + regExcluidoId + " " +
                                     "AND a.TipoRelacaoID =" + tipoRelacaoId + ") " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";

                            //Clientelia Transporte
                        }
                        else if ((quemÉNaRel.Equals("OBJ") && (tipoRelacaoId.intValue() == 23)))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, " +
                                     "a.PessoaID AS fldID, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK) " +
                                            "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoObjetoID " +
                                            "INNER JOIN Pessoas c WITH(NOLOCK) ON c.TipoPessoaID = b.TipoSujeitoID " +
                                            "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.ClassificacaoID = d.ItemID " +
                                     "WHERE a.EstadoID = 2 " +
                                     "AND b.TipoRelacaoID=" + tipoRelacaoId + " " +
                                     "AND c.PessoaID = " + regExcluidoId + " " +
                                     "AND d.Filtro LIKE '%<TRA>%' " +
                                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " +
                                     "WHERE a.SujeitoID = " + regExcluidoId + " " +
                                     "AND a.TipoRelacaoID =" + tipoRelacaoId + ") " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }
                        //Clientelia Banco
                        else if ((quemÉNaRel.Equals("OBJ") && (tipoRelacaoId.intValue() == 24)))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, " +
                                     "a.PessoaID AS fldID, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK) " +
                                            "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoObjetoID " +
                                            "INNER JOIN Pessoas c WITH(NOLOCK) ON c.TipoPessoaID = b.TipoSujeitoID " +
                                            "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.ClassificacaoID = d.ItemID " +
                                     "WHERE a.EstadoID = 2 " +
                                     "AND b.TipoRelacaoID=" + tipoRelacaoId + " " +
                                     "AND c.PessoaID = " + regExcluidoId + " " +
                                     "AND d.Filtro LIKE '%<BAN>%' " +
                                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " +
                                     "WHERE a.SujeitoID = " + regExcluidoId + " " +
                                     "AND a.TipoRelacaoID =" + tipoRelacaoId + ") " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }
                        //Clientelia Serviços
                        else if ((quemÉNaRel.Equals("OBJ") && (tipoRelacaoId.intValue() == 25)))
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, " +
                                     "a.PessoaID AS fldID, a.Fantasia AS fldName, a.Observacao AS Servico " +
                                     "FROM Pessoas a WITH(NOLOCK) " +
                                        "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoObjetoID " +
                                        "INNER JOIN Pessoas c WITH(NOLOCK) ON c.TipoPessoaID = b.TipoSujeitoID " +
                                        "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.ClassificacaoID = d.ItemID " +
                                     "WHERE a.EstadoID = 2 " +
                                     "AND b.TipoRelacaoID=" + tipoRelacaoId + " " +
                                     "AND c.PessoaID = " + regExcluidoId + " " +
                                     "AND d.Filtro LIKE '%<PS>%' " +
                                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " +
                                     "WHERE a.SujeitoID = " + regExcluidoId + " " +
                                     "AND a.TipoRelacaoID =" + tipoRelacaoId + ") " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }

                        //Funcional
                        else if (quemÉNaRel.Equals("OBJ") && tipoRelacaoId.intValue() == 31)
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                                     "WHERE a.EstadoID = 2 AND a.TipoPessoaID = b.TipoObjetoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND c.PessoaID = " + (regExcluidoId) + " " +
                                     "AND c.TipoPessoaID = b.TipoSujeitoID " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }

                        //Outras Relacoes
                        else if (quemÉNaRel.Equals("OBJ") && tipoRelacaoId.intValue() != 22 && tipoRelacaoId.intValue() != 23
                                  && tipoRelacaoId.intValue() != 24 && tipoRelacaoId.intValue() != 25)
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                                     "WHERE a.EstadoID = 2 AND a.TipoPessoaID = b.TipoObjetoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND c.PessoaID = " + (regExcluidoId) + " " +
                                     "AND c.TipoPessoaID = b.TipoSujeitoID " +
                                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " +
                                     "WHERE a.SujeitoID = " + (regExcluidoId) + " " +
                                     "AND a.TipoRelacaoID =" + (tipoRelacaoId) + ") " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }
                        //Filiador de Programa de Marketing 
                        else if (comboId.Equals("selFiliadorID_07"))
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = 52 " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }

                        break;

                    case 5240:   //Visitas

                        if ((quemÉNaRel.Equals("PESSOA") && (comboId.Equals("selPessoaID"))))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                     "a.Fantasia AS fldName, a.Sexo " +
                                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";

                        }
                        else if ((quemÉNaRel.Equals("PROSPECT") && (comboId.Equals("selProspectID"))))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.ProspectID AS fldID, " +
                                     "a.Fantasia AS fldName, '' AS Sexo " +
                                     "FROM Prospect a WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";

                        }

                        break;

                    case 2120:   //Relacoes entre Conceitos

                        //Sujeito
                        if (quemÉNaRel.Equals("SUJ"))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.ConceitoID AS fldID, a.Conceito AS fldName " +
                                     "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND a.TipoConceitoID = b.TipoSujeitoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND a.Conceito " + Operator + " " + toFind + " " +
                                     "ORDER BY fldName";
                        }
                        //Objeto
                        else if (quemÉNaRel.Equals("OBJ"))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.ConceitoID AS fldID, a.Conceito AS fldName " +
                                     "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " +
                                     "WHERE a.EstadoID = 2 AND a.TipoConceitoID = b.TipoObjetoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND c.ConceitoID = " + (regExcluidoId) + " " +
                                     "AND c.TipoConceitoID = b.TipoSujeitoID " +
                                     "AND a.ConceitoID NOT IN (SELECT a.ObjetoID FROM RelacoesConceitos a WITH(NOLOCK) " +
                                     "WHERE a.SujeitoID = " + (regExcluidoId) + " " +
                                     "AND a.TipoRelacaoID =" + (tipoRelacaoId) + ") " +
                                     "AND a.Conceito " + Operator + " " + toFind + " " +
                                     "ORDER BY fldName";

                        }

                        break;

                    case 1120:   // Relacoes entre Recursos

                        // Sujeito
                        if (quemÉNaRel.Equals("SUJ"))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.RecursoID AS fldID, a.RecursoFantasia AS fldName " +
                                     "FROM Recursos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND a.TipoRecursoID = b.TipoSujeitoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND a.RecursoFantasia " + Operator + " " + toFind + " " +
                                     "ORDER BY fldName";
                        }
                        // Objeto
                        else if (quemÉNaRel.Equals("OBJ"))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.RecursoID AS fldID, a.RecursoFantasia AS fldName " +
                                     "FROM Recursos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK) " +
                                     "WHERE a.EstadoID = 2 AND a.TipoRecursoID = b.TipoObjetoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND c.RecursoID = " + (regExcluidoId) + " " +
                                     "AND c.TipoRecursoID = b.TipoSujeitoID " +
                                     "AND a.RecursoID NOT IN (SELECT a.ObjetoID FROM RelacoesRecursos a WITH(NOLOCK) " +
                                     "WHERE a.SujeitoID = " + (regExcluidoId) + " " +
                                     "AND a.TipoRelacaoID =" + (tipoRelacaoId) + ") " +
                                     "AND a.RecursoFantasia " + Operator + " " + toFind + " " +
                                     "ORDER BY fldName";
                        }


                        break;

                    case 2130: // Relacoes entre Pessoas e Conceitos

                        // Produto
                        if (quemÉNaRel.Equals("SUJ") && tipoRelacaoId.intValue() == 61)
                        {

                            sql = "SELECT DISTINCT TOP 100 a.PessoaID AS fldID, a.Nome AS fldCompleteName, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "AND a.PessoaID = " + (empresaId) + " " +
                                     "ORDER BY fldCompleteName";


                        }
                        // Fabricante    
                        else if (quemÉNaRel.Equals("SUJ") && tipoRelacaoId.intValue() == 62)
                        {

                            sql = "SELECT DISTINCT TOP 100 a.PessoaID AS fldID, a.Nome AS fldCompleteName, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK) " +
                                        "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoSujeitoID " +
                                        "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON a.ClassificacaoID = c.ItemID " +
                                     "WHERE a.EstadoID=2 " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND c.Filtro LIKE '%<FAB>%'  AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";


                        }
                        // Marca
                        else if (quemÉNaRel.Equals("SUJ") && tipoRelacaoId.intValue() == 63)
                        {

                            sql = "SELECT DISTINCT TOP 100 a.PessoaID AS fldID, a.Nome AS fldCompleteName, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";


                        }
                        // Objeto
                        else if (quemÉNaRel.Equals("OBJ"))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.ConceitoID AS fldID, a.Conceito AS fldName " +
                                     "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                                     "WHERE a.EstadoID = 2 AND a.TipoConceitoID = b.TipoObjetoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND c.PessoaID = " + (regExcluidoId) + " " +
                                     "AND c.TipoPessoaID = b.TipoSujeitoID " +
                                     "AND a.ConceitoID NOT IN (SELECT a.ObjetoID FROM RelacoesPesCon a WITH(NOLOCK) " +
                                     "WHERE a.SujeitoID = " + (regExcluidoId) + " " +
                                     "AND a.TipoRelacaoID =" + (tipoRelacaoId) + ") " +
                                     "AND a.Conceito " + Operator + " " + toFind + " " +
                                     "ORDER BY fldName";
                        }

                        break;

                    case 1130: // Relacoes entre Pessoas e Recursos

                        // Sujeito
                        if (quemÉNaRel.Equals("SUJ"))
                        {

                            sql = "SELECT DISTINCT TOP 100 a.PessoaID AS fldID, a.Nome AS fldCompleteName, a.Fantasia AS fldName " +
                                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " +
                                     "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }

                        break;

                    case 5110: // Pedidos

                        if (key.Equals("1"))
                        {
                            key = "a.PessoaID";

                            if (toFind.Equals("''"))
                            {
                                toFind = "0";
                            }

                        }
                        else if (key.Equals("2"))
                        {
                            key = "a.Nome";
                        }
                        else if (key.Equals("3"))
                        {
                            key = "a.Fantasia";
                        }

                        sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " +
                                 "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 101, 0) AS Documento, f.Localidade " +
                                 "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " +
                                 "FROM Pessoas a WITH(NOLOCK) " +
                                 "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
                                 "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
                                 "WHERE a.PessoaID IN (319040) " +
                                 "ORDER BY " + key;

                        break;

                    case 5150: // DPA

                        // selFornecedorID
                        if (quemÉNaRel.Equals("OBJ"))
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " +
                                     "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 101, 0) AS Documento, f.Localidade " +
                                     "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " +
                                     "FROM RelacoesPessoas b WITH(NOLOCK), Pessoas a WITH(NOLOCK) " +
                                     "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
                                     "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
                                     "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
                                     "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
                                     "WHERE a.EstadoID = 2 AND a.PessoaID=b.ObjetoID AND b.EstadoID=2 AND dbo.fn_Empresa_Sistema(b.SujeitoID) = 1 AND " +
                                     "a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }
                        // selClienteID
                        else
                        {
                            if (regExcluidoId.intValue() > 0)
                            {
                                programaMarketing = ", dbo.fn_ProgramaMarketing_Identificador(" + (regExcluidoId) + ", a.PessoaID) AS Identificador ";
                            }
                            else
                            {
                                programaMarketing = ", NULL AS Identificador ";
                            }

                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " +
                                     "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 101, 0) AS Documento, f.Localidade " +
                                     "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " + programaMarketing + " " +
                                     "FROM RelacoesPessoas b WITH(NOLOCK), Pessoas a WITH(NOLOCK) " +
                                     "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
                                     "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
                                     "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
                                     "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
                                     "WHERE a.EstadoID = 2 AND a.PessoaID=b.SujeitoID AND b.EstadoID=2 AND dbo.fn_Empresa_Sistema(b.ObjetoID) = 1 AND " +
                                     "a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }

                        break;

                    case 5160: // Campanhas
                        // selFornecedorID
                        if (quemÉNaRel.Equals("OBJ"))
                        {
                            sql = "SELECT fldCompleteName, fldID, fldName, dbo.fn_Pessoa_Documento(fldID, NULL, NULL, 111, 101, 0) AS Documento, " +
                                    "Cidade, UF, Pais " +
                                "FROM (SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " +
                                            "f.Localidade AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " +
                                        "FROM Pessoas a WITH(NOLOCK) " +
                                            "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON a.PessoaID = b.ObjetoID " +
                                            "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
                                            "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
                                            "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
                                            "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
                                            "INNER JOIN (SELECT SujeitoID " +
                                                            "FROM RelacoesPesRec WITH(NOLOCK) " +
                                                            "WHERE TipoRelacaoID = 12 AND EstadoID = 2) EmpSis ON EmpSis.SujeitoID = b.SujeitoID " +
                                        "WHERE b.TipoRelacaoID = 21 " +
                                                "AND a.EstadoID = 2 " +
                                                "AND b.EstadoID = 2 " +
                                                "AND a.Nome " + Operator + " " + toFind + " " +
                                        "ORDER BY fldCompleteName) Pessoas ";

                        }
                        // selClienteID    
                        else
                        {
                            if (regExcluidoId.intValue() > 0)
                            {
                                programaMarketing = ", dbo.fn_ProgramaMarketing_Identificador(" + (regExcluidoId) + ", a.PessoaID) AS Identificador ";
                            }
                            else
                            {
                                programaMarketing = ", NULL AS Identificador ";
                            }

                            sql = "SELECT fldCompleteName, fldID, fldName, dbo.fn_Pessoa_Documento(fldID, NULL, NULL, 111, 101, 0) AS Documento, " +
                                    "Cidade, UF, Pais, Identificador " +
                                "FROM (SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " +
                                            "f.Localidade AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais  " + programaMarketing + " " +
                                        "FROM Pessoas a WITH(NOLOCK) " +
                                            "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON a.PessoaID = b.SujeitoID " +
                                            "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
                                            "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
                                            "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
                                            "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
                                            "INNER JOIN (SELECT SujeitoID " +
                                                            "FROM RelacoesPesRec WITH(NOLOCK) " +
                                                            "WHERE TipoRelacaoID = 12 AND EstadoID = 2) EmpSis ON EmpSis.SujeitoID = b.ObjetoID " +
                                        "WHERE b.TipoRelacaoID = 21 " +
                                                "AND a.EstadoID = 2 " +
                                                "AND b.EstadoID = 2 " +
                                                "AND a.Nome " + Operator + " " + toFind + " " +
                                        "ORDER BY fldCompleteName) Pessoas ";
                        }


                        break;

                    case 5310:
                        sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                 "a.Fantasia AS fldName " +
                                 "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                 "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " +
                                 "AND b.TipoRelacaoID=" + (tipoRelacaoId) + " " +
                                 "AND a.Nome " + Operator + " " + toFind + " " +
                                 "ORDER BY fldCompleteName";

                        break;

                    case 5410:
                        sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                 "a.Fantasia AS fldName " +
                                 "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " +
                                 "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " +
                                 "AND b.TipoRelacaoID = " + (tipoRelacaoId) + " " +
                                 "AND a.Nome " + Operator + " " + toFind + " " +
                                 "ORDER BY fldCompleteName";

                        break;


                    case 7140: // RRC ou PSC
                    case 7150:

                        // selClienteID
                        if (regExcluidoId.intValue() == 0)
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                     "a.Fantasia AS fldName, " +
                                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " +
                                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " +
                                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " +
                                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " +
                                     "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND b.EstadoID=2 AND a.PessoaID=b.SujeitoID AND b.ObjetoID = " + (empresaId) + " AND " +
                                     "a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }
                        // selContatoID
                        else
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                     "a.Fantasia AS fldName, " +
                                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " +
                                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " +
                                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " +
                                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " +
                                     "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), RelacoesPessoas_Contatos c WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND b.EstadoID=2 AND a.PessoaID=c.ContatoID AND b.SujeitoID = " + (regExcluidoId) + " AND " +
                                     "b.ObjetoID = " + empresaId + " AND c.RelacaoID=b.RelacaoID " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }


                        break;

                    // QAF ou Competencias
                    case 7180:
                    case 12210:

                        // selFornecedorID
                        if (regExcluidoId.intValue() == 0)
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                     "a.Fantasia AS fldName, " +
                                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " +
                                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " +
                                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " +
                                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " +
                                     "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND b.EstadoID=2 AND a.PessoaID=b.ObjetoID AND b.SujeitoID = " + (empresaId) + " AND " +
                                     "a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }
                        // Prest de Servico
                        else if (regExcluidoId.Equals(-1))
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                     "a.Fantasia AS fldName, " +
                                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " +
                                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " +
                                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " +
                                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " +
                                     "FROM Pessoas a WITH(NOLOCK) " +
                                        "INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.ClassificacaoID = b.ItemID " +
                                     "WHERE a.EstadoID=2 AND a.TipoPessoaID IN (51,52) AND b.Filtro LIKE '%<PS>%'  AND " +
                                     "a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }
                        // selContatoID
                        else
                        {
                            sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " +
                                     "a.Fantasia AS fldName, " +
                                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " +
                                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " +
                                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " +
                                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " +
                                     "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), RelacoesPessoas_Contatos c WITH(NOLOCK) " +
                                     "WHERE a.EstadoID=2 AND b.EstadoID=2 AND a.PessoaID=c.ContatoID AND b.ObjetoID = " + (regExcluidoId) + " AND " +
                                     "b.SujeitoID = " + (empresaId) + " AND c.RelacaoID=b.RelacaoID " +
                                     "AND a.Nome " + Operator + " " + toFind + " " +
                                     "ORDER BY fldCompleteName";
                        }

                        break;

                    case 9130: // Valores a localizar

                        if (key.Equals("1"))
                        {
                            key = "a.PessoaID";

                            if (toFind.Equals("''"))
                            {
                                toFind = "0";
                            }

                        }
                        else if (key.Equals("2"))
                        {
                            key = "a.Nome";
                        }
                        else if (key.Equals("3"))
                        {
                            key = "a.Fantasia";
                        }

                        sql = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " +
                                 "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 101, 0) AS Documento, f.Localidade " +
                                 "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " +
                                 "FROM Pessoas a WITH(NOLOCK) " +
                                 "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
                                 "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
                                 "WHERE a.EstadoID = 2 AND " + key + " " + Operator + " " + toFind + " " +
                                 "ORDER BY " + key;

                        break;

                    default:
                        break;
                }

                return sql;
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            // Gera o resultado para o usuario.
            WriteResultXML(DataInterfaceObj.getRemoteData(Sql));
        }
    }
}
