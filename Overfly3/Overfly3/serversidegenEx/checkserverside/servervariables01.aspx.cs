using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

namespace Overfly3.serversidegenEx.checkserverside
{
	public partial class servervariables01 : System.Web.UI.OverflyPage
	{
		protected override void PageLoad(object sender, EventArgs e)
		{
			Response.Cache.SetNoStore();
			Response.Cache.SetCacheability(HttpCacheability.NoCache);
		}
	}
}
