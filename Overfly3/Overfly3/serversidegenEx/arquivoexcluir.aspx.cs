﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.serversidegenEx
{
    public partial class arquivoexcluir: System.Web.UI.OverflyPage
    {
        private string ArquivoID;
        protected string sArquivoID
        {
            get { return ArquivoID; }
            set { ArquivoID = value; }
        }           

        protected override void PageLoad(object sender, EventArgs e)
        {

            string sql = "DELETE FROM [OVERFLYBASE_EXT2].Overfly_Documentos.dbo.Documentos WHERE DocumentoID IN (" + sArquivoID + ")";
            // Executa o pacote
            int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(sql);

             WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + rowsAffected + "' as nLinhas" 
            ));
        }
    }
}
