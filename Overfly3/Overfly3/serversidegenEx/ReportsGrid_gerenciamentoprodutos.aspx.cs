﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_gerenciamentoprodutos : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        private int RelatorioID = Convert.ToInt32(HttpContext.Current.Request.Params["RelatorioID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        private bool bToExcel = Convert.ToBoolean(HttpContext.Current.Request.Params["bToExcel"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string glb_sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);

        int Datateste = 0;
        bool nulo = false;

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "1":
                        relatorioGerenciamentoProdutos();
                        break;
                }
            }
        }

        public void relatorioGerenciamentoProdutos()
        {
            //Parametros da procedure
            string selFornecedor = Convert.ToString(HttpContext.Current.Request.Params["selFornecedor"]);
            string selServico = Convert.ToString(HttpContext.Current.Request.Params["selServico"]);
            string selFiltroEspecifico = Convert.ToString(HttpContext.Current.Request.Params["selFiltroEspecifico"]);
            bool chkEnc = Convert.ToBoolean(HttpContext.Current.Request.Params["chkEnc"]);
            string selFiltroEspecificoFiltro = Convert.ToString(HttpContext.Current.Request.Params["selFiltroEspecificoFiltro"]);
            string selFamilia = Convert.ToString(HttpContext.Current.Request.Params["selFamilia"]);
            string selMarca = Convert.ToString(HttpContext.Current.Request.Params["selMarca"]);
            string selLinha = Convert.ToString(HttpContext.Current.Request.Params["selLinha"]);
            int glb_nMoedaFaturamentoID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nMoedaFaturamentoID"]);
            string glb_sReadOnlyClassFiscal = Convert.ToString(HttpContext.Current.Request.Params["glb_sReadOnlyClassFiscal"]);
            bool glb_Excel = Convert.ToBoolean(HttpContext.Current.Request.Params["glb_Excel"]);
            bool glb_Inicio = Convert.ToBoolean(HttpContext.Current.Request.Params["glb_Inicio"]);
            int glb_nRegistros = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nRegistros"]);
            string glb_sFiltroEspecifico = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltroEspecifico"]);
            string sAGrid = Convert.ToString(HttpContext.Current.Request.Params["sAGrid"]);
            string sPercentualPV = Convert.ToString(HttpContext.Current.Request.Params["sPercentualPV"]);
            string glb_sChavePesquisa = Convert.ToString(HttpContext.Current.Request.Params["glb_sChavePesquisa"]);
            string glb_sOrdem = Convert.ToString(HttpContext.Current.Request.Params["glb_sOrdem"]);
            string selTransacao = Convert.ToString(HttpContext.Current.Request.Params["selTransacao"]);
            string selProjeto = Convert.ToString(HttpContext.Current.Request.Params["selProjeto"]);
            string sFiltroLotesItens = Convert.ToString(HttpContext.Current.Request.Params["sFiltroLotesItens"]);
            string glb_sFiltroContexto = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltroContexto"]);
            string glb_sPesquisa = Convert.ToString(HttpContext.Current.Request.Params["glb_sPesquisa"]);
            string glb_sFiltro = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltro"]);
            string glb_sFiltroDireitos = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltroDireitos"]);
            string glb_sFiltroFamilia = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltroFamilia"]);
            string glb_sFiltroProprietario = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltroProprietario"]);
            int glb_aEmpresaData0 = Convert.ToInt32(HttpContext.Current.Request.Params["glb_aEmpresaData0"]);
            string selFiltroArgumentosVendaWhere = Convert.ToString(HttpContext.Current.Request.Params["selFiltroArgumentosVendaWhere"]);

            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            int DATE_SQL_PARAM = 103;

            if (sLinguaLogada != 246) {
                param = "MM/dd/yyy";
                DATE_SQL_PARAM = 101;
            }

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            param = "<Format>" + param + "</Format>";

            string Title = "Posição de produtos - " + glb_sEmpresaFantasia + " - " + _data;

            string strSQL = "";
            string sFiltroFornecedor = "";
            string sFiltroComprar = "";
            string sCustoComprar = "";
            string sFiltroData = "";
            string nWhereTransacao = "";
            string glb_sFiltroMarca = "";
            string glb_sFiltroLinha = "";

            if (selFornecedor != "0" && selFornecedor != "")
                sFiltroFornecedor = " AND (SELECT COUNT(*) FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID AND FornecedorID = " + selFornecedor + ")) > 0";
            
            // Servico 5 Comprar
            if (selServico == "GERENCIA_DE_ESTOQUE")
                sFiltroComprar = " AND a.PrevisaoVendas >= 0 ";

            if ((selFiltroEspecifico != "0" && selFiltroEspecifico != "") && (!chkEnc))
                glb_sFiltroEspecifico = " AND (" + selFiltroEspecificoFiltro + ")";
            else
                glb_sFiltroEspecifico = "";

            if (selFamilia != "0" && selFamilia != "")
                glb_sFiltroFamilia = " AND ((SELECT aa.ProdutoID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = " + selFamilia + ")";
            else
                glb_sFiltroFamilia = "";

            if (selMarca != "0" && selMarca != "")
                glb_sFiltroMarca = " AND ((SELECT aa.MarcaID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = " + selMarca + ")";
            else
                glb_sFiltroMarca = "";

            if (selLinha != "0" && selLinha != "")
                glb_sFiltroLinha = " AND ((SELECT aa.LinhaProdutoID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = " + selLinha + ")";
            else
                glb_sFiltroLinha = "";

            if (selFornecedor != "0" && selFornecedor != "")
            {
                //SAS0081 - Pedidos Open Micrososft
                if ((selFornecedor == "10003") && (chkEnc))
                {
                    sCustoComprar = "CONVERT(NUMERIC(11,2),(SELECT TOP 1 dbo.fn_Preco_Cotacao(704, " + glb_nMoedaFaturamentoID + ", NULL, GETDATE()) * aa.CustoFOB " +
                                    "FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) " +
                                    "WHERE aa.RelacaoID=a.RelacaoID AND aa.FornecedorID=" + selFornecedor +
                                    "ORDER BY aa.Ordem)) AS CustoComprar, ";
                }
                else {
                    sCustoComprar = "CONVERT(NUMERIC(11,2),(SELECT TOP 1 dbo.fn_Preco_Cotacao(aa.MoedaID, " + glb_nMoedaFaturamentoID + ", NULL, GETDATE()) * aa.CustoReposicao " +
                                    "FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) " +
                                    "WHERE aa.RelacaoID=a.RelacaoID AND aa.FornecedorID=" + selFornecedor +
                                    "ORDER BY aa.Ordem)) AS CustoComprar, ";
                }
            }
            else
                sCustoComprar = "0 AS CustoComprar, ";

            //Classificação fiscal ReadOnly depende do direito.
            string sNCM = "0";
            if ((glb_sReadOnlyClassFiscal == "") && (!glb_Excel))
                sNCM = "a.ClassificacaoFiscalID AS NCM, ";
            else if ((glb_sReadOnlyClassFiscal != "") && (!glb_Excel))
                sNCM = "a.ClassificacaoFiscalID, (SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ClassificacaoFiscalID)) AS NCM, ";
            else if (glb_Excel)
                sNCM = "(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ClassificacaoFiscalID)) AS NCM, ";

            if (!chkEnc) {
                if (glb_Inicio == true)
                {
                    strSQL = "SET NOCOUNT ON " +
                            "DECLARE @Table2 TABLE (RelacaoID INT) " +
                            "INSERT INTO @Table2 " +
                                "SELECT TOP 1  a.RelacaoID " +
                                "FROM RelacoesPesCon a WITH(NOLOCK) ";

                    glb_Inicio = false;
                }
                else
                {
                    strSQL = "SET NOCOUNT ON " +
                            "DECLARE @Table2 TABLE (RelacaoID INT) " +
                            "INSERT INTO @Table2 " +
                    "SELECT TOP " + glb_nRegistros + " a.RelacaoID " +
                                "FROM RelacoesPesCon a WITH(NOLOCK) ";
                }

                strSQL += "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(aa.RelacaoID, NULL, 1, GETDATE()), 1)) AS ProdutoInconsistencia, aa.RelacaoID AS _RelacaoID " +
                                    "FROM  RelacoesPesCon aa WITH(NOLOCK)) AS ProdutosInconsistentes ON (ProdutosInconsistentes._RelacaoID = a.RelacaoID) " +
                            "INNER JOIN (SELECT dbo.fn_Produto_Estoque(bb.SujeitoID, bb.ObjetoID, 341, NULL, GETDATE(), NULL, NULL, NULL) AS Estoque, bb.RelacaoID AS _RelacaoID " +
                                    "FROM RelacoesPesCon bb WITH(NOLOCK)) AS Estoque ON (Estoque._RelacaoID = a.RelacaoID) " +
                            "INNER JOIN (SELECT dbo.fn_Produto_Comprar(cc.SujeitoID, cc.ObjetoID, NULL, GETDATE(), 10) AS Compras, cc.RelacaoID AS _RelacaoID " +
                                    "FROM  RelacoesPesCon cc WITH(NOLOCK)) AS ComprasPendentes ON (ComprasPendentes._RelacaoID = a.RelacaoID) " +
                            "INNER JOIN (SELECT dbo.fn_Produto_Comprar(dd.SujeitoID, dd.ObjetoID, NULL, GETDATE(), 20) AS Excesso, dd.RelacaoID AS _RelacaoID " +
                                    "FROM  RelacoesPesCon dd WITH(NOLOCK)) AS EstoqueExcesso ON (EstoqueExcesso._RelacaoID = a.RelacaoID) " +
                            "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ee.RelacaoID, 10, 1, GETDATE()), 1)) AS EstadoInconsistencia, ee.RelacaoID AS _RelacaoID " +
                                    "FROM  RelacoesPesCon ee WITH(NOLOCK)) AS EstadosInconsistentes ON (EstadosInconsistentes._RelacaoID = a.RelacaoID) " +
                            "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ff.RelacaoID, 20, 1, GETDATE()), 1)) AS CVPInconsistencia, ff.RelacaoID AS _RelacaoID " +
                                    "FROM  RelacoesPesCon ff WITH(NOLOCK)) AS CVPInconsistentes ON (CVPInconsistentes._RelacaoID = a.RelacaoID) " +
                            "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(gg.RelacaoID, 30, 1, GETDATE()), 1)) AS PVInconsistencia, gg.RelacaoID AS _RelacaoID  " +
                                    "FROM  RelacoesPesCon gg WITH(NOLOCK)) AS PVInconsistentes ON (PVInconsistentes._RelacaoID = a.RelacaoID) " +
                            "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(hh.RelacaoID, 40, 1, GETDATE()), 1)) AS MCInconsistencia, hh.RelacaoID AS _RelacaoID " +
                                    "FROM  RelacoesPesCon hh WITH(NOLOCK)) AS MCInconsistentes ON (MCInconsistentes._RelacaoID = a.RelacaoID) " +
                            "INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ii.RelacaoID, 50, 1, GETDATE()), 1)) AS InternetInconsistencia, ii.RelacaoID AS _RelacaoID " +
                                    "FROM  RelacoesPesCon ii WITH(NOLOCK)) AS InternetInconsistentes ON (InternetInconsistentes._RelacaoID = a.RelacaoID) " +
                            " INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(jj.RelacaoID, 60, 1, GETDATE()), 1)) AS PagamentoInconsistencia, jj.RelacaoID AS _RelacaoID " +
                                    "FROM  RelacoesPesCon jj WITH(NOLOCK)) AS PagamentosInconsistentes ON (PagamentosInconsistentes._RelacaoID = a.RelacaoID) " +
                            "WHERE (";


                
                if (selFiltroArgumentosVendaWhere != "")
                    strSQL += selFiltroArgumentosVendaWhere + ") AND (";

                strSQL += sAGrid;

                strSQL += "SELECT " +

                // Dados comuns inicio
                "a.RelacaoID AS RelacaoID, a.ObjetoID, a.EstadoID AS EstadoID, a.SujeitoID AS SujeitoID, " +
                "(SELECT RecursoAbreviado FROM Recursos WITH(NOLOCK) WHERE RecursoID=a.EstadoID) AS Estado, " +
                "(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ObjetoID)) AS Produto, " +
                "(SELECT p.Conceito FROM Conceitos p WITH(NOLOCK) INNER JOIN Conceitos m WITH(NOLOCK) ON p.ConceitoID = m.MarcaID AND a.ObjetoID = m.ConceitoID) AS Marca, " +
                "(SELECT Iniciais FROM RelacoesPesRec WITH(NOLOCK) " +
                    "WHERE ((TipoRelacaoID = 11) AND (ObjetoID = 999) AND (SujeitoID = a.ProprietarioID))) AS Proprietario, " +
                "(SELECT Iniciais FROM RelacoesPesRec WITH(NOLOCK) " +
                    "WHERE ((TipoRelacaoID = 11) AND (ObjetoID = 999) AND (SujeitoID = a.AlternativoID))) AS Alternativo, " +
                "a.Observacao, a.UsuarioID AS _UsuarioID, " +

                // Ciclo de vida
                "a.dtInicio, a.dtFim, (DATEDIFF(dd, GETDATE(), a.dtFim)) AS DiasFim, " +
                "a.dtControle, (DATEDIFF(dd, GETDATE(), a.dtControle)) AS DiasControle, " +
                "a.dtMediaPagamento, (DATEDIFF(dd, GETDATE(), a.dtMediaPagamento)) AS DiasPagamento, " +
                "(CONVERT(VARCHAR(10), dbo.fn_Produto_Movimento_Kardex(a.SujeitoID, a.ObjetoID, 1, 341), " + DATE_SQL_PARAM + ")) AS dtUltimaEntrada, " +
                "(CONVERT(VARCHAR(10), dbo.fn_Produto_Movimento_Kardex(a.SujeitoID, a.ObjetoID, 2, 341), " + DATE_SQL_PARAM + ")) AS dtUltimaSaida, " +
                "(DATEDIFF(dd, dbo.fn_Produto_Movimento_Kardex(a.SujeitoID, a.ObjetoID, 2, 341), GETDATE())) AS DiasUltimaSaida, " +
                "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -3, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -3, 0, 0), 1)) AS VendasMes3, " +
                "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -2, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -2, 0, 0), 1)) AS VendasMes2, " +
                "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -1, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -1, 0, 0), 1)) AS VendasMes1, " +
                "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3,  0, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3,  0, 0, 0), 1)) AS VendasMes0, " +
                "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, 0, 1, 0), " +
                    "dbo.fn_Data_Dia(GETDATE(), 3, 0, 0, 0), 2)) AS MediaVendas13, " +

                // Previsao de vendas				        
                "a.PrevisaoVendas, " +
                "0 AS Contribuicao, " +
                "0 AS PlanejamentoVendasTotal, " +
                "0 AS Variacao, " +
                "dbo.fn_Produto_Custo(a.RelacaoID, GETDATE(), " +
                    "(SELECT TOP 1 MoedaID  " +
                        "FROM Recursos WITH(NOLOCK) " +
                        "WHERE RecursoID = 999),NULL ,1) AS CustoMedio, " +
                "((dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 341, NULL, NULL, NULL, 375, NULL)) * (dbo.fn_Produto_Custo(a.RelacaoID, GETDATE(),(SELECT TOP 1 MoedaID  FROM Recursos WITH(NOLOCK) WHERE RecursoID = 999),null, 1))) AS Custo_Total, " +
                "a.MargemPadrao, a.MargemContribuicao, " +
                "0 AS MargemReal, " +
                "a.MargemMinima, 0 AS MargemMedia, " +
                "0 AS PrecoBase, " +
                "0 AS PrecoBaseMinimo, 0 AS PrecoBaseMedio, " +
                "a.PrecoMinimo AS PrecoMinimo, " +
                "dbo.fn_Preco_PrecoLista(a.SujeitoID,a.ObjetoID, NULL, 13, 647, GETDATE(), 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, " +
                    " NULL, NULL, NULL, NULL, a.MargemContribuicao, NULL, NULL, 2, 0, NULL,NULL, " +
                    "dbo.fn_EmpresaPessoaTransacao_CFOP(a.SujeitoID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, a.ObjetoID, NULL)) AS Preco, " +
                "0 AS PrecoConcorrentes, " +

                // Informacoes basicas				    
                "a.MultiploVenda, a.MultiploCompra, a.PrazoEstoque, " + sNCM +
                // ???  
                "(SELECT aa.SimboloMoeda FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.MoedaEntradaID)) AS MoedaEntrada, " +
                "(SELECT TOP 1 bb.SimboloMoeda " +
                    "FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK), Conceitos bb WITH(NOLOCK) " +
                    "WHERE ((aa.RelacaoID = a.RelacaoID) AND (aa.MoedaID = bb.ConceitoID)) " +
                    "ORDER BY aa.Ordem) AS MoedaReposicao, " +

                // Compras
                "0 AS QuantidadeComprar, " + sCustoComprar +
                "(CONVERT(NUMERIC(5), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, " + sPercentualPV + ", GETDATE(), 30))) AS ComprarExcesso, " +
                "(SELECT TOP 1 aa.CustoReposicao FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) WHERE (aa.RelacaoID = a.RelacaoID) ORDER BY aa.Ordem) AS CustoReposicao1, " +
                "SPACE(0) AS Projeto, SPACE(0) AS PartNumber, " +
                "(CONVERT(NUMERIC(3), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 17))) AS PrazoPagamento1, " +
                "(SELECT TOP 1 aa.PrazoReposicao FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) WHERE (aa.RelacaoID = a.RelacaoID) ORDER BY aa.Ordem) AS PrazoReposicao1, " +
                "(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 341, NULL, NULL, NULL, 375, NULL)) AS Estoque, " +
                "(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 351, NULL, NULL, NULL, 375, NULL)) AS ReservaCompra, " +
                "(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 354, NULL, NULL, NULL, 375, NULL)) AS ReservaVendaConfirmada, " +
                "(dbo.fn_Produto_PrevisaoEntregaContratual(a.ObjetoID, a.SujeitoID, 1, NULL, 0)) AS PrevisaoEntrega, " +
                "NULL AS LoteID, " +
                "NULL AS Lote, " +
                //'0 AS Aplicar, ' +

                // Internet
                "a.MargemPublicacao, " +
                "0 AS PrecoInternet, " +

                "a.Publica, a.PublicaPreco, a.PercentualPublicacaoEstoque, " +
                "(dbo.fn_Produto_EstoqueWeb(a.SujeitoID, a.ObjetoID, 356, NULL, GETDATE(),NULL)) AS EstoqueInternet, " +
                "a.PrazoTroca, a.PrazoGarantia, a.PrazoAsstec, a.MargemMinima AS MMInternet, " +

                // Cores
                "(dbo.fn_Produto_Auditoria(a.RelacaoID, 10, 2, GETDATE())) AS Est_Auditoria, " +
                "(dbo.fn_Produto_Auditoria(a.RelacaoID, 50, 2, GETDATE())) AS Int_Auditoria, " +
                "(dbo.fn_Produto_Auditoria(a.RelacaoID, 60, 2, GETDATE())) AS Pag_Auditoria, " +
                "(dbo.fn_Produto_Auditoria(a.RelacaoID, 40, 2, GETDATE())) AS MC_Auditoria, " +
                //'(SUBSTRING(dbo.fn_Produto_Auditoria(a.RelacaoID, 40, 1, GETDATE()), 3, 1)) AS MPub_Auditoria, ' +
                "(dbo.fn_Produto_Auditoria(a.RelacaoID, 30, 2, GETDATE())) AS PV_Auditoria, " +
                "(dbo.fn_Produto_Auditoria(a.RelacaoID, 21, 2, GETDATE())) AS CVPInicio_Auditoria, " +
                "(dbo.fn_Produto_Auditoria(a.RelacaoID, 22, 2, GETDATE())) AS CVPFim_Auditoria, " +
                "(dbo.fn_Produto_Auditoria(a.RelacaoID, 23, 2, GETDATE())) AS CVPControle_Auditoria " +

                "FROM RelacoesPesCon a WITH(NOLOCK) " +
                    "INNER JOIN @Table2 b ON (b.RelacaoID = a.RelacaoID) " +
                "ORDER BY " + glb_sChavePesquisa + " " + glb_sOrdem + " " +
                "SET NOCOUNT OFF";

            }
            else
            {
                //Quando for a transacao de serviço, deve olhar para a 215. BJBN
                if (selTransacao == "211")
                    nWhereTransacao = "AND aa.TransacaoID = 215";
                else
                    nWhereTransacao = "AND aa.TransacaoID = 115";

                string sJoinLotesItens = "LEFT OUTER JOIN Lotes_Itens f WITH(NOLOCK) ON (f.PedItemID = c.PedItemID) ";

                if (selProjeto != "" && selProjeto != "0")
                {
                    sJoinLotesItens = "INNER JOIN Lotes_Itens f WITH(NOLOCK) ON (f.PedItemID = c.PedItemID) ";

                    sFiltroLotesItens = " AND ('" + sFiltroLotesItens + "' LIKE '%/'  + CONVERT(VARCHAR(10), f.LoteID) + '/%') ";
                }

                strSQL = ((selProjeto == "") ? "" : "IF EXISTS(SELECT 1 FROM tempdb..sysobjects WHERE id = object_id('tempdb..#TempTable')) DROP TABLE #TempTable ");

                //Pedidos
                strSQL += "SELECT " +
                            " c.ProdutoID AS ProdutoID, " +
                            "(SELECT p.Conceito FROM Conceitos p WITH(NOLOCK) INNER JOIN Conceitos m WITH(NOLOCK) ON p.ConceitoID = m.MarcaID AND c.ProdutoID = m.ConceitoID) AS Marca, " +
                            "e.RecursoAbreviado AS Estado, d.Conceito AS Produto, aa.NumeroProjeto AS Projeto, d.PartNumber AS PartNumber, " +
                            "dbo.fn_Pessoa_Iniciais(a.ProprietarioID) AS Proprietario, dbo.fn_Pessoa_Iniciais(a.AlternativoID) AS Alternativo, " + sCustoComprar + " " +
                            ((selProjeto == "") ? "c.Quantidade AS Quantidade, " : "0 AS Quantidade, ") +
                            "c.ValorUnitario AS Unitario, aa.PedidoID, b.RecursoAbreviado AS Estado2, " +
                            "(CASE WHEN aa.PessoaID = aa.ParceiroID THEN dbo.fn_Pessoa_Fantasia(" +
                            "(CONVERT(INT,REPLACE((REPLACE((SUBSTRING((SUBSTRING(aa.Observacoes, CHARINDEX('<OPEN:', aa.Observacoes), (LEN(CONVERT(VARCHAR(MAX),aa.Observacoes)) - (CHARINDEX('<OPEN:', aa.Observacoes) - 1)))), " +
                            "0, (CHARINDEX('>', (SUBSTRING(aa.Observacoes, CHARINDEX('<OPEN:', aa.Observacoes), (LEN(CONVERT(VARCHAR(MAX),aa.Observacoes)) - (CHARINDEX('<OPEN:', aa.Observacoes) - 1))))) + 1))), '<OPEN:',SPACE(0))),'>',SPACE(0)))),0) ELSE " +
                            "dbo.fn_Pessoa_Fantasia(aa.PessoaID, 0) END) AS Cliente, " +
                            "dbo.fn_Pessoa_Fantasia(aa.ParceiroID, 0) AS Revenda, " +
                            "dbo.fn_Pessoa_Fantasia(aa.ProprietarioID, 0) AS Vendedor, aa.TransacaoID AS Transacao, aa.Observacao, " +
                            "c.PedItemID AS PedItemID, " +
                            "NULL AS LoteID, " +
                            "NULL AS Lote, " +
                            "f.LoteID AS ProjetoLoteID, " +
                            "dbo.fn_Produto_Internacional(a.RelacaoID, 1) AS Internacional " +
                            ((selProjeto == "") ? "" : "INTO #TempTable ") +
                    "FROM Pedidos aa WITH(NOLOCK) " +
                            "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = aa.EstadoID) " +
                            "INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = aa.PedidoID) " +
                            "INNER JOIN RelacoesPesCon a WITH(NOLOCK) ON (a.ObjetoID = c.ProdutoID) " +
                            "INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = a.ObjetoID) " +
                            "INNER JOIN Recursos e WITH(NOLOCK) ON (e.RecursoID = a.EstadoID) " +
                            sJoinLotesItens +
                    "WHERE ((aa.EstadoID = 33) AND (a.SujeitoID = aa.EmpresaID) AND (c.EhEncomenda = 1) " + nWhereTransacao +
                        sFiltroLotesItens + " AND (c.PedItemEncomendaID IS NULL) AND " + glb_sFiltroContexto + " " + glb_sPesquisa + " " +
                        glb_sFiltro + glb_sFiltroEspecifico + " " + glb_sFiltroDireitos + " " +
                        sFiltroFornecedor + " " + glb_sFiltroFamilia + " " + glb_sFiltroMarca + " " +
                        glb_sFiltroLinha + " " + glb_sFiltroProprietario + " " +
                        "AND (aa.EmpresaID = " + glb_aEmpresaData0 + ")) " +
                    "ORDER BY " + glb_sChavePesquisa + " " + glb_sOrdem + " ";

                if (selProjeto != "")
                {
                    strSQL += "UPDATE #TempTable " +
                                    "SET Quantidade = (CASE WHEN (Internacional = 0) " +
                                                        "THEN dbo.fn_Lotes_ProdutosQuantidades(ProjetoLoteID, ProdutoID,  3, NULL) " +
                                                        "ELSE dbo.fn_Lotes_ProdutosQuantidades(ProjetoLoteID, ProdutoID, 15, NULL) END) " +
                                "SELECT ProdutoID, Marca, Estado, Produto, Projeto, PartNumber, Proprietario, Alternativo, CustoComprar, Quantidade, Unitario, PedidoID, Estado2, Cliente, Revenda, Vendedor, Transacao, Observacao, PedItemID, LoteID, Lote, ProjetoLoteID, Internacional FROM #TempTable WHERE (Quantidade > 0) ";
                }
            }

            if (bToExcel)
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                DataSet dsFin = new DataSet();
                dsFin = (DataSet)HttpContext.Current.Session["Query"];
                Datateste = dsFin.Tables.Count;

                //Verifica se a Query está vazia e retorna mensagem de erro
                if (dsFin.Tables["Query1"].Rows.Count == 0)
                    nulo = true;

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    return;
                }
                else
                {
                    if (Formato == 1) //Se formato PDF
                    {
                        ;
                    }

                    else if (Formato == 2) //Se formato Excel
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query1", false, false, false);

                            Relatorio.CriarObjColunaNaTabela("RelacaoID", "RelacaoID", true, "RelacaoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "RelacaoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ObjetoID", "ObjetoID", true, "ObjetoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "ObjetoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("EstadoID", "EstadoID", true, "EstadoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "EstadoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("SujeitoID", "SujeitoID", true, "SujeitoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "SujeitoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Estado", "Estado", true, "Estado");
                            Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Produto", "Produto", true, "Produto");
                            Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Marca", "Marca", true, "Marca");
                            Relatorio.CriarObjCelulaInColuna("Query", "Marca", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Proprietario", "Proprietario", true, "Proprietario");
                            Relatorio.CriarObjCelulaInColuna("Query", "Proprietario", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Alternativo", "Alternativo", true, "Alternativo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Alternativo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Observacao", "Observacao", true, "Observacao");
                            Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("dtInicio", "dtInicio", true, "dtInicio");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtInicio", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("dtFim", "dtFim", true, "dtFim");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtFim", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("DiasFim", "DiasFim", true, "DiasFim");
                            Relatorio.CriarObjCelulaInColuna("Query", "DiasFim", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("dtControle", "dtControle", true, "dtControle");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtControle", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("DiasControle", "DiasControle", true, "DiasControle");
                            Relatorio.CriarObjCelulaInColuna("Query", "DiasControle", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("dtMediaPagamento", "dtMediaPagamento", true, "dtMediaPagamento");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtMediaPagamento", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("DiasPagamento", "DiasPagamento", true, "DiasPagamento");
                            Relatorio.CriarObjCelulaInColuna("Query", "DiasPagamento", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("dtUltimaEntrada", "dtUltimaEntrada", true, "dtUltimaEntrada");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtUltimaEntrada", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("dtUltimaSaida", "dtUltimaSaida", true, "dtUltimaSaida");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtUltimaSaida", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("DiasUltimaSaida", "DiasUltimaSaida", true, "DiasUltimaSaida");
                            Relatorio.CriarObjCelulaInColuna("Query", "DiasUltimaSaida", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("VendasMes3", "VendasMes3", true, "VendasMes3");
                            Relatorio.CriarObjCelulaInColuna("Query", "VendasMes3", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("VendasMes2", "VendasMes2", true, "VendasMes2");
                            Relatorio.CriarObjCelulaInColuna("Query", "VendasMes2", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("VendasMes1", "VendasMes1", true, "VendasMes1");
                            Relatorio.CriarObjCelulaInColuna("Query", "VendasMes1", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("VendasMes0", "VendasMes0", true, "VendasMes0");
                            Relatorio.CriarObjCelulaInColuna("Query", "VendasMes0", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MediaVendas13", "MediaVendas13", true, "MediaVendas13");
                            Relatorio.CriarObjCelulaInColuna("Query", "MediaVendas13", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrevisaoVendas", "PrevisaoVendas", true, "PrevisaoVendas");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrevisaoVendas", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Contribuicao", "Contribuicao", true, "Contribuicao");
                            Relatorio.CriarObjCelulaInColuna("Query", "Contribuicao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PlanejamentoVendasTotal", "PlanejamentoVendasTotal", true, "PlanejamentoVendasTotal");
                            Relatorio.CriarObjCelulaInColuna("Query", "PlanejamentoVendasTotal", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Variacao", "Variacao", true, "Variacao");
                            Relatorio.CriarObjCelulaInColuna("Query", "Variacao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("CustoMedio", "CustoMedio", true, "CustoMedio");
                            Relatorio.CriarObjCelulaInColuna("Query", "CustoMedio", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Custo_Total", "Custo_Total", true, "Custo_Total");
                            Relatorio.CriarObjCelulaInColuna("Query", "Custo_Total", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MargemPadrao", "MargemPadrao", true, "MargemPadrao");
                            Relatorio.CriarObjCelulaInColuna("Query", "MargemPadrao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MargemContribuicao", "MargemContribuicao", true, "MargemContribuicao");
                            Relatorio.CriarObjCelulaInColuna("Query", "MargemContribuicao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MargemReal", "MargemReal", true, "MargemReal");
                            Relatorio.CriarObjCelulaInColuna("Query", "MargemReal", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MargemMinima", "MargemMinima", true, "MargemMinima");
                            Relatorio.CriarObjCelulaInColuna("Query", "MargemMinima", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MargemMedia", "MargemMedia", true, "MargemMedia");
                            Relatorio.CriarObjCelulaInColuna("Query", "MargemMedia", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrecoBase", "PrecoBase", true, "PrecoBase");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrecoBase", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrecoBaseMinimo", "PrecoBaseMinimo", true, "PrecoBaseMinimo");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrecoBaseMinimo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrecoBaseMedio", "PrecoBaseMedio", true, "PrecoBaseMedio");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrecoBaseMedio", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrecoMinimo", "PrecoMinimo", true, "PrecoMinimo");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrecoMinimo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Preco", "Preco", true, "Preco");
                            Relatorio.CriarObjCelulaInColuna("Query", "Preco", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrecoConcorrentes", "PrecoConcorrentes", true, "PrecoConcorrentes");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrecoConcorrentes", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MultiploVenda", "MultiploVenda", true, "MultiploVenda");
                            Relatorio.CriarObjCelulaInColuna("Query", "MultiploVenda", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MultiploCompra", "MultiploCompra", true, "MultiploCompra");
                            Relatorio.CriarObjCelulaInColuna("Query", "MultiploCompra", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoEstoque", "PrazoEstoque", true, "PrazoEstoque");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoEstoque", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("NCM", "NCM", true, "NCM");
                            Relatorio.CriarObjCelulaInColuna("Query", "NCM", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MoedaEntrada", "MoedaEntrada", true, "MoedaEntrada");
                            Relatorio.CriarObjCelulaInColuna("Query", "MoedaEntrada", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MoedaReposicao", "MoedaReposicao", true, "MoedaReposicao");
                            Relatorio.CriarObjCelulaInColuna("Query", "MoedaReposicao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("QuantidadeComprar", "QuantidadeComprar", true, "QuantidadeComprar");
                            Relatorio.CriarObjCelulaInColuna("Query", "QuantidadeComprar", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("CustoComprar", "CustoComprar", true, "CustoComprar");
                            Relatorio.CriarObjCelulaInColuna("Query", "CustoComprar", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ComprarExcesso", "ComprarExcesso", true, "ComprarExcesso");
                            Relatorio.CriarObjCelulaInColuna("Query", "ComprarExcesso", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("CustoReposicao1", "CustoReposicao1", true, "CustoReposicao1");
                            Relatorio.CriarObjCelulaInColuna("Query", "CustoReposicao1", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Projeto", "Projeto", true, "Projeto");
                            Relatorio.CriarObjCelulaInColuna("Query", "Projeto", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PartNumber", "PartNumber", true, "PartNumber");
                            Relatorio.CriarObjCelulaInColuna("Query", "PartNumber", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoPagamento1", "PrazoPagamento1", true, "PrazoPagamento1");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoPagamento1", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoReposicao1", "PrazoReposicao1", true, "PrazoReposicao1");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoReposicao1", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Estoque", "Estoque", true, "Estoque");
                            Relatorio.CriarObjCelulaInColuna("Query", "Estoque", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ReservaCompra", "ReservaCompra", true, "ReservaCompra");
                            Relatorio.CriarObjCelulaInColuna("Query", "ReservaCompra", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ReservaVendaConfirmada", "ReservaVendaConfirmada", true, "ReservaVendaConfirmada");
                            Relatorio.CriarObjCelulaInColuna("Query", "ReservaVendaConfirmada", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrevisaoEntrega", "PrevisaoEntrega", true, "PrevisaoEntrega");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrevisaoEntrega", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("LoteID", "LoteID", true, "LoteID");
                            Relatorio.CriarObjCelulaInColuna("Query", "LoteID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Lote", "Lote", true, "Lote");
                            Relatorio.CriarObjCelulaInColuna("Query", "Lote", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MargemPublicacao", "MargemPublicacao", true, "MargemPublicacao");
                            Relatorio.CriarObjCelulaInColuna("Query", "MargemPublicacao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrecoInternet", "PrecoInternet", true, "PrecoInternet");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrecoInternet", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Publica", "Publica", true, "Publica");
                            Relatorio.CriarObjCelulaInColuna("Query", "Publica", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PublicaPreco", "PublicaPreco", true, "PublicaPreco");
                            Relatorio.CriarObjCelulaInColuna("Query", "PublicaPreco", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PercentualPublicacaoEstoque", "PercentualPublicacaoEstoque", true, "PercentualPublicacaoEstoque");
                            Relatorio.CriarObjCelulaInColuna("Query", "PercentualPublicacaoEstoque", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("EstoqueInternet", "EstoqueInternet", true, "EstoqueInternet");
                            Relatorio.CriarObjCelulaInColuna("Query", "EstoqueInternet", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoTroca", "PrazoTroca", true, "PrazoTroca");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoTroca", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoGarantia", "PrazoGarantia", true, "PrazoGarantia");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoGarantia", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoAsstec", "PrazoAsstec", true, "PrazoAsstec");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoAsstec", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MMInternet", "MMInternet", true, "MMInternet");
                            Relatorio.CriarObjCelulaInColuna("Query", "MMInternet", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Est_Auditoria", "Est_Auditoria", true, "Est_Auditoria");
                            Relatorio.CriarObjCelulaInColuna("Query", "Est_Auditoria", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Int_Auditoria", "Int_Auditoria", true, "Int_Auditoria");
                            Relatorio.CriarObjCelulaInColuna("Query", "Int_Auditoria", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Pag_Auditoria", "Pag_Auditoria", true, "Pag_Auditoria");
                            Relatorio.CriarObjCelulaInColuna("Query", "Pag_Auditoria", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MC_Auditoria", "MC_Auditoria", true, "MC_Auditoria");
                            Relatorio.CriarObjCelulaInColuna("Query", "MC_Auditoria", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PV_Auditoria", "PV_Auditoria", true, "PV_Auditoria");
                            Relatorio.CriarObjCelulaInColuna("Query", "PV_Auditoria", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("CVPInicio_Auditoria", "CVPInicio_Auditoria", true, "CVPInicio_Auditoria");
                            Relatorio.CriarObjCelulaInColuna("Query", "CVPInicio_Auditoria", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("CVPFim_Auditoria", "CVPFim_Auditoria", true, "CVPFim_Auditoria");
                            Relatorio.CriarObjCelulaInColuna("Query", "CVPFim_Auditoria", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("CVPControle_Auditoria", "CVPControle_Auditoria", true, "CVPControle_Auditoria");
                            Relatorio.CriarObjCelulaInColuna("Query", "CVPControle_Auditoria", "DetailGroup");

                        Relatorio.TabelaEnd();
                    }
                    Relatorio.CriarPDF_Excel(Title, Formato);
                }
            }
        }
    }
}