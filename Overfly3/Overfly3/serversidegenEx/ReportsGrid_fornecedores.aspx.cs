﻿using System;
using System.Web;
using System.Data;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_fornecedores : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;

        private int RelatorioID = Convert.ToInt32(HttpContext.Current.Request.Params["RelatorioID"]);
        private int Formato = Convert.ToInt32(HttpContext.Current.Request.Params["formato"]);
        private bool bToExcel = Convert.ToBoolean(HttpContext.Current.Request.Params["bToExcel"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string glb_sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);

        int Datateste = 0;
        bool nulo = false;

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "1":
                        relatorioFornecedores();
                        break;
                }
            }
        }

        public void relatorioFornecedores()
        {
            //Parametros da procedure
            int glb_nModalType = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nModalType"]);
            string selFornecedor = Convert.ToString(HttpContext.Current.Request.Params["selFornecedor"]);
            string selFornecedor2 = Convert.ToString(HttpContext.Current.Request.Params["selFornecedor2"]);
            string selServico = Convert.ToString(HttpContext.Current.Request.Params["selServico"]);
            string selFiltroEspecifico = Convert.ToString(HttpContext.Current.Request.Params["selFiltroEspecifico"]);
            string selFiltroEspecificoFiltro = Convert.ToString(HttpContext.Current.Request.Params["selFiltroEspecificoFiltro"]);
            string selFamilia = Convert.ToString(HttpContext.Current.Request.Params["selFamilia"]);
            string selMarca = Convert.ToString(HttpContext.Current.Request.Params["selMarca"]);
            string selLinha = Convert.ToString(HttpContext.Current.Request.Params["selLinha"]);
            int glb_nProprietarioID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nProprietarioID"]);
            int glb_nRegistros = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nRegistros"]);
            string sPercentualPV = Convert.ToString(HttpContext.Current.Request.Params["sPercentualPV"]);
            string glb_sFiltroContexto = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltroContexto"]);
            string glb_sPesquisa = Convert.ToString(HttpContext.Current.Request.Params["glb_sPesquisa"]);
            string glb_sFiltro = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltro"]);
            string glb_sFiltroDireitos = Convert.ToString(HttpContext.Current.Request.Params["glb_sFiltroDireitos"]);
            string glb_aEmpresaData0 = Convert.ToString(HttpContext.Current.Request.Params["glb_aEmpresaData0"]);
            string glb_sChavePesquisa = Convert.ToString(HttpContext.Current.Request.Params["glb_sChavePesquisa"]);
            string glb_sOrdem = Convert.ToString(HttpContext.Current.Request.Params["glb_sOrdem"]);
            bool bComboDataSemelhante = Convert.ToBoolean(HttpContext.Current.Request.Params["bComboDataSemelhante"]);
            int seldtAtualizacaoSelectedIndex = Convert.ToInt32(HttpContext.Current.Request.Params["seldtAtualizacaoSelectedIndex"]);
            string seldtAtualizacaoValue = Convert.ToString(HttpContext.Current.Request.Params["seldtAtualizacaoValue"]);
            bool chkDataSemelhante = Convert.ToBoolean(HttpContext.Current.Request.Params["chkDataSemelhante"]);
            string sAGrid = Convert.ToString(HttpContext.Current.Request.Params["sAGrid"]);
            int glb_nMoedaFaturamentoID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nMoedaFaturamentoID"]);
            

            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            int DATE_SQL_PARAM = 103;
            if (sLinguaLogada != 246) {
                param = "MM/dd/yyy";
                DATE_SQL_PARAM = 101;
            }

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            param = "<Format>" + param + "</Format>";

            string Title = "Posição de produtos - " + glb_sEmpresaFantasia + " - " + _data;

            string strSQL = "";

            string sFiltroFornecedor = "";
            string sFiltroComprar = "";
            string sCustoComprar = "";
            string sFiltroData = "";
            string glb_sFiltroEspecifico;
            string glb_sFiltroFamilia;
            string glb_sFiltroMarca;
            string glb_sFiltroLinha;
            string glb_sFiltroProprietario;


            if ((glb_nModalType == 9) && ((selFornecedor != "0") && (selFornecedor != "")))
                sFiltroFornecedor = " AND (SELECT COUNT(*) FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID AND FornecedorID = " + selFornecedor + ")) > 0";
            else if ((glb_nModalType == 10) && ((selFornecedor2 != "0") && (selFornecedor2 != "")))
                sFiltroFornecedor = " AND (a.FornecedorID = " + selFornecedor2 + ")";

            if ((selServico == "5") && (glb_nModalType == 9))
                sFiltroComprar = " AND a.PrevisaoVendas >= 0 ";

            if (glb_nModalType == 9)
            {
                if (selFiltroEspecifico != "0" && selFiltroEspecifico != "")
                    glb_sFiltroEspecifico = " AND (" + selFiltroEspecificoFiltro + ")";
                else
                    glb_sFiltroEspecifico = "";

                if (selFamilia != "0" && selFamilia != "")
                    glb_sFiltroFamilia = " AND ((SELECT aa.ProdutoID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = " + selFamilia + ")";
                else
                    glb_sFiltroFamilia = "";

                if (selMarca != "0" && selMarca != "")
                    glb_sFiltroMarca = " AND ((SELECT aa.MarcaID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = " + selMarca + ")";
                else
                    glb_sFiltroMarca = "";

                if (selLinha != "0" && selLinha != "")
                    glb_sFiltroLinha = " AND ((SELECT aa.LinhaProdutoID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = " + selLinha + ")";
                else
                    glb_sFiltroLinha = "";

                if (selFornecedor != "0" && selFornecedor != "")
                {
                    sCustoComprar = "CONVERT(NUMERIC(11,2),(SELECT TOP 1 dbo.fn_Preco_Cotacao(aa.MoedaID, " + glb_nMoedaFaturamentoID + ", NULL, GETDATE()) * aa.CustoReposicao " +
                                    "FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) " +
                                    "WHERE aa.RelacaoID=a.RelacaoID AND aa.FornecedorID=" + selFornecedor +
                                    "ORDER BY aa.Ordem)) AS CustoComprar, ";
                }
                else
                {
                    sCustoComprar = "0 AS CustoComprar, ";
                }

                if (glb_nProprietarioID != 0)
                    glb_sFiltroProprietario = " AND (a.ProprietarioID = " + glb_nProprietarioID + ")";
                else
                    glb_sFiltroProprietario = "";

                
                strSQL = "SELECT TOP " + glb_nRegistros + " " +

                            // Dados comuns inicio
                            "a.RelacaoID AS _RelacaoID, a.ObjetoID, a.EstadoID AS _EstadoID, a.SujeitoID AS _SujeitoID, " +
                            "(SELECT RecursoAbreviado FROM Recursos WITH(NOLOCK) WHERE RecursoID=a.EstadoID) AS Estado, " +
                            "(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ObjetoID)) AS Produto, " +
                            "(SELECT Iniciais FROM RelacoesPesRec WITH(NOLOCK) WHERE ((TipoRelacaoID = 11) AND (ObjetoID = 999) AND (SujeitoID = a.ProprietarioID))) AS Proprietario, " +
                            "(SELECT Iniciais FROM RelacoesPesRec WITH(NOLOCK) WHERE ((TipoRelacaoID = 11) AND (ObjetoID = 999) AND (SujeitoID = a.AlternativoID))) AS Alternativo, " +
                            "a.Observacao, a.UsuarioID AS _UsuarioID, " +

                            // Ciclo de vida
                            "a.dtInicio, a.dtFim, (DATEDIFF(dd, GETDATE(), a.dtFim)) AS DiasFim, " +
                            "a.dtControle, (DATEDIFF(dd, GETDATE(), a.dtControle)) AS DiasControle, " +
                            "a.dtMediaPagamento, (DATEDIFF(dd, GETDATE(), a.dtMediaPagamento)) AS DiasPagamento, " +
                            "(CONVERT(VARCHAR(10), dbo.fn_Produto_Movimento(a.SujeitoID, a.ObjetoID, 1, 341), " + DATE_SQL_PARAM + ")) AS dtUltimaEntrada, " +
                            "(CONVERT(VARCHAR(10), dbo.fn_Produto_Movimento(a.SujeitoID, a.ObjetoID, 2, 341), " + DATE_SQL_PARAM + ")) AS dtUltimaSaida, " +
                            "(DATEDIFF(dd, dbo.fn_Produto_Movimento(a.SujeitoID, a.ObjetoID, 2, 341), GETDATE())) AS DiasUltimaSaida, " +
                            "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -3, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -3, 0, 0), 1)) AS VendasMes3, " +
                            "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -2, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -2, 0, 0), 1)) AS VendasMes2, " +
                            "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -1, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -1, 0, 0), 1)) AS VendasMes1, " +
                            "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3,  0, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3,  0, 0, 0), 1)) AS VendasMes0, " +
                            "(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, 0, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, 0, 0, 0), 2)) AS MediaVendas13, " +

                            // Previsao de vendas				        
                            "a.PrevisaoVendas, " +
                            "0 AS Contribuicao, 0 AS PlanejamentoVendasTotal, 0 AS Variacao, " +
                            "dbo.fn_Produto_Custo(a.RelacaoID, GETDATE(), (SELECT TOP 1 MoedaID FROM Recursos WITH(NOLOCK) WHERE RecursoID = 999), NULL,1) AS CustoMedio, " +
                            "a.MargemPadrao, a.MargemContribuicao, " +
                            "0 AS MargemReal, a.MargemMinima, 0 AS MargemMedia, 0 AS PrecoBase, 0 AS PrecoBaseMinimo, 0 AS PrecoBaseMedio, 0 AS Preco, 0 AS PrecoConcorrentes, " +

                            // Informacoes basicas				    
                            "a.MultiploVenda, a.MultiploCompra, a.PrazoEstoque, a.ClassificacaoFiscalID, " +
                            "(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ClassificacaoFiscalID)) AS NCM, " +

                            // ???  
                            "(SELECT aa.SimboloMoeda FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.MoedaEntradaID)) AS MoedaEntrada, " +
                            "(SELECT TOP 1 bb.SimboloMoeda " +
                                "FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK), Conceitos bb WITH(NOLOCK) " +
                                "WHERE ((aa.RelacaoID = a.RelacaoID) AND (aa.MoedaID = bb.ConceitoID)) " +
                                "ORDER BY aa.Ordem) AS MoedaReposicao, " +

                            // Compras
                            "0 AS QuantidadeComprar, " + sCustoComprar +
                            "(CONVERT(NUMERIC(5), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, " + sPercentualPV + ", GETDATE(), 30))) AS ComprarExcesso, " +
                            "(SELECT TOP 1 aa.CustoReposicao FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) WHERE (aa.RelacaoID = a.RelacaoID) ORDER BY aa.Ordem) AS CustoReposicao1, " +
                            "(CONVERT(NUMERIC(3), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 17))) AS PrazoPagamento1, " +
                            "(SELECT TOP 1 aa.PrazoReposicao FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) WHERE (aa.RelacaoID = a.RelacaoID) ORDER BY aa.Ordem) AS PrazoReposicao1, " +
                            "(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 341, NULL, NULL, NULL, 375, NULL)) AS Estoque, " +
                            "(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 351, NULL, NULL, NULL, 375, NULL)) AS ReservaCompra, " +
                            "(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 354, NULL, NULL, NULL, 375, NULL)) AS ReservaVendaConfirmada, " +
                            "(dbo.fn_Produto_PrevisaoEntregaContratual(a.ObjetoID, a.SujeitoID, 1, NULL, 0)) AS PrevisaoEntrega, " +

                            // Internet
                            "a.MargemPublicacao, 0 AS PrecoInternet, " +

                            "a.Publica, a.PublicaPreco, a.PercentualPublicacaoEstoque, " +
                            "(dbo.fn_Produto_EstoqueWeb(a.SujeitoID, a.ObjetoID, 356, NULL, GETDATE(), NULL)) AS EstoqueInternet, " +
                            "a.PrazoTroca, a.PrazoGarantia, a.PrazoAsstec, " +

                            // Cores
                            "(dbo.fn_Produto_Auditoria(a.RelacaoID, 10, 2, GETDATE()) AS Est_Auditoria, " +
                            "(dbo.fn_Produto_Auditoria(a.RelacaoID, 50, 2, GETDATE()) AS Int_Auditoria, " +
                            "(dbo.fn_Produto_Auditoria(a.RelacaoID, 60, 2, GETDATE()) AS Pag_Auditoria, " +
                            "(dbo.fn_Produto_Auditoria(a.RelacaoID, 40, 2, GETDATE()) AS MC_Auditoria, " +
                            "(dbo.fn_Produto_Auditoria(a.RelacaoID, 30, 2, GETDATE()) AS PV_Critica, " +
                            "(dbo.fn_Produto_Auditoria(a.RelacaoID, 21, 2, GETDATE()) AS CVPInicio_Auditoria, " +
                            "(dbo.fn_Produto_Auditoria(a.RelacaoID, 22, 2, GETDATE()) AS CVPFim_Auditoria, " +
                            "(dbo.fn_Produto_Auditoria(a.RelacaoID, 23, 2, GETDATE()) AS CVPControle_Auditoria " +

                        "FROM RelacoesPesCon a WITH(NOLOCK) " +
                        "WHERE (" + glb_sFiltroContexto + " " + glb_sPesquisa + " " + glb_sFiltro +
                            glb_sFiltroEspecifico + " " + glb_sFiltroDireitos + " " + sFiltroFornecedor + " " +
                            glb_sFiltroFamilia + " " + glb_sFiltroMarca + " " + glb_sFiltroLinha + " " +
                            glb_sFiltroProprietario + " " +
                            "AND (a.SujeitoID = " + glb_aEmpresaData0 + ")) " +
                        "ORDER BY " + glb_sChavePesquisa + " " + glb_sOrdem;
            }
            else if (glb_nModalType == 10)
            {
                if (bComboDataSemelhante)
                    strSQL = "SELECT DISTINCT " +
                                "CONVERT(VARCHAR,dtAtualizacao," + DATE_SQL_PARAM + ") AS fldName, " +
                                "CONVERT(VARCHAR,dtAtualizacao,101) AS fldID, dbo.fn_Data_Zero(dtAtualizacao) AS dtAtualizacao ";
                else
                {
                    strSQL = "SELECT " +
                                "a.RelPesConFornecID, a.RelacaoID, " +
                                "(SELECT f.ObjetoID FROM RelacoesPesCon f WITH(NOLOCK) WHERE a.RelacaoID=f.RelacaoID) AS ObjetoID, " +
                                "(SELECT e.RecursoAbreviado FROM RelacoesPesCon c WITH(NOLOCK), Recursos e WITH(NOLOCK) WHERE a.RelacaoID=c.RelacaoID AND c.EstadoID=e.RecursoID) AS Estado, " +
                                "(SELECT b.Conceito FROM Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK) WHERE a.RelacaoID=c.RelacaoID AND b.ConceitoID=c.ObjetoID) AS Produto, " +
                                "(SELECT d.Fantasia FROM Pessoas d WITH(NOLOCK) WHERE d.PessoaID=a.FornecedorID) AS Fornecedor, " +
                                "(SELECT b.Modelo FROM Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK) WHERE a.RelacaoID=c.RelacaoID AND b.ConceitoID=c.ObjetoID) AS Modelo, " +
                                "(SELECT b.PartNumber FROM Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK) WHERE a.RelacaoID=c.RelacaoID AND b.ConceitoID=c.ObjetoID) AS PartNumber, " +
                                "a.Ordem,a.MoedaID, a.CustoFOB, a.CustoReposicao, a.dtAtualizacao, a.MoedaRebateID, a.RebateCliente, a.ClassificacaoFiscalID, a.PrazoPagamento, a.PrazoReposicao, " +
                                "a.Observacao, a.PrazoTroca, a.PrazoGarantia, a.PrazoAsstec, a.AsstecProprio, a.AsstecTerceirizado, " +
                                    "(SELECT dbo.fn_Preco_Preco(PesCon.SujeitoID, PesCon.ObjetoID, NULL, Fornecedores.CustoReposicao, 0, NULL, a.MoedaID, GETDATE(), " +
                                        "1, 0, 0, 1, PesCon.SujeitoID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL) " +
                                    "FROM RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) " +
                                    "INNER JOIN RelacoesPesCon PesCon WITH(NOLOCK) ON (PesCon.RelacaoID = Fornecedores.RelacaoID)" +
                                    "WHERE (Fornecedores.RelPesConFornecID = a.RelPesConFornecID)) as Preco," +
                                "dbo.fn_Produto_FatorInternacao(-a.RelPesConFornecID, 1, GETDATE()) AS FIE, " +
                                "a.FatorInternacaoSaida, a.PortoOrigemID, a.ViaTransporteID, (CASE a.AsstecTerceirizado WHEN  1 THEN 'VERDADEIRO' ELSE 'FALSO' END) AS AsstecTerceirizado_1, " +
                                "dbo.fn_RelPesConFornec_AliquotaNaoInc(a.RelPesConFornecID) AS AliquotaNaoInc ";

                    if ((seldtAtualizacaoSelectedIndex >= 1) && (!bComboDataSemelhante))
                    {
                        if (chkDataSemelhante)
                            sFiltroData = " AND dbo.fn_Data_Zero(a.dtAtualizacao) = '" + seldtAtualizacaoValue + "' ";
                        else
                            sFiltroData = " AND (dbo.fn_Data_Zero(a.dtAtualizacao) <> '" + seldtAtualizacaoValue + "' OR a.dtAtualizacao IS NULL) ";
                    }
                }
                strSQL += "FROM RelacoesPesCon_Fornecedores a WITH(NOLOCK) " +
                    "WHERE a.RelacaoID IN (0";
            }

            strSQL += sAGrid;


            strSQL = strSQL + ')' + sFiltroFornecedor + sFiltroComprar + sFiltroData;

            strSQL = strSQL + glb_sFiltroDireitos;

            if (!bComboDataSemelhante)
                strSQL = strSQL + " ORDER BY Fornecedor, a.RelacaoID, a.Ordem";
            else
                strSQL = strSQL + " ORDER BY dtAtualizacao DESC ";

            //SE EXCEL btnExcel
            if (bToExcel)
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");


                DataSet dsFin = new DataSet();
                dsFin = (DataSet)HttpContext.Current.Session["Query"];
                Datateste = dsFin.Tables.Count;

                //Verifica se a Query está vazia e retorna mensagem de erro
                if (dsFin.Tables["Query1"].Rows.Count == 0)
                    nulo = true;

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    return;
                }
                else
                {
                    if (Formato == 1) //Se formato PDF
                    {
                        ;
                    }

                    else if (Formato == 2) //Se formato Excel
                    {
                        Relatorio.CriarObjTabela("0.00", "0.00", "Query1", false, false, false);

                            Relatorio.CriarObjColunaNaTabela("RelPesConFornecID", "RelPesConFornecID", true, "RelPesConFornecID");
                            Relatorio.CriarObjCelulaInColuna("Query", "RelPesConFornecID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("RelacaoID", "RelacaoID", true, "RelacaoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "RelacaoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ObjetoID", "ObjetoID", true, "ObjetoID");
                            Relatorio.CriarObjCelulaInColuna("Query", "ObjetoID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Estado", "Estado", true, "Estado");
                            Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Produto", "Produto", true, "Produto");
                            Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Fornecedor", "Fornecedor", true, "Fornecedor");
                            Relatorio.CriarObjCelulaInColuna("Query", "Fornecedor", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Modelo", "Modelo", true, "Modelo");
                            Relatorio.CriarObjCelulaInColuna("Query", "Modelo", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PartNumber", "PartNumber", true, "PartNumber");
                            Relatorio.CriarObjCelulaInColuna("Query", "PartNumber", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Ordem", "Ordem", true, "Ordem");
                            Relatorio.CriarObjCelulaInColuna("Query", "Ordem", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("MoedaID", "MoedaID", true, "MoedaID");
                            Relatorio.CriarObjCelulaInColuna("Query", "MoedaID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("CustoFOB", "CustoFOB", true, "CustoFOB");
                            Relatorio.CriarObjCelulaInColuna("Query", "CustoFOB", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("CustoReposicao", "CustoReposicao", true, "CustoReposicao");
                            Relatorio.CriarObjCelulaInColuna("Query", "CustoReposicao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("dtAtualizacao", "dtAtualizacao", true, "dtAtualizacao");
                            Relatorio.CriarObjCelulaInColuna("Query", "dtAtualizacao", "DetailGroup", Decimais: param);

                            Relatorio.CriarObjColunaNaTabela("MoedaRebateID", "MoedaRebateID", true, "MoedaRebateID");
                            Relatorio.CriarObjCelulaInColuna("Query", "MoedaRebateID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("RebateCliente", "RebateCliente", true, "RebateCliente");
                            Relatorio.CriarObjCelulaInColuna("Query", "RebateCliente", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ClassificacaoFiscalID", "ClassificacaoFiscalID", true, "ClassificacaoFiscalID");
                            Relatorio.CriarObjCelulaInColuna("Query", "ClassificacaoFiscalID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoPagamento", "PrazoPagamento", true, "PrazoPagamento");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoPagamento", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoReposicao", "PrazoReposicao", true, "PrazoReposicao");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoReposicao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Observacao", "Observacao", true, "Observacao");
                            Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoTroca", "PrazoTroca", true, "PrazoTroca");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoTroca", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoGarantia", "PrazoGarantia", true, "PrazoGarantia");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoGarantia", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PrazoAsstec", "PrazoAsstec", true, "PrazoAsstec");
                            Relatorio.CriarObjCelulaInColuna("Query", "PrazoAsstec", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("AsstecProprio", "AsstecProprio", true, "AsstecProprio");
                            Relatorio.CriarObjCelulaInColuna("Query", "AsstecProprio", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("AsstecTerceirizado", "AsstecTerceirizado", true, "AsstecTerceirizado");
                            Relatorio.CriarObjCelulaInColuna("Query", "AsstecTerceirizado", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("Preco", "Preco", true, "Preco");
                            Relatorio.CriarObjCelulaInColuna("Query", "Preco", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("FIE", "FIE", true, "FIE");
                            Relatorio.CriarObjCelulaInColuna("Query", "FIE", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("FatorInternacaoSaida", "FatorInternacaoSaida", true, "FatorInternacaoSaida");
                            Relatorio.CriarObjCelulaInColuna("Query", "FatorInternacaoSaida", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("PortoOrigemID", "PortoOrigemID", true, "PortoOrigemID");
                            Relatorio.CriarObjCelulaInColuna("Query", "PortoOrigemID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("ViaTransporteID", "ViaTransporteID", true, "ViaTransporteID");
                            Relatorio.CriarObjCelulaInColuna("Query", "ViaTransporteID", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("AsstecTerceirizado_1", "AsstecTerceirizado_1", true, "AsstecTerceirizado_1");
                            Relatorio.CriarObjCelulaInColuna("Query", "AsstecTerceirizado_1", "DetailGroup");

                            Relatorio.CriarObjColunaNaTabela("AliquotaNaoInc", "AliquotaNaoInc", true, "AliquotaNaoInc");
                            Relatorio.CriarObjCelulaInColuna("Query", "AliquotaNaoInc", "DetailGroup");

                        Relatorio.TabelaEnd();
                    }
                    Relatorio.CriarPDF_Excel(Title, Formato);
                }
            }
        }
    }
}