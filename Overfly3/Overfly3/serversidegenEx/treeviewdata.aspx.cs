using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class treeviewdata : System.Web.UI.OverflyPage
    {
        private string userId;
        private string empresaId;

        public string userID
        {
            set { userId = value; }
        }
        public string empresaID
        {
            set { empresaId = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(DataInterfaceObj.getRemoteData(
            "SELECT DISTINCT d.Recurso AS Modulo,e.Recurso AS SubModulo, " +
         "f.Recurso AS FormTitle,f.FormName AS FormName,d.RecursoID,e.RecursoID,f.RecursoID AS btnLeftRightOrdem, f.NomeBotao AS BtnValue, ISNULL(f.Ordem, 99) AS BtnOrdem " +
         "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), Recursos e WITH(NOLOCK), Recursos f WITH(NOLOCK), " +
         "Recursos_Direitos g WITH(NOLOCK), Recursos_Direitos h WITH(NOLOCK), Recursos_Direitos i WITH(NOLOCK), Recursos_Direitos j WITH(NOLOCK), Recursos l WITH(NOLOCK) " +
         "WHERE a.SujeitoID IN ((SELECT " + userId.ToString() + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + userId.ToString() + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
         "AND a.ObjetoID=999 AND a.TipoRelacaoID=11 AND a.RelacaoID=b.RelacaoID " +
         "AND b.EmpresaID= " + empresaId.ToString() + " AND c.EstadoID=2 AND c.TipoRecursoID=1 " +
         "AND c.RecursoID=d.RecursoMaeID AND d.EstadoID=2 AND d.TipoRecursoID=2 AND d.ClassificacaoID=9 " +
         "AND d.RecursoID=e.RecursoMaeID AND e.EstadoID=2 AND e.TipoRecursoID=2 AND e.ClassificacaoID=10 " +
         "AND e.RecursoID=f.RecursoMaeID AND f.EstadoID=2 AND f.TipoRecursoID=2 AND f.ClassificacaoID=11 " +
         "AND c.RecursoID=g.RecursoID AND b.PerfilID=g.PerfilID AND (g.Consultar1=1 OR g.Consultar2=1) " +
         "AND d.RecursoID=h.RecursoID AND b.PerfilID=h.PerfilID AND (h.Consultar1=1 OR h.Consultar2=1) " +
         "AND e.RecursoID=i.RecursoID AND b.PerfilID=i.PerfilID AND (i.Consultar1=1 OR i.Consultar2=1) " +
         "AND f.RecursoID=j.RecursoID AND b.PerfilID=j.PerfilID AND (j.Consultar1=1 OR j.Consultar2=1) " +
         "AND b.PerfilID = l.RecursoID AND l.EstadoID = 2 " +
         "ORDER BY d.RecursoID,e.RecursoID,f.RecursoID "));
        }
    }
}
