using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class staticcmbsdatainf : System.Web.UI.OverflyPage
    {
        private Integer formId;
        private Integer empresaId;
        private Integer empresaPaisId;
        private Integer userId;
        private Integer idiomaSistemaId;
        private Integer idiomaEmpresaId;

        public Integer nFormID
        {
            set { formId = value; }
        }
        public Integer nEmpresaID
        {
            set { empresaId = value; }
        }
        public Integer nEmpresaPaisID
        {
            set { empresaPaisId = value; }
        }
        public Integer nUserID
        {
            set { userId = value; }
        }
        public Integer nIdiomaSistemaID
        {
            set { idiomaSistemaId = value; }
        }
        public Integer nIdiomaEmpresaID
        {
            set { idiomaEmpresaId = value; }
        }

        protected string Sql
        {
            get
            {
                string sql = "";

                if (formId == null) return sql;

                switch (formId.intValue())
                {
                    case 1110:
                        sql = "SELECT '1' AS Indice,STR(CompetenciaID) AS Ordem2,Competencia AS fldName,CompetenciaID AS fldID " +
                                " FROM Competencias WITH(NOLOCK) WHERE (TipoCompetenciaID = 1701 AND EstadoID=2) " +
                                "ORDER BY Indice, Ordem2";
                        break;

                    case 1130:
                        sql = "SELECT '1' AS Indice,STR(CompetenciaID) AS Ordem2,Competencia AS fldName,CompetenciaID AS fldID " +
                                " FROM Competencias WITH(NOLOCK) WHERE (TipoCompetenciaID = 1701 AND EstadoID=2) " +
                                "ORDER BY Indice, Ordem2";
                        break;

                    case 1210:
                        sql = "SELECT '1' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID " +
                                "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID = 16 AND EstadoID=2 " +
                                "ORDER BY Indice, Ordem2";
                        break;

                    case 2130:
                        sql = "SELECT '1' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                "FROM Conceitos WITH(NOLOCK) " +
                                "WHERE EstadoID = 2 AND TipoConceitoID = 307 " +
                                "AND PaisID = " + empresaPaisId + " " +
                                "UNION ALL SELECT '2' as Indice, c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, c.SimboloMoeda AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                "FROM RelacoesPesRec a WITH(NOLOCK) " +
                                     "INNER JOIN RelacoesPesRec_Moedas b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " +
                                     "INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MoedaID = c.ConceitoID) " +
                                "WHERE a.SujeitoID = " + empresaId + " " +
                                "AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 " +
                                "UNION ALL SELECT Indice, fldID, fldName, Ordem2, NameSuj, NameObj, Filtro, Descricao FROM dbo.fn_staticcmb_ClassificacaoFiscal_tbl (3,  " + empresaPaisId + ") " +
                                "UNION ALL " +
                                "SELECT '4' as Indice, ItemID AS fldId, ItemAbreviado AS fldName, STR(Ordem) AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ItemMAsculino AS Descricao " +
                                "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                                "WHERE TipoID = 114 AND EstadoID = 2 " +
                                "UNION ALL " +
                                "SELECT '5' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                "UNION ALL SELECT '5' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ISNULL(CONVERT(VARCHAR(8000),Observacoes), SPACE(0)) AS Descricao  " +
                                "FROM Conceitos WITH(NOLOCK) " +
                                "WHERE EstadoID = 2 AND TipoConceitoID = 319 " +
                                "AND PaisID = " + empresaPaisId + " " +
                                "ORDER BY Indice,Ordem2";
                        break;

                    case 5110:
                        break;

                    default:
                        sql = "SELECT '1' AS Indice, STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID " +
                                "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID = -9999 " +
                                "ORDER BY Indice, Ordem2";
                        break;
                }

                return sql;
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(DataInterfaceObj.getRemoteData(Sql));
        }
    }
}
