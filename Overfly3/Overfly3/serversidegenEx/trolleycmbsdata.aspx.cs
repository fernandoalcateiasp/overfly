using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class trolleycmbsdata : System.Web.UI.OverflyPage
    {
		private Integer empresaId;
		private Integer empresaPaisId;
		private Integer pessoaId;
		private Integer parceiroId;
		private Integer userId;
		private Integer tipo;

		public Integer nEmpresaID
		{
			set { empresaId = value; }
		}
		public Integer nEmpresaPaisID
		{
			set { empresaPaisId = value; }
		}
		public Integer nPessoaID
		{
			set { pessoaId = value; }
		}
		public Integer nParceiroID
		{
			set { parceiroId = value; }
		}
		public Integer nUserID
		{
			set { userId = value; }
		}
		public Integer nTipo
		{
			set { tipo = value; }
		}

		// Roda a procedure sp_CarrinhosCompras_Combos
		protected DataSet CarrinhosComprasCombos()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[6];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                empresaId.ToString());

            procParams[1] = new ProcedureParameters(
                "@VendedorID",
                System.Data.SqlDbType.Int,
                (userId.intValue() == 0) ?
                    System.DBNull.Value : (Object)userId.ToString());

            procParams[2] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                (pessoaId.intValue() == 0) ?
					System.DBNull.Value : (Object)pessoaId.ToString());

            procParams[3] = new ProcedureParameters(
                "@ParceiroID",
                System.Data.SqlDbType.Int,
				(parceiroId.intValue() == 0) ?
					System.DBNull.Value : (Object)parceiroId.ToString());

            procParams[4] = new ProcedureParameters(
                "@IdiomaID",
                System.Data.SqlDbType.Int,
				System.DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@Tipo",
                System.Data.SqlDbType.Int,
                tipo.ToString());

            return DataInterfaceObj.execQueryProcedure(
                "sp_SacolasCompras_Combos", 
				procParams);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
			WriteResultXML(CarrinhosComprasCombos());
        }
    }
}
