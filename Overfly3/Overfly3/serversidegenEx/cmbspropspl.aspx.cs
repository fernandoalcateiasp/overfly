using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class cmbspropspl : System.Web.UI.OverflyPage
    {
		private static Integer ZERO = new Integer(0);
		private static java.lang.Boolean FALSE = new java.lang.Boolean(false);
		
        private Integer formId = ZERO;
		private Integer contextoId = ZERO;
		private Integer empresaId = ZERO;
		private Integer usuarioId = ZERO;
		private java.lang.Boolean allList = FALSE;

		public Integer nFormID
		{
			set { formId = value != null ? value : ZERO; }
		}
		public Integer nContextoID
		{
			set { contextoId = value != null ? value : ZERO; }
		}
		public Integer nEmpresaID
		{
			set { empresaId = value != null ? value : ZERO; }
		}
		public Integer nUsuarioID
		{
			set { usuarioId = value != null ? value : ZERO; }
		}
		public java.lang.Boolean bAllList
		{
			set { allList = value != null ? value : FALSE; }
		}

        protected DataSet FormProprietarios()
        {
			// Cria os parametros para a procedure.			
            ProcedureParameters[] proprietariosProcParam =
                new ProcedureParameters[5];

            proprietariosProcParam[0] = new ProcedureParameters(
                "@FormID", System.Data.SqlDbType.Int, formId.ToString());
            proprietariosProcParam[1] = new ProcedureParameters(
                "@ContextoID", System.Data.SqlDbType.Int, contextoId.ToString());
            proprietariosProcParam[2] = new ProcedureParameters(
                "@EmpresaID", System.Data.SqlDbType.Int, empresaId.ToString());
			proprietariosProcParam[3] = new ProcedureParameters(
				"@UsuarioID", System.Data.SqlDbType.Int, usuarioId.ToString());
            proprietariosProcParam[4] = new ProcedureParameters(
                "@Proprietarios", System.Data.SqlDbType.Bit, allList.ToString());

			// Executa a procedure
            return DataInterfaceObj.execQueryProcedure(
				"sp_Form_Proprietarios", 
				proprietariosProcParam);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
			WriteResultXML(FormProprietarios());
        }
    }
}
