using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;


namespace Overfly3.serversidegenEx
{
    public partial class AtualizaProduto : System.Web.UI.OverflyPage
    {
        private int nSacolaID = Convert.ToInt32(HttpContext.Current.Request.Params["nSacolaID"]);

        protected override void PageLoad(object sender, EventArgs e)
        {

            string sql = "EXEC sp_CotadorCorporativo_SacolaAtualizaProduto " + nSacolaID;

            try
            {                
                    WriteResultXML(DataInterfaceObj.getRemoteData(sql));
            }
            catch (System.Exception exception)
            {

                WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select 'Erro ao atualizar o produto' as fldError"));

            }
        }
    }
}