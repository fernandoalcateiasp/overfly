using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class staticcmbsdatasup : System.Web.UI.OverflyPage
    {
        private Integer formId;
        private Integer empresaId;
        private Integer empresaPaisId;
        private Integer userId;
        private Integer idiomaSistemaId;
        private Integer idiomaEmpresaId;

        public Integer nFormID
        {
            set { formId = value; }
        }
        public Integer nEmpresaID
        {
            set { empresaId = value; }
        }
        public Integer nEmpresaPaisID
        {
            set { empresaPaisId = value; }
        }
        public Integer nUserID
        {
            set { userId = value; }
        }
        public Integer nIdiomaSistemaID
        {
            set { idiomaSistemaId = value; }
        }
        public Integer nIdiomaEmpresaID
        {
            set { idiomaEmpresaId = value; }
        }

        private string Sql
        {
            get
            {
                string sql = "";

                if (formId == null) return sql;

                switch (formId.intValue())
                {
                    case 1110:   //Form de Recursos
                        sql =
                        "SELECT '1' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                            " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=1 " +
                            "UNION ALL (SELECT '2' AS Indice, SimboloMoeda AS Ordem2, SimboloMoeda AS fldName,ConceitoID AS fldID, ' ' AS Filtro " +
                            " FROM Conceitos WITH(NOLOCK) WHERE EstadoID=2 AND TipoConceitoID=308 AND SimboloMoeda is not null) " +
                            "UNION ALL (SELECT '3' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID,' ' AS Filtro " +
                            " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=48) " +
                            "UNION ALL (SELECT '4' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                            " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=2) " +
                            "UNION ALL (SELECT '5' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                            " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=3) " +
                            "UNION ALL (SELECT '6' AS Indice,STR(Ordem) AS Ordem2,ItemAbreviado AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                            " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=4) " +
                            "UNION ALL (SELECT '7' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                            " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=5) " +
                            "UNION ALL (SELECT '8' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                            " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=11) " +
                            "UNION ALL (SELECT '9' AS Indice,SPACE(0) AS Ordem2,SPACE(0) AS fldName,0 AS fldID, ' ' AS Filtro) " +
                            "UNION ALL (SELECT '9' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                            " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=9) " +
                            "ORDER BY Indice,Ordem2";

                        return sql;

                    case 1120:   //Form de Rela��es entre Recursos
                        sql =
                        "SELECT '1' as Indice, TipoRelacaoID as fldID, TipoRelacao as fldName, TipoRelacao AS Ordem2,Sujeito AS NameSuj,Objeto AS NameObj, Filtro AS Filtro " +
                        "FROM TiposRelacoes WITH(NOLOCK) WHERE EstadoID=2 AND RelacaoEntreID=131 " +
                        "UNION ALL SELECT '2' AS Indice, 0 AS fldID, SPACE(0) AS fldName, SPACE(0) AS Ordem2,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " +
                        "UNION ALL SELECT '2' AS Indice,RecursoID AS fldID, RecursoFantasia AS fldName, RecursoFantasia AS Ordem2,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " +
                        " FROM Recursos WITH(NOLOCK) WHERE EstadoID=2 AND TipoRecursoID = 8 " +
                        "ORDER BY Indice,Ordem2";

                        return sql;

                    case 1210: //Form de Pessoas
                        sql =
                        "SELECT '1' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, Filtro AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID = 12 AND EstadoID=2 " +
                        "UNION ALL (SELECT '21' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 13 AND Filtro LIKE '%(51)%' " +
                        "AND (dbo.fn_Direitos_TiposAuxiliares(ItemID, " + empresaId.ToString() + ", " + userId.ToString() + ", GETDATE()) > 0) ) " +
                        "UNION ALL (SELECT '22' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 13 AND Filtro LIKE '%(52)%' " +
                        "AND (dbo.fn_Direitos_TiposAuxiliares(ItemID, " + empresaId.ToString() + ", " + userId.ToString() + ", GETDATE()) > 0) ) " +
                        "UNION ALL (SELECT '3' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 14) " +
                        "UNION ALL (SELECT '4' AS Indice,STR(CompetenciaID) AS Ordem2,Competencia AS fldName,CompetenciaID AS fldID, ' ' AS Filtro " +
                        " FROM Competencias WITH(NOLOCK) WHERE TipoCompetenciaID = 1701 AND EstadoID=2) " +
                        "UNION ALL (SELECT '5' AS Indice, Localidade AS Ordem2,Localidade AS fldName,LocalidadeID AS fldID, ' ' AS Filtro " +
                        " FROM Localidades WITH(NOLOCK) WHERE TipoLocalidadeID = 203 AND (EstadoID = 2 OR EstadoID = 3)) " +
                        "UNION ALL (SELECT '6' AS Indice, Codigo AS Ordem2,Codigo AS fldName,BancoID AS fldID, ' ' AS Filtro " +
                        " FROM Bancos WITH(NOLOCK) WHERE EstadoID = 2) " +
                        "UNION ALL (SELECT '7' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 10) " +
                        "UNION ALL (SELECT '8' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 24) " +
                        "UNION ALL (SELECT '9' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 58) " +
                        "UNION ALL (SELECT '10' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 119) " +
                        "UNION ALL (SELECT '11' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 120) " +
                        "ORDER BY Indice, Ordem2 ";

                        return sql;

                    case 1130: //Form de Rela��es Pessoas e Recursos
                        sql =
                        "SELECT '1' AS Indice, TipoRelacao AS Ordem2, 0 AS Ordem, TipoRelacao AS fldName, TipoRelacaoID AS fldID,Sujeito AS NameSuj,Objeto AS NameObj, Filtro AS Filtro " +
                        " FROM TiposRelacoes WITH(NOLOCK) WHERE RelacaoEntreID=132 AND EstadoID = 2 " +
                        "UNION ALL (SELECT '2' AS Indice, STR(Ordem) AS Ordem2, 0 AS Ordem, ItemMasculino AS fldName,ItemID as fldID,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID=34) " +
                        "UNION ALL (SELECT '3' as Indice, RecursoFantasia AS Ordem2, 0 AS Ordem, RecursoFantasia AS fldName,RecursoID as fldID,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " +
                        " FROM Recursos WITH(NOLOCK) WHERE TipoRecursoID=1) " +
                        "UNION ALL " +
                        "SELECT '4' AS Indice, SPACE(0) AS Ordem2, 0 AS Ordem, SPACE(0) AS FldName, 0 AS fldID, '-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " +
                        "UNION ALL (SELECT DISTINCT '4' as Indice, SPACE(0) AS Ordem2, NivelInspecao AS Ordem, CONVERT(VARCHAR(1),NivelInspecao) AS fldName, NivelInspecao as fldID,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " +
                        "FROM Documentos_CodigosAmostra WITH(NOLOCK) WHERE DocumentoID=743) " +
                        "UNION ALL " +
                        "SELECT '5' AS Indice, SPACE(0) AS Ordem2, a.PessoaID AS Ordem, a.Fantasia AS FldName, a.PessoaID AS fldID, '-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " +
                        "FROM Pessoas a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK) " +
                        "WHERE a.PessoaID = b.SujeitoID AND b.TipoRelacaoID=12 AND b.ObjetoID=999 AND b.EstadoID=2 " +
                        "ORDER BY Indice, Ordem2, Ordem";

                        return sql;

                    case 1220:   //Form de Relacoes Entre Pessoas
                        sql =
                        "SELECT '1' AS Indice, TipoRelacao AS Ordem2, TipoRelacao AS fldName, '-1' AS fldNameAlt, TipoRelacaoID AS fldID,Sujeito AS NameSuj,Objeto AS NameObj, Filtro AS Filtro " +
                        " FROM TiposRelacoes WITH(NOLOCK) WHERE RelacaoEntreID=133 AND EstadoID = 2 " +
                        "UNION ALL (SELECT '2' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=13 and Filtro Like '%{1221}%')) " +
                        "UNION ALL (SELECT '3' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=28)) " +
                        "UNION ALL (SELECT '4' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=29)) " +
                        "UNION ALL (SELECT '5' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=30)) " +
                        "UNION ALL (SELECT '7' AS Indice, STR(0) AS Ordem2, SPACE(0) AS fldName, '-1' AS fldNameAlt, 0 AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro) " +
                        "UNION ALL (SELECT '7' AS Indice, STR(Ordem) AS Ordem2, ItemAbreviado AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE (EstadoID=2 AND TipoID=804 AND Filtro LIKE '%(1221)%')) " +
                        "UNION ALL (SELECT '9' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemFeminino AS fldNameAlt , ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=31) " +
                        "UNION ALL (SELECT '10' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=33) " +

                        "UNION ALL (SELECT '11' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1001 AND Filtro LIKE '%<F>%') " +

                        "ORDER BY Indice, Ordem2";

                        return sql;

                    case 1230: //Form prospect
                        sql =
                        "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND ItemID IN(51,52)  " +
                        "UNION ALL " +
                        "SELECT '2' AS Indice, Localidade AS Ordem2, Localidade AS fldName, LocalidadeID AS fldID " +
                        "FROM Localidades WITH(NOLOCK) WHERE EstadoID = 2 AND TipoLocalidadeID=203 " +
                        "ORDER BY Indice, Ordem2";

                        return sql;

                    case 5240: //Form prospect
                        sql =
                        "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 37 " +
                        "UNION ALL " +
                        "SELECT '2' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 38 " +
                        "ORDER BY Indice, Ordem2";

                        return sql;

                    case 1320: //Form de Tipos de Rela��es
                        sql =
                        "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 25 " +
                        "UNION ALL (SELECT '2' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 26) " +
                        "ORDER BY Indice, Ordem2";

                        return sql;

                    case 1330: //Form de Localidades
                        sql =
                        "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 42 " +
                        "UNION ALL (SELECT '2' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 43) " +
                        "UNION ALL (SELECT '3' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 47) " +
                        "UNION ALL (SELECT '5' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 44) " +
                        "UNION ALL (SELECT '6' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 45) " +
                        "ORDER BY Indice, Ordem2";

                        return sql;

                    case 1340: //Form de de termo de uso
                        sql =
                        "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " +
                        "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID=610) ORDER BY Indice, Ordem2";

                        return sql;


                    case 1350: //Form de de termo de uso
                        sql =
                        "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemFeminino AS fldName, ItemID AS fldID, ' ' AS Filtro " +
                        "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID=42 AND Aplicar = 1) ORDER BY Indice, Ordem2";

                        return sql;

                    case 1360: //Form de Dicionario
                        sql =
                        "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 48 " +
                        "ORDER BY Indice, Ordem2";

                        return sql;

                    case 2110: //Form de Conceitos
                        sql =
                        "SELECT '1' as Indice, ItemID as fldID,ItemMasculino as fldName, STR(Ordem) AS Ordem2, Filtro AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        " WHERE EstadoID=2 AND TipoID=101 " +
                        "UNION ALL " +
                        "SELECT '2' as Indice, ItemID as fldID,ItemMasculino as fldName, STR(Ordem) AS Ordem2, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        " WHERE EstadoID=2 AND TipoID=102 " +
                        "UNION ALL " +
                        "SELECT '3' as Indice, ConceitoID as fldID, " +
                            "dbo.fn_Tradutor(Conceito, " + idiomaSistemaId.ToString() + ", " + idiomaEmpresaId.ToString() + ", NULL) as fldName, " +
                            "dbo.fn_Tradutor(Conceito, " + idiomaSistemaId.ToString() + ", " + idiomaEmpresaId.ToString() + ", NULL) AS Ordem2, ' ' AS Filtro " +
                        " FROM Conceitos WITH(NOLOCK) " +
                        " WHERE EstadoID=2 AND TipoConceitoID=302 " +
                        "UNION ALL " +
                        "SELECT '4' as Indice, ItemID as fldID,(ItemAbreviado + CHAR(45) + ItemMasculino) as fldName, STR(Ordem) AS Ordem2, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        " WHERE EstadoID=2 AND TipoID=103 " +
                        "UNION ALL " +
                        "SELECT '5' as Indice, LocalidadeID as fldID, Localidade as fldName, Localidade as Ordem2, ' ' AS Filtro " +
                        " FROM Localidades WITH(NOLOCK) " +
                        " WHERE EstadoID=2 AND TipoLocalidadeID = 203 " +
                        "UNION ALL " +
                        "SELECT '6' AS Indice, ItemID as fldID, ItemMasculino as fldName, STR(Ordem) as Ordem2, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        " WHERE EstadoID=2 AND TipoID=46 " +
                        "UNION ALL " +
                        "SELECT '7' AS Indice, ItemID as fldID, ItemMasculino as fldName, STR(Ordem) as Ordem2, ' ' AS Filtro " +
                        " FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        " WHERE EstadoID=2 AND TipoID=104 " +
                        "UNION ALL " +
                        "SELECT '8' as Indice, LocalidadeID as fldID, Localidade as fldName, Localidade as Ordem2, ' ' AS Filtro " +
                        " FROM Localidades WITH(NOLOCK) " +
                        " WHERE TipoLocalidadeID = 203 " +
                        "ORDER BY Indice,Ordem2";


                        return sql;

                    case 2120: //Form de Rela��es entre Conceitos
                        sql =
                        "SELECT '1' as Indice, TipoRelacaoID as fldID, TipoRelacao as fldName, TipoRelacao AS Ordem2,Sujeito AS NameSuj,Objeto AS NameObj, Filtro AS Filtro " +
                        "FROM TiposRelacoes WITH(NOLOCK) WHERE EstadoID=2 AND RelacaoEntreID=134 " +
                        "ORDER BY Indice,Ordem2";

                        return sql;

                    case 2130: //Form de Rela��es entre Pessoas e Conceitos

                        sql = "SELECT '1' as Indice, TipoRelacaoID as fldID,TipoRelacao as fldName, TipoRelacao AS Ordem2, SPACE(1) AS Ordem3 ,Sujeito AS NameSuj, Objeto AS NameObj, Filtro AS Filtro, SPACE(0) AS Descricao " +
                                   "FROM TiposRelacoes WITH(NOLOCK) " +
                                   "WHERE EstadoID=2 AND RelacaoEntreID=135 " +
                                   "UNION ALL SELECT '2' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                   "FROM Conceitos WITH(NOLOCK) " +
                                   "WHERE EstadoID = 2 AND TipoConceitoID = 307 " +
                                   "AND PaisID = " + empresaPaisId + " " +
                                   "UNION ALL SELECT '3' as Indice, c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, c.SimboloMoeda AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                   "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " +
                                   "WHERE a.SujeitoID = " + empresaId + " " +
                                   "AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 " +
                                   "AND a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID " +
                                   "UNION ALL SELECT '4' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                   "UNION ALL SELECT DISTINCT '4' as Indice, b.CodigoTaxaID AS fldID, LTRIM(RTRIM(STR(b.CodigoTaxaID))) AS fldName, LTRIM(RTRIM(STR(b.CodigoTaxaID))) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                   "FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Moedas b WITH(NOLOCK) " +
                                   "WHERE a.ObjetoID= " + empresaId + " AND a.TipoRelacaoID=21 AND a.EstadoID=2 " +
                                   "AND a.RelacaoID=b.RelacaoID " +
                                   "UNION ALL SELECT Indice, fldID, fldName, Ordem2, Ordem3, NameSuj, NameObj, Filtro, Descricao FROM dbo.fn_staticcmb_ClassificacaoFiscal_tbl (5,  " + empresaPaisId + ") " +
                                   "UNION ALL SELECT '6' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                   "UNION ALL SELECT '6' as Indice, b.ConServicoID AS fldID, LEFT(ISNULL(dbo.fn_Produto_Descricao2(b.ConServicoID, " + empresaPaisId + " , 13),SPACE(0)),20) AS fldName, a.Conceito AS Ordem2, LEFT(ISNULL(dbo.fn_Produto_Descricao2(b.ConServicoID, " + empresaPaisId + ", 13),SPACE(0)),20) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ISNULL(dbo.fn_Produto_Descricao2(b.ConServicoID, " + empresaPaisId + ", 13),SPACE(0)) AS Descricao  " +
                                   "FROM Conceitos a WITH(NOLOCK) " +
                                   "INNER JOIN Conceitos_Servicos b WITH(NOLOCK) ON (a.ConceitoID = b.ConceitoID) " +
                                   "WHERE (a.EstadoID = 2) AND (a.TipoConceitoID = 320) " +
                                   "AND (a.PaisID = " + empresaPaisId + ") " +
                                   "AND (b.LocalidadeID = dbo.fn_Pessoa_Localidade(" + empresaId + " , 3, NULL, NULL)) " +
                                   "AND (a.Observacao LIKE '%|PS|%') " +
                                   "AND (b.EstadoID = 2) " +
                                   "UNION ALL " +
                                   "SELECT '7' as Indice, ItemID AS fldID, ItemAbreviado AS fldName, STR(Ordem) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ItemMAsculino AS Descricao " +
                                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                                   "WHERE TipoID = 114 AND EstadoID = 2 " +
                                   "UNION ALL SELECT '8' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " +
                                   "UNION ALL SELECT '8' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ISNULL(CONVERT(VARCHAR(8000),Observacoes), SPACE(0)) AS Descricao  " +
                                   "FROM Conceitos WITH(NOLOCK) " +
                                   "WHERE EstadoID = 2 AND TipoConceitoID = 319 " +
                                   "ORDER BY Indice, Ordem2, Ordem3";
                        return sql;

                    case 3140:  //Form Or�amentos
                        sql =
                        "SELECT '1' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, ' ' as Filtro " +
                            "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " +
                            "WHERE (a.SujeitoID = " + empresaId.ToString() + " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " +
                            "AND b.MoedaID = c.ConceitoID AND d.RecursoID=999) " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 4210: //Form de Vitrines
                        sql =
                        "SELECT '1' as Indice, STR(Ordem) AS Ordem2,ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 303 AND EstadoID=2 " +
                        "UNION ALL " +
                        "SELECT '2' as Indice, STR(Ordem) AS Ordem2, ItemFeminino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 110 AND EstadoID=2 " +
                        "UNION ALL " +
                        "SELECT '3' as Indice, STR(Ordem) AS Ordem2,ItemFeminino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 108 AND EstadoID=2 " +
                        "UNION ALL " +
                        "SELECT DISTINCT '4' as Indice, Marcas.Conceito AS Ordem2, Marcas.Conceito AS fldName, Marcas.ConceitoID AS fldID, SPACE(0) AS Filtro " +
                        "FROM RelacoesPesCon ProdutosEmpresa, Conceitos Produtos, Conceitos Marcas " +
                            //"WHERE ProdutosEmpresa.SujeitoID=" + empresaId.ToString() + " AND ProdutosEmpresa.EstadoID NOT IN (1, 5) AND " + 
                        "WHERE ProdutosEmpresa.SujeitoID IN (SELECT " + (empresaId.ToString()) + " " +
                        " UNION ALL SELECT EmpresaAlternativaID FROM FopagEmpresas WITH(NOLOCK) WHERE (EmpresaID = " + (empresaId.ToString()) + " AND " +
                        "FuncionarioID IS NULL) AND (EmpresaAlternativaID = dbo.fn_Empresa_Matriz(EmpresaAlternativaID, 1, 1))) AND " +
                        "ProdutosEmpresa.EstadoID NOT IN (1, 5) AND Marcas.EstadoID = 2 AND " +
                        "ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND  " +
                        "Produtos.MarcaID=Marcas.ConceitoID  " +
                        "ORDER BY Indice,Ordem2";

                        return sql;

                    case 4220: //Form A��es de Marketing
                        sql =
                        "SELECT '1' as Indice, ItemMasculino AS fldName, ItemID AS fldID, Observacao " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 304 AND EstadoID=2 ORDER BY Ordem";

                        return sql;

                    case 4230: //Form Email Marketing
                        sql =
                        "SELECT '1' as Indice, a.Vitrine AS fldName, a.VitrineID AS fldID, " +
                        "(CASE WHEN (a.LojaID IS NULL AND a.MarcaID IS NULL) THEN 1 WHEN (a.LojaID IS NOT NULL) THEN b.Ordem ELSE 999 END) AS Ordem1, " +
                        "c.Conceito AS Ordem2 " +
                        "FROM Vitrines a WITH(NOLOCK) " +
                        "LEFT OUTER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.LojaID) " +
                        "LEFT OUTER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.MarcaID) " +
                        "WHERE (a.EstadoID=2 AND a.EmpresaID=" + empresaId.ToString() + ") " +
                        "UNION ALL " +
                        "SELECT '2' as Indice, ItemMasculino AS fldName, ItemID AS fldID, Ordem AS Ordem1, SPACE(0) AS Ordem2 " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID = 306) " +
                        "UNION ALL " +
                        "SELECT '3' as Indice, '' AS fldName, 0 AS fldID, 0 AS Ordem1, SPACE(0) AS Ordem2 " +
                        "UNION ALL " +
                        "SELECT '3' as Indice, ItemMasculino AS fldName, ItemID AS fldID, Ordem AS Ordem1, SPACE(0) AS Ordem2 " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID = 56) " +
                            "AND (Filtro LIKE '%<23300>%') " +
                        "ORDER BY Indice, Ordem1, Ordem2 ";

                        return sql;

                    case 4240: //Form Publicacoes
                        sql =
                        "SELECT '1' as Indice, ItemMasculino AS fldName, ItemID AS fldID " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 311 AND EstadoID=2 ORDER BY Ordem";

                        return sql;


                    case 4410: //Form de Novidades
                        sql =
                        "SELECT '1' as Indice, STR(Ordem) AS Ordem2,ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 301 AND EstadoID=2 " +
                        "ORDER BY Indice,Ordem2";

                        return sql;

                    case 4420: //Form de Eventos
                        sql =
                        "SELECT '1' as Indice, STR(Ordem) AS Ordem2,ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 302 AND EstadoID=2 " +
                        "ORDER BY Indice,Ordem2";

                        return sql;

                    case 5110: //Form de Pedidos
                        sql =
                        "SELECT '1' as Indice, ItemID as fldID, ItemMasculino as fldName, Ordem as Ordem2, 0 AS Cotacao, 0 AS TaxaVenda, Filtro AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE EstadoID=2 AND TipoID=402 " +
                        "UNION ALL " +
                        "SELECT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, b.Ordem as Ordem2, " +
                        "dbo.fn_Preco_Cotacao(d.MoedaID,c.ConceitoID,NULL,GETDATE()) AS Cotacao, e.TaxaVenda AS TaxaVenda, ' ' AS Filtro " +
                        "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec_Financ e WITH(NOLOCK) " +
                        "WHERE (a.SujeitoID IN (" + empresaId.ToString() + ", 7) AND a.ObjetoID=999 AND a.TipoRelacaoID=12 AND a.RelacaoID = b.RelacaoID AND b.Faturamento=1 " +
                        "AND b.MoedaID = c.ConceitoID " +
                        "AND d.RecursoID=999 AND a.RelacaoID=e.RelacaoID AND b.MoedaID=e.MoedaID) " +
                        "UNION ALL " +
                        "SELECT '6' as Indice, NumeroItens as fldID, 'NumeroItens' as fldName, 0 as Ordem2, 0 AS Cotacao, 0 AS TaxaVenda, ' ' AS Filtro " +
                        "FROM RelacoesPesRec WITH(NOLOCK) " +
                        "WHERE (SujeitoID = " + empresaId.ToString() + " AND ObjetoID=999 AND TipoRelacaoID=12) " +
                        "UNION ALL " +
                        "SELECT '7' as Indice, NumeroParcelas as fldID, 'NumeroParcelas' as fldName, 0 as Ordem2, 0 AS Cotacao, 0 AS TaxaVenda, ' ' AS Filtro " +
                        "FROM RelacoesPesRec WITH(NOLOCK) " +
                        "WHERE (SujeitoID = " + empresaId.ToString() + " AND ObjetoID=999 AND TipoRelacaoID=12) " +
                        "UNION ALL " +
                        "SELECT '8' as Indice, DiasBase as fldID, 'DiasBase' as fldName, 0 as Ordem2, 0 AS Cotacao, 0 AS TaxaVenda, ' ' AS Filtro " +
                        "FROM RelacoesPesRec WITH(NOLOCK) " +
                        "WHERE (SujeitoID = " + empresaId.ToString() + " AND ObjetoID=999 AND TipoRelacaoID=12) " +
                        "ORDER BY Indice, Ordem2 ";

                        return sql;

                    case 5120:
                        sql = "SELECT '1' as Indice, STR(Ordem) AS Ordem2, ItemAbreviado as fldName, ItemID as fldID, ' ' AS Filtro " +
                                "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                                "WHERE EstadoID=2 AND TipoID=434 " +
                                "ORDER BY Ordem ";

                        return sql;


                    case 5140: //Form de Opera��es
                        sql =
                            "SELECT '1' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, Filtro AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 407 AND EstadoID=2 " +
                        "UNION ALL " +
                        "SELECT '2' AS Indice,SPACE(0) AS Ordem2,SPACE(0) AS fldName,0 AS fldID, ' ' AS Filtro " +
                        "UNION ALL SELECT '2' AS Indice, Localidade AS Ordem2, Localidade AS fldName, LocalidadeID as fldID, ' ' AS Filtro " +
                        "FROM Localidades WITH(NOLOCK) " +
                        "WHERE EstadoID=2 AND TipoLocalidadeID=203 " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 408 AND EstadoID=2 " +
                        "UNION ALL " +
                        "SELECT '4' AS Indice,STR(Ordem) AS Ordem2,ItemAbreviado AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 107 AND EstadoID=2 " +
                        "UNION ALL " +
                        "SELECT '5' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 115 AND EstadoID=2 AND Filtro LIKE '%<Custo>%' " +
                        "UNION ALL " +
                        "SELECT '6' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE TipoID = 433 AND EstadoID=2 " +
                        "UNION ALL " +
                        "SELECT '7' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldID, ' ' AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE EstadoID=2 AND TipoID=115 AND Filtro LIKE '%<ValorUnitario>%' " +
                        "UNION ALL " +
                        "SELECT '8' AS Indice,SPACE(0) AS Ordem2,SPACE(0) AS fldName,0 AS fldID, ' ' AS Filtro " +
                        "UNION ALL SELECT '8' AS Indice,HistoricoPadrao AS Ordem2,HistoricoPadrao AS fldName,HistoricoPadraoID AS fldID, ' ' AS Filtro " +
                        "FROM HistoricosPadrao WITH(NOLOCK) " +
                        "WHERE EstadoID=2 " +
                        "ORDER BY Indice, Ordem2";

                        return sql;

                    case 5145: // Form de Regras Fiscais
                        sql = "SELECT '1' as Indice, Ordem, ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID=426) " +
                        "ORDER BY Indice, Ordem";

                        return sql;

                    case 5160: //Form de Campanhas
                        sql =
                        "SELECT DISTINCT '1' as Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem, SPACE(0) as Ordem2, Filtro " +
                        "FROM tiposauxiliares_itens WITH(NOLOCK) " +
                        "WHERE Tipoid=812 and Aplicar=1 and Estadoid=2 " +
                        "UNION ALL " +
                        "SELECT DISTINCT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, 0 AS Ordem, c.SimboloMoeda as Ordem2, ' ' AS Filtro " +
                        "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec_Financ e WITH(NOLOCK) " +
                        "WHERE (a.ObjetoID=999 AND a.TipoRelacaoID=12 AND a.RelacaoID = b.RelacaoID " +
                        "AND b.MoedaID = c.ConceitoID AND d.RecursoID=999 AND a.RelacaoID=e.RelacaoID AND b.MoedaID=e.MoedaID) " +
                        "UNION ALL " +
                        "SELECT DISTINCT '3' as Indice, 1 as fldID, 'Dia' as fldName, 1 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " +
                        "UNION ALL " +
                        "SELECT DISTINCT '3' as Indice, 2 as fldID, 'Semana' as fldName, 2 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " +
                        "UNION ALL " +
                        "SELECT DISTINCT '3' as Indice, 3 as fldID, 'M�s' as fldName, 3 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " +
                        "UNION ALL " +
                        "SELECT DISTINCT '3' as Indice, 4 as fldID, 'Quarter' as fldName, 4 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " +
                        "UNION ALL " +
                        "SELECT DISTINCT '3' as Indice, 5 as fldID, 'Ano' as fldName, 5 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " +
                        "ORDER BY Indice, Ordem, Ordem2";
                        return sql;

                    case 5220:  //Form de Lista de Preco
                        sql =
                        "SELECT '1' as Indice, 0 AS fldID, ' ' AS fldName, ' ' AS Ordem2, 0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " +
                        "UNION ALL SELECT DISTINCT '1' as Indice, UFs.LocalidadeID AS fldID, UFs.CodigoLocalidade2 AS fldName, UFs.CodigoLocalidade2 as Ordem2, " +
                        "0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " +
                        "FROM Conceitos Impostos WITH(NOLOCK), Conceitos_Aliquotas Aliquotas WITH(NOLOCK), Pessoas_Enderecos EmpresaEndereco WITH(NOLOCK), Localidades UFs WITH(NOLOCK) " +
                        "WHERE Impostos.TipoConceitoID=306 AND Impostos.IncidePreco=1 AND Impostos.InclusoPreco=1 AND " +
                        "Impostos.AlteraAliquota=1 AND Impostos.DestacaNota=1 AND " +
                        "Impostos.ConceitoID=Aliquotas.ImpostoID AND EmpresaEndereco.PessoaID= " + empresaId.ToString() + " " +
                        "AND EmpresaEndereco.UFID=Aliquotas.LocalidadeOrigemID " +
                        "AND LocalidadeDestinoID=UFs.LocalidadeID AND EmpresaEndereco.Ordem = 1 " +
                        "UNION ALL " +
                        "SELECT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, " +
                        "dbo.fn_Preco_Cotacao(d.MoedaID,c.ConceitoID,-a.SujeitoID,GETDATE()) AS Cotacao, " +
                        "a.DiasBase as DiasBase, e.TaxaVenda as TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " +
                        "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec_Financ e WITH(NOLOCK) " +
                        "WHERE (a.SujeitoID = " + empresaId.ToString() + " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " +
                        "AND b.MoedaID = c.ConceitoID AND d.RecursoID=999) " +
                        "AND a.RelacaoID = e.RelacaoID AND e.MoedaID = c.ConceitoID " +
                        "UNION ALL " +
                        "SELECT '3' as Indice, c.FinanciamentoID as fldID, " +
                        "(CONVERT(VARCHAR(50), CONVERT(INT, ROUND(dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL),2))) + SPACE(1) + CHAR(40) + c.Financiamento + CHAR(41)) AS fldName, " +
                        "STR(c.Ordem) as Ordem2, " +
                        "NULL AS Cotacao, NULL AS DiasBase, NULL AS TaxaVenda, 0 AS Aliquota, " +
                        "dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL) AS PMP, c.NumeroParcelas AS NumeroParcelas " +
                        "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_FinanciamentosPadrao b WITH(NOLOCK), FinanciamentosPadrao c WITH(NOLOCK) " +
                        "WHERE (a.SujeitoID = " + empresaId.ToString() + " AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 AND a.RelacaoID = b.RelacaoID " +
                        "AND b.EstadoID = 2 AND b.FinanciamentoID = c.FinanciamentoID AND c.FinanciamentoID <> 1) " +
                        "UNION ALL " +
                        "SELECT DISTINCT '4' as Indice, 0 AS fldID, SPACE(0) AS fldName, '-1' AS Ordem2, 0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " +
                        "UNION ALL " +
                        "SELECT DISTINCT '4' as Indice, 1 AS fldID, 'Pago' AS fldName, '-2' AS Ordem2, 0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " +
                        "UNION ALL " +
                        "SELECT DISTINCT '4' as Indice, ItemID AS fldID, ItemMasculino AS fldName, LTRIM(RTRIM(STR(Ordem))) AS Ordem2, 0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " +
                        "FROM tiposauxiliares_itens WITH(NOLOCK) " +
                        "WHERE Tipoid=405 and Aplicar=1 and Estadoid=2  AND Filtro= '%<N>% '" +
                        "UNION ALL " +
                        "SELECT '5' as Indice, EmpresaAlternativaID as fldID, SPACE(0) as fldName, STR(0) as Ordem2, " +
                        "0 AS Cotacao, 0 as DiasBase, 0 as TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " +
                        "FROM RelacoesPesRec WITH(NOLOCK) " +
                        "WHERE (SujeitoID = " + empresaId.ToString() + " AND ObjetoID=999) " +
                        "ORDER BY Indice, Ordem2";

                        return sql;

                    case 5310: // Form Importacao
                        sql = "SELECT '1' AS Indice, LocalidadeID AS fldID, CodigoLocalidade2 AS fldName, 0 AS Ordem " +
                            "FROM Localidades WITH(NOLOCK) " +
                            "WHERE TipoLocalidadeID = 204 and EstadoID = 2 AND LocalizacaoID = " + empresaPaisId + " " +
                            "UNION ALL " +
                            "SELECT '2' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE TipoID = 420 and EstadoID = 2 " +
                            "UNION ALL " +
                            "SELECT '3' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE TipoID = 405 and EstadoID = 2 and filtro like '%<I>%' " +
                            "UNION ALL " +
                            "SELECT '4' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE TipoID = 435 and EstadoID = 2 " +
                            "UNION ALL " +
                            "SELECT '5' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE TipoID = 438 and EstadoID = 2 " +
                            "UNION ALL " +
                            "SELECT '6' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE TipoID = 436 and EstadoID = 2 " +
                            "ORDER BY Indice, Ordem";

                        return sql;



                    case 5330: // Form invoices
                        sql = "SELECT DISTINCT '1' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, 0 AS Ordem, c.SimboloMoeda as Ordem2, ' ' AS Filtro " +
                                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec_Financ e WITH(NOLOCK) " +
                                    "WHERE ( a.SujeitoID = " + empresaId.ToString() + " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " +
                                        "AND b.MoedaID = c.ConceitoID AND d.RecursoID=999 AND a.RelacaoID=e.RelacaoID AND b.MoedaID=e.MoedaID) " +
                                "UNION ALL " +
                                "SELECT '2' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem, ' ' as Ordem2, ' ' AS Filtro " +
                                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                                    "WHERE TipoID = 420 and EstadoID = 2 ";
                        return sql;

                    case 5410: // Form Documentos de Transporte
                        sql = "SELECT '1' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem " +
                                    "FROM TiposAuxiliares_Itens WITH (NOLOCK) " +
                                    "WHERE EstadoID=2 AND TipoID=428 " +
                            "UNION ALL " +
                            "SELECT '2' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem " +
                                    "FROM TiposAuxiliares_Itens WITH (NOLOCK) " +
                                    "WHERE EstadoID=2 AND TipoID=422 " +
                            "UNION ALL " +
                            "SELECT '3' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem " +
                                    "FROM TiposAuxiliares_Itens WITH (NOLOCK) " +
                                    "WHERE EstadoID=2 AND TipoID=403 " +
                            "UNION ALL " +
                            "SELECT '4' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem " +
                                    "FROM TiposAuxiliares_Itens WITH (NOLOCK) " +
                                    "WHERE EstadoID=2 AND TipoID=405  AND Filtro like '%<N>%'" +
                            "ORDER BY Indice, Ordem";

                        return sql;

                    case 7110: // Form Documentos
                        sql =
                        "SELECT '1' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem AS Ordem2, SPACE(0) AS Ordem3, ' ' AS Filtro, SPACE(0) AS Hint " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID=601) " +
                        "UNION ALL " +
                        "SELECT '2' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, ItemID AS Ordem2, SPACE(0) AS Ordem3, ' ' AS Filtro, ItemMasculino AS Hint " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID=602) " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem2, SPACE(0) AS Ordem3, ' ' AS Filtro, SPACE(0) AS Hint " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice, b.RecursoID AS fldID, (a.RecursoFantasia + CHAR(47) + b.RecursoFantasia) AS fldName, 0 AS Ordem2, (a.RecursoFantasia + CHAR(47) + b.RecursoFantasia) AS Ordem3, ' ' AS Filtro, SPACE(0) AS Hint " +
                        "FROM Recursos a WITH(NOLOCK), Recursos b WITH(NOLOCK) " +
                        "WHERE (a.EstadoID=2 AND a.TipoRecursoID=2 AND a.ClassificacaoID=11 AND a.RecursoID=b.RecursoMaeID AND " +
                        "b.EstadoID=2 AND b.TipoRecursoID=3) " +
                        "ORDER BY Indice, Ordem2, Ordem3";

                        return sql;

                    case 7120:  //Form RIQ
                        sql =
                        "SELECT '1' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, SPACE(0) AS Ordem3, Filtro AS Filtro, SPACE(0) AS Hint " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID=606) " +
                        "UNION ALL " +
                        "SELECT '2' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, ItemID AS Ordem2, SPACE(0) AS Ordem3, Filtro AS Filtro, ItemMasculino AS Hint " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID=602) " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem2, '0' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice, 1 AS fldID, 'Mensal' AS fldName, 1 AS Ordem2, '1' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice, 2 AS fldID, 'Bimestral' AS fldName, 2 AS Ordem2, '2' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice, 3 AS fldID, 'Trimestral' AS fldName, 3 AS Ordem2, '3' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice, 6 AS fldID, 'Semestral' AS fldName, 6 AS Ordem2, '6' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " +
                        "UNION ALL " +
                        "SELECT '3' AS Indice, 12 AS fldID, 'Anual' AS fldName, 12 AS Ordem2, '12' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " +
                        "ORDER BY Indice, Ordem2, Ordem3";

                        return sql;

                    case 7140: //Form RRC
                        sql =
                        "SELECT '1' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, SPACE(0) AS Ordem3, Filtro AS Filtro, SPACE(0) AS Hint " +
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                        "WHERE (EstadoID=2 AND TipoID=605) " +
                        "ORDER BY Indice, Ordem2, Ordem3";

                        return sql;

                    case 7170: //Form RACP
                        sql =
                        "SELECT '1' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, SPACE(0) AS Ordem3, Filtro AS Filtro, SPACE(0) AS Hint " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE (EstadoID=2 AND TipoID=603) " +
                            "ORDER BY Indice, Ordem2, Ordem3";

                        return sql;

                    case 9110:  //Form Financeiro
                        sql =
                         "SELECT '1' as Indice, a.ItemID as fldID, a.ItemAbreviado as fldName, STR(a.Ordem) as Ordem2, ' ' as Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " +
                    "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
                    "WHERE (a.EstadoID = 2 AND TipoID=804) " +
                    "UNION ALL " +
                    "SELECT DISTINCT '2' as Indice, a.HistoricoPadraoID as fldID, a.HistoricoPadrao as fldName, a.HistoricoPadrao as Ordem2, ' ' as Filtro, " +
                    "a.TipoLancamentoID, " +
                    "CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.TipoLancamentoID = 1571 AND (aa.Pedido = 1 OR aa.Financeiro = 1)))) AS Pagamento, " +
                    "(CASE WHEN (a.HistoricoPadraoID IN (2,4)) THEN 1 ELSE CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.TipoLancamentoID = 1572 AND (aa.Pedido = 1 OR aa.Financeiro = 1)))) END) AS Recebimento, " +
                    "(CASE WHEN (a.HistoricoPadraoID IN (2,4)) THEN 1 ELSE CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.TipoLancamentoID = 1573 AND (aa.Pedido = 1 OR aa.Financeiro = 1)))) END) AS Importe, " +
                    "dbo.fn_HistoricoPadrao_TipoDetalhamento(a.HistoricoPadraoID, 1571, 1543) AS ImpostoPagamento, " +
                    "dbo.fn_HistoricoPadrao_TipoDetalhamento(a.HistoricoPadraoID, 1572, 1543) AS ImpostoRecebimento, " +
                    "dbo.fn_HistoricoPadrao_TipoDetalhamento(a.HistoricoPadraoID, 1573, 1543) AS ImpostoImporte, " +
                    "CONVERT(BIT, ISNULL(a.Marketing, 0)) AS Marketing " +
                    "FROM HistoricosPadrao a WITH(NOLOCK), HistoricosPadrao_Contas b WITH(NOLOCK) " +
                    "WHERE (a.EstadoID = 2 AND ((a.HistoricoPadraoID = b.HistoricoPadraoID AND (b.Pedido = 1 OR b.Financeiro = 1)) OR (a.HistoricoPadraoID IN (2,4)))) " +
                    "UNION ALL " +
                    "SELECT '3' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, ' ' as Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " +
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " +
                    "WHERE (a.SujeitoID = " + empresaId.ToString() + " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " +
                    "AND b.MoedaID = c.ConceitoID AND b.Faturamento = 1 " +
                    "AND d.RecursoID=999) " +
                    "UNION ALL " +
                    "SELECT '4' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, ' ' as Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " +
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " +
                    "WHERE (a.SujeitoID = " + empresaId.ToString() + " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " +
                    "AND b.MoedaID = c.ConceitoID " +
                    "AND d.RecursoID=999) " +
                    "UNION ALL " +
                    "SELECT '5' as Indice, c.RelPesContaID as fldID, f.Codigo as fldName, f.Banco as Ordem2, ' ' AS Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " +
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Cobranca b WITH(NOLOCK), RelacoesPessoas_Contas c WITH(NOLOCK), RelacoesPessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK), Bancos f WITH(NOLOCK) " +
                    "WHERE a.TipoRelacaoID = 12 AND a.ObjetoID=999 AND a.SujeitoID= " + empresaId.ToString() + " " +
                    "AND a.RelacaoID=b.RelacaoID AND b.EstadoID=2 AND b.RelPesContaID = c.RelPesContaID " +
                    "AND c.RelacaoID = d.RelacaoID AND d.EstadoID = 2 AND d.ObjetoID = e.PessoaID " +
                    "AND e.EstadoID = 2 AND e.BancoID = f.BancoID AND f.EstadoID = 2 " +
                    "UNION ALL " +
                    "SELECT '6' as Indice, 0 as fldID, LTRIM(STR(a.DiasBase)) as fldName, ' ' as Ordem2, ' ' as Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " +
                    "FROM RelacoesPesRec a WITH(NOLOCK) " +
                    "WHERE (a.TipoRelacaoID = 12 AND a.ObjetoID = 999 AND a.SujeitoID = " + empresaId.ToString() + ") " +
                    "UNION ALL " +
                    "SELECT '7' as Indice, 0 as fldID, SPACE(0) as fldName, SPACE(0) as Ordem2, ' ' AS Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " +
                    "UNION ALL " +
                    "SELECT '7' as Indice, ConceitoID as fldID, Imposto as fldName, Imposto as Ordem2, ' ' AS Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " +
                    "FROM Conceitos WITH(NOLOCK) " +
                    "WHERE (TipoConceitoID = 306 AND EstadoID = 2 AND PaisID = " + empresaPaisId.ToString() + ") " +
                    " UNION ALL " +
                    " SELECT '8' AS Indice, 0 AS fldID, SPACE(0) AS fldName, SPACE(0) AS Ordem2, ' ' AS Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento,  NULL AS Recebimento, NULL AS Importe," +
                    " NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing" +
                    " UNION ALL " +
                    " SELECT '8' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, ItemMasculino AS Ordem2, ' ' AS Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, " +
                    " NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " +
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                    "WHERE (TipoID  = 814 AND EstadoID = 2)" +
                    "ORDER BY Indice, Ordem2";
                        return sql;

                    case 9130:  //Form Valores a Localizar
                        sql =
                         "SELECT '1' as Indice, a.ItemID as fldID, a.ItemAbreviado as fldName, SPACE(0) AS Ordem, a.Ordem as Ordem2, Filtro AS Filtro , 0 AS Inc " +
                                "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
                                "WHERE (a.EstadoID = 2 AND TipoID=804 AND Filtro LIKE '%(9130)%') " +
                                "UNION ALL " +
                                "SELECT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, SPACE(0) AS Ordem, b.Ordem as Ordem2, ' ' as Filtro , 0 AS Inc " +
                                "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " +
                                "WHERE (a.SujeitoID = " + empresaId + " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " +
                                "AND b.MoedaID = c.ConceitoID AND b.Faturamento = 1 " +
                                "AND d.RecursoID=999) " +
                                "UNION ALL " +
                                "SELECT '3' as Indice, c.RelPesContaID as fldID, dbo.fn_ContaBancaria_Nome(c.RelPesContaID, 4) as fldName, " +
                                "dbo.fn_ContaBancaria_Nome(c.RelPesContaID, 4) as Ordem, 0 AS Ordem2,  ' ' AS Filtro , 0 AS Inc " +
                                "FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), RelacoesPessoas_Contas c WITH(NOLOCK), Bancos d WITH(NOLOCK) " +
                                "WHERE a.SujeitoID= " + empresaId + " AND a.TipoRelacaoID = 24 AND a.ObjetoID=b.PessoaID AND " +
                                "b.EstadoID = 2 AND a.RelacaoID=c.RelacaoID AND c.EstadoID = 2 AND b.BancoID = d.BancoID AND d.EstadoID = 2 " +
                                "UNION ALL " +
                                "SELECT '4' as Indice, a.ItemID as fldID, a.ItemMasculino as fldName, SPACE(0) AS Ordem, a.Ordem as Ordem2, Filtro AS Filtro " +
                                ", (CASE WHEN Filtro LIKE '%<INC>%' THEN 1 ELSE 0 END) AS Inc " +
                                "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
                                "WHERE (TipoID=821) " +
                                "ORDER BY Indice, Ordem, Ordem2";

                        return sql;

                    case 9150:  //Form Depositos Bancarios
                        sql =
                        "SELECT '1' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, ' ' as Filtro " +
                            "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " +
                            "WHERE (a.SujeitoID = " + empresaId.ToString() + " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " +
                            "AND b.MoedaID = c.ConceitoID AND b.Faturamento = 1 " +
                            "AND d.RecursoID=999) " +
                            "UNION ALL " +
                            "SELECT '2' as Indice, b.RelPesContaID as fldID, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as fldName, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as Ordem2, ' ' AS Filtro " +
                            "FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Contas b WITH(NOLOCK) " +
                            "WHERE (a.TipoRelacaoID = 24 AND a.SujeitoID = " + empresaId.ToString() + " AND a.EstadoID = 2 AND " +
                            "a.RelacaoID = b.RelacaoID AND b.TipoContaID = 1506 AND b.EstadoID=2) " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 9160:  //Form Cr�ditos
                        sql = "SELECT '1' AS Indice, 0 AS fldID, SPACE(0) AS fldName, SPACE(0) AS Ordem2, SPACE(0) AS Filtro UNION ALL " +
                        "SELECT DISTINCT TOP 50 '1' AS Indice, a.PessoaID AS fldID,  a.Fantasia AS fldName, a.Fantasia AS Ordem2,  ' ' AS Filtro " + 
                            "FROM Pessoas a WITH(NOLOCK) " +
                            "WHERE a.EstadoID = 2 AND a.ClassificacaoID = 70 AND a.Observacoes LIKE '%<SeguradoraCredito%'";

                        return sql;

                    case 9210:  //Form Cobranca
                        sql =
                        "SELECT '1' as Indice, b.RelPesContaID as fldID, f.Codigo as fldName, f.Banco as Ordem2, ' ' AS Filtro " +
                            "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Cobranca b WITH(NOLOCK), RelacoesPessoas_Contas c WITH(NOLOCK), RelacoesPessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK), Bancos f WITH(NOLOCK) " +
                            "WHERE a.TipoRelacaoID = 12 AND a.ObjetoID=999 AND a.SujeitoID= " + empresaId.ToString() + " " +
                            "AND a.RelacaoID=b.RelacaoID AND b.RelPesContaID = c.RelPesContaID AND c.RelacaoID = d.RelacaoID " +
                            "AND d.ObjetoID = e.PessoaID AND e.BancoID = f.BancoID " +
                            "UNION ALL SELECT '2' as Indice, ItemID AS fldID, ItemMasculino as fldName, STR(Ordem) as Ordem2, Filtro AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID=809 AND EstadoID=2 " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 9220:  //Form Bancos
                        sql =
                        "SELECT '1' as Indice, a.LocalidadeID as fldID, a.Localidade as fldName, a.Localidade as Ordem2, ' ' AS Filtro " +
                            "FROM Localidades a WITH(NOLOCK) " +
                            "WHERE a.TipoLocalidadeID = 203 AND a.EstadoID=2 " +
                            "ORDER BY Indice, Ordem2";

                        return sql;



                    case 9230:  //Form Bancos
                        sql =
                        "SELECT DISTINCT '1' as Indice, b.ObjetoID AS fldID, a.Fantasia AS fldName" +
                        " FROM Pessoas a WITH(NOLOCK)" +
                        "  INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.ObjetoID = a.PessoaID)" +
                        " WHERE ((a.TipoPessoaID = 55) AND (a.EstadoID = 2)" +
                        " AND (dbo.fn_TagValor(a.Observacoes, 'ProgramaMarketing' , '<', ',', 1) LIKE 'MARKETPLACE')" +
                        " AND (b.TipoRelacaoID = 35) AND (b.EstadoID = 2))" +
                        " UNION ALL" +
                        " SELECT DISTINCT '1' as Indice, a.ObjetoID AS fldID, c.Fantasia AS fldName" +
                        " FROM RelacoesPessoas a WITH(NOLOCK)" +
                        " INNER JOIN RelacoesPesRec b WITH(NOLOCK) ON (b.SujeitoID = a.SujeitoID)" +
                        " INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.ObjetoID)" +
                        " WHERE (a.EstadoID = 2) AND (a.TipoRelacaoID = 21) AND (b.EstadoID = 2) AND (b.TipoRelacaoID = 12)" +
                        "   AND (dbo.fn_TagValor(c.Observacoes, 'AdquirenteFinanceiro' , '<', ',', 1) LIKE '1')" +
                        " ORDER BY Indice";
                        return sql;

                    case 10110: //Form PlanoContas
                        sql =
                        "SELECT '1' as Indice, 0 as fldID, SPACE(0) as fldName, 0 as Ordem2, ' ' AS Filtro " +
                            "UNION ALL " +
                            "SELECT '1' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE (EstadoID=2 AND TipoID=901) " +
                            "UNION ALL " +
                            "SELECT '2' as Indice, 0 as fldID, SPACE(0) as fldName, 0 as Ordem2, ' ' AS Filtro " +
                            "UNION ALL " +
                            "SELECT '2' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE (EstadoID=2 AND TipoID=903) " +
                            "UNION ALL " +
                            "SELECT '3' as Indice, 0 as fldID, SPACE(0) as fldName, 0 as Ordem2, ' ' AS Filtro " +
                            "UNION ALL " +
                            "SELECT '3' as Indice, a.ContaID as fldID, CONVERT(VARCHAR(10), a.ContaID) as fldName, a.ContaID as Ordem2, ' ' AS Filtro " +
                            "FROM PlanoContas a WITH(NOLOCK) " +
                            "WHERE a.EstadoID = 2 " +
                            "UNION ALL " +
                            "SELECT '4' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE (EstadoID=2 AND TipoID=904) " +
                            "UNION ALL " +
                            "SELECT '5' as Indice, 0 as fldID, SPACE(0) as fldName, 0 as Ordem2, ' ' AS Filtro " +
                            "UNION ALL " +
                            "SELECT '5' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE (EstadoID=2 AND TipoID=905) " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 10120: //Form Hist�ricos Padr�o
                        sql = "SELECT '1' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem AS Ordem2, ' ' AS Filtro " +
                               "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                               "WHERE (EstadoID=2 AND TipoID=907) " +
                               "UNION ALL " +
                               "SELECT '2' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " +
                               "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                               "WHERE (EstadoID=2 AND TipoID=909) ";

                        return sql;

                    case 10130: //Form Lancamentos
                        sql =
                        "SELECT '1' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem AS Ordem2, ' ' AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
                            "WHERE (EstadoID=2 AND TipoID=907) " +
                            "UNION ALL " +
                            "SELECT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, b.Ordem as Ordem2, ' ' AS Filtro " +
                            "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " +
                            "WHERE (a.SujeitoID = " + empresaId.ToString() + " AND a.ObjetoID=999 AND a.TipoRelacaoID=12 AND a.RelacaoID = b.RelacaoID AND b.Faturamento=1 " +
                            "AND b.MoedaID = c.ConceitoID) " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 10140:  //Form ExtratosBancarios
                        sql =
                        "SELECT '1' as Indice, b.RelPesContaID as fldID, " +
                            "dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as fldName, " +
                            "dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as Ordem2, ' ' AS Filtro " +
                            "FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Contas b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Bancos d WITH(NOLOCK) " +
                            "WHERE a.TipoRelacaoID = 24 AND a.EstadoID = 2 AND a.SujeitoID= " + empresaId.ToString() + " " +
                            "AND a.RelacaoID=b.RelacaoID AND b.EstadoID = 2 AND a.ObjetoID = c.PessoaID AND c.EstadoID = 2 " +
                            "AND c.BancoID = d.BancoID AND d.EstadoID = 2 " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 12210:  //Form Competencias
                        sql =
                        "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1101 AND Aplicar = 1 " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 12220:  //Form RET
                        sql =
                        "SELECT '1' AS Indice, Competencia AS Ordem2, Competencia AS fldName, CompetenciaID AS fldID, SPACE(0) AS Filtro " +
                            "FROM Competencias WITH(NOLOCK) WHERE EstadoID = 2 AND TipoCompetenciaID = 1704 " +
                            "UNION ALL SELECT '2' as Indice, SPACE(0) AS Ordem2, SPACE(0) AS fldName, 0 AS fldID,  SPACE(0) AS Filtro  " +
                            "UNION ALL SELECT '2' as Indice, c.SimboloMoeda AS Ordem2, c.SimboloMoeda AS fldName, c.ConceitoID AS fldID,  SPACE(0) AS Filtro  " +
                            "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " +
                            "WHERE a.SujeitoID = " + empresaId.ToString() + " " +
                            "AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 " +
                            "AND a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 12320:  //Form Avaliacoes
                        sql =
                        "SELECT '1' AS Indice, CONVERT(VARCHAR,Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1201 " +
                            "UNION ALL " +
                            "SELECT '2' AS Indice, RecursoFantasia AS Ordem2, RecursoFantasia AS fldName, RecursoID AS fldID, SPACE(0) AS Filtro " +
                            "FROM Recursos WITH(NOLOCK) WHERE (EstadoID BETWEEN 1 AND 2) AND TipoRecursoID=6 AND EhCargo=1 " +
                            "UNION ALL " +
                            "SELECT '3' AS Indice, b.Fantasia AS Ordem2, b.Fantasia AS fldName, b.PessoaID AS fldID, SPACE(0) AS Filtro " +
                            "FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " +
                            "WHERE a.ObjetoID=" + empresaId.ToString() + " AND a.TipoRelacaoID=31 AND a.EstadoID=2 AND a.SujeitoID=b.PessoaID " +
                            "ORDER BY Indice, Ordem2";

                        return sql;

                    case 12510:  //Form Fopag
                        sql = "SELECT '1' AS Indice, Ordem AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, NULL AS Filtro " +
                               "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1001 " +
                               "ORDER BY Indice, Ordem2";

                        return sql;

                    case 12530:  //Form Verbas
                        sql =
                        "SELECT '1' AS Indice, ItemMasculino AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1002 " +
                            "UNION ALL SELECT '2' AS Indice, ItemMasculino AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1003 " +
                            "UNION ALL SELECT '3' AS Indice, ItemMasculino AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " +
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1004 " +
                            "UNION ALL SELECT '4' AS Indice, CONVERT(VARCHAR(10),ContaID) AS Ordem2, Conta AS fldName, ContaID AS fldID, SPACE(0) AS Filtro " +
                            "FROM  PlanoContas WITH(NOLOCK) WHERE EstadoID=2 and dbo.fn_Conta_AceitaLancamento(Contaid) = 1 " +
                            "UNION ALL SELECT '5' AS Indice, SPACE(0) AS Ordem2, SPACE(0) AS fldName, 0 AS fldID, SPACE(0) AS Filtro " +
                            "UNION ALL SELECT '5' AS Indice, Imposto AS Ordem2, Imposto AS fldName, ConceitoID AS fldID, SPACE(0) AS Filtro " +
                            "FROM Conceitos WITH(NOLOCK) " +
                            "WHERE EstadoID=2 AND TipoConceitoID=306 AND PaisID=" + empresaPaisId.ToString() + " " +
                            "ORDER BY Indice, Ordem2";

                        return sql;
                }

                return null;
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(DataInterfaceObj.getRemoteData(Sql));
        }
    }
}
