using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSOverflyRDS;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class gerarpedidocompra : System.Web.UI.OverflyPage
    {
        private Integer userId;
        private Integer depositoId;
        private Integer empresaId;
        private Integer pessoaId;
        private Integer transacaoId;
        private Integer financiamentoId;
        private Integer encomenda;
        private Integer consolidar;
        private int PedItemCompraID;
        private bool nCriaProdutoLote;
        private int nNovoLoteID;
        private int nConsolidar;

        private string Resultado;
        private string[] Descricao;
        private java.lang.Double[] custo;
        private double[] valorinterno;
        private Integer[] produtoId;
        private Integer[] quantidade;        
        private Integer[] LoteID;
        private Integer[] internacional;
        private Integer geraPedidoNacional;
        private Integer geraPedidoInternacional;
        private string ProjetoID;
        private int LotPedItemID;
        private Integer[] valoricmsstinterno;
        private Integer[] pedItemVendaID;
        private int loteID;

        private Int32[] pedidoId = new Int32[0];

        public Integer nUserID
        {
            set { userId = value; }
        }

        public Integer nDepositoID
        {
            set { depositoId = value; }
        }

        public Integer nEmpresaID
        {
            set { empresaId = value; }
        }

        public Integer nPessoaID
        {
            set { pessoaId = value; }
        }

        public Integer nTransacaoID
        {
            set { transacaoId = value; }
        }

        public Integer nFinanciamentoID
        {
            set { financiamentoId = value; }
        }

        public Integer nEncomenda
        {
            set { encomenda = value; }
        }

        public Integer[] nProdutoID
        {
            set { produtoId = value; }
        }

        public Integer[] nPedItemVendaID
        {
            set { pedItemVendaID = value; }
        }

        public Integer[] nQuantidade
        {
            set { quantidade = value; }
        }

        public Integer[] nLoteID
        {
            set { LoteID = value; }
        }
        public Integer[] nInternacional
        {
            set { internacional = value; }
        }
        public Integer nGeraPedidoNacional
        {
            set { geraPedidoNacional = value; }
        }
        public Integer nGeraPedidoInternacional
        {
            set { geraPedidoInternacional = value; }
        }
        public java.lang.Double[] nCusto
        {
            set { custo = value; }
        }
        public string[] sDescricao
        {
            set { Descricao = value; }
        }
        public string sProjetoID
        {
            set { ProjetoID = value; }
        }
       
        // Executa procedure sp_Pedido_Gerador
        protected void PedidoGerador(bool pedidoInternacional)
        {
            // Roda a procedure sp_Pedido_Gerador
            ProcedureParameters[] procParams = new ProcedureParameters[23];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                 empresaId.intValue());

            procParams[1] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                pessoaId.intValue());

            procParams[2] = new ProcedureParameters(
                "@ParceiroID",
                System.Data.SqlDbType.Int,
                pessoaId.intValue());

            procParams[3] = new ProcedureParameters(
                "@EhCliente",
                System.Data.SqlDbType.Int,
               0);

            procParams[4] = new ProcedureParameters(
                "@TransacaoID",
                System.Data.SqlDbType.Int,
                transacaoId.intValue());

            procParams[5] = new ProcedureParameters(
                "@Observacao",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value);

            procParams[6] = new ProcedureParameters(
                "@SeuPedido",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value);

            procParams[7] = new ProcedureParameters(
                "@OrigemPedidoID",
                System.Data.SqlDbType.Int,
                605);

            procParams[8] = new ProcedureParameters(
                "@FinanciamentoID",
                System.Data.SqlDbType.Int,
                financiamentoId.intValue());

            procParams[9] = new ProcedureParameters(
                "@dtPrevisaoEntrega",
                System.Data.SqlDbType.DateTime,
                System.DBNull.Value);

            procParams[10] = new ProcedureParameters(
                "@TransportadoraID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[11] = new ProcedureParameters(
                "@MeioTransporteID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[12] = new ProcedureParameters(
                "@ModalidadeTransporteID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[13] = new ProcedureParameters(
                "@Frete",
                System.Data.SqlDbType.Bit,
                0);

            procParams[14] = new ProcedureParameters(
                "@DepositoID",
                System.Data.SqlDbType.Int,
                depositoId.intValue());

            procParams[15] = new ProcedureParameters(
                "@ProprietarioID",
                System.Data.SqlDbType.Int,
                userId.intValue());

            procParams[16] = new ProcedureParameters(
                "@AlternativoID",
                System.Data.SqlDbType.Int,
                userId.intValue());

            procParams[17] = new ProcedureParameters(
                "@Observacoes",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);
            procParams[17].Length = 8000;

            procParams[18] = new ProcedureParameters(
                "@NumProjeto",
                System.Data.SqlDbType.VarChar,
                ProjetoID);

            procParams[19] = new ProcedureParameters(
                "@ProgramaMarketingID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[20] = new ProcedureParameters(
                "@URLEtiqueta",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value);

            procParams[21] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value,
                ParameterDirection.InputOutput);

            procParams[22] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                System.DBNull.Value,
                ParameterDirection.Output);
            procParams[22].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Pedido_Gerador",
                procParams);

            Array.Resize(ref pedidoId, pedidoId.Length + 1);

            pedidoId[pedidoId.Length - 1] = (Int32)procParams[21].Data; //new java.lang.Integer((int)procParams[21].Data);
        }

        // Pega os parâmetros para a procedure sp_PedidoItem_Gerador
        protected void PedidoItemGerador(bool pedidoInternacional)
        {
            ProcedureParameters[] procParams = new ProcedureParameters[15];

            procParams[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                pedidoId[pedidoId.Length - 1]);

            procParams[1] = new ProcedureParameters(
                "@ProdutoID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@ServicoID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[3] = new ProcedureParameters(
                "@Quantidade",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[4] = new ProcedureParameters(
                "@ValorInterno",
                System.Data.SqlDbType.Money,
                System.DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@ValorICMSSTInterno",
                System.Data.SqlDbType.Money,
                System.DBNull.Value);

            procParams[6] = new ProcedureParameters(
                "@ValorRevenda",
                System.Data.SqlDbType.Money,
                System.DBNull.Value);

            procParams[7] = new ProcedureParameters(
                "@ValorICMSSTRevenda",
                System.Data.SqlDbType.Money,
                System.DBNull.Value);

            procParams[8] = new ProcedureParameters(
                "@ValorUnitario",
                System.Data.SqlDbType.Money,
                System.DBNull.Value);

            procParams[9] = new ProcedureParameters(
                "@FinalidadeID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[10] = new ProcedureParameters(
                "@CFOPID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[11] = new ProcedureParameters(
                "@PedItemReferenciaID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[12] = new ProcedureParameters(
                "@PedItemEncomendaID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value);

            procParams[13] = new ProcedureParameters(
                "@TipoGerador",
                System.Data.SqlDbType.Int,
                3);

            procParams[14] = new ProcedureParameters(
                "@NovoPedItemID",
                System.Data.SqlDbType.Int,
                System.DBNull.Value,
                ParameterDirection.Output);

            for (int i = 0; i < produtoId.Length; i++)
            {
                //ProdutoID
                procParams[1].Data = produtoId[i];
                //ServicoID
                procParams[2].Data = DBNull.Value;
                //Quantidade
                procParams[3].Data = quantidade[i];
                //Valoricmsstinterno
                procParams[5].Data = DBNull.Value;
                //ValorRevenda
                procParams[6].Data = DBNull.Value;
                //ValorUnitario
                procParams[8].Data = custo[i];
                //CFOPID
                procParams[10].Data = DBNull.Value;
                //PedItemEncomendaID
                procParams[12].Data = pedItemVendaID[i].ToString();

                bool prodInternacional;

                if (internacional[i].intValue() == 1)
	                prodInternacional = true;
                else
                    prodInternacional = false;

                if (prodInternacional == pedidoInternacional)
                {
                    DataInterfaceObj.execNonQueryProcedure(
                    "sp_PedidoItem_Gerador",
                    procParams);

                    if (procParams[14].Data != DBNull.Value)
                    {
                        PedItemCompraID = ((int)procParams[14].Data);

                        if ((LoteID[i].ToString() != null) && (LoteID[i].ToString() != ""))
                        {
                            if (LoteID[i].ToString() == "0")
                            {
                                if ((nConsolidar == 0) || (nConsolidar == 1) && (nNovoLoteID == 0))
                                {
                                    ProcedureParameters[] procParamsLote = new ProcedureParameters[9];

                                    procParamsLote[0] = new ProcedureParameters(
                                        "@EmpresaID",
                                        System.Data.SqlDbType.Int,
                                        empresaId.ToString());

                                    procParamsLote[1] = new ProcedureParameters(
                                        "@Descricao",
                                        System.Data.SqlDbType.VarChar,
                                        Descricao[i].ToString());

                                    procParamsLote[2] = new ProcedureParameters(
                                        "@dtEmissao",
                                        System.Data.SqlDbType.Int,
                                        System.DBNull.Value);

                                    procParamsLote[3] = new ProcedureParameters(
                                        "@dtInicio",
                                        System.Data.SqlDbType.VarChar,
                                        System.DBNull.Value);

                                    procParamsLote[4] = new ProcedureParameters(
                                        "@DtFim",
                                        System.Data.SqlDbType.VarChar,
                                        System.DBNull.Value);

                                    procParamsLote[5] = new ProcedureParameters(
                                        "@Observacao",
                                        System.Data.SqlDbType.VarChar,
                                        "Modal Gerente Produto");

                                    procParamsLote[6] = new ProcedureParameters(
                                        "@ProprietarioID",
                                        System.Data.SqlDbType.Int,
                                        userId);

                                    procParamsLote[7] = new ProcedureParameters(
                                        "@UsuarioID",
                                        System.Data.SqlDbType.Int,
                                        userId);

                                    procParamsLote[8] = new ProcedureParameters(
                                        "@LoteID",
                                        System.Data.SqlDbType.Int,
                                        DBNull.Value,
                                        ParameterDirection.InputOutput);

                                    DataInterfaceObj.execNonQueryProcedure(
                                        "sp_Lote_Gerador",
                                        procParamsLote);

                                    if (procParamsLote[8].Data != null)
                                    {
                                        nNovoLoteID = Convert.ToInt32(procParamsLote[8].Data.ToString());
                                    }

                                    loteID = nNovoLoteID;
                                    nCriaProdutoLote = true;
                                }

                                if (nCriaProdutoLote)
                                {
                                    ProcedureParameters[] procParamsAssociar = new ProcedureParameters[6];
                                    procParamsAssociar[0] = new ProcedureParameters(
                                        "@LoteID",
                                        System.Data.SqlDbType.Int,
                                         loteID);

                                    procParamsAssociar[1] = new ProcedureParameters(
                                        "@PedItemID",
                                        System.Data.SqlDbType.Int,
                                        PedItemCompraID);

                                    procParamsAssociar[2] = new ProcedureParameters(
                                        "@Quantidade",
                                        System.Data.SqlDbType.Int,
                                        quantidade[i]);

                                    procParamsAssociar[3] = new ProcedureParameters(
                                        "@CriarProduto",
                                        System.Data.SqlDbType.Bit,
                                        nCriaProdutoLote);

                                    procParamsAssociar[4] = new ProcedureParameters(
                                        "@LotPedItemID",
                                        System.Data.SqlDbType.Int,
                                        System.DBNull.Value,
                                        ParameterDirection.InputOutput);

                                    procParamsAssociar[5] = new ProcedureParameters(
                                        "@Resultado",
                                        System.Data.SqlDbType.VarChar,
                                        System.DBNull.Value,
                                        ParameterDirection.InputOutput);
                                    procParamsAssociar[5].Length = 8000;

                                    DataInterfaceObj.execNonQueryProcedure(
                                        "sp_Lote_ItemAssociar",
                                        procParamsAssociar);

                                    if (procParamsAssociar[4].Data != DBNull.Value)
                                        LotPedItemID = int.Parse(procParamsAssociar[4].Data.ToString());
                                    else
                                        LotPedItemID = 0;


                                    if (procParamsAssociar[5].Data != DBNull.Value)
                                        Resultado = procParamsAssociar[5].Data.ToString();
                                    else
                                        LotPedItemID = 0;


                                    // Associação do pedido de saida

                                    procParamsAssociar[1].Data = pedItemVendaID[i].ToString();
                                    procParamsAssociar[3].Data = false;
                                    procParamsAssociar[4].Data = System.DBNull.Value;
                                    procParamsAssociar[5].Data = System.DBNull.Value;


                                    DataInterfaceObj.execNonQueryProcedure(
                                      "sp_Lote_ItemAssociar",
                                      procParamsAssociar);

                                    if (procParamsAssociar[4].Data != DBNull.Value)
                                        LotPedItemID = int.Parse(procParamsAssociar[4].Data.ToString());
                                    else
                                        LotPedItemID = 0;


                                    if (procParamsAssociar[5].Data != DBNull.Value)
                                        Resultado = procParamsAssociar[5].Data.ToString();
                                    else
                                        LotPedItemID = 0;
                                }

                                nCriaProdutoLote = false;
                            }
                            // Associa pedido de compra do projeto ao lote
                            else if (Convert.ToInt32(LoteID[i]) > 0)
                            {
                                ProcedureParameters[] procParamsAssociar = new ProcedureParameters[6];
                                procParamsAssociar[0] = new ProcedureParameters(
                                    "@LoteID",
                                    System.Data.SqlDbType.Int,
                                     Convert.ToInt32(LoteID[i]));

                                procParamsAssociar[1] = new ProcedureParameters(
                                    "@PedItemID",
                                    System.Data.SqlDbType.Int,
                                    PedItemCompraID);

                                procParamsAssociar[2] = new ProcedureParameters(
                                    "@Quantidade",
                                    System.Data.SqlDbType.Int,
                                    quantidade[i]);

                                procParamsAssociar[3] = new ProcedureParameters(
                                    "@CriarProduto",
                                    System.Data.SqlDbType.Bit,
                                    nCriaProdutoLote);

                                procParamsAssociar[4] = new ProcedureParameters(
                                    "@LotPedItemID",
                                    System.Data.SqlDbType.Int,
                                    System.DBNull.Value,
                                    ParameterDirection.InputOutput);

                                procParamsAssociar[5] = new ProcedureParameters(
                                    "@Resultado",
                                    System.Data.SqlDbType.VarChar,
                                    System.DBNull.Value,
                                    ParameterDirection.InputOutput);
                                procParamsAssociar[5].Length = 8000;

                                DataInterfaceObj.execNonQueryProcedure(
                                    "sp_Lote_ItemAssociar",
                                    procParamsAssociar);

                                if (procParamsAssociar[4].Data != DBNull.Value)
                                    LotPedItemID = int.Parse(procParamsAssociar[4].Data.ToString());
                                else
                                    LotPedItemID = 0;


                                if (procParamsAssociar[5].Data != DBNull.Value)
                                    Resultado = procParamsAssociar[5].Data.ToString();
                                else
                                    LotPedItemID = 0;

                            }
                        }
                    }
                }
            }
        }
        // Pega os parâmetros para a procedure sp_Pedido_Parcelas
        protected void PedidoParcelas()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[2];

            procParams[0] = new ProcedureParameters(
                "@PedidoID",
                System.Data.SqlDbType.Int,
                pedidoId[pedidoId.Length - 1]);

            procParams[1] = new ProcedureParameters(
                "@FormaPagamentoID",
                System.Data.SqlDbType.Int,
                  System.DBNull.Value);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Pedido_Parcelas",
                procParams);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            if (geraPedidoNacional.intValue() == 1)
            {
                PedidoGerador(false);
                PedidoItemGerador(false);
                PedidoParcelas();
            }

            if (geraPedidoInternacional.intValue() == 1)
            {
                // Transação de compra internacional
                transacaoId = new Integer(113);

                PedidoGerador(true);
                PedidoItemGerador(true);
                PedidoParcelas();
            }

            var strSQL = "";

            for (int i = 0; i < pedidoId.Length; i++)
            {
                strSQL += "SELECT " + pedidoId[i].ToString() + " AS PedidoID" + ((i < (pedidoId.Length - 1)) ? " UNION ALL " : "");
            }

            // Gera o resultado.
            WriteResultXML(
                DataInterfaceObj.getRemoteData(strSQL)
            );
        }
    }
}
