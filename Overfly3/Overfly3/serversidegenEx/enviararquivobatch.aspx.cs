﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class enviararquivobatch : System.Web.UI.OverflyPage
    {
        private string Resultado;

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            ProcedureParameters[] procParams = new ProcedureParameters[3];

            procParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                EmpresaID.ToString());

            procParams[1] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
               UsuarioID);

            procParams[2] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.VarChar,
                null,
                ParameterDirection.Output);
            procParams[2].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_RelatorioMicrosoft_BatchOpen",
                procParams);


            if (procParams[2].Data.ToString() != null)
            {
                Resultado = procParams[2].Data.ToString();
            }

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT " + Resultado + "as Resultado "));
        }
    }
}