using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class incluiitem : System.Web.UI.OverflyPage
    {
        private Integer documentoId;
        private Integer registroId;
        private Integer documentoOrigemId;
        private Integer registroOrigemId;

		public Integer nDocumentoID
		{
			set { documentoId = value; }
		}
		public Integer nRegistroID
		{
			set { registroId = value; }
		}
		public Integer nDocumentoOrigemID
		{
			set { documentoOrigemId = value; }
		}
		public Integer nRegistroOrigemID
		{
			set { registroOrigemId = value; }
		}
		
        protected override void PageLoad(object sender, EventArgs e)
        {
            // Executa o pacote
			int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(
                "INSERT INTO documentosRelacionados (DocumentoID, RegistroID, DocumentoOrigemID, RegistroOrigemID) " +
                "VALUES (" + documentoId + ", " + registroId + ", " + documentoOrigemId + ", " +
                registroOrigemId + ")"
            );

            // Gera o resultado para o usuario.
            WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + rowsAffected + " as fldresp"
				)
            );
        }
    }
}
