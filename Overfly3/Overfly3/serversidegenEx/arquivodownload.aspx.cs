﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.serversidegenEx
{
    public partial class arquivodownload: System.Web.UI.OverflyPage
    {

        
        private string FileID;
        protected string nFileID
        {
            get { return FileID; }
            set { FileID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            fileDownload();
        }

        protected void fileDownload()
        {
            try
            {
                //Verifica conexao do overfly.
                if (DataInterfaceObj.connectDatabase())
                {
                    //Atribui conexao do overfly.
                    SqlConnection CN;
                    CN = DataInterfaceObj.getConnection();

                    //Gera DataAdapter com o conteudo do arquivo que foi selecionado para download
                    SqlDataAdapter da = new SqlDataAdapter("SELECT Arquivo, ContentType, Nome FROM [OVERFLYBASE_EXT2].Overfly_Documentos.dbo.Documentos WITH (NOLOCK) WHERE DocumentoID = '" + nFileID + "'", CN);
                    SqlCommandBuilder MyCB = new SqlCommandBuilder(da);

                    //Gera um DataSet para receber o nome, tipo e conteudo do arquivo.
                    DataSet ds = new DataSet();
                    da.Fill(ds, "files");

                    //Gera um DataRow com o conteudo do DataSet.
                    DataRow myRow = default(DataRow);
                    myRow = ds.Tables[0].Rows[0];

                    //Gera um vetor de byte com o conteudo do arquivo.
                    byte[] MyData = null;
                    MyData = (byte[])myRow[0];

                    //Limpa o Response da pagina.
                    Response.ClearContent();
                    Response.ClearHeaders();

                    //Atribui o nome do arquivo no Response da pagina.
                    Response.AddHeader("Content-disposition", "attachment; filename=" + myRow[2].ToString());
                    Response.AddHeader("content-length", MyData.Length.ToString());
                    
                    //Atribui o tipo do arquivo no Response da pagina.
                    Response.ContentType = myRow[1].ToString();

                    //Realiza a leitura do vetor de byte com o conteudo do arquivo no Response da pagina.
                    Response.BinaryWrite(MyData);
                    
                    //Carrega no Navegador.
                    Response.Flush();
                    
                    //Conclui a operação.
                    Response.Close();
                }
                else
                {
                    //Exibe mensagem de erro de conexão do overfly.
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Verifique a conexão!')", true);
                }
            }
            catch (System.Exception ex)
            {   
                //Exibe mensagem de erros não previstos.
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Erro: \\n" + ex.Message + "')", true);
            }
        }
    }
}