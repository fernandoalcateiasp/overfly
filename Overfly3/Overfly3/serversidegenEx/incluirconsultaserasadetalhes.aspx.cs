using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class incluirconsultaserasadetalhes : System.Web.UI.OverflyPage
    {
        private Integer validacaoDadoId;
        private string[] nivel;
        private string[] nodeMae;
        private string[] node;
        private string[] atributo;
        private string[] valor;
        private string fonteDadoId;
        private string paisId;

		public Integer nValidacaoDadoID
		{
			set { validacaoDadoId =  value; }
		}
		public string[] nNivel
		{
			set
			{
				nivel = value;

				for(int i = 0; i < nivel.Length; i++) {
					nivel[i] = (nivel[i] != null && nivel[i].Length > 0) ? 
						nivel[i] : "NULL";
				}
			}
		}
		public string[] sNodeMae
		{
			set
			{
				nodeMae = value;

				for (int i = 0; i < nodeMae.Length; i++)
				{
					if (nodeMae[i] != null && nodeMae[i].Length > 0) {
						nodeMae[i] = (nodeMae[i].Length > 50) ?
							"'" + nodeMae[i].Substring(0, 50) + "'" :
							"'" + nodeMae[i] + "'";
					}
					else
					{
						nodeMae[i] = "NULL";
					}
				}
				
				
			}
		}
		public string[] sNode
		{
			set
			{
				node = value;

				for (int i = 0; i < node.Length; i++)
				{
					if (node[i] != null && node[i].Length > 0) {
						node[i] = (node[i].Length > 50) ?
							"'" + node[i].Substring(0, 50) + "'" :
							"'" + node[i] + "'";
					} else {
						node[i] = "NULL";
					}
				}
					
			}
		}
		public string[] sAtributo
		{
			set
			{
				atributo = value;

				for (int i = 0; i < atributo.Length; i++)
				{
					if (atributo[i] != null && atributo[i].Length > 0)
					{
						atributo[i] = (atributo[i].Length > 50) ?
							"'" + atributo[i].Substring(0, 50) + "'" :
							"'" + atributo[i] + "'";
					}
					else
					{
						atributo[i] = "NULL";
					}
				}
			}
		}
		public string[] sValor
		{
			set
			{
				valor = value;

				for (int i = 0; i < valor.Length; i++) {
					if (valor[i] != null && valor[i].Length > 0)
					{
						valor[i] = (valor[i].Length > 50) ?
							"'" + valor[i].Substring(0, 50) + "'" :
							"'" + valor[i] + "'";
					}
					else
					{
						valor[i] = "NULL";
					}
				}
			}
		}
		public string nFonteDadoID
		{
			set { fonteDadoId = value; }
		}
		public string nPaisID
		{
			set { paisId = value; }
		}

        protected int IncluirConsulta()
        {
            // Gera o comando SQL
            string sql = "";
            for (int i = 0; i < nivel.Length; i++)
            {
                sql += " INSERT INTO ValidacoesDados_Detalhes (ValidacaoDadoID, Nivel, NodeMae, Node, Atributo, ValorID, Valor) " +
                    "SELECT " + validacaoDadoId + ", " + nivel[i] + ", " + nodeMae[i] + ", " +
                            node[i] + ", " + atributo[i] + ", NULL, " + valor[i];

                if (node[i] == "'DDD'" && fonteDadoId == "287")
                {
                    sql += " UNION ALL SELECT " + validacaoDadoId + ", " + nivel[i] + ", " + nodeMae[i] + ", " +
                            "'DDI', " + atributo[i] + ", NULL, (SELECT DDDi FROM Localidades_DDDs WHERE LocalidadeID=" + paisId + ") ";
                }

                if (node[i] == "'UF'" && fonteDadoId == "287")
                {
                    sql += " UNION ALL SELECT " + validacaoDadoId + ", " + nivel[i] + ", " + nodeMae[i] + ", " +
                            "'Pais', " + atributo[i] + ", " + paisId + ", (SELECT Localidade FROM Localidades WHERE LocalidadeID=" + paisId + ") ";
                }	                                                
            }

            // Executa o pacote
            return DataInterfaceObj.ExecuteSQLCommand(sql);
		}
					
        protected override void PageLoad(object sender, EventArgs e)
        {
            int rowsAffected = IncluirConsulta();

            // Gera o resultado para o usuario.
            WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + rowsAffected + " as fldresp"
				)
			);
        }
    }
}
