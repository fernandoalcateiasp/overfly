using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class pageinf : System.Web.UI.OverflyPage
    {
        private int GetFieldDataTypeCode(string fieldDataType)
        {
            if (fieldDataType.Equals("bigint")) return 20;
            else if (fieldDataType.Equals("binary")) return 128;
            else if (fieldDataType.Equals("bit")) return 11;
            else if (fieldDataType.Equals("char")) return 129;
            else if (fieldDataType.Equals("datetime")) return 135;
            else if (fieldDataType.Equals("decimal")) return 131;
            else if (fieldDataType.Equals("float")) return 5;
            else if (fieldDataType.Equals("image")) return 205;
            else if (fieldDataType.Equals("int")) return 3;
            else if (fieldDataType.Equals("money")) return 6;
            else if (fieldDataType.Equals("nchar")) return 130;
            else if (fieldDataType.Equals("ntext")) return 203;
            else if (fieldDataType.Equals("numeric")) return 131;
            else if (fieldDataType.Equals("nvarchar")) return 202;
            else if (fieldDataType.Equals("real")) return 4;
            else if (fieldDataType.Equals("smalldatetime")) return 135;
            else if (fieldDataType.Equals("smallint")) return 2;
            else if (fieldDataType.Equals("smallmoney")) return 6;
            else if (fieldDataType.Equals("sql_variant")) return 12;
            else if (fieldDataType.Equals("text")) return 201;
            else if (fieldDataType.Equals("timestamp")) return 128;
            else if (fieldDataType.Equals("tinyint")) return 17;
            else if (fieldDataType.Equals("uniqueidentifier")) return 72;
            else if (fieldDataType.Equals("varbinary")) return 204;
            else if (fieldDataType.Equals("varchar")) return 200;
            else return -1;
        }

        private string formId = "";
        private string fldKey;
        private string folderId;
        private string order = "";
        private string strSQLExecAfterCreateTable = "";
        private string textToSearch;
        private string condition = "";
        private string optParam1 = "";
        private string optParam2 = "";
        private string contextoId = "0";
        private string empresaId = "0";
        private string empresaCidadeId = "0";
        private string filtroId;
        private string filtro = " ";
        private string a1;
        private string a2;
        private string linkFieldName;
        private string linkFieldValue;
        private string[] linkField;
        private string pageNumber;
        private string pageSize;

        private string sql1 = "";
        private string sql2 = "";


        public string nFormID
        {
            get { return formId; }
            set { formId = stringWithNoNull(value); }
        }

        public string sFldKey
        {
            get { return fldKey; }

            set
            {
                if (value != null)
                {
                    fldKey = value.Substring(0, 1) +
                        "." + value.Substring(1, value.Length - 1);
                }
                else
                {
                    fldKey = "";
                }
            }
        }

        public string nFolderID
        {
            get { return folderId; }
            set { folderId = stringWithNoNull(value); }
        }

        public string nOrder
        {
            get { return order; }
            set { order = stringWithNoNull(value); }
        }

        public string sTextToSearch
        {
            get { return textToSearch; }
            set
            {
                if (value != null && value.Length > 0)
                {
                    textToSearch = "'" + value + "'";
                }
                else
                {
                    textToSearch = "'0'";
                }
            }
        }

        public string sCondition
        {
            get { return condition; }
            set
            {
                condition = stringWithNoNull(value);

                if (condition != "")
                {
                    condition = "AND (" + condition + ")";
                }
            }
        }

        public string vOptParam1
        {
            get { return optParam1; }
            set { optParam1 = stringWithNoNull(value); }
        }

        public string vOptParam2
        {
            get { return optParam2; }
            set { optParam2 = stringWithNoNull(value); }
        }

        public string nContextoID
        {
            get { return contextoId; }
            set { contextoId = stringWithNoNull(value); }
        }

        public string nEmpresaID
        {
            get { return empresaId; }
            set { empresaId = stringWithNoNull(value); }
        }

        public string nEmpresaCidadeID
        {
            get { return empresaCidadeId; }
            set { empresaCidadeId = stringWithNoNull(value); }
        }

        public string nFiltroID
        {
            get { return filtroId; }
            set { filtroId = stringWithNoNull(value); }
        }

        public string sFiltro
        {
            get { return filtro; }
            set { filtro = stringWithNoNull(value); }
        }

        public string A1
        {
            get { return a1; }
            set { a1 = stringWithNoNull(value); }
        }

        public string A2
        {
            get { return a2; }
            set { a2 = stringWithNoNull(value); }
        }

        public string sLinkFieldName
        {
            get { return linkFieldName; }
            set
            {
                linkFieldName = stringWithNoNull(value);

                linkField = linkFieldName.Split(Separator);
            }
        }

        public string sLinkFieldValue
        {
            get { return linkFieldValue; }
            set
            {
                linkFieldValue = stringWithNoNull(value);
            }
        }

        public string nPageNumber
        {
            get { return pageNumber; }
            set { pageNumber = stringWithNoNull(value); }
        }

        public string nPageSize
        {
            get { return pageSize; }
            set { pageSize = stringWithNoNull(value); }
        }

        public string ExpLink
        {
            get
            {
                string expLink = " (";

                if (linkField.Length > 0)
                {
                    for (int i = 0; i < linkField.Length; i++)
                    {
                        expLink = expLink + " " + "a." + linkField[i] + " = " + linkFieldValue;

                        if (i == linkField.Length - 1) break;

                        expLink = expLink + " OR ";
                    }

                    expLink = expLink + " ) AND ";
                }
                else
                {
                    expLink = " " + "a." + linkFieldName + " = " + linkFieldValue + " AND ";
                }

                return expLink;
            }
        }

        public string Operator
        {
            get
            {
                if (order == "DESC" && textToSearch != "0")
                    return " <= ";
                else
                    return " >= ";
            }
        }

        public string StartID
        {
            get
            {
                int result =
                    ((int.Parse(pageNumber) * int.Parse(pageSize)) -
                        int.Parse(pageSize));

                return "" + result;
            }
        }

        private java.lang.Boolean isSohConciliacao = new java.lang.Boolean(false);
        private string sohConciliacao = "";

        protected java.lang.Boolean bSohConciliacao
        {
            set
            {
                if (value != null) isSohConciliacao = value;

                if (isSohConciliacao.booleanValue())
                    sohConciliacao = " dbo.fn_Lancamento_Conciliado(a.LanContaID) = 1 AND ";
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            // filtro (COMBO 2 INFERIOR) -- INICIO
            try
            {
                System.Data.DataSet dsFiltro = DataInterfaceObj.getRemoteData(
                    "SELECT ISNULL(filtro, SPACE(0)) AS filtro " +
                        "From Recursos WITH (NOLOCK) WHERE RecursoID = " +
                            filtroId.ToString());

                if (dsFiltro.Tables[1].Rows.Count > 0 &&
                    dsFiltro.Tables[1].Rows[0]["filtro"].ToString() != "")
                {
                    filtro = filtro + " AND (" +
                        dsFiltro.Tables[1].Rows[0]["filtro"].ToString() +
                        ") ";
                }
            }
            catch (Exception)
            {
                filtro = " ";
            }
            // filtro (COMBO 2 INFERIOR) -- FIM

            strSQLExecAfterCreateTable = "";

            // Preenche os comandos para o form indicado.
            makeSQLCommand(int.Parse(formId));

            // Roda a procedure sp_PesqList
            ProcedureParameters[] procParams = new ProcedureParameters[4];

            procParams[0] = new ProcedureParameters(
                "@iPageSize",
                System.Data.SqlDbType.Int,
                pageSize != null ?
                    (Object)int.Parse(pageSize) :
                    System.DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@sSQL",
                System.Data.SqlDbType.VarChar,
                sql1 != null ? (Object)sql1 : System.DBNull.Value);
            procParams[1].Length = sql1.Length;

            procParams[2] = new ProcedureParameters(
                "@sSQL2",
                System.Data.SqlDbType.VarChar,
                sql2 != null ? (Object)sql2 : System.DBNull.Value);
            procParams[2].Length = sql2.Length;

            procParams[3] = new ProcedureParameters(
                "@sExecAfterCreateTable",
                System.Data.SqlDbType.VarChar,
                strSQLExecAfterCreateTable != null ? (Object)strSQLExecAfterCreateTable : System.DBNull.Value);
            procParams[3].Length = strSQLExecAfterCreateTable.Length;

            // Gera o resultado para o usuario.
            WriteResultXML(
                DataInterfaceObj.execQueryProcedure("sp_PesqList", procParams)
            );
        }

        private void makeSQLCommand(int formID)
        {
            switch (formID) //formID /basico/Recursos formID
            //a.RelacaoID-->sFldKey
            {
                case 1110:
                    if (folderId == "20011") // Recursos Filhos
                    {
                        if (optParam1 == "1" || optParam1 == "2")
                        {
                            if (fldKey == "b.RecursoID")
                            {
                                fldKey = "a.RecursoID";
                            }
                            else if (fldKey == "b.RecursoFantasia")
                            {
                                fldKey = "a.RecursoFantasia";
                            }

                            sql1 =
                                "INSERT INTO @temptable (IDFromPesq) " +
                                "SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) +
                                    " a.RecursoID " +
                                "FROM Recursos a WITH (NOLOCK) " +
                                "WHERE " +
                                    ExpLink + fldKey + Operator + textToSearch + " " +
                                     condition + filtro;

                            if (fldKey == "a.RecursoID")
                            {
                                sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                            }
                            else
                            {
                                sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,a.RecursoID";
                            }

                            sql2 =
                                "SELECT TOP " + pageSize.ToString() +
                                    " a.RecursoID,b.RecursoAbreviado,a.RecursoFantasia, " +
                                    "c.ItemMasculino " +
                                "FROM Recursos a WITH (NOLOCK), Recursos b WITH (NOLOCK), TiposAuxiliares_Itens c WITH (NOLOCK), " +
                                    "@temptable z " +
                                "WHERE a.EstadoID = b.RecursoID AND " +
                                    "a.TipoRecursoID = c.ItemID AND " +
                                     ExpLink + fldKey + Operator + textToSearch + " " +
                                     condition + filtro + " AND " +
                                     "z.IDFromPesq=a.RecursoID AND " +
                                     "z.IDTMP > " + StartID + " " +
                                "ORDER BY z.IDTMP";
                        }
                        else if (optParam1 == "3")
                        {
                            if (fldKey == "b.RecursoID")
                            {
                                fldKey = "a.RecursoID";
                            }
                            else if (fldKey == "b.RecursoFantasia")
                            {
                                fldKey = "a.RecursoFantasia";
                            }

                            sql1 =
                                "INSERT INTO @temptable (IDFromPesq) " +
                                "SELECT TOP " +
                                    (int.Parse(pageSize) * int.Parse(pageNumber)) +
                                    " b.RecursoID " +
                                "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK) " +
                                "WHERE a.SujeitoID=b.RecursoID AND " +
                                    ExpLink + fldKey + Operator + textToSearch + " " +
                                    condition + filtro;

                            if (fldKey == "b.RecursoID")
                            {
                                sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                            }
                            else
                            {
                                sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,b.RecursoID";
                            }

                            sql2 = "SELECT TOP " + pageSize + " b.RecursoID,c.RecursoAbreviado,b.RecursoFantasia,d.ItemMasculino " +
                                     "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK), Recursos c WITH (NOLOCK), TiposAuxiliares_Itens d WITH (NOLOCK), @temptable z " +
                                     "WHERE b.EstadoID = c.RecursoID AND b.TipoRecursoID = d.ItemID AND a.TipoRelacaoID = 1 AND " +
                                     "a.SujeitoID=b.RecursoID AND " +
                                     ExpLink + fldKey + Operator + textToSearch + " " +
                                     condition + filtro + " AND z.IDFromPesq=b.RecursoID " +
                                     "AND z.IDTMP > " + StartID + " " +
                                     "ORDER BY z.IDTMP";
                        }
                        else if (optParam1 == "4")
                        {
                            if (fldKey == "a.RecursoID")
                            {
                                fldKey = "b.RecursoID";
                            }
                            else if (fldKey == "a.RecursoFantasia")
                            {
                                fldKey = "b.RecursoFantasia";
                            }

                            sql1 =
                                "INSERT INTO @temptable (IDFromPesq) " +
                                "SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) +
                                    " b.RecursoID " +
                                "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK)" +
                                "WHERE a.SujeitoID=b.RecursoID AND " +
                                    ExpLink + fldKey + Operator + textToSearch + " " +
                                    condition + filtro;

                            if (fldKey == "b.RecursoID")
                            {
                                sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                            }
                            else
                            {
                                sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,b.RecursoID";
                            }

                            sql2 =
                                "SELECT TOP " + pageSize +
                                    " b.RecursoID,c.RecursoAbreviado, " +
                                    "b.RecursoFantasia,d.ItemMasculino " +
                                "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK), Recursos c WITH (NOLOCK), " +
                                    "TiposAuxiliares_Itens d WITH (NOLOCK), @temptable z " +
                                "WHERE b.EstadoID = c.RecursoID AND " +
                                    "b.TipoRecursoID = d.ItemID AND a.TipoRelacaoID = 2 AND " +
                                    "a.SujeitoID=b.RecursoID AND " +
                                    ExpLink + fldKey + Operator + textToSearch + " " +
                                    condition + filtro + " AND z.IDFromPesq=b.RecursoID AND " +
                                    "z.IDTMP > " + StartID + " " +
                                "ORDER BY z.IDTMP";
                        }
                    }
                    else if (folderId == "20016")
                    {
                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) + " a.RelacaoID " +
                                "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK)";

                        if (optParam1 == "SUJEITO")
                        {
                            sql1 = sql1 + "WHERE a.SujeitoID = b.RecursoID AND a.TipoRelacaoID = " + optParam2 + " AND ";
                        }
                        else if (optParam1 == "OBJETO")
                        {
                            sql1 = sql1 + "WHERE a.ObjetoID = b.RecursoID AND a.TipoRelacaoID = " + optParam2 + " AND ";
                        }

                        sql1 = sql1 + ExpLink + fldKey + Operator + textToSearch + " " + condition + filtro;

                        if (fldKey == "a.RelacaoID")
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";
                        }

                        sql2 = "SELECT TOP " + pageSize + " b.RecursoFantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " +
                                    "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK), @temptable z ";

                        if (optParam1 == "SUJEITO")
                        {
                            sql2 = sql2 + "WHERE a.SujeitoID = b.RecursoID AND a.TipoRelacaoID = " + optParam2 + " AND ";
                        }
                        else if (optParam1 == "OBJETO")
                        {
                            sql2 = sql2 + "WHERE a.ObjetoID = b.RecursoID AND a.TipoRelacaoID = " + optParam2 + " AND ";
                        }

                        sql2 = sql2 + ExpLink + fldKey + Operator + textToSearch + " " +
                                 condition + filtro + " AND z.IDFromPesq=a.RelacaoID " +
                                 "AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }
                    break;

                case 1120: //formID  /basico/relacoes/relentrerecs
                case 1330: //formID  /basico/Localidades

                    if (folderId == "20246") // Localidades Filhas
                    {
                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) + " a.LocalidadeID " +
                                 "FROM Localidades a WITH (NOLOCK) WHERE " +
                                 ExpLink + fldKey + Operator + textToSearch + " " + condition + filtro;

                        if (fldKey.Equals("a.LocalidadeID"))
                        {
                            sql1 += " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 += " ORDER BY " + fldKey + " " + order + " ,a.LocalidadeID";
                        }

                        sql2 = "SELECT TOP " + pageSize + " a.LocalidadeID,b.RecursoAbreviado,a.Localidade,c.ItemMasculino " +
                                 "FROM Localidades a WITH (NOLOCK), Recursos b WITH (NOLOCK), TiposAuxiliares_Itens c WITH (NOLOCK), @temptable z " +
                                 "WHERE a.EstadoID = b.RecursoID AND a.TipoLocalidadeID = c.ItemID AND " +
                                 ExpLink + fldKey + Operator + textToSearch + " " +
                                 condition + filtro + " AND z.IDFromPesq=a.LocalidadeID " +
                                 "AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }

                    break;

                case 1210: //formID  /basico/Pessoas
                    if (folderId == "20120") // Pedidos
                    {
                        string fldKeyOld = fldKey;

                        // se o tipo do campo de pesquisa for data
                        if (returnFldType("Pedidos_Itens", fldKey) == 135 && textToSearch == "'0'")
                        {
                            textToSearch = "0";
                        }

                        if (fldKey == "a.dtMovimento")
                        {
                            fldKey = " ISNULL((SELECT TOP 1 dtMovimento FROM Pedidos_Itens WHERE (PedidoID = a.PedidoID)), GETDATE()) ";
                        }

                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) +
                            " a.PedidoID " +
                                 "FROM Pedidos a WITH (NOLOCK), Recursos b WITH (NOLOCK), Conceitos d WITH (NOLOCK), Pessoas e WITH (NOLOCK), Pessoas f WITH (NOLOCK), Operacoes g WITH (NOLOCK) " +
                                 "WHERE a.EstadoID=b.RecursoID AND a.MoedaID=d.ConceitoID AND " +
                                 "a.EmpresaID=e.PessoaID AND a.ProprietarioID=f.PessoaID AND a.TransacaoID = g.OperacaoID AND " +
                                 ExpLink + fldKey + Operator + textToSearch + " " + condition + filtro;

                        if (fldKeyOld == "a.dtMovimento")
                        {
                            sql1 = sql1 + " ORDER BY ISNULL((SELECT TOP 1 dtMovimento FROM Pedidos_Itens WITH (NOLOCK) WHERE (PedidoID = a.PedidoID)), GETDATE()) DESC,a.PedidoID ";
                        }
                        else if (fldKey == "a.PedidoID")
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,a.PedidoID";
                        }

                        sql2 = "SELECT TOP " + pageSize + " e.Fantasia, a.PedidoID, b.RecursoAbreviado, " +
                              "(SELECT TOP 1 dtMovimento FROM Pedidos_Itens WHERE (PedidoID = a.PedidoID)) AS dtPedido, d.SimboloMoeda, " +
                              "a.ValorTotalPedido AS TotalPedido, " +
                              "dbo.fn_Tradutor(i.Operacao, 246, dbo.fn_Pais_idioma(dbo.fn_Pessoa_Localidade(a.EmpresaID, 1, NULL,	NULL)), 'A') AS Codigo, f.Fantasia AS Vendedor, " +
                              "g.Fantasia AS Pessoa, h.NotaFiscal, h.dtNotaFiscal, a.Observacao " +
                              "FROM Pedidos a WITH (NOLOCK)" +
                                "LEFT OUTER JOIN NotasFiscais h WITH (NOLOCK) ON (a.NotaFiscalID=h.NotaFiscalID) " +
                                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID=b.RecursoID) " +
                                "INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID=d.ConceitoID) " +
                                "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.EmpresaID=e.PessoaID) " +
                                "INNER JOIN Pessoas f WITH(NOLOCK) ON (a.ProprietarioID=f.PessoaID) " +
                                "INNER JOIN Pessoas g WITH(NOLOCK) ON (a.PessoaID=g.PessoaID) " +
                                "INNER JOIN Operacoes i WITH(NOLOCK) ON (a.TransacaoID = i.OperacaoID) " +
                                "INNER JOIN @temptable z ON (z.IDFromPesq = a.PedidoID) " +
                              "WHERE " +
                              ExpLink + fldKey + Operator + textToSearch + " " +
                              condition + filtro + "  " +
                              "AND z.IDTMP > " + StartID +
                              " ORDER BY z.IDTMP";
                    }
                    else if (folderId == "20121") // Produtos
                    {
                        // Se o tipo do campo de pesquisa for data
                        if (returnFldType("Pedidos_Itens", fldKey) == 135 && textToSearch == "'0'")
                        {
                            textToSearch = "0";
                        }

                        var bdate = (returnFldType("Pedidos_Itens", fldKey) == 135);

                        sql1 =
                            "INSERT INTO @temptable (IDFromPesq) " +
                            "SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) +
                                " c.PedItemID " +
                            "FROM Pedidos a WITH (NOLOCK), Pessoas b WITH (NOLOCK), Pedidos_Itens c WITH (NOLOCK), " +
                                "Conceitos d WITH (NOLOCK), Pessoas e WITH (NOLOCK), Conceitos g WITH (NOLOCK), Operacoes h WITH (NOLOCK) " +
                            "WHERE a.EmpresaID=b.PessoaID AND " +
                                "a.PedidoID=c.PedidoID AND c.ProdutoID=d.ConceitoID AND " +
                                "a.ProprietarioID=e.PessoaID AND " +
                                "a.MoedaID=g.ConceitoID AND " +
                                "a.TransacaoID = h.OperacaoID AND " +
                                (textToSearch.Replace("'", "") == "0" ? ExpLink.Replace("AND", " ") : ExpLink + fldKey +
                                (bdate == true ? Operator + textToSearch : " LIKE '%" + textToSearch.Replace("'", "") + "%' ")) + " " +
                                 condition + filtro;

                        if (fldKey == "c.PedItemID")
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,c.PedItemID";
                        }

                        sql2 =
                            "SELECT TOP " + pageSize +
                                " c.dtMovimento, a.PedidoID, " +
                                "h.RecursoAbreviado AS Estado, " +
                                "i.Fantasia AS Pessoa, ISNULL(dbo.fn_Tradutor(j.Operacao, 246, dbo.fn_Pais_idioma(dbo.fn_Pessoa_Localidade(a.EmpresaID, 1, NULL, NULL)), '  + '\'' + 'A' + '\'), SPACE(0)) AS Codigo, " +
                                "d.ConceitoID, d.Conceito, c.Quantidade, " +
                                "g.SimboloMoeda, " +
                                "c.ValorUnitario AS Valor, " +
                                "(c.ValorUnitario*c.Quantidade) AS ValorTotal, " +
                                "e.Fantasia AS Vendedor, " +
                                "l.PedidoID AS PedidoReferencia, b.Fantasia AS Empresa, " +
                                "a.Observacao " +
                            "FROM Pedidos a WITH(NOLOCK) " +
                                    "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.EmpresaID = b.PessoaID) " +
                                    "INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (a.PedidoID = c.PedidoID) " +
                                    "INNER JOIN Conceitos d WITH(NOLOCK) ON (c.ProdutoID = d.ConceitoID) " +
                                    "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID = e.PessoaID) " +
                                    "INNER JOIN Conceitos g WITH(NOLOCK) ON (a.MoedaID = g.ConceitoID) " +
                                    "INNER JOIN Recursos h WITH(NOLOCK) ON (a.EstadoID = h.RecursoID) " +
                                    "INNER JOIN Pessoas i WITH(NOLOCK) ON (a.PessoaID = i.PessoaID) " +
                                    "INNER JOIN Operacoes j WITH(NOLOCK) ON (a.TransacaoID = j.OperacaoID) " +
                                    "LEFT OUTER JOIN Pedidos_Itens l WITH(NOLOCK) ON (c.PedItemReferenciaID = l.PedItemID) " +
                                    "INNER JOIN @temptable z ON (z.IDFromPesq=c.PedItemID)" +
                            "WHERE " +
                                (textToSearch.Replace("'", "") == "0" ? ExpLink.Replace("AND", " ") : ExpLink + fldKey +
                                (bdate == true ? Operator + textToSearch : " LIKE '%" + textToSearch.Replace("'", "") + "%' ")) + " " +
                                condition + filtro + " AND " +
                                "z.IDTMP > " + StartID +
                            " ORDER BY z.IDTMP";
                    }
                    else if (folderId == "20122") // Financeiros
                    {
                        // Se o tipo do campo de pesquisa for data
                        if (returnFldType("Financeiro", fldKey) == 135 && textToSearch == "'0'")
                        {
                            textToSearch = "0";
                        }

                        if (a1 == "0" || a2 == "0")
                        {
                            filtro = filtro + " AND a.PedidoID IS NOT NULL ";
                        }

                        sql1 =
                            "INSERT INTO @temptable (IDFromPesq) " +
                            "SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) +
                                " a.FinanceiroID " +
                            "FROM Financeiro a WITH (NOLOCK), Recursos c WITH (NOLOCK), TiposAuxiliares_Itens d WITH (NOLOCK), " +
                                "Pessoas e WITH (NOLOCK), TiposAuxiliares_Itens g WITH (NOLOCK), " +
                                "Conceitos h WITH (NOLOCK), Pessoas i WITH (NOLOCK) " +
                            "WHERE a.EstadoID=c.RecursoID AND " +
                                "a.TipoFinanceiroID=d.ItemID AND " +
                                "a.PessoaID=e.PessoaID AND " +
                                 "a.FormaPagamentoID=g.ItemID AND a.MoedaID=h.ConceitoID AND " +
                                 "a.EmpresaID=i.PessoaID AND " +
                                 ExpLink + fldKey + Operator +
                                 textToSearch + " " + condition + filtro;

                        if (fldKey == "a.FinanceiroID")
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,a.FinanceiroID";
                        }

                        sql2 =
                            "SELECT TOP " + pageSize +
                                " a.dtVencimento, dbo.fn_Financeiro_Totais(a.FinanceiroID, GETDATE(), 40) AS Atraso, " +
                                "a.FinanceiroID,c.RecursoAbreviado AS Estado,d.ItemMasculino AS TipoFinanceiro, " +
                                "a.PedidoID AS Pedido, dbo.fn_Tradutor(j.Operacao, 246, dbo.fn_Pais_idioma(dbo.fn_Pessoa_Localidade(b.EmpresaID, 1, NULL,	NULL)), 'A') AS Codigo, e.Fantasia AS Pessoa, a.Duplicata, a.dtEmissao, " +
                                "g.ItemAbreviado AS FormaPagamento, a.PrazoPagamento, h.SimboloMoeda AS Moeda, a.Valor, " +
                                "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, " +
                                "dbo.fn_Financeiro_Posicao(a.FinanceiroID, NULL, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, i.Fantasia AS Empresa, a.Observacao " +
                            "FROM Financeiro a WITH (NOLOCK) LEFT OUTER JOIN Pedidos b WITH (NOLOCK) ON (a.PedidoID = b.PedidoID) " +
                                "LEFT OUTER JOIN Operacoes j WITH (NOLOCK) ON (b.TransacaoID = j.OperacaoID), Recursos c WITH (NOLOCK), TiposAuxiliares_Itens d WITH (NOLOCK), Pessoas e WITH (NOLOCK), " +
                                "TiposAuxiliares_Itens g WITH (NOLOCK), Conceitos h WITH (NOLOCK), Pessoas i WITH (NOLOCK), @temptable z " +
                            "WHERE a.EstadoID=c.RecursoID AND a.TipoFinanceiroID=d.ItemID AND a.PessoaID=e.PessoaID AND " +
                                "a.FormaPagamentoID=g.ItemID AND a.MoedaID=h.ConceitoID AND " +
                                "a.EmpresaID=i.PessoaID AND " +
                                ExpLink + fldKey + Operator + textToSearch + " " + condition + filtro +
                                " AND z.IDFromPesq=a.FinanceiroID " + "AND z.IDTMP > " + StartID +
                            " ORDER BY z.IDTMP";
                    }
                    else if (folderId == "20123") // Rel entre pessoas
                    {
                        sql1 =
                            "INSERT INTO @temptable (IDFromPesq) " +
                            "SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) +
                                " a.RelacaoID " +
                            "FROM RelacoesPessoas a WITH (NOLOCK), Pessoas b WITH (NOLOCK)";
                        if (optParam1 == "SUJEITO")
                        {
                            sql1 = sql1 + "WHERE a.SujeitoID = b.PessoaID AND a.TipoRelacaoID = " + optParam2 + " AND ";
                        }
                        else if (optParam1 == "OBJETO")
                        {
                            sql1 = sql1 + "WHERE a.ObjetoID = b.PessoaID AND a.TipoRelacaoID = " + optParam2 + " AND ";
                        }

                        sql1 = sql1 + ExpLink + fldKey + Operator + textToSearch + " " +
                                 condition + filtro;

                        if (fldKey == "a.RelacaoID")
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";
                        }

                        sql2 =
                            "SELECT TOP " + pageSize +
                                " b.Fantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, " +
                                "a.ObjetoID, a.TipoRelacaoID, " +
                                "a.Observacao, a.ProprietarioID, a.AlternativoID " +
                            "FROM RelacoesPessoas a WITH (NOLOCK) ";

                        if (optParam1 == "SUJEITO")
                        {
                            sql2 = sql2 + "INNER JOIN Pessoas b WITH(NOLOCK) ON a.SujeitoID = b.PessoaID ";
                        }
                        else if (optParam1 == "OBJETO")
                        {
                            sql2 = sql2 + "INNER JOIN Pessoas b WITH(NOLOCK) ON a.ObjetoID = b.PessoaID ";
                        }

                        sql2 = sql2 + "INNER JOIN @temptable z ON z.IDFromPesq=a.RelacaoID ";

                        sql2 = sql2 + "WHERE a.TipoRelacaoID = " + optParam2 + " AND ";

                        sql2 = sql2 + ExpLink + fldKey + Operator + textToSearch + " " +
                                 condition + filtro + " AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }
                    else if (folderId == "20124") // Rel com recursos
                    {
                        sql1 =
                            "INSERT INTO @temptable (IDFromPesq) " +
                            "SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) +
                                " a.RelacaoID " +
                            "FROM RelacoesPesRec a WITH (NOLOCK), Recursos b WITH (NOLOCK) " +
                            "WHERE a.ObjetoID = b.RecursoID AND a.TipoRelacaoID = " +
                                optParam1 + " AND " +
                                ExpLink + fldKey + Operator + textToSearch + " " +
                                condition + filtro;

                        if (fldKey == "a.RelacaoID")
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";
                        }

                        sql2 =
                            "SELECT TOP " + pageSize + " b.RecursoFantasia, a.RelacaoID, " +
                                "a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " +
                            "FROM RelacoesPesRec a WITH (NOLOCK), Recursos b WITH (NOLOCK), @temptable z " +
                            "WHERE a.ObjetoID = b.RecursoID AND a.TipoRelacaoID = " + optParam1 + " AND " +
                                 ExpLink + fldKey + Operator + textToSearch + " " +
                                 condition + filtro + " AND z.IDFromPesq=a.RelacaoID " +
                                 "AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }
                    else if (folderId == "20125") // Rel com Conceitos
                    {
                        sql1 =
                            "INSERT INTO @temptable (IDFromPesq) " +
                            "SELECT TOP " + (int.Parse(pageSize) * int.Parse(pageNumber)) +
                                " a.RelacaoID " +
                            "FROM RelacoesPesCon a WITH (NOLOCK), Conceitos b WITH (NOLOCK) " +
                            "WHERE a.ObjetoID=b.ConceitoID AND a.TipoRelacaoID = " + optParam1 + " AND " +
                                 ExpLink + fldKey + Operator + textToSearch + " " +
                                 condition + filtro;

                        if (fldKey == "a.RelacaoID")
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,a.RelacaoID";
                        }

                        sql2 =
                            "SELECT TOP " + pageSize + " b.Conceito, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " +
                            "FROM RelacoesPesCon a WITH (NOLOCK), Conceitos b WITH (NOLOCK), @temptable z " +
                            "WHERE a.ObjetoID=b.ConceitoID AND a.TipoRelacaoID = " + optParam1 + " AND " +
                                 ExpLink + fldKey + Operator + textToSearch + " " +
                                 condition + filtro + " AND z.IDFromPesq=a.RelacaoID " +
                                 "AND z.IDTMP > " + StartID +
                            " ORDER BY z.IDTMP";
                    }
                    break;

                case 1320:   //formID  /basico/tabelasaux/tiposrel
                case 2120:   //formID  /basico/relacoes/relconceitos

                    if (nFolderID == "21042") // Cotacoes
                    {
                        if ((returnFldType("RelacoesConceitos_Cotacoes", sFldKey) == 135) &&
                            (textToSearch == "'0'"))
                        {
                            textToSearch = "0";
                        }

                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " +
                                    (int.Parse(nPageSize) * int.Parse(nPageNumber)) + " a.RelConCotacaoID " +
                                 "FROM RelacoesConceitos_Cotacoes a WITH (NOLOCK) " +
                                 "WHERE " +
                                 ExpLink + sFldKey + Operator + textToSearch + " " +
                                 sCondition + sFiltro;

                        if (sFldKey == "a.RelConCotacaoID")
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder;
                        }
                        else
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder + " ,a.RelConCotacaoID";
                        }

                        sql2 = "SELECT TOP " + nPageSize + " * " +
                                 "FROM RelacoesConceitos_Cotacoes a WITH (NOLOCK), @temptable z " +
                                 "WHERE " +
                                 ExpLink + sFldKey + Operator + textToSearch + " " +
                                 sCondition + sFiltro + " AND z.IDFromPesq=a.RelConCotacaoID " +
                                 "AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }

                    break;

                case 1220: //formID  /basico/relacoes/relpessoas
                case 1130: //formID  /basico/relacoes/relpessoaserecursos
                case 1310: //formID  /basico/tabelasaux/tiposaux

                    if (nFolderID == "20201") // Itens
                    {
                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " +
                                    (int.Parse(nPageSize) * int.Parse(nPageNumber)) + " a.ItemID " +
                                 "FROM TiposAuxiliares_Itens a WITH (NOLOCK) WHERE " +
                                 ExpLink + sFldKey + Operator + sTextToSearch + " " +
                                 sCondition + sFiltro;

                        if (sFldKey == "a.ItemID")
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder;
                        }
                        else
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder + " ,a.ItemID";
                        }

                        sql2 = "SELECT TOP " + nPageSize + " a.* " +
                                 "FROM TiposAuxiliares_Itens a WITH (NOLOCK), @temptable z WHERE " +
                                 ExpLink + sFldKey + Operator + sTextToSearch + " " +
                                 sCondition + sFiltro + " AND z.IDFromPesq=a.ItemID " +
                                 "AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }

                    break;

                //case 1320: //formID  /basico/tabelasaux/tiposrel
                case 2110: //formID  /pcm/conceitos

                    if (nFolderID == "21015") // Rel entre Conceitos
                    {
                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " +
                                    (int.Parse(nPageSize) * int.Parse(nPageNumber)) + " a.RelacaoID " +
                                 "FROM RelacoesConceitos a WITH (NOLOCK), Conceitos b WITH (NOLOCK) ";

                        if (vOptParam1 == "SUJEITO")
                        {
                            sql1 += "WHERE a.SujeitoID = b.ConceitoID AND a.TipoRelacaoID = " + vOptParam2 + " AND ";
                        }
                        else if (vOptParam1 == "OBJETO")
                        {
                            sql1 += "WHERE a.ObjetoID = b.ConceitoID AND a.TipoRelacaoID = " + vOptParam2 + " AND ";
                        }

                        sql1 += ExpLink + sFldKey + Operator + sTextToSearch + " " +
                                 sCondition + sFiltro;

                        if (sFldKey == "a.RelacaoID")
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder;
                        }
                        else
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder + " ,a.RelacaoID";
                        }

                        sql2 = "SELECT TOP " + nPageSize + " b.Conceito, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " +
                                 "FROM RelacoesConceitos a WITH (NOLOCK), Conceitos b WITH (NOLOCK), @temptable z ";

                        if (vOptParam1 == "SUJEITO")
                        {
                            sql2 += "WHERE a.SujeitoID = b.ConceitoID AND a.TipoRelacaoID = " + vOptParam2 + " AND ";
                        }
                        else if (vOptParam1 == "OBJETO")
                        {
                            sql2 += "WHERE a.ObjetoID = b.ConceitoID AND a.TipoRelacaoID = " + vOptParam2 + " AND ";
                        }

                        sql2 += ExpLink + sFldKey + Operator + sTextToSearch + " " +
                                 sCondition + sFiltro + " AND z.IDFromPesq=a.RelacaoID " +
                                 "AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }
                    else if (nFolderID == "21016") // Rel com Pessoas
                    {
                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " +
                                    (int.Parse(nPageSize) * int.Parse(nPageNumber)) + " a.RelacaoID " +
                                 "FROM RelacoesPesCon a WITH (NOLOCK), Pessoas b WITH (NOLOCK) " +
                                 "WHERE a.SujeitoID = b.PessoaID AND a.TipoRelacaoID = " + vOptParam1 + " AND " +
                                 ExpLink + sFldKey + Operator + sTextToSearch + " " +
                                 sCondition + sFiltro;

                        if (sFldKey == "a.RelacaoID")
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder;
                        }
                        else
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder + " ,a.RelacaoID";
                        }

                        sql2 = "SELECT TOP " + nPageSize + " b.Fantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " +
                                 "FROM RelacoesPesCon a WITH (NOLOCK), Pessoas b WITH (NOLOCK), @temptable z " +
                                 "WHERE a.SujeitoID = b.PessoaID AND a.TipoRelacaoID = " + vOptParam1 + " AND " +
                                 ExpLink + sFldKey + Operator + sTextToSearch + " " +
                                 sCondition + sFiltro + " AND z.IDFromPesq=a.RelacaoID " +
                                 "AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }

                    break;

                case 2130: //formID  /basico/relacoes/relpessoaseconceitos

                    if (nFolderID == "21066")
                    { // ' CustoMedio

                        // se o tipo do campo de pesquisa for data
                        if ((returnFldType("RelacoesPesCon_Custos_Antiga", sFldKey) == 135) && (textToSearch == "'0'"))
                        {
                            textToSearch = "0";
                        }

                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " + ((int.Parse(pageSize) * int.Parse(pageNumber))) + " a.RelPesConCustoID AS IDFromPesq " +
                             "FROM RelacoesPesCon_Custos_Antiga a WITH(NOLOCK) " +
                                 "INNER JOIN RelacoesPesCon h WITH(NOLOCK) ON (a.RelacaoID = h.RelacaoID) " +
                             "WHERE " + ExpLink + fldKey + Operator + textToSearch + " " + condition + sFiltro;

                        if (fldKey == "a.RelPesConCustoID")
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        else
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,a.RelPesConCustoID " + order;


                        sql2 = "SELECT TOP " + int.Parse(pageSize) + " " +
                                "dbo.fn_Data_Fuso(a.dtCusto, NULL, 15231) AS Data, e.ItemAbreviado AS Metodo, dbo.fn_RelPesConCusto_Encomenda(a.RelPesConCustoID) AS Encomenda, " +
                                "a.QuantidadeEstoque AS QuantidadeEstoque, a.QuantidadePedido AS QuantidadePedido, A.QuantidadeTotal AS QuantidadeTotal, b.SimboloMoeda AS Moeda, a.CustoEstoque AS CustoEstoque, " +
                                "a.CustoPedido AS CustoPedido, a.CustoMedio AS CustoMedio, a.DespesaEstoque AS DespesaEstoque, a.DespesaPedido AS DespesaPedido, a.DespesaMedia AS DespesaMedia, a.dtPagamentoEstoque AS dtPagamentoEstoque, " +
                                "a.dtPagamentoPedido AS dtPagamentoPedido, a.dtMediaPagamento AS dtMediaPagamento, c.SimboloMoeda AS Moeda1, a.CustoContabilEstoque AS CustoContabilEstoque, a.CustoContabilPedido AS CustoContabilPedido, " +
                                "a.CustoContabilMedio AS CustoContabilMedio, d.PedidoID AS PedidoID, d.CFOPID AS CFOPID, " +
                                "g.Fantasia AS Pessoa, a.ValorUnitario AS Unitario, a.TaxaMoeda AS Taxa, a.Observacao AS Observacao, a.RelPesConCustoID AS RelPesConCustoID " +
                            "FROM RelacoesPesCon_Custos_Antiga a WITH(NOLOCK) " +
                                "LEFT OUTER JOIN Conceitos b WITH(NOLOCK) ON (a.MoedaCustoID = b.ConceitoID) " +
                                "LEFT OUTER JOIN Conceitos c WITH(NOLOCK) ON (a.MoedaPedidoID = c.ConceitoID) " +
                                "INNER JOIN Pedidos_Itens d WITH(NOLOCK) ON (a.PedItemID = d.PedItemID) " +
                                "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.TipoCustoID = e.ItemID) " +
                                "INNER JOIN Pedidos f WITH(NOLOCK) ON (d.PedidoID = f.PedidoID) " +
                                "INNER JOIN Pessoas g WITH(NOLOCK) ON (f.PessoaID = g.PessoaID) " +
                                "INNER JOIN @temptable z ON (z.IDFromPesq = a.RelPesConCustoID) " +
                            "WHERE " +
                                ExpLink + fldKey + Operator + textToSearch + " " +
                                condition + filtro + " " +
                                "AND z.IDTMP > " + StartID + " " +
                            "ORDER BY z.IDTMP";
                    }

                    else if (nFolderID == "21067")// Then ' Pedidos
                    {

                        string sDataIsNull, sAuxChar;

                        sDataIsNull = "";
                        sAuxChar = "";

                        //' se o tipo do campo de pesquisa for data
                        if ((returnFldType("Pedidos_Itens", fldKey) == 135) && (textToSearch == "'0'"))
                        {
                            textToSearch = "0";
                            sAuxChar = "(";
                            sDataIsNull = " OR " + fldKey + " IS NULL) ";
                        }

                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " + ((int.Parse(pageSize) * int.Parse(pageNumber))) + " b.PedItemID " +
                            "FROM RelacoesPesCon a WITH(NOLOCK) " +
                                "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.ProdutoID = a.ObjetoID) " +
                                    "INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " +
                                        "INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID) " +
                                        "INNER JOIN Conceitos e WITH(NOLOCK) ON (e.ConceitoID = c.MoedaID) " +
                                        "INNER JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID = c.PessoaID) " +
                                        "INNER JOIN Pessoas g WITH(NOLOCK) ON (g.PessoaID = c.ProprietarioID) " +
                            "WHERE (c.EmpresaID = a.SujeitoID) AND " +
                                 ExpLink + sAuxChar + fldKey + Operator + textToSearch + sDataIsNull +
                                 condition + filtro;

                        if (fldKey == "b.PedItemID")
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        else
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + ", b.PedItemID";


                        sql2 = "SELECT TOP " + int.Parse(pageSize) + " " +
                              "b.dtMovimento AS dtMovimento, c.PedidoID AS PedidoID, " +
                              "d.RecursoAbreviado AS Estado, f.Fantasia AS Pessoa, " +
                               "CONVERT(VARCHAR, b.CFOPID) + ' - ' + dbo.fn_Tradutor(h.Operacao, 246, dbo.fn_Pais_idioma(dbo.fn_Pessoa_Localidade(c.EmpresaID, 1, NULL,	NULL)), 'A') AS Operacao, " +
                              "b.Quantidade AS Quantidade, e.SimboloMoeda AS Moeda, b.ValorUnitario AS ValorUnitario, " +
                              "CONVERT(NUMERIC(11,2), b.ValorContribuicaoAjustada * c.TaxaMoeda) AS Contribuicao, " +
                              "ROUND((dbo.fn_DivideZero(b.ValorContribuicaoAjustada, b.ValorFaturamentoBase)*100), 2) AS Margem, " +
                              "g.Fantasia AS Vendedor, " +
                              "b.PedidoReferencia AS PedidoReferencia, c.Observacao AS Observacao " +
                          "FROM RelacoesPesCon a WITH(NOLOCK) " +
                              "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.ProdutoID = a.ObjetoID) " +
                                  "INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " +
                                      "INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID) " +
                                      "INNER JOIN Conceitos e WITH(NOLOCK) ON (e.ConceitoID = c.MoedaID) " +
                                      "INNER JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID = c.PessoaID) " +
                                      "INNER JOIN Pessoas g WITH(NOLOCK) ON (g.PessoaID = c.ProprietarioID) " +
                                      "LEFT JOIN Operacoes h WITH(NOLOCK) ON (h.OperacaoID = b.CFOPID) " +
                                  "INNER JOIN @temptable z ON (z.IDFromPesq = b.PedItemID) " +
                          "WHERE (c.EmpresaID = a.SujeitoID) AND " +
                              ExpLink + sAuxChar + fldKey + Operator + textToSearch + sDataIsNull +
                              condition + filtro + " AND (z.IDTMP > " + StartID + ") " +
                          "ORDER BY z.IDTMP";
                    }
                    else if (folderId == "21068") // Movimentos
                    {
                        //' se o tipo do campo de pesquisa for data
                        if ((returnFldType("RelacoesPesCon_Movimentos", fldKey) == 135) && (textToSearch == "'0'"))
                        {
                            textToSearch = "0";
                        }


                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " + ((int.Parse(pageSize) * int.Parse(pageNumber))) + " b.RelPesConMovimentoID " +
                            "FROM RelacoesPesCon_Movimentos b WITH(NOLOCK) " +
                                "INNER JOIN RelacoesPesCon a WITH(NOLOCK) ON ((a.ObjetoID = b.ProdutoID) AND (a.SujeitoID = b.EmpresaID)) " +
                            "WHERE " + ExpLink + fldKey + Operator + textToSearch + " " + condition + sFiltro;

                        if (fldKey == "b.RelPesConMovimentoID")
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order;
                        else
                            sql1 = sql1 + " ORDER BY " + fldKey + " " + order + " ,b.RelPesConMovimentoID";


                        sql2 = "SELECT TOP " + int.Parse(pageSize) + " " +
                                "dbo.fn_Data_Fuso(b.dtMovimento, NULL, " + (empresaCidadeId.ToString()) + ") AS dtMovimento, b.PedidoID AS PedidoID, " +
                                "b.AsstecID AS AsstecID, c.Operacao AS Transacao, e.ItemAbreviado AS Estoque, d.Fantasia AS Deposito, " +
                                "b.Quantidade AS Quantidade " +
                            "FROM RelacoesPesCon_Movimentos b WITH (NOLOCK) " +
                                "INNER JOIN RelacoesPesCon a WITH (NOLOCK) ON ((a.ObjetoID = b.ProdutoID) AND (a.SujeitoID = b.EmpresaID)) " +
                                "LEFT OUTER JOIN Operacoes c WITH (NOLOCK) ON (c.OperacaoID = b.TransacaoID) " +
                                "LEFT OUTER JOIN Pessoas d WITH (NOLOCK) ON (d.PessoaID = b.DepositoID) " +
                                "INNER JOIN TiposAuxiliares_Itens e WITH (NOLOCK) ON (e.ItemID = b.EstoqueID) " +
                                "INNER JOIN @temptable z ON (z.IDFromPesq = b.RelPesConMovimentoID) " +
                            "WHERE " +
                                ExpLink + fldKey + Operator + textToSearch + " " +
                                condition + filtro + " " +
                                "AND z.IDTMP > " + StartID + " " +
                            "ORDER BY z.IDTMP";
                    }
                    break;

                case 5110: //formID  /modcomercial/subpedidos/frmpedidos
                case 5220: //formID  /modcomercial/subatendimento/listaprecos
                case 5320: //formID  /modcomercial/submodimportacao/siscoserv
                    //RAS
                    if (int.Parse(folderId) == 24311)
                    {
                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " +
                             (int.Parse(pageSize) * int.Parse(pageNumber)) + " a.SisPedidoID " +
                                  " FROM Siscoserv_Pedidos a WITH(NOLOCK) " +
                                  " LEFT OUTER JOIN SiscoServ_Retornos d WITH(NOLOCK) ON (a.SisRetornoID = d.SisRetornoID) " +
                                  " WHERE  " + ExpLink + optParam1;

                        sql2 = "SELECT TOP " + pageSize + " " +
                               " a.SisPedidoID AS SisPedidoID, a.SiscoServID AS SiscoServID, c.ArquivoZip AS ArquivoZip, " +
                               " ISNULL(dbo.fn_TipoAuxiliar_Item(d.StatusArquivoID, 2), SPACE(1)) AS StatusArquivo, ISNULL(dbo.fn_TipoAuxiliar_Item(d.ErroID, 2), SPACE(1)) AS Erro, " +
                               " a.PedidoID AS PedidoID, " +
                               " ISNULL(a.RAS, SPACE(1)) AS RAS, b.NBS AS NBS, b.dtInicio AS dtInicio, b.dtFim AS dtFim, " +
                               " dbo.fn_SiscoServ_Totais(a.SiscoServID, b.SisPedidoID, NULL, NULL, NULL, 1) AS Valor, " +
                               " dbo.fn_SiscoServ_Totais(a.SiscoServID, b.SisPedidoID, NULL, NULL, NULL, 2) AS Saldo, " +
                               " a.Arquivo as Arquivo, a.ArquivoXML as ArquivoXML, " +
                               " a.Observacao AS Observacao, ISNULL(dbo.fn_TipoAuxiliar_Item(d.StatusArquivoID, 0), SPACE(1)) AS StatusDescricao, " +
                               " ISNULL(dbo.fn_TipoAuxiliar_Item(d.ErroID, 0), SPACE(1)) AS ErroDescricao, ISNULL(dbo.fn_TipoAuxiliar_Item(d.ErroID, 5), 1) AS ErroAplica " +
                               "  FROM Siscoserv_Pedidos a WITH(NOLOCK) " +
                               " INNER JOIN SiscoServ_Pedidos_Servicos b WITH(NOLOCK) ON (a.SisPedidoID = b.SisPedidoID) " +
                               " LEFT OUTER JOIN Siscoserv_Protocolos c WITH(NOLOCK) ON (a.SisProtocoloID = c.SisProtocoloID) " +
                               " LEFT OUTER JOIN SiscoServ_Retornos d WITH(NOLOCK) ON (a.SisRetornoID = d.SisRetornoID) " +
                               " INNER JOIN @temptable z ON (z.IDFromPesq=a.SisPedidoID) " +
                               " WHERE  " + ExpLink + optParam1 +
                               " AND z.IDTMP > " + StartID +
                               " ORDER BY z.IDTMP";

                    }

                        //Pagamentos
                    else if (int.Parse(folderId) == 24312)
                    {
                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " +
                             (int.Parse(pageSize) * int.Parse(pageNumber)) + " a.SisPagamentoID " +
                                  " FROM Siscoserv_Pagamentos a WITH(NOLOCK) " +
                                  " LEFT OUTER JOIN SiscoServ_Retornos d WITH(NOLOCK) ON (a.SisRetornoID = d.SisRetornoID) " +
                                  " WHERE  " + ExpLink + optParam1;

                        sql2 = "SELECT TOP " + pageSize + " " +
                               " a.SisPagamentoID AS SisPagamentoID, a.SiscoServID AS SiscoServID, c.ArquivoZip AS ArquivoZip, " +
                               " ISNULL(dbo.fn_TipoAuxiliar_Item(d.StatusArquivoID, 2), SPACE(1)) AS StatusArquivo, ISNULL(dbo.fn_TipoAuxiliar_Item(d.ErroID, 2), SPACE(1)) AS Erro,  " +
                               " a.FinanceiroID AS FinanceiroID,  a.ValorID AS ValorID, ISNULL(a.RP, SPACE(1)) as RP, " +
                               " CONVERT(NUMERIC(5,2), (dbo.fn_DivideZero(b.ValorPercentual, b.Valor) * 100)) AS PercentualPago, a.Arquivo as Arquivo, a.Observacao AS Observacao,  " +
                               " ISNULL(dbo.fn_TipoAuxiliar_Item(d.StatusArquivoID, 0), SPACE(1)) AS StatusDescricao, " +
                               " ISNULL(dbo.fn_TipoAuxiliar_Item(d.ErroID, 0), SPACE(1)) AS ErroDescricao, ISNULL(dbo.fn_TipoAuxiliar_Item(d.ErroID, 5), 1) AS ErroAplica " +
                               " FROM Siscoserv_Pagamentos a WITH(NOLOCK) " +
                               " INNER JOIN SiscoServ_Pagamentos_Servicos b WITH(NOLOCK) ON (a.SisPagamentoID = b.SisPagamentoID) " +
                               " LEFT OUTER JOIN Siscoserv_Protocolos c WITH(NOLOCK) ON (a.SisProtocoloID = c.SisProtocoloID) " +
                               " LEFT OUTER JOIN SiscoServ_Retornos d WITH(NOLOCK) ON (a.SisRetornoID = d.SisRetornoID) " +
                               " INNER JOIN @temptable z ON (z.IDFromPesq = a.SisPagamentoID) " +
                               " WHERE  " + ExpLink + optParam1 +
                               " AND z.IDTMP > " + StartID +
                               " GROUP BY a.SisPagamentoID, a.SiscoServID, c.ArquivoZip, dbo.fn_TipoAuxiliar_Item(d.StatusArquivoID, 2), dbo.fn_TipoAuxiliar_Item(d.ErroID, 2),  " +
                               " a.FinanceiroID, a.ValorID, a.RP, a.Arquivo, a.Observacao, " +
                               "dbo.fn_TipoAuxiliar_Item(d.StatusArquivoID, 0), dbo.fn_TipoAuxiliar_Item(d.ErroID, 0), dbo.fn_TipoAuxiliar_Item(d.ErroID, 5), " +
                               "CONVERT(NUMERIC(5,2), (dbo.fn_DivideZero(b.ValorPercentual, b.Valor) * 100))";
                    }

                    break;

                case 10110: //formID /modcontabil/submodcontabilidade/planocontas
                    // Lancamentos
                    if (int.Parse(folderId) == 29001)
                    {
                        if (returnFldType("Lancamentos", fldKey) == 135 && textToSearch.Equals("'0'"))
                        {
                            textToSearch = "0";
                        }

                        string PessoasJoinOperator, ImpostosJoinOperator;

                        // Pessoas
                        if (int.Parse(optParam1) >= 1532 && int.Parse(optParam1) <= 1541)
                        {
                            PessoasJoinOperator = " INNER JOIN ";
                            ImpostosJoinOperator = " LEFT OUTER JOIN ";
                        }
                        // RelPesConta
                        else if (int.Parse(optParam1) == 1542)
                        {
                            PessoasJoinOperator = " LEFT OUTER JOIN ";
                            ImpostosJoinOperator = " LEFT OUTER JOIN ";
                        }
                        // Impostos
                        else if (int.Parse(optParam1) == 1543)
                        {
                            PessoasJoinOperator = " LEFT OUTER JOIN ";
                            ImpostosJoinOperator = " INNER JOIN ";
                        }
                        else
                        {
                            PessoasJoinOperator = " LEFT OUTER JOIN ";
                            ImpostosJoinOperator = " LEFT OUTER JOIN ";
                        }

                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " +
                            (int.Parse(pageSize) * int.Parse(pageNumber)) + " a.LanContaID " +
                                 "FROM Lancamentos_Contas a WITH (NOLOCK) " +
                                    "INNER JOIN Lancamentos b WITH (NOLOCK) ON (a.LancamentoID = b.LancamentoID) " +
                                    "INNER JOIN Recursos c WITH (NOLOCK) ON (b.EstadoID = c.RecursoID) " +
                                    "INNER JOIN Conceitos d WITH (NOLOCK) ON (b.MoedaID = d.ConceitoID) " +
                                    PessoasJoinOperator + " Pessoas e WITH (NOLOCK) ON (a.PessoaID = e.PessoaID) " +
                                    ImpostosJoinOperator + " Conceitos f WITH (NOLOCK) ON (a.ImpostoID = f.ConceitoID) " +
                                 "WHERE " +
                                 "dbo.fn_Conta_EhFilha(a.ContaID, " + linkFieldValue + ", 1) = 1 AND " +
                                 "b.EmpresaID = " + empresaId + " AND " +
                                 "b.EstadoID IN (2, 81, 82) AND " +
                                 sohConciliacao +
                                 fldKey + Operator + textToSearch + " " +
                                 condition + filtro;

                        if (fldKey.Equals("a.LanContaID"))
                        {
                            sql1 += " ORDER BY " + fldKey + " " + order;
                        }
                        else
                        {
                            sql1 += " ORDER BY " + fldKey + " " + order + " ,a.LanContaID " + order;
                        }

                        sql2 = "SELECT TOP " + pageSize + " " +
                                     "a.LanContaID, b.dtLancamento, b.dtBalancete, b.LancamentoID, c.RecursoAbreviado AS Estado," +
                                     "dbo.fn_Lancamento_Contrapartida(a.LanContaID) AS Contrapartida, d.SimboloMoeda, a.Valor," +
                                     "0 AS Saldo," +
                                     "a.Documento, e.Fantasia, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS ContaBancaria," +
                                     "f.Imposto AS Imposto, dbo.fn_Lancamento_Historico(b.LancamentoID, 1) AS Historico " +
                                 "FROM Lancamentos_Contas a WITH (NOLOCK) " +
                                    "LEFT OUTER JOIN Pessoas e WITH (NOLOCK) ON (a.PessoaID = e.PessoaID) " +
                                    "LEFT OUTER JOIN Conceitos f WITH (NOLOCK) ON (a.ImpostoID = f.ConceitoID) " +
                                    "INNER JOIN Lancamentos b WITH (NOLOCK) ON (a.LancamentoID = b.LancamentoID) " +
                                    "INNER JOIN Recursos c WITH (NOLOCK) ON (b.EstadoID = c.RecursoID) " +
                                    "INNER JOIN Conceitos d WITH (NOLOCK) ON (b.MoedaID = d.ConceitoID) " +
                                    "INNER JOIN @temptable z ON (z.IDFromPesq=a.LanContaID) " +
                                 "WHERE b.EstadoID IN (2, 81, 82) AND " +
                                 fldKey + Operator + textToSearch + " " +
                                 condition + filtro + " " +
                                 "AND z.IDTMP > " + StartID +
                                 " ORDER BY z.IDTMP";
                    }
                    break;
                /*case 5420: //formID  /submodtransporte/faturatransporte
                    if (folderId == "24351") // DocumentoTransporte
                    {
                        string fldKeyOld = fldKey;
                        
                        //feito isso porque n�o pode alterar no banco, pois � utilizado em mais lugares
                        sFiltro = sFiltro.Replace("a.EstadoID", "b.EstadoID");

                        // se o tipo do campo de pesquisa for data
                        if (textToSearch == "'0'")
                        {
                            textToSearch = "0";
                        }
                        
                        sql1 = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " +
                                    (int.Parse(nPageSize) * int.Parse(nPageNumber)) + " a.FatDocumentoTransporteID " +
                                "FROM  dbo.Faturastransporte_documentostransporte a WITH(NOLOCK) " +
                                "INNER JOIN dbo.DocumentosTransporte b WITH(NOLOCK) ON (a.DocumentoTransporteID= b.DocumentoTransporteID) " +
                                "WHERE " + sFldKey + Operator + sTextToSearch + " " + sCondition + sFiltro;

                        if (sFldKey == "b.Numero")
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder;
                        }
                        else
                        {
                            sql1 += " ORDER BY " + sFldKey + " " + nOrder + " ,a.FatDocumentoTransporteID";
                        }
                        
                        
                        sql2 = "SELECT TOP " + nPageSize + " " +
                                " a.DocumentoTransporteID AS DocumentoTransporteID,  " +
                                " b.Numero AS NumeroDocumentoTransporte,  " +
                                "dbo.fn_Recursos_Campos(b.EstadoID, 1) AS Estado,  " +
                                "b.dtEmissao AS dtEmissaoDocumentoTransporte, " +
                            //'b.dtEmissao AS V_dtEmissaoDocumentoTransporte, ' +
                                "b.ValorTotal, " +
                                "dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 1) AS ValorTotalParcelasFrete, " +
                                "dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 2) AS ValorDiferenca, " +
                                "dbo.fn_DocumentoTransporte_Totais(a.DocumentoTransporteID, 3) AS PercentualDiferenca, " +
                                "b.ValorDesconto, " +
                                "dbo.fn_TipoAuxiliar_Item(b.TipoDocumentoTransporteID,4) AS Tipo, " +
                                "ISNULL(d.MensagemErro, e.Observacao) AS Problema, " +
                                "b.Observacao as Observacao, " +
                                "a.FatDocumentoTransporteID " +
                            "FROM  dbo.FaturasTransporte_DocumentosTransporte a WITH(NOLOCK) " +
                                "INNER JOIN dbo.DocumentosTransporte b WITH(NOLOCK) ON (a.DocumentoTransporteID= b.DocumentoTransporteID) " +
                                "LEFT OUTER JOIN dbo.TransportadoraDados_Detalhes_Erros c WITH(NOLOCK) ON (c.RegistroID = a.DocumentoTransporteID) " +
                                "LEFT OUTER JOIN dbo.FreteErros d WITH(NOLOCK) ON (d.FreteErroID = c.CodigoErro) " +
                                "LEFT OUTER JOIN TransportadoraDados_Detalhes e ON (e.TransportadoraArquivoDetalheID = b.TransportadoraArquivoDetalheID) " +
                                "INNER JOIN @temptable z ON (z.IDFromPesq = a.FatDocumentoTransporteID) " +
                            "WHERE " +
                                sFldKey + Operator + sTextToSearch + " " +
                                sCondition + sFiltro + " " +
                                "AND z.IDTMP > " + StartID + " " +
                            "ORDER BY z.IDTMP";
                    }

					break;*/

                default:
                    sql1 = "";
                    break;
            }
        }

        private int returnFldType(String tableName, String fieldName)
        {
            string campo = fieldName.Substring(2);

            System.Data.DataSet rsTable =
                DataInterfaceObj.getRemoteData("SELECT TOP 1 * FROM " + tableName + " WITH(NOLOCK) ");

            int fieldsLength = rsTable.Tables[0].Rows.Count;

            for (int field = 0; fieldsLength > 0 && field < fieldsLength; field++)
            {
                if (rsTable.Tables[0].Rows[field][0].Equals(campo))
                {
                    return GetFieldDataTypeCode(rsTable.Tables[0].Rows[field][24].ToString());
                }
            }

            return -1;
        }

        private string stringWithNoNull(string val)
        {
            if (val != null) return val;
            else return "";
        }
    }
}
