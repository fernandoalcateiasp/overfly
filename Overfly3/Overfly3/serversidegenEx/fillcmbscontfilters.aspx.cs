using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSOverflyRDS;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class fillcmbscontfilters : System.Web.UI.Page
    {
        private string m_UserID;
        private string m_EmpresaID;
        private string m_FormID;

        private string m_StrXML;
        public string StrXML
        {
            get { return m_StrXML; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            Response.Cache.SetNoStore();
            Response.Cache.SetCacheability(HttpCacheability.NoCache);

            dataInterface oDataInterface = new dataInterface(
                System.Configuration.ConfigurationManager.AppSettings["application"]
            );

            System.Xml.XmlDocument xmlDataSet;

            // L� os par�metros da p�gina
            m_UserID = Request.QueryString["userID"];
            m_EmpresaID = Request.QueryString["empresaID"];
            m_FormID = Request.QueryString["formID"];

            int defContextID = 0;

            // Os contextos
            System.Data.DataSet dsData1 = oDataInterface.getRemoteData(
                "SELECT DISTINCT 'sup' AS ControlBar, '1' AS Combo,a.RecursoFantasia AS Recurso,a.RecursoID,-1 AS SubFormID, " +
                "a.Ordem AS Ordem, a.EhDefault AS EhDefault, g.Consultar1 AS C1,g.Consultar2 AS C2 ,g.Incluir AS I, " +
                "g.Alterar1 AS A1,g.Alterar2 AS A2,g.Excluir1 AS E1,g.Excluir2 AS E2, ISNULL(h.SujeitoID, 0) AS EstadoInicialID " +
                "FROM Recursos a WITH(NOLOCK),RelacoesPesRec b WITH(NOLOCK), RelacoesPesRec_Perfis c WITH(NOLOCK), Recursos_Direitos d WITH(NOLOCK), " +
                "RelacoesRecursos e WITH(NOLOCK) " +
                "LEFT OUTER JOIN RelacoesRecursos h WITH(NOLOCK) ON (e.MaquinaEstadoID=h.ObjetoID AND h.Ordem=1 AND h.TipoRelacaoID=3), " +
                "Recursos f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos i WITH(NOLOCK) " +
                "WHERE a.TipoRecursoID = 3 AND a.EstadoID=2 AND a.RecursoMaeID=" + (m_FormID) + " " +
                "AND b.ObjetoID=999 AND b.TipoRelacaoID=11 " +
                "AND b.SujeitoID IN ((SELECT " + (m_UserID) + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + (m_UserID) + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND b.RelacaoID=c.RelacaoID " +
                "AND c.EmpresaID=" + (m_EmpresaID) + " AND d.RecursoID=a.RecursoID AND c.PerfilID=d.PerfilID " +
                "AND (d.Consultar1=1 OR d.Consultar2=1) AND a.RecursoID=e.ObjetoID AND e.TipoRelacaoID=1 AND e.EstadoID=2 " +
                "AND e.SujeitoID=f.RecursoID AND f.Principal=1 AND f.RecursoID=g.RecursoID AND a.RecursoID=g.RecursoMaeID AND g.PerfilID=c.PerfilID " +
                "AND c.PerfilID = i.RecursoID AND i.EstadoID = 2 " +
                "ORDER BY a.Ordem"
            );

            // Converte o DSO para XML
            if (dsData1.Tables[1].Rows.Count == 0)
            {
                // Converte o DSO para XML
                xmlDataSet = oDataInterface.datasetToXML(dsData1);
                // Concatena o cabecalho e o conteudo do rsultado.
                m_StrXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                    xmlDataSet.InnerXml;

                return;
            }

            // Obter o Contexto default ou primeiro Contexto se nao tem default
            // 1. O primeiro Contexto por garantia
            defContextID = int.Parse(dsData1.Tables[1].Rows[0]["RecursoID"].ToString());

            // 2. Tenta obter o Contexto default
            for (int i = 0; i < dsData1.Tables[1].Rows.Count; i++)
            {
                if (dsData1.Tables[1].Rows[i]["EhDefault"] != null &&
                    (bool)dsData1.Tables[1].Rows[i]["EhDefault"])
                {
                    defContextID = int.Parse(dsData1.Tables[1].Rows[i]["RecursoID"].ToString());

                    break;
                }
            }

            // Os filtros do subform superior associados ao presente contexto
            string sql =
                "SELECT DISTINCT 'sup' AS ControlBar, '2' AS Combo, d.RecursoID AS RecursoID, " +
                "d.RecursoFantasia AS Recurso, c.Ordem AS Ordem, a.SujeitoID AS SubFormID, " +
                "c.EhDefault, 0 AS C1, 0 AS C2, 0 AS I, 0 AS A1, 0 AS A2, 0 AS E1, 0 AS E2, 0 AS EstadoInicialID " +
                "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec e WITH(NOLOCK), " +
                "RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " +
                "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID=" + defContextID + " " +
                "AND a.SujeitoID=b.RecursoID AND b.Principal=1 AND a.SujeitoID=c.ObjetoID AND c.EstadoID=2 " +
                "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.ClassificacaoID=12 " +
                "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) " +
                "WHERE a.RecursoID=" + defContextID + ")) " +
                "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " +
                "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " +
                "AND e.SujeitoID IN ((SELECT " + (m_UserID) + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + (m_UserID) + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND e.RelacaoID=f.RelacaoID " +
                "AND f.EmpresaID=" + (m_EmpresaID) + " AND a.ObjetoID=g.ContextoID AND c.ObjetoID=g.RecursoMaeID AND c.SujeitoID=g.RecursoID " +
                "AND (g.Consultar1=1 OR g.Consultar2=1) " +
                "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " +
                "ORDER BY c.Ordem";

            // Executa procedure sp_RelatorioEstoque
            ProcedureParameters[] procParams = new ProcedureParameters[1];
            procParams[0] = new ProcedureParameters(
                "@sSQL",
                System.Data.SqlDbType.VarChar,
                sql);
            procParams[0].Length = sql.Length;

            System.Data.DataSet dsData2 = oDataInterface.execQueryProcedure(
                                                "sp_RelatorioEstoque",
                                                procParams);

            // Os filtros do subform inferior associados ao presente contexto
            sql =
                "SELECT DISTINCT 'inf' AS ControlBar, '2' AS Combo, d.RecursoID AS RecursoID, " +
                "d.RecursoFantasia AS Recurso, c.Ordem AS Ordem, a.SujeitoID AS SubFormID, " +
                "c.EhDefault, 0 AS C1, 0 AS C2, 0 AS I, 0 AS A1, 0 AS A2, 0 AS E1, 0 AS E2, 0 AS EstadoInicialID " +
                "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec e WITH(NOLOCK), " +
                "RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " +
                "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID=" + defContextID + " " +
                "AND a.SujeitoID=b.RecursoID AND b.Principal=0 AND a.SujeitoID=c.ObjetoID AND c.EstadoID=2 " +
                "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.ClassificacaoID=12 " +
                "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) " +
                "WHERE a.RecursoID=" + defContextID + ")) " +
                "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " +
                "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " +
                "AND e.SujeitoID IN ((SELECT " + (m_UserID) + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + m_UserID.ToString() + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND e.RelacaoID=f.RelacaoID " +
                "AND f.EmpresaID=" + (m_EmpresaID) + " AND a.ObjetoID=g.ContextoID AND c.ObjetoID=g.RecursoMaeID AND c.SujeitoID=g.RecursoID " +
                "AND (g.Consultar1=1 OR g.Consultar2=1) " +
                "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " +
                "ORDER BY c.Ordem";

            // Executa procedure sp_RelatorioEstoque
            procParams = new ProcedureParameters[1];
            procParams[0] = new ProcedureParameters(
                "@sSQL",
                System.Data.SqlDbType.VarChar,
                sql);
            procParams[0].Length = sql.Length;

            System.Data.DataSet dsData3 = oDataInterface.execQueryProcedure(
                                                "sp_RelatorioEstoque",
                                                procParams);

            // As pastas do subform inferior associadas ao presente contexto
            sql = "SELECT DISTINCT 'inf' AS ControlBar, '1' AS Combo, b.RecursoID AS RecursoID, " +
                "b.RecursoFantasia AS Recurso, b.RecursoID AS Ordem, 0 AS SubFormID, " +
                "0 AS EhDefault, e.Consultar1 AS C1, e.Consultar2 AS C2, e.Incluir AS I, " +
                "e.Alterar1 AS A1, e.Alterar2 AS A2, e.Excluir1 AS E1, e.Excluir2 AS E2, " +
                "ISNULL(f.SujeitoID,0) AS EstadoInicialID " +
                "FROM RelacoesRecursos a WITH(NOLOCK) " +
                "LEFT OUTER JOIN RelacoesRecursos f ON (a.MaquinaEstadoID=f.ObjetoID AND f.Ordem=1 AND f.TipoRelacaoID=3), " +
                "Recursos b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK), " +
                "RelacoesPesRec_Perfis d WITH(NOLOCK), Recursos_Direitos e WITH(NOLOCK), Recursos g WITH(NOLOCK) " +
                "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID= " + defContextID + " " +
                "AND a.SujeitoID=b.RecursoID AND b.Principal=0 AND b.EstadoID=2 " +
                "AND c.ObjetoID=999 AND c.TipoRelacaoID=11 " +
                "AND c.SujeitoID IN ((SELECT " + (m_UserID) + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + (m_UserID) + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND c.RelacaoID=d.RelacaoID " +
                "AND d.EmpresaID= " + (m_EmpresaID) +
                " AND a.ObjetoID=e.RecursoMaeID AND a.SujeitoID=e.RecursoID " +
                "AND (e.Consultar1=1 OR e.Consultar2=1) AND d.PerfilID=e.PerfilID " +
                "AND d.PerfilID = g.RecursoID AND g.EstadoID = 2 " +
                "ORDER BY Ordem";

            // Executa procedure sp_RelatorioEstoque
            procParams = new ProcedureParameters[1];
            procParams[0] = new ProcedureParameters(
                "@sSQL",
                System.Data.SqlDbType.VarChar,
                sql);
            procParams[0].Length = sql.Length;

            System.Data.DataSet dsData4 = oDataInterface.execQueryProcedure("sp_RelatorioEstoque", procParams);

            ///////////////////////////////////////////////////////////////////
            int recursoID;
            bool dirC1;
            bool dirC2;
            bool dirI;
            bool dirA1;
            bool dirA2;
            bool dirE1;
            bool dirE2;
            bool dirC1C2True;
            bool dirA1A2True;
            bool dirE1E2True;
            //bool dirITrue;
            string sBtnNumber;

            int rowNumber;
            DataRow sourceRow;
            DataRow targetRow;
            DataRow[] rowsResult;
            DataRow[] rowsDsData5;

            DataSet result = makeResultDataSet();

            // Copia o conteudo dos rds para o rsNew
            // Copia os contextos    
            for (rowNumber = 0; rowNumber < dsData1.Tables[1].Rows.Count; rowNumber++)
            {
                sourceRow = dsData1.Tables[1].Rows[rowNumber];
                targetRow = result.Tables[1].NewRow();

                recursoID = (int)sourceRow["RecursoID"];
                dirC1 = (bool)sourceRow["C1"];
                dirC2 = (bool)sourceRow["C2"];
                dirI = (bool)sourceRow["I"];
                dirA1 = (bool)sourceRow["A1"];
                dirA2 = (bool)sourceRow["A2"];
                dirE1 = (bool)sourceRow["E1"];
                dirE2 = (bool)sourceRow["E2"];

                targetRow["ControlBar"] = sourceRow["ControlBar"];
                targetRow["Combo"] = sourceRow["Combo"];
                targetRow["Recurso"] = sourceRow["Recurso"];
                targetRow["RecursoID"] = sourceRow["RecursoID"];
                targetRow["SubFormID"] = sourceRow["SubFormID"];
                targetRow["EhDefault"] = sourceRow["EhDefault"];
                targetRow["EstadoInicialID"] = sourceRow["EstadoInicialID"];

                dirC1C2True = (dirC1 && dirC2);
                dirA1A2True = (dirA1 && dirA2);
                dirE1E2True = (dirE1 && dirE2);

                // Pega a proxima linha;
                for (rowNumber++; rowNumber < dsData1.Tables[1].Rows.Count; rowNumber++)
                {
                    sourceRow = dsData1.Tables[1].Rows[rowNumber];

                    if (recursoID == (int)sourceRow["RecursoID"])
                    {
                        dirC1C2True = ((bool)sourceRow["C1"] && (bool)sourceRow["C2"]) ? true : dirC1C2True;
                        dirA1A2True = ((bool)sourceRow["A1"] && (bool)sourceRow["A2"]) ? true : dirA1A2True;
                        dirE1E2True = ((bool)sourceRow["E1"] && (bool)sourceRow["E2"]) ? true : dirE1E2True;

                        dirC1 = dirC1 || ((bool)sourceRow["C1"]);
                        dirC2 = dirC2 || ((bool)sourceRow["C2"]);
                        dirI = dirI || ((bool)sourceRow["I"]);
                        dirA1 = dirA1 || ((bool)sourceRow["A1"]);
                        dirA2 = dirA2 || ((bool)sourceRow["A2"]);
                        dirE1 = dirE1 || ((bool)sourceRow["E1"]);
                        dirE2 = dirE2 || ((bool)sourceRow["E2"]);
                    }
                    else
                    {
                        break;
                    }
                }

                rowNumber--;

                if (dirC1 && dirC2 && !dirC1C2True)
                {
                    dirC1 = false;
                    dirC2 = true;
                }
                if (dirA1 && dirA2 && !dirA1A2True)
                {
                    dirA1 = true;
                    dirA2 = false;
                }
                if (dirE1 && dirE2 && !dirE1E2True)
                {
                    dirE1 = true;
                    dirE2 = false;
                }

                targetRow["C1"] = dirC1;
                targetRow["C2"] = dirC2;
                targetRow["I"] = dirI;
                targetRow["A1"] = dirA1;
                targetRow["A2"] = dirA2;
                targetRow["E1"] = dirE1;
                targetRow["E2"] = dirE2;

                result.Tables[1].Rows.Add(targetRow);
            }

            // Copia os filtros superiores
            for (rowNumber = 0; rowNumber < dsData2.Tables[1].Rows.Count; rowNumber++)
            {
                sourceRow = dsData2.Tables[1].Rows[rowNumber];
                targetRow = result.Tables[1].NewRow();

                targetRow["ControlBar"] = sourceRow["ControlBar"];
                targetRow["Combo"] = sourceRow["Combo"];
                targetRow["Recurso"] = sourceRow["Recurso"];
                targetRow["RecursoID"] = sourceRow["RecursoID"];
                targetRow["SubFormID"] = sourceRow["SubFormID"];
                targetRow["EhDefault"] = sourceRow["EhDefault"];
                targetRow["EstadoInicialID"] = sourceRow["EstadoInicialID"];

                result.Tables[1].Rows.Add(targetRow);
            }

            // Copia os filtros inferiores
            for (rowNumber = 0; rowNumber < dsData3.Tables[1].Rows.Count; rowNumber++)
            {
                sourceRow = dsData3.Tables[1].Rows[rowNumber];
                targetRow = result.Tables[1].NewRow();

                targetRow["ControlBar"] = sourceRow["ControlBar"];
                targetRow["Combo"] = sourceRow["Combo"];
                targetRow["Recurso"] = sourceRow["Recurso"];
                targetRow["RecursoID"] = sourceRow["RecursoID"];
                targetRow["SubFormID"] = sourceRow["SubFormID"];
                targetRow["EhDefault"] = sourceRow["EhDefault"];
                targetRow["EstadoInicialID"] = sourceRow["EstadoInicialID"];

                result.Tables[1].Rows.Add(targetRow);
            }

            // Copia as pastas
            for (rowNumber = 0; rowNumber < dsData4.Tables[1].Rows.Count; rowNumber++)
            {
                sourceRow = dsData4.Tables[1].Rows[rowNumber];
                targetRow = result.Tables[1].NewRow();

                recursoID = (int)sourceRow["RecursoID"];
                dirC1 = (bool)sourceRow["C1"];
                dirC2 = (bool)sourceRow["C2"];
                dirI = (bool)sourceRow["I"];
                dirA1 = (bool)sourceRow["A1"];
                dirA2 = (bool)sourceRow["A2"];
                dirE1 = (bool)sourceRow["E1"];
                dirE2 = (bool)sourceRow["E2"];

                targetRow["ControlBar"] = sourceRow["ControlBar"];
                targetRow["Combo"] = sourceRow["Combo"];
                targetRow["Recurso"] = sourceRow["Recurso"];
                targetRow["RecursoID"] = sourceRow["RecursoID"];
                targetRow["SubFormID"] = sourceRow["SubFormID"];
                targetRow["EhDefault"] = sourceRow["EhDefault"];
                targetRow["EstadoInicialID"] = sourceRow["EstadoInicialID"];

                dirC1C2True = (dirC1 && dirC2);
                dirA1A2True = (dirA1 && dirA2);
                dirE1E2True = (dirE1 && dirE2);

                // Pega a proxima linha;
                for (rowNumber++; rowNumber < dsData4.Tables[1].Rows.Count; rowNumber++)
                {
                    sourceRow = dsData4.Tables[1].Rows[rowNumber];

                    if (recursoID == (int)sourceRow["RecursoID"])
                    {
                        // dirX1 e dirX2 e o anterior sempre
                        dirC1C2True = ((bool)sourceRow["C1"] && (bool)sourceRow["C2"]) ? true : dirC1C2True;
                        dirA1A2True = ((bool)sourceRow["A1"] && (bool)sourceRow["A2"]) ? true : dirA1A2True;
                        dirE1E2True = ((bool)sourceRow["E1"] && (bool)sourceRow["E2"]) ? true : dirE1E2True;

                        dirC1 = dirC1 || ((bool)sourceRow["C1"]);
                        dirC2 = dirC2 || ((bool)sourceRow["C2"]);
                        dirI = dirI || ((bool)sourceRow["I"]);
                        dirA1 = dirA1 || ((bool)sourceRow["A1"]);
                        dirA2 = dirA2 || ((bool)sourceRow["C2"]);
                        dirE1 = dirE1 || ((bool)sourceRow["E1"]);
                        dirE2 = dirE2 || ((bool)sourceRow["E2"]);
                    }
                    else
                    {
                        break;
                    }
                }

                rowNumber--;

                if (dirC1 && dirC2 && !dirC1C2True)
                {
                    dirC1 = false;
                    dirC2 = true;
                }
                if (dirA1 && dirA2 && !dirA1A2True)
                {
                    dirA1 = false;
                    dirA2 = true;
                }
                if (dirE1 && dirE2 && !dirE1E2True)
                {
                    dirE1 = false;
                    dirE2 = true;
                }

                targetRow["C1"] = dirC1;
                targetRow["C2"] = dirC2;
                targetRow["I"] = dirI;
                targetRow["A1"] = dirA1;
                targetRow["A2"] = dirA2;
                targetRow["E1"] = dirE1;
                targetRow["E2"] = dirE2;

                result.Tables[1].Rows.Add(targetRow);
            }

            // False em todos os campos de bits dos botoes especificos
            for (rowNumber = 0; rowNumber < result.Tables[1].Rows.Count; rowNumber++)
            {
                targetRow = result.Tables[1].Rows[rowNumber];

                for (int i=1; i<=16; i++)
                {
                    targetRow["B" + i.ToString() + "A1"] = 0;
                    targetRow["B" + i.ToString() + "A2"] = 0;
                    targetRow["B" + i.ToString() + "C1"] = 0;
                    targetRow["B" + i.ToString() + "C2"] = 0;
                    targetRow["B" + i.ToString() + "I"] = 0;
                    targetRow["B" + i.ToString() + "E1"] = 0;
                    targetRow["B" + i.ToString() + "E2"] = 0;
                }
            }

            // Os Direitos e botoes especificos dos SubForms
            sql = "SELECT DISTINCT b.RecursoID AS SubFormID, b.Principal AS Principal, " +
                "c.SujeitoID AS BotaoID, g.Alterar1,g.Alterar2, g.Consultar1, g.Consultar2, g.Incluir, g.Excluir1, g.Excluir2 " +
                "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " +
                "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " +
                "WHERE a.ObjetoID= " + defContextID + " AND a.TipoRelacaoID=1 AND a.EstadoID=2 AND a.SujeitoID=b.RecursoID " +
                "AND (b.Principal=1 OR b.Principal=0) AND b.EstadoID=2 AND b.TipoRecursoID=4 AND b.RecursoID=c.ObjetoID " +
                "AND c.TipoRelacaoID=2 AND c.EstadoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.TipoRecursoID=5 " +
                "AND d.ClassificacaoID=13 AND (d.RecursoID BETWEEN 40001 AND 40016) " +
                "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " + defContextID + " )) " +
                "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " +
                "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " +
                "AND e.SujeitoID IN ((SELECT " + (m_UserID) + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + (m_UserID) + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND e.RelacaoID=f.RelacaoID " +
                "AND f.EmpresaID= " + (m_EmpresaID) + " AND g.RecursoID=d.RecursoID " +
                "AND g.RecursoMaeID=b.RecursoID " +
                "AND g.ContextoID= " + defContextID + " AND g.PerfilID=f.PerfilID " +
                "AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " +
                "ORDER BY SubFormID";

            // Executa procedure sp_RelatorioEstoque
            System.Data.DataSet dsData5 = oDataInterface.getRemoteData(sql);

            if (dsData5.Tables[1].Rows.Count > 0)
            {
                // Primeiro os direitos dos botoes especificos do control bar superior.
                // So interessa o contexto default
                rowsResult = result.Tables[1].Select(
                    "RecursoID = " + defContextID);

                rowsDsData5 = dsData5.Tables[1].Select(
                    "Principal = 1");

                if (rowsDsData5.Length > 0 && rowsResult.Length > 0)
                {
                    for (int i = 1; i <= 16; i++)
                    {
                        sBtnNumber = (40000 + i).ToString();
                        rowsDsData5 = dsData5.Tables[1].Select(
                            "Principal = 1 AND BotaoID=" + sBtnNumber);

                        for (rowNumber = 0; rowNumber < rowsDsData5.Length; rowNumber++)
                        {
                            sourceRow = rowsDsData5[rowNumber];

                            dirA1 = (bool)sourceRow["Alterar1"];
                            dirA2 = (bool)sourceRow["Alterar2"];
                            dirC1 = (bool)sourceRow["Consultar1"];
                            dirC2 = (bool)sourceRow["Consultar2"];
                            dirI = (bool)sourceRow["Incluir"];
                            dirE1 = (bool)sourceRow["Excluir1"];
                            dirE2 = (bool)sourceRow["Excluir2"];

                            dirA1A2True = (dirA1 && dirA2);
                            dirC1C2True = (dirC1 && dirC2);
                            dirE1E2True = (dirE1 && dirE2);

                            for (rowNumber++; rowNumber < rowsDsData5.Length; rowNumber++)
                            {
                                if ((bool)rowsDsData5[rowNumber]["Alterar1"] &&
                                    (bool)rowsDsData5[rowNumber]["Alterar2"])
                                {
                                    dirA1A2True = true;
                                }
                                dirA1 = dirA1 || (bool)rowsDsData5[rowNumber]["Alterar1"];
                                dirA2 = dirA2 || (bool)rowsDsData5[rowNumber]["Alterar2"];

                                if ((bool)rowsDsData5[rowNumber]["Consultar1"] &&
                                    (bool)rowsDsData5[rowNumber]["Consultar2"])
                                {
                                    dirC1C2True = true;
                                }
                                dirC1 = dirC1 || (bool)rowsDsData5[rowNumber]["Consultar1"];
                                dirC2 = dirC2 || (bool)rowsDsData5[rowNumber]["Consultar2"];

                                if ((bool)rowsDsData5[rowNumber]["Excluir1"] &&
                                    (bool)rowsDsData5[rowNumber]["Excluir2"])
                                {
                                    dirE1E2True = true;
                                }
                                dirE1 = dirE1 || (bool)rowsDsData5[rowNumber]["Excluir1"];
                                dirE2 = dirE2 || (bool)rowsDsData5[rowNumber]["Excluir2"];

                                dirI = dirI || (bool)rowsDsData5[rowNumber]["Incluir"]; ;
                            }

                            rowNumber--;

                            if (dirA1 && dirA2 && !dirA1A2True)
                            {
                                dirA1 = false;
                                dirA2 = true;
                            }

                            if (dirC1 && dirC2 && !dirC1C2True)
                            {
                                dirC1 = false;
                                dirC2 = true;
                            }

                            if (dirE1 && dirE2 && !dirE1E2True)
                            {
                                dirE1 = false;
                                dirE2 = true;
                            }

                            rowsResult[0]["B" + i + "A1"] = dirA1;
                            rowsResult[0]["B" + i + "A2"] = dirA2;
                            rowsResult[0]["B" + i + "C1"] = dirC1;
                            rowsResult[0]["B" + i + "C2"] = dirC2;
                            rowsResult[0]["B" + i + "I"] = dirI;
                            rowsResult[0]["B" + i + "E1"] = dirE1;
                            rowsResult[0]["B" + i + "E2"] = dirE2;
                        }
                    }
                }

                // Agora os direitos dos botoes especificos do control bar inferior.
                // Para todas as pastas
                for (rowNumber = 0; rowNumber < result.Tables[1].Rows.Count; rowNumber++)
                {
                    targetRow = result.Tables[1].Rows[rowNumber];

                    int folderID = 0;

                    if (((string)targetRow["ControlBar"] == "inf") && ((string)targetRow["Combo"] == "1"))
                    {
                        folderID = (int)targetRow["RecursoID"];
                    }

                    rowsDsData5 = dsData5.Tables[1].Select(
                        "SubFormID = " + folderID);

                    if (folderID != 0 && rowsDsData5.Length > 0)
                    {
                        for (int i = 1; i <= 16; i++)
                        {
                            sBtnNumber = (40000 + i).ToString();
                            rowsDsData5 = dsData5.Tables[1].Select(
                                "SubFormID=" + folderID + " AND Principal = 0 AND BotaoID=" + sBtnNumber);

                            for (int linha = 0; linha < rowsDsData5.Length; linha++)
                            {
                                dirA1 = (bool)rowsDsData5[linha]["Alterar1"];
                                dirA2 = (bool)rowsDsData5[linha]["Alterar2"];
                                dirC1 = (bool)rowsDsData5[linha]["Consultar1"];
                                dirC2 = (bool)rowsDsData5[linha]["Consultar2"];
                                dirI = (bool)rowsDsData5[linha]["Incluir"];
                                dirE1 = (bool)rowsDsData5[linha]["Excluir1"];
                                dirE2 = (bool)rowsDsData5[linha]["Excluir2"];

                                dirA1A2True = (dirA1 && dirA2);
                                dirC1C2True = (dirC1 && dirC2);
                                dirE1E2True = (dirE1 && dirE2);

                                for (linha++; linha < rowsDsData5.Length; linha++)
                                {
                                    if ((bool)rowsDsData5[linha]["Alterar1"] &&
                                        (bool)rowsDsData5[linha]["Alterar2"])
                                    {
                                        dirA1A2True = true;
                                    }
                                    dirA1 = dirA1 || (bool)rowsDsData5[linha]["Alterar1"];
                                    dirA2 = dirA2 || (bool)rowsDsData5[linha]["Alterar2"];

                                    if ((bool)rowsDsData5[linha]["Consultar1"] &&
                                        (bool)rowsDsData5[linha]["Consultar2"])
                                    {
                                        dirC1C2True = true;
                                    }
                                    dirC1 = dirC1 || (bool)rowsDsData5[linha]["Consultar1"];
                                    dirC2 = dirC2 || (bool)rowsDsData5[linha]["Consultar2"];

                                    if ((bool)rowsDsData5[linha]["Excluir1"] &&
                                        (bool)rowsDsData5[linha]["Excluir2"])
                                    {
                                        dirE1E2True = true;
                                    }
                                    dirE1 = dirE1 || (bool)rowsDsData5[linha]["Excluir1"];
                                    dirE2 = dirE2 || (bool)rowsDsData5[linha]["Excluir2"];

                                    dirI = dirI || (bool)rowsDsData5[linha]["Incluir"]; ;
                                }

                                if (dirA1 && dirA2 && !dirA1A2True)
                                {
                                    dirA1 = false;
                                    dirA2 = true;
                                }

                                if (dirC1 && dirC2 && !dirC1C2True)
                                {
                                    dirC1 = false;
                                    dirC2 = true;
                                }

                                if (dirE1 && dirE2 && !dirE1E2True)
                                {
                                    dirE1 = false;
                                    dirE2 = true;
                                }

                                targetRow["B" + i + "A1"] = dirA1;
                                targetRow["B" + i + "A2"] = dirA2;
                                targetRow["B" + i + "C1"] = dirC1;
                                targetRow["B" + i + "C2"] = dirC2;
                                targetRow["B" + i + "I"] = dirI;
                                targetRow["B" + i + "E1"] = dirE1;
                                targetRow["B" + i + "E2"] = dirE2;
                            }
                        }
                    }
                }
            }


            // Gera o resultado.
            // Converte o DSO para XML
            xmlDataSet = oDataInterface.datasetToXML(result);
            // Concatena o cabecalho e o conteudo do rsultado.
            m_StrXML = "<?xml version=\"1.0\" encoding=\"utf-8\" ?>" +
                xmlDataSet.InnerXml;
        }

        public DataSet makeResultDataSet()
        {
            // Gera o resultado.
            dataInterface di = new dataInterface(
                System.Configuration.ConfigurationManager.AppSettings["application"]
            );

            return di.getRemoteData(
                "SELECT " +
                    "CONVERT(VARCHAR(3), '') AS ControlBar, " +
                    "CONVERT(VARCHAR(1), '') AS Combo, " +
                    "CONVERT(VARCHAR(20), '') AS Recurso, " +
                    "CONVERT(INT, -1) AS RecursoID, " +
                    "CONVERT(INT, -1) AS SubFormID, " +
                    "CONVERT(BIT, 0) AS EhDefault, " +
                    "CONVERT(BIT, 0) AS C1, " +
                    "CONVERT(BIT, 0) AS C2, " +
                    "CONVERT(BIT, 0) AS I, " +
                    "CONVERT(BIT, 0) AS A1, " +
                    "CONVERT(BIT, 0) AS A2, " +
                    "CONVERT(BIT, 0) AS E1, " +
                    "CONVERT(BIT, 0) AS E2, " +
                    "CONVERT(INT, -1) AS EstadoInicialID, " +
                    "CONVERT(BIT, 0) AS B1A1, " +
                    "CONVERT(BIT, 0) AS B1A2, " +
                    "CONVERT(BIT, 0) AS B1C1, " +
                    "CONVERT(BIT, 0) AS B1C2, " +
                    "CONVERT(BIT, 0) AS B1I, " +
                    "CONVERT(BIT, 0) AS B1E1, " +
                    "CONVERT(BIT, 0) AS B1E2, " +
                    "CONVERT(BIT, 0) AS B2A1, " +
                    "CONVERT(BIT, 0) AS B2A2, " +
                    "CONVERT(BIT, 0) AS B2C1, " +
                    "CONVERT(BIT, 0) AS B2C2, " +
                    "CONVERT(BIT, 0) AS B2I, " +
                    "CONVERT(BIT, 0) AS B2E1, " +
                    "CONVERT(BIT, 0) AS B2E2, " +
                    "CONVERT(BIT, 0) AS B3A1, " +
                    "CONVERT(BIT, 0) AS B3A2, " +
                    "CONVERT(BIT, 0) AS B3C1, " +
                    "CONVERT(BIT, 0) AS B3C2, " +
                    "CONVERT(BIT, 0) AS B3I, " +
                    "CONVERT(BIT, 0) AS B3E1, " +
                    "CONVERT(BIT, 0) AS B3E2, " +
                    "CONVERT(BIT, 0) AS B4A1, " +
                    "CONVERT(BIT, 0) AS B4A2, " +
                    "CONVERT(BIT, 0) AS B4C1, " +
                    "CONVERT(BIT, 0) AS B4C2, " +
                    "CONVERT(BIT, 0) AS B4I, " +
                    "CONVERT(BIT, 0) AS B4E1, " +
                    "CONVERT(BIT, 0) AS B4E2, " +
                    "CONVERT(BIT, 0) AS B5A1, " +
                    "CONVERT(BIT, 0) AS B5A2, " +
                    "CONVERT(BIT, 0) AS B5C1, " +
                    "CONVERT(BIT, 0) AS B5C2, " +
                    "CONVERT(BIT, 0) AS B5I, " +
                    "CONVERT(BIT, 0) AS B5E1, " +
                    "CONVERT(BIT, 0) AS B5E2, " +
                    "CONVERT(BIT, 0) AS B6A1, " +
                    "CONVERT(BIT, 0) AS B6A2, " +
                    "CONVERT(BIT, 0) AS B6C1, " +
                    "CONVERT(BIT, 0) AS B6C2, " +
                    "CONVERT(BIT, 0) AS B6I, " +
                    "CONVERT(BIT, 0) AS B6E1, " +
                    "CONVERT(BIT, 0) AS B6E2, " +
                    "CONVERT(BIT, 0) AS B7A1, " +
                    "CONVERT(BIT, 0) AS B7A2, " +
                    "CONVERT(BIT, 0) AS B7C1, " +
                    "CONVERT(BIT, 0) AS B7C2, " +
                    "CONVERT(BIT, 0) AS B7I, " +
                    "CONVERT(BIT, 0) AS B7E1, " +
                    "CONVERT(BIT, 0) AS B7E2, " +
                    "CONVERT(BIT, 0) AS B8A1, " +
                    "CONVERT(BIT, 0) AS B8A2, " +
                    "CONVERT(BIT, 0) AS B8C1, " +
                    "CONVERT(BIT, 0) AS B8C2, " +
                    "CONVERT(BIT, 0) AS B8I, " +
                    "CONVERT(BIT, 0) AS B8E1, " +
                    "CONVERT(BIT, 0) AS B8E2, " +
                    "CONVERT(BIT, 0) AS B9A1, " +
                    "CONVERT(BIT, 0) AS B9A2, " +
                    "CONVERT(BIT, 0) AS B9C1, " +
                    "CONVERT(BIT, 0) AS B9C2, " +
                    "CONVERT(BIT, 0) AS B9I, " +
                    "CONVERT(BIT, 0) AS B9E1, " +
                    "CONVERT(BIT, 0) AS B9E2, " +
                    "CONVERT(BIT, 0) AS B10A1, " +
                    "CONVERT(BIT, 0) AS B10A2, " +
                    "CONVERT(BIT, 0) AS B10C1, " +
                    "CONVERT(BIT, 0) AS B10C2, " +
                    "CONVERT(BIT, 0) AS B10I, " +
                    "CONVERT(BIT, 0) AS B10E1, " +
                    "CONVERT(BIT, 0) AS B10E2, " +
                    "CONVERT(BIT, 0) AS B11A1, " +
                    "CONVERT(BIT, 0) AS B11A2, " +
                    "CONVERT(BIT, 0) AS B11C1, " +
                    "CONVERT(BIT, 0) AS B11C2, " +
                    "CONVERT(BIT, 0) AS B11I, " +
                    "CONVERT(BIT, 0) AS B11E1, " +
                    "CONVERT(BIT, 0) AS B11E2, " +
                    "CONVERT(BIT, 0) AS B12A1, " +
                    "CONVERT(BIT, 0) AS B12A2, " +
                    "CONVERT(BIT, 0) AS B12C1, " +
                    "CONVERT(BIT, 0) AS B12C2, " +
                    "CONVERT(BIT, 0) AS B12I, " +
                    "CONVERT(BIT, 0) AS B12E1, " +
                    "CONVERT(BIT, 0) AS B12E2, " +
                    "CONVERT(BIT, 0) AS B13A1, " +
                    "CONVERT(BIT, 0) AS B13A2, " +
                    "CONVERT(BIT, 0) AS B13C1, " +
                    "CONVERT(BIT, 0) AS B13C2, " +
                    "CONVERT(BIT, 0) AS B13I, " +
                    "CONVERT(BIT, 0) AS B13E1, " +
                    "CONVERT(BIT, 0) AS B13E2, " +
                    "CONVERT(BIT, 0) AS B14A1, " +
                    "CONVERT(BIT, 0) AS B14A2, " +
                    "CONVERT(BIT, 0) AS B14C1, " +
                    "CONVERT(BIT, 0) AS B14C2, " +
                    "CONVERT(BIT, 0) AS B14I, " +
                    "CONVERT(BIT, 0) AS B14E1, " +
                    "CONVERT(BIT, 0) AS B14E2, " +
                    "CONVERT(BIT, 0) AS B15A1, " +
                    "CONVERT(BIT, 0) AS B15A2, " +
                    "CONVERT(BIT, 0) AS B15C1, " +
                    "CONVERT(BIT, 0) AS B15C2, " +
                    "CONVERT(BIT, 0) AS B15I, " +
                    "CONVERT(BIT, 0) AS B15E1, " +
                    "CONVERT(BIT, 0) AS B15E2, " +
                    "CONVERT(BIT, 0) AS B16A1, " +
                    "CONVERT(BIT, 0) AS B16A2, " +
                    "CONVERT(BIT, 0) AS B16C1, " +
                    "CONVERT(BIT, 0) AS B16C2, " +
                    "CONVERT(BIT, 0) AS B16I, " +
                    "CONVERT(BIT, 0) AS B16E1, " +
                    "CONVERT(BIT, 0) AS B16E2 " +
                    "FROM Pessoas WITH ( NOLOCK ) WHERE PessoaID = -999"
            );
        }
    }
}
