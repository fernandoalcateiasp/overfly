using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class incluirconsultaserasa : System.Web.UI.OverflyPage
    {
        private Integer fonteDadoId;
		private Integer tipoDadoId;
		private Integer tipoRegistroId;
		private Integer formId;
		private Integer subFormId;
		private Integer registroId;
        private string dtConsulta;
		private Integer usuarioId;

		private int validacaoDadoId;
		
		public Integer nFonteDadoID
		{
			set { fonteDadoId = value; }
		}
		public Integer nTipoDadoID
		{
			set { tipoDadoId = value; }
		}
		public Integer nTipoRegistroID
		{
			set { tipoRegistroId = value; }
		}
		public Integer nFormID
		{
			set { formId = value; }
		}
        public Integer nSubFormID
		{
			set { subFormId = value; }
		}
		public Integer nRegistroID
		{
			set { registroId = value; }
		}
		public string nDtConsulta
		{
			set { dtConsulta = value; }
		}
		public Integer nUsuarioID
		{
			set { usuarioId = value; }
		}
		
		protected void ValidacoesDadosInsert()
		{
            // Cria os parametros da procedure sp_ValidacoesDados_Insert
            ProcedureParameters[] procParams = new ProcedureParameters[9];

            procParams[0] = new ProcedureParameters(
				"@FonteDadoID",
                System.Data.SqlDbType.Int,
				fonteDadoId != null ? (Object)fonteDadoId : DBNull.Value);

            procParams[1] = new ProcedureParameters(
				"@TipoDadoID",
                System.Data.SqlDbType.Int,
				tipoDadoId != null ? (Object)tipoDadoId : DBNull.Value);

            procParams[2] = new ProcedureParameters(
				"@TipoRegistroID",
                System.Data.SqlDbType.Int,
				tipoRegistroId != null ? (Object)tipoRegistroId : DBNull.Value);

            procParams[3] = new ProcedureParameters(
                "@FormID",
                System.Data.SqlDbType.Int,
				formId != null ? (Object)formId : DBNull.Value);

            procParams[4] = new ProcedureParameters(
				"@SubFormID",
                System.Data.SqlDbType.Int,
				subFormId != null ? (Object)subFormId : DBNull.Value);

            procParams[5] = new ProcedureParameters(
				"@RegistroID",
                System.Data.SqlDbType.Int,
				registroId != null ? (Object)registroId : DBNull.Value);

            procParams[6] = new ProcedureParameters(
				"@DtConsulta",
                System.Data.SqlDbType.DateTime,
				dtConsulta != null ? (Object)dtConsulta : DBNull.Value);

            procParams[7] = new ProcedureParameters(
				"@UsuarioID",
                System.Data.SqlDbType.Int,
				usuarioId != null ? (Object)usuarioId : DBNull.Value);

            procParams[8] = new ProcedureParameters(
				"@ValidacaoDadoID",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

			DataInterfaceObj.execNonQueryProcedure(
				"sp_ValidacoesDados_Insert", 
				procParams);

			if (procParams[8].Data != DBNull.Value && procParams[8].Data.ToString().Length > 0)
			{
				try {
					validacaoDadoId = int.Parse(procParams[8].Data.ToString());
				} catch (System.Exception) {
					validacaoDadoId = -1;
				}
			}
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			ValidacoesDadosInsert();
			
            // Gera o resultado.
            WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + validacaoDadoId + " as ValidacaoDadoID"
				)
			);
        }
    }
}
