﻿using System;
using System.Data.OleDb;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;
//using System.Linq;
//using System.Xml.Linq;

namespace Overfly3.serversidegenEx
{
    public partial class arquivoupload : System.Web.UI.OverflyPage
    {
        private int iAceitaMaisArquivo;
        private int fileID;
        private int cont = 0;
        private int nTipoArquivoID;

        protected override void PageLoad(object sender, EventArgs e)
        {
            sendStringToJS(true);
        }

        protected void sendStringToJS(bool sAddTags)
        {
            string nVerExisteArquivo = Request.QueryString["nVerExisteArquivo"].ToString();

            nVerExisteArquivo = "var glb_sVerExisteArquivo = '" + nVerExisteArquivo.ToString() + "' ";

            if (sAddTags == false)
                nVerExisteArquivo = "<script type=\"text/javascript\"> " + nVerExisteArquivo.ToString() + " </script>";

            Page.ClientScript.RegisterStartupScript(Page.GetType(), "glb_sVerExisteArquivo", nVerExisteArquivo, sAddTags);
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            bool baddtag = true;

            nTipoArquivoID = int.Parse(Request.QueryString["nTipoArquivoID"].ToString());

            try
            {
                //Limita tamanho do arquivo.
                //Atualmente em 200MB (utilizar medida em byte).
                if (FileUpload1.PostedFile.ContentLength < (1048576 * 200))
                {
                    //Verifica se um arquivo está selecionado ou vazio.
                    if (FileUpload1.HasFile && FileUpload1.PostedFile != null)
                    {
                        //Se for Imagem, valida se arquivo selecionado é do tipo Imagem
                        if (((nTipoArquivoID == 1451) && (FileUpload1.PostedFile.ContentType.ToString().Contains("image"))) || (nTipoArquivoID != 1451))
                        {
                            if (nTipoArquivoID != 0)
                            {
                                //Guarda tamanho do arquivo.
                                int ilength = Convert.ToInt32(FileUpload1.PostedFile.InputStream.Length);
                                //Guarda tipo do arquivo.
                                string content = FileUpload1.PostedFile.ContentType;

                                //Guarda nome do arquivo sem caminho.
                                string[]
                                    ar_file_name = FileUpload1.PostedFile.FileName.ToString().Trim().Split('\\');
                                string file_name = ar_file_name[ar_file_name.Length - 1].ToString().Trim();

                                //Gera vetor com tamanho do arquivo
                                byte[] bytecontent = new System.Byte[ilength];
                                FileUpload1.PostedFile.InputStream.Read(bytecontent, 0, ilength);
                                //Guarda ID do form que a modal pertence.
                                string nFormID = Request.QueryString["nFormID"].ToString();
                                //Guarda ID do subform que a modal pertence.
                                string nSubFormID = Request.QueryString["nSubFormID"].ToString();
                                //Guarda ID do Registro que a modal pertence.
                                string nRegistroID = Request.QueryString["nRegistroID"].ToString();
                                //Guarda ID do Usuário logado.
                                string nUserID = Request.QueryString["nUserID"].ToString();
                                //Guarda Tamanho da imagem
                                string nTamanhoImagem = Request.QueryString["nTamanhoImagem"].ToString();
                                //Guarda Ordem da imagem
                                string nOrdemImagem = Request.QueryString["nOrdemImagem"].ToString();

                                bool bTipoImagem = (nTipoArquivoID == 1451);

                                // Travas para imagens
                                if (bTipoImagem)
                                {
                                    System.IO.Stream stream = FileUpload1.PostedFile.InputStream;
                                    System.Drawing.Image image = System.Drawing.Image.FromStream(stream);

                                    bool sizeImgOK = true;
                                    bool sizeDimOK = true;

                                    int nMaxFileSizeKB = int.Parse(Request.QueryString["nMaxFileSizeKB"].ToString());
                                    int nMaxImgWidth = int.Parse(Request.QueryString["nMaxImgWidth"].ToString());
                                    int nMaxImgHeight = int.Parse(Request.QueryString["nMaxImgHeight"].ToString());

                                    int imgPostedFileSizeKB = FileUpload1.PostedFile.ContentLength / 1024;
                                    int imgPostedWidth = image.Width;
                                    int imgPostedHeight = image.Height;

                                    // Valida se tamanho da imagem postada excede o tamanho máximo permitido (ContentType em bytes / 1024 = kilobytes)
                                    if (imgPostedFileSizeKB > nMaxFileSizeKB)
                                        sizeImgOK = false;

                                    //Valida se excede as dimensões máximas
                                    if ((imgPostedWidth > nMaxImgWidth) || (imgPostedHeight > nMaxImgHeight))
                                        sizeDimOK = false;

                                    if ((!sizeImgOK) || (!sizeDimOK))
                                    {
                                        string errorMessage = "";

                                        if (!sizeImgOK)
                                            errorMessage = "Tamanho da imagem excede " + nMaxFileSizeKB.ToString() + " kbytes.\\n";

                                        if (!sizeDimOK)
                                            errorMessage += "Imagem excedeu as dimensões máximas permitidas: " +
                                                               nMaxImgWidth.ToString() + " x " +
                                                               nMaxImgHeight.ToString() + " pixels.";

                                        // Envia mensagem de erro pro client e sai do método
                                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('" + errorMessage + "')", true);
                                        baddtag = false;
                                        sendStringToJS(baddtag);
                                        return;
                                    }
                                }

                                if (DataInterfaceObj.connectDatabase())
                                {
                                    //Atribui conexao do overfly.
                                    SqlConnection con;
                                    con = DataInterfaceObj.getConnection();
                                    string sSQL;

                                    sSQL = "SELECT dbo.fn_Documentos_ArquivoVerifica(" + nFormID + ", " +
                                                nSubFormID + ", " + nRegistroID + ", " + nTipoArquivoID.ToString() + ") AS AceitaMaisArquivo ";

                                    SqlCommand cmd = new SqlCommand(sSQL, con);
                                    cmd.CommandTimeout = DataInterfaceObj.timeout;
                                    SqlDataReader dr = cmd.ExecuteReader();
                                    dr.Read();
                                    iAceitaMaisArquivo = dr.GetInt32(0);
                                    // Fecha Objeto
                                    dr.Close();
                                    //Verifica se ja existi um arquivo do mesmo nome e do mesmo Tipo.
                                    //Atribui conexao do overfly.
                                    SqlConnection conn;
                                    conn = DataInterfaceObj.getConnection();
                                    sSQL = "SELECT TOP 1 DocumentoID " +
                                                "FROM    [OVERFLYBASE_EXT2].Overfly_Documentos.dbo.Documentos WITH (NOLOCK) " +
                                                "WHERE FormID =" + nFormID +
                                                " AND SubformID = " + nSubFormID +
                                                " AND RegistroID = " + nRegistroID +
                                                ((bTipoImagem) ? "" : " AND nome ='" + file_name.ToString() + "' ") +
                                                " AND TipoArquivoID = " + nTipoArquivoID.ToString();

                                    // Para casos de imagem
                                    if (bTipoImagem)
                                    {
                                        sSQL += " AND TamanhoImagem =" + nTamanhoImagem +
                                                " AND OrdemImagem = " + nOrdemImagem;
                                    }

                                    SqlCommand cmds = new SqlCommand(sSQL, conn);
                                    cmds.CommandTimeout = DataInterfaceObj.timeout;
                                    SqlDataReader ver = cmds.ExecuteReader();
                                    ver.Read();

                                    try
                                    {
                                        fileID = ver.GetInt32(0);
                                    }
                                    catch (System.Exception)
                                    {
                                        fileID = 0;
                                    }
                                }
                                // Verifica o tamanho do nome do documento
                                cont = file_name.Length;
                                if (cont <= 50)
                                {
                                    if ((iAceitaMaisArquivo == 0) && (fileID > 0))
                                    {
                                        //Se for sobrescrito ALTERA
                                        //Verifica conexao do overfly.
                                        if (DataInterfaceObj.connectDatabase())
                                        {
                                            //Atribui conexao do overfly.
                                            SqlConnection con;
                                            con = DataInterfaceObj.getConnection();

                                            //Atribui query.
                                            string sql = "DELETE FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] WHERE DocumentoID = @fileID";

                                            //Gera SqlCommand com parametos da query e conexao.
                                            SqlCommand cmd = new SqlCommand(sql, con);
                                            cmd.CommandTimeout = DataInterfaceObj.timeout;

                                            //Atribui parametros com os dados do arquivo
                                            cmd.Parameters.AddWithValue("@fileID", fileID);
                                            
                                            //Executa o SqlCommando para a gravacao do arquivo no banco de dados.
                                            cmd.ExecuteNonQuery();
                                        }
                                    }
                                    
                                    {
                                        //Se não existe arquivo INSERE
                                        //Verifica conexao do overfly.
                                        if (DataInterfaceObj.connectDatabase())
                                        {
                                            //Atribui conexao do overfly.
                                            SqlConnection con;
                                            con = DataInterfaceObj.getConnection();

                                            //Atribui query.
                                            string sql = "INSERT INTO [OVERFLYBASE_EXT2].Overfly_Documentos.dbo.Documentos" +
                                                                "(Arquivo,Nome,ContentType, Tamanho, dtDocumento, FormID, TipoArquivoID, SubformID, RegistroID, UsuarioID" + (bTipoImagem ? ", TamanhoImagem, OrdemImagem) " : ") ") +
                                                            "VALUES(@file, @name, @content, @size, @data, @form, @tipoarquivoid, @subform, @registro, @user" + (bTipoImagem ? ", @tamanhoImagem, @ordemImagem) " : ") ");

                                            //Gera SqlCommand com parametos da query e conexao.
                                            SqlCommand cmd = new SqlCommand(sql, con);
                                            cmd.CommandTimeout = DataInterfaceObj.timeout;

                                            //Faz a leitura do arquivo para o vetor bytecontent.
                                            FileUpload1.PostedFile.InputStream.Read(bytecontent, 0, ilength);

                                            //Atribui parametros com os dados do arquivo
                                            cmd.Parameters.AddWithValue("@file", bytecontent);
                                            cmd.Parameters.AddWithValue("@name", file_name);
                                            cmd.Parameters.AddWithValue("@content", content);
                                            cmd.Parameters.AddWithValue("@size", ilength / 1024);
                                            cmd.Parameters.AddWithValue("@data", DateTime.Now);
                                            cmd.Parameters.AddWithValue("@form", nFormID);
                                            cmd.Parameters.AddWithValue("@tipoarquivoid", nTipoArquivoID);
                                            cmd.Parameters.AddWithValue("@subform", nSubFormID);
                                            cmd.Parameters.AddWithValue("@registro", nRegistroID);
                                            cmd.Parameters.AddWithValue("@user", nUserID);

                                            if (bTipoImagem)
                                            {
                                                cmd.Parameters.AddWithValue("@tamanhoImagem", nTamanhoImagem);
                                                cmd.Parameters.AddWithValue("@ordemImagem", nOrdemImagem);
                                            }

                                            //Executa o SqlCommando para a gravacao do arquivo no banco de dados.
                                            cmd.ExecuteNonQuery();

                                            //Exibe mensagem de arquivo gravado com sucesso.

                                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "window.parent.btnCancelar_onclick();", true);

                                            baddtag = false;
                                        }
                                        else
                                        {
                                            //Exibe mensagem de erro de conexão do overfly.
                                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Verifique a conexão!')", true);
                                            baddtag = false;
                                        }
                                    }
                                }
                                else
                                {
                                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Nome do documento é maior do que o permitido, diminua e tente novamente!')", true);
                                    baddtag = false;
                                }
                            }
                            else
                            {
                                Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Selecione um arquivo')", true);
                                baddtag = false;
                            }
                        }
                        else
                        {
                            //Exibe mensagem de erro para arquivo não selecionado ou vazio.
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Arquivo selecionado não é uma imagem.')", true);
                            baddtag = false;
                        }
                    }
                    else
                    {
                        //Exibe mensagem de erro para arquivo não selecionado ou vazio.
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Não foi possivel realizar o upload! \\n Verifique se o caminho é válido e se o arquivo não está vazio.')", true);
                        baddtag = false;
                    }
                }
                else
                {
                    //Exibe mensagem de erro para arquivo que excede o tamanho em bytes.
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Não foi possivel realizar o upload! \\n O tamanho do arquivo excede 2MB.')", true);
                    baddtag = false;
                }
            }
            catch (System.Exception ex)
            {   //Exibe mensagem de erros não previstos.
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Erro: \\n" + ex.Message + "')", true);
                baddtag = false;
            }
            sendStringToJS(baddtag);
        }
    }
}
