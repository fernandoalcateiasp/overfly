using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class fillpropalt : System.Web.UI.OverflyPage
    {
        private string caller;
        private string toFind;
        private Integer numeroRegistros;

        public string sCaller
        {
            set { caller = value; }
		}
        public string strToFind
        {
            set { toFind = value; }
		}
		public Integer nNumeroRegistros
        {
            set { numeroRegistros = value; }
		}

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "";

            if(caller == "PROP") {
	            sql = "SELECT TOP " + numeroRegistros + " b.Nome, b.PessoaID, b.Fantasia FROM RelacoesPesRec a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " +
	                     "WHERE (a.EstadoID=2 AND a.TipoRelacaoID=11 AND a.ObjetoID=999 AND b.Nome >='" + toFind + "' " +
	                     "AND a.SujeitoID=b.PessoaID AND b.TipoPessoaID=51 AND b.EstadoID=2) " +
	                     "ORDER BY b.Nome";
            }
            else
            {
	            sql = "SELECT TOP " + numeroRegistros + " Nome, PessoaID, Fantasia FROM Pessoas WITH(NOLOCK) " +
	                     "WHERE (EstadoID=2 AND TipoPessoaID IN (51,53) AND Nome >='" + toFind + "') " +
	                     "ORDER BY Nome";
            }

            // Gera o resultado.
			WriteResultXML(DataInterfaceObj.getRemoteData(sql));
        }
    }
}
