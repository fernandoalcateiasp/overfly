using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class gravarSocio : System.Web.UI.OverflyPage
    {
        private string EmpresaID = null;//Constants.INT_ZERO;
        protected string nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }

        private string PaisID = null;// Constants.INT_ZERO;
        protected string nPaisID
        {
            get { return PaisID; }
            set { PaisID = value; }
        }

        private string TipoPesspaID = null;// Constants.INT_ZERO;
        protected string nTipoPessoaID
        {
            get { return TipoPesspaID; }
            set { TipoPesspaID = value; }
        }

        private string Documento = null;//"";
        protected string sDocumento
        {
            get { return Documento; }
            set { Documento = value; }
        }

        private string Nome = null;//"";
        protected string sNome
        {
            get { return Nome; }
            set { Nome = value; }
        }

        private string Participacao = null;// Constants.INT_ZERO;
        protected string nParticipacao
        {
            get { return Participacao; }
            set { Participacao = value; }
        }

        private string dtAdmissao;//
        protected string sdtAdmissao
        {
            get { return dtAdmissao; }
            set { dtAdmissao = value; }
        }

        private string ContratoSocial;//
        protected string bContratoSocial
        {
            get { return ContratoSocial; }
            set { ContratoSocial = value; }
        }

        private string Observacao = null;//"";
        protected string sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }

        private string UsuarioID = null;
        protected string nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        private string resultado = null;//"";
        protected string Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }

        protected void Gravar()
        {
            ProcedureParameters[] param = new ProcedureParameters[] {
				new ProcedureParameters("@EmpresaID", SqlDbType.Int, nEmpresaID),
				new ProcedureParameters("@PaisID", SqlDbType.Int, nPaisID),
                new ProcedureParameters("@TipoPessoaID", SqlDbType.Int, nTipoPessoaID),
                new ProcedureParameters("@Documento", SqlDbType.VarChar, sDocumento, 20),
                new ProcedureParameters("@Nome", SqlDbType.VarChar, sNome, 20),
                new ProcedureParameters("@Participacao", SqlDbType.Int, nParticipacao.Length > 0 ? (Object)int.Parse(nParticipacao) : DBNull.Value),
                new ProcedureParameters("@dtAdmissao", SqlDbType.DateTime, sdtAdmissao),
                new ProcedureParameters("@ContratoSocial", SqlDbType.Bit, ContratoSocial == "1" ? true : false),
                new ProcedureParameters("@Observacao", SqlDbType.VarChar, sObservacao, 30),
                new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUsuarioID),
                new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, ParameterDirection.Output, 8000)
			};

            DataInterfaceObj.execNonQueryProcedure("sp_Socio_Gerador", param);

            Resultado = param[10].Data.ToString();
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            Gravar();

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT " + (Resultado != "" ? "'" + Resultado + "' " : "NULL") + " AS Resultado "));
        }
    }
}
