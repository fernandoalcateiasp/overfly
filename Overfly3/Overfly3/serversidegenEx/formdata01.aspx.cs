using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class formdata01 : System.Web.UI.OverflyPage
    {
        private string name;

		public string formName
		{
			set { name = value; }
		}

        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT RecursoID, RecursoFantasia AS Recurso, FormName, FrameStart, ArqStart " +
					"FROM Recursos WITH(NOLOCK) " +
					"WHERE FormName='" + name + "' AND TipoRecursoID = 2 AND ClassificacaoID = 11"
				)
			);
        }
    }
}
