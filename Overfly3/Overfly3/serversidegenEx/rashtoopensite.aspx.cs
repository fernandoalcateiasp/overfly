using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class rashtoopensite : System.Web.UI.OverflyPage
    {
        private string userId;
        private string sPagina;
        private string nParceiroID;
        private string nPessoaID;
        private string nContatoID;
        private string nEmpresaID;

        public string nUserID
        {
            get { return userId; }
            set { userId = (value != null ? value : "NULL"); }
        }
        public string Pagina
        {
            get { return sPagina; }
            set { sPagina = value; }
        }
        public string ParceiroID
        {
            get { return nParceiroID; }
            set { nParceiroID = (value != null ? value : "NULL"); }
        }
        public string ContatoID
        {
            get { return nContatoID; }
            set { nContatoID = (value != null ? value : "NULL"); }
        }
        public string EmpresaID
        {
            get { return nEmpresaID; }
            set { nEmpresaID = (value != null ? value : "NULL"); }
        }
        public string PessoaID
        {
            get { return nPessoaID; }
            set { nPessoaID = (value != "null" ? value : nParceiroID); }
        }

        protected string PassPessoa()
        {
            string passw = "";


            System.Data.DataSet tbSenha = new System.Data.DataSet();


            tbSenha = DataInterfaceObj.getRemoteData(
           " SELECT TOP 1 d.Senha AS Senha,b.ContatoID AS ContatoID " +
           "   FROM    dbo.RelacoesPessoas a WITH ( NOLOCK )" +
           "   INNER JOIN dbo.RelacoesPessoas_Contatos b WITH ( NOLOCK ) ON a.RelacaoID = b.RelacaoID " +
           "   INNER JOIN dbo.Pessoas c WITH ( NOLOCK ) ON b.ContatoID = c.PessoaID " +
           "   INNER JOIN dbo.Pessoas_Senhas d WITH ( NOLOCK ) ON ( c.PessoaID = d.PessoaID ) AND ( b.PesURLID = d.PesURLID )" +
           "   WHERE   a.SujeitoID = " + nParceiroID + " AND a.ObjetoID =" + nEmpresaID + (nContatoID != null ? (" AND b.ContatoID = " + nContatoID) : " ") + " AND d.Tipo = 2 " +
           "   AND b.web = 1 AND c.EstadoID = 2 AND a.EstadoID = 2  AND c.TipoPessoaID = 51" +
           "  ORDER BY d.dtSenha DESC, d.PesSenhaID ");


            if (tbSenha.Tables[1].Rows.Count == 0)
            {
                return null;
            }
            if (tbSenha.Tables[1].Rows[0]["Senha"] != null)
            {
                passw = tbSenha.Tables[1].Rows[0]["Senha"].ToString();
                nContatoID = tbSenha.Tables[1].Rows[0]["ContatoID"].ToString();
            }

            return passw;

        }

        protected string sp_AutoLogin_GeraHash(string passw)
        {
            string result = "";

            //Gera Rashcode
            ProcedureParameters[] procParams_AutoLogin = new ProcedureParameters[8];

            procParams_AutoLogin[0] = new ProcedureParameters(
                "@Usuario",
                System.Data.SqlDbType.Int,
                int.Parse(nContatoID));

            procParams_AutoLogin[1] = new ProcedureParameters(
                "@Senha",
                System.Data.SqlDbType.VarChar,
                (passw),
                50);

            procParams_AutoLogin[2] = new ProcedureParameters(
                "@ParceiroID",
                System.Data.SqlDbType.Int,
                 int.Parse(nParceiroID));

            procParams_AutoLogin[3] = new ProcedureParameters(
                "@ClienteID",
                System.Data.SqlDbType.Int,
                 int.Parse(nPessoaID));

            procParams_AutoLogin[4] = new ProcedureParameters(
                "@Pagina",
                System.Data.SqlDbType.VarChar,
                (sPagina),
                200);

            procParams_AutoLogin[5] = new ProcedureParameters(
                "@Validade",
                System.Data.SqlDbType.DateTime,
                null);

            procParams_AutoLogin[6] = new ProcedureParameters(
                "@PKID",
                System.Data.SqlDbType.VarChar,
                null,
                ParameterDirection.Output,
                4000);

            procParams_AutoLogin[7] = new ProcedureParameters(
                "@HashCode",
                System.Data.SqlDbType.VarChar,
                "",
                ParameterDirection.Output,
                4000);

            DataInterfaceObj.execNonQueryProcedure("ECOMMERCECL.Alcateia_Ecommerce.dbo.sp_AutoLogin_GeraHash", procParams_AutoLogin);

            result = (procParams_AutoLogin[7].Data != DBNull.Value) ? (string)procParams_AutoLogin[7].Data : "";

            return result;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string passw = PassPessoa();
            string sHashCode = "";

            if (passw == null)
            {
                sHashCode = passw;
            }
            else
            {
                sHashCode = sp_AutoLogin_GeraHash(passw);
            }

            // Gera o resultado para o usuario.
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select '" + sHashCode + "' as sHashCode"
                )
            );
        }
    }
}
