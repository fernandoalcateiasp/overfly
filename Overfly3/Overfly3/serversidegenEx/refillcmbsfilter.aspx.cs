using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Reflection;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class refillcmbsfilter : System.Web.UI.OverflyPage
    {
        private string userId;
        private string empresaId;
        private string formId;
        private string contextId;

        private int recursoID;
        private bool dirC1;
        private bool dirC2;
        private bool dirI;
        private bool dirA1;
        private bool dirA2;
        private bool dirE1;
        private bool dirE2;

        private bool dirC1C2True;
        private bool dirA1A2True;
        private bool dirE1E2True;

        private object[] lineArray;

        public string userID
        {
            get { return userId; }
            set { userId = value; }
        }
        public string empresaID
        {
            get { return empresaId; }
            set { empresaId = value; }
        }
        public string formID
        {
            get { return formId; }
            set { formId = value; }
        }
        public string contextID
        {
            get { return contextId; }
            set { contextId = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            dsNew = DataInterfaceObj.getRemoteData(
                "select TOP 0 " +
                    "'' as ControlBar, '' as Combo, '' as Recurso, 0 as RecursoID, " +
                    "0 as SubFormID, convert(bit, 0) as EhDefault, 0 as EstadoInicialID, " +
                    "convert(bit, 0) as C1, convert(bit, 0) as C2, convert(bit, 0) as I, " +
                    "convert(bit, 0) as A1, convert(bit, 0) as A2, convert(bit, 0) as E1, " +
                    "convert(bit, 0) as E2, " +
                    "CONVERT(BIT, 0) AS B1A1, " +
                    "CONVERT(BIT, 0) AS B1A2, " +
                    "CONVERT(BIT, 0) AS B1C1, " +
                    "CONVERT(BIT, 0) AS B1C2, " +
                    "CONVERT(BIT, 0) AS B1I, " +
                    "CONVERT(BIT, 0) AS B1E1, " +
                    "CONVERT(BIT, 0) AS B1E2, " +
                    "CONVERT(BIT, 0) AS B2A1, " +
                    "CONVERT(BIT, 0) AS B2A2, " +
                    "CONVERT(BIT, 0) AS B2C1, " +
                    "CONVERT(BIT, 0) AS B2C2, " +
                    "CONVERT(BIT, 0) AS B2I, " +
                    "CONVERT(BIT, 0) AS B2E1, " +
                    "CONVERT(BIT, 0) AS B2E2, " +
                    "CONVERT(BIT, 0) AS B3A1, " +
                    "CONVERT(BIT, 0) AS B3A2, " +
                    "CONVERT(BIT, 0) AS B3C1, " +
                    "CONVERT(BIT, 0) AS B3C2, " +
                    "CONVERT(BIT, 0) AS B3I, " +
                    "CONVERT(BIT, 0) AS B3E1, " +
                    "CONVERT(BIT, 0) AS B3E2, " +
                    "CONVERT(BIT, 0) AS B4A1, " +
                    "CONVERT(BIT, 0) AS B4A2, " +
                    "CONVERT(BIT, 0) AS B4C1, " +
                    "CONVERT(BIT, 0) AS B4C2, " +
                    "CONVERT(BIT, 0) AS B4I, " +
                    "CONVERT(BIT, 0) AS B4E1, " +
                    "CONVERT(BIT, 0) AS B4E2, " +
                    "CONVERT(BIT, 0) AS B5A1, " +
                    "CONVERT(BIT, 0) AS B5A2, " +
                    "CONVERT(BIT, 0) AS B5C1, " +
                    "CONVERT(BIT, 0) AS B5C2, " +
                    "CONVERT(BIT, 0) AS B5I, " +
                    "CONVERT(BIT, 0) AS B5E1, " +
                    "CONVERT(BIT, 0) AS B5E2, " +
                    "CONVERT(BIT, 0) AS B6A1, " +
                    "CONVERT(BIT, 0) AS B6A2, " +
                    "CONVERT(BIT, 0) AS B6C1, " +
                    "CONVERT(BIT, 0) AS B6C2, " +
                    "CONVERT(BIT, 0) AS B6I, " +
                    "CONVERT(BIT, 0) AS B6E1, " +
                    "CONVERT(BIT, 0) AS B6E2, " +
                    "CONVERT(BIT, 0) AS B7A1, " +
                    "CONVERT(BIT, 0) AS B7A2, " +
                    "CONVERT(BIT, 0) AS B7C1, " +
                    "CONVERT(BIT, 0) AS B7C2, " +
                    "CONVERT(BIT, 0) AS B7I, " +
                    "CONVERT(BIT, 0) AS B7E1, " +
                    "CONVERT(BIT, 0) AS B7E2, " +
                    "CONVERT(BIT, 0) AS B8A1, " +
                    "CONVERT(BIT, 0) AS B8A2, " +
                    "CONVERT(BIT, 0) AS B8C1, " +
                    "CONVERT(BIT, 0) AS B8C2, " +
                    "CONVERT(BIT, 0) AS B8I, " +
                    "CONVERT(BIT, 0) AS B8E1, " +
                    "CONVERT(BIT, 0) AS B8E2, " +
                    "CONVERT(BIT, 0) AS B9A1, " +
                    "CONVERT(BIT, 0) AS B9A2, " +
                    "CONVERT(BIT, 0) AS B9C1, " +
                    "CONVERT(BIT, 0) AS B9C2, " +
                    "CONVERT(BIT, 0) AS B9I, " +
                    "CONVERT(BIT, 0) AS B9E1, " +
                    "CONVERT(BIT, 0) AS B9E2, " +
                    "CONVERT(BIT, 0) AS B10A1, " +
                    "CONVERT(BIT, 0) AS B10A2, " +
                    "CONVERT(BIT, 0) AS B10C1, " +
                    "CONVERT(BIT, 0) AS B10C2, " +
                    "CONVERT(BIT, 0) AS B10I, " +
                    "CONVERT(BIT, 0) AS B10E1, " +
                    "CONVERT(BIT, 0) AS B10E2, " +
                    "CONVERT(BIT, 0) AS B11A1, " +
                    "CONVERT(BIT, 0) AS B11A2, " +
                    "CONVERT(BIT, 0) AS B11C1, " +
                    "CONVERT(BIT, 0) AS B11C2, " +
                    "CONVERT(BIT, 0) AS B11I, " +
                    "CONVERT(BIT, 0) AS B11E1, " +
                    "CONVERT(BIT, 0) AS B11E2, " +
                    "CONVERT(BIT, 0) AS B12A1, " +
                    "CONVERT(BIT, 0) AS B12A2, " +
                    "CONVERT(BIT, 0) AS B12C1, " +
                    "CONVERT(BIT, 0) AS B12C2, " +
                    "CONVERT(BIT, 0) AS B12I, " +
                    "CONVERT(BIT, 0) AS B12E1, " +
                    "CONVERT(BIT, 0) AS B12E2, " +
                    "CONVERT(BIT, 0) AS B13A1, " +
                    "CONVERT(BIT, 0) AS B13A2, " +
                    "CONVERT(BIT, 0) AS B13C1, " +
                    "CONVERT(BIT, 0) AS B13C2, " +
                    "CONVERT(BIT, 0) AS B13I, " +
                    "CONVERT(BIT, 0) AS B13E1, " +
                    "CONVERT(BIT, 0) AS B13E2, " +
                    "CONVERT(BIT, 0) AS B14A1, " +
                    "CONVERT(BIT, 0) AS B14A2, " +
                    "CONVERT(BIT, 0) AS B14C1, " +
                    "CONVERT(BIT, 0) AS B14C2, " +
                    "CONVERT(BIT, 0) AS B14I, " +
                    "CONVERT(BIT, 0) AS B14E1, " +
                    "CONVERT(BIT, 0) AS B14E2, " +
                    "CONVERT(BIT, 0) AS B15A1, " +
                    "CONVERT(BIT, 0) AS B15A2, " +
                    "CONVERT(BIT, 0) AS B15C1, " +
                    "CONVERT(BIT, 0) AS B15C2, " +
                    "CONVERT(BIT, 0) AS B15I, " +
                    "CONVERT(BIT, 0) AS B15E1, " +
                    "CONVERT(BIT, 0) AS B15E2, " +
                    "CONVERT(BIT, 0) AS B16A1, " +
                    "CONVERT(BIT, 0) AS B16A2, " +
                    "CONVERT(BIT, 0) AS B16C1, " +
                    "CONVERT(BIT, 0) AS B16C2, " +
                    "CONVERT(BIT, 0) AS B16I, " +
                    "CONVERT(BIT, 0) AS B16E1, " +
                    "CONVERT(BIT, 0) AS B16E2 "
            );

            rsData();
            rsData2();
            rsData3();
            rsData4();
            rsData5();

            WriteResultXML(dsNew);
        }

        private DataSet dsNew;

        private void rsData()
        {
            DataSet rsd = DataInterfaceObj.getRemoteData(
                "SELECT DISTINCT 'sup' AS ControlBar, '1' AS Combo,a.RecursoFantasia AS Recurso,a.RecursoID,-1 AS SubFormID, " +
                "a.Ordem AS Ordem, a.EhDefault AS EhDefault, g.Consultar1 AS C1,g.Consultar2 AS C2 ,g.Incluir AS I, " +
                "g.Alterar1 AS A1,g.Alterar2 AS A2,g.Excluir1 AS E1,g.Excluir2 AS E2, ISNULL(h.SujeitoID,0) AS EstadoInicialID " +
                "FROM Recursos a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), RelacoesPesRec_Perfis c WITH(NOLOCK), Recursos_Direitos d WITH(NOLOCK), " +
                "RelacoesRecursos e WITH(NOLOCK) " +
                "LEFT OUTER JOIN RelacoesRecursos h WITH(NOLOCK) ON (e.MaquinaEstadoID=h.ObjetoID AND h.Ordem=1 AND h.TipoRelacaoID=3), " +
                "Recursos f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos i WITH(NOLOCK) " +
                "WHERE a.TipoRecursoID = 3 AND a.EstadoID=2 AND a.RecursoMaeID=" + formID + " " +
                "AND b.ObjetoID=999 AND b.TipoRelacaoID=11 " +
                "AND b.SujeitoID IN ((SELECT " + userID + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + userID + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND b.RelacaoID=c.RelacaoID " +
                "AND c.EmpresaID=" + empresaID + " AND d.RecursoID=a.RecursoID AND c.PerfilID=d.PerfilID " +
                "AND (d.Consultar1=1 OR d.Consultar2=1) AND a.RecursoID=e.ObjetoID AND e.TipoRelacaoID=1 AND e.EstadoID=2 " +
                "AND e.SujeitoID=f.RecursoID AND f.Principal=1 AND f.RecursoID=g.RecursoID AND a.RecursoID=g.RecursoMaeID AND g.PerfilID=c.PerfilID " +
                "AND c.PerfilID = i.RecursoID AND i.EstadoID = 2 " +
                "ORDER BY a.Ordem"
            );


            for (int i = 0; i < rsd.Tables[1].Rows.Count; i++)
            {
                lineArray = new object[30];

                recursoID = (int)rsd.Tables[1].Rows[i]["RecursoID"];
                dirC1 = (bool)rsd.Tables[1].Rows[i]["C1"];
                dirC2 = (bool)rsd.Tables[1].Rows[i]["C2"];
                dirI = (bool)rsd.Tables[1].Rows[i]["I"];
                dirA1 = (bool)rsd.Tables[1].Rows[i]["A1"];
                dirA2 = (bool)rsd.Tables[1].Rows[i]["A2"];
                dirE1 = (bool)rsd.Tables[1].Rows[i]["E1"];
                dirE2 = (bool)rsd.Tables[1].Rows[i]["E2"];

                dirC1C2True = (dirC1 && dirC2);
                dirA1A2True = (dirA1 && dirA2);
                dirE1E2True = (dirE1 && dirE2);

                // Preenche os primeiros dados da linha.
                lineArray[0] = rsd.Tables[1].Rows[i]["ControlBar"];
                lineArray[1] = rsd.Tables[1].Rows[i]["Combo"];
                lineArray[2] = rsd.Tables[1].Rows[i]["Recurso"];
                lineArray[3] = rsd.Tables[1].Rows[i]["RecursoID"];
                lineArray[4] = rsd.Tables[1].Rows[i]["SubFormID"];
                lineArray[5] = rsd.Tables[1].Rows[i]["EhDefault"];
                lineArray[6] = rsd.Tables[1].Rows[i]["EstadoInicialID"];

                for (i++; i < rsd.Tables[1].Rows.Count; i++)
                {
                    if (recursoID == (int)rsd.Tables[1].Rows[i]["RecursoID"])
                    {
                        // dirX1 e dirX2 e o anterior sempre
                        dirC1C2True |= ((bool)rsd.Tables[1].Rows[i]["C1"] && (bool)rsd.Tables[1].Rows[i]["C2"]);

                        dirA1A2True |= ((bool)rsd.Tables[1].Rows[i]["A1"] && (bool)rsd.Tables[1].Rows[i]["A2"]);

                        dirE1E2True |= ((bool)rsd.Tables[1].Rows[i]["E1"] && (bool)rsd.Tables[1].Rows[i]["E2"]);

                        dirC1 |= (bool)rsd.Tables[1].Rows[i]["C1"];
                        dirC2 |= (bool)rsd.Tables[1].Rows[i]["C2"];
                        dirI |= (bool)rsd.Tables[1].Rows[i]["I"];
                        dirA1 |= (bool)rsd.Tables[1].Rows[i]["A1"];
                        dirA2 |= (bool)rsd.Tables[1].Rows[i]["A2"];
                        dirE1 |= (bool)rsd.Tables[1].Rows[i]["E1"];
                        dirE2 |= (bool)rsd.Tables[1].Rows[i]["E2"];
                    }
                    else
                    {
                        i--;
                        break;
                    }
                }


                if (dirC1 && dirC2)
                {
                    if (!dirC1C2True)
                    {
                        dirC1 = false;
                        dirC2 = true;
                    }
                }

                if (dirA1 && dirA2)
                {
                    if (!dirA1A2True)
                    {
                        dirA1 = false;
                        dirA2 = true;
                    }
                }

                if (dirE1 && dirE2)
                {
                    if (!dirE1E2True)
                    {
                        dirE1 = false;
                        dirE2 = true;
                    }
                }

                // Acaba de preencher os dados da linha.
                lineArray[7] = dirC1;
                lineArray[8] = dirC2;
                lineArray[9] = dirI;
                lineArray[10] = dirA1;
                lineArray[11] = dirA2;
                lineArray[12] = dirE1;
                lineArray[13] = dirE2;

                dsNew.Tables[1].Rows.Add(lineArray);
            }
        }

        private void rsData2()
        {
            DataSet rsd2 = DataInterfaceObj.getRemoteData(
                "SELECT DISTINCT 'sup' AS ControlBar, '2' AS Combo, d.RecursoID AS RecursoID, " +
                "d.RecursoFantasia AS Recurso, c.Ordem AS Ordem, a.SujeitoID AS SubFormID, " +
                "c.EhDefault, 0 AS C1, 0 AS C2, 0 AS I, 0 AS A1, 0 AS A2, 0 AS E1, 0 AS E2, 0 AS EstadoInicialID " +
                "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec e WITH(NOLOCK), " +
                "RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " +
                "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID=" + contextID + " " +
                "AND a.SujeitoID=b.RecursoID AND b.Principal=1 AND a.SujeitoID=c.ObjetoID AND c.EstadoID=2 " +
                "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.ClassificacaoID=12 " +
                "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) " +
                "WHERE a.RecursoID=" + contextID + ")) " +
                "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " +
                "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " +
                "AND e.SujeitoID IN ((SELECT " + userID + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + userID + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND e.RelacaoID=f.RelacaoID " +
                "AND f.EmpresaID=" + empresaID + " AND a.ObjetoID=g.ContextoID AND c.ObjetoID=g.RecursoMaeID AND c.SujeitoID=g.RecursoID " +
                "AND (g.Consultar1=1 OR g.Consultar2=1) " +
                "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " +
                "ORDER BY c.Ordem"
            );

            // Coleta as informações do segundo dataset.
            foreach (DataRow dr in rsd2.Tables[1].Rows)
            {
                lineArray = new object[30];

                lineArray[0] = dr["ControlBar"];
                lineArray[1] = dr["Combo"];
                lineArray[2] = dr["Recurso"];
                lineArray[3] = dr["RecursoID"];
                lineArray[4] = dr["SubFormID"];
                lineArray[5] = dr["EhDefault"];
                lineArray[6] = dr["EstadoInicialID"];

                dsNew.Tables[1].Rows.Add(lineArray);
            }
        }

        private void rsData3()
        {
            DataSet rsd3 = DataInterfaceObj.getRemoteData(
                "SELECT DISTINCT 'inf' AS ControlBar, '2' AS Combo, d.RecursoID AS RecursoID, " +
                "d.RecursoFantasia AS Recurso, c.Ordem AS Ordem, a.SujeitoID AS SubFormID, " +
                "c.EhDefault, 0 AS C1, 0 AS C2, 0 AS I, 0 AS A1, 0 AS A2, 0 AS E1, 0 AS E2, 0 AS EstadoInicialID " +
                "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec e WITH(NOLOCK), " +
                "RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " +
                "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID=" + contextID + " " +
                "AND a.SujeitoID=b.RecursoID AND b.Principal=0 AND a.SujeitoID=c.ObjetoID AND c.EstadoID=2 " +
                "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.ClassificacaoID=12 " +
                "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) " +
                "WHERE a.RecursoID=" + contextID + ")) " +
                "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " +
                "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " +
                "AND e.SujeitoID IN ((SELECT " + userID + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + userID + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND e.RelacaoID=f.RelacaoID " +
                "AND f.EmpresaID=" + empresaID + " AND a.ObjetoID=g.ContextoID AND c.ObjetoID=g.RecursoMaeID AND c.SujeitoID=g.RecursoID " +
                "AND (g.Consultar1=1 OR g.Consultar2=1) " +
                "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " +
                "ORDER BY c.Ordem"
            );

            // Coleta as informações do terceiro dataset.
            foreach (DataRow dr in rsd3.Tables[1].Rows)
            {
                lineArray = new object[30];

                lineArray[0] = dr["ControlBar"];
                lineArray[1] = dr["Combo"];
                lineArray[2] = dr["Recurso"];
                lineArray[3] = dr["RecursoID"];
                lineArray[4] = dr["SubFormID"];
                lineArray[5] = dr["EhDefault"];
                lineArray[6] = dr["EstadoInicialID"];

                dsNew.Tables[1].Rows.Add(lineArray);
            }
        }

        // Copia as pastas
        private void rsData4()
        {
            DataSet rsd4 = DataInterfaceObj.getRemoteData(
                "SELECT DISTINCT 'inf' AS ControlBar, '1' AS Combo, b.RecursoID AS RecursoID, " +
                    "b.RecursoFantasia AS Recurso, b.RecursoID AS Ordem, 0 AS SubFormID, " +
                    "0 AS EhDefault, e.Consultar1 AS C1, e.Consultar2 AS C2, e.Incluir AS I, " +
                    "e.Alterar1 AS A1, e.Alterar2 AS A2, e.Excluir1 AS E1, e.Excluir2 AS E2, ISNULL(f.SujeitoID,0) AS EstadoInicialID " +
                    "FROM RelacoesRecursos a WITH(NOLOCK) " +
                    "LEFT OUTER JOIN RelacoesRecursos f WITH(NOLOCK) ON (a.MaquinaEstadoID=f.ObjetoID AND f.Ordem=1 AND f.TipoRelacaoID=3), " +
                    "Recursos b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK), " +
                    "RelacoesPesRec_Perfis d WITH(NOLOCK), Recursos_Direitos e WITH(NOLOCK), Recursos g WITH(NOLOCK) " +
                "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID= " + contextID + " " +
                    "AND a.SujeitoID=b.RecursoID AND b.Principal=0 AND b.EstadoID=2 " +
                    "AND c.ObjetoID=999 AND c.TipoRelacaoID=11 " +
                    "AND c.SujeitoID IN ((SELECT " + userID + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + userID + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                    "AND c.RelacaoID=d.RelacaoID " +
                    "AND d.EmpresaID= " + empresaID + " " +
                    "AND a.ObjetoID=e.RecursoMaeID AND a.SujeitoID=e.RecursoID " +
                    "AND (e.Consultar1=1 OR e.Consultar2=1) AND d.PerfilID=e.PerfilID " +
                    "AND d.PerfilID = g.RecursoID AND g.EstadoID = 2 " +
                "ORDER BY Ordem"
            );

            DataRow dr;

            for (int i = 0; i < rsd4.Tables[1].Rows.Count; i++)
            {
                dr = rsd4.Tables[1].Rows[i];
                lineArray = new object[30];

                recursoID = (int)dr["RecursoID"];
                dirC1 = (bool)dr["C1"];
                dirC2 = (bool)dr["C2"];
                dirI = (bool)dr["I"];
                dirA1 = (bool)dr["A1"];
                dirA2 = (bool)dr["A2"];
                dirE1 = (bool)dr["E1"];
                dirE2 = (bool)dr["E2"];

                lineArray[0] = dr["ControlBar"];
                lineArray[1] = dr["Combo"];
                lineArray[2] = dr["Recurso"];
                lineArray[3] = dr["RecursoID"];
                lineArray[4] = dr["SubFormID"];
                lineArray[5] = dr["EhDefault"];
                lineArray[6] = dr["EstadoInicialID"];

                dirC1C2True = (dirC1 && dirC2);
                dirA1A2True = (dirA1 && dirA2);
                dirE1E2True = (dirE1 && dirE2);

                for (i++; i < rsd4.Tables[1].Rows.Count; i++)
                {
                    dr = rsd4.Tables[1].Rows[i];

                    if (recursoID == (int)dr["RecursoID"])
                    {
                        // dirX1 e dirX2 e o anterior sempre
                        if ((bool)dr["C1"] && (bool)dr["C2"])
                        {
                            dirC1C2True = true;
                        }

                        if ((bool)dr["A1"] && (bool)dr["A2"])
                        {
                            dirA1A2True = true;
                        }

                        if ((bool)dr["E1"] && (bool)dr["E2"])
                        {
                            dirE1E2True = true;
                        }

                        dirC1 |= (bool)dr["C1"];
                        dirC2 |= (bool)dr["C2"];
                        dirI |= (bool)dr["I"];
                        dirA1 |= (bool)dr["A1"];
                        dirA2 |= (bool)dr["A2"];
                        dirE1 |= (bool)dr["E1"];
                        dirE2 |= (bool)dr["E2"];
                    }
                    else
                    {
                        i--;
                        break;
                    }
                }

                if (dirC1 && dirC2)
                {
                    if (!dirC1C2True)
                    {
                        dirC1 = false;
                        dirC2 = true;
                    }
                }

                if (dirA1 && dirA2)
                {
                    if (!dirA1A2True)
                    {
                        dirA1 = false;
                        dirA2 = true;
                    }
                }

                if (dirE1 && dirE2)
                {
                    if (!dirE1E2True)
                    {
                        dirE1 = false;
                        dirE2 = true;
                    }
                }

                // Acaba de preencher os dados da linha.
                lineArray[7] = dirC1;
                lineArray[8] = dirC2;
                lineArray[9] = dirI;
                lineArray[10] = dirA1;
                lineArray[11] = dirA2;
                lineArray[12] = dirE1;
                lineArray[13] = dirE2;

                // Insere a linha no resultado.
                dsNew.Tables[1].Rows.Add(lineArray);
            }
        }

        private void rsData5()
        {
            foreach (DataRow dr in dsNew.Tables[1].Rows)
            {
                for (int i = 1; i <= 16; i++)
                {
                    dr["B" + i.ToString() + "A1"] = 0;
                    dr["B" + i.ToString() + "A2"] = 0;
                    dr["B" + i.ToString() + "C1"] = 0;
                    dr["B" + i.ToString() + "C2"] = 0;
                    dr["B" + i.ToString() + "I"] = 0;
                    dr["B" + i.ToString() + "E1"] = 0;
                    dr["B" + i.ToString() + "E2"] = 0;
                }
            }

            DataSet rsData5 = DataInterfaceObj.getRemoteData(
                "SELECT DISTINCT b.RecursoID AS SubFormID, b.Principal AS Principal, " +
                "c.SujeitoID AS BotaoID, g.Alterar1, g.Alterar2, g.Incluir, g.Consultar1, g.Consultar2, g.Excluir1, g.Excluir2 " +
                "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " +
                "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " +
                "WHERE a.ObjetoID= " + contextID + " AND a.TipoRelacaoID=1 AND a.EstadoID=2 AND a.SujeitoID=b.RecursoID " +
                "AND (b.Principal=1 OR b.Principal=0) AND b.EstadoID=2 AND b.TipoRecursoID=4 AND b.RecursoID=c.ObjetoID " +
                "AND c.TipoRelacaoID=2 AND c.EstadoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.TipoRecursoID=5 " +
                "AND d.ClassificacaoID=13 AND (d.RecursoID BETWEEN 40001 AND 40016) " +
                "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " + contextID + " )) " +
                "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " +
                "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " +
                "AND e.SujeitoID IN ((SELECT " + userID + " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + userID + " AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
                "AND e.RelacaoID=f.RelacaoID " +
                "AND f.EmpresaID= " + empresaID + " AND g.RecursoID=d.RecursoID " +
                "AND g.RecursoMaeID=b.RecursoID " +
                "AND g.ContextoID= " + contextID + " AND g.PerfilID=f.PerfilID " +
                "AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " +
                "ORDER BY SubFormID"
            );

            DataRow dr5;
            DataRow[] dsNew_F;
            DataRow[] rsData5_F;
            //int i;

            if (rsData5.Tables[1].Rows.Count > 0)
            {
                dsNew_F = dsNew.Tables[1].Select("RecursoID = " + contextID);
                rsData5_F = rsData5.Tables[1].Select("Principal = 1");

                if (dsNew_F.Length > 0 && rsData5_F.Length > 0)
                {
                    for (int i = 0; i < 16; i++)
                    {
                        for (int i5 = 0; i5 < rsData5_F.Length; i5++)
                        {
                            dr5 = rsData5_F[i5];

                            if ((int)dr5["BotaoID"] != (40000 + i + 1))
                            {
                                continue;
                            }

                            // Direitos de Alteracao
                            dirA1 = (bool)dr5["Alterar1"];
                            dirA2 = (bool)dr5["Alterar2"];
                            dirA1A2True = (dirA1 && dirA2);

                            // Direitos de Consulta
                            dirC1 = (bool)dr5["Consultar1"];
                            dirC2 = (bool)dr5["Consultar2"];
                            dirC1C2True = (dirC1 && dirC2);

                            // Direitos de Exclusao
                            dirE1 = (bool)dr5["Excluir1"];
                            dirE2 = (bool)dr5["Excluir2"];
                            dirE1E2True = (dirE1 && dirE2);

                            // Direitos de Inclusao
                            dirI = (bool)dr5["Incluir"];

                            while (++i5 < rsData5_F.Length)
                            {
                                dr5 = rsData5_F[i5];
                                
                                if ((int)dr5["BotaoID"] != (40000 + i + 1))
                                {
                                    continue;
                                }

                                if ((bool)dr5["Alterar1"] && (bool)dr5["Alterar2"])
                                {
                                    dirA1A2True = true;
                                }
                                dirA1 |= (bool)dr5["Alterar1"];
                                dirA2 |= (bool)dr5["Alterar2"];

                                if ((bool)dr5["Consultar1"] && (bool)dr5["Consultar2"])
                                {
                                    dirC1C2True = true;
                                }
                                dirC1 |= (bool)dr5["Consultar1"];
                                dirC2 |= (bool)dr5["Consultar2"];

                                if ((bool)dr5["Excluir1"] && (bool)dr5["Excluir2"])
                                {
                                    dirE1E2True = true;
                                }
                                dirE1 |= (bool)dr5["Excluir1"];
                                dirE2 |= (bool)dr5["Excluir2"];

                                dirI |= (bool)dr5["Incluir"];
                            }

                            if (dirA1 && dirA2)
                            {
                                if (!dirA1A2True)
                                {
                                    dirA1 = false;
                                    dirA2 = true;
                                }
                            }

                            if (dirC1 && dirC2)
                            {
                                if (!dirC1C2True)
                                {
                                    dirC1 = false;
                                    dirC2 = true;
                                }
                            }

                            if (dirE1 && dirE2)
                            {
                                if (!dirE1E2True)
                                {
                                    dirE1 = false;
                                    dirE2 = true;
                                }
                            }

                            dsNew_F[0]["B" + (i + 1).ToString() + "A1"] = dirA1;
                            dsNew_F[0]["B" + (i + 1).ToString() + "A2"] = dirA2;
                            dsNew_F[0]["B" + (i + 1).ToString() + "C1"] = dirC1;
                            dsNew_F[0]["B" + (i + 1).ToString() + "C2"] = dirC2;
                            dsNew_F[0]["B" + (i + 1).ToString() + "E1"] = dirE1;
                            dsNew_F[0]["B" + (i + 1).ToString() + "E2"] = dirE2;
                            dsNew_F[0]["B" + (i + 1).ToString() + "I"] = dirI;
                        }
                    }
                }

                // Agora os direitos dos botoes especificos do control bar inferior.
                // Para todas as pastas
                int folderID;
                foreach (DataRow drnew in dsNew.Tables[1].Rows)
                {
                    folderID = 0;
                    if (drnew["ControlBar"].ToString().Equals("inf") && int.Parse(drnew["Combo"].ToString()) == 1)
                    {
                        folderID = int.Parse(drnew["RecursoID"].ToString());
                    }

                    rsData5_F = rsData5.Tables[1].Select("SubFormID = " + folderID);

                    if (folderID != 0 && rsData5_F.Length > 0)
                    {
                        for (int i = 0; i < 16; i++)
                        {
                            for (int i5 = 0; i5 < rsData5_F.Length; i5++)
                            {
                                dr5 = rsData5_F[i5];

                                if (((bool)dr5["Principal"]) || int.Parse(dr5["BotaoID"].ToString()) != (40000 + i + 1))
                                {
                                    continue;
                                }

                                // Direitos de Alteracao
                                dirA1 = (bool)dr5["Alterar1"];
                                dirA2 = (bool)dr5["Alterar2"];
                                dirA1A2True = (dirA1 && dirA2);

                                // Direitos de Consulta
                                dirC1 = (bool)dr5["Consultar1"];
                                dirC2 = (bool)dr5["Consultar2"];
                                dirC1C2True = (dirC1 && dirC2);

                                // Direitos de Exclusao
                                dirE1 = (bool)dr5["Excluir1"];
                                dirE2 = (bool)dr5["Excluir2"];
                                dirE1E2True = (dirE1 && dirE2);

                                // Direitos de Inclusao
                                dirI = (bool)dr5["Incluir"];

                                while (++i5 < rsData5_F.Length)
                                {
                                    dr5 = rsData5_F[i5];

                                    if (((bool)dr5["Principal"]) || int.Parse(dr5["BotaoID"].ToString()) != (40000 + i + 1))
                                    {
                                        continue;
                                    }

                                    dirA1A2True = ((bool)dr5["Alterar1"] && (bool)dr5["Alterar2"]);
                                    dirA1 |= (bool)dr5["Alterar1"];
                                    dirA2 |= (bool)dr5["Alterar2"];

                                    dirC1C2True = ((bool)dr5["Consultar1"] && (bool)dr5["Consultar2"]);
                                    dirC1 |= (bool)dr5["Consultar1"];
                                    dirC2 |= (bool)dr5["Consultar2"];

                                    dirE1E2True = ((bool)dr5["Excluir1"] && (bool)dr5["Excluir2"]);
                                    dirE1 |= (bool)dr5["Excluir1"];
                                    dirE2 |= (bool)dr5["Excluir2"];

                                    dirI |= (bool)dr5["Incluir"];
                                }
                                

                                if (dirA1 && dirA2)
                                {
                                    if (!dirA1A2True)
                                    {
                                        dirA1 = false;
                                        dirA2 = true;
                                    }
                                }

                                if (dirC1 && dirC2)
                                {
                                    if (!dirC1C2True)
                                    {
                                        dirC1 = false;
                                        dirC2 = true;
                                    }
                                }

                                if (dirE1 && dirE2)
                                {
                                    if (!dirE1E2True)
                                    {
                                        dirE1 = false;
                                        dirE2 = true;
                                    }
                                }

                                drnew["B" + (i + 1).ToString() + "A1"] = dirA1;
                                drnew["B" + (i + 1).ToString() + "A2"] = dirA2;
                                drnew["B" + (i + 1).ToString() + "C1"] = dirC1;
                                drnew["B" + (i + 1).ToString() + "C2"] = dirC2;
                                drnew["B" + (i + 1).ToString() + "E1"] = dirE1;
                                drnew["B" + (i + 1).ToString() + "E2"] = dirE2;
                                drnew["B" + (i + 1).ToString() + "I"] = dirI;
                            }
                        }
                    }
                }
            }
        }
    }
}
