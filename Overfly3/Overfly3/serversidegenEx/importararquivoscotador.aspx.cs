﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web.UI;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class importararquivoscotador : System.Web.UI.OverflyPage
    {
        private int cont = 0;

        protected override void PageLoad(object sender, EventArgs e)
        {
            
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUpload1.Enabled = false;
            btnUpload.Enabled = false;

            try
            {
                //Limita tamanho do arquivo.
                //Atualmente em 200MB (utilizar medida em byte).
                if (FileUpload1.PostedFile.ContentLength < (1048576 * 200))
                {
                    //Verifica se um arquivo está selecionado ou vazio.
                    if (FileUpload1.HasFile && FileUpload1.PostedFile != null)
                    {
                        //Guarda tamanho do arquivo.
                        int ilength = Convert.ToInt32(FileUpload1.PostedFile.InputStream.Length);

                        //Guarda tipo do arquivo.
                        string content = FileUpload1.PostedFile.ContentType;

                        //Guarda nome do arquivo sem caminho.
                        string[]
                            ar_file_name = FileUpload1.PostedFile.FileName.ToString().Trim().Split('\\');
                        string file_name = ar_file_name[ar_file_name.Length - 1].ToString().Trim();

                        //Gera vetor com tamanho do arquivo
                        byte[] bytecontent = new System.Byte[ilength];
                        FileUpload1.PostedFile.InputStream.Read(bytecontent, 0, ilength);

                        //Tipo de arquivo
                        int nTipoArquivoID = int.Parse(Request.QueryString["nTipoArquivoID"].ToString());

                        //Tipo de arquivo do cotador (carga de desconto/carga completa)
                        int nTipoArquivoCotadorID = int.Parse(Request.QueryString["nTipoArquivoCotadorID"].ToString());

                        //MarcaID
                        int nMarcaID = int.Parse(Request.QueryString["nMarcaID"].ToString());

                        //Guarda ID do form que a modal pertence.
                        string nFormID = Request.QueryString["nFormID"].ToString();

                        //Guarda ID do subform que a modal pertence.
                        string nSubFormID = Request.QueryString["nSubFormID"].ToString();

                        //Guarda ID do Usuário logado.
                        string nUserID = Request.QueryString["nUserID"].ToString();

                        if (nTipoArquivoCotadorID == 0)
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Não foi possivel realizar o upload! \\n Por favor, escolha o tipo de arquivo.')", true);
                        else if (nMarcaID == 0)
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Não foi possivel realizar o upload! \\n Por favor, selecione a marca.')", true);
                        else
                            //Passa stream de dados para salvar no Overfly  
                            saveData(file_name, bytecontent, ilength, content, nTipoArquivoID, nTipoArquivoCotadorID, nMarcaID, nFormID, nSubFormID, nUserID);

                        //Executa procedure de processamento dos dados obtidos
                        //processaDados();

                        //Envia email se houveram erros no processamento
                        //int result = enviaEmail();

                        FileUpload1.Enabled = true;
                        btnUpload.Enabled = true;
                    }
                    else
                    {
                        FileUpload1.Enabled = true;
                        btnUpload.Enabled = true;

                        //Exibe mensagem de erro para arquivo não selecionado ou vazio.
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Não foi possivel realizar o upload! \\n Verifique se o caminho é válido e se o arquivo não está vazio.')", true);
                    }
                }
                else
                {
                    FileUpload1.Enabled = true;
                    btnUpload.Enabled = true;

                    //Exibe mensagem de erro para arquivo que excede o tamanho em bytes.
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Não foi possivel realizar o upload! \\n O tamanho do arquivo excede 200MB.')", true);
                }
            }
            catch (System.Exception ex)
            {
                FileUpload1.Enabled = true;
                btnUpload.Enabled = true;

                //Exibe mensagem de erros não previstos.
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Erro: \\n" + ex.Message + "')", true);
            }
        }

        private void saveData(string file_name, byte[] bytecontent, int ilength,  string content, int nTipoArquivoID, int nTipoArquivoCotadorID, int nMarcaID, string nFormID, string nSubFormID, string nUserID)
        {
            // Verifica o tamanho do nome do documento
            cont = file_name.Length;

            if (cont <= 50)
            {
                //Se não existe arquivo INSERE
                //Verifica conexao do overfly.
                if (DataInterfaceObj.connectDatabase())
                {
                    //string dtUpload = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
                    
                    //Atribui conexao do overfly.
                    SqlConnection con;
                    con = DataInterfaceObj.getConnection();

                    //Atribui query.
                    string sql =    /*"DECLARE @CotAtualizacaoID INT " +
                                    "DECLARE @CotAtualizacao TABLE (CotAtualizacaoID INT) " +*/

                                    "INSERT INTO Cotador_Atualizacoes (MarcaID, TipoArquivoID, Arquivo, StatusID, dtAtualizacao, UsuarioID) " +
	                                //"OUTPUT inserted.CotAtualizacaoID INTO @CotAtualizacao " +
                                    "VALUES(@marcaid, @tipoArquivocotadorid, @name, 0, @data, @user) " +

                                    "INSERT INTO [OVERFLYBASE_EXT2].Overfly_Documentos.dbo.Documentos " +
                                        "(Arquivo,Nome,ContentType, Tamanho, dtDocumento, FormID, TipoArquivoID, SubformID, RegistroID, UsuarioID) " +
                                    "VALUES (@file, @name, @content, @size, @data, @form, @tipoarquivoid, @subform, (SELECT TOP 1 CotAtualizacaoID FROM Cotador_Atualizacoes ORDER BY CotAtualizacaoID DESC), @user) ";

                    //Gera SqlCommand com parametos da query e conexao.
                    SqlCommand cmd = new SqlCommand(sql, con);
                    cmd.CommandTimeout = DataInterfaceObj.timeout;

                    //Faz a leitura do arquivo para o vetor bytecontent.
                    FileUpload1.PostedFile.InputStream.Read(bytecontent, 0, ilength);

                    //Atribui parametros com os dados do arquivo
                    cmd.Parameters.AddWithValue("@file", bytecontent);
                    cmd.Parameters.AddWithValue("@name", file_name);
                    cmd.Parameters.AddWithValue("@content", content);
                    cmd.Parameters.AddWithValue("@size", ilength / 1024);
                    cmd.Parameters.AddWithValue("@data", DateTime.Now);
                    cmd.Parameters.AddWithValue("@form", nFormID);
                    cmd.Parameters.AddWithValue("@tipoarquivoid", nTipoArquivoID);
                    cmd.Parameters.AddWithValue("@tipoArquivocotadorid", nTipoArquivoCotadorID);
                    cmd.Parameters.AddWithValue("@marcaid", nMarcaID);
                    cmd.Parameters.AddWithValue("@subform", nSubFormID);
                    cmd.Parameters.AddWithValue("@user", nUserID);

                    //Executa o SqlCommando para a gravacao do arquivo no banco de dados.
                    cmd.ExecuteNonQuery();

                    //Exibe mensagem de arquivo gravado com sucesso.
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "window.parent.btnCanc_onclick();", true);
                }
                else
                {
                    //Exibe mensagem de erro de conexão do overfly.
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Verifique a conexão!')", true);
                }
            }
            else
            {
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Nome do documento é maior do que o permitido, diminua e tente novamente!')", true);
            }
        }
    }
}