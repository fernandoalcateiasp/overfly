﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class cmbspropspl2 : System.Web.UI.OverflyPage
    {
        private static Integer ZERO = new Integer(0);
        private static java.lang.Boolean FALSE = new java.lang.Boolean(false);
        
        private Integer contextoId = ZERO;
        private Integer subFormID = ZERO;
        private Integer filtroPadraoID = ZERO;
        private java.lang.Boolean registrosVencidos = FALSE;
        private Integer empresaId = ZERO;
        private Integer usuarioId = ZERO;
        private java.lang.Boolean allList = FALSE;
        private java.lang.Boolean resultado = FALSE;

        public Integer nContextoID
        {
            set { contextoId = value != null ? value : ZERO; }
        }
        public Integer nSubFormID
        {
            set { subFormID = value != null ? value : ZERO; }
        }
        public Integer nFiltroPadraoID
        {
            set { filtroPadraoID = value != null ? value : ZERO; }
        }
        public java.lang.Boolean bRegistrosVencidos
        {
            set { registrosVencidos = value != null ? value : FALSE; }
        }
        public Integer nEmpresaID
        {
            set { empresaId = value != null ? value : ZERO; }
        }
        public Integer nUsuarioID
        {
            set { usuarioId = value != null ? value : ZERO; }
        }
        public java.lang.Boolean bAllList
        {
            set { allList = value != null ? value : FALSE; }
        }
        public java.lang.Boolean bResultado
        {
            set { resultado = value != null ? value : FALSE; }
        }
 
        protected DataSet FormProprietarios()
        {
            // Cria os parametros para a procedure.			
            ProcedureParameters[] proprietariosProcParam =
                new ProcedureParameters[8];

            proprietariosProcParam[0] = new ProcedureParameters(
                "@ContextoID", System.Data.SqlDbType.Int, contextoId.ToString());
            proprietariosProcParam[1] = new ProcedureParameters(
                "@SubFormID", System.Data.SqlDbType.Int, subFormID.ToString());
            proprietariosProcParam[2] = new ProcedureParameters(
                "@FiltroPadraoID", System.Data.SqlDbType.Int, filtroPadraoID.ToString());
            proprietariosProcParam[3] = new ProcedureParameters(
                "@RegistrosVencidos", System.Data.SqlDbType.Bit, registrosVencidos.ToString());
            proprietariosProcParam[4] = new ProcedureParameters(
                "@EmpresaID", System.Data.SqlDbType.Int, empresaId.ToString());
            proprietariosProcParam[5] = new ProcedureParameters(
                "@UsuarioID", System.Data.SqlDbType.Int, usuarioId.ToString());
            proprietariosProcParam[6] = new ProcedureParameters(
                "@Proprietarios", System.Data.SqlDbType.Bit, allList.ToString());
            proprietariosProcParam[7] = new ProcedureParameters(
                "@Resultado", System.Data.SqlDbType.Bit, resultado.ToString());

            // Executa a procedure
            return DataInterfaceObj.execQueryProcedure(
                "sp_Form_Proprietarios2",
                proprietariosProcParam);
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(FormProprietarios());
        }
    }
}
