using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSOverflyRDS;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class notificacoestabledata : System.Web.UI.OverflyPage
    {
        private Integer usuarioId;
        private java.lang.Boolean lidasBit;
        private String Html = String.Empty;
        private java.lang.Boolean habilitaEmail;

        public Integer nUsuarioID
		{
            set { usuarioId = value; }
		}

        public java.lang.Boolean nLidasBit
		{
            set { lidasBit = value; }
		}
        public java.lang.Boolean nHabilitaEmail
        {
            set { habilitaEmail = value; }
        }

		// Executa procedure 
        protected void execPesqlistSql()
        {
            ProcedureParameters[] procParams = new ProcedureParameters[5];

            procParams[0] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                usuarioId.ToString());

            procParams[1] = new ProcedureParameters(
                "@dtAgora",
                System.Data.SqlDbType.VarChar,
                DBNull.Value);

            procParams[2] = new ProcedureParameters(
                "@SoNotificacoesNaoLidas",
                System.Data.SqlDbType.Bit,
                lidasBit.booleanValue() ? 1 : 0);

            procParams[3] = new ProcedureParameters(
                "@HabilitaEnvioEmail",
                System.Data.SqlDbType.Bit,
                habilitaEmail.booleanValue() ? 1 : 0);

            procParams[4] = new ProcedureParameters(
                "@HTML",
                System.Data.SqlDbType.VarChar,
                null,
                ParameterDirection.Output);
            procParams[4].Length = 15000;
            
            DataInterfaceObj.execNonQueryProcedure(
                "sp_Notificacoes",
                procParams);

            Html = procParams[4].Data.ToString();
        }


		protected override void PageLoad(object sender, EventArgs e)
		{
            execPesqlistSql();

            Html = Html.Replace("'", "''");

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT ' " + Html + " ' AS Texto "));
        }
    }
}
