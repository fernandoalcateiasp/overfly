﻿using System;
using System.Data;
using System.Data.OleDb;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Text;
using System.Web.UI;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class importadadosfornecedor : System.Web.UI.OverflyPage
    {
        int atualizacaoID;

        protected override void PageLoad(object sender, EventArgs e)
        {
            
        }

        protected void btnUpload_Click(object sender, EventArgs e)
        {
            FileUpload1.Enabled = false;
            btnUpload.Enabled = false;

            try
            {
                //Limita tamanho do arquivo.
                //Atualmente em 200MB (utilizar medida em byte).
                if (FileUpload1.PostedFile.ContentLength < (1048576 * 200))
                {
                    //Verifica se um arquivo está selecionado ou vazio.
                    if (FileUpload1.HasFile && FileUpload1.PostedFile != null)
                    {
                        //Guarda tamanho do arquivo.
                        int ilength = Convert.ToInt32(FileUpload1.PostedFile.InputStream.Length);
                        //Guarda tipo do arquivo.
                        string content = FileUpload1.PostedFile.ContentType;

                        //Guarda nome do arquivo sem caminho.
                        string[]
                            ar_file_name = FileUpload1.PostedFile.FileName.ToString().Trim().Split('\\');
                        string file_name = ar_file_name[ar_file_name.Length - 1].ToString().Trim();

                        //Guarda ID do Usuário logado.
                        string nUserID = Request.QueryString["nUserID"].ToString();

                        //Passa stream de dados para salvar no Overfly
                        saveData(FileUpload1.PostedFile.InputStream, nUserID, file_name);

                        //Executa procedure de processamento dos dados obtidos
                        processaDados();

                        //Envia email se houveram erros no processamento
                        int result = enviaEmail();

                        FileUpload1.Enabled = true;
                        btnUpload.Enabled = true;

                        //Exibe mensagem sobre processamento do arquivo
                        if (result == 0)
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "window.parent.btnCancelar_onclick(true, 'Arquivo " + file_name + ": \\n Registros atualizados com sucesso!')", true);
                        else if (result == 1)
                            Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "window.parent.btnCancelar_onclick(false, 'Arquivo " + file_name + ": \\n Alguns registros não puderam ser atualizados, verifique seu email.')", true);
                    }
                    else
                    {
                        FileUpload1.Enabled = true;
                        btnUpload.Enabled = true;

                        //Exibe mensagem de erro para arquivo não selecionado ou vazio.
                        Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Não foi possivel realizar o upload! \\n Verifique se o caminho é válido e se o arquivo não está vazio.')", true);
                    }
                }
                else
                {
                    FileUpload1.Enabled = true;
                    btnUpload.Enabled = true;

                    //Exibe mensagem de erro para arquivo que excede o tamanho em bytes.
                    Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Não foi possivel realizar o upload! \\n O tamanho do arquivo excede 200MB.')", true);
                }
            }
            catch (System.Exception ex)
            {
                FileUpload1.Enabled = true;
                btnUpload.Enabled = true;

                //Exibe mensagem de erros não previstos.
                Page.ClientScript.RegisterStartupScript(Page.GetType(), "alerta", "alert('Erro: \\n" + ex.Message + "')", true);
            }
        }
        
        private void saveData(Stream fileStream, string nUserID, string fileName)
        {
            char charsplit = fileName.Contains(".tsv") ? Convert.ToChar(9) : ';';
            //Lê o stream de dados do arquivo
            StreamReader sr = new StreamReader(fileStream, Encoding.GetEncoding(65001)); // Codepage: 65001 - UTF-8
            string[] headers = sr.ReadLine().Split(charsplit);
            DataTable dt = new DataTable();

            foreach (string header in headers)
                dt.Columns.Add(header);

            while (!sr.EndOfStream)
            {
                string[] rows = sr.ReadLine().Split(charsplit);
                DataRow dr = dt.NewRow();

                for (int i = 0; i < headers.Length; i++)
                    dr[i] = rows[i];

                dt.Rows.Add(dr);
            }
            sr.Close();

            // Colunas da tabela CAPA - _Atualização_DadosProdutos
            // atualizacaoID;
            int usuarioID = int.Parse(nUserID);
            string dtUpload = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            
            // dtProcessamento = null
            string nomeArquivo = fileName;

            if (DataInterfaceObj.connectDatabase())
            {
                //Atribui conexao do overfly.
                SqlConnection con;
                con = DataInterfaceObj.getConnection();

                //Atribui query.
                string sql = "INSERT INTO _Atualização_DadosProdutos (UsuarioID, dtUpload, NomeArquivo) VALUES (@usuarioID, @dtUpload, @nomeArquivo); SELECT SCOPE_IDENTITY () AS AtualizacaoID";

                //Gera SqlCommand com parametos da query e conexao.
                SqlCommand cmd = new SqlCommand(sql, con);
                cmd.CommandTimeout = DataInterfaceObj.timeout;

                //Atribui parametros com os dados do arquivo
                cmd.Parameters.AddWithValue("@usuarioID", usuarioID);
                cmd.Parameters.AddWithValue("@dtUpload", dtUpload);
                cmd.Parameters.AddWithValue("@nomeArquivo", nomeArquivo);

                //Executa o SqlCommando para e pega ID gerado no INSERT.
                //atualizacaoID = cmd.ExecuteReader(CommandBehavior.KeyInfo).GetInt32(0);

                SqlDataReader dr = cmd.ExecuteReader();
                
                if (dr.HasRows)
                {
                    dr.Read();
                    atualizacaoID = Convert.ToInt32(dr["AtualizacaoID"]);
                }

                dr.Close();
                
                //Se gravou CAPA do arquivo, insere dados do CSV na tabela do Overfly
                if (atualizacaoID > 0)
                {
                    for (int i = 0; i < dt.Rows.Count; i++)
                    {
                        // Colunas da tabela Detalhe - _Atualização_DadosProdutosDetalhes
                        // relPesConFornecID = null
                        string pais = (dt.Rows[i]["Pais"].ToString() == "" ? null : dt.Rows[i]["Pais"].ToString());    // Colunas da planilha
                        string UF = (dt.Rows[i]["UF"].ToString() == "" ? null : dt.Rows[i]["UF"].ToString());    // Colunas da planilha
                        string origem = (dt.Rows[i]["Origem"].ToString() == "" ? null : dt.Rows[i]["Origem"].ToString());    // Colunas da planilha
                        string partNumber = (dt.Rows[i]["PartNumber"].ToString() == "" ? null : dt.Rows[i]["PartNumber"].ToString());    // Colunas da planilha
                        string moeda = (dt.Rows[i]["Moeda"].ToString() == "" ? null : dt.Rows[i]["Moeda"].ToString());    // Colunas da planilha
                        string custoFOB = (dt.Rows[i]["CustoFOB"].ToString() == "" ? null : dt.Rows[i]["CustoFOB"].ToString());    // Colunas da planilha
                        string FIE = (dt.Rows[i]["FIE"].ToString() == "" ? null : dt.Rows[i]["FIE"].ToString());    // Colunas da planilha
                        string dtValidade = (dt.Rows[i]["dtValidade"].ToString() == "" ? null : dt.Rows[i]["dtValidade"].ToString());    // Colunas da planilha
                        // dtAtualizacao - Colunas da planilha
                        // resultado - Colunas da planilha

                        custoFOB = (custoFOB == null ? null : custoFOB.Replace(",", "."));
                        FIE = (FIE == null ? null : FIE.Replace(",", "."));
                        dtValidade = (dtValidade == null ? null : DateTime.ParseExact(dtValidade, "dd/MM/yyyy", CultureInfo.CreateSpecificCulture("pt-BR")).ToString("yyyy-MM-dd HH:mm:ss"));

                        //Atribui query.
                        sql = "INSERT INTO _Atualização_DadosProdutosDetalhes (AtualizacaoID, Pais, UF, Origem, PartNumber, Moeda, CustoFOB, FIE, dtValidade) " +
                                        "VALUES (@atualizacaoID, @pais, @UF, @origem, @partNumber, @moeda, @custoFOB, @FIE, @dtValidade)";

                        //Gera SqlCommand com parametos da query e conexao.
                        cmd = new SqlCommand(sql, con);
                        cmd.CommandTimeout = DataInterfaceObj.timeout;

                        //Atribui parametros com os dados do arquivo
                        cmd.Parameters.AddWithValue("@atualizacaoID", atualizacaoID);
                        cmd.Parameters.AddWithValue("@pais", ((object)pais) ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@UF", ((object)UF) ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@origem", ((object)origem) ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@partNumber", ((object)partNumber) ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@moeda", ((object)moeda) ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@custoFOB", ((object)custoFOB) ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@FIE", ((object)FIE) ?? DBNull.Value);
                        cmd.Parameters.AddWithValue("@dtValidade", ((object)dtValidade) ?? DBNull.Value);

                        //Executa commando SQL para fazer o insert
                        cmd.ExecuteNonQuery();
                    } 
                }
            }
        }

        private void processaDados()
        {
            // Roda a procedure sp_Importa_CustoFOB
            ProcedureParameters[] procParams = new ProcedureParameters[1];

            procParams[0] = new ProcedureParameters(
                "@AtualizacaoID",
                System.Data.SqlDbType.Int,
                atualizacaoID);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Produto_AtualizaCustoFOB",
                procParams);
        }

        private int enviaEmail()
        {
            // Roda a procedure sp_Importa_CustoFOB
            ProcedureParameters[] procParams = new ProcedureParameters[2];

            procParams[0] = new ProcedureParameters(
                "@AtualizacaoID",
                System.Data.SqlDbType.Int,
                atualizacaoID.ToString());

            procParams[1] = new ProcedureParameters(
                "@Resultado",
                System.Data.SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_AtualizaCustoFob_Email",
                procParams);

            return int.Parse(procParams[1].Data.ToString());
        }
    }
}