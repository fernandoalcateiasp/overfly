using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Globalization;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class SacolaEntrega : System.Web.UI.OverflyPage
    {
        private string sacolaID;
        private string empresaID;
        private string totalFrete;
        private string fretePago;
        private string incluirFretePreco;
        private string transportadoraID;
        private string meioTransporteID;
        private string portadorNome;
        private string portadorDocumento;
        private string portadorPlaca;
        private string portadorUFVeiculo;
        private string transportadoraNome;
        private string transportadoraContato;
        private string transportadoraTelefone;
        private string gerarPedido;
        private string usuario2ID;
        private int sacEntregaID;
        private int pedidoID;

        public string nSacolaID
        {    
			set { sacolaID = value; }
		}
        public string nEmpresaID
        {
            set { empresaID = value; }
        }
        public string nTotalFrete
        {
            set { totalFrete = value; }
        }
        public string nFretePago
		{
			set { fretePago = value; }
		}
        public string nIncluirFretePreco
        {
            set { incluirFretePreco = value; }
        }
        public string nTransportadoraID
        {
			set { transportadoraID = value; }
		}
        
        public string nMeioTransporteID
        {
            set { meioTransporteID = value; }
        }
        public string sPortadorNome
        {
			set { portadorNome = value; }
		}
		public string sPortadorDocumento
        {
			set { portadorDocumento = value; }
		}
		public string sPortadorPlaca
        {
			set { portadorPlaca = value; }

		}
        public string sPortadorUFVeiculo
        {
			set { portadorUFVeiculo = value; }
		}
        public string sTransportadoraNome
        {
            set { transportadoraNome = value; }
        }

        public string sTransportadoraContato
        {
            set { transportadoraContato = value; }
        }

        public string sTransportadoraTelefone
        {
            set { transportadoraTelefone = value; }
        }

        public string sGerarPedido
        {
            set { gerarPedido = value; }
        }

        public string nUsuario2ID
        {
            set { usuario2ID = value; }
        }

        // Executa procedure sp_ListaPrecos_Comprar e retorna a mensagem.
        protected string ListaPrecosComprar()
        {
            string result = "";
            ProcedureParameters[] procParams = new ProcedureParameters[24];

            for (int i = 0; i < procParams.Length; i++)
            {
                procParams[i] = new ProcedureParameters();
            }

            procParams[0].Name = "@SacolaID";
			procParams[0].Type = System.Data.SqlDbType.Int;
			procParams[0].Data = int.Parse(sacolaID);

			procParams[1].Name = "@EmpresaID";
			procParams[1].Type = System.Data.SqlDbType.Int;
            procParams[1].Data = int.Parse(empresaID);

			procParams[2].Name = "@DepositoID";
			procParams[2].Type = System.Data.SqlDbType.Int;
			procParams[2].Data = System.DBNull.Value;

			procParams[3].Name = "@TotalSacola";
			procParams[3].Type = System.Data.SqlDbType.Decimal;
			procParams[3].Data = System.DBNull.Value;

            procParams[4].Name = "@TotalFrete";
			procParams[4].Type = System.Data.SqlDbType.Decimal;
			procParams[4].Data = totalFrete.Length > 0 ? (Object)totalFrete : System.DBNull.Value;

            procParams[5].Name = "@FretePago";
			procParams[5].Type = System.Data.SqlDbType.Bit;
			procParams[5].Data = int.Parse(fretePago);

            procParams[6].Name = "@IncluirFretePreco";
            procParams[6].Type = System.Data.SqlDbType.Bit;
            procParams[6].Data = int.Parse(incluirFretePreco);

            procParams[7].Name = "@TransportadoraID";
            procParams[7].Type = System.Data.SqlDbType.Int;
            procParams[7].Data = transportadoraID.Length > 0 ? (Object)transportadoraID : System.DBNull.Value;
                 
            procParams[8].Name = "@MeioTransporteID";
            procParams[8].Type = System.Data.SqlDbType.Int;
            procParams[8].Data = meioTransporteID.Length > 0 ? (Object)meioTransporteID : System.DBNull.Value;

            procParams[9].Name = "@PortadorNome";
            procParams[9].Type = System.Data.SqlDbType.VarChar;
            procParams[9].Data = ((portadorNome.Length > 0) ? (Object)portadorNome : System.DBNull.Value);
            procParams[9].Length = 20;

            procParams[10].Name = "@PortadorDocumento";
			procParams[10].Type = System.Data.SqlDbType.VarChar;
			procParams[10].Data = ((portadorDocumento.Length > 0) ? (Object)portadorDocumento : System.DBNull.Value);
            procParams[10].Length = 15;

            procParams[11].Name = "@PortadorDocumentoFederal";
			procParams[11].Type = System.Data.SqlDbType.VarChar;
			procParams[11].Data = System.DBNull.Value;
            procParams[11].Length = 20;

            procParams[12].Name = "@PortadorPlaca";
            procParams[12].Type = System.Data.SqlDbType.VarChar;
            procParams[12].Data = ((portadorPlaca.Length > 0) ? (Object)portadorPlaca : System.DBNull.Value);
            procParams[12].Length = 7;

            procParams[13].Name = "@PortadorUFVeiculo";
			procParams[13].Type = System.Data.SqlDbType.VarChar;
			procParams[13].Data = ((portadorUFVeiculo.Length > 0) ? (Object)portadorUFVeiculo : System.DBNull.Value);
            procParams[13].Length = 2;

            procParams[14].Name = "@TransportadoraNome";
			procParams[14].Type = System.Data.SqlDbType.VarChar;
			procParams[14].Data = ((transportadoraNome.Length > 0) ? (Object)transportadoraNome : System.DBNull.Value);
            procParams[14].Length = 40;

            procParams[15].Name = "@TransportadoraContato";
			procParams[15].Type = System.Data.SqlDbType.VarChar;
			procParams[15].Data = ((transportadoraContato.Length > 0) ? (Object)transportadoraContato : System.DBNull.Value);
            procParams[15].Length = 40;

            procParams[16].Name = "@TransportadoraDDD";
			procParams[16].Type = System.Data.SqlDbType.VarChar;
            procParams[16].Data = System.DBNull.Value;
            procParams[16].Length = 4;

            procParams[17].Name = "@TransportadoraTelefone";
			procParams[17].Type = System.Data.SqlDbType.VarChar;
			procParams[17].Data = ((transportadoraTelefone.Length > 0) ? (Object)transportadoraTelefone : System.DBNull.Value);
            procParams[17].Length = 9;

            procParams[18].Name = "@AnteciparEntrega";
            procParams[18].Type = System.Data.SqlDbType.Bit;
            procParams[18].Data = false;

            procParams[19].Name = "@GerarPedido";
			procParams[19].Type = System.Data.SqlDbType.Bit;
			procParams[19].Data = ((gerarPedido == "1") ? true : false);

            procParams[20].Name = "@Usuario2ID";
            procParams[20].Type = System.Data.SqlDbType.Int;
            procParams[20].Data = usuario2ID.Length > 0 ? (Object)usuario2ID : System.DBNull.Value;

            procParams[21].Name = "@SacEntregaID";
            procParams[21].Type = System.Data.SqlDbType.Int;
            procParams[21].Data = System.DBNull.Value;
            procParams[21].Direction = ParameterDirection.Output;

            procParams[22].Name = "@PedidoID";
			procParams[22].Type = System.Data.SqlDbType.Int;
			procParams[22].Data = System.DBNull.Value;
            procParams[22].Direction = ParameterDirection.Output;

            procParams[23].Name = "@Resultado";
			procParams[23].Type = System.Data.SqlDbType.VarChar;
			procParams[23].Data = "";
			procParams[23].Direction = ParameterDirection.Output;
            procParams[23].Length = 8000;


			DataInterfaceObj.execNonQueryProcedure("sp_SacolasComprasEntrega_Gerador", procParams);

            // Gera o resultado para o usuario.
            if (procParams[21].Data != DBNull.Value)
                sacEntregaID = (int)procParams[21].Data;

            if (procParams[22].Data != DBNull.Value)
                pedidoID = (int)procParams[22].Data;

            result = (procParams[23].Data != DBNull.Value) ? (string)procParams[23].Data + (char)13 + (char)10 : "";
            
            return (result != null || result.Length > 0) ? result : " NULL ";
		}
		            
        protected override void PageLoad(object sender, EventArgs e)
        {
			string Mensagem = ListaPrecosComprar();
			
			// Gera o resultado para o usuario.
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select '" + Mensagem + "' as Mensagem, " + sacEntregaID.ToString() + " AS SacEntregaID, " + pedidoID.ToString() + " AS PedidoID "
				)
			);
		}
    }
}