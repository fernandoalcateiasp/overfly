using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.serversidegenEx
{
    public partial class imagemrelatorio : System.Web.UI.OverflyPage
    {
		private Integer formId;
		private Integer subFormId;
		private Integer conceitoId;

		public Integer nFormID
		{
			set { formId = value; }
		}
		public Integer nSubFormID
		{
			set { subFormId = value; }
		}
		public Integer nConceitoID
		{
			set { conceitoId = value; }
		}

        protected override void PageLoad(object sender, EventArgs e)
        {
            // Gera o resultado.
            WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT dbo.fn_Imagem_Resumo(" + formId + ", " +
					subFormId + ", " + conceitoId + ") AS ImagemResumo"
				)
			);
        }
    }
}
