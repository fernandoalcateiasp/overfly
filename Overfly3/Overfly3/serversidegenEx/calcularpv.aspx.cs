﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.serversidegenEx
{
    public partial class calcularpv : System.Web.UI.OverflyPage
    {
        protected static Integer ZERO = new Integer(0);

        protected Integer DataLen = ZERO;
        protected Integer nDataLen
        {
            get { return DataLen; }
            set { if (value != null) DataLen = value; }
        }

        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer [] Produtos;

        public Integer [] nProdutoID
        {
            get { return Produtos; }
            set { Produtos = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            for (int i = 0; i < DataLen.intValue(); i++)
            {
                ProcedureParameters[] procParams = new ProcedureParameters[2];

                procParams[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                    EmpresaID.ToString());

                procParams[1] = new ProcedureParameters(
                    "@ProdutoID",
                    System.Data.SqlDbType.Int,
                   Produtos[i].intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(Produtos[i].ToString()));

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Produto_PrevisaoVendas",
                    procParams);
            }

            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT 'Previsão de vendas OK' AS Resultado "));
        }
    }
}