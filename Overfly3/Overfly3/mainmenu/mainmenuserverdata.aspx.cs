﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Overfly3.mainmenu
{
    public partial class mainmenuserverdata : System.Web.UI.OverflyPage
    {
        protected override void PageLoad(object sender, EventArgs e)
        {
            string SQL = "";
            SQL = "SELECT Tabela, Coluna, Tipo, CAST(ColumnDescription AS INTEGER) AS [Size] " +
                "FROM dbo.Tabelas_Estrutura WITH(NOLOCK) " +
                "WHERE (UPPER(Tipo) = 'INT') OR (UPPER(Tipo) = 'BIGINT') " +
                "AND Deletado = 0 " +
                "ORDER BY Tabela, Coluna ASC";
            WriteResultXML(DataInterfaceObj.getRemoteData(SQL));
        }
    }
}