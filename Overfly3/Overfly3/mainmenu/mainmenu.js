/********************************************************************
mainmenu.js

Library javascript para o mainmenu.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var __ELEMSTATE = new Array();  //array: controlId, state
var __ELEMCOUNT = 0;

// O botao de e-mails
var __BTN_EMAILS = 14;

// Variavel de imagem
var logoImg = new Image();

// Array para campo int e bigint
var glb_intFields = new Array();

// Array da posicao da primeira letra dos nomes das tabelas
// no array glb_intFields
var glb_intFieldsPos = new Array();

var glb_pageEMailsLoaded = false;

var glb_STRCONN = null;

var dspDataSpace = new ActiveXObject('RDS.DataSpace');

var overflySrvCfg = dspDataSpace.CreateObject('OverflySvrCfg.OverflyMTS',
                                              SYS_DATABASEDOMINIUM);

// Os botoes rapidos (fast btns)
var __i;
// var __NUMMAXFASTBTNS = 10;
var __NUMMAXFASTBTNS = 17;
var glb_aFastBtnsData = new Array();

for (__i = 0; __i < __NUMMAXFASTBTNS; __i++) {
    glb_aFastBtnsData.length = (__i + 1);

    // Composicao do array
    // [0] = btn.value
    // [1] = btn.title
    // [2] = formName a chamar
    // [3] = btnLeftRightOrdem
    glb_aFastBtnsData[__i] = new Array(null, null, null, null);
}

var glb_MainMenuTimer = null;
var dsoFldsTbls = new CDatatransport('dsoFldsTbls');
var dsoGen01 = new CDatatransport('dsoGen01');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
LISTA DAS FUNCOES

window_onload()
window_onunload()
wndJSProc(idElement, msg, param1, param2)
adjustTreeView()
adjustLogo()
remountNavTreeview(empresaID)
dsoGen01_treeviewdata_DSC()
setupGridInterface(grid)
__selFieldContent(theField)
lockControls(cmdLock)
loadStatusBarMain()
loadFastButtonsMain()
mainBrowserUserLoaded()
adjustElement(elem, cmdLock)
adjustActiveXControl(elem, cmdLock)
__loadForm(formName)
dsoGen01_loadForm_DSC()
treatsFastButtonsBar()
showEMailInterface()
showLogoInterface(initEMails)
fillArrayFastBtnsData()

********************************************************************/

// Precarrega imagem
logoImg.src = imgPath + '/aguia.jpg';

// IMPLEMENTACAO DAS FUNCOES

function fg_MainMenuDblClick() {
    var node = fg.GetNode();

    sendJSMessage('statusbarmaindHtml', JS_MSGRESERVED, 'GETEMPRESATOSTARTFORM', null);

    if (fg.TextMatrix(fg.Row, 1) != '')
        __loadForm(fg.TextMatrix(fg.Row, 1));
    else
        node.Expanded = !node.Expanded;

}

/********************************************************************
Descritivo da interface
1. Nome da interface = 'USERMAINBROWSER'
2. Componentes:
Composicao da interface:
html/asp              bin  hex    frame               observacao
--------              ---  ---    -----               ----------
mainmenu.aspx          001  0X1    frameMainMenu
statusbarmain.asp     010  0X2    frameStatusBar
fastbuttonsmain.asp   100  0X4    frameFastButtons

toda interface        111  0X7
********************************************************************/

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    if (glb_DeveloperInHotBase) {
        var confirmaBaseQuente = window.confirm("C U I D A D O - VOCE ESTA NA BASE QUENTE. Deseja continuar?");
        if (!confirmaBaseQuente) {
            window.top.close();
            return null;
        }
    }

    var elem;

    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');

    if (elem != null)
        elem.className = 'fldGeneral';

    elem.style.visibility = 'hidden';

    setupGridInitialInterface(elem);

    elem.style.visibility = 'inherit';

    // Ze em 17/03/08
    dealWithObjects_Load();
    dealWithGrid_Load();
    // Fim de Ze em 17/03/08

    // glb_STRCONN = overflySrvCfg.DataBaseStrConn(window.top.__APP_NAME__);

    // inicia o controle do carregamento de interface
    initInterfaceLoad('USERMAINBROWSER', 0X7);

    // esconde todos os frames
    showAllFramesInHtmlTop(false);

    // ajusta o TreeView
    adjustTreeView();

    // ajusta o logotipo
    adjustLogo();

    // ajusta o body do html
    with (mainmenuBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // este arquivo asp carregou
    //sendJSMessage('mainmenuHtml', JS_PAGELOADED, new Array(window.top.name.toString(), 'USERMAINBROWSER'), 0X1);

    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();

    dsoFldsTbls.URL = SYS_ASPURLROOT + '/mainmenu/mainmenuserverdata.aspx';
    dsoFldsTbls.ondatasetcomplete = thisPageWasLoaded;
    dsoFldsTbls.Refresh();

    // Translada interface pelo dicionario do sistema
    translateInterface(window, null);

    glb_MainMenuTimer = window.setInterval('loadMoreInterfaces()', 10, 'JavaScript');
}

function loadMoreInterfaces() {
    if (glb_MainMenuTimer != null) {
        window.clearInterval(glb_MainMenuTimer);
        glb_MainMenuTimer = null;
    }

    // carrega a interface do browser mae do usuario
    loadStatusBarMain();
    loadFastButtonsMain();
}

function thisPageWasLoaded() {
    // RETORNA O NUMERO DE CARACTERES DOS CAMPOS DO BANCO DE DADOS
    // Nas bibliotecas Java usar:
    // var nRet = sendJSMessage('', JS_FIELDINTLEN, NomeDaTabela, NomeDoCampo);

    var i = 0;
    var sTabela_Int, sColuna_Int, sTipo_Int, nSize_Int;

    // Atencao para uso de indice e nao nome de campo no loop.
    // Qualquer mudanca da ordem dos campos que retornam do
    // SELECT, revisar o loop abaixo.
    while (!dsoFldsTbls.recordset.EOF) {
        sTabela_Int = '';
        sColuna_Int = '';
        sTipo_Int = '';
        nSize_Int = 0;

        if (dsoFldsTbls.recordset.Fields[0].value != null)
            sTabela_Int = dsoFldsTbls.recordset.Fields[0].value.toUpperCase();

        if (dsoFldsTbls.recordset.Fields[1].value != null)
            sColuna_Int = dsoFldsTbls.recordset.Fields[1].value.toUpperCase();

        if (dsoFldsTbls.recordset.Fields[2].value != null)
            sTipo_Int = dsoFldsTbls.recordset.Fields[2].value.toUpperCase();

        if (dsoFldsTbls.recordset.Fields[3].value != null)
            nSize_Int = dsoFldsTbls.recordset.Fields[3].value;
        else {
            if (sTipo_Int == "INT")
                nSize_Int = 10;

        }

        glb_intFields[i] = new Array(sTabela_Int, sColuna_Int, sTipo_Int, nSize_Int);

        dsoFldsTbls.recordset.MoveNext();

        i++;
    }

    mountArrayFirstLetterTablePos();

    sendJSMessage('mainmenuHtml', JS_PAGELOADED, new Array(window.top.name.toString(), 'USERMAINBROWSER'), 0X1);
}

/********************************************************************
Setup de saida da janela
********************************************************************/
function window_onunload() {
    dealWithObjects_Unload();

    // descarrega a pagina de e-mails
    // Oculta o frame de nova senha e descarrega a pagina 
    var frameRef = getFrameInHtmlTop('frameGen01');
    showFrameInHtmlTop(frameRef, false);
    moveFrameInHtmlTop(frameRef, new Array(0, 0, 0, 0));
    //frameRef.src = '';
    //top.frames(frameRef.name).document.location.replace('');
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2) {
    switch (msg) {
        case JS_FIELDINTLEN:
            {
                //var nRet = sendJSMessage('SUP_HTML', JS_FIELDINTLEN, 'abc', 'def');
                //alert(nRet);

                //alert(param1 + '\n' + param2);
                //return 123;

                return intFieldDescription(param1, param2);
            }
            break;

        case JS_PAGELOADED:
            {
                if (param1[0].toUpperCase() == window.top.name.toUpperCase()) {
                    if (param1[1].toUpperCase() == getInterfaceName()) {
                        if (componentOfInterfaceLoaded(param2) == true) {
                            // a interface terminou de carregar
                            // a funcao abaixo faz os ajustes finais da
                            // interface e exibe a mesma para o usuario
                            mainBrowserUserLoaded();
                        }
                    }
                }
            }
            break;

        case JS_CHILDBROWSERLOADED:
            {
                // aqui retorna do browser filho apos seu carregamento total
                // (browser + interface do browser + form)
                // param1 = array: window top name do browser filho, arqStart e frameStart

                // destrava este form
                lockControls(false);

                // destrava o status bar
                sendJSMessage('AnyHtml', JS_STBARGEN, 'CMBSTATUS', false);

                // destrava a barra de botoes acesso rapido
                unlockFastButtons();

                return true;
            }
            break;

            // botao clicado na barra de botoes de acesso rapido 
            // param1 e o id do botao 
            // param2 e o nome do form 
        case JS_FASTBUTTONCLICKED:
            {
                __loadForm(param2);
                return true;
            }
            break;

            // combo de empresas mudou a selecao 
            // param1 == novo EmpresaID 
        case JS_COMBOSELCHANGE:
            {
                if (idElement.toUpperCase() == 'STATUSBARMAINHTML') {
                    // volta interface para logo
                    showLogoInterface(true);

                    // remonta o tree view de navegacao
                    remountNavTreeview(param1);
                    return true;
                }
            }
            break;

            // String de conexao  
            // retorno de sendJSMessage(getHtmlId(), JS_WIDEMSG, JS_STRCONN, null); 
        case JS_WIDEMSG:
            {
                if (param1 == JS_STRCONN)
                    return glb_STRCONN;
            }
            break;

            // Pagina de emails carregou no frame frameGen01 
            // Usuario clicou o botao de e-mail no browser mae 
        case JS_MSGRESERVED:
            {
                if (param1 == 'LOADFORMFROMFORM') {
                    __loadForm(param2);
                    return true;
                }
                // Pagina de emails carregou no frame frameGen01
                if ((param1 == 'PAGE_COLABORADORES') && (param2 == 'LOADED')) {
                    glb_pageEMailsLoaded = true;
                    //setupFastButton(__BTN_EMAILS, new Array(true, true, 'Ferramentas', 'Ferramentas'), null);

                    return 0;
                }
                // Usuario clicou o botao de e-mail no browser mae
                if ((param1 == 'BTN_FAST_EMAIL') && (param2 == 'MAIN')) {
                    showEMailInterface();

                    return 0;
                }
                // Usuario clicou o botao btnCanc da pagina de e-mails
                // Pagina de emails carregou no frame frameGen01
                if ((param1 == 'PAGE_COLABORADORES') && (param2 == 'CANC')) {
                    showLogoInterface();
                    return 0;
                }
            }

        default:
            return null;
    }
}

/********************************************************************
Configura o treeview
********************************************************************/
function adjustTreeView() {
    divTreeView.scroll = 'no';
    with (divTreeView.style) {
        backgroundColor = 'transparent';
        top = ELEM_GAP;
        left = ELEM_GAP;
        width = MAX_FRAMEWIDTH / 3;
        height = 528;
        visibility = 'visible';
    }

    // ajusta o grid
    with (fg.style) {
        top = 0;
        left = 0;
        width = parseInt(divTreeView.currentStyle.width, 10);
        height = parseInt(divTreeView.currentStyle.height, 10) - 2;
    }

    setupGridInterface(fg);
}

/********************************************************************
Configura o logotipo
********************************************************************/
function adjustLogo() {
    with (imgLogo) {
        style.backgroundColor = 'transparent';

        style.top = ELEM_GAP;
        style.left = parseInt(divTreeView.currentStyle.left) +
                     parseInt(divTreeView.currentStyle.width) + ELEM_GAP;

        //style.width = 500;
        //style.height = 384;

        style.width = 635;
        //style.height = 488;
        style.height = 525;

        style.cursor = 'hand';
        title = 'Clique duplo';

        src = logoImg.src;
        ondblclick = imgLogo_ondblclick;
    }
}

function imgLogo_ondblclick() {
    if (!fastButtonsIsLocked())
        sendJSMessage('fastbuttonsmainHtml', JS_MSGRESERVED, 'BTN_FAST_EMAIL', 'MAIN');
}

/********************************************************************
Remonta o treeview
********************************************************************/
function remountNavTreeview(empresaID) {
    var i, j;

    // trava este form    
    lockControls(true);

    // trava o status bar
    sendJSMessage('AnyHtml', JS_STBARGEN, 'CMBSTATUS', true);

    // trava a barra de botoes de acesso rapido
    lockFastButtons();

    // zera array glb_TVLine
    for (i = 0; i < glb_TVLine.length; i++) {
        for (j = 0; j < glb_TVLine[i].length; j++)
            glb_TVLine[i][j] = null;

        //glb_TVLine[i] = null;    
    }

    // obtem dados basicos do form do lado do servidor
    var strPars = new String();
    strPars = '?';
    strPars += 'userID=';
    strPars += escape((window.top.glb_CURRUSERID).toString());
    strPars += '&empresaID=';
    strPars += escape(empresaID);

    dsoGen01.URL = SYS_ASPURLROOT + '/serversidegenEx/treeviewdata.aspx' + strPars;
    dsoGen01.ondatasetcomplete = dsoGen01_treeviewdata_DSC;

    dsoGen01.refresh();

}

/********************************************************************
Retorno do servidor com dados do treview, em funcao da empresa
********************************************************************/
function dsoGen01_treeviewdata_DSC() {
    var i;
    var numRecs = 0;

    // redimensiona o array glb_TVLine se necessario
    if (!(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF)) {
        dsoGen01.recordset.MoveLast();
        numRecs = dsoGen01.recordset.RecordCount();
        dsoGen01.recordset.MoveFirst();

        if (numRecs > glb_TVLine.length) {
            for (i = glb_TVLine.length; i < numRecs; i++)
                glb_TVLine[i] = new Array('', '', '', '', '', -1, -1);
        }
    }

    // preenche o array glb_TVLine
    i = 0;
    while (!dsoGen01.recordset.EOF) {
        if (dsoGen01.recordset.Fields['Modulo'].value != null)
            glb_TVLine[i][0] = dsoGen01.recordset.Fields['Modulo'].value;

        if (dsoGen01.recordset.Fields['SubModulo'].value != null)
            glb_TVLine[i][1] = dsoGen01.recordset.Fields['SubModulo'].value;

        if (dsoGen01.recordset.Fields['FormTitle'].value != null)
            glb_TVLine[i][2] = dsoGen01.recordset.Fields['FormTitle'].value;

        if (dsoGen01.recordset.Fields['FormName'].value != null)
            glb_TVLine[i][3] = dsoGen01.recordset.Fields['FormName'].value;

        if (dsoGen01.recordset.Fields['BtnValue'].value != null)
            glb_TVLine[i][4] = dsoGen01.recordset.Fields['BtnValue'].value;

        if (dsoGen01.recordset.Fields['BtnOrdem'].value != null)
            glb_TVLine[i][5] = dsoGen01.recordset.Fields['BtnOrdem'].value;

        if (dsoGen01.recordset.Fields['btnLeftRightOrdem'].value != null)
            glb_TVLine[i][6] = dsoGen01.recordset.Fields['btnLeftRightOrdem'].value;

        dsoGen01.recordset.MoveNext();
        i++;
    }

    fg.Redraw = 0;
    setupGridInterface(fg);
    fg.Redraw = 2;

    // destrava a interface
    // destrava este form
    lockControls(false);

    // destrava o status bar
    sendJSMessage('AnyHtml', JS_STBARGEN, 'CMBSTATUS', false);

    // destrava a barra de botoes acesso rapido
    unlockFastButtons();

    // libera os botoes de acesso rapido conforme o tree view
    // (montado pelos direitos do usuario)
    treatsFastButtonsBar();
}

/********************************************************************
Funcao complementar de configuracao do treeview
********************************************************************/
function setupGridInitialInterface(grid) {
    var i;

    var sSpace = " ";

    for (i = 0; i < 50; i++) {
        sSpace += " ";
    }

    with (grid) {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '8';
        Editable = false;
        AllowUserResizing = 1;
        VirtualData = true;
        Ellipsis = 1;
        SelectionMode = 0;
        AllowSelection = false;
        Rows = 1;
        Cols = 2;
        FixedRows = 1;
        FixedCols = 0;
        ScrollBars = 3;
        OutLineBar = 5;
        TreeColor = 0X000000;
        GridLines = 0;
        FormatString = translateTerm('Menu do Sistema', null) + sSpace;
        ColHidden(1) = true;
        Appearance = 1;
        BorderStyle = 1;
        //ColDataType(1) = 11;
    }
}

/********************************************************************
Funcao complementar de configuracao do treeview
********************************************************************/
function setupGridInterface(grid) {
    var i;
    var previousMod = '';
    var previousSubMod = '';

    setupGridInitialInterface(fg);

    // Popula o grid com o array glb_TVLine
    // glb_TVLine[i][0] - Modulo
    // glb_TVLine[i][1] - SubModulo
    // glb_TVLine[i][3] - FormTitle
    // glb_TVLine[i][4] - FormName (coluna escondida)

    for (i = 0; i < glb_TVLine.length; i++) {
        if (glb_TVLine[i][0] == null)
            break;

        // Modulo
        if (previousMod != glb_TVLine[i][0]) {
            grid.AddItem(glb_TVLine[i][0]);
            grid.IsSubTotal(grid.Rows - 1) = true;
            grid.RowOutlineLevel(grid.Rows - 1) = 0;
            previousMod = glb_TVLine[i][0];
            grid.Row = grid.Rows - 1;
            grid.CellFontBold = true;
            grid.CellFontSize = 10;
        }
        // SubModulo
        if ((previousSubMod != glb_TVLine[i][1]) && (glb_TVLine[i][1] != '')) {
            grid.AddItem(glb_TVLine[i][1]);
            grid.IsSubTotal(grid.Rows - 1) = true;
            grid.RowOutlineLevel(grid.Rows - 1) = 1;
            previousSubMod = glb_TVLine[i][1];
            grid.Row = grid.Rows - 1;
            grid.CellFontBold = true;
            grid.CellFontSize = 8;
        }
        // FormTitle e FormName
        if (glb_TVLine[i][2] != '') {
            grid.AddItem(glb_TVLine[i][2] + '\t' + glb_TVLine[i][3]);
            grid.Row = grid.Rows - 1;
            grid.CellFontBold = false;
            grid.CellFontSize = 8;
            grid.CellForeColor = 0XFF0000;
        }
    }

    if (grid.Rows > 1)
        grid.Row = 1;

    for (i = 0; i < grid.Rows; i++) {
        if (grid.IsSubTotal(i) == true)
            grid.IsCollapsed(i) = 2;
    }
}

/********************************************************************
Seleciona conteudo de campo texto
********************************************************************/
function __selFieldContent(theField) {
    theField.select();
}

/********************************************************************
Esta funcao habilita/desabilita todos os controles do html/asp corrente.

Parametros:
cmdLock         - true (desabilita) / false (habilita)

Retornos:
Nenhum
********************************************************************/
function lockControls(cmdLock) {
    __ELEMCOUNT = 0;
    var i;
    var elem;

    try {
        for (i = 0; i < window.document.all.length; i++) {
            elem = window.document.all.item(i);

            // controles ActiveX
            // o grid
            if (elem.tagName.toUpperCase() == 'OBJECT') {
                if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072') {
                    // grid
                    adjustActiveXControl(elem, cmdLock);
                }
            }

            // demais elementos        
            if (elem.tagName.toUpperCase() == 'INPUT')
                adjustElement(elem, cmdLock);
            else if (elem.tagName.toUpperCase() == 'SELECT')
                adjustElement(elem, cmdLock);
            else if (elem.tagName.toUpperCase() == 'TEXTAREA')
                adjustElement(elem, cmdLock);
            else if (elem.tagName.toUpperCase() == 'A')
                adjustElement(elem, cmdLock);
        }
    }
    catch (e) {
        ;
    }
}

/********************************************************************
Esta funcao carrega o statusbar do browser mae do usuario ja logado.

O statusbar:
recebe o nome do sistema
carrega o combo de empresas do usuario
recebe o nome do usuario
indica que esta no menu principal do usuario
indica a operacao possivel - Selecionar

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function loadStatusBarMain() {
    sendJSMessage('mainmenuHtml', JS_PAGELOAD, new Array(window.top.name.toString(), 'frameStatusBar', SYS_PAGESURLROOT + '/statusbar/statusbarmain.aspx'), null);
}

/********************************************************************
Esta funcao carrega a botoeira de acesso rapido do browser mae
do usuario ja logado.
Carrega os botoes de acesso rapido que o usuario tem direito

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function loadFastButtonsMain() {
    sendJSMessage('mainmenuHtml', JS_PAGELOAD, new Array(window.top.name.toString(), 'frameFastButtons', SYS_PAGESURLROOT + '/fastbuttons/fastbuttonsmain.asp'), null);
}

/********************************************************************
Esta funcao recebe o indicacao de final de carregamento da interface.
Nela � feita a configuracao final e mostra-se a interface.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function mainBrowserUserLoaded() {
    var frameID;
    var frameDim = new Array();
    var y, i;
    // Configura statusBar
    // Titulo do programa
    writeInStatusBar('main', 'cellPrgName', SYS_NAME);
    writeInStatusBar('main', 'cellUserName', glb_userFantasyName);
    writeInStatusBar('main', 'cellForm', 'Form Principal');
    writeInStatusBar('main', 'cellMode', 'Pronto');
    // ajusta a posicao e dimensoes do statusbar
    frameID = getFrameIdByHtmlId('statusbarmainHtml');
    moveFrameInHtmlTop(frameID, new Array(0, 0, MAX_FRAMEWIDTH, 24));
    // mostra o statusbar
    showFrameInHtmlTop(frameID, true);

    // Configura os botoes de acesso rapido como desabilitados
    // e invisiveis.
    initializeAllFastBtns();

    // pega posicao e dimensoes do statusbar
    frameID = getFrameIdByHtmlId('statusbarmainHtml');
    frameDim = getRectFrameInHtmlTop(frameID);
    // ajusta a posicao e dimensoes do fastbuttons
    frameID = getFrameIdByHtmlId('fastbuttonsmainHtml');
    moveFrameInHtmlTop(frameID, new Array(0, (frameDim[1] + frameDim[3] + 1), MAX_FRAMEWIDTH, 27));
    // mostra o frame do fastbuttons
    showFrameInHtmlTop(frameID, true);

    // ajusta a posicao e dimensoes do mainmenu (esta pagina)
    // pega posicao e dimensoes do fastbuttons
    frameID = getFrameIdByHtmlId('fastbuttonsmainHtml');
    frameDim = getRectFrameInHtmlTop(frameID);
    // ajusta a posicao e dimensoes do mainmenu (esta pagina)
    //moveExtFrame(window, new Array(0, (frameDim[1] + frameDim[3] + 1), MAX_FRAMEWIDTH, (MAX_FRAMEHEIGHT - 78 - (frameDim[1] + frameDim[3] + 1))));
    moveExtFrame(window, new Array(0, (frameDim[1] + frameDim[3] + 1), MAX_FRAMEWIDTH, 539));

    // ajusta a posicao e dimensoes do frame para e-mails
    // pega posicao e dimensoes do mainmenu
    frameID = getFrameIdByHtmlId('mainmenuHtml');
    frameDim = getRectFrameInHtmlTop(frameID);
    // ajusta a posicao e dimensoes do frame para e-mails
    frameID = 'frameGen01';
    moveFrameInHtmlTop(frameID, new Array((MAX_FRAMEWIDTH / 3) + (2 * ELEM_GAP),
                                           frameDim[1],
                                           (2 * MAX_FRAMEWIDTH) / 3 - (2 * ELEM_GAP),
                                           //frameDim[3]));
                                           539));

    // ajusta a posicao e dimensoes do frame para delegar direitos
    // pega posicao e dimensoes do mainmenu
    frameID = getFrameIdByHtmlId('mainmenuHtml');
    frameDim = getRectFrameInHtmlTop(frameID);
    // ajusta a posicao e dimensoes do frame para e-mails
    frameID = 'frameGen03';
    moveFrameInHtmlTop(frameID, new Array((MAX_FRAMEWIDTH / 3) + (2 * ELEM_GAP),
                                           frameDim[1],
                                           (2 * MAX_FRAMEWIDTH) / 3 - (2 * ELEM_GAP),
                                           //frameDim[3]));
                                           539));

    // ajusta a posicao e dimensoes do frame para vale
    // pega posicao e dimensoes do mainmenu
    frameID = getFrameIdByHtmlId('mainmenuHtml');
    frameDim = getRectFrameInHtmlTop(frameID);
    // ajusta a posicao e dimensoes do frame para vale
    frameID = 'frameGen04';
    moveFrameInHtmlTop(frameID, new Array((MAX_FRAMEWIDTH / 3) + (2 * ELEM_GAP),
                                           frameDim[1],
                                           (2 * MAX_FRAMEWIDTH) / 3 - (2 * ELEM_GAP),
                                           //frameDim[3]));
                                           539));

    // carrega o frame logo e e-mails -> frameGen01 com a pagina correspondente
    loadURLInFrame('frameGen01', SYS_PAGESURLROOT + '/home/colaboradores/colaboradores.asp');

    // mostra o frame do mainmenu (esta pagina)    
    showExtFrame(window, true);

    // libera os botoes de acesso rapido conforme o tree view
    // (montado pelos direitos do usuario)
    treatsFastButtonsBar();
}

/********************************************************************
Esta funcao trava ou destrava um elemento de html/asp da interface
do browser filho corrente.

Parametros:
elem            - elemento corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustElement(elem, cmdLock) {
    // cmdLock == true => desabilitar elemento

    if (cmdLock == true) {
        // desabilita o elemento
        if ((elem.tagName.toUpperCase() == 'TEXTAREA') ||
             ((elem.tagName.toUpperCase() == 'INPUT') &&
               (elem.type.toUpperCase() == 'TEXT'))) {
            // salva status corrente
            __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.readOnly);
            elem.readOnly = true;
        }
        else {
            // salva status corrente
            __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.disabled);
            elem.disabled = true;
        }

        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        if ((elem.tagName.toUpperCase() == 'TEXTAREA') ||
             ((elem.tagName.toUpperCase() == 'INPUT') &&
               (elem.type.toUpperCase() == 'TEXT')))
            window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).readOnly = (__ELEMSTATE[__ELEMCOUNT])[1];
        else
            window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).disabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Esta funcao trava ou destrava um elemento ACTIVEX CONTROL
de html/asp da interface do browser filho corrente.

Parametros:
elem            - elemento ACTIVEX CONTROL corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustActiveXControl(elem, cmdLock) {
    // cmdLock == true => desabilitar elemento

    if (cmdLock == true) {
        // salva status corrente
        __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.Enabled);
        // desabilita o elemento
        elem.Enabled = false;
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).Enabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Esta funcao abre um browser filho e carrega um form

Parametros:
formName            - o nome do form

Retornos:
nenhum
********************************************************************/
function __loadForm(formName) {
    // trava este form    
    lockControls(true);

    // trava o status bar
    sendJSMessage('AnyHtml', JS_STBARGEN, 'CMBSTATUS', true);

    // trava a barra de botoes de acesso rapido
    lockFastButtons();

    // obtem dados basicos do form do lado do servidor
    var strPars = new String();
    strPars = '?';
    strPars += 'formName=';
    strPars += escape(formName);

    dsoGen01.URL = SYS_ASPURLROOT + '/serversidegenEx/formdata01.aspx' + strPars;
    dsoGen01.ondatasetcomplete = dsoGen01_loadForm_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Funcao complementar da funcao __loadForm(formName)

Parametros:
formName            - o nome do form

Retornos:
nenhum
********************************************************************/
function dsoGen01_loadForm_DSC() {
    if (dsoGen01.recordset.BOF && dsoGen01.recordset.EOF)
        return null;

    var arqStart;
    var formName;

    formName = dsoGen01.recordset['FormName'].value;

    arqStart = overflyRDSForms.form(
		SYS_PAGESURLROOT + dsoGen01.recordset['ArqStart'].value,
		glb_devMode);

    var frameStart = dsoGen01.recordset['FrameStart'].value;

    openForm('mainmenuHtml', window.top.name, formName, arqStart, frameStart, true);
}

/********************************************************************
Trata os botoes da barra em funcao dos direitos do usuario

Parametros:
nenhum

Retornos:
nenhum
********************************************************************/
function treatsFastButtonsBar() {
    // Inicializa os botoes
    var qtyFastBtns = initializeAllFastBtns();

    // Prepara array de dados dos botoes
    fillArrayFastBtnsData();

    var i, j;

    // se a pagina de e-mails esta carregada, habilita o botao
    if (glb_pageEMailsLoaded) {
        ;
        //setupFastButton(__BTN_EMAILS, new Array(true, true, 'Ferramentas', 'Ferramentas'), null);
    }
    else {
        ;
        //setupFastButton(__BTN_EMAILS, new Array(true, false, 'Ferramentas', 'Ferramentas'), null);
    }

    // libera os botoes de acesso rapido conforme o tree view
    // (montado pelos direitos do usuario)

    // Artificio necessario porque o array de dados foi
    // sorteado do menor para o maior elemento [3]
    // logo os elementos null estao primeiro

    j = 0;

    for (i = 0; i < glb_aFastBtnsData.length; i++) {
        if (j >= __NUMMAXFASTBTNS)
            break;

        if ((glb_aFastBtnsData[i][2] == null) ||
		      (glb_aFastBtnsData[i][2] == ''))
            continue;

        setupFastButton((j + 1), new Array(true, true, glb_aFastBtnsData[i][0], glb_aFastBtnsData[i][1]), glb_aFastBtnsData[i][2]);
        j++;
    }
}

/********************************************************************
Trata o array de dados dos botoes da barra do usuario

Parametros:
nenhum

Retornos:
nenhum
********************************************************************/
function fillArrayFastBtnsData() {
    var i = glb_aFastBtnsData.length;
    var j;
    var nFirstElemOrdem1;
    var numBtns;

    // Zera o array
    for (i = 0; i < __NUMMAXFASTBTNS; i++) {
        for (j = 0; j < glb_aFastBtnsData[i].length; j++)
            glb_aFastBtnsData[i][j] = null;
    }

    // Sorteia o array de dados usado no treeview pelos btnOrdem
    asort(glb_TVLine, 5);

    // Procura o primeiro btn de Ordem 1
    for (i = 0; i < glb_TVLine.length; i++) {
        if ((glb_TVLine[i][5] != null)) {
            if (glb_TVLine[i][5] > 0) {
                nFirstElemOrdem1 = i;
                break;
            }
        }
    }

    // O usuario tera tantos botoes quanto o que for menor:
    numBtns = Math.min(__NUMMAXFASTBTNS, glb_TVLine.length);

    j = 0;
    for (i = 0; i < numBtns; i++) {
        if (j > numBtns)
            break;

        if ((nFirstElemOrdem1 + i) > (glb_TVLine.length - 1))
            break;

        // [0] = btn.value
        // [1] = btn.title
        // [2] = formName a chamar
        // [3] = btnLeftRightOrdem
        glb_aFastBtnsData[j][0] = glb_TVLine[nFirstElemOrdem1 + i][4];
        glb_aFastBtnsData[j][1] = glb_TVLine[nFirstElemOrdem1 + i][2];
        glb_aFastBtnsData[j][2] = glb_TVLine[nFirstElemOrdem1 + i][3];
        glb_aFastBtnsData[j][3] = glb_TVLine[nFirstElemOrdem1 + i][6];

        j++;
    }
}

/********************************************************************
Mostra a interface de e-mails

Parametros:
nenhum

Retornos:
nenhum
********************************************************************/
function showEMailInterface() {
    var frameDimMainMenu;
    var frameDimEMails;

    //setupFastButton(__BTN_EMAILS, new Array(true, false, 'Ferramentas', 'Ferramentas'), null);

    // reduz frame do grid de menu
    frameDimMainMenu = getRectFrameInHtmlTop('frameMainMenu');
    frameDimEMails = getRectFrameInHtmlTop('frameGen01');

    moveExtFrame(window, new Array(frameDimMainMenu[0],
                                   frameDimMainMenu[1],
                                   frameDimEMails[0] - ELEM_GAP,
                                   frameDimMainMenu[3]));

    // esconde o logo
    imgLogo.style.visibility = 'hidden';

    // mostra o frame de e-mails
    showFrameInHtmlTop('frameGen01', true);

    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'BTN_FAST_EMAIL', 'SHOWED');
}

/********************************************************************
Mostra a interface de e-mails

Parametros:
initEMails      - se != null zera interface de e-mails

Retornos:
nenhum
********************************************************************/
function showLogoInterface(initEMails) {
    var frameDimMainMenu;
    var frameDimEMails;

    //setupFastButton(__BTN_EMAILS, new Array(true, true, 'Ferramentas', 'Ferramentas'), null);

    // aumenta frame do grid de menu
    frameDimMainMenu = getRectFrameInHtmlTop('frameMainMenu');
    frameDimEMails = getRectFrameInHtmlTop('frameGen01');

    moveExtFrame(window, new Array(frameDimMainMenu[0],
                                   frameDimMainMenu[1],
                                   frameDimEMails[0] + frameDimEMails[2],
                                   frameDimMainMenu[3]));

    // esconde o frame de e-mails
    showFrameInHtmlTop('frameGen01', false);

    // mostra o logo
    imgLogo.style.visibility = 'visible';

    if (initEMails != null)
        sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'EMAIL', 'ZERO');
}

/********************************************************************
Retorna o campo Description do array tabelas x campos int

Parametros:
param1	- nome da tabela
param2  - nome do campo

Retornos:
O campo Description
********************************************************************/
function intFieldDescription(param1, param2) {
    if ((param1 == null) || (param2 == null) || (param1 == '') || (param2 == ''))
        return -1;

    var nRet = -1;
    var i = 0;
    var iStart = 0;
    var j = glb_intFields.length;
    var firstLetter = 0;

    param1 = param1.toUpperCase();
    param2 = param2.toUpperCase();

    firstLetter = param1.charCodeAt(0);

    iStart = getFirstLetterTablePos(firstLetter);

    // Seek ASC
    for (i = iStart; i < j; i++) {
        if (glb_intFields[i][0] == param1) {
            if (glb_intFields[i][1] == param2) {
                nRet = glb_intFields[i][3];
                break;
            }
        }
    }

    return nRet;
}

/********************************************************************
Monta array da posicao da primeira letra dos nomes das tabelas
no array tabelas x campos int.

Parametros:
Nenhum

Retornos:
Nenhum
********************************************************************/
function mountArrayFirstLetterTablePos() {
    if (glb_intFields.length == 0)
        return null;

    var i = 0;
    var j = glb_intFields.length;
    var firstLetter = 0;

    for (i = 0; i < j; i++) {
        if (glb_intFields[i][0].charCodeAt(0) == firstLetter)
            continue;

        firstLetter = (glb_intFields[i][0]).charCodeAt(0);

        glb_intFieldsPos[glb_intFieldsPos.length] = new Array(firstLetter, i);
    }

    return null;
}

/********************************************************************
Obtem a posicao da primeira letra dos nomes das tabelas
no array das primeiras letras das tabelas x campos int.

Parametros:
charFirstLetter  - char a letra a obter a posicao

Retornos:
Nenhum
********************************************************************/
function getFirstLetterTablePos(charFirstLetter) {
    var i = 0;
    var j = 0;
    var nRet = 0;

    j = glb_intFieldsPos.length;

    for (i = 0; i < j; i++) {
        if (glb_intFieldsPos[i][0] == charFirstLetter) {
            nRet = glb_intFieldsPos[i][1];
            break;
        }
    }

    return nRet;
}