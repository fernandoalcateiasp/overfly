﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="mainmenu.aspx.cs" Inherits="Overfly3.mainmenu.mainmenu" %>

<%         
    string pagesURLRoot;
    string strConn;
    string ApplicationName;
    bool DevMode;

    OVERFLYSVRCFGLib.OverflyMTS objSvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();

    WSData.dataInterface oDataInterface = new WSData.dataInterface(
           System.Configuration.ConfigurationManager.AppSettings["application"]
       );
    pagesURLRoot = objSvrCfg.PagesURLRoot(
           System.Configuration.ConfigurationManager.AppSettings["application"]);

    DevMode = objSvrCfg.DevMode(System.Configuration.ConfigurationManager.AppSettings["application"]);
    strConn = objSvrCfg.DataBaseStrConn(System.Configuration.ConfigurationManager.AppSettings["application"]);

    ApplicationName = oDataInterface.ApplicationName;
%>
<%
    Response.Write("<script ID=" + (char)34 + "serverCfgVars" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
    Response.Write("\r\n");

    //Diretorio de imagens
    Response.Write("var imgPath =" + (char)39 + pagesURLRoot + "/images" + (char)39 + ";");
    Response.Write("\r\n");

    if (ApplicationName.ToString().ToUpper() == "OVERFLY3")
    {
        if ((strConn.ToUpper().Contains(("OverflyBase").ToUpper())) && (DevMode))
        {
            Response.Write("var glb_DeveloperInHotBase = true;");
            Response.Write("\r\n");
        }
        else
        {
            Response.Write("var glb_DeveloperInHotBase = false;");
            Response.Write("\r\n");
        }
    }
    else
    {
        Response.Write("var glb_DeveloperInHotBase = false;");
        Response.Write("\r\n");
    }
    Response.Write("\r\n");
    Response.Write("</script>");
%>
<html id="mainmenuHtml" name="mainmenuHtml">
<head>
    <title></title>
</head>
<%
    //Links de estilo, bibliotecas da automacao e especificas
    Response.Write("<LINK REL=" + (char)34 + "stylesheet" + (char)34 + " HREF=" + (char)34 + pagesURLRoot + "/mainmenu/mainmenu.css" + (char)34 + "type=" + (char)34 + "text/css" + (char)34 + ">"); Response.Write("\r\n");

    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_sysbase.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_constants.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_htmlbase.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_strings.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_interface.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_statusbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_fastbuttonsbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_controlsbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_modalwin.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/formlibs/js_gridsex.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_rdsfacil.js" + (char)34 + "></script>"); Response.Write("\r\n");

    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/Defines.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CRecordsetParser.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CDatatransportSystem.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CReturnParam.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CFieldStructure.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CField.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CFields.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CRecordset.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CDatatransport.js" + (char)34 + "></script>"); Response.Write("\r\n");
    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CReturnParam.js" + (char)34 + "></script>"); Response.Write("\r\n");

    Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/mainmenu/mainmenu.js" + (char)34 + "></script>"); Response.Write("\r\n");                
%>
<%
    Response.Write("\r\n");
    Response.Write("<script ID=" + (char)34 + "globalVars" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
    Response.Write("\r\n");

    // Variavel de Desenvolvimento.

    if (DevMode)
    {
        Response.Write("var glb_devMode = true;");
    }
    else
    {
        Response.Write("var glb_devMode = false;");
    }

    // Libera o objeto.    
    //Set objSvrCfg = Nothing
    // IP do usuário logado.
    Response.Write("var glb_UserIP = '" + Request["REMOTE_ADDR"].ToString() + "';");

    //O nome fantasia do usuario logado
    Response.Write("var glb_userFantasyName = null;");
    Response.Write("\r\n");

    Usuario = DataInterfaceObj.getRemoteData("SELECT Fantasia FROM Pessoas WITH(NOLOCK) " +
                                         "WHERE PessoaID=" + Session["userID"].ToString());

    for (int i = 0; Usuario != null && i < Usuario.Tables[1].Rows.Count; i++)
    {
        Response.Write("glb_userFantasyName = '" + Usuario.Tables[1].Rows[i]["Fantasia"].ToString() + "';");
    }

    Usuario = DataInterfaceObj.getRemoteData("SELECT DISTINCT DM.Recurso AS Modulo, DSM.Recurso AS SubModulo, " +
         "DF.Recurso AS FormTitle,DF.FormName AS FormName, DM.RecursoID, DSM.RecursoID, DF.RecursoID AS btnLeftRightOrdem, " +
         "DF.NomeBotao AS btnValue, ISNULL(DF.Ordem, 99) AS btnOrdem " +
     "FROM RelacoesPesRec a WITH(NOLOCK) " +
            "INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON a.RelacaoID=b.RelacaoID " +
            "INNER JOIN dbo.vw_Direitos_Form DF with(noexpand) ON DF.PerfilID = b.PerfilID " +
            "INNER JOIN dbo.vw_Direitos_SubModulo DSM with(noexpand) ON DSM.PerfilID = b.PerfilID AND DSM.RecursoID = DF.RecursoMaeID " +
            "INNER JOIN dbo.vw_Direitos_Modulo DM with(noexpand) ON DM.PerfilID = b.PerfilID AND DM.RecursoID = DSM.RecursoMaeID " +
            "INNER JOIN dbo.vw_Direitos_Overfly DO with(noexpand) ON DO.PerfilID = b.PerfilID AND DO.RecursoID = DM.RecursoMaeID " +
            "INNER JOIN Recursos l WITH(NOLOCK) ON b.PerfilID = l.RecursoID " +
     "WHERE a.SujeitoID IN ((SELECT " + Session["userID"].ToString() + " " +
                                "UNION ALL " +
                            "SELECT UsuarioDeID " +
                                "FROM DelegacaoDireitos WITH(NOLOCK) " +
                                "WHERE (UsuarioParaID = " + Session["userID"].ToString() + "  AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
            "AND a.ObjetoID=999 " +
            "AND a.TipoRelacaoID=11 " +
            "AND b.EmpresaID = a.EmpresaDefaultID " +
            "AND (DF.Consultar1=1 OR DF.Consultar2=1) " +
            "AND (DSM.Consultar1=1 OR DSM.Consultar2=1) " +
            "AND (DM.Consultar1=1 OR DM.Consultar2=1) " +
            "AND (DO.Consultar1=1 OR DO.Consultar2=1) " +
            "AND l.EstadoID = 2 " +
     "ORDER BY DM.RecursoID,DSM.RecursoID,DF.RecursoID");

    Response.Write("\r\n");
    Response.Write("var glb_TVLine = new Array();");
    Response.Write("\r\n");

    for (int i = 0; Menu != null && i < Menu.Tables[1].Rows.Count; i++)
    {
        string modulo = "";
        string subModulo = "";
        string formTitle = "";
        string formName = "";
        string btnValue = "";
        string btnOrdem = "-1";
        string btnLeftRightOrdem = "-1";

        if (Menu.Tables[1].Rows[i]["Modulo"] != null)
        {
            modulo = Menu.Tables[1].Rows[i]["Modulo"].ToString();
        }

        if (Menu.Tables[1].Rows[i]["SubModulo"] != null)
        {
            subModulo = Menu.Tables[1].Rows[i]["SubModulo"].ToString();
        }

        if (Menu.Tables[1].Rows[i]["FormTitle"] != null)
        {
            formTitle = Menu.Tables[1].Rows[i]["FormTitle"].ToString();
        }

        if (Menu.Tables[1].Rows[i]["FormName"] != null)
        {
            formName = Menu.Tables[1].Rows[i]["FormName"].ToString();
        }

        if (Menu.Tables[1].Rows[i]["BtnValue"] != null)
        {
            btnValue = Menu.Tables[1].Rows[i]["BtnValue"].ToString();
        }

        if (Menu.Tables[1].Rows[i]["btnOrdem"] != null)
        {
            btnOrdem = Menu.Tables[1].Rows[i]["btnOrdem"].ToString();
        }

        if (Menu.Tables[1].Rows[i]["btnLeftRightOrdem"] != null)
        {
            btnLeftRightOrdem = Menu.Tables[1].Rows[i]["btnLeftRightOrdem"].ToString();
        }

        Response.Write("\r\n");
        Response.Write("glb_TVLine[" + i + "] = new Array(");
        Response.Write("'" + modulo + "', ");
        Response.Write("'" + subModulo + "', ");
        Response.Write("'" + formTitle + "', ");
        Response.Write("'" + formName + "', ");
        Response.Write("'" + btnValue + "', ");
        Response.Write(btnOrdem + ", ");
        Response.Write(btnLeftRightOrdem + ");");
        Response.Write("\r\n");
    }
    Response.Write("\r\n");
    Response.Write("</script>");
    Response.Write("\r\n");
%>
<script id="wndJSProc" language="javascript">
<!--
    //-->
</script>
<script language="javascript" for="fg" event="DblClick">
<!--
    fg_MainMenuDblClick();
    //-->
</script>
<body id="mainmenuBody" name="mainmenuBody" language="javascript" onload="return window_onload()" onunload="return window_onunload()">
    <!-- Dados campos tabela .URL  -->

    <div id="divTreeView" name="divTreeView" class="divExtern">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" class="gridTV" viewastext></object>
    </div>
    <img id="imgLogo" name="imgLogo" class="imgGeneral"></img>
</body>
</html>
