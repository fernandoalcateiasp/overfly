﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using OVERFLYSVRCFGLib;
using WSData;


namespace Overfly3.mainmenu
{
    public partial class mainmenu : System.Web.UI.OverflyPage
    {
        protected DataSet Usuario;
        protected DataSet Menu;
        protected override void PageLoad(object sender, EventArgs e)
        {

        }

        // Cria Objeto
        public static Object comCreateObject(string sProgID)
        {
            Type oType = Type.GetTypeFromProgID(sProgID);
            if (oType != null)
            {
                return Activator.CreateInstance(oType);
            }
            return null;
        }
    }
}