/********************************************************************
statemachmodal.js

Library javascript para o statemachinfmodal.asp e statemachinfmodal.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_StMachTimer = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

// LOOP DE MENSAGENS ************************************************

function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        // Mensagens do programador
        // programador mandou interceptou o botao OK e
        // madou prosseguir operacao da maquina de estado
        case JS_DATAINFORM :
        {
            if ( param1 == 'STMACH_SUP_GO' )
            {
                glb_StMachTimer = window.setInterval (
                    'stateMachGoAhead(' + '\'' + param2 + '\'' + ')' , 10, 'JavaScript');
                
                return true;    
            }    
        }
        break;
        
        default:
            return null;
    }
}

// FINAL DE LOOP DE MENSAGENS ***************************************

/********************************************************************
Processa o modal window
********************************************************************/
function btn_onclick(ctl)
{
    var i, coll, estadoIDSel, sMotivo;
    var currEstadoID = 0;

	sMotivo = '';
	
	// Zera variavel globa de motivo da troca de estado glb_sMotivoSup
	// Esta variavel so existe no sup
    if ( getHtmlId() == 'statemachsupmodal01Html' )
		sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sMotivoSup = ' + '\'' + '\'' + ';');
	
    if (ctl.id.toUpperCase() == 'BTNOK')    
    {
        // esta funcao trava o html contido na janela modal
        lockControlsInModalWin(true);

        coll = document.getElementsByTagName('INPUT');
        
        // estadoID selecionado
        for (i=0; i<coll.length; i++)
        {
            if (coll.item(i).type == 'radio')
            {
                if (coll.item(i).checked == true)        
                {
					if ( coll.item(i).getAttribute('TemMotivo', 1) == 1 )
					{
						txtMotivo.value = trimStr(txtMotivo.value);					
						
						if ( txtMotivo.value == '' )
						{
							if (window.top.overflyGen.Alert ('Digite o motivo.') == 0)
								return null;	
						
							lockControlsInModalWin(false);
							
							window.focus();
							txtMotivo.focus();
							
							return null;
						}
					}
					
					sMotivo = txtMotivo.value;
                    estadoIDSel = glb_EstadoID[parseInt(coll.item(i).id.substr(3), 10) - 1];
                }
            }
        }
        
        // interceptacao do btnOK pelo programador
        // isto ocorre apenas no statemachsupmodal01Html
        if ( getHtmlId() == 'statemachsupmodal01Html' )
        {
			// Coloca valor na variavel global de motivo da troca de estado glb_sMotivoSup
			sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sMotivoSup = ' + '\'' + sMotivo + '\'' + ';');

            // corrente estado ID do registro no xxx_superior.js
            currEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                         'dsoSup01.recordset['+ '\'' + 'EstadoID' + '\'' + '].value');
        
            // evalua funcao no xxx_superior.js
            // posterga a gravacao se o programador retorna diferente de null ou
            // grava aqui o novo estado
            if ( sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'stateMachBtnOK(' + parseInt(currEstadoID, 10) + ', ' + parseInt(estadoIDSel, 10) +' )') == false )
                sendJSMessage( getHtmlId(), JS_DATAINFORM, 'ESTIDCHANGED' , new Array (estadoIDSel, sMotivo) );
        }
        // e no statemachinfmodal01Html
        else
        {
	        // Retorna o estadoID selecionado        
            sendJSMessage( getHtmlId(), JS_DATAINFORM, 'ESTIDCHANGED' , new Array (estadoIDSel, sMotivo) );
        }    
    }
    else if (ctl.id.toUpperCase() == 'BTNCANC')    
    {
        // esta funcao fecha a janela modal e destrava a interface
        restoreInterfaceFromModal();    
        
        // a janela modal escondeu
        sendJSMessage(getHtmlId(), JS_STMACHMODAL, 'STATEMACH_HIDDEN', null);
    }    
}

/********************************************************************
Prossegue operacao da maquina de estado apos usuario interceptar
o botao OK
********************************************************************/
function stateMachGoAhead(action)
{
	var sMotivo = '';
    if ( glb_StMachTimer != null )
    {
        window.clearInterval(glb_StMachTimer);
        glb_StMachTimer = null;
    }

    if ( action == 'OK' )
    {
        var i, coll, estadoIDSel;
        
        coll = document.getElementsByTagName('INPUT');
        
        // estadoID selecionado
        for (i=0; i<coll.length; i++)
        {
            if (coll.item(i).type == 'radio')
            {
                if (coll.item(i).checked == true)
                {
					sMotivo = txtMotivo.value;                
                    estadoIDSel = glb_EstadoID[parseInt(coll.item(i).id.substr(3), 10) - 1];
				}
            }        
        }
        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'ESTIDCHANGED' , new Array(estadoIDSel, sMotivo));
    }
    else if ( action == 'CANC' )
    {
        // esta funcao fecha a janela modal e destrava a interface
        restoreInterfaceFromModal();    
        
        // a janela modal escondeu
        sendJSMessage(getHtmlId(), JS_STMACHMODAL, 'STATEMACH_HIDDEN', null);
    }
}

/********************************************************************
Processa o radio button clicado
********************************************************************/
function estado_onclick(ctl)
{
	var elem;
	
    if ( ctl == null )
		elem = window.document.activeElement;
	else
		elem = ctl;
			
    var coll, i;
    var temp;
    var bTemMotivo = false;
    
    if (elem.tagName == 'DIV')
    {
        if ( elem.name == '__lbl' )
        {
            temp = elem.id.substr(3);
            
            coll = window.document.getElementsByTagName('INPUT');
            for ( i=0; i<coll.length; i++ )
            {
                if ( coll.item(i).type == 'radio' )
                    coll.item(i).checked = false;
            }
            
            elem = 'rad' + temp;
            
            window.document.getElementById(elem).checked = true;
            
            bTemMotivo = window.document.getElementById(elem).getAttribute('TemMotivo', 1);
        }
    }
    
    if (elem.type == 'radio')
    {
        coll = window.document.getElementsByTagName('INPUT');
        for ( i=0; i<coll.length; i++ )
        {
            if ( coll.item(i).type == 'radio' )
                coll.item(i).checked = false;
        }
        
        elem.checked = true;
        
        bTemMotivo = elem.getAttribute('TemMotivo', 1);
    }
    
    enable_Disable_TxtMotivo(bTemMotivo);

}
 
/********************************************************************
Trata o campo txtMotivo e o botao OK
********************************************************************/
function enable_Disable_TxtMotivo(bTemMotivo)    
{
	if (bTemMotivo)
	{
		lblMotivo.style.visibility = 'inherit';
		txtMotivo.style.visibility = 'inherit';
		txtMotivo.disabled = false;
		
		// foca o controle txtMotivo
		putFocusInTxtMotivo();
    }
    else
    {
		lblMotivo.style.visibility = 'hidden';
		txtMotivo.style.visibility = 'hidden';
		txtMotivo.value = '';
		txtMotivo.disabled = true;
		
		// foca botao OK
		putFocusInBtnOK();    
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Altera��o de Estado', 1);

    var i, elem;
    var bRadiosChecked = false;

    // cria mensagem se nao tem estados
    if ( glb_EstadoID.length == 0 )
    {
        // desabilita o botao OK
        btnOK.disabled = true;
        
        // cria campo de mensagem
        createFldMsg();
        
        // posiciona os elementos na janela e
		// redimensiona e reposiciona a mesma
		finishAdjustWindowForMsg();
        
        return null;
    }
	else
	{
		// cria os radios e labels para os estados
		createFldsState();
	
		// Seleciona o primeiro radio se nenhum esta selecionado
		coll = window.document.getElementsByTagName('INPUT');
	
		for ( i=0; i<coll.length; i++ )
		{
		    if ( coll.item(i).type == 'radio' )
		        if ( coll.item(i).checked == true )
		            bRadiosChecked = true;
		}
    
		if ( (bRadiosChecked == false) && (coll.length > 0) )
		{
		    for ( i=0; i<coll.length; i++ )
		    {
		        if ( coll.item(i).type == 'radio' )
		        {
		            coll.item(i).checked = true;
		            break;
		        }        
		    }
		}
		
		// posiciona os elementos na janela e
		// redimensiona e reposiciona a mesma
		finishAdjustWindowForStates();
    }
}

function createFldMsg()
{
	var elem;
	
	elem = document.createElement('P');
	elem.name = '__lbl';
	elem.id = '__lbl';
	elem.innerText = 'Opera��o n�o liberada';
	elem.className = 'paraNormal';
	elem.style.height = '18px';
	elem.style.width = (elem.innerText).length * FONT_WIDTH;
	elem.style.color = 'black';
	elem.style.backgroundColor = 'transparent';
	elem.style.fontSize = '10pt'; 
	elem.style.textAlign = 'center';
	elem.style.cursor = 'default';
	        
	// acrescenta os elementos
	window.document.body.appendChild(elem);
}

function createFldsState()
{
	var i;
	var elem, elem1;
	
	// cria os radios buttons e seus labels
	for (i=0; i <glb_EstadoID.length; i++)
	{
	    elem = document.createElement('INPUT');
	    elem.type = 'radio';
	    elem.name = 'rad';
	    elem.className = 'btns';
	    elem.style.width = '10pt';
	    elem.style.height = '10pt';
	    elem.style.backgroundColor = 'transparent';
	    //elem.checked = true;// glb_EstadoIDDefault[i];
	    elem.onclick = estado_onclick;
		    
	    elem.setAttribute('TemMotivo', glb_TemMotivo[i], 1);
		    
	    elem.id = 'rad' + (i + 1 ).toString();
		    
	    elem1 = document.createElement('DIV');
	    elem1.name = '__lbl';
	    elem1.className = 'paraNormal';
	    elem1.style.height = '10pt';
	    elem1.style.color = 'black';
	    elem1.style.backgroundColor = 'transparent';
	    elem1.style.fontSize = '10pt'; 
	    elem1.style.cursor = 'default';
	    elem1.innerText = glb_EstadoNome[i];
	    elem1.onclick = estado_onclick;
		    
	    elem1.setAttribute('TemMotivo', glb_TemMotivo[i], 1);
		            
	    elem1.id = 'lbl' + (i + 1).toString();  

	    // acrescenta os elementos
	    window.document.body.appendChild(elem);
	    window.document.body.appendChild(elem1);
		    
	    elem.checked = glb_EstadoIDDefault[i];
	}
}

/********************************************************************
Ajusta elementos na modal, se nao tem estados
********************************************************************/
function finishAdjustWindowForMsg()
{
	var elem;
	var frameRect;
	var modWidth, modHeight;
	var topAfterBar;
    var freeHeightBetweenBarBtns;
	
	// Esconde e coloca dimensoes zero no campo e no label motivo
	elem = lblMotivo;
	elem.disabled = true;
	with (elem.style)
	{
		left = 0;
		top = 0;
		width = 0;
		height = 0;
		visibility = 'hidden';
	}
		
	elem = txtMotivo;
	elem.disabled = true;
	with (elem.style)
	{
		left = 0;
		top = 0;
		width = 0;
		height = 0;
		visibility = 'hidden';
	}
        
    redimAndReposicionModalWin(0, 0, true);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }
        
	// posiciona a mensagem   
    elem = document.getElementById('divMod01');
    
    var topAfterBar = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    var freeHeightBetweenBarBtns = parseInt(btnOK.currentStyle.top, 10) - topAfterBar;
        
    elem = document.getElementById('__lbl');
        
    elem.style.left = (modWidth - parseInt(elem.currentStyle.width, 10)) / 2;
    elem.style.top = topAfterBar +
		((freeHeightBetweenBarBtns - parseInt(elem.currentStyle.height, 10)) / 2);
}

/********************************************************************
Ajusta elementos na modal, se tem estados
********************************************************************/
function finishAdjustWindowForStates()
{
	// A janela tera a seguinte largura padrao: 436
	// A janela tera a seguinte altura:
	// frameBorder Superior = 6 +
	// divMod01 height +
	// ELEM_GAP +
	// n radios height +
	// n - 1 ELEM_GAP +
	// ELEM_GAP +
	// lblMotivo height +
	// txtMotivo height +
	// ELEM_GAP +
	// btnOK height +
	// ELEM_GAP +
	// frameBorder Inferior = 6
	
	// Os radios ocupam a seguinte area
	// top:
	// frameBorder Superior = 6 +
	// divMod01 height +
	// ELEM_GAP
	// height:
	// n radios height +
	// n - 1 ELEM_GAP +
	
	var modWinWidth = 434;
	var modWinHeight;
	var radiosTop, eachRadioAreaHeight, eachRadioTop;
	var i;
	var elem;
	var finalGap;
	
	var oneRadioHeight = parseInt(rad1.currentStyle.height, 10);

	modWinHeight = 6 + parseInt(divMod01.currentStyle.height, 10) + 
	               ELEM_GAP + (glb_EstadoID.length * oneRadioHeight) +
	               ((glb_EstadoID.length - 1) * ELEM_GAP) +
	               ELEM_GAP +
	               parseInt(lblMotivo.currentStyle.height, 10) +
	               parseInt(txtMotivo.currentStyle.height, 10) +
	               ELEM_GAP +
	               parseInt(btnOK.currentStyle.height, 10) +
	               ELEM_GAP + 6;
	               
	// Redimensiona a janela
	redimAndReposicionModalWin(modWinWidth, modWinHeight, false);
	
	radiosTop = 6 + parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
	
	eachRadioAreaHeight = oneRadioHeight + ELEM_GAP;
	               
	eachRadioTop = 0;
	
	// Posiciona cada radio
	for (i=0; i <glb_EstadoID.length; i++)
    {
        elem = 'rad' + (i + 1).toString();
            
        document.getElementById(elem).style.left = parseInt(document.getElementById('btnOK').currentStyle.left, 10);
        document.getElementById(elem).style.top = radiosTop + eachRadioTop;
                        
        elem1 = 'lbl' + (i + 1).toString();
            
        document.getElementById(elem1).style.left = parseInt(document.getElementById(elem).currentStyle.left, 10) + parseInt(document.getElementById(elem).currentStyle.width, 10) + ELEM_GAP;
        document.getElementById(elem1).style.top = parseInt(document.getElementById(elem).currentStyle.top) - 3;
            
        document.getElementById(elem1).style.width = document.getElementById(elem1).innerText.length * FONT_WIDTH;
        
        eachRadioTop += eachRadioAreaHeight;
    }
    
    eachRadioTop -= ELEM_GAP;

	// Posiciona o lbl e o campo motivo	
	setupFldMotivo(radiosTop + eachRadioTop + ELEM_GAP);
	
	// Ajuste final da altura da janela
	finalGap = parseInt(btnOK.currentStyle.top, 10) -
	           (parseInt(txtMotivo.currentStyle.top, 10) +
	            parseInt(txtMotivo.currentStyle.height, 10));
	
	modWinHeight += (ELEM_GAP - finalGap);               
	
	// Redimensiona a janela
	redimAndReposicionModalWin(modWinWidth, modWinHeight, false);
}

/********************************************************************
Coloca foco no campo txtMotivo
********************************************************************/
function putFocusInTxtMotivo()
{
    // foca controle txtMotivo
    if ( !txtMotivo.disabled )
    {
        try
        {
            window.focus();
            txtMotivo.focus();
        }
        catch (e)
        {
            ;
        }
    }
}

/********************************************************************
Coloca foco no botao OK
********************************************************************/
function putFocusInBtnOK()
{
    // foca botao OK
    if ( !btnOK.disabled )
    {
        try
        {
            window.focus();
            btnOK.focus();
        }
        catch (e)
        {
            ;
        }
    }
}

/********************************************************************
Configura e posiciona o campo motivo, reposiciona os botoes OK e Cancela
e redimensiona e reposiciona a janela.
********************************************************************/
function setupFldMotivo(lblMotivoTop)
{
	var elem;
	var nMotivoNumChars = 50;
	var i;
	var nModalHorizCenter = 0;
	var btnOK_Top;
		
	lblMotivo.style.visibility = 'hidden';
	
	txtMotivo.maxLength = nMotivoNumChars;
	txtMotivo.onfocus = selFieldContent;
	txtMotivo.onkeyup = setonkeyup;
	txtMotivo.style.visibility = 'hidden';
	txtMotivo.disabled = true;
	
	nModalHorizCenter = btnOK.offsetLeft + btnOK.offsetWidth +
	                    (btnCanc.offsetLeft - (btnOK.offsetLeft + btnOK.offsetWidth)) / 2;
	
	// posiciona o label e o campo Motivo
	elem = txtMotivo;
	with (elem.style)
	{
		width = nMotivoNumChars * FONT_WIDTH;
		left = nModalHorizCenter - (parseInt(elem.currentStyle.width, 10) / 2);
	}
	
	elem = lblMotivo;
	with (elem.style)
	{
		left = parseInt(txtMotivo.currentStyle.left, 10);
		top = lblMotivoTop;
		width = elem.innerText.length * FONT_WIDTH;
	}
	
	elem = txtMotivo;
	with (elem.style)
	{
		top = parseInt(lblMotivo.currentStyle.top, 10) + 
		      parseInt(lblMotivo.currentStyle.height, 10);
	}
}
/********************************************************************
Configuracao do enter no campo motivo
********************************************************************/
function setonkeyup()
{
    if ( event.keyCode == 13 )
		btn_onclick(btnOK);
}
