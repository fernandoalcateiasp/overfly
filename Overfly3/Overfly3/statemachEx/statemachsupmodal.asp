
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="statemachsupmodal01Html" name="statemachsupmodal01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/statemachEx/statemachmodal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/statemachEx/statemachmodal.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

'Response.Redirect("../modalgen/modalgenmessage.asp?sCaption=Aten��o&sMessage=Esta � uma mensagem de teste")

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'A partir do ID do subform, obtem os dados dos recurso
'para os quais o estado pode transitar
'============================================================

Dim i, estadoID, formID, contextoID, userID, empresaID, nProp1, nProp2, sCondition

estadoID = 0
formID = 0
contextoID = 0
userID = 0
empresaID = 0
nProp1 = 0
nProp2 = 0
sCondition = ""

For i = 1 To Request.QueryString("estadoID").Count    
    estadoID = Request.QueryString("estadoID")(i)
Next

For i = 1 To Request.QueryString("formID").Count    
    formID = Request.QueryString("formID")(i)
Next
    
For i = 1 To Request.QueryString("contextoID").Count    
    contextoID = Request.QueryString("contextoID")(i)
Next

For i = 1 To Request.QueryString("userID").Count    
    userID = Request.QueryString("userID")(i)
Next

For i = 1 To Request.QueryString("empresaID").Count    
    empresaID = Request.QueryString("empresaID")(i)
Next

For i = 1 To Request.QueryString("nProp1").Count    
    nProp1 = Request.QueryString("nProp1")(i)
Next

For i = 1 To Request.QueryString("nProp2").Count    
    nProp2 = Request.QueryString("nProp2")(i)
Next
'Usuario e Proprietario do Registro
If ( (CLng(nProp1)=1) AND (CLng(nProp2)=1) ) Then
    sCondition = "AND (h.Alterar1=1 OR h.Alterar2=1) "     
'Usuario e Alternativo do Registro
ElseIf ( (CLng(nProp1)=0) AND (CLng(nProp2)=1) ) Then
    sCondition = "AND (h.Alterar2=1) "     
'Usuario nao tem nenhuma propriedade sobre o registro
Else
    sCondition = "AND (h.Alterar1=1 AND h.Alterar2=1) "     
End If

Dim rsData, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")
 
strSQL = "SELECT DISTINCT i.RecursoID AS RecursoID,d.EhDefault AS EhDefault, d.TemMotivo, " & _
         "i.RecursoFantasia AS Recurso,e.Ordem " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), RelacoesRecursos_Estados d WITH(NOLOCK), " & _
         "RelacoesRecursos e WITH(NOLOCK), RelacoesPesRec f WITH(NOLOCK), RelacoesPesRec_Perfis g WITH(NOLOCK), Recursos_Direitos h WITH(NOLOCK), Recursos i WITH(NOLOCK), Recursos j WITH(NOLOCK) " & _
         "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID=" & CStr(contextoID) & " " & _
         "AND a.SujeitoID=b.RecursoID AND b.Principal=1 " & _ 
         "AND a.MaquinaEstadoID=c.ObjetoID AND c.EstadoID=2 AND c.TipoRelacaoID=3 " & _ 
         "AND c.SujeitoID=" & CStr(estadoID) & " AND c.RelacaoID=d.RelacaoID AND ISNULL(d.UsoSistema, 0) = 0 " & _
         "AND e.SujeitoID=d.RecursoID AND e.ObjetoID=a.MaquinaEstadoID AND e.TipoRelacaoID=3 AND e.EstadoID=2 " & _
         "AND f.SujeitoID IN ((SELECT " & CStr(userID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(userID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND f.ObjetoID=999 AND f.TipoRelacaoID=11 " & _
         "AND f.EstadoID=2 AND f.RelacaoID=g.RelacaoID " & _
         "AND g.EmpresaID=" & CStr(empresaID) & _
         " AND h.RecursoID=" & CStr(estadoID) & " AND h.EstadoParaID=d.RecursoID " & _
         "AND h.RecursoMaeID=b.RecursoID " & _
         "AND h.ContextoID=" & CStr(contextoID) & " AND h.PerfilID=g.PerfilID " & _
          CStr(sCondition) & " AND h.EstadoParaID=i.RecursoID " & _
         "AND g.PerfilID = j.RecursoID AND j.EstadoID = 2 " & _
         "ORDER BY e.Ordem "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

'----------------------------------------------------------------
'Escreve as variaveis no form
i = 0

Response.Write "var glb_EstadoID = new Array();"
Response.Write vbcrlf
Response.Write "var glb_EstadoIDDefault = new Array();"
Response.Write vbcrlf
Response.Write "var glb_EstadoNome = new Array();"
Response.Write vbcrlf
Response.Write "var glb_TemMotivo = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf

While Not rsData.EOF
      
    Response.Write "glb_EstadoID[" & CStr(i) & "] = " & CStr(rsData.Fields("RecursoID").Value) & ";"
    Response.Write vbcrlf
    Response.Write "glb_EstadoIDDefault[" & CStr(i) & "] = " & LCase(CStr(rsData.Fields("EhDefault").Value)) & ";"
    Response.Write vbcrlf
    Response.Write "glb_EstadoNome[" & CStr(i) & "] = '" & CStr(rsData.Fields("Recurso").Value) & "';"
    Response.Write vbcrlf
    
	If IsNull(rsData.Fields("TemMotivo").Value) Then
		Response.Write "glb_TemMotivo[" & CStr(i) & "] = 0;"
	ElseIf (rsData.Fields("TemMotivo").Value = 0) Then
		Response.Write "glb_TemMotivo[" & CStr(i) & "] = 0;"
	Else
		Response.Write "glb_TemMotivo[" & CStr(i) & "] = 1;"
    End If
    
    Response.Write vbcrlf
    
    i = i + 1
    rsData.MoveNext
Wend
'----------------------------------------------------------------
'Fecha objetos
rsData.Close
Set rsData = Nothing    

'Necessario para compatibilidade da automacao
Response.Write "var glb_USERID = 0;"

'----------------------------------------------------------------
'Fecha script
Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	var coll, i;
	
    window_onload_1stPart();
    
    // ajusta o body do html
    with (statemachsupmodal01Body)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    // este arquivo carregou na janela modal
    sendJSMessage('statemachsupmodal01Html', JS_STMACHMODAL, 'STATEMACH_VISIBLE', null);

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    coll = window.document.getElementsByTagName('INPUT');
    
    for ( i=0; i<coll.length; i++ )
    {
        if ( coll.item(i).type == 'radio' )
        {
            if (coll.item(i).checked)
            {
				estado_onclick(coll.item(i));
				break;
			}	
        }    
    }
}

//-->
</script>

</head>

<body id="statemachsupmodal01Body" name="statemachsupmodal01Body" LANGUAGE="javascript" onload="return window_onload()">

    <p id="lblMotivo" name="lblMotivo" class="lblGeneral">Motivo</p>
    <input type="text" id="txtMotivo" name="txtMotivo" class="fldGeneral"></input>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
