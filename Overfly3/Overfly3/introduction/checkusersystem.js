/********************************************************************
checkusersystem.js

Library javascript para o checkusersystem.html
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

********************************************************************/

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    // esconde todos os frames
    showAllFramesInHtmlTop(false);
    
    var elem;
    var lMargin = 15;
    var tMargin = 10;
    var vGap = 0;
    var vFixGap = 10;
    
    // configura os elementos
    elem = window.document.getElementById('checkusersystemBody');
    elem.scroll = 'no';
    
    // ajusta o paragrafo Overfly
    elem = window.document.getElementById('titulo1');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = lMargin;
        top = tMargin;
        width = 100;
        height = 26;
        
        vGap = parseInt(top, 10) + parseInt(height, 10);
        
        fontSize = '14pt'; 
        fontWeight = 'bold';
        
    }
    
    // ajusta o paragrafo frase1
    elem = window.document.getElementById('frase1');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = lMargin;
        top = vGap + vFixGap;
        width = 300;
        height = 22;
        
        vGap = parseInt(top, 10) + parseInt(height, 10);
        
        fontSize = '12pt'; 
        fontWeight = 'normal';
    }
    
    // ajusta o table tblCheck
    elem = window.document.getElementById('tblCheck');
    with (elem.style)
    {
        left = lMargin;
        top = vGap + vFixGap;
        width = 480;
        height = 20 + 60 + 60;
        
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta o paragrafo frase1
    elem = window.document.getElementById('frase2');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = lMargin;
        top = vGap +  2 * vFixGap + 4;
        width = 480;
        height = 22;
        
        vGap = parseInt(top, 10) + parseInt(height, 10);
        
        fontSize = '12pt'; 
        fontWeight = 'normal';
    }
    
    headerCol0.style.width = 100;
    cellCol0Ln0.style.width = 100;
    cellCol0Ln1.style.width = 100;
    
    // ajusta o frame para mostrar a p�gina
    moveExtFrame(window, new Array(0, 0, MAX_FRAMEWIDTH, MAX_FRAMEHEIGHT));

    // verifica o sistema do usuario
    if (checkUserSys())
    {
        //sendJSMessage('checkusersystemHtml', JS_PAGELOAD, new Array(window.top.name.toString(), 'frameGen01', SYS_PAGESURLROOT + '/login/userlogin.asp'), null);

        var nDeviceID = localStorage.getItem("OVERFLY_DEVICEID");
        nDeviceID = (nDeviceID == null ? 0 : nDeviceID);

        sendJSMessage('checkusersystemHtml', JS_PAGELOAD, new Array(window.top.name.toString(), 'frameGen01', SYS_PAGESURLROOT + '/login/userlogin.aspx?deviceID=' + escape(nDeviceID)), null);
    }
    else {
        window.document.getElementById('frase2').innerText =
            'Seu sistema nao est� compativel com o ' + SYS_NAME + '.';
        // mostra o frame que contem o html
        showExtFrame(window, true);
    }
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        default:
            return null;
    }
}

/********************************************************************
Esta funcao verifica se o sistema do usuario � compativel:
sistema operacional + browser.

Parametros: nenhum

Retornos: true se compat�vel ou false se nao compativel.
********************************************************************/
function checkUserSys()
{
    var str = new String();
    var str2 = new String();
    var retVal = false;
    var iPos;
       
    str = navigator.platform;
    window.document.getElementById('cellCol2Ln0').innerText = str;
    
    str = navigator.appName;
    str +=" ";
    str += navigator.appVersion;
    window.document.getElementById('cellCol2Ln1').innerText = str;
    
    // enable btnGoOverfly?
    str = navigator.userAgent;
    iPos = (str.toUpperCase()).indexOf('MSIE');
    
    if (iPos != -1)
    {
        str = navigator.userAgent;
        str2 = str.substr((iPos + 5), 1);
        if ( eval(str2) < 5 )
            retVal = false;
        else    
            retVal = true;
    }    
    else
        retVal = false;
    
    return retVal;
}