<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<HTML id="checkusersystemHtml">

<HEAD>

<TITLE>Overfly</TITLE>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/introduction/checkusersystem.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/introduction/checkusersystem.js" & Chr(34) & "></script>" & vbCrLf                
%>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<!--

//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=window EVENT=onload>
<!--
 window_onload();
//-->
</SCRIPT>

</HEAD>
<BODY id="checkusersystemBody">
    <p id="titulo1" name="titulo1" class="lblGeneral">
    Overfly</p>

    <p id="frase1" name="frase1" class="lblGeneral">
    Verificação dos Recursos do Sistema</p>
    
    <table id="tblCheck" name="tblCheck" align="left" cellpadding="4" class = "tbl">
        <tr id="tblCheckHeader" name="tblCheckHeader" class="cellHeader">
            <th id="headerCol0" name="headerCol0" class="cellHeader">Recurso</th>
            <th id="headerCol1" name="headerCol1" class="cellHeader">Requerido pelo Overfly</th>
            <th id="headerCol2" name="headerCol2" class="cellHeader">Seu sistema</th>
        </tr>
        <tr id="tblLine0" name="tblLine0" class="cellLine">
            <td id="cellCol0Ln0" name="cellCol0Ln0" class="cellLine">Plataforma</th>
            <td id="cellCol1Ln0" name="cellCol1Ln0" class="cellLine">Win 32 - Window 95 (ou acima) ou NT4 (ou acima)</th>
            <td id="cellCol2Ln0" name="cellCol2Ln0" class="cellLine"></th>
        </tr>
        <tr id="tblLine1" name="tblLine1" class="cellLine">
            <td id="cellCol0Ln1" name="cellCol0Ln1" class="cellLine">Browser</th>
            <td id="cellCol1Ln1" name="cellCol1Ln1" class="cellLine">Microsoft Internet Explorer 5 (ou acima) instalação completa</th>
            <td id="cellCol2Ln1" name="cellCol2Ln1" class="cellLine"></th>
        </tr>
    </table>

    <p id="frase2" name="frase2" class="lblGeneral"></p>

</BODY>

</HTML>
