/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Recursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

var nEmpresaData = getCurrEmpresaData();
var nUsuarioID = getCurrUserID();
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)    
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    setConnection(dso);
    
    var currCmb2Data;
    var aValue;

    // Ordem Normal
    glb_bCheckInvert = false;

    //@@
    if (folderID == 20008) // Observacoes
    {
        dso.SQL = 'SELECT a.PessoaID, a.Observacoes ' +
                  'FROM Pessoas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.PessoaID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    //@@
    else if (folderID == 20009) // Prop/Altern
    {
        dso.SQL = 'SELECT a.PessoaID, a.ProprietarioID, a.AlternativoID ' +
                  'FROM Pessoas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.PessoaID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT a.LOGID, a.FormID, a.SubFormID, a.RegistroID, a.Data, a.UsuarioID, a.EventoID, a.EstadoID, ISNULL(a.Motivo, SPACE(1)) AS Motivo, ' +
                        'dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso, b.Campo, (CASE a.EventoID WHEN 28 THEN SPACE(1) ELSE b.Conteudo END) AS Conteudo, ' +
                        'dbo.fn_Log_Mudanca(b.LogDetalheID) AS Mudanca ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                    'LEFT OUTER JOIN LOGs_Detalhes b WITH(NOLOCK) ON b.LOGID=a.LOGID ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID, a.Data DESC';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20106) // Contrato Social
    {
        dso.SQL = 'SELECT a.PessoaID, ' +
                            'a.dtUltimaAlteracao, CONVERT(VARCHAR, a.dtUltimaAlteracao, '+DATE_SQL_PARAM+') AS V_dtUltimaAlteracao, ' +
                            '(SELECT COUNT(LOGID) ' +
                                'FROM LOGs WITH(NOLOCK) WHERE (FormID = 1210) AND (RegistroID = ' + idToFind + ') AND (EventoID = 29)) AS Alteracao, ' +
                            'a.CapitalSocial, a.PatrimonioLiquido, a.FaturamentoAnual, ' +
                            'a.ObjetoSocial, ' +
				            'ISNULL(dbo.fn_Direitos_Valor(' + nUsuarioID + ', ' + nEmpresaData[0] + ', NULL, 47, 0, GETDATE()), 1) AS NivelPagamentoMaximo, ' +
                            'a.UsuarioID, a.NumeroFuncionarios ' +
                    'FROM Pessoas a WITH(NOLOCK) ' +
                    'WHERE ' + sFiltro + ' a.PessoaID = ' + idToFind;
        return 'dso01JoinSup' + '_DSC';
    }
    else if (folderID == 20108) // Transportadora
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pessoas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.PessoaID = ' + idToFind;
        return 'dso01JoinSup' + '_DSC';
    }
    else if (folderID == 20110) // Documentos
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pessoas_Documentos a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.PessoaID = '+ idToFind + ' ' +
                  'ORDER BY ISNULL(dbo.fn_Pessoa_PaisOrdem(a.PessoaID, a.PaisID), 999), ' +
					'(SELECT b.Ordem FROM TiposAuxiliares_Itens b WITH(NOLOCK) WHERE b.ItemID=a.TipoDocumentoID), UFID';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20138) // Certid�es
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pessoas_Certidoes a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.PessoaID = ' + idToFind + ' ' +
                  'ORDER BY a.TipoCertidaoID';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 20111) // Enderecos
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pessoas_Enderecos a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.PessoaID = '+ idToFind + ' ' +
                  'ORDER BY a.Ordem';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20112) // Telefones
    {
        dso.SQL = 'SELECT a.PesTelefoneID, a.PessoaID, a.TipoTelefoneID, a.Ordem, a.DDI, a.DDD, a.Numero, a.Observacao, a.Publica, a.UsuarioID ' +
                  'FROM Pessoas_Telefones a WITH(NOLOCK) ' +
                  'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.TipoTelefoneID = b.ItemID ' +
                  'WHERE ' + sFiltro + ' a.PessoaID = '+ idToFind + ' ' +
                  'ORDER BY b.Ordem, a.TipoTelefoneID ';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20113) // URLS
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pessoas_URLs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.PessoaID = '+ idToFind + ' ' +
                  'ORDER BY a.TipoURLID, a.Ordem';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20114) // Formacoes
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pessoas_Formacoes a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.PessoaID = '+ idToFind + ' ' +
                  'ORDER BY a.Ordem';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20115) // Isen��es
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Pessoas_Isencoes a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.PessoaID = '+ idToFind + ' ' +
                  'ORDER BY a.PesIsencaoID';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20120) // Pedidos
    {
        // Ordem Invertida
        glb_bCheckInvert = true;
        
        var nFiltroID = getCmbCurrDataInControlBar('inf', 2);
        if ((nFiltroID != null) && (nFiltroID[1] == 41042))
            sFiltro = sFiltro + ' AND a.EmpresaID=' + nEmpresaData[0] + ' AND a.EstadoID > 23 ';
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        // Paginacao Inferior
        glb_currGridTable = 'Pedidos';
        glb_aCmbKeyPesq = [['Data','adtMovimento'],['ID','aPedidoID']];
        // paginacao de grid
        return execPaging(dso, 'ParceiroID,PessoaID', idToFind, folderID, sFiltro);
    }

    else if (folderID == 20121) // Produtos
    {
        // Ordem Invertida
        glb_bCheckInvert = true;
    
        var nFiltroID = getCmbCurrDataInControlBar('inf', 2);
        if ( (nFiltroID != null) && (nFiltroID[1] == 41042) )
            sFiltro = sFiltro + ' AND a.EmpresaID=' + nEmpresaData[0] + ' ';
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        // Paginacao Inferior
        glb_currGridTable = 'Pedidos';
        glb_aCmbKeyPesq = [['Data','cdtMovimento'], ['Produto','dConceito']];
        // paginacao de grid
        return execPaging(dso, 'ParceiroID,PessoaID', idToFind, folderID, sFiltro);
    }
    else if (folderID == 20122) // Financeiros
    {
        // Ordem Invertida
        glb_bCheckInvert = true;
    
        var nFiltroID = getCmbCurrDataInControlBar('inf', 2);
        if ( (nFiltroID != null) && (nFiltroID[1] == 41042) )
            sFiltro = sFiltro + ' AND a.EmpresaID=' + nEmpresaData[0] + ' ';
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        // Paginacao Inferior
        glb_currGridTable = 'Financeiros';
        glb_aCmbKeyPesq = [['Data','adtVencimento'],['Duplicata','VaDuplicata']];
        // paginacao de grid
        return execPaging(dso, 'ParceiroID,PessoaID', idToFind, folderID, sFiltro);
    }
    else if (folderID == 20123) // Rel entre pessoas
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'RelacoesPessoas';
        glb_aCmbKeyPesq = [['ID','aRelacaoID'],[aValue[2],'bFantasia']];
        glb_vOptParam1 = aValue[1];
        glb_vOptParam2 = aValue[0];
        // Final da automacao =========================================================
        
        if (aValue[1] == 'SUJEITO')
            return execPaging(dso, 'ObjetoID', idToFind, folderID);
        else if (aValue[1] == 'OBJETO')
            return execPaging(dso, 'SujeitoID', idToFind, folderID);
    }
    else if (folderID == 20124) // Rel com recursos
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'RelacoesPesRec';
        glb_aCmbKeyPesq = [['ID','aRelacaoID'],[aValue[2],'bRecursoFantasia']];
        glb_vOptParam1 = aValue[0];
        // Final da automacao =========================================================

        return execPaging(dso, 'SujeitoID', idToFind, folderID);
    }
    else if (folderID == 20125) // Rel com Conceitos
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'RelacoesPesCon';
        glb_aCmbKeyPesq = [['ID','aRelacaoID'],[aValue[2],'bConceito']];
        glb_vOptParam1 = aValue[0];
        // Final da automacao =========================================================

        return execPaging(dso, 'SujeitoID', idToFind, folderID);
    }
    else if (folderID == 20126) // Referencias Comerciais
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Pessoas_RefComerciais a WITH(NOLOCK) ' +
        	      'WHERE (a.PessoaID = ' + idToFind + ') ' +
        	      'ORDER BY Contato';

        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20127) // Empresas
    {
        dso.SQL = 'SELECT DISTINCT a.Cargo, c.PessoaID AS ClienteID, c.Fantasia AS Cliente, ' +
                    'd.PessoaID AS FornecedorID, d.Fantasia AS Fornecedor ' +
                  'FROM RelacoesPessoas_Contatos a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK) ' +
        	      'WHERE (a.ContatoID = ' + idToFind + ' AND a.RelacaoID = b.RelacaoID AND ' +
        	            'b.SujeitoID = c.PessoaID AND b.ObjetoID = d.PessoaID)';

        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20130) // Senhas
    {
        dso.SQL = 'SELECT * ' +
	                'FROM Pessoas_Senhas a WITH(NOLOCK) ' +
	                'WHERE ' + sFiltro + 'a.PessoaID = ' + idToFind + ' ' +
	                'ORDER BY a.PesSenhaID DESC';

        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20128) // Contatos
    {
        dso.SQL = 'SELECT DISTINCT e.PessoaID AS ContatoID, e.Fantasia AS Contato, b.Cargo, ' +
	                	'dbo.fn_Pessoa_Telefone(e.PessoaID, 119, 120, 1, 0, NULL) AS Telefone, ' +
	                	'ISNULL((SELECT TOP 1 URL FROM Pessoas_URLs WITH(NOLOCK) WHERE(PessoaID = e.PessoaID AND TipoURLID = 124 AND Ordem = 1)), SPACE(0)) AS Email,' +
	                	'CONVERT(BIT, (CASE WHEN a.SujeitoID = ' + idToFind + ' THEN 1 ELSE 0 END)) AS EhCliente,' +
	                	'(CASE WHEN a.SujeitoID = ' + idToFind + ' THEN d.PessoaID ELSE c.PessoaID END) AS EmpresaID,' +
	                	'(CASE WHEN a.SujeitoID = ' + idToFind + ' THEN d.Fantasia ELSE c.Fantasia END) AS Empresa ' +
	                'FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Contatos b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK) ' +
	                'WHERE (a.TipoRelacaoID = 21 AND ' + idToFind + ' IN (a.SujeitoID, a.ObjetoID) AND ' +
	                	'a.RelacaoID = b.RelacaoID AND a.SujeitoID = c.PessoaID AND a.ObjetoID = d.PessoaID AND ' +
	                	'b.ContatoID = e.PessoaID)';

        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20129) // Dados de transporte
    {
        dso.SQL = 'SELECT * ' +
	                'FROM Pessoas_Transportadoras a WITH(NOLOCK) ' +
	                'WHERE a.PessoaID = ' + idToFind + ' ' +
	                'ORDER BY a.dtInicio DESC';

        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20131) // Visitas    
    {                            
        dso.SQL = 'SELECT a.VisitaID, d.Fantasia AS Empresa, c.RecursoAbreviado AS Estado, a.dtVisita, ' +
                   'a.dtVencimento, f.ItemMasculino AS TipoVisitaID, g.ItemMasculino AS TipoVisitadoID, e.Fantasia AS Vendedor, a.Observacao ' +
	                'FROM Visitas a WITH(NOLOCK) INNER JOIN Pessoas b WITH(NOLOCK) ON a.PessoaID = b.PessoaID ' +
	                'INNER JOIN Recursos c WITH(NOLOCK) ON a.EstadoID = c.RecursoID ' +
	                'INNER JOIN Pessoas d WITH(NOLOCK) ON a.EmpresaID = d.PessoaID ' +
	                'INNER JOIN Pessoas e WITH(NOLOCK) ON a.ProprietarioID = e.PessoaID ' +
	                'INNER JOIN TiposAuxiliares_itens f WITH(NOLOCK) ON a.TipoVisitaID = f.ItemID ' +
	                'INNER JOIN TiposAuxiliares_itens g WITH(NOLOCK) ON a.TipoVisitadoID = g.ItemID ' +
	                'WHERE a.PessoaID = ' + idToFind + ' ' +
	                'ORDER BY a.VisitaID ';

        return 'dso01Grid' + '_DSC';
    }    
    else if (folderID == 20132) // Atendimento
    {                            
        dso.SQL = 'SELECT a.dtInicio, b.Fantasia AS Empresa, c.Fantasia AS Usuario, d.ItemMasculino AS TipoAtendimentoID, ' +
                    'a.AtendimentoOK AS OK, SUBSTRING(a.Observacoes,1,100) AS Observacoes, a.dtAgendamento ' +
	                'FROM Pessoas_Atendimentos a WITH(NOLOCK) ' +
	                'INNER JOIN Pessoas b WITH(NOLOCK) ON a.EmpresaID = b.PessoaID ' +
	                'INNER JOIN Pessoas c WITH(NOLOCK) ON a.UsuarioID = c.PessoaID ' +
	                'INNER JOIN TiposAuxiliares_itens d WITH(NOLOCK) ON a.TipoAtendimentoID = d.ItemID ' +	                
	                'WHERE a.PessoaID = ' + idToFind + ' ' +
	                'ORDER BY a.dtInicio DESC, a.dtAgendamento ';

        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 32033) // Dados Banc�rios
    {
        dso.SQL = 'SELECT * ' +
                    'FROM Pessoas_DadosBancarios WITH(NOLOCK) ' +
                    'WHERE PessoaID = ' + idToFind + ' ' +
                    'ORDER BY PesDadosBancariosID';

        return 'dso01Grid' + '_DSC';
    }
    //Heraldo Grid
    else if (folderID == 20135) // Empresas
    {
        dso.SQL = 'SELECT * FROM Pessoas_Empresas a WITH(NOLOCK) ' +
                  'WHERE a.PessoaID =' + idToFind;
        return 'dso01Grid' + '_DSC';
    }
    

    //Parametros Marketplace
    else if (folderID == 20137) 
    {
        dso.SQL = 'SELECT * FROM Pessoas_MarketplaceParametros a WITH(NOLOCK) ' +
                  'WHERE a.PessoaID =' + idToFind;
        return 'dso01Grid' + '_DSC';
    }
    //Creditos
    else if (folderID == 29094)
    {
        dso.SQL = 'SELECT a.Bloquear, a.FaturamentoDireto, a.Compartilhar, a.NivelPagamento, a.FinanciamentoPadraoID, FinanciamentoLimiteID, FormaPagamentoID, ' +
            '(SELECT b.SimboloMoeda FROM Conceitos b WITH(NOLOCK) WHERE(b.ConceitoID = a.MoedaID)) AS Moeda, ' +
            '(SELECT c.CodigoLocalidade3 FROM Localidades c WITH(NOLOCK) WHERE(c.LocalidadeID = a.PaisID)) AS Pais, ' +
            '(SELECT MAX(d.dtFim) FROM Creditos d WITH(NOLOCK) WHERE (d.ParceiroID = a.PessoaID) AND (d.PaisID = a.PaisID)) AS dtVencimento, ' +
                    'dbo.fn_Pessoa_Credito(a.PessoaID, 1, a.PaisID) AS CreditoEmpresa, ' +
                    'dbo.fn_Pessoa_Credito(a.PessoaID, 2, a.PaisID) AS CreditoSeguradora, ' +
                    'dbo.fn_Pessoa_Credito(a.PessoaID, 3, a.PaisID) AS CreditoConcedido, ' +
                    'dbo.fn_Pessoa_Credito(a.PessoaID, 4, a.PaisID) AS SaldoCredito, ' +
                    'dbo.fn_Pessoa_Credito(a.PessoaID, 6, a.PaisID) AS SaldoCreditoGrupo, ' +
                    'a.Observacao, a.PesCreditoID ' +
            'FROM Pessoas_Creditos a WITH(NOLOCK) ' +
            'WHERE(a.PessoaID = ' + idToFind + ')';
        return 'dso01Grid' + '_DSC';
    }

    return null;
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');

    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }

    // Documentos
    else if (pastaID == 20110) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT LocalidadeID AS fldID, Localidade AS fldValue, CodigoLocalidade2 ' +
                'FROM Localidades WITH(NOLOCK) ' +
                'WHERE EstadoID=2 AND TipoLocalidadeID=203 ' +
                'ORDER BY Localidade';
        }
        else if (dso.id == 'dsoCmb02Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=17 ' +
                      'AND Filtro LIKE ' + '\'' + '%(' + nTipoRegistroID + ')%' + '\'' + ' ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb03Grid_01') {
            dso.SQL = 'SELECT NULL AS fldID, \'\' AS fldValue, \'\' AS CodigoLocalidade2 ' +
                      'UNION ALL ' +
                      'SELECT LocalidadeID AS fldID, Localidade AS fldValue, CodigoLocalidade2 ' +
                      'FROM Localidades WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoLocalidadeID=204 AND LocalizacaoID=' + nEmpresaData[1] + ' ' +
                      'ORDER BY fldValue';
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT TOP 1 PaisID, 0 AS Ordem2 ' +
                      'FROM Pessoas_Enderecos WITH(NOLOCK) ' +
                      'WHERE PessoaID = ' + nRegistroID + ' ' +
                      'AND EndFaturamento = 1 AND Ordem = 1 ' +
                      'UNION ALL SELECT TOP 1 PaisID, ISNULL(dbo.fn_Pessoa_PaisOrdem(PessoaID, PaisID), 999) AS Ordem2 ' +
                      'FROM Pessoas_Documentos WITH(NOLOCK) ' +
                      'WHERE PessoaID = ' + nRegistroID + ' ' +
                      'AND TipoDocumentoID IN (101,111) ' +
                      'ORDER BY Ordem2';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = "SELECT a.TipoDocumentoID, CASE WHEN b.Filtro LIKE '%{232}%' THEN 1 ELSE 0 END AS Documento_Estadual " +
                      "FROM Pessoas_Documentos a WITH(NOLOCK) " +
                      "INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON b.ItemID = a.TipoDocumentoID " +
                      "WHERE a.PessoaID = " + nRegistroID;
        }
    }
    else if (pastaID == 20138) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = "SELECT a.LocalidadeID AS fldID, a.Localidade AS fldValue "+               
                        "FROM Localidades a WITH(NOLOCK) " +
                        "INNER JOIN Localidades b WITH(NOLOCK) ON (b.LocalidadeID = a.LocalizacaoID) AND (b.LocalizacaoID = " + nEmpresaData[1] + " ) " +
                        "LEFT OUTER JOIN Pessoas c WITH(NOLOCK) ON (c.UFNascimento LIKE '%' + b.CodigoLocalidade2 +'%') AND (c.PessoaID = + " + nRegistroID + " ) " +
                        "WHERE a.TipoLocalidadeID = 205 " +
                        "ORDER BY a.Localidade ASC";
        }
        else if (dso.id == 'dsoCmb02Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=23 ' +
                      'ORDER BY Ordem';
        }
    }
    // Enderecos
    else if (pastaID == 20111) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT LocalidadeID,Localidade,CodigoLocalidade2 ' +
                      'FROM Localidades WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoLocalidadeID=203 ' +
                      'ORDER BY CodigoLocalidade2';
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT DISTINCT b.LocalidadeID,b.Localidade,b.CodigoLocalidade2 ' +
                      'FROM Pessoas_Enderecos a WITH(NOLOCK), Localidades b WITH(NOLOCK) ' +
                      'WHERE (a.PessoaID = ' + nRegistroID + ' AND a.UFID = b.LocalidadeID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT DISTINCT b.LocalidadeID,b.Localidade,b.CodigoLocalidade2 ' +
                      'FROM Pessoas_Enderecos a WITH(NOLOCK), Localidades b WITH(NOLOCK) ' +
                      'WHERE (a.PessoaID = ' + nRegistroID + ' AND a.CidadeID = b.LocalidadeID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT TOP 1 a.PaisID, 0 AS Ordem2, ISNULL(b.EnderecoInvertido, 0) AS EndInvertido ' +
                      'FROM Pessoas_Enderecos a WITH(NOLOCK), Localidades b WITH(NOLOCK) ' +
                      'WHERE a.PessoaID = ' + nRegistroID + ' ' +
                      'AND a.EndFaturamento = 1 AND a.Ordem = 1 ' +
                      'AND a.PaisID = b.LocalidadeID ' +
                      'UNION ALL SELECT TOP 1 a.PaisID, ISNULL(dbo.fn_Pessoa_PaisOrdem(a.PessoaID, a.PaisID), 999) AS Ordem2, ISNULL(b.EnderecoInvertido, 0) AS EndInvertido ' +
                      'FROM Pessoas_Documentos a WITH(NOLOCK), Localidades b WITH(NOLOCK) ' +
                      'WHERE a.PessoaID = ' + nRegistroID + ' ' +
                      'AND a.TipoDocumentoID IN (101,111) ' +
                      'AND a.PaisID = b.LocalidadeID ' +
                      'ORDER BY Ordem2';
        }
    }
    // Telefones
    else if (pastaID == 20112) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=19 ' +
			          'ORDER BY Ordem';
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT \'DDD\' as TipoDDDI, a.DDDI ' +
                      'FROM Localidades_ddds a WITH(NOLOCK), Pessoas_Enderecos b WITH(NOLOCK) ' +
                      'WHERE b.Ordem = 1 AND a.Ordem = 1 AND ' +
                      'a.LocalidadeID = b.cidadeid AND b.PessoaID = ' +
                      nRegistroID + ' ' +
                      'UNION ALL ' +
                      'SELECT \'DDI\' as TipoDDDI, a.DDDI ' +
                      'FROM Localidades_ddds a WITH(NOLOCK), Pessoas_Enderecos b WITH(NOLOCK) ' +
                      'WHERE b.Ordem = 1 AND a.Ordem = 1 AND ' +
                      'a.LocalidadeID = b.paisid ' +
                      'AND b.PessoaID = ' + nRegistroID;
        }
    }
    // Dados Banc�rios
    else if (pastaID == 32033) 
    {
        if (dso.id == 'dsoCmb01Grid_01') 
        {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=902 AND ' +
                      'Filtro LIKE ' + '\'' + '%<CLI>%' + '\'' + ' ' +
			          'ORDER BY Ordem';
        }
        if (dso.id == 'dsoCmb02Grid_01') 
        {
            dso.SQL = 'SELECT a.BancoID, a.Codigo, a.Banco ' +
                      'FROM Bancos a WITH(NOLOCK) ' +
                      'WHERE a.EstadoID = 2 ' +                      
			          'ORDER BY a.Codigo';
        }
    }
    // URLs
    else if (pastaID == 20113) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=20 ' +
			          'ORDER BY Ordem';
        }
    }
    // Formacoes
    else if (pastaID == 20114) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT LocalidadeID,Localidade,CodigoLocalidade2 ' +
                      'FROM Localidades WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoLocalidadeID=203 ' +
                      'ORDER BY CodigoLocalidade2';
        }
    }
    // Insen��es
    else if (pastaID == 20115) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = ' SELECT ConceitoID as ImpostoID, Imposto ' +
                      ' FROM Conceitos WITH(NOLOCK) ' +
                      ' WHERE EstadoID=2 AND TipoConceitoID=306 AND DestacaNota=1 AND IncidePreco=1 AND PaisID=' + nEmpresaData[1] +
			          ' ORDER BY Imposto ';
        }
    }
    else if (pastaID == 20122) // Financeiros
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT dbo.fn_Pessoa_Posicao(a.PessoaID, -' + nEmpresaData[0] + ', 9, GETDATE(), 0, NULL, NULL, NULL, NULL) AS SaldoDevedor, ' +
                             'dbo.fn_Pessoa_Posicao(a.PessoaID, -' + nEmpresaData[0] + ', 8, GETDATE(), 0, NULL, NULL, NULL, NULL) AS SaldoAtualizado ' +
					  'FROM Pessoas a WITH(NOLOCK) ' +
                      'WHERE a.PessoaID = ' + nRegistroID;
        }
    }
    // Relpessoas
    else if (pastaID == 20123) {
        if (dso.id == 'dso01GridLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            if (aValue[1] == 'SUJEITO') {
                dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                          'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                          'WHERE a.ObjetoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.SujeitoID = b.PessoaID ';
            }
            else if (aValue[1] == 'OBJETO') {
                dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                          'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                          'WHERE a.SujeitoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.ObjetoID = b.PessoaID ';
            }
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                          'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas b WITH(NOLOCK) ON a.ProprietarioID = b.PessoaID ' +
                          'WHERE ((a.SujeitoID = ' + nRegistroID + ') OR (a.ObjetoID = ' + nRegistroID + ') ) AND a.TipoRelacaoID = ' + aValue[0];

        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                          'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas b WITH(NOLOCK) ON a.AlternativoID = b.PessoaID ' +
                          'WHERE ((a.SujeitoID = ' + nRegistroID + ') OR (a.ObjetoID = ' + nRegistroID + ') ) AND a.TipoRelacaoID = ' + aValue[0];

        }
        else if (dso.id == 'dsoStateMachineLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            if (aValue[1] == 'SUJEITO') {
                dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                          'FROM RelacoesPessoas a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                          'WHERE a.ObjetoID = ' + nRegistroID +
                          ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.EstadoID = b.RecursoID ';
            }
            else if (aValue[1] == 'OBJETO') {
                dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                          'FROM RelacoesPessoas a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                          'WHERE a.SujeitoID = ' + nRegistroID +
                          ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.EstadoID = b.RecursoID ';
            }
        }
    }
    else if (pastaID == 20124) // Rel com recurso
    {
        if (dso.id == 'dso01GridLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM RelacoesPesRec a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.SujeitoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                      'AND a.ObjetoID = b.RecursoID ';
        }
        else if (dso.id == 'dsoStateMachineLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM RelacoesPesRec a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.SujeitoID = ' + nRegistroID +
                      ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                      'AND a.EstadoID = b.RecursoID ';
        }
    }
    else if (pastaID == 20125) // Rel com Conceito
    {
        if (dso.id == 'dso01GridLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            dso.SQL = 'SELECT b.ConceitoID, b.Conceito ' +
                      'FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                      'WHERE a.SujeitoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                      'AND a.ObjetoID = b.ConceitoID ';
        }
        else if (dso.id == 'dsoStateMachineLkp') {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM RelacoesPesCon a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.SujeitoID = ' + nRegistroID +
                      ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                      'AND a.EstadoID = b.RecursoID ';
        }
    }
    else if (pastaID == 20126) // Referencias Comerciais
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT DISTINCT b.PessoaID, b.Fantasia ' +
                      'FROM Pessoas_RefComerciais a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE a.PessoaID = ' + nRegistroID + ' AND a.EmpresaID = b.PessoaID';
        }
        else if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName ' +
                'FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=30)';
        }
    }
    else if (pastaID == 20129) {
        if (dso.id == 'dsoStateMachineLkp') {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM Pessoas_Transportadoras a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.PessoaID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID';
        }
    }
    else if (pastaID == 20135) 
    {
        if (dso.id == 'dsoCmb01Grid_01') 
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                        'FROM RelacoesPesRec a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.SujeitoID) ' +
                       'WHERE (a.TipoRelacaoID = 12) AND (a.ObjetoID = 999) AND  ' +
                            '(a.EstadoID = 2) AND (b.EstadoID = 2) ' +
                       'ORDER BY Fantasia';
        }
    }
    // Parametros Marketplace
    else if (pastaID == 20137)
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                      'WHERE a.EstadoID=2 AND a.TipoID=816 ' +
                      'ORDER BY a.Ordem';
        }
    }
    // Cr�ditos
    else if (pastaID == 29094)
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT DISTINCT ISNULL(a.NivelPagamento, 1) AS NivelPagamento ' +
                            'FROM (SELECT a.NivelPagamento ' +
                                    'FROM Pessoas_Creditos a WITH(NOLOCK)) a ' +
                                    'WHERE(a.NivelPagamento <= (SELECT DISTINCT b.NivelPagamentoMaximo ' +
                                                                    'FROM (SELECT CONVERT(INT, ISNULL(dbo.fn_Direitos_Valor((' + nUsuarioID + '), (' + nEmpresaData[0] + '), NULL, 47, 0, GETDATE()), 1)) | 1 NivelPagamentoMaximo) b)) ' +
                            'ORDER BY ISNULL(a.NivelPagamento, 1)';
        }
        else if ((dso.id == 'dsoCmb02Grid_01') || (dso.id == 'dsoCmb03Grid_01')) {
            dso.SQL = 'SELECT DISTINCT 0 AS fldID,  SPACE(0) AS fldName,  0 AS Ordem UNION ALL ' +
                'SELECT DISTINCT b.FinanciamentoID AS fldID, b.Financiamento AS fldName, b.Ordem AS Ordem ' +
                'FROM Pessoas_Creditos a WITH(NOLOCK), FinanciamentosPadrao b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK) ' +
                'WHERE (a.PessoaID = ' + nPessoaID + ' AND a.FinanciamentoLimiteID = b.FinanciamentoID AND ' +
                'c.SujeitoID = ' + nEmpresaData[0] + ' AND c.TipoRelacaoID = 12 AND c.ObjetoID = 999) ' +
                'UNION ALL ' +
                'SELECT DISTINCT a.FinanciamentoID AS fldID, a.Financiamento AS fldName, a.Ordem AS Ordem ' +
                'FROM FinanciamentosPadrao a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), RelacoesPesRec_FinanciamentosPadrao c WITH(NOLOCK) ' +
                'WHERE (a.EstadoID = 2 AND a.NumeroParcelas = 1 AND b.SujeitoID = ' + nEmpresaData[0] + ' AND b.TipoRelacaoID = 12 AND ' +
                'b.ObjetoID = 999 AND b.RelacaoID = c.RelacaoID AND c.EstadoID = 2 AND ' +
                'c.FinanciamentoID = a.FinanciamentoID AND ' +
                'c.NivelPagamento <= dbo.fn_Direitos_Valor(' + nUsuarioID + ', ' + nEmpresaData[0] + ', NULL, 47, 0, GETDATE()) AND ' +
                '(a.FinanciamentoID NOT IN (ISNULL((SELECT MAX(FinanciamentoLimiteID) ' +
                'FROM Pessoas_Creditos WITH(NOLOCK) ' +
                'WHERE (PessoaID = ' + nPessoaID + ' AND FinanciamentoLimiteID = a.FinanciamentoID)), 0)))) ' +
                'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb04Grid_01') {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem UNION ALL ' +
            'SELECT DISTINCT a.ItemID AS fldID, a.ItemAbreviado AS fldName, LTRIM(RTRIM(STR(a.Ordem))) AS Ordem ' +
            'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
            'WHERE(a.TipoID = 804 AND a.EstadoID = 2 AND a.Filtro LIKE \'%(1221)%\')' +
            'ORDER BY Ordem ';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf',1);

    var nClass = 0;
    var nEhMktplace = 0;

    //@@ Comeca modificar aqui
    
    nClass = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
        'dsoSup01.recordset[' + '\'' + 'ClassificacaoID' + '\'' + '].value');
    //Marketplace
    nEhMktplace = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
        'dsoSup01.recordset[' + '\'' + 'EhMarketplace' + '\'' + '].value');

 	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es
    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Propriet�rios
    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

	if ((nTipoRegistroID == 51) || (nTipoRegistroID == 52)) // Pess. fisica ou juridica
    {
        if (nTipoRegistroID == 52)
        {
	        vPastaName = window.top.getPastaName(20106);
		    if (vPastaName != '')
			    addOptionToSelInControlBar('inf', 1, vPastaName, 20106); //Contrato Social
        }

	    if ( (nTipoRegistroID == 52) && (nClass == 68) )
	    {
			vPastaName = window.top.getPastaName(20108);
			if (vPastaName != '')
				addOptionToSelInControlBar('inf', 1, vPastaName, 20108); //Transportadora
	    }
	    
		vPastaName = window.top.getPastaName(20110);
		if (vPastaName != '')
		    addOptionToSelInControlBar('inf', 1, vPastaName, 20110); //Documentos

        vPastaName = window.top.getPastaName(20138);
		if (vPastaName != '')
		    addOptionToSelInControlBar('inf', 1, vPastaName, 20138); //Certid�es

		vPastaName = window.top.getPastaName(20111);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20111); //Endere�os

		vPastaName = window.top.getPastaName(20112);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20112); //Telefones

		vPastaName = window.top.getPastaName(20113);
		if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 20113); //URLs

        vPastaName = window.top.getPastaName(29094);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 29094); //Creditos
			
	}
	if (nTipoRegistroID == 51)
	{
		vPastaName = window.top.getPastaName(20114);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20114); //Forma��es
	}
	if ((nTipoRegistroID == 51) || (nTipoRegistroID == 52))
	{

        vPastaName = window.top.getPastaName(20115);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20115); //Isen��es

        vPastaName = window.top.getPastaName(20126);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20126); //Referencias Comerciais
			
        if (nTipoRegistroID == 51) 
        {
            vPastaName = window.top.getPastaName(20127);
		    if (vPastaName != '')
		    	addOptionToSelInControlBar('inf', 1, vPastaName, 20127); //Empresas

            vPastaName = window.top.getPastaName(20130);
		    if (vPastaName != '')
		    	addOptionToSelInControlBar('inf', 1, vPastaName, 20130); //Senhas		    	
		}
		if ( (nTipoRegistroID == 52) && (nClass == 68) ) // Dados de Transporte
        {
            vPastaName = window.top.getPastaName(20129);
		    if (vPastaName != '')
		    	addOptionToSelInControlBar('inf', 1, vPastaName, 20129); //Dados de Transporte
		}
		
		vPastaName = window.top.getPastaName(20128);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20128); //Contatos

		vPastaName = window.top.getPastaName(20120);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20120); //Pedidos

		vPastaName = window.top.getPastaName(20121);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20121); //Produtos

		vPastaName = window.top.getPastaName(20122);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20122); //Financeiros
	}
	if (nTipoRegistroID <= 52)
    {
        vPastaName = window.top.getPastaName(20132);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 20132); //Atendimento            

        vPastaName = window.top.getPastaName(32033);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 32033); //Dados Banc�rios            
            
        vPastaName = window.top.getPastaName(20131);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 20131); //Visitas
    }


    //Heraldo Grid
    if (nTipoRegistroID <= 54) 
    {
        vPastaName = window.top.getPastaName(20135);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 20135); //Empresas 
    }

    vPastaName = window.top.getPastaName(20123);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20123); //Relacao Entre pessoas

	if ((nTipoRegistroID == 51) || (nTipoRegistroID == 52))
    {
        vPastaName = window.top.getPastaName(20124);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20124); //Relacao com Recurso
    }

	if (nTipoRegistroID == 52)
    {
        vPastaName = window.top.getPastaName(20125);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20125); //Relacao com Conceito
    }
   
    //Programas de Marketing
	if ((nTipoRegistroID == 55) && (nEhMktplace == 1))
	{
	    vPastaName = window.top.getPastaName(20137);
	    if (vPastaName != '')
	        addOptionToSelInControlBar('inf', 1, vPastaName, 20137); //Parametros Marketplace
	}    
    
    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia ao dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    var currCmb2Data;
    var aValue;
    

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 20110) //Documentos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 3], ['PessoaID', registroID]);

        else if (folderID == 20138) //Certid�es
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 3, 4, 5, 6, 7, 8, 9], ['PessoaID', registroID]);

        else if (folderID == 32033) //Dados Banc�rios
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1,2,3,5], ['PessoaID', registroID]);
            
        else if (folderID == 20111) //Enderecos
        {
		    var nColumnAddress = 8;

            if (!((dso03GridLkp.recordset.BOF)&&(dso03GridLkp.recordset.EOF)))
		    {
		    	dso03GridLkp.recordset.moveFirst();
		        if (dso03GridLkp.recordset['EndInvertido'].value == true)
		        	nColumnAddress = 9;
		    }

            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[3,4,nColumnAddress,13],['PessoaID',registroID], [12,13]);
        }    
        else if (folderID == 20112) //Telefones
        {                        
            if (fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'DDI')) == 55)
            {
                if (fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'DDD')).length != 2) 
                {
                    if (window.top.overflyGen.Alert('A coluna DDD deve conter 2 (dois) d�gitos.') == 0 )
                        return null;

                    currLine = -1;
                }
            }
            
            if (currLine == 0)
                currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[0,4],['PessoaID',registroID]);           
        }
        else if (folderID == 20113) //URLs
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[0,2],['PessoaID',registroID]);
        else if (folderID == 20114) //Formacoes
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[1],['PessoaID',registroID]);
        else if (folderID == 20115) //Isen��es
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[0],['PessoaID',registroID]);
        else if ( (folderID == 20123) || (folderID == 20124) || (folderID == 20125) ) //Relacoes entre pessoas/Relacoes com recurso/Relacoes com Conceito
        {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            
            fg.TextMatrix(keepCurrLineInGrid(), 4) = aValue[0];
            
            if (aValue[1] == 'SUJEITO')
                currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[2,4,5],['ObjetoID',registroID],[4,5]);
            else if (aValue[1] == 'OBJETO')
                currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[2,4,5],['SujeitoID',registroID],[4,5]);
        }
        else if (folderID == 20126) //Referencias Comerciais
        {
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[0,1,2],['PessoaID',registroID],[12]);
        }
        else if (folderID == 20129) //Dados de transporte
        {
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[1,3,4,5,6],['PessoaID',registroID],[]);
        }
        
        else if (folderID == 20130) //Senhas
        {
            currLine = criticLineGridAndFillDsoEx(fg,currDSO,keepCurrLineInGrid(),[5],['PessoaID',registroID],[]);
        }

        else if (folderID == 20135) //Empresas
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['PessoaID', registroID], []);
        }
        else if (folderID == 20137) //Param Marketplace
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['PessoaID', registroID], []);
        }
        else if (folderID == 29094) //Creditos
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['PessoaID', registroID], []);
        }

        if (currLine < 0)
        {
            if (fg.disabled == false)
            {
				try
                {
                    fg.Editable = true;
					window.focus();
					fg.focus();
				}	
                catch(e)
                {
					;
                }
            }    
        
            return false;
        }    
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var currCmb2Data;
    var aValue;
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    var nEmpresaData = getCurrEmpresaData();

    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'Campo',
                       'De',
                       'Para',
                       'LOGID'], [9]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'Campo',
                                 'Conteudo',
                                 'Mudanca',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '', '', '', '']);

        alignColsInGrid(fg, [9]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 20110) // Documentos
    {
        headerGrid(fg,['Pa�s',
                       'Documento',
                       'UF',
                       'N�mero',
                       'Org�o Emissor',
                       'Emiss�o1',
                       'Emiss�o',
                       'Validade',
                       'Observa��o',
                       'Documento_Estadual',
                       'PesDocumentoID'], [9,10]);

        
        fillGridMask(fg,currDSO,['PaisID',
                                 'TipoDocumentoID',
                                 'UFID',
                                 'Numero',
                                 'OrgaoEmissor',
                                 'dtEmissao1',
                                 'dtEmissao',
                                 'dtValidade',
                                 'Observacao',
                                 '^TipoDocumentoID^dso02GridLkp^TipoDocumentoID^Documento_Estadual*',
                                 'PesDocumentoID'],
                                 ['','','','','','99/99/9999','99/99/9999','99/99/9999','',''],
                                 ['','','','','',dTFormat,dTFormat,dTFormat,'','']);

        //pinta celulas readonly
        currDSO.recordset.moveFirst();

        var i;
        var nColUF = getColIndexByColKey(fg, 'UFID');
       
        for (i = 1; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TipoDocumentoID')) != 112) 
            {
                fg.Cell(6, i, nColUF, i, nColUF) = 0XDCDCDC; // cinza
            }
        }

        alignColsInGrid(fg);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 20111) // Enderecos
    {
		var bEndInvertido = false;
		var sLabel1 = 'Endere�o';
		var sCampo1 = 'Endereco';
		var sLabel2 = 'N�mero';
		var sCampo2 = 'Numero';

        if (!((dso03GridLkp.recordset.BOF)&&(dso03GridLkp.recordset.EOF)))
		{
			dso03GridLkp.recordset.moveFirst();
			bEndInvertido = dso03GridLkp.recordset['EndInvertido'].value;
		}

		if (bEndInvertido)
		{
			sLabel1 = 'N�mero';
			sCampo1 = 'Numero';
			sLabel2 = 'Endere�o';
			sCampo2 = 'Endereco';
		}

        headerGrid(fg,['Ordem',
                       'Fat',
                       'Cob',
                       'Pa�s',
                       'CEP',
                       'UF',
                       'Cidade',
                       'Bairro',
                       sLabel1,
                       sLabel2,
                       'Complemento',
                       'Observa��o',
                       'UF',
                       'Cidade',
                       'PesEnderecoID'],[12,13,14]);
                       
        fillGridMask(fg,currDSO,['Ordem',
                                 'EndFaturamento',
                                 'EndCobranca',
                                 'PaisID',
                                 'CEP',
                                 '^UFID^dso01GridLkp^LocalidadeID^CodigoLocalidade2',
                                 '^CidadeID^dso02GridLkp^LocalidadeID^Localidade',
                                 'Bairro',
								 sCampo1,
								 sCampo2,
                                 'Complemento',
                                 'Observacao',
                                 'UFID*',
                                 'CidadeID*',
                                 'PesEnderecoID'],['99','','','','','','','','','','','','']);

		// A mascara que estava no CEP eh a seguinte:                                 
		// 'aaaaaaaaa', foi suspensa temporariamente para implantacao
		// na allplus

        alignColsInGrid(fg,[0]);                           
        fg.FrozenCols = 1;
    }
    else if (folderID == 20112) // Telefones
    {
        headerGrid(fg,['Tipo',
                       'Ordem',
                       'DDI',
                       'DDD',
                       'N�mero',
                       'Observa��o',
                       'Publica',
                       'PesTelefoneID'],[7]);
        
        fillGridMask(fg,currDSO,['TipoTelefoneID',
                                   'Ordem',
                                   'DDI',
                                   'DDD',
                                   'Numero',
                                   'Observacao',
                                   'Publica',
                                   'PesTelefoneID'],['','99','9999','9999','999999999','','','']);
        
        glb_aCelHint = [[0, 6, 'Publica o telefone da pessoa?']];
    }
    else if (folderID == 20113) // URLs
    {
        headerGrid(fg,['Tipo',
                       'Ordem',
                       'URL',
                       'Observa��o',
                       'NFe',
                       'PesURLID'],[5]);
                       
        fillGridMask(fg,currDSO,['TipoURLID',
                                   'Ordem',
                                   'URL',
                                   'Observacao',
                                   'NFe',
                                   'PesURLID'], ['', '99', '', '', '', '']);

        glb_aCelHint = [[0, 4, 'Esta URL receber� e-mail da nota fiscal eletr�nica?']];
    }
    else if (folderID == 20114) // Formacoes
    {
        headerGrid(fg,['Ordem',
                       'Forma��o',
                       'Escola',
                       'Ano',
                       'Pa�s',
                       'Observa��o',
                       'PesFormacaoID'],[6]);
                       
        fillGridMask(fg,currDSO,['Ordem',
                                  'Formacao',
                                  'Escola', 
                                  'Ano',
                                  'PaisID', 
                                  'Observacao',
                                  'PesFormacaoID'],['99','','','9999','','','']);
        fg.FrozenCols = 1;
        alignColsInGrid(fg,[3]);
    }
    else if (folderID == 20115) // Isen��es
    {
        headerGrid(fg,['Imposto',
						'Mensagem',
						'Data',
						'Vig�ncia In�cio',
						'Vig�ncia Fim',
						'Observa��o',
						'PesIsencaoID'],[6]);
                       
        fillGridMask(fg,currDSO,['ImpostoID',
                                  'Mensagem',
                                  'dtData',
                                  'dtVigenciaInicio',
                                  'dtVigenciaFim',
                                  'Observacao',
                                  'PesIsencaoID'],
                                  ['','','99/99/9999','99/99/9999','99/99/9999','',''],
                                  ['','',dTFormat,dTFormat,dTFormat,'','']);
        fg.FrozenCols = 1;
        alignColsInGrid(fg,[]);
    }
    else if (folderID == 20120) // Pedidos
    {
        headerGrid(fg,['Data',
                       'Pedido', 
                       'Est',
                       'Pessoa',
                       'Nota',
                       'Data Nota',
                       'CFOP',
                       'Moeda',
                       'Total',
                       'Vendedor',
                       'Empresa',
                       'Observa��o'],[]);
                       
        fillGridMask(fg,currDSO,['dtPedido',
                                 'PedidoID',
                                 'RecursoAbreviado',
                                 'Pessoa',
                                 'NotaFiscal',
                                 'dtNotaFiscal',
                                 'Codigo',
                                 'SimboloMoeda',
                                 'TotalPedido',
                                 'Vendedor',
                                 'Fantasia',
                                 'Observacao'],
                                 ['','','','','','','','','','','',''],
                                 [dTFormat,'','','','','','','','###,###,###.00','','','']);

        alignColsInGrid(fg,[1,4,8]);                           
        fg.FrozenCols = 2;

        // Linha de Totais
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[8,'###,###,###,###.00','S']]);
        
    }
    else if (folderID == 20121) // Produtos
    {
        headerGrid(fg,['Data',
                       'Pedido',
                       'Est',
                       'Pessoa',
                       'CFOP',
                       'ID',
                       'Produto',
                       'Quant',
                       'Moeda',
                       'Valor',
                       'Valor Total',
                       'Vendedor',
                       'Refer�ncia',
                       'Empresa',
                       'Observa��o'],[]);
                       
        fillGridMask(fg,currDSO,['dtMovimento',
                                 'PedidoID',
                                 'Estado',
                                 'Pessoa',
                                 'Codigo',
                                 'ConceitoID',
                                 'Conceito',
                                 'Quantidade',
                                 'SimboloMoeda',
                                 'Valor',
                                 'ValorTotal',
                                 'Vendedor',
                                 'PedidoReferencia',
                                 'Empresa',
                                 'Observacao'],
                                 ['','','','','','','','','','','','','','',''],
                                 [dTFormat,'','','','','','','','','###,###,##0.00','###,###,##0.00','','','','']);

        alignColsInGrid(fg,[1,5,7,9,10,12]);
        fg.FrozenCols = 2;

        // Linha de Totais
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[7,'#####0','S'],[10,'###,###,###,##0.00','S']]);
    }
    else if (folderID == 20122) // Financeiros
    {
        headerGrid(fg,['Vencimento',
					   'Atr',
                       'Financeiro',
                       'Est',
                       'Tipo',
                       'Pedido',
                       'CFOP',
                       'Pessoa',
                       'Duplicata',
                       'Emiss�o',
                       'Forma',
                       'Prazo',
                       'Moeda',
                       'Valor',
                       'Saldo',
                       'Saldo Atual',
                       'Empresa',
                       'Observa��o'],[]);
                       
        fillGridMask(fg,currDSO,['dtVencimento',
								 'Atraso',
                                 'FinanceiroID',
                                 'Estado',
                                 'TipoFinanceiro',
                                 'Pedido',
                                 'Codigo',
                                 'Pessoa',
                                 'Duplicata',
                                 'dtEmissao',
                                 'FormaPagamento',
                                 'PrazoPagamento',
                                 'Moeda',
                                 'Valor',
                                 'SaldoDevedor',
                                 'SaldoAtualizado',
                                 'Empresa',
                                 'Observacao'],
                                 ['','','','','','','','','','','','','','','','','',''],
                                 [dTFormat,'','','','','','','','',dTFormat,'','','',
                                 '###,###,###.00','###,###,###.00','###,###,###.00','','']);

        alignColsInGrid(fg,[1,2,11,13,14,15]);
        fg.FrozenCols = 3;

        // Linha de Totais
        gridHasTotalLine(fg, 'Totais (US$)', 0xC0C0C0, null, true, [[14,'###,###,###,##0.00','S']]);

        // Atualiza o valor Saldo Devedor e do Saldo Atualizado
        if (fg.Rows>2)
        {
            if (!((dso01GridLkp.recordset.BOF)&&(dso01GridLkp.recordset.EOF)))
            {
                dso01GridLkp.recordset.moveFirst();
                fg.TextMatrix(1, 14) = dso01GridLkp.recordset['SaldoDevedor'].value;
                fg.TextMatrix(1, 15) = dso01GridLkp.recordset['SaldoAtualizado'].value;
            }       
        }
        
    }
    else if (folderID == 20131) // Visitas
    {
        headerGrid(fg,['ID',
					   'Empresa',
                       'Est',
                       'Data Visita',
                       'Vencimento',
                       'Tipo Visita',
                       'Tipo Visitado',
                       'Vendedor',
                       'Observa��o'],[]);
                       
        fillGridMask(fg,currDSO,['VisitaID',
								 'Empresa',
                                 'Estado',
                                 'dtVisita',
                                 'dtVencimento',
                                 'TipoVisitaID',
                                 'TipoVisitadoID',
                                 'Vendedor',
                                 'Observacao'],
                                 ['','','','','','','','',''],
                                 ['','','',dTFormat,dTFormat,'','','','']);

        alignColsInGrid(fg,[0]);
        fg.FrozenCols = 3;       
        
    }
    else if (folderID == 20132) // Atendimento
    {
        dTFormat = 'dd/mm/yyyy hh:mm';
        
        headerGrid(fg,['Data',
					   'Empresa',
                       'Usu�rio',
                       'Atendimento',
                       'OK',
                       'Observa��es',
                       'Agendamento'],[]);
                       
        fillGridMask(fg,currDSO,['dtInicio',
								 'Empresa',
                                 'Usuario',
                                 'TipoAtendimentoID',
                                 'OK',
                                 'Observacoes',
                                 'dtAgendamento'],
                                 ['99/99/9999 99:99','','','','','','99/99/9999 99:99'],
                                 [dTFormat,'','','','','',dTFormat]);                                         
    }
    else if (folderID == 20138) // Certid�es
    {
        headerGrid(fg, ['Tipo',
                       'Matricula',
                       'Declara��o Nascido Vivo',
                       'N�mero',
                       'Livro',
                       'Folha',
                       'Cart�rio',
                       'Cidade',
                       'Local Nascimento',
                       'Emiss�o',
                       'PesCertidaoID'], [10]);


        fillGridMask(fg, currDSO, ['TipoCertidaoID',
                                   'Matricula',
                                   'DeclaracaoNascidoVivo',
                                   'Numero',
                                   'Livro',
                                   'Folha',
                                   'Cartorio',
                                   'CidadeID',
                                   'LocalNascimento',
                                   'dtEmissao',
                                   'PesCertidaoID'],
                                   ['', '', '', '', '', '', '', '', '', '99/99/9999', ''],
                                   ['', '', '', '', '', '', '', '', '',  dTFormat, '']);
                                  
        //pinta celulas readonly
        currDSO.recordset.moveFirst();
        alignColsInGrid(fg);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 32033) // Dados Banc�rios
    {
        headerGrid(fg, ['PessoaID',
                       'Banco',
                       'Tipo',
                       'Ag�ncia',
                       'DV',
                       'Conta',
                       'DV',
                       'Titular',
                       'DadosBancarios'], [0, 8]);

        fillGridMask(fg, currDSO, ['PessoaID',
                                 'BancoID',
                                 'TipoContaID',
                                 'Agencia',
                                 'AgenciaDV',
                                 'Conta',
                                 'ContaDV',
                                 'Titular',
                                 'PesDadosBancariosID'],
                                 ['','','','','','','',''],
                                 ['','','','','','','','']);
    }
//Heraldo Grid
    else if (folderID == 20135) // Empresas
    {
        headerGrid(fg, ['Empresa',
					   'C�digo do Provedor',
					   'PesEmpresaID'], [2]);

        fillGridMask(fg, currDSO, ['EmpresaID',
                                   'CodigoProvedor', 
                                   'PesEmpresaID'],
                                  ['', '',''],['', '','']);
    }    
       
    else if (folderID == 20123) // Rel entre pessoas
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        if (aValue[1] == 'SUJEITO')
        {
            headerGrid(fg,['ID',
                           'Est',
                            aValue[2],
                           '_calc_HoldKey_1',
                           'TipoRelacaoID',
                           aValue[2],
                           'Observa��o',
                           'Propriet�rio',
                           'Alternativo',
                           'RelacaoID'],[3,4,5,9]);
                           
            fillGridMask(fg,currDSO,['RelacaoID*',
                                     '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                     '^SujeitoID^dso01GridLkp^PessoaID^Fantasia*',
                                     '_calc_HoldKey_1',
                                     'TipoRelacaoID',
                                     'SujeitoID',
                                     'Observacao*',
                                     '^ProprietarioID^dso02GridLkp^PessoaID^Fantasia*',
                                     '^AlternativoID^dso03GridLkp^PessoaID^Fantasia*',
                                     'RelacaoID'],['','','','9','','','']);
        }
        else if (aValue[1] == 'OBJETO')
        {
            headerGrid(fg,['ID',
                           'Est',
                            aValue[2],
                           '_calc_HoldKey_1',
                           'TipoRelacaoID',
                           aValue[2],
                           'Observa��o',
                           'Propriet�rio',
                           'Alternativo',
                           'RelacaoID'],[3,4,5,9]);

            fillGridMask(fg,currDSO,['RelacaoID*',
                                     '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                     '^ObjetoID^dso01GridLkp^PessoaID^Fantasia*',
                                     '_calc_HoldKey_1',
                                     'TipoRelacaoID',
                                     'ObjetoID',
                                     'Observacao*',
                                     '^ProprietarioID^dso02GridLkp^PessoaID^Fantasia*',
                                     '^AlternativoID^dso03GridLkp^PessoaID^Fantasia*',
                                     'RelacaoID'],['','','','9','','','']);
        }
    }
    else if (folderID == 20124) // Rel com Recursos
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        headerGrid(fg,['ID',
                       'Est',
                        aValue[2],
                       '_calc_HoldKey_1',
                       'TipoRelacaoID',
                       aValue[2],
                       'RelacaoID'],[3,4,5,6]);
                               
        fillGridMask(fg,currDSO,['RelacaoID*',
                                 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                 '^ObjetoID^dso01GridLkp^RecursoID^RecursoFantasia*',
                                 '_calc_HoldKey_1',
                                 'TipoRelacaoID',
                                 'ObjetoID',
                                 'RelacaoID'],['','','','9','','']);
    }
    else if (folderID == 20125) // Rel com Conceitos
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        headerGrid(fg,['ID',
                       'Est',
                        aValue[2],
                       '_calc_HoldKey_1',
                       'TipoRelacaoID',
                       aValue[2],
                       'RelacaoID'],[3,4,5,6]);
                           
        fillGridMask(fg,currDSO,['RelacaoID*',
                                 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                 '^ObjetoID^dso01GridLkp^ConceitoID^Conceito*',
                                 '_calc_HoldKey_1',
                                 'TipoRelacaoID',
                                 'ObjetoID',
                                 'RelacaoID'],['','','','9','','']);
    }
    else if (folderID == 20126) // Referencias Comerciais
    {
        headerGrid(fg,['Data',
                       'Empresa',
                       'Contato',
                       'Cliente Desde',
                       '�ltima Compra',
                       'Valor',
                       'Maior Compra',
                       'Valor',
                       'Limite de Cr�dito',
                       'Prazo',
                       'Atraso',
                       'Conceito',
                       'EmpresaID',
                       'PesReferenciaID'],[12, 13]);

        fillGridMask(fg,currDSO,['dtReferencia',
                                 '^EmpresaID^dso01GridLkp^PessoaID^Fantasia*',
                                 'Contato',
                                 'dtInicio',
                                 'dtUltimaCompra',
                                 'ValorUltimaCompra',
                                 'dtMaiorCompra',
                                 'ValorMaiorCompra',
                                 'LimiteCredito',
                                 'Prazo',
                                 'Atraso',
                                 'ConceitoID',
                                 'EmpresaID',
                                 'PesReferenciaID'],
                                 ['99/99/9999', '', '', '99/99/9999', '99/99/9999', '999999999.99', '99/99/9999', '999999999.99',
                                  '999999999.99', '999', '999', '', '', ''],
                                 [dTFormat, '', '', dTFormat, dTFormat, '###,###,###.00', dTFormat, '###,###,###.00',
                                  '###,###,###.00', '###', '###', '', '', '']);

    }
    else if (folderID == 20129) // Dados de transporte
    {
        headerGrid(fg,['Est',
					   'In�cio',
					   'Fim',
					   'PCR',
					   'PCA',
					   'PBC',
					   'PBE',
					   'PBI',
					   'PBP',
					   'Ajuste Peso(%)',
					   'Ajuste Valor(%)',
					   'Doc Transporte(%)',
					   'Fat Transporte(%)',
					   'Observa��o',
					   'PesTransportadoraID'],[14]);

        glb_aCelHint = [[0,3,'Peso cubado rodovi�rio'],
                        [0,4,'Peso cubado a�reo'],
                        [0,5,'Peso Base Coleta'],
                        [0,6,'Peso Base Entrega'],
                        [0,7,'Peso Base Interioriza��o'],
                        [0,8,'Peso Base Ped�gio'],
                        [0,9,'Ajuste do peso(%)'],
                        [0, 10, 'Ajuste do valor(%)'],
                        [0, 11, 'Toler�ncia da diferen�a entre a parcela frete e o documento de transporte(%)'],
                        [0, 12, 'Toler�ncia da diferen�a entre as parcelas frete a fatura de transporte(%)']];

		fillGridMask(fg,currDSO,['^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
								 'dtInicio',
								 'dtFim',
								 'PesoCubadoRodoviario',
								 'PesoCubadoAereo',
								 'PesoBaseColeta',
								 'PesoBaseEntrega',
								 'PesoBaseInteriorizacao',
								 'PesoBasePedagio',
								 'PercentualAjustePeso',
								 'PercentualAjusteValor',
								 'PercentualVariacaoConhecimento',
								 'PercentualVariacaoFatura',
								 'Observacao',
								 'PesTransportadoraID'],
                                 ['', '99/99/9999', '99/99/9999', '999.99', '999.99', '999.99', '999.99', '999.99', '999.99', '999.99', '999.99','999.99','999.99','',''],
                                 ['', dTFormat, dTFormat, '###.00', '###.00', '###.00', '###.00', '###.00', '###.00', '###.00', '###.00','###.00','###.00','','']);

        alignColsInGrid(fg,[3,4,5,6,7,8,9,10,11,12]);
    }
    else if (folderID == 20127) // Empresas
    {
        headerGrid(fg,['Cargo',
                       'ID',
                       'Cliente',
                       'ID',
                       'Fornecedor'],[]);
                               
        fillGridMask(fg,currDSO,['Cargo*',
                                 'ClienteID*',
                                 'Cliente*',
                                 'FornecedorID*',
                                 'Fornecedor*'],['','','','','']);
        
        alignColsInGrid(fg,[1,3]);
    }
    else if (folderID == 20130) // Senhas
    {
        headerGrid(fg,['Data Senha',
                       'Senha',
                       'Tipo',
                       'Tentativas',
                       '�ltima Tentativa',
                       'Bloqueios',
                       'FT',
                       'PesSenhaID'],[7]);
                       
        glb_aCelHint = [[0,6,'For�ar troca?']];                       
                               
        fillGridMask(fg,currDSO,['dtSenha*',
                                   'Senha*',
                                   'Tipo*',
                                   'NumeroTentativas*',
                                   'dtUltimaTentativa*',
                                   'NumeroBloqueios*',
                                   'ForcarTroca',
                                   'PesSenhaID'],
                                   ['99/99/999 99:99:99','','','','99/99/999 99:99:99','','',''],
                                   [dTFormat + ' hh:mm:ss','','','',dTFormat + ' hh:mm:ss','','','']);
        
        alignColsInGrid(fg,[2,3,4]);
    }
    else if (folderID == 20128) // Contatos
    {
        headerGrid(fg,['ID',
                       'Contato',
                       'Cargo',
                       'Telefone',
                       'Email',
                       'Cliente',
                       'ID',
                       'Empresa',
                       'ContatoID'],[8]);

        fillGridMask(fg,currDSO,['ContatoID*',
                                 'Contato*',
                                 'Cargo*',
                                 'Telefone*',
                                 'Email*',
                                 'EhCliente*',
                                 'EmpresaID*',
                                 'Empresa*',
                                 'ContatoID'],['','','','','','','','','']);

        alignColsInGrid(fg,[0,6]);
    }

    else if (folderID == 20137) // Parametros Marketplace
    {
        headerGrid(fg, ['Parametro',
                       'Perc',
                       'PessoaID',
                       'PesParametroID'], [2,3]);

        fillGridMask(fg, currDSO, ['TipoParametroID',
                                 'ValorParametro',//
                                 'PessoaID',
                                 'PesParametroID'],
                                 ['', '999.99', '', ''],
                                 ['', '###.00', '', '']);

        alignColsInGrid(fg, [1]);
    }
    // Cr�ditos
    else if (folderID == 29094)
    {
        headerGrid(fg, ['Cr�dito',
            'Bloquear',
            'Fat Direto',
            'Compartilhar',
            'Nivel Pagamento',
            'Financ Padr�o',
            'Financ Limite',
            'Forma Pagamento',
            'Moeda',
            'Vencimento',
            'Cr�dito Empresa',
            'Cr�dito Seguradora',
            'Cr�dito Concedido',
            'Saldo Cr�dito',
            'Saldo Cr�dito Grupo',
            'Observa��o',
            'PesCreditoID'],[16]);

        glb_aCelHint = [[0, 2, 'Revenda est� autorizada a fazer faturamento direto ?'],
                        [0, 3, 'Compartilhar cr�dito com empresas do grupo econ�mico ?']];

        fillGridMask(fg, currDSO, ['Pais*',
            'Bloquear',
            'FaturamentoDireto',
            'Compartilhar',
            'NivelPagamento',
            'FinanciamentoPadraoID',
            'FinanciamentoLimiteID',
            'FormaPagamentoID',
            'Moeda*',
            'dtVencimento*',
            'CreditoEmpresa*',
            'CreditoSeguradora*',
            'CreditoConcedido*',
            'SaldoCredito*',
            'SaldoCreditoGrupo*',
            'Observacao',
            'PesCreditoID'],
            ['', '', '', '', '', '', '', '', '', '99/99/9999', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '', ''],
            ['', '', '', '', '', '', '', '', '', dTFormat, '###,###,###.00', '###,###,###.00', '###,###,###.00,', '###,###,###.00', '###,###,###.00', '', '']);

        fg.FrozenCols = 1;

    }

}
