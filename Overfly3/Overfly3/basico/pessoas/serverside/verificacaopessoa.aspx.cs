using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
    public partial class verificacaopessoa : System.Web.UI.OverflyPage
	{
		private string pessoaId = "";
		private string currEstadoId = "";
		private string newEstadoId = "";
		
		private string response = "";

		protected string nPessoaID
		{
			set { pessoaId = value != null ? value : ""; } 
		}
		
		protected string nCurrEstadoID
		{
            set { currEstadoId = value != null ? value : ""; } 
		}

		protected string nNewEstadoID
		{
            set { newEstadoId = value != null ? value : ""; }
		}
		
		protected void verificaPessoa()
		{
			// Executa procedure sp_Atendimento_Realiza
			ProcedureParameters[] aParams = new ProcedureParameters[4];

			aParams[0] = new ProcedureParameters(
				"@PessoaID",
				System.Data.SqlDbType.Int,
				pessoaId == null || pessoaId.Length == 0 ? 
					System.DBNull.Value :
					(Object)int.Parse(pessoaId));

			aParams[1] = new ProcedureParameters(
				"@EstadoDeID",
				System.Data.SqlDbType.Int,
				currEstadoId == null || currEstadoId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(currEstadoId));

			aParams[2] = new ProcedureParameters(
				"@EstadoParaID",
				System.Data.SqlDbType.Int,
				newEstadoId == null || newEstadoId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(newEstadoId));

			aParams[3] = new ProcedureParameters(
				"@Resultado",
				System.Data.SqlDbType.VarChar,
				System.DBNull.Value, 
				ParameterDirection.InputOutput);
			aParams[3].Length = 8000;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pessoa_Verifica", 
				aParams);

			response = aParams[3].Data.ToString();
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
            verificaPessoa();

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT " + ((response == null || response.Length == 0) ? "NULL" : 
					"'" + response + "'") + " AS Resultado"
				)
			);

		}
	}
}
