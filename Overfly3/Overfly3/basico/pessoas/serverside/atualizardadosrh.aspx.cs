using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class atualizardadosrh : System.Web.UI.OverflyPage
    {
        private int nLinha = 0;
        //private string e = "";
        private string erro = "";
        private string pessoaid = "";
        private string datafim = "";
        private string diascontrato = "";
        private string sindicato = "";
        private string contribuicaosindicalanual = "";
        private string tipoadmissao = "";
        private string horariodetrabalho = "";
        private string cargahoraria = "";
        private string adiantamentoquinzenal = "";
        private string areadeatuacao = "";
        private string numeroapoliceseguro = "";
        private string curso = "";
        private string semestre = "";
        private string instituicaoensino = "";
        private string agenteintegracao = "";
        private string supervisor = "";
        private string tiponecessidade = "";
        private string codigocid = "";

        private string result = "";
		
		public string sPessoaID
		{
            set { pessoaid = value != "" ? value : "NULL"; }
		}

        public string sDataFim
        {
            set { datafim = value != "" ? "'" + value + "'" : "NULL"; }
        }

        public string nDiasContrato
        {
            set { diascontrato = value != "" ? value : "NULL"; }
        }

        public string nSindicato
        {
            set { sindicato = value != "" ? value : "NULL"; }
        }

        public string bContribuicaoSindicalAnual
        {
            set { contribuicaosindicalanual  = value != "" ? value : "NULL"; }
        }

        public string nTipoAdmissao
        {
            set { tipoadmissao = value != "" ? value : "NULL"; }
        }

        public string sHorariodeTrabalho
        {
            set { horariodetrabalho = value != "" ? "'" + value + "'" : "NULL"; }
        }

        public string sCargaHoraria
        {
            set { cargahoraria = value != "" ? "'" + value + "'" : "NULL"; }
        }

        public string bAdiantamentoQuinzenal
        {
            set { adiantamentoquinzenal = value != "" ? value : "NULL"; }
        }

        public string sAreadeAtuacao
        {
            set { areadeatuacao = value != "" ? "'" + value + "'" : "NULL"; }
        }

        public string sNumeroApoliceSeguro
        {
            set { numeroapoliceseguro = value != "" ? "'" + value + "'" : "NULL"; }
        }

        public string sCurso
        {
            set { curso = value != "" ? "'" + value + "'" : "NULL"; }
        }

        public string nSemestre
        {
            set { semestre = value != "" ? value : "NULL"; }
        }

        public string nInstituicaoEnsino
        {
            set { instituicaoensino = value != "" ? value : "NULL"; }
        }

        public string nSupervisor
        {
            set { supervisor = value != "" ? value : "NULL"; }
        }

        public string nAgenteIntegracao
        {
            set { agenteintegracao = value != "" ? value : "NULL"; }
        }

        public string nTipoNecessidade
        {
            set { tiponecessidade = value != "" ? value : "NULL"; }
        }

        public string sCodigoCID
        {
            set { codigocid = value != "" ? "'" + value + "'" : "NULL"; }
        }

        /** Atualiza a tabela Pessoas_DadosRH. */
		private void atualizaDadosRH()
		{
            string sql = "";            
            
            System.Data.DataSet resultDataSet;

            sql = "SELECT PesDadosRHID FROM Pessoas_DadosRH WITH(NOLOCK) WHERE PessoaID = " + pessoaid; 
            resultDataSet = DataInterfaceObj.getRemoteData(sql);

            string nPesDadosRHID;

            try
            {
                nPesDadosRHID = resultDataSet.Tables[1].Rows[0]["PesDadosRHID"].ToString();
            }
            catch (System.Exception E)
            {
                nPesDadosRHID = null;
            }

            if (nPesDadosRHID == null)
            {

            //insert
                 sql =  "INSERT INTO Pessoas_DadosRH (PessoaID,dtContratoFim,DiasContratoID,SindicatoID,ContribuicaoSindicalAnual,TipoAdmissaoID,HorarioTrabalho,CargaHoraria,AdiantQuinzenal, " +
                                                "AreaAtuacao,ApoliceSeguro,CursoEstagio,SemestreCurso,InstituicaoCursoID,AgenteIntegracaoID,SupervisorID,TipoNecessidadeID,CodigoCID) " +
                        "SELECT " + pessoaid+ ", " + datafim + "," + diascontrato + "," + sindicato + "," + contribuicaosindicalanual + ","  + tipoadmissao + "," + horariodetrabalho + "," + 
                                    cargahoraria + "," + adiantamentoquinzenal + "," + areadeatuacao + "," + numeroapoliceseguro  + "," + curso + "," + semestre + "," + instituicaoensino + "," + 
                                    agenteintegracao + "," + supervisor + "," + tiponecessidade + "," + codigocid; 

            }
            else {
                //update
                sql = "UPDATE Pessoas_DadosRH " +
                           "SET dtContratoFim = " + datafim +
                           ",DiasContratoID = " + diascontrato +
                           ",SindicatoID = " + sindicato +
                           ",ContribuicaoSindicalAnual = " + contribuicaosindicalanual +
                           ",TipoAdmissaoID = " + tipoadmissao +
                           ",HorarioTrabalho = " + horariodetrabalho +
                           ",CargaHoraria = " + cargahoraria +
                           ",AdiantQuinzenal = " + adiantamentoquinzenal +
                           ",AreaAtuacao = " + areadeatuacao +
                           ",ApoliceSeguro = " + numeroapoliceseguro +
                           ",CursoEstagio = " + curso +
                           ",SemestreCurso = " + semestre +
                           ",InstituicaoCursoID = " + instituicaoensino +
                           ",AgenteIntegracaoID = " + agenteintegracao +
                           ",SupervisorID = " + supervisor +
                           ",TipoNecessidadeID = " + tiponecessidade +
                           ",CodigoCID = " + codigocid +
                        " WHERE PesDadosRHID = " + nPesDadosRHID;
            }

            nLinha = DataInterfaceObj.ExecuteSQLCommand(sql);
		}
	
		/** Evento de carregamento do p�gina. */
		protected override void PageLoad(object sender, EventArgs e)
		{


            atualizaDadosRH();

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
                    "select " + nLinha + " as Resultado"
				)
			);
		}
	}
}
