using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class verificacaocredito : System.Web.UI.OverflyPage
	{
		private string empresaId;
		private string userId;
		private string limiteCredito;
		private string pessoaId;

		public string nEmpresaID
		{
			set { empresaId = value != null ? value : "0"; }
		}

		public string nUserID
		{
			set { userId = value != null ? value : "0"; }
		}

		public string nLimiteCredito
		{
			set { limiteCredito = value != null ? value : "0"; }
		}

		public string nPessoaID
		{
			set { pessoaId = value != null ? value : "0"; }
		}

		private DataSet gerarResultado()
		{
			return DataInterfaceObj.getRemoteData(
				"SELECT dbo.fn_Direitos_Verifica(" + userId + ", " +
					empresaId + ", " +
					pessoaId + ", 36, " +
					limiteCredito + ", GETDATE()) AS OK, " +
				"CONVERT(VARCHAR(12), ISNULL(dbo.fn_Direitos_Valor(" + userId + ", " +
					empresaId + ", " +
					pessoaId + ", 36, 0, GETDATE()), 0)) AS ValorMaximo ");
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(gerarResultado());
		}
	}
}
