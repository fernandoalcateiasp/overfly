
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew
Dim nPessoaID, nPosicaoFila
Dim rsCommand
Dim nResponse
Dim i

nPessoaID = 0
nPosicaoFila = 0

For i = 1 To Request.QueryString("nPessoaID").Count    
    nPessoaID = Request.QueryString("nPessoaID")(i)
Next

Set rsCommand = Server.CreateObject("ADODB.Command")

With rsCommand
    .CommandTimeout = 60 * 10
	.ActiveConnection = strConn
	.CommandText = "sp_Pessoa_SolicitacaoCadastro"
	.CommandType = adCmdStoredProc
	
    .Parameters.Append( .CreateParameter("@PessoaID", adInteger, adParamInput) )
	.Parameters("@PessoaID").Value = CLng(nPessoaID)
	
	.Parameters.Append( .CreateParameter("@PosicaoFila", adInteger, adParamOutput) )

	.Execute
	
	nPosicaoFila = rsCommand.Parameters("@PosicaoFila").Value

End With

Set rsCommand = Nothing
    
Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "Resultado", adVarChar, 8000, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew

If ( NOT IsNull(nResponse)) Then
    If (NOT nPosicaoFila = 0) Then
        rsNew.Fields("Resultado").Value = "Solicita��o de cadastro realizada. " & chr(13) & chr(10) & _
	        "Posi��o na fila: " & CStr(nPosicaoFila)
	Else
	    rsNew.Fields("Resultado").Value = "Pessoa j� est� com seus dados confirmados."
    End If
End If





rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
%>
