using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
    public partial class gravarAtributo : System.Web.UI.OverflyPage
    {
        private string UsuarioID = null;//Constants.INT_ZERO;
        protected string nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        private string FormID = null;// Constants.INT_ZERO;
        protected string nFormID
        {
            get { return FormID; }
            set { FormID = value; }
        }

        private string SubFormID = null;//Constants.INT_ZERO;
        protected string nSubFormID
        {
            get { return SubFormID; }
            set { SubFormID = value; }
        }

        private string[] RegistroID = null;//Constants.INT_EMPTY_SET;
        protected string[] nRegistroID
        {
            get { return RegistroID; }
            set { RegistroID = value; }
        }

        private string[] TipoAtributoID = null;//Constants.INT_EMPTY_SET;
        protected string[] nTipoAtributoID
        {
            get { return TipoAtributoID; }
            set { TipoAtributoID = value; }
        }

        private string[] TipoAlteracao;//Constants.INT_EMPTY_SET;
        protected string[] nTipoAlteracao
        {
            get { return TipoAlteracao; }
            set { TipoAlteracao = value; }
        }

        private string resultado = null;//"";
        protected string Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }

        private string pessoaid = null;
        protected string PessoaID
        {
            get { return pessoaid; }
            set { pessoaid = value; }
        }

        protected void Gravar()
        {
            if (nTipoAlteracao[0] == "1")
            {
                ProcedureParameters[] param = new ProcedureParameters[] {
				    new ProcedureParameters("@FormID", SqlDbType.Int, DBNull.Value),
				    new ProcedureParameters("@SubFormID", SqlDbType.Int, DBNull.Value),
				    new ProcedureParameters("@RegistroID", SqlDbType.Int, DBNull.Value),
				    new ProcedureParameters("@Data", SqlDbType.DateTime, DBNull.Value),
				    new ProcedureParameters("@UsuarioID", SqlDbType.Int, DBNull.Value),
				    new ProcedureParameters("@TipoAtributoID", SqlDbType.Int, DBNull.Value),
				    new ProcedureParameters("@Valor", SqlDbType.VarChar, DBNull.Value, 30),
				    new ProcedureParameters("@Observacao", SqlDbType.VarChar, DBNull.Value, 30),
				    new ProcedureParameters("@Resultado", SqlDbType.VarChar, DBNull.Value, ParameterDirection.Output, 8000)
			    };

                for (int i = 0; i < nRegistroID.Length; i++)
                {
                    param[0].Data = nFormID;//.intValue();
                    param[1].Data = nSubFormID;//.intValue();0
                    param[2].Data = nRegistroID[i];//.intValue();
                    //param[3].Data = null; 
                    param[4].Data = nUsuarioID;//.intValue();
                    param[5].Data = nTipoAtributoID[i];//.intValue();
                    //param[6].Data = null;
                    //param[7].Data = null;
                    //param[8].Data = null;

                    DataInterfaceObj.execNonQueryProcedure("sp_Atributo_Gerador", param);

                    Resultado = param[8].Data.ToString();
                    PessoaID = nRegistroID[i];
                }
            }
            else
            {
                for (int i = 0; i < nRegistroID.Length; i++)
                {
                    int retorno = DataInterfaceObj.ExecuteSQLCommand(
                        "DELETE FROM Atributos WHERE FormID = " + nFormID + " AND SubFormID = " + nSubFormID +
                            " AND RegistroID = " + nRegistroID[i] + " AND TipoAtributoID = " + nTipoAtributoID[i]
                    );
                    if (retorno == 1)
                        Resultado = "";
                    else
                        Resultado = " n�o possui este atributo.";
                        
                    PessoaID = nRegistroID[i];
                }
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            Gravar();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "SELECT " + (Resultado != "" ? "'" + Resultado + "' " : "NULL") + " AS Resultado , " +
                    (PessoaID != "" ? "'" + PessoaID + "' " : "NULL") + " AS PessoaID "
            ));
        }
    }
}
