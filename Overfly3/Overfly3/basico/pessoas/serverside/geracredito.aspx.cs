﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
    public partial class geracredito : System.Web.UI.OverflyPage
    {

        private string sMensagem;

        private Integer ParceiroID;
        public Integer nParceiroID
        {
            get { return ParceiroID; }
            set { ParceiroID = value; }
        }

        private Integer PaisID;
        public Integer nPaisID
        {
            get { return PaisID; }
            set { PaisID = value; }
        }

        private Integer MoedaID;
        public Integer nMoedaID
        {
            get { return MoedaID; }
            set { MoedaID = value; }
        }

        private String ValorSolicitado;
        public String nValorSolicitado
        {
            get { return ValorSolicitado; }
            set { ValorSolicitado = value; }
        }

        private Integer SolicitanteID;
        public Integer nSolicitanteID
        {
            get { return SolicitanteID; }
            set { SolicitanteID = value; }
        }

        private String Seguradora;
        public String bSeguradora
        {
            get { return Seguradora; }
            set { Seguradora = value; }
        }

        private String Prioridade;
        public String bPrioridade
        {
            get { return Prioridade; }
            set { Prioridade = value; }
        }

        private String Motivo;
        public String sMotivo
        {
            get { return Motivo; }
            set { Motivo = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {

            ProcedureParameters[] procParams = new ProcedureParameters[9];

            procParams[0] = new ProcedureParameters(
            "@ParceiroID",
            System.Data.SqlDbType.Int,
            ParceiroID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(ParceiroID.ToString()));

            procParams[1] = new ProcedureParameters(
            "@PaisID",
            System.Data.SqlDbType.Int,
            PaisID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(PaisID.ToString()));

            procParams[2] = new ProcedureParameters(
            "@MoedaID",
            System.Data.SqlDbType.Int,
            MoedaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(MoedaID.ToString()));

            procParams[3] = new ProcedureParameters(
            "@ValorSolicitado",
            System.Data.SqlDbType.Money,
            ValorSolicitado != null ? (Object)double.Parse(ValorSolicitado) : DBNull.Value);

            procParams[4] = new ProcedureParameters(
            "@SolicitanteID",
            System.Data.SqlDbType.Int,
            SolicitanteID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(SolicitanteID.ToString()));

            procParams[5] = new ProcedureParameters(
            "@Seguradora",
            System.Data.SqlDbType.Bit,
            (Seguradora == "0") ? false : true);

            procParams[6] = new ProcedureParameters(
            "@Prioridade",
            System.Data.SqlDbType.Bit,
            (Prioridade == "0") ? false : true);

            procParams[7] = new ProcedureParameters(
            "@Motivo",
            System.Data.SqlDbType.VarChar,
            Motivo.ToString());

            procParams[8] = new ProcedureParameters(
            "@Mensagem",
            System.Data.SqlDbType.VarChar,
            DBNull.Value,
            ParameterDirection.InputOutput);
            procParams[8].Length = 8000;


            DataInterfaceObj.execNonQueryProcedure("sp_Credito_Gerador", procParams);

            if (procParams[8].Data != DBNull.Value)
                sMensagem += procParams[8].Data.ToString() + "\r \n";
            
            WriteResultXML(DataInterfaceObj.getRemoteData(
                 "select '" + sMensagem + "' as Mensagem"));
        }
    }
}