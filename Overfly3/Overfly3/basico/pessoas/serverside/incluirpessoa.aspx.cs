using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
    public partial class incluirpessoa : System.Web.UI.OverflyPage
    {
        private string erros = "";
        private string nome = "";
        private string tipoPessoaId;
        private string fantasia = "";
        private string clasId;
        private string cnae;
        private string documentoFederal = "";
        private string documentoEstadual = "";
        private string faturamentoAnual;
        private string numeroFuncionarios;
        private string dtNascimento;
        private string paisId = "0";
        private string cep;
        private string ufId;
        private string cidadeId;
        private string bairro;
        private string endereco;
        private string numero;
        private string complemento;
        private string observacao;
        private string ddi = "";
        private string ddd = "";
        private string tel = "";
        private string email = "0";
        private string emailnfe = "0";
        private string userId = "0";
        private string relacionadoId;
        private string tipoCadastro = "2";
        private string resposta = null;

        protected string sNome
        {
            set { nome = value != null ? value : ""; }
        }

        public string nTipoPessoaID
        {
            set { tipoPessoaId = value != null ? value : "0"; }
        }

        public string sFantasia
        {
            set { fantasia = value != null ? value : ""; }
        }

        public string nClasID
        {
            set { clasId = value != null ? value : "0"; }
        }

        public string sCNAE
        {
            set { cnae = value; }
        }

        public string sDocumentoFederal
        {
            set { documentoFederal = value != null ? value : ""; }
        }

        public string sDocumentoEstadual
        {
            set { documentoEstadual = value; }
        }

        public string nFaturamentoAnual
        {
            set { faturamentoAnual = value; }
        }

        public string nNumeroFuncionarios
        {
            set { numeroFuncionarios = value; }
        }

        public string dDtNascimento
        {
            set { dtNascimento = value; }
        }

        public string nPaisID
        {
            set { paisId = value != null ? value : "0"; }
        }

        public string sCEP
        {
            set { cep = value; }
        }

        public string nUFID
        {
            set { ufId = value; }
        }

        public string nCidadeID
        {
            set { cidadeId = value; }
        }

        public string sBairro
        {
            set { bairro = value; }
        }

        public string sEndereco
        {
            set { endereco = value; }
        }

        public string sNumero
        {
            set { numero = value; }
        }

        public string sComplemento
        {
            set { complemento = value != null ? value : ""; }
        }

        public string sObservacao
        {
            set { observacao = value != null ? value : ""; }
        }

        public string sDDI
        {
            set { ddi = value != null ? value : ""; }
        }

        public string sDDD
        {
            set { ddd = value != null ? value : ""; }
        }

        public string sTel
        {
            set { tel = value != null ? value : ""; }
        }

        public string sEmail
        {
            set { email = value != null ? value : ""; }
        }

        public string sEmailNFe
        {
            set { emailnfe = value != null ? value : ""; }
        }

        public string nUserID
        {
            set { userId = value != null ? value : ""; }
        }

        public string nRelacionadoID
        {
            set { relacionadoId = value != null ? value : ""; }
        }

        public string nTipoCadastro
        {
            set { tipoCadastro = value != null ? value : "2"; }
        }

        /** Roda a procedure sp_Pessoa_Cadastro2 */
        private void pessoaCadastro2()
        {
            try
            {
                ProcedureParameters[] parameters = new ProcedureParameters[37];

                parameters[0] = new ProcedureParameters(
                    "@TipoCadastro",
                    System.Data.SqlDbType.Int,
                    tipoCadastro);

                parameters[1] = new ProcedureParameters(
                    "@Nome",
                    System.Data.SqlDbType.VarChar,
                    nome);
                parameters[1].Length = 40;

                parameters[2] = new ProcedureParameters(
                    "@Fantasia",
                    System.Data.SqlDbType.VarChar,
                    fantasia);
                parameters[2].Length = 20;

                parameters[3] = new ProcedureParameters(
                    "@TipoPessoaID",
                    System.Data.SqlDbType.Int,
                    tipoPessoaId);

                parameters[4] = new ProcedureParameters(
                    "@ClassificacaoID",
                    System.Data.SqlDbType.Int,
                    clasId != null ? (Object)clasId : DBNull.Value);

                parameters[5] = new ProcedureParameters(
                    "@CNAE",
                    System.Data.SqlDbType.Int,
                    cnae != null ? (Object)cnae : DBNull.Value);

                parameters[6] = new ProcedureParameters(
                    "@EnquadramentoEmpresaID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value);

                parameters[7] = new ProcedureParameters(
                    "@RegimeTributarioID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value);

                parameters[8] = new ProcedureParameters(
                    "@DocumentoFederal",
                    System.Data.SqlDbType.VarChar,
                    documentoFederal);
                parameters[8].Length = 20;

                parameters[9] = new ProcedureParameters(
                    "@DocumentoEstadual",
                    System.Data.SqlDbType.VarChar,
                    documentoEstadual);
                parameters[9].Length = 20;

                parameters[10] = new ProcedureParameters(
                    "@FaturamentoAnual",
                    System.Data.SqlDbType.Money,
                    faturamentoAnual != null ? (Object)double.Parse(faturamentoAnual) : DBNull.Value);

                parameters[11] = new ProcedureParameters(
                    "@NumeroFuncionarios",
                    System.Data.SqlDbType.Int,
                    numeroFuncionarios != null ? (Object)int.Parse(numeroFuncionarios) : DBNull.Value);

                parameters[12] = new ProcedureParameters(
                    "@dtNascimento",
                    System.Data.SqlDbType.Int,
                    dtNascimento != null ? (Object)DateTime.Parse(dtNascimento) : DBNull.Value);

                parameters[13] = new ProcedureParameters(
                    "@PaisID",
                    System.Data.SqlDbType.Int,
                    paisId);

                parameters[14] = new ProcedureParameters(
                    "@CEP",
                    System.Data.SqlDbType.VarChar,
                    cep != null ? (Object)cep : DBNull.Value);
                parameters[14].Length = 8;

                parameters[15] = new ProcedureParameters(
                    "@UFID",
                    System.Data.SqlDbType.Int,
                    ufId != null ? (Object)ufId : DBNull.Value);

                parameters[16] = new ProcedureParameters(
                    "@CidadeID",
                    System.Data.SqlDbType.Int,
                    cidadeId != null ? (Object)cidadeId : DBNull.Value);

                parameters[17] = new ProcedureParameters(
                    "@Bairro",
                    System.Data.SqlDbType.VarChar,
                    bairro != null ? (Object)bairro : DBNull.Value);
                parameters[17].Length = 20;

                parameters[18] = new ProcedureParameters(
                    "@Endereco",
                    System.Data.SqlDbType.VarChar,
                    endereco != null ? (Object)endereco : DBNull.Value);
                parameters[18].Length = 35;

                parameters[19] = new ProcedureParameters(
                    "@Numero",
                    System.Data.SqlDbType.VarChar,
                    numero != null ? (Object)numero : DBNull.Value);
                parameters[19].Length = 6;

                parameters[20] = new ProcedureParameters(
                    "@Complemento",
                    System.Data.SqlDbType.VarChar,
                    complemento != null ? (Object)complemento : DBNull.Value);
                parameters[20].Length = 20;

                parameters[21] = new ProcedureParameters(
                    "@Observacao",
                    System.Data.SqlDbType.VarChar,
                    observacao != null ? (Object)observacao : DBNull.Value);
                parameters[21].Length = 20;

                parameters[22] = new ProcedureParameters(
                    "@DDITelefone",
                    System.Data.SqlDbType.VarChar,
                    ddi);
                parameters[22].Length = 4;

                parameters[23] = new ProcedureParameters(
                    "@DDDTelefone1",
                    System.Data.SqlDbType.VarChar,
                    ddd);
                parameters[23].Length = 4;

                parameters[24] = new ProcedureParameters(
                    "@NumeroTelefone1",
                    System.Data.SqlDbType.VarChar,
                    tel);
                parameters[24].Length = 9;

                parameters[25] = new ProcedureParameters(
                    "@DDDTelefone2",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value);
                parameters[25].Length = 4;

                parameters[26] = new ProcedureParameters(
                    "@NumeroTelefone2",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value);
                parameters[26].Length = 9;

                parameters[27] = new ProcedureParameters(
                    "@Email",
                    System.Data.SqlDbType.VarChar,
                    email);
                parameters[27].Length = 80;

                parameters[28] = new ProcedureParameters(
                    "@EmailNFe",
                    System.Data.SqlDbType.VarChar,
                    email);
                parameters[28].Length = 80;

                parameters[29] = new ProcedureParameters(
                    "@Site",
                    System.Data.SqlDbType.VarChar,
                    DBNull.Value);
                parameters[29].Length = 80;

                parameters[30] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    userId);

                parameters[31] = new ProcedureParameters(
                    "@RelacionarComID",
                    System.Data.SqlDbType.Int,
                    (relacionadoId != null && relacionadoId.Length > 0) ?
                        (Object)relacionadoId :
                        DBNull.Value);

                parameters[32] = new ProcedureParameters(
                    "@CadastroOK",
                    System.Data.SqlDbType.Int,
                    DBNull.Value);

                parameters[33] = new ProcedureParameters(
                    "@Sexo",
                    System.Data.SqlDbType.Int,
                    DBNull.Value);

                parameters[34] = new ProcedureParameters(
                    "@EstadoCivilID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value);

                parameters[35] = new ProcedureParameters(
                    "@EhContribuinte",
                    System.Data.SqlDbType.Bit,
                    DBNull.Value);

                parameters[36] = new ProcedureParameters(
                    "@PessoaID",
                    System.Data.SqlDbType.Int,
                    DBNull.Value,
                   ParameterDirection.Output);

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_Pessoa_Cadastro2",
                    parameters);

                resposta = parameters[36].Data.ToString();
            }
            catch (Exception Erro)
            {
                erros = Erro.Message.ToString();
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            pessoaCadastro2();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + (resposta != null ? "'" + resposta + "' " : "NULL") +
                    " as PessoaID , " +
                   (erros != "" ? "'" + erros + "' " : "NULL") + " AS Erros "

                )
            );
        }
    }
}
