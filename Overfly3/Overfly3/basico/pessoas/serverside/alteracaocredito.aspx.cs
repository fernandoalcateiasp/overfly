using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class alteracaocredito : System.Web.UI.OverflyPage
	{
		private string[] servico;
        private string[] usuarioId;
        private string[] classificacaoId;
        private string[] observacao;
        private string[] faturamentoAnual;
        private string[] numeroFuncionarios;
        private string[] cb;
        private string[] cg;
        private string[] es;
        private string[] registroId;
        private string[] tipoId;
        private string[] dataValidade;
        private string[] potencialCredito;
        private string[] proprietarioId;
        private string[] tipoPessoaId;
        private string[] ddiTelefone;
        private string[] dddComercial;
        private string[] telComercial;
        private string[] dddFaxCelular;
        private string[] faxCelular;
        private string[] email;
        private string[] site;
		private Integer dataLen;

        protected Integer DataLen
        {
			set { dataLen = value; }
		}

        protected string[] nServico
        {
			set
			{
				servico = value;

				for (int i = 0; i < servico.Length; i++)
				{
					if (servico[i] == null || servico[i].Length == 0)
						servico[i] = "NULL";
				}
			}
		}

        protected string[] nUsuarioID
        {
			set
			{
				usuarioId = value;
		
				for (int i = 0; i < usuarioId.Length; i++)
				{
					if (usuarioId[i] == null || usuarioId[i].Length == 0)
						usuarioId[i] = "NULL";
				}
			}
		}

        protected string[] nClassificacaoID
        {
			set
			{
				classificacaoId = value; 
				
				for (int i = 0; i < classificacaoId.Length; i++)
				{
					if (classificacaoId[i] == null || classificacaoId[i].Length == 0)
						classificacaoId[i] = "NULL";
				}
			}
		}
        
        protected string[] sObservacao
        {
			set
			{
				observacao = value; 
			
				for (int i = 0; i < observacao.Length; i++)
				{
					if (observacao[i] == null || observacao[i].Length == 0)
						observacao[i] = "NULL";
					else
						observacao[i] = "'" + observacao[i] + "'";
				}
			}
		}

        protected string[] nFaturamentoAnual
        {
			set
			{
				faturamentoAnual = value; 
				
				for (int i = 0; i < faturamentoAnual.Length; i++)
				{
					if (faturamentoAnual[i] == null || faturamentoAnual[i].Length == 0)
						faturamentoAnual[i] = "NULL";
				}
			}
		}

        protected string[] nNumeroFuncionarios
        {
			set
			{
				numeroFuncionarios = value; 
				
				for (int i = 0; i < numeroFuncionarios.Length; i++)
				{
					if (numeroFuncionarios[i] == null || numeroFuncionarios[i].Length == 0)
						numeroFuncionarios[i] = "NULL";
				}
			}
		}
		
        protected string[] bCB
        {
			set
			{
				cb = value; 
				
				for (int i = 0; i < cb.Length; i++)
				{
					if (cb[i] == null || cb[i].Length == 0)
						cb[i] = "NULL";
				}
			}
		}
		
        protected string[] bCG
        {
			set
			{
				cg = value; 
				
				for (int i = 0; i < cg.Length; i++)
				{
					if (cg[i] == null || cg[i].Length == 0)
						cg[i] = "NULL";
				}
			}
		}

        protected string[] bES
        {
			set
			{
				es = value; 
				
				for (int i = 0; i < cg.Length; i++)
				{
					if (es[i] == null || es[i].Length == 0)
						es[i] = "NULL";
				}
			}
		}
        
        protected string[] nRegistroID
        {
			set
			{
				registroId = value; 
				
				for (int i = 0; i < cg.Length; i++)
				{
					if (registroId[i] == null || registroId[i].Length == 0)
						registroId[i] = "NULL";
				}
			}
		}
        
        protected string[] nTipoID
        {
			set
			{
				tipoId = value; 
				
				for (int i = 0; i < tipoId.Length; i++)
				{
					if (tipoId[i] == null || tipoId[i].Length == 0)
						tipoId[i] = "NULL";
				}
			}
		}

        protected string[] dtValidade
        {
			set
			{
				dataValidade = value;

				for (int i = 0; i < dataValidade.Length; i++)
				{
					if (dataValidade[i] == null || dataValidade[i].Length == 0)
						dataValidade[i] = "NULL";
					else
						dataValidade[i] = dataValidade[i];
				}
			}
		}

        protected string[] nPotencialCredito
        {
			set
			{
				potencialCredito = value; 
				
				for (int i = 0; i < potencialCredito.Length; i++)
				{
					if (potencialCredito[i] == null || potencialCredito[i].Length == 0)
						potencialCredito[i] = "NULL";
				}
			}
		}

        protected string[] nProprietarioID
        {
			set
			{
				proprietarioId = value; 
				
				for (int i = 0; i < proprietarioId.Length; i++)
				{
					if (proprietarioId[i] == null || proprietarioId[i].Length == 0)
						proprietarioId[i] = "NULL";
				}
			}
		}

        protected string[] sTipoPessoaID
        {
			set
			{
				tipoPessoaId = value; 
			
				for (int i = 0; i < tipoPessoaId.Length; i++)
				{
					if (tipoPessoaId[i] == null || tipoPessoaId[i].Length == 0)
						tipoPessoaId[i] = "NULL";
				}
			}
		}

        protected string[] sDDITelefone
        {
			set
			{
				ddiTelefone = value; 
				
				for (int i = 0; i < ddiTelefone.Length; i++)
				{
					if (ddiTelefone[i] == null || ddiTelefone[i].Length == 0)
						ddiTelefone[i] = "NULL";
					else
						ddiTelefone[i] = "'" + ddiTelefone[i] + "'";
				}
			}
		}

        protected string[] sDDDComercial
        {
			set
			{
				dddComercial = value; 
				
				for (int i = 0; i < dddComercial.Length; i++)
				{
					if (dddComercial[i] == null || dddComercial[i].Length == 0)
						dddComercial[i] = "NULL";
					else
						dddComercial[i] = "'" + dddComercial[i] + "'";
				}
			}
		}        

        protected string[] sTelComercial
        {
			set
			{
				telComercial = value; 
				
				for (int i = 0; i < telComercial.Length; i++)
				{
					if (telComercial[i] == null || telComercial[i].Length == 0)
						telComercial[i] = "NULL";
					else
						telComercial[i] = "'" + telComercial[i] + "'";
				}
			}
		}

        protected string[] sDDDFaxCelular
        {
			set
			{
				dddFaxCelular = value; 
				
				for (int i = 0; i < dddFaxCelular.Length; i++)
				{
					if (dddFaxCelular[i] == null || dddFaxCelular[i].Length == 0)
						dddFaxCelular[i] = "NULL";
					else
						dddFaxCelular[i] = "'" + dddFaxCelular[i] + "'";
				}
			}
		}

        protected string[] sFaxCelular
        {
			set
			{
				faxCelular = value; 
				
				for (int i = 0; i < faxCelular.Length; i++)
				{
					if (faxCelular[i] == null || faxCelular[i].Length == 0)
						faxCelular[i] = "NULL";
					else
						faxCelular[i] = "'" + faxCelular[i] + "'";
				}
			}
		}

        protected string[] sEmail
        {
			set
			{
				email = value; 
				
				for (int i = 0; i < faxCelular.Length; i++)
				{
					if (email[i] == null || email[i].Length == 0)
						email[i] = "NULL";
					else
						email[i] = "'" + email[i] + "'";
				}
			}
		}

        protected string[] sSite
        {
			set
			{
				site = value; 
				
				for (int i = 0; i < site.Length; i++)
				{
					if (site[i] == null || site[i].Length == 0)
						site[i] = "NULL";
					else
						site[i] = "'" + site[i] + "'";
				}
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			string sql = "";
            int currTipoID = 0;

            for (int i = 0; i < dataLen.intValue(); i++)
            {
                currTipoID = int.Parse(tipoId[i]);

                if (currTipoID == 1)
                {
                    //Pessoas
                    if (int.Parse(servico[i]) == 1)
                    {
                        sql = "UPDATE Pessoas SET ClassificacaoID=" + classificacaoId[i] + ", FaturamentoAnual=" +
                            faturamentoAnual[i] + ", NumeroFuncionarios=" + numeroFuncionarios[i] + "," +
                            " UsuarioID=" + usuarioId[i] + "," +
                            "ProprietarioID= " + proprietarioId[i] + " " +
                            "WHERE PessoaID=" + registroId[i] + " " +
                            "EXEC sp_Pessoa_Cadastro2 3, NULL, NULL, " + tipoPessoaId[i] + ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " +
                            "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " + ddiTelefone[i] + ", " + dddComercial[i] + ", " +
                            telComercial[i] + " " + ", " + dddFaxCelular[i] + ", " + faxCelular[i] + " " + ", " +
                            email[i] + ", " + site[i] + ", " + usuarioId[i] + ", NULL, NULL, NULL, NULL, NULL, NULL, " + registroId[i];
                    }
                    else
                    {
                        sql = "UPDATE Pessoas SET ClassificacaoID=" + classificacaoId[i] + ", FaturamentoAnual=" +
                            faturamentoAnual[i] + ", NumeroFuncionarios=" + numeroFuncionarios[i] + ", " +
                            "UsuarioID=" + usuarioId[i] + ", " +
                            "Observacao= " + observacao[i] + ", " +
                            "ProprietarioID= " + proprietarioId[i] + " " +
                            "WHERE PessoaID=" + registroId[i] + " " +
                            "EXEC sp_Pessoa_Cadastro2 3, NULL, NULL, " + tipoPessoaId[i] + ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " +
                                "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " + ddiTelefone[i] + ", " + dddComercial[i] + ", "
                                + telComercial[i] + " " +
                            ", " + dddFaxCelular[i] + ", " + faxCelular[i] + " " +
                            ", " + email[i] + ", " + site[i] + ", " + usuarioId[i] + ", NULL, NULL, NULL, NULL, NULL, NULL, " + registroId[i];
                    }
                }
                //RelPessoas
                else if (currTipoID == 2)
                {
                    if (int.Parse(servico[i]) != 1)
                        sql = "UPDATE RelacoesPessoas SET UsuarioID=" + usuarioId[i] + ", " +
                            "Observacao= " + observacao[i] + ", " +
                            "ProprietarioID= " + proprietarioId[i] + " " +
                            "WHERE RelacaoID=" + registroId[i];
                }
                //Contatos
                else if (currTipoID == 4)
                {
                    sql = "UPDATE Pessoas SET UsuarioID=" + usuarioId[i] + ", " +
                        "Observacao= " + observacao[i] + ", " +
                        "ProprietarioID= " + proprietarioId[i] + " " +
                        "WHERE PessoaID=" + registroId[i] + " " +
                        "EXEC sp_Pessoa_Cadastro2 3, NULL, NULL, " + tipoPessoaId[i] + ", NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " + ddiTelefone[i] + ", " + dddComercial[i] + ", " + telComercial[i] + " " +
                        ", " + dddFaxCelular[i] + ", " + faxCelular[i] + " " +
                        ", " + email[i] + ", " + site[i] + ", " + usuarioId[i] + ", NULL, NULL, NULL, NULL, " + registroId[i];
                }
            }

            // Executa o pacote
            int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(sql);

            // Gera o resultado para o usuario.
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT " + rowsAffected + " as Resultado"
				)
			);
		}
	}
}
