﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_pessoas : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string nPaisID = Convert.ToString(HttpContext.Current.Request.Params["nPaisID"]);
        private string nPessoaInicialID = Convert.ToString(HttpContext.Current.Request.Params["nPessoaInicialID"]);
        private string nPessoaFinalID = Convert.ToString(HttpContext.Current.Request.Params["nPessoaFinalID"]);
        private string sEmpresas = Convert.ToString(HttpContext.Current.Request.Params["sEmpresas"]);
        private string sDtInicio = Convert.ToString(HttpContext.Current.Request.Params["sDtInicio"]);
        private string sDtFim = Convert.ToString(HttpContext.Current.Request.Params["sDtFim"]);
        private string sVendedor = Convert.ToString(HttpContext.Current.Request.Params["sVendedor"]);
        private string sTipoAtendimentoID = Convert.ToString(HttpContext.Current.Request.Params["sTipoAtendimentoID"]);
        private string sClienteID = Convert.ToString(HttpContext.Current.Request.Params["sClienteID"]);
        private string sClassificacaoResultante = Convert.ToString(HttpContext.Current.Request.Params["sClassificacaoResultante"]);
        private string sClassificacaoInterna = Convert.ToString(HttpContext.Current.Request.Params["sClassificacaoInterna"]);
        private string sClassificacaoExterna = Convert.ToString(HttpContext.Current.Request.Params["sClassificacaoExterna"]);
        private string sDiasMinimo = Convert.ToString(HttpContext.Current.Request.Params["sDiasMinimo"]);
        private string sDiasMaximo = Convert.ToString(HttpContext.Current.Request.Params["sDiasMaximo"]);
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "40106":
                        relatorioPosicaoPessoas();
                        break;

                    case "40107":
                        relatorioAtendimento();
                        break;
                }

            }
        }

        public void relatorioPosicaoPessoas()
        {
            // Excel
            int Formato = 2;
            string Title = "Posição Pessoa";

            string strSQL = "EXEC sp_Pessoa_Posicao " + nPaisID + ", " + nPessoaInicialID + ", " + nPessoaFinalID;

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "8", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "3.13834", "0.0", "8", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "6.5", "0.0", "8", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }




        public void relatorioAtendimento()
        {
            // Excel
            int Formato = 2;
            string Title = "Relatório de Atendimento";

            string strSQL = "EXEC sp_Pessoa_Atendimentos " + sEmpresas + ", " + sDtInicio + ", " + sDtFim + ", " + sVendedor + ", " + sTipoAtendimentoID + ", NULL, " + sClienteID + ", " +
                            sClassificacaoResultante + ", " + sClassificacaoInterna + ", " + sClassificacaoExterna + ", " + sDiasMinimo + ", " + sDiasMaximo;

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
