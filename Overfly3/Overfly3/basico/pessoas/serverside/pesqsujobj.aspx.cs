using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class pesqsujobj : System.Web.UI.OverflyPage
	{
		private string strOperator = " >= ";

		private string pastaId = "0";
		private string tipoRelId = "0";
		private string tipoSujObj = "";
		private string toFind = "";
		private string sujObjId = "0";
		private string relEntreId = "0";
		private string tipoPessoaId = "0";
		private string classificacao = "";

		public string nPastaID
		{
			set { pastaId = value != null ? value : "0"; }
		}

		public string nTipoRelID
		{
			set { tipoRelId = value != null ? value : "0"; }
		}

		public string sTipoSujObj
		{
			set { tipoSujObj = value != null ? value : ""; }
		}

		public string strToFind
		{
			set
			{
				toFind = value != null ? value : "";

				toFind.Trim();

				if (toFind.StartsWith("%") || toFind.EndsWith("%"))
				{
					strOperator = " LIKE ";
				}
				else
				{
					strOperator = " >= ";
				}
			}
		}

		protected string nSujObjID
		{
			set { sujObjId = value != null ? value : "0"; }
		}

		public string nRelEntreID
		{
			set { relEntreId = value != null ? value : "0"; }
		}

		public string nTipoPessoaID
		{
			set { tipoPessoaId = value != null ? value : ""; }
		}

		public string Classificacao
		{
			set { classificacao = value != null ? value : ""; }
		}

		private DataSet gerarResultado()
		{
			string sql = "";

			// Rel entre Pessoas
			if (relEntreId.Equals("133")) {

				if (tipoPessoaId.Equals("51") || tipoPessoaId.Equals("52"))
				{
					if(tipoSujObj.Equals("SUJEITO"))
					{
						sql = "SELECT TOP 100 c.Nome AS fldName,c.PessoaID AS fldID,c.Fantasia AS Fantasia, " + 
								 "dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 111, 0) AS Documento, g.Localidade " + 
								 "AS Cidade, h.CodigoLocalidade2 AS UF, i.CodigoLocalidade2 AS Pais " +
                                 "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                                 "LEFT OUTER JOIN Pessoas_Enderecos f WITH(NOLOCK) ON (c.PessoaID = f.PessoaID AND (f.Ordem=1)) " +
                                 "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON f.CidadeID = g.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON f.UFID = h.LocalidadeID " +
                                 "LEFT OUTER JOIN Localidades i WITH(NOLOCK) ON f.PaisID = i.LocalidadeID " +
								 "WHERE a.PessoaID = " + sujObjId + " AND a.TipoPessoaID = b.TipoObjetoID " +
								 "AND b.TipoRelacaoID =  " + tipoRelId + " AND b.TipoSujeitoID = c.TipoPessoaID " + 
								 "AND c.EstadoID = 2 " +
                                 "AND c.PessoaID NOT IN (SELECT a.SujeitoID FROM RelacoesPessoas a WITH(NOLOCK) " +
								 "WHERE a.ObjetoID= " + sujObjId + " AND a.TipoRelacaoID=" + tipoRelId + " ) " +
								 "AND (c.Nome " + strOperator + "'" + toFind + "' ";
		                         
								 if(strOperator.Equals(" LIKE "))
								 {
									sql += "OR c.Fantasia " + strOperator + "'" + toFind + "') ";
								 }
								 else
								 {
									sql += ") ";
								 }
		                         
								 sql += "ORDER BY fldName ";

					}
					else if(tipoSujObj.Equals("OBJETO"))
					{
						if(tipoRelId.Equals("22"))
						{
							Classificacao = " AND c.ClassificacaoID = 67 ";
						}
						else if (tipoRelId.Equals("23"))
						{
							Classificacao = " AND c.ClassificacaoID = 68 ";
						}
						else if (tipoRelId.Equals("24"))
						{
							Classificacao = " AND c.ClassificacaoID = 69 ";
						}
						else if (tipoRelId.Equals("25"))
						{
							Classificacao = " AND c.ClassificacaoID = 70 ";
						}

						sql = "SELECT TOP 100 c.Nome AS fldName,c.PessoaID AS fldID,c.Fantasia AS Fantasia, " + 
							"dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 111, 0) AS Documento, g.Localidade " + 
							"AS Cidade, h.CodigoLocalidade2 AS UF, i.CodigoLocalidade2 AS Pais " +
                            "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
                            "LEFT OUTER JOIN Pessoas_Enderecos f WITH(NOLOCK) ON (c.PessoaID = f.PessoaID AND (f.Ordem=1)) " +
                            "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON f.CidadeID = g.LocalidadeID " +
                            "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON f.UFID = h.LocalidadeID " +
                            "LEFT OUTER JOIN Localidades i WITH(NOLOCK) ON f.PaisID = i.LocalidadeID " +
							"WHERE a.PessoaID = " + sujObjId + " AND a.TipoPessoaID = b.TipoSujeitoID " +
							"AND b.TipoRelacaoID =  " + tipoRelId + " AND b.TipoObjetoID = c.TipoPessoaID " + 
							"AND c.EstadoID = 2 " +
                            "AND c.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " +
							"WHERE a.SujeitoID= " + sujObjId + " AND a.TipoRelacaoID=" + tipoRelId + " ) " +
							classificacao + 
							"AND (c.Nome " + strOperator + "'" + toFind + "' ";
		                         
						 if(strOperator.Equals(" LIKE "))
						 {
							sql += "OR c.Fantasia " + strOperator + "'" + toFind + "') ";
						 }
						 else 
						 {
							sql += ") ";
						 }
		                         
						 sql += "ORDER BY fldName ";
					}    
				}
				else
				{
					if (tipoSujObj.Equals("SUJEITO"))
					{
						sql =
							"SELECT TOP 100 c.Nome AS fldName,c.PessoaID AS fldID,c.Fantasia AS Fantasia " +
                            "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
							"WHERE a.PessoaID = " + sujObjId + " AND a.TipoPessoaID = b.TipoObjetoID " +
							"AND b.TipoRelacaoID = " + tipoRelId + " AND b.TipoSujeitoID = c.TipoPessoaID " +
							"AND c.EstadoID = 2 " +
                            "AND c.PessoaID NOT IN (SELECT a.SujeitoID FROM RelacoesPessoas a WITH(NOLOCK) " +
							"WHERE a.ObjetoID=" + sujObjId + " AND a.TipoRelacaoID=" + tipoRelId + ") " +
							"AND (c.Nome " + strOperator + "'" + toFind + "' ";

						if(strOperator.Equals(" LIKE "))
						{
							sql = sql + "OR c.Fantasia " + strOperator + "'" + toFind + "') ";
						}
						else
						{
							sql = sql + ") ";
						}

						sql = sql + "ORDER BY fldName ";
		                         
					}
					else if (tipoSujObj.Equals("OBJETO"))
					{
						sql =
							"SELECT TOP 100 c.Nome AS fldName,c.PessoaID AS fldID,c.Fantasia AS Fantasia " +
                            "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " +
							"WHERE a.PessoaID = " + sujObjId + " AND a.TipoPessoaID = b.TipoSujeitoID " +
							"AND b.TipoRelacaoID = " + tipoRelId + " AND b.TipoObjetoID = c.TipoPessoaID " +
							"AND c.EstadoID = 2 " +
                            "AND c.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " +
							"WHERE a.SujeitoID=" + sujObjId + " AND a.TipoRelacaoID=" + tipoRelId + ") " +
							"AND (c.Nome " + strOperator + "'" + toFind + "' ";
		                         
						 if(strOperator.Equals(" LIKE "))
						 {
							sql = sql + "OR c.Fantasia " + strOperator + "'" + toFind + "') ";
						 }
						 else
						 {
							sql = sql + ") ";
						 }
                         
						 sql = sql + "ORDER BY fldName ";
					}    
				}    
			}

			// Rel entre Pessoas e Recursos
			else if (relEntreId.Equals("132"))
			{
				sql =
					"SELECT TOP 100 c.Recurso AS fldName,c.RecursoID AS fldID,c.RecursoFantasia AS Fantasia " +
                    "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK) " +
					"WHERE a.PessoaID = " + sujObjId + " AND a.TipoPessoaID = b.TipoSujeitoID " +
					"AND b.TipoRelacaoID = " + tipoRelId + " AND b.TipoObjetoID = c.TipoRecursoID " +
					"AND c.EstadoID = 2 " +
                    "AND c.RecursoID NOT IN (SELECT a.ObjetoID FROM RelacoesPesRec a WITH(NOLOCK) " +
					"WHERE a.SujeitoID=" + sujObjId + " AND a.TipoRelacaoID=" + tipoRelId + ") " +
					"AND c.Recurso " + strOperator + "'" + toFind + "' " +
					"ORDER BY fldName ";
			}

			// Rel entre Pessoas e Conceitos
			else if (relEntreId.Equals("135"))
			{
				sql =
					"SELECT TOP 100 c.Conceito AS fldName,c.ConceitoID AS fldID,c.Conceito AS Fantasia " +
                    "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " +
					"WHERE a.PessoaID = " + sujObjId + " AND a.TipoPessoaID = b.TipoSujeitoID " +
					"AND b.TipoRelacaoID = " + tipoRelId + " AND b.TipoObjetoID = c.TipoConceitoID " +
					"AND c.EstadoID = 2 " +
                    "AND c.ConceitoID NOT IN (SELECT a.ObjetoID FROM RelacoesPesCon a WITH(NOLOCK) " +
					"WHERE a.SujeitoID=" + sujObjId + " AND a.TipoRelacaoID=" + tipoRelId + ") " +
					"AND c.Conceito " + strOperator + "'" + toFind + "' " +
					"ORDER BY fldName ";
			}

			return DataInterfaceObj.getRemoteData(sql);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(gerarResultado());
		}
	}
}
