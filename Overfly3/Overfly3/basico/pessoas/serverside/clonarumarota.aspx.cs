using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class clonarumarota : System.Web.UI.OverflyPage
	{
		private string oldPesTraRotaId;
		private string newPesTraRotaId;

		public string nOldPesTraRotaID
		{
			set { oldPesTraRotaId = (value != null ? value : "0"); }
		}

		public string nNewPesTraRotaID
		{
			set { newPesTraRotaId = (value != null ? value : "0"); }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			string sql = "";
			int fldResp;

			// Gera o resultado.
			sql =
				"DELETE FROM Pessoas_Transportadoras_Rotas_Pesos WHERE (PesTraRotaID = " + oldPesTraRotaId + ") " +
				"INSERT INTO Pessoas_Transportadoras_Rotas_Pesos (PesTraRotaID, MKM, PercentualNotaFiscal, PesoMaximo, TaxaPeso, ValorMinimo) " +
				"SELECT " + oldPesTraRotaId + ", MKM, PercentualNotaFiscal, PesoMaximo, TaxaPeso, ValorMinimo " +
				"FROM Pessoas_Transportadoras_Rotas_Pesos WITH(NOLOCK) WHERE (PesTraRotaID = " + newPesTraRotaId + ") "
			;


			fldResp = DataInterfaceObj.ExecuteSQLCommand(sql);

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select " + fldResp + " as fldresp"
				)
			);
		}
	}
}
