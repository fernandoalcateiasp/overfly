using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class atualizarpessoa : System.Web.UI.OverflyPage
	{
        private string erro = "";
        private string nome = "";
		private string fantasia = "";
		private string tipoPessoaId = "";
		private string classificacaoId = "0";
		private string cnae = "";
        private string enquadramentoEmpresaID = "";
        private string regimeTributario = "";
        private string ehContribuinte = "";
		private string documentoFederal = "";
		private string documentoEstadual = "";
		private string faturamentoAnual = "";
		private string numeroFuncionarios = "";
        private string sexoID = "0";
        private string dtNascimento = "";
		private string paisId = "0";
		private string cep = "";
		private string ufId = "";
		private string cidadeId = "";
		private string bairro = "";
		private string endereco = "";
		private string numero = "";
		private string complemento = "";
		private string observacao = "";
		private string ddiTelefone = "";
		private string dddTelefone1 = "";
		private string numeroTelefone1 = "";
		private string dddTelefone2 = "";
		private string numeroTelefone2 = "";
		private string email = "";
        private string emailnfe = "";
        private string tipo = "";
		private string site = "";
        private string estadocivil = "";
        
		private string usuarioId = "0";
		private string relacionarComId = "0";
		private string pessoaId = "0";
		private string[] valDetalheId = new String[0];
		private string[] valorAlterado = new String[0];
		private string cadastroOK = "0";
		private string result = "";

		public string sNome
		{
			set { nome = (value == null ? "" : value); }
		}

		public string sFantasia
		{
			set { fantasia = (value == null ? "" : value); }
		}

		public string nTipoPessoaID
		{
			set { tipoPessoaId = (value == null ? "0" : value); }
		}

		public string nClassificacaoID
		{
			set { classificacaoId = (value == null ? "0" : value); }
		}

		public string sCNAE
		{
			set { cnae = (value == null ? "" : value); }
		}

        public string nEnquadramentoEmpresaID
        {
            set { enquadramentoEmpresaID = (value == null ? "0" : value); }
        }

        public string nRegimeTributarioID
        {
            set { regimeTributario = (value == null ? "0" : value); }
        }

        public string nEhContribuinte
        {
            set { ehContribuinte = (value == null ? "0" : value); }
        }

		public string sDocumentoFederal
		{
			set { documentoFederal = (value == null ? "" : value); }
		}

		public string sDocumentoEstadual
		{
			set { documentoEstadual = (value == null ? "" : value); }
		}

		public string nFaturamentoAnual
		{
			set { faturamentoAnual = (value == null ? "0" : value); }
		}

		public string nNumeroFuncionarios
		{
			set { numeroFuncionarios = (value == null ? "0" : value); }
		}

        public string nSexo
		{
            set { sexoID = (value == null ? "" : value); }
		}

        public string dDtNascimento
		{
			set { dtNascimento = (value == null ? "" : value); }
		}

		public string nPaisID
		{
			set { paisId = (value == null ? "0" : value); }
		}

		public string sCEP
		{
			set { cep = (value == null ? "" : value); }
		}

		public string nUFID
		{
			set { ufId = (value == null ? "0" : value); }
		}

		public string nCidadeID
		{
			set { cidadeId = (value == null ? "0" : value); }
		}

		public string sBairro
		{
			set { bairro = (value == null ? "" : value); }
		}

		public string sEndereco
		{
			set { endereco = (value == null ? "" : value); }
		}

		public string sNumero
		{
			set { numero = (value == null ? "" : value); }
		}

		public string sComplemento
		{
			set { complemento = (value == null ? "" : value); }
		}

		public string sObservacao
		{
			set { observacao = (value == null ? "" : value); }
		}

		public string sDDITelefone
		{
			set { ddiTelefone = (value == null ? "" : value); }
		}

		public string sDDDTelefone1
		{
			set { dddTelefone1 = (value == null ? "" : value); }
		}

		public string sNumeroTelefone1
		{
			set { numeroTelefone1 = (value == null ? "" : value); }
		}

		public string sDDDTelefone2
		{
			set { dddTelefone2 = (value == null ? "" : value); }
		}

		public string sNumeroTelefone2
		{
			set { numeroTelefone2 = (value == null ? "" : value); }
		}

		public string sEmail
		{
			set { email = (value == null ? "" : value); }
		}

        public string sEmailNFe
        {
            set { emailnfe = (value == null ? "" : value); }
        }

        public string sFonteDadoID
        {
            set { tipo = (value == null ? null : value); }
        }

		public string sSite
		{
			set { site = (value == null ? "" : value); }
		}

        public string nEstadoCivilID
		{
			set { estadocivil = (value == null ? "0" : value); }
		}

		public string nUsuarioID
		{
			set { usuarioId = (value == null ? "0" : value); }
		}

		public string nRelacionarComID
		{
			set { relacionarComId = (value == null ? "0" : value); }
		}

		public string nPessoaID
		{
			set { pessoaId = (value == null ? "0" : value); }
		}

		public string[] ValDetalheID
		{
			set { valDetalheId = (value == null ? new string[] { } : value); }
		}

		public string[] ValorAlterado
		{
			set { valorAlterado = (value == null ? new string[] { } : value); }
		}

		public string bCadastroOK
		{
			set { cadastroOK = (value == null ? "0" : value); }
		}

		/** Atualiza a tabela ValidacoesDados_Detalhes. */
		private void atualizaValidacoesDadosDetalhes()
		{
			string sql = "";
			
			string ValorUsado;
			string ValorAnteriorUsado;

			for (int i = 0; i < valDetalheId.Length; i++)
			{
				if (valorAlterado[i].Equals("0"))
				{
					ValorUsado = "1";
					ValorAnteriorUsado = "0";
				}
				else
				{
					ValorUsado = "0";
					ValorAnteriorUsado = "1";
				}

				sql +=
					" UPDATE ValidacoesDados_Detalhes SET " +
						" ValorUsado = " + ValorUsado + ", " +
						" ValorAnteriorUsado = " + ValorAnteriorUsado + " " +
					" WHERE (ValDetalheID = " + valDetalheId[i] + ")";
			}

			sql +=
				"UPDATE Pessoas SET " +
					"EstadoID=2, UsuarioID=" + usuarioId + " " +
				"WHERE (PessoaID = " + pessoaId + " AND CadastroOK=1 AND EstadoID=1)";
				
			DataInterfaceObj.ExecuteSQLCommand(sql);
		}

		/** Roda a procedure dbo.sp_Pessoa_Cadastro2. */
		private void pessoaCadastro2()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[37];

			parameters[0] = new ProcedureParameters(
				"@TipoCadastro",
				System.Data.SqlDbType.Int,
				4);

			parameters[1] = new ProcedureParameters(
				"@Nome",
				System.Data.SqlDbType.VarChar,
				nome != null && nome.Length > 0 ? (Object)nome : System.DBNull.Value);
			parameters[1].Length = 40;

			parameters[2] = new ProcedureParameters(
				"@Fantasia",
				System.Data.SqlDbType.VarChar,
				fantasia != null && fantasia.Length > 0 ? (Object)fantasia : System.DBNull.Value);
			parameters[2].Length = 20;

			parameters[3] = new ProcedureParameters(
				"@TipoPessoaID",
				System.Data.SqlDbType.Int,
				int.Parse(tipoPessoaId));

			parameters[4] = new ProcedureParameters(
				"@ClassificacaoID",
				System.Data.SqlDbType.Int,
				int.Parse(classificacaoId));

			parameters[5] = new ProcedureParameters(
				"@CNAE",
				System.Data.SqlDbType.VarChar,
				cnae != null && cnae.Length > 0 ? (Object)cnae : System.DBNull.Value);
			parameters[5].Length = 7;

            parameters[6] = new ProcedureParameters(
                "@EnquadramentoEmpresaID",
                System.Data.SqlDbType.Int,
                enquadramentoEmpresaID.Length > 0 ? (Object)int.Parse(enquadramentoEmpresaID) : DBNull.Value); 
    
            parameters[7] = new ProcedureParameters(
                "@RegimeTributarioID",
                System.Data.SqlDbType.Int,
                regimeTributario.Length > 0 ? (Object)int.Parse(regimeTributario) : DBNull.Value); 

			parameters[8] = new ProcedureParameters(
				"@DocumentoFederal",
				System.Data.SqlDbType.VarChar,
				documentoFederal != null && documentoFederal.Length > 0 ?
					(Object)documentoFederal : System.DBNull.Value);
			parameters[8].Length = 20;

			parameters[9] = new ProcedureParameters(
				"@DocumentoEstadual",
				System.Data.SqlDbType.VarChar,
				documentoEstadual != null && documentoEstadual.Length > 0 ?
					(Object)documentoEstadual : System.DBNull.Value);
			parameters[9].Length = 20;

			parameters[10] = new ProcedureParameters(
				"@FaturamentoAnual",
				System.Data.SqlDbType.Money,
				faturamentoAnual.Length > 0 ?
					(Object) double.Parse(faturamentoAnual) : 
					DBNull.Value);

			parameters[11] = new ProcedureParameters(
				"@NumeroFuncionarios",
				System.Data.SqlDbType.Int,
				numeroFuncionarios.Length > 0 ?
					(Object)int.Parse(numeroFuncionarios) :
					DBNull.Value);

			parameters[12] = new ProcedureParameters(
				"@dtNascimento",
				System.Data.SqlDbType.DateTime,
				dtNascimento.Length > 0 ?
					(Object)DateTime.Parse(dtNascimento, System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat) :
					DBNull.Value);

			parameters[13] = new ProcedureParameters(
				"@PaisID",
				System.Data.SqlDbType.Int,
				paisId.Length > 0 ? (Object)int.Parse(paisId) : DBNull.Value);

			parameters[14] = new ProcedureParameters(
				"@CEP",
				System.Data.SqlDbType.VarChar,
				cep.Length > 0 ? (Object)cep : DBNull.Value);

			parameters[15] = new ProcedureParameters(
				"@UFID",
				System.Data.SqlDbType.Int,
				ufId.Length > 0 ? (Object)int.Parse(ufId) : DBNull.Value);

			parameters[16] = new ProcedureParameters(
				"@CidadeID",
				System.Data.SqlDbType.Int,
				cidadeId.Length > 0 ? (Object)int.Parse(cidadeId) : DBNull.Value);

			parameters[17] = new ProcedureParameters(
				"@Bairro",
				System.Data.SqlDbType.VarChar,
				bairro.Length > 0 ? (Object)bairro : DBNull.Value);
			parameters[17].Length = 20;

			parameters[18] = new ProcedureParameters(
				"@Endereco",
				System.Data.SqlDbType.VarChar,
				endereco.Length > 0 ? (Object)endereco : DBNull.Value);
			parameters[18].Length = 35;

			parameters[19] = new ProcedureParameters(
				"@Numero",
				System.Data.SqlDbType.VarChar,
				numero.Length > 0 ? (Object)numero : DBNull.Value);
			parameters[19].Length = 6;

			parameters[20] = new ProcedureParameters(
				"@Complemento",
				System.Data.SqlDbType.VarChar,
				complemento.Length > 0 ? (Object)complemento : DBNull.Value);
			parameters[20].Length = 20;

			parameters[21] = new ProcedureParameters(
				"@Observacao",
				System.Data.SqlDbType.VarChar,
				observacao.Length > 0 ? (Object)observacao : DBNull.Value);
			parameters[21].Length = 20;

			parameters[22] = new ProcedureParameters(
				"@DDITelefone",
				System.Data.SqlDbType.VarChar,
				ddiTelefone.Length > 0 ? (Object)ddiTelefone : DBNull.Value);
			parameters[22].Length = 4;

			parameters[23] = new ProcedureParameters(
				"@DDDTelefone1",
				System.Data.SqlDbType.VarChar,
				dddTelefone1.Length > 0 ? (Object)dddTelefone1 : DBNull.Value);
			parameters[23].Length = 4;

			parameters[24] = new ProcedureParameters(
				"@NumeroTelefone1",
				System.Data.SqlDbType.VarChar,
				numeroTelefone1.Length > 0 ? (Object)numeroTelefone1 : DBNull.Value);
			parameters[24].Length = 9;

			parameters[25] = new ProcedureParameters(
				"@DDDTelefone2",
				System.Data.SqlDbType.VarChar,
				dddTelefone2.Length > 0 ? (Object)dddTelefone2 : DBNull.Value);
			parameters[25].Length = 4;

			parameters[26] = new ProcedureParameters(
				"@NumeroTelefone2",
				System.Data.SqlDbType.VarChar,
				numeroTelefone2.Length > 0 ? (Object)numeroTelefone2 : DBNull.Value);
			parameters[26].Length = 9;

			parameters[27] = new ProcedureParameters(
				"@Email",
				System.Data.SqlDbType.VarChar,
				email.Length > 0 ? (Object)email : DBNull.Value);
			parameters[27].Length = 80;

            parameters[28] = new ProcedureParameters(
                "@EmailNFe",
                System.Data.SqlDbType.VarChar,
                emailnfe.Length > 0 ? (Object)emailnfe : DBNull.Value);
            parameters[28].Length = 80;

			parameters[29] = new ProcedureParameters(
				"@Site",
				System.Data.SqlDbType.VarChar,
				site.Length > 0 ? (Object)site : DBNull.Value);
			parameters[29].Length = 80;

			parameters[30] = new ProcedureParameters(
				"@UsuarioID",
				System.Data.SqlDbType.Int,
				usuarioId.Length > 0 ? (Object)int.Parse(usuarioId) : DBNull.Value);

			parameters[31] = new ProcedureParameters(
				"@RelacionarComID",
				System.Data.SqlDbType.Int,
				relacionarComId.Length > 0 ? (Object)int.Parse(relacionarComId) : DBNull.Value);

			parameters[32] = new ProcedureParameters(
				"@CadastroOK",
				System.Data.SqlDbType.Bit,
				cadastroOK.Length > 0 ? (Object)int.Parse(cadastroOK) : DBNull.Value);

            parameters[33] = new ProcedureParameters(
                "@Sexo",
                System.Data.SqlDbType.Int,
                sexoID.Length > 0 ? (Object)int.Parse(sexoID) : DBNull.Value);

            parameters[34] = new ProcedureParameters(
                "@EstadoCivilID",
                System.Data.SqlDbType.Int,
                estadocivil.Length > 0 ? (Object)int.Parse(estadocivil) : DBNull.Value);

            //parameters[35] = new ProcedureParameters( 
            //    "@Tipo",
            //    System.Data.SqlDbType.Int,
            //    290);

            parameters[35] = new ProcedureParameters(
               "@EhContribuinte",
               System.Data.SqlDbType.Bit,
               ehContribuinte.Length > 0 ? (Object)int.Parse(ehContribuinte) : DBNull.Value); 

            parameters[36] = new ProcedureParameters(
				"@PessoaID",
				System.Data.SqlDbType.Int,
				pessoaId.Length > 0 ? (Object)int.Parse(pessoaId) : DBNull.Value,
				ParameterDirection.InputOutput);

			// Roda a procedure.
            try
            {
                DataInterfaceObj.execNonQueryProcedure("sp_Pessoa_Cadastro2", parameters);
            }
            catch (System.Exception e)
            {
                erro += e.Message;
            }

			// Set o id da pessoa.
            result = parameters[36].Data.ToString();
		}

		/** Evento de carregamento do p�gina. */
		protected override void PageLoad(object sender, EventArgs e)
		{
			pessoaCadastro2();

			atualizaValidacoesDadosDetalhes();

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
                    "select " + result + " as PessoaID, '" + erro + "' as Error"
				)
			);
		}
	}
}
