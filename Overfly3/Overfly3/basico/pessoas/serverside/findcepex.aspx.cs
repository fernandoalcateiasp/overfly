using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class findcepex : System.Web.UI.OverflyPage
	{
		private string cep;
		private string paisId;

		public string sCep
		{
			get { return cep; }
			set { cep = value != null ? value : ""; }
		}

		public string nPaisID
		{
			get { return paisId; }
			set { paisId = value != null ? value : "0"; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT Cidades.LocalidadeID AS CidadeID, Cidades.Localidade AS Cidade, UFs.LocalidadeID AS UFID, " +
                            "UFs.CodigoLocalidade2 AS UF, CEPs.CEPInicial, CEPs.CEPFinal, (SELECT TOP 1 DDDI FROM Localidades_DDDs WITH(NOLOCK) WHERE LocalidadeID=Cidades.LocalidadeID) AS DDD " +
                        "FROM Localidades_CEPs CEPs WITH(NOLOCK), Localidades Cidades WITH(NOLOCK), " +
                            "Localidades UFs WITH(NOLOCK), Localidades Paises WITH(NOLOCK) " +
						"WHERE ('" + cep + "' BETWEEN CEPs.CEPInicial AND CEPs.CEPFinal AND " +
							"LEN('" + cep + "') BETWEEN LEN(CEPs.CEPInicial) AND LEN(CEPs.CEPFinal) AND " +
							"CEPs.LocalidadeID = Cidades.LocalidadeID AND Cidades.EstadoID = 2 AND " +
							"Cidades.TipoLocalidadeID = 205 AND Cidades.LocalizacaoID = UFs.LocalidadeID AND " +
							"UFs.EstadoID = 2 AND UFs.TipoLocalidadeID = 204 AND UFs.LocalizacaoID = Paises.LocalidadeID AND " +
							"Paises.EstadoID = 2 AND Paises.TipoLocalidadeID = 203 AND Paises.LocalidadeID = " + paisId + ") " +
					"UNION ALL " +
                    "SELECT Cidades.LocalidadeID AS CidadeID, Cidades.Localidade AS Cidade, NULL AS UFID, NULL AS UF, CEPs.CEPInicial, CEPs.CEPFinal, (SELECT TOP 1 DDDI FROM Localidades_DDDs WITH(NOLOCK) WHERE LocalidadeID=Cidades.LocalidadeID) AS DDD " +
                        "FROM Localidades_CEPs CEPs WITH(NOLOCK), Localidades Cidades WITH(NOLOCK), Localidades Paises WITH(NOLOCK) " +
						"WHERE ('" + cep + "' BETWEEN CEPs.CEPInicial AND CEPs.CEPFinal AND " +
							"LEN('" + cep + "') BETWEEN LEN(CEPs.CEPInicial) AND LEN(CEPs.CEPFinal) AND " +
							"CEPs.LocalidadeID = Cidades.LocalidadeID AND Cidades.EstadoID = 2 AND " +
							"Cidades.TipoLocalidadeID = 205 AND Cidades.LocalizacaoID = Paises.LocalidadeID AND " +
							"Paises.EstadoID = 2 AND Paises.TipoLocalidadeID = 203 AND Paises.LocalidadeID = " + paisId + ") " +
							"ORDER BY Cidades.Localidade, UF "
				)
			);
		}
	}
}
