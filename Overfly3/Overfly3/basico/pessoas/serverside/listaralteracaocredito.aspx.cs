using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
    public partial class listaralteracaocredito : System.Web.UI.OverflyPage
	{
        private string registroId;
        private string empresaId;
        private string pessoas;
        private string relacoesPessoas;
        private string segurosCredito;
        private string contatos;
        private string empresas;
        private string estados;
        private string proprietario;
        private string classificacaoId;
        private string classificacaoResultante;
        private string classificacaoInterna;
        private string classificacaoExterna; 
        private string pedidosPendentes;
        private string limiteMinimo;
        private string limiteMaximo;
        private string saldoMinimo;
        private string diasVencidos; 
        private string diasMinimo;
        private string diasMaximo;
        private string pesquisa;
        private string seguradoraId;
        private string marcaId;
        private string servico;
        private string atendimento;
        private string ordemAgendamento;
        private string pessoaGravacaoId;
        private string userId;

        protected string RegistroID { set { registroId = value != null ? value : ""; } }
        protected string EmpresaID { set { empresaId = value != null ? value : ""; } }
        protected string bPessoas { set { pessoas = value != null ? value : ""; } }
        protected string bRelacoesPessoas { set { relacoesPessoas = value != null ? value : ""; } }
        protected string bSegurosCredito { set { segurosCredito = value != null ? value : ""; } }
        protected string bContatos { set { contatos = value != null ? value : ""; } }
        protected string sEmpresas { set { empresas = value != null ? value : ""; } }
        protected string sEstados { set { estados = value != null ? value : ""; } }
        protected string sProprietario { set { proprietario = value != null ? value : ""; } }
        protected string nClassificacaoID { set { classificacaoId = value != null ? value : ""; } }
        protected string sClassificacaoResultante { set { classificacaoResultante = value != null ? value : ""; } }
        protected string sClassificacaoInterna { set { classificacaoInterna = value != null ? value : ""; } }
        protected string sClassificacaoExterna { set { classificacaoExterna = value != null ? value : ""; } }
        protected string bPedidosPendentes { set { pedidosPendentes = value != null ? value : ""; } }
        protected string sLimiteMinimo { set { limiteMinimo = value != null ? value : ""; } }
        protected string sLimiteMaximo { set { limiteMaximo = value != null ? value : ""; } }
        protected string sSaldoMinimo { set { saldoMinimo = value != null ? value : ""; } }
        protected string sDiasVencidos { set { diasVencidos = value != null ? value : ""; } }
        protected string sDiasMinimo { set { diasMinimo = value != null ? value : ""; } }
        protected string sDiasMaximo { set { diasMaximo = value != null ? value : ""; } }
        protected string sPesquisa { set { pesquisa = value != null ? value : ""; } }
        protected string sSeguradoraID { set { seguradoraId = value != null ? value : ""; } }
        protected string sMarcaID { set { marcaId = value != null ? value : ""; } }
        protected string nServico { set { servico = value != null ? value : ""; } }
        protected string nAtendimento { set { atendimento = value != null ? value : ""; } }
        protected string nOrdemAgendamento { set { ordemAgendamento = value != null ? value : ""; } }
        protected string nPessoaGravacaoID { set { pessoaGravacaoId = value != null ? value : ""; } }
        protected string sUserID { set { userId = value != null ? value : ""; } }
		
		protected DataSet AtendimentoRealiza()
		{
            // Executa procedure sp_Atendimento_Realiza
            ProcedureParameters[] atendimentoRealizaParams = new ProcedureParameters[32];

            atendimentoRealizaParams[0] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                empresaId == null || empresaId.Length == 0 ?
                System.DBNull.Value :
                (Object) int.Parse(empresaId));

            atendimentoRealizaParams[1] = new ProcedureParameters(
                "@Registros",
                System.Data.SqlDbType.Int,
                registroId == null || registroId.Length == 0 ?
                System.DBNull.Value :
                (Object) int.Parse(registroId));

            atendimentoRealizaParams[2] = new ProcedureParameters(
                "@Pessoas",
                System.Data.SqlDbType.Int,
                pessoas == null || pessoas.Length == 0 ?
                false : (Object)(pessoas == "1"));

            atendimentoRealizaParams[3] = new ProcedureParameters(
                "@RelacoesPessoas",
                System.Data.SqlDbType.Bit,
                relacoesPessoas == null || relacoesPessoas.Length == 0 ?
                false : (Object)(relacoesPessoas == "1"));

            atendimentoRealizaParams[4] = new ProcedureParameters(
                "@Seguros",
                System.Data.SqlDbType.Bit,
                segurosCredito == null || segurosCredito.Length == 0 ?
                false : (Object)(segurosCredito == "1"));

            atendimentoRealizaParams[5] = new ProcedureParameters(
                "@IncluiContatos",
                System.Data.SqlDbType.Bit,
                contatos == null || contatos.Length == 0 ?
                false : (Object)(contatos == "1"));

            atendimentoRealizaParams[6] = new ProcedureParameters(
                "@Empresas",
                System.Data.SqlDbType.VarChar,
                empresas == null || empresas.Length == 0 ?
                System.DBNull.Value : (Object)empresas);
            atendimentoRealizaParams[6].Length = 8000;

            atendimentoRealizaParams[7] = new ProcedureParameters(
                "@Estados",
                System.Data.SqlDbType.VarChar,
                estados == null || estados.Length == 0 ?
                System.DBNull.Value : (Object)estados);
            atendimentoRealizaParams[7].Length = 8000;

            atendimentoRealizaParams[8] = new ProcedureParameters(
                "@ProprietarioID",
                System.Data.SqlDbType.Int,
                proprietario == null || proprietario.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(proprietario));

            atendimentoRealizaParams[9] = new ProcedureParameters(
                "@ClassificacaoID",
                System.Data.SqlDbType.Int,
                classificacaoId == null || classificacaoId.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(classificacaoId));

            atendimentoRealizaParams[10] = new ProcedureParameters(
                "@ClassificacaoResultante",
                System.Data.SqlDbType.VarChar,
                classificacaoResultante == null || classificacaoResultante.Length == 0 ?
                System.DBNull.Value : (Object)classificacaoResultante);
            atendimentoRealizaParams[10].Length = 9;

            atendimentoRealizaParams[11] = new ProcedureParameters(
                "@ClassificacaoInterna",
                System.Data.SqlDbType.VarChar,
                classificacaoInterna == null || classificacaoInterna.Length == 0 ?
                System.DBNull.Value : (Object)classificacaoInterna);
            atendimentoRealizaParams[11].Length = 1;

            atendimentoRealizaParams[12] = new ProcedureParameters(
                "@ClassificacaoExterna",
                System.Data.SqlDbType.VarChar,
                classificacaoExterna == null || classificacaoExterna.Length == 0 ?
                System.DBNull.Value : (Object)classificacaoExterna);
            atendimentoRealizaParams[12].Length = 1;

            atendimentoRealizaParams[13] = new ProcedureParameters(
                "@PedidosPendentes",
                System.Data.SqlDbType.Bit,
                pedidosPendentes == null || pedidosPendentes.Length == 0 ?
                false : (Object)(pedidosPendentes == "1"));

            atendimentoRealizaParams[14] = new ProcedureParameters(
                "@LimiteMinimo",
                System.Data.SqlDbType.Money,
                limiteMinimo == null || limiteMinimo.Length == 0 ?
                System.DBNull.Value : (Object)float.Parse(limiteMinimo));
            
            atendimentoRealizaParams[15] = new ProcedureParameters(
                "@LimiteMaximo",
                System.Data.SqlDbType.Money,
                limiteMaximo == null || limiteMaximo.Length == 0 ?
                System.DBNull.Value : (Object)float.Parse(limiteMaximo));

            atendimentoRealizaParams[16] = new ProcedureParameters(
                "@SaldoMinimo",
                System.Data.SqlDbType.Money,
                saldoMinimo == null || saldoMinimo.Length == 0 ?
                System.DBNull.Value : (Object)float.Parse(saldoMinimo));

            atendimentoRealizaParams[17] = new ProcedureParameters(
                "@DiasVencido",
                System.Data.SqlDbType.Int,
                diasVencidos == null || diasVencidos.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(diasVencidos));

            atendimentoRealizaParams[18] = new ProcedureParameters(
                "@DiasMinimo",
                System.Data.SqlDbType.Int,
                diasMinimo == null || diasMinimo.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(diasMinimo));

            atendimentoRealizaParams[19] = new ProcedureParameters(
                "@DiasMaximo",
                System.Data.SqlDbType.Int,
                diasMaximo == null || diasMaximo.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(diasMaximo));

            atendimentoRealizaParams[20] = new ProcedureParameters(
                "@Pesquisa",
                System.Data.SqlDbType.VarChar,
                pesquisa == null || pesquisa.Length == 0 ?
                System.DBNull.Value : (Object)pesquisa);
            atendimentoRealizaParams[20].Length = 1000;

            atendimentoRealizaParams[21] = new ProcedureParameters(
                "@SeguradoraID",
                System.Data.SqlDbType.Int,
                seguradoraId == null || seguradoraId.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(seguradoraId));
            
            atendimentoRealizaParams[22] = new ProcedureParameters(
                "@MarcaID",
                System.Data.SqlDbType.Int,
                marcaId == null || marcaId.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(marcaId));

            atendimentoRealizaParams[23] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                servico == null || servico.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(servico));

            atendimentoRealizaParams[24] = new ProcedureParameters(
                "@TipoAtendimentoID",
                System.Data.SqlDbType.Int,
                atendimento == null || atendimento.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(atendimento));

            atendimentoRealizaParams[25] = new ProcedureParameters(
                "@OrdemAgendamento",
                System.Data.SqlDbType.Int,
                ordemAgendamento == null || ordemAgendamento.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(ordemAgendamento));

            atendimentoRealizaParams[26] = new ProcedureParameters(
                "@dtData",
                System.Data.SqlDbType.DateTime,
                System.DBNull.Value);

            atendimentoRealizaParams[27] = new ProcedureParameters(
                "@PessoaGravacaoID",
                System.Data.SqlDbType.Int,
                pessoaGravacaoId == null || pessoaGravacaoId.Length == 0 || pessoaGravacaoId == "0"?
                System.DBNull.Value : (Object)int.Parse(pessoaGravacaoId));

            atendimentoRealizaParams[28] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                userId == null || userId.Length == 0 ?
                System.DBNull.Value : (Object)int.Parse(userId));

            return DataInterfaceObj.execQueryProcedure(
				"sp_Pessoa_AlteracaoCreditos", 
				atendimentoRealizaParams);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(AtendimentoRealiza());
		}
	}
}
