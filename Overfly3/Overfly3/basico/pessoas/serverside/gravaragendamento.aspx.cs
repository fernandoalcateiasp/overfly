using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class gravaragendamento : System.Web.UI.OverflyPage
	{
		private string pessoaId;
		private string status;
		private string empresaId;
		private string usuarioId;
		private string tipoAtendimentoId;
		private string meioAtendimentoId;
		private string resultadoId;
		private string observacoes;
		private string produtos;
		private string dtAgendamento;
		private string pesAtendimentoId;
		private string erro;

		private IFormatProvider formatProvider =
			new System.Globalization.CultureInfo("en-US", true);


		protected string nPessoaID
		{
			set { pessoaId = value != null ? value : ""; }
		}
		protected string nStatus
		{
			set { status = value != null ? value : ""; }
		}
		protected string nEmpresaID
		{
			set { empresaId = value != null ? value : ""; }
		}
		protected string nUserID
		{
			set { usuarioId = value != null ? value : ""; }
		}
		protected string nTipoAtendimentoID
		{
			set { tipoAtendimentoId = value != null ? value : ""; }
		}
		protected string nMeioAtendimentoID
		{
			set { meioAtendimentoId = value != null ? value : ""; }
		}
		protected string sResultadoID
		{
			set { resultadoId = value != null ? value : ""; }
		}
		protected string sObservacoes
		{
			set { observacoes = value != null ? value : ""; }
		}
		protected string sProdutos
		{
			set { produtos = value != null ? value : ""; }
		}
		protected string sDtAgendamento
		{
			set { dtAgendamento = value != null ? value : ""; }
		}
		protected string nPesAtendimentoID
		{
			set { pesAtendimentoId = value != null ? value : ""; }
		}


		protected bool GravaAtendimento
		{
			get
			{
				return (status != null && status.Length > 0 && int.Parse(status) == 1) &&
					(pesAtendimentoId != null && pesAtendimentoId.Length > 0 && int.Parse(pesAtendimentoId) != 0) && 
					(produtos != null && produtos.Length > 0);
			}
		}
		protected void AtendimentoRealiza()
		{
			// Executa procedure sp_Atendimento_Realiza
			ProcedureParameters[] atendimentoRealizaParams = new ProcedureParameters[10];

			atendimentoRealizaParams[0] = new ProcedureParameters(
				"@Status",
				System.Data.SqlDbType.Int,
				status == null || status.Length == 0 ?
					System.DBNull.Value :
					(Object) int.Parse(status));

			atendimentoRealizaParams[1] = new ProcedureParameters(
				"@PessoaID",
				System.Data.SqlDbType.Int,
				pessoaId == null || pessoaId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(pessoaId));

			atendimentoRealizaParams[2] = new ProcedureParameters(
				"@EmpresaID",
				System.Data.SqlDbType.Int,
				empresaId == null || empresaId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(empresaId));

			atendimentoRealizaParams[3] = new ProcedureParameters(
				"@UsuarioID",
				System.Data.SqlDbType.Int,
				usuarioId == null || usuarioId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(usuarioId));

			atendimentoRealizaParams[4] = new ProcedureParameters(
				"@TipoAtendimentoID",
				System.Data.SqlDbType.Int,
				tipoAtendimentoId == null || tipoAtendimentoId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(tipoAtendimentoId));

			atendimentoRealizaParams[5] = new ProcedureParameters(
				"@MeioAtendimentoID",
				System.Data.SqlDbType.Int,
				meioAtendimentoId == null || meioAtendimentoId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(meioAtendimentoId));

			atendimentoRealizaParams[6] = new ProcedureParameters(
				"@ResultadoID",
				System.Data.SqlDbType.Int,
				resultadoId == null || resultadoId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(resultadoId));

			atendimentoRealizaParams[7] = new ProcedureParameters(
				"@Observacoes",
				System.Data.SqlDbType.VarChar,
				observacoes == null ?
					System.DBNull.Value :
					(Object)observacoes);
			atendimentoRealizaParams[7].Length = 8000;

			atendimentoRealizaParams[8] = new ProcedureParameters(
				"@dtAgendamento",
				System.Data.SqlDbType.DateTime,
				dtAgendamento == null || dtAgendamento.Length == 0 ?
					System.DBNull.Value :
					(Object)DateTime.Parse(
						dtAgendamento,
						formatProvider,
						System.Globalization.DateTimeStyles.NoCurrentDateDefault));

			atendimentoRealizaParams[9] = new ProcedureParameters(
				"@PesAtendimentoID",
				System.Data.SqlDbType.Int,
				pesAtendimentoId == null || pesAtendimentoId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(pesAtendimentoId),
				ParameterDirection.InputOutput);

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Atendimento_Realiza", 
				atendimentoRealizaParams);
				
			pesAtendimentoId = atendimentoRealizaParams[9].Data.ToString();
        }
               
		protected void AtendimentoGravaProdutos()
		{
			// Executa procedure sp_Atendimento_GravaProdutos
			ProcedureParameters[] atendimentoGravaProdutosParams = new ProcedureParameters[3];

			atendimentoGravaProdutosParams[0] = new ProcedureParameters(
				"@PesAtendimentoID",
				System.Data.SqlDbType.Int,
				pesAtendimentoId == null || pesAtendimentoId.Length == 0 ?
					System.DBNull.Value :
					(Object)int.Parse(pesAtendimentoId));

			atendimentoGravaProdutosParams[1] = new ProcedureParameters(
				"@DadosCotacao",
				System.Data.SqlDbType.VarChar,
				produtos == null || produtos.Length == 0 ?
					System.DBNull.Value :
					(Object)produtos);
			atendimentoGravaProdutosParams[1].Length = 8000;

			atendimentoGravaProdutosParams[2] = new ProcedureParameters(
				"@Erro",
				System.Data.SqlDbType.VarChar,
				erro == null || erro.Length == 0 ? System.DBNull.Value : (Object)erro,
				ParameterDirection.InputOutput);
			atendimentoGravaProdutosParams[2].Length = 100;

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Atendimento_GravaProdutos", 
				atendimentoGravaProdutosParams);

			erro = atendimentoGravaProdutosParams[2].Data.ToString();
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			AtendimentoRealiza();

			if (GravaAtendimento)
			{
				AtendimentoGravaProdutos();
			}

			if (erro == null || erro.Length == 0)
			{
				WriteResultXML(
					DataInterfaceObj.getRemoteData(
						"select " + pesAtendimentoId + " as PesAtendimentoID"
					)
				);
			}
			else
			{
				throw new Exception(erro);
			}
		}
	}
}
