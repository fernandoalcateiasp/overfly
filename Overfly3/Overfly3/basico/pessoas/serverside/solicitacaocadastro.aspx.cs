using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class solicitacaocadastro : System.Web.UI.OverflyPage
	{
		private string pessoaId;
		private string posicaoFila;

		public string nPessoaID
		{
			set { pessoaId = value; }
		}

		/** Roda a procedure sp_Pessoa_SolicitacaoCadastro */
		private void pessoaSolicitacaoCadastro()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[2];

			parameters[0] = new ProcedureParameters(
				"@PessoaID",
				System.Data.SqlDbType.Int,
				int.Parse(pessoaId));

			parameters[1] = new ProcedureParameters(
                "@PosicaoFila",
				System.Data.SqlDbType.Int,
				DBNull.Value,
				ParameterDirection.Output);

			DataInterfaceObj.execNonQueryProcedure(
				"sp_Pessoa_SolicitacaoCadastro", 
				parameters);
			
			posicaoFila = parameters[1].Data.ToString();
		}


		private DataSet gerarResultado()
		{
			string sql = "";

			sql = "select ";
			if (posicaoFila != null && posicaoFila.Length > 0)
			{
				sql += 
					"'Solicita��o de cadastro realizada." + 
					(char)13 + (char)10 +
					"Posi��o na fila: " + posicaoFila + "'";
			}
			else
			{
				sql += "NULL";
			}
            sql += " as PosicaoFila";

			return DataInterfaceObj.getRemoteData(sql);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			pessoaSolicitacaoCadastro();
			
			WriteResultXML(gerarResultado());
		}
	}
}
