using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class listarvalidacaodados : System.Web.UI.OverflyPage
	{
		private string pessoaID = "0";
		private string validacaoDadoID = null;
		private string dateSqlParam = "";
		private string sohMostraveis = "0";
		private string pendentes = null;
		private string sohValidas = "";

		public string PessoaID
		{
			get { return pessoaID; }
			set { pessoaID = value != null ? value : "0"; }
		}

		public string ValidacaoDadoID
		{
			get { return validacaoDadoID; }
			set { validacaoDadoID = value; }
		}

		public string DATE_SQL_PARAM
		{
			get { return dateSqlParam; }
			set { dateSqlParam = value != null ? value : ""; }
		}

		public string SohMostraveis
		{
			get { return sohMostraveis; }
			set { sohMostraveis = value != null ? value : "0"; }
		}

		public string Pendentes
		{
			get { return pendentes; }
			set { pendentes = value; }
		}

		public string SohValidas
		{
			get { return sohValidas; }
			set { sohValidas = value != null ? value : ""; }
		}

		/** Roda a procedure sp_ValidacoesDados_Listagem */
		private DataSet validacoesDadosListagem()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[6];

			parameters[0] = new ProcedureParameters(
				"@PessoaID",
				System.Data.SqlDbType.Int,
				PessoaID);

			parameters[1] = new ProcedureParameters(
				"@ValidacaoDadoID",
				System.Data.SqlDbType.Int,
				(ValidacaoDadoID == null || ValidacaoDadoID == "0") ? 
					DBNull.Value :
					(Object)int.Parse(ValidacaoDadoID));

			parameters[2] = new ProcedureParameters(
				"@DATE_SQL_PARAM",
				System.Data.SqlDbType.Int,
				dateSqlParam);

			parameters[3] = new ProcedureParameters(
				"@SomenteMostraveis",
				System.Data.SqlDbType.Bit,
				SohMostraveis.Equals("0") || 
					SohMostraveis.ToLower().Equals("false") ||
					SohMostraveis.ToLower().Equals("falso") ? 0 : 1);

			parameters[4] = new ProcedureParameters(
				"@Pendentes",
				System.Data.SqlDbType.Bit,
				Pendentes.Equals("0") || 
					Pendentes.ToLower().Equals("false") ||
					Pendentes.ToLower().Equals("falso") ? 0 : 1);

			parameters[5] = new ProcedureParameters(
				"@SomenteValidas",
				System.Data.SqlDbType.Bit,
				SohValidas.Equals("0") ||
					SohValidas.ToLower().Equals("false") ||
					SohValidas.ToLower().Equals("falso") ? 0 : 1);

			return DataInterfaceObj.execQueryProcedure(
				"sp_ValidacoesDados_Listagem",
				parameters);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(validacoesDadosListagem());
		}
	}
}
