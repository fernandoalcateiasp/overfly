using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class emailconfirmacaocadastro : System.Web.UI.OverflyPage
	{
		private string pessoaId;
		private string tipoEmail;
		private string empresaId;
		private string userId;
		private string msg;

		public string nPessoaID
		{
			get { return pessoaId; }
			set { pessoaId = value; }
		}

		public string nTipoEmail
		{
			get { return tipoEmail; }
			set { tipoEmail = value != null ? value : "0"; }
		}

		public string nEmpresaID
		{
			get { return empresaId; }
			set { empresaId = value != null ? value : "0"; }
		}

		public string nUserID
		{
			get { return userId; }
			set { userId = value != null ? value : "0"; }
		}

		/** Roda a procedure sp_Email_ConfirmacaoCadastro */
		private void emailConfirmacaoCadastro()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[5];

			parameters[0] = new ProcedureParameters(
				"@PessoaID",
				System.Data.SqlDbType.Int,
				pessoaId != null ? (Object) int.Parse(pessoaId) : DBNull.Value);

			parameters[1] = new ProcedureParameters(
				"@EmpresaID",
				System.Data.SqlDbType.Int,
				int.Parse(empresaId));

			parameters[2] = new ProcedureParameters(
				"@UsuarioID",
				System.Data.SqlDbType.Int,
				int.Parse(userId));

			parameters[3] = new ProcedureParameters(
				"@TipoEmail",
				System.Data.SqlDbType.Int,
				int.Parse(tipoEmail));

			parameters[4] = new ProcedureParameters(
				"@Resultado",
				System.Data.SqlDbType.VarChar,
				DBNull.Value,
				ParameterDirection.Output);
			parameters[4].Length = 100;
	
			DataInterfaceObj.execNonQueryProcedure(
				"sp_Email_ConfirmacaoCadastro", 
				parameters);
				
			msg = parameters[4].Data.ToString();
		}


		protected override void PageLoad(object sender, EventArgs e)
		{
			emailConfirmacaoCadastro();

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"select '" + (msg != null ? msg : "null") + "' as Msg"
				)
			);

		}
	}
}
