using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class criasegurocredito : System.Web.UI.OverflyPage
	{
		private string relacaoId;
		private string seguradoraId;
		private string marcaId;
		private string usuarioId;
		private string excluir;

		private string erroMessage = null;

		public string RelacaoID
		{
			set { relacaoId = (value != null ? value : "0"); }
		}

		public string SeguradoraID
		{
			set { seguradoraId = (value != null ? value : "0"); }
		}

		public string MarcaID
		{
			set { marcaId = value; }
		}

		public string UsuarioID
		{
			set { usuarioId = (value != null ? value : "0"); }
		}

		public string Excluir
		{
			set { excluir = (value != null ? value : "0"); }
		}

		public string ErroMessage
		{
			set { erroMessage = value; }
		}

		/** Roda a procedure sp_RelacoesPessoas_SeguroCredito */
		private void relacoesPessoasSeguroCredito()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[5];

			parameters[0] = new ProcedureParameters(
				"@RelacaoID",
				System.Data.SqlDbType.Int,
				int.Parse(relacaoId));

			parameters[1] = new ProcedureParameters(
				"@SeguradoraID",
				System.Data.SqlDbType.Int,
				int.Parse(seguradoraId));

			parameters[2] = new ProcedureParameters(
				"@MarcaID",
				System.Data.SqlDbType.Int,
				marcaId != null ? (Object)int.Parse(marcaId) : DBNull.Value);

			parameters[3] = new ProcedureParameters(
				"@UsuarioID",
				System.Data.SqlDbType.Int,
				int.Parse(usuarioId));

			parameters[4] = new ProcedureParameters(
				"@Excluir",
				System.Data.SqlDbType.Binary,
				int.Parse(excluir) != 0 ? 1 : 0);

			DataInterfaceObj.execNonQueryProcedure(
				"sp_RelacoesPessoas_SeguroCredito", 
				parameters);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			relacoesPessoasSeguroCredito();

			WriteResultXML(DataInterfaceObj.getRemoteData("select null as Resultado"));
		}
	}
}
