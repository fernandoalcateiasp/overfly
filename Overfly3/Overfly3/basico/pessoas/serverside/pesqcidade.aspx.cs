using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class pesqcidade : System.Web.UI.OverflyPage
	{
		private string pais = "";
		private string estado = "0";
		private string toFind = "";

		public string nPais
		{
			get { return pais; }
			set { pais = value != null ? value : ""; }
		}

		public string nEstado
		{
			get { return estado; }
			set { estado = value != null ? value : "0"; }
		}

		public string strToFind
		{
			get { return toFind; }
			set { toFind = value != null ? value : ""; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT TOP 100 a.CEPInicial,a.CEPFinal,a.LocalidadeID,b.Localidade AS Cidade, (SELECT TOP 1 DDDI FROM Localidades_DDDs WITH(NOLOCK) WHERE LocalidadeID=b.LocalidadeID) AS DDD " +
                    "FROM Localidades_CEPs a WITH(NOLOCK), Localidades b WITH(NOLOCK) " +
					"WHERE (a.localidadeid = b.localidadeid AND b.TipoLocalidadeID=205 AND " +
					"b.EstadoID = 2) AND (b.LocalizacaoID = " + pais + " OR b.LocalizacaoID = " +
					estado + ") AND " + "(b.Localidade >= '" + toFind + "') " +
					"ORDER BY b.Localidade,a.CEPInicial "
				)
			);

		}
	}
}
