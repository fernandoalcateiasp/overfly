using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class clonarrota : System.Web.UI.OverflyPage
	{
		private string destinoPesTransportadoraId;
		private string origemPesTransportadoraId;

		public string nDestinoPesTransportadoraID
		{
			set { destinoPesTransportadoraId = (value != null ? value : "0"); }
		}

		public string nOrigemPesTransportadoraID
		{
			set { origemPesTransportadoraId = (value != null ? value : "0"); }
		}

		/** Roda a procedure sp_Trasportadora_RotasClonar */
		private void trasportadoraRotasClonar()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[2];

			parameters[0] = new ProcedureParameters(
				"@PesTransportadoraID",
				System.Data.SqlDbType.Int,
				destinoPesTransportadoraId);

			parameters[1] = new ProcedureParameters(
				"@PesTransportadoraCloneID",
				System.Data.SqlDbType.Int,
				origemPesTransportadoraId);
				
			DataInterfaceObj.execNonQueryProcedure(
				"sp_Trasportadora_RotasClonar", 
				parameters);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			trasportadoraRotasClonar();

			WriteResultXML(DataInterfaceObj.getRemoteData("select NULL as Resultado"));
		}
	}
}
