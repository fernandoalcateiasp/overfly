using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.basico.pessoas.serverside
{
	public partial class fillwizard : System.Web.UI.OverflyPage
	{
		private string contextoId;
        private string userId;
		private string empresaId;

		public string nContextoID
		{
			get { return contextoId; }
			set { contextoId = value != null ? value : "0"; }
		}

		public string nUserID
		{
			get { return userId; }
			set { userId = value != null ? value : "0"; }
		}

		public string nEmpresaID
		{
			get { return empresaId; }
			set { empresaId = value != null ? value : "0"; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT f.SubFormID " +
                    "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b,RelacoesPesRec c WITH(NOLOCK), " +
                    "RelacoesPesRec_Perfis d WITH(NOLOCK), Recursos_Direitos e WITH(NOLOCK), Recursos_Wizards f WITH(NOLOCK), Recursos g WITH(NOLOCK) " +
					"WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID= " + contextoId + " " +
					"AND a.SujeitoID=b.RecursoID AND b.Principal=0 " +
					"AND c.ObjetoID=999 AND c.TipoRelacaoID=11 " +
					"AND c.SujeitoID IN ((SELECT " + userId +
						" UNION ALL SELECT UsuarioDeID " +
                        "FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " + userId +
						" AND GETDATE() BETWEEN dtInicio AND dtFim))) " +
					"AND c.RelacaoID=d.RelacaoID " +
					"AND d.EmpresaID= " + empresaId + " AND a.ObjetoID=e.RecursoMaeID " +
					"AND a.SujeitoID=e.RecursoID " +
					"AND (e.Consultar1=1 OR e.Consultar2=1) AND d.PerfilID=e.PerfilID " +
					"AND f.ContextoID= " + contextoId + " " +
					"AND b.RecursoID=f.SubFormID AND d.PerfilID = g.RecursoID AND g.EstadoID = 2 " +
					"ORDER BY f.Ordem"
				)
			);
		}
	}
}
