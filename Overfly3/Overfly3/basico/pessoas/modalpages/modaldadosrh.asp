<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn

    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalDadosRHHtml" name="modalDadosRHHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/pessoas/modalpages/modaldadosrh.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
                                                               
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/pessoas/modalpages/modaldadosrh.js" & Chr(34) & "></script>" & vbCrLf

%>

<%
'Script de variaveis globais

Dim i, sCaller, nPessoaID

sCaller = ""
nPessoaID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nPessoaID
For i = 1 To Request.QueryString("nPessoaID").Count    
    nPessoaID = Request.QueryString("nPessoaID")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPessoaID = " & Chr(39) & nPessoaID & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

</head>

<body id="modalDadosRHBody" name="modalDadosRHBody" LANGUAGE="javascript" onload="return window_onload()">
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>  
    
     <!-- Div Texts-->
    <p id="lblContratodeTrabalho" name="lblContratodeTrabalho" class="lblGeneral"><B>Contrato de Trabalho</B></p>
    
    <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Data Fim</p>
    <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral"></input>   

     <p id="lblDiasContrato" name="lblDiasContrato" class="lblGeneral">Dias Contrato</p>
     <select id="selDiasContrato" name="selDiasContrato" class="fldGeneral">
        <%
            Dim strSQL, rsData
            
            Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                "WHERE EstadoID = 2 AND (TipoID = 1007) " & _
                "ORDER BY Ordem " 

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
            Set rsData = Nothing
           
        %>
      </select>	

     <p id="lblSindicato" name="lblSindicato" class="lblGeneral">Sindicato</p>
     <select id="selSindicato" name="selSindicato" class="fldGeneral">
             <%
            Set rsData = Server.CreateObject("ADODB.Recordset")

             strSQL = "SELECT NULL AS fldID, SPACE(0) AS fldName  UNION ALL " & _
                      "SELECT PessoaID AS fldID, Fantasia AS fldName " & _
                      "FROM Pessoas WITH(NOLOCK)" & _
                      "WHERE EstadoID = 2 AND dbo.fn_TagValor(Observacoes, 'ADM_PUBLICA' , '|', ';', 1) = 'SINDICATO' "


            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
            Set rsData = Nothing
           
        %>
      </select>	

     <p id="lblContribuicaoSindicalAnual" name="lblContribuicaoSindicalAnual" class="lblGeneral">Contribui��o Sindical Anual?</p>
     <input type="checkbox" id="chkContribuicaoSindicalAnual" name="chkContribuicaoSindicalAnual" class="fldGeneral"></input>   

     <p id="lblSituacaodeTrabalho" name="lblSituacaodeTrabalho" class="lblGeneral"><B>Situa��o de Trabalho</B></p>

     <p id="lblTipoAdmissao" name="lblTipoAdmissao" class="lblGeneral">Tipo Admiss�o</p>
     <select id="selTipoAdmissao" name="selTipoAdmissao" class="fldGeneral">
             <%
            Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                "WHERE EstadoID = 2 AND (TipoID = 1008) " & _
                "ORDER BY Ordem " 

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
            Set rsData = Nothing
           
        %>
      </select>	

     <p id="lblHorariodeTrabalho" name="lblHorariodeTrabalho" class="lblGeneral">Hor�rio de Trabalho</p>
    <input type="text" id="txtHorariodeTrabalho" name="txtHorariodeTrabalho" class="fldGeneral"></input>   

    <p id="lblCargaHoraria" name="lblCargaHoraria" class="lblGeneral">Carga Hor�ria</p>
    <input type="text" id="txtCargaHoraria" name="txtCargaHoraria" class="fldGeneral"></input>   

     <p id="lblAdiantamentoQuinzenal" name="lblAdiantamentoQuinzenal" class="lblGeneral">Adiantamento Quinzenal</p>
     <input type="checkbox" id="chkAdiantamentoQuinzenal" name="chkAdiantamentoQuinzenal" class="fldGeneral"></input>   

     <p id="lblDadosdeEstagio" name="lblDadosdeEstagio" class="lblGeneral"><B>Dados de Est�gio</B></p>

     <p id="lblAreadeAtuacao" name="lblAreadeAtuacao" class="lblGeneral">Area de Atua��o</p>
     <input type="text" id="txtAreadeAtuacao" name="txtAreadeAtuacao" class="fldGeneral"></input> 
    
     <p id="lblNumeroApoliceSeguro" name="lblNumeroApoliceSeguro" class="lblGeneral">N�mero Apolice Seguro</p>
     <input type="text" id="txtNumeroApoliceSeguro" name="txtNumeroApoliceSeguro" class="fldGeneral"></input> 
    
     <p id="lblCurso" name="lblCurso" class="lblGeneral">Curso</p>
     <input type="text" id="txtCurso" name="txtCurso" class="fldGeneral"></input>   
    
    <p id="lblSemestre" name="lblSemestre" class="lblGeneral">Semestre</p>
     <select id="selSemestre" name="selSemestre" class="fldGeneral">
             <%
            Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT NULL AS fldID, NULL AS fldName  UNION ALL " & _
                     "SELECT 1 AS fldID, 1 AS fldName UNION ALL " & _
                     "SELECT 2 AS fldID, 2 AS fldName UNION ALL " & _
                     "SELECT 3 AS fldID, 3 AS fldName UNION ALL " & _
                     "SELECT 4 AS fldID, 4 AS fldName UNION ALL " & _
                     "SELECT 5 AS fldID, 5 AS fldName UNION ALL " & _
                     "SELECT 6 AS fldID, 6 AS fldName UNION ALL " & _
                     "SELECT 7 AS fldID, 7 AS fldName UNION ALL " & _
                     "SELECT 8 AS fldID, 8 AS fldName UNION ALL " & _
                     "SELECT 9 AS fldID, 9 AS fldName UNION ALL " & _
                     "SELECT 10 AS fldID, 10 AS fldName UNION ALL " & _
                     "SELECT 11 AS fldID, 11 AS fldName UNION ALL " & _
                     "SELECT 12 AS fldID, 12 AS fldName " 

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
            Set rsData = Nothing
           
        %>
      </select>	

      <p id="lblInstituicaoEnsino" name="lblInstituicaoEnsino" class="lblGeneral">Institui��o Ensino</p>
      <select id="selInstituicaoEnsino" name="selInstituicaoEnsino" class="fldGeneral">
             <%
            Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT NULL AS fldID, SPACE(0) AS fldName  UNION ALL " & _
                     "SELECT PessoaID AS fldID, Fantasia AS fldName " & _
                     "FROM Pessoas WITH(NOLOCK)" & _
                     "WHERE dbo.fn_TagValor(Observacoes, 'INSTITUICAO' , '|', ';', 1) = 'UNIVERSIDADE' "

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
            Set rsData = Nothing
           
        %>
       </select>	
   
     <p id="lblAgenteIntegracao" name="lblAgenteIntegracao" class="lblGeneral">Agente Integra��o</p>
      <select id="selAgenteIntegracao" name="selAgenteIntegracao" class="fldGeneral">
             <%
            Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT NULL AS fldID, SPACE(0) AS fldName  UNION ALL " & _
                     "SELECT PessoaID AS fldID, Fantasia AS fldName " & _
                     "FROM Pessoas WITH(NOLOCK)" & _
                     "WHERE EstadoID = 2 AND dbo.fn_TagValor(Observacoes, 'INSTITUICAO' , '|', ';', 1) = 'AGENTE' "

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
            Set rsData = Nothing
           
        %>
       </select>
    
      <p id="lblSupervisor" name="lblSupervisor" class="lblGeneral">Supervisor</p>
      <select id="selSupervisor" name="selSupervisor" class="fldGeneral">
             <%
            Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT NULL AS fldID, SPACE(0) AS fldName  UNION ALL " & _
                     "SELECT a.PessoaID AS fldID, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK)" & _
                     "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.SujeitoID = a.PessoaID) AND (b.EstadoID = 2) AND (b.DepartamentoID = 9113) " & _
                     "WHERE a.EstadoID = 2 AND a.ClassificacaoID = 57 "

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
            Set rsData = Nothing
           
        %>
       </select>	
    
     <p id="lblNecessidadesEspeciais" name="lblNecessidadesEspeciais" class="lblGeneral"><B>Necessidades Especiais</B></p>	

     <p id="lblTipoNecessidade" name="lblTipoNecessidade" class="lblGeneral">Tipo Necessidade</p>
      <select id="selTipoNecessidade" name="selTipoNecessidade" class="fldGeneral">
             <%
            Set rsData = Server.CreateObject("ADODB.Recordset")

              strSQL = "SELECT NULL AS fldID, SPACE(0) AS fldName, 0 AS Ordem  UNION ALL " & _
                "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                "WHERE EstadoID = 2 AND (TipoID = 24) " & _
                "ORDER BY Ordem " 

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
            Set rsData = Nothing
           
        %>
       </select>	

     <p id="lblCodigoCID" name="lblCodigoCID" class="lblGeneral">C�digo CID</p>
     <input type="text" id="txtCodigoCID" name="txtCodigoCID" class="fldGeneral"></input> 

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">      
 
</body>

</html>
