<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot    
    Dim strConn
        
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalconfirmacaocadastroHtml" name="modalconfirmacaocadastroHtml">

<head>
	<META HTTP-EQUIV="Content-Type" CONTENT="text/html;charset=iso-8859-1">

	<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL='stylesheet' HREF='" & pagesURLRoot & "/basico/pessoas/modalpages/modalconfirmacaocadastro.css'type='text/css'>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_sysbase.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_constants.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_consultaserasa.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_htmlbase.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_strings.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_interface.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_statusbar.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_fastbuttonsbar.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_controlsbar.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_modalwin.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/formlibs/js_gridsex.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/js_rdsfacil.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/modalgen/commonmodal.js'></script>" & vbCrLf

    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/Defines.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js'></script>" & vbCrLf
	Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CField.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CFields.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js'></script>" & vbCrLf
    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js'></script>" & vbCrLf
	Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js'></script>" & vbCrLf

    Response.Write "<script LANGUAGE='JavaScript' SRC='" & pagesURLRoot & "/basico/pessoas/modalpages/modalconfirmacaocadastro.js'></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nRegistroID, nEmpresaPaisID

sCaller = ""
nRegistroID = 0
nEmpresaPaisID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nRegistroID").Count
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

'Pais ID data empresa logada
For i = 1 To Request.QueryString("nEmpresaPaisID").Count
    nEmpresaPaisID = Request.QueryString("nEmpresaPaisID")(i)
Next

If (nRegistroID = 0) Then
    Response.Write "var glb_nRegistroID = 'NULL';" & vbcrlf    
Else
    Response.Write "var glb_nRegistroID = " & CStr(nRegistroID) & ";" & vbcrlf    
End If

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 fg_AfterRowColChange();
 fg_modalconfirmacaocadastro_AfterRowColChange();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
	js_fg_DblClick(fg.Row, fg.Col);
//-->
</SCRIPT>
</head>

<body id="modalconfirmacaocadastroBody" name="modalconfirmacaocadastroBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>  
    
    <!-- Div Texts-->
    <div id="divText" name="divText" class="divGeneral">
    
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">ID</p>
        <input type="text" id="txtPessoaID" name="txtPessoaID" class="fldGeneral"></input>         
        
        <p id="lblEstado" name="lblEstado" class="lblGeneral">Est</p>
        <input type="text" id="txtEstado" name="txtEstado" class="fldGeneral"></input>         
        
        <p id="lblNome" name="lblNome" class="lblGeneral">Nome</p>
        <input type="text" id="txtNome" name="txtNome" class="fldGeneral"></input>         
        
        <p id="lblFantasia" name="lblFantasia" class="lblGeneral">Fantasia</p>
        <input type="text" id="txtFantasia" name="txtFantasia" class="fldGeneral"></input>         
        
        <p id="lblTipo" name="lblTipo" class="lblGeneral">Tipo</p>
        <select id="selTipo" name="selTipo" class="fldGeneral">
        <%
            Dim strSQL, rsData
            
            Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                "WHERE EstadoID = 2 AND (TipoID = 12) AND (Filtro LIKE ('%(1211)%')) " & _
                "ORDER BY Ordem " 

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            If (Not (rsData.BOF AND rsData.EOF)) Then
                While Not (rsData.EOF)
	                Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
                    Response.Write( rsData.Fields("fldName").Value & "</option>" )
	                rsData.MoveNext()
                WEnd
            End If

            rsData.Close
        %>
        </select>		
        
        <p id="lblDocumentoFederal" name="lblDocumentoFederal" class="lblGeneral">Documento federal</p>
        <input type="text" id="txtDocumentoFederal" name="txtDocumentoFederal" class="fldGeneral"></input>         
        
        <p id="lblDocumentoEstadual" name="lblDocumentoEstadual" class="lblGeneral">Documento estadual</p>
        <input type="text" id="txtDocumentoEstadual" name="txtDocumentoEstadual" class="fldGeneral"></input>         

        <p id="lblClassificacaoID" name="lblClassificacaoID" class="lblGeneral">Classifica��o</p>
        <select id="selClassificacaoID" name="selClassificacaoID" class="fldGeneral"></select>		
        
        <p id="lblCNAE" name="lblCNAE" class="lblGeneral">CNAE</p>
        <input type="text" id="txtCNAE" name="txtCNAE" class="fldGeneral"></input>         
<!--
        <p id="lblTipoEmpresaID" name="lblTipoEmpresaID" class="lblGeneral">Tipo de Empresa</p>
        <select id="selTipoEmpresaID" name="selTipoEmpresaID" class="fldGeneral">
        <%
	           'strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                   ' "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                   ' "WHERE (TipoID = 119) AND EstadoID = 2 " & _
                   ' "ORDER BY Ordem " 

    	      ' rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	          ' If (Not (rsData.BOF AND rsData.EOF)) Then
	        	  '  While Not (rsData.EOF)
	    	    	   ' Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
	    		       ' Response.Write( rsData.Fields("fldName").Value & "</option>" )
    	    		   ' rsData.MoveNext()
    	    	  '  WEnd
         	  '  End If

	          '  rsData.Close
        %>
        </select>	
-->
        <p id="lblEnquadramentoEmpresaID" name="lblEnquadramentoEmpresaID" class="lblGeneral">Enquadramento</p>
        <select id="selEnquadramentoEmpresaID" name="selEnquadramentoEmpresaID" class="fldGeneral">
        <%
	           strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                    "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                    "WHERE (TipoID = 119) AND EstadoID = 2 " & _
                    "ORDER BY Ordem " 

    	       rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	           If (Not (rsData.BOF AND rsData.EOF)) Then
	        	    While Not (rsData.EOF)
	    	    	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
	    		        Response.Write( rsData.Fields("fldName").Value & "</option>" )
    	    		    rsData.MoveNext()
    	    	    WEnd
         	    End If

	            rsData.Close
        %>
        </select>	

        <p id="lblRegimeTributarioID" name="lblRegimeTributarioID" class="lblGeneral">Regime Tribut�rio</p>
        <select id="selRegimeTributarioID" name="selRegimeTributarioID" class="fldGeneral">
        <%
	           strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                    "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                    "WHERE (TipoID = 120) AND EstadoID = 2 " & _
                    "ORDER BY Ordem " 

    	       rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	           If (Not (rsData.BOF AND rsData.EOF)) Then
	        	    While Not (rsData.EOF)
	    	    	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
	    		        Response.Write( rsData.Fields("fldName").Value & "</option>" )
    	    		    rsData.MoveNext()
    	    	    WEnd
         	    End If

	            rsData.Close
        %>
        </select>	
        
        <p id="lblEhContribuinte" name="lblEhContribuinte" class="lblGeneral" Title="� contribuinte de ICMS?">Cont</p> 
	    <input type="checkbox" id="chkEhContribuinte" name="chkEhContribuinte" class="fldGeneral" Title="� contribuinte de ICMS?"> 
        
        <p id="lblSexo" name="lblSexo" class="lblGeneral">Sexo</p>
        <select id="selSexo" name="selSexo" class="fldGeneral">			
        <%
	           strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                    "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                    "WHERE EstadoID = 2 AND (TipoID = 10)  " & _
                    "ORDER BY Ordem " 

    	       rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	           If (Not (rsData.BOF AND rsData.EOF)) Then
	        	    While Not (rsData.EOF)
	    	    	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
	    		        Response.Write( rsData.Fields("fldName").Value & "</option>" )
    	    		    rsData.MoveNext()
    	    	    WEnd
         	    End If

	            rsData.Close
        %>
        </select>		
        
        <p id="lblEstadoCivilID" name="lblEstadoCivilID" class="lblGeneral">Estado civil</p>
        <select id="selEstadoCivilID" name="selEstadoCivilID" class="fldGeneral">
        <%
	           strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                    "FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _ 
                    "WHERE (TipoID = 14) AND EstadoID = 2 " & _
                    "ORDER BY Ordem " 

    	       rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	           If (Not (rsData.BOF AND rsData.EOF)) Then
	        	    While Not (rsData.EOF)
	    	    	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
	    		        Response.Write( rsData.Fields("fldName").Value & "</option>" )
    	    		    rsData.MoveNext()
    	    	    WEnd
         	    End If

	            rsData.Close
        %>
        </select>		
        
        <p id="lblFaturamentoAnual" name="lblFaturamentoAnual" class="lblGeneral" title="Brasil = R$ / Outras Localidades = U$">Fat anual</p>
        <input type="text" id="txtFaturamentoAnual" name="txtFaturamentoAnual" class="fldGeneral"></input>
        
        <p id="lblNumeroFuncionarios" name="lblNumeroFuncionarios" class="lblGeneral">N�m func</p>
        <input type="text" id="txtNumeroFuncionarios" name="txtNumeroFuncionarios" class="fldGeneral"></input>
        
        <!--<p id="lblTratamento" name="lblTratamento" class="lblGeneral">Tratamento</p>
        <input type="text" id="txtTratamento" name="txtTratamento" class="fldGeneral"></input> -->
        
        <p id="lblNascimento" name="lblNascimento" class="lblGeneral">Nascimento</p>
        <input type="text" id="txtNascimento" name="txtNascimento" class="fldGeneral"></input>
        
        <p id="lblPais" name="lblPais" class="lblGeneral">Pa�s</p>
        <select id="selPais" name="selPais" class="fldGeneral">
        <%
	           strSQL = "SELECT a.LocalidadeID AS fldID, a.Localidade AS fldName, b.DDDI AS fldOption " & _
            "FROM Localidades a WITH(NOLOCK) " & _
                "LEFT JOIN Localidades_DDDs b WITH(NOLOCK) ON (a.LocalidadeID = b.LocalidadeID) AND (b.Ordem = 1) " & _
            "WHERE a.EstadoID=2 AND a.TipoLocalidadeID=203 " & _
            "ORDER BY Localidade" 

    	       rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	           If (Not (rsData.BOF AND rsData.EOF)) Then
	        	    While Not (rsData.EOF)
	    	    	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "' DDDI= '" & rsData.Fields("fldOption").Value & "' ")
    	                If (CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaPaisID)) Then
    		                Response.Write( " selected " )
                        End If
    	                Response.Write( ">" & rsData.Fields("fldName") & "</option>" )
    	                rsData.MoveNext()
    	    	    WEnd
         	    End If

	            rsData.Close
        %>
        </select>		
        
        <p id="lblCEP" name="lblCEP" class="lblGeneral">CEP</p>
        <input type="text" id="txtCEP" name="txtCEP" class="fldGeneral"></input>         
        
        <p id="lblUFID" name="lblUFID" class="lblGeneral">UF</p>
        <select id="selUFID" name="selUFID" class="fldGeneral"></select>		
        
        <p id="lblCidade" name="lblCidade" class="lblGeneral">Cidade</p>
        <select id="selCidade" name="selCidade" class="fldGeneral"></select>		
        
        <p id="lblBairro" name="lblBairro" class="lblGeneral">Bairro</p>
        <input type="text" id="txtBairro" name="txtBairro" class="fldGeneral"></input>         
        
        <p id="lblEndereco" name="lblEndereco" class="lblGeneral">Endere�o</p>
        <input type="text" id="txtEndereco" name="txtEndereco" class="fldGeneral"></input>         
        
        <p id="lblNumero" name="lblNumero" class="lblGeneral">N�mero</p>
        <input type="text" id="txtNumero" name="txtNumero" class="fldGeneral"></input>         
        
        <p id="lblComplemento" name="lblComplemento" class="lblGeneral">Complemento</p>
        <input type="text" id="txtComplemento" name="txtComplemento" class="fldGeneral"></input>         
        
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral"></input>   
        
        <p id="lblDDI" name="lblDDI" class="lblGeneral">DDI</p>
        <input type="text" id="txtDDI" name="txtDDI" class="fldGeneral"></input>         
        
        <p id="lblDDDTelefone" name="lblDDDTelefone" class="lblGeneral">DDD</p>
        <input type="text" id="txtDDDTelefone" name="txtDDDTelefone" class="fldGeneral"></input>         
        
        <p id="lblTelefone" name="lblTelefone" class="lblGeneral">Telefone</p>
        <input type="text" id="txtTelefone" name="txtTelefone" class="fldGeneral"></input>         
        
        <p id="lblDDDCelular" name="lblDDDCelular" class="lblGeneral">DDD</p>
        <input type="text" id="txtDDDCelular" name="txtDDDCelular" class="fldGeneral"></input>         
        
        <p id="lblCelular" name="lblCelular" class="lblGeneral">Celular</p>
        <input type="text" id="txtCelular" name="txtCelular" class="fldGeneral"></input>   
        
        <p id="lblEmail" name="lblEmail" class="lblGeneral">E-mail</p>
        <input type="text" id="txtEmail" name="txtEmail" class="fldGeneral"></input>
        
        <p id="lblEmailNFe" name="lblEmailNFe" class="lblGeneral">E-mail NFe</p>
        <input type="text" id="txtEmailNfe" name="txtEmailNfe" class="fldGeneral"></input>                  
        
        <p id="lblSite" name="lblSite" class="lblGeneral">Site</p>
        <input type="text" id="txtSite" name="txtSite" class="fldGeneral"></input>                 
        
        <p id="lblGrid" name="lblGrid" title="Ampliar gride?" class="lblGeneral">Grid</p>
        <input type="checkbox" id="chkGrid" name="chkGrid" title="Aumentar/diminuir a altura do grid?" class="fldGeneral"></input>         
        
        <p id="lblConsultasValidas" name="lblConsultasValidas" title="Mostrar somente consultas validas?" class="lblGeneral">Val</p>
        <input type=checkbox id="chkConsultasValidas" name="chkConsultasValidas" title="Mostrar somente consultas validas?" class="fldGeneral"></input>

        <p id="lblFonte" name="lblFonte" class="lblGeneral">Fonte</p>
        <select id="selFonte" name="selFonte" class="fldGeneral"></select>		
        
        <p id="lblAplicaveis" name="lblAplicaveis" title="Mostrar somente os registros selecionados ou todos?" class="lblGeneral">Apl</p>
        <input type=checkbox id="chkAplicaveis" name="chkAplicaveis" title="Mostrar somente os registros aplic?veis?" class="fldGeneral"></input>

        <p id="lblCadastroOK" name="lblCadastroOK" title="Cadastro OK?" class="lblGeneral">Cad</p>
        <input type=checkbox id="chkCadastroOK" name="chkCadastroOK" title="Cadastro OK?" class="fldGeneral"></input>

        <input type="button" id="btnSerasa" name="btnSerasa" value="Serasa" LANGUAGE="javascript" title="Efetuar nova consulta no Serasa" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnReceita" name="btnReceita" value="Receita" title="Acessa o site da Receita Federal" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnSintegra" name="btnSintegra" value="Sintegra" title="Acessa o site do Sintegra" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnFichaCadastral" name="btnFichaCadastral" value="Ficha Cadastral" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnProximo" name="btnProximo" value="Pr�ximo" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    </div>      

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>
    
    
    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
</body>

</html>
