<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalatendimentoHtml" name="modalatendimentoHtml">

    <head>

        <title></title>

        <%
            'Links de estilo, bibliotecas da automacao e especificas
            Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/pessoas/modalpages/modalatendimento.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
            
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
            
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
            
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/pessoas/modalpages/modalatendimento.js" & Chr(34) & "></script>" & vbCrLf
        %>

        <%
        'Script de variaveis globais

            Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
            Response.Write vbcrlf
            Response.Write vbcrlf

            Dim i, sCaller, sCurrDateFormat, nEmpresaID, nUserID, nA1, nA2

            sCaller = ""
            nEmpresaID = 0
            nUserID = 0
            nA1 = 0
            nA2 = 0

            For i = 1 To Request.QueryString("sCaller").Count    
                sCaller = Request.QueryString("sCaller")(i)
            Next

            For i = 1 To Request.QueryString("nEmpresaID").Count    
                nEmpresaID = Request.QueryString("nEmpresaID")(i)
            Next

            For i = 1 To Request.QueryString("nUserID").Count    
                nUserID = Request.QueryString("nUserID")(i)
            Next

            For i = 1 To Request.QueryString("nA1").Count    
                nA1 = Request.QueryString("nA1")(i)
            Next

            For i = 1 To Request.QueryString("nA2").Count    
                nA2 = Request.QueryString("nA2")(i)
            Next

            Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
            Response.Write vbcrlf
            Response.Write "var glb_nA1 = " & Chr(39) & CStr(nA1) & Chr(39) & ";"
            Response.Write vbcrlf
            Response.Write "var glb_nA2 = " & Chr(39) & CStr(nA2) & Chr(39) & ";"
            Response.Write vbcrlf

            'Formato corrente de data
            For i = 1 To Request.QueryString("sCurrDateFormat").Count
                scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
            Next

            If (sCurrDateFormat = "DD/MM/YYYY") Then
                Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
            ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
                Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
            ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
                Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
            Else
                Response.Write "var glb_dCurrDate = '';" & vbcrlf
            End If

            Response.Write "</script>"
            Response.Write vbcrlf
        %>

        <script ID="wndJSProc" LANGUAGE="javascript">
        <!--

        //-->
        </script>

        <!-- //@@ Eventos de grid -->
        <!-- fg -->
        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
            <!--
            if (glb_GridIsBuilding)
                return;

             js_modalatendimento_ValidateEdit(fg, fg.Row, fg.Col)
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
            <!--
            if (glb_GridIsBuilding)
                return;

             js_modalatendimento_AfterEdit (arguments[0], arguments[1]);
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
            <!--
            if (glb_GridIsBuilding)
                return;

             js_modalatendimentoKeyPress(arguments[0]);
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
            <!--
            if (glb_GridIsBuilding)
                return;

            fg_DblClick();
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
            <!--
            if (glb_GridIsBuilding)
                return;

             js_modalatendimento_BeforeEdit(fg, fg.Row, fg.Col);
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
            <!--
            if (glb_GridIsBuilding)
                return;

            js_fg_modalatendimentoBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
            <!--
             js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
            <!--
            if (glb_GridIsBuilding)
                return;

             js_fg_modalatendimentoEnterCell (fg);   
             js_fg_EnterCell (fg);
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
            <!--
            if (glb_GridIsBuilding)
                return;

            js_fg_modalatendimentoAfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
            js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
            //Para celulas checkbox que sao readonly
            treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
            //-->
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
            <!--
            if (glb_GridIsBuilding)
                return;

             js_fg_modalatendimento_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
            //-->
        </SCRIPT>

        <SCRIPT ID="vbFunctions" LANGUAGE="vbscript">
            <!--

            Function daysBetween(date1, date2)
                daysBetween = DateDiff("d",date1,date2)
            End Function

            //-->
        </SCRIPT>

    </head>

    <body id="modalatendimentoBody" name="modalatendimentoBody" LANGUAGE="javascript" onload="return window_onload()">
        <!-- Div Title Bar -->
        <div id="divMod01" name="divMod01" class="divGeneral">
            <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
        </div>    

        <!-- Div Controles -->
        <div id="divControls" name="divControls" class="divGeneral">
    <%
            Dim strSQL, rsData
            
            Set rsData = Server.CreateObject("ADODB.Recordset")
    %>		
    
            <!-- Combo Empresas  -->
            <p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
		    <select id="selEmpresas" name="selEmpresas" class="fldGeneral" MULTIPLE>
    <%
	            strSQL = "SELECT DISTINCT d.PessoaID AS fldID, d.Fantasia AS fldName " & _
				    "FROM	RelacoesPesRec a  WITH (NOLOCK) " & _ 
					    "INNER JOIN RelacoesPesRec_Perfis b  WITH (NOLOCK) ON (a.RelacaoID = b.RelacaoID) " & _  
					    "INNER JOIN Recursos_Direitos c  WITH (NOLOCK) ON (b.PerfilID = c.PerfilID) " & _  
					    "INNER JOIN Pessoas d  WITH (NOLOCK) ON (b.EmpresaID = d.PessoaID) " & _  
					    "INNER JOIN RelacoesPesRec e  WITH (NOLOCK) ON (b.EmpresaID = e.SujeitoID) " & _  
				    "WHERE (a.SujeitoID =  " & CStr(nUserID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND e.EstaEmOperacao=1 AND c.RecursoID = 40006 " & _ 
					    "AND c.RecursoMaeID = 20100 AND c.ContextoID = 1211 AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND e.TipoRelacaoID = 12 " & _ 
					    "AND e.ObjetoID = 999) " & _   
				    "ORDER BY fldName " 

    	       rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	           If (Not (rsData.BOF AND rsData.EOF)) Then
	        	    While Not (rsData.EOF)
	    	    	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "' SELECTED>" )
	    		        Response.Write( rsData.Fields("fldName").Value & "</option>" )
    	    		    rsData.MoveNext()
    	    	    WEnd
         	    End If

	            rsData.Close
    %>		
		    </select>		


            <!-- Combo Estados  -->
		    <p id="lblEstados" name="lblEstados" class="lblGeneral">Est</p>
		    <select id="selEstados" name="selEstados" class="fldGeneral" MULTIPLE>
    <%
                strSQL = "SELECT b.RecursoID AS fldID, b.RecursoAbreviado AS fldName " & _
                     "FROM RelacoesRecursos a WITH(NOLOCK) " & _ 
	                     "INNER JOIN Recursos b WITH(NOLOCK) ON a.SujeitoID = b.RecursoID " & _ 
                    "WHERE (a.ObjetoID = 301 AND a.TipoRelacaoID = 3 AND b.RecursoID NOT IN (5)) " & _ 
                    "ORDER BY a.Ordem "

        	    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	            If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
		                Response.Write("<option value='" & rsData.Fields("fldID").Value & "'")

    		            If ((rsData.Fields("fldID").Value = "2") Or (rsData.Fields("fldID").Value = "3"))Then
	        	            Response.Write(" SELECTED>" )
                        Else
    		                Response.Write(">" )
        			    End If   

		        	    Response.Write( rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
        		    WEnd

	            End If

	            rsData.Close
    %>										
		    </select>		


            <!-- Combo Classifica��es resultantes  -->
   		    <p id="lblClassificacaoResultante" name="lblClassificacaoResultante" title="Classifica��o resultante" class="lblGeneral">Cla</p>
		    <select id="selClassificacaoResultante" name="selClassificacaoResultante" title="Classifica��o resultante" class="fldGeneral"  MULTIPLE>
    <%
                strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                    "FROM dbo.TiposAuxiliares_Itens WITH (NOLOCK) " & _ 
                    "WHERE (TipoID = 29) " & _
                    "ORDER BY Ordem "

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	    If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
		        	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "' " )
		        	    
		        	    If ((rsData.Fields("fldName").Value) <= "C") Then
		        	        Response.Write(" SELECTED")
        			    End If
		        	    
        			    Response.Write( ">" & rsData.Fields("fldName").Value & "</option>" )
		        	    rsData.MoveNext()
        		    WEnd
        	    End If

        	    rsData.Close
    %>										
		    </select>


            <!-- Combo Classifica��o interna  -->
		    <p id="lblClassificacaoInterna" name="lblClassificacaoInterna" title="Classifica��o interna" class="lblGeneral">Int</p>
		    <select id="selClassificacaoInterna" name="selClassificacaoInterna" title="Classifica��o interna" class="fldGeneral">
    <%
                strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem " & _
                    "UNION ALL " & _
                    "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                        "FROM dbo.TiposAuxiliares_Itens WITH (NOLOCK) " & _ 
                        "WHERE (TipoID = 29) " & _
                        "ORDER BY Ordem "

        	    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	    If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
		        	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
        			    Response.Write( rsData.Fields("fldName").Value & "</option>" )
		        	    rsData.MoveNext()
        		    WEnd
	            End If

        	    rsData.Close
    %>										
		    </select>
    		
            <!-- Combo Classifica��o externa  -->
		    <p id="lblClassificacaoExterna" name="lblClassificacaoExterna" title="Classifica��o externa" class="lblGeneral">Ext</p>
		    <select id="selClassificacaoExterna" name="selClassificacaoExterna" title="Classifica��o externa" class="fldGeneral">
    <%
                strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem " & _
                    "UNION ALL " & _
                    "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
                        "FROM dbo.TiposAuxiliares_Itens WITH (NOLOCK) " & _ 
                        "WHERE TipoID = 29 " & _
                        "ORDER BY Ordem "

        	    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	            If (Not (rsData.BOF AND rsData.EOF) ) Then
		            While Not (rsData.EOF)
        			    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
		        	    Response.Write( rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
        		    WEnd
	            End If

	            rsData.Close
    %>												
		    </select>		
                	
            <p id="lblPessoas" name="lblPessoas" title="Pessoas" class="lblGeneral">P</p>
            <input type=checkbox id="chkPessoas" name="chkPessoas" title="Pessoas" class="fldGeneral"></input>         
            <p id="lblRelacoesPessoas" name="lblRelacoesPessoas" title="Relaca��es Pessoas" class="lblGeneral">R</p>
            <input type=checkbox id="chkRelacoesPessoas" name="chkRelacoesPessoas" title="Relaca��es Pessoas" class="fldGeneral"></input>         
            <p id="lblSegurosCredito" name="lblSegurosCredito" title="Seguros de Cr�dito" class="lblGeneral">S</p>
            <input type=checkbox id="chkSegurosCredito" name="chkSegurosCredito" title="Seguros de Cr�dito" class="fldGeneral"></input>
            <p id="lblContatos" name="lblContatos" title="Contatos?" class="lblGeneral">C</p>
            <input type="checkbox" id="chkContatos" name="chkContatos" title="Contatos?" class="fldGeneral"></input>
            
            
            <!-- Combo Classifica��o  -->
   		    <p id="lblClassificacao" name="lblClassificacao" class="lblGeneral">Classifica��o</p>
		    <select id="selClassificacao" name="selClassificacao" class="fldGeneral">
    <%
                strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, -1 AS Ordem " & _
                    "UNION ALL " & _
                    "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem " & _
                        "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                        "WHERE (EstadoID = 2 AND TipoID = 13 AND " & _
                            "dbo.fn_Direitos_TiposAuxiliares(ItemID, NULL, " & CStr(nUserID) & ", GETDATE()) > 0) " & _
                    "ORDER BY Ordem"

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	    If (Not (rsData.BOF AND rsData.EOF) ) Then
		            While Not (rsData.EOF)
        			    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
		        	    Response.Write( rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
		            WEnd
	            End If

	            rsData.Close
    %>										
		    </select>
            

            <!-- Combo Proprietario  -->
   		    <p id="lblProprietario" name="lblProprietario" class="lblGeneral">Propriet�rio</p>
		    <select id="selProprietario" name="selProprietario" class="fldGeneral">
    <%
                strSQL = ""
        
                If (CInt(nA2) = 1) Then
        
                    strSQL = "SELECT 0 AS Ordem, 0 AS fldID, SPACE(0) AS fldName " & _
                        "UNION ALL " & _
                        "SELECT DISTINCT 0 AS Ordem, b.PessoaID AS fldID, b.Fantasia AS fldName " & _
                            "FROM Pessoas a WITH(NOLOCK) " & _
                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) " & _                   
                            "WHERE b.PessoaID=" & CStr(nUserID) & " " & _   
                        "UNION " & _
                        "SELECT DISTINCT 0 AS Ordem, b.PessoaID AS fldID, b.Fantasia AS fldName " & _
                            "FROM RelacoesPessoas a WITH(NOLOCK) " & _
                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) " & _                    
                            "WHERE (a.TipoRelacaoID = 21) " & _                           
                        "UNION " & _
                        "SELECT DISTINCT 1 AS Ordem, b.PessoaID AS fldID, b.Fantasia AS fldName " & _
                            "FROM RelacoesPessoas a WITH(NOLOCK) " & _
                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ObjetoID = b.PessoaID) " & _
                            "WHERE (a.TipoRelacaoID = 34 AND a.EstadoID = 2 AND a.SujeitoID=" & CStr(nUserID) & ") " & _
                        "ORDER BY Ordem, fldName"
                Else     
                    strSQL = "SELECT DISTINCT 0 AS Ordem, d.PessoaID AS fldID, d.Fantasia AS fldName " & _
                            "FROM RelacoesPessoas a WITH(NOLOCK) " & _
                                "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (a.ObjetoID = b.ObjetoID) " & _
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON (b.SujeitoID = c.ProprietarioID) " & _                    
                                "INNER JOIN Pessoas d WITH(NOLOCK) ON (b.SujeitoID = d.PessoaID) " & _
                                "INNER JOIN Pedidos e WITH(NOLOCK) ON (d.PessoaID = e.ProprietarioID) " & _
                            "WHERE (a.TipoRelacaoID = 34 AND a.EstadoID = 2 AND b.TipoRelacaoID = 34 AND b.EstadoID = 2 " & _
                                "AND a.SujeitoID=" & CStr(nUserID) & ") " & _    
                                "AND dbo.fn_Colaborador_Dado(d.PessoaID, 3) IN (562,563) " & _
                        "UNION " & _
                        "SELECT DISTINCT 0 AS Ordem, d.PessoaID AS fldID, d.Fantasia AS fldName " & _
                            "FROM RelacoesPessoas a WITH(NOLOCK) " & _
                                "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (a.ObjetoID = b.ObjetoID) " & _
                                "INNER JOIN RelacoesPessoas c WITH(NOLOCK) ON (b.SujeitoID = c.ProprietarioID) " & _
                                "INNER JOIN Pessoas d WITH(NOLOCK) ON (b.SujeitoID = d.PessoaID) " & _
                                "INNER JOIN Pedidos e WITH(NOLOCK) ON (d.PessoaID = e.ProprietarioID) " & _
                            "WHERE (a.TipoRelacaoID = 34 AND a.EstadoID = 2 AND b.TipoRelacaoID = 34 AND b.EstadoID = 2 " & _
                                "AND c.TipoRelacaoID = 21 AND a.SujeitoID=" & CStr(nUserID) & ") " & _
                                "AND dbo.fn_Colaborador_Dado(d.PessoaID, 3) IN (562,563) " & _
                        "UNION " & _
                        "SELECT DISTINCT 1 AS Ordem, b.PessoaID AS fldID, b.Fantasia AS fldName " & _
                            "FROM RelacoesPessoas a WITH(NOLOCK) " & _
                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ObjetoID = b.PessoaID) " & _
                            "WHERE (a.TipoRelacaoID = 34 AND a.EstadoID = 2 AND a.SujeitoID=" & CStr(nUserID) & ") " & _
                        "ORDER BY Ordem, fldName"
                End If

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	    If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
		        	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'" )

        			    If (CDbl(rsData.Fields("fldID").Value) = CDbl(nUserID)) Then
		        	        Response.Write(" SELECTED")
        			    End If
    			
		        	    Response.Write(">" & rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
		            WEnd
        	    End If

	            rsData.Close
    %>										
		    </select>

            <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Pesquisa</p>
            <input type="text" id="txtPesquisa" name="txtPesquisa" class="fldGeneral"></input>             
            
            <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>

            <!-- Combo Servi�os  -->
            <p id="lblServico" name="lblServico" class="lblGeneral">Servi�o</p>
		    <select id="selServico" name="selServico" class="fldGeneral">
    		    <option value="1">Cadastro</option>
                <option value="2">Cr�dito</option>
                <option value="3" SELECTED>Carteira</option>
		    </select>	
            
            <!-- Combo Atendimento  -->
            <p id="lblAtendimento" name="lblAtendimento" class="lblGeneral">Atendimento</p>
		    <select id="selAtendimento" name="selAtendimento" class="fldGeneral">
    <%
                strSQL = "SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName, EhDefault " & _
	                "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
                    "WHERE ((a.TipoID = 51) AND (a.EstadoID = 2)) " & _
                    "ORDER BY a.Ordem "

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	    If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
        		        Response.Write("<option value='" & rsData.Fields("fldID").Value & "' ")
    		    
        		        If (CBool(rsData.Fields("EhDefault").Value))Then
        		            Response.Write(" SELECTED " )
                        End If
                
        			    Response.Write(">" )
        			    Response.Write(rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
        		    WEnd
        	    End If

        	    rsData.Close
    %>										
		    </select>	

            <!-- Combo Abordagem  -->
            <p id="lblAbordagem" name="lblAbordagem" class="lblGeneral">Abordagem</p>
		    <select id="Select1" name="selAbordagem" class="fldGeneral">
    <%
                strSQL = "SELECT a.ItemID AS fldID, a.ItemAbreviado AS fldName, a.Filtro, " & _
    	        	    "SUBSTRING(a.Filtro, (CHARINDEX('<', a.Filtro) + 1), ((CHARINDEX('>', a.Filtro) - CHARINDEX('<', a.Filtro) - 1))) AS DiasMinimo, " & _
	    	            "SUBSTRING(a.Filtro, (CHARINDEX('(', a.Filtro) + 1), ((CHARINDEX(')', a.Filtro) - CHARINDEX('(', a.Filtro) - 1))) AS DiasMaximo, " & _
        	    	    "EhDefault " & _
	                "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
                    "WHERE ((a.TipoID = 50) AND (a.EstadoID = 2)) " & _
                    "ORDER BY a.Ordem "

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	    If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
        		        Response.Write("<option value='" & rsData.Fields("fldID").Value & "' ")
    		    
        		        If (CBool(rsData.Fields("EhDefault").Value))Then
        		            Response.Write(" SELECTED " )
                        End If
                
                        Response.Write("DiasMinimo='" & rsData.Fields("DiasMinimo").Value & "' ")
                        Response.Write("DiasMaximo='" & rsData.Fields("DiasMaximo").Value & "' ")
        			    Response.Write(">" )
        			    Response.Write(rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
        		    WEnd
        	    End If

        	    rsData.Close
    %>										
		    </select>	

            <p id="lblPedidosPendentes" name="lblPedidosPendentes" title="S� pedidos pendentes?" class="lblGeneral">PP</p>
            <input type="checkbox" id="chkPedidosPendentes" name="chkPedidosPendentes" title="S� pedidos pendentes?" class="fldGeneral"></input>         
            <p id="lblSeguradora" name="lblSeguradora" class="lblGeneral">Seguradora</p>

		    <select id="selSeguradora" name="selSeguradora" class="fldGeneral">
    <%
                strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
                    "UNION ALL " & _
                    "SELECT a.PessoaID AS fldID, a.Fantasia AS fldName " & _
    		            "FROM	Pessoas a WITH(NOLOCK) " & _  
	    	            "WHERE (a.EstadoID = 2 AND a.ClassificacaoID = 70) " & _  
		            "ORDER BY fldName" 		

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

                If (Not (rsData.BOF AND rsData.EOF) ) Then
    		        While Not (rsData.EOF)
	        	        Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
        			    Response.Write( rsData.Fields("fldName").Value & "</option>" )
		        	    rsData.MoveNext()
		            WEnd
	            End If

        	    rsData.Close
    %>								
		    </select>


		    <p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
		    <select id="selMarca" name="selMarca" class="fldGeneral">
    <%
                strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
                    "UNION ALL " & _    
                    "SELECT DISTINCT a.ConceitoID AS fldID, a.Conceito AS fldName " & _
			    	    "FROM Conceitos a WITH(NOLOCK) " & _
				        "WHERE (a.EstadoID = 2 AND a.TipoConceitoID = 304) " & _
				        "ORDER BY fldName"

        	    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	            If (Not (rsData.BOF AND rsData.EOF) ) Then
		            While Not (rsData.EOF)
			            Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			            Response.Write( rsData.Fields("fldName").Value & "</option>" )
			            rsData.MoveNext()
		            WEnd
	            End If

	            rsData.Close
    %>								
		    </select>				        

            <p id="lblLimiteMinimo" name="lblLimiteMinimo" class="lblGeneral">Lim min</p>
            <input type="text" id="txtLimiteMinimo" name="txtLimiteMinimo" title="Limite de cr�dito m�nimo" class="fldGeneral"></input>         
            <p id="lblLimiteMaximo" name="lblLimiteMaximo" class="lblGeneral">Lim max</p>
            <input type="text" id="txtLimiteMaximo" name="txtLimiteMaximo" title="Limite de cr�dito m�ximo" class="fldGeneral"></input>         
            <p id="lblSaldoMinimo" name="lblSaldoMinimo" class="lblGeneral">Saldo min</p>
            <input type="text" id="txtSaldoMinimo" name="txtSaldoMinimo" title="Saldo de cr�dito m�nimo" class="fldGeneral"></input>                        
            <p id="lblAgendamento" name="lblAgendamento" title="Aplicar ordem de agendamento?" class="lblGeneral">Ag</p>
            <input type="checkbox" id="chkAgendamento" name="chkAgendamento" title="Aplicar ordem de agendamento?" class="fldGeneral"></input>
            
            <input type="button" id="btnWeb" name="btnWeb" value="W" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Acessar Web"></input>
            <input type="button" id="btnAtender" name="btnAtender" value="Atender" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            <input type="button" id="btnListaPrecos" name="btnListaPrecos" value="Pre�os" title="Detalhar lista de pre�os" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            
            <p id="lblDiasMinimo" name="lblDiasMinimo" class="lblGeneral">Dias m�n</p>
            <input type="text" id="txtDiasMinimo" name="txtDiasMinimo" title="Dias da �ltima compra - m�nimo" class="fldGeneral"></input>         
            <p id="lblDiasMaximo" name="lblDiasMaximo" class="lblGeneral">Dias max</p>
            <input type="text" id="txtDiasMaximo" name="txtDiasMaximo" title="Dias da �ltima compra - m�ximo" class="fldGeneral"></input>                         
            <p id="lblDiasVencidos" name="lblDiasVencidos" class="lblGeneral">DV</p>
            <input type="text" id="txtDiasVencidos" name="txtDiasVencidos" title="N�mero de dias vencidos das informa��es de cr�dito" class="fldGeneral"></input>         

            <input type="button" id="btnIncluir" name="btnIncluir" value="I" title="Incluir seguro de cr�dito" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            <input type="button" id="btnExcluir" name="btnExcluir" value="E" title="Excluir seguro de cr�dito" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        </div>    


        <!-- Div Grid -->
        <div id="divFG" name="divFG" class="divGeneral">
            <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
            </object>
            <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
            <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
            <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
        </div>    

        
        <!-- Div Telefones/URLs -->
        <div id="divControlsFone" name="divControlsFone" class="divGeneral">
            <p id="lblDDITelefone" name="lblDDITelefone" class="lblGeneral">DDI</p>
            <input type="text" id="txtDDITelefone" name="txtDDITelefone" class="fldGeneral"></input>         
            <p id="lblDDDComercial" name="lblDDDComercial" class="lblGeneral">DDD</p>
            <input type="text" id="txtDDDComercial" name="txtDDDComercial" class="fldGeneral"></input>         
            <p id="lblFoneComercial" name="lblFoneComercial" class="lblGeneral">Comercial</p>
            <input type="text" id="txtFoneComercial" name="txtFoneComercial" class="fldGeneral"></input>           
            <p id="lblDDDFaxCelular" name="lblDDDFaxCelular" class="lblGeneral">DDD</p>
            <input type="text" id="txtDDDFaxCelular" name="txtDDDFaxCelular" class="fldGeneral"></input>         
            <p id="lblFaxCelular" name="lblFaxCelular" class="lblGeneral">Celular</p>
            <input type="text" id="txtFaxCelular" name="txtFaxCelular" class="fldGeneral"></input>         
            <p id="lblEmail" name="lblEmail" class="lblGeneral">E-mail</p>
            <input type="text" id="txtEmail" name="txtEmail" class="fldGeneral"></input>         
            <p id="lblSite" name="lblSite" class="lblGeneral">Site</p>
            <input type="text" id="txtSite" name="txtSite" class="fldGeneral"></input>                             
        </div>    

    <div id="divFinalizarAtendimento" name="divFinalizarAtendimento" class="divGeneral" style="top:-25px;">
        <p class="lblGeneral" id="lblMeioAtendimento" style="width:120px; height:17px; top:0px; left:10px;">
			<b>Meio de atendimento</b>
		</p>

<%
	Dim height
	Dim top
	Dim left
	
	height =  17
	top    =  0
	left   =  10

    strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName " & _
		"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
		"WHERE TipoID = 52 AND EstadoID=2 " & _
		"ORDER BY Ordem"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
        Response.Write "<input type='radio' " & _
            "id='rd" & rsData.Fields("fldID").Value & "' " & _
            "registroID='" & rsData.Fields("fldID").Value & "' " & _
            "onclick='return rdMeioAntedimentoOnclick(this)' " & _
			"name='rdMeioAtendimento' value='" & rsData.Fields("fldName").Value & "' " & _
			"class='fldGeneral' " & _
			"style='width:17px; height:17px; top:" & CStr(top+height) & "px; left:" & CStr(left) & "px;'/>" & vbcrlf
			
        Response.Write "<p class='lblGeneral' " & _
        "id='lbl" & rsData.Fields("fldID").Value & "' " & _
        "registroID='" & rsData.Fields("fldID").Value & "' " & _
        "meioAtendimentoID='" & rsData.Fields("fldID").Value & "' " & _
        "onclick='return rdMeioAntedimentoOnclick(this)' " & _
        "style='width:120px; height:17px; top:" & CStr(top+height) & "px; left:" & CStr(left + 17) & "px;'>" & _
        rsData.Fields("fldName").Value & "</p>" & vbcrlf

		rsData.MoveNext
		top = (top + 17)
	Wend

	rsData.Close
%>
        <p class="lblGeneral" id="lblResultadoAtendimento" style="width:160px; height:17px; top:0px; left:190px;">
			<b>Resultado do atendimento</b>
		</p>
<%
	height =  17
	top    =  0
	left   =  190

    strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Numero " & _
		"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
		"WHERE TipoID = 49 AND EstadoID = 2 " & _
		"ORDER BY Ordem"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData.EOF)
        Response.Write "<input type='radio' " & _
            "id='rd" & rsData.Fields("fldID").Value & "' " & _
            "registroID='" & rsData.Fields("fldID").Value & "' " & _
            "numero='" & rsData.Fields("Numero").Value & "' " & _
            "onclick='return rdResultadoAtendimentoOnclick(this)' " & _
			"name='rdResultadoAtendimento' " & _
			"class='fldGeneral' " & _
			"style='width:17px; height:17px; top:" & CStr(top+height) & "px; left:" & CStr(left) & "px;'/>" & vbcrlf
			
        Response.Write "<p class='lblGeneral' " & _
            "id='lbl" & rsData.Fields("fldID").Value & "' " & _
            "registroID='" & rsData.Fields("fldID").Value & "' " & _
            "numero='" & rsData.Fields("Numero").Value & "' " & _
            "onclick='return rdResultadoAtendimentoOnclick(this)' " & _
            "style='width:120px; height:17px; top:" & CStr(top+height) & "px; left:" & CStr(left + 17) & "px;'>" & _
            rsData.Fields("fldName").Value & _
        "</p>" & vbcrlf

		rsData.MoveNext
		top = (top + 17)
	Wend

	rsData.Close
	Set rsData = Nothing
%>

        <p class="lblGeneral" id="lblAdiarDias" name="lblAdiarDias">Adiar (d/h/m)</p>
        <input type="text" id="txtAdiarDias" name="txtAdiarDias" class="fldGeneral"/>
        <p class="lblGeneral" id="lblAdiarHoras" name="lblAdiarHoras"></p>
        <input type="text" id="txtAdiarHoras" name="txtAdiarHoras" class="fldGeneral"/>
        <p class="lblGeneral" id="lblAdiarMinutos" name="lblAdiarMinutos"></p>
        <input type="text" id="txtAdiarMinutos" name="txtAdiarMinutos" class="fldGeneral"/>

        <p class="lblGeneral" id="lblCalcularAgenda"></p>
		<input type="button" id="btnCalcularAgenda" name="btnCalcularAgenda" value="Calcular" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Calcular data do agendamento"/>
		
		<input type="button" id="btnSolicitacaoCredito" name="btnSolicitacaoCredito" value="Solicitar an�lise de cr�dito" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Solicitar an�lise de cr�dito?"/>

        <p class="lblGeneral" id="lblAgendar">Agendar para</p>
        <input type="text" id="txtAgendar" name="txtAgendar" class="fldGeneral"/>

        <p class="lblGeneral" id="lblObservacoes">Observa��es</p>
        <textarea id="txtObservacoes" name="txtObservacoes" class="fldGeneral"></textarea>        
    
		<input type="button" id="btnConfirmarFinalizacao" name="btnConfirmarFinalizacao" value="OK" onclick="return btn_onclick(this)" class="btns"/>
		<input type="button" id="btnCancelarFinalizacao" name="btnCancelarFinalizacao" value="Cancelar" onclick="return btn_onclick(this)" class="btns"/>
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        
    </body>

</html>
