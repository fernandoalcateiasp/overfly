/********************************************************************
modalemailcadastro.js

Library javascript para o modalemailcadastro.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0; // Controla se algum dado do grid foi alterado

// Gravacao de dados do grid .RDS
var dsoGravacao = new CDatatransport("dsoGravacao");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
sels_onchange()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

	fillControlsData();
	
    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalemailcadastroBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
	secText('E-mail de confirmação de cadastro', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var y_Gap = 0;
    var div_Width = 0;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // reajusta dimensoes e reposiciona a janela
	redimAndReposicionModalWin(320, 160, false);
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    elem1 = lblTexto;
	with (elem1.style)
    {
		left = 0;
		top = 0;
		fontSize = '8pt';
		width = (elem1.innerText.length)*5;
	    height = '12pt';
	    backgroundColor = 'transparent';
	}
    y_Gap = elem1.offsetTop + elem1.offsetHeight + ELEM_GAP;
    div_Width = elem1.offsetLeft + elem1.offsetWidth;
    
    elem = rdEmailPessoa;
    elem1 = lblEmailPessoa;
    with (elem.style)
    {
		left = 20;
		top = y_Gap;
		width = '20pt';
	    height = '10pt';
	}
	with (elem1.style)
    {
		left = elem.offsetLeft + elem.offsetWidth + (ELEM_GAP / 2);
		top = elem.offsetTop - 2;
		width = (elem1.innerText.length)*FONT_WIDTH;
	    height = '12pt';
		fontSize = '10pt';
	    backgroundColor = 'transparent';
	}
	elem1.onclick = lbl_onclick;
	
	div_Width = Math.max( div_Width, elem1.offsetLeft + elem1.offsetWidth);
	y_Gap = elem1.offsetTop + elem1.offsetHeight + ELEM_GAP;
	
    elem = rdEmailUsuarioLogado;
    elem1 = lblEmailUsuarioLogado;
    with (elem.style)
    {
		left = 20;
		top = y_Gap;
		width = '20pt';
	    height = '10pt';
	}
	with (elem1.style)
    {
		left = elem.offsetLeft + elem.offsetWidth + (ELEM_GAP / 2);
		top = elem.offsetTop - 2;
		width = (elem1.innerText.length)*6;
	    height = '12pt';
	    fontSize = '10pt';
	    backgroundColor = 'transparent';
	}
    elem1.onclick = lbl_onclick;
    //elem1.innerHTML = getCurrUserEmail();
    
    div_Width = Math.max( div_Width, elem1.offsetLeft + elem1.offsetWidth);
    
	// ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = div_Width;    
        
        left = btnOK.offsetLeft + ((btnCanc.offsetLeft + btnCanc.offsetWidth - btnOK.offsetLeft) / 2) -
			   (div_Width / 2);
        
        height = lblEmailUsuarioLogado.offsetTop + lblEmailUsuarioLogado.offsetHeight + 2;
    }
    
}

/********************************************************************
Usuario clicou label de radio
********************************************************************/
function lbl_onclick()
{
	if (this == lblEmailPessoa)
	{
		rdEmailPessoa.checked = true;
		rdEmailUsuarioLogado.checked = false;
	}
	else if (this == lblEmailUsuarioLogado)
	{
		rdEmailPessoa.checked = false;
		rdEmailUsuarioLogado.checked = true;
	}
}

/********************************************************************
Usuario teclou Enter em campo texto
********************************************************************/
function txt_onkeydown()
{
	if ( event.keyCode == 13 )	
		;
}

/********************************************************************
Usuario clicou em checkbox
********************************************************************/
function chks_onclick()
{
	;
}

/********************************************************************
Usuario alterou selecao de combo
********************************************************************/
function sels_onchange()
{
	adjustLabelCombos(this);

	if (this == selConceitoID)
	{
		adjustBtnsStatus();
	}
}

function adjustLabelCombos(elem)
{
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		enviarEmail();

		return null;
    }
    else if (controlID == 'btnCanc')
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null); 
        
        var nPessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCellValueByColKey(fg, \'PessoaID\', fg.Row)') );
        
        if (glb_sCaller=='PL')
            window.top.openModalHTML(nPessoaID, 'Ficha Cadastral', 'PL', 3, 1);
        else if (glb_sCaller=='S')            
            window.top.openModalHTML(nPessoaID, 'Ficha Cadastral', 'S', 3, 1);
    }
}

function enviarEmail()
{
	if ((!rdEmailPessoa.checked) && (!rdEmailUsuarioLogado.checked))
	{
		if ( window.top.overflyGen.Alert('Selecione um e-mail.') == 0 )
		    return null;
		    
		return null;		    
	}

	lockControlsInModalWin(true);
	
	var strPars = new String();
	var nTipoEmail = 0;
	
	if (rdEmailPessoa.checked)
		nTipoEmail = 1;
	else
		nTipoEmail = 2;

    var strPars = new String();
    var empresa = getCurrEmpresaData();
    var nEmpresaID = escape(empresa[0].toString());
    var nUserID = escape(getCurrUserID());

    strPars = '?nPessoaID=' + glb_nPessoaID;
    strPars += '&nTipoEmail=' + escape(nTipoEmail);
    strPars += '&nEmpresaID=' + nEmpresaID;
    strPars += '&nUserID=' + nUserID;

    dsoGravacao.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/emailconfirmacaocadastro.aspx' + strPars;
    dsoGravacao.ondatasetcomplete = gravacao_DSC;
    dsoGravacao.refresh();
}

function gravacao_DSC()
{
    if (! (dsoGravacao.recordset.BOF && dsoGravacao.recordset.EOF) )
    {
		if ( window.top.overflyGen.Alert(dsoGravacao.recordset['Msg'].value) == 0 )
		    return null;
    }
    
	// Simula o botao OK para fechar a janela e forcar um refresh no sup
	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Preenche os campos de dado
********************************************************************/
function fillControlsData()
{
	lockControlsInModalWin(false);
}


function adjustBtnsStatus()
{
	;
}
