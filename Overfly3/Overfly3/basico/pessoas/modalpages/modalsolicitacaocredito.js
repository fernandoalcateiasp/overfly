/********************************************************************
Declara��o de vari�veis globais.
********************************************************************/
var glb_aEmpresa = getCurrEmpresaData();
var glb_nUserID = getCurrUserID();
var glb_nParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PessoaID'].value");
var glb_nMoedaEmpresaSistemaID = glb_aEmpresa[9];
var glb_nValorCreditoPadrao;
var dsoGeraCredito = new CDatatransport("dsoGeraCredito");
var dsoValoresRelPesRec = new CDatatransport("dsoValoresRelPesRec");

/********************************************************************
Configura o html.
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    with (modalSolicitacaoCreditoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    with (divSolicitacaoCredito.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
    }

    valoresRelPesRec();
    setupPage();
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html.
********************************************************************/
function setupPage() {

    secText('Solicita��o de Cr�dito', 1);

    var elem;
    var frameRect;
    var modWidth = 0;

    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
    }

    // ajusta o divProduto
    adjustElementsInForm([['lblPrioridade', 'chkPrioridade', 3, 1, -5],
        ['lblMoeda', 'selMoeda', 7, 1, 3],
        ['lblValorSolicitado', 'txtValorSolicitado', 20, 1, 3],
        ['lblSeguradoraCredito', 'chkSeguradoraCredito', 3, 1, 3],
        ['lblMotivo', 'txtMotivo', 37, 2, -5]],null, null, true);

    selMoeda.value = glb_nMoedaEmpresaSistemaID;
    selMoeda.disabled = true;

    txtValorSolicitado.onkeypress = verifyNumericEnterNotLinked;
    txtValorSolicitado.setAttribute('verifyNumPaste', 1);
    txtValorSolicitado.setAttribute('thePrecision', 11, 1);
    txtValorSolicitado.setAttribute('theScale', 2, 1);
    txtValorSolicitado.setAttribute('minMax', new Array(0, 99999999999), 1);
    txtValorSolicitado.maxLength = 13;
    txtMotivo.maxLength = 50;
    lblPrioridade.style.color = lblValorSolicitado.style.color = lblSeguradoraCredito.style.color = lblMotivo.style.color = 'green';
}

/********************************************************************
Clique botao OK ou Cancela.
********************************************************************/
function btn_onclick(ctl) {

    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        geraCredito();
    }
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
}

/********************************************************************
Cria Cr�dito.
********************************************************************/
function geraCredito() {

    var _retMsg;
    var _msg = new String();
    var _valorSolicitado = txtValorSolicitado.value;
    var _seguradora = chkSeguradoraCredito.checked ? "1" : "0";
    var _prioridade = chkPrioridade.checked ? "1" : "0";
    var _motivo = txtMotivo.value;

    if ((_valorSolicitado == "") || (_valorSolicitado == 0)) {
        _msg += 'Voc� n�o informou o valor da solicita��o de cr�dito. Ser� solicitado o valor padr�o de ' + glb_aEmpresa[10] + ' ' + glb_nValorCreditoPadrao + '.';
    }

    if (_msg.length > 0) {

        _retMsg = window.top.overflyGen.Confirm(_msg + '\nDeseja solicitar?');

        if ((_retMsg == 0) || (_retMsg == 2)) {
            lockControlsInModalWin(false);
            return;
        } else {
            _valorSolicitado = glb_nValorCreditoPadrao;
        }
    }

    var strPars = new String();
    strPars = '?nParceiroID=' + glb_nParceiroID;
    strPars += '&nPaisID=' + glb_aEmpresa[1];
    strPars += '&nMoedaID=' + glb_nMoedaEmpresaSistemaID;
    strPars += '&nValorSolicitado=' + _valorSolicitado;
    strPars += '&nSolicitanteID=' + glb_nUserID;
    strPars += '&bSeguradora=' + _seguradora;
    strPars += '&bPrioridade=' + _prioridade;
    strPars += '&sMotivo=' + _motivo;



    dsoGeraCredito.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/geracredito.aspx' + strPars;
    dsoGeraCredito.ondatasetcomplete = geraCredito_DSC;
    dsoGeraCredito.Refresh();
}

/********************************************************************
Retorno da cria��o de cr�dito.
********************************************************************/
function geraCredito_DSC() {
    var _mensagem = dsoGeraCredito.recordset['Mensagem'].value;
    if (!((dsoGeraCredito.recordset.BOF) && (dsoGeraCredito.recordset.EOF))) {
            if (window.top.overflyGen.Alert(_mensagem) == 0)
                return null;
    }

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null);
    lockControlsInModalWin(false);
}

/********************************************************************
Retorno da cria��o de cr�dito.
********************************************************************/
function valoresRelPesRec() {

    setConnection(dsoValoresRelPesRec);

    dsoValoresRelPesRec.SQL = 'SELECT MAX(a.ValorCreditoAutomatico) AS ValorCreditoAutomatico ' +
                                'FROM RelacoesPesRec_Creditos a WITH(NOLOCK) ' +
                                    'INNER JOIN RelacoesPesRec b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
                                'WHERE ((dbo.fn_Pessoa_Localidade(b.SujeitoID, 1, NULL, NULL)) = ' + glb_aEmpresa[1] + ')';

    dsoValoresRelPesRec.ondatasetcomplete = valoresRelPesRec_DSC;
    dsoValoresRelPesRec.Refresh();
}

/********************************************************************
Retorno da cria��o de cr�dito.
********************************************************************/
function valoresRelPesRec_DSC() {

    glb_nValorCreditoPadrao = dsoValoresRelPesRec.recordset['ValorCreditoAutomatico'].value;
    return null;
}