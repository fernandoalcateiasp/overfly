/********************************************************************
modalclonarrotas.js

Library javascript para o modalclonarrotas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_CaracteristicasTimerInt = null;

// Gravacao no Servidor .URL
var dsoClonar = new CDatatransport("dsoClonar");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalclonarrotas.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalclonarrotas.ASP

js_fg_modalclonarrotasBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalclonarrotasDblClick( grid, Row, Col)
js_modalclonarrotasKeyPress(KeyAscii)
js_modalclonarrotas_AfterRowColChange
js_modalclonarrotas_ValidateEdit()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalclonarrotasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Clonar rotas', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = (selRota.options.length == 0);
    selRota.disabled = (selRota.options.length == 0);

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    adjustElementsInForm([['lblRota','selRota',40,1,0,20]]);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		clonarRota();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }
        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
}

function clonarRota()
{
	var strPars = '?nDestinoPesTransportadoraID=' + escape(glb_nPesTransportadoraID) +
		'&nOrigemPesTransportadoraID=' + escape(selRota.value);

    dsoClonar.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/clonarrota.aspx' + strPars;
    dsoClonar.ondatasetcomplete = dsoClonar_DSC;
    dsoClonar.Refresh();
}

function dsoClonar_DSC()
{
	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    showExtFrame(window, true);
}

// FINAL DE EVENTOS DE GRID *****************************************
