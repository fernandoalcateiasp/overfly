/********************************************************************
modalRateios.js

Library javascript para o modalRateios.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

//var glb_aEmpresaData = getCurrEmpresaData();
var dsoPessoas = new CDatatransport("dsoPessoas");
var dsoDadosRH = new CDatatransport("dsoDadosRH");
var dsoIncluirPessoa = new CDatatransport("dsoIncluirPessoa");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalDadosRHBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
    fillPessoas();

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 39;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }
    
    //top = parseInt(divText.currentStyle.top, 10) + parseInt(divText.currentStyle.height, 10) + ELEM_GAP;

    //1-aElements        -> Array  bidimensional que contem sub-arrays dentro dela:
    //Este array tem a seguinte estrutura:
    //Primeira dimensao: -> Um array que contem os sub-arrays dentro dele
    //Segunda dimensao: ->  Um array com 3 elementos na seguinte ordem:
    //0-> ID do Label relacionado ao controle
    //1-> ID do controle
    //2-> Quantidade de caracteres que o controle suporta
    //3-> Numero da linha que aparecera na tela (para o form superior de 1 a 4)

    adjustElementsInForm([
       ['lblContratodeTrabalho', 'lblContratodeTrabalho', 20, 1, 0, ELEM_GAP],
       ['lblDataFim', 'txtDataFim', 10, 2, 0, -10],
       ['lblDiasContrato', 'selDiasContrato', 15, 2],
       ['lblSindicato', 'selSindicato', 20, 2],
       ['lblContribuicaoSindicalAnual', 'chkContribuicaoSindicalAnual', 3, 2, 0, 3],
       ['lblSituacaodeTrabalho', 'lblSituacaodeTrabalho', 20, 3, 0, -10],
       ['lblTipoAdmissao', 'selTipoAdmissao', 15, 4, 0, -10],
       ['lblHorariodeTrabalho', 'txtHorariodeTrabalho', 15, 4],
       ['lblCargaHoraria', 'txtCargaHoraria', 15, 4],
       ['lblAdiantamentoQuinzenal', 'chkAdiantamentoQuinzenal', 3, 4, 0, 3],
       ['lblAgenteIntegracao', 'selAgenteIntegracao', 16, 5],
       ['lblDadosdeEstagio', 'lblDadosdeEstagio', 20, 6, 0, -10],
       ['lblAreadeAtuacao', 'txtAreadeAtuacao', 19, 7, 0, -10],
       ['lblNumeroApoliceSeguro', 'txtNumeroApoliceSeguro', 18, 7],
       ['lblCurso', 'txtCurso', 19, 7],
       ['lblSemestre', 'selSemestre', 6, 7],
       ['lblInstituicaoEnsino', 'selInstituicaoEnsino', 22, 8, 0, ELEM_GAP],
       ['lblSupervisor', 'selSupervisor', 18, 8],
       ['lblNecessidadesEspeciais', 'lblNecessidadesEspeciais', 20, 9, 0, -10],
       ['lblTipoNecessidade', 'selTipoNecessidade', 22, 10, 0, -10],
       ['lblCodigoCID', 'txtCodigoCID', 20, 10]
       ], null, null, true);

    carregaCampos();

}

// EVENTOS DE GRID **************************************************

// FINAL DE EVENTOS DE GRID *****************************************


/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id){
        gravaConfirmacaoDados();
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);

    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);

}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
- null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // mostra a janela modal
    //preencheFabricante();

    ;
}

function fillPessoas() {
    var nPessoaID = glb_nPessoaID;

    if ((nPessoaID == null) || (nPessoaID == ''))
        return null;

    setConnection(dsoPessoas);

    //Dados RH - Fernando Gomes (2114)
    strSQL = 'SELECT \'Dados RH - \' + dbo.fn_Pessoa_Fantasia(a.PessoaID,0) + SPACE(1) + CHAR(40) + CONVERT(VARCHAR(10),a.PessoaID) + CHAR(41) AS Pessoa '  +
	            'FROM Pessoas a WITH(NOLOCK) ' +
		        'WHERE a.PessoaID = ' + nPessoaID  + '';

    dsoPessoas.SQL = strSQL;
    dsoPessoas.ondatasetcomplete = fillPessoas_DSC;
    dsoPessoas.Refresh();

}

function fillPessoas_DSC() {

    var nTitulo = (dsoPessoas.recordset['Pessoa'].value == null ? '' : dsoPessoas.recordset['Pessoa'].value).toString();
    secText(nTitulo, 1);

}

function carregaCampos() {
    var nPessoaID = glb_nPessoaID;

    if ((nPessoaID == null) || (nPessoaID == ''))
        return null;

    setConnection(dsoDadosRH);

    strSQL = 'SELECT  a.PessoaID, CONVERT(VARCHAR(30),a.dtContratoFim,103) AS dtContratoFim, a.DiasContratoID, a.SindicatoID, a.ContribuicaoSindicalAnual, a.TipoAdmissaoID, a.HorarioTrabalho, a.CargaHoraria, ' +
                     'a.AdiantQuinzenal, a.AreaAtuacao, a.ApoliceSeguro, a.CursoEstagio, a.SemestreCurso, a.InstituicaoCursoID, a.AgenteIntegracaoID, ' +
                     'a.SupervisorID,    a.TipoNecessidadeID, a.CodigoCID ' +
	            'FROM Pessoas_DadosRH a WITH(NOLOCK) ' +
		        'WHERE a.PessoaID = ' + nPessoaID + '';

    dsoDadosRH.SQL = strSQL;
    dsoDadosRH.ondatasetcomplete = fillcarregaCampos_DSC;
    dsoDadosRH.Refresh();
}

function fillcarregaCampos_DSC() {

    if (!((dsoDadosRH.recordset.BOF) || (dsoDadosRH.recordset.EOF))) {
        txtDataFim.value = (dsoDadosRH.recordset['dtContratoFim'].value == null ? '' : dsoDadosRH.recordset['dtContratoFim'].value);
        selDiasContrato.value = (dsoDadosRH.recordset['DiasContratoID'].value == null ? '' : dsoDadosRH.recordset['DiasContratoID'].value);
        selSindicato.value = (dsoDadosRH.recordset['SindicatoID'].value == null ? '' : dsoDadosRH.recordset['SindicatoID'].value);
        chkContribuicaoSindicalAnual.value = (dsoDadosRH.recordset['ContribuicaoSindicalAnual'].value == null ? '' : dsoDadosRH.recordset['ContribuicaoSindicalAnual'].value);
        selTipoAdmissao.value = (dsoDadosRH.recordset['TipoAdmissaoID'].value == null ? '' : dsoDadosRH.recordset['TipoAdmissaoID'].value);
        txtHorariodeTrabalho.value = (dsoDadosRH.recordset['HorarioTrabalho'].value == null ? '' : dsoDadosRH.recordset['HorarioTrabalho'].value);
        txtCargaHoraria.value = (dsoDadosRH.recordset['CargaHoraria'].value == null ? '' : dsoDadosRH.recordset['CargaHoraria'].value);
        chkAdiantamentoQuinzenal.value = (dsoDadosRH.recordset['AdiantQuinzenal'].value == null ? '' : dsoDadosRH.recordset['AdiantQuinzenal'].value);
        txtAreadeAtuacao.value = (dsoDadosRH.recordset['AreaAtuacao'].value == null ? '' : dsoDadosRH.recordset['AreaAtuacao'].value);
        txtNumeroApoliceSeguro.value = (dsoDadosRH.recordset['ApoliceSeguro'].value == null ? '' : dsoDadosRH.recordset['ApoliceSeguro'].value);
        txtCurso.value = (dsoDadosRH.recordset['CursoEstagio'].value == null ? '' : dsoDadosRH.recordset['CursoEstagio'].value);
        selSemestre.value = (dsoDadosRH.recordset['SemestreCurso'].value == null ? '' : dsoDadosRH.recordset['SemestreCurso'].value);
        selInstituicaoEnsino.value = (dsoDadosRH.recordset['InstituicaoCursoID'].value == null ? '' : dsoDadosRH.recordset['InstituicaoCursoID'].value);
        selAgenteIntegracao.value = (dsoDadosRH.recordset['AgenteIntegracaoID'].value == null ? '' : dsoDadosRH.recordset['AgenteIntegracaoID'].value);
        selSupervisor.value = (dsoDadosRH.recordset['SupervisorID'].value == null ? '' : dsoDadosRH.recordset['SupervisorID'].value);
        selTipoNecessidade.value = (dsoDadosRH.recordset['TipoNecessidadeID'].value == null ? '' : dsoDadosRH.recordset['TipoNecessidadeID'].value);
        txtCodigoCID.value = (dsoDadosRH.recordset['CodigoCID'].value == null ? '' : dsoDadosRH.recordset['CodigoCID'].value);

        if (chkAdiantamentoQuinzenal.value == 1) {
            chkAdiantamentoQuinzenal.checked = true;
        }

        if (chkContribuicaoSindicalAnual.value == 1) {
            chkContribuicaoSindicalAnual.checked = true;
        }

    }
}

function gravaConfirmacaoDados() {

    var nPessoaID = glb_nPessoaID;

    lockControlsInModalWin(true);

    if ((nPessoaID == null) || (nPessoaID == ''))
        return null;
    else {
        var strPars = new String();
        var sDataFim = dateFormatToSearch(txtDataFim.value);
        var nDiasContrato = selDiasContrato.value;
        var nSindicato = selSindicato.value;
        var nContribuicaoSindicalAnual = null;
        var nTipoAdmissao = selTipoAdmissao.value;
        var sHorariodeTrabalho = txtHorariodeTrabalho.value;
        var sCargaHoraria = txtCargaHoraria.value;
        var nAdiantamentoQuinzenal = null;
        var sAreadeAtuacao = txtAreadeAtuacao.value;
        var sNumeroApoliceSeguro = txtNumeroApoliceSeguro.value;
        var sCurso = txtCurso.value;
        var nSemestre = selSemestre.value;
        var nInstituicaoEnsino = selInstituicaoEnsino.value;
        var nAgenteIntegracao = selAgenteIntegracao.value;
        var nSupervisor = selSupervisor.value;
        var nTipoNecessidade = selTipoNecessidade.value;
        var sCodigoCID = txtCodigoCID.value;


        if (chkContribuicaoSindicalAnual.checked) {
            nContribuicaoSindicalAnual = 1;
        }
        else {
            nContribuicaoSindicalAnual = 0;
        }

        if (chkAdiantamentoQuinzenal.checked) {
            nAdiantamentoQuinzenal = 1;
        }
        else {
            nAdiantamentoQuinzenal = 0;
        }

        strPars = '?sPessoaID=' + escape(nPessoaID);
        strPars += '&sDataFim=' + escape(sDataFim);
        strPars += '&nDiasContrato=' + escape(nDiasContrato);
        strPars += '&nSindicato=' + escape(nSindicato);
        strPars += '&bContribuicaoSindicalAnual=' + escape(nContribuicaoSindicalAnual);
        strPars += '&nTipoAdmissao=' + escape(nTipoAdmissao);
        strPars += '&sHorariodeTrabalho=' + escape(sHorariodeTrabalho);
        strPars += '&sCargaHoraria=' + escape(sCargaHoraria);
        strPars += '&bAdiantamentoQuinzenal=' + escape(nAdiantamentoQuinzenal);
        strPars += '&sAreadeAtuacao=' + escape(sAreadeAtuacao);
        strPars += '&sNumeroApoliceSeguro=' + escape(sNumeroApoliceSeguro);
        strPars += '&sCurso=' + escape(sCurso);
        strPars += '&nSemestre=' + escape(nSemestre);
        strPars += '&nInstituicaoEnsino=' + escape(nInstituicaoEnsino);
        strPars += '&nAgenteIntegracao=' + escape(nAgenteIntegracao);
        strPars += '&nSupervisor=' + escape(nSupervisor);
        strPars += '&nTipoNecessidade=' + escape(nTipoNecessidade);
        strPars += '&sCodigoCID=' + escape(sCodigoCID);

        dsoIncluirPessoa.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/atualizardadosrh.aspx' + strPars;
        dsoIncluirPessoa.ondatasetcomplete = gravaConfirmacaoDados_DSC;
        dsoIncluirPessoa.refresh();
        
    }
}

function gravaConfirmacaoDados_DSC() {

    if (!((dsoIncluirPessoa.recordset.BOF) || (dsoIncluirPessoa.recordset.BOF))) {
        if (dsoIncluirPessoa.recordset['Resultado'].value != 1) {
            if (window.top.overflyGen.Alert("Erro ao gravar dados.") == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert("Registro gravado com sucesso.") == 0)
                return null;
        }
    }
    lockControlsInModalWin(false);
}
