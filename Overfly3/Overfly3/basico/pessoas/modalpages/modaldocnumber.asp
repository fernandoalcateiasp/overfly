<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modaldocnumberHtml" name="modaldocnumberHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/pessoas/modalpages/modaldocnumber.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Dim i, sCaller, ndirC1, ndirC2, nEmpresaPaisID

sCaller = ""
ndirC1=0
ndirC2=0
nEmpresaPaisID=0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

'ndirC1 Direito do usu�rio
For i = 1 To Request.QueryString("ndirC1").Count    
    ndirC1 = Request.QueryString("ndirC1")(i)
Next

'ndirC2 Direito do usu�rio
For i = 1 To Request.QueryString("ndirC2").Count    
    ndirC2 = Request.QueryString("ndirC2")(i)
Next

'Pais ID data empresa logada
For i = 1 To Request.QueryString("nEmpresaPaisID").Count
    nEmpresaPaisID = Request.QueryString("nEmpresaPaisID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_ndirC1 = " &  ndirC1 & ";"
Response.Write vbcrlf

Response.Write "var glb_ndirC2 = " &  ndirC2 & ";"
Response.Write vbcrlf

'Necessario para compatibilidade da automacao
Response.Write "var glb_USERID = 0;"

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<body id="modaldocnumberBody" name="modaldocnumberBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divControls" name="divControls" class="divGeneral">	
        <p id="lblFisica" name="lblFisica" class="lblGeneral">Pessoa f�sica</p>    
        <input type="radio" id="rdFisica" name="rdFisica" class="btns" ItemID=0></input>        

        <p id="lblJuridica" name="lblJuridica" class="lblGeneral">Pessoa jur�dica</p>    
        <input type="radio" id="rdJuridica" name="rdJuridica" class="btns" ItemID=1></input>
        

        <p id="lblPaisResidenciaID" name="lblPaisResidenciaID" class="lblGeneral">Pa�s de resid�ncia</p>
        <select id="selPaisResidenciaID" name="selPaisResidenciaID" class="fldGeneral">
        <%
        Dim rsData, strSQL
        Set rsData = Server.CreateObject("ADODB.Recordset")
        
        strSQL = "SELECT a.LocalidadeID AS fldID, a.Localidade AS fldValue, b.DDDI AS fldOption " & _
            "FROM Localidades a WITH(NOLOCK) " & _
                "LEFT JOIN Localidades_DDDs b WITH(NOLOCK) ON (a.LocalidadeID = b.LocalidadeID) AND (b.Ordem = 1) " & _
            "WHERE a.EstadoID=2 AND a.TipoLocalidadeID=203 " & _
            "ORDER BY Localidade"
        
        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
                
        Do While Not rsData.EOF
            Response.Write( "<option value='" & rsData.Fields("fldID").Value & "' DDDI= '" & rsData.Fields("fldOption").Value & "' ")
    	    If (CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaPaisID)) Then
    		    Response.Write( " selected " )
            End If
    	    Response.Write( ">" & rsData.Fields("fldValue") & "</option>" )
            rsData.MoveNext
        Loop

        rsData.Close
        Set rsData = Nothing
        %>
        </select>
        
        <p id="lblNumDoc" name="lblNumDoc" class="lblGeneral">N�mero do documento</p>    
        <input type="text" id="txtNumDoc" name="txtNumDoc" class="fldGeneral"></input>
        
        <p id="lblNomeCompleto" name="lblNomeCompleto" class="lblGeneral">Raz�o social</p>    
        <input type="text" id="txtNomeCompleto" name="txtNomeCompleto" class="fldGeneral"></input>
        
        <p id="lblNomeFantasia" name="lblNomeFantasia" class="lblGeneral">Fantasia</p>    
        <input type="text" id="txtNomeFantasia" name="txtNomeFantasia" class="fldGeneral"></input>
        
        <p id="lblClassificacao" name="lblClassificacao" class="lblGeneral">Classifica��o</p>
        <select id="selClassificacao" name="selClassificacao" class="fldGeneral"></select>
        
        <p id="lblInvRelacao" name="lblInvRelacao" class="lblGeneral" LANGUAGE=javascript>Inv</p>
        <input type="checkbox" id="chkInvRelacao" name="chkInvRelacao" class="fldGeneral"></input>

        <p id="lblDDI" name="lblDDI" class="lblGeneral">DDI</p>    
        <input type="text" id="txtDDI" name="txtDDI" class="fldGeneral"></input>
        
        <p id="lblDDD" name="lblDDD" class="lblGeneral">DDD</p>    
        <input type="text" id="txtDDD" name="txtDDD" class="fldGeneral"></input>
        
        <p id="lblTelefone" name="lblTelefone" class="lblGeneral">Telefone</p>    
        <input type="text" id="txtTelefone" name="txtTelefone" class="fldGeneral"></input>
        
        <p id="lblEmail" name="lblEmail" class="lblGeneral">E-mail</p>    
        <input type="text" id="txtEmail" name="txtEmail" class="fldGeneral"></input>
        
        <p id="lblRelacionar" name="lblRelacionar" class="lblGeneral">Pesquisa</p>    
        <input type="text" id="txtRelacionar" name="txtRelacionar" class="fldGeneral"></input>
        
        <p id="lblRelacao" name="lblRelacao" class="lblGeneral">Relacionar com</p>
        <select id="selRelacao" name="selRelacao" class="fldGeneral"></select>

        <input type="button" id="btnValidar" name="btnValidar" value="Validar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
        <input type="button" id="btnPreencher" name="btnPreencher" value="P" title="Preenche o combo Relacionar com" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    </div>    
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
