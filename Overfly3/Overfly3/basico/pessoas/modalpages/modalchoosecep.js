/********************************************************************
modalchoosecep.js

Library javascript para o modalchoosecep.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalchoosecepBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // configuracao inicial do html
    setupPage();   
           
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1,false,0);
    
    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    
    fg.Redraw = 2;
    
    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Cidades');
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Cidade', 1);
        
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (28 + (2 * 10) ) * FONT_WIDTH;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(divFG.currentStyle.top, 10) -  ELEM_GAP - 5;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    fillGridCEP();
    
}

/********************************************************************
Preenche o grid de CEP
********************************************************************/
function fillGridCEP()
{
    var dsoCEP = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,
                  'dsoAddrData');
                      
    startGridInterface(fg);
    headerGrid(fg,['Cidade',
                   'UF',
                   'CEP Inicial',
                   'CEP Final',
                   'UFID',
                   'CidadeID'], [4,5]);

    fillGridMask(fg,dsoCEP,['Cidade',
                            'UF',
                            'CEPInicial',
                            'CEPFinal',
                            'UFID',
                            'CidadeID'],['','','','','','']);
    
    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    

    writeInStatusBar('child', 'cellMode', 'Cidades');
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    // 1. Usuario clicou o botao OK
    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , new Array(fg.TextMatrix(fg.Row, 5),
                      fg.TextMatrix(fg.Row, 0), fg.TextMatrix(fg.Row, 4),
                      fg.TextMatrix(fg.Row, 1)));
                      
    }    
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
}
