/********************************************************************
modalprint_pessoas.js

Library javascript para o modalprint.asp
Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_DsoToArrive = 0;

// dso usado para envia e-mails
var dsoEMail = new CDatatransport("dsoEMail");
// dso usado para montar combo de paises do relatorio de posicao de pessoa
var dsoPessoasPaises = new CDatatransport("dsoPessoasPaises");
// dsos usados para montar combos do relatorio de atendimento
var dsoEmpresas = new CDatatransport("dsoEmpresas");
var dsoVendedores = new CDatatransport("dsoVendedores");
var dsoPessoas = new CDatatransport("dsoPessoas");
var dsoClassificacoes = new CDatatransport("dsoClassificacoes");
var dsoAtendimento = new CDatatransport("dsoAtendimento");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()	
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    if ( selReports.disabled == false )
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    else if (ctl.id == btnPesquisar.id )
    {
	    // Pesquisar pessoas no relatorio de atendimento
	    pesquisaPessoas();	
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Relat�rios', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style)
    {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta combo de relatorios
    with (selReports.style)
    {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
	// ajusta o divPosicaoPessoa
    with (divPosicaoPessoa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    
    // ajusta o divAtendimento
    with (divAtendimento.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
    
    // O estado do botao btnOK
    btnOK_Status();
    
    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // linka os divs nos relatorios
    linkDivsAndReports();
    
    // ajusta o divRelatorioVendas
    adjustdivPosicaoPessoa();
    
    // ajusta o divAtendimento
    adjustdivAtendimento();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}
function adjustdivAtendimento()
{
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    var nB7A2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrRightValue(\'SUP\',\'B7A2\')');
    var dirA1= valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    
    glb_DsoToArrive = 4;
    
    setConnection(dsoEmpresas);

    dsoEmpresas.SQL = 'SELECT DISTINCT d.PessoaID AS fldID, d.Fantasia AS fldName ' +
				    'FROM	RelacoesPesRec a WITH(NOLOCK) ' +
					    'INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID ' +
					    'INNER JOIN Recursos_Direitos c WITH(NOLOCK) ON b.PerfilID = c.PerfilID ' +
					    'INNER JOIN Pessoas d WITH(NOLOCK) ON b.EmpresaID = d.PessoaID ' +
					    'INNER JOIN RelacoesPesRec e WITH(NOLOCK) ON b.EmpresaID = e.SujeitoID ' +  
				    'WHERE (a.SujeitoID =  ' + nUserID + ' AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND e.EstaEmOperacao=1 AND c.RecursoID = 40006 ' + 
					    'AND c.RecursoMaeID = 20100 AND c.ContextoID = 1211 AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND e.TipoRelacaoID = 12 ' + 
					    'AND e.ObjetoID = 999) ' +   
				    'ORDER BY fldName '; 

    dsoEmpresas.ondatasetcomplete = adjustdivAtendimento_DSC;
    dsoEmpresas.refresh();	
    
    setConnection(dsoVendedores);
    
	if(dirA1==1)
	{
	    dsoVendedores.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
                'UNION ALL ' +
                'SELECT DISTINCT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
                    'FROM Pessoas a WITH(NOLOCK) ' +
                        'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +                   
                    'WHERE b.PessoaID=' + nUserID + ' ' +   
                'UNION ' +                    
                'SELECT DISTINCT a.PessoaID AS fldID, a.Fantasia AS fldName ' +
                    'FROM Pessoas a WITH(NOLOCK) ' +
                        'INNER JOIN Pessoas_Atendimentos b WITH(NOLOCK) ON (a.PessoaID = b.UsuarioID) AND (a.EstadoID = 2) ' +
                'UNION ' +
                'SELECT DISTINCT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
                    'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                        'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +                    
                    'WHERE (a.TipoRelacaoID = 21) ' +   
                    'ORDER BY fldName';
     }
     else
     {   
        dsoVendedores.SQL = 'SELECT a.PessoaID AS fldID, a.Fantasia AS fldName ' +            
                        'FROM Pessoas a WITH(NOLOCK) ' +                        
                        'WHERE a.PessoaID=' + nUserID;
    }

    dsoVendedores.ondatasetcomplete = adjustdivAtendimento_DSC;
    dsoVendedores.refresh();	

    setConnection(dsoAtendimento);

    dsoAtendimento.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem ' +
                    'UNION ' +
                    'SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName, a.Ordem AS Ordem ' +
	                'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE ((a.TipoID = 51) AND (a.EstadoID = 2)) ' +
                    'ORDER BY Ordem '; 

    dsoAtendimento.ondatasetcomplete = adjustdivAtendimento_DSC;
    dsoAtendimento.refresh();	
                    
    setConnection(dsoClassificacoes);

    dsoClassificacoes.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem ' +
                             'UNION ALL ' +
                            'SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem ' +
                                'FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) ' + 
                                'WHERE TipoID = 29 ' +
                                'ORDER BY Ordem';

    dsoClassificacoes.ondatasetcomplete = adjustdivAtendimento_DSC;
    dsoClassificacoes.refresh();	                
}
function adjustdivAtendimento_DSC()
{
    glb_DsoToArrive--;
    
    if(glb_DsoToArrive>0)
        return;
    
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    
    clearComboEx(['selEmpresas']);
	
    while (! dsoEmpresas.recordset.EOF )
    {
        optionStr = dsoEmpresas.recordset['fldName'].value;
		optionValue = dsoEmpresas.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selEmpresas.add(oOption);
        dsoEmpresas.recordset.MoveNext();
    }
    
    clearComboEx(['selVendedor']);
	
    while (! dsoVendedores.recordset.EOF )
    {
        optionStr = dsoVendedores.recordset['fldName'].value;
		optionValue = dsoVendedores.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selVendedor.add(oOption);
        dsoVendedores.recordset.MoveNext();
    } 
    
    selOptByValueInSelect(getHtmlId(), selVendedor.id, nUserID);
    
    clearComboEx(['selAtendimento']);
	
    while (! dsoAtendimento.recordset.EOF )
    {
        optionStr = dsoAtendimento.recordset['fldName'].value;
		optionValue = dsoAtendimento.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selAtendimento.add(oOption);
        dsoAtendimento.recordset.MoveNext();
    }
    
    clearComboEx(['selClassificacaoResultante']);
	
    while (! dsoClassificacoes.recordset.EOF )
    {
        optionStr = dsoClassificacoes.recordset['fldName'].value;
		optionValue = dsoClassificacoes.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selClassificacaoResultante.add(oOption);
        dsoClassificacoes.recordset.MoveNext();
    }
    
    clearComboEx(['selClassificacaoInterna']);
	
	dsoClassificacoes.recordset.MoveFirst();
	
    while (! dsoClassificacoes.recordset.EOF )
    {
        optionStr = dsoClassificacoes.recordset['fldName'].value;
		optionValue = dsoClassificacoes.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selClassificacaoInterna.add(oOption);
        dsoClassificacoes.recordset.MoveNext();
    }
    
    clearComboEx(['selClassificacaoExterna']);
	
	dsoClassificacoes.recordset.MoveFirst();
	
    while (! dsoClassificacoes.recordset.EOF )
    {
        optionStr = dsoClassificacoes.recordset['fldName'].value;
		optionValue = dsoClassificacoes.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selClassificacaoExterna.add(oOption);
        dsoClassificacoes.recordset.MoveNext();
    }
        
    adjustElementsInForm([['lblEmpresas','selEmpresas',15,1,-10,-10],
                          ['lblDtInicio','txtDtInicio',9,1],
                          ['lblDtFim','txtDtFim',9,1],
                          ['lblVendedor','selVendedor',20,1],
                          ['lblAtendimento','selAtendimento',10,2,120,2],
                          ['lblClassificacaoResultante','selClassificacaoResultante',4,2],
                          ['lblClassificacaoInterna','selClassificacaoInterna',4,2],
                          ['lblClassificacaoExterna','selClassificacaoExterna',4,2],
                          ['lblDiasMinimo','txtDiasMinimo',6,2],
                          ['lblDiasMaximo','txtDiasMaximo',6,2],
                          ['lblPesquisa','txtPesquisa',14,3,-10],
                          ['btnPesquisar','btn',17,3],
                          ['lblPessoas','selPessoas',40,3,-5]],null,null,true);
                          
    selEmpresas.style.height = 70;
    selPessoas.disabled = true;
    
    selVendedor.onchange = adjustLabelsCombos;
    selPessoas.onchange = adjustLabelsCombos;
    
    txtDtInicio.onkeypress = verifyDateTimeNotLinked;        	
	txtDtInicio.setAttribute('verifyNumPaste', 1);
	txtDtInicio.setAttribute('thePrecision', 10, 1);
	txtDtInicio.setAttribute('theScale', 0, 1);
	txtDtInicio.setAttribute('minMax', new Array(0, 9999999999), 1);
	txtDtInicio.onfocus = selFieldContent;
    txtDtInicio.maxLength = 10;
    
    txtDtFim.onkeypress = verifyDateTimeNotLinked;        	
	txtDtFim.setAttribute('verifyNumPaste', 1);
	txtDtFim.setAttribute('thePrecision', 10, 1);
	txtDtFim.setAttribute('theScale', 0, 1);
	txtDtFim.setAttribute('minMax', new Array(0, 9999999999), 1);
	txtDtFim.onfocus = selFieldContent;
    txtDtFim.maxLength = 10;
    
    txtDiasMinimo.onkeypress = verifyNumericEnterNotLinked;        	
	txtDiasMinimo.setAttribute('verifyNumPaste', 1);
	txtDiasMinimo.setAttribute('thePrecision', 10, 1);
	txtDiasMinimo.setAttribute('theScale', 0, 1);
	txtDiasMinimo.setAttribute('minMax', new Array(0, 999), 1);
	txtDiasMinimo.onfocus = selFieldContent;
    txtDiasMinimo.maxLength = 3;
    
    txtDiasMaximo.onkeypress = verifyNumericEnterNotLinked;        	
	txtDiasMaximo.setAttribute('verifyNumPaste', 1);
	txtDiasMaximo.setAttribute('thePrecision', 10, 1);
	txtDiasMaximo.setAttribute('theScale', 0, 1);
	txtDiasMaximo.setAttribute('minMax', new Array(0, 999), 1);
	txtDiasMaximo.onfocus = selFieldContent;
    txtDiasMaximo.maxLength = 3;
    
	txtPesquisa.onfocus = selFieldContent;
    txtPesquisa.maxLength = 15;
    
    adjustLabelsCombos();
}

function adjustdivPosicaoPessoa()
{
	setConnection(dsoPessoasPaises);

	dsoPessoasPaises.SQL = 'SELECT 0 AS Indice, 0 as PaisID, SPACE(0) AS Pais ' + 
		'UNION ALL ' +
		'SELECT DISTINCT 1 AS Indice, Paises.LocalidadeID AS PaisID, Paises.Localidade AS Pais ' +
			'FROM Localidades Paises WITH(NOLOCK), Pessoas_Enderecos Enderecos WITH(NOLOCK) ' +
			'WHERE Paises.LocalidadeID = Enderecos.PaisID ' +
		'ORDER BY Indice, Pais';

    dsoPessoasPaises.ondatasetcomplete = adjustdivPosicaoPessoa_DSC;
    dsoPessoasPaises.refresh();	
}

function adjustdivPosicaoPessoa_DSC()
{
	clearComboEx(['selPessoaPaises']);
	
    while (! dsoPessoasPaises.recordset.EOF )
    {
        optionStr = dsoPessoasPaises.recordset['Pais'].value;
		optionValue = dsoPessoasPaises.recordset['PaisID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selPessoaPaises.add(oOption);
        dsoPessoasPaises.recordset.MoveNext();
    }

    adjustElementsInForm([['lblPessoaPaises','selPessoaPaises',20,1,-10,-10],
						  ['lblPessoaInicialID','txtPessoaInicialID',10,1],
						  ['lblPessoaFinalID','txtPessoaFinalID',10,1]],null,null,true);

	txtPessoaInicialID.onkeypress = verifyNumericEnterNotLinked;
	txtPessoaInicialID.setAttribute('verifyNumPaste', 1);
	txtPessoaInicialID.setAttribute('thePrecision', 10, 1);
	txtPessoaInicialID.setAttribute('theScale', 0, 1);
	txtPessoaInicialID.setAttribute('minMax', new Array(0, 9999999999), 1);
	txtPessoaInicialID.onfocus = selFieldContent;
    txtPessoaInicialID.maxLength = 10;

	txtPessoaFinalID.onkeypress = verifyNumericEnterNotLinked;
	txtPessoaFinalID.setAttribute('verifyNumPaste', 1);
	txtPessoaFinalID.setAttribute('thePrecision', 10, 1);
	txtPessoaFinalID.setAttribute('theScale', 0, 1);
	txtPessoaFinalID.setAttribute('minMax', new Array(0, 9999999999), 1);
	txtPessoaFinalID.onfocus = selFieldContent;
    txtPessoaFinalID.maxLength = 10;
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    if ( glb_arrayReports != null )
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
	divPosicaoPessoa.setAttribute('report', 40106 , 1);
	divAtendimento.setAttribute('report', 40107, 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if ( this.disabled == false )
        this.focus();
    
    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    var i, elem;
    
    if ( selReports.selectedIndex != -1 )
    {
        btnOKStatus = false;                
    }
    
    // 0 div visivel deve ser coerente com o relatorio selecionado
    if ( selReports.value != getCurrDivReportAttr() )
        btnOKStatus = true;
    
	// Posicao de Pessoas
    if (selReports.value == 40106)
        btnOKStatus = false;
        
    // Atendimento
    if (selReports.value == 40107)
        btnOKStatus = false;        

	// Homologa��o
    if (selReports.value == 40177)
        btnOKStatus = false;

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
	if ( (selReports.value == 40177) && (glb_sCaller == 'PL') )
        relatorioRAT();
	else if ( (selReports.value == 40106) && (glb_sCaller == 'PL') )
        relatorioPosicaoPessoas();
    else if ( (selReports.value == 40107) && (glb_sCaller == 'PL') )
        relatorioAtendimento();        
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
        
        
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';    
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
    if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}
function relatorioAtendimento()
{   
    var sEmpresas = '';
    var sNomeEmpresas = '';
    var sDtInicio = '';
    var sDtFim = '';
    var sVendedor = '';
    var sClienteID = '';
    var sTipoAtendimentoID = '';
    var sClassificacaoResultante = '';
    var sClassificacaoInterna = '';
    var sClassificacaoExterna = '';
    var sDiasMinimo = '';
    var sDiasMaximo = '';
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var formato = 2;
    // var formato = selFormato.value;
    
    for (i=0; i<selEmpresas.length; i++)
    {
        if ( selEmpresas.options[i].selected == true )
        { 
            sEmpresas += (sEmpresas == '' ? '\'' + selEmpresas.options[i].value : ',' + selEmpresas.options[i].value); //+ selEmpresas.options[i].value + '/';
            //sEmpresas += (sEmpresas == '' ? '/' : '') + selEmpresas.options[i].value + '/';

        } 
    }    
    
    sEmpresas = (sEmpresas == '' ? 'NULL' : '' + sEmpresas + '\'');

    if ( (!verificaData(txtDtInicio.value)) || (!verificaData(txtDtFim.value)) )
    	return null;

    sDtInicio = (txtDtInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDtInicio.value, true) + '\'');
    sDtFim = (txtDtFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDtFim.value, true) + '\'');
    
    sVendedor = (selVendedor.value <= 0 ? 'NULL' : selVendedor.value);
    
    sClienteID = (selPessoas.value == 0 ? 'NULL' : selPessoas.value);
    
    sTipoAtendimentoID = (selAtendimento.value == 0 ? 'NULL' : selAtendimento.value);
    
    sClassificacaoResultante = (selClassificacaoResultante.value == 0 ? 'NULL' : '\'' + selClassificacaoResultante.options.item(selClassificacaoResultante.selectedIndex).innerText + '\'');

    sClassificacaoInterna = (selClassificacaoInterna.value == 0 ? 'NULL' : '\'' + selClassificacaoInterna.options.item(selClassificacaoInterna.selectedIndex).innerText + '\'');

    sClassificacaoExterna = (selClassificacaoExterna.value == 0 ? 'NULL' : '\'' + selClassificacaoExterna.options.item(selClassificacaoExterna.selectedIndex).innerText  + '\'');

    sDiasMinimo = (txtDiasMinimo.value == '' ? 'NULL' : txtDiasMinimo.value);
    
    sDiasMaximo = (txtDiasMaximo.value == '' ? 'NULL' : txtDiasMaximo.value);

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&sEmpresas=" + sEmpresas + "&sDtInicio=" + sDtInicio + "&sDtFim=" + sDtFim + "&sVendedor=" + sVendedor + "&sTipoAtendimentoID=" + sTipoAtendimentoID +
                        "&sClienteID=" + sClienteID + "&sClassificacaoResultante=" + sClassificacaoResultante + "&sClassificacaoInterna=" + sClassificacaoInterna +
                        "&sClassificacaoExterna=" + sClassificacaoExterna + "&sDiasMinimo=" + sDiasMinimo + "&sDiasMaximo=" + sDiasMaximo;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/basico/pessoas/serverside/Reports_pessoas.aspx?' + strParameters;

}

function adjustLabelsCombos()
{
    setLabelOfControl(lblVendedor,selVendedor.value);
    setLabelOfControl(lblPessoas,selPessoas.value);    
}

function pesquisaPessoas()
{
    var sSql = '';
    var nRegistros = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    var sPesquisa = txtPesquisa.value;
    var sOperador = '';
    var sEmpresas = '';
    
    for (i=0; i<selEmpresas.length; i++)
    {
        if ( selEmpresas.options[i].selected == true )
        { 
            sEmpresas += (sEmpresas == '' ? ' AND a.ObjetoID IN (' : ',') + selEmpresas.options[i].value;
        } 
    }    
    
    sEmpresas = (sEmpresas == '' ? '' : sEmpresas + ') ');
    
    if ( (sPesquisa.substr(0,1) == '%') || (sPesquisa.substr(sPesquisa.length-1,1) == '%') )
		sOperador = 'LIKE';
	else
		sOperador = '>=';
    
    setConnection(dsoPessoas);

    sSql = 'SELECT DISTINCT TOP ' + nRegistros + ' b.PessoaID AS fldID, b.Nome AS fldName ' +
                        'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas b WITH(NOLOCK) ON a.SujeitoID = b.PessoaID ' +
                        'WHERE dbo.fn_Empresa_Sistema(a.SujeitoID) = 0 AND a.TipoRelacaoID = 21 AND a.EstadoID != 5 ' + sEmpresas +
                        'AND (b.Nome ' + sOperador  + ' \'' + sPesquisa + '\') ';                         
    
    if ((selVendedor.value >= 0) && (selVendedor.options(selVendedor.selectedIndex).innerText != ''))                        
        sSql += 'AND a.ProprietarioID = ' + selVendedor.value + ' ';
        
    sSql += 'ORDER BY fldName ';        
    
    dsoPessoas.SQL = sSql;
    dsoPessoas.ondatasetcomplete = pesquisaPessoas_DSC;
    dsoPessoas.refresh();	

}

function pesquisaPessoas_DSC() {
    lockControlsInModalWin(false);
    
    selPessoas.disabled = false;            
    
    clearComboEx(['selPessoas']);
    
    lockControlsInModalWin(true);
		
	if (!((dsoPessoas.recordset.BOF) || (dsoPessoas.recordset.EOF)))
    {
        while (! dsoPessoas.recordset.EOF )
        {
            if (selPessoas.length == 0)
            {
                oOption = document.createElement("OPTION");
                oOption.text = '';
                oOption.value = 0;
            }
            else
            {
                optionStr = dsoPessoas.recordset['fldName'].value;
		        optionValue = dsoPessoas.recordset['fldID'].value;
                oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;            
                dsoPessoas.recordset.MoveNext();
            }
            
            selPessoas.add(oOption);
        }
        if(selPessoas.length == 2)
            selPessoas.selectedIndex = 1;
    }
    
    adjustLabelsCombos();
    
    lockControlsInModalWin(false);
    
    if (selPessoas.length > 1)
            selPessoas.disabled = false;            
    else
        selPessoas.disabled = true;            
    
    if (!selPessoas.disabled)
        selPessoas.focus();
}

function relatorioPosicaoPessoas()
{
    var nPessoaInicialID = txtPessoaInicialID.value;
    var nPessoaFinalID = txtPessoaFinalID.value;
    var nPaisID = 0;
    var sInformation = '';
    
    if (selPessoaPaises.selectedIndex > 0)
		nPaisID = selPessoaPaises.value;
	else
		nPaisID = '0';

    if ((nPessoaInicialID == null) || (nPessoaInicialID == ''))
		nPessoaInicialID = '0';
	
	if ((nPessoaFinalID == null) || (nPessoaFinalID == ''))
		nPessoaFinalID = '0';

	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    // var formato = selFormato.value;
	var formato = 2;

	var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&nPaisID=" + nPaisID + "&nPessoaInicialID=" + nPessoaInicialID + "&nPessoaFinalID=" + nPessoaFinalID;

	pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
	lockControlsInModalWin(true);
	window.document.onreadystatechange = reports_onreadystatechange;
	window.document.location = SYS_PAGESURLROOT + '/basico/pessoas/serverside/Reports_pessoas.aspx?' + strParameters;
}

function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        lockControlsInModalWin(false);
		return false;
	}
	return true;
}
