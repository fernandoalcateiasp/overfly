/********************************************************************
modalatributos.js

Library javascript para o modalatributos.asp
Pessoas  
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:

window_onload()
__gridControlFill()
setupPage()
loadDataAndTreatInterface()
fg_DblClick()
btn_onclick(ctl)
btnOK_Clicked()
showModalAfterDataLoad()
startDynamicGrids()
startDynamicGrids_DSC()
cmbOnChange(cmb)
adjustLabelsCombos(bPaging)
adjustLabelsCombos2(bPaging)
startDynamicCmbs()
dsoCmbDynamic_DSC()
startDynamicCmbs2()
dsoCmbDynamic_DSC2()
Copiar()
Limpar()
js_fg_DblClick(grid, Row, Col)  
saveDataFromGrid()
sendDataToServer()
sendDataToServer_DSC()
js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol)
FimGridEntidades() 
paintCellsSpecialyReadOnly(fg)   
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_bCarregando = false;
var glb_sResultado = '';
var glb_FirstTime = true;
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();
var glb_CounterCmbsDynamics2 = 0;
var glb_CounterCmbsDynamics3 = 0;
//variaveis de eventos do grid
var glb_PassedOneInDblClick = false;
// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
//vari�veis para enviar dados ao servidor (excesso de registros no grid)
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_sCliForDe;
var gbl_btnListarClicked;

// Gravacao no Servidor .URL    
var dsoGrava = new CDatatransport("dsoGrava");
var dsoGridEntidades = new CDatatransport("dsoGridEntidades");
var dsoGridAtributos = new CDatatransport("dsoGridAtributos");
var dsoPapelEmpresa = new CDatatransport("dsoPapelEmpresa");
var dsoClassificacao = new CDatatransport("dsoClassificacao");
var dsoComboMultiplo = new CDatatransport("dsoComboMultiplo");
var dsoCountList = new CDatatransport("dsoCountList");
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    glb_aStaticCombos = null;

    // configuracao inicial do html
    setupPage();
    window.focus();
    // final de carregamento e visibilidade de janela
    // foi movido daqui
    // ajusta o body do html
    with (modalatributosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    (getFrameInHtmlTop(getExtFrameID(window))).style.top =
		parseInt((getFrameInHtmlTop('frameSup01')).offsetTop,10) - 53;
    
}

/********************************************************************
Objeto controlador do carregamento dos dsos de grids e
preenchimento destes dsos 

Instanciado pela variavel glb_gridControlFill
********************************************************************/
function __gridControlFill() {
    this.bFollowOrder = false;
    this.nDsosToArrive = 0;
    this.dso1_Ref = null;
    this.dso2_Ref = null;
    this.grid1_Ref = null;
    this.grid2_Ref = null;
}

var glb_gridControlFill = new __gridControlFill();

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao
    secText('Atributos', 1);
    
    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    //Carrega grids
    glb_gridControlFill.bFollowOrder = false;
    glb_gridControlFill.nDsosToArrive = 2;
    glb_gridControlFill.dso1_Ref = dsoGridEntidades;
    glb_gridControlFill.dso2_Ref = dsoGridAtributos;
    glb_gridControlFill.grid1_Ref = fgEntidades;
    glb_gridControlFill.grid2_Ref = fgAtributos;

    startDynamicCmbs();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    var elem;
    // ajusta elementos da janela
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    var btn_width2 = btn_width + 8;
    var btn_width3 = btn_width2 + 14;
    var btn_height = parseInt(btnOK.currentStyle.height, 10);
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    adjustElementsInForm([['lblTipoPessoa', 'selTipoPessoa', 14, 1, -5],
                          ['lblAtributo', 'selAtributo', 18, 1, 0],
                          ['lblChavePesquisa', 'selChavePesquisa', 10, 1, 0],
                          ['lblPesquisa', 'txtPesquisa', 30, 1, -3],
						  ['lblFiltro', 'txtFiltro', 34, 1, 0],
						  ['btnListar', 'btn', btn_width, 1, 7, -11],
						  ['lblClassificacao', 'selClassificacao', 20, 2, -5, 10],
						  ['lblRelacao', 'selRelacao', 17, 2, 0, -10],
						  ['lblPapelEmpresa', 'selPapelEmpresa', 17, 3, 165],
						  ['lblCliForDe', 'selCliForDe', 17, 2, 312],        
						  ['lblAtributo2', 'selAtributo2', 21, 3, 344],
						  ['btnCopiar', 'btn', btn_width2, 3, 10],
						  ['btnGravar', 'btn', btn_width, 3, 10],
						  ['btnLimpar', 'btn', btn_width3, 3, 10]], null, null, true);
    
    txtPesquisa.maxLength = 50;
    txtFiltro.maxLength = 80;

    btnListar.style.height = btn_height;
    btnCopiar.style.height = btn_height;
    btnLimpar.style.height = btn_height;
    btnGravar.style.height = btn_height;
    btnOK.style.visibility = 'hidden';
    btnOK.disabled = true;
    btnCanc.style.visibility = 'hidden';
    btnCanc.disabled = true;
    lblCliForDe.innerText = 'De: ';
    
    startGridInterface(fgEntidades);
    startGridInterface(fgAtributos); 
    fgEntidades.Rows = 0;
    fgEntidades.Cols = 0;
    fgAtributos.Rows = 0;
    fgAtributos.Cols = 0; 
    // inicio da altura livre
    topFree = parseInt(divLinha1.currentStyle.top, 10) + parseInt(divLinha1.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;

    // largura livre
    widthFree = modWidth;

    // ajusta o divLinha1
    elem = window.document.getElementById('divLinha1');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP -1;
        top = topFree + ELEM_GAP;
        width = widthFree - 2 * ELEM_GAP - 170;
        height = heightFree - 2 * ELEM_GAP;
    }
    
    elem = window.document.getElementById('divLinha2');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP -1;
        top = parseInt(divLinha1.currentStyle.top, 10) + ELEM_GAP;
        width = widthFree - 2 * ELEM_GAP - 170;
        height = (heightFree - parseInt(divLinha1.currentStyle.height, 10)) - 2 * ELEM_GAP;
    }

    elem = window.document.getElementById('divLinha3');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP + 173 -1;
        top = parseInt(divLinha2.currentStyle.top, 10) + ELEM_GAP + 40;
        width = widthFree - 2 * ELEM_GAP - 170;
        height = (heightFree - parseInt(divLinha2.currentStyle.height, 10)) - 2 * ELEM_GAP;
    }

    elem = selClassificacao;
    with (elem.style) {
        height = 125;
    }

    elem = selCliForDe;
    with (elem.style) {
        height = 125;
    }

    elem = window.document.getElementById('divGridEntidades');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        width = modWidth - (3 * ELEM_GAP / 2) -170;
        height = (modHeight / 4);
        left = ELEM_GAP -1;
        top = parseInt(divLinha3.currentStyle.top, 10) + ELEM_GAP + 90;
    }

    elem = window.document.getElementById('fgEntidades');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        width = modWidth - (7 * ELEM_GAP / 2) + 3;
        height = (modHeight / 4)  + 50;
        left = ELEM_GAP - 5 -1;
        top = parseInt(divLinha3.currentStyle.top, 10) + ELEM_GAP - 55;
    }

    elem = window.document.getElementById('fgAtributos');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        width = fgEntidades.currentStyle.width;
        height = (modHeight / 6) + 25;
        left = parseInt(fgEntidades.currentStyle.left, 10) + 8;
        top = parseInt(fgEntidades.currentStyle.top, 10) + ELEM_GAP + 345 + 50;
    }

    // Por default o botao OK vem travado
    btnOK.disabled = true;
}

function txtPesquisa_ondigit(ctl) {
    if (event.keyCode == 13)
        btn_onclick(btnListar);
}

function fg_DblClick() {
    // chama o botao OK 
    if (fg.Row >= 1)
        btn_onclick(btnOK);
}

/********************************************************************
Clique de qualquer bot�o
********************************************************************/
function btn_onclick(ctl) {
    window.focus();

    if ((ctl.id == btnOK.id) && (btnOK.disabled == false))
        btnOK.focus();
    else if ((ctl.id == btnCanc.id) && (btnCanc.disabled == false))
        btnCanc.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
    else if (ctl.id == btnListar.id) {
        if (validaCamposObrigatorios()) {
           
           gbl_btnListarClicked = true;
           
            //Carrega grids
            glb_gridControlFill.bFollowOrder = false;
            glb_gridControlFill.nDsosToArrive = 1;
            glb_gridControlFill.dso1_Ref = dsoGridEntidades;
            glb_gridControlFill.grid1_Ref = fgEntidades;
            glb_gridControlFill.dso2_Ref = null; //dsoGridAtributos;
            glb_gridControlFill.grid2_Ref = null; //fgAtributos;

            startDynamicGrids();
        }
        else
            lockControlsInModalWin(false);
    }
    else if (ctl.id == btnCopiar.id) {
        Copiar();
    }
    else if (ctl.id == btnLimpar.id) {
        var bOk = false;
        for (i = 1; i < fgEntidades.Rows; i++) {
            if (getCellValueByColKey(fgEntidades, 'OK', i) != 0) {
                bOk = true;
            }
        }

        if (!bOk) {
            var sResultado = 'Selecione uma pessoa...';
            window.top.overflyGen.Alert(sResultado);

            lockControlsInModalWin(false);
            fgEntidades.Editable = true;
            fgEntidades.Redraw = 2;

            return null;
        }

        var select_list_field = document.getElementById('selAtributo2');
        var select_list_selected_index = select_list_field.selectedIndex;
        var text = select_list_field.options[select_list_selected_index].text;

        fgEntidades.AutoSizeMode = 0;
        fgEntidades.AutoSize(0, fgEntidades.Cols - 1);

        lockControlsInModalWin(false);
        fgEntidades.Editable = true;
        fgEntidades.Redraw = 2;

        if (text == "") {
            sResultado = 'O Campo Atributo n�o foi selecionado...';
            window.top.overflyGen.Alert(sResultado);
            return null;
        }
        else {
            var sError = "Deseja remover o atributo \"" + text + "\" das linhas selecionadas?";
            var _retMsg = window.top.overflyGen.Confirm(sError);

            if (_retMsg == 1) {
                Limpar();
                saveDataFromGrid(2);
                //return null;
            }
            else if ((_retMsg == 0) || (_retMsg == 2)) {
                return null;
            }
        }
    }
    else if (ctl.id == btnGravar.id) {
        saveDataFromGrid(1);
    }
}

/********************************************************************
Usuario clicou o botao OK (compatibilidade com a automa��o)
********************************************************************/
function btnOK_Clicked() {
    ;
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad() {
    // ajusta o body do html
    with (modalatributosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Vai ao servidor buscar dados dos grids
********************************************************************/
function startDynamicGrids() {
    /*if (!glb_FirstTime)
        lockControlsInModalWin(true);*/

    if (glb_sResultado != '') {
        if (window.top.overflyGen.Alert(glb_sResultado) == 0)
            return null;

        glb_sResultado = '';
    }

    var sFiltro = '';
    var sData = '';
    var nValor = '';
    var sEmpresas = '', sClassificacoes = '';
    var primeiro = true;

    glb_bDissociar = null;

    if (glb_gridControlFill.dso1_Ref != null) {
        glb_bCarregando = true;

        var strCampo = selClassificacao.value, i = 0;

        while (i < selClassificacao.length) {
            if ((selClassificacao.options[i].selected) && (strCampo != null) && (strCampo != '0') && (strCampo != ''))
                sClassificacoes += (sClassificacoes == '' ? ' AND a.ClassificacaoID IN (' : ',') + selClassificacao.options[i].value;

            i++;

            if (i == selClassificacao.length && sClassificacoes != '')
                sFiltro += sClassificacoes + ') ';
        }

        strCampo = selTipoPessoa.value;

        if ((strCampo != null) && (strCampo != '') && (strCampo != '0'))
            sFiltro += ' AND a.TipoPessoaID = ' + strCampo + ' ';

        strCampo = selAtributo.value;

        if ((strCampo != null) && (strCampo != '') && (strCampo != '0'))
            sFiltro += ' AND c.TipoAtributoID = ' + strCampo + ' ';
        
        strCampo = txtPesquisa.value;

        if ((strCampo != null) && (strCampo != '') && (selChavePesquisa.value >= 1))

            if (selChavePesquisa.value == 1)
                sFiltro += ' AND a.PessoaID = ' + txtPesquisa.value + ' ';

            else if (selChavePesquisa.value == 2)
                sFiltro += ' AND a.Nome LIKE \'%' + txtPesquisa.value + '%\' ';

            else if (selChavePesquisa.value == 3)
                sFiltro += ' AND a.Fantasia LIKE \'%' + txtPesquisa.value + '%\' ';

            else if (selChavePesquisa.value == 4)
                sFiltro += ' AND dbo.fn_Pessoa_Documento(a.PessoaID, 130, NULL, 101, 111, 0) LIKE \'%' + txtPesquisa.value + '%\' ';

        strCampo = txtFiltro.value;

        if ((strCampo != null) && (strCampo != ''))
            sFiltro += ' AND ' + strCampo + ' ';

        strCampo = selRelacao.value;

        if ((strCampo != null) && (strCampo != '') && (strCampo != '0'))
            sFiltro += ' AND b.TipoRelacaoID = ' + strCampo + ' ';

        strCampo = selPapelEmpresa.value; //1 - Sujeito
                                          //2 - Objeto

        // Empresa
        for (i = 0; i < selCliForDe.length; i++)
            if (selCliForDe.options[i].selected)
                sEmpresas += (sEmpresas == '' ? '' : ',') + selCliForDe.options[i].value;

        var sJoin = '', strSQL = '', sOrderBy = '';

        strSQL = 'SELECT ' + ((gbl_btnListarClicked) ? (' COUNT(1) AS Resultado ') : ('DISTINCT a.PessoaID AS ID, a.Nome AS Nome, a.Fantasia AS Fantasia, ' + 
                    'dbo.fn_Pessoa_Documento(a.PessoaID, 130, NULL, 101, 111, 0) AS DocFederal, b_a.TipoRelacao AS Relacao, STUFF(( ' +
                        'SELECT \'/\'+ b2.Fantasia ' +
                        'FROM RelacoesPessoas b1 WITH(NOLOCK) ' +
                        'INNER JOIN Pessoas b2 WITH(NOLOCK) ON b1.ObjetoID = b2.PessoaID ' +
                        'WHERE b1.SujeitoID = a.PessoaID AND b1.TipoRelacaoID = b.TipoRelacaoID AND b1.EstadoID = 2 AND b2.EstadoID = 2 ' +
                        'FOR XML PATH(\'b\'), TYPE).value(\'.\',\'nvarchar(max)\'),1,1,\'\') AS Empresas, ' +
                    'COUNT(c.RegistroID) AS Atributos, \'\' AS Atributo, 0 AS OK, 0 AS AtributoID ')) +
                    'FROM Pessoas a WITH(NOLOCK) ' +
                    'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON a.PessoaID = ' + (strCampo == '2' ? 'SujeitoID' : 'ObjetoID') + ' ' +
                    'INNER JOIN TiposRelacoes b_a WITH(NOLOCK) ON b.TipoRelacaoID = b_a.TipoRelacaoID ' +
                    'LEFT JOIN Atributos c WITH(NOLOCK) ON c.RegistroID = a.PessoaID AND c.FormID = 1210 AND SubFormID = 20100 ' +
                    'WHERE a.EstadoID = 2 AND b.EstadoID = 2 ' + sFiltro + ' AND ' + (strCampo == '2' ? 'b.ObjetoID' : 'b.SujeitoID') + ' IN(' + sEmpresas + ') ' +
                    ((gbl_btnListarClicked) ? ('') : ('GROUP BY a.PessoaID, a.Nome, a.Fantasia, b_a.TipoRelacao, ' + (strCampo == '2' ? 'ObjetoID' : 'SujeitoID') + ', b.TipoRelacaoID '));

        sOrderBy = ' ORDER BY a.PessoaID ';

        if (gbl_btnListarClicked)
        {
            countList(strSQL);
        }
        else
        {
            dso = glb_gridControlFill.dso1_Ref;

            setConnection(dsoGridEntidades);

            dsoGridEntidades.SQL = strSQL;

            dsoGridEntidades.ondatasetcomplete = startDynamicGrids_DSC;

            try {
                dsoGridEntidades.Refresh();
            }
            catch (e) {
                if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
                    return null;

                lockControlsInModalWin(false);
                window.focus();
            }
        }
    }

    if (glb_gridControlFill.dso2_Ref != null) {
        if (fgEntidades.Rows > 0) {

            var sRegistroID = getCellValueByColKey(fgEntidades, 'ID*', fgEntidades.Row);

            dso = glb_gridControlFill.dso2_Ref;

            setConnection(dsoGridAtributos);

            if (getCellValueByColKey(fgEntidades, 'Atributos*', fgEntidades.Row) > 0) {
                dsoGridAtributos.SQL = 'SELECT AtributoID AS ID, b.ItemMasculino AS Atributo, a.Valor AS Valor, CONVERT(VARCHAR(20),a.Data,103) AS Data, ' +
                                ' c.Fantasia AS Usuario, a.Observacao AS Observacao ' +
                                    ' FROM Atributos a WITH(NOLOCK) ' +
                                    ' INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.TipoAtributoID = b.ItemID ' +
                                    ' INNER JOIN Pessoas c WITH(NOLOCK) ON a.UsuarioID = c.PessoaID ' +
                                    ' WHERE a.FormID = 1210 AND a.SubFormID = 20100 AND a.RegistroID =' + sRegistroID + ' ';
            }
            else {
                dsoGridAtributos.SQL = 'SELECT NULL AS ID, NULL AS Atributo, NULL AS Valor, NULL AS Data, ' +
                                ' NULL AS Usuario, NULL AS Observacao ';
            }

            dsoGridAtributos.ondatasetcomplete = startDynamicGrids_DSC;
            dsoGridAtributos.Refresh();
        } 
    }
}

/********************************************************************
Preenche os grids e mostra a janela
********************************************************************/
function startDynamicGrids_DSC() {
    glb_gridControlFill.nDsosToArrive--;

    if (glb_gridControlFill.nDsosToArrive > 0)
        return null;

    var i;
    var dso, grid;
    var dTFormat = '';
    var nTipoID, sColor;
    var bPreencheuExtrato = false;

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    if (glb_gridControlFill.dso1_Ref != null) {
        dso = glb_gridControlFill.dso1_Ref;
        grid = glb_gridControlFill.grid1_Ref;

        if (!(dsoGridEntidades.recordset.BOF && dsoGridEntidades.recordset.EOF)) {
            // move o cursor do dso para o primeiro registro
            dsoGridEntidades.recordset.MoveFirst();
        }


        grid.ExplorerBar = 5;
        //startGridInterface(grid, 1, null);
        startGridInterface(grid);
        grid.FontSize = '8';
        grid.Editable = false;
        grid.BorderStyle = 1;

        headerGrid(grid, ['ID',
		               'Nome',
		               'Fantasia',
		               'Doc Federal',
		               'Rela��o',
		               'Atributos',
		               'Atributo',
		               'OK',
		               'Empresas',
		               'AtributoID'],[9]);


        fillGridMask(grid, dsoGridEntidades, ['ID*',
		                       'Nome*',
		                       'Fantasia*',
		                       'DocFederal*',
		                       'Relacao*',
                               'Atributos*',
		                       'Atributo*',
		                       'OK',
		                       'Empresas*',
		                       'AtributoID'],
		                       ['', '', '', '', '', '', '', '', '',''],
		                       ['', '', '', '', '', '', '', '', '','']);

        gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

        var nPessoas = 0;
        var i;

        for (i = 2; i < grid.Rows; i++) {
            if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'ID*')) != 0) {
                nPessoas += 1;
            }
        }

        if (nPessoas > 0)
            grid.TextMatrix(1, getColIndexByColKey(grid, 'ID*')) = nPessoas;
        
        // Coloca tipo de dado nas colunas
        with (grid) {
            ColDataType(0) = 3;
            ColDataType(1) = 8;
            ColDataType(2) = 8;
            ColDataType(3) = 8;
            ColDataType(4) = 8;
            ColDataType(5) = 3;
            ColDataType(6) = 8;
            ColDataType(7) = 11;
            ColDataType(8) = 8;
            ColDataType(9) = 3;
        }

        if (!(dsoGridEntidades.recordset.BOF && dsoGridEntidades.recordset.EOF)) {
            dsoGridEntidades.recordset.Filter = '';

            // move o cursor do dso para o primeiro registro
            dsoGridEntidades.recordset.MoveFirst();
        }

        alignColsInGrid(grid, []);
        grid.AutoSizeMode = 0;
        grid.AutoSize(0, grid.Cols - 1);
        grid.FrozenCols = 4;
        grid.MergeCells = 4;
        grid.MergeCol(0) = true;
        grid.MergeCol(1) = true;
        grid.MergeCol(2) = true;
        grid.MergeCol(3) = true;

        paintCellsSpecialyReadOnly(grid);
        
        grid.ExplorerBar = 5;

        if (grid.Rows > 1)
            grid.Row = 1;

        if ((grid.Rows > 0) && grid.Row > 0) {
            grid.TopRow = grid.Row;
        }
        grid.Redraw = 2;
        grid.Editable = true;

        if (!(dsoGridEntidades.recordset.BOF && dsoGridEntidades.recordset.EOF)) {
            FimGridEntidades();
        }
        glb_bCarregando = false;
    }

    if (glb_gridControlFill.dso2_Ref != null) {
        if (dsoGridAtributos.recordset.aRecords.length > 0) {

            dso = glb_gridControlFill.dso2_Ref;
            grid = glb_gridControlFill.grid2_Ref;

            if (!(dsoGridAtributos.recordset.BOF && dsoGridAtributos.recordset.EOF))
            // move o cursor do dso para o primeiro registro
                dsoGridAtributos.recordset.MoveFirst();

            grid.ExplorerBar = 0;
            startGridInterface(grid, 1, null);
            grid.FontSize = '8';
            grid.Editable = false;
            grid.BorderStyle = 1;

            headerGrid(grid, ['Atributo',
		                 'Valor',
		                 'Data',
		                 'Usu�rio',
		                 'Observa��o'], []);

            fillGridMask(grid, dsoGridAtributos, ['Atributo',
		                         'Valor',
		                         'Data',
		                         'Usuario',
		                         'Observacao'],
		                       ['', '', '', '', ''],
		                       ['', '', dTFormat, '', '']);

            if (!(dsoGridAtributos.recordset.BOF && dsoGridAtributos.recordset.EOF)) {
                // move o cursor do dso para o primeiro registro
                dsoGridAtributos.recordset.MoveFirst();
            }


            // Coloca tipo de dado nas colunas
            with (grid) {
                ColDataType(0) = 8;
                ColDataType(1) = 8;
                ColDataType(2) = 7;
                ColDataType(3) = 8;
                ColDataType(4) = 8;
            }

            if (!(dsoGridAtributos.recordset.BOF && dsoGridAtributos.recordset.EOF)) {
                // move o cursor do dso para o primeiro registro
                dsoGridAtributos.recordset.MoveFirst();
            }

            alignColsInGrid(grid, []);
            grid.AutoSizeMode = 0;
            grid.AutoSize(0, grid.Cols - 1);
            grid.FrozenCols = 4;
            grid.MergeCells = 4;
            grid.MergeCol(0) = true;
            grid.MergeCol(1) = true;

            grid.ExplorerBar = 5;

            if (grid.Rows > 1)
                grid.Row = 1;

            if ((grid.Rows > 0) && grid.Row > 0) {
                grid.TopRow = grid.Row;
            }

            grid.Editable = false;
        }
    }
   
    lockControlsInModalWin(false); 

}

/********************************************************************
Troca de valores em um combo
********************************************************************/
function cmbOnChange(cmb) {
    if ((cmb.id).toUpperCase() == 'SELRELACAO') {
        startDynamicCmbs();
        adjustLabelsCombos();       
    } else if ((cmb.id).toUpperCase() == 'SELTIPOPESSOA') {
        startDynamicCmbs2();
        adjustLabelsCombos();
    } else if ((cmb.id).toUpperCase() == 'SELCLASSIFICACAO') {
        adjustLabelsCombos();
    } else if ((cmb.id).toUpperCase() == 'SELPAPELEMPRESA') {
        adjustLabelsCombos();
    } else if ((cmb.id).toUpperCase() == 'SELATRIBUTO') {
        adjustLabelsCombos();
    } else if ((cmb.id).toUpperCase() == 'SELATRIBUTO2') {
        adjustLabelsCombos();
    }
}

/********************************************************************
Ajuste de labels em combos
********************************************************************/
function adjustLabelsCombos(bPaging) {
    setLabelOfControl(lblRelacao, selRelacao.value);
    setLabelOfControl(lblTipoPessoa, selTipoPessoa.value);
    setLabelOfControl(lblClassificacao, selClassificacao.value);
    setLabelOfControl(lblAtributo, selAtributo.value);
    setLabelOfControl(lblAtributo2, selAtributo2.value);

    var select_list_field = document.getElementById('selTipoPessoa');
    var select_list_selected_index = select_list_field.selectedIndex;
    if (select_list_selected_index == 0)
        selClassificacao.disabled = true;
    else
        selClassificacao.disabled = false;

    adjustLabelsCombos2();
    
}

/********************************************************************
Ajuste de labels em combo (especial - combo multiplo)
********************************************************************/
function adjustLabelsCombos2(bPaging) {
    var select_list_field = document.getElementById('selPapelEmpresa');
    var select_list_selected_index = select_list_field.selectedIndex;

    var text_selected = select_list_field.options[select_list_selected_index].text;
    var text;

    if (text_selected == '')
        text = '';
    else if (text_selected != glb_sCliForDe[0])
        text = glb_sCliForDe[0];
    else
        text = glb_sCliForDe[1];

    lblCliForDe.innerText = text + ' De: ';


//    if (select_list_selected_index == 1)
//        indice = 2;
//    else if (select_list_selected_index == 2)
//        indice = 1;
//    else
//        indice = 0;

//    var text = select_list_field.options[indice].text;
//    var text = '';
//    lblCliForDe.innerText = text + ' De: ';

//    var select_list_field = document.getElementById('selRelacao');
//    var select_list_selected_index = select_list_field.selectedIndex;

//    if (select_list_selected_index == 0)
//        selPapelEmpresa.disabled = true;
//    else
//        selPapelEmpresa.disabled = false;


}

/********************************************************************
Vai ao servidor buscar dados dos combos
********************************************************************/
function startDynamicCmbs() {
    var id = selRelacao.value;
    var sFiltro = '';

    if ((id != null) && (id != ""))
       sFiltro = 'WHERE a.TipoRelacaoID = ' + id;
    else if (id == "")
        sFiltro = 'WHERE a.TipoRelacaoID = 0';

    glb_CounterCmbsDynamics = 1;

    setConnection(dsoPapelEmpresa);

    dsoPapelEmpresa.SQL = 'SELECT DISTINCT 0 as fldIndex, 0 as fldID, SPACE(0) as fldName, 0 as fldTest ' +
                          'UNION ALL ' +
                          'SELECT DISTINCT 1 as fldIndex, 1 as fldID, a.Sujeito as fldName, b.TipoSujeitoID as fldTest ' +
	                      'FROM TiposRelacoes a WITH(NOLOCK) ' +
	                      'INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON (b.TipoRelacaoID = a.TipoRelacaoID) ' +
	                      sFiltro + ' ' +
	                      'UNION ALL ' +
                          'SELECT DISTINCT 2 as fldIndex, 2 as fldID, a.Objeto as fldName, b.TipoObjetoID as fldTest ' +
	                      'FROM TiposRelacoes a WITH(NOLOCK) ' +
	                      'INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON (b.TipoRelacaoID = a.TipoRelacaoID) ' +
	                      sFiltro + ' ' +
						  'ORDER BY fldIndex,fldID';

    dsoPapelEmpresa.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoPapelEmpresa.Refresh();
}

/********************************************************************
Preenchimento dos combos
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue, optionTest;
    var aCmbsDynamics = [selPapelEmpresa];
    var aDSOsDynamics = [dsoPapelEmpresa];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i,j=0;
    var lFirstRecord;
    var nQtdCmbs = 1;
    glb_sCliForDe = new Array();

    // Inicia o carregamento de combos dinamicos (selPapelEmpresa)
    clearComboEx(['selPapelEmpresa']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < nQtdCmbs; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;
            while (!aDSOsDynamics[i].recordset.EOF) {
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                optionTest = aDSOsDynamics[i].recordset['fldTest'].value;

                if (optionTest == 52 || optionTest == 0) { //Pessoa Jur�dica (rela��o com pessoa empresa do grupo, que � sempre jur�dica)
                    var oOption = document.createElement("OPTION");
                    oOption.text = optionStr;
                    oOption.value = optionValue;
                    aCmbsDynamics[i].add(oOption);
                    
                    // Fornecedor Defaut
                    if (optionStr == "Fornecedor")
                        aCmbsDynamics[i].selectedIndex = oOption.index;
                }
                else {
                    glb_sCliForDe[j] = optionStr;
                    j++;
                }

                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        if (!glb_FirstTime)
            lockControlsInModalWin(false);
    }

    startDynamicCmbs2();
    adjustLabelsCombos();
    return null;
}

/********************************************************************
Ida ao servidor para o segundo combo
********************************************************************/
function startDynamicCmbs2() {
    var sFiltro = '';
    glb_CounterCmbsDynamics2 = 1;
    if (selTipoPessoa.value != null)
        sFiltro = 'AND Filtro LIKE \'%(' + selTipoPessoa.value + ')%\' ';

    setConnection(dsoClassificacao);

    dsoClassificacao.SQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName ' +
                            'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                            'WHERE TipoID = 13 AND EstadoID = 2 ' + sFiltro +
                            'ORDER BY fldID';

    dsoClassificacao.ondatasetcomplete = dsoCmbDynamic_DSC2;
    dsoClassificacao.Refresh();
}

/********************************************************************
Retorno do servidor
********************************************************************/
function dsoCmbDynamic_DSC2() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selClassificacao];
    var aDSOsDynamics = [dsoClassificacao];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 1;

    // Inicia o carregamento de combos dinamicos (selPapelEmpresa,selClassificacao)
    clearComboEx(['selClassificacao']);

    glb_CounterCmbsDynamics2--;

    if (glb_CounterCmbsDynamics2 == 0) {
        for (i = 0; i < nQtdCmbs; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;
            while (!aDSOsDynamics[i].recordset.EOF) {
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        if (!glb_FirstTime)
            lockControlsInModalWin(false);
    }

    adjustLabelsCombos();
    return null;
}

/********************************************************************
Fun��o copiar (btnCopiar)
********************************************************************/
function Copiar() {
    var bOk = false;
    var select_list_field = document.getElementById('selAtributo2');
    var select_list_selected_index = select_list_field.selectedIndex;
    var text = select_list_field.options[select_list_selected_index].text;
    var value = select_list_field.options[select_list_selected_index].value; 
    
    fgEntidades.Editable = false;
    fgEntidades.Redraw = 0;
    lockControlsInModalWin(true);

    for (i = 2; i < fgEntidades.Rows; i++) {
        if (getCellValueByColKey(fgEntidades, 'OK', i) != 0) {
            bOk = true;
        }
    }

    if (!bOk) {
        var sResultado = 'Selecione uma pessoa...';
        window.top.overflyGen.Alert(sResultado);

        lockControlsInModalWin(false);
        fgEntidades.Editable = true;
        fgEntidades.Redraw = 2;

        return null;
    }

    if (text != "") {
        for (i = 1; i < fgEntidades.Rows; i++) {
            if (getCellValueByColKey(fgEntidades, 'OK', i) != 0) {
                fgEntidades.TextMatrix(i, getColIndexByColKey(fgEntidades, 'Atributo*')) = text;
                fgEntidades.TextMatrix(i, getColIndexByColKey(fgEntidades, 'AtributoID')) = value;
            }
        }
    } 
    else {
        Limpar();
    }
    
    
    fgEntidades.AutoSizeMode = 0;
    fgEntidades.AutoSize(0, fgEntidades.Cols - 1);

    lockControlsInModalWin(false);
    fgEntidades.Editable = true;
    fgEntidades.Redraw = 2;
}

/********************************************************************
Fun��o limpar (btnLimpar)
********************************************************************/
function Limpar() {
    fgEntidades.Editable = false;
    fgEntidades.Redraw = 0;
    lockControlsInModalWin(true);

    for (i = 1; i < fgEntidades.Rows; i++) {
        if (getCellValueByColKey(fgEntidades, 'OK', i) != 0) {
            fgEntidades.TextMatrix(i, getColIndexByColKey(fgEntidades, 'Atributo*')) = "";
            fgEntidades.TextMatrix(i, getColIndexByColKey(fgEntidades, 'AtributoID')) = 0;
        }
    }
    
    fgEntidades.AutoSizeMode = 0;
    fgEntidades.AutoSize(0, fgEntidades.Cols - 1);
    lockControlsInModalWin(false);
    fgEntidades.Editable = true;
    fgEntidades.Redraw = 2;
}


/********************************************************************
Evento de grid particular desta pagina (Duplo Clique)   
********************************************************************/
function js_fg_DblClick(grid, Row, Col) {
    /*if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }*/

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (getColIndexByColKey(grid, 'OK') != Col)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
 
    //glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();
}


/********************************************************************
Salvar os dados do grid (marcados com OK) no banco (btnGravar)
********************************************************************/
function saveDataFromGrid(ntipoAlteracao) {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_sResultado = '';

    lockControlsInModalWin(true);

    for (i = 2; i < fgEntidades.Rows; i++) {
        if ((getCellValueByColKey(fgEntidades, 'OK', i) != 0) 
            && ((getCellValueByColKey(fgEntidades, 'AtributoID', i) != 0 && ntipoAlteracao == 1) || (ntipoAlteracao == 2))){

            nBytesAcum = strPars.length;

            if ((nBytesAcum <= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }
                strPars += '?nInicio=' + escape(1);
                strPars += '&nUsuarioID=' + escape(glb_nUserID);
                strPars += '&nFormID=' + escape(1210); //Form Pessoas
                strPars += '&nSubFormID=' + escape(20100); //SFS Grupo-Pessoa
                strPars += '&nRegistroID=' + escape(getCellValueByColKey(fgEntidades, 'ID*', i)); //PessoaID
                (ntipoAlteracao == 1 ? (strPars += '&nTipoAtributoID=' + escape(getCellValueByColKey(fgEntidades, 'AtributoID', i))) : (strPars += '&nTipoAtributoID=' + document.getElementById('selAtributo2')[document.getElementById('selAtributo2').selectedIndex].value));
                strPars += '&nTipoAlteracao=' + escape(ntipoAlteracao);
            }

            nDataLen++;
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    sendDataToServer();
}


/********************************************************************
Envio de dados ao servidor para gravar (btnGravar)
********************************************************************/
function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/gravarAtributo.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(true);
            glb_gridControlFill.bFollowOrder = false;
            glb_gridControlFill.nDsosToArrive = 1;
            glb_gridControlFill.dso1_Ref = dsoGridEntidades;
            glb_gridControlFill.grid1_Ref = fgEntidades;
            glb_gridControlFill.dso2_Ref = null;
            glb_gridControlFill.grid2_Ref = null;
            startDynamicGrids();
        }
    }
    catch (e) {
        lockControlsInModalWin(true);
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 1;
        glb_gridControlFill.dso1_Ref = dsoGridEntidades;
        glb_gridControlFill.grid1_Ref = fgEntidades;
        glb_gridControlFill.dso2_Ref = null;
        glb_gridControlFill.grid2_Ref = null;
        startDynamicGrids();

        lockControlsInModalWin(false);
        
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;
    }
}

/********************************************************************
Retorno do servidor (btnGravar)
********************************************************************/
function sendDataToServer_DSC() {
    var nPessoaID;

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        if (dsoGrava.recordset['Resultado'].value != null) {
            nPessoaID = dsoGrava.recordset['PessoaID'].value;
            glb_sResultado = glb_sResultado + 'Registro ' + nPessoaID + ' ' + dsoGrava.recordset['Resultado'].value + (glb_sResultado = '' ? '' : '\n');
         }   
        }
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()',    INTERVAL_BATCH_SAVE, 'JavaScript');  
    
}

/********************************************************************
Evento de grid particular desta pagina
Troca de linha do grid Entidades
********************************************************************/
function js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    if (NewCol != getColIndexByColKey(fg, 'OK') && (!glb_bCarregando))
        fgEntidades.Col = getColIndexByColKey(fg, 'OK');   
   
    if ((fgEntidades.Rows > 0) && (btnListar.disabled == false) && (OldRow != NewRow)) { 
            lockControlsInModalWin(true);
            glb_gridControlFill.bFollowOrder = false;
            glb_gridControlFill.nDsosToArrive = 1;
            glb_gridControlFill.dso1_Ref = null;
            glb_gridControlFill.grid1_Ref = null;
            glb_gridControlFill.dso2_Ref = dsoGridAtributos;
            glb_gridControlFill.grid2_Ref = fgAtributos;
            startDynamicGrids();

            lockControlsInModalWin(false);
        }
}

/********************************************************************
Evento de grid particular desta pagina.

Parametros:
grid
OldRow
OldCol
NewRow
NewCol
********************************************************************/
function js_fg_AfterRowColmodalatributos(grid, OldRow, OldCol, NewRow, NewCol) {
    var firstLine = 1;

    //if (glb_totalCols__ != false)
        firstLine = 2;

    if (NewRow >= firstLine)
        grid.Row = NewRow;
    else if ((NewRow < firstLine) && (grid.Rows > firstLine))
        grid.Row = firstLine;
}

/******************************************************************************************
Fun��o para carregar o Grid Atributos quando o Grid Entidades acabar de carregar
******************************************************************************************/
function FimGridEntidades() {
    if (fgEntidades.Rows > 0 && dsoGridAtributos != null) {
        lockControlsInModalWin(true);
        glb_gridControlFill.bFollowOrder = false;
        glb_gridControlFill.nDsosToArrive = 1;
        glb_gridControlFill.dso1_Ref = null;
        glb_gridControlFill.grid1_Ref = null;
        glb_gridControlFill.dso2_Ref = dsoGridAtributos;
        glb_gridControlFill.grid2_Ref = fgAtributos;
        startDynamicGrids();

        lockControlsInModalWin(false);
    }
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly(fg) {
    paintReadOnlyCols(fg);
}

/********************************************************************
Valida campos obrigadorios
********************************************************************/
function validaCamposObrigatorios() {
    var bEmpresaSelected = false;

    // Verifica se possui empresa selecionada
    for (i = 0; i < selCliForDe.length && !bEmpresaSelected; i++) {
        bEmpresaSelected = selCliForDe.options[i].selected;
    }

    if (!bEmpresaSelected) {
        sResultado = 'Selecione a empresa relacionada para busca...';
        window.top.overflyGen.Alert(sResultado);
        return null;
    }
    else if ((selTipoPessoa.value == null) || (selTipoPessoa.value == '0')) {
        sResultado = 'O Campo Tipo Pessoa n�o foi selecionado...';
        window.top.overflyGen.Alert(sResultado);
        return null;
    }
    else if ((selRelacao.value == null) || (selRelacao.value == '0')) {
        sResultado = 'O Campo Rela��o n�o foi selecionado...';
        window.top.overflyGen.Alert(sResultado);
        return null;
    }
    else if ((selPapelEmpresa.value == null) || (selPapelEmpresa.value == '0')) {
        sResultado = 'O Campo Papel Empresa n�o foi selecionado...';
        window.top.overflyGen.Alert(sResultado);
        return null;
    }

    return true;
}

/********************************************************************
Faz contagem inicial da quantidade
********************************************************************/
function countList(strSQL) {
    var dsoCount;
    
    setConnection(dsoCountList);
    dsoCountList.SQL = strSQL;
    dsoCountList.ondatasetcomplete = dsoCountList_DSC;

    try {
        dsoCountList.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}
function dsoCountList_DSC() {
    if (!(dsoCountList.recordset.BOF && dsoCountList.recordset.EOF)) {
        // Se o usuario filtrar para listagem acima de 1000 registros exibe alert de lentid�o
        if (dsoCountList.recordset['Resultado'].value > 1000) {
            var _retMsg = window.top.overflyGen.Confirm('A listagem excede 1000 registros, deseja listar mesmo assim?');

            if (_retMsg == 1) {
                gbl_btnListarClicked = false;
                startDynamicGrids();
                return null;
            }
            else if ((_retMsg == 0) || (_retMsg == 2)) {
                lockControlsInModalWin(false);
                return null;
            }
        }
        else {
            gbl_btnListarClicked = false;
            startDynamicGrids();
        }
    }

    lockControlsInModalWin(false);
}