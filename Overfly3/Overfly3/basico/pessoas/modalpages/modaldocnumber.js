/********************************************************************
modaldocnumber_relpessoas.js

Library javascript para o modaldocnumber.asp
Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_NumDocOK = false;

// Dso generico para banco de dados 
var dsoGen01 = new CDatatransport("dsoGen01");
// Dso generico para banco de dados 
var dsoGen02 = new CDatatransport("dsoGen02");
// Dso generico para banco de dados 
var dsoClassificacao = new CDatatransport("dsoClassificacao");
// Dso generico para banco de dados 
var dsoRelacao = new CDatatransport("dsoRelacao");
// Dso generico para banco de dados 
var dsoIncluirPessoa = new CDatatransport("dsoIncluirPessoa");
var dsoGravaConsulta = new CDatatransport("dsoGravaConsulta");
var dsoGravaConsultaDetalhes = new CDatatransport("dsoGravaConsultaDetalhes");
var glb_aEmpresaData = getCurrEmpresaData();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    
EVENTOS DOS OBJETOS DE IMPRESSAO:

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    with (modaldocnumberBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Documento');

    window.focus();

    if (txtNumDoc.disabled == false)
        txtNumDoc.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();

    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id) {
        if (btnCanc.style.visibility != 'hidden')
            btnCanc.focus();
    }

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    if (ctl.id == btnValidar.id) {
        // verifica se o numDoc que pode ser um CPF ou CGC esta correto
        // e se nao exite no cadastro de pessoas

        verifyDireitoFamiliar();

        btnOK_Status();
    }
    else if (ctl.id == btnPreencher.id)
        fillcmbRelacao();

    else if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Solicita��o de cadastro de pessoas', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // O estado do botao btnOK
    btnOK_Status();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // ajusta elementos da janela
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    adjustElementsInForm([['lblFisica', 'rdFisica', 3, 1, 95, -20],
                                          ['lblJuridica', 'rdJuridica', 3, 1, 55],
                                          ['lblPaisResidenciaID', 'selPaisResidenciaID', 30, 2, 100],
                                          ['lblNumDoc', 'txtNumDoc', 20, 3, 100],
                                          ['btnValidar', 'btn', btn_width, 3, 5],
                                          ['lblNomeCompleto', 'txtNomeCompleto', 57, 4, -10, 10],
                                          ['lblNomeFantasia', 'txtNomeFantasia', 25, 5, -10],
                                          ['lblClassificacao', 'selClassificacao', 20, 5, -1],
                                          ['lblInvRelacao', 'chkInvRelacao', 3, 5, -1],
                                          ['lblDDI', 'txtDDI', 4, 6, -10],
                                          ['lblDDD', 'txtDDD', 4, 6, -1],
                                          ['lblTelefone', 'txtTelefone', 9, 6, -1],
                                          ['lblEmail', 'txtEmail', 37, 6, -3],
                                          ['lblRelacionar', 'txtRelacionar', 8, 7, -10],
                                          ['btnPreencher', 'btn', btn_width - 51, 7, 4],
                                          ['lblRelacao', 'selRelacao', 45, 7, -3],
                                          ['btnOk', 'btn', btn_width - 10, 8, 162, 10],
                                          ['btnCanc', 'btn', btn_width - 10, 8, 6]], null, null, true);

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = rdFisica.offsetLeft + rdFisica.offsetWidth + ELEM_GAP;
        height = 2.5 * (rdFisica.offsetTop + rdFisica.offsetHeight);
    }

    with (lblFisica.style) {
        left = parseInt(rdFisica.currentStyle.left, 10) + parseInt(rdFisica.currentStyle.width, 10);
        top = parseInt(rdFisica.currentStyle.top, 10) + 5;
    }
    with (lblJuridica.style) {
        left = parseInt(rdJuridica.currentStyle.left, 10) + parseInt(rdJuridica.currentStyle.width, 10);
        top = parseInt(rdJuridica.currentStyle.top, 10) + 5;
    }

    // Por default o botao OK vem travado
    btnOK.disabled = true;

    // click nos radios e seus labels
    rdFisica.onclick = clickRdLblButton;
    rdJuridica.onclick = clickRdLblButton;
    lblFisica.onclick = clickRdLblButton;
    lblJuridica.onclick = clickRdLblButton;

    // txtNumDoc max length
    txtNumDoc.onkeyup = keyUpTxtField;
    txtNumDoc.onkeydown = keyDownTxtNumDoc;
    txtNumDoc.onfocus = selFieldContent;
    txtNumDoc.maxLength = 20;
    txtNumDoc.setAttribute('verifyNumPaste', 1);

    txtNomeCompleto.onfocus = selFieldContent;
    txtNomeCompleto.maxLength = 40;
    txtNomeCompleto.onkeyup = keyUpTxtField;

    txtNomeFantasia.onfocus = selFieldContent;
    txtNomeFantasia.maxLength = 20;
    txtNomeFantasia.onkeyup = keyUpTxtField;

    txtDDI.onkeypress = verifyNumericEnterNotLinked;
    txtDDI.onkeyup = keyUpTxtField;
    txtDDI.setAttribute('verifyNumPaste', 1);
    txtDDI.setAttribute('thePrecision', 10, 1);
    txtDDI.setAttribute('theScale', 0, 1);
    txtDDI.setAttribute('minMax', new Array(0, 9999), 1);
    txtDDI.onfocus = selFieldContent;
    txtDDI.maxLength = 4;
    txtDDI.value = selPaisResidenciaID.options.item(selPaisResidenciaID.selectedIndex).getAttribute('DDDI', 1);

    txtDDD.onkeypress = verifyNumericEnterNotLinked;
    txtDDD.onkeyup = keyUpTxtField;
    txtDDD.setAttribute('verifyNumPaste', 1);
    txtDDD.setAttribute('thePrecision', 10, 1);
    txtDDD.setAttribute('theScale', 0, 1);
    txtDDD.setAttribute('minMax', new Array(0, 9999), 1);
    txtDDD.onfocus = selFieldContent;
    txtDDD.maxLength = 4;

    txtTelefone.onkeypress = verifyNumericEnterNotLinked;
    txtTelefone.onkeyup = keyUpTxtField;
    txtTelefone.ondblclick = txtTelefone_ondblclick;
    txtTelefone.setAttribute('verifyNumPaste', 1);
    txtTelefone.setAttribute('thePrecision', 10, 1);
    txtTelefone.setAttribute('theScale', 0, 1);
    txtTelefone.setAttribute('minMax', new Array(0, 999999999), 1);
    txtTelefone.onfocus = selFieldContent;
    txtTelefone.maxLength = 9;

    txtEmail.onfocus = selFieldContent;
    txtEmail.maxLength = 80;
    txtEmail.onkeyup = keyUpTxtField;
    txtEmail.ondblclick = txtEmail_ondblclick;
    txtEmail.value = '_@_.com.br';

    txtRelacionar.maxLength = 30;
    txtRelacionar.onkeydown = txtRelacionar_onkeydown;
    txtRelacionar.onfocus = selFieldContent;

    selPaisResidenciaID.onchange = selPaisResidenciaID_onchange;
    selClassificacao.onchange = selClassificacao_onchange;
    selRelacao.onchange = selRelacao_onchange;

    // rdJuridica entra checado
    rdJuridica.checked = true;

    selRelacao.disabled = true;

    fillCmbClassificacao();
    adjustLabelsCombos();
    showTextsAfterValidated();
}

function txtEmail_ondblclick() {
    if ((trimStr(txtEmail.value) != '') &&
             (trimStr(txtEmail.value) != '_@_.com.br') &&
              (trimStr(txtEmail.value) != '@_.com.br') &&
              (trimStr(txtEmail.value) != '_@.com.br') &&
             (trimStr(txtEmail.value) != '@.com.br'))
        window.open('mailto:' + txtEmail.value);
}

function txtTelefone_ondblclick() {
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');

    if ((txtDDD.value == '') || (txtTelefone.length < 8))
        return true;

    lockInterface(true);

    dialByModalPage(txtDDD.value, txtTelefone.value, nUserID);

    lockInterface(false);
}

function dialByModalPage(sDDD, sNumero, nPessoaID) {
    sDDD = sDDD.toString();
    sNumero = sNumero.toString();
    nPessoaID = parseInt(nPessoaID, 10);

    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

function selPaisResidenciaID_onchange() {
    glb_NumDocOK = false;
    showTextsAfterValidated();

    adjustLabelsCombos();
}
function selClassificacao_onchange() {
    clearComboEx(['selRelacao']);

    selRelacao.disabled = true;

    adjustLabelsCombos();

    btnOK_Status();

    if (selClassificacao.value == "58") {
        lblInvRelacao.style.visibility = 'inherit';
        chkInvRelacao.style.visibility = 'inherit';
    }
    else {
        lblInvRelacao.style.visibility = 'hidden';
        chkInvRelacao.style.visibility = 'hidden';
    }
}
function selRelacao_onchange() {
    adjustLabelsCombos();

    btnOK_Status();
}

function fillCmbClassificacao() {
    lockControlsInModalWin(true);

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    var sTipoPessoa = (rdJuridica.checked ? '52' : '51');

    setConnection(dsoClassificacao);
    dsoClassificacao.SQL = 'SELECT a.ItemMasculino AS Classificacao, a.ItemID AS ClassificacaoID, (CASE WHEN a.Filtro LIKE \'%<OBR>%\' THEN 1 ELSE 0 END) AS ExigeRelacao ' +
                                'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                                'WHERE a.EstadoID = 2 AND a.TipoID = 13 AND a.Filtro LIKE \'%(' + sTipoPessoa + ')%\' ' +
                                    'AND (dbo.fn_Direitos_TiposAuxiliares(a.ItemID, ' + aEmpresaData[0] + ', ' + nUserID + ', GETDATE()) > 0) ' +
                                    'AND a.ItemID <> 56 ' +
                                'ORDER BY a.Ordem ';

    dsoClassificacao.ondatasetcomplete = fillCmbClassificacao_DSC;
    dsoClassificacao.Refresh();
}

function adjustLabelsCombos() {
    setLabelOfControl(lblPaisResidenciaID, selPaisResidenciaID.value);
    setLabelOfControl(lblClassificacao, selClassificacao.value);
    setLabelOfControl(lblRelacao, selRelacao.value);

}

function fillCmbClassificacao_DSC() {
    clearComboEx(['selClassificacao']);

    if (!((dsoClassificacao.recordset.BOF) || (dsoClassificacao.recordset.EOF))) {
        while (!dsoClassificacao.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoClassificacao.recordset['Classificacao'].value;
            oOption.value = dsoClassificacao.recordset['ClassificacaoID'].value;
            oOption.setAttribute('ExigeRelacao', dsoClassificacao.recordset['ExigeRelacao'].value, 1);

            selClassificacao.add(oOption);
            dsoClassificacao.recordset.MoveNext();
        }
    }

    selClassificacao.selectedIndex = -1;
    adjustLabelsCombos();

    lockControlsInModalWin(false);
}
function fillcmbRelacao() {
    selRelacao.disabled = false;

    lockControlsInModalWin(true);

    var nRegistros = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    var sTipoPessoa = (rdJuridica.checked ? '52' : '51');
    var sClassificacao = (selClassificacao.selectedIndex >= 0 ? selClassificacao.value : '\'\'');
    var sPesquisa = txtRelacionar.value;
    var sOperador = '';

    if ((sPesquisa.substr(0, 1) == '%') || (sPesquisa.substr(sPesquisa.length - 1, 1) == '%'))
        sOperador = 'LIKE';
    else
        sOperador = '>=';

    setConnection(dsoRelacao);

    dsoRelacao.SQL = 'SELECT DISTINCT TOP ' + nRegistros + ' b.PessoaID, (b.Nome + \' (\' + CONVERT(VARCHAR(8),b.PessoaID) + \')\') AS Nome ' +
            'FROM TiposRelacoes_Classificacoes a WITH(NOLOCK) ' +
                         'INNER JOIN Pessoas b WITH(NOLOCK) ON ( (a.ClassificacaoSujeitoID = b.ClassificacaoID AND EhObjeto = 1) ' +
                                                     'OR (a.ClassificacaoObjetoID = b.ClassificacaoID AND EhObjeto = 0) ) ' +
                         'INNER JOIN TiposRelacoes_Itens c WITH(NOLOCK) ON a.TipoRelacaoID = c.TipoRelacaoID ' +
            'WHERE ((a.Excecao = 0)  ' +
                    'AND (((a.ClassificacaoSujeitoID = ' + sClassificacao + ') AND (a.EhObjeto = 0))  ' +
                            'OR ((a.ClassificacaoObjetoID = ' + sClassificacao + ') AND (a.EhObjeto = 1)))) ' +
                       'AND ( (a.EhObjeto = 0 AND c.TipoSujeitoID = ' + sTipoPessoa + ' AND c.TipoObjetoID = b.TipoPessoaID) ' +
                                'OR (a.EhObjeto = 1 AND c.TipoObjetoID = ' + sTipoPessoa + ' AND c.TipoSujeitoID = b.TipoPessoaID) ) ' +
                       'AND (b.EstadoID = 2) ' +
                       'AND (b.Nome ' + sOperador + ' \'' + sPesquisa + '\') ' +
            'ORDER BY Nome ';

    dsoRelacao.ondatasetcomplete = fillcmbRelacao_DSC;
    dsoRelacao.Refresh();
}
function fillcmbRelacao_DSC() {
    var oOption;

    clearComboEx(['selRelacao']);

    if (!((dsoRelacao.recordset.BOF) || (dsoRelacao.recordset.EOF))) {
        while (!dsoRelacao.recordset.EOF) {
            if (selRelacao.length == 0) {
                oOption = document.createElement("OPTION");
                oOption.text = '';
                oOption.value = 0;
            }
            else {
                oOption = document.createElement("OPTION");
                oOption.text = dsoRelacao.recordset['Nome'].value;
                oOption.value = dsoRelacao.recordset['PessoaID'].value;

                dsoRelacao.recordset.MoveNext();
            }

            selRelacao.add(oOption);
        }

        if (selRelacao.length == 2)
            selRelacao.selectedIndex = 1;
    }

    adjustLabelsCombos();
    lockControlsInModalWin(false);

    if (selRelacao.length > 1)
        selRelacao.disabled = false;
    else
        selRelacao.disabled = true;

    if (!selRelacao.disabled)
        selRelacao.focus();
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver documento digitado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = false;
    var nEmpresaPaisID = glb_aEmpresaData[1];

    if (txtNumDoc.value.length == 0)
        btnOKStatus = true;

    else if (txtNomeCompleto.value.length == 0)
        btnOKStatus = true;

    else if (txtNomeFantasia.value.length == 0)
        btnOKStatus = true;

    else if (selClassificacao.selectedIndex < 0)
        btnOKStatus = true;

    else if (txtDDI.value.length == 0)
        btnOKStatus = true;

    else if (txtDDD.value.length == 0)
        btnOKStatus = true;

    else if ((txtTelefone.value.length < 8 && nEmpresaPaisID == 130) || (txtTelefone.value.length == 0 && nEmpresaPaisID != 130))
        btnOKStatus = true;

    else if ((txtEmail.value.length == 0) ||
                (txtEmail.value == '_@_.com.br') ||
                (txtEmail.value == '@_.com.br') ||
                (txtEmail.value == '_@.com.br') ||
                (txtEmail.value == '@.com.br'))
        btnOKStatus = true;

    else if (!glb_NumDocOK)
        btnOKStatus = true;

    else if ((selClassificacao.options.item(selClassificacao.selectedIndex).getAttribute('ExigeRelacao', 1) == 1) && (selRelacao.selectedIndex < 1))
        btnOKStatus = true;

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {

    if (selClassificacao.value != "58") {
        chkInvRelacao.checked = false;
    }

    incluirPessoa();
}

/*******************************************************************
Tenta incluir pessoa pela procedure sp_Pessoa_Cadastro2
*******************************************************************/
function incluirPessoa() {
    var strPars = new String();
    var nTipoPessoaID = (rdFisica.checked ? 51 : 52);
    var sNome = trimStr(txtNomeCompleto.value);
    var sFantasia = trimStr(txtNomeFantasia.value);
    var nClasID = (selClassificacao.value == 0 ? '' : selClassificacao.value);
    var sDocumentoFederal = txtNumDoc.value;
    var nPaisID = (selPaisResidenciaID.value == 0 ? '' : selPaisResidenciaID.value);
    var sDDI = txtDDI.value;
    var sDDD = txtDDD.value;
    var sTel = txtTelefone.value;
    var sEmail = trimStr(txtEmail.value);
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');;
    var nRelacionadoID = (selRelacao.value == 0 ? '' : selRelacao.value);
    var nTipoCadastro = (chkInvRelacao.checked ? 5 : 2);

    strPars = '?sNome=' + escape(sNome);
    strPars += '&nTipoPessoaID=' + escape(nTipoPessoaID);
    strPars += '&sFantasia=' + escape(sFantasia);
    strPars += '&nClasID=' + escape(nClasID);
    strPars += '&sDocumentoFederal=' + escape(sDocumentoFederal);
    strPars += '&nPaisID=' + escape(nPaisID);
    strPars += '&sDDI=' + escape(sDDI);
    strPars += '&sDDD=' + escape(sDDD);
    strPars += '&sTel=' + escape(sTel);
    strPars += '&sEmail=' + escape(sEmail);
    strPars += '&nUserID=' + escape(nUserID);
    strPars += '&nRelacionadoID=' + escape(nRelacionadoID);
    strPars += '&nTipoCadastro=' + escape(nTipoCadastro);

    setConnection(dsoIncluirPessoa);

    dsoIncluirPessoa.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/incluirpessoa.aspx' + strPars;
    dsoIncluirPessoa.ondatasetcomplete = incluirPessoa_DSC;
    dsoIncluirPessoa.refresh();
}
function incluirPessoa_DSC()
{
    var nPessoaID = dsoIncluirPessoa.recordset['PessoaID'].value;
    var sErros = dsoIncluirPessoa.recordset['Erros'].value;

    if (sErros != null) {
        if (window.top.overflyGen.Alert(sErros.toString()) == 0)
            return null;        
    }
    else {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, nPessoaID);
    }


    lockControlsInModalWin(false);
}

/********************************************************************
Usuario clicou em radio ou seu label
********************************************************************/
function clickRdLblButton() {
    if ((this == rdFisica) || (this == lblFisica)) {
        if (rdJuridica.checked) {
            glb_NumDocOK = false;
            showTextsAfterValidated();
        }

        rdFisica.checked = true;
        rdJuridica.checked = false;
        lblNomeCompleto.innerText = 'Nome completo';
    }
    else if ((this == rdJuridica) || (this == lblJuridica)) {
        if (rdFisica.checked) {
            glb_NumDocOK = false;
            showTextsAfterValidated();
        }

        rdFisica.checked = false;
        rdJuridica.checked = true;
        lblNomeCompleto.innerText = 'Raz�o social';
    }

    fillCmbClassificacao();
    txtNumDoc.focus();
}

/********************************************************************
Usuario alterou o texto do txtDocNum
********************************************************************/
function keyUpTxtField() {
    if (txtEmail.value == '')
        txtEmail.value = '_@_.com.br';

    if (this == txtEmail) {
        if ((trimStr(txtEmail.value) != '') &&
                      (trimStr(txtEmail.value) != '_@_.com.br') &&
                      (trimStr(txtEmail.value) != '@_.com.br') &&
                      (trimStr(txtEmail.value) != '_@.com.br') &&
                      (trimStr(txtEmail.value) != '@.com.br')) {
            txtEmail.style.cursor = 'hand';
            txtEmail.style.color = 'blue';
        }
        else {
            txtEmail.style.cursor = 'default';
            txtEmail.style.color = 'black';
        }
    }
    else if ((this == txtTelefone) || (this == txtDDD)) {
        if ((txtDDD.value.length > 0) && (txtTelefone.value.length >= 8)) {
            txtTelefone.style.cursor = 'hand';
            txtTelefone.style.color = 'blue';
        }
        else {
            txtTelefone.style.cursor = 'default';
            txtTelefone.style.color = 'black';
        }
    }

    btnOK_Status();
}

/********************************************************************
Usuario apertou a tecla Enter no campo txtRelacionar
********************************************************************/
function txtRelacionar_onkeydown() {
    if (event.keyCode == 13)
        fillcmbRelacao();
}

/********************************************************************
Usuario apertou a tecla Enter no campo txtNumDoc
********************************************************************/
function keyDownTxtNumDoc() {
    glb_NumDocOK = false;

    showTextsAfterValidated();

    if (event.keyCode == 13)
        verifyDireitoFamiliar();
}

/********************************************************************
Verifica se o usuario tem direito de cadastrar pessoa do tipo familiar

Parametros:
nenhum

Retorno:
true ou false
********************************************************************/
function verifyDireitoFamiliar() {
    var nUserID = getCurrUserID();
    var aEmpresa = getCurrEmpresaData();

    setConnection(dsoGen02);

    dsoGen02.SQL = 'SELECT COUNT(RelPesRec.RelacaoID) AS Direito ' +
                        'FROM RelacoesPesRec RelPesRec WITH(NOLOCK) ' +
                                     'INNER JOIN RelacoesPesRec_Perfis RelPesRecPerfil WITH(NOLOCK) ON RelPEsRec.RelacaoID = RelPesRecPerfil.RelacaoID ' +
                                     'INNER JOIN TiposAuxiliares_Itens Itens WITH(NOLOCK) ON Itens.Filtro LIKE \'%/\' + CONVERT(VARCHAR(16),RelPesRecPerfil.PerfilID) + \'/%\' ' +
                        'WHERE RelPesRec.SujeitoID = ' + nUserID + ' ' +
                                     'AND RelPesRec.ObjetoID = 999 ' +
                                     'AND RelPesRec.TipoRelacaoID = 11 ' +
                                     'AND RelPesRecPerfil.EmpresaID = ' + aEmpresa[0] + ' ' +
                                     'AND Itens.TipoID = 13 ' +
                                     'AND Itens.Filtro LIKE \'%<FAM>%\'';

    dsoGen02.ondatasetcomplete = verifyDocNumber;
    dsoGen02.Refresh();
}

/********************************************************************
Valida o CGC/CPF digitado no banco de dados

Parametros:
nenhum

Retorno:
true ou false
********************************************************************/
function verifyDocNumber() {
    var sNumero = '';
    var nTipoDocumentoID = 0;
    var bDireitoFamiliar = false;

    if (!(dsoGen02.recordset.BOF && dsoGen02.recordset.EOF)) {
        // seta variavel de direito familiar
        if (dsoGen02.recordset['Direito'].value >= 1)
            bDireitoFamiliar = true;
    }

    // stripa o digitado
    txtNumDoc.value = trimStr(txtNumDoc.value);

    sNumero = (txtNumDoc.value == '' ? 'NULL' : '\'' + txtNumDoc.value + '\'');

    lockControlsInModalWin(true);

    if (rdJuridica.checked == true) {
        if (bDireitoFamiliar)
            nTipoDocumentoID = -111;
        else
            nTipoDocumentoID = 111;
    }
    else {
        if (bDireitoFamiliar)
            nTipoDocumentoID = -101;
        else
            nTipoDocumentoID = 101;
    }

    setConnection(dsoGen01);

    dsoGen01.SQL = 'SELECT ' +
                    'dbo.fn_Documento_Verifica(' + sNumero + ', ' +
                                                                                   selPaisResidenciaID.value + ', ' +
                                                                                   nTipoDocumentoID + ') AS Verificacao ';

    dsoGen01.ondatasetcomplete = verifyDocNumber_DSC;
    dsoGen01.Refresh();
}
/********************************************************************
Retorno do servidor da funcao que verifica no servidor,
se o numero do documento e unico e valido no cadastro de pessoas

Parametros:
nenhum

Retorno:
nao relevante
********************************************************************/
function verifyDocNumber_DSC() {
    var bDireito = true;

    if (!(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF)) {
        // volta para o detalhe com o numero do documento digitado
        if (dsoGen01.recordset['Verificacao'].value == null) {
            if (!glb_NumDocOK) {
                glb_NumDocOK = true;
                showTextsAfterValidated();
            }

            lockControlsInModalWin(false);
            txtNomeCompleto.focus();
            return null;

        }
            // Documento Inv�lido
        else if (dsoGen01.recordset['Verificacao'].value == 0) {
            if (window.top.overflyGen.Alert('N�mero do documento inv�lido.') == 0)
                return null;

            lockControlsInModalWin(false);
            txtNumDoc.focus();
            return null;
        }
            // Outra Pessoa com este documento
        else {
            // Sem Direito
            //DireitoEspecifico
            //Pessoas->F�sicas/jur�dicas->Modal Documentos
            //20100 SFS-Grupo Pessoas -> C1&&C2 -> Verifica documento digitado, caso o mesmo j� exista no banco, permite o usu�rio detalhar o registro existente.            
            if ((glb_ndirC1 == 0) && (glb_ndirC2 == 0))
                bDireito = false;

            // tem direito para ver o registro ja existente no cadastro
            if (bDireito) {
                var nPessoaID;
                nPessoaID = dsoGen01.recordset['Verificacao'].value;

                var _retMsg = window.top.overflyGen.Confirm('Documento pertence a outra pessoa j� cadastrada.' + '\n' + 'Exibe os dados desta pessoa?');

                if (_retMsg == 0)
                    return null;
                else if (_retMsg == 1) {
                    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, nPessoaID);
                }
                else {
                    lockControlsInModalWin(false);
                    txtNumDoc.focus();
                    return null;
                }

            }
                // nao tem direito para ver o registro ja existente no cadastro
            else {
                if (window.top.overflyGen.Alert('Documento pertence a outra pessoa j� cadastrada!') == 0)
                    return null;

                lockControlsInModalWin(false);
                txtNumDoc.focus();
                return null;
            }
        }
    }
    else {
        if (window.top.overflyGen.Alert('Documento inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        txtNumDoc.focus();
        return null;
    }
    return null;
}

/*********************************************************************
Mostra/esconde e limpa os restantes dos campos caso o documento seja v�lido
*********************************************************************/
function showTextsAfterValidated() {
    var sState = 'hidden';

    if (glb_NumDocOK)
        sState = 'inherit';

    lblNomeCompleto.style.visibility = sState;
    txtNomeCompleto.style.visibility = sState;
    lblNomeFantasia.style.visibility = sState;
    txtNomeFantasia.style.visibility = sState;
    lblClassificacao.style.visibility = sState;
    selClassificacao.style.visibility = sState;
    lblDDI.style.visibility = sState;
    txtDDI.style.visibility = sState;
    lblDDD.style.visibility = sState;
    txtDDD.style.visibility = sState;
    lblTelefone.style.visibility = sState;
    txtTelefone.style.visibility = sState;
    lblEmail.style.visibility = sState;
    txtEmail.style.visibility = sState;
    lblRelacionar.style.visibility = sState;
    txtRelacionar.style.visibility = sState;
    btnPreencher.style.visibility = sState;
    lblRelacao.style.visibility = sState;
    selRelacao.style.visibility = sState;
    btnOK.style.visibility = sState;
    btnCanc.style.visibility = sState;

    txtNomeCompleto.value = '';
    txtNomeFantasia.value = '';
    selClassificacao.selectedIndex = -1;
    setLabelOfControl(lblClassificacao, selClassificacao.value);
    txtDDI.value = selPaisResidenciaID.options.item(selPaisResidenciaID.selectedIndex).getAttribute('DDDI', 1);
    txtDDD.value = '';
    txtTelefone.value = '';
    txtEmail.value = '_@_.com.br';
    txtRelacionar.value = '';
    clearComboEx(['selRelacao']);
    setLabelOfControl(lblRelacao, selRelacao.value);

    lblInvRelacao.style.visibility = 'hidden';
    chkInvRelacao.style.visibility = 'hidden';
}
