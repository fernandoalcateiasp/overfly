/********************************************************************
modalatendimento.js

Library javascript para o modalatendimento.aspx
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Variaveis de eventos de grid
var glb_OcorrenciasTimerInt = null;
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_GravacaoPendente = false;
var glb_getServerData = 0;
var glb_ResultadoID = 0;
var glb_MeioAtendimentoID = 0;
var glb_RetryWrite = 0;
var glb_rewriteInterval = null;
var glb_nPesAtendimentoID = null;
var glb_aDivControls = null;
var glb_aDivControlsFone = null;
var glb_dtAtendimentoInicio = null;
var glb_dtAtendimentoFim = null;
var glb_AtendimentoTimer = null;
var glb_nPessoaGravacaoID = 0;
var glb_sColunaClassificacao = 'ClassificacaoID*';

var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoSaldoCredito = new CDatatransport('dsoSaldoCredito');
var dsoClassificacao = new CDatatransport('dsoClassificacao');
var dsoProprietario = new CDatatransport('dsoProprietario');
var dsoAgendarHorario = new CDatatransport('dsoAgendarHorario');
var dsoGravarAgendamento = new CDatatransport('dsoGravarAgendamento');
var dsoListaPrecos = new CDatatransport('dsoListaPrecos');
var dsoWeb = new CDatatransport('dsoWeb');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
fillGridData()
fillGridData_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalatendimentoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_modalatendimentoKeyPress(KeyAscii)
js_modalatendimento_ValidateEdit()
js_fg_modalatendimentoEnterCell (fg)
js_fg_AfterRowColmodalatendimento (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Atendimento', 1);
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
                         
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // desabilita o botao OK e Cancelar
    btnOK.disabled = true;
	btnCanc.disabled = true;
	btnOK.style.visibility = 'hidden';
	btnCanc.style.visibility = 'hidden';
    	
    // Ajusta o DivControls
    adjustElementsInForm([['lblEmpresas','selEmpresas',11,1,-10,-10],
    	['lblEstados','selEstados',3,1,-5],
		['lblClassificacaoResultante','selClassificacaoResultante',3,1,-3],
	    ['lblClassificacaoInterna','selClassificacaoInterna',4,1,-3],
        ['lblPessoas','chkPessoas',3,1,-5],
		['lblRelacoesPessoas','chkRelacoesPessoas',3,1,-10],
		['lblSegurosCredito','chkSegurosCredito',3,1,-10],
        ['lblContatos','chkContatos',3,1,-10],
    	['lblClassificacao','selClassificacao',16,1,-8],
	    ['lblProprietario','selProprietario',16,1,-5],
        ['lblPesquisa','txtPesquisa',12,1,-5],
        ['btnListar','btn',btn_width-28,1,5],
		['btnGravar','btn',btn_width-28,1,3],
        ['lblClassificacaoExterna','selClassificacaoExterna',4,2,145],
        ['lblServico','selServico',10,2,-5],
        ['lblAtendimento','selAtendimento',10,2,-7],						  
        ['lblAbordagem','selAbordagem',12,2,-7],						  
        ['lblDiasMinimo','txtDiasMinimo',6,2,-7],
        ['lblDiasMaximo','txtDiasMaximo',6,2,-7],
        ['lblPedidosPendentes','chkPedidosPendentes',3,2,-5],
        ['lblSeguradora','selSeguradora',13,2,-12],
        ['lblMarca','selMarca',10,2,-7],
        ['lblLimiteMinimo','txtLimiteMinimo',5,2,-7],
        ['lblLimiteMaximo','txtLimiteMaximo',5,2,-7],
        ['lblSaldoMinimo','txtSaldoMinimo',5,2,-7],
        ['lblAgendamento','chkAgendamento',3,2,-5],						  
        ['btnWeb','btn',20,2],        
        ['lblDiasVencidos','txtDiasVencidos',3,2,-7],
        ['btnIncluir','btn',btn_width-55,2,-4],
        ['btnExcluir','btn',btn_width-55,2]], null, null, true);
        
        //btnAtender.style.top = btnListar.offsetTop + 30;
        btnAtender.style.top = txtDiasVencidos.offsetTop;
        btnAtender.style.left = btnListar.offsetLeft;
        btnAtender.style.width = btnListar.offsetWidth;
        btnAtender.setAttribute('state', 0, 1);
        
        // status=0 iniciar, =1-finalizar
        setBtnAtenderStatus(0);
        //btnListaPrecos.style.top = btnGravar.offsetTop + 30;
        btnListaPrecos.style.top = btnAtender.offsetTop;
        btnListaPrecos.style.left = btnGravar.offsetLeft;
        btnListaPrecos.style.width = btnGravar.offsetWidth;

    // Ajusta o DivTelefones/URLs
    adjustElementsInForm([['lblDDITelefone','txtDDITelefone',4,1,-10,-10],
		['lblDDDComercial','txtDDDComercial',4,1,-3],
		['lblFoneComercial','txtFoneComercial',9,1,-10],						    
		['lblDDDFaxCelular','txtDDDFaxCelular',4,1,-3],
		['lblFaxCelular','txtFaxCelular',9,1,-10],
		['lblEmail','txtEmail',30,1,-2],	
		['lblSite','txtSite',30,1,-2]], null, null, true);						  

    // Ajusta o divFinalizarAtendimento
    adjustElementsInForm([['lblAdiarDias', 'txtAdiarDias', 3, 1, 383],
		['lblAdiarHoras', 'txtAdiarHoras', 3, 1, -55, 0],
		['lblAdiarMinutos', 'txtAdiarMinutos', 3, 1],
		['btnCalcularAgenda', 'btn', 3, 1, 10],
		['lblAgendar','txtAgendar', 19, 2, 383, 0],
		['btnSolicitacaoCredito', 'btn', 148, 3, 391, -14],		
		['lblObservacoes','txtObservacoes',25,4, 0, -8]], null, null, true);
	
	btnSolicitacaoCredito.style.visibility = 'hidden';
	
	btnCalcularAgenda.style.width = 46;
	
	txtObservacoes.style.width = 535;
	txtObservacoes.style.height = 150;
	txtObservacoes.onfocus = selFieldContent;
	txtObservacoes.maxLength = 1000;
	
	btnConfirmarFinalizacao.style.top = txtObservacoes.offsetTop + txtObservacoes.offsetHeight + 10;
	btnConfirmarFinalizacao.style.left = ((txtObservacoes.offsetLeft + txtObservacoes.offsetWidth) /2) - btnConfirmarFinalizacao.offsetWidth - 5;
	btnConfirmarFinalizacao.value = 'OK';

	btnCancelarFinalizacao.style.top = txtObservacoes.offsetTop + txtObservacoes.offsetHeight + 10;
	btnCancelarFinalizacao.style.left = btnConfirmarFinalizacao.offsetLeft + btnConfirmarFinalizacao.offsetWidth + 10;
	btnCancelarFinalizacao.value = 'Cancelar';

    selEmpresas.onchange = limpaGrid;
    selEmpresas.style.height = 70;
    
    selEstados.style.height = 70;
    selEstados.onchange = limpaGrid;

    selClassificacaoResultante.style.height = 70;
    selClassificacaoResultante.onchange = limpaGrid;
    selClassificacaoInterna.onchange = limpaGrid;
    selClassificacaoExterna.onchange = limpaGrid;

	chkPessoas.checked = true;
    chkPessoas.onclick = chkPessoas_onclick;
    chkRelacoesPessoas.onclick = chkRelacoesPessoas_onclick;
	chkContatos.checked = true;
    chkSegurosCredito.onclick = chkSegurosCredito_onclick;

    selClassificacao.onchange = limpaGrid;
    selProprietario.onchange = selProprietario_onchange;

    txtPesquisa.maxLength = 40;
    txtPesquisa.onkeypress = txtPesquisa_onKeyPress;

	selServico.onchange= selServico_onChange;
	selAtendimento.onchange= selServico_onChange;
	selAbordagem.onchange= selAbordagem_onChange;

    selSeguradora.onchange = limpaGrid;
    selMarca.onchange = limpaGrid;
    chkPedidosPendentes.onclick = limpaGrid;    
    
	selSeguradora.selectedIndex = 0;
	    
    
    
    txtDiasMinimo.onkeypress = verifyNumericEnterNotLinked;        
	txtDiasMinimo.setAttribute('verifyNumPaste', 1);
	txtDiasMinimo.setAttribute('thePrecision', 10, 1);
	txtDiasMinimo.setAttribute('theScale', 0, 1);
	txtDiasMinimo.setAttribute('minMax', new Array(0, 9999), 1);
	txtDiasMinimo.onfocus = selFieldContent;
    txtDiasMinimo.maxLength = 4;
    txtDiasMinimo.value = 15;
    
    txtDiasMaximo.onkeypress = verifyNumericEnterNotLinked;        
	txtDiasMaximo.setAttribute('verifyNumPaste', 1);
	txtDiasMaximo.setAttribute('thePrecision', 10, 1);
	txtDiasMaximo.setAttribute('theScale', 0, 1);
	txtDiasMaximo.setAttribute('minMax', new Array(0, 9999), 1);
	txtDiasMaximo.onfocus = selFieldContent;
    txtDiasMaximo.maxLength = 4;
    
    txtLimiteMinimo.onkeypress = verifyNumericEnterNotLinked;        
    txtLimiteMinimo.onkeyup = txtPesquisa_onKeyPress;
	txtLimiteMinimo.setAttribute('verifyNumPaste', 1);
	txtLimiteMinimo.setAttribute('thePrecision', 11, 1);
	txtLimiteMinimo.setAttribute('theScale', 2, 1);
	txtLimiteMinimo.setAttribute('minMax', new Array(0, 999999999.99), 1);
	txtLimiteMinimo.onfocus = selFieldContent;
    txtLimiteMinimo.maxLength = 11;
    
    txtLimiteMaximo.onkeypress = verifyNumericEnterNotLinked;        
    txtLimiteMaximo.onkeyup = txtPesquisa_onKeyPress;
	txtLimiteMaximo.setAttribute('verifyNumPaste', 1);
	txtLimiteMaximo.setAttribute('thePrecision', 11, 1);
	txtLimiteMaximo.setAttribute('theScale', 2, 1);
	txtLimiteMaximo.setAttribute('minMax', new Array(0, 999999999.99), 1);
	txtLimiteMaximo.onfocus = selFieldContent;
    txtLimiteMaximo.maxLength = 11;
    
    txtSaldoMinimo.onkeypress = verifyNumericEnterNotLinked;     
    txtSaldoMinimo.onkeyup = txtPesquisa_onKeyPress;   
	txtSaldoMinimo.setAttribute('verifyNumPaste', 1);
	txtSaldoMinimo.setAttribute('thePrecision', 11, 1);
	txtSaldoMinimo.setAttribute('theScale', 2, 1);
	txtSaldoMinimo.setAttribute('minMax', new Array(0, 999999999.99), 1);
	txtSaldoMinimo.onfocus = selFieldContent;
    txtSaldoMinimo.maxLength = 11;
        
    txtDiasVencidos.onkeypress = verifyNumericEnterNotLinked;      
    txtDiasVencidos.onkeyup = txtPesquisa_onKeyPress;  
	txtDiasVencidos.setAttribute('verifyNumPaste', 1);
	txtDiasVencidos.setAttribute('thePrecision', 10, 1);
	txtDiasVencidos.setAttribute('theScale', 0, 1);
	txtDiasVencidos.setAttribute('minMax', new Array(0, 9999), 1);
	txtDiasVencidos.onfocus = selFieldContent;
    txtDiasVencidos.maxLength = 4;
    
/*
    lblRadio1.onclick = rdResultadoAtendimentoOnclick;
    lblRadio2.onclick = rdResultadoAtendimentoOnclick;
    lblRadio3.onclick = rdResultadoAtendimentoOnclick;
    lblRadio4.onclick = rdResultadoAtendimentoOnclick;
    lblRadio5.onclick = rdResultadoAtendimentoOnclick;
    
    Radio1.onclick = rdResultadoAtendimentoOnclick;
    Radio2.onclick = rdResultadoAtendimentoOnclick;
    Radio3.onclick = rdResultadoAtendimentoOnclick;
    Radio4.onclick = rdResultadoAtendimentoOnclick;
    Radio5.onclick = rdResultadoAtendimentoOnclick;
*/
    
    txtAdiarDias.onkeypress = verifyNumericEnterNotLinked;      
    txtAdiarDias.setAttribute('verifyNumPaste', 1);
	txtAdiarDias.setAttribute('thePrecision', 10, 1);
	txtAdiarDias.setAttribute('theScale', 0, 1);
	txtAdiarDias.setAttribute('minMax', new Array(0, 999), 1);
	txtAdiarDias.onfocus = selFieldContent;
    txtAdiarDias.maxLength = 2;
    
    txtAdiarHoras.onkeypress = verifyNumericEnterNotLinked;      
    txtAdiarHoras.setAttribute('verifyNumPaste', 1);
	txtAdiarHoras.setAttribute('thePrecision', 10, 1);
	txtAdiarHoras.setAttribute('theScale', 0, 1);
	txtAdiarHoras.setAttribute('minMax', new Array(0, 999), 1);
	txtAdiarHoras.onfocus = selFieldContent;
    txtAdiarHoras.maxLength = 2;
    
    txtAdiarMinutos.onkeypress = verifyNumericEnterNotLinked;      
    txtAdiarMinutos.setAttribute('verifyNumPaste', 1);
	txtAdiarMinutos.setAttribute('thePrecision', 10, 1);
	txtAdiarMinutos.setAttribute('theScale', 0, 1);
	txtAdiarMinutos.setAttribute('minMax', new Array(0, 999), 1);
	txtAdiarMinutos.onfocus = selFieldContent;
    txtAdiarMinutos.maxLength = 2;
    
    txtAgendar.onkeypress = verifyDateTimeNotLinked;      
    txtAgendar.onkeydown= txtAgendar_onkeydown;          
    txtAgendar.setAttribute('verifyNumPaste', 1);
	txtAgendar.setAttribute('thePrecision', 10, 1);
	txtAgendar.setAttribute('theScale', 0, 1);
	txtAgendar.setAttribute('minMax', new Array(0, 9999999999999999), 1);
	txtAgendar.onfocus = selFieldContent;
    txtAgendar.maxLength = 16;

    txtObservacoes.maxLength = 2000;
    txtObservacoes.onfocus = selFieldContent;
    
    txtDDITelefone.onkeypress = verifyNumericEnterNotLinked;      
    txtDDITelefone.onkeyup = txtFoneControls_onKeyup;  
	txtDDITelefone.setAttribute('verifyNumPaste', 1);
	txtDDITelefone.setAttribute('thePrecision', 10, 1);
	txtDDITelefone.setAttribute('theScale', 0, 1);
	txtDDITelefone.setAttribute('minMax', new Array(0, 9999), 1);
	txtDDITelefone.onfocus = selFieldContent;
    txtDDITelefone.maxLength = 4;
    
    txtDDDComercial.onkeypress = verifyNumericEnterNotLinked;     
    txtDDDComercial.onkeyup = txtFoneControls_onKeyup;       
	txtDDDComercial.setAttribute('verifyNumPaste', 1);
	txtDDDComercial.setAttribute('thePrecision', 10, 1);
	txtDDDComercial.setAttribute('theScale', 0, 1);
	txtDDDComercial.setAttribute('minMax', new Array(0, 9999), 1);
	txtDDDComercial.onfocus = selFieldContent;
    txtDDDComercial.maxLength = 4;
    
    txtFoneComercial.onkeypress = verifyNumericEnterNotLinked;       
    txtFoneComercial.onkeyup = txtFoneControls_onKeyup;
    txtFoneComercial.ondblclick = txtFoneComercial_ondblclick; 
	txtFoneComercial.setAttribute('verifyNumPaste', 1);
	txtFoneComercial.setAttribute('thePrecision', 10, 1);
	txtFoneComercial.setAttribute('theScale', 0, 1);
	txtFoneComercial.setAttribute('minMax', new Array(0, 99999999), 1);
	txtFoneComercial.onfocus = selFieldContent;
    txtFoneComercial.maxLength = 8;
    
    txtDDDFaxCelular.onkeypress = verifyNumericEnterNotLinked;        
    txtDDDFaxCelular.onkeyup = txtFoneControls_onKeyup;
	txtDDDFaxCelular.setAttribute('verifyNumPaste', 1);
	txtDDDFaxCelular.setAttribute('thePrecision', 10, 1);
	txtDDDFaxCelular.setAttribute('theScale', 0, 1);
	txtDDDFaxCelular.setAttribute('minMax', new Array(0, 9999), 1);
	txtDDDFaxCelular.onfocus = selFieldContent;
    txtDDDFaxCelular.maxLength = 4;
    
	txtFaxCelular.onkeypress = verifyNumericEnterNotLinked;       
	txtFaxCelular.onkeyup = txtFoneControls_onKeyup;
	txtFaxCelular.ondblclick = txtFaxCelular_ondblclick; 
	txtFaxCelular.setAttribute('verifyNumPaste', 1);
	txtFaxCelular.setAttribute('thePrecision', 10, 1);
	txtFaxCelular.setAttribute('theScale', 0, 1);
	txtFaxCelular.setAttribute('minMax', new Array(0, 99999999), 1);
	txtFaxCelular.onfocus = selFieldContent;
    txtFaxCelular.maxLength = 8;
        
    txtEmail.maxLength = 80;    
    txtEmail.onkeyup = txtFoneControls_onKeyup;
	txtEmail.ondblclick = txtEmail_ondblclick; 
	
    txtSite.maxLength = 80;
    txtSite.onkeyup = txtFoneControls_onKeyup;
	txtSite.ondblclick = txtSite_ondblclick; 
	    
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = selEmpresas.offsetLeft + selEmpresas.offsetWidth + ELEM_GAP;    
        height = (selEmpresas.offsetTop + selEmpresas.offsetHeight);
    }
    divControls.setAttribute('bEnable', true, 1);

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6) - (1.18 * (txtDDITelefone.offsetTop + txtDDITelefone.offsetHeight)) - (1.18 * (txtAdiarDias.offsetTop + txtAdiarDias.offsetHeight));
        height = 272;
    }

    // ajusta o grid    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
    
    // ajusta o divTelefones/URLs
    with (divControlsFone.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFG.currentStyle.top, 10) + parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
        //top = 354;
        width = txtDDITelefone.offsetLeft + txtDDITelefone.offsetWidth + ELEM_GAP;    
        height = 1.18 * (txtDDITelefone.offsetTop + txtDDITelefone.offsetHeight);    
    }
    divControlsFone.setAttribute('bEnable', true, 1);

    // ajusta o divAtendimento   
    with (divFinalizarAtendimento.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = 122;
        //top = parseInt(divControlsFone.currentStyle.top, 10) + parseInt(divControlsFone.currentStyle.height, 10) + ELEM_GAP - 10 ;
        top = divFG.currentStyle.top;
        width = txtObservacoes.offsetLeft + txtObservacoes.offsetWidth + ELEM_GAP;    
        height = 200;    
    }

    if ((glb_nA1 == 1) && (glb_nA2 == 1))
        glb_sColunaClassificacao = 'ClassificacaoID';

    glb_getServerData = 2;

    setConnection(dsoClassificacao);

    dsoClassificacao.SQL = 'SELECT ItemID AS ClassificacaoID, ItemMasculino AS Classificacao, ' +
        'CONVERT(VARCHAR(8000), Observacoes) AS Observacoes ' +
        'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
        'WHERE (EstadoID=2 AND TipoID = 13 AND ' +
            'dbo.fn_Direitos_TiposAuxiliares(ItemID, NULL, ' + glb_USERID + ', GETDATE()) > 0) ' +
        'ORDER BY Ordem ';
    
    dsoClassificacao.ondatasetcomplete = dsoServerData_DSC;
    dsoClassificacao.Refresh();

    setConnection(dsoProprietario);

    dsoProprietario.SQL = 'SELECT DISTINCT b.PessoaID AS ProprietarioID, b.Fantasia AS Proprietario ' +
            'FROM Pessoas a WITH(NOLOCK) ' +
                'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +
        'UNION ' +
        'SELECT DISTINCT b.PessoaID AS ProprietarioID, b.Fantasia AS Proprietario ' +
            'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +
            'WHERE (a.TipoRelacaoID = 21) ' +
        'ORDER BY Proprietario';

    dsoProprietario.ondatasetcomplete = dsoServerData_DSC;
    dsoProprietario.Refresh();
}

function setBtnAtenderStatus(nStatus)
{
    if (nStatus != null)
        btnAtender.state = nStatus;

    btnAtender.value = (btnAtender.state == 0 ? 'Iniciar' : 'Finalizar');
    btnAtender.title = (btnAtender.state == 0 ? 'Iniciar atendimento' : 'Finalizar atendimento');
}

function selProprietario_onchange()
{
    limpaGrid();
    
    if (selProprietario.value == 0)
    {
        chkSegurosCredito.checked = false;
        chkContatos.checked = false;        
    }
    
}   

function txtFoneComercial_ondblclick()
{
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    
    if ( (txtDDDComercial.value == '') || (txtFoneComercial.value.length < 8) )
		return true;

	lockInterface(true);
	dialByModalPage(txtDDDComercial.value, txtFoneComercial.value, nUserID);
	lockInterface(false);

}

function txtFaxCelular_ondblclick()
{
    var nTipoPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoPessoaID'));
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    
    if ( (txtFaxCelular.value.length < 8) || (txtDDDFaxCelular.value.length < 1) || (nTipoPessoaID != 51) )
        return true;
    
	lockInterface(true);
	dialByModalPage(txtDDDFaxCelular.value, txtFaxCelular.value, nUserID);
	lockInterface(false);
}

function txtEmail_ondblclick()
{
   if ((trimStr(txtEmail.value) != '') &&
		 (trimStr(txtEmail.value) != '_@_.com.br') && 
		 (trimStr(txtEmail.value) != '@_.com.br') && 
		 (trimStr(txtEmail.value) != '_@.com.br') &&
		 (trimStr(txtEmail.value) != '@.com.br'))
		window.open('mailto:' + txtEmail.value);
}


function txtSite_ondblclick()
{
   if ((trimStr(txtSite.value) != '') &&
		 (trimStr(txtSite.value) != 'http://www._.com.br') && 
		 (trimStr(txtSite.value) != 'http://www_.com.br') && 
		 (trimStr(txtSite.value) != 'http://www._com.br') && 
		 (trimStr(txtSite.value) != 'http://www.com.br'))
		window.open(txtSite.value);
}

function dialByModalPage(sDDD, sNumero, nPessoaID)
{
	sDDD = sDDD.toString();
	sNumero = sNumero.toString();
	nPessoaID = parseInt(nPessoaID, 10);
		
    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

function uncheckRadiosByName(sName)
{
    if (sName == 'rdMeioAtendimento')
        glb_MeioAtendimentoID = 0;

    if (sName == 'rdResultadoAtendimento')
        glb_ResultadoID = 0;
        
    coll = window.document.getElementsByTagName('INPUT');
    var primeiro = true;
    for ( i=0; i<coll.length; i++ )
    {
        if (coll.item(i).type.toUpperCase() == 'RADIO')
        {
			if(coll.item(i).name == sName)
			{
			    coll.item(i).checked = false;
			}
        }    
    }
}

function txtAgendar_onkeydown()
{
/*    if ( ((event.keyCode >= 48) && (event.keyCode <= 57)) || ((event.keyCode >= 96) && (event.keyCode <= 105)))
    {    
        if(txtAgendar.value.length==2)
            txtAgendar.value = txtAgendar.value + '/';
        else if(txtAgendar.value.length==5)        
            txtAgendar.value = txtAgendar.value + '/';
        else if(txtAgendar.value.length==10)        
            txtAgendar.value = txtAgendar.value + ' ';
        else if(txtAgendar.value.length==13)        
            txtAgendar.value = txtAgendar.value + ':';        
    }            */;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // codigo privado desta janela
    var _retMsg;
    var sMsg = '';
    var iTipoQuantSelected = 0;
        
    if ( sMsg != '' )
    {
        _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
    }    
    
    if (controlID == btnListar.id)
		fillGridData();
	else if (controlID == btnGravar.id)
		saveDataInGrid();
	else if (controlID == btnIncluir.id)
		incluiSeguro();
	else if (controlID == btnExcluir.id)
		excluiSeguro();
    else if (controlID == btnWeb.id)		
        openSiteWeb();
    else if (controlID == btnCalcularAgenda.id)		
		agendarHorario();
    else if (controlID == btnSolicitacaoCredito.id)		
    {
		if ( window.top.overflyGen.Alert ('Solicita��o de cr�dito realizada.') == 0 )
            return null;
    }		
    else if (controlID == btnAtender.id)		
		atender();
	else if (controlID == btnListaPrecos.id)		
		detalharListaPreco();
    else if (controlID == btnConfirmarFinalizacao.id)
        finalizarAtendimento();
    else if (controlID == btnCancelarFinalizacao.id)
        cancelarFinalizacaoAtendimento();
	else if (controlID == btnCanc.id)
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{   
    // @@ atualizar parametros e dados aqui
    lockControlsInModalWin(true);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(nPessoaGravacaoID)
{
    if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

	lockControlsInModalWin(true);

	var nRegistros = (((selServico.value == 3) && (selAtendimento.value == 281)) ? 1 : sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value'));
	var sEmpresas = '';
	var sEstados = '';
	var sClassificacaoResultante = '';	
	var sClassificacaoInterna = (selClassificacaoInterna.value == 0 ? '' : selClassificacaoInterna.options.item(selClassificacaoInterna.selectedIndex).innerText);
	var sClassificacaoExterna = (selClassificacaoExterna.value == 0 ? '' : selClassificacaoExterna.options.item(selClassificacaoExterna.selectedIndex).innerText);	
	var bPessoas = (chkPessoas.checked ? 1 : 0);
	var bRelacoesPessoas = (chkRelacoesPessoas.checked ? 1 : 0);
	var bSegurosCredito = (chkSegurosCredito.checked ? 1 : 0);
	var bContatos = (chkContatos.checked ? 1 : 0);
	var nClassificacaoID = (selClassificacao.value == 0 ? '' : selClassificacao.value);		    
	var sProprietario = (selProprietario.value == 0 ? '' : selProprietario.value);		    
	var sPesquisa = trimStr(txtPesquisa.value);
    var nServico = (selServico.value);
    var nAtendimento = (selAtendimento.value);
	var sLimiteMinimo = trimStr(txtLimiteMinimo.value);
	var sLimiteMaximo = trimStr(txtLimiteMaximo.value);
	var sSaldoMinimo = trimStr(txtSaldoMinimo.value);
	var sDiasVencidos = trimStr(txtDiasVencidos.value);     	    
	var sDiasMinimo = trimStr(txtDiasMinimo.value);     	    
	var sDiasMaximo= trimStr(txtDiasMaximo.value);     	    
	var sSeguradoraID = (selSeguradora.value == 0 ? '' : selSeguradora.value);		    
    var sMarcaID = (selMarca.value == 0 ? '' : selMarca.value);
	var bPedidosPendentes = (chkPedidosPendentes.checked ? 1 : 0);
    var nOrdemAgendamento = 0;
    var strPars = '';
    var nItensSelected = 0;
    var i = 0;
    
    if (selServico.value == 3)
    {
        if (selAtendimento.value == 281)
            nOrdemAgendamento=1;
        else
            nOrdemAgendamento=(chkAgendamento.checked ? 1 : 0);
    }
        
    for (i=0; i<selEmpresas.length; i++)
    {
        if ( selEmpresas.options[i].selected == true )
        { 
            sEmpresas += '/' + selEmpresas.options[i].value;
        } 
    }    
    sEmpresas = (sEmpresas == '' ? '' : sEmpresas + '/');
    
    if (sEmpresas == '')
    {
        if ( window.top.overflyGen.Alert ('Selecione pelo menos uma empresa.') == 0 )
            return null;
            
        lockControlsInModalWin(false);
            
        return null;
    }
    
    for (i=0; i<selClassificacaoResultante.length; i++)
    {
        if ( selClassificacaoResultante.options[i].selected == true )
        { 
            sClassificacaoResultante += '/' + selClassificacaoExterna.options.item(i+1).innerText;
        } 
    }
    sClassificacaoResultante = (sClassificacaoResultante == '' ? '' : sClassificacaoResultante + '/');
    
    
    for (i=0; i<selEstados.length; i++)
    {
        if ( selEstados.options[i].selected == true )
        { 
            sEstados += '/' + selEstados.options[i].value;
        } 
    }
    sEstados = (sEstados == '' ? '' : sEstados + '/');
    
	strPars = '?RegistroID=' + escape(nRegistros);
	strPars += '&EmpresaID=' + escape(glb_aEmpresaData[0]);
	strPars += '&sEmpresas=' + escape(sEmpresas);
	strPars += '&sEstados=' + escape(sEstados);
	strPars += '&sClassificacaoResultante=' + escape(sClassificacaoResultante);
	strPars += '&sClassificacaoInterna=' + escape(sClassificacaoInterna);
	strPars += '&sClassificacaoExterna=' + escape(sClassificacaoExterna);
	strPars += '&bPessoas=' + escape(bPessoas);
	strPars += '&bRelacoesPessoas=' + escape(bRelacoesPessoas);
	strPars += '&bSegurosCredito=' + escape(bSegurosCredito);
	strPars += '&bContatos=' + escape(bContatos);
	strPars += '&nClassificacaoID=' + escape(nClassificacaoID);
	strPars += '&sProprietario=' + escape(sProprietario);
	strPars += '&sPesquisa=' + escape(sPesquisa);
	strPars += '&nServico=' + escape(nServico);
    strPars += '&nAtendimento=' + escape(nAtendimento);
	strPars += '&sDiasMinimo=' + escape(sDiasMinimo);
	strPars += '&sDiasMaximo=' + escape(sDiasMaximo);	
	strPars += '&nOrdemAgendamento=' + escape(nOrdemAgendamento);
	strPars += '&sLimiteMinimo=' + escape(sLimiteMinimo);
	strPars += '&sLimiteMaximo=' + escape(sLimiteMaximo);
	strPars += '&sSaldoMinimo=' + escape(sSaldoMinimo);
	strPars += '&sDiasVencidos=' + escape(sDiasVencidos);
	strPars += '&sSeguradoraID=' + escape(sSeguradoraID);
	strPars += '&sMarcaID=' + escape(sMarcaID);
	strPars += '&bPedidosPendentes=' + escape(bPedidosPendentes);
	strPars += '&nPessoaGravacaoID=' + escape(nPessoaGravacaoID == null ? 0 : nPessoaGravacaoID);
    strPars += '&sUserID=' + escape(glb_nUserID);

	dsoGrid.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/listaralteracaocredito.aspx' + strPars;
	dsoGrid.ondatasetcomplete = fillGridData_DSC;
	dsoGrid.Refresh();
}

function atender()
{
    var sAlert = '';
    var sdtAgendamento = '';    
    var nPessoaID = 0;
    
    if (fg.Row>0)
        nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));
        
    if (nPessoaID<=0)        
        sAlert = 'Selecione um cliente no grid';

    if (btnAtender.state == 2)
    {
        setBtnAtenderStatus(1);
        glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);
        btnAtender.disabled = false;
    }

    // Finalizar atendimento
    if (btnAtender.state == 1)
    {
        setBtnAtenderStatus(2);
        visibilityControls();
        btnAtender.disabled = true;
        return null;
    }

    if (sAlert.length > 0)
    {
        if (window.top.overflyGen.Alert(sAlert) == 0 )
		    return null;
        return null;	
    }

    var sResultadoID = glb_ResultadoID;
    var sObservacao = trimStr(txtObservacoes.value);
    var nAtendimento = selAtendimento.value;
    
    lockControlsInModalWin(true);
    var dtStartAtendimento = new Date();
    glb_dtAtendimentoInicio = dtStartAtendimento.getTime();
    setTimerAgendamentoStatus(true);
    
    nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));
    
    strPars = '?nPessoaID='+ escape(nPessoaID);
    strPars += '&nStatus=' + escape(btnAtender.state == 0 ? '0' : '1');
	strPars += '&nEmpresaID=' + escape(glb_aEmpresaData[0]);
	strPars += '&nUserID=' + escape(glb_nUserID);
	strPars += '&nTipoAtendimentoID=' + escape(nAtendimento);
	
    try
    {
        dsoGravarAgendamento.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/gravaragendamento.aspx' + strPars;
        dsoGravarAgendamento.ondatasetcomplete = atender_DSC;
        dsoGravarAgendamento.Refresh();
    }
    catch(e)
	{
	    glb_RetryWrite++;
	    
	    if (glb_RetryWrite <= 3)
        {
            glb_rewriteInterval = window.setInterval('rewriteInterval()', 300, 'JavaScript');
            return null;
        }
        
        glb_RetryWrite = 0;
	    
	    if ( window.top.overflyGen.Alert('P1 - N�o foi poss�vel gravar o atendimento, tente novamente.') == 0 )
			return null;

		lockControlsInModalWin(false);	
	}
}

function rewriteInterval()
{
	if (glb_rewriteInterval != null)
	{
		window.clearInterval(glb_rewriteInterval);
		glb_rewriteInterval = null;
	}
	
	atender();
}

function atender_DSC()
{
    glb_nPesAtendimentoID = dsoGravarAgendamento.recordset['PesAtendimentoID'].value;

    try
    {
        lockControlsInModalWin(false);

        // Altera o status de Iniciar para finalizar
        setBtnAtenderStatus(1);
        visibilityControls();
    }
    catch(e)
    {
        if ( window.top.overflyGen.Alert('P2 - ' + e.message) == 0 )
			return null;
			
        lockControlsInModalWin(false);
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    if(((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF)) ||(dsoGrid.recordset.fields.Count==1))
    {
        lockControlsInModalWin(false);
        return;
    }
    
	window.focus();
	
    var dTFormat = '';
    
    var sTexto  = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var nServico = (selServico.value);
    
    var aHidenCols;
    
    //Credito
    if (nServico == 1)
        aHidenCols = new Array(13,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41);
    else if (nServico == 2)
        aHidenCols = new Array(1,6,7,28,29,30,31,32,33,34,35,36,37,38,39,40,41);
    // Carteira
    else if (nServico == 3)
        aHidenCols = new Array(6,7,16,17,18,19,20,21,25,26,27,28,29,30,31,32,33,34,35,36,37,38,41);

    var i;
            
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

	headerGrid(fg,['Pessoa',
                   'Nome completo',
   				   'ID',
   				   'Classifica��o',
				   'T',
                   'E',
   				   'Observa��o',
   				   'Observa��o Edit�vel',
   				   'Fat Anual US$',
   				   'Func',
   				   'C',
				   'I',
				   'E',
				   'Dias',
				   'Empresa',
				   'Propriet�rio',
				   'Validade',
				   'Dias',
				   'Pot Cr�dito',
				   'Limite Cr�dito',
				   'Valor Aberto',
				   'Saldo Cr�dito',
				   'Limite M�ximo',
				   'Seguradora',
				   'Marca',
				   'TipoID',
				   'RegistroID',
				   'TipoPessoaID',
				   'DDITelefone',
				   'DDDComercial',
				   'Tel Comercial',
				   'DDDFaxCelular',
				   'FaxCelular',
				   'Email',
				   'Site',
				   'SujeitoID',
				   'Agendamento',
				   'Observa��o',
				   'OK'],aHidenCols);
				   
    glb_aCelHint = [[0,4,'Tipo'],
                    [0,5,'Estado'],
                    [0,8,'Faturamento anual (US$)'],
                    [0,9,'N�mero de funcion�rios'],
                    [0,10,'Classifica��o resultante'],
					[0,11,'Classifica��o interna'],
                    [0,12,'Classifica�ao externa'],
                    [0,13,'N�mero de dias do �ltimo pedido de compra do cliente']];				   

    fillGridMask(fg,dsoGrid,['Pessoa*',
                   'PessoaNome*',
   				   'PessoaID*',
                   glb_sColunaClassificacao,
				   'Tipo*',
                   'Estado*',
                   'Observacao*',
                   'ObservacaoEditavel',
                   'FaturamentoAnual',
                   'NumeroFuncionarios',
                   'ClassificacaoResultante*',
				   'ClassificacaoInterna*',
				   'ClassificacaoExterna*',
				   'DiasUltimaCompra*',
				   'Empresa*',
				   'ProprietarioID',
				   'dtValidade',
				   'Dias*',
				   'PotencialCredito',
				   'LimiteCredito',
				   'ValorAberto*',
				   'SaldoCredito*',
				   'LimiteMaximo*',
				   'Seguradora*',
				   'Marca*',
				   'TipoID',
				   'RegistroID',
				   'TipoPessoaID',
				   'DDITelefone',
				   'DDDComercial',
				   'TelComercial',
				   'DDDFaxCelular',
				   'FaxCelular',
				   'Email',
				   'Site',
				   'SujeitoID',
				   'dtAgendamento*',
				   'ObservacaoAgendamento*',
				   'OK'],
					 ['','','','','','','','','999999999999.99','999999','','','','','','','99/99/9999','','99999999.99','99999999.99','','','','','','','','','','','','','','','','99/99/9999 99:99','',''],
					 ['','','','','','','','','###,###,###,##0.00','','','','','','','',dTFormat,'','###,###,##0.00','###,###,##0.00','###,###,##0.00','###,###,##0.00','###,###,##0.00','','','','','','','','','','','','',dTFormat,'','']);
    
    // Eh credito
    if (nServico == 2)
        fg.FrozenCols = 4;
    else        
        fg.FrozenCols = 1;

    fg.MergeCells = 4;
	fg.MergeCol(0) = true;
	fg.MergeCol(1) = true;
	fg.MergeCol(2) = true;
	fg.MergeCol(3) = true;
	fg.MergeCol(4) = true;
	fg.MergeCol(5) = true;

    fg.Redraw = 0;
    
    alignColsInGrid(fg,[2,8,9,13,17,21,22,23,24,25]);

    // Combo de Classifica��o
    insertcomboData(fg, getColIndexByColKey(fg, glb_sColunaClassificacao), dsoClassificacao, '*Classificacao|Observacoes','ClassificacaoID');

    // Combo de Propriet�rio
    insertcomboData(fg, getColIndexByColKey(fg, 'ProprietarioID'), dsoProprietario, 'Proprietario','ProprietarioID');

	fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
	//paintCellsSpecialyReadOnly();
	
	for (i=1; i<fg.Rows; i++)
	{
	    nCol = getColIndexByColKey(fg, glb_sColunaClassificacao);
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;

	    nCol = getColIndexByColKey(fg, 'ObservacaoEditavel');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;

	    nCol = getColIndexByColKey(fg, 'FaturamentoAnual');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;
       //vermelho 0X7280FA
        else if (((fg.TextMatrix(i, nCol)=='') || (fg.TextMatrix(i, nCol)=='0')) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'Tipo*'))=='P'))
            fg.Cell(6, i, nCol, i, nCol) = 0X7280FA;   
            
        nCol = getColIndexByColKey(fg, 'NumeroFuncionarios');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;   
        //vermelho 0X7280FA
        else if (((fg.TextMatrix(i, nCol)=='') || (fg.TextMatrix(i, nCol)=='0')) && (fg.TextMatrix(i, getColIndexByColKey(fg, 'Tipo*'))=='P'))
            fg.Cell(6, i, nCol, i, nCol) = 0X7280FA;                        

	    nCol = getColIndexByColKey(fg, 'dtValidade');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;

	    nCol = getColIndexByColKey(fg, 'LimiteCredito');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;

	    nCol = getColIndexByColKey(fg, 'PotencialCredito');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;

	    nCol = getColIndexByColKey(fg, 'ProprietarioID');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;
	}
	    
	fg.ExplorerBar = 5;
	
	sTexto = 'Atendimento';
	
	if (fg.Rows == 2)
	    sTexto += ': ' + (fg.Rows - 1) + ' registro';    	    
    else if	(fg.Rows > 2)    
	    sTexto += ': ' + (fg.Rows - 1) + ' registros';
	    
    if ((glb_dtAtendimentoInicio !=null) && (glb_dtAtendimentoFim != null))
        sTexto = sTexto + '  [' + tempoDecorrido(glb_dtAtendimentoInicio, glb_dtAtendimentoFim) + ']';
	    
	secText(sTexto, 1);
	
	lockControlsInModalWin(false);
	
	fg.Editable = true;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    fg.Redraw = 2;
    
    setBtnState();
    
    js_fg_modalatendimentoEnterCell (fg);
    
    controlsFoneReadOnly();
}

// EVENTOS DE GRID **************************************************
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalatendimentoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalatendimentoEnterCell (grid)
{
    if (glb_GridIsBuilding)
        return;
        
    if ((grid.Rows<=0) || (grid.Row<=0))
        return;
        
    var nTipoPessoaID = grid.ValueMatrix(grid.Row, getColIndexByColKey(grid, 'TipoPessoaID'));
    var sTipo = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'Tipo*'));
    var sEmail = '';
    var sSite = '';
    
    if ((sTipo == 'P') || (sTipo == 'C'))
    {
        txtDDITelefone.disabled = false;
        txtDDDComercial.disabled = false;
        txtFoneComercial.disabled = false;
        txtDDDFaxCelular.disabled = false;
        txtFaxCelular.disabled = false;
        txtEmail.disabled = false;
        txtSite.disabled = false;
        lblFaxCelular.innerText = 'Celular';
        
        if (nTipoPessoaID == 52)
            lblFaxCelular.innerText = 'Fax';        
            
        txtDDITelefone.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'DDITelefone'));
        txtDDDComercial.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'DDDComercial'));
        txtFoneComercial.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'TelComercial'));
        txtDDDFaxCelular.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'DDDFaxCelular'));
        txtFaxCelular.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'FaxCelular'));
        
        sEmail = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'Email'));
        sSite = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'Site'));
        
        if (sEmail == '')
            txtEmail.value = '_@_.com.br';
        else
            txtEmail.value = sEmail;
            
        if (sSite == '')
            txtSite.value = 'http://www._.com.br';
        else
            txtSite.value = sSite;    
    }
    else
    {
        txtDDITelefone.value = '';
        txtDDDComercial.value = '';
        txtFoneComercial.value = '';
        txtDDDFaxCelular.value = '';
        txtFaxCelular.value = '';
        txtEmail.value = '';
        txtSite.value = '';
    
        txtDDITelefone.disabled = true;
        txtDDDComercial.disabled = true;
        txtFoneComercial.disabled = true;
        txtDDDFaxCelular.disabled = true;
        txtFaxCelular.disabled = true;
        txtEmail.disabled = true;
        txtSite.disabled = true;
    }
    
    setLinkFoneUrl();                
}
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalatendimentoKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalatendimento_BeforeEdit(grid, row, col)
{
    if (glb_GridIsBuilding)
        return;

    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
    var sTipo = '';
    
    // limita qtd de caracteres digitados em uma celula do grid    
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
    {
        if (getColIndexByColKey(fg, 'ObservacaoEditavel') == col)
        {
            // Dependendo do tipo da linha, P, R ou S, seta o maxlength do campo Observacao
            sTipo = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Tipo*'));
            
            if (sTipo == 'P')
                grid.EditMaxLength = 20;
            else if (sTipo == 'R')
                grid.EditMaxLength = 30;
            else if (sTipo == 'S')
                grid.EditMaxLength = 40;                    
        }
        else
            grid.EditMaxLength = fldDet[1];
    }
    else    
        grid.EditMaxLength = 0;
}


/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalatendimento_ValidateEdit()
{
    ;
}
function fg_DblClick()
{
    if (fg.Row < 1)    
        return true;
    lockControlsInModalWin(true);
    
    var PessoaID = getCellValueByColKey(fg, 'SujeitoID', fg.Row);

    setConnection(dsoSaldoCredito);                              
    dsoSaldoCredito.SQL = 'SELECT dbo.fn_Pessoa_Posicao(' + PessoaID + ', -' + glb_aEmpresaData[0] + ', 9, GETDATE(), 0, 0, NULL, NULL, NULL) AS ValorAberto';
    dsoSaldoCredito.ondatasetcomplete = dsoSaldoCredito_DSC;
    dsoSaldoCredito.refresh();
}

function dsoSaldoCredito_DSC()
{
    var bZeraValorAberto = false;
    var bZeraSaldoCredito = false;
    var nLimiteCredito = 0;
    var nValorAberto = 0;
    
    if (!((dsoSaldoCredito.recordset.BOF)||(dsoSaldoCredito.recordset.EOF)))
    {
        nLimiteCredito = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'LimiteCredito'));
        
        if (!isNaN(dsoSaldoCredito.recordset['ValorAberto'].value))
        {
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorAberto*')) = dsoSaldoCredito.recordset['ValorAberto'].value;
            nLimiteCredito = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'LimiteCredito'));
            
            if (! isNaN(nLimiteCredito))
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SaldoCredito*')) = nLimiteCredito - dsoSaldoCredito.recordset['ValorAberto'].value;
            else
                bZeraSaldoCredito = true;
        }
        else if (! isNaN(nLimiteCredito))
        {
            bZeraValorAberto = true;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SaldoCredito*')) = nLimiteCredito;
        }
        else
        {
            bZeraValorAberto = true;
            bZeraSaldoCredito = true;
        }
    }
    
    if (bZeraValorAberto)
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorAberto*')) = '';
        
    if (bZeraSaldoCredito)
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SaldoCredito*')) = '';

    lockControlsInModalWin(false);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalatendimento_AfterEdit(Row, Col)
{
    if (glb_GridIsBuilding)
        return;

    if((Row>0) && (Col>0))
    {
        if ( (Col == getColIndexByColKey(fg, glb_sColunaClassificacao)) ||
             (Col == getColIndexByColKey(fg, 'FaturamentoAnual')) ||
             (Col == getColIndexByColKey(fg, 'NumeroFuncionarios')) ||
             (Col == getColIndexByColKey(fg, 'ProprietarioID')) )
        {
            if (cellIsLocked(Row, Col))
            {
		        if (fg.ValueMatrix(Row, Col) == 0)
			        fg.TextMatrix(Row, Col) = 1;
		        else
			        fg.TextMatrix(Row, Col) = 0;
            }
        }
        
	    if (Col == getColIndexByColKey(fg, 'LimiteCredito'))
	    {
		    fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    		
		    if (fg.ValueMatrix(Row, Col) > fg.ValueMatrix(Row, getColIndexByColKey(fg, 'LimiteMaximo*')))
		    {
		        fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, getColIndexByColKey(fg, 'LimiteMaximo*')));
		    }
        }
        
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
        glb_GravacaoPendente = true;
        setBtnState();
    }
}
    
/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	var bCB = 0;
	var bCG = 0;
	var bES = 0;
	var sEmail = '';
	var sSite = '';

	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
	glb_GravacaoPendente = false;
    glb_nPessoaGravacaoID = 0;
    
    lockControlsInModalWin(true);
    
    // Carteira - Ativo
    if ((selServico.value == 3) && (selAtendimento.value == 281) && (fg.Rows > 1))
        glb_nPessoaGravacaoID = getCellValueByColKey(fg, 'PessoaID*', 1);
    
    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?nUsuarioID=' + escape(glb_nUserID);
			}

            strPars += '&nServico=' + escape(selServico.value);

		    if (fg.TextMatrix(i, getColIndexByColKey(fg, 'ObservacaoEditavel')) != '')
			    strPars += '&sObservacao=' + escape('\'' + fg.TextMatrix(i, getColIndexByColKey(fg, 'ObservacaoEditavel')) + '\'');
		    else
			    strPars += '&sObservacao=' + escape('NULL');

            strPars += '&nClassificacaoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, glb_sColunaClassificacao)));
            strPars += '&nFaturamentoAnual=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'FaturamentoAnual')));
            strPars += '&nNumeroFuncionarios=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'NumeroFuncionarios')));

		    strPars += '&nRegistroID=' + escape(getCellValueByColKey(fg, 'RegistroID', i));
		    strPars += '&nTipoID=' + escape(getCellValueByColKey(fg, 'TipoID', i));
		    
		    if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtValidade')) != 0)
			    strPars += '&dtValidade=' + escape('\'' + normalizeDate_DateTime(getCellValueByColKey(fg, 'dtValidade', i), true) + '\'');
		    else
			    strPars += '&dtValidade=' + escape('NULL');

            strPars += '&nLimiteCredito=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'LimiteCredito')));
            strPars += '&nPotencialCredito=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PotencialCredito')));
            strPars += '&nProprietarioID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ProprietarioID')));
            strPars += '&sTipoPessoaID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'TipoPessoaID')));
            strPars += '&sDDITelefone=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'DDITelefone')));
            strPars += '&sDDDComercial=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'DDDComercial')));
            strPars += '&sTelComercial=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'TelComercial')));
            strPars += '&sDDDFaxCelular=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'DDDFaxCelular')));
            strPars += '&sFaxCelular=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'FaxCelular')));
            
            sEmail = fg.TextMatrix(i, getColIndexByColKey(fg, 'Email'));
            sSite = fg.TextMatrix(i, getColIndexByColKey(fg, 'Site'));
            
            if ((sEmail == '_@_.com.br') ||
                (sEmail == '@_.com.br') ||
                (sEmail == '_@.com.br') ||
                (sEmail == '@.com.br'))
                strPars += '&sEmail=';
            else
                strPars += '&sEmail=' + escape(sEmail);

            if ((sSite == 'http://www._.com.br') ||
                (sSite == 'http://www_.com.br') ||
                (sSite == 'http://www._com.br') ||
                (sSite == 'http://www.com.br'))
                strPars += '&sSite=';
            else
                strPars += '&sSite=' + escape(sSite);
                
			nDataLen++;
		}
	}

	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/alteracaocredito.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData(' + glb_nPessoaGravacaoID.toString() + ')', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData(' + glb_nPessoaGravacaoID.toString() + ')', 10, 'JavaScript');
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function detalharListaPreco()
{
    if (fg.Row<=0)
    {
        if ( window.top.overflyGen.Alert('Selecione um cliente no grid') == 0 )
			return null;
			
        return null;
    }    

    lockInterface(true);
    
    var empresa = getCurrEmpresaData();
    var nClienteID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));
    var strPars = new String();

    strPars = '?';
    strPars += 'nClienteID=';
    strPars += escape(nClienteID.toString());
    strPars += '&nEmpresaID=';
    strPars += escape(empresa[0].toString());
                
    dsoListaPrecos.URL = SYS_ASPURLROOT + '/serversidegenEx/listaprecosdata.aspx'+strPars;
    dsoListaPrecos.ondatasetcomplete = detalhalistaPrecos_DSC;
    dsoListaPrecos.refresh();    
}

function detalhalistaPrecos_DSC()
{
    var empresa = getCurrEmpresaData();
    var aListaPrecoData = new Array();
    
    if ( !(dsoListaPrecos.recordset.BOF && dsoListaPrecos.recordset.EOF) )
    {
        aListaPrecoData[0] = dsoListaPrecos.recordset['Fantasia'].value;
        aListaPrecoData[1] = dsoListaPrecos.recordset['PessoaID'].value;
		aListaPrecoData[2] = dsoListaPrecos.recordset['UFID'].value;
		aListaPrecoData[3] = dsoListaPrecos.recordset['EmpresaSistema'].value;
		aListaPrecoData[4] = dsoListaPrecos.recordset['ListaPreco'].value;
		aListaPrecoData[5] = dsoListaPrecos.recordset['PMP'].value;
		aListaPrecoData[6] = dsoListaPrecos.recordset['NumeroParcelas'].value;
		aListaPrecoData[7] = dsoListaPrecos.recordset['FinanciamentoPadrao'].value;
		aListaPrecoData[8] = dsoListaPrecos.recordset['ClassificacaoInterna'].value;
		aListaPrecoData[9] = dsoListaPrecos.recordset['ClassificacaoExterna'].value;
    
        // variavel global no sup para controlar quem mandou o carrier
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_CarrierSendListaPrecoAtentimento = true;');
        // Manda o id da pessoa a detalhar 
        sendJSCarrier(getHtmlId(), 'SETPESSOAPRICELIST', new Array(empresa[0], aListaPrecoData, glb_nPesAtendimentoID, glb_dtAtendimentoInicio,
            selAtendimento.options[selAtendimento.selectedIndex].value));
    }
    
    lockInterface(false);
}

function incluiSeguro()
{
    var sErro = '';
    var strPars = '';
    
    if (fg.Row < 1)
        sErro = 'Selecione uma linha do tipo R no grid para incluir um seguro de cr�dito';
    else if (getCellValueByColKey(fg, 'TipoID', fg.Row) != 2)
        sErro = 'Selecione uma linha do tipo R no grid para incluir um seguro de cr�dito';
    else if (selSeguradora.selectedIndex <= 0)
        sErro = 'Selecione uma seguradora';

    if (sErro != '')
    {
        if ( window.top.overflyGen.Alert(sErro) == 0 )
            return null;

        lockControlsInModalWin(false);
        return null;
    }
    
    strPars  = '?RelacaoID=' + escape(getCellValueByColKey(fg, 'RegistroID', fg.Row));
    strPars += '&SeguradoraID=' + escape(selSeguradora.value);
    strPars += '&MarcaID=' + escape(selMarca.selectedIndex > 0 ? selMarca.value : 0);
    strPars += '&UsuarioID=' + escape(glb_nUserID);
    strPars += '&Excluir=' + escape(0);

	dsoGrava.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/criasegurocredito.aspx' + strPars;
	dsoGrava.ondatasetcomplete = segurocredito_DSC;
	dsoGrava.refresh();
}

function agendarHorario()
{
    try
    {
        if (rd255.checked)
        {
            lockControlsInModalWin(false);
            txtAgendar.value = '';
            return;
        }
    }
    catch(e)
    {
        ;
    }
        
    lockControlsInModalWin(true);
    
    var sDias = (txtAdiarDias.value == '' ? '0' : txtAdiarDias.value);
    var sHoras = (txtAdiarHoras.value == '' ? '0' : txtAdiarHoras.value);
    var sMinutos = (txtAdiarMinutos.value == '' ? '0' : txtAdiarMinutos.value);

    setConnection(dsoAgendarHorario);

    dsoAgendarHorario.SQL = 'SELECT CONVERT(VARCHAR(10),(dbo.fn_Data_ProjetaHora(GETDATE(), ' + sDias + ', ' + sHoras + ', ' + sMinutos + ', 0, ' + glb_aEmpresaData[0] + ')),' + DATE_SQL_PARAM + ') + \' \' +   ' +
	                            'CONVERT(VARCHAR(5),(dbo.fn_Data_ProjetaHora(GETDATE(), ' + sDias + ', ' + sHoras + ', ' + sMinutos + ', 0, ' + glb_aEmpresaData[0] + ')),108) AS Data';
    
    dsoAgendarHorario.ondatasetcomplete = agendarHorario_DSC;
    dsoAgendarHorario.Refresh();
}

function agendarHorario_DSC()
{
    if ( !((dsoAgendarHorario.recordset.BOF) || (dsoAgendarHorario.recordset.EOF)) )
    {
        if ((dsoAgendarHorario.recordset['Data'].value != null) && (dsoAgendarHorario.recordset['Data'].value) != '')
            txtAgendar.value = dsoAgendarHorario.recordset['Data'].value;            
    }
    
    lockControlsInModalWin(false);
}

function segurocredito_DSC()
{
    if ( !((dsoGrava.recordset.BOF) || (dsoGrava.recordset.EOF)) )
    {
        if ((dsoGrava.recordset['Resultado'].value != null) &&
            (trimStr(dsoGrava.recordset['Resultado'].value) != ''))
        {
            if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
                return null;
        }
    }

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

function js_fg_modalatendimento_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
    if (glb_GridIsBuilding)
        return;

	//Forca celula read-only    
	if ( glb_FreezeRolColChangeEvents )
		return true;

	if (cellIsLocked(NewRow, NewCol))
	{
		glb_validRow = OldRow;
		glb_validCol = OldCol;
	}
	else
	    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
}

function cellIsLocked(nRow, nCol)
{
    var retVal = false;
    
    // RelPessoas
    else if (getCellValueByColKey(fg, 'TipoID', nRow) == 2)
    {
        if ( (nCol == getColIndexByColKey(fg, glb_sColunaClassificacao)) ||
             (nCol == getColIndexByColKey(fg, 'FaturamentoAnual')) ||
             (nCol == getColIndexByColKey(fg, 'NumeroFuncionarios')) ||
             (nCol == getColIndexByColKey(fg, 'dtValidade')) ||
             (nCol == getColIndexByColKey(fg, 'PotencialCredito')) )
            retVal = true;
    }
    // Seguro Credito
    else if (getCellValueByColKey(fg, 'TipoID', nRow) == 3)
    {
        if ( (nCol == getColIndexByColKey(fg, glb_sColunaClassificacao)) ||
             (nCol == getColIndexByColKey(fg, 'FaturamentoAnual')) ||
             (nCol == getColIndexByColKey(fg, 'NumeroFuncionarios')) ||
             (nCol == getColIndexByColKey(fg, 'dtValidade')) ||
             (nCol == getColIndexByColKey(fg, 'PotencialCredito')) )
            retVal = true;
        else if ((glb_nA1 == 0) || (glb_nA2 == 0))
            retVal = true;
    }
    // Contatos
    else if (getCellValueByColKey(fg, 'TipoID', nRow) == 4)
    {
        if ( (nCol == getColIndexByColKey(fg, glb_sColunaClassificacao)) ||
             (nCol == getColIndexByColKey(fg, 'FaturamentoAnual')) ||
             (nCol == getColIndexByColKey(fg, 'NumeroFuncionarios')) ||
             (nCol == getColIndexByColKey(fg, 'dtValidade')) ||
             (nCol == getColIndexByColKey(fg, 'PotencialCredito')) )
            retVal = true;
    }
    
    return retVal;
}

function excluiSeguro()
{
    if (fg.Row < 1)
    {
        lockControlsInModalWin(false);
        return null;
    }
    else if (getCellValueByColKey(fg, 'TipoID', fg.Row) != 3)
    {
        if ( window.top.overflyGen.Alert ('S� � permitido excluir linhas to tipo "S".') == 0 )
            return null;

        lockControlsInModalWin(false);
        return null;
    }
    
	var _retMsg = window.top.overflyGen.Confirm('Deseja realmente excluir este seguro ?');
    
	if ( _retMsg == 1 )
	{
        lockControlsInModalWin(true);
        
        strPars  = '?RelacaoID=' + escape(getCellValueByColKey(fg, 'RegistroID', fg.Row));
        strPars += '&SeguradoraID=' + escape(0);
        strPars += '&MarcaID=' + escape(0);
        strPars += '&UsuarioID=' + escape(glb_nUserID);
        strPars += '&Excluir=' + escape(1);

	    dsoGrava.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/criasegurocredito.aspx' + strPars;
	    dsoGrava.ondatasetcomplete = segurocredito_DSC;
	    dsoGrava.refresh();
    }
}

function js_fg_modalatendimentoAfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol)
{
    if (glb_GridIsBuilding)
        return;

    setBtnState();
}

function setBtnState()
{
    var bListar = true;
    var bInclui = false;
    var bExclui = false;
    
    if ((chkPessoas.checked) || (chkRelacoesPessoas.checked) || (chkSegurosCredito.checked))
        bListar = false;

    if (btnAtender.state >= 1)
        bListar = true;

    btnListar.disabled = bListar;

    btnGravar.disabled = !glb_GravacaoPendente;
    
    if (fg.Row > 0)
    {
        if (getCellValueByColKey(fg, 'TipoID', fg.Row) == 2)
            bInclui = true;

        if (getCellValueByColKey(fg, 'TipoID', fg.Row) == 3)
            bExclui = true;
    }
    
    btnIncluir.disabled = (!bInclui) || (glb_nA1 == 0) || (glb_nA2 == 0);
    btnExcluir.disabled = (!bExclui) || (glb_nA1 == 0) || (glb_nA2 == 0);
    
}

function txtPesquisa_onKeyPress()
{
    limpaGrid();
    if ( event.keyCode == 13 )
        fillGridData();
}

/********************************************************************
Retorno do servidor
********************************************************************/
function dsoServerData_DSC()
{
    glb_getServerData--;

    if (glb_getServerData != 0)
        return null;

    // ajusta o body do html
    with (modalatendimentoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
	glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // mostra a modal
    showExtFrame(window, true);
    
    selServico_onChange();
    
    selAbordagem_onChange();

    setBtnState();
    
   // fillGridData();
}

function chkPessoas_onclick()
{
    limpaGrid();
    visibilityControls();
    setBtnState();    
}

function chkRelacoesPessoas_onclick()
{
    limpaGrid();
    visibilityControls();
    setBtnState();
}

function chkSegurosCredito_onclick()
{
    limpaGrid();
    visibilityControls();
    setBtnState();
}

/********************************************************************
Alteracao nos texts de Telefones e urls
********************************************************************/
function txtFoneControls_onKeyup()
{   
    var nTipoPessoaID = 0;
    
    if (fg.Rows > 1) 
    {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DDITelefone')) = txtDDITelefone.value ;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DDDComercial')) = txtDDDComercial.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TelComercial')) = txtFoneComercial.value;  					   
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DDDFaxCelular')) = txtDDDFaxCelular.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FaxCelular')) = txtFaxCelular.value; 
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Email')) = txtEmail.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Site')) = txtSite.value;    
        
        js_modalatendimento_AfterEdit(fg.Row, fg.Col);
    }
    
    if (this == txtEmail)
    {   
        if(txtEmail.value == '')
            txtEmail.value = '_@_.com.br';
    }
    
    if (this == txtSite)
    {   
        if(txtSite.value == '')
            txtSite.value = 'http://www._.com.br';
    }
    
    setLinkFoneUrl();
}

function setLinkFoneUrl()
{       
    var nTipoPessoaID = '';
    var sTipo = '';
    
    if (fg.Row>0)
    {
        nTipoPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoPessoaID'));
        sTipo = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Tipo*'));
    }
    
    if ((sTipo == 'P') || (sTipo == 'C'))
    {
        if (txtDDITelefone.value.length > 0)
            txtDDITelefone.style.backgroundColor = 'transparent';
        else    
            txtDDITelefone.style.backgroundColor = 'salmon';
            
        if (txtDDDComercial.value != '')
            txtDDDComercial.style.backgroundColor = 'transparent';
        else    
            txtDDDComercial.style.backgroundColor = 'salmon';                
            
        if (txtFoneComercial.value.length == 8)
            txtFoneComercial.style.backgroundColor = 'transparent';
        else
            txtFoneComercial.style.backgroundColor = 'salmon';            
            
        if (txtFoneComercial.value.length == 8)
            txtFoneComercial.style.backgroundColor = 'transparent';
        else    
            txtFoneComercial.style.backgroundColor = 'salmon';        
            
        if (txtDDDFaxCelular.value != '')
            txtDDDFaxCelular.style.backgroundColor = 'transparent';
        else    
            txtDDDFaxCelular.style.backgroundColor = 'salmon';                
            
        if (txtFaxCelular.value.length == 8)
            txtFaxCelular.style.backgroundColor = 'transparent';
        else    
            txtFaxCelular.style.backgroundColor = 'salmon';                
            
        if ((trimStr(txtEmail.value) != '') &&
	             (trimStr(txtEmail.value) != '_@_.com.br') && 
	             (trimStr(txtEmail.value) != '@_.com.br') && 
	             (trimStr(txtEmail.value) != '_@.com.br') &&
	             (trimStr(txtEmail.value) != '@.com.br') )    
            txtEmail.style.backgroundColor = 'transparent';
        else
            txtEmail.style.backgroundColor = 'salmon';

        if ((trimStr(txtSite.value) != '') &&
	             (trimStr(txtSite.value) != 'http://www._.com.br') && 
	             (trimStr(txtSite.value) != 'http://www_.com.br') && 
	             (trimStr(txtSite.value) != 'http://www._com.br') && 
	             (trimStr(txtSite.value) != 'http://www.com.br'))
            txtSite.style.backgroundColor = 'transparent';
        else
            txtSite.style.backgroundColor = 'salmon';
    
        if ( (txtDDDComercial.value.length > 0) && (txtFoneComercial.value.length == 8) )
        {
            txtFoneComercial.style.cursor = 'hand';    
            txtFoneComercial.style.color = 'blue';
        }
        else
        {
            txtFoneComercial.style.cursor = 'default';
            txtFoneComercial.style.color = 'black';           
        }
        
        if ( (txtDDDFaxCelular.value.length > 0) && (txtFaxCelular.value.length == 8) && (nTipoPessoaID==51))
        {
            txtFaxCelular.style.cursor = 'hand';    
            txtFaxCelular.style.color = 'blue';
        }
        else
        {
            txtFaxCelular.style.cursor = 'default';
            txtFaxCelular.style.color = 'black';   
        }
        
        if ((trimStr(txtEmail.value) != '') &&
	             (trimStr(txtEmail.value) != '_@_.com.br') && 
	             (trimStr(txtEmail.value) != '@_.com.br') && 
	             (trimStr(txtEmail.value) != '_@.com.br') &&
	             (trimStr(txtEmail.value) != '@.com.br') )
        {
            txtEmail.style.cursor = 'hand';    
            txtEmail.style.color = 'blue';
        }
        else
        {
            txtEmail.style.cursor = 'default';
            txtEmail.style.color = 'black';   
        }

        if ((trimStr(txtSite.value) != '') &&
	             (trimStr(txtSite.value) != 'http://www._.com.br') && 
	             (trimStr(txtSite.value) != 'http://www_.com.br') && 
	             (trimStr(txtSite.value) != 'http://www._com.br') && 
	             (trimStr(txtSite.value) != 'http://www.com.br'))
        {
            txtSite.style.cursor = 'hand';    
            txtSite.style.color = 'blue';
        }
        else
        {
            txtSite.style.cursor = 'default';
            txtSite.style.color = 'black';   
        }     
    }
    else
    {
        txtDDITelefone.style.backgroundColor = 'transparent';
        txtDDDComercial.style.backgroundColor = 'transparent';        
        
        txtFoneComercial.style.backgroundColor = 'transparent';
        txtFoneComercial.style.cursor = 'default';
        txtFoneComercial.style.color = 'black';           
        
        txtDDDFaxCelular.style.backgroundColor = 'transparent';
        
        txtFaxCelular.style.backgroundColor = 'transparent';
        txtFaxCelular.style.cursor = 'default';
        txtFaxCelular.style.color = 'black';           
        
        txtEmail.style.backgroundColor = 'transparent';
        txtEmail.style.cursor = 'default';
        txtEmail.style.color = 'black';   
        
        txtSite.style.backgroundColor = 'transparent';
        txtSite.style.cursor = 'default';
        txtSite.style.color = 'black';   
    }
}

/********************************************************************
Trata a visibilidade do divControlsFone dependendo dos chechboxs:
chkPessoas, chkrelacoesPessoas e chkSegurosCredito
********************************************************************/
function visibilityControls()
{
    var sStateControlsFone = 'hidden';
    var sStateAtendimento = 'hidden';
    var sFinalizarAtendimento = 'hidden';
    var sDivFg = 'inherit';
    var iHeightControlsFone = 0;    
    var iHeightAtendimento = 0;    
    var iTopControlsFone = 0;    
    var iTopAtendimento = 0;    
    var frameRect;    
    var modHeight = 0;    
    var nServico = selServico.value;
    var nAtendimento = selAtendimento.value;
                         
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    
    if (frameRect)
        modHeight = frameRect[3];
    
    //Cadastro ou Credito
    if((nServico==1) || (nServico==2))   
    {
        sStateControlsFone = 'inherit';
        iHeightControlsFone = (1.18 * (txtDDITelefone.offsetTop + txtDDITelefone.offsetHeight));
        //divFG.style.height = modHeight - parseInt(divFG.style.top, 10) - (ELEM_GAP + 6) - iHeightControlsFone - iHeightAtendimento;
        iTopControlsFone = parseInt(divFG.currentStyle.top, 10) + parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
        
    }
    // Carteira
    else if (nServico==3)
    {
        if ((nAtendimento == 281) || (glb_nA2==1))
        {
            //sStateControlsFone = 'inherit';
            sStateAtendimento = 'inherit';
            iHeightControlsFone = (1.18 * (txtDDITelefone.offsetTop + txtDDITelefone.offsetHeight));
            iHeightAtendimento = (1.18 * (txtAdiarDias.offsetTop + txtAdiarDias.offsetHeight));    
            //divFG.style.height = modHeight - parseInt(divFG.style.top, 10) - (ELEM_GAP + 6) - iHeightControlsFone - iHeightAtendimento;
            iTopControlsFone = parseInt(divFG.style.top, 10) + parseInt(divFG.style.height, 10) + ELEM_GAP;
            iTopAtendimento = iTopControlsFone + iHeightControlsFone + ELEM_GAP - 10;
        }
        else
        {
            sStateAtendimento = 'inherit';
            iHeightAtendimento = (1.18 * (txtAdiarDias.offsetTop + txtAdiarDias.offsetHeight));    
            //divFG.style.height = modHeight - parseInt(divFG.style.top, 10) - (ELEM_GAP + 6) - iHeightAtendimento;
            iTopAtendimento = parseInt(divFG.style.top, 10) + parseInt(divFG.style.height, 10) + ELEM_GAP;
        }

        if (btnAtender.state == 1)
        {
            sDivFg = 'inherit';
            sStateControlsFone = 'inherit';
            sFinalizarAtendimento = 'hidden';
            //setBtnAtenderStatus(2);
            glb_aDivControls = enableDivControls(divControls, glb_aDivControls, false);
            btnAtender.disabled = false;
            btnListaPrecos.disabled = false;
        }
        else if (btnAtender.state == 2)
        {
            sDivFg = 'hidden';
            sStateControlsFone = 'hidden';
            sFinalizarAtendimento = 'inherit';
            //setBtnAtenderStatus(2);
            glb_aDivControls = enableDivControls(divControls, glb_aDivControls, false);
            btnListaPrecos.disabled = true;
        }
        else
        {
            glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);
            btnAtender.disabled = false;
            btnListaPrecos.disabled = true;
        }
        
        btnListar.disabled = (btnAtender.state >= 1);
    }
    else
    {
        ;//divFG.style.height = modHeight - parseInt(divFG.style.top, 10) - (ELEM_GAP + 6);
    }
	
    divControlsFone.style.visibility = sStateControlsFone;
    divControlsFone.style.top = iTopControlsFone;
    divFinalizarAtendimento.style.visibility = sFinalizarAtendimento;
    divFG.style.visibility = sDivFg;
    
    //divAtendimento.style.visibility = sStateAtendimento;
    //divAtendimento.style.visibility = 'hidden';
    //divAtendimento.style.top = iTopAtendimento;
        
    fg.Redraw = 0;      
    
    //fg.style.height = parseInt(divFG.style.height, 10);
    
    drawBordersAroundTheGrid(fg);
    fg.Redraw = 2;
    uncheckRadiosByName('rdMeioAtendimento');
    uncheckRadiosByName('rdResultadoAtendimento');
    limpaTextAtendimento();    
}

function limpaTextAtendimento()
{
    txtObservacoes.value = '';
    txtAdiarDias.value = '';
    txtAdiarHoras.value = '';
    txtAgendar.value = '';
}
/*************************************************************
Limpa grid e campos dos dados de telefone e urls
**************************************************************/
function limpaGrid()
{
    glb_GravacaoPendente = false;
    
    fg.Rows = 1;
    
    secText('Atendimento', 1);
    
    txtDDITelefone.value = '';
    txtDDDComercial.value = '';
    txtFoneComercial.value = '';
    txtDDDFaxCelular.value = '';
    txtFaxCelular.value = '';
    txtEmail.value = '';
    txtSite.value = '';
    
    setBtnState();
    
    setLinkFoneUrl();                
    
    controlsFoneReadOnly();
}

function selServico_onChange()
{
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    var sState = 'hidden';
    
    lblAtendimento.style.visibility = sState;        
    selAtendimento.style.visibility = sState;            
    lblAbordagem.style.visibility = sState;        
    selAbordagem.style.visibility = sState;            
    lblPedidosPendentes.style.visibility = sState;
    chkPedidosPendentes.style.visibility = sState;
    lblSeguradora.style.visibility = sState;
    selSeguradora.style.visibility = sState;
    lblMarca.style.visibility = sState;
    selMarca.style.visibility = sState;
    lblLimiteMinimo.style.visibility = sState;
    txtLimiteMinimo.style.visibility = sState;
    lblLimiteMaximo.style.visibility = sState;
    txtLimiteMaximo.style.visibility = sState;
    lblSaldoMinimo.style.visibility = sState;
    txtSaldoMinimo.style.visibility = sState;
    lblAgendamento.style.visibility = sState;
    chkAgendamento.style.visibility = sState;
    lblDiasVencidos.style.visibility = sState;
    txtDiasVencidos.style.visibility = sState;
    lblDiasMaximo.style.visibility = sState;
    txtDiasMaximo.style.visibility = sState;
    lblDiasMinimo.style.visibility = sState;
    txtDiasMinimo.style.visibility = sState;
    
    /*lblRadio1.style.visibility = sState;
    Radio1.style.visibility = sState;
    lblRadio2.style.visibility = sState;
    Radio2.style.visibility = sState;
    lblRadio3.style.visibility = sState;
    Radio3.style.visibility = sState;
*/    
    btnWeb.style.visibility = sState;
    btnIncluir.style.visibility = sState;
    btnExcluir.style.visibility = sState;
    btnAtender.style.visibility = sState;
    btnListaPrecos.style.visibility = sState;
    
    // Eh Cadastro
    if(selServico.value == 1)
    {
    /*
        lblRadio1.style.visibility = 'inherit';        
        Radio1.style.visibility = 'inherit';        
        lblRadio2.style.visibility = 'inherit';        
        Radio2.style.visibility = 'inherit';        
        lblRadio3.style.visibility = 'inherit';        
        Radio3.style.visibility = 'inherit';        
    */
    ;    
    }
	//Eh credito
	else if(selServico.value == 2)
	{
	    lblPedidosPendentes.style.visibility = 'inherit';
        chkPedidosPendentes.style.visibility = 'inherit';
        lblSeguradora.style.visibility = 'inherit';
        selSeguradora.style.visibility = 'inherit';
        lblMarca.style.visibility = 'inherit';
        selMarca.style.visibility = 'inherit';
        lblLimiteMinimo.style.visibility = 'inherit';
        txtLimiteMinimo.style.visibility = 'inherit';
        lblLimiteMaximo.style.visibility = 'inherit';
        txtLimiteMaximo.style.visibility = 'inherit';
        lblSaldoMinimo.style.visibility = 'inherit';
        txtSaldoMinimo.style.visibility = 'inherit';
        lblDiasVencidos.style.visibility = 'inherit';
        txtDiasVencidos.style.visibility = 'inherit';
        btnIncluir.style.visibility = 'inherit';        
        btnExcluir.style.visibility = 'inherit';        
        /*
        lblRadio1.style.visibility = 'inherit';        
        Radio1.style.visibility = 'inherit';        
        lblRadio2.style.visibility = 'inherit';        
        Radio2.style.visibility = 'inherit';        
        lblRadio3.style.visibility = 'inherit';        
        Radio3.style.visibility = 'inherit';        
        */
        
        adjustElementsInForm([            
			['lblSeguradora','selSeguradora',13,2,265,-10],
			['lblMarca','selMarca',10,2,-8],
			['lblLimiteMinimo','txtLimiteMinimo',5,2,-8],
			['lblLimiteMaximo','txtLimiteMaximo',5,2,-8],
			['lblSaldoMinimo','txtSaldoMinimo',6,2,-8],
			['lblAgendamento','chkAgendamento',3,2,-40],						  
			['lblDiasVencidos','txtDiasVencidos',3,2,-7],
			['lblPedidosPendentes','chkPedidosPendentes',3,2,-7],
			['btnIncluir','btn',btn_width-55,2,-4],
			['btnExcluir','btn',btn_width-55,2]
        ], null, null, true);
	}
	//Eh carteira
	else if (selServico.value == 3)
	{
	    // Ativo
	    if(selAtendimento.value == 281)	
	    {	     
	        txtPesquisa.value = '';
	           
            chkSegurosCredito.checked = false;
    	
            lblAtendimento.style.visibility = 'inherit';
            selAtendimento.style.visibility = 'inherit';     
            lblAbordagem.style.visibility = 'inherit';
            selAbordagem.style.visibility = 'inherit';        
	        lblDiasMaximo.style.visibility = 'inherit';
            txtDiasMaximo.style.visibility = 'inherit';
            lblDiasMinimo.style.visibility = 'inherit';
            txtDiasMinimo.style.visibility = 'inherit';
            lblSaldoMinimo.style.visibility = 'inherit';
            txtSaldoMinimo.style.visibility = 'inherit';        
            btnWeb.style.visibility = 'inherit';        
            
            /*
            lblRadio1.style.visibility = 'inherit';        
            Radio1.style.visibility = 'inherit';        
            lblRadio2.style.visibility = 'inherit';        
            Radio2.style.visibility = 'inherit';        
            lblRadio3.style.visibility = 'inherit';        
            Radio3.style.visibility = 'inherit';        
            */
            adjustElementsInForm([
                ['lblAtendimento','selAtendimento',10,2,265,-10],						  
                ['lblAbordagem','selAbordagem',10,2,-5],						  
			    ['lblDiasMinimo','txtDiasMinimo',6,2,-5],
			    ['lblDiasMaximo','txtDiasMaximo',6,2,-5],					      
			    ['lblSaldoMinimo','txtSaldoMinimo',6,2,-8],
			    ['btnWeb','btn',20,2,-2]
            ], null, null, true);
        }
        // Receptivo
        else
        {
            chkSegurosCredito.checked = false;
    	    
            lblAtendimento.style.visibility = 'inherit';
            selAtendimento.style.visibility = 'inherit';
            lblAbordagem.style.visibility = 'inherit';
            selAbordagem.style.visibility = 'inherit';
	        lblDiasMaximo.style.visibility = 'inherit';
            txtDiasMaximo.style.visibility = 'inherit';
            lblDiasMinimo.style.visibility = 'inherit';
            txtDiasMinimo.style.visibility = 'inherit';
            lblSaldoMinimo.style.visibility = 'inherit';
            txtSaldoMinimo.style.visibility = 'inherit';   
            lblAgendamento.style.visibility = 'inherit';   
            chkAgendamento.style.visibility = 'inherit';   
            btnWeb.style.visibility = 'inherit';        
            
            adjustElementsInForm([
                ['lblAtendimento','selAtendimento',10,2,265,-10],						  
                ['lblAbordagem','selAbordagem',10,2,-5],						  
			    ['lblDiasMinimo','txtDiasMinimo',6,2,-5],
			    ['lblDiasMaximo','txtDiasMaximo',6,2,-5],
			    ['lblSaldoMinimo','txtSaldoMinimo',6,2,-5],
			    ['lblAgendamento','chkAgendamento',3,2,-8],
			    ['btnWeb','btn',20,2,-3]
            ], null, null, true);
        }
        
        btnAtender.style.visibility = 'inherit';
        btnListaPrecos.style.visibility = 'inherit';
	}
	    
    limpaGrid();
    setBtnState();
    visibilityControls();
    
    if ((selServico.value == 3) && (selAtendimento.value == 281))
        fillGridData();
}

function selAbordagem_onChange()
{
    txtDiasMinimo.value = selAbordagem.options[selAbordagem.selectedIndex].getAttribute("DiasMinimo", 1);
    txtDiasMaximo.value = selAbordagem.options[selAbordagem.selectedIndex].getAttribute("DiasMaximo", 1);
}    

/************************************************************
*************************************************************/
function controlsFoneReadOnly()
{
    var readOnly = true;
    
    if (fg.Rows >1)
        readOnly = false;
        
    txtDDITelefone.readOnly= readOnly;
    txtDDDComercial.readOnly = readOnly;
    txtFoneComercial.readOnly = readOnly;
    txtDDDFaxCelular.readOnly = readOnly;
    txtFaxCelular.readOnly= readOnly;
    txtEmail.readOnly = readOnly;
    txtSite.readOnly = readOnly;
}

function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        lockControlsInModalWin(false);
		return false;
	}
	return true;
}

function rdMeioAntedimentoOnclick(ctrl)
{
    eval('rd' + ctrl.registroID + '.checked=true;');
    glb_MeioAtendimentoID = ctrl.registroID;
}

function rdResultadoAtendimentoOnclick(ctrl)
{
    var nDias = 0;
    var nHoras = 0;
    var nMinutos = 0;
    var nRadioValue = ctrl.numero;
    
    eval('rd' + ctrl.registroID + '.checked=true;');
    glb_ResultadoID = ctrl.registroID;
    
    if(!isNaN(nRadioValue))
    {
        nDias = parseInt((nRadioValue/24), 10);
        nHoras = parseInt((nRadioValue - (nDias * 24)), 10);
        nMinutos = parseInt(roundNumber(((nRadioValue - (nDias * 24) - nHoras) * 60), 0), 10);
    }
                
    if (nDias > 0)
        txtAdiarDias.value = nDias;
    else
        txtAdiarDias.value = '';
    
    if (nHoras > 0)
        txtAdiarHoras.value = nHoras;
    else
        txtAdiarHoras.value = '';
                
    if (nMinutos > 0)
        txtAdiarMinutos.value = nMinutos;
    else
        txtAdiarMinutos.value = '';
        
    //uncheckRadiosByName('rdResultadoAtendimento');
    
    agendarHorario(); 
}

function finalizarAtendimento()
{
    var sAlert = '';
    var sdtAgendamento = '';    
    var sMeioAtendimentoID = glb_MeioAtendimentoID;
    var sResultadoID = glb_ResultadoID;
    var sObservacoes = trimStr(txtObservacoes.value);
    sObservacoes = replaceStr(sObservacoes, '\'', '');
    sObservacoes = replaceStr(sObservacoes, '"', '');
    var nAtendimento = selAtendimento.value;
    
    // Finalizar atendimento
    if (!verificaData(txtAgendar.value))
	    return null;

    sdtAgendamento = normalizeDate_DateTime(txtAgendar.value, true);
    sdtAgendamento = (sdtAgendamento == null ? '' : sdtAgendamento);

    if ((sMeioAtendimentoID<=0) || (sMeioAtendimentoID==null) || (sMeioAtendimentoID==''))
        sAlert += '\nSelecione o meio de atendimento';

    if ((sResultadoID<=0) || (sResultadoID==null) || (sResultadoID==''))
        sAlert += '\nSelecione o resultado do atendimento';

    if ((glb_ResultadoID != null) && (glb_ResultadoID != 0) && (glb_ResultadoID != 255))
    {
        if ((sdtAgendamento=='')||(sdtAgendamento==null))
            sAlert += '\nAgende a data do pr�ximo atendimento';

        if ((sObservacoes == '') || (sObservacoes == null) || (sObservacoes.length < 2))
            sAlert += '\nRegistre observa��es do atendimento';
    }

    if (sAlert.length > 0)
    {
        if (window.top.overflyGen.Alert(sAlert) == 0 )
		    return null;

        //txtObservacoes.focus();
        			
        return null;	
    }
    
    lockControlsInModalWin(true);
    
    nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));
    
    strPars = '?nStatus=' + escape(btnAtender.state == 0 ? '0' : '1');
    strPars += '&nMeioAtendimentoID=' + escape(sMeioAtendimentoID);
    strPars += '&sResultadoID=' + escape(sResultadoID);
    strPars += '&sObservacoes=' + escape(sObservacoes);
    strPars += '&sdtAgendamento=' + escape(sdtAgendamento);	
    strPars += '&nPesAtendimentoID=' + escape(glb_nPesAtendimentoID);

    setTimerAgendamentoStatus(false);

    try
    {
        dsoGravarAgendamento.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/gravaragendamento.aspx' + strPars;
        dsoGravarAgendamento.ondatasetcomplete = finalizarAtendimento_DSC;
        dsoGravarAgendamento.Refresh();
    }
    catch(e)
	{
	    glb_RetryWrite++;
	    
	    if (glb_RetryWrite <= 3)
        {
            glb_rewriteInterval = window.setInterval('rewriteInterval()', 300, 'JavaScript');
            return null;
        }
        
        glb_RetryWrite = 0;
	    
	    if ( window.top.overflyGen.Alert('P1 - N�o foi poss�vel gravar o atendimento, tente novamente.') == 0 )
			return null;

		lockControlsInModalWin(false);	
	}
}

function finalizarAtendimento_DSC()
{
    lockControlsInModalWin(false);
    glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);
    btnAtender.disabled = false;
    // Altera o status de Finalizando para iniciar
    setBtnAtenderStatus(0);
    visibilityControls();
    uncheckRadiosByName('rdMeioAtendimento');
    uncheckRadiosByName('rdResultadoAtendimento');
    limpaTextAtendimento();
    fillGridData();
}

function cancelarFinalizacaoAtendimento()
{
    glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);
    btnAtender.disabled = false;
    setBtnAtenderStatus(1);
    visibilityControls();
}

function enableDivControls(divCtrl, aDivControls, bEnable)
{
    selEmpresas.disabled = !bEnable;
    selEstados.disabled = !bEnable;
    selClassificacaoResultante.disabled = !bEnable;
    selClassificacaoInterna.disabled = !bEnable;
    chkPessoas.disabled = !bEnable;
    chkRelacoesPessoas.disabled = !bEnable;
    chkSegurosCredito.disabled = !bEnable;
    chkContatos.disabled = !bEnable;
    selClassificacao.disabled = !bEnable;
    selProprietario.disabled = !bEnable;
    txtPesquisa.disabled = !bEnable;
    btnListar.disabled = !bEnable;
    btnGravar.disabled = !bEnable;
    selClassificacaoExterna.disabled = !bEnable;
    selServico.disabled = !bEnable;
    selAtendimento.disabled = !bEnable;
    selAbordagem.disabled = !bEnable;
    txtDiasMinimo.disabled = !bEnable;
    txtDiasMaximo.disabled = !bEnable;
    txtLimiteMinimo.disabled = !bEnable;
    txtLimiteMaximo.disabled = !bEnable;
    txtSaldoMinimo.disabled = !bEnable;
    chkAgendamento.disabled = !bEnable;
}

function atendimentoTransferido()
{
    glb_dtAtendimentoInicio = null;
    setTimerAgendamentoStatus(false);
    finalizarAtendimento_DSC();
}

/*
Inicia/Finaliza o timer que controla o tempo de um atendimento
Parametros: bInicia: 1-Inicia, 0-Finaliza
*/
function setTimerAgendamentoStatus(bInicia)
{
    if (bInicia)
        glb_AtendimentoTimer = window.setInterval('atualizaLabelTimer()', 1000, 'JavaScript');
    else if (glb_AtendimentoTimer != null)
    {
        window.clearInterval(glb_AtendimentoTimer);
        glb_AtendimentoTimer = null;
    }
}

function atualizaLabelTimer()
{
    if (glb_dtAtendimentoInicio == null)
        secText('Atendimento', 1);
    else
    {
        var dtAtendimentoFim = new Date();
        glb_dtAtendimentoFim = dtAtendimentoFim.getTime();
        
	    var sTexto = 'Atendimento';
    	
	    if (fg.Rows == 2)
	        sTexto += ': ' + (fg.Rows - 1) + ' registro';    	    
        else if	(fg.Rows > 2)    
	        sTexto += ': ' + (fg.Rows - 1) + ' registros';    	    
	        
        sTexto = sTexto + '  [' + tempoDecorrido(glb_dtAtendimentoInicio, glb_dtAtendimentoFim) + ']';
        secText(sTexto , 1); 
    }
}

function openSiteWeb()
{
    var sUsuario = glb_nUserID;    
    var sPagina = 'vitrine.aspx';
    var nParceiroID = '';
    
    if (fg.Row > 0)
        nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));
		
	strPars = '?sUsuario=' + escape(sUsuario) +			
		'&sPagina=' + escape(sPagina) +
		'&nParceiroID=' + escape(nParceiroID) +
		'&nPessoaID=' + escape(nParceiroID);			
	
	dsoWeb.URL = SYS_ASPURLROOT + '/serversidegenEx/rashtoopensite.aspx' + strPars;
	dsoWeb.ondatasetcomplete = openSiteWeb_DCS;
	dsoWeb.Refresh();
}

function openSiteWeb_DCS()
{
    var sSite;
    
    if (!((dsoWeb.recordset.BOF) || (dsoWeb.recordset.BOF)))
    {
        if (dsoWeb.recordset['Error'].value != null)
        {
		    if ( window.top.overflyGen.Alert(dsoWeb.recordset['Erro ao abrir site. Tente mais tarde.'].value) == 0 )
		        return null;
		        
            lockControlsInModalWin(false);
            return null;
		}
		
		var sHash = dsoWeb.recordset['sHashCode'].value;
		
		if (sHash != null)
        {	            	    
            sSite = 'http://www.alcateia.com.br/alcateia/login.aspx?hc=' + sHash;                  
    
            window.open(sSite);            
		        
            lockControlsInModalWin(false);
            return null;
		}
		
    }
}