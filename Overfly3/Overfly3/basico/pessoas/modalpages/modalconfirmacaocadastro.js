/********************************************************************
modalconfirmacaocadastro.js

Library javascript para o modalconfirmacaocadastro.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_DsoToReturn = 0;
//cep j� copnsultado
var glb_CEPJaConsultado = '';
var glb_msgAtiva = false;
var glb_timerMensagem = null;
var glb_currRegistroListagem = 0;
var glb_aListIDs = new Array();
var glb_fonteDadoID;

var dsoGrid = new CDatatransport("dsoGrid");
var dsoDataFields = new CDatatransport("dsoDataFields");
var dsoClassificacaoID = new CDatatransport("dsoClassificacaoID");
var dsoFonte = new CDatatransport("dsoFonte");
var dsoAtualizaPessoa = new CDatatransport("dsoAtualizaPessoa");
var dsoAddrData = new CDatatransport("dsoAddrData");
var dsoGravaConsultaSerasa = new CDatatransport("dsoGravaConsultaSerasa");
var dsoGravaConsultaSerasaDetalhes = new CDatatransport("dsoGravaConsultaSerasaDetalhes");
var dsoDeletaConsultaSerasa = new CDatatransport("dsoDeletaConsultaSerasa");
var dsoIncluirPessoa = new CDatatransport("dsoIncluirPessoa");
var dsoEmail = new CDatatransport("dsoEmail");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
getCurrEmpresaData()
fillGridData()
fillGridData_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalconfirmacaocadastro.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

js_fg_DblClick(Row, Col)

FINAL DE DEFINIDAS NO ARQUIVO modalconfirmacaocadastro.ASP
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    
    // ajusta o body do html
    with (modalconfirmacaocadastroBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_aListIDs = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');
    
    var nRowGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'fg.Row');
    
    glb_currRegistroListagem = (nRowGrid >= 1 ? nRowGrid - 1 : 0);
    
    // configuracao inicial do html
    setupPage();
    
    with (btnCanc.style)
    {
		left = (parseInt(divFG.style.width,10) / 2) - (parseInt(btnCanc.style.width,10) / 2);
		visibility = 'hidden';
	}
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Confirma��o de cadastro', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }    
    
    // Ajusta o DivText
    adjustElementsInForm([
        ['lblPessoaID','txtPessoaID',10,1,-10,-15],
		['lblEstado','txtEstado',3,1],		
		['lblNome','txtNome',40,1],		
		['lblFantasia','txtFantasia',20,1],		
		['lblTipo','selTipo',18,1],		
		['lblDocumentoFederal','txtDocumentoFederal',16,2,-10,-6],		
		['lblDocumentoEstadual','txtDocumentoEstadual',14,2],
		['lblClassificacaoID','selClassificacaoID',19,2],
		['lblCNAE','txtCNAE',6,2],		
		['lblSexo','selSexo',10,2],
		['lblEstadoCivilID','selEstadoCivilID',10,2],
		['lblNascimento','txtNascimento',8,2],		
		['lblGrid','chkGrid',3,2],		
		['lblPais','selPais',20,3,-10,-6],
		['lblCEP','txtCEP',9,3],
		['lblUFID','selUFID',5,3],
		['lblCidade','selCidade',24,3],
		['lblBairro','txtBairro',33,3],
		['lblEndereco','txtEndereco',45,4,-10,-6],
		['lblNumero','txtNumero',6,4,-2],
		['lblComplemento','txtComplemento',22,4,-2],
		['lblObservacao','txtObservacao',20,4,-2],
		['lblDDI','txtDDI',4,5,-10,-8],
		['lblDDDTelefone','txtDDDTelefone',4,5],
		['lblTelefone','txtTelefone',8,5,-5],
		['lblDDDCelular','txtDDDCelular',4,5],
		['lblCelular','txtCelular',9,5,-5],
		['lblEmail', 'txtEmail', 31, 6, -10],
		['lblEmailNFe', 'txtEmailNfe', 31, 6],
		['lblSite','txtSite',31,6,-2],				
		['btnSerasa','btn',btnWidth-11,7,0,-3],		
		['lblConsultasValidas','chkConsultasValidas',3,7],
		['lblFonte','selFonte',22,7],
		['lblAplicaveis','chkAplicaveis',3, 7],
		['btnReceita', 'btn', btnWidth - 11, 7, 0],
		['btnSintegra', 'btn', btnWidth - 11, 7, 7],
		['lblCadastroOK', 'chkCadastroOK', 3, 7, 10],
		['btnGravar', 'btn', btnWidth - 11, 7],
		['btnFichaCadastral','btn',btnWidth+8, 7, 7],
		['btnProximo', 'btn', btnWidth - 11, 7, 7]
	], null, null, true);			
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
        
    chkGrid.onclick = chkGrid_onclick;    
    selTipo.onchange = selTipo_onchange; 
    selFonte.onchange = selFonte_onchange; 
    chkConsultasValidas.onclick = chkConsultasValidas_onclick;
    chkAplicaveis.onclick = chkAplicaveis_onclick;
    txtPessoaID.readOnly = true;
    txtEstado.readOnly = true;
    selUFID.disabled = true;
    selCidade.disabled = true;
    selCidade.onchange = selCidade_onchange; 

	txtNome.maxLength = 60;
	txtFantasia.maxLength = 40;
	txtDocumentoFederal.maxLength = 20;
	txtDocumentoEstadual.maxLength = 20;
	
	txtFaturamentoAnual.onkeypress = verifyNumericEnterNotLinked;        
	txtFaturamentoAnual.setAttribute('verifyNumPaste', 1);
	txtFaturamentoAnual.setAttribute('thePrecision', 11, 1);
	txtFaturamentoAnual.setAttribute('theScale', 2, 1);
	txtFaturamentoAnual.setAttribute('minMax', new Array(0, 99999999999), 1);
	txtFaturamentoAnual.onfocus = selFieldContent;
    txtFaturamentoAnual.maxLength = 13;    
    
    txtNumeroFuncionarios.onkeypress = verifyNumericEnterNotLinked;        
	txtNumeroFuncionarios.setAttribute('verifyNumPaste', 1);
	txtNumeroFuncionarios.setAttribute('thePrecision', 10, 1);
	txtNumeroFuncionarios.setAttribute('theScale', 0, 1);
	txtNumeroFuncionarios.setAttribute('minMax', new Array(0, 9999999999), 1);
	txtNumeroFuncionarios.onfocus = selFieldContent;
    txtNumeroFuncionarios.maxLength = 6;    
	
	txtNascimento.onkeypress = verifyDateTimeNotLinked;        
	txtNascimento.setAttribute('verifyNumPaste', 1);
	txtNascimento.setAttribute('thePrecision', 10, 1);
	txtNascimento.setAttribute('theScale', 0, 1);
	txtNascimento.setAttribute('minMax', new Array(0, 9999999999), 1);
	txtNascimento.onfocus = selFieldContent;
    txtNascimento.maxLength = 10;
    	
	txtCEP.maxLength = 10;
	txtCEP.onblur = txtCEP_onblur;
	
	txtBairro.maxLength = 60;
	txtEndereco.maxLength = 60;
	txtNumero.maxLength = 6;
	txtComplemento.maxLength = 60;
	txtObservacao.maxLength = 20;
	
	txtDDI.onkeypress = verifyNumericEnterNotLinked;
	txtDDI.onkeyup = setLinkFoneUrl;
	txtDDI.setAttribute('verifyNumPaste', 1);
	txtDDI.setAttribute('thePrecision', 10, 1);
	txtDDI.setAttribute('theScale', 0, 1);
	txtDDI.setAttribute('minMax', new Array(0, 9999), 1);
	txtDDI.onfocus = selFieldContent;
    txtDDI.maxLength = 4;    
	
	txtDDDTelefone.onkeypress = verifyNumericEnterNotLinked;        
	txtDDDTelefone.onkeyup = setLinkFoneUrl;
	txtDDDTelefone.setAttribute('verifyNumPaste', 1);
	txtDDDTelefone.setAttribute('thePrecision', 10, 1);
	txtDDDTelefone.setAttribute('theScale', 0, 1);
	txtDDDTelefone.setAttribute('minMax', new Array(0, 9999), 1);
	txtDDDTelefone.onfocus = selFieldContent;
    txtDDDTelefone.maxLength = 4;    
    
	txtTelefone.onkeypress = verifyNumericEnterNotLinked;
	txtTelefone.onkeyup = setLinkFoneUrl;
	txtTelefone.setAttribute('verifyNumPaste', 1);
	txtTelefone.setAttribute('thePrecision', 10, 1);
	txtTelefone.setAttribute('theScale', 0, 1);
	txtTelefone.setAttribute('minMax', new Array(0, 99999999), 1);
	txtTelefone.onfocus = selFieldContent;
    txtTelefone.maxLength = 8;    
    txtTelefone.ondblclick = txtTelefone_ondblclick;
	
	txtDDDCelular.onkeypress = verifyNumericEnterNotLinked;        
	txtDDDCelular.onkeyup = setLinkFoneUrl;
	txtDDDCelular.setAttribute('verifyNumPaste', 1);
	txtDDDCelular.setAttribute('thePrecision', 10, 1);
	txtDDDCelular.setAttribute('theScale', 0, 1);
	txtDDDCelular.setAttribute('minMax', new Array(0, 9999), 1);
	txtDDDCelular.onfocus = selFieldContent;
    txtDDDCelular.maxLength = 4;    
	
	txtCelular.onkeypress = verifyNumericEnterNotLinked;        
	txtCelular.onkeyup = setLinkFoneUrl;
	txtCelular.setAttribute('verifyNumPaste', 1);
	txtCelular.setAttribute('thePrecision', 10, 1);
	txtCelular.setAttribute('theScale', 0, 1);
	txtCelular.setAttribute('minMax', new Array(0, 999999999), 1);
	txtCelular.onfocus = selFieldContent;
    txtCelular.maxLength = 9;
    txtCelular.ondblclick = txtCelular_ondblclick;
    
	txtEmail.maxLength = 80;    
	txtEmail.ondblclick = txtEmail_ondblclick;
	txtEmail.onkeyup = setLinkFoneUrl;
	txtEmailNfe.maxLength = 80;
	txtEmailNfe.ondblclick = txtEmailNfe_ondblclick;
	txtEmailNfe.onkeyup = setLinkFoneUrl;
	txtSite.maxLength = 80;
	txtSite.ondblclick = txtSite_ondblclick;
	txtSite.onkeyup = setLinkFoneUrl;

	txtCNAE.maxLength = 7;
        
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    
    // ajusta o divFG
    with (divText.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;            
        height = selFonte.offsetTop + selFonte.offsetHeight;
    }
		
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divText.currentStyle.top, 10) + parseInt(divText.currentStyle.height, 10) + ELEM_GAP;        
        width = modWidth - 2 * ELEM_GAP - 4;
        height = modHeight - parseInt(divFG.style.top,10);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10) - 2 * ELEM_GAP;
    }
    
    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = 
		(getFrameInHtmlTop( 'frameSup01')).offsetTop;
		
	redimAndReposicionModalWin(modWidth, modHeight, false);

	modalFrame = getFrameInHtmlTop('frameModal');
	modalFrame.style.top = 27;
    
    configuraGrid();
    
    setAlertStatus(false);
    
    fillComboAndFields();
}

function txtTelefone_ondblclick()
{
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');;
    
    if ( (txtDDDTelefone.value == '') || (txtTelefone.value.length < 8) )
		return true;

	lockInterface(true);
	dialByModalPage(txtDDDTelefone.value, txtTelefone.value, nUserID);
	lockInterface(false);
}

function txtCelular_ondblclick()
{
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');;
    
    if ( (txtDDDCelular.value == '') || (txtCelular.value.length < 9) )
		return true;

	lockInterface(true);
	dialByModalPage(txtDDDCelular.value, txtCelular.value, nUserID);
	lockInterface(false);
}

function dialByModalPage(sDDD, sNumero, nPessoaID)
{
	sDDD = sDDD.toString();
	sNumero = sNumero.toString();
	nPessoaID = parseInt(nPessoaID, 10);
		
    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

function txtSite_ondblclick()
{
   if ((trimStr(txtSite.value) != '') &&
		 (trimStr(txtSite.value) != 'http://www._.com.br') && 
		 (trimStr(txtSite.value) != 'http://www_.com.br') && 
		 (trimStr(txtSite.value) != 'http://www._com.br') && 
		 (trimStr(txtSite.value) != 'http://www.com.br'))
        window.open(txtSite.value);
}

function txtEmail_ondblclick()
{
   if ((trimStr(txtEmail.value) != '') &&
		 (trimStr(txtEmail.value) != '_@_.com.br') && 
		 (trimStr(txtEmail.value) != '@_.com.br') && 
		 (trimStr(txtEmail.value) != '_@.com.br') &&
		 (trimStr(txtEmail.value) != '@.com.br'))
		window.open('mailto:' + txtEmail.value);
}

function txtEmailNfe_ondblclick() 
{
    if ((trimStr(txtEmailNfe.value) != '') &&
		 (trimStr(txtEmailNfe.value) != '_@_.com.br') &&
		 (trimStr(txtEmailNfe.value) != '@_.com.br') &&
		 (trimStr(txtEmailNfe.value) != '_@.com.br') &&
		 (trimStr(txtEmailNfe.value) != '@.com.br'))
        window.open('mailto:' + txtEmailNfe.value);
}

function configuraGrid()
{
    startGridInterface(fg);
    
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    
    with (fg)
    {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '8';
   		Editable = false;
		AllowUserResizing = 1;
		VirtualData = true;
		Ellipsis = 1;
		SelectionMode = 1;
		AllowSelection = false;
		Rows = 1;
		Cols = 3;
		FixedRows = 1;
		FixedCols = 1;
		ScrollBars = 3;
		OutLineBar = 5;
		GridLines = 0;
		FormatString = 'Campo' + '\t' + 'ValorID' + '\t' + 'Obr' + '\t' + 'Valor' + '\t' + ' ' + '\t' + 'ValorAnteriorID' + '\t' + 'Valor anterior' + '\t' + ' ' + '\t' + 'Nivel' + '\t' + 'CampoInterface' + '\t' + 'Mostrar' + '\t' + 'UtilizaValorID' + '\t' + 'Alterado' + '\t' + 'ValDetalheID';
		OutLineBar = 1;
        GridLinesFixed = 13;
		GridLines = 1;
		GridColor = 0X000000;		
		ColKey(0) = 'Campo';
		ColKey(1) = 'ValorID';
		ColKey(2) = 'Obrigatorio';
		ColKey(3) = 'Valor';
		ColKey(4) = 'ValorUsado';
		ColKey(5) = 'ValorAnteriorID';
		ColKey(6) = 'ValorAnterior';
		ColKey(7) = 'ValorAnteriorUsado';
		ColKey(8) = 'Nivel';
		ColKey(9) = 'CampoInterface';
		ColKey(10) = 'Mostrar';
		ColKey(11) = 'UtilizaValorID';
		ColKey(12) = 'Alterado';
		ColKey(13) = 'ValDetalheID';
		ColHidden(1) = true;
		ColHidden(5) = true;		
		ColHidden(8) = true;
		ColHidden(9) = true;
		ColHidden(10) = true;
		ColHidden(11) = true;
		ColHidden(12) = true;
		ColHidden(13) = true;
    }
    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;
    }    
    else if (controlID == 'btnCanc')
    {    
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );         
    }
    else if (controlID == 'btnSerasa')
    {    
        startconsultaSerasa();
    }
    else if (controlID == 'btnGravar')
    {    
        gravaConfirmacaoDados();
    }
    else if (controlID == 'btnFichaCadastral')
    {    
        var nPessoaID = txtPessoaID.value;
        
        if ((nPessoaID==null)||(nPessoaID==''))
        {
            if ( window.top.overflyGen.Alert('Pessoa deve estar cadastrada') == 0 )
			    return null;            
			    
            return null;                
        }
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );         
            
        window.top.openModalHTML(nPessoaID, 'Ficha Cadastral', glb_sCaller, 3, 1);
    }
    else if (controlID == 'btnReceita')
    {    
        window.open('http://www.receita.fazenda.gov.br');
    }
    else if (controlID == 'btnSintegra')
    {    
        window.open('http://www.sintegra.gov.br');
    }
    else if (controlID == 'btnProximo')
    {    
        proximoRegistro();
    }
}

function proximoRegistro()
{
    if ((glb_currRegistroListagem + 1) >= (glb_aListIDs.length))
    {
        if ( window.top.overflyGen.Alert('Fim da listagem') == 0 )
		    return null;            
    }
    else
    {
        glb_currRegistroListagem++;
        fillComboAndFields(false);
    }
}

function fillComboAndFields(bRetornoBanco)
{   
    lockControlsInModalWin(true);
    
    glb_DsoToReturn = 2;
    
    var strSQL = '';
    var nPessoaID = 0;
    
    if (glb_sCaller == 'PL')
    {
        if (glb_aListIDs.length == 0)
        {
	        showExtFrame(window, true);
	        window.focus();
        
	        if ( window.top.overflyGen.Alert('Nenhum registro na listagem') == 0 )
	            return null;
    	        
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
            return null;
        }
        else
            nPessoaID = glb_aListIDs[glb_currRegistroListagem];
    }
    else
        nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');

    strSQL = ' EXEC sp_Pessoa_ConfirmacaoCadastro ' + glb_aEmpresaData[0] + ',' + nPessoaID + ',' + DATE_SQL_PARAM;

    setConnection(dsoDataFields);
    
    dsoDataFields.SQL = strSQL;
    dsoDataFields.ondatasetcomplete = fillComboAndFields_DSC;
    dsoDataFields.Refresh();
    
    setConnection(dsoClassificacaoID);

	strSQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName, ' +
	            'CASE WHEN Filtro LIKE \'%(51)%\' THEN 51 ELSE 0 END AS Fisica, ' +
			    'CASE WHEN Filtro LIKE \'%(52)%\' THEN 52 ELSE 0 END AS Juridica ' +
                    'FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) ' +
                    'WHERE (EstadoID = 2) AND (TipoID = 13) ' +
                    'ORDER BY Ordem ';

    dsoClassificacaoID.SQL = strSQL;
    dsoClassificacaoID.ondatasetcomplete = fillComboAndFields_DSC;
    dsoClassificacaoID.Refresh();
}

function fillComboAndFields_DSC()
{
    glb_DsoToReturn--;
    
    if (glb_DsoToReturn != 0)
        return;
        
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selClassificacaoID];
    var aDSOsDynamics = [dsoClassificacaoID];

    clearComboEx(['selClassificacaoID']);
    
    for (i=0; i<1; i++)
    {
        oldDataSrc = aCmbsDynamics[i].dataSrc;
        oldDataFld = aCmbsDynamics[i].dataFld;
        aCmbsDynamics[i].dataSrc = '';
        aCmbsDynamics[i].dataFld = '';

        while (! aDSOsDynamics[i].recordset.EOF )
        {
            optionStr = aDSOsDynamics[i].recordset['fldName'].value;
        	optionValue = aDSOsDynamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDynamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].dataSrc = oldDataSrc;
        aCmbsDynamics[i].dataFld = oldDataFld;
    }   
    
    if (!((dsoDataFields.recordset.BOF) || (dsoDataFields.recordset.EOF)))
    {
        txtPessoaID.value = (dsoDataFields.recordset['PessoaID'].value == null ? '' : dsoDataFields.recordset['PessoaID'].value);
		txtEstado.value = (dsoDataFields.recordset['Estado'].value == null ? '' : dsoDataFields.recordset['Estado'].value);
		txtNome.value = (dsoDataFields.recordset['Nome'].value == null ? '' : dsoDataFields.recordset['Nome'].value);
		txtFantasia.value = (dsoDataFields.recordset['Fantasia'].value == null ? '' : dsoDataFields.recordset['Fantasia'].value);
        selTipo.value = (dsoDataFields.recordset['TipoPessoaID'].value == null ? -1 : dsoDataFields.recordset['TipoPessoaID'].value);
	    selClassificacaoID.value = (dsoDataFields.recordset['ClassificacaoID'].value == null ? -1 : dsoDataFields.recordset['ClassificacaoID'].value);
	    txtCNAE.value = (dsoDataFields.recordset['CNAE'].value == null ? '' : dsoDataFields.recordset['CNAE'].value);
	    //selTipoEmpresaID.value = (dsoDataFields.recordset['TipoEmpresaID'].value == null ? -1 : dsoDataFields.recordset['TipoEmpresaID'].value);
	    selEnquadramentoEmpresaID.value = (dsoDataFields.recordset['EnquadramentoEmpresaID'].value == null ? -1 : dsoDataFields.recordset['EnquadramentoEmpresaID'].value);
	    selRegimeTributarioID.value = (dsoDataFields.recordset['RegimeTributarioID'].value == null ? -1 : dsoDataFields.recordset['RegimeTributarioID'].value);
	    chkEhContribuinte.checked = (dsoDataFields.recordset['EhContribuinteICMS'].value == null ? 0 : dsoDataFields.recordset['EhContribuinteICMS'].value);
		txtDocumentoFederal.value = (dsoDataFields.recordset['Documento1'].value == null ? '' : dsoDataFields.recordset['Documento1'].value);
		txtDocumentoEstadual.value = (dsoDataFields.recordset['Documento2'].value == null ? '' : dsoDataFields.recordset['Documento2'].value);
		selSexo.value = (dsoDataFields.recordset['Sexo'].value == null ? '' : dsoDataFields.recordset['Sexo'].value);
		selEstadoCivilID.value = (dsoDataFields.recordset['EstadoCivilID'].value == null ? -1 : dsoDataFields.recordset['EstadoCivilID'].value);
		txtNascimento.value = (dsoDataFields.recordset['dtNascimento'].value == null ? '' : dsoDataFields.recordset['dtNascimento'].value);		
		txtFaturamentoAnual.value = (dsoDataFields.recordset['FaturamentoAnual'].value == null ? '' : dsoDataFields.recordset['FaturamentoAnual'].value);
		txtNumeroFuncionarios.value = (dsoDataFields.recordset['NumeroFuncionarios'].value == null ? '' : dsoDataFields.recordset['NumeroFuncionarios'].value);		
		selPais.value = (dsoDataFields.recordset['PaisID'].value == null ? selPais.value : dsoDataFields.recordset['PaisID'].value) ;
		txtCEP.value = (dsoDataFields.recordset['CEP'].value == null ? '' : dsoDataFields.recordset['CEP'].value);		
		txtBairro.value = (dsoDataFields.recordset['Bairro'].value == null ? '' : dsoDataFields.recordset['Bairro'].value);
		txtEndereco.value = (dsoDataFields.recordset['Endereco'].value == null ? '' : dsoDataFields.recordset['Endereco'].value);
		txtNumero.value = (dsoDataFields.recordset['Numero'].value == null ? '' : dsoDataFields.recordset['Numero'].value);
		txtComplemento.value = (dsoDataFields.recordset['Complemento'].value == null ? '' : dsoDataFields.recordset['Complemento'].value);
		txtObservacao.value = (dsoDataFields.recordset['Observacao'].value == null ? '' : dsoDataFields.recordset['Observacao'].value);
		txtEmail.value = (dsoDataFields.recordset['Email'].value == null ? '' : dsoDataFields.recordset['Email'].value);
		txtEmailNfe.value = (dsoDataFields.recordset['EmailNFe'].value == null ? '' : dsoDataFields.recordset['EmailNFe'].value);
		txtSite.value = (dsoDataFields.recordset['Site'].value == null ? '' : dsoDataFields.recordset['Site'].value);
		txtDDI.value = (dsoDataFields.recordset['TelefoneDDI'].value == null ? '' : dsoDataFields.recordset['TelefoneDDI'].value);
		txtDDDTelefone.value = (dsoDataFields.recordset['Telefone1DDD'].value == null ? '' : dsoDataFields.recordset['Telefone1DDD'].value);
		txtTelefone.value = (dsoDataFields.recordset['Telefone1Numero'].value == null ? '' : dsoDataFields.recordset['Telefone1Numero'].value);
		txtDDDCelular.value = (dsoDataFields.recordset['Telefones2DDD'].value == null ? '' : dsoDataFields.recordset['Telefones2DDD'].value);
		txtCelular.value = (dsoDataFields.recordset['Telefones2Numero'].value == null ? '' : dsoDataFields.recordset['Telefones2Numero'].value);
		chkCadastroOK.checked = (dsoDataFields.recordset['CadastroOK'].value == null ? 0 : dsoDataFields.recordset['CadastroOK'].value);
		glb_fonteDadoID = dsoDataFields.recordset['FonteDadoID'].value;
		
        clearComboEx(['selUFID', 'selCidade']);		

		var oOption = document.createElement("OPTION");
		
		oOption.text = (dsoDataFields.recordset['UF'].value == null ? '' : dsoDataFields.recordset['UF'].value); 
		oOption.value = (dsoDataFields.recordset['UFID'].value == null ? '' : dsoDataFields.recordset['UFID'].value);
		selUFID.add(oOption);
		selUFID.value = (oOption.value > 0 ? oOption.value : -1);
		
		var oOption = document.createElement("OPTION");
		oOption.text = (dsoDataFields.recordset['Cidade'].value == null ? '' : dsoDataFields.recordset['Cidade'].value);
		oOption.value = (dsoDataFields.recordset['CidadeID'].value == null ? '' : dsoDataFields.recordset['CidadeID'].value);
		selCidade.add(oOption);    
		
		selCidade.value = (oOption.value > 0 ? oOption.value : -1);
		
		var nPendentes = (dsoDataFields.recordset['RegistrosPendentes'].value == null ? '' : dsoDataFields.recordset['RegistrosPendentes'].value);
		
		if (nPendentes=='1')
		     nPendentes = ' - ' + nPendentes + ' pendencia';
        else if (nPendentes=='0')		        
            nPendentes = '';		    
        else if (nPendentes!='')		        
            nPendentes = ' - ' + nPendentes + ' pendencias';
		
		secText('Confirma��o de cadastro' + nPendentes, 1);
    }
    else
    {
	    showExtFrame(window, true);
	    window.focus();
    
	    if ( window.top.overflyGen.Alert('Nenhum cadastro pendente') == 0 )
	        return null;
	        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
        return null;
    }
    
    glb_CEPJaConsultado = txtCEP.value;
    
    fieldsTipoPessoa();
    
    fillComboFonte(txtPessoaID.value);
    fillComboClassificacao();
    fillGridData(true);
	showExtFrame(window, true);
	window.focus();
	setLinkFoneUrl();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(bLoadPage)
{
    lockControlsInModalWin(true);

    var strPars = new String();
    strPars = '?PessoaID=' + escape(txtPessoaID.value);
    strPars += '&ValidacaoDadoID=' + escape(selFonte.selectedIndex <= 0 ? 0 : selFonte.value);
    strPars += '&DATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);
    strPars += '&SohMostraveis=' + escape(chkAplicaveis.checked ? 1 : 0);
    strPars += '&Pendentes=' + escape(selFonte.value == 0 ? 1 : 0);
    strPars += '&SohValidas=' + escape(chkConsultasValidas.checked ? 1 : 0);

    dsoGrid.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/listarvalidacaodados.aspx' + strPars;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    var dTFormat = '';
    var i, j;
    var bgColorGreen, bgColorYellow, bgColorRed;    
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Rows = 1;
    
	var bgColorWhite = 0XFFFFFF; 
	var bgColorGreen = 0X90EE90;
	var bgColorYellow = 0X8CE6F0;
	var bgColorOrange = 0X60A4F4;
	var bgColorRed = 0X7280FA;
	var bgColorPurple = 0XFF8080; 

    if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
        dsoGrid.recordset.MoveFirst();
    
	if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
	{
		glb_GridIsBuilding = true;
		while (!dsoGrid.recordset.EOF)
		{
			fg.Row = fg.Rows - 1;
			
		    fg.AddItem('', fg.Row+1);
			
		    if (fg.Row < (fg.Rows-1))
			    fg.Row++;
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Campo')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Campo')) = (dsoGrid.recordset['Node'].value == null ? '' : dsoGrid.recordset['Node'].value);	
			
			fg.ColDataType(getColIndexByColKey(fg, 'ValorID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorID')) = (dsoGrid.recordset['ValorID'].value == null ? '' : dsoGrid.recordset['ValorID'].value);
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Obrigatorio')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Obrigatorio')) = (dsoGrid.recordset['Obrigatorio'].value == null ? '' : dsoGrid.recordset['Obrigatorio'].value);

		    fg.ColDataType(getColIndexByColKey(fg, 'Valor')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Valor')) = (dsoGrid.recordset['Valor'].value == null ? '' : dsoGrid.recordset['Valor'].value);
			
		    fg.ColDataType(getColIndexByColKey(fg, 'ValorUsado')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorUsado')) = (dsoGrid.recordset['ValorUsado'].value == null ? '' : dsoGrid.recordset['ValorUsado'].value);
			
			fg.ColDataType(getColIndexByColKey(fg, 'ValorAnteriorID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorAnteriorID')) = (dsoGrid.recordset['ValorAnteriorID'].value == null ? '' : dsoGrid.recordset['ValorAnteriorID'].value);
			
		    fg.ColDataType(getColIndexByColKey(fg, 'ValorAnterior')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorAnterior')) = (dsoGrid.recordset['ValorAnterior'].value == null ? '' : dsoGrid.recordset['ValorAnterior'].value);
			
		    fg.ColDataType(getColIndexByColKey(fg, 'ValorAnteriorUsado')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorAnteriorUsado')) = (dsoGrid.recordset['ValorAnteriorUsado'].value == null ? '' : dsoGrid.recordset['ValorAnteriorUsado'].value);

		    fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) = (dsoGrid.recordset['Nivel'].value == null ? '' : dsoGrid.recordset['Nivel'].value);	
		
		    fg.ColDataType(getColIndexByColKey(fg, 'CampoInterface')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CampoInterface')) = (dsoGrid.recordset['CampoInterface'].value == null ? '' : dsoGrid.recordset['CampoInterface'].value);
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Mostrar')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Mostrar')) = (dsoGrid.recordset['Mostrar'].value == null ? 0 : dsoGrid.recordset['Mostrar'].value);
			
		    fg.ColDataType(getColIndexByColKey(fg, 'UtilizaValorID')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'UtilizaValorID')) = (dsoGrid.recordset['UtilizaValorID'].value == null ? 0 : dsoGrid.recordset['UtilizaValorID'].value);
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Alterado')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Alterado')) = (dsoGrid.recordset['Alterado'].value == null ? 0 : dsoGrid.recordset['Alterado'].value);

            fg.ColDataType(getColIndexByColKey(fg, 'ValDetalheID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValDetalheID')) = (dsoGrid.recordset['ValDetalheID'].value == null ? 0 : dsoGrid.recordset['ValDetalheID'].value);
			
		    fg.IsSubTotal(fg.Row) = true;
		    fg.RowOutlineLevel(fg.Row) = dsoGrid.recordset['Nivel'].value;
            
            dsoGrid.recordset.MoveNext();
		}
	}

	for(i = 1; i < fg.Rows; i++)
	{
		var nomeCampoInterface = fg.TextMatrix(i, getColIndexByColKey(fg, 'CampoInterface'));
		
		if(nomeCampoInterface == "")
		{
			continue;
		}
		
		var valor = fg.TextMatrix(i, getColIndexByColKey(fg, 'Valor'));
		var valorAnterior = fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorAnterior'));
		var valorUsado = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorUsado'));
		var valorAnteriorUsado = fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorAnteriorUsado'));
		var valorCampoInterface = eval(nomeCampoInterface + '.value');
		
		if (nomeCampoInterface.substr((nomeCampoInterface.length - 2), 2) == 'ID')
		{
			valor = fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorID'));
			valorAnterior = fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorAnteriorID'));
		}

		var corValor;
		var corValorAnterior;
				
		// Se o campo for "selecionavel"
		// if (campoSelecionavel)
		// {
			if(((valorUsado != 0) && (valorAnteriorUsado == 0)) || ((valorUsado == 0) && (valorAnteriorUsado == 0)))
			{
				if(valor.toString().toUpperCase() == valorCampoInterface.toString().toUpperCase())
				{
					corValor = bgColorWhite;
				}
				else
				{
					corValor = bgColorYellow;
				}
				
				if(valorAnterior.toString().toUpperCase() == valor.toString().toUpperCase())
				{
					corValorAnterior = corValor;
				}
				else if(valorAnterior.toString().toUpperCase() == valorCampoInterface.toString().toUpperCase())
				{
					corValorAnterior = bgColorWhite;
				}
				else
				{
					if (corValor == bgColorWhite)
					{
						corValorAnterior = bgColorYellow;
					}
					else
					{
						corValorAnterior = bgColorRed;
					}
				}
			}
			else
			{
				if(valorAnterior.toString().toUpperCase() == valorCampoInterface.toString().toUpperCase())
				{
					corValorAnterior = bgColorWhite;
				}
				else
				{
					corValorAnterior = bgColorYellow;
				}
				
				if(valor.toString().toUpperCase() == valorAnterior.toString().toUpperCase())
				{
					corValor = corValorAnterior;
				}
				else if(valor.toString().toUpperCase() == valorCampoInterface.toString().toUpperCase())
				{
					corValor = bgColorWhite;
				}
				else
				{
					if (corValorAnterior == bgColorWhite)
					{
						corValor = bgColorYellow;
					}
					else
					{
						corValor = bgColorRed;
					}
				}
			}
			
			if (valorAnterior == '')
			{
				corValorAnterior = bgColorWhite;
			}

			fg.Cell(6, i, getColIndexByColKey(fg, 'Valor') , i, getColIndexByColKey(fg, 'Valor') ) = corValor;
			fg.Cell(6, i, getColIndexByColKey(fg, 'ValorAnterior') , i, getColIndexByColKey(fg, 'ValorAnterior') ) = corValorAnterior;
		//}
	}
	
	glb_GridIsBuilding = false;
    
    alignColsInGrid(fg,[]);    
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    lockControlsInModalWin(false);

    setFieldsStatus();

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }            
}

function setFieldsStatus()
{
    var nTipoPessoaID = 0;
		
	if (!((dsoDataFields.recordset.BOF) || (dsoDataFields.recordset.EOF)))
	    nTipoPessoaID = dsoDataFields.recordset['TipoPessoaID'].value;

    btnSerasa.disabled = (nTipoPessoaID != 52);
    selFonte.disabled = (nTipoPessoaID != 52);
    chkAplicaveis.disabled = (nTipoPessoaID != 52);
    chkConsultasValidas.disabled = (nTipoPessoaID != 52);
    btnProximo.disabled = (glb_sCaller != 'PL');
}

function js_fg_DblClick(Row, Col)
{
	var node = fg.GetNode();	
	var sCampoInterface = '';
	
    if (fg.Row>0)
    {
        sCampoInterface = (fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CampoInterface')) == null ? '' : fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CampoInterface')));    
    }
        
    if (sCampoInterface != '')
        copyRowToFields(sCampoInterface, Row, Col);        

	try
	{
		node.Expanded = !node.Expanded;
	}	
	catch(e)
	{
		;	
	}	
}

function fg_modalconfirmacaocadastro_AfterRowColChange()
{
    if ( glb_GridIsBuilding )
        return true;
}

function validDDD()
{
    var bOk = true;
    var sMsgErro = '';
    
    if (txtDDI.value == 55) 
    {
        if (txtCelular.value.length > 0 && txtDDDCelular.value.length != 2)
        {
            sMsgErro = 'DDD deve conter 2 (dois) d�gitos.';
            txtCelular.focus();
        }
        
        if (txtTelefone.value.length > 0 && txtDDDTelefone.value.length != 2)
        {
            sMsgErro = 'DDD do telefone deve conter 2 (dois) d�gitos.';
            txtTelefone.focus();
        }
        
        if (txtTelefone.value.length > 8)
        {
            sMsgErro = 'Telefone deve conter no m�ximo 8 (oito) d�gitos.';
            txtTelefone.focus();
        }

        if (txtCelular.value.length <= 9) 
        {
            if ((txtCelular.value.length > 0) &&  (txtCelular.value.length != 8) && (txtDDDCelular.value != 11)) {
                sMsgErro = 'N�mero do Celular ' + txtCelular.value + ' inv�lido.';
                txtTelefone.focus();
                
                //return bOk;
            }
            else if ((txtCelular.value.length > 0) && (txtCelular.value.length != 9) && (txtDDDCelular.value == 11)) {

            sMsgErro = 'Celular com DDD ' +txtDDDCelular.value+ ' ' + txtCelular.value + ' inv�lido.';
            txtTelefone.focus();
                //return bOk;
            }
         
        }
        else 
        {
            sMsgErro = 'Celular deve conter no m�ximo 9 (nove) d�gitos.';
            txtTelefone.focus();
        } 
 
	    if(sMsgErro!='')
	    {
	        if ( window.top.overflyGen.Alert(sMsgErro) == 0 )
	            return false;
            
            return false;
        }
        else
	        return bOk;
    }
    else 
        return bOk;
}

function validFieldsLength()
{
    var bOk = true;
	var sMsgErro = '';
	var nNomeLength = 40;
	var nFantasiaLength = 20;
	var nCNAELength = 7;
	var nEnderecoLength = 35;
	var nNumeroLength = 6;
	var nComplementoLength = 20;
	var nCEPLength = 9;
	var nBairroLength = 20;
	var nCidadeLength = 30;
	var nUFLength = 2;

	if (txtNome.value.length > nNomeLength)
	{
	    bOk = false;
	    sMsgErro = 'Tamanho m�ximo para o campo Nome � de ' + nNomeLength + ' caracteres\n';	    
	    txtEndereco.focus();
	}
	
	if (txtFantasia.value.length > nFantasiaLength)
	{
	    bOk = false;
	    sMsgErro = 'Tamanho m�ximo para o campo Fantasia � de ' + nFantasiaLength + ' caracteres\n';	    
	    txtEndereco.focus();
	}

	if (txtCNAE.value.length > nCNAELength)
	{
	    bOk = false;
	    sMsgErro = 'Tamanho m�ximo para o campo CNAE � de ' + nCNAELength + ' caracteres\n';
	    txtCNAE.focus();
	}

	if (txtEndereco.value.length > nEnderecoLength)
	{
	    bOk = false;
	    sMsgErro = 'Tamanho m�ximo para o campo Endere�o � de ' + nEnderecoLength + ' caracteres\n';	    
	    txtEndereco.focus();
	}
	if (txtNumero.value.length > nNumeroLength)
	{
	    if (sMsgErro=='')
	        txtNumero.focus();
	        
	    bOk = false;
	    sMsgErro += 'Tamanho m�ximo para o campo N�mero � de ' + nNumeroLength + ' caracteres\n';	    
	}
	if (txtComplemento.value.length > nComplementoLength)
	{
	    if (sMsgErro=='')
	        txtComplemento.focus();
	    
	    bOk = false;
	    sMsgErro += 'Tamanho m�ximo para o campo Complemento � de ' + nComplementoLength + ' caracteres\n';	    	    
	}
	if (txtCEP.value.length > nCEPLength)
	{
        if (sMsgErro=='')
            txtCEP.focus();
            
	    bOk = false;
	    sMsgErro += 'Tamanho m�ximo para o campo CEP � de ' + nCEPLength + ' caracteres\n';	    	    
	}

	if (txtBairro.value.length > nBairroLength)
	{
        if (sMsgErro=='')
            txtBairro.focus();
            
	    bOk = false;
	    sMsgErro += 'Tamanho m�ximo para o campo Bairro � de ' + nBairroLength + ' caracteres\n';	    	    
	}
	
	if(sMsgErro!='')
	{
	    if ( window.top.overflyGen.Alert(sMsgErro) == 0 )
	        return null;
    }
	
	return bOk;
}

function validaRegimeTributario() 
{
    var bOk = true;
    var sMsgErro = '';


    if (selRegimeTributarioID.value == 2920) 
    {
        if (txtFaturamentoAnual.value == '') 
        {
            bOk = false;
            sMsgErro = 'O campo Faturamento anual n�o foi preenchido.';
            txtFaturamentoAnual.focus();
        }
    }

    if (sMsgErro != '') {
        if (window.top.overflyGen.Alert(sMsgErro) == 0)
            return null;
    }

    return bOk;

}

function alertEhContribuinte() 
{
   var alert = "";

   if ((!chkEhContribuinte.checked) && (txtDocumentoEstadual.value != '')) 
   {
       alert = "Aviso: Pessoa possui Inscri��o Estadual e n�o foi classificada como contribuinte, lembre-se de validar esta informa��o no Sintegra.";

       if (window.top.overflyGen.Alert(alert) == 0)
           return null;
   }

}

function formatStr(str)
{
      var aLowerCase = new Array('DO', 'DA', 'DOS', 'DAS', 'DE', 'PARA', 'P/', 'E', 'A');
      var retVal = '';
      var i = 0;
      str = removeDiatricsOnJava(trimStr(str));
    aStr = str.split(' ');
    
    for (i=0; i<aStr.length; i++)
    {
            if (aseek(aLowerCase, aStr[i]) >= 0)
            {
                  retVal += (retVal == '' ? '' : ' ') + aStr[i].toLowerCase();
                  continue;
            }
            
            retVal += (retVal == '' ? '' : ' ') + formatWord(aStr[i]);
    }
    
    return retVal;
}

function formatWord(sWord)
{
      return sWord.substr(0, 1).toUpperCase() + sWord.substr(1, sWord.length + 1).toLowerCase();
}

function chkGrid_onclick()
{
    var btnWidth = 78;
    var modWidth = 0;
    var modHeight = 0;    
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    if (!chkGrid.checked)
    {    
        btnGravar.disabled = false;
            
        fieldsTipoPessoa();

        with (divFG.style)
        {
            top = parseInt(divText.currentStyle.top, 10) + parseInt(divText.currentStyle.height, 10) + ELEM_GAP;             
            height = modHeight - parseInt(divFG.style.top,10);
        }
        
        with (fg.style)
        {
            top = 0;         
            height = parseInt(divFG.style.height, 10) - 2 * ELEM_GAP;
        }

    }
    else
    {
        btnGravar.disabled = true;
        
        lblEstadoCivilID.style.visibility = 'hidden';
        selEstadoCivilID.style.visibility = 'hidden';
        lblFaturamentoAnual.style.visibility = 'hidden';
        txtFaturamentoAnual.style.visibility = 'hidden';
        lblNumeroFuncionarios.style.visibility = 'hidden';
        txtNumeroFuncionarios.style.visibility = 'hidden';
    
        adjustElementsInForm([
        
            ['lblPessoaID','txtPessoaID',10,1,-10,-15],
	        ['lblEstado','txtEstado',3,1],		
	        ['lblNome','txtNome',40,1],		
	        ['lblFantasia','txtFantasia',20,1],		
	        ['lblTipo','selTipo',18,1],	
	        ['btnSerasa','btn',btnWidth-11,2,-3,-6],		
	        ['lblConsultasValidas','chkConsultasValidas',3,2],
	        ['lblFonte','selFonte',22,2],
	        ['lblAplicaveis','chkAplicaveis',3,2],
		    ['btnReceita','btn',btnWidth-11,2,0],
		    ['btnSintegra','btn',btnWidth-11,2,7],
	        ['lblCadastroOK','chkCadastroOK',3,2,10],
	        ['btnGravar','btn',btnWidth-11,2],
	        ['btnFichaCadastral','btn',btnWidth+8,2,7],
	        ['btnProximo','btn',btnWidth-11,2,7],
	        ['lblGrid','chkGrid',3,2,13],
	        ['lblDocumentoFederal','txtDocumentoFederal',16,3,-10,-6],		
	        ['lblDocumentoEstadual','txtDocumentoEstadual',14,3],
	        ['lblClassificacaoID','selClassificacaoID',19,3],
	        ['lblCNAE', 'txtCNAE', 6, 3],
	        //['lblTipoEmpresaID', 'selTipoEmpresaID', 19, 3],
	        ['lblEnquadramentoEmpresaID', 'selEnquadramentoEmpresaID', 19, 3],
	        ['lblRegimeTributarioID', 'selRegimeTributarioID', 15, 3],
	        ['lblSexo','selSexo',10,3],
	        ['lblEstadoCivilID','selEstadoCivilID',10,3],
	        ['lblNascimento','txtNascimento',8,3],
	        ['lblPais','selPais',20,4,-10,-6],
	        ['lblCEP','txtCEP',9,4],
	        ['lblUFID','selUFID',5,4],
	        ['lblCidade','selCidade',24,4],
	        ['lblBairro','txtBairro',33,4],
	        ['lblEndereco','txtEndereco',45,5,-10,-6],
	        ['lblNumero','txtNumero',6,5,-2],
	        ['lblComplemento','txtComplemento',22,5,-2],
	        ['lblObservacao','txtObservacao',20,5,-2],
	        ['lblDDI','txtDDI',4,6,-10,-8],
		    ['lblDDDTelefone','txtDDDTelefone',4,6],
		    ['lblTelefone','txtTelefone',8,6,-5],
		    ['lblDDDCelular','txtDDDCelular',4,6],
		    ['lblCelular','txtCelular',9,6,-5],
		    ['lblEmail', 'txtEmail', 31, 6, -10],
		    ['lblEmailNFe', 'txtEmailNfe', 31, 6],
		    ['lblSite','txtSite',31,6,-2]		
        ], null, null, true);			
        
        with (divFG.style)
        {            
            top = txtDocumentoFederal.offsetTop + txtDocumentoFederal.offsetHeight;
            height = modHeight - parseInt(divFG.style.top,10);
        }
        
        with (fg.style)
        {
            top = 0;         
            height = parseInt(divFG.style.height, 10) - 2 * ELEM_GAP;
        }
    }
    
    //configuraGrid();
    drawBordersAroundTheGrid(fg);    
}
function fieldsTipoPessoa()
{
    var btnWidth = 78;
    
    lblCNAE.style.visibility = 'hidden';
    txtCNAE.style.visibility = 'hidden';
    //lblTipoEmpresaID.style.visibility = 'hidden';
    //selTipoEmpresaID.style.visibility = 'hidden';
    lblEnquadramentoEmpresaID.style.visibility = 'hidden';
    selEnquadramentoEmpresaID.style.visibility = 'hidden';
    lblRegimeTributarioID.style.visibility = 'hidden';
    selRegimeTributarioID.style.visibility = 'hidden';
    lblEstadoCivilID.style.visibility = 'hidden';
    selEstadoCivilID.style.visibility = 'hidden';
    lblFaturamentoAnual.style.visibility = 'hidden';
    txtFaturamentoAnual.style.visibility = 'hidden';
    lblNumeroFuncionarios.style.visibility = 'hidden';
    txtNumeroFuncionarios.style.visibility = 'hidden';
    
    if (selTipo.value == 51)
    {
        lblNascimento.innerText = 'Nascimento';
        lblSexo.innerText = 'Sexo';
        lblCelular.innerText = 'Celular';
    }
    else
    {
        lblNascimento.innerText = 'Funda��o';
        lblSexo.innerText = 'Tratam.';
        lblCelular.innerText = 'Fax';
    }

    if (!chkGrid.checked)
    {
        if (selTipo.value == 51)
        {
		        lblEstadoCivilID.style.visibility = 'inherit';
		        selEstadoCivilID.style.visibility = 'inherit';
		        lblEhContribuinte.style.visibility = 'hidden';
		        chkEhContribuinte.style.visibility = 'hidden';

                adjustElementsInForm([
                ['lblPessoaID','txtPessoaID',10,1,-10,-15],
		        ['lblEstado','txtEstado',3,1],		
		        ['lblNome','txtNome',40,1],		
		        ['lblFantasia','txtFantasia',20,1],		
		        ['lblTipo','selTipo',18,1],		
		        ['lblDocumentoFederal','txtDocumentoFederal',16,2,-10,-6],		
		        ['lblDocumentoEstadual','txtDocumentoEstadual',14,2],
		        ['lblClassificacaoID','selClassificacaoID',19,2],
		        ['lblSexo','selSexo',10,2],
		        ['lblEstadoCivilID','selEstadoCivilID',10,2],
		        ['lblNascimento','txtNascimento',8,2],		
		        ['lblGrid','chkGrid',3,2,71],		
		        ['lblPais','selPais',20,3,-10,-6],
		        ['lblCEP','txtCEP',9,3],
		        ['lblUFID','selUFID',5,3],
		        ['lblCidade','selCidade',24,3],
		        ['lblBairro','txtBairro',33,3],
		        ['lblEndereco','txtEndereco',45,4,-10,-6],
		        ['lblNumero','txtNumero',6,4,-2],
		        ['lblComplemento','txtComplemento',22,4,-2],
		        ['lblObservacao','txtObservacao',20,4,-2],
                ['lblDDI','txtDDI',4,5,-10,-8],
		        ['lblDDDTelefone','txtDDDTelefone',4,5],
		        ['lblTelefone','txtTelefone',8,5,-5],
		        ['lblDDDCelular','txtDDDCelular',4,5],
		        ['lblCelular','txtCelular',9,5,-5],
		        ['lblEmail', 'txtEmail', 31, 6, -10],
		        ['lblEmailNFe', 'txtEmailNfe', 31, 6],
		        ['lblSite','txtSite',31,6,-2],		        
		        ['btnSerasa','btn',btnWidth-11,7,-3,-3],		
		        ['lblConsultasValidas','chkConsultasValidas',3,7],
		        ['lblFonte','selFonte',22,7],
		        ['lblAplicaveis','chkAplicaveis',3,7],
		        ['btnReceita','btn',btnWidth-11,7,0],
		        ['btnSintegra','btn',btnWidth-11,7,7],
		        ['lblCadastroOK','chkCadastroOK',3,7,10],
		        ['btnGravar','btn',btnWidth-11,7],
		        ['btnFichaCadastral','btn',btnWidth+8,7,7],
		        ['btnProximo','btn',btnWidth-11,7,7]
	        ], null, null, true);			
        }
        else
        {       
                lblCNAE.style.visibility = 'inherit';
                txtCNAE.style.visibility = 'inherit';
                //lblTipoEmpresaID.style.visibility = 'inherit';
                //selTipoEmpresaID.style.visibility = 'inherit';
                lblEnquadramentoEmpresaID.style.visibility = 'inherit';
                selEnquadramentoEmpresaID.style.visibility = 'inherit';
                lblRegimeTributarioID.style.visibility = 'inherit';
                selRegimeTributarioID.style.visibility = 'inherit';
                lblEstadoCivilID.style.visibility = 'hidden';
                selEstadoCivilID.style.visibility = 'hidden';
                
		        lblFaturamentoAnual.style.visibility = 'inherit';
                txtFaturamentoAnual.style.visibility = 'inherit';
		        lblNumeroFuncionarios.style.visibility = 'inherit';
		        txtNumeroFuncionarios.style.visibility = 'inherit';
		        
                adjustElementsInForm([
                ['lblPessoaID','txtPessoaID',10,1,-10,-15],
		        ['lblEstado','txtEstado',3,1],		
		        ['lblNome','txtNome',31,1],		
		        ['lblFantasia','txtFantasia',20,1],		
		        ['lblTipo','selTipo',18,1],		
		        ['lblDocumentoFederal','txtDocumentoFederal',15,1],
		        ['lblDocumentoEstadual','txtDocumentoEstadual',15,1],
		        ['lblClassificacaoID','selClassificacaoID',18,2,-10,-6],
		        ['lblCNAE','txtCNAE',6,2,-2],
		        //['lblTipoEmpresaID', 'selTipoEmpresaID', 19, 2],
		        ['lblEnquadramentoEmpresaID', 'selEnquadramentoEmpresaID', 19, 2],
		        ['lblRegimeTributarioID', 'selRegimeTributarioID', 15, 2],
		        ['lblEhContribuinte', 'chkEhContribuinte', 3, 2, -3],
		        ['lblFaturamentoAnual','txtFaturamentoAnual',8,2, -5],
		        ['lblNumeroFuncionarios','txtNumeroFuncionarios',6,2],
		        ['lblSexo','selSexo',9,2],
		        ['lblNascimento','txtNascimento',8,2],
		        ['lblGrid','chkGrid',3,2],
		        ['lblPais','selPais',20,3,-10,-6],
		        ['lblCEP','txtCEP',9,3],
		        ['lblUFID','selUFID',5,3],
		        ['lblCidade','selCidade',24,3],
		        ['lblBairro','txtBairro',33,3],
		        ['lblEndereco','txtEndereco',45,4,-10,-6],
		        ['lblNumero','txtNumero',6,4,-2],
		        ['lblComplemento','txtComplemento',22,4,-2],
		        ['lblObservacao','txtObservacao',20,4,-2],
		        ['lblDDI','txtDDI',4,5,-10,-8],
		        ['lblDDDTelefone','txtDDDTelefone',4,5],
		        ['lblTelefone','txtTelefone',8,5,-5],
		        ['lblDDDCelular','txtDDDCelular',4,5],
		        ['lblCelular','txtCelular',9,5,-5],
		        ['lblEmail', 'txtEmail', 31, 6, -10],
		        ['lblEmailNFe', 'txtEmailNfe', 31, 6],
		        ['lblSite','txtSite',31,6,-2],				        
		        ['btnSerasa','btn',btnWidth-11,7,-3,-3],		
		        ['lblConsultasValidas','chkConsultasValidas',3,7],
		        ['lblFonte','selFonte',22,7],
		        ['lblAplicaveis','chkAplicaveis',3,7],
		        ['btnReceita','btn',btnWidth-11,7,0],
		        ['btnSintegra','btn',btnWidth-11,7,7],
		        ['lblCadastroOK','chkCadastroOK',3,7,10],
		        ['btnGravar','btn',btnWidth-11,7],
		        ['btnFichaCadastral','btn',btnWidth+8,7,7],
		        ['btnProximo','btn',btnWidth-11,7,7]
	        ], null, null, true);			
        }
    }    
}
function selTipo_onchange()
{
    fieldsTipoPessoa();
    
    fillComboClassificacao();
}

function fillComboClassificacao()
{   
    var optionStr;
	var optionValue;
	var nFisica;
	var nJuridica;
	var nTipoPessoa = selTipo.value;
	var nClassificacaoPessoa = 0;
	
	if ( !(dsoDataFields.recordset.BOF && dsoDataFields.recordset.EOF) ) 
	    nClassificacaoPessoa = dsoDataFields.recordset['ClassificacaoID'].value; 
	
	clearComboEx(['selClassificacaoID']);
	
    if ( !(dsoClassificacaoID.recordset.BOF && dsoClassificacaoID.recordset.EOF) ) 
    {
        dsoClassificacaoID.recordset.MoveFirst();
	
        while (! dsoClassificacaoID.recordset.EOF )
        {
            optionStr = dsoClassificacaoID.recordset['fldName'].value;
		    optionValue = dsoClassificacaoID.recordset['fldID'].value;
		    nFisica = dsoClassificacaoID.recordset['Fisica'].value;
		    nJuridica = dsoClassificacaoID.recordset['Juridica'].value;		

		    var oOption = document.createElement("OPTION");
    		
		    oOption.text = optionStr;
		    oOption.value = optionValue;		
    		
		    if ((nTipoPessoa == nFisica) || (nTipoPessoa == nJuridica))
		        selClassificacaoID.add(oOption);		

            dsoClassificacaoID.recordset.MoveNext();
        }
        
        if(nClassificacaoPessoa > 0)
            selClassificacaoID.value = nClassificacaoPessoa;
        else            
            selClassificacaoID.value = -1;
    }        
}

/********************************************************************
Funcao criada pelo programador.
Complementa a funcao ufAndCityFromCEP() com operacoes de servidor

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fillUFCityFromCEP()
{
	var strFind = txtCEP.value;

	if ( (strFind != null) && (strFind != '') && (glb_CEPJaConsultado != strFind))
	{
        lockInterface(true);

        glb_CEPJaConsultado = strFind;
        	
	    // trazer as cidades do servidor
        var strPars = new String();
        strPars = '?';
        strPars += 'sCep=' + escape(strFind);
        strPars += '&nPaisID=' + escape(selPais.value);
    	    
	    dsoAddrData.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/findcepex.aspx'+strPars;
	    dsoAddrData.ondatasetcomplete = dsoAddrDataComplete_DSC;
	    dsoAddrData.refresh();
	}
	
	if ((strFind == null)||(strFind == ''))
	{
	    selUFID.value = -1;
	    selCidade.value = -1;
	}    
}

/********************************************************************
Funcao criada pelo programador.
Retorno do servidor da funcao function fillUFCityFromCEP(strFind)

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoAddrDataComplete_DSC()
{
    var htmlPath = '';
    var strPars = '';
    var optionStrUF = '';
    var optionValueUF = '';
    var optionStrCidade = '';
    var optionValueCidade = '';
    
    clearComboEx(['selUFID', 'selCidade']);
    
    if ((!dsoAddrData.recordset.BOF) && (!dsoAddrData.recordset.EOF))
    {
		if (dsoAddrData.recordset.RecordCount() > 1)
		{
			if ( window.top.overflyGen.Alert ('H� mais de uma cidade para este CEP.') == 0 )
                return null;
                
            //selUFID.disabled = false;
            selCidade.disabled = false;
                
            dsoAddrData.recordset.MoveFirst();
        	
            while (! dsoAddrData.recordset.EOF )
            {
                optionStrUF = dsoAddrData.recordset['UF'].value;
	            optionValueUF = dsoAddrData.recordset['UFID'].value;
	            
	            optionStrCidade = dsoAddrData.recordset['Cidade'].value;
	            optionValueCidade = dsoAddrData.recordset['CidadeID'].value;

	            var oOptionUF = document.createElement("OPTION");
	            var oOptionCidade = document.createElement("OPTION");
        		
	            oOptionUF.text = optionStrUF;
	            oOptionUF.value = optionValueUF;		
	            
	            oOptionCidade.text = optionStrCidade;
	            oOptionCidade.value = optionValueCidade;		
        		
	            selUFID.add(oOptionUF);		
	            selCidade.add(oOptionCidade);		

                dsoAddrData.recordset.MoveNext();
            }          
            
            selUFID.selectedIndex = -1;
            selCidade.selectedIndex = -1;
            
            lockInterface(false);
            
            selCidade.focus();                  
            
            return null;
        }
		else
		{
		    dsoAddrData.recordset.MoveFirst();
		    
		    var oOption = document.createElement("OPTION");
		    oOption.text = (dsoAddrData.recordset['Cidade'].value == null ? '' : dsoAddrData.recordset['Cidade'].value);
		    oOption.value = (dsoAddrData.recordset['CidadeID'].value == null ? '' : dsoAddrData.recordset['CidadeID'].value);
			selCidade.add(oOption);

            var oOption = document.createElement("OPTION");
		    oOption.text = (dsoAddrData.recordset['UF'].value == null ? '' : dsoAddrData.recordset['UF'].value);
		    oOption.value = (dsoAddrData.recordset['UFID'].value == null ? '' : dsoAddrData.recordset['UFID'].value);		    
            selUFID.add(oOption);
            
            selUFID.selectedIndex = 0;
            selCidade.selectedIndex = 0;
            
            selCidade.disabled = true;
    
            lockInterface(false);
            
            txtBairro.focus();			
            
            return null;
		}
    }
    else
    {
        selCidade.value = '';
        selUFID.value = '';
        
        selCidade.disabled = true;
        
        if ( window.top.overflyGen.Alert ('Nenhuma cidade dispon�vel com este CEP.') == 0 )
        {            
            return null;
        }    
        
        lockInterface(false);
        
        txtCEP.focus();
        
        return null;
    }             
}

function txtCEP_onblur()
{
    fillUFCityFromCEP();
}

function selCidade_onchange()
{
    selUFID.selectedIndex = selCidade.selectedIndex;
}

function fillComboFonte(nRegistroID)
{
    if ((nRegistroID==null) || (nRegistroID == ''))
        return null;
        
    setConnection(dsoFonte);
    
	strSQL = 'SELECT \'Todas as consultas pendentes\' AS fldName, 0 AS fldID, GETDATE() AS dtConsulta, 1 AS ConsultaValida ' + 
	        'UNION ALL ' +
	        'SELECT CONVERT(VARCHAR(16), a.dtConsulta, 3) + \' \' + CONVERT(VARCHAR(5), dtConsulta, 8) + \' \' + ' +
	            'b.ItemMasculino AS fldName, a.ValidacaoDadoID AS fldID, a.dtConsulta, a.ConsultaValida ' +
                'FROM ValidacoesDados a WITH(NOLOCK) ' +
		            'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.FonteDadoID = b.ItemID ' +
                'WHERE a.RegistroID = ' + nRegistroID + ' ' +
                'ORDER BY dtConsulta DESC ';    

    dsoFonte.SQL = strSQL;
    dsoFonte.ondatasetcomplete = fillComboFonte_DSC;
    dsoFonte.Refresh();
}

function fillComboFonte_DSC()
{
    var optionStr;
	var optionValue;
	
	clearComboEx(['selFonte']);
	
    if ( !(dsoFonte.recordset.BOF && dsoFonte.recordset.EOF) ) 
    {
        dsoFonte.recordset.MoveFirst();
	
        while (! dsoFonte.recordset.EOF )
        {
            if (chkConsultasValidas.checked)
            {
                if (dsoFonte.recordset['ConsultaValida'].value != 1)
                {
                    dsoFonte.recordset.MoveNext();
                    continue;
                }
            }
            
            optionStr = dsoFonte.recordset['fldName'].value;
            
            if (dsoFonte.recordset['ConsultaValida'].value != 1)
                optionStr = optionStr + ' (inv�lida)' ;
            
		    optionValue = dsoFonte.recordset['fldID'].value;

		    var oOption = document.createElement("OPTION");
    		
		    oOption.text = optionStr;
		    oOption.value = optionValue;		

            selFonte.add(oOption);		

            dsoFonte.recordset.MoveNext();
        }
        
        dsoFonte.selectedIndex = 0;
        
    }        
}

function chkAplicaveis_onclick()
{
    fillGridData();
}

function chkConsultasValidas_onclick()
{
    fillComboFonte_DSC();
    fillGridData();
}

/***********************************************
Faz consulta no Serasa
***********************************************/
function startconsultaSerasa()
{
    var nTipoPessoaID = dsoDataFields.recordset['TipoPessoaID'].value;
    
    if (nTipoPessoaID == 51)
    {
        if ( window.top.overflyGen.Alert('Consulta n�o liberada para pessoa f�sica') == 0 )
            return null;

        return null;
    }

    setAlertStatus(true);
    var nRegistroID = txtPessoaID.value;
    var sDocumento = trimStr(txtDocumentoFederal.value);    
    var nTipoPessoaID = selTipo.value;
        
    if ((nRegistroID == null) || (nRegistroID == 0))
    {
        if ( window.top.overflyGen.Alert('Pessoa deve estar cadastrada') == 0 )
			    return null;
    			
        return null;
    }
        
    if ((sDocumento == null) || (sDocumento == ''))
    {
        if ( window.top.overflyGen.Alert('Digite um documento federal') == 0 )
			return null;
			
        return null;
    }
    
    if (!((nTipoPessoaID==51) || (nTipoPessoaID==52)))
    {
        if ( window.top.overflyGen.Alert('Pessoa inv�lida para consulta') == 0 )
		        return null;
        		
            return null;
    }

    lockControlsInModalWin(true);
    
    consultaSerasa(nRegistroID, 295, nTipoPessoaID, sDocumento);
}    

function consultaSerasaEnded(bSucesso)
{
    var nRegistroID = txtPessoaID.value;     
    
    fillComboAndFields();
    setAlertStatus(false);
    
    if (bSucesso)
        chkCadastroOK.checked = true;
}

function selFonte_onchange()
{
    fillGridData();
}

function copyRowToFields(sCampoInterface, Row, Col)
{
    var sValor = '';
    var sValorID = -1;
    
    var bUtilizaValorID = fg.ValueMatrix(Row, getColIndexByColKey(fg, 'UtilizaValorID'));
    
    if ((Col == getColIndexByColKey(fg, 'Valor')) || (Col == getColIndexByColKey(fg, 'ValorUsado')))
    {
        sValor = fg.TextMatrix(Row, getColIndexByColKey(fg, 'Valor'));
        sValorID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorID'));
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorUsado')) = 1;
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorAnteriorUsado')) = 0;
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'Alterado')) = 1;
    }
    
    if ((Col == getColIndexByColKey(fg, 'ValorAnterior')) || (Col == getColIndexByColKey(fg, 'ValorAnteriorUsado')))
    {
        sValor = fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorAnterior'));
        sValorID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorAnteriorID'));
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorUsado')) = 0;
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'ValorAnteriorUsado')) = 1;
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'Alterado')) = 1;
    }
    
    if (bUtilizaValorID!=0) 
        selOptByValueInSelect(getHtmlId(), sCampoInterface, sValorID);
    else            
        eval(sCampoInterface + '.value=\'' + sValor + '\'');
        
    if (sCampoInterface=='txtCEP')        
    {   
        glb_CEPJaConsultado = '';
        fillUFCityFromCEP();           
    }
}

function gravaConfirmacaoDados()
{
    if(!validDDD())
        return null;
        
    if (!validFieldsLength())
        return null;

    if (!validaRegimeTributario())
        return null;

    if (selTipo.value != 51)
    {
        alertEhContribuinte();
    }

    var strPars = new String();
    var sNome = txtNome.value;
    var sFantasia = txtFantasia.value;
    var nTipoPessoaID = selTipo.value;
    var nClassificacaoID = selClassificacaoID.value;
    var sCNAE = txtCNAE.value;
    //var nTipoEmpresaID = selTipoEmpresaID.value;
    var nEnquadramentoEmpresaID = selEnquadramentoEmpresaID.value;
    var nRegimeTributarioID = selRegimeTributarioID.value;
    var nEhContribuinte;
    var sDocumentoFederal = txtDocumentoFederal.value;
    var sDocumentoEstadual = txtDocumentoEstadual.value;
    var nFaturamentoAnual = txtFaturamentoAnual.value;
    var nNumeroFuncionarios = txtNumeroFuncionarios.value;
    var nSexo = selSexo.value;
    var dDtNascimento = ( trimStr(txtNascimento.value) == '' ? '' : dateFormatToSearch(trimStr(txtNascimento.value)) );
    var nPaisID = selPais.value;
    var sCEP = txtCEP.value;
    var nUFID = selUFID.value;
    var nCidadeID = selCidade.value;
    var sBairro = txtBairro.value;
    var sEndereco = txtEndereco.value;
    var sNumero = txtNumero.value;
    var sComplemento = txtComplemento.value;
    var sObservacao = txtObservacao.value;
    var sDDITelefone = txtDDI.value;
    var sDDDTelefone1 = txtDDDTelefone.value;
    var sNumeroTelefone1 = txtTelefone.value;
    var sDDDTelefone2 = txtDDDCelular.value;
    var sNumeroTelefone2 = txtCelular.value;
    var sEmail = txtEmail.value;
    var sEmailNFe = txtEmailNfe.value;
    var sFonteDadoID = glb_fonteDadoID;
    var sSite = txtSite.value;
    var nEstadoCivilID = selEstadoCivilID.value;
    var nUsuarioID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    var nPessoaID = txtPessoaID.value;

    if (chkEhContribuinte.checked) 
    {
        nEhContribuinte = 1; 
    }
    else 
    {
        nEhContribuinte = 0; 
    }
    
    strPars = '?sNome='+escape(sNome);
    strPars += '&sFantasia=' + escape(sFantasia);    
    strPars += '&nTipoPessoaID=' + escape(nTipoPessoaID);
    strPars += '&nClassificacaoID=' + escape(nClassificacaoID);
    strPars += '&sCNAE=' + escape(sCNAE);
    //strPars += '&nTipoEmpresaID=' + escape(nTipoEmpresaID);
    strPars += '&nEnquadramentoEmpresaID=' + escape(nEnquadramentoEmpresaID);
    strPars += '&nRegimeTributarioID=' + escape(nRegimeTributarioID);
    strPars += '&nEhContribuinte=' + escape(nEhContribuinte);
    strPars += '&sDocumentoFederal=' + escape(sDocumentoFederal);
    strPars += '&sDocumentoEstadual=' + escape(sDocumentoEstadual);
    strPars += '&nFaturamentoAnual=' + escape(nFaturamentoAnual);
    strPars += '&nNumeroFuncionarios=' + escape(nNumeroFuncionarios);    
    strPars += '&nSexo=' + escape(nSexo);
    strPars += '&dDtNascimento=' + escape(dDtNascimento);
    strPars += '&nPaisID=' + escape(nPaisID);
    strPars += '&sCEP=' + escape(sCEP);
    strPars += '&nUFID=' + escape(nUFID);    
    strPars += '&nCidadeID=' + escape(nCidadeID);
    strPars += '&sBairro=' + escape(sBairro);
    strPars += '&sEndereco=' + escape(sEndereco);
    strPars += '&sNumero=' + escape(sNumero);
    strPars += '&sComplemento=' + escape(sComplemento);
    strPars += '&sObservacao=' + escape(sObservacao);
    strPars += '&sDDITelefone=' + escape(sDDITelefone);    
    strPars += '&sDDDTelefone1=' + escape(sDDDTelefone1);
    strPars += '&sNumeroTelefone1=' + escape(sNumeroTelefone1);
    strPars += '&sDDDTelefone2=' + escape(sDDDTelefone2);
    strPars += '&sNumeroTelefone2=' + escape(sNumeroTelefone2);
    strPars += '&sEmail=' + escape(sEmail);
    strPars += '&sEmailNFe=' + escape(sEmailNFe);
    strPars += '&sFonteDadoID=' + escape(sFonteDadoID);
    strPars += '&sSite=' + escape(sSite);
    strPars += '&nEstadoCivilID=' + escape(nEstadoCivilID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);    
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&bCadastroOK=' + escape(chkCadastroOK.checked ? 1 : 0);
    
    var i=0;
    var sValorAlterado = 0; //0-O campo Valor foi usado, 1-O campo Valor Anterior foi usado
    var nQtdAlterados = 0;

    for (i=1; i<fg.Rows; i++)
    {
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Alterado')) == 1)
        {
            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorUsado')) == '1')
                sValorAlterado = '0';
            else if (fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorAnteriorUsado')) == '1')
                sValorAlterado = '1';
            else
                continue;

            nQtdAlterados++;
            strPars += '&ValDetalheID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'ValDetalheID')));
            strPars += '&ValorAlterado=' + escape(sValorAlterado);
        }
    }

    dsoIncluirPessoa.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/atualizarpessoa.aspx' + strPars;
    dsoIncluirPessoa.ondatasetcomplete = gravaConfirmacaoDados_DSC;
    dsoIncluirPessoa.refresh();
}

function gravaConfirmacaoDados_DSC()
{
    if (!((dsoIncluirPessoa.recordset.BOF) || (dsoIncluirPessoa.recordset.BOF)))
    {
        if (dsoIncluirPessoa.recordset['Error'].value != null && dsoIncluirPessoa.recordset['Error'].value.length > 0)
        {
            if (window.top.overflyGen.Alert(new String(dsoIncluirPessoa.recordset['Error'].value).replace("\nThe transaction ended in the trigger. The batch has been aborted.", "")) == 0)
		        return null;
		        
            lockControlsInModalWin(false);
            return null;
		}
    }

    if ((chkCadastroOK.checked) && (txtEmail.value.length > 5))
    {
        var _retMsg = window.top.overflyGen.Confirm(' Enviar e-mail de confirma��o de cadastro?');    
        
        if (_retMsg == 1 )
            enviarEmail();
        else
            fillComboAndFields(true);
    }
    else
        fillComboAndFields(true);
}

function setAlertStatus(bLiga)
{
    glb_msgAtiva = bLiga;
    
    if (!bLiga)
        window.status = '';
	else
	    window.status = 'Por favor aguarde, consulta em andamento...';
}

function enviarEmail()
{
	lockControlsInModalWin(true);
	
	var strPars = new String();
	var nTipoEmail = 1;
	
    var strPars = new String();
    var empresa = getCurrEmpresaData();
    var nPessoaID = txtPessoaID.value;
    var nEmpresaID = escape(empresa[0].toString());
    var nUserID = escape(getCurrUserID());

    strPars = '?nPessoaID=' + nPessoaID;
    strPars += '&nTipoEmail=' + escape(nTipoEmail);
    strPars += '&nEmpresaID=' + nEmpresaID;
    strPars += '&nUserID=' + nUserID;

    dsoEmail.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/emailconfirmacaocadastro.aspx' + strPars;
    dsoEmail.ondatasetcomplete = gravacao_DSC;
    dsoEmail.refresh();
}

function gravacao_DSC()
{
	var emailRS = dsoEmail.recordset;
	
    if(!(emailRS.BOF && emailRS.EOF)) {
		if((emailRS['Msg'] != null) && (emailRS['Msg'].value != null) && (emailRS['Msg'].value != "")) {
			if(window.top.overflyGen.Alert(emailRS['Msg'].value) == 0)
				return null;
		}
	}
    
	fillComboAndFields(true);
}

function setLinkFoneUrl()
{
    if (txtDDI.value.length > 0)
        txtDDI.style.backgroundColor = 'transparent';
    else    
        txtDDI.style.backgroundColor = 'salmon';
        
    if (txtDDDTelefone.value != '')
        txtDDDTelefone.style.backgroundColor = 'transparent';
    else    
        txtDDDTelefone.style.backgroundColor = 'salmon';                
        
    if (txtTelefone.value.length == 8)
        txtTelefone.style.backgroundColor = 'transparent';
    else    
        txtTelefone.style.backgroundColor = 'salmon';        
        
    if (txtDDDCelular.value != '')
        txtDDDCelular.style.backgroundColor = 'transparent';
    else    
        txtDDDCelular.style.backgroundColor = 'salmon';                
        
    if ((txtCelular.value.length == 9)||(txtCelular.value.length == 8))
        txtCelular.style.backgroundColor = 'transparent';
    else    
        txtCelular.style.backgroundColor = 'salmon';                
        
    if ((trimStr(txtEmail.value) != '') &&
        (trimStr(txtEmail.value) != '_@_.com.br') && 
        (trimStr(txtEmail.value) != '@_.com.br') && 
        (trimStr(txtEmail.value) != '_@.com.br') &&
        (trimStr(txtEmail.value) != '@.com.br') )
        txtEmail.style.backgroundColor = 'transparent';
    else
        txtEmail.style.backgroundColor = 'salmon';

    if ((trimStr(txtEmailNfe.value) != '') &&
        (trimStr(txtEmailNfe.value) != '_@_.com.br') &&
        (trimStr(txtEmailNfe.value) != '@_.com.br') &&
        (trimStr(txtEmailNfe.value) != '_@.com.br') &&
        (trimStr(txtEmailNfe.value) != '@.com.br'))
        txtEmailNfe.style.backgroundColor = 'transparent';
    else
        txtEmailNfe.style.backgroundColor = 'salmon';

    if ((trimStr(txtSite.value) != '') &&
             (trimStr(txtSite.value) != 'http://www._.com.br') && 
             (trimStr(txtSite.value) != 'http://www_.com.br') && 
             (trimStr(txtSite.value) != 'http://www._com.br') && 
             (trimStr(txtSite.value) != 'http://www.com.br'))
        txtSite.style.backgroundColor = 'transparent';
    else
        txtSite.style.backgroundColor = 'salmon';

    if ( (txtDDDTelefone.value.length > 0) && (txtTelefone.value.length == 8) )
    {
        txtTelefone.style.cursor = 'hand';    
        txtTelefone.style.color = 'blue';
    }
    else
    {
        txtTelefone.style.cursor = 'default';
        txtTelefone.style.color = 'black';           
    }
    
    if ((txtDDDCelular.value.length > 0) && (txtCelular.value.length == 9))
    {
        txtCelular.style.cursor = 'hand';    
        txtCelular.style.color = 'blue';
    }
    else
    {
        txtCelular.style.cursor = 'default';
        txtCelular.style.color = 'black';   
    }
    
    if ((trimStr(txtEmail.value) != '') &&
             (trimStr(txtEmail.value) != '_@_.com.br') && 
             (trimStr(txtEmail.value) != '@_.com.br') && 
             (trimStr(txtEmail.value) != '_@.com.br') &&
             (trimStr(txtEmail.value) != '@.com.br') )
    {
        txtEmail.style.cursor = 'hand';    
        txtEmail.style.color = 'blue';
    }
    else
    {
        txtEmail.style.cursor = 'default';
        txtEmail.style.color = 'black';
    }

    if ((trimStr(txtEmailNfe.value) != '') &&
             (trimStr(txtEmailNfe.value) != '_@_.com.br') &&
             (trimStr(txtEmailNfe.value) != '@_.com.br') &&
             (trimStr(txtEmailNfe.value) != '_@.com.br') &&
             (trimStr(txtEmailNfe.value) != '@.com.br')) {
        txtEmailNfe.style.cursor = 'hand';
        txtEmailNfe.style.color = 'blue';
    }
    else 
    {
        txtEmailNfe.style.cursor = 'default';
        txtEmailNfe.style.color = 'black';
    }

    if ((trimStr(txtSite.value) != '') &&
             (trimStr(txtSite.value) != 'http://www._.com.br') && 
             (trimStr(txtSite.value) != 'http://www_.com.br') && 
             (trimStr(txtSite.value) != 'http://www._com.br') && 
             (trimStr(txtSite.value) != 'http://www.com.br'))
    {
        txtSite.style.cursor = 'hand';    
        txtSite.style.color = 'blue';
    }
    else
    {
        txtSite.style.cursor = 'default';
        txtSite.style.color = 'black';   
    }     
}
