
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalenderecoHtml" name="modalenderecoHtml">

<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">
<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/pessoas/modalpages/modalendereco.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/pessoas/modalpages/modalendereco.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'1. Captura o id do Pais, o numero do CEP, o id do Estado e o id da Cidade
Dim i, sPaisID, varTemp
Dim sCaller
sCaller = "I"

For i = 1 To Request.QueryString("sPaisID").Count    
    varTemp = Request.QueryString("sPaisID")(i)
    sPaisID = varTemp
Next

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_currPaisID = " & CStr(varTemp) & ";"
Response.Write vbcrlf

Response.Write "var glb_sCaller = '" & sCaller & "';"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nCEP").Count    
    varTemp = Request.QueryString("nCEP")(i)
Next

If (Not IsNumeric(CStr(varTemp))) Then
	Response.Write "var glb_currCEP = '" & CStr(varTemp) & "';"
Else
	Response.Write "var glb_currCEP = " & CStr(varTemp) & ";"
End If

Response.Write vbcrlf

For i = 1 To Request.QueryString("sEstadoID").Count    
    varTemp = Request.QueryString("sEstadoID")(i)
Next

Response.Write "var glb_currEstadoID = " & CStr(varTemp) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("sCidadeID").Count    
    varTemp = Request.QueryString("sCidadeID")(i)
Next

Response.Write "var glb_currCidadeID = " & CStr(varTemp) & ";"
Response.Write vbcrlf

'2. Monta array dos estados, com seus ids, nomes e siglas
Response.Write "// Array dos estados, com seus ids, nomes e siglas"
Response.Write vbcrlf

'SELECT LocalidadeID, Localidade, CodigoLocalidade2 FROM Localidades
'WHERE EstadoID = 2 AND TipoLocalidadeID = 204 AND LocalizacaoID = 44
'ORDER BY Localidade

Dim rsData, strSQL

strSQL = "SELECT LocalidadeID, Localidade, CodigoLocalidade2 FROM Localidades WITH(NOLOCK) " & _
         "WHERE EstadoID = 2 AND TipoLocalidadeID = 204 AND " & _
         "LocalizacaoID = " & CStr(sPaisID) & " " & _
         "ORDER BY Localidade"

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_arrayEstado = new Array();"
Response.Write vbcrlf

i = 0
While Not rsData.EOF
    
    'glb_arrayEstado[i] = new Array(12, 'Bahia', 'BA');
    
    Response.Write "glb_arrayEstado[" & CStr(i) & "] = new Array("
    Response.Write CStr(rsData.Fields("LocalidadeID").Value)
    Response.Write ","
    Response.Write Chr(34)
    Response.Write CStr(rsData.Fields("Localidade").Value)
    Response.Write Chr(34)
    Response.Write ","
    Response.Write Chr(34)
    Response.Write CStr(rsData.Fields("CodigoLocalidade2").Value)
    Response.Write Chr(34)
    Response.Write ");"
    rsData.MoveNext
    i = i + 1
Wend

rsData.Close
Set rsData = Nothing

Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
fg_DblClick()
//-->
</SCRIPT>
</head>

<body id="modalenderecoBody" name="modalenderecoBody" LANGUAGE="javascript" onload="return window_onload()">
     <div id="divUF" name="divUF" class="divGeneral">
        <p id="lblUF" name="lblUF" class="lblGeneral">UF</p>
        <select id="selUF" name="selUF" class="fldGeneral" LANGUAGE=javascript onchange="return selUF_onchange()"></select>
    </div>    

     <div id="divCidade" name="divCidade" class="divGeneral">
        <p id="lblCidade" name="lblCidade" class="lblGeneral">Cidade</p>
        <input type="text" id="txtCidade" name="txtCidade" class="fldGeneral" LANGUAGE="javascript" onkeyup="return txtCidade_ondigit(this)" onfocus="return __selFieldContent(this)">
        <input type="button" id="btnFindPesquisa" name="btnFindPesquisa" value="Listar" LANGUAGE="javascript" onclick="return btnFindPesquisa_onclick(this)" class="btns">
    </div>    

     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
</body>

</html>
