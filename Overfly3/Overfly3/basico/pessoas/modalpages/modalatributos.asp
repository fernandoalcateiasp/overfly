
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalatributosHtml" name="modalatributosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
      
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/pessoas/modalpages/modalatributos.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, nContextoID, nUserID

sCaller = ""
nEmpresaID = 0
nContextoID = 0
nUserID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

'nUserID
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

'nEmpresaID
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " &  nEmpresaID & ";"
Response.Write vbcrlf

'Necessario para compatibilidade da automacao
Response.Write "var glb_nUserID = " & nUserID & ";"

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>
<SCRIPT LANGUAGE=javascript FOR=fgEntidades EVENT=AfterRowColChange>
<!--
	js_fg_AfterRowColChange(fgEntidades, arguments[0], arguments[1], arguments[2], arguments[3]);
	js_fg_AfterRowColmodalatributos (fgEntidades, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fgEntidades EVENT=DblClick>
<!--
js_fg_DblClick (fgEntidades, fgEntidades.Row, fgEntidades.Col);
//-->
</SCRIPT>

</head>

<body id="modalatributosBody" name="modalatributosBody" LANGUAGE="javascript" onload="return window_onload()">
   

	
    <div id="divLinha1" name="divLinha1" class="divGeneral">
        <p id="lblAtributo" name="lblAtributo" class="lblGeneral">Atributo</p>
		<select id="selAtributo" name="selAtributo" class="fldGeneral"  onchange="return cmbOnChange(this)">
		<%        
            Dim strSQL, rsData
	        Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName " & _
			         "UNION ALL SELECT 1 AS Indice, ItemID AS fldID, ItemMasculino AS fldName " & _
			         "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			         "WHERE TipoID = 56 AND EstadoID = 2 AND Filtro LIKE '%<20100>%'" & _
			         "ORDER BY fldID"

	        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	        If (Not (rsData.BOF AND rsData.EOF) ) Then

		        While Not (rsData.EOF)

			        Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			        Response.Write( rsData.Fields("fldName").Value & "</option>" )

			        rsData.MoveNext()

		        WEnd

	        End If

	        rsData.Close
	        Set rsData = Nothing
        %>        
		</select>
		<p id="lblTipoPessoa" name="lblTipoPessoa" class="lblGeneral">Tipo Pessoa</p>
		<select id="selTipoPessoa" name="selTipoPessoa" class="fldGeneral"  onchange="return cmbOnChange(this)">
		<%
	        Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName " & _
			         "UNION ALL SELECT 1 AS Indice, ItemID AS fldID, ItemMasculino AS fldName " & _
			         "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			         "WHERE TipoID = 12 AND EstadoID = 2 AND Filtro LIKE '%(1211)%' " & _
			         "ORDER BY fldID"

	        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	        If (Not (rsData.BOF AND rsData.EOF) ) Then

		        While Not (rsData.EOF)

			        Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'" )
			        
			        If (CLng(rsData.Fields("fldID").Value = "52")) Then
                        Response.Write(" SELECTED ")
                    End If

			        Response.Write(">" & rsData.Fields("fldName").Value & "</option>" )

			        rsData.MoveNext()

		        WEnd

	        End If

	        rsData.Close
	        Set rsData = Nothing
        %>        
        </select>		

        <p id="lblClassificacao" name="lblClassificacao" class="lblGeneral">Classificação</p>
        <select id="selClassificacao" name="selClassificacao" class="fldGeneral" MULTIPLE>
        </select>

        <p id="lblChavePesquisa" name="lblChavePesquisa" class="lblGeneral">Chave Pesquisa</p>
        <select id="selChavePesquisa" name="selChavePesquisa" class="fldGeneral"  onchange="return cmbOnChange(this)">  
            <option id="NID" name="NID" value="1">ID</option>
            <option id="VNome" name="VNome" value="2">Nome</option>
            <option id="VFantasia" name="VFantasia" value="3">Fantasia</option>
            <option id="NDocFederal" name="NDocFederal" value="4">Doc Federal</option>
        </select>
        <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Pesquisa</p>
        <input type="text" id="txtPesquisa" name="txtPesquisa" class="fldGeneral" onkeyup="return txtPesquisa_ondigit(this)" LANGUAGE="javascript">
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral" LANGUAGE="javascript">    
    </div>   
    <div id="divLinha2" name="divLinha2" class="divGeneral">
        <p id="lblRelacao" name="lblRelacao" class="lblGeneral">Relação</p>
		<select id="selRelacao" name="selRelacao" class="fldGeneral"  onchange="return cmbOnChange(this)">
        <%
	        Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT TipoRelacaoID AS fldID, TipoRelacao AS fldName" & _            
                     " FROM    TiposRelacoes a WITH ( NOLOCK )" & _
                     " WHERE   a.RelacaoEntreID = 133 " & _
                     " AND a.EstadoID = 2  AND EXISTS ( SELECT * FROM   dbo.TiposAuxiliares_Itens b WITH(NOLOCK) WHERE  b.TipoID = 56 " & _
                     " AND b.Filtro LIKE '%('+ CONVERT(VARCHAR, a.TipoRelacaoID) + ')%' )"       
                     
	        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	        If (Not (rsData.BOF AND rsData.EOF) ) Then

		        While Not (rsData.EOF)

			        Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			        Response.Write( rsData.Fields("fldName").Value & "</option>" )
			        rsData.MoveNext()

		        WEnd

	        End If

	        rsData.Close
	        Set rsData = Nothing
        %>        
        </select>        
        <p id="lblPapelEmpresa" name="lblPapelEmpresa" class="lblGeneral">Papel Empresa</p>
		<select id="selPapelEmpresa" name="selPapelEmpresa" class="fldGeneral"  onchange="return cmbOnChange(this)">
        </select>
        <p id="lblCliForDe" name="lblCliForDe" class="lblGeneral">Clientes/Fornecedores De:</p>
        <select id="selCliForDe" name="selCliForDe" class="fldGeneral" MULTIPLE>
       	<%
            Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "DECLARE @SQL VARCHAR(MAX), @Empresas VARCHAR(40) = dbo.fn_Fopag_Empresas(NULL,NULL,0),  " & _
		             "         @EmpresasSelecionadas VARCHAR(40) = dbo.fn_Fopag_Empresas(2,2010,1) " & _
                     "SET @SQL = 'SELECT 1 AS Indice, PessoaID AS fldID, Fantasia AS fldName , ' + " & _
                     "           '(CASE WHEN PessoaID IN (' + @EmpresasSelecionadas + ') THEN 1 ELSE 0 END) AS Selected ' + " & _
			         "           'FROM Pessoas WITH(NOLOCK) ' + " & _
			         "           'WHERE ClassificacaoID = 56 AND EstadoID = 2 AND PessoaID <> 20 AND PessoaID IN (' + @Empresas + ') ' + " & _
			         "           'ORDER BY fldID ' " & _
                     "EXEC (@SQL)"

	        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	        If (Not (rsData.BOF AND rsData.EOF) ) Then

		        While Not (rsData.EOF)

			        Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'" )
			        
                    If (CLng(rsData.Fields("Selected").Value = "1")) Then
                        Response.Write(" SELECTED ")
                    End If

                    Response.Write(">" &  rsData.Fields("fldName").Value & "</option>" )

			        rsData.MoveNext()

		        WEnd

	        End If

	        rsData.Close
	        Set rsData = Nothing
        %>     
       
       
        </select>
        <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    </div> 
    <div id="divLinha3" name="divLinha3" class="divGeneral">
        <p id="lblAtributo2" name="lblAtributo2" class="lblGeneral">Atributo</p>
		<select id="selAtributo2" name="selAtributo2" class="fldGeneral"  onchange="return cmbOnChange(this)">
        <%        
	        Dim nLinhas
	        Set rsData = Server.CreateObject("ADODB.Recordset")

            strSQL = "SELECT 0 AS Indice, 0 AS fldID, SPACE(0) AS fldName " & _
			         "UNION ALL SELECT 1 AS Indice, ItemID AS fldID, ItemMasculino AS fldName " & _
			         "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
			         "WHERE TipoID = 56 AND EstadoID = 2 AND Filtro LIKE '%<20100>%' " & _
			         "ORDER BY fldID"

	        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	        If (Not (rsData.BOF AND rsData.EOF) ) Then
                
                nLinhas = 0
                While Not (rsData.EOF)
                    nLinhas = nLinhas + 1
                    rsData.MoveNext()
                WEnd
                
                rsData.MoveFirst
		        
		        While Not (rsData.EOF)

			        Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'" )
			        
			        If (nLinhas = 2) Then
                        Response.Write(" SELECTED ")
			        End If
			        
			        Response.Write( ">" & rsData.Fields("fldName").Value & "</option>" )

			        rsData.MoveNext()

		        WEnd

	        End If

	        rsData.Close
	        Set rsData = Nothing
        %> 
       
        </select>  
        <input type="button" id="btnCopiar" name="btnCopiar" value="Copiar Atributo" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnLimpar" name="btnLimpar" value="Remover Atributo" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    </div> 
    <div id="divGridEntidades" name="divGridEntidades" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgEntidades" name="fgEntidades" VIEWASTEXT>
        </object>
    </div>
    <div id="divGridAtributos" name="divGridAtributos" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgAtributos" name="fgAtributos" VIEWASTEXT>
        </object>
    </div>
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
 
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
