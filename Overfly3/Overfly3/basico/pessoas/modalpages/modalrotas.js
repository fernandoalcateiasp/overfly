/********************************************************************
modalrotas.js


Library javascript para o modalrotas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_dataGrid2WasChanged = false;
var glb_FGTimerInt = null;
var glb_FG2TimerInt = null;
var glb_nDOSGrid1 = 0;
var glb_nDOSGrid2 = 0;
var glb_bFocusToFG = false;

// Dados do grid .RDS
var dsoPedido = new CDatatransport("dsoPedido");
var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbClonar = new CDatatransport("dsoCmbClonar");
var dsoCmb01 = new CDatatransport("dsoCmb01");
var dsoCmb02 = new CDatatransport("dsoCmb02");
var dsoCmb03 = new CDatatransport("dsoCmb03");
var dsoCidade = new CDatatransport("dsoCidade");
var dsoLkp01 = new CDatatransport("dsoLkp01");
var dsoGrid2 = new CDatatransport("dsoGrid2");
var dsoClonaRota = new CDatatransport("dsoClonaRota");
	
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalrotas.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalrotas.ASP

js_fg_modalrotasBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalrotasDblClick( grid, Row, Col)
js_modalrotasKeyPress(KeyAscii)
js_modalrotas_AfterRowColChange
js_modalrotas_ValidateEdit()
js_modalrotas_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalrotasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Rotas', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    with (divButtons1.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = ELEM_GAP * 4;
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        //top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        top = divButtons1.offsetTop + divButtons1.offsetHeight + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = 170;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
	with (divButtons2.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFG.offsetTop + divFG.offsetHeight;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = ELEM_GAP * 4;
    }    
    
    // ajusta o divFG
    with (divFG2.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        //top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        top = divButtons2.offsetTop + divButtons2.offsetHeight + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = 170;
    }
    
    with (fg2.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }
    
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
    
    btnRefresh.style.top = 5;
    btnRefresh.style.left = 0;
    
    btnGravar.style.top = btnRefresh.offsetTop;
    btnGravar.style.left = btnRefresh.offsetLeft + btnRefresh.offsetWidth + ELEM_GAP;
    
    btnIncluir.style.top = btnRefresh.offsetTop;
    btnIncluir.style.left = btnGravar.offsetLeft + btnGravar.offsetWidth + ELEM_GAP;

    btnExcluir.style.top = btnRefresh.offsetTop;
    btnExcluir.style.left = btnIncluir.offsetLeft + btnIncluir.offsetWidth + ELEM_GAP;
    
	btnFindCidade.style.top = btnRefresh.offsetTop;
	btnFindCidade.style.left = btnExcluir.offsetLeft + btnExcluir.offsetWidth + ELEM_GAP;
	btnFindCidade.style.width = 24;
	btnFindCidade.style.height = 24;

	lblCidadeID.style.top = btnRefresh.offsetTop - 12;
	lblCidadeID.style.left = btnFindCidade.offsetLeft + btnFindCidade.offsetWidth + ELEM_GAP;
	
	selCidadeID.style.top = lblCidadeID.offsetTop + 16;
	selCidadeID.style.left = lblCidadeID.offsetLeft;
	selCidadeID.style.width = FONT_WIDTH * 20;

    btnPC.style.top = btnRefresh.offsetTop;
    btnPC.style.left = selCidadeID.offsetLeft + selCidadeID.offsetWidth + ELEM_GAP;
    
    btnRefresh2.style.top = ELEM_GAP;
    btnRefresh2.style.left = 0;
    
    btnGravar2.style.top = btnRefresh2.offsetTop;
    btnGravar2.style.left = btnRefresh2.offsetLeft + btnRefresh2.offsetWidth + ELEM_GAP;
    
    btnIncluir2.style.top = btnRefresh2.offsetTop;
    btnIncluir2.style.left = btnGravar2.offsetLeft + btnGravar2.offsetWidth + ELEM_GAP;

    btnExcluir2.style.top = btnRefresh2.offsetTop;
    btnExcluir2.style.left = btnIncluir2.offsetLeft + btnIncluir2.offsetWidth + ELEM_GAP;

    lblClonarRota.style.top = btnRefresh2.offsetTop - 9;
    lblClonarRota.style.left = btnExcluir2.offsetLeft + btnExcluir2.offsetWidth + ELEM_GAP;
    selClonarRota.style.top = lblClonarRota.offsetTop + 16;
    selClonarRota.style.left = btnExcluir2.offsetLeft + btnExcluir2.offsetWidth + ELEM_GAP;
    selClonarRota.style.width = FONT_WIDTH * 35;
    selClonarRota.onchange = selClonarRota_onchange;
    
    btnClonar.style.top = btnRefresh2.offsetTop;
    btnClonar.style.left = selClonarRota.offsetLeft + selClonarRota.offsetWidth + ELEM_GAP;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;    
    fg2.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;   
    }
    // codigo privado desta janela
    else if (controlID == 'btnCanc')
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }
    else if (controlID == 'btnRefresh')
    {
		fillGridData();
    }
    else if (controlID == 'btnGravar')
    {
		saveDataInGrid();
    }
    else if (controlID == 'btnExcluir')
    {
		excluirLinha(1);
    }
    else if (controlID == 'btnIncluir')
    {
		incluir();
    }
    else if (controlID == 'btnPC')
    {
		fillCidadeDestinoID();
    }
    else if (controlID == 'btnRefresh2')
    {
		fillGridData2();
	}
    else if (controlID == 'btnGravar2')
    {
		saveDataInGrid2();
    }
    else if (controlID == 'btnIncluir2')
    {
		incluir2();
    }
    else if (controlID == 'btnExcluir2')
    {
		excluirLinha(2);
    }
    else if (controlID == 'btnClonar')
    {
		clonarRotas();
    }
}

function fillCidadeDestinoID()
{
	if (fg.Row > 0)
	{
		// Altera o campo do dso com o novo valor
		if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
		{
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CidadeDestinoID')) = 
				(selCidadeID.value == 0 ? '' : selCidadeID.value);

			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^CidadeDestinoID^dsoLkp01^FldID^FldName*')) = 
				selCidadeID.options[selCidadeID.selectedIndex].innerText;
		
			return null;
		}

		dsoGrid.recordset.MoveFirst();
		
		if (fg.TextMatrix(fg.Row, fg.Cols-1) != '')
			dsoGrid.recordset.Find(fg.ColKey(fg.Cols-1), fg.TextMatrix(fg.Row, fg.Cols-1));

		if (!dsoGrid.recordset.EOF)
		{
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CidadeDestinoID')) = 
				(selCidadeID.value == 0 ? '' : selCidadeID.value);

			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '^CidadeDestinoID^dsoLkp01^FldID^FldName*')) = 
				selCidadeID.options[selCidadeID.selectedIndex].innerText;
				
			if (fg.TextMatrix(fg.Row, fg.Cols-1) != '')
				dsoGrid.recordset['CidadeDestinoID'].value = (selCidadeID.value == 0 ? null : selCidadeID.value);

			glb_dataGridWasChanged = true;
			setupBtnsFromGridState();
		}
	}
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    var bHasRowSelected = fg.Row > 0;
    var bHasRowsInGrid2 = fg2.Rows > 1;
    var bHasRowSelected2 = fg2.Row > 0;
    
    if (glb_dataGrid2WasChanged)
    {
		fg.Enabled = false;
		btnOK.disabled = true;
		btnRefresh.disabled = true;
		btnGravar.disabled = true;
		btnExcluir.disabled = true;
		btnIncluir.disabled = true;
		btnPC.disabled = true;
    }
    else
    {
		fg.Enabled = true;
		btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
		btnRefresh.disabled = false;
		btnGravar.disabled = !glb_dataGridWasChanged;
		btnExcluir.disabled = !bHasRowsInGrid;
		btnIncluir.disabled = false;
		btnPC.disabled = !bHasRowSelected;
    }

	if ((glb_dataGridWasChanged) || ((selClonarRota.selectedIndex > 0)))
	{
		fg2.Enabled = false;
		btnRefresh2.disabled = true;
		btnGravar2.disabled = true;
		btnIncluir2.disabled = true;
		btnExcluir2.disabled = true;
		btnClonar.disabled = (selClonarRota.selectedIndex <= 0);
	}
	else
	{
		fg2.Enabled = true;
		btnRefresh2.disabled = !bHasRowSelected;
		btnGravar2.disabled = !glb_dataGrid2WasChanged;
		btnIncluir2.disabled = !bHasRowSelected;
		btnExcluir2.disabled = !bHasRowSelected2;
		btnClonar.disabled = true;
	}
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    if (glb_FGTimerInt != null)
    {
        window.clearInterval(glb_FGTimerInt);
        glb_FGTimerInt = null;
    }

    var nTransportadoraID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['PessoaID'].value");
    var nPesTransportadoraID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'fg.TextMatrix(fg.Row, fg.Cols-1)');
	var aEmpresa = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');

    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;

    glb_dataGridWasChanged = false;
	glb_nDOSGrid1 = 6;

    setConnection(dsoCmb01);
	dsoCmb01.SQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName ' +
		'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
		'WHERE TipoID=405 AND EstadoID=2 AND Aplicar=1 AND Filtro like \'\%<N>%\'\ ';
    dsoCmb01.ondatasetcomplete = fillGridData_DSC;
    dsoCmb01.Refresh();
    
    setConnection(dsoCmb02);
	dsoCmb02.SQL = 'SELECT DISTINCT d.LocalidadeID AS fldID, d.Localidade AS fldName ' +
		'FROM RelacoesPesRec a WITH(NOLOCK), Pessoas b WITH(NOLOCK), Pessoas_Enderecos c WITH(NOLOCK), Localidades d WITH(NOLOCK), Pessoas e WITH(NOLOCK), Pessoas_Enderecos f WITH(NOLOCK) ' +
		'WHERE (a.ObjetoID = 999 AND a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND ' +
			'a.SujeitoID = b.PessoaID AND b.EstadoID = 2 AND b.PessoaID = c.PessoaID AND ' +
			'c.CidadeID = d.LocalidadeID AND e.PessoaID = ' + nTransportadoraID + ' AND e.PessoaID = f.PessoaID AND ' +
			'f.PaisID = c.PaisID) ' +
		'ORDER BY Localidade';
    dsoCmb02.ondatasetcomplete = fillGridData_DSC;
    dsoCmb02.Refresh();

    setConnection(dsoCmb03);
	dsoCmb03.SQL = 'SELECT DISTINCT c.LocalidadeID AS fldID, c.CodigoLocalidade2 AS fldName1, c.Localidade AS fldName2 ' +
		'FROM Pessoas a WITH(NOLOCK), Pessoas_Enderecos b WITH(NOLOCK), Localidades c WITH(NOLOCK) ' +
		'WHERE (a.PessoaID = ' + nTransportadoraID + ' AND a.PessoaID = b.PessoaID AND ' +
			'b.PaisID = c.LocalizacaoID) ' +
		'ORDER BY c.CodigoLocalidade2';
    dsoCmb03.ondatasetcomplete = fillGridData_DSC;
    dsoCmb03.Refresh();
    
    setConnection(dsoLkp01);
	dsoLkp01.SQL = 'SELECT DISTINCT b.LocalidadeID AS FldID, b.Localidade AS FldName ' +
		'FROM Pessoas_Transportadoras_Rotas a WITH(NOLOCK), Localidades b WITH(NOLOCK) ' +
		'WHERE (a.PesTransportadoraID = ' + nPesTransportadoraID + ' AND a.CidadeDestinoID=b.LocalidadeID)';
    dsoLkp01.ondatasetcomplete = fillGridData_DSC;
    dsoLkp01.Refresh();

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);
    dsoGrid.SQL = 'SELECT a.* FROM Pessoas_Transportadoras_Rotas a WITH(NOLOCK) WHERE a.PesTransportadoraID = ' + nPesTransportadoraID + ' ' +
			'ORDER BY (SELECT bb.ItemMasculino FROM TiposAuxiliares_Itens bb WITH(NOLOCK) WHERE (bb.ItemID = a.MeioTransporteID)), ' +
					 '(SELECT bb.Localidade FROM Localidades bb WITH(NOLOCK) WHERE (bb.LocalidadeID = a.CidadeOrigemID)), ' +
					 '(SELECT bb.Localidade FROM Localidades bb WITH(NOLOCK) WHERE (bb.LocalidadeID = a.UFDestinoID)), a.Capital DESC, ' +
					 '(SELECT bb.Localidade FROM Localidades bb WITH(NOLOCK) WHERE (bb.LocalidadeID = a.CidadeDestinoID))';
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();

    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbClonar);
    dsoCmbClonar.SQL = 'SELECT b.ItemMasculino AS MeioTransporte, c.Localidade AS CidadeOrigem, ' +
		'd.CodigoLocalidade2 AS UFDestino, e.Localidade AS CidadeDestino, ' +
			'CONVERT(VARCHAR(1),ISNULL(a.Capital,0)) AS Cap, ' +
			'CONVERT(VARCHAR(1),ISNULL(a.Metropole,0)) AS Met, ' +
			'CONVERT(VARCHAR(1),ISNULL(a.Interior,0)) AS Int, ' +
			'CONVERT(VARCHAR(1),ISNULL(a.Fluvial,0)) AS Flu, ' +

            'a.HoraCorte   AS HoraCorte ,' +

			'a.PrazoEntrega  AS PrazoEntrega , ' +
			
			
			
			
			'a.PesTraRotaID ' +
			'FROM Pessoas_Transportadoras_Rotas a WITH(NOLOCK) ' +
			    'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (a.MeioTransporteID = b.ItemID) ' +
			    'LEFT OUTER JOIN Localidades c WITH(NOLOCK) ON (a.CidadeOrigemID = c.LocalidadeID) ' +
			    'LEFT OUTER JOIN Localidades d WITH(NOLOCK) ON (a.UFDestinoID = d.LocalidadeID) ' +
			    'LEFT OUTER JOIN Localidades e WITH(NOLOCK) ON (a.CidadeDestinoID = e.LocalidadeID) ' +
			'WHERE (a.PesTransportadoraID = ' + nPesTransportadoraID + ' ) ' +
			'ORDER BY (SELECT bb.ItemMasculino FROM TiposAuxiliares_Itens bb WITH(NOLOCK) WHERE (bb.ItemID = a.MeioTransporteID)), ' +
					 '(SELECT bb.Localidade FROM Localidades bb WITH(NOLOCK) WHERE (bb.LocalidadeID = a.CidadeOrigemID)), ' +
					 '(SELECT bb.Localidade FROM Localidades bb WITH(NOLOCK) WHERE (bb.LocalidadeID = a.UFDestinoID)), a.Capital DESC, ' +
					 '(SELECT bb.Localidade FROM Localidades bb WITH(NOLOCK) WHERE (bb.LocalidadeID = a.CidadeDestinoID))';

    dsoCmbClonar.ondatasetcomplete = fillGridData_DSC;
    dsoCmbClonar.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDOSGrid1--;
	
	if (glb_nDOSGrid1 > 0)
		return;

    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 3;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    headerGrid(fg,['Meio',
                  'Cidade Orig',
                  'UF Dest',
                  'Cidade Dest',
                  'Cap',
                  'Met',
                  'Int',
                  'Flu',

                  'Hora Corte ', 
                  'P.E ',
                  'P.A',
                  
                  'Valor TAS',
                  'Perc GRIS',
                  'Valor GRIS M�n',
                  'Perc Seg',
                  'Valor Col',
                  'Valor Col Exc',
                  'Valor Entr',
                  'Valor Entr Exc',
                  'Valor Int',
                  'Valor Int Exc',
                  'Valor Ped',
                  'Valor ZF',
                  'Perc TRF',
                  'Valor TRF M�n',
                  'Valor M�n',
                  'Valor Desemb',
                  'CidadeDestinoID',
                  'PesTraRotaID'],[24,25]);
    
    fillGridMask(fg,dsoGrid,['MeioTransporteID',
                             'CidadeOrigemID',
                             'UFDestinoID',
                             '^CidadeDestinoID^dsoLkp01^FldID^FldName*',
                             'Capital',
                             'Metropole',
                             'Interior',
                             'Fluvial',

                             'HoraCorte',
                             'PrazoEntrega',
                             'PrazoAdicional',
                             
                             
                             'ValorTAS',
                             'PercentualGRIS',
                             'ValorGRISMinimo',
                             'PercentualSeguro',
                             'ValorColeta',
                             'ValorColetaExcedente',
                             'ValorEntrega',
                             'ValorEntregaExcedente',
                             'ValorInteriorizacao',
                             'ValorInteriorizacaoExcedente',
                             'ValorPedagio',
                             'ValorZonaFranca',
                             'PercentualTRF',
                             'ValorTRFMinimo',
                             'ValorMinimo',
                             'ValorDesembaraco',
                             'CidadeDestinoID',
                             'PesTraRotaID'],
                              ['', '', '', '', '', '', '', '','', '', '', '999999999.99', '999.99', '999999999.99', '999.999', '999999999.99',
                             '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99',
                             '999999999.99', '999.99', '999999999.99', '999999999.99', '999999999.99', '', '']
                             ,

                             ['', '', '', '', '', '', '', '','', '', '', '###,###,##0.00', '##0.00', '###,###,##0.00', '##0.000', '###,###,##0.00',
                             '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00',
                             '###,###,##0.00', '##0.00', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '', '']);

    //old alignColsInGrid(fg, [8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23]);
    alignColsInGrid(fg, [10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 26]);


	insertcomboData(fg, getColIndexByColKey(fg, 'MeioTransporteID'), dsoCmb01, 'fldName', 'fldID');	
	insertcomboData(fg, getColIndexByColKey(fg, 'CidadeOrigemID'), dsoCmb02, 'fldName', 'fldID');	
	insertcomboData(fg, getColIndexByColKey(fg, 'UFDestinoID'), dsoCmb03, '*fldName1|fldName2', 'fldID');	

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
	fg.ColWidth(getColIndexByColKey(fg, '^CidadeDestinoID^dsoLkp01^FldID^FldName*')) = 100 * 18;    
    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = 4;
    
    if (fg.Rows > 1)
        fg.Editable = true;

	fillCmbClonarRota();

    lockControlsInModalWin(false);
    
    if (selClonarRota.options.length == 0)
		selClonarRota.disabled = true;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
    
	glb_bFocusToFG = true;
	if (glb_FG2TimerInt == null)
		glb_FG2TimerInt = window.setInterval('fillGridData2()', 30, 'JavaScript');
}

function fillGridData2()
{
    if (glb_FG2TimerInt != null)
    {
        window.clearInterval(glb_FG2TimerInt);
        glb_FG2TimerInt = null;
    }
	
	if (fg.Row < 1)
	{
		fg.Enabled = true;
		glb_bFocusToFG = false;
		fg2.Rows = 1;
		setupBtnsFromGridState();
		return null;
	}
	
	var nPesTraRotaID = 0;
	if (selClonarRota.selectedIndex > 0)
		nPesTraRotaID = selClonarRota.value;
	else
		nPesTraRotaID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PesTraRotaID'));

	if ((nPesTraRotaID == null) || (nPesTraRotaID == ''))
	{
		fg.Enabled = true;
		return null;
	}
		
    lockControlsInModalWin(true);

    // zera o grid
    fg2.Rows = 1;

    glb_dataGrid2WasChanged = false;
    glb_nDOSGrid2 = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid2);
    dsoGrid2.SQL = 'SELECT * ' +
		'FROM Pessoas_Transportadoras_Rotas_Pesos WITH(NOLOCK) ' +
		'WHERE PesTraRotaID = ' + nPesTraRotaID + ' ' +
		'ORDER BY PesoMaximo';

    dsoGrid2.ondatasetcomplete = fillGridData_DSC2;
    dsoGrid2.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC2()
{
	glb_nDOSGrid2--;
	
	if (glb_nDOSGrid2 > 0)
		return;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2);
    fg2.FontSize = '8';
    fg2.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg2,['Peso M�ximo',
                  'Perc NF',
                  'Tx Peso',
                  'Valor M�n',
                  'MKM',
                  'PesTraRotPesoID'],[5]);
    
    fillGridMask(fg2,dsoGrid2,['PesoMaximo',
							  'PercentualNotaFiscal',
							  'TaxaPeso',
							  'ValorMinimo',
							  'MKM',
							  'PesTraRotPesoID'],
                             ['99999.99','999.99','99999.99','999999999.99','999.99',''],
                             ['##,##0.00','##0.00','##,##0.00','##,##0.00','##0.00','']);

	alignColsInGrid(fg2,[0,1,2,3,4]);

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0,fg2.Cols-1);
    fg2.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg2.Rows > 1 )
        fg2.Col = 0;
    
    if (fg2.Rows > 1)
        fg2.Editable = true;

    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg2.Row < 1) && (fg2.Rows > 1) )
        fg2.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg2.Redraw = 2;
	fg.Enabled = true;
    // se tem linhas no grid, coloca foco no grid
    if (glb_bFocusToFG)
    {
		glb_bFocusToFG = false;
        window.focus();
        fg.focus();
    }
    else if (fg2.Rows > 1)
    {
        window.focus();
        if (fg2.Enabled)
			fg2.focus();
    }         
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    if (!writeNewRowsInDSO(1))
		return null;
	
	glb_dataGridWasChanged = false;		
	lockControlsInModalWin(true);		
    try
    {
        dsoGrid.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg.Rows = 1;
        lockControlsInModalWin(false);
        
		glb_FGTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
        return null;
    }    

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    glb_FGTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid2()
{
    if (!writeNewRowsInDSO(2))
		return null;
	
	glb_dataGrid2WasChanged = false;		
	lockControlsInModalWin(true);		
    try
    {
        dsoGrid2.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg2.Rows = 1;
        lockControlsInModalWin(false);
        
		if (glb_FG2TimerInt == null)
			glb_FG2TimerInt = window.setInterval('fillGridData2()', 30, 'JavaScript');
        return null;
    }    

    dsoGrid2.ondatasetcomplete = saveDataInGrid2_DSC;
    dsoGrid2.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid2_DSC() {
    if (glb_FG2TimerInt == null)
		glb_FG2TimerInt = window.setInterval('fillGridData2()', 30, 'JavaScript');
}

function excluirLinha(nTipo)
{
	var grid;
	var dso;
	var oldRow = 0;
	
	if (nTipo == 1)
	{
		grid = fg;
		dso = dsoGrid;
	}
	else if (nTipo == 2)
	{
		grid = fg2;
		dso = dsoGrid2;
	}
	
	if (fg.Row > 0)
	{
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir esta linha?");
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
	
		if (grid.TextMatrix(grid.Row, grid.Cols-1) == '')
		{
			oldRow = grid.Row;
			grid.RemoveItem(grid.Row);
			
			if (grid.Rows > oldRow)
				grid.Row = oldRow;
			else if (grid.Rows > 1)
				grid.Row = grid.Rows - 1;
				
			setupBtnsFromGridState();
			return null;
		}
		
        // Altera o campo do dso com o novo valor
		dso.recordset.MoveFirst();
		dso.recordset.Find(grid.ColKey(grid.Cols-1), grid.TextMatrix(grid.Row, grid.Cols-1));

        if (!dso.recordset.EOF)
        {
			grid.RemoveItem(grid.Row);
			dso.recordset.Delete();
			if (nTipo == 1)
				glb_dataGridWasChanged = true;
			else if (nTipo == 2)
				glb_dataGrid2WasChanged = true;
				
			if (nTipo == 1)
				saveDataInGrid();
			else if (nTipo == 2)
				saveDataInGrid2();
        }
	}
}

function incluir()
{
	fg.Editable = true;
	fg.Rows = fg.Rows + 1;
	fg.TopRow = fg.Rows - 1;
	fg.Row = fg.Rows - 1;
	glb_dataGridWasChanged = true;
	setupBtnsFromGridState();
}

function incluir2()
{
	fg2.Editable = true;
	fg2.Rows = fg2.Rows + 1;
	fg2.TopRow = fg2.Rows - 1;
	fg2.Row = fg2.Rows - 1;
	glb_dataGrid2WasChanged = true;
	setupBtnsFromGridState();
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalrotasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalrotasDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalrotasKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalrotas_ValidateEdit()
{
    ;
}

//<Heraldo>

function ValidarHora(valor) {

    //divide a hora em tres parte semaprando pelos :

    var ValorHora = valor.split(":");


    //valida se a faixa de hora � valida
    if ((parseFloat(ValorHora[0]) < 0) || (parseFloat(ValorHora[0]) > 23)) {
        window.top.overflyGen.Alert("Hora inv�lida, formato correto � : HH:mm (12:00)");
        return false;
    }

    //valida se a faixa de minutos � valida    
    if ((parseFloat(ValorHora[1]) < 0) || (parseFloat(ValorHora[1]) > 59)) {
        window.top.overflyGen.Alert("Minutos inv�lidos, formato correto �: HH:mm (12:00)");
        return false;
    }

    //valida se a faixa de segundos � valida
    if (ValorHora.length > 2) {
        if ((parseFloat(ValorHora[2]) < 0) || (parseFloat(ValorHora[2]) > 59))
            window.top.overflyGen.Alert("Segundos inv�lidos, formato correto � : HH:mm (12:00)");
        return false;
    }

    if (ValorHora.length > 3) {
        //if ((parseFloat(ValorHora[2]) < 0) || (parseFloat(ValorHora[2]) > 59))
        window.top.overflyGen.Alert("Formato inv�lidos, formato correto � : HH:mm (12:00)");
        return false;
    }

    return true;
}

//</Heraldo>

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalrotas_AfterEdit(Row, Col)
{
    var nType = 0;


    //<Heraldo>

    var i = 1;

    while (i < fg.Rows) {

        var DataRota = fg.TextMatrix(i, getColIndexByColKey(fg, 'HoraCorte'));

        if (ValidarHora(DataRota) == false) {

            fg.TextMatrix(i, getColIndexByColKey(fg, 'HoraCorte')) = window.focus();
            //if (window.top.overflyGen.Alert('Hora Inv�lida') == 0);
            return false;
            // else
            // if (window.top.overflyGen.Alert('Hora V�lida') == 0);
        }
        i++;
    }
       
//</Heraldo>	
	
	
    if (fg.Editable)
    {
		if (dsoGrid.recordset.fields.Count > 0)
		{
			nType = dsoGrid.recordset[fg.ColKey(Col)].type;
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
				fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
		}

		if (fg.TextMatrix(Row, fg.Cols-1) != '')
		{
			// Altera o campo do dso com o novo valor

			dsoGrid.recordset.MoveFirst();
			dsoGrid.recordset.Find(fg.ColKey(fg.Cols-1), fg.TextMatrix(Row, fg.Cols-1));

			if ( !(dsoGrid.recordset.EOF) )
			{
				nType = dsoGrid.recordset[fg.ColKey(Col)].type;
	            
				// Se decimal , numerico , int, bigint ou boolean
				if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
					dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
				else    
					dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);
	                
				glb_dataGridWasChanged = true;
				setupBtnsFromGridState();
			}
        }
        else
        {
			glb_dataGridWasChanged = true;
			setupBtnsFromGridState();
		}
    }
}

function fg_Rotas_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
{
	if (!fg.Enabled)
		return null;
		
	if (NewRow != OldRow)
	{
		clearComboEx(['selCidadeID']);
        var oOption = document.createElement("OPTION");
        oOption.text = '';
        oOption.value = 0;
        selCidadeID.add(oOption);
        fg2.Rows = 1;
		glb_bFocusToFG = true;

		if (glb_FG2TimerInt == null)
		{
			if (selClonarRota.options.length > 0)
				selClonarRota.selectedIndex = 0;
			fg.Enabled = false;
			glb_FG2TimerInt = window.setInterval('fillGridData2()', 30, 'JavaScript');
		}
	}
}

function js_modalrotas_AfterEdit2(Row, Col)
{
    var nType;
	
    if (fg2.Editable)
    {
		if (dsoGrid.recordset.fields.Count > 0)
		{
			nType = dsoGrid2.recordset[fg2.ColKey(Col)].type;
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
				fg2.TextMatrix(Row, Col) = treatNumericCell(fg2.TextMatrix(Row, Col));
		}
    
		if (fg2.TextMatrix(Row, fg2.Cols-1) != '')
		{
			// Altera o campo do dso com o novo valor
			dsoGrid2.recordset.MoveFirst();
			dsoGrid2.recordset.Find(fg2.ColKey(fg2.Cols-1), fg2.TextMatrix(Row, fg2.Cols-1));

			if ( !(dsoGrid2.recordset.EOF) )
			{
				nType = dsoGrid2.recordset[fg2.ColKey(Col)].type;
	            
				// Se decimal , numerico , int, bigint ou boolean
				if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
					dsoGrid2.recordset[fg2.ColKey(Col)].value = fg2.ValueMatrix(Row, Col);
				else    
					dsoGrid2.recordset[fg2.ColKey(Col)].value = fg2.TextMatrix(Row, Col);
	                
				glb_dataGrid2WasChanged = true;
				setupBtnsFromGridState();
			}
        }
        else
        {
			glb_dataGrid2WasChanged = true;
			setupBtnsFromGridState();
		}
    }
}

// FINAL DE EVENTOS DE GRID *****************************************

function btnLupaClicked()
{
	if (fg.Row < 1)
		return null;

	var nUFID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'UFDestinoID'));

	if (isNaN(nUFID))
		return null;

	if ((nUFID == null)||(nUFID == ''))
		return null;		

	lockControlsInModalWin(true);
    setConnection(dsoCidade);
	dsoCidade.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL ' +
		'SELECT LocalidadeID AS fldID, Localidade AS fldName ' +
		'FROM Localidades WITH(NOLOCK) ' +
		'WHERE LocalizacaoID=' + nUFID + ' AND EstadoID=2 ' +
		'ORDER BY fldName';
    dsoCidade.ondatasetcomplete = fillCidade_DSC;
    dsoCidade.Refresh();
}

function fillCidade_DSC() {
    if (!((dsoCidade.recordset.BOF) && (dsoCidade.recordset.EOF)))
	{
	    clearComboEx(['selCidadeID']);
    
        while (! dsoCidade.recordset.EOF )
        {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCidade.recordset['fldName'].value;
            oOption.value = dsoCidade.recordset['fldID'].value;
            selCidadeID.add(oOption);
            dsoCidade.recordset.MoveNext();
        }
	}
	lockControlsInModalWin(false);
}

// nTipo 1 : grid 1
// nTipo 2 : grid 2
function writeNewRowsInDSO(nTipo)
{
	var i, j;
	var sColuna = '';
	var nFieldKeyID = 0;
	var nType = 0;
	var aCamposObrigatorios = new Array();
	var sMsgError = '';
	var nLineError = 0;
	var bRetVal = true;
	var nLinkedFieldValueID = 0;
	var sFldKey = '';
	var sLinkedField = '';
	var grid;
	var dso;
	
	if (nTipo == 1)
	{
		sFldKey = 'PesTraRotaID';
		sLinkedField = 'PesTransportadoraID';
		grid = fg;
		dso = dsoGrid;
		nLinkedFieldValueID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'fg.TextMatrix(fg.Row, fg.Cols-1)');
	}
	else if (nTipo == 2)
	{
		sFldKey = 'PesTraRotPesoID';
		sLinkedField = 'PesTraRotaID';
		grid = fg2;
		dso = dsoGrid2;
		nLinkedFieldValueID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, sLinkedField));
	}
		
	for (i=1; i<grid.Rows; i++)
	{
		if (!(dso.recordset.BOF && dso.recordset.EOF))
		{
			dso.recordset.MoveFirst();
			nFieldKeyID = grid.TextMatrix(i,getColIndexByColKey(grid, sFldKey));
		}

		// Linha Nova
		if (nFieldKeyID == '')
			dso.recordset.AddNew();
		else
			continue;

		dso.recordset[sLinkedField].value = nLinkedFieldValueID;			
		for (j=0; j<grid.Cols; j++)
		{
			sColuna = grid.ColKey(j);

			if ( (sColuna.substr(0,6).toUpperCase() == '_CALC_') ||
				 (sColuna.substr(0,1) == '^') )
				 continue;

			if ((nTipo==1) && (sColuna.toUpperCase() == 'PESTRAROTAID'))
				continue;

			if ((nTipo==2) && (sColuna.toUpperCase() == 'PESTRAROTPESOID'))
				continue;
				
			if (nTipo==1)
			{
				if ((sColuna.toUpperCase() == 'MEIOTRANSPORTEID') &&
					(grid.TextMatrix(i, j) == ''))
				{
					sMsgError = 'Preencha o campo (Meio)';
					nLineError = i;
				}
				else if ((sColuna.toUpperCase() == 'CIDADEORIGEMID') &&
					(grid.TextMatrix(i, j) == ''))
				{
					sMsgError = 'Preencha o campo (Cidade Orig)';
					nLineError = i;
				}
				else if ((sColuna.toUpperCase() == 'UFDESTINOID') &&
						(grid.TextMatrix(i, j) == ''))
				{
					sMsgError = 'Preencha o campo (UF Dest)';
					nLineError = i;
				}
			}
			
			if (sMsgError != '')
				break;

			nType = dso.recordset[grid.ColKey(j)].type;
	        
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
			{
				if ( (sColuna.substr(sColuna.length - 2).toUpperCase() == 'ID') &&
					 (grid.ValueMatrix(i, j) == 0) )
					dso.recordset[grid.ColKey(j)].value = null;
				else
					dso.recordset[grid.ColKey(j)].value = grid.ValueMatrix(i, j);
			}
			else    
				dso.recordset[grid.ColKey(j)].value = grid.TextMatrix(i, j);
		}
	}
	
	if (sMsgError != '')
	{
		dso.recordset.Delete();
		bRetVal = false;
        if ( window.top.overflyGen.Alert (sMsgError) == 0 )
            return null;
            
		grid.Row = nLineError;
	}
	
	return bRetVal;
}

function fillCmbClonarRota()
{
	var sText = '';
	clearComboEx(['selClonarRota']);
	
	if (dsoCmbClonar.recordset.BOF && dsoCmbClonar.recordset.EOF)
		return;
	
	dsoCmbClonar.recordset.moveFirst();
	
    var oOption = document.createElement("OPTION");
	oOption.text =	'';
    oOption.value = 0;
	selClonarRota.add(oOption);

	while (!dsoCmbClonar.recordset.EOF)
	{
        var oOption = document.createElement("OPTION");
        sText = isNull(dsoCmbClonar.recordset['MeioTransporte'].value, '') + '/' +
			isNull(dsoCmbClonar.recordset['CidadeOrigem'].value, '') + '/' +
			isNull(dsoCmbClonar.recordset['UFDestino'].value, '') + '/' +
			isNull(dsoCmbClonar.recordset['CidadeDestino'].value, '') + '/' +
			isNull(dsoCmbClonar.recordset['Cap'].value, '') + '/' +
			isNull(dsoCmbClonar.recordset['Met'].value, '') + '/' +
			isNull(dsoCmbClonar.recordset['Int'].value, '') + '/' +
			isNull(dsoCmbClonar.recordset['Flu'].value, '');
			
		oOption.text = replaceStr(sText, '/' + '/', '/');
			
        oOption.value = dsoCmbClonar.recordset['PesTraRotaID'].value;
        selClonarRota.add(oOption);
        dsoCmbClonar.recordset.moveNext();
	}
}

function isNull(value, valueIfIsNull)
{
	if (value == null)
		return valueIfIsNull;
	else
		return value;
}

function selClonarRota_onchange()
{
	setupBtnsFromGridState();
	fillGridData2();
}

function clonarRotas()
{
	var strPars = '?nOldPesTraRotaID=' + escape(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PesTraRotaID'))) +
		'&nNewPesTraRotaID=' + escape(selClonarRota.value);
	lockControlsInModalWin(true);
	dsoClonaRota.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/clonarumarota.aspx' + strPars;
	dsoClonaRota.ondatasetcomplete = clonarRotas_DSC;
	dsoClonaRota.refresh();
}

function clonarRotas_DSC() {
    if (selClonarRota.options.length > 0)
		selClonarRota.selectedIndex = 0;

	fillGridData2();
}
