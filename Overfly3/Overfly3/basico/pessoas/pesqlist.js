/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form pessoas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()
	dialByModalPage(sDDD, sNumero, nPessoaID)
********************************************************************/
var glb_first = true;
var glb_contexto = 0;

var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
var dsoListaPrecos = new CDatatransport('dsoListaPrecos');

// IMPLEMENTACAO DAS FUNCOES

function window_onload() {
    windowOnLoad_1stPart();

    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Nome', 'Fantasia/Apelido', 'Tipo', 'Classifica��o', 'Cr�d Venc', 'Observa��o', 'Localiza��o', 'TipoPessoaID', 'Responsavel', 'CorPessoa');

    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', '', dTFormat);


    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked() {
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var nCorPessoa = getColIndexByColKey(fg, 'CorPessoa');
    var bgColorWhite = 0XFFFFFF;
    var bgColorGreen = 0X90EE90;
    var bgColorYellow = 0x00ffff;
    var bgColorBlue = 0xf49518;
    var bgColorOrange = 0X60A4F4;
    var bgColorRed = 0X0000FF;
    var bgColorPurple = 0X66008C;
    var bgColorBrown = 0x4872ab;
    var bgColorBlack = 0x000010;

    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Imagem', 'Confirma��o de cadastro', 'Ficha Cadastral', 'Atendimento', 'Atributos', 'DadosRH', 'S�cios']);

    if ((fg.Rows > 1) && (fg.Row > 0))
        setupEspecBtnsControlBar('sup', 'HHHDHHHHHH');
    else
        setupEspecBtnsControlBar('sup', 'HHHDHDHHHD');

    var contextoID = getCmbCurrDataInControlBar('sup', 1);

    if (fg.Cols >= 6)
        fg.ColHidden(5) = (contextoID[1] != 1211);

    if (fg.Cols > 0) {
        fg.ColHidden(getColIndexByColKey(fg, 'TipoPessoaID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'Responsavel')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'CorPessoa')) = true;
    }

    alignColsInGrid(fg, [0]);

    btnGetProps.style.visibility = 'hidden';

    if (glb_first == true) {
        carregaProprietarios();
        glb_contexto = contexto[1];
    } else if (glb_contexto != contexto[1]) {
        carregaProprietarios();
        glb_first = true;
        glb_contexto = contexto[1];
    }

    var i;

    FillStyle = 1;

    for (i = 1; i < fg.Rows; i++) {

        var _nCorPessoa = null;

        switch (fg.TextMatrix(i, nCorPessoa).substring(0, 1))
        {
            case "1":
                _nCorPessoa = bgColorWhite;
            break;

            case "2":
                _nCorPessoa = bgColorYellow;
            break;

            case "3":
                _nCorPessoa = bgColorBlue;
            break;

            case "4":
                _nCorPessoa = bgColorRed;
            break;

            case "5":
                _nCorPessoa = bgColorGreen;
            break;

            case "6":
                _nCorPessoa = bgColorPurple;
                break;

            case "7":
                _nCorPessoa = bgColorBrown;
                break;

            case "8":
                _nCorPessoa = bgColorBlack;
                break;

            default:
                _nCorPessoa = null;
            break;
        }

        if (_nCorPessoa != null)
        {
            //Pinta c�lula
            fg.Cell(6, i, getColIndexByColKey(fg, 'CredVenc'), i, getColIndexByColKey(fg, 'CredVenc')) = _nCorPessoa;

            if ((_nCorPessoa == bgColorBlack) || (_nCorPessoa == bgColorBlue) || (_nCorPessoa == bgColorPurple)) {
                //Pinta cor da fonte
                fg.Cell(7, i, getColIndexByColKey(fg, 'CredVenc'), i, getColIndexByColKey(fg, 'CredVenc')) = bgColorWhite;
            }
        }

    }
    FillStyle = 0;


    lblProprietariosPL.title = 'Clique simples: recarrega o combo \n' +
                              'Clique duplo: alterna o combo entre Propriet�rio e Alternativo e recarrega o combo';

    // Proprietario
    lblProprietariosPL.onmouseover = lblProprietariosPL_onmouseover;
    lblProprietariosPL.onmouseout = lblProprietariosPL_onmouseout;

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {

    // usuario clicou botao documentos
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }

        // usuario clicou botao imprimir
    else if (btnClicked == 2)
        openModalPrint();
    else if (btnClicked == 3) {
        window.top.openModalControleDocumento('PL', '', 720, null, '21', 'T');
    }
    else if (btnClicked == 5) {
        openModalConfirmacaoCadastro();
    }
    else if (btnClicked == 6) {
        var nCurrPessoaID = getCellValueByColKey(fg, 'PessoaID', fg.Row);
        window.top.openModalHTML(nCurrPessoaID, 'Ficha Cadastral', 'PL', 3, 1);
    }
    else if (btnClicked == 7) {
        openModalAtendimento();
    }
    else if (btnClicked == 8) {
        openModalAtributos();
    }
    else if (btnClicked == 9) {
        openModalDadosRH();
    }
    else if (btnClicked == 10) {
        var nCurrPessoaID = getCellValueByColKey(fg, 'PessoaID', fg.Row);
        var nCurrTipoPessoaID = getCellValueByColKey(fg, 'TipoPessoaID', fg.Row);

        openModalSocios(nCurrPessoaID, nCurrTipoPessoaID);
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCONFIRMACAOCADASTROHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
        // Modal Alteracao de Credito
    else if (idElement.toUpperCase() == 'MODALATENDIMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALGERIMAGENSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALATRIBUTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (param1 == 'CANCEL') {
        // esta funcao fecha a janela modal e destrava a interface
        restoreInterfaceFromModal();
        // escreve na barra de status
        writeInStatusBar('child', 'cellMode', 'Detalhe');

        // nao mexer
        return 0;
    }
    else if (idElement.toUpperCase() == 'MODALDADOSRHHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de S�cios
    else if (idElement.toUpperCase() == 'MODALSOCIOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch() {

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Usuario clicou botao de detalhar Lista de Precos
********************************************************************/
function detalhalistaPrecos() {
    lockInterface(true);

    var empresa = getCurrEmpresaData();
    var nClienteID = fg.ValueMatrix(fg.Row, 0);
    var strPars = new String();

    strPars = '?';
    strPars += 'nClienteID=';
    strPars += escape(nClienteID);
    strPars += '&nEmpresaID=';
    strPars += escape(empresa[0].toString());

    dsoListaPrecos.URL = SYS_ASPURLROOT + '/serversidegenEx/listaprecosdata.aspx' + strPars;
    dsoListaPrecos.ondatasetcomplete = detalhalistaPrecos_DSC;
    dsoListaPrecos.refresh();
}

/********************************************************************
Retorno do servidor - Usuario clicou botao de detalhar Lista de Precos
********************************************************************/
function detalhalistaPrecos_DSC() {
    var empresa = getCurrEmpresaData();
    var aListaPrecoData = new Array();

    if (!(dsoListaPrecos.recordset.BOF && dsoListaPrecos.recordset.EOF)) {
        aListaPrecoData[0] = dsoListaPrecos.recordset['Fantasia'].value;
        aListaPrecoData[1] = dsoListaPrecos.recordset['PessoaID'].value;
        aListaPrecoData[2] = dsoListaPrecos.recordset['UFID'].value;
        aListaPrecoData[3] = dsoListaPrecos.recordset['EmpresaSistema'].value;
        aListaPrecoData[4] = dsoListaPrecos.recordset['ListaPreco'].value;
        aListaPrecoData[5] = dsoListaPrecos.recordset['PMP'].value;
        aListaPrecoData[6] = dsoListaPrecos.recordset['NumeroParcelas'].value;
        aListaPrecoData[7] = dsoListaPrecos.recordset['FinanciamentoPadrao'].value;
        aListaPrecoData[8] = dsoListaPrecos.recordset['ClassificacaoInterna'].value;
        aListaPrecoData[9] = dsoListaPrecos.recordset['ClassificacaoExterna'].value;

        // Manda o id da pessoa a detalhar 
        sendJSCarrier(getHtmlId(), 'SETPESSOAPRICELIST', new Array(empresa[0], aListaPrecoData));
    }

    lockInterface(false);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (fg.disabled == false)
        fg.focus();

    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(480, 280));
}

/********************************************************************
Recebe input de janela modal para discar pelo carrier.
Definir e implementar no pesqlist, no sup e no inf do form.
********************************************************************/
function dialByModalPage(sDDD, sNumero, nPessoaID) {
    sDDD = sDDD.toString();
    sNumero = sNumero.toString();
    nPessoaID = parseInt(nPessoaID, 10);

    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

function openModalConfirmacaoCadastro() {
    if (fg.Rows <= 1) {
        window.top.overflyGen.Alert('Nenhum registro na listagem');
        return;
    }

    var htmlPath;
    var strPars = new String();
    var nWidth = 980;
    var nHeight = 570;
    var nRegistroID = 0;
    var aEmpresa = getCurrEmpresaData();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nRegistroID=' + escape(nRegistroID);
    strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);


    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalconfirmacaocadastro.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

function openModalAtendimento() {
    window.top.overflyGen.Alert('Modal Atendimento foi transferida para o form lista de pre�o.');
    return;

    return null;

    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = aEmpresaData[0];
    var userID = getCurrUserID();
    var htmlPath;
    var strPars = new String();
    var nA1 = getCurrRightValue('SUP', 'B6A1');
    var nA2 = getCurrRightValue('SUP', 'B6A2');

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nA1=' + escape(nA1 ? 1 : 0);
    strPars += '&nA2=' + escape(nA2 ? 1 : 0);

    // carregar modal - faz operacao de banco no carregamento
    //htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modalatendimento.asp'+strPars;
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalatendimento.asp' + strPars;

    showModalWin(htmlPath, new Array(775, 460));

    if (userID == 1000) {
        var rectModal = new Array(0, 0, 775, 460);
        moveFrameInHtmlTop('frameModal', rectModal);
        var modalFrame = getFrameInHtmlTop('frameModal');

        if (modalFrame != null) {
            modalFrame.style.zIndex = 2;
        }

        showFrameInHtmlTop('frameModal', true);
    }
}

/********************************************************************
Funcao criada pelo programador.
retorna um array com os id's que estao no grid no momento

Parametro:
nenhum

Retorno:
array com os id's que estao no grid no momento
********************************************************************/
function retArrayGrid() {
    if (fg.Rows <= 1)
        return new Array();

    var aGrid = new Array(fg.Rows - 1);
    var i;

    for (i = 1; i < fg.Rows; i++)
        aGrid[i - 1] = fg.TextMatrix(i, 0);

    return aGrid;
}

/********************************************************************
Funcao criada pelo programador.
Chama a Modal Atributos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalAtributos() {

    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 600;
    var nRegistroID = 0;
    var aEmpresa = getCurrEmpresaData();
    var nEmpresaID = aEmpresa[0];
    var userID = getCurrUserID();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nUserID=' + escape(userID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalatributos.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}
/********************************************************************
Funcao criada pelo programador.
Chama a Modal DadosRH

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalDadosRH() {

    var htmlPath;
    var strPars = new String();
    var nPessoaID = 0;

    if (fg.Row > 0) {
        nPessoaID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID'));

        // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
        strPars = '?sCaller=' + escape('PL');
        strPars += '&nPessoaID=' + escape(nPessoaID);

        htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modaldadosrh.asp' + strPars;
        //showModalWin(htmlPath, new Array(995, 532));
        showModalWin(htmlPath, new Array(555, 462));
    }
}

/********************************************************************
Funcao criada pelo programador.
Chama a Modal S�cios

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalSocios(nPessoaID, nTipoPessoaID) {

    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 600;
    var userID = getCurrUserID();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nTipoPessoaID=' + escape(nTipoPessoaID);
    strPars += '&nCreditoID=' + escape(0);
    strPars += '&nUserID=' + escape(userID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modalgen/modalsocios.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Retorno de usuario clicou o botao do combo de proprietarios

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function btnGetProps_onClick_DSC() {
    var optionStr, optionValue;

    clearComboEx([selProprietariosPL.name]);

    while (!dsoPropsPL.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoPropsPL.recordset['Proprietario'].value;
        oOption.value = dsoPropsPL.recordset['ProprietarioID'].value;
        selProprietariosPL.add(oOption);
        dsoPropsPL.recordset.MoveNext();
    }

    if (glb_first == true) {
        carregaProprietarios();
        glb_first = false;
    }
    else {
        lockInterface(false);
    }
}

function carregaProprietarios() {
    lockInterface(true);
    var cmbContextData = getCmbCurrDataInControlBar('sup', 1);
    var cmbFiltroPadraoData = getCmbCurrDataInControlBar('sup', 2);
    var contextID = cmbContextData[1];
    var FiltroPadraoID = cmbFiltroPadraoData[1];
    var Proprietario = 0;
    var RegistrosVencidos = 0;

    // Registros Vencidos
    if (chkRegistrosVencidos.checked)
        RegistrosVencidos = 1;
    else
        RegistrosVencidos = 0;

    // Parametros para o servidor
    var strPars = new String();
    strPars = '?';

    var subFormID = window.top.subFormID;  // variavel global do browser filho    

    // formID
    strPars += 'nSubFormID=';
    strPars += encodeURIComponent(subFormID.toString());
    // contextoID
    strPars += '&nContextoID=';
    strPars += encodeURIComponent(contextID.toString());
    //FiltroPadraoID
    strPars += '&nFiltroPadraoID=';
    strPars += encodeURIComponent(FiltroPadraoID.toString());
    // RegistrosVencidos
    strPars += '&bRegistrosVencidos=' + encodeURIComponent(RegistrosVencidos);
    // empresaID
    strPars += '&nEmpresaID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_COMBODATA, 'VALUE', null)).toString());
    // userID
    strPars += '&nUsuarioID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_WIDEMSG, '__CURRUSERID', null)).toString());
    strPars += '&bAllList=' + encodeURIComponent(1);
    // Resultado
    strPars += '&bResultado=' + encodeURIComponent((glb_modoComboProprietario == true ? 1 : 0));

    dsoPropsPL.URL = SYS_ASPURLROOT + '/serversidegenEx/cmbspropspl2.aspx' + strPars;
    dsoPropsPL.ondatasetcomplete = btnGetProps_onClick_DSC;
    dsoPropsPL.refresh();
}


function lblProprietariosPL_onClick() {
    glb_TimerClick = window.setInterval('lblProprietariosPL_onClick_Continue()', 10, 'JavaScript');;
}

function lblProprietariosPL_onClick_Continue() {
    if (glb_TimerClick != null) {
        window.clearInterval(glb_TimerClick);
        glb_TimerClick = null;
    }

    carregaProprietarios();
}

function lblProprietariosPL_ondblClick() {
    glb_modoComboProprietario = !glb_modoComboProprietario;

    lblProprietariosPL.innerText = (glb_modoComboProprietario == true ? 'Propriet�rio' : 'Alternativo');

    carregaProprietarios();
}


function lblProprietariosPL_onmouseover() {
    lblProprietariosPL.style.color = 'blue';
    lblProprietariosPL.style.cursor = 'hand';
}

function lblProprietariosPL_onmouseout() {
    lblProprietariosPL.style.color = 'black';
    lblProprietariosPL.style.cursor = 'point';
}

function js_fg_MouseMove_Prg(grid, Button, Shift, X, Y) {

    var currRow = fg.MouseRow;
    var currCol = fg.MouseCol;
    var colDePara = null;
    var sHint = null;

    if (currRow > 0 && currCol > 0)
    {
        if (fg.ColKey(currCol) == 'CredVenc')
            colDePara = getColIndexByColKey(fg, 'CorPessoa');
    }

    if (colDePara != null)
    {
        sHint = fg.TextMatrix(currRow, colDePara);
        sHint = (sHint.substring(3, sHint.length));

        glb_aCelHint = [[currRow, currCol, sHint]];
    }

}