<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="pessoassup01Html" name="pessoassup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/pessoas/pessoassup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/pessoas/especificsup.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="pessoassup01Body" name="pessoassup01Body" LANGUAGE="javascript" onload="return window_onload()">
    
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(this), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        
		<p id="lblNome" name="lblNome" class="lblGeneral" >Nome</p>
		<input type="text" id="txtNome" name="txtNome" DATASRC="#dsoSup01" DATAFLD="Nome" class="fldGeneral"></input>
		<p id="lblFantasia" name="lblFantasia" class="lblGeneral">Fantasia/Apelido</p>
		<input type="text" id="txtFantasia" name="txtFantasia" DATASRC="#dsoSup01" DATAFLD="Fantasia" class="fldGeneral"></input>
        
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoPessoaID"></select>    
    </div>
    
    <!-- Primeiro div secundario superior - Pessoa Fisica -->    
    <div id="divSup02_01" name="divSup02_01" class="divExtern">
        <p id="lblHrPessoaFisica" name="lblHrPessoaFisica" class="lblGeneral">Pessoa F�sica</p>
        <hr id="hrPessoaFisica" id="hrPessoaFisica" class="lblGeneral">
		<p id="lblClassificacaoID" name="lblClassificacaoID" class="lblGeneral" title="Classifica��o da pessoa em rela��o ao mercado">Classifica��o</p>
		<select id="selClassificacaoID" name="selClassificacaoID" DATASRC="#dsoSup01" DATAFLD="ClassificacaoID" class="fldGeneral" title="Classifica��o da pessoa em rela��o ao mercado"></select>
		<p id="lblSexo" name="lblSexo" class="lblGeneral">Sexo</p>
		<select id="selSexo" name="selSexo" DATASRC="#dsoSup01" DATAFLD="Sexo" class="fldGeneral"></select>
		<p id="lblEstadoCivilID" name="lblEstadoCivilID" class="lblGeneral">Estado Civil</p>
		<select id="selEstadoCivilID" name="selEstadoCivilID" DATASRC="#dsoSup01" DATAFLD="EstadoCivilID" class="fldGeneral"></select>
		<p id="lblEducacaoID" name="lblEducacaoID" class="lblGeneral">Educa��o</p>
		<select id="selEducacaoID" name="selEducacaoID" DATASRC="#dsoSup01" DATAFLD="EducacaoID" class="fldGeneral"></select>
		<p id="lblDuracao" name="lblDuracao" class="lblGeneral">Dur</p>
		<input type="text" id="txtDuracao" name="txtDuracao" DATASRC="#dsoSup01" DATAFLD="Duracao" class="fldGeneral" title="Dura��o do curso (anos)"></input>
		<p id="lblConcluido" name="lblConcluido" class="lblGeneral">Con</p>
		<input type="text" id="txtConcluido" name="txtConcluido" DATASRC="#dsoSup01" DATAFLD="Concluido" class="fldGeneral" title="Anos concluidos"></input>
	    <p id="lblCursando" name="lblCursando" class="lblGeneral">Curs</p>
	    <input type="checkbox" id="chkCursando" name="chkCursando" DATASRC="#dsoSup01" DATAFLD="Cursando" class="fldGeneral" Title="Est� cursando?">
		<p id="lblPaisNascimentoID" name="lblPaisNascimentoID" class="lblGeneral">Pa�s de Nascimento</p>
		<select id="selPaisNascimentoID" name="selPaisNascimentoID" DATASRC="#dsoSup01" DATAFLD="PaisNascimentoID" class="fldGeneral"></select>
		<p id="lblCodigoUFNascimento" name="lblCodigoUFNascimento" class="lblGeneral">UF</p>
		<input type="text" id="txtCodigoUFNascimento" name="txtCodigoUFNascimento" DATASRC="#dsoSup01" DATAFLD="UFNascimento" class="fldGeneral"></input>
		<p id="lblCidadeNascimento" name="lblCidadeNascimento" class="lblGeneral">Cidade</p>
		<input type="text" id="txtCidadeNascimento" name="txtCidadeNascimento" DATASRC="#dsoSup01" DATAFLD="CidadeNascimento" class="fldGeneral"></input>
		<p id="lbldtNascimento" name="lbldtNascimento" class="lblGeneral">Nascimento</p>
		<input type="text" id="txtdtNascimento" name="txtdtNascimento" DATASRC="#dsoSup01" DATAFLD="V_dtNascimento" class="fldGeneral"></input>
		<p id="lbldtFalecimento" name="lbldtFalecimento" class="lblGeneral">Falecimento</p>
		<input type="text" id="txtdtFalecimento" name="txtdtFalecimento" DATASRC="#dsoSup01" DATAFLD="V_dtFalecimento" class="fldGeneral"></input>
		<p id="lblAscendencia" name="lblAscendencia" class="lblGeneral">Ascend�ncia</p>
		<input type="text" id="txtAscendencia" name="txtAscendencia" DATASRC="#dsoSup01" DATAFLD="Ascendencia" class="fldGeneral"></input>
		<p id="lblRacaID" name="lblRacaID" class="lblGeneral">Cor/Ra�a</p>
		<select id="selRacaID" name="selRacaID" DATASRC="#dsoSup01" DATAFLD="RacaID" class="fldGeneral"></select>
		<p id="lblDeficienciaID" name="lblDeficienciaID" class="lblGeneral">Defici�ncia</p>
		<select id="selDeficienciaID" name="selDeficienciaID" DATASRC="#dsoSup01" DATAFLD="DeficienciaID" class="fldGeneral"></select>
	    <p id="lblImagem_PF" name="lblImagem_PF" class="lblGeneral">Imagem</p>
	    <input type="checkbox" id="chkImagem_PF" name="chkImagem_PF" class="fldGeneral" Title="Tem imagem dispon�vel?">
        <p  id="lblCadastroOK2" name="lblCadastroOK2" class="lblGeneral">Cad</p>
        <input type="checkbox" id="chkCadastroOK2" name="chkCadastroOK2" DATASRC="#dsoSup01" DATAFLD="CadastroOK" class="fldGeneral" title="Cadastro OK?"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
		<input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
    </div>

    <!-- Segundo div secundario superior - Pessoa Jur�dica -->    
    <div id="divSup02_02" name="divSup02_02" class="divExtern">
        <p id="lblHrPessoaJuridica" name="lblHrPessoaJuridica" class="lblGeneral">Pessoa Jur�dica</p>
        <hr id="hrPessoaJuridica" name="hrPessoaJuridica" class="lblGeneral">
		<p id="lblJurClassificacaoID" name="lblJurClassificacaoID" class="lblGeneral" title="Classifica��o da pessoa em rela��o ao mercado">Classifica��o</p>
		<select id="selJurClassificacaoID" name="selJurClassificacaoID" DATASRC="#dsoSup01" DATAFLD="ClassificacaoID" class="fldGeneral" title="Classifica��o da pessoa em rela��o ao mercado"></select>
		<p id="lblJurSexo" name="lblJurSexo" class="lblGeneral">Tratamento</p>
		<select id="selJurSexo" name="selJurSexo" DATASRC="#dsoSup01" DATAFLD="Sexo" class="fldGeneral"></select>
		<p id="lblJurdtNascimento" name="lblJurdtNascimento" class="lblGeneral">Funda��o</p>
		<input type="text" id="txtJurdtNascimento" name="txtJurdtNascimento" DATASRC="#dsoSup01" DATAFLD="V_dtNascimento" class="fldGeneral"></input>
		<p id="lblJurdtFalecimento" name="lblJurdtFalecimento" class="lblGeneral">Encerramento</p>
		<input type="text" id="txtJurdtFalecimento" name="txtJurdtFalecimento" DATASRC="#dsoSup01" DATAFLD="V_dtFalecimento" class="fldGeneral"></input>
		<p id="lblCNAEJuridica" name="lblCNAEJuridica" class="lblGeneral">CNAE</p>
		<input type="text" id="txtCNAEJuridica" name="txtCNAEJuridica" DATASRC="#dsoSup01" DATAFLD="CNAE" title="Classifica��o Nacional de Atividade Econ�mica" class="fldGeneral"></input>
        <p  id="lblRodoviario" name="lblRodoviario" class="lblGeneral">Rodovi�rio</p>
        <input type="checkbox" id="chkRodoviario" name="chkRodoviario" DATASRC="#dsoSup01" DATAFLD="Rodoviario" class="fldGeneral" title="Efetua transporte Rodovi�rio?"></input>
        <p  id="lblAereo" name="lblAereo" class="lblGeneral">A�reo</p>
        <input type="checkbox" id="chkAereo" name="chkAereo" DATASRC="#dsoSup01" DATAFLD="Aereo" class="fldGeneral" title="Efetua transporte A�reo?"></input>
        <p  id="lblMaritimo" name="lblMaritimo" class="lblGeneral">Mar�timo</p>
        <input type="checkbox" id="chkMaritimo" name="chkMaritimo" DATASRC="#dsoSup01" DATAFLD="Maritimo" class="fldGeneral" title="Efetua transporte Mar�timo?"></input>
        <p  id="lblPP" name="lblPP" class="lblGeneral">PP</p>
        <input type="checkbox" id="chkPP" name="chkPP" DATASRC="#dsoSup01" DATAFLD="PP" class="fldGeneral" title="Porta a Porta"></input>
        <p  id="lblPT" name="lblPT" class="lblGeneral">PT</p>
        <input type="checkbox" id="chkPC" name="chkPC" DATASRC="#dsoSup01" DATAFLD="PC" class="fldGeneral" title="Porta a Container"></input>
        <p  id="lblPC" name="lblPC" class="lblGeneral">PC</p>
        <input type="checkbox" id="chkPT" name="chkPT" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PT" title="Porta a Transportadora"></input>
        <p  id="lblTP" name="lblTP" class="lblGeneral">TP</p>
        <input type="checkbox" id="chkTP" name="chkTP" DATASRC="#dsoSup01" DATAFLD="TP" class="fldGeneral" title="Transportadora a Porta"></input>
        <p  id="lblTT" name="lblTT" class="lblGeneral">TT</p>
        <input type="checkbox" id="chkTT" name="chkTT" DATASRC="#dsoSup01" DATAFLD="TT" class="fldGeneral" title="Transportadora a Transportadora"></input>
        <p  id="lblEhFabrica" name="lblEhFabrica" class="lblGeneral">F�b</p>
        <input type="checkbox" id="chkEhFabrica" name="chkEhFabrica" DATASRC="#dsoSup01" DATAFLD="EhFabrica" class="fldGeneral" title="� fabrica?"></input>
	    <p id="lblImagem_PJ" name="lblImagem_PJ" class="lblGeneral">Imagem</p>
	    <input type="checkbox" id="chkImagem_PJ" name="chkImagem_PJ" class="fldGeneral" Title="Tem imagem dispon�vel?">
        <p id="lblSimplesNacional" name="lblSimplesNacional" class="lblGeneral">SN</p> 
	    <input type="checkbox" id="chkSimplesNacional" name="chkSimplesNacional" DATASRC="#dsoSup01" DATAFLD="SimplesNacional" class="fldGeneral" Title="Optante pelo Simples Nacional?"> 
        <p  id="lblCadastroOK1" name="lblCadastroOK1" class="lblGeneral">Cad</p>
        <input type="checkbox" id="chkCadastroOK1" name="chkCadastroOK1" DATASRC="#dsoSup01" DATAFLD="CadastroOK" class="fldGeneral" title="Cadastro OK?"></input>
        <p id="lblReceitaBrutaSimplesNacional" name="lblReceitaBrutaSimplesNacional" class="lblGeneral">Receita Bruta SN</p>
	    <input type="text" id="txtReceitaBrutaSimplesNacional" name="txtReceitaBrutaSimplesNacional" DATASRC="#dsoSup01" DATAFLD="ReceitaBrutaSimplesNacional" class="fldGeneral" Title="Receita Bruta no Simples Nacional (Anual em R$)">
        <p id="lblEhContribuinte" name="lblEhContribuinte" class="lblGeneral" Title="� contribuinte de ICMS?">Cont</p> 
	    <input type="checkbox" id="chkEhContribuinte" name="chkEhContribuinte" DATASRC="#dsoSup01" DATAFLD="EhContribuinteICMS" class="fldGeneral" Title="� contribuinte de ICMS?"> 
        
		<!-- <p id="lblTipoEmpresaID" name="lblTipoEmpresaID" class="lblGeneral" title="teste">Tipo de Empresa</p> -->
		<!-- <select id="selTipoEmpresaID" name="selTipoEmpresaID" DATASRC="#dsoSup01" DATAFLD="TipoEmpresaID" class="fldGeneral" title=""></select> -->
		<p id="lblEnquadramentoEmpresaID" name="lblEnquadramentoEmpresaID" class="lblGeneral" title="">Enquadramento</p>
		<select id="selEnquadramentoEmpresaID" name="selEnquadramentoEmpresaID" DATASRC="#dsoSup01" DATAFLD="EnquadramentoEmpresaID" class="fldGeneral" title=""></select>
		<p id="lblRegimeTributarioID" name="lblRegimeTributarioID" class="lblGeneral" title="">Regime Tribut�rio</p>
		<select id="selRegimeTributarioID" name="selRegimeTributarioID" DATASRC="#dsoSup01" DATAFLD="RegimeTributarioID" class="fldGeneral"></select>

		<p id="lblBancoID" name="lblBancoID" class="lblGeneral">Banco</p>
		<select id="selBancoID" name="selBancoID" DATASRC="#dsoSup01" DATAFLD="BancoID" class="fldGeneral"></select>
		<p id="lblAgencia" name="lblAgencia" class="lblGeneral">Ag�ncia</p>
		<input type="text" id="txtAgencia" name="txtAgencia" DATASRC="#dsoSup01" DATAFLD="Agencia" class="fldGeneral"></input>
		<p id="lblDigitoAgencia" name="lblDigitoAgencia" class="lblGeneral">DV</p>
		<input type="text" id="txtDigitoAgencia" name="txtDigitoAgencia" DATASRC="#dsoSup01" DATAFLD="AgenciaDV" class="fldGeneral"></input>
		<p id="lblJurObservacao" name="lblJurObservacao" class="lblGeneral">Observa��o</p>
		<input type="text" id="txtJurObservacao" name="txtJurObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
		<p id="lblJurFaturamentoAnual" name="lblJurFaturamentoAnual" class="lblGeneral" title="Faturamento Anual:  Brasil(R$) Outros(US$)">Fat Anual</p>
		<input type="text" id="txtJurFaturamentoAnual" name="txtJurFaturamentoAnual" DATASRC="#dsoSup01" DATAFLD="FaturamentoAnual" class="fldGeneral" title="Faturamento Anual:  Brasil(R$) Outros(US$"></input>
		<p id="lblNumeroFuncionarios" name="lblNumeroFuncionarios" class="lblGeneral" title="N�mero de funcionarios">N�m Func</p>
		<input type="text" id="txtNumeroFuncionarios" name="txtNumeroFuncionarios" DATASRC="#dsoSup01" DATAFLD="NumeroFuncionarios" class="fldGeneral" title="N�mero de funcionarios"></input>
    </div>
    
    <!-- Terceiro div secundario superior - Grupo de Pessoas -->    
    <div id="divSup02_03" name="divSup02_03" class="divExtern">
        <p id="lblHrGrupoPessoas" name="lblHrGrupoPessoas" class="lblGeneral">Grupo de Pessoas</p>
        <hr id="hrGrupoPessoas" name="hrGrupoPessoas" class="lblGeneral">

		<p id="lblPercentualComissaoVendas" name="lblPercentualComissaoVendas" class="lblGeneral">CV</p>
		<input type="text" id="txtPercentualComissaoVendas" name="txtPercentualComissaoVendas" DATASRC="#dsoSup01" DATAFLD="PercentualComissaoVendas" class="fldGeneral" title="Percentual de Comiss�o de Vendas do Grupo"></input>
    </div>
</body>

</html>
