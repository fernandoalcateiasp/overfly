/********************************************************************
pessoasinf01.js

Library javascript para o pessoasinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Variavel contem o ultimo bot�o.
var glb_lastBtnClicked = null;
// Usada nas funcoes do grid de enderecos
var glb_ASSYNC_IN_CURSE = false;
// Variaveis usadas pelo Wizard
var glb_IsNewRecord = false;
var glb_aWizard = new Array();
var glb_nPointerWizard = 0;
var glb_lOperateWizard = false;

var glb_localTimerInt = null;
var glb_lastCGC_CPF = null;
var glb_currDate = null;
var glb_PessoasClassificacaoID = null;

var glb_AlteraUF = null;
var glb_Row = null;
var glb_EmpresaData = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dso01JoinSup = new CDatatransport('dso01JoinSup');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dso01Grid = new CDatatransport('dso01Grid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb02Grid_01 = new CDatatransport('dsoCmb02Grid_01');
var dsoCmb03Grid_01 = new CDatatransport('dsoCmb03Grid_01');
var dsoCmb04Grid_01 = new CDatatransport('dsoCmb04Grid_01');
var dso01GridLkp = new CDatatransport('dso01GridLkp');
var dso02GridLkp = new CDatatransport('dso02GridLkp');
var dso03GridLkp = new CDatatransport('dso03GridLkp');
var dso04GridLkp = new CDatatransport('dso04GridLkp');
var dsoParallelGrid = new CDatatransport('dsoParallelGrid');
var dso01PgGrid = new CDatatransport('dso01PgGrid');
var dsoCurrRelation = new CDatatransport('dsoCurrRelation');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoWizard = new CDatatransport('dsoWizard');
var dsoStateMachineLkp = new CDatatransport('dsoStateMachineLkp');
var dsoFiltroInf = new CDatatransport('dsoFiltroInf');
var dsoCmbsPropAlt = new CDatatransport('dsoCmbsPropAlt');
var dsoAddrData = new CDatatransport('dsoAddrData');
var dsoCmbRel = new CDatatransport('dsoCmbRel');
var dsoGen01 = new CDatatransport('dsoGen01');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoDocumentoPessoa = new CDatatransport('dsoDocumentoPessoa');
var dsoGravaConsultaSerasa = new CDatatransport('dsoGravaConsultaSerasa');
var dsoGravaConsultaSerasaDetalhes = new CDatatransport('dsoGravaConsultaSerasaDetalhes');
var dsoDeletaConsultaSerasa = new CDatatransport('dsoDeletaConsultaSerasa');
var dsoCredito = new CDatatransport('dsoCredito');

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)

    
FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG:
    js_fg_Pessoas_AfterEdit (Row, Col)
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();
    
    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 
    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', '']]],
                              [20110, [[0, 'dsoCmb01Grid_01', '*CodigoLocalidade2|fldValue', 'fldID'],
                                       [1, 'dsoCmb02Grid_01', 'ItemMasculino', 'ItemID'],
                                       [2, 'dsoCmb03Grid_01', '*CodigoLocalidade2|fldValue', 'fldID'],
                                       [0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', '']]],
                              [20111, [[3, 'dsoCmb01Grid_01', '*CodigoLocalidade2|Localidade', 'LocalidadeID'],
                                       [0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', '']]],
                              [20112, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
                                       [0, 'dso01GridLkp', '', '']]],
                              [32033, [[2, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
                                       [1, 'dsoCmb02Grid_01', 'Codigo|*Banco', 'BancoID']]],
                              [20113, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
                              [20114, [[4, 'dsoCmb01Grid_01', '*CodigoLocalidade2|Localidade', 'LocalidadeID']]],
                              [20115, [[0, 'dsoCmb01Grid_01', 'Imposto', 'ImpostoID']]],
                              [20122, [[0, 'dso01GridLkp', '', '']]],
                              [20123, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dsoStateMachineLkp', '', '']]],
                              [20124, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dsoStateMachineLkp', '', '']]],
                              [20125, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dsoStateMachineLkp', '', '']]],
                              [20126, [[0, 'dso01GridLkp', '', ''],
                                       [11, 'dsoCmb01Grid_01', 'fldName', 'fldID']]],
                              [20129, [[0, 'dsoStateMachineLkp', '', '']]],
                              [20135, [[0, 'dsoCmb01Grid_01', 'Fantasia', 'PessoaID']]],
                              [20137, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
                              [20138, [[7, 'dsoCmb01Grid_01', 'fldValue', 'fldID'],
                                       [0, 'dsoCmb02Grid_01', 'ItemMasculino', 'ItemID']]],
                              [29094, [[4, 'dsoCmb01Grid_01', 'NivelPagamento', 'NivelPagamento'],
                                       [5, 'dsoCmb02Grid_01', 'fldName', 'fldID'],
                                       [6, 'dsoCmb03Grid_01', 'fldName', 'fldID'],
                                       [7, 'dsoCmb04Grid_01', 'fldName', 'fldID']]]];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms( null,  ['divInf01_01',
                                 'divInf01_02',
                                 'divInf01_03',
                                 'divInf01_06'],
                                          [20008, 20009, 20106, 
                                          [20010, 20110,20111, 20112, 20113, 20114, 20115,
                                           20120, 20121, 20122, 20123, 20124, 20125, 20126, 20127, 20128, 20129, 20130, 20131, 20132, 32033, 20135, 20137, 20138, 29094]] );
    
    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

function setupPage()
{
    //@@ Ajustar os divs
    /*** Contrato Social - Div secundario inferior divInf01_03 ***/
    adjustElementsInForm([['lbldtUltimaAlteracao','txtdtUltimaAlteracao',14,1],
                          ['lblAlteracao','txtAlteracao',6,1],
                          ['lblCapitalSocial','txtCapitalSocial',14,1],
                          ['lblPatrimonioLiquido','txtPatrimonioLiquido',14,1],
                          ['lblFaturamentoAnual', 'txtFaturamentoAnual', 14, 1],
                          ['lblNumeroFuncionarios', 'txtNumeroFuncionarios', 6, 1, -15],
                          ['lblObjetoSocial','txtObjetoSocial',96,2]],null,'INF');

    glb_EmpresaData = getCurrEmpresaData();
}

// EVENTOS DO GRID FG ***********************************************

// Nota - evento particular desta pagina

function js_fg_Pessoas_AfterEdit (Row, Col)
{
    // Pasta de Enderecos
    if ( (keepCurrFolder() == 20111) && (Col == 4) )
    {
        fg.Editable = false;
        if (trimStr(fg.TextMatrix(Row, Col)) != '.')
			fg.TextMatrix(Row, Col) = (stripNonAlphaNumericChars(fg.TextMatrix(Row, Col))).toUpperCase();
		else
			fg.TextMatrix(Row, Col) = (fg.TextMatrix(Row, Col)).toUpperCase();

        fg.Editable = true;
    }    
}

// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{
    // 1. Se o grid ativo e o de enderecos, impede acesso
    // as celulas de UF e Cidade
    protectUFCity();
        
    // 2. A celula CEP so e acessavel se tiver pais selecionado    
    protectCEP();
}

function fg_DblClick_Prg()
{
    // Telefones
    if (keepCurrFolder() == 20112)
    {
        var sDDD = getCellValueByColKey(fg, 'DDD', fg.Row);
        var sNumero = getCellValueByColKey(fg, 'Numero', fg.Row);
        var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
        
        sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
    }
    else if (keepCurrFolder() == 20126)
    {
        sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL',
                        [fg.TextMatrix(fg.Row, 12), null, null]);
    }
    else if (keepCurrFolder() == 20132)
        startObsWindow_inf( fg, dso01Grid, 'Observacoes', getColIndexByColKey(fg, 'Observacoes') );
    else
    {
        // Invoca Outlook ou Internet Explorer p/ pasta de URLs 20113
        // a pasta e tratada direto na funcao
        invoqueOutLookOrIE();
    
        callCarrier(null);
    }
}

function callCarrier(btnClicked)
{
    // Se for pasta relacoes
    // Captura o id da relacao na coluna zero do grid
    // Manda mensagem
    var empresa = getCurrEmpresaData();
    
    // Pedidos - usa segundo botao
    if ( (keepCurrFolder() == 20120) && (fg.Rows > 1) && (btnIsEnableInCtrlBar('inf', 13)) ) 
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], fg.TextMatrix(fg.Row,1)));
    // Produtos - usa segundo botao
    else if ( (keepCurrFolder() == 20121) && (fg.Rows > 1) )  
    {
        if ( ((btnClicked==null)  && (btnIsEnableInCtrlBar('inf', 13))) || (btnClicked==2) )
        {
            openRelationByCarrier();
        }    
        else if ((btnIsEnableInCtrlBar('inf', 14)) && (btnClicked==3))
            sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], fg.TextMatrix(fg.Row,1)));
    }
    // Financeiro - usa segundo botao
    else if ( (keepCurrFolder() == 20122) && (fg.Rows > 1) ) 
    {
        if ( ((btnClicked==null) && (btnIsEnableInCtrlBar('inf', 13))) || (btnClicked==2) )
            sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(empresa[0], fg.TextMatrix(fg.Row,2)));
        else if ((btnIsEnableInCtrlBar('inf', 14)) && (btnClicked==3))
            sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], fg.TextMatrix(fg.Row,5)));
    }
    // Rel Entre Pessoas - usa terceiro botao
    else if ( (keepCurrFolder() == 20123) && (btnIsEnableInCtrlBar('inf', 14)) )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESSOAS', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
    // Rel com Recursos    
    else if ( (keepCurrFolder() == 20124) && (btnIsEnableInCtrlBar('inf', 14)) )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESREC', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));       
    // Rel Com Conceitos    
    else if ( (keepCurrFolder() == 20125) && (btnIsEnableInCtrlBar('inf', 14)) )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESCON', new Array(empresa[0], fg.TextMatrix(fg.Row, 0))); 
    else if ( (keepCurrFolder() == 20131) && (btnIsEnableInCtrlBar('inf', 12)) )
        sendJSCarrier(getHtmlId(), 'SHOWVISITAS', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));                 
}

function openRelationByCarrier()
{
    var aEmpresa = getCurrEmpresaData();
    // parametrizacao do dso dsoCurrRelation
    setConnection(dsoCurrRelation);
    
    dsoCurrRelation.SQL = 'SELECT RelacaoID ' +
                          'FROM RelacoesPesCon WITH(NOLOCK) ' +
                          'WHERE ObjetoID = ' + fg.TextMatrix(fg.Row,5) + ' AND ' +
                          'SujeitoID = ' + aEmpresa[0];
    dsoCurrRelation.ondatasetcomplete = dsoCurrRelation_DSC;
    dsoCurrRelation.Refresh();
}

function dsoCurrRelation_DSC() {
    var aEmpresa = getCurrEmpresaData();

    sendJSCarrier(getHtmlId(), 'SHOWRELPESCON', new Array(aEmpresa[0], dsoCurrRelation.recordset['RelacaoID'].value));
}

function fg_ChangeEdit_Prg()
{
    // Pasta de Enderecos
    if (keepCurrFolder() == 20111)
        addressOperations();
}

function fg_ValidateEdit_Prg()
{
    // Se for a Pasta de Enderecos: Preenche UF e cidade a partir de um CEP
    if ((keepCurrFolder() == 20111) && (!glb_ASSYNC_IN_CURSE))
        ufAndCityFromCEP();
    // Se for a Pasta de Enderecos: Preenche UF e cidade a partir de um CEP
    else if (keepCurrFolder() == 20113)
        selTipoURL();
}

function js_fg_BeforeRowColChange_Prg(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    if (keepCurrFolder() == 20110) {

        //Forca celula read-only
        if (!glb_GridIsBuilding) {
            var PlanoID = grid.TextMatrix(NewRow, getColIndexByColKey(fg, 'PesDocumentoID'));
            glb_Row = NewRow;

            if (PlanoID > 0) {
                colunareadonly(PlanoID);
                if ((glb_AlteraUF && NewCol == getColIndexByColKey(grid, 'UFID')) || (NewCol != getColIndexByColKey(grid, 'UFID')))
                    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
            }
        }
    }
    else
        js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
}

function fg_EnterCell_Prg()
{

}

function fg_MouseUp_Prg()
{

}

function fg_MouseDown_Prg()
{

}

function fg_BeforeEdit_Prg()
{

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
}

/********************************************************************
Criado pelo programador
Configura titulos e tracos do div Transportadora
Div secundario inferior divInf01_04

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function configTitleTrace(lbl, trace, lbl_cx, lbl_cy, lbl_width, trace_width)
{
    with (lbl.style)
    {
        backgroundColor = 'transparent';
        fontSize = '10pt';
        fontWeight = 'bold';
        width = lbl_width;
        left = lbl_cx;
        top =  lbl_cy;
        previousWidth_Title = parseInt(width, 10);
    }
    
    with (trace.style)
    {
        color = 'black';
        topLeft = parseInt(top, 10) + -5;
        height = 2;
        width = trace_width;
        left = lbl_cx + previousWidth_Title + (ELEM_GAP /2);
        top =  lbl_cy + ELEM_GAP - 2;
    }
}

/********************************************************************
Criado pelo programador
Configura labels dos checkboxes do div Transportadora
Div secundario inferior divInf01_04

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function configLbl(lbl, lbl_width, lbl_cx, lbl_cy)
{
    with (lbl.style)
    {
        backgroundColor = 'transparent';
        fontSize = '10pt';
        width = lbl_width;
        height = (LABEL_HEIGHT * 2) + 5;
        left = lbl_cx;
        top = lbl_cy;
    }
    var es = lbl.style;
    return ( new Array(parseInt(es.width, 10), parseInt(es.left, 10), parseInt(es.left, 10), (parseInt(es.top, 10) + ELEM_GAP)) );
}

/********************************************************************
Criado pelo programador
Configura checkboxes do div Transportadora
Div secundario inferior divInf01_04

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function configChkBox(chk, chk_cx, chk_cy)
{
    with (chk.style)
    {
        left = chk_cx - 5;
        top = chk_cy + (ELEM_GAP / 2);
        width = FONT_WIDTH * 3;
        height = FONT_WIDTH * 3;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    // Este form tem grids com paginacao
    glb_aFoldersPaging = [20120,20121,20122,20123,20124,20125];
    
    glb_pastaDefault = 20112;

}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    glb_lastBtnClicked = btnClicked;
    // controla o retorno do servidor dos dados de combos dinamicos
    
    var nEmpresaID, nPessoaID, nUsuarioID;

    nEmpresaID = getCurrEmpresaData()[0];
    nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
    nUsuarioID = getCurrUserID();
    
    // busca data corrente do servidor de paginas
	//dsoCurrData.URL = SYS_PAGESURLROOT + '/serversidegenEx/currdate.aspx?currDateFormat='+escape(DATE_FORMAT);
	//dsoCurrData.ondatasetcomplete = dsoCurrDataComplete_DSC;
	//dsoCurrData.Refresh();

    dsoCmbRelComplete();
}

function dsoCmbRelComplete() {
    if (! (dsoCurrData.recordset.BOF && dsoCurrData.recordset.EOF) )
        glb_currDate = dsoCurrData.recordset['currDate'].value;
        
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

    // Uso avancado, so roda uma vez
    if ( dsoCmbRel.recordset.fields.Count == 0 )
    {
        var formID = window.top.formID;  // variavel global do browser filho
        // passa o id do form por parametro
        var strPars = new String();
        strPars = '?';
        strPars += 'formID=';
        strPars += escape(formID.toString());

        dsoCmbRel.URL = SYS_ASPURLROOT + '/serversidegenEx/tiposrelacoes.aspx'+strPars;
	    dsoCmbRel.ondatasetcomplete = dsoCmbRelComplete_DSC;
	    dsoCmbRel.refresh();
    }    
    else
	    // Mover esta funcao para os finais retornos de operacoes no
	    // banco de dados
	    finalOfInfCascade(glb_lastBtnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    var i;
    
    showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    
    // Pasta de enderecos tem botao adicional
    if ( pastaID == 20111 )
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Preencher Cidade','Tabela de abrevia��es','','']);
    }
    // Pasta de telefones tem botao adicional
    else if ( pastaID == 20112 )
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Discar','','','']);
    }
    // Pasta de URLs tem botao adicional
    else if ( pastaID == 20113 )
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Detalha URL','','','']);
    }
    // Pedidos
    else if (pastaID == 20120)
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar','Detalhar Pedido','','']);
    }
    //Produtos
    else if (pastaID == 20121)
    {
        showBtnsEspecControlBar('inf', true, [1,1,1,0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar','Detalhar Produto','Detalhar Pedido','']);
    }
    //Financeiro
    else if (pastaID == 20122)
    {
        showBtnsEspecControlBar('inf', true, [1,1,1,0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar','Detalhar Financeiro','Detalhar Pedido','']);
    }
    
    //Atendimento
    else if (pastaID == 20132)
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Observa��es','','','']);
    }
    //Dados Banc�rios
    else if (pastaID == 32033) 
    {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Executar', 'Visualizar/Aplicar detalhes', '', '']);
    }
    // pasta de Rel entre pessoas/Rel com Recursos/Rel com Conceitos
    else if ( (pastaID == 20123) || (pastaID == 20124) || (pastaID == 20125) )
    {
        showBtnsEspecControlBar('inf', true, [1,1,1,0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar','Preenche combo','Detalhar Rela��o','']);
    }
    //Referencias Comerciais
    else if (pastaID == 20126)
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Preencher Empresa','Detalhar Refer�ncia','','']);
    }
    //Dados de transporte
    else if (pastaID == 20129)
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Preencher rotas','Clonar valores','','']);
    }
    //Visitas
    else if (pastaID == 20131)
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Visita','','','']);
    }
    //Parametros Marketplace
    else if (pastaID == 20137) {
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '']);
    }
    else if (pastaID = 29094) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 0]);
        tipsBtnsEspecControlBar('inf', ['Solicita��o de Cr�dito', 'Resumo', 'Detalhar Cr�dito', '']);
    }
    else if (pastaID != null)
    {
        showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    }

    // Nao mexer - Inicio de automacao ==============================
    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    // Mostra esconde campos condicionais
    if ( cmbID == 'selJurClassificacaoID' )
        showHideByClassificacao();
      
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Ao final das operacoes particulares
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{
    //@@
    // abre janela de verificacao de documento, se inclusao
    // no sup e contexto de pessoa fisica/juridica e pais da empresa 
    // corrente e o Brasil

    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') 
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC') )
    {      
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'adjustLabelsCombos(true)');
    }    

    if ( btnClicked == 'SUPINCL' )
    {
        var currContext = getCmbCurrDataInControlBar('sup', 1);
        
        if ( currContext[1] == 1211 )
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'openModalDocNumber()');    
            
        return null;    
    }    
    else if ( btnClicked == 'SUPALT' )
    {
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'lockCmbTipoPessoa()');    
        return null;   
    }
    
    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
    var nTipoPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPessoaID' + '\'' + '].value');
    var nPaisNascimentoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PaisNascimentoID' + '\'' + '].value');
    
    // Endereco
    if(folderID == 20111)
    {
        setupEspecBtnsControlBar('inf', 'DHDD');
    }
    // Telefones
    else if(folderID == 20112)
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // URLs
    else if(folderID == 20113)
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Pedidos
    else if(folderID == 20120)
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHDD');
        else
            setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Produtos/Financeiro
    else if( (folderID == 20121) || (folderID == 20122) )
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHHD');
        else
            setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Visitas
    else if(folderID == 20131)
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Atendimento
    else if(folderID == 20132)
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Rel Entre Pessoas, Rel com Recursos, Rel com Conceitos
    else if ( (folderID == 20123) || (folderID == 20124) || (folderID == 20125) )
    {
        // relacao nao pode ser incluida se o registro nao estiver ativo
	    var strBtns = currentBtnsCtrlBarString('inf');
        var aStrBtns = strBtns.split('');
	    
	    if ( nEstadoID != 2 )
            aStrBtns[2] = 'D';
        else    
            aStrBtns[2] = 'H';
            
        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window,'inf', strBtns);

        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDHD');
        else
            setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Ref Comerciais
    else if ( folderID == 20126 )
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'DHDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Dados de transporte
    else if ( folderID == 20129 )
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Param Marketplace
    else if (folderID == 20137) {
       setupEspecBtnsControlBar('inf', 'DDDD');
    }
    else if (folderID == 20138) {
        if (nPaisNascimentoID != null) {
            if (nPaisNascimentoID != 130) {
                setupEspecBtnsControlBar('inf', 'DDDDDDDDD');
            }
            else {
                setupEspecBtnsControlBar('inf', 'HHHHHHHHH');
            }
        }
    }
    else if (folderID == 29094) {
        if (fg.Rows == 3) {
            setupEspecBtnsControlBar('inf', 'HHHD');
        } else {
            setupEspecBtnsControlBar('inf', 'DDDD');
        }
    }

    
    if (glb_IsNewRecord)
    {
        // modificacao em 18/12/2001 - 30 milisec
        lockInterface(true);
        glb_localTimerInt = window.setInterval('fillWizard()', 30, 'JavaScript');
        return null;
    }
    
    //if ((nEstadoID == 2) && ((nTipoPessoaID==51) || (nTipoPessoaID==52)))
    if ( (nTipoPessoaID==51) || (nTipoPessoaID==52) )
    {
		/*
		if (nEstadoID == 2)
			setupEspecBtnsControlBar('sup', 'DHHHHHD');
		else
			setupEspecBtnsControlBar('sup', 'DHHDDHD');
		*/	
        if (nEstadoID == 2)
			setupEspecBtnsControlBar('sup', 'HDHHHHDHHH');
		else
			setupEspecBtnsControlBar('sup', 'HDHDHHDHHH');			
    }    
    else
        //setupEspecBtnsControlBar('sup', 'DDHDDHD');
        setupEspecBtnsControlBar('sup', 'DDDDDHDHDH');
    
    if (glb_lOperateWizard)
        setupControlBarToWizard();
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{
    var nEmpresaData;
    nEmpresaData = getCurrEmpresaData();
    var nIdiomaEmpresaID = nEmpresaData[8];

    // Pasta de documentos, seta o pais da empresa como default do documento
    if ( folderID == 20110 )
    {
        if (!((dso01GridLkp.recordset.BOF)&&(dso01GridLkp.recordset.EOF)))
		{
			dso01GridLkp.recordset.moveFirst();
			fg.TextMatrix(nLineInGrid, 0) = dso01GridLkp.recordset['PaisID'].value;
		}
		else
			fg.TextMatrix(nLineInGrid, 0) = nEmpresaData[1];

        // Merge de Colunas
        fg.MergeCells = 0;
    }
    
    // Pasta de enderecos vem com os 3 enderecos checados
    if ( folderID == 20111 )
    {
        fg.TextMatrix(nLineInGrid, 1) = 1;
        fg.TextMatrix(nLineInGrid, 2) = 1;
        
        if (!((dso03GridLkp.recordset.BOF)&&(dso03GridLkp.recordset.EOF)))
		{
			dso03GridLkp.recordset.moveFirst();
			fg.TextMatrix(nLineInGrid, 3) = dso03GridLkp.recordset['PaisID'].value;
		}
		else
			fg.TextMatrix(nLineInGrid,3) = nEmpresaData[1];

        // forca configuracao do controlbar inferior
        setupEspecBtnsControlBar('inf', 'HHDD');
        // coloca foco no campo de cep
        fg.Col = 4;
    }
 
    // Em urls: entra com TipoURLID = 124 (e-mail)
    if ( folderID == 20113 )
    {
        if (fg.TextMatrix(keepCurrLineInGrid(),0) = '124')           
        // coloca foco no campo URL (e-mail)(_@_.com.br)
        fg.TextMatrix(keepCurrLineInGrid(),2) = urlEmailTerminator(nIdiomaEmpresaID, 124);
        
        fg.Col = 2;

    }    

    // Em formacoes: entra com PaisID da Empresa Logada
    if ( folderID == 20114 )
    {
       nEmpresaData = getCurrEmpresaData();
       fg.TextMatrix(keepCurrLineInGrid(),4) = nEmpresaData[1];
    }    

    // Em telefones entra com o DDD/DDI do Pais/Cidade do Endere�o da Pessoa     
    if ( (folderID == 20112)&&
        (!((dso01GridLkp.recordset.BOF)&&(dso01GridLkp.recordset.EOF))))
    {
        dso01GridLkp.recordset.moveFirst();
        while (!dso01GridLkp.recordset.EOF)
        {
            var nCol = (dso01GridLkp.recordset['TipoDDDI'].value == 'DDI' ? 2 : 3);
            fg.TextMatrix(keepCurrLineInGrid(),nCol) = dso01GridLkp.recordset['DDDI'].value;
            dso01GridLkp.recordset.moveNext();
        }
        // seleciona TipoTelefoneID = 119 (Comercial)
        // coloca foco no campo Numero
        fg.TextMatrix(keepCurrLineInGrid(),0) = 119;
        fg.Col = 4;
        
    }       
    
    // Rel Entre Pessoas, Rel com Recursos, Rel com Conceitos
    if ( (folderID == 20123) || (folderID == 20124) || (folderID == 20125) )
        setupEspecBtnsControlBar('inf', 'DHDD');
        
    // Referencias Comerciais
    if (folderID == 20126)
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
        // Insere o dia corrente no grid
        if (glb_currDate != null)
        {
            fg.TextMatrix(fg.Row, 0) = glb_currDate;
        }
    }    
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    var empresa = getCurrEmpresaData();
    var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
    
    // Usuario apertou o botao 1 - pasta de enderecos - 20111
    if ( (keepCurrFolder() == 20111) && (btnClicked == 1) )
        openModalCity();

    else if ( (keepCurrFolder() == 20111) && (btnClicked == 2) )
		window.top.openModalControleDocumento('I', '', 72001, null, null, 'T');

    else if ((keepCurrFolder() == 29094) && (btnClicked == 1))
        openModalSolicitacaoCredito();

    else if ((keepCurrFolder() == 29094) && (btnClicked == 2))
        window.top.openModalHTML(-nPessoaID, 'Resumo do Cr�dito', 'I', 12);

    else if ((keepCurrFolder() == 29094) && (btnClicked == 3))
    {
        detalhaCredito(nPessoaID, false);
    }

    // Usuario apertou o botao 1 - pasta de telefones - 20112
    else if ( (keepCurrFolder() == 20112) && (btnClicked == 1) )        
        fg_DblClick_Prg();

    // Usuario apertou o botao 1 - pasta de URLs - 20113
    else if ( (keepCurrFolder() == 20113) && (btnClicked == 1) )
        invoqueOutLookOrIE();

    // Usuario apertou o botao 2 - pasta de Rel Pes/Rec/Con - 20123/20124/20125
    if ( ( (keepCurrFolder() == 20123)||(keepCurrFolder() == 20124)||(keepCurrFolder() == 20125) ) 
        && (btnClicked == 2) )
        openModalRelacoes();

    // Usuario apertou o botao 1 - pasta de Rel Pes/Rec/Con - 20123/20124/20125
    if ( ((keepCurrFolder() == 20120)||(keepCurrFolder() == 20121)||(keepCurrFolder() == 20122)||
          (keepCurrFolder() == 20123)||(keepCurrFolder() == 20124)||(keepCurrFolder() == 20125)) && (btnClicked == 1) )
    {
        // Invoca janela modal
        loadModalOfPagingPesq();
        return null;
    }
    if ( (keepCurrFolder() == 20120) || (keepCurrFolder() == 20121) || 
         (keepCurrFolder() == 20122) )
        callCarrier(btnClicked);
    else if ( (keepCurrFolder() == 20123) && (btnClicked == 3) )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESSOAS', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));       
    else if ( (keepCurrFolder() == 20124) && (btnClicked == 3) )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESREC', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));       
    else if ( (keepCurrFolder() == 20125) && (btnClicked == 3) )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESCON', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
    else if (keepCurrFolder() == 20126)
    {
        if (btnClicked == 1)
            openModalEmpresas();
        else if (btnClicked == 2)
            sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL',
                    [fg.TextMatrix(fg.Row, 12), null, null]);

        // sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], fg.TextMatrix(fg.Row, 12)));
    }    
    else if (keepCurrFolder() == 20129)
    {
        if (btnClicked == 1)
            openModalRotas();
        else if (btnClicked == 2)
            openModalClonarRotas();
	}
	else if ( (keepCurrFolder() == 20131) && (btnClicked == 1) )
        sendJSCarrier(getHtmlId(), 'SHOWVISITAS', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));       

    // Atendimento
    else if ( (keepCurrFolder() == 20132) && (btnClicked == 1) )
    {        
        startObsWindow_inf( fg, dso01Grid, 'Observacoes', getColIndexByColKey(fg, 'Observacoes') );
    }     
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    if ( btnClicked == 'INFOK' )
    {
        if ( (keepCurrFolder() == 20111) && (glb_ASSYNC_IN_CURSE) )
            return false;
            
		// Pasta de contrato social
        if ( keepCurrFolder() == 20106 )
		{
			var nFuncionario;
			var nTipoPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'TipoPessoaID' + '\'' + '].value');
			
			nFuncionario = txtNumeroFuncionarios.value;

			if ((nFuncionario == '') || (nFuncionario == 0))
			{
			    window.top.overflyGen.Alert('Favor preencher o n�mero de funcion�rios');
			    txtNumeroFuncionarios.value = (dso01JoinSup.recordset['NumeroFuncionarios'].value == null ? '' : dso01JoinSup.recordset['NumeroFuncionarios'].value);
			    return true;
            }

            // Coloca ID do usuario logado no campo UsuarioID
            // ao gravar registro da pasta de contrato social
            dso01JoinSup.recordset['UsuarioID'].value = getCurrUserID();

		}

        // Pasta de Enderecos
        if ( keepCurrFolder() == 20111 )
        {
            fg.Editable = false;
            if (trimStr(fg.TextMatrix(fg.Row, 4)) != '.')
				fg.TextMatrix(fg.Row, 4) = (stripNonAlphaNumericChars(fg.TextMatrix(fg.Row, 4))).toUpperCase();
			else
				fg.TextMatrix(fg.Row, 4) = (fg.TextMatrix(fg.Row, 4)).toUpperCase();
        }        
    
        // Documentos
        if ( keepCurrFolder() == 20110 )
        {
            // verifica o numero do documento e
            // ao final chama a automacao, se for o caso
                    
            verifyDocNumber();
                    
            // para a automacao
            return false;
        }
        // URLS - N�o pode haver duplicidade de e-mails de contatos. LFS 23/05/2011
        if (keepCurrFolder() == 20113) {
            verifyUrl();

            // para a automacao
            return false;
        }
    }

    if ( btnClicked == 'INFEXCL' )
    {
        if ( keepCurrFolder() == 20110 )
        {
            var nTipoDocumentoID = parseInt(fg.TextMatrix(fg.Row, 1), 10);
            if ( (nTipoDocumentoID == 101) || 
                 (nTipoDocumentoID == 111) )
            {
                if ( window.top.overflyGen.Alert ('Este documento n�o pode ser deletado!') == 0 )
                    return null;
                return true;
            }                 
        }
    }

    if( (btnClicked == 'INFAVANC')||(btnClicked == 'INFRETRO') )
    {
        controlWizard(btnClicked);
        return false;
    }

    if ((keepCurrFolder() == 20110) && (fg.Row > 0)) {
        fg.Col = getColIndexByColKey(fg, 'PesDocumentoID');
    }
    if ((keepCurrFolder() == 20138) && (fg.Row > 0)) {
        fg.Col = getColIndexByColKey(fg, 'PesCertidaoID');
    }
    
    // Para prosseguir a automacao retornar null
    return null;
}
/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALCLONARROTASHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    if ( (keepCurrFolder() == 20111) && 
         (idElement.toUpperCase() == 'MODALENDERECOHTML') )
    {
        if ( param1 == 'OK' )                
        {
            // Escreve UF, cidade e CEP no grid de enderecos
            writeAddressInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            if (fg.disabled == false)
            {
                window.focus();
                fg.focus();
            }    
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            if (fg.disabled == false)
            {
                window.focus();
                fg.focus();
            }    
            return 0;
        }
    }

    if ( (keepCurrFolder() == 20111) && 
         (idElement.toUpperCase() == 'MODALCHOOSECEPHTML') )
    {
        if ( param1 == 'OK' )                
        {
            // Escreve UF, cidade e CEP no grid de enderecos
            writeAddressInGridEx(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            glb_ASSYNC_IN_CURSE = true;
            fg.Col = 7;
            if (fg.disabled == false)
            {
                window.focus();
                fg.focus();
            }
            glb_ASSYNC_IN_CURSE = false;
            
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            glb_ASSYNC_IN_CURSE = true;
            fg.Col = 4;
            if (fg.disabled == false)
            {
                window.focus();
                fg.focus();
            }
            glb_ASSYNC_IN_CURSE = false;
            
            return 0;
        }
    }
    
    if ( ( (keepCurrFolder() == 20123)||(keepCurrFolder() == 20124)||(keepCurrFolder() == 20125) ) 
            && (idElement.toUpperCase() == 'MODALRELACOESHTML') )
    {
        if ( param1 == 'OK' )                
        {
            // Escreve ID, Label e TipoRelacaoID no grid de relacoes
            writeSujObjInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    
    if (keepCurrFolder() == 20126)
    {
        if ( param1 == 'OK' )
        {
            // Escreve ID, Label e TipoRelacaoID no grid de relacoes
            fg.TextMatrix(fg.Row, 1) = param2[1];
            fg.TextMatrix(fg.Row, 12) = param2[0];
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    if ((keepCurrFolder() == 20132) && (idElement.toUpperCase() == 'MODALOBSERVACAOHTML'))
    {
        if ( param1 == 'OK' )
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    
    if (idElement.toUpperCase() == 'MODALROTASHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    // Janela de pesquisa para paging
    if (idElement.toUpperCase() == 'MODALOFPAGINGINFHTML')
        execPesqInPaging(param1, param2);


    if ((keepCurrFolder() == 29094) &&
        (idElement.toUpperCase() == 'MODALSOLICITACAOCREDITOHTML')) {
        if (param1 == 'OK') {
            // Escreve UF, cidade e CEP no grid de enderecos
            writeAddressInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            if (fg.disabled == false) {
                window.focus();
                fg.focus();
            }
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            if (fg.disabled == false) {
                window.focus();
                fg.focus();
            }
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{
    // Rel.pessoas/Rel Recursos/Rel Conceitos
    if ((keepCurrFolder() == 20123)||(keepCurrFolder() == 20124)
         ||(keepCurrFolder() == 20125))
        setupEspecBtnsControlBar('inf', 'DHDD');
    // Enderecos / Referencias Comerciais
    else if ((folderID == 20111) || (folderID == 20126))
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHDD');
        else
            setupEspecBtnsControlBar('inf', 'DHDD');
    }
    else if (folderID == 20110)
    {
        // Merge de Colunas
        fg.MergeCells = 0;

        glb_AlteraUF = true;
    }
    else if (folderID == 20138)
    {
        // Merge de Colunas
        fg.MergeCells = 0;

        glb_AlteraUF = true;
    }        
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{
    if (folderID == 20106) 
    {
        direitoCampos();
        txtdtUltimaAlteracao.readOnly = txtAlteracao.readOnly = true;
    }
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.

    // Inicializacao de variavel de grid em modo de paginacao
    // Pedidos    
    if (folderID == 20120)
        glb_scmbKeyPesqSel = 'adtMovimento';
    else if (folderID == 20121)
        glb_scmbKeyPesqSel = 'cdtMovimento';
    else if (folderID == 20122)
        glb_scmbKeyPesqSel = 'adtVencimento';
    else if ( (folderID == 20123) || (folderID == 20124) || (folderID == 20125) )
        glb_scmbKeyPesqSel = 'aRelacaoID';

    if ( (folderID != 20123) && (folderID != 20124) && (folderID != 20125) )
        return true;
        
    var nClassificacaoID, nCurrRegID, nCurrTipoRegID;
    var aCmbTipoRelacaoText = new Array();
    var aCmbTipoRelacaoValue = new Array();
    var sDefaultRel = '';
    var nRelacaoEntreID;
    var i = 0;
    
    // Preenche o combo de filtros com as relacoes possiveis
    if (folderID == 20123) //Rel entre Pessoas
    {
        cleanupSelInControlBar('inf',2);
        nCurrRegID = getCurrDataInControl('sup','txtRegistroID');
        nCurrTipoRegID = getCurrDataInControl('sup','selTipoRegistroID');
        if (nCurrTipoRegID == 51)
            nClassificacaoID = getCurrDataInControl('sup','selClassificacaoID');
        else
            nClassificacaoID = getCurrDataInControl('sup','selJurClassificacaoID');

        if ( ! ((dsoCmbRel.recordset.BOF) && (dsoCmbRel.recordset.EOF)) )
        {
            dsoCmbRel.recordset.moveFirst();
            if (! dsoCmbRel.recordset.EOF )
            {
                while (!dsoCmbRel.recordset.EOF)
                {
                    if ( (nClassificacaoID != 67) && (nClassificacaoID != 68) && 
                         (nClassificacaoID != 69) && (nClassificacaoID != 70) )
                    {
                        if ( (dsoCmbRel.recordset['TipoPossivel'].value == nCurrTipoRegID) &&
                             (dsoCmbRel.recordset['RelacaoEntreID'].value == 133) &&
                             ! ( ((dsoCmbRel.recordset['TipoRelacaoID'].value == 22) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 23) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 24) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 25) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ))
                        {
                            if ( ascan(aCmbTipoRelacaoText,dsoCmbRel.recordset['NomePasta'].value,false) < 0 )
                            {
                                aCmbTipoRelacaoText[i] = dsoCmbRel.recordset['NomePasta'].value;
                                aCmbTipoRelacaoValue[i] = [dsoCmbRel.recordset['TipoRelacaoID'].value,
                                                           dsoCmbRel.recordset['Tipo'].value,
                                                           dsoCmbRel.recordset['Header'].value,
                                                           dsoCmbRel.recordset['RelacaoEntreID'].value];

                                if ( (dsoCmbRel.recordset['EhDefault'].value) && (dsoCmbRel.recordset['Tipo'].value == 'OBJETO') )
                                    sDefaultRel = (aCmbTipoRelacaoValue[i]).toString();

                                i++;
                            }
                        }
                    }
                    else if (nClassificacaoID == 67) // Assistencia Tecnica
                    {
                        if ( (dsoCmbRel.recordset['TipoPossivel'].value == nCurrTipoRegID) &&
                             (dsoCmbRel.recordset['RelacaoEntreID'].value == 133) &&
                             ! ( ((dsoCmbRel.recordset['TipoRelacaoID'].value == 23) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 24) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 25) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ))
                        {
                            if ( ascan(aCmbTipoRelacaoText,dsoCmbRel.recordset['NomePasta'].value,false) < 0 )
                            {
                                aCmbTipoRelacaoText[i] = dsoCmbRel.recordset['NomePasta'].value;
                                aCmbTipoRelacaoValue[i] = [dsoCmbRel.recordset['TipoRelacaoID'].value,
                                                           dsoCmbRel.recordset['Tipo'].value,
                                                           dsoCmbRel.recordset['Header'].value,
                                                           dsoCmbRel.recordset['RelacaoEntreID'].value];
                                                           
                                if ( (dsoCmbRel.recordset['EhDefault'].value) && (dsoCmbRel.recordset['Tipo'].value == 'OBJETO') )
                                    sDefaultRel = (aCmbTipoRelacaoValue[i]).toString();

                                i++;
                            }
                        }
                    }
                    else if (nClassificacaoID == 68) // Transportadora
                    {
                        if ( (dsoCmbRel.recordset['TipoPossivel'].value == nCurrTipoRegID) &&
                             (dsoCmbRel.recordset['RelacaoEntreID'].value == 133) &&
                             ! ( ((dsoCmbRel.recordset['TipoRelacaoID'].value == 22) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 24) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 25) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ))
                        {
                            if ( ascan(aCmbTipoRelacaoText,dsoCmbRel.recordset['NomePasta'].value,false) < 0 )
                            {
                                aCmbTipoRelacaoText[i] = dsoCmbRel.recordset['NomePasta'].value;
                                aCmbTipoRelacaoValue[i] = [dsoCmbRel.recordset['TipoRelacaoID'].value,
                                                           dsoCmbRel.recordset['Tipo'].value,
                                                           dsoCmbRel.recordset['Header'].value,
                                                           dsoCmbRel.recordset['RelacaoEntreID'].value];

                                if ( (dsoCmbRel.recordset['EhDefault'].value) && (dsoCmbRel.recordset['Tipo'].value == 'OBJETO') )
                                    sDefaultRel = (aCmbTipoRelacaoValue[i]).toString();

                                i++;
                            }
                        }
                    }
                    else if (nClassificacaoID == 69) // Banco
                    {
                        if ( (dsoCmbRel.recordset['TipoPossivel'].value == nCurrTipoRegID) &&
                             (dsoCmbRel.recordset['RelacaoEntreID'].value == 133) &&
                             ! ( ((dsoCmbRel.recordset['TipoRelacaoID'].value == 22) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 23) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 25) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ))
                        {
                            if ( ascan(aCmbTipoRelacaoText,dsoCmbRel.recordset['NomePasta'].value,false) < 0 )
                            {
                                aCmbTipoRelacaoText[i] = dsoCmbRel.recordset['NomePasta'].value;
                                aCmbTipoRelacaoValue[i] = [dsoCmbRel.recordset['TipoRelacaoID'].value,
                                                           dsoCmbRel.recordset['Tipo'].value,
                                                           dsoCmbRel.recordset['Header'].value,
                                                           dsoCmbRel.recordset['RelacaoEntreID'].value];
                                
                                if ( (dsoCmbRel.recordset['EhDefault'].value) && (dsoCmbRel.recordset['Tipo'].value == 'OBJETO') )
                                    sDefaultRel = (aCmbTipoRelacaoValue[i]).toString();

                                i++;
                            }
                        }
                    }
                    else if (nClassificacaoID == 70) // Servicos
                    {
                        if ( (dsoCmbRel.recordset['TipoPossivel'].value == nCurrTipoRegID) &&
                             (dsoCmbRel.recordset['RelacaoEntreID'].value == 133) &&
                             ! ( ((dsoCmbRel.recordset['TipoRelacaoID'].value == 22) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 23) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ||
                                 ((dsoCmbRel.recordset['TipoRelacaoID'].value == 24) && (dsoCmbRel.recordset['Tipo'].value == 'SUJEITO')) ))
                        {
                            if ( ascan(aCmbTipoRelacaoText,dsoCmbRel.recordset['NomePasta'].value,false) < 0 )
                            {
                                aCmbTipoRelacaoText[i] = dsoCmbRel.recordset['NomePasta'].value;
                                aCmbTipoRelacaoValue[i] = [dsoCmbRel.recordset['TipoRelacaoID'].value,
                                                           dsoCmbRel.recordset['Tipo'].value,
                                                           dsoCmbRel.recordset['Header'].value,
                                                           dsoCmbRel.recordset['RelacaoEntreID'].value];

                                if ( (dsoCmbRel.recordset['EhDefault'].value) && (dsoCmbRel.recordset['Tipo'].value == 'OBJETO') )
                                    sDefaultRel = (aCmbTipoRelacaoValue[i]).toString();

                                i++;
                            }
                        }
                    }
                                        
                    dsoCmbRel.recordset.MoveNext();
                }
            }
        }

        if (aCmbTipoRelacaoText.length > 0)
        {
            for (i=0; i<aCmbTipoRelacaoText.length; i++)
            {
                addOptionToSelInControlBar('inf', 2, aCmbTipoRelacaoText[i], (aCmbTipoRelacaoValue[i]).toString() );
            }
        }
        if (! selOptByValueOfSelInControlBar('inf', 2, sDefaultRel))
            if (aCmbTipoRelacaoValue.length > 0)
                selOptByValueOfSelInControlBar('inf', 2, (aCmbTipoRelacaoValue[0]).toString());
        return false;
    }
    else if ( ( folderID == 20124) || (folderID == 20125) ) //Rel com Conceitos/Rel com Recursos
    {
        cleanupSelInControlBar('inf',2);
        nCurrRegID = getCurrDataInControl('sup','txtRegistroID');
        nCurrTipoRegID = getCurrDataInControl('sup','selTipoRegistroID');
        
        if (folderID == 20124)
           nRelacaoEntreID = 132;
        else if (folderID == 20125)
           nRelacaoEntreID = 135;   
           
        if ( ! ((dsoCmbRel.recordset.BOF) && (dsoCmbRel.recordset.EOF)) )
        {
            dsoCmbRel.recordset.moveFirst();
            if (! dsoCmbRel.recordset.EOF )
            {
                while (!dsoCmbRel.recordset.EOF)
                {
                    if ( (dsoCmbRel.recordset['TipoPossivel'].value == nCurrTipoRegID) &&
                         (dsoCmbRel.recordset['RelacaoEntreID'].value == nRelacaoEntreID) )
                    {
                        if ( ascan(aCmbTipoRelacaoText,dsoCmbRel.recordset['NomePasta'].value,false) < 0 )
                        {
                            aCmbTipoRelacaoText[i] = dsoCmbRel.recordset['NomePasta'].value;
                            aCmbTipoRelacaoValue[i] = [dsoCmbRel.recordset['TipoRelacaoID'].value,
                                                       dsoCmbRel.recordset['Tipo'].value,
                                                       dsoCmbRel.recordset['Header'].value,
                                                       dsoCmbRel.recordset['RelacaoEntreID'].value];

                            if ( (dsoCmbRel.recordset['EhDefault'].value) && (dsoCmbRel.recordset['Tipo'].value == 'OBJETO') )
                                sDefaultRel = (aCmbTipoRelacaoValue[i]).toString();

                            i++;
                        }
                    }
                    dsoCmbRel.recordset.MoveNext();
                }
            }
        }

        if (aCmbTipoRelacaoText.length > 0)
        {
            for (i=0; i<aCmbTipoRelacaoText.length; i++)
            {
                addOptionToSelInControlBar('inf', 2, aCmbTipoRelacaoText[i], (aCmbTipoRelacaoValue[i]).toString() );
            }
        }
        
        if (! selOptByValueOfSelInControlBar('inf', 2, sDefaultRel))
            if (aCmbTipoRelacaoValue.length > 0)
                selOptByValueOfSelInControlBar('inf', 2, (aCmbTipoRelacaoValue[0]).toString());
        
        return false;
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{
    // este form usa o combo de filtros para outras finalidades
    // desta forma o combo deve ser preenchido aqui e nao
    // na funcao prgInterfaceInf
    if ( !((optValue == 20123) || (optValue == 20124) || (optValue == 20125)) )
        refillCmb2Inf(optValue);

    // Uso avancado, so roda uma vez
    if ( ((optValue == 20123) || (optValue == 20124) || (optValue == 20125)) )
    {
        if ( dsoCmbRel.recordset.fields.Count == 0 )
        {
            var formID = window.top.formID;  // variavel global do browser filho
            // passa o id do form por parametro
            var strPars = new String();
            strPars = '?';
            strPars += 'formID=';
            strPars += escape(formID.toString());

            dsoCmbRel.URL = SYS_ASPURLROOT + '/serversidegenEx/tiposrelacoes.aspx'+strPars;
	        dsoCmbRel.ondatasetcomplete = dsoCmbRelComplete_1_DSC;
	        dsoCmbRel.refresh();
            // Para a automacao
            return true;
        }
    }

    //@@ retornar null para prosseguir a automacao
    return null;
}

function dsoCmbRelComplete_1_DSC() {
    setCurrDSOByCmb1(keepCurrFolder(), true);
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    // Pasta de LOG, Pedidos, Produtos, Financeiro, Empresas, Contatos Read Only
    if ( (folderID == 20010) || (folderID == 20120) || (folderID == 20121) || 
         (folderID == 20122) || (folderID == 20127) || (folderID == 20128) || 
         (folderID == 20131) || (folderID == 20132) )
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // As pastas de Relacoes so habilita o botao de incluir
    else if ( (folderID == 20123) || (folderID == 20124) || (folderID == 20125) )
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else if (folderID == 29094)
    {
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}
/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
    
}

/********************************************************************
Funcao criada pelo programador.
Usada pela funcao fg_AfterRowColChange_Prg()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function protectUFCity()
{
    // Pasta corrente precisa ser Endereco - 20111
    if ( keepCurrFolder() == 20111 )
    {
        if ( fg.Col == 5 )
            fg.Col = 4;
        else if ( fg.Col == 6 )    
            fg.Col = 7;
    }
}

/********************************************************************
Funcao criada pelo programador.
Usada pela funcao fg_AfterRowColChange_Prg()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function protectCEP()
{
    // Pasta corrente precisa ser Endereco - 20111
    if ( keepCurrFolder() == 20111 )
        if ( (fg.Col == 4) && (fg.TextMatrix(keepCurrLineInGrid(),3) == '') )
            fg.Col = 3;
}

/********************************************************************
Funcao criada pelo programador.
Usada pela funcao fg_DblClick_Prg()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function invoqueOutLookOrIE()
{
    var currDSO;
    
    // Vale para pastas URLs - 20113
    if ( (! fg.Editable) && (keepCurrFolder() == 20113) )
    {
        currDSO = keepCurrDso();
        
        if ( (currDSO.recordset.BOF) && (currDSO.recordset.EOF) )
            return null;
        
        currDSO.recordset.moveFirst();
        
        var currID = fg.TextMatrix(keepCurrLineInGrid(),fg.Cols-1);
        
        currDSO.recordset.find('PesURLID ', currID);
        
        if ( (currDSO.recordset.BOF) || (currDSO.recordset.EOF) )
            return null;
        
        var sURL = currDSO.recordset['URL'].value;
        
        if ( currDSO.recordset['TipoURLID'].value == 125 ) // site
            window.open(sURL);
        else if ( currDSO.recordset['TipoURLID'].value == 124 ) // e-mail
            window.location = 'mailto:' + sURL;
            
        else if (currDSO.recordset['TipoURLID'].value == 127) // Dominio
            window.open(sURL);
     

    }
}    

/********************************************************************
Funcao criada pelo programador.
Usada pela funcao execChangeEdit_Prg()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function addressOperations()
{
    var currDSO;

    // IMPORTANTE:
    // 1. Se a pasta corrente e a pasta de Enderecos - 20111
    // esta em modo de edicao e a linha corrente e a linha de edicao
    
    // 1.1. Habilita/desabilita o botao de pesquisa de Cidade no control bar inferior
    // se um Pais for selecionado
    if ( (fg.Editable) &&
         fg.Row == keepCurrLineInGrid() &&
         fg.Col == 3 &&
         (fg.EditText != '') )
        adjustControlBarEx(window,'inf',  'DDDDDDHHDDDHHDD');
    else if ( (fg.Editable) &&
         fg.Row == keepCurrLineInGrid() &&
         (fg.TextMatrix(keepCurrLineInGrid(), 3) != '') )
        adjustControlBarEx(window,'inf',  'DDDDDDHHDDDHHDD');
    else
        adjustControlBarEx(window,'inf',  'DDDDDDHHDDDDHDD');
        
    // 1.2. Se o usuario troca o pais, limpa as celulas de CEP, UF e Cidade.
    if ( (fg.Editable) && 
         fg.Row == keepCurrLineInGrid() &&
         fg.Col == 3 &&
         (fg.EditText != '') )
    {  
        // obter o LocalidadeID do pais selecionado
        currDSO = dsoCmb01Grid_01;
        
        var temp = currDSO.recordset.Bookmark();
        
        currDSO.recordset.moveFirst();
        currDSO.recordset.find('CodigoLocalidade2' ,fg.EditText);

        if (!currDSO.recordset.EOF)
        {
            if ( currDSO.recordset['LocalidadeID'].value != fg.TextMatrix(keepCurrLineInGrid(),3) )
            {
                fg.TextMatrix(keepCurrLineInGrid(),4) = '';
                fg.TextMatrix(keepCurrLineInGrid(),5) = '';
                fg.TextMatrix(keepCurrLineInGrid(),6) = '';
                fg.TextMatrix(keepCurrLineInGrid(),12) = '';
                fg.TextMatrix(keepCurrLineInGrid(),13) = '';
            }
        }
        
        currDSO.recordset.gotoBookmark(temp);
    }    
}

/********************************************************************
Funcao criada pelo programador.
Usada pela funcao execValidadeEdit_Prg()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function ufAndCityFromCEP()
{
    // IMPORTANTE:
    // 1. Se a pasta corrente e a pasta de Enderecos - 20111
    // esta em modo de edicao e a linha corrente e a linha de edicao
    
    // 1.1. Se o usuario digitar um CEP valido o sistema preenche as celulas
    // de UF e Cidade.
    // Se o CEP nao for valido as celulas de UF e Cidade ficam em branco.
    if ( (fg.Editable) &&
         (keepCurrFolder() == 20111) &&
         fg.Row == keepCurrLineInGrid() &&
         fg.Col == 4 &&
         (fg.EditText != '') )
    {  
        fg.TextMatrix(keepCurrLineInGrid(),5) = '';
        fg.TextMatrix(keepCurrLineInGrid(),6) = '';
        fg.TextMatrix(keepCurrLineInGrid(),12) = '';
        fg.TextMatrix(keepCurrLineInGrid(),13) = '';
        
        if (trimStr(fg.EditText) != '.')
        {
			fg.EditText = (stripNonAlphaNumericChars(fg.EditText)).toUpperCase();
		}	
		else
			fg.EditText = (fg.EditText).toUpperCase();
		
		fillUFCityFromCEP(fg.EditText);
    }
}

/********************************************************************
Funcao criada pelo programador.
Complementa a funcao ufAndCityFromCEP() com operacoes de servidor

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fillUFCityFromCEP(strFind)
{
	//strFind = trimStr(strFind)

	if ( (strFind != null) && (strFind != '') )
	{
        lockInterface(true);
	
	    // trazer as cidades do servidor
        var strPars = new String();
        strPars = '?';
        strPars += 'sCep=';
        strPars += escape(strFind);
        strPars += '&nPaisID=';
        strPars += escape(fg.TextMatrix(keepCurrLineInGrid(),3));
    
	    glb_ASSYNC_IN_CURSE = true;
	    dsoAddrData.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/findcepex.aspx'+strPars;
	    dsoAddrData.ondatasetcomplete = dsoAddrDataComplete_DSC;
	    dsoAddrData.refresh();
	}
}

/********************************************************************
Funcao criada pelo programador.
Retorno do servidor da funcao function fillUFCityFromCEP(strFind)

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoAddrDataComplete_DSC() {
    var htmlPath = '';
    var strPars = '';
    
    if ((!dsoAddrData.recordset.BOF) && (!dsoAddrData.recordset.EOF))
    {
		if (dsoAddrData.recordset.RecordCount() > 1)
		{
			htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalchoosecep.asp' + strPars;
			showModalWin(htmlPath, new Array(410,190));
			writeInStatusBar('child', 'cellMode', 'Cidades', true);
			glb_ASSYNC_IN_CURSE = false;
			return null;
        }
		else
		{
			dsoAddrData.recordset.moveFirst();
			fg.TextMatrix(keepCurrLineInGrid(),6) = dsoAddrData.recordset['Cidade'].value;
			fg.TextMatrix(keepCurrLineInGrid(),13) = dsoAddrData.recordset['CidadeID'].value;

			if (dsoAddrData.recordset['UF'].value != null)
				fg.TextMatrix(keepCurrLineInGrid(),5)  = dsoAddrData.recordset['UF'].value;
			else			
				fg.TextMatrix(keepCurrLineInGrid(),5)  = '';

			if (dsoAddrData.recordset['UFID'].value != null)
				fg.TextMatrix(keepCurrLineInGrid(),12) = dsoAddrData.recordset['UFID'].value;
			else
				fg.TextMatrix(keepCurrLineInGrid(),12) = '';
			        
			fg.Col = 7;
			fg.Redraw = 2;
		}
    }
    else
    {
        fg.TextMatrix(keepCurrLineInGrid(),5) = '';
        fg.TextMatrix(keepCurrLineInGrid(),6) = '';
        fg.TextMatrix(keepCurrLineInGrid(),12) = '';
        fg.TextMatrix(keepCurrLineInGrid(),13) = '';
        fg.Col = 4;
        
        if ( window.top.overflyGen.Alert ('Nenhuma cidade dispon�vel com este CEP.') == 0 )
        {
            glb_ASSYNC_IN_CURSE = false;
            return null;
        }    
    }             

    glb_ASSYNC_IN_CURSE = false;
    lockInterface(false);
    
    if (fg.disabled == false)
    {
        window.focus();
        fg.focus();
    }
    
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de enderecos para a pasta de enderecos - 20111

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCity()
{
    var htmlPath;
    
    // trap se nao tiver pais selecionado
    if ( fg.TextMatrix(keepCurrLineInGrid(),3) == '' )
        return;
    
    // mandar os parametros para o servidor
    var strPars = new Array();
    strPars = '?';
    strPars += 'sPaisID=';
    strPars += escape(fg.TextMatrix(keepCurrLineInGrid(),3));
    strPars += '&nCEP=';
    if ( fg.TextMatrix(keepCurrLineInGrid(),4) == '' )
        strPars += '0';
    else    
        strPars += escape(fg.TextMatrix(keepCurrLineInGrid(),4));
        
    strPars += '&sEstadoID=';
    if ( fg.TextMatrix(keepCurrLineInGrid(),12) == '' )
        strPars += '0';
    else    
        strPars += escape(fg.TextMatrix(keepCurrLineInGrid(),12));
        
    strPars += '&sCidadeID=';
    
    if ( fg.TextMatrix(keepCurrLineInGrid(),13) == '' )
        strPars += '0';
    else    
        strPars += escape(fg.TextMatrix(keepCurrLineInGrid(),13));
       
    // carregar modal - faz operacao de banco no carregamento    
    htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalendereco.asp'+strPars;
    showModalWin(htmlPath, new Array(410,334));
    
    writeInStatusBar('child', 'cellMode', 'Cidades', true);    
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de solicita��o de credito para a pasta de creditos - 20111

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalSolicitacaoCredito() {
    var htmlPath;
    htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalsolicitacaocredito.asp';
    showModalWin(htmlPath, new Array(250, 125));

    writeInStatusBar('child', 'cellMode', 'Solicita��o de Credito', true);
}

/********************************************************************
Funcao criada pelo programador.
Escreve CEP, estado e cidade vindo da janela modal de enderecos.
Pasta de enderecos - 20111

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeAddressInGrid(aAddress)
{
    // o array aAddress contem: EstadoID, CidadeID,
    // CepInicial, CEPFinal, Nome do Estado, Sigla do Estado e Nome da Cidade
    fg.Redraw = 0;
    // Escreve os IDs
    
    // Allplus
    if (trimStr(fg.TextMatrix(keepCurrLineInGrid(),4)) != '.')
		fg.TextMatrix(keepCurrLineInGrid(),4) = aAddress[2];
		
    fg.TextMatrix(keepCurrLineInGrid(),12) = aAddress[0];
    fg.TextMatrix(keepCurrLineInGrid(),13) = aAddress[1];
    
    // Escreve os Textos
    fg.TextMatrix(keepCurrLineInGrid(),5) = aAddress[5];
    fg.TextMatrix(keepCurrLineInGrid(),6) = aAddress[6];
    
    fg.Redraw = 2;
}

/********************************************************************
Funcao criada pelo programador.
Escreve estado e cidade vindo da janela modal de choosecep.
Pasta de enderecos - 20111

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeAddressInGridEx(aAddress)
{
    // o array aAddress contem: EstadoID, CidadeID,
    // Nome do Estado, Sigla do Estado e Nome da Cidade
    fg.Redraw = 0;
    // Escreve os IDs
    fg.TextMatrix(keepCurrLineInGrid(),13) = aAddress[0];
    fg.TextMatrix(keepCurrLineInGrid(),6) = aAddress[1];
    fg.TextMatrix(keepCurrLineInGrid(),12) = aAddress[2];
    fg.TextMatrix(keepCurrLineInGrid(),5) = aAddress[3];
    
    fg.Redraw = 2;
}

/********************************************************************
Funcao criada pelo programador.
Retorno do servidor dos dados do combo de filtros, usado para relacoes.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoCmbRelComplete_DSC() {
    // ao final volta para automacao
    finalOfInfCascade(glb_lastBtnClicked);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Relacoes

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalRelacoes()
{
    var currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
    var aValue = currCmb2Data[1].split(',');
    var htmlPath;
    var strPars = new Array();
    var nTipoRelID = aValue[0];
    var sTipoSujObj = aValue[1];
    var sHeader = aValue[2];
    var nSujObjID = getCurrDataInControl('sup', 'txtRegistroID');
    var nRelEntreID = aValue[3];
    var nTipoPessoaID = getCurrDataInControl('sup', 'selTipoRegistroID');
    var horizGap = 0;
    
    // aumenta a dimensao horizontal para janela de pesquisa de pessoas
    if ( keepCurrFolder() == 20123 )
        horizGap = 146;
        
    // mandar os parametros para o servidor
    strPars = '?';
    strPars += 'nPastaID=' + escape(keepCurrFolder());
    strPars += '&nTipoRelID=' + escape(nTipoRelID);
    strPars += '&sTipoSujObj=' + escape(sTipoSujObj);
    strPars += '&sHeader=' + escape(sHeader);
    strPars += '&nSujObjID=' + escape(nSujObjID);
    strPars += '&nRelEntreID=' + escape(nRelEntreID);
    strPars += '&nTipoPessoaID=' + escape(nTipoPessoaID);
       
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modalrelacoes.asp'+strPars;
    showModalWin(htmlPath, new Array((571 + horizGap),284));
}

function openModalEmpresas()
{
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modalempresas.asp';
    showModalWin(htmlPath, new Array((571),284));
}

function openModalRotas() 
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 990; //770;
    var nHeight = 530; //470;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('I');

    // carregar a modal
    htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modalrotas.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

function openModalClonarRotas()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 350;
    var nHeight = 220;
	var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
	
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('I');
    strPars += '&nPesTransportadoraID=' + escape(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PesTransportadoraID')));
    strPars += '&nPessoaID=' + escape(nPessoaID);

    // carregar a modal
    htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modalclonarrotas.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Escreve Suj/Obj e ID da Relacao janela modal de relacoes.
Pasta de Relacoes

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeSujObjInGrid(aSujObj)
{
    // o array aSujObj contem: PessoaID, Fantasia, TipoRelacaoID
    
    fg.Redraw = 0;
    // Escreve nos campos
    
    fg.TextMatrix(keepCurrLineInGrid(),2) = aSujObj[0];
    fg.TextMatrix(keepCurrLineInGrid(),4) = aSujObj[2];
    fg.TextMatrix(keepCurrLineInGrid(),5) = aSujObj[1];
    
    fg.Redraw = 2;
}

/********************************************************************
Funcao criada pelo programador.
Carrega o Wizard

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fillWizard()
{
    if ( glb_localTimerInt != null )
    {
        window.clearInterval(glb_localTimerInt);
        glb_localTimerInt = null;
    }    
    
    // comentado abaixo em 18/12/2001
    //lockInterface(true);
    
    // movido para esta posicao em 19/12/2001
    glb_IsNewRecord = false;
    
    // O contexto
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = null;
    
    if ( ((typeof(aContextoID)).toUpperCase() == 'OBJECT') && (aContextoID.length >= 2) )
        nContextoID = aContextoID[1];
    
    // O usuario
    var nUserId = null;
    nUserID =  getCurrUserID();
    
    // A empresa
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID = null;
    
    if ( ((typeof(aEmpresaData)).toUpperCase() == 'OBJECT') && (aEmpresaData.length > 0) )
        nEmpresaID = aEmpresaData[0];
    
    // A string de parametros para a pagina ASP
    var strPas = new String();
    
    strPas = '?nContextoID=';
    strPas += escape(nContextoID);
    
    strPas += '&nUserID=';
    strPas += escape(nUserID);
    
    strPas += '&nEmpresaID=';
    strPas += escape(nEmpresaID);
    
    // se erro em um dos parametros
    if ( nContextoID == null || nContextoID == 0 || nUserID == null || nUserID == 0 || nEmpresaID == null || nEmpresaID == 0 )
    {
        var alertMsg;
        
        alertMsg = 'Mensagem do Wizard, favor copiar e entregar ao Camilo ou M�rcio:\n';
        alertMsg += 'nContextoID = ' +  nContextoID + '\n';
        alertMsg += 'nUserID = ' +  nUserID + '\n';
        alertMsg += 'nEmpresaID = ' +  nEmpresaID + '\n' + 'Obrigado.' ;
        
        if ( window.top.overflyGen.Alert(alertMsg) == 0 )
                return null;
                
        fillWizard_DSC();
        return null;
    }
    
    dsoWizard.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/fillwizard.aspx'+strPas;
    dsoWizard.ondatasetcomplete = fillWizard_DSC;
    dsoWizard.Refresh();
}

/********************************************************************
Funcao criada pelo programador.
Retorno do carregamento do Wizard

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fillWizard_DSC() {
    var iCounter = 0;
    
    // movido para esta posicao em 18/12/2001
    // era a ultima chamada desta funcao
    lockInterface(false);
    
    // zera o wizard
    glb_aWizard = [];
    
    // movido para outra posicao
    // glb_IsNewRecord = false;
    
    if ( dsoWizard.recordset.fields.Count != 0 )
    {
        if ( !((dsoWizard.recordset.BOF) && (dsoWizard.recordset.EOF)) )
        {
            while (!dsoWizard.recordset.EOF)
            {
                glb_aWizard[iCounter] = dsoWizard.recordset['SubFormID'].value;
                dsoWizard.recordset.MoveNext();
                iCounter++;
            }
            
            glb_nPointerWizard = -1;
            controlWizard('INFAVANC');
        }
    }
}

/********************************************************************
Funcao criada pelo programador.
Funcao que opera o wizard

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function controlWizard(btnClicked)
{
    var optDataInCmb;
    
    if (btnClicked == 'INFAVANC')
        glb_nPointerWizard++;
    else if (btnClicked == 'INFRETRO')
        glb_nPointerWizard--;    
    
    if (glb_nPointerWizard == glb_aWizard.length)
    {
        setupControlBarToWizard();
        return null;
    }    
        
    if (! selOptByValueOfSelInControlBar('inf', 1, glb_aWizard[glb_nPointerWizard]))
    {
        // sai fora do wizard se a pasta nao e disponivel
        glb_nPointerWizard = -1;
        setupControlBarToWizard();
        return null;
    }
    
/**********************    
Comentado e substituido pelo refr abaixo em 19/12/2001
    // forca um refresh para carregar a primeira pasta do wizard
    // optDataInCmb = getCmbCurrDataInControlBar('inf', 1);

    // __combo_1('inf', optDataInCmb[2], optDataInCmb[1], optDataInCmb[0]);
    
    //setupControlBarToWizard();
    
**********************/    
    __btn_REFR('inf');
    
}

/********************************************************************
Funcao criada pelo programador.
Funcao que opera o wizard

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function setupControlBarToWizard()
{
    var aStrBtns = new Array();
    var strBtns = currentBtnsCtrlBarString('inf');
    var disabledButtons = false;
    aStrBtns = strBtns.split('');
    
    if ( ((glb_nPointerWizard == glb_aWizard.length) && (glb_aWizard.length > 0)) ||
         (glb_nPointerWizard == -1) )
    {
        aStrBtns[0] = 'D';
        aStrBtns[1] = 'D';
        glb_lOperateWizard = false;
        glb_nPointerWizard = 0;
        disabledButtons = true;
    }
    else if (glb_nPointerWizard == 0)
    {
        aStrBtns[0] = 'D';
        aStrBtns[1] = 'H';
    }    
    else if (glb_nPointerWizard >= 1)
    {
        aStrBtns[0] = 'H';
        aStrBtns[1] = 'H';    
    }
    
    strBtns = aStrBtns.toString();
    re = /,/g;
    strBtns = strBtns.replace(re, '');
    adjustControlBarEx(window,'inf', strBtns);
    
    if (disabledButtons)
		sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'__btn_REFR(' + '\'' + 'sup' + '\'' + ')');
}

/********************************************************************
Funcao criada pelo programador.
Usada pela funcao execValidadeEdit_Prg() na pasta de URLs

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function selTipoURL()
{
    var nEmpresaData;
    nEmpresaData = getCurrEmpresaData();
    var nIdiomaEmpresaID = nEmpresaData[8];

    if ( fg.Editable)
    {
        var sURL = fg.TextMatrix(fg.Row,2);
        var nTipoURL;

        // Tipo de URL
        if ( (fg.Col == 0)&&((sURL == urlEmailTerminator(nIdiomaEmpresaID, 124)) || 
             (sURL == urlEmailTerminator(nIdiomaEmpresaID, 125))) )
        {
            nTipoURL = fg.ComboData;
            // e-mail
            if (nTipoURL == 124)
                fg.TextMatrix(fg.Row,2) = urlEmailTerminator(nIdiomaEmpresaID, 124);
            // site
            else if (nTipoURL == 125)
                fg.TextMatrix(fg.Row,2) = urlEmailTerminator(nIdiomaEmpresaID, 125);

            fg.col=2;
        }
    }
}

/********************************************************************
Funcao criada pelo programador.
Valida E-mail e verifica duplicidade no banco de dados

Parametro:
nenhum

Retorno:
irrelevante
********************************************************************/
function verifyUrl() 
{    
    var nTipoURL = fg.Textmatrix(fg.Row, 0);

    //Somente valida emails
    if (nTipoURL == 124) 
    {
        if (fg.TextMatrix(fg.Row, 2) == urlEmailTerminator(getCurrEmpresaData()[8], 124)) {
            if (window.top.overflyGen.Alert('E-mail inv�lido') == 0)
                return null;

            fg.Col = 2;
            fg.focus();
            return false;
        }
        else {
            glb_PessoasClassificacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selClassificacaoID.value');

            //Somente deve validar para pessoas que sejam do tipo contato.
            if (glb_PessoasClassificacaoID == 66) {
                setConnection(dsoGen01);

                dsoGen01.SQL = 'SELECT dbo.fn_Contato_Email_Existente(' +
                            '\'' + fg.TextMatrix(fg.Row, 2) + '\'' +
                            ',' + parseInt("0" + fg.TextMatrix(fg.Row, 5), 10) + ') As Verificacao';

                dsoGen01.ondatasetcomplete = verifyUrl_DSC;
                dsoGen01.Refresh();
            }
            else {
                lockInterface(false);

                // prossegue a gravacao
                glb_btnCtlSupInf = 'INFOK';
                saveRegister_1stPart();
            }
        }
    }
    else if ((nTipoURL == 125)|| (nTipoURL == 127)) {
        lockInterface(false);
        // prossegue a gravacao
        glb_btnCtlSupInf = 'INFOK';
        saveRegister_1stPart();
    }
    
}

/********************************************************************
Retorno do servidor da funcao que verifica no servidor,
se o email do contato � unico e valido no cadastro de pessoas

Parametros:
nenhum

Retorno:
nao relevante
********************************************************************/
function verifyUrl_DSC() 
{
    if (!(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF)) 
    {
        if (dsoGen01.recordset['Verificacao'].value == null) 
        {
            lockInterface(false);

            // prossegue a gravacao
            glb_btnCtlSupInf = 'INFOK';
            saveRegister_1stPart();
        }
        else {
            if (window.top.overflyGen.Alert('E-mail j� cadastrado para o(s) contato(s): ' + dsoGen01.recordset['Verificacao'].value) == 0);
                return null;

                fg.Col = 2;
                fg.focus();
                return false;
        }
    }
}

/********************************************************************
Funcao criada pelo programador.
Valida CGC/CPF e verifica duplicidade no banco de dados

Parametro:
nenhum

Retorno:
irrelevante
********************************************************************/
function verifyDocNumber()
{
    var sNumero = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Numero'));
    var nTipoDocumentoID = parseInt(fg.TextMatrix(fg.Row, 1), 10);

	sNumero = trimStr(sNumero);                    

    var nVerifica = 1;

    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Numero')) = sNumero;
    
    if (sNumero.length == 0)
    {
        lockInterface(false);
        if ( window.top.overflyGen.Alert ('N�mero do documento inv�lido!') == 0 )
            return null;
            
        fg.Col = 2;
        fg.focus();
        return false;
    }    
    
    setConnection(dsoGen01);

    dsoGen01.SQL = 'SELECT ' +
			'dbo.fn_Documento_Verifica(' + '\'' + sNumero + '\'' + ', ' + 
												  parseInt(fg.TextMatrix(fg.Row, 0), 10) + ', ' + 
												  nTipoDocumentoID + ') AS Verificacao ';

    dsoGen01.ondatasetcomplete = verifyDocNumber_DSC;
    dsoGen01.Refresh();
}    

/********************************************************************
Retorno do servidor da funcao que verifica no servidor,
se o numero do documento e unico e valido no cadastro de pessoas

Parametros:
nenhum

Retorno:
nao relevante
********************************************************************/
function verifyDocNumber_DSC() {
    var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');

    if ( !(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF) )
    {
		// grava o registro
		if ( (dsoGen01.recordset['Verificacao'].value == null) ||
			 (dsoGen01.recordset['Verificacao'].value == nPessoaID) )
		{
			// Usado no final do processo para prosseguir a automacao
			// de gravacao

			lockInterface (false);
			                    
			// prossegue a gravacao
			glb_btnCtlSupInf = 'INFOK';
			saveRegister_1stPart();
		}
		// Documento Inv�lido
		else if (dsoGen01.recordset['Verificacao'].value == 0)
		{
			lockInterface(false);
			if ( window.top.overflyGen.Alert ('N�mero do documento inv�lido.') == 0 )
			    return null;
			    
			fg.Col = 2;
			fg.focus();
			return false;
		}
		// Outra Pessoa com este documento
		else
		{
			var dirC1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			          ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'C1' + '\'' + ')') );

			var dirC2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			          ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'C2' + '\'' + ')') );

			nPessoaID = dsoGen01.recordset['Verificacao'].value;

			var bDireito=true;
			
			// Sem Direito
			//DireitoEspecifico
			//Pessoas->F�sicas/jur�dicas->Documentos
            //20100 SFS-Grupo Pessoas -> C1&&C2 -> Ao inserir ou alterar um documento, caso o mesmo j� exista no banco, permite o usu�rio detalhar o registro existente.            
			if ((dirC1 == 0) && (dirC2 == 0))
				bDireito=false;

			// tem direito para ver o registro ja existente no cadastro
			if (bDireito)
			{
				var _retMsg = window.top.overflyGen.Confirm('Documento pertence a outra pessoa j� cadastrada.' + '\n' + 'Exibe os dados desta pessoa?');
    
				if ( _retMsg == 0 )
				    return null;
				else if ( _retMsg == 1 )
				{
				    lockInterface(false);
				    // trava o grid para edicao
				    fg.Editable = false;
				    
				    glb_localTimerInt = window.setInterval('showPersonByID(' + nPessoaID + ')', 10, 'JavaScript');  
				    
				    return false;
				}             
				else
				{
				    if ( window.top.overflyGen.Alert ('N�mero do documento inv�lido!') == 0 )
				        return null;
				        
				    lockInterface(false);
				    fg.Col = 2;
				    fg.focus();
				    return false;
				}             
			}
			// nao tem direito para ver o registro ja existente no cadastro
			else
			{
				if ( window.top.overflyGen.Alert ('Documento pertence a outra pessoa j� cadastrada!') == 0 )
				    return null;
				    
				lockInterface(false);
				fg.Col = 2;
				fg.focus();
				return false;
			}                 
		}
    }
	else
	{
		lockInterface(false);
		if ( window.top.overflyGen.Alert ('N�mero do documento inv�lido.') == 0 )
		    return null;
			    
		fg.Col = 2;
		fg.focus();
		return false;
	}        
    return null;    
}

/********************************************************************
Trata o CGC/CPF digitado

Parametros:
nenhum

Retorno:
o numDoc tratado
********************************************************************/
function treatsNumDoc(numDoc)
{
    var strRet = '';
    var strTemp = '';
    var rExp;
    var i;
    
    strTemp = trimStr(numDoc);
                
    rExp = /\D/g;
    strTemp = strTemp.split(rExp);
    
    for ( i=0; i< strTemp.length; i++ )
        strRet += strTemp[i];
    
    return strRet;
}

/********************************************************************
Criada pelo programador
Na inclusao, retorno da modal de numero de documento, esta funcao
abre o detalhe de um usuario ja cadastrado
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function showPersonByID(pessoaID)
{
    if ( glb_localTimerInt != null )
    {
        window.clearInterval(glb_localTimerInt);
        glb_localTimerInt = null;
    }    
    
    sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', 
      [pessoaID, null, null]);
}

/********************************************************************
Criada pelo programador
Volta o combo de pastas para a pasta observacao, porque neste momento
as unicas pastas carregadas no combo: observacoes, prop/alter, LOG
e relpessoas. 
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function returnToDefaultFolder()
{
    selOptByValueOfSelInControlBar('inf', 1, 20008);
}

/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos com direitos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function direitoCampos()
{
    var nNivelPagamentoMaximo, bReadOnly;

	nNivelPagamentoMaximo = dso01JoinSup.recordset['NivelPagamentoMaximo'].value;	
    
    if (nNivelPagamentoMaximo == 4)
        bReadOnly = false;
    else
        bReadOnly = true;
}

function colunareadonly(PlanoID) {
    setConnection(dsoDocumentoPessoa);
    dsoDocumentoPessoa.SQL = "SELECT COUNT(1) AS Documento_Estadual " +
                                "FROM Pessoas_Documentos a WITH(NOLOCK) " +
                                    "INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON b.ItemID = a.TipoDocumentoID " +
                                "WHERE b.Filtro LIKE '%{232}%' AND a.PesDocumentoID = " + PlanoID;
    dsoDocumentoPessoa.ondatasetcomplete = colunareadonly_DSC;
    dsoDocumentoPessoa.Refresh();

}

function colunareadonly_DSC() {

    var nColUF = getColIndexByColKey(fg, 'UFID');

    glb_AlteraUF = true;

    fg.Cell(6, glb_Row, nColUF, glb_Row, nColUF) = 0XFFFFFF; // branco

    if (dsoDocumentoPessoa.recordset['Documento_Estadual'].value != 112) {
        fg.Cell(6, glb_Row, nColUF, glb_Row, nColUF) = 0XDCDCDC;
        glb_AlteraUF = false;
        fg.TextMatrix(glb_Row, nColUF) = "";
    }

    else {
        if ((typeof (aRecords)).toUpperCase() == 'UNDEFINED')
            return null;

        if ((fg.TextMatrix(glb_Row, nColUF) == "") && (dso01Grid.recordset.aRecords[glb_Row - 1][nColUF] > 0)) {
            if (dso01Grid.recordset.aRecords[glb_Row - 1][nColUF] == 130)
                fg.TextMatrix(glb_Row, nColUF) = "";
            else
                fg.TextMatrix(glb_Row, nColUF) = aRecords[glb_Row - 1][nColUF];
        }
    }
}

/********************************************************************
Funcao do programador.
Forca refresh no inf
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function forceInfRefresh() {
    window.clearInterval(true);
    __btn_REFR('inf');
}

function detalhaCredito(nParceiroID, bSeguro) {

    setConnection(dsoCredito);
    dsoCredito.SQL = "SELECT TOP 1 CreditoID, ParceiroID " +
                        "FROM Creditos WITH(NOLOCK) " +
                        "WHERE ParceiroID = " + nParceiroID + " AND PaisID = " + glb_EmpresaData[1] + " " /*" AND SeguradoraID " + (bSeguro == false ? "IS NULL " : "IS NOT NULL ")*/ +
                        "ORDER BY dtFim DESC ";
    dsoCredito.ondatasetcomplete = detalhaCredito_DSC;
    dsoCredito.Refresh();
}

function detalhaCredito_DSC() {
    var nCreditoID = dsoCredito.recordset['CreditoID'].value;

    if (nCreditoID > 0)
        sendJSCarrier(getHtmlId(), 'SHOWCREDITO', new Array(glb_EmpresaData[0], nCreditoID));
    else {
        window.top.overflyGen.Alert('N�o h� an�lise de cr�dito para essa pessoa.');
        return true;
    }
}