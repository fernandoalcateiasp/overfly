/********************************************************************
pessoassup01.js

Library javascript para o pessoassup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_localTimerInt = null;

// Guarda o tipo do registro selecionado para ser usado no select
// que traz o registro de volta na gravacao, pois este form tem
// Identity calculado por trigger
var glb_PessoasTipoRegID = null;
var glb_PessoasClassificacaoID = null;
var glb_timerEditRegister = null;
var glb_CarrierSendListaPrecoAtentimento = false;
var glb_liberaCampoEspecifico = false;

var glb_PessoaSupTimer = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoListaPrecos = new CDatatransport('dsoListaPrecos');
var dsoEmailCadastro = new CDatatransport('dsoEmailCadastro');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoDireito = new CDatatransport('dsoDireito');
var dsoEhVendedor = new CDatatransport('dsoEhVendedor');

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    finalOfSupCascade(btnClicked)
    btnLupaClicked(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()    
    formFinishLoad()
	dialByModalPage(sDDD, sNumero, nPessoaID)
	
FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSaved )

FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selClassificacaoID', '21'],
                          ['selJurClassificacaoID', '22'],
                          ['selEstadoCivilID', '3'],
                          ['selEducacaoID', '4'],
                          ['selPaisNascimentoID', '5'],
                          ['selBancoID', '6'],
                          ['selSexo','7'],
                          ['selJurSexo', '7'],
                          ['selDeficienciaID', '8'],
                          ['selRacaID', '9'],
						  //['selTipoEmpresaID', '10'],
						  ['selEnquadramentoEmpresaID', '10'],
						  ['selRegimeTributarioID', '11']]);
                          
    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/basico/pessoas/pessoasinf01.asp',
                              SYS_PAGESURLROOT + '/basico/pessoas/pessoaspesqlist.asp');                          
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', ['divSup02_01', 'divSup02_02', 'divSup02_03'], [51, 52, 53]);

    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'PessoaID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoPessoaID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01'],
                      [2,'divSup02_01'],
                      [2,'divSup02_02'],
                      [2,'divSup02_03']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblNome','txtNome',40,1],
                          ['lblFantasia','txtFantasia',20,1],
                          ['lblTipoRegistroID','selTipoRegistroID',18,1]]);

    //@@ *** Pessoa Fisica - Div secundario superior divSup02_01 ***/
    adjustElementsInForm([['lblClassificacaoID','selClassificacaoID',19,2],
                          ['lblObservacao','txtObservacao',19,2],
                          ['lblSexo','selSexo',11,2],
                          ['lblEstadoCivilID','selEstadoCivilID',11,2],
                          ['lblEducacaoID','selEducacaoID',18,2],
                          ['lblDuracao','txtDuracao',3,2],
                          ['lblConcluido','txtConcluido',3,2],
                          ['lblCursando', 'chkCursando',3,2],
                          ['lblDeficienciaID', 'selDeficienciaID',20, 2, 5],
                          ['lblPaisNascimentoID','selPaisNascimentoID',21,3],
                          ['lblCodigoUFNascimento','txtCodigoUFNascimento',4,3],
                          ['lblCidadeNascimento','txtCidadeNascimento',25,3],
                          ['lbldtNascimento','txtdtNascimento',10,3],
                          ['lbldtFalecimento','txtdtFalecimento',10,3],
                          ['lblAscendencia', 'txtAscendencia', 20, 3],
                          ['lblRacaID', 'selRacaID', 20, 3],
                          ['lblImagem_PF','chkImagem_PF',3,4],
                          ['lblCadastroOK2','chkCadastroOK2',3,4]],
                          ['lblHrPessoaFisica','hrPessoaFisica']);

    //@@ *** Pessoa Juridica - Div secundario superior divSup02_02 ***/
    adjustElementsInForm([['lblJurClassificacaoID','selJurClassificacaoID',19,2],
                          ['lblJurObservacao','txtJurObservacao',18,2,-5],
                          ['lblJurFaturamentoAnual','txtJurFaturamentoAnual',11,2,-5],
                          ['lblNumeroFuncionarios','txtNumeroFuncionarios',6,2,-5],                          
                          ['lblJurSexo','selJurSexo',10,2,-5],
                          ['lblJurdtNascimento','txtJurdtNascimento',10,2,-5],
                          ['lblJurdtFalecimento','txtJurdtFalecimento',10,2,-5],
                          ['lblCNAEJuridica','txtCNAEJuridica',7,2,-5],
                         // ['lblTipoEmpresaID', 'selTipoEmpresaID', 39, 3],
                          ['lblEnquadramentoEmpresaID', 'selEnquadramentoEmpresaID', 26, 3],
                          ['lblRegimeTributarioID', 'selRegimeTributarioID', 16, 3],
                          ['lblRodoviario','chkRodoviario',3,3,25],
                          ['lblAereo','chkAereo',3,3,-10],
                          ['lblMaritimo','chkMaritimo',3,3],
                          ['lblPP','chkPP',3,3,20],
                          ['lblPT', 'chkPT', 3, 3],
                          ['lblPC', 'chkPC', 3, 3],
                          ['lblTP','chkTP',3,3],
                          ['lblTT', 'chkTT', 3, 3],
                          ['lblEhFabrica','chkEhFabrica',3,4],
                          ['lblImagem_PJ', 'chkImagem_PJ', 3, 4],
                          ['lblCadastroOK1', 'chkCadastroOK1', 3, 4],
                          ['lblEhContribuinte', 'chkEhContribuinte', 3, 4],
                          ['lblSimplesNacional', 'chkSimplesNacional', 3, 4],
                          ['lblReceitaBrutaSimplesNacional', 'txtReceitaBrutaSimplesNacional', 11, 4, -5]],
                          ['lblHrPessoaJuridica','hrPessoaJuridica']);
                   
    adjustElementsInForm([['lblBancoID', 'selBancoID',6,3,355],
                          ['lblAgencia','txtAgencia',7,3],
                          ['lblDigitoAgencia','txtDigitoAgencia',3,3]],
                          ['lblHrPessoaJuridica','hrPessoaJuridica']);

    //@@ *** Grupo de Pessoas - Div secundario superior divSup02_03 ***/
    adjustElementsInForm([['lblPercentualComissaoVendas','txtPercentualComissaoVendas',6,2]],
                          ['lblHrGrupoPessoas', 'hrGrupoPessoas']);

    chkSimplesNacional.onclick = ReceitaBrutaSN;

    txtJurFaturamentoAnual.onkeypress = verifyNumericEnterNotLinked;
    txtJurFaturamentoAnual.setAttribute('verifyNumPaste', 1);
    txtJurFaturamentoAnual.setAttribute('thePrecision', 11, 1);
    txtJurFaturamentoAnual.setAttribute('theScale', 2, 1);
    txtJurFaturamentoAnual.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtJurFaturamentoAnual.maxLength = 11;

    lblSimplesNacional.style.visibility = 'hidden';
    chkSimplesNacional.style.visibility = 'hidden';
    lblReceitaBrutaSimplesNacional.style.visibility = 'hidden';
    txtReceitaBrutaSimplesNacional.style.visibility = 'hidden';

    EhVendedor();
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWPESSOA')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{
    // O tratamento do carrier abaixo foi disparado pela modal "ModalAtendimento"
    // Disparada para o form de lista de precos
    if (glb_CarrierSendListaPrecoAtentimento)
    {
        if (param1==null)
        {
            if ( window.top.overflyGen.Alert('N�o h� form Lista de Pre�os dispon�vel para transferir este atendimento.\n' + 
                'Abra um novo form Lista de Pre�os para transferir o atendimento; ou\n' +
                'finalize o atendimento em um dos forms Lista de Pre�os.') == 0 )
                return null;
        }
        else
            sendJSMessage('MODAL_HTML', JS_DATAINFORM, EXECEVAL, 'atendimentoTransferido()');
    }

    if (glb_CarrierSendListaPrecoAtentimento)
        glb_CarrierSendListaPrecoAtentimento = false;

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@ atributos especiais para campos que ficam visiveis ou invisiveis,
    //   habilitados ou desabilitados conforme tipo de registro ou outras
    //   condicoes particulares do form

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
    // Lista de nomes de atributos:
    // Esconder/mostrar campo pela automacao: fnShowHide
    
    txtPercentualComissaoVendas.setAttribute('minMax', new Array(0, 100), 1);
    chkImagem_PF.disabled = true;
    chkImagem_PJ.disabled = true;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,
                  'fillCmbFiltsInfAutomatic(keepCurrFolder())');
    
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') )
	{
		chkImagem_PF.checked = dsoSup01.recordset['TemImagem'].value;
		chkImagem_PJ.checked = dsoSup01.recordset['TemImagem'].value;
	}

	// Mover esta funcao para os finais de retornos de operacoes no
	// banco de dados
	finalOfSupCascade(btnClicked);
}

/********************************************************************
Funcao de uso do programador.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    if ( btnClicked == 'SUPINCL' )
    {
        // Habilita desabilita campo frete chkFrete
        direitoCampos(false);
    }

    // Campo sob controle do programador
    showHideByClassificacao();    

    // Campo sob controle do programador
    showHideLblTxtFalecimentoEncerramento();
    
    //Campo receita bruta SN
    ReceitaBrutaSN();

    direitoBotao5();

	txtCNAEJuridica.title = dsoSup01.recordset['DescricaoCNAE'].value;

    // Problema de cadastro
    if ( ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')) && 
         (parseInt(dsoSup01.recordset['TipoPessoaID'].value, 10) <= 52))
        auditoria();
        
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do sup e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    // Mostra esconde campos condicionais - Pessoa Fisica
    if ( (cmbID == 'selClassificacaoID')||(cmbID == 'selTipoRegistroID') )
        showHideByClassificacao(true);
        
    // Mostra esconde campos condicionais - Pessoa Juridica
    if ( cmbID == 'selJurClassificacaoID'||(cmbID == 'selTipoRegistroID') )
        showHideByClassificacao(true);

    // Se est� incluindo uma Pessoa F�sica
    if ( (cmbID == 'selTipoRegistroID')&&(cmb.value == 51) )
    {
        if ( (typeof(dsoSup01.recordset[glb_sFldIDName].value)).toUpperCase() == 'UNDEFINED' )
            if (selPaisNascimentoID.selectedIndex == -1)
            {
               var empresa = getCurrEmpresaData();
               setValueInControlLinked(selPaisNascimentoID, dsoSup01, empresa[1]);
            }
    }

    adjustLabelsCombos(false);

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{   
    verifyPessoaInServer(currEstadoID, newEstadoID);
    return true;
    
    //@@ prossegue a automacao
    //return false;    
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
    showHideLblTxtFalecimentoEncerramento();
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {

    // Documentos
    if ( (controlBar=='SUP') && (btnClicked == 1) ) {

        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }

    if ( (controlBar=='SUP') && (btnClicked == 3) )
    {
		window.top.openModalControleDocumento('S', '', 720, null, '21', 'T');
    }
    //else if ( (controlBar=='SUP') && (btnClicked == 4) )
    //{
	//	loadModalImagem();
    //}
    else if ( (controlBar=='SUP') && (btnClicked == 5) )
    {
        openModalConfirmacaoCadastro();
    }
    else if ( (controlBar=='SUP') && (btnClicked == 6) )
    {
        window.top.openModalHTML(dsoSup01.recordset['PessoaID'].value, 'Ficha Cadastral', 'S', 3, 1);
    }
    else if ( (controlBar=='SUP') && (btnClicked == 7) )
    {
        ;
    }
    else if ((controlBar == 'SUP') && (btnClicked == 9)) {
        openModalDadosRH();
    }
    else if ((controlBar == 'SUP') && (btnClicked == 10)) {
        openModalSocios(dsoSup01.recordset['PessoaID'].value, dsoSup01.recordset['TipoPessoaID'].value);
    }
}

function dsoEmailCadastro_DSC() {
    if (window.top.overflyGen.Alert(dsoEmailCadastro.recordset['Msg'].value) == 0)
        return null;

    lockInterface(false);
}

/********************************************************************
Retorno do servidor - Usuario clicou botao de detalhar Lista de Precos
********************************************************************/
function detalhalistaPrecos_DSC() {
    var empresa = getCurrEmpresaData();
    var aListaPrecoData = new Array();
    
    if ( !(dsoListaPrecos.recordset.BOF && dsoListaPrecos.recordset.EOF) )
    {   
        aListaPrecoData[0] = dsoListaPrecos.recordset['Fantasia'].value;
        aListaPrecoData[1] = dsoListaPrecos.recordset['PessoaID'].value;
		aListaPrecoData[2] = dsoListaPrecos.recordset['UFID'].value;
		aListaPrecoData[3] = dsoListaPrecos.recordset['EmpresaSistema'].value;
		aListaPrecoData[4] = dsoListaPrecos.recordset['ListaPreco'].value;
		aListaPrecoData[5] = dsoListaPrecos.recordset['PMP'].value;
		aListaPrecoData[6] = dsoListaPrecos.recordset['NumeroParcelas'].value;
		aListaPrecoData[7] = dsoListaPrecos.recordset['FinanciamentoPadrao'].value;
		aListaPrecoData[8] = dsoListaPrecos.recordset['ClassificacaoInterna'].value;
		aListaPrecoData[9] = dsoListaPrecos.recordset['ClassificacaoExterna'].value;
    
        // Manda o id da pessoa a detalhar 
        sendJSCarrier(getHtmlId(), 'SETPESSOAPRICELIST', new Array(empresa[0], aListaPrecoData));
    }
    
	lockInterface(false);
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var strToEval1 = '';
    var strToEval2 = '';
    var nDuracao = 0;
    var nConcluido = 0;
    var sErrorMsg = '';
    var elemToFocus = null;
    
    if  (btnClicked == 'SUPOK') 
    {
		txtDuracao.value = trimStr(txtDuracao.value);
		if (dsoSup01.recordset['TipoPessoaID'].value == "51" && txtDuracao.value != '')
		{
			nDuracao = parseFloat(txtDuracao.value);
			
			if (nDuracao < 1.0)
			{
				sErrorMsg = 'Dura��o do curso � inv�lida.';
				elemToFocus = txtDuracao;
			}	
			else
			{
				if (txtConcluido.value != '')
				{
					nConcluido = parseFloat(txtConcluido.value);
					
					if ((nConcluido < 0 ) || (nConcluido > nDuracao ))
					{
						sErrorMsg = 'Anos concluidos do curso � inv�lido.';
						elemToFocus = txtConcluido;
					}	
				}	
				else
				{
					sErrorMsg = 'Anos concluidos do curso � inv�lido.';
					elemToFocus = txtConcluido;
				}
			}

			if ((nDuracao == nConcluido) && (sErrorMsg == ''))
				chkCursando.checked = false;
				
			if (sErrorMsg != '')
			{
				if ( window.top.overflyGen.Alert(sErrorMsg) == 0 )
				    return null;
				    
				window.focus();    
				elemToFocus.focus();
				
				return false;
			}
		}
		else
		{
			txtConcluido.value = '';
			chkCursando.checked = false;
		}

        // Identity Calculado
        glb_PessoasTipoRegID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
        glb_PessoasClassificacaoID = dsoSup01.recordset['ClassificacaoID'].value;
    
        // se � um registro novo
        if ( (typeof(dsoSup01.recordset[glb_sFldIDName].value)).toUpperCase() == 'UNDEFINED' )
        {
            if ( ((glb_PessoasTipoRegID == 51) || (glb_PessoasTipoRegID == 52)) &&
                 ((glb_PessoasClassificacaoID == null)||(glb_PessoasClassificacaoID == 0))  )
            {
                if ( window.top.overflyGen.Alert ('O campo Classifica��o n�o foi preenchido.') == 0 )
                    return null;
                return false;
            }     
        
            strToEval1 = 'glb_IsNewRecord = true';
            strToEval2 = 'glb_lOperateWizard = true';
            // seleciona pasta Observacoes (ver nota na funcao returnToDefaultFolder())
            sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL , 'returnToDefaultFolder()');
        }
        else
            strToEval1 = 'glb_lOperateWizard = false';

        // destrava o combo de tipo de pessoa, eventualmente
        // travado no retorno da modal de documentos
        selTipoRegistroID.disabled = false;

        
        //Valida FaturamentoAnual - Nova valida��o de Simples Nacional
        if (selRegimeTributarioID.value == 2920) 
        {
            if (txtJurFaturamentoAnual.value == '') {
                if (window.top.overflyGen.Alert('O campo Faturamento anual n�o foi preenchido.') == 0)
                    return null;
                window.focus();
                txtJurFaturamentoAnual.focus();
                return false;
            } 
        }
        
    }
    else if (btnClicked == 'SUPCANC')
    {
        // destrava o combo de tipo de pessoa, eventualmente
        // travado no retorno da modal de documentos
        selTipoRegistroID.disabled = false;

        strToEval1 = 'glb_lOperateWizard = false';
    }
    else if (btnClicked == 'SUPLIST')
    {
        strToEval1 = 'glb_lOperateWizard = false';
    }
    
    if (strToEval1 != '')
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,strToEval1);        

    if (strToEval2 != '')
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL ,strToEval2);
    
    // Para prosseguir a automacao retornar null
    return null;
}
/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALDOCNUMBERHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // param2 e um array: tipoDocumentoID e numero do documento
            dsoSup01.recordset['PaisDocumentoID'].value = parseInt(param2[0], 10);
            dsoSup01.recordset['TipoDocumentoID'].value = parseInt(param2[1], 10);
            dsoSup01.recordset['NumeroDocumento'].value = param2[2];
            
            if ( parseInt(param2[1], 10) == 101 )
                dsoSup01.recordset['TipoPessoaID'].value = 51;
            else if ( parseInt(param2[1], 10) == 111 )
                dsoSup01.recordset['TipoPessoaID'].value = 52;   
            
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            // trava o combo de tipo de pessoa
            selTipoRegistroID.disabled = true;
            // funcao da automacao que mostra o div secundario coerente
            adjustSupInterface();
            
            // foco no primeiro txt
            txtNome.focus();
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // param2 != null e o id da pessoa existente no cadaqstro
            // com este documento
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // Simula cancelamento de inclusao
            if ( param2 == null )
                __btn_CANC('SUP');
            else
            {    
                glb_localTimerInt = window.setInterval('showPersonByID(' + param2 + ')', 10, 'JavaScript');  
            }
            
            // nao mexer
            return 0;
        }    
    }
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALEMAILCADASTROHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if ( idElement.toUpperCase() == 'MODALCONFIRMACAOCADASTROHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
			glb_PessoaSupTimer = window.setInterval('refreshSup()', 10, 'JavaScript');
			
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
           
			glb_PessoaSupTimer = window.setInterval('refreshSup()', 10, 'JavaScript');

            // nao mexer
            return 0;
        }
    }
    // Imagens
    if ( (idElement.toUpperCase() == 'MODALGERIMAGENSHTML')||
          (idElement.toUpperCase() == 'MODALSHOWIMAGENSHTML'))
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALOPENHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALDADOSRHHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            glb_PessoaSupTimer = window.setInterval('refreshSup()', 10, 'JavaScript');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            glb_PessoaSupTimer = window.setInterval('refreshSup()', 10, 'JavaScript');

            // nao mexer
            return 0;
        }
    }
    // Modal de S�cios
    else if (idElement.toUpperCase() == 'MODALSOCIOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }


}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // Habilita desabilita campo frete chkFrete
    direitoCampos(true);

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Imagem', 'Confirma��o de cadastro', 'Resumo', 'Atendimento', 'Atributos', 'DadosRH', 'S�cios']);

    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);
}

/********************************************************************
Funcao criada pelo programador.
Da refresh no sup

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function refreshSup()
{
    if ( glb_PessoaSupTimer != null )
    {
        window.clearInterval(glb_PessoaSupTimer);
        glb_PessoaSupTimer = null;
    }
  
    __btn_REFR('sup');  
}

/********************************************************************
Criada pelo programador
O par label 'lblJurServico', campo 'txtJurServico' e visivel
se tipoRegistroID == 52 e ClassificacaoID == 69.
Isto e feito pela funcao showHideByClassificacao(), abaixo definida
a funcao e invocada na funcao abaixo optChangedInCmb e tambem na
automacao pelos atributos especiais abaixo:
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function showHideByClassificacao(bEditing)
{
    var nTipoRegistroID;
    var nClassificacao;
    if (bEditing == true)
        nTipoRegistroID = selTipoRegistroID.value;
    else
        nTipoRegistroID = dsoSup01.recordset['TipoPessoaID'].value;
        

    if (nTipoRegistroID == 51)
    {
        if (bEditing == true)
            nClassificacao = selClassificacaoID.value;
        else    
            nClassificacao = dsoSup01.recordset['ClassificacaoID'].value;
    }
    else if (nTipoRegistroID == 52)
    {
        if (bEditing == true)
            nClassificacao = selJurClassificacaoID.value;
        else    
            nClassificacao = dsoSup01.recordset['ClassificacaoID'].value;
    }
        
    lblRodoviario.style.visibility = 'hidden';
    chkRodoviario.style.visibility = 'hidden';
    lblAereo.style.visibility = 'hidden';
    chkAereo.style.visibility = 'hidden';
    lblMaritimo.style.visibility = 'hidden';
    chkMaritimo.style.visibility = 'hidden';
    lblPP.style.visibility = 'hidden';
    chkPP.style.visibility = 'hidden';
    lblPT.style.visibility = 'hidden';
    chkPT.style.visibility = 'hidden';
    lblPC.style.visibility = 'hidden';
    chkPC.style.visibility = 'hidden';
    lblTP.style.visibility = 'hidden';
    chkTP.style.visibility = 'hidden';
    lblTT.style.visibility = 'hidden';
    chkTT.style.visibility = 'hidden';
	lblBancoID.style.visibility = 'hidden';
	selBancoID.style.visibility = 'hidden';
	lblAgencia.style.visibility = 'hidden';
	txtAgencia.style.visibility = 'hidden';
	lblDigitoAgencia.style.visibility = 'hidden';
	txtDigitoAgencia.style.visibility = 'hidden';

    if ( (nTipoRegistroID == 51) || (nTipoRegistroID == 52) )
    {
        if (nClassificacao == 68)
        {
            lblRodoviario.style.visibility = 'visible';
            chkRodoviario.style.visibility = 'visible';
            lblAereo.style.visibility = 'visible';
            chkAereo.style.visibility = 'visible';
            lblMaritimo.style.visibility = 'visible';
            chkMaritimo.style.visibility = 'visible';
            lblPP.style.visibility = 'visible';
            chkPP.style.visibility = 'visible';
            lblPT.style.visibility = 'visible';
            chkPT.style.visibility = 'visible';
            lblPC.style.visibility = 'visible';
            chkPC.style.visibility = 'visible';
            lblTP.style.visibility = 'visible';
            chkTP.style.visibility = 'visible';
            lblTT.style.visibility = 'visible';
            chkTT.style.visibility = 'visible';
        }
        else if ((nClassificacao == 69) && (nTipoRegistroID == 52))
        {
			lblBancoID.style.visibility = 'visible';
			selBancoID.style.visibility = 'visible';
			lblAgencia.style.visibility = 'visible';
			txtAgencia.style.visibility = 'visible';
			lblDigitoAgencia.style.visibility = 'visible';
			txtDigitoAgencia.style.visibility = 'visible';
        }
    }
}

/********************************************************************
Criada pelo programador
Esta funcao mostra ou esconde um par de controles.
    
O par label 'lbldtFalecimento', campo 'txtdtFalecimento' e visivel
se tipoRegistroID == 51 e txtEstadoID == 4 (D) ou 5 (Z) .
Isto e feito pela funcao showHideLblTxtFalecimentoEncerramento(), abaixo definida
a funcao e invocada na funcao abaixo optChangedInCmb e tambem na
automacao pelos atributos especiais abaixo:
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function showHideLblTxtFalecimentoEncerramento()
{
    // Pessoa Fisica
    if (getCurrTipoRegID() == 51)
    {
        txtdtFalecimento.setAttribute('prog_SHHD', false, 1);
        lbldtFalecimento.style.visibility = 'visible';
        txtdtFalecimento.style.visibility = 'visible';
    }
    else
    {
        txtdtFalecimento.setAttribute('prog_SHHD', true, 1);
        lbldtFalecimento.style.visibility = 'hidden';
        txtdtFalecimento.style.visibility = 'hidden';
    }
    
    // Pessoa Juridica
    if (getCurrTipoRegID() == 52)
    {
        txtJurdtFalecimento.setAttribute('prog_SHHD', false, 1);
        lblJurdtFalecimento.style.visibility = 'visible';
        txtJurdtFalecimento.style.visibility = 'visible';
    }
    else
    {
        txtJurdtFalecimento.setAttribute('prog_SHHD', true, 1);
        lblJurdtFalecimento.style.visibility = 'hidden';
        txtJurdtFalecimento.style.visibility = 'hidden';
    }
}

/********************************************************************
Criada pelo programador
Esta funcao abre a janela modal de docNumber
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function openModalDocNumber()
{
    var htmlPath;
    var strPars = new String();
    var dirC1;
    var dirC2;
    var aEmpresa = getCurrEmpresaData();

    dirC1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                  ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'C1' + '\'' + ')') );

    dirC2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                  ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'C2' + '\'' + ')') );
    
    var nTipoPessoaID = getCurrDataInControl('sup', 'selTipoRegistroID');
        
    if ( (nTipoPessoaID == null) || (nTipoPessoaID == '') )
        nTipoPessoaID = 0;
            
    // mandar os parametros para o servidor
    strPars = '?sCaller=' + escape('S');   
    strPars += '&ndirC1=' + escape(dirC1);   
    strPars += '&ndirC2=' + escape(dirC2);
    strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);
               
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modaldocnumber.asp'+strPars;
    showModalWin(htmlPath, new Array(480, 400));
        
    // Prossegue a automacao
    return null;
}

/********************************************************************
Criada pelo programador
Na inclusao, retorno da modal de numero de documento, esta funcao
abre o detalhe de um usuario ja cadastrado
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function showPersonByID(pessoaID)
{
    if ( glb_localTimerInt != null )
    {
        window.clearInterval(glb_localTimerInt);
        glb_localTimerInt = null;
    }    
    
    sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', 
      [pessoaID, null, null]);
}

/********************************************************************
Criada pelo programador
Chamada pelo inf, trava combo do tipo de pessoa
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function lockCmbTipoPessoa()
{
// trava combo de tipo de pessoa se o tipo e 51 ou 52
    // (fisica ou juridica)
    if ( (dsoSup01.recordset['TipoPessoaID'].value == 51) ||
           (dsoSup01.recordset['TipoPessoaID'].value == 52) )
        selTipoRegistroID.disabled = true;
}

/********************************************************************
Verificacoes da Pessoa
********************************************************************/
function verifyPessoaInServer(nCurrEstadoID, nNewEstadoID)
{
    var nPessoaID = dsoSup01.recordset['PessoaID'].value;
    var strPars = new String();

    strPars = '?nPessoaID='+escape(nPessoaID);
    strPars += '&nCurrEstadoID=' + escape(nCurrEstadoID);
    strPars += '&nNewEstadoID=' + escape(nNewEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/verificacaopessoa.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyPessoaInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes da Pessoa
********************************************************************/
function verifyPessoaInServer_DSC() {
    var sResultado = dsoVerificacao.recordset['Resultado'].value;
    
    if (sResultado == null)
    {
        stateMachSupExec('OK');
    }    
    else
    {
        if ( window.top.overflyGen.Alert(sResultado) == 0 )
            return null;
        stateMachSupExec('CANC');
    }
}

function adjustLabelsCombos(bPaging)
{
    var nTipoPessoaID = 0;
    if (bPaging)
    {
        setLabelOfControl(lblTipoRegistroID, dsoSup01.recordset['TipoPessoaID'].value);
        nTipoPessoaID = dsoSup01.recordset['TipoPessoaID'].value;
        
        if (nTipoPessoaID == 51)
        {
            setLabelOfControl(lblClassificacaoID, dsoSup01.recordset['ClassificacaoID'].value);
            setLabelOfControl(lblSexo, dsoSup01.recordset['Sexo'].value);
            setLabelOfControl(lblEstadoCivilID, dsoSup01.recordset['EstadoCivilID'].value);
            setLabelOfControl(lblEducacaoID, dsoSup01.recordset['EducacaoID'].value);
            setLabelOfControl(lblPaisNascimentoID, dsoSup01.recordset['PaisNascimentoID'].value);
            setLabelOfControl(lblDeficienciaID, dsoSup01.recordset['DeficienciaID'].value);
            setLabelOfControl(lblRacaID, dsoSup01.recordset['RacaID'].value);
        }    
        else
        {
            setLabelOfControl(lblJurClassificacaoID, dsoSup01.recordset['ClassificacaoID'].value);
            setLabelOfControl(lblJurSexo, dsoSup01.recordset['Sexo'].value);
        }    
    }
    else
    {
        setLabelOfControl(lblTipoRegistroID, selTipoRegistroID.value);
        setLabelOfControl(lblJurSexo, selJurSexo.value);
        nTipoPessoaID = selTipoRegistroID.value;

        if (nTipoPessoaID == 51)
        {
            setLabelOfControl(lblClassificacaoID, selClassificacaoID.value);
            setLabelOfControl(lblSexo, selSexo.value);
            setLabelOfControl(lblEstadoCivilID, selEstadoCivilID.value);
            setLabelOfControl(lblEducacaoID, selEducacaoID.value);
            setLabelOfControl(lblPaisNascimentoID, selPaisNascimentoID.value);
            setLabelOfControl(lblDeficienciaID, selDeficienciaID.value);
            setLabelOfControl(lblRacaID, selRacaID.value);
            
        }    
        else
        {
            setLabelOfControl(lblJurClassificacaoID, selJurClassificacaoID.value);
            setLabelOfControl(lblJurSexo, selJurSexo.value);
        }    
    }
}

function auditoria()
{
    if (dsoSup01.recordset['Auditoria'].value != null)
    {
        if ( window.top.overflyGen.Alert ('Inconsist�ncia(s)' + SYS_SPACE + 'de cadastro:' + dsoSup01.recordset['Auditoria'].value) == 0 )
            return null;
        return false;
    }
}

function loadModalImagem()
{
	var strPars = new String();
	
	var nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'PessoaID' + '\'' + '].value');
 
	var nTipoPessoaID = dsoSup01.recordset['TipoPessoaID'].value;
	
	var sCaller = 'S';
 
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );

	strPars = '?nRegistroID=' + escape(nPessoaID);
	strPars += '&nTipoRegistroID=' + escape(nTipoPessoaID);
	strPars += '&sCaller=' + escape(sCaller);
	
	//DireitoEspecifico
    //Pessoas->F�sicas/jur�dicas->Modal Imagens
    //20100 SFS-Grupo Pessoas -> A1&&A2 -> Permite abrir a modal para gerenciar imagens, caso contrario apenas mostra a imagem cadastrada.
    if ( (nA1 == 1) && (nA2 == 1) )
    {
		htmlPath = SYS_ASPURLROOT + '/modalgen/modalgerimagens.asp'+strPars;
    	
		//showModalWin(htmlPath, new Array(524, 488));
		showModalWin(htmlPath, new Array(724, 488));
    }
    else
    {
		htmlPath = SYS_ASPURLROOT + '/modalgen/modalshowimagens.asp'+strPars;
    
		showModalWin(htmlPath, new Array(0,0));
    }
}

/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos com direitos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function direitoCampos(runBotao5)
{
    var nUserID = getCurrUserID();
    var bReadOnly;
    var bReadOnlyEspecific;

    if (runBotao5) {
        direitoBotao5();
    }

    // UserID temporario Eduardo/Douglas/Janete/Silvia/Batman/Yander/Ze/Drielly/Carla
    // HABS - 19/07/2010 Incluido ID 1385 - Conforme E-mail da Silvia 
    if ((nUserID == 1004) || (nUserID == 1303) || (nUserID == 1083) || (nUserID == 1121) ||
        (nUserID == 1508) || (nUserID == 1356) || (nUserID == 1000) || (nUserID == 1735) || (nUserID == 1385) ||
        (nUserID == 1807) || (nUserID == 2285) || (nUserID == 1820) || (nUserID == 1142) || (nUserID == 2144))
        bReadOnly = false;
    else
        bReadOnly = true;

    if (glb_liberaCampoEspecifico == false) 
        bReadOnlyEspecific = true;
    else    
        bReadOnlyEspecific = false;   

    chkCadastroOK1.disabled = bReadOnly;
    chkCadastroOK2.disabled = bReadOnly;

    txtNumeroFuncionarios.disabled = true;
    txtReceitaBrutaSimplesNacional.style.visibility = 'hidden';

    selEnquadramentoEmpresaID.disabled = bReadOnlyEspecific;
    selRegimeTributarioID.disabled = bReadOnlyEspecific;
    txtJurFaturamentoAnual.disabled = bReadOnlyEspecific;
    chkEhContribuinte.disabled = bReadOnlyEspecific;
    selJurClassificacaoID.disabled = bReadOnlyEspecific;
    
    
}

/********************************************************************
Recebe input de janela modal para discar pelo carrier.
Definir e implementar no pesqlist, no sup e no inf do form.
********************************************************************/
function dialByModalPage(sDDD, sNumero, nPessoaID)
{
	sDDD = sDDD.toString();
	sNumero = sNumero.toString();
	nPessoaID = parseInt(nPessoaID, 10);
		
    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

/********************************************************************
Criado pelo programador
Abre a JM

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalEmailConfirmacao()
{
    var htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modalemailcadastro.asp';

    var strPars = new String();
    strPars = '?sCaller=' + escape('S');
    strPars += '&nPessoaID=' + escape(dsoSup01.recordset['PessoaID'].value);

    showModalWin( (htmlPath + strPars) , new Array(0, 0) );
}

function editRegisterParticular()
{
	if ( glb_timerEditRegister == null )
		glb_timerEditRegister = window.setInterval('editRegisterParticular_AfterTimer()', 750, 'JavaScript');
}

function editRegisterParticular_AfterTimer()
{
	if ( glb_timerEditRegister != null )
    {
        window.clearInterval(glb_timerEditRegister);
        glb_timerEditRegister = null;
    }    
    
	editRegister();
}

function openModalConfirmacaoCadastro()
{       
    var htmlPath;
    var strPars = new String();
    var nWidth = 980;
    var nHeight = 570; 
    var nRegistroID = getCurrDataInControl('sup','txtRegistroID');
    var aEmpresa = getCurrEmpresaData();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');     
    strPars += '&nRegistroID=' + escape(nRegistroID);
    strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);
    

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalconfirmacaocadastro.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Chama a Modal DadosRH

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalDadosRH() {

    var htmlPath;
    var strPars = new String();
    var nPessoaID = dsoSup01.recordset['PessoaID'].value;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');
    strPars += '&nPessoaID=' + escape(nPessoaID);

    htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modaldadosrh.asp' + strPars;
    showModalWin(htmlPath, new Array(555, 462));
}

/********************************************************************
Funcao criada pelo programador.
Chama a Modal S�cios

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalSocios(nPessoaID, nTipoPessoaID) {

    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 600;
    var userID = getCurrUserID();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nTipoPessoaID=' + escape(nTipoPessoaID);
    strPars += '&nCreditoID=' + escape(0);
    strPars += '&nUserID=' + escape(userID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modalgen/modalsocios.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}


/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao do campo ReceitaBrutaSN

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function ReceitaBrutaSN() {
    var visivel = 'hidden';

    if (chkSimplesNacional.checked)
        visivel = 'visible';

    lblReceitaBrutaSimplesNacional.style.visibility = visivel;
    txtReceitaBrutaSimplesNacional.style.visibility = visivel;

}

/**********************************************************************
Fun��o que retorna o Direito em do usu�rio em alterar os registros de 
Enquadramento, Regime Tribut�rio, EhContribuinte
************************************************************************/
function direitoBotao5() 
{
    var aEmpresaID = getCurrEmpresaData();
    var nEmpresaID = aEmpresaID[0];
    var nFormID = window.top.formID;
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aContextoID[1];
    var nSubFormID = window.top.subFormID;
    var nUsuarioID = getCurrUserID();
    
    setConnection(dsoDireito);

    dsoDireito.SQL = 'SELECT dbo.fn_Direitos(' + nEmpresaID + ', ' + nFormID + ', ' + nContextoID + ', ' + nSubFormID + ', 40005, NULL, NULL, 2, ' + nUsuarioID + ', 0, 4) AS DireitoBotao ';

    dsoDireito.ondatasetcomplete = direitoBotao5_DSC;
    dsoDireito.Refresh();
}

function direitoBotao5_DSC() 
{
    glb_liberaCampoEspecifico = dsoDireito.recordset['DireitoBotao'].value; ;            
}

// Regra inclu�da para deixar o campo Observa��o "ReadOnly" para os Vendedores, conforme solicitado no Ticket#2018100896000772 pelo Daniel Gon�alves. LYF 08/10/2018
function EhVendedor() {
    setConnection(dsoEhVendedor);

    dsoEhVendedor.SQL = 'SELECT CONVERT(BIT, COUNT(DISTINCT a.RelacaoID)) AS EhVendedor ' +
                           'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                            'WHERE (a.SujeitoID = ' + getCurrUserID() + '' +
                                'AND a.CargoID IN (651) ' +
                                'AND a.TipoRelacaoID = 31 ' +
                                'AND a.EstadoID = 2 ' +
                                'AND dbo.fn_Colaborador_Empresa(a.SujeitoID) = a.ObjetoID ' +
                                'AND dbo.fn_Pessoa_Localidade(a.ObjetoID, 1, NULL, NULL) = 130 ' +
                                'AND dbo.fn_Pessoa_LiderEquipe(a.SujeitoID) = 0)';

    dsoEhVendedor.ondatasetcomplete = EhVendedor_DSC;
    dsoEhVendedor.Refresh();
}

function EhVendedor_DSC() {
    var bEhVendedor = dsoEhVendedor.recordset['EhVendedor'].value;

    if (bEhVendedor) {
        txtObservacao.disabled = true;
        txtJurObservacao.disabled = true;
    } else {
        txtObservacao.disabled = false;
        txtJurObservacao.disabled = false;
    }
}