/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Pessoas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var __refrEspecSupTimer = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;
    //@@
    var nUserID = getCurrUserID();
	var nFormID = window.top.formID;
	var nSubFormID = window.top.subFormID;
	
	var sAuditoria = '';
	
	if (glb_btnCtlSup == 'SUPREFR')
		sAuditoria = 'dbo.fn_Pessoa_Auditoria(PessoaID, NULL, NULL, 2)';
	else
		sAuditoria = 'NULL';
	

    if (newRegister == true)
    {
        sSQL = 'SELECT TOP 1 *, CONVERT(BIT, dbo.fn_Imagem(' + nFormID + ', ' + nSubFormID + ', PessoaID, 2, NULL, NULL)) AS TemImagem, ' + 
               'CONVERT(VARCHAR, dtNascimento, '+DATE_SQL_PARAM+') as V_dtNascimento,' +
               'CONVERT(VARCHAR, dtFalecimento, '+DATE_SQL_PARAM+') as V_dtFalecimento,' +
               'NULL AS Auditoria, ' +
               'ISNULL((SELECT TOP 1 AtividadeEconomica FROM CNAEs WITH(NOLOCK) WHERE(CNAE = Pessoas.CNAE)), ' + 
					'\'' + 'Classificação Nacional de Atividade Econômica' + '\'' + ') AS DescricaoCNAE, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Pessoas.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               '(CASE WHEN (dbo.fn_TagValor(Observacoes,\'ProgramaMarketing\',\'<\',\':\',1) LIKE \'Marketplace\') THEN 1 ELSE 0 END) AS EhMarketplace, ' +
               '0 AS Responsavel ' +
               'FROM Pessoas WITH(NOLOCK) ' +
               'WHERE ProprietarioID = '+ nID + ' ';
               
        // ID Calculado pela Trigger
        if (window.top.registroID_Type == 16)
        {
            sSQL += 'AND ' + glb_sFldTipoRegistroName + ' = ' + glb_PessoasTipoRegID + ' ';
            
            if ( (glb_PessoasTipoRegID==51) || (glb_PessoasTipoRegID==52) )
                sSQL += ' AND ClassificacaoID = ' + glb_PessoasClassificacaoID + ' ';
        }            
               
        sSQL += 'ORDER BY PessoaID DESC';
    }               
    else
    {


        sSQL = 'SELECT *, CONVERT(BIT, dbo.fn_Imagem(' + nFormID + ', ' + nSubFormID + ', PessoaID, 2, NULL, NULL)) AS TemImagem, ' +
                    'CONVERT(VARCHAR, dtNascimento, '+DATE_SQL_PARAM+') as V_dtNascimento,' +
                    'CONVERT(VARCHAR, dtFalecimento, '+DATE_SQL_PARAM+') as V_dtFalecimento,' +
                    sAuditoria + ' AS Auditoria, ' +
					'ISNULL((SELECT TOP 1 AtividadeEconomica FROM CNAEs WITH(NOLOCK) WHERE(CNAE = Pessoas.CNAE)), ' + 
						'\'' + 'Classificação Nacional de Atividade Econômica' + '\'' + ') AS DescricaoCNAE, ' +
                    'Prop1 = CASE ProprietarioID ' +
                    'WHEN ' + nUserID + ' THEN 1 ' +
                    'ELSE 0 ' +
                    'END, ' +
                    'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
                    'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
	                'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
                    'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Pessoas.AlternativoID)>0 THEN 1 ' +
                    'ELSE 0 ' +
                    'END), ' +
                    '(CASE WHEN (dbo.fn_TagValor(Observacoes,\'ProgramaMarketing\',\'<\',\':\',1) LIKE \'Marketplace\') THEN 1 ELSE 0 END) AS EhMarketplace, ' +
                    '(CASE WHEN ' + nUserID + ' = ProprietarioID THEN 1 WHEN ' + nUserID + ' = AlternativoID THEN 1 WHEN dbo.fn_Pessoa_Grupo(' + nUserID + ') = AlternativoID THEN 1 ELSE 0 END) AS Responsavel ' +
               'FROM Pessoas WITH(NOLOCK) ' + 
               'WHERE PessoaID = ' + nID + ' ' +
               'ORDER BY PessoaID DESC';
	}
	
    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{    
    setConnection(dso);

	var nFormID = window.top.formID;

	var nSubFormID = window.top.subFormID;
    
    var sSQL;

    sSQL = 'SELECT *, CONVERT(BIT, dbo.fn_Imagem(' + nFormID + ', ' + nSubFormID + ', PessoaID, 2, NULL, NULL)) AS TemImagem, ' +
                'CONVERT(VARCHAR, dtNascimento, '+DATE_SQL_PARAM+') as V_dtNascimento,' +
                'CONVERT(VARCHAR, dtFalecimento, '+DATE_SQL_PARAM+') as V_dtFalecimento,' +
                'NULL AS Auditoria, ' +
                'ISNULL((SELECT TOP 1 AtividadeEconomica FROM CNAEs  WHERE(CNAE = Pessoas.CNAE)), ' + 
					'\'' + 'Classificação Nacional de Atividade Econômica' + '\'' + ') AS DescricaoCNAE, ' +
           '0 AS Prop1, 0 AS Prop2, ' +
           '(CASE WHEN (dbo.fn_TagValor(Observacoes,\'ProgramaMarketing\',\'<\',\':\',1) LIKE \'Marketplace\') THEN 1 ELSE 0 END) AS EhMarketplace, ' +
           '0 AS Responsavel ' +
           'FROM Pessoas WITH(NOLOCK) ' +
           'WHERE PessoaID = 0';
    
    dso.SQL = sSQL;          
}              
