using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.prospect.serverside
{
    public partial class pesquisapessoa : System.Web.UI.OverflyPage
    {
        private string documento;

        protected string nDocumento
        {
            set { documento = (value != null) ? value : "0"; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            // Gera o resultado para o usuario.
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "SELECT PessoaID FROM Pessoas_Documentos WITH (NOLOCK) WHERE Numero = '" + documento + "'"
                )
            );
        }
    }
}
