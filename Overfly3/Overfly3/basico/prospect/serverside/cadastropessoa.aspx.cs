using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.prospect.serverside
{
    public partial class cadastropessoa : System.Web.UI.OverflyPage
    {
        private string empresaID;
        private string registroID;
        private string nome;
        private string fantasia;
        private string tipoPessoaID;
        private string classificacaoID;
        private string sexo;
        private string estadoCivilID;
        private string cnae;
        private string paisNascimentoID;
        private string ufNascimento;
        private string cidadeNascimento;
        private string dtnascimento;
        private string ascendencia;
        private string documento1;
        private string documento2;
        private string paisID;
        private string cep;
        private string ufid;
        private string cidadeID;
        private string bairro;
        private string endereco;
        private string numero;
        private string complemento;
        private string ddiTelefone1;
        private string dddTelefone1;
        private string numeroTelefone1;
        private string ddiTelefone2;
        private string dddTelefone2;
        private string numeroTelefone2;
        private string sitio;
        private string email;
        private string observacao;
        private string origem;
        private string usuarioID;
        private string pessoaID;

        private string resposta = null;

        protected string EmpresaID
        {
            set { empresaID = value; }
        }

        protected string RegistroID
        {
            set { registroID = value; }
        }

        protected string Nome
        {
            set { nome = value; }
        }

        protected string Fantasia
        {
            set { fantasia= value; }
        }

        protected string TipoPessoaID
        {
            set { tipoPessoaID = value; }
        }

        protected string ClassificacaoID
        {
            set { classificacaoID = value; }
        }

        protected string Sexo
        {
            set { sexo = value; }
        }

        protected string EstadoCivilID
        {
            set { estadoCivilID = value; }
        }

        protected string CNAE
        {
            set { cnae = value; }
        }

        protected string PaisNascimentoID
        {
            set { paisNascimentoID = value; }
        }

        protected string UFNascimento
        {
            set { ufNascimento = value; }
        }

        protected string CidadeNascimento
        {
            set { cidadeNascimento = value; }
        }

        protected string dtNascimento
        {
            set { dtnascimento = value; }
        }

        protected string Ascendencia
        {
            set { ascendencia = value; }
        }

        protected string Documento1
        {
            set { documento1 = value; }
        }

        protected string Documento2
        {
            set { documento2 = value; }
        }

        protected string  PaisID
        {
            set { paisID = value; }
        }

        protected string CEP
        {
            set { cep = value; }
        }

        protected string UFID
        {
            set { ufid = value; }
        }

        protected string CidadeID
        {
            set { cidadeID = value; }
        }

        protected string Bairro
        {
            set { bairro = value; }
        }

        protected string Endereco
        {
            set { endereco = value; }
        }

        protected string Numero
        {
            set { numero = value; }
        }

        protected string Complemento
        {
            set { complemento = value; }
        }

        protected string DDITelefone1
        {
            set { ddiTelefone1 = value; }
        }

        protected string DDDTelefone1
        {
            set { dddTelefone1 = value; }
        }

        protected string NumeroTelefone1
        {
            set { numeroTelefone1 = value; }
        }        

        protected string DDITelefone2
        {
            set { ddiTelefone2 = value; }
        }

        protected string DDDTelefone2
        {
            set { dddTelefone2 = value; }
        }

        protected string NumeroTelefone2
        {
            set { numeroTelefone2 = value; }
        }

        protected string Sitio
        {
			set { sitio = value; }
        }

        protected string Email
        {
            set { email = value; }
        }

        protected string Observacao
        {
            set { observacao = value; }
        }

        protected string Origem
        {
            set { origem = value; }
        }

        protected string UsuarioID
        {
            set { usuarioID = value; }
        }

        protected string PessoaID
        {
            set { pessoaID = value; }
        }

        private void pessoaCadastro()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[44];

            parameters[0] = new ProcedureParameters(
                "@TipoCadastroID",
                System.Data.SqlDbType.Int,
                0);

            parameters[1] = new ProcedureParameters(
                "@ClienteID",
                System.Data.SqlDbType.Int,
                DBNull.Value);

            parameters[2] = new ProcedureParameters(
                "@EmpresaID",
                System.Data.SqlDbType.Int,
                empresaID);

            parameters[3] = new ProcedureParameters(
                "@RegistroID",
                System.Data.SqlDbType.Int,
                (registroID != null ? (Object)registroID : DBNull.Value));

            parameters[4] = new ProcedureParameters(
                "@Nome",
                System.Data.SqlDbType.VarChar,
                (nome != null ? (Object)registroID : DBNull.Value));
            parameters[4].Length = 40;

            parameters[5] = new ProcedureParameters(
                "@Fantasia",
                System.Data.SqlDbType.VarChar,
                (fantasia != null ? (Object)registroID : DBNull.Value));
            parameters[5].Length = 20;

            parameters[6] = new ProcedureParameters(
                "@TipoPessoaID",
                System.Data.SqlDbType.Int,
                (tipoPessoaID != null ? (Object)registroID : DBNull.Value));

            parameters[7] = new ProcedureParameters(
                "@ClassificacaoID",
                System.Data.SqlDbType.Int,
                (classificacaoID != null ? (Object)registroID : DBNull.Value));

            parameters[8] = new ProcedureParameters(
                "@Sexo",
                System.Data.SqlDbType.Int,
                (sexo != null ? (Object)registroID : DBNull.Value));

            parameters[9] = new ProcedureParameters(
                "@EstadoCivilID",
                System.Data.SqlDbType.Int,
                (estadoCivilID != null ? (Object)registroID : DBNull.Value));

            parameters[10] = new ProcedureParameters(
                "@CNAECNPJ",
                System.Data.SqlDbType.VarChar,
                (cnae != null ? (Object)registroID : DBNull.Value));
            parameters[10].Length = 20;

            parameters[11] = new ProcedureParameters(
                "@PaisNascimentoID",
                System.Data.SqlDbType.Int,
                (paisNascimentoID != null ? (Object)registroID : DBNull.Value));

            parameters[12] = new ProcedureParameters(
                "@UFNascimento",
                System.Data.SqlDbType.VarChar,
                (ufNascimento != null ? (Object)registroID : DBNull.Value));
            parameters[12].Length = 2;

            parameters[13] = new ProcedureParameters(
                "@CidadeNascimento",
                System.Data.SqlDbType.VarChar,
                (cidadeNascimento != null ? (Object)registroID : DBNull.Value));
            parameters[13].Length = 30;

            parameters[14] = new ProcedureParameters(
                "@dtNascimento",
                System.Data.SqlDbType.DateTime,
                dtnascimento != null ? (Object)dtnascimento : DBNull.Value);

            parameters[15] = new ProcedureParameters(
                "@Ascendencia",
                System.Data.SqlDbType.VarChar,
                (ascendencia != null ? (Object)registroID : DBNull.Value));
            parameters[15].Length = 20;

            parameters[16] = new ProcedureParameters(
                "@Documento1",
                System.Data.SqlDbType.VarChar,
                (documento1 != null ? (Object)registroID : DBNull.Value));
            parameters[16].Length = 20;

            parameters[17] = new ProcedureParameters(
                "@Documento2",
                System.Data.SqlDbType.VarChar,
                (documento1 != null ? (Object)registroID : DBNull.Value));
            parameters[17].Length = 20;

            parameters[18] = new ProcedureParameters(
                "@PaisID",
                System.Data.SqlDbType.Int,
                (paisID != null ? (Object)registroID : DBNull.Value));

            parameters[19] = new ProcedureParameters(
                "@CEP",
                System.Data.SqlDbType.VarChar,
                (cep != null ? (Object)registroID : DBNull.Value));
            parameters[19].Length = 9;

            parameters[20] = new ProcedureParameters(
                "@UFID",
                System.Data.SqlDbType.Int,
                (ufid != null ? (Object)registroID : DBNull.Value));

            parameters[21] = new ProcedureParameters(
                "@CidadeID",
                System.Data.SqlDbType.Int,
                (cidadeID != null ? (Object)registroID : DBNull.Value));

            parameters[22] = new ProcedureParameters(
                "@Bairro",
                System.Data.SqlDbType.VarChar,
                (bairro != null ? (Object)registroID : DBNull.Value));
            parameters[22].Length = 20;

            parameters[23] = new ProcedureParameters(
                "@Endereco",
                System.Data.SqlDbType.VarChar,
                (endereco != null ? (Object)registroID : DBNull.Value));
            parameters[23].Length = 35;

            parameters[24] = new ProcedureParameters(
                "@Numero",
                System.Data.SqlDbType.VarChar,
                (numero != null ? (Object)registroID : DBNull.Value));
            parameters[24].Length = 6;

            parameters[25] = new ProcedureParameters(
                "@Complemento",
                System.Data.SqlDbType.VarChar,
                (complemento != null ? (Object)registroID : DBNull.Value));
            parameters[25].Length = 20;

            parameters[26] = new ProcedureParameters(
                "@DDITelefone1",
                System.Data.SqlDbType.VarChar,
                (ddiTelefone1 != null ? (Object)registroID : DBNull.Value));
            parameters[26].Length = 4;

            parameters[27] = new ProcedureParameters(
                "@DDDTelefone1",
                System.Data.SqlDbType.VarChar,
                (dddTelefone1 != null ? (Object)registroID : DBNull.Value));
            parameters[27].Length = 4;

            parameters[28] = new ProcedureParameters(
                "@NumeroTelefone1",
                System.Data.SqlDbType.VarChar,
                (numeroTelefone1 != null ? (Object)registroID : DBNull.Value));
            parameters[28].Length = 9;

            parameters[29] = new ProcedureParameters(
                "@DDITelefone2",
                System.Data.SqlDbType.VarChar,
                (ddiTelefone2 != null ? (Object)registroID : DBNull.Value));
            parameters[29].Length = 4;

            parameters[30] = new ProcedureParameters(
                "@DDDTelefone2",
                System.Data.SqlDbType.VarChar,
                (dddTelefone2 != null ? (Object)registroID : DBNull.Value));
            parameters[30].Length = 4;

            parameters[31] = new ProcedureParameters(
                "@NumeroTelefone2",
                System.Data.SqlDbType.VarChar,
                (numeroTelefone2 != null ? (Object)registroID : DBNull.Value));
            parameters[31].Length = 9;

            parameters[32] = new ProcedureParameters(
                "@ContatoPerfilID",
                System.Data.SqlDbType.Int,
                DBNull.Value);

			parameters[33] = new ProcedureParameters(
				"@RecebeEmail",
				System.Data.SqlDbType.Bit,
				1);

			parameters[34] = new ProcedureParameters(
				"@Corporativo",
				System.Data.SqlDbType.Bit,
				tipoPessoaID.Equals("52") ? 1 : 0);

			parameters[35] = new ProcedureParameters(
				"@Comercial",
				System.Data.SqlDbType.Bit,
				1);

			parameters[36] = new ProcedureParameters(
				"@Financeiro",
				System.Data.SqlDbType.Bit,
				1);
			
			parameters[37] = new ProcedureParameters(
				"@Tecnica",
				System.Data.SqlDbType.Bit,
				1);
			 
			parameters[38] = new ProcedureParameters(
				"@LojasInteresse",
				System.Data.SqlDbType.VarChar,
				DBNull.Value);
			parameters[38].Length = 8000;
			 
			parameters[39] = new ProcedureParameters(
                "@Site",
                System.Data.SqlDbType.VarChar,
                (sitio != null ? (Object)registroID : DBNull.Value));
            parameters[39].Length = 80;

            parameters[40] = new ProcedureParameters(
                "@Email",
                System.Data.SqlDbType.VarChar,
                (email != null ? (Object)registroID : DBNull.Value));
            parameters[40].Length = 80;

            parameters[41] = new ProcedureParameters(
                "@Origem",
                System.Data.SqlDbType.Int,
                (origem != null ? (Object)registroID : DBNull.Value));

            parameters[42] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                (usuarioID != null ? (Object)registroID : DBNull.Value));

            parameters[43] = new ProcedureParameters(
                "@PessoaID",
                System.Data.SqlDbType.Int,
                (pessoaID != null ? (Object)registroID : DBNull.Value));

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Pessoa_Cadastro",
                parameters);

            resposta = parameters[43].Data.ToString();
        }

        protected override void PageLoad(object sender, EventArgs e)
        {        
            pessoaCadastro();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + (resposta != null ? "'" + resposta + "' " : "NULL") + " as PessoaID, " +
                    "'' as fldErrorText"
                )
            );
        }
    }
}
