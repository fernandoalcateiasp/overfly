<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="prospectsup01Html" name="prospectsup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf        
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/prospect/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/prospect/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="prospectsup01Body" name="prospectsup01Body" LANGUAGE="javascript" onload="return window_onload()">           

    <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblNome" name="lblNome" class="lblGeneral">Nome</p>
        <input type="text" id="txtNome" name="txtNome" DATASRC="#dsoSup01" DATAFLD="Nome" class="fldGeneral">
        <p id="lblFantasia" name="lblFantasia" class="lblGeneral">Fantasia</p>
        <input type="text" id="txtFantasia" name="txtFantasia" DATASRC="#dsoSup01" DATAFLD="Fantasia" class="fldGeneral">
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoPessoaID"></select>    
		<p id="lblClassificacaoID" name="lblClassificacaoID" class="lblGeneral" title="Classifica��o da pessoa em rela��o ao mercado">Classifica��o</p>
		<select id="selClassificacaoID" name="selClassificacaoID" DATASRC="#dsoSup01" DATAFLD="ClassificacaoID" class="fldGeneral" title="Classifica��o da pessoa em rela��o ao mercado"></select>

		<p id="lblEmpresaID" name="lblEmpresaID" class="lblGeneral">Empresa</p>
		<select id="selEmpresaID" name="selEmpresaID" DATASRC="#dsoSup01" DATAFLD="EmpresaID" class="fldGeneral">
<%
	Dim rsData
	Dim strSQL
	
    Set rsData = Server.CreateObject("ADODB.Recordset")
    strSQL = "SELECT b.PessoaID, b.Fantasia " & _
		"FROM RelacoesPesRec a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " & _
		"WHERE a.EstadoID=2 AND a.TipoRelacaoID=12 AND a.ObjetoID=999 AND a.SujeitoID=b.PessoaID"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    Do While Not rsData.EOF
        Response.Write( "<option value='" & CStr(rsData.Fields("PessoaID").Value) & "'" )
    	Response.Write( ">" & rsData.Fields("Fantasia") & "</option>" )
        rsData.MoveNext
    Loop

    rsData.Close
    Set rsData = Nothing
%>
    </select>

        <p id="lblMailing" name="lblMailing" class="lblGeneral">M</p>
		<input type="checkbox" id="chkMailing" name="chkMailing" DATASRC="#dsoSup01" DATAFLD="Mailing" class="fldGeneral" Title="Recebe mala direta?">

        <p id="lblEmails" name="lblEmails" class="lblGeneral">E</p>
		<input type="checkbox" id="chkEmails" name="chkEmails" DATASRC="#dsoSup01" DATAFLD="Emails" class="fldGeneral" Title="Recebe e-mail marketing?">
		
        <p id="lblDocumento" name="lblDocumento" class="lblGeneral">Documento</p>
        <input type="text" id="txtDocumento" name="txtDocumento" DATASRC="#dsoSup01" DATAFLD="Documento" class="fldGeneral" Title="CNPJ/CPF">
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral">
		<p id="lblPaisID" name="lblPaisID" class="lblGeneral" title="">Pa�s</p>
		<select id="selPaisID" name="selPaisID" DATASRC="#dsoSup01" DATAFLD="PaisID" class="fldGeneral" title=""></select>
        <p id="lblCEP" name="lblCEP" class="lblGeneral">CEP</p>
        <input type="text" id="txtCEP" name="txtCEP" DATASRC="#dsoSup01" DATAFLD="CEP" class="fldGeneral">
        <p id="lblUFID" name="lblUFID" class="lblGeneral">UF</p>
        <select id="selUFID" name="selUFID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="UFID"></select>
        <p id="lblCidadeID" name="lblCidadeID" class="lblGeneral">Cidade</p>
        <select id="selCidadeID" name="selCidadeID" DATASRC="#dsoSup01" DATAFLD="CidadeID" class="fldGeneral" title=""></select>
		<input type="image" id="btnFindCidade" name="btnFindCidade" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
        <p id="lblBairro" name="lblBairro" class="lblGeneral">Bairro</p>
        <input type="text" id="txtBairro" name="txtBairro" DATASRC="#dsoSup01" DATAFLD="Bairro" class="fldGeneral">
        <p id="lblEndereco" name="lblEndereco" class="lblGeneral">Endere�o</p>
        <input type="text" id="txtEndereco" name="txtEndereco" DATASRC="#dsoSup01" DATAFLD="Endereco" class="fldGeneral">
        <p id="lblNumero" name="lblNumero" class="lblGeneral">N�mero</p>
        <input type="text" id="txtNumero" name="txtNumero" DATASRC="#dsoSup01" DATAFLD="Numero" class="fldGeneral">
        <p id="lblComplemento" name="lblComplemento" class="lblGeneral">Complemento</p>
        <input type="text" id="txtComplemento" name="txtComplemento" DATASRC="#dsoSup01" DATAFLD="Complemento" class="fldGeneral">
        <p id="lblTelefoneDDD" name="lblTelefoneDDD" class="lblGeneral">DDD</p>
        <input type="text" id="txtDDD" name="txtDDD" DATASRC="#dsoSup01" DATAFLD="TelefoneDDD" class="fldGeneral">
        <p id="lblTelefone" name="lblTelefone" class="lblGeneral">Telefone</p>
        <input type="text" id="txtTelefone" name="txtTelefone" DATASRC="#dsoSup01" DATAFLD="TelefoneNumero" class="fldGeneral">
        <p id="lblCelularDDD" name="lblCelularDDD" class="lblGeneral">DDD</p>
        <input type="text" id="txtCelularDDD" name="txtCelularDDD" DATASRC="#dsoSup01" DATAFLD="CelularDDD" class="fldGeneral">
        <p id="lblCelular" name="lblCelular" class="lblGeneral">Celular</p>
        <input type="text" id="txtCelular" name="txtCelular" DATASRC="#dsoSup01" DATAFLD="CelularNumero" class="fldGeneral">
        <p id="lblSite" name="lblSite" class="lblGeneral">Site</p>
        <input type="text" id="txtSite" name="txtSite" DATASRC="#dsoSup01" DATAFLD="Site" class="fldGeneral" title="">
        <p id="lblEmail" name="lblEmail" class="lblGeneral">E-Mail</p>
        <input type="text" id="txtEmail" name="txtEmail" DATASRC="#dsoSup01" DATAFLD="Email" class="fldGeneral" title="">
        <p id="lblPessoaID" name="lblPessoaID" class="lblGeneral">PessoaID</p>
        <input type="text" id="txtPessoaID" name="txtPessoaID" DATASRC="#dsoSup01" DATAFLD="PessoaID" class="fldGeneral" title="">
        <p id="lblParceiroID" name="lblParceiroID" class="lblGeneral">Contato de</p>
        <input type="text" id="txtParceiroID" name="txtParceiroID" DATASRC="#dsoSup01" DATAFLD="ParceiroID" class="fldGeneral" title="">
    </div>
</body>

</html>
