/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// controla o retorno do servidor dos dados de combos dinamicos
var glb_CounterCmbsDynamics = 0;

// guarda o botao clicado enviado pelo framework
var glb_BtnFromFramWork;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoCurrData = new CDatatransport('dsoCurrData');
var dsoUFs = new CDatatransport('dsoUFs');
var dsoCidade = new CDatatransport('dsoCidade');
var dsoAddrData = new CDatatransport('dsoAddrData');
var dsoGen01 = new CDatatransport('dsoGen01');
var dsoClassificacao = new CDatatransport('dsoClassificacao');
var dsoPesquisaPessoa = new CDatatransport('dsoPesquisaPessoa');
var dsoCadastroPessoa = new CDatatransport('dsoCadastroPessoa');

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
						  ['selPaisID','2']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/basico/prospect/inferior.asp',
                              SYS_PAGESURLROOT + '/basico/prospect/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'ProspectID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
							['lblEstadoID','txtEstadoID',2,1],
							['lblNome','txtNome',40,1],
							['lblFantasia','txtFantasia',20,1],
							['lblTipoRegistroID','selTipoRegistroID',18,1],
							['lblClassificacaoID','selClassificacaoID',19,2],
							['lblEmpresaID','selEmpresaID',18,2],
							['lblMailing','chkMailing',3,2],
							['lblEmails','chkEmails',3,2],
							['lblDocumento','txtDocumento',14,2],
							['lblObservacao','txtObservacao',32,2],
							['lblPaisID','selPaisID',22,3],
							['lblCEP','txtCEP',9,3],
							['lblUFID','selUFID',6,3],
							['lblCidadeID','selCidadeID',30,3],
							['btnFindCidade','btn',24,3],
							['lblBairro','txtBairro',20,3],
							['lblEndereco','txtEndereco',35,4],
							['lblNumero','txtNumero',9,4],
							['lblComplemento','txtComplemento',20,4],
							['lblTelefoneDDD','txtDDD',4,4],
							['lblTelefone','txtTelefone',8,4],
							['lblCelularDDD','txtCelularDDD',4,4],
							['lblCelular','txtCelular',8,4],
							['lblSite','txtSite',38,5,0,-3],
							['lblEmail','txtEmail',38,5],
							['lblPessoaID','txtPessoaID',8,5],
							['lblParceiroID','txtParceiroID',8,5]], null, null, true);

	chkMailing.onclick = chkMailing_onclick;
	chkEmails.onclick = chkEmails_onclick;
	txtCEP.onblur = txtCEP_onblur;
	txtEmail.ondblclick = txtEmail_ondblclick;
	txtSite.ondblclick = txtSite_ondblclick;
}

function txtEmail_ondblclick()
{
    if ((trimStr(txtEmail.value) != '') &&
		(trimStr(txtEmail.value) != '_@_.com.br'))
		window.open('mailto:' + txtEmail.value);
}

function txtSite_ondblclick()
{
	var sSite = '';
	
    if ((trimStr(txtSite.value) != '') &&
		(trimStr(txtSite.value) != 'http://www._.com.br'))
	{
		if (txtSite.value.substr(0,7).toUpperCase() != 'HTTP://')
			sSite = 'http://' + txtSite.value;
		else
			sSite = txtSite.value;
			
		window.open(sSite);
	}	
}

function txtCEP_onblur()
{
	if (trimStr(txtCEP.value) == '')
		return;
		
	if (selPaisID.value == '')
		return;

	clearComboEx(['selCidadeID']);
	lockCombos(false);
	
    lockInterface(true);

	// trazer as cidades do servidor
    var strPars = new String();
    strPars = '?';
    strPars += 'sCep=';
    strPars += escape(txtCEP.value);
    strPars += '&nPaisID=';
    strPars += escape(selPaisID.value);

	dsoAddrData.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/findcepex.aspx'+strPars;
	dsoAddrData.ondatasetcomplete = dsoAddrDataComplete_DSC;
	dsoAddrData.refresh();
}

function dsoAddrDataComplete_DSC() {
    var htmlPath = '';
    var strPars = '';

    if ((!dsoAddrData.recordset.BOF) && (!dsoAddrData.recordset.EOF))
    {
		if (dsoAddrData.recordset.RecordCount == 1)
		{
			dsoAddrData.recordset.MoveFirst;
			oldDataSrc = selCidadeID.dataSrc;
			oldDataFld = selCidadeID.dataFld;
			selCidadeID.dataSrc = '';
			selCidadeID.dataFld = '';
			optionStr = dsoAddrData.recordset['Cidade'].value;
			optionValue = dsoAddrData.recordset['CidadeID'].value;
			var oOption = document.createElement("OPTION");
			oOption.text = optionStr;
			oOption.value = optionValue;
			selCidadeID.add(oOption);
			selCidadeID.dataSrc = oldDataSrc;
			selCidadeID.dataFld = oldDataFld;
			dsoSup01.recordset['CidadeID'].value = optionValue;

			if (dsoAddrData.recordset['UF'].value != null)
				dsoSup01.recordset['UFID'].value  = dsoAddrData.recordset['UFID'].value;
			else
				dsoSup01.recordset['UFID'].value  = 0;
				
			if ((dsoAddrData.recordset['DDD'].value != null) && (dsoAddrData.recordset['DDD'].value != ''))
			{
				txtDDD.value = dsoAddrData.recordset['DDD'].value;
				txtCelularDDD.value = dsoAddrData.recordset['DDD'].value;
			}
				
			lockInterface(false);				
		}
    }
    else
    {
		clearComboEx(['selCidadeID']);
        /*fg.TextMatrix(keepCurrLineInGrid(),5) = '';
        fg.TextMatrix(keepCurrLineInGrid(),6) = '';
        fg.TextMatrix(keepCurrLineInGrid(),12) = '';
        fg.TextMatrix(keepCurrLineInGrid(),13) = '';*/
        
        if ( window.top.overflyGen.Alert ('Nenhuma cidade dispon�vel com este CEP.') == 0 )
        {
			lockInterface(false);
            return null;
        }    
    }             
    
    lockInterface(false);
    return null;
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWPROSPECT')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields()
{
    txtPessoaID.readOnly = true;
    txtParceiroID.readOnly = true;    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    if ( (btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG') )
    {
		setFieldsColor(false);
		setFieldsHint();
        startDynamicCmbs();
    }    
    else    
        // volta para a automacao    
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    //adjustLabelsCombos('selTipoLancamentoID', true);
    if (btnClicked == 'SUPINCL')
    {
        // funcao que controla campos read-only
        controlReadOnlyFields();
    }    

	setFieldsColor(false);
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
    {
        adjustSupInterface();
        fillCmbClassificacao();
    }
    else if ( cmbID == 'selPaisID' )
        selPaisChanged();
	
	adjustLabelsCombos(false);

    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
	openModalCity();
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    // Documentos
    if (btnClicked == 1) {
        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    }
	else if (btnClicked == 4)
		cadastroPessoa();
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
	if (btnClicked == 'SUPOK')
	{
		if (fieldsOK())
			verifyDocNumber();

		return false;
	}
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALENDERECOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // Escreve UF, cidade e CEP no grid de enderecos
            writeAddress(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            if (fg.disabled == false)
            {
                window.focus();
                fg.focus();
            }    
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            if (fg.disabled == false)
            {
                window.focus();
                fg.focus();
            }    
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    
}

function writeAddress(aAddress)
{
    var oldDataSrc = '';
    var oldDataFld = '';
    
    // Allplus
    if (trimStr(txtCEP.value) != '.')
		txtCEP.value = aAddress[2];

    clearComboEx(['selCidadeID']);
    
    if ((aAddress[1] != null) && (aAddress[1] != ''))
    {
        oldDataSrc = selCidadeID.dataSrc;
        oldDataFld = selCidadeID.dataFld;
        selCidadeID.dataSrc = '';
        selCidadeID.dataFld = '';

        optionStr = aAddress[6];
	    optionValue = aAddress[1];
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selCidadeID.add(oOption);

        selCidadeID.dataSrc = oldDataSrc;
        selCidadeID.dataFld = oldDataFld;
        
        dsoSup01.recordset['UFID'].value = aAddress[0];
        selOptByValueInSelect(getHtmlId(), 'selUFID', aAddress[0]);
        dsoSup01.recordset['CidadeID'].value = aAddress[1];
        txtDDD.value = aAddress[7];
        txtCelularDDD.value = aAddress[7];
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
	
	lockCombos(false);
	controlReadOnlyFields();
	setFieldsColor(true);
    supInitEditMode_Continue();
}

function startIncl()
{
	setFieldsColor(true);
	var aEmpresa = getCurrEmpresaData();
	var nEmpresaPais = aEmpresa[1];
	
	clearComboEx(['selUFID', 'selCidadeID']);
	lockCombos(false);
	dsoSup01.recordset['Email'].value = '_@_.com.br';
	dsoSup01.recordset['Site'].value = 'http://www._.com.br';
	dsoSup01.recordset['PaisID'].value = nEmpresaPais;
	selOptByValueInSelect(getHtmlId(), 'selPaisID', nEmpresaPais);
	dsoSup01.recordset['EmpresaID'].value = aEmpresa[0];
	selOptByValueInSelect(getHtmlId(), 'selEmpresaID', aEmpresa[0]);
	dsoSup01.recordset['TipoPessoaID'].value = 51;
	selOptByValueInSelect(getHtmlId(), 'selTipoPessoaID', 51);
	dsoSup01.recordset['ClassificacaoID'].value = 66;
	selOptByValueInSelect(getHtmlId(), 'selClassificacaoID', 66);
	selPaisChanged();
	fillCmbClassificacao();
}
/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    showBtnsEspecControlBar('sup', true, [1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios','Procedimento','Cadastrar pessoa']);

    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 0);
}

function startDynamicCmbs()
{
	var nPaisID = 0;
	var nCidadeID = 0;
	var nTipoPessoaID = 0;
	
	nPaisID = dsoSup01.recordset['PaisID'].value;
	if ((nPaisID==null)||(nPaisID==''))
		nPaisID = 0;
		
	nCidadeID = dsoSup01.recordset['CidadeID'].value;
	if ((nCidadeID==null)||(nCidadeID==''))
		nCidadeID = 0;
		
	nTipoPessoaID = dsoSup01.recordset['TipoPessoaID'].value;
	if ((nTipoPessoaID==null)||(nTipoPessoaID==''))
		nTipoPessoaIDD = 0;
		
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selProdutoID e selFornecedorID)
    glb_CounterCmbsDynamics = 3;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selProdutoID)
    setConnection(dsoUFs);
    
    dsoUFs.SQL = 'SELECT LocalidadeID AS fldID, CodigoLocalidade2 AS fldName ' +
                          'FROM Localidades WITH(NOLOCK) ' +
                          'WHERE EstadoID=2 AND TipoLocalidadeID=204 AND LocalizacaoID = ' + nPaisID;

    dsoUFs.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoUFs.Refresh();
    
    // parametrizacao do dso dsoCmbDynamic02 (designado para selFornecedorID)
    setConnection(dsoCidade);
    
    dsoCidade.SQL = 'SELECT 0 as fldID, SPACE(0) AS fldName ' +
					'UNION ALL ' +
					'SELECT LocalidadeID as fldID, Localidade AS fldName ' +
                    'FROM Localidades WITH(NOLOCK) ' +
                    'WHERE LocalidadeID = ' + nCidadeID;

    dsoCidade.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCidade.Refresh();

    setConnection(dsoClassificacao);
    
    dsoClassificacao.SQL = 'SELECT ItemMasculino AS fldName, ItemID AS fldID ' +
		'FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 13 AND Filtro LIKE ' + '\'' + '%(' +
			nTipoPessoaID + ')%' + '\'' + ' ' +
		'ORDER BY fldName';

    dsoClassificacao.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoClassificacao.Refresh();
}

function dsoCmbDynamic_DSC() {
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selUFID, selCidadeID, selClassificacaoID];
    var aDSOsDunamics = [dsoUFs, dsoCidade, dsoClassificacao];

    // Inicia o carregamento de combos dinamicos (selProdutoID
    // e selFornecedorID)

    clearComboEx(['selUFID', 'selCidadeID', 'selClassificacaoID']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<3; i++)
        {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';

            while (! aDSOsDunamics[i].recordset.EOF )
            {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }

            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        // volta para a automacao
	    finalOfSupCascade(glb_BtnFromFramWork);
	}
	
	adjustLabelsCombos(true);
	setFieldsHint();
	return null;
}

function openModalCity()
{
    var htmlPath;
    if (selPaisID.value == '')
		return null;
		
    // mandar os parametros para o servidor
    var strPars = new Array();
    strPars = '?';
    strPars += 'sPaisID=' + escape(selPaisID.value);
    strPars += '&nCEP=' + escape(trimStr(txtCEP.value) == '' ? '0' : trimStr(txtCEP.value));
    strPars += '&sEstadoID=' + escape(selUFID.value != '' ? selUFID.value : '0');
    strPars += '&sCidadeID=' + escape(selCidadeID.value != '' ? selCidadeID.value : '0');
    strPars += '&sCaller=' + escape('S');
       
    // carregar modal - faz operacao de banco no carregamento    
    htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalendereco.asp'+strPars;
    showModalWin(htmlPath, new Array(410,334));
    
    writeInStatusBar('child', 'cellMode', 'Cidades', true);    
}

function lockCombos(bLock)
{
	selTipoRegistroID.disabled = bLock || (selTipoRegistroID.length == 0);
	selClassificacaoID.disabled = bLock || (selClassificacaoID.length == 0);
	selPaisID.disabled = bLock || (selPaisID.length == 0);
	selUFID.disabled = bLock || (selUFID.length == 0);
	selCidadeID.disabled = bLock || (selCidadeID.length == 0);
	adjustLabelsCombos(false);
}

function selPaisChanged()
{
	clearComboEx(['selUFID','selCidadeID']);
    setConnection(dsoUFs);
    dsoUFs.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL ' +
		'SELECT LocalidadeID as fldID, CodigoLocalidade2 AS fldName ' +
		'FROM Localidades WITH(NOLOCK) WHERE (EstadoID=2 and TipolocalidadeID=204 and LocalizacaoID=' + selPaisID.value + ')';
    dsoUFs.ondatasetcomplete = selPaisChanged_DSC;
    dsoUFs.Refresh();
}

function selPaisChanged_DSC() {
    while (! dsoUFs.recordset.EOF )
    {
        optionStr = dsoUFs.recordset['fldName'].value;
	    optionValue = dsoUFs.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selUFID.add(oOption);
        dsoUFs.recordset.MoveNext();
    }
    
    if (selUFID.length == 2)
		dsoSup01.recordset['UFID'] = selUFID.options(1).value;
	else
		selUFID.selectedIndex = 0;
    lockCombos(false);
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
        setLabelOfControl(lblTipoRegistroID, dsoSup01.recordset['TipoPessoaID'].value);
        setLabelOfControl(lblClassificacaoID, dsoSup01.recordset['ClassificacaoID'].value);
        setLabelOfControl(lblEmpresaID, dsoSup01.recordset['EmpresaID'].value);
        setLabelOfControl(lblPaisID, dsoSup01.recordset['PaisID'].value);
        setLabelOfControl(lblUFID, dsoSup01.recordset['UFID'].value);
        setLabelOfControl(lblCidadeID, dsoSup01.recordset['CidadeID'].value);
    }
    else
    {
        setLabelOfControl(lblTipoRegistroID, selTipoRegistroID.value);
        setLabelOfControl(lblClassificacaoID, selClassificacaoID.value);
        setLabelOfControl(lblEmpresaID, selEmpresaID.value);
        setLabelOfControl(lblPaisID, selPaisID.value);
        setLabelOfControl(lblUFID, (selUFID.value == 0 ? '' : selUFID.value));
        setLabelOfControl(lblCidadeID, (selCidadeID.value == 0 ? '' : selCidadeID.value));
    }
}


/********************************************************************
Valida o CGC/CPF digitado no banco de dados

Parametros:
nenhum

Retorno:
true ou false
********************************************************************/
function verifyDocNumber()
{
    // stripa o digitado
    txtDocumento.value = trimStr(txtDocumento.value);

    var sNumero = txtDocumento.value;
    var nTipo = 0;
    var nVerifica = 1;
    
    txtDocumento.value = sNumero;
    
    if (sNumero.length == 0)
    {
		saveRegister_1stPart();
        return false;
	}
    
    if (selTipoRegistroID.value == 52)
        nTipo = 1;
    else if (selTipoRegistroID.value == 51)
        nTipo = 2;
    else
    {
		if ( window.top.overflyGen.Alert ('Defina o Tipo de Pessoa.') == 0 )
			return null;
		
		window.focus();
		selTipoRegistroID.focus();
		return null;
	}
		
	if ((selPaisID.value == '0') || (selPaisID.value == ''))
    {
		if ( window.top.overflyGen.Alert ('Defina o Pa�s.') == 0 )
			return null;
		
		window.focus();
		selTipoRegistroID.focus();
		return null;
	}
    
    lockInterface(true);

    var nTipoDocumentoID=0;
    if (nTipo == 1)
        nTipoDocumentoID=111;
    else
        nTipoDocumentoID=101;
                
    setConnection(dsoGen01);
    
    dsoGen01.SQL = 'SELECT ' +
			'dbo.fn_Documento_Verifica(' + '\'' + sNumero + '\'' + ', ' + 
												  selPaisID.value + ', ' + 
												  nTipoDocumentoID + ') AS Verificacao ';
    
    dsoGen01.ondatasetcomplete = verifyDocNumber_DSC;
    dsoGen01.Refresh();
}    
/********************************************************************
Retorno do servidor da funcao que verifica no servidor,
se o numero do documento e unico e valido no cadastro de pessoas

Parametros:
nenhum

Retorno:
nao relevante
********************************************************************/
function verifyDocNumber_DSC() {
    var bValido = false;

    if ( !(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF) )
		bValido = (dsoGen01.recordset['Verificacao'].value != 0);
	
	if (!bValido)
	{
		if ( window.top.overflyGen.Alert ('N�mero do documento inv�lido.') == 0 )
			return null;
					
		lockInterface(false);
		txtDocumento.focus();
		return null;
	}
	
	lockInterface(false);
	saveRegister_1stPart();	
    return null;    
}

function chkMailing_onclick()
{
	setFieldsColor(true);
}

function chkEmails_onclick()
{
	setFieldsColor(true);
}

function setFieldsColor(bEditingMode)
{
	var bMailing = chkMailing.checked;
	var bEmails = chkEmails.checked;

	txtSite.style.color = (bEditingMode ? 'black' : 'blue');
	txtEmail.style.color = (bEditingMode ? 'black' : 'blue');

	lblNome.style.color = (bEditingMode ? 'blue' : 'black');
	lblFantasia.style.color = (bEditingMode ? 'blue' : 'black');
	lblTipoRegistroID.style.color = (bEditingMode ? 'green' : 'black');
	lblClassificacaoID.style.color = (bEditingMode ? 'green' : 'black');
	lblEmpresaID.style.color = (bEditingMode ? 'green' : 'black');
	lblDocumento.style.color = (bEditingMode ? 'green' : 'black');

	lblPaisID.style.color = (bEditingMode ? 'green' : 'black');
	lblCEP.style.color = (bEditingMode ? (bMailing ? 'blue' : 'black') : 'black');
	lblUFID.style.color = (bEditingMode ? (bMailing ? 'green' : 'black') : 'black');
	lblCidadeID.style.color = (bEditingMode ? (bMailing ? 'green' : 'black') : 'black');
	lblBairro.style.color = (bEditingMode ? (bMailing ? 'blue' : 'black') : 'black');
	lblEndereco.style.color = (bEditingMode ? (bMailing ? 'blue' : 'black') : 'black');
	lblNumero.style.color = (bEditingMode ? (bMailing ? 'blue' : 'black') : 'black');
	lblComplemento.style.color = (bEditingMode ? (bMailing ? 'blue' : 'black') : 'black');

	lblMailing.style.color = (bEditingMode ? 'blue' : 'black');
	lblEmails.style.color = (bEditingMode ? 'blue' : 'black');
	lblPaisID.style.color = (bEditingMode ? 'blue' : 'black');
	lblTelefoneDDD.style.color = (bEditingMode ? 'blue' : 'black');
	lblTelefone.style.color = (bEditingMode ? 'blue' : 'black');
	lblEmail.style.color = (bEditingMode ? (bEmails ? 'blue' : 'black') : 'black');
}

function fillCmbClassificacao()
{
	lockInterface(true);
	var nTipoPessoaID = selTipoRegistroID.value;
    setConnection(dsoClassificacao);
    
    dsoClassificacao.SQL = 'SELECT ItemMasculino AS fldName, ItemID AS fldID ' +
		'FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 13 AND Filtro LIKE ' + '\'' + '%(' +
			nTipoPessoaID + ')%' + '\'' + ' ' +
		'ORDER BY fldName';

    dsoClassificacao.ondatasetcomplete = fillCmbClassificacao_DSC;
    dsoClassificacao.Refresh();
}

function fillCmbClassificacao_DSC() {
    clearComboEx(['selClassificacaoID']);
	
    while (! dsoClassificacao.recordset.EOF )
    {
        optionStr = dsoClassificacao.recordset['fldName'].value;
	    optionValue = dsoClassificacao.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selClassificacaoID.add(oOption);
        dsoClassificacao.recordset.MoveNext();
    }

	adjustLabelsCombos(false);
	lockInterface(false);
	selClassificacaoID.disabled = (selClassificacaoID.options.length == 0);
}

function fieldsOK()
{
	var bMailing = chkMailing.checked;
	var bEmails = chkEmails.checked;
	var sErrorMsg = '';

	if (bMailing)
	{
		if (selPaisID.value == '')
			sErrorMsg = 'Pa�s';
		if (trimStr(txtCEP.value) == '')
			sErrorMsg += (sErrorMsg != '' ? ',' : '') + 'CEP';
		if (selUFID.value == '')
			sErrorMsg += (sErrorMsg != '' ? ',' : '') + 'UF';
		if (selCidadeID.value == '')
			sErrorMsg += (sErrorMsg != '' ? ',' : '') + 'Cidade';
		if (trimStr(txtBairro.value) == '')
			sErrorMsg += (sErrorMsg != '' ? ',' : '') + 'Bairro';
		if (trimStr(txtEndereco.value) == '')
			sErrorMsg += (sErrorMsg != '' ? ',' : '') + 'Endere�o';
		if (trimStr(txtNumero.value) == '')
			sErrorMsg += (sErrorMsg != '' ? ',' : '') + 'N�mero';
	}

	if (bEmails)
	{
		if ((trimStr(txtEmail.value) == '') || (trimStr(txtEmail.value) == '_@_.com.br'))
			sErrorMsg += (sErrorMsg != '' ? ',' : '') + 'E-Mail';
	}
	
	if (sErrorMsg != '')
	{
		if ( window.top.overflyGen.Alert ('O(s) campo(s):\n' + sErrorMsg + '\ndeve(m) ser preenchido(s).') == 0 )
			return null;
		
		return false;
	}
	
	return true;
}

function setFieldsHint()
{
	if ((trimStr(dsoSup01.recordset['Site'].value) != '') &&
		(trimStr(dsoSup01.recordset['Site'].value) != 'http://www._.com.br'))
		txtSite.title = 'Clique 2 vezes para abrir o site.';
	else
		txtSite.title = '';
		
    if ((trimStr(dsoSup01.recordset['Email'].value) != '') &&
		(trimStr(dsoSup01.recordset['Email'].value) != '_@_.com.br'))
		txtEmail.title = 'Clique 2 vezes para enviar um e-mail.';
	else
		txtEmail.title = '';
}

function cadastroPessoa()
{
	lockInterface(true);

	var objFocus = null;
	var sFieldsRequired = '';
	
	if (trimStr(txtDocumento.value) == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'Documento';
		objFocus = txtDocumento;
	}
	
	if (trimStr(txtDDD.value) == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'DDD';
		objFocus = txtDDD;
	}
	
	if (trimStr(txtTelefone.value) == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'Telefone';
		objFocus = txtTelefone;
	}
	
	if (selPaisID.value == '')
	{
		sFieldsRequired = 'Pa�s';
		objFocus = selPaisID;
	}
	
	if (trimStr(txtCEP.value) == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'CEP';
		objFocus = txtCEP;
	}
	
	if (selUFID.value == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'UF';
		objFocus = selUFID;
	}
	
	if (selCidadeID.value == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'Cidade';
		objFocus = selCidadeID;
	}
	
	if (trimStr(txtBairro.value) == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'Bairro';
		objFocus = txtBairro;
	}
	
	if (trimStr(txtEndereco.value) == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'Endere�o';
		objFocus = txtEndereco;
	}
	
	if (trimStr(txtNumero.value) == '')
	{
		sFieldsRequired += (sFieldsRequired != '' ? ',' : '') + 'N�mero';
		objFocus = txtNumero;
	}
	
	if (sFieldsRequired != '')
	{
        var _retMsg = window.top.overflyGen.Confirm('O(s) campo(s):\n' + sFieldsRequired + '\ndeve(m) ser preenchido(s).\n\nDeseja preench�-los agora?');
		
		lockInterface(false);
        if ( _retMsg == 0 )
            return null;
        else if ( _retMsg == 1 )
        {
			lockInterface(false);
            __btn_ALT('SUP');
            
            window.focus();
            objFocus.focus();
        }
        else
            return true;

		return false;
	}
	
	var strPars = '?nDocumento=' + escape(txtDocumento.value);
    // pagina asp no servidor saveitemspedido.asp
    dsoPesquisaPessoa.URL = SYS_ASPURLROOT + '/basico/prospect/serverside/pesquisapessoa.aspx' + strPars;
    dsoPesquisaPessoa.ondatasetcomplete = dsoPesquisaPessoa_DSC;
    dsoPesquisaPessoa.refresh();
}

function dsoPesquisaPessoa_DSC() {
    var empresa = getCurrEmpresaData();

    if ( !(dsoPesquisaPessoa.recordset.BOF && dsoPesquisaPessoa.recordset.EOF) )
    {
		if (dsoPesquisaPessoa.recordset['PessoaID'].value != null)
		{
			lockInterface(false);
			var _retMsg = window.top.overflyGen.Confirm('Esta pessoa ja est� cadastrada.\nDeseja detalhar?');
	    
			if ( _retMsg == 0 )
				return null;
			else if ( _retMsg == 1 )
			{
				sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], dsoPesquisaPessoa.recordset['PessoaID'].value));
			}
			else
				return true;
				
			
		}
		else
			cadastroPessoa2ndPart();
    }
    else
		cadastroPessoa2ndPart();
}

function cadastroPessoa2ndPart()
{
    var empresa = getCurrEmpresaData();
    var _retMsg = window.top.overflyGen.Confirm('Deseja criar o cadastro desta pessoa?');

    if ( _retMsg != 1 )
    {
		lockInterface(false);
        return null;
    }

	var strPars = '?EmpresaID=' + escape(empresa[0]);
		strPars += '&RegistroID=' + escape(parseNULL(dsoSup01.recordset['ProspectID'].value));
		strPars += '&Nome=' + escape(parseNULL(dsoSup01.recordset['Nome'].value));
		strPars += '&Fantasia=' + escape(parseNULL(dsoSup01.recordset['Fantasia'].value));
		strPars += '&TipoPessoaID=' + escape(parseNULL(dsoSup01.recordset['TipoPessoaID'].value));
		strPars += '&ClassificacaoID=' + escape(parseNULL(dsoSup01.recordset['ClassificacaoID'].value));
		strPars += '&Documento1=' + escape(parseNULL(dsoSup01.recordset['Documento'].value));
		strPars += '&PaisID=' + escape(parseNULL(dsoSup01.recordset['PaisID'].value));
		strPars += '&CEP=' + escape(parseNULL(dsoSup01.recordset['CEP'].value));
		strPars += '&UFID=' + escape(parseNULL(dsoSup01.recordset['UFID'].value));
		strPars += '&CidadeID=' + escape(parseNULL(dsoSup01.recordset['CidadeID'].value));
		strPars += '&Bairro=' + escape(parseNULL(dsoSup01.recordset['Bairro'].value));
		strPars += '&Endereco=' + escape(parseNULL(dsoSup01.recordset['Endereco'].value));
		strPars += '&Numero=' + escape(parseNULL(dsoSup01.recordset['Numero'].value));
		strPars += '&Complemento=' + escape(parseNULL(dsoSup01.recordset['Complemento'].value));
		strPars += '&DDDTelefone1=' + escape(parseNULL(dsoSup01.recordset['TelefoneDDD'].value));
		strPars += '&NumeroTelefone1=' + escape(parseNULL(dsoSup01.recordset['TelefoneNumero'].value));
		strPars += '&DDDTelefone2=' + escape(parseNULL(dsoSup01.recordset['CelularDDD'].value));
		strPars += '&NumeroTelefone2=' + escape(parseNULL(dsoSup01.recordset['CelularNumero'].value));
		strPars += '&Sitio=' + escape(parseNULL(dsoSup01.recordset['Site'].value));
		strPars += '&Email=' + escape(parseNULL(dsoSup01.recordset['Email'].value));
		strPars += '&Origem=' + escape(1);
		strPars += '&UsuarioID=' + escape(parseNULL(getCurrUserID()));

    dsoCadastroPessoa.URL = SYS_ASPURLROOT + '/basico/prospect/serverside/cadastropessoa.aspx' + strPars;
    dsoCadastroPessoa.ondatasetcomplete = dsoCadastroPessoa_DSC;
    dsoCadastroPessoa.refresh();
}

function dsoCadastroPessoa_DSC() {
    lockInterface(false);
    
    if ( !(dsoCadastroPessoa.recordset.BOF && dsoCadastroPessoa.recordset.EOF) )
    {
		if ((dsoCadastroPessoa.recordset['fldErrorText'].value != null) &&
			(dsoCadastroPessoa.recordset['fldErrorText'].value != ''))
		{
			if ( window.top.overflyGen.Alert (dsoCadastroPessoa.recordset['fldErrorText'].value) == 0 )
				return null;
		}
		else
			__btn_REFR('sup');
    }
    else
		__btn_REFR('sup');
}

function parseNULL(pValue)
{
	if (pValue == null)
		return '';
	else
		return pValue;
}
