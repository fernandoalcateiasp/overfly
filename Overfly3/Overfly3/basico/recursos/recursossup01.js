/********************************************************************
recursossup01.js

Library javascript para o recursossup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// controla o retorno do servidor dos dados de combos dinamicos
// (selRecursoMaeID e selRecursoMaeID2)
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo framework
var glb_BtnFromFramWork;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

 
FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID ) 
    
FUNCOES ESPECIFICAS DO FORM:    
    selClassificacaoID5Changed()
    loadDataCmbsRecursoMae()
    loadDataCmbsRecursoMae_DSC()

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)    

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selMoedaID', '2'],
                          ['selMoedaAlternativaID', '2'],
                          ['selIdiomaID','3'],
                          ['selIdiomaInternacionalID','3'],
                          ['selClassificacaoID', '4'],
                          ['selClassificacaoID5', '5'],
                          ['selFocoID', '6'],
                          ['selFocoID2', '6'],
                          ['selFocoID4', '6'],
                          ['selRelevanciaID', '7'],
                          ['selRelevanciaID2', '7'],
                          ['selRelevanciaID4', '7'],
                          ['selTipoID', '8']]);
                          //,
                          //['selTipoPerfilComercialID', '9'],
                          //['selTipoPerfilComercialInadimplenciaID','9'],
                          //['selTipoPerfilComercialObsolescenciaID','9']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/basico/recursos/recursosinf01.asp',
                              SYS_PAGESURLROOT + '/basico/recursos/recursospesqlist.asp');                          

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01',  ['divSup02_01',
                                         'divSup02_02',
                                         'divSup02_03',
                                         'divSup02_04',
                                         'divSup02_05',
                                         'divSup02_06',
                                         'divSup02_07'],
                                        [1, 2, 3, 4, 5, 6, 7] );

    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RecursoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoRecursoID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01'],
                      [2,'divSup02_01'],
                      [2,'divSup02_02'],
                      [2,'divSup02_03'],
                      [2,'divSup02_04'],
                      [2,'divSup02_05'],
                      [2,'divSup02_06'],
                      [2,'divSup02_07']]);
    
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblRecurso','txtRecurso',39,1],
                          ['lblRecursoFantasia','txtRecursoFantasia',20,1],
                          ['lblTipoRegistroID','selTipoRegistroID',20,1]]);
                  
    //@@ *** Sistema - Div secundario superior divSup02_01 ***/
    adjustElementsInForm([['lblMoedaID','selMoedaID',10,2],
                          ['lblMoedaAlternativaID','selMoedaAlternativaID',10,2],
                          ['lblIdiomaID','selIdiomaID',12,2,-15],
                          ['lblIdiomaInternacionalID','selIdiomaInternacionalID',12,2],
                          ['lblLocalidadeID','selLocalidadeID',32,2,-20],
                          ['btnFindLocalidade','btn',24,2],
                          ['lblDiasAlerta', 'txtDiasAlerta', 3, 3],
                          ['lblDiasValidadeCredito', 'txtDiasValidadeCredito', 4, 3, -5],
                          ['lblNumeroEmails', 'txtNumeroEmails', 5, 3, -5]],
                          ['lblHrSistema','hrSistema']);

    //@@ *** Componente - Div secundario superior divSup02_02 ***/
    adjustElementsInForm([['lblClassificacaoID','selClassificacaoID',18,2],
                          ['lblRecursoMaeID','selRecursoMaeID',20,2],
                          ['lblFocoID','selFocoID',7,2],
                          ['lblRelevanciaID','selRelevanciaID',15,2],
                          ['lblOrdemForm','txtOrdemForm',3,2],
                          ['lblTabela','txtTabela',25,2],
                          ['lblFormName','txtFormName',24,3],
                          ['lblFrameStart','txtFrameStart',24,3],
                          ['lblNomeBotao','txtNomeBotao',13,3],
                          ['lblArqStart','txtArqStart',17,3],
                          ['lblTipoID','selTipoID',14,3],
                          ['lblFiltro1','txtFiltro1',96,4]],['lblHrComponente','hrComponente']);

    //@@ *** Contexto - Div secundario superior divSup02_03 ***/
    adjustElementsInForm([['lblRecursoMaeID2','selRecursoMaeID2',20,2],
                          ['lblFocoID2','selFocoID2',7,2],
                          ['lblRelevanciaID2','selRelevanciaID2',15,2],
                          ['lblOrdemContexto','txtOrdemContexto',3,2],
                          ['lblEhDefaultContexto','chkEhDefaultContexto',3,2],
                          ['lblFiltroContexto','txtFiltroContexto',96,3]],['lblHrContexto','hrContexto']);

    //@@ *** Sub-Form - Div secundario superior divSup02_04 ***/
    adjustElementsInForm([['lblFocoID4','selFocoID4',7,2],
                          ['lblRelevanciaID4','selRelevanciaID4',15,2],
                          ['lblPrincipal4','chkPrincipal4',3,2]],['lblHrSubForm','HrSubForm']);

    //@@ *** Feature - Div secundario superior divSup02_05 ***/
    adjustElementsInForm([['lblClassificacaoID5','selClassificacaoID5',18,2],
                          ['lblEhDetalhe', 'chkEhDetalhe', 3, 2],
                          ['lblFiltroFeature','txtFiltroFeature',96,3]],['lblHrFeature','hrFeature']);

    //@@ *** Perfil - Div secundario superior divSup02_06 ***/
    adjustElementsInForm([['lblEhCargo','chkEhCargo',3,2],
						  ['lblEhCargoSuperior','chkEhCargoSuperior',3,2],
						  ['lblPublica','chkPublica',3,2],
						  ['lblQualidade','chkQualidade',3,2],
						 
//						  ['lblTipoPerfilComercialID','selTipoPerfilComercialID',21,2],
//						  ['lblTipoPerfilComercialInadimplenciaID','selTipoPerfilComercialInadimplenciaID',21,2],
//						  ['lblTipoPerfilComercialObsolescenciaID', 'selTipoPerfilComercialObsolescenciaID', 21, 2],
						  
						  
						  ['lblRecebimentoArmazenar','chkRecebimentoArmazenar',3,3],
						  ['lblRecebimentoConserto','chkRecebimentoConserto',3,3],
						  ['lblRecebimentoDevolver','chkRecebimentoDevolver',3,3]],
                          ['lblHrPerfilUsuario','hrPerfilUsuario']);

    //@@ *** Estado - Div secundario superior divSup02_07 ***/
    adjustElementsInForm([['lblRecursoAbreviado','txtRecursoAbreviado',2,2]],
                          ['lblHrEstado','hrEstado']);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWRECURSO')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        window.top.focus();
        
        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    txtFormName.disabled = true;
	txtFrameStart.disabled = true;
	txtArqStart.disabled = true;
	txtFiltro1.disabled = true;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selRecursoMaeID e selRecursoMaeID2)
    glb_BtnFromFramWork = btnClicked;

    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') )
    {
        if ( (dsoSup01.recordset[glb_sFldTipoRegistroName].value == 1) ||
             (dsoSup01.recordset[glb_sFldTipoRegistroName].value == 2) ||
             (dsoSup01.recordset[glb_sFldTipoRegistroName].value == 3) )
             startDynamicCmbs();
        else
             // volta para a automacao    
	        finalOfSupCascade(glb_BtnFromFramWork);
    }             
    else    
        // volta para a automacao    
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    showCompForm(false);

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

	var sTitle = 'N�mero de emails marketing.'; 

	if (dsoSup01.recordset['TotalEmails'].value != null)
		sTitle += '\nN�mero de emails pendentes: ' + dsoSup01.recordset['TotalEmails'].value;

	lblNumeroEmails.title = sTitle;
	txtNumeroEmails.title = sTitle;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario
    // opera os campos de classificacao = Form
    
    if ( ((cmbID == 'selClassificacaoID') && (cmb.value == 11)) || (cmbID == 'selClassificacaoID5') )
        showCompForm(true);

    if ( (cmbID == 'selTipoRegistroID') && ( (cmb.value == 3)||(cmb.value == 2) ) )
        fillCmbRecursoMaeID2(cmb);
    else if (cmbID == 'selClassificacaoID')
        fillCmbRecursoMaeID(cmb);
    else
    {
        // Troca a interface do sup em funcao de item selecionado
        // no combo de tipo.
        // Se for necessario ao programador, repetir o if abaixo
        // definido, antes deste comentario.
        // Nao mexer - Inicio de automacao ==============================
        if ( cmbID == 'selTipoRegistroID' )
            adjustSupInterface();
                
        // Final de Nao mexer - Inicio de automacao =====================
    }        
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
    
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.

Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    if (btnClicked.id == 'btnFindLocalidade')
        openModalLocalidade();
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
        
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if ( idElement.toUpperCase() == 'MODALLOCALIDADEHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // param2 = array
            // param2[0] = texto digitado
            // param2[1] = id do combo a preencher
            // param2[2] = id do form
            
			var oldDataSrc = selLocalidadeID.dataSrc;
			var oldDataFld = selLocalidadeID.dataFld;
			selLocalidadeID.dataSrc = '';
			selLocalidadeID.dataFld = '';

            clearComboEx(['selLocalidadeID']);
            var oOption = document.createElement("OPTION");
            oOption.text = param2[0];
            oOption.value = param2[1];
            selLocalidadeID.add(oOption);
            
			selLocalidadeID.dataSrc = oldDataSrc;
			selLocalidadeID.dataFld = oldDataFld;

            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            
            selLocalidadeID.disabled = (selLocalidadeID.options.length == 0);

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');

            // nao mexer
            return 0;
        }
    }    
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
	selLocalidadeID.disabled = (selLocalidadeID.options.length == 0);

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    especBtnIsPrintBtn('sup', 0);
}

/********************************************************************
Funcao do programador
Disparada pela troca do combo selClassificacaoID e selTipoRegisttroID
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function showCompForm(lCmbOnchange)
{
    var nTipoRegistroID;
    var nClassificacaoID;

    if (lCmbOnchange)
    {
        nTipoRegistroID = selTipoRegistroID.value;
        nClassificacaoID = selClassificacaoID5.value;
    }
    else
    {
        nTipoRegistroID = dsoSup01.recordset['TipoRecursoID'].value;
        nClassificacaoID = dsoSup01.recordset['ClassificacaoID'].value;
    }
    
    if ( (nTipoRegistroID == 2) && (nClassificacaoID == 11) )
    {
        lblOrdemForm.style.visibility = 'visible';
        txtOrdemForm.style.visibility = 'visible';
        lblTabela.style.visibility = 'visible';
        txtTabela.style.visibility = 'visible';
        lblFormName.style.visibility = 'visible';
        txtFormName.style.visibility = 'visible';
		lblFrameStart.style.visibility = 'visible';
		txtFrameStart.style.visibility = 'visible';
		lblArqStart.style.visibility = 'visible';
		txtArqStart.style.visibility = 'visible';
		lblNomeBotao.style.visibility = 'visible';
		txtNomeBotao.style.visibility = 'visible';
		lblTipoID.style.visibility = 'visible';
		selTipoID.style.visibility = 'visible';
		lblFiltro1.style.visibility = 'visible';
		txtFiltro1.style.visibility = 'visible';
    }
    else
    {
        lblOrdemForm.style.visibility = 'hidden';
        txtOrdemForm.style.visibility = 'hidden';
        lblTabela.style.visibility = 'hidden';
        txtTabela.style.visibility = 'hidden';
        lblFormName.style.visibility = 'hidden';
        txtFormName.style.visibility = 'hidden';
        lblFrameStart.style.visibility = 'hidden';
        txtFrameStart.style.visibility = 'hidden';
        lblArqStart.style.visibility = 'hidden';
        txtArqStart.style.visibility = 'hidden';
        lblNomeBotao.style.visibility = 'hidden';
        txtNomeBotao.style.visibility = 'hidden';
		lblTipoID.style.visibility = 'hidden';
		selTipoID.style.visibility = 'hidden';
        lblFiltro1.style.visibility = 'hidden';
        txtFiltro1.style.visibility = 'hidden';
    }
    
    if ( nClassificacaoID == 12 )
    {
        lblFiltroFeature.style.visibility = 'inherit';
        txtFiltroFeature.style.visibility = 'inherit';
        lblEhDetalhe.style.visibility = 'hidden';
        chkEhDetalhe.style.visibility = 'hidden';
    }
    else if ( nClassificacaoID == 14 )
    {
        lblFiltroFeature.style.visibility = 'hidden';
        txtFiltroFeature.style.visibility = 'hidden';
        lblEhDetalhe.style.visibility = 'inherit';
        chkEhDetalhe.style.visibility = 'inherit';
    }
    else
    {
        lblFiltroFeature.style.visibility = 'hidden';
        txtFiltroFeature.style.visibility = 'hidden';
        lblEhDetalhe.style.visibility = 'hidden';
        chkEhDetalhe.style.visibility = 'hidden';
    }
}
/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selRecursoMaeID e selRecursoMaeID2)
    glb_CounterCmbsDynamics = 1;

    var nTipoRecursoID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
    var nClassificacaoID = dsoSup01.recordset['ClassificacaoID'].value;

        
    setConnection(dsoCmbDynamic01);

	if(nTipoRecursoID == 1)
    {
		var nCurrLocalidadeID = dsoSup01.recordset['LocalidadeID'].value;
		if ( (nCurrLocalidadeID == null) || (nCurrLocalidadeID == '') )
			nCurrLocalidadeID == '0';
			
        dsoCmbDynamic01.SQL = 'SELECT LocalidadeID AS fldID, Localidade AS fldName ' +
                              'FROM Localidades  WITH (NOLOCK) ' +
                              'WHERE LocalidadeID= ' + nCurrLocalidadeID;
    }
    else if( (nTipoRecursoID == 2)&&(nClassificacaoID == 9) )  
    {
        dsoCmbDynamic01.SQL = 'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                              'FROM Recursos  WITH (NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoRecursoID=1 ' +
                              'ORDER BY fldName';
    }
    else if( (nTipoRecursoID == 2)&&(nClassificacaoID == 10) )  
    {
        dsoCmbDynamic01.SQL = 'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                              'FROM Recursos  WITH (NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoRecursoID=2 AND ClassificacaoID=9 ' +
                              'ORDER BY fldName';
    }
    else if( (nTipoRecursoID == 2)&&(nClassificacaoID == 11) )  
    {
        dsoCmbDynamic01.SQL = 'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                              'FROM Recursos  WITH (NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoRecursoID=2 AND ClassificacaoID=10 ' +
                              'ORDER BY fldName';
    }
    else if( nTipoRecursoID == 3 )  
    {
        dsoCmbDynamic01.SQL = 'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                              'FROM Recursos  WITH (NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoRecursoID=2 AND ClassificacaoID=11 ' +
                              'ORDER BY fldName';
    }

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic01_DSC;
    dsoCmbDynamic01.Refresh();
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic01_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics;
    var aDSOsDunamics = [dsoCmbDynamic01];
    var oOption;
    var nTipoRecursoID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
    var i;
    
    if (nTipoRecursoID == 1)
		aCmbsDynamics = [selLocalidadeID];
    else if (nTipoRecursoID == 2)
        aCmbsDynamics = [selRecursoMaeID];
    else
        aCmbsDynamics = [selRecursoMaeID2];

    // Inicia o carregamento de combo dinamico
    clearComboEx(['selRecursoMaeID','selRecursoMaeID2','selLocalidadeID']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i<1; i++)
        {
            while (! aDSOsDunamics[i].recordset.EOF )
            {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.moveNext();
            }
        }

        // volta para a automacao
	    finalOfSupCascade(glb_BtnFromFramWork);
	}
	return null;
}

/********************************************************************
Funcao do programador
Disparada pela funcao optChangedInCmb() quando o combo de tipo de 
Classificacao e alterado, ela preenche o combo de RecursoMaeID de acordo com 
o tipo do registro.
           
Parametros: 
cmb     -> Objeto (combo)

Retorno:
nenhum
********************************************************************/
function fillCmbRecursoMaeID(cmb)
{
    var nClassificaoID;
    
    nClassificacaoID = cmb.value;

    clearComboEx(['selRecursoMaeID','selRecursoMaeID2']);

    lockInterface(true);
    // parametrizacao do dso dsoCmbDynamic01 (designado para selRecursoMaeID)
    setConnection(dsoCmbDynamic01);
            
    if (nClassificacaoID == 9)
    {
        dsoCmbDynamic01.SQL = 'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                              'FROM Recursos  WITH (NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoRecursoID=1 ' +
                              'ORDER BY fldName';
    }
    else if (nClassificacaoID == 10)
    {
        dsoCmbDynamic01.SQL = 'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                              'FROM Recursos  WITH (NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoRecursoID=2 AND ClassificacaoID=9 ' +
                              'ORDER BY fldName';
    }
    else if (nClassificacaoID == 11)
    {
        dsoCmbDynamic01.SQL = 'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                              'FROM Recursos  WITH (NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoRecursoID=2 AND ClassificacaoID=10 ' +
                              'ORDER BY fldName';
    }

    dsoCmbDynamic01.ondatasetcomplete = fillCmbRecursoMaeID_DSC;
    dsoCmbDynamic01.refresh();

}
/********************************************************************
Funcao do programador
Funcao de datasetcomplete disparada pela funcao fillCmbRecursoMaeID
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function fillCmbRecursoMaeID_DSC() {
    var optionStr, optionValue;

    while (! dsoCmbDynamic01.recordset.EOF )
    {
        optionStr = dsoCmbDynamic01.recordset['fldName'].value;
		optionValue = dsoCmbDynamic01.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selRecursoMaeID.add(oOption);
        dsoCmbDynamic01.recordset.moveNext();
    }

    lockInterface(false);

    if ( selRecursoMaeID.options.length>0)
        selRecursoMaeID.disabled = false;
    else        
        selRecursoMaeID.disabled = true;

    // Volta para automacao 
     adjustSupInterface();
}

/********************************************************************
Funcao do programador
Disparada pela funcao optChangedInCmb() quando o combo de tipo de 
registro e alterado, ela preenche o combo de RecursoMaeID de acordo com 
o tipo do registro.
           
Parametros: 
cmb     -> Objeto (combo)

Retorno:
nenhum
********************************************************************/
function fillCmbRecursoMaeID2(cmb)
{
    var nTipoRecursoID;

    nTipoRecursoID = cmb.value;

    // Para Modulo/SubModulo/Form limpa o combo de RecursoMae e volta
    // para a automacao
    if ( nTipoRecursoID == 2)
    {
        clearComboEx(['selRecursoMaeID','selRecursoMaeID2']);
        adjustSupInterface();
    }
    // Para Contexto, preenche o combo de RecursoMae
    else
    {
        clearComboEx(['selRecursoMaeID','selRecursoMaeID2']);

        lockInterface(true);
        // parametrizacao do dso dsoCmbDynamic01 (designado para selRecursoMaeID2)
        setConnection(dsoCmbDynamic01);
            
        dsoCmbDynamic01.SQL = 'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                              'FROM Recursos  WITH (NOLOCK) ' +
                              'WHERE EstadoID=2 AND TipoRecursoID=2 AND ClassificacaoID=11 ' +
                              'ORDER BY fldName';

        dsoCmbDynamic01.ondatasetcomplete = fillCmbRecursoMaeID2_DSC;
        dsoCmbDynamic01.refresh();
    }

}

/********************************************************************
Funcao do programador
Funcao de datasetcomplete disparada pela funcao fillCmbRecursoMaeID2
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function fillCmbRecursoMaeID2_DSC() {
    var optionStr, optionValue;

    while (! dsoCmbDynamic01.recordset.EOF )
    {
        optionStr = dsoCmbDynamic01.recordset['fldName'].value;
		optionValue = dsoCmbDynamic01.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selRecursoMaeID2.add(oOption);
        dsoCmbDynamic01.recordset.moveNext();
    }

    lockInterface(false);

    if ( selRecursoMaeID2.options.length>0)
        selRecursoMaeID2.disabled = false;
    else        
        selRecursoMaeID2.disabled = true;

    // Volta para automacao 
     adjustSupInterface();
}

function openModalLocalidade()
{
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/recursos/modalpages/modallocalidade.asp';
    showModalWin(htmlPath, new Array(410,334));
}
