/********************************************************************
modalManutencaoSalarial.js

Library javascript para o modalManutencaoSalarial.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
// Dados da empresa
var glb_Empresa = getCurrEmpresaData();
// Indica se todos os checkbox est�o marcados.
var glb_TodosMarcados = false;
// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
// Gravacao de dados do grid .RDS
var dsoGrava = new CDatatransport("dsoGrava");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
//setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
listar()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalManutencaoSalarialBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalManutencaoSalarial_DblClick(grid, Row, Col)
js_fg_modalManutencaoSalarialKeyPress(KeyAscii)
js_modalManutencaoSalarial_ValidateEdit()
js_fg_modalManutencaoSalarial_BeforeEdit(grid, row, col)
js_modalManutencaoSalarial_AfterEdit(Row, Col)
js_fg_AfterRowColModalManutencaoSalarial (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    //setupPage();   

    // ajusta o body do html
    with (modalManutencaoSalarialBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
	}

    // configuracao inicial do html
	setupPage();

	showExtFrame(window, true);
    
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;

    secText("Manuten��o Salarial", 1);

    // desabilita o botao OK
//    btnOK.disabled = false;
	btnFillGrid.disabled = false;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';

    // Aqui ajusta os controles do divControls
    adjustElementsInForm([['lblModo', 'selModo', 15, 1, 0, -10, -19],
						  ['lblVigencia', 'txtVigencia', 10, 1, 0],
						  ['lblInicio', 'txtInicio', 10, 1, 3],
						  ['lblFim', 'txtFim', 10, 1, 3],
						  ['lblCargo', 'selCargo', 20, 1, 3],
						  ['lblNivel', 'selNivel', 5, 1, 3],
						  ['lblAjusteSalario', 'txtAjusteSalario', 7, 1, 3, 0, -50]], null, null, true);

    lblModo.style.left = 0;
    selModo.style.left = 0;

	// Seta o campo de ajuste.
    txtAjusteSalario.onkeypress = verifyNumericEnterNotLinked;
    txtAjusteSalario.setAttribute('verifyNumPaste', 1);
    txtAjusteSalario.setAttribute('thePrecision', 6, 1);
    txtAjusteSalario.setAttribute('theScale', 2, 1);
    txtAjusteSalario.setAttribute('minMax', new Array(-999, 999), 1);
    txtAjusteSalario.onfocus = selFieldContent;
    txtAjusteSalario.maxLength = 7;

    // Ajusta o divControls
    with (divControls.style) {
    	border = 'none';
    	backgroundColor = 'transparent';
    	left = ELEM_GAP;
    	top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
    	width = (30 * FONT_WIDTH) + 24 + 20 + 42;
    	temp = parseInt(width);
    	height = 40;
    }

    // ajusta o divFG
    with (divFG.style) {
    	border = 'none';
    	backgroundColor = 'transparent';
    	left = 12;
    	top = 67 + ELEM_GAP;
    	width = 813;
    	height = 422;
    }

    with (fg.style) {
    	left = 0;
    	top = 0;
    	width = parseInt(divFG.style.width, 10);
    	height = parseInt(divFG.style.height, 10);
    }

    // Reposiciona botao OK
    with (btnFillGrid) {
    	style.top = divControls.offsetTop + txtAjusteSalario.offsetTop - 1;
    	style.width = 54;
    	style.left = txtAjusteSalario.offsetLeft - 20;
    }

    with (btnOK)
    {
    	style.width = btnFillGrid.currentStyle.width;
    	style.height = btnFillGrid.currentStyle.height;
    	style.top = btnFillGrid.currentStyle.top;
    	style.left = parseInt(btnFillGrid.currentStyle.left, 10) + parseInt(btnFillGrid.currentStyle.width, 10) + ELEM_GAP;
    }

    with (btnExcel)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(btnOK.currentStyle.left, 10) + parseInt(btnOK.currentStyle.width, 10) + ELEM_GAP;
    }

    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    // Desenha o resto da interface e preenche os combos.
    cargo_onchange();
    selModoOnChange();
	
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
    fg.Editable = false;

    txtInicio.disabled = false;
}

/********************************************************************
Preenche o n�vel em funcao do cargo.
********************************************************************/
function cargo_onchange() {
	var nivelMaximo;
	var i;

	for (i = 0; i <= selCargo.options.length; i++) {
		if (selCargo.options[i].selected) {
			nivelMaximo = selCargo.options.item(i).getAttribute('NivelMaximo', 0);

			break;
		}
	}

	clearComboEx(['selNivel']);

	var oOption = document.createElement("OPTION");

	oOption.text = "";
	oOption.value = "0";

	selNivel.add(oOption);

	for (i = 1; i <= nivelMaximo;  i++) {
		var oOption = document.createElement("OPTION");
		
		oOption.text = i;
		oOption.value = i;

		selNivel.add(oOption);
	}
}

/********************************************************************
 O campo Modo mudou.
********************************************************************/
function selModoOnChange() {
	fg.Rows = 1;

	if (selModo.value == '1') {
		consultaSelected();
	} else if (selModo.value == '2') {
		manutencaoSelected();
	}

	habilitaGravar();
}

/********************************************************************
 Manuten��o selecionada.
********************************************************************/
function manutencaoSelected() {
	// Mostra a vig�ncia e oculta o In�cio e Fim.
	lblInicio.style.visibility = 'hidden';
	txtInicio.style.visibility = 'hidden';
	txtInicio.value = "";
	lblFim.style.visibility = 'hidden';
	txtFim.style.visibility = 'hidden';
	txtFim.value = "";

	lblVigencia.style.visibility = 'inherit';
	txtVigencia.style.visibility = 'inherit';
	txtVigencia.value = "";

	lblAjusteSalario.style.visibility = 'inherit';
	txtAjusteSalario.style.visibility = 'inherit';
	txtAjusteSalario.value = "";

	// Ajusta a posi��o dos campos vis�veis.
	txtVigencia.style.left = parseInt(selModo.style.left) + parseInt(selModo.style.width) + ELEM_GAP;
	lblVigencia.style.left = txtVigencia.style.left;

	selCargo.style.left = parseInt(txtVigencia.style.left) + parseInt(txtVigencia.style.width) + ELEM_GAP;
	lblCargo.style.left = selCargo.style.left;

	selNivel.style.left = parseInt(selCargo.style.left) + parseInt(selCargo.style.width) + ELEM_GAP;
	lblNivel.style.left = selNivel.style.left;

	txtAjusteSalario.style.left = parseInt(selNivel.style.left) + parseInt(selNivel.style.width) + ELEM_GAP;
	lblAjusteSalario.style.left = txtAjusteSalario.style.left;
}

/********************************************************************
Consulta selecionada.
********************************************************************/
function consultaSelected() {
	// Oculta a vig�ncia e mostra o In�cio e Fim.
	lblInicio.style.visibility = 'inherit';
	txtInicio.style.visibility = 'inherit';
	txtInicio.value = "";
	lblFim.style.visibility = 'inherit';
	txtFim.style.visibility = 'inherit';
	txtFim.value = "";

	lblVigencia.style.visibility = 'hidden';
	txtVigencia.style.visibility = 'hidden';
	txtVigencia.value = "";

	lblAjusteSalario.style.visibility = 'hidden';
	txtAjusteSalario.style.visibility = 'hidden';
	txtAjusteSalario.value = "";

	// Ajusta a posi��o dos campos vis�veis.
	txtInicio.style.left = parseInt(selModo.style.left) + parseInt(selModo.style.width) + ELEM_GAP;
	lblInicio.style.left = txtInicio.style.left;

	txtFim.style.left = parseInt(txtInicio.style.left) + parseInt(txtInicio.style.width) + ELEM_GAP;
	lblFim.style.left = txtFim.style.left;

	selCargo.style.left = parseInt(txtFim.style.left) + parseInt(txtFim.style.width) + ELEM_GAP;
	lblCargo.style.left = selCargo.style.left;

	selNivel.style.left = parseInt(selCargo.style.left) + parseInt(selCargo.style.width) + ELEM_GAP;
	lblNivel.style.left = selNivel.style.left;
}

/********************************************************************
Enter no campo data e filtro
********************************************************************/
function txtField_onKeyPress()
{
    if ( event.keyCode == 13 )
	{
        fillGridData('btnFillGrid');
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	// ctl.id retorna o id do botao clicado (OK ou Cancelar)
	var controlID = ctl.id;

	// esta funcao fecha a janela modal e destrava a interface
	if (controlID == 'btnOK') {
		saveDataInGrid();
	}
	else if (controlID == 'btnFillGrid' || controlID == 'btnExcel') {
		//btnOK.disabled = true;		
	    fillGridData(controlID);
	}
	else {
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);
	}
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar interface aqui
    //setupBtnsFromGridState();

	showExtFrame(window, true);

	selModo.focus();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(controlID) {
    var Campos = "";
    var empresaData = getCurrEmpresaData();
    var nLinguaLogada = getDicCurrLang();

	if (selModo.value == 2) {
		if (trimStr(txtVigencia.value) != "" && !chkDataEx(txtVigencia.value)) {
			window.top.overflyGen.Alert("A data da vig�ncia n�o � v�lida.\nO formato correto deve ser " + DATE_FORMAT + ".");
			return null;
		}
	}
	else if (selModo.value == 1) {
		if(trimStr(txtInicio.value) != "" && !chkDataEx(txtInicio.value)) {
			window.top.overflyGen.Alert("A data de in�cio n�o � v�lida.\nO formato correto deve ser " + DATE_FORMAT + ".");
			return null;
		}

		if (trimStr(txtFim.value) != "" && !chkDataEx(txtFim.value)) {
			window.top.overflyGen.Alert("A data de fim n�o � v�lida.\nO formato correto deve ser " + DATE_FORMAT + ".");
			return null;
		}
	}

	lockControlsInModalWin(true);
   
    // Se no campo ajuste de sal�rio n�o possuir n�mero ou  
    // for zero, deixa o campo vazio.
	try {
		if (parseFloat(txtAjusteSalario.value) == 0.0)
			txtAjusteSalario.value = "";
	} catch(e) {
		txtAjusteSalario.value = "";
	}
    
    // parametrizacao do dso dsoGrid
	var strPars = '?selModo=' + selModo.value;
	strPars += '&empresaID=' + empresaData[0];;
	strPars += '&txtVigencia=' + txtVigencia.value;
	strPars += '&txtVigencia2=' + normalizeDate_DateTime(txtVigencia.value, true);
	strPars += '&txtAjusteSalario=' + txtAjusteSalario.value;
	strPars += '&txtAjusteSalario2=' + parseFloat(txtAjusteSalario.value);
	strPars += '&txtInicio=' + txtInicio.value;
	strPars += '&txtInicio2=' + normalizeDate_DateTime(txtInicio.value, true);
	strPars += '&txtFim=' + txtFim.value;
	strPars += '&txtFim2=' + normalizeDate_DateTime(txtFim.value, true);
	strPars += '&selCargo=' + selCargo.value;
	strPars += '&selNivel=' + selNivel.value;
	strPars += '&controlID=' + controlID;
	strPars += '&sEmpresaFantasia=' + empresaData[3];
	strPars += '&nLinguaLogada=' + nLinguaLogada;
	strPars += '&controle=' + 'listaExcel';

	if (controlID == 'btnExcel') {
	    lockControlsInModalWin(true);
	    var frameReport = document.getElementById("frmReport");
	    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/basico/recursos/serverside/ReportsGrid_manutencaosalarial.aspx' + strPars;
	    lockControlsInModalWin(false);
	}
	else {
	    // zera o grid
	    fg.Rows = 1;

	    setConnection(dsoGrid);

	    dsoGrid.URL = SYS_ASPURLROOT + '/basico/recursos/serverside/ReportsGrid_manutencaosalarial.aspx' + strPars;

	    dsoGrid.ondatasetcomplete = fillGridData_DSC;
	    dsoGrid.Refresh();

	    // Seta o DSO para grava��o.
	    setConnection(dsoGrava);
	    dsoGrava.SQL = "SELECT " +
                "c.RecHistoricoID, c.EmpresaID, c.RecursoID, c.dtVigencia, c.Salario, c.Observacao " +
            "FROM Recursos_HistoricosSalarios c WITH (NOLOCK) " +
            "WHERE " +
                ((txtInicio.value != "") ?
                    ("c.dtVigencia >= '" + normalizeDate_DateTime(txtInicio.value, true) + "' AND ") : ("")) +
                ((txtFim.value != "") ?
                    ("c.dtVigencia <= '" + normalizeDate_DateTime(txtFim.value, true) + "' AND ") : ("")) +
                ((selCargo.value != "" && selCargo.value != 0) ? ("c.RecursoID = " + selCargo.value + " AND ") : ("")) +
                ((selNivel.value != "" && selNivel.value != 0) ? ("c.Nivel = " + selNivel.value + " AND ") : ("")) +
                "c.EmpresaID = " + glb_Empresa[0];

	    // Se for manuten��o mostra apenas o �ltimo item do hist�rico.
	    if (selModo.value == '2') {
	        dsoGrava.SQL += " AND " +
                "c.RecHistoricoID =  " +
                "( " +
                    "SELECT TOP 1 RecHistoricoID  " +
                    "FROM Recursos_HistoricosSalarios z  WITH (NOLOCK) " +
                    "WHERE z.EmpresaID = c.EmpresaID AND  " +
                        "z.RecursoID = c.RecursoID AND " +
                        "z.Nivel = c.Nivel " +
                    "ORDER BY z.dtVigencia DESC " +
                ") ";
	    }

	    dsoGrava.ondatasetcomplete = function () { };
	    dsoGrava.refresh();
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	var dTFormat = '';

	var alinharDireita = [2,3];
	var idxAlinhar = 2;
	var holdCols = [];
	var idxHoldCols = 0;
	var header = ["ID", "Cargo", "Vig�ncia", "Sal�rio"];
	var idxHeader = 4;
	var colunm = ["CargoID*", "Cargo*", "dtVigencia*", "Salario*"];
	var idxColunm = 4;
	var mascara = ["", "", "99/99/9999", "#999.99"];
	var idxMascara = 4;
	var formato = ["", "", dTFormat, "###,###,##0.00"];
	var idxFormato = 4;
	
	if (DATE_FORMAT == "DD/MM/YYYY")
		dTFormat = 'dd/mm/yyyy';
	else if (DATE_FORMAT == "MM/DD/YYYY")
		dTFormat = 'mm/dd/yyyy';

	startGridInterface(fg);

	glb_GridIsBuilding = true;    

	fg.Redraw = 0;
	
	if (selModo.value == '2') {
		// Inclui as colunas extras.
		header[idxHeader++] = "Vig�ncia";
		header[idxHeader++] = "Sal�rio";

		colunm[idxColunm++] = "dtVigencia2";
		alinharDireita[idxAlinhar++] = idxColunm;
		colunm[idxColunm++] = "Salario2";

		mascara[idxMascara++] = "99/99/9999";
		mascara[idxMascara++] = "#999.99";

		formato[idxFormato++] = dTFormat;
		formato[idxFormato++] = "###,###,##0.00";
	}
	
	// Inclui as demais colunas.
	header[idxHeader++] = "Observa��o";
	header[idxHeader++] = "RecHistoricoID";

	colunm[idxColunm++] = "Observacao" + ((selModo.value == '1') ? "*" : "");
	holdCols[idxHoldCols++] = idxColunm;
	colunm[idxColunm++] = "RecHistoricoID";
	
	mascara[idxMascara++] = "";
	mascara[idxMascara++] = "";

	formato[idxFormato++] = "";
	formato[idxFormato++] = "";

	if (selModo.value == '1') {
		// Configura a edi��o do grid
		fg.Editable = false;
	} else if (selModo.value == '2') {
		// Configura a edi��o do grid
		fg.Editable = true;

		// Inclui as colunas extras.
		header[idxHeader++] = "OK";
		colunm[idxColunm++] = "OK";
		mascara[idxMascara++] = "";
		formato[idxFormato++] = "";
	}

	headerGrid(fg, header, holdCols);

	fillGridMask(fg,dsoGrid,colunm,mascara,formato);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);

    paintCellsSpecialyReadOnly();

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;

    fg.Redraw = 2;

    alignColsInGrid(fg, alinharDireita);

    if (selModo.value == '2') {
    	fg.ColWidth(getColIndexByColKey(fg, "dtVigencia2")) = 71 * 18;
    	fg.ColWidth(getColIndexByColKey(fg, "Salario2")) = 60 * 18;
    }

    if (fg.Rows > 1) {
    	fg.Row = 1;
    	fg.TopRow = 1;
    }

    lockControlsInModalWin(false);
    
    glb_GridIsBuilding = false;

    habilitaGravar();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
	lockControlsInModalWin(true);

	var i;
	var idxCargoID = getColIndexByColKey(fg, "CargoID*");
	var idxCargo = getColIndexByColKey(fg, "Cargo*");
	var idxDtVigencia = getColIndexByColKey(fg, "dtVigencia*");
	var idxDtVigencia2 = getColIndexByColKey(fg, "dtVigencia2");
	var idxSalario2 = getColIndexByColKey(fg, "Salario2");
	var idxObservacao = getColIndexByColKey(fg, "Observacao");
	var idxOK = getColIndexByColKey(fg, "OK");

	var dataDB;
	var dataUsr;
	var erroDataViGencia = "";
	
	// Atualiza o DSO de grava��o com os dados das colunas Vig�ncia e Sal�rio
	for (i = 1; i < fg.Rows; i++) {
		// Se n�o for pra gravar passa pra pr�xima.
		if (fg.ValueMatrix(i, idxOK) == 0)
			continue;

		// Atualiza a data de vig�ncia.
		if (fg.TextMatrix(i, idxDtVigencia2) != "") {
			dataDB = strToDate(fg.TextMatrix(i, idxDtVigencia));
			dataUsr = strToDate(fg.TextMatrix(i, idxDtVigencia2));

			// Se a vig�ncia digitada for maior que a atual, atualiza.
			// Caso contr�rio, acumula mais uma mensagem de erro.
			if (dataUsr > dataDB)
				setDsoData(i, idxDtVigencia2);
			else
				erroDataViGencia += fg.TextMatrix(i, idxCargoID) + " - " + fg.TextMatrix(i, idxCargo) + ": Nova vig�ncia n�o � maior que a anterior.\n" ;
		}
		
		// Atualiza o sal�rio.
		if (fg.TextMatrix(i, idxSalario2) != "")
			setDsoData(i, idxSalario2);
		// Atualiza a observa��o.
		setDsoData(i, idxObservacao);
	}

	if (erroDataViGencia == "") {
		dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
		dsoGrava.SubmitChanges();
	} else {
		window.top.overflyGen.Alert(erroDataViGencia);
		lockControlsInModalWin(false);
	}
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
	fg.Rows = 1;

	txtVigencia.value = "";
	txtAjusteSalario.value = "";
	
	fillGridData('btnFillGrid');
}

/********************************************************************
Atualiza o campo do dso ap�s uma edi��o.
********************************************************************/
function setDsoData(Row, Col) {
	var idxDtVigencia2 = getColIndexByColKey(fg, "dtVigencia2");
	var idxSalario2 = getColIndexByColKey(fg, "Salario2");
	var idxObservacao = getColIndexByColKey(fg, "Observacao");
	var idxRecHistoricoID = getColIndexByColKey(fg, "RecHistoricoID");
	
	// N�o � uma coluna edit�vel, ent�o o dso n�o � alterado.
	if ((Col != idxDtVigencia2) && (Col != idxSalario2) && (Col != idxObservacao)) {
		return;
	}

	dsoGrava.recordset.Find("RecHistoricoID", fg.TextMatrix(Row, idxRecHistoricoID));

	if (!dsoGrava.recordset.EOF && !dsoGrava.recordset.BOF) {
		if (fg.TextMatrix(Row, Col) != "")
			dsoGrava.recordset.fields[Col - 1].value = (Col != idxSalario2) ? fg.TextMatrix(Row, Col) : transformStringInNumeric(fg.TextMatrix(Row, Col));
		else
			dsoGrava.recordset.fields[Col - 1].value = null;
	}
}

/********************************************************************
Avalia se deve habilitar ou desabilitar o bot�o gravar.
********************************************************************/
function habilitaGravar() {
	btnOK.disabled = !(selModo.value == '2' && (txtVigencia.value != "" || txtAjusteSalario.value != ""));
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly() {
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalManutencaoSalarialBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalManutencaoSalarial_DblClick(grid, Row, Col) {
	var l;
	
	// Se n�o for manuten��o, n�o trata
	if (selModo.value != '2') return;

	// Inverte o valor de todos marcados.
	glb_TodosMarcados = !glb_TodosMarcados;
	
	// Se o click foi no label da coluna OK, linha zero e coluna 7,
	// inverte a
	if (Col == getColIndexByColKey(fg, "OK")) {
		for (l = 1; l < fg.Rows; l++) {
			fg.TextMatrix(l, getColIndexByColKey(fg, "OK")) = glb_TodosMarcados ? 1 : 0;
		}

		fg.Row = 1;
	}
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalManutencaoSalarialKeyPress(KeyAscii) {
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalManutencaoSalarial_ValidateEdit() {
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalManutencaoSalarial_BeforeEdit(grid, row, col) {
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalManutencaoSalarial_AfterEdit(Row, Col) {
	var idxSalario = getColIndexByColKey(fg, "Salario");
	var idxDtVigencia2 = getColIndexByColKey(fg, "dtVigencia2");
	var idxSalario2 = getColIndexByColKey(fg, "Salario2");
	var idxObservacao = getColIndexByColKey(fg, "Observacao");
	var idxRecHistoricoID = getColIndexByColKey(fg, "RecHistoricoID");
	var idxOK = getColIndexByColKey(fg, "OK");

	// N�o � uma coluna edit�vel, ent�o o dso n�o � alterado.
	if ((Col != idxDtVigencia2) && (Col != idxSalario2) && (Col != idxObservacao)) {
		return;
	}

	if (Col == idxSalario || Col == idxSalario2) {
		fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
	}
	
	if (Col == idxDtVigencia2 && !chkDataEx(fg.TextMatrix(Row, Col), true)) {
		alert("A data da vig�ncia n�o � v�lida.\nO formato correto deve ser " + DATE_FORMAT + ".");

		fg.TextMatrix(Row, Col) = "";
		return;
	}

	// Libera o bot�o para gravar
	btnOK.disabled = false;
	
	// Marca a linha para gravar.
	fg.TextMatrix(Row, idxOK) = "1";
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColModalManutencaoSalarial(grid, OldRow, OldCol, NewRow, NewCol) {
	if (glb_GridIsBuilding)
		return true;
	
	//setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
