/********************************************************************
modallocalidade.js

Library javascript para o modallocalidade.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport('dsoPesq');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modallocalidadeBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // configuracao inicial do html
    setupPage();   
           
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    fg.Redraw = 0;                             
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1,false,0);
    fg.Redraw = 2;

	if ( document.getElementById('txtCidade').disabled == false )
        txtCidade.focus();
        
    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Cidades');
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Cidade', 1);
        
    // ajusta o divCidade
    with (divCidade.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (21 * FONT_WIDTH) + 24 + 20;    
        height = 40;
        
    }
    
    // txtCidade
    txtCidade.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtCidade.style)
    {
        left = 0;
        top = 16;
        width = (txtCidade.maxLength ) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtCidade.onkeypress = txtCidade_onkeypress;
    
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtCidade.style.top, 10);
        left = parseInt(txtCidade.style.left, 10) + parseInt(txtCidade.style.width, 10) + ELEM_GAP;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divCidade.style.top, 10) + parseInt(divCidade.style.height, 10) + ELEM_GAP;
        width = (28 + (2 * 10) ) * FONT_WIDTH;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 7) + 4;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    headerGrid(fg,['Cidade',
				   'Localização',
                   'LocalidadeID'], [2]);
    fg.Redraw = 2;
}

function txtCidade_onkeypress()
{
    if ( event.keyCode == 13 )
    {
        btnFindPesquisa_onclick(btnFindPesquisa);
    }    
}

function txtCidade_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    txtCidade.value = trimStr(txtCidade.value);
    
    changeBtnState(txtCidade.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtCidade.value);
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    // 1. Usuario escolheu uma cidade, deu um duplo clique na mesma
    // ou escolheu uma cidade e clicou o botao OK
    if (ctl.id == btnOK.id )
    {
        // loop no array de estados
        
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S' , new Array(
                      fg.TextMatrix(fg.Row, 0), fg.TextMatrix(fg.Row, 2)));
    }    
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null );    
    
}

/********************************************************************
Executa pesquisa das cidades em funcao do estado.

Parametros:
estadoID        - id do estado
strCidade       - string parcial nome da cidade

Retorno:
nenhum
********************************************************************/
function startPesq(strCidade)
{
    lockControlsInModalWin(true);
    
    writeInStatusBar('child', 'Listando', 'cellMode' , true);
        
    var strPas = '?';
    strPas += 'strToFind='+escape(strCidade);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/basico/recursos/serverside/pesqcidade.aspx'+strPas;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

/********************************************************************
Retorno do servidor, da funcao startPesq

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function dsopesq_DSC() {
    startGridInterface(fg);
    headerGrid(fg,['Cidade',
				   'Localização',
                   'LocalidadeID'], [2]);
    
    fillGridMask(fg,dsoPesq,['Cidade',
  							 'Localizacao',
							 'LocalidadeID'],['','','']);

    lockControlsInModalWin(false);

    fg.Redraw = 0;                             
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1,false,0);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    fg.Redraw = 2;

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    
        
    writeInStatusBar('child', 'cellMode', 'Cidades');    
    
}
