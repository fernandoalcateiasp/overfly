
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalprofilesHtml" name="modalprofilesHtml">

<head>
<meta NAME="GENERATOR" Content="Microsoft Visual Studio 6.0">

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/recursos/modalpages/modalprofiles.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf                    
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/recursos/modalpages/modalprofiles.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nWidth, nHeight, nPerfilID, sPerfilFantasia

nWidth = 0
nHeight = 0
nPerfilID = 0
sPerfilFantasia = ""

For i = 1 To Request.QueryString("nWidth").Count    
    nWidth = Request.QueryString("nWidth")(i)
Next

Response.Write "var glb_nWidth = " & CStr(nWidth) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nHeight").Count    
    nHeight = Request.QueryString("nHeight")(i)
Next

Response.Write "var glb_nHeight = " & CStr(nHeight) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nPerfilID").Count    
    nPerfilID = Request.QueryString("nPerfilID")(i)
Next

Response.Write "var glb_nPerfilID = " & CStr(nPerfilID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("sPerfilFantasia").Count    
    sPerfilFantasia = Request.QueryString("sPerfilFantasia")(i)
Next

Response.Write "var glb_sPerfilFantasia = " & Chr(39) & sPerfilFantasia & Chr(39) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

'--------------------------------------------------------------------
'Os selects
Dim rsData, strSQL
Dim bProfileWithOutRights

Set rsData = Server.CreateObject("ADODB.Recordset")

'1. Verifica se  o perfil corrente tem direitos cadastrados
strSQL = "SELECT COUNT (*) AS numRights " & _
         "FROM Recursos_Direitos  WITH (NOLOCK) " & _ 
         "WHERE PerfilID=" & CStr(nPerfilID)

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

Response.Write "var glb_bProfileWithOutRights ="

' Nao tem direitos cadastrados
If ( (rsData.Fields("numRights").Value) = (0) ) Then
    Response.Write "true;"
    bProfileWithOutRights = TRUE
'Tem direitos cadastrados
Else
    Response.Write "false;"
    bProfileWithOutRights = FALSE
End If

Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf

'Fecha objeto
rsData.Close

'2. Os perfis cadastrados para preencher o combo selPerfis se e o perfil 
strSQL = "SELECT a.RecursoID AS RecursoID, a.RecursoFantasia AS Recurso " & _
         "FROM Recursos a  WITH (NOLOCK) " & _
         "WHERE a.RecursoID=" & CStr(nPerfilID) & " " & _
         "UNION ALL SELECT DISTINCT a.RecursoID AS RecursoID, a.RecursoFantasia AS Recurso " & _
         "FROM Recursos a, Recursos_Direitos b " & _ 
         "WHERE a.EstadoID<>4 AND a.EstadoID<>5 AND a.TipoRecursoID=6 AND a.RecursoID=b.PerfilID " & _ 
         "AND a.RecursoID NOT IN (SELECT a.RecursoID FROM Recursos a WHERE a.RecursoID=" & CStr(nPerfilID) & ") " & _ 
         "ORDER BY Recurso"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

'----------------------------------------------------------------

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 fg_md_BeforeRowColChange();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=RowColChange>
<!--
 fg_md_RowColChange();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 fg_md_AfterRowColChange();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
 fg_md_DblClick();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 fg_md_AfterEdit();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseDown>
<!--
 fg_md_MouseDown();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyDown>
<!--
 fg_md_KeyDown();
//-->
</SCRIPT>
</head>

<body id="modalprofilesBody" name="modalprofilesBody" LANGUAGE="javascript" onload="return window_onload()">

    <div id="divCtlBar" name="divCtlBar" class="divGeneral">
    <!-- Combo 1 -->
    <select id="selPerfis" name="selPerfis" class = "fldGeneral" title="Perfis Existentes">
    
<%
'Preenche os options do combo de Perfils
Dim optSel, idNum
optSel = "selected"
idNum = 1

While Not rsData.EOF

    'Seleciona o option cujo ID e o do perfil corrente
    If CLng(rsData.Fields("RecursoID").Value) = CLng(nPerfilID) Then
        optSel = "selected"
    Else
        optSel = ""
    End If        
    Response.Write "<option value =" & rsData.Fields("RecursoID").Value & _
                   " id=optPerfil" & CStr(idNum) & _
                   " " & optSel & ">" & _
                   rsData.Fields("Recurso").Value & _
                   "</option>" & chr(13) & chr(10)        
    idNum = idNum + 1
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>            
    
    </select>

    <!-- Botao 1 -->
    <input type=button  id="btnSincroniza" name="btnSincroniza" class = "btns" value="Sincronizar" title="Sincronizar direitos"></input>    
    <!-- Botao 2 -->
    <input type=button  id="btnReplica" name="btnReplica" class = "btns" value="Replicar" title="Replicar direitos um nivel abaixo"></input>    
    <!-- Botao 3 -->
    <input type=button  id="btnReplicaAll" name="btnReplicaAll" class = "btns" value="Repl Todos" title="Replicar direitos at� o ultimo nivel"></input>

    <!-- Botao 4 -->
    <input type=button  id="btnDetalha" name="btnDetalha" class = "btns" value="Detalhar" title="Detalhar direitos dos componentes do form"></input>        
    
    <!-- Linha de arremate inferior -->
    <hr id="bottonLine" name="bottonLine" class="sepGeneral"></hr>        
    </div>    
    
     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
    
    <input type="button" id="btnOK" name="btnOK" value="Aplicar" title="Aplicar altera��es" LANGUAGE="javascript" class="btns">
    
    <input type=button  id="btnUndo" name="btnUndo" class = "btns" value="Cancelar" title="Cancelar altera��o"></input>        
    
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" title="Fechar janela" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
