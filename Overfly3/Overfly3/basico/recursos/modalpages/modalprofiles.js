/********************************************************************
modalprofiles.js

Library javascript para o modalprofiles.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_LabelFolha = '';
var glb_lastPerfilIDSel = 0;
var glb_fgChangingRowColByCode = true;
var glb_bPendent = false;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 512*1;
var glb_strPars = '';
var glb_nDataLen = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoTreeView = new CDatatransport('dsoTreeView');
var dsoEditTable = new CDatatransport('dsoEditTable');
var dsoSaveEdition = new CDatatransport('dsoSaveEdition');
var dsoNewLoad = new CDatatransport('dsoNewLoad');

/* INDICE DAS FUNCOES ***********************************************

GERAIS:
    selPerfil_Changed(ctrl)
    dsoTreeViewComplete_DSC()
    btnControlBar_Clicked(ctrl)
    enableDisableInterfaceControls()
    setupTreeView(grid, dso)
    mountTreeView(grid, dso)
    addRootNode(grid, dso)
    addModulesNode(grid, dso)
    addSubModulesNode(grid, dso)
    addFormsNode(grid, dso)
    addNodeInGrid(grid, nodeLabel, parentNode, keyNode)
    addLeafInNode( grid, nodeRef, dsoInPos )
    setDsoInRoot(dso)
    sincronizaClicked()
    insertProfileInCombo()
    sincronizeRights()
    getFormDetails()
    getFormDetails_DSC()
    removeAllFormsDetails(fg, currFormID)
    removeFormDetails(grid, currFormNode)
    insertFormDetails(grid)
    insertFormContexts(grid, dso, currFormNode)
    insertContextSubForms(grid, dso, currFormNode)    
    insertSubFormsFeatures(grid, dso, currFormNode)
    insertStateMach(grid, dso, currFormNode)
    insertStateMachTransition(grid, dso, currFormNode)
    insertRights()
    replicaDireitoUmNivel()
    replicaDireitoTodosNiveis()
    replicaDireitos(grid, currNode, currLeafRow)

EVENTOS DO GRID:
    fg_md_DblClick()
    fg_md_BeforeRowColChange()
    fg_md_RowColChange()
    fg_md_AfterRowColChange()
    fg_md_AfterEdit()
    fg_md_MouseDown()

DSO DE EDICAO DA TABELA DE DIREITOS
    loadDataToEdit(nFormID)
    loadDataToEditModSubModForm_DSC
    loadDataToEditFormDetail_DSC
    saveEdition(grid, dso)
    updateGridLineInArray(grid, dso, line, registroID)

NOTAS:
    O dso e usado apenas para translado de dados entre o servidor e a
    interface.
    
    O grid mantem os dados necessarios a manipulacao dos direitos.
    Cada registro tem sempre duas linhas uma de label e outra de direitos.
    Desta forma o grid tem as seguintes colunas:
    0       - label do componente em no
            - label dos direitos em folha
    1 a 7   - bits de direito
    8       - nivel do componente ( de 1 a 7)
    9       - id do componente
    10      - id da mae do componente
    11      - id do contexto (para features (filtros, botoes e relatorios))
    12      - id do registro na tabela de direitos do perfil
    13      - id do estado para
    14      - linha que nao deve ser gravada. 1 nao grava, qualquer outro
              valor ou null grava
    
    As colunas de 1 a xxx sao preenchidas sempre na segunda linha.

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
            
    // ajusta o body do html
    var elem = document.getElementById('modalprofilesBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // opcao atualmente selecionada no combo de perfis
    if ( selPerfis.selectedIndex != -1 )
        glb_lastPerfilIDSel = selPerfis.value;
    
    // se o perfil corrente tem direitos
    if ( glb_bProfileWithOutRights == false )
        selPerfil_Changed(selPerfis);
    else
    {
        // destrava combo de perfis se for o caso
        if ( selPerfis.length >  0 )
            selPerfis.disabled = false;
            
        // coloca foco no campo apropriado
        if ( fg.disabled == false )
            fg.focus();
        
        enableDisableInterfaceControls();
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Perfil: ' + glb_sPerfilFantasia, 1);
    
    // ajusta elementos da janela
    var eLeft = 2;
    var eTop = 2;
    var rQuote;
    var btnWidth = 78;
    var i, coll;
    
    // ajusta o divCtlBar
    with (divCtlBar.style)
    {
        border = 'none';
        backgroundColor = 'silver';
        left = 0;
        top = parseInt(divMod01.currentStyle.height);
        height = 27;
        width = (glb_nWidth - 5);    
    }
 
    // combo de perfis
    with ( selPerfis.style )
    {
        left = eLeft;
        top = eTop;
        width = 155;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    selPerfis.onchange = selPerfil_Changed;
    // trava o combo por definicao
    selPerfis.disabled = true;
 
    // botoes
    
    // sincronizar
    with (btnSincroniza.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop;
        width = btnWidth;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    // replicar
    with (btnReplica.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop;
        width = btnWidth;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    // replicar tudo
    with (btnReplicaAll.style)
    {
        left = rQuote + 1;
        top = eTop;
        width = btnWidth;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    
    // detalhar
    with (btnDetalha.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop;
        width = btnWidth;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }    
    
    // move o btnOK, o btnUndo e o btnCanc
    divCtlBar.appendChild(btnOK);
    divCtlBar.appendChild(btnUndo);
    divCtlBar.appendChild(btnCanc);
    
    // OK
    with (btnOK.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop;
        width = btnWidth;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    
    // UNDO
    with (btnUndo.style)
    {
        left = rQuote + 1;
        top = eTop;
        width = btnWidth;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    } 
    
    // Canc - fecha a janela
    with (btnCanc.style)
    {
        left = rQuote + 1;
        top = eTop;
        width = btnWidth;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    } 
     
    // linha horizontal de arremate inferior bottonLine
    with ( bottonLine.style )
    {
        color = 'gray';
        left = 0;
        top = parseInt(divCtlBar.currentStyle.height) - 1;
        height = 1;
        width = (glb_nWidth - 5);
    }   

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = 7 * ELEM_GAP - 7;
        width = (glb_nWidth - 3 * ELEM_GAP) + 6 ;    
        height = glb_nHeight - parseInt(top) - ELEM_GAP - 2;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.currentStyle.width);
        height = parseInt(divFG.currentStyle.height);
    }
    
    startGridInterface(fg);
    
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(fg.currentStyle.width, 10) * 18;
    
    // Funcao de clique de botao
    coll = window.document.getElementsByTagName('INPUT');
    for ( i=0; i<coll.length; i++ ) 
    {
        if ( (coll[i].type).toUpperCase() == 'BUTTON'  )
            coll[i].onclick = btnControlBar_Clicked;
    }
}

/********************************************************************
Fecha o modal window
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    
}

/********************************************************************
Selecao do combo de direitos foi alterada pelo usuario

Parametro:
ctrl        - Opcional. referencia ao combo
nAction     - Opcional. Acao a tomar no servidor

Retorno:
nenhum
********************************************************************/
function selPerfil_Changed(ctrl, nAction)
{
    // so pergunta se for acao do usuario
    if ( ctrl == null && nAction == null)
    {
        if ( glb_bProfileWithOutRights == false )    
        {
            var _retMsg = window.top.overflyGen.Confirm('Visualiza os direitos do perfil selecionado ignorando os atuais?');
    
            if ( _retMsg == 0 )
                return null;
            else if ( _retMsg == 2 )
            {
                selOptByValueInSelect(getHtmlId(), selPerfis.id, glb_lastPerfilIDSel);
                return null;
            }   
        }
    }    
    
    // guarda ultimo perfilID selecionado
    if ( selPerfis.selectedIndex != -1 )
        glb_lastPerfilIDSel = selPerfis.value;
        
    if ( ctrl == null )
        ctrl = this;
    
    glb_bPendent = false;        
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);    
    
    // chama pagina asp no servidor com o value da opcao selecionada
    // remonta o grid
    // passa o id do form por parametro
    
    var strPars = new String();
    strPars = '?';
    strPars += 'nPerfilID=';
    strPars += escape(ctrl.value);
    
    var nRecursoID = 0;
        
    if ( nAction == null )
        nAction = 0;
    // se o no corrente for Sistema,Modulo,SubModulo
    // manda o id do no corrente
    else
    {
        if ( fg.GetNode(fg.Row) != null )
            nRecursoID = (fg.GetNode(fg.Row)).Key;
    }

    strPars += '&nRecursoID=';
    strPars += escape(nRecursoID);
        
    strPars += '&nAction=';
    strPars += escape(nAction);
    
    
    dsoTreeView.URL = SYS_ASPURLROOT + '/basico/recursos/serverside/loadprofile.aspx'+strPars;
	dsoTreeView.ondatasetcomplete = dsoTreeViewComplete_DSC;
	dsoTreeView.refresh();
    
    // monta grid e destrava a interface no retorno
}

function dsoTreeView_DSC() 
{
    var strPars = '?nPerfilID=' + escape(glb_nPerfilID);
    strPars += '&nAction=0';
    
    dsoNewLoad.URL = SYS_ASPURLROOT + '/basico/recursos/serverside/loadprofile.aspx'+strPars;
	dsoNewLoad.ondatasetcomplete = dsoNewLoad_DSC;
	dsoNewLoad.refresh();
}

function dsoNewLoad_DSC()
{
    dsoTreeViewComplete_DSC(dsoNewLoad);
}

/********************************************************************
Retorno do servidor da chamada da pagina loadprofile.aspx

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoTreeViewComplete_DSC(dso)
{
    if (dso == null)
        dso = dsoTreeView;
    // carrega dados no dso de edicao dsoEditTable
    loadDataToEdit();
    
    // monta o treeview com o dso read only
    setupTreeView(fg, dso);
    
    // ajusta botoeira
    enableDisableInterfaceControls();
}

/********************************************************************
Usuario clicou botao da barra de controles

Parametro:
ctrl        - Opcional. referencia ao sender.

Retorno:
nenhum
********************************************************************/
function btnControlBar_Clicked(ctrl)
{
    var currBtn;

    // o botao corrente
    if ( ctrl == null )
        currBtn = this;
    else
        currBtn = ctrl;    
    
    if ( currBtn.disabled )
        return true;
        
    // Particular desta pagina para dar funcionalidade
    // ao btnCloseWin    
	if ( currBtn.id.toUpperCase() == 'BTNCLOSEWIN' )
	{
		currBtn= btnCanc;
	}        
    
    // redireciona se for OK e Cancela
    
    if ( currBtn == btnCanc )
    {
        var _retMsg = window.top.overflyGen.Confirm('Fechar a janela?');
    
        if ( _retMsg == 0 )
            return null;
        else if ( _retMsg == 1 )
            btn_onclick(currBtn);
        else
            return true;    
    }    
    else if ( currBtn == btnSincroniza )
        sincronizaClicked();
    else if ( currBtn == btnReplica )
        replicaDireitoUmNivel();
    else if ( currBtn == btnReplicaAll )
        replicaDireitoTodosNiveis();
    else if ( currBtn == btnDetalha )
        getFormDetails(0);    
    else if ( currBtn == btnUndo )
        selPerfil_Changed(selPerfis);
    else if ( currBtn == btnOK )
        saveEdition(fg, dsoEditTable);    
}

/********************************************************************
Habilita ou desabilita os controles da interface

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function enableDisableInterfaceControls()
{
    // 1. Perfil sem direitos, combo de perfis qualquer situacao,
    // grid read only qualquer situacao.
    // selPerfis destravado
    // btnSincroniza destravado
    // btnReplica travado
    // btnReplicaAll travado
    // btnDetalha travado
    // btnOK travado
    // btnCanc destravado (Fechar)
    // btnUndo travado (Cancelar)

    // fg destravado e read only
    if ( (glb_bProfileWithOutRights == true) &&
         (selPerfis.value != null) &&
         (fg.Rows > 1) )
    {
        selPerfis.disabled = false;
        btnSincroniza.disabled = false;
        btnReplica.disabled = true;
        btnReplicaAll.disabled = true;
        btnDetalha.disabled = true;
        btnOK.disabled = true;
        btnUndo.disabled = true;
        btnCanc.disabled = false;
        fg.Editable = false;
    }
    
    // 2. Perfil com direitos, combo de perfis qualquer situacao,
    // grid editavel se o combo esta no perfil corrente
    // selPerfis destravado
    // btnSincroniza destravado
    // btnReplica destravado se o combo esta no perfil corrente
    // btnReplicaAll destravado se ocombo esta no perfil corrente
    // btnDetalha destravado se o combo esta no perfil corrente
    // btnOK destravado se o combo esta no perfil corrente
    // btnUndo destravado
    // btnCanc destravado
    // fg destravado e editavel apenas se o combo esta no perfil corrente
    if ( (glb_bProfileWithOutRights == false) &&
         (selPerfis.value != null) &&
         (fg.Rows > 1) )
    {
        selPerfis.disabled = true;
        btnSincroniza.disabled = true;
        btnReplica.disabled = true;
        btnReplicaAll.disabled = true;
        btnDetalha.disabled = true;
        btnOK.disabled = true;
        btnUndo.disabled = true;
        btnCanc.disabled = false;
        fg.Editable = false;
        
        if ( glb_nPerfilID == selPerfis.value )
        {  
            if (glb_bPendent)
            {            
                // se a folha corrente tem vizinhos
                var currNode = fg.GetNode(fg.Row);
                var firstSubNode = currNode.GetNode(2);
                // So habilita se tem nos filhos
                if ( firstSubNode != null )
                {
                   btnReplica.disabled = false;
                   
                   // so habilita se nao for o estado
                   if ( parseInt(fg.TextMatrix(currNode.Row + 1, 8)) != 8 )
                       btnReplicaAll.disabled = false;        
                }
                btnOK.disabled = false;
                btnUndo.disabled = false;
                fg.Editable = true;
            }
            else
            {
                // se a folha corrente tem vizinhos
                var currNode = fg.GetNode(fg.Row);
                var firstSubNode = currNode.GetNode(2);

                selPerfis.disabled = false;
                // So habilita se nao esta no ultimo nivel
                if ( parseInt(fg.TextMatrix((currNode.Row + 1), 8), 10) < 7 )
                    btnSincroniza.disabled = false;

                // So habilita se tem nos filhos
                if ( firstSubNode != null )
                {
                   btnReplica.disabled = false;
                   
                   // so habilita se nao for o estado
                   if ( parseInt(fg.TextMatrix(currNode.Row + 1, 8)) != 8 )
                       btnReplicaAll.disabled = false;        
                }

                // se o no corrente e form            
                if ( fg.TextMatrix(fg.Row, 8) == 4 )
                {
                    btnDetalha.disabled = false;
                }
                fg.Editable = true;
            }
        }            
        else
        {
            selPerfis.disabled = false;
            btnSincroniza.disabled = false;
            btnReplica.disabled = true;
            btnReplicaAll.disabled = true;
            btnOK.disabled = true;
            btnUndo.disabled = true;
            btnCanc.disabled = false;
            fg.Editable = false;
        }            
    }
}

/********************************************************************
Inicializa o grid

Parametro:
grid        - referencia ao grid
dso         - referencia ao dso

Retorno:
nenhum
********************************************************************/
function setupTreeView(grid, dso)
{
    // controla se a mudanca de linha do grid e
    // por codigo ou por acao do usuario
    glb_fgChangingRowColByCode = true;
    
    grid.Redraw = 0;
    
    var i;
    
    with (grid)
    {
        // diversos
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '10';
   		Editable = false;
		AllowUserResizing = 1;
		VirtualData = true;
		Ellipsis = 1;
		SelectionMode = 1;
		AllowSelection = false;
		Rows = 1;
		Cols = 15;
		FixedRows = 1;
		FixedCols = 1;
		ScrollBars = 3;
		OutLineBar = 1;
		GridLines = 0;
		FormatString = 'Componentes do Sistema' + '\t' +
		               'C1' + '\t' + 'C2' + '\t' + 'A1' + '\t' + 'A2' + '\t' +
		               'I' + '\t' + 'E1' + '\t' + 'E2';
		// cores
		TreeColor = 0X000000;
		//BackColorFixed = 0XFFFFFF;
		// linhas
		// Coloca botoes na barra
        GridLinesFixed = 13;
		GridLines = 1;
		GridColor = 0X000000;
		
		// colunas escondidas
		ColHidden(8) = true;
		ColHidden(9) = true;
		ColHidden(10) = true;
		ColHidden(11) = true;
		ColHidden(12) = true;
		ColHidden(13) = true;
		ColHidden(14) = true;
		
		// coluna 1
		ColWidth(0) = (1440 * 5) + 4*(240 + 90) - 100;
				
		// colunas tipo checkBox
		for ( i=1; i<8; i++ )
		{
		    ColDataType(i) = 11;
		    
    		// largura das colunas checkBox
    		ColWidth(i) = 240 + 90;
		}
    }
    
    // monta o treeview se o dso tem registros
    if ( dso.recordset.BOF && dso.recordset.EOF )
    {
        grid.Redraw = 2;
        
        // controla se a mudanca de linha do grid e
        // por codigo ou por acao do usuario
        glb_fgChangingRowColByCode = false;
        
        return null;
    }
    
    dso.recordset.MoveFirst();
    
    mountTreeView(grid, dso);
        
    grid.Redraw = 2;
    
    // controla se a mudanca de linha do grid e
    // por codigo ou por acao do usuario
    glb_fgChangingRowColByCode = false;
    
    if (grid.Rows > 2)
        grid.Row = 2;
        
    else
        grid.Row = 1;
}

/********************************************************************
Monta o grid

Parametro:
grid        - referencia ao grid
dso         - referencia ao dso

Retorno:
nenhum
********************************************************************/
function mountTreeView(grid, dso)
{
    addRootNode(grid, dso);
    addModulesNode(grid, dso);
    addSubModulesNode(grid, dso);    
    addFormsNode(grid, dso);
}

/********************************************************************
Adiciona o no raiz ao grid

Parametro:
grid        - referencia ao grid
dso         - referencia ao dso

Retorno:
nenhum

O grid tem as seguintes colunas:
0       - label do componente em no
        - label dos direitos em folha
1 a 7   - bits de direito
8       - nivel do componente ( de 1 a 7)
9       - id do componente
10      - id da mae do componente
11      - id do contexto (para features (filtros, botoes e relatorios))
    
********************************************************************/
function addRootNode(grid, dso)
{
    var i;
    var rootNode;

    // o no raiz - Overfly
    if ( setDsoInRoot(dso) )
    {
        // adiciona o no e o label
        grid.AddItem(dso.recordset['Recurso'].value);
        grid.IsSubTotal(grid.Rows - 1) = true;
        grid.RowOutlineLevel(grid.Rows - 1) = 0;
        grid.Row = grid.Rows - 1;
        
        // a folha
        grid.AddItem(glb_LabelFolha);
        grid.Row = grid.Rows - 1;
        
        rootNode = grid.GetNode(1);
        rootNode.Key = dso.recordset['RecursoID'].value.toString();
        
        // adiciona os direitos por loop horizontal nas colunas
        // do dso
        for ( i=1;i<=7; i++ )
        {
            grid.TextMatrix(grid.Row, i) = 
                dso.recordset.fields[4 + i].value;   
        }
        
        // adiciona o nivel do componente
        grid.TextMatrix(grid.Row, 8) = 
            dso.recordset['Nivel'].value;
            
        // adiciona o id do componente
        grid.TextMatrix(grid.Row, 9) = 
            dso.recordset['RecursoID'].value;   
        
        // adiciona o id da mae do componente
        if ( dso.recordset['RecursoMaeID'].value != null )
        {
            grid.TextMatrix(grid.Row, 10) = 
                dso.recordset['RecursoMaeID'].value;       
        }
        else
            grid.TextMatrix(grid.Row, 10) = '';    
        
        // adiciona id do contexto do componente
        grid.TextMatrix(grid.Row, 11) = 
            dso.recordset['ContextoID'].value;
            
        // adiciona id do registro do direito do componente
        grid.TextMatrix(grid.Row, 12) = 
            dso.recordset['RegistroID'].value;                  
    }

}

/********************************************************************
Adiciona os nos dos modulos ao grid

Parametro:
grid        - referencia ao grid
dso         - referencia ao dso

Retorno:
nenhum
********************************************************************/
function addModulesNode(grid, dso)
{
    // obtem o id da mae, que neste caso e o overfly
    var motherRecursoID = 999;
    var parentNode;
    
    // referencia ao no raiz que e o Overfly
    parentNode = grid.GetNode(1);
    
    // move o cursor do dso para o primeiro registro
    dso.recordset.MoveFirst();
    
    // filtra o dso para todos os modulos associados ao no raiz
    dso.recordset.setFilter('RecursoMaeID=' + motherRecursoID.toString() + ' ' + 'AND Nivel = 2');
    
    // loop em todos os modulos, preenchendo o grid com os modulos
    while (!dso.recordset.EOF)
    {
        nodeRef = addNodeInGrid(grid, dso.recordset['Recurso'].value,
                                parentNode,
                                dso.recordset['RecursoID'].value);

        if ( nodeRef != null )
            addLeafInNode( grid, nodeRef, dso );
        
        dso.recordset.MoveNext();
    }
    
    // desfiltra o dso
    dso.recordset.setFilter('');
}

/********************************************************************
Adiciona os nos dos sub modulos ao grid

Parametro:
grid        - referencia ao grid
dso         - referencia ao dso

Retorno:
nenhum
********************************************************************/
function addSubModulesNode(grid, dso)
{
    var rootNode;
    var modNode; 
    var motherRecursoID;    
    
    // referencia ao no raiz que e o Overfly
    rootNode = grid.GetNode(1);
    // pega referencia ao primeiro no modulo do Overfly
    modNode = rootNode.GetNode(2);

    while (modNode != null)
    {    
        // obtem o id da mae
        motherRecursoID = parseFloat(modNode.Key);
            
        // move o cursor do dso para o primeiro registro
        dso.recordset.MoveFirst();
            
        // filtra o dso para todos os submodulos associados ao modulo corrente
        dso.recordset.setFilter('RecursoMaeID=' + motherRecursoID.toString() + ' ' + 'AND Nivel = 3');
            
        // loop em todos os sub modulos do modulo corrente,
        // preenchendo o grid com os submodulos do modulo corrente
        while (!dso.recordset.EOF)
        {
            nodeRef = addNodeInGrid(grid, dso.recordset['Recurso'].value,
                                modNode,
                                dso.recordset['RecursoID'].value);

            if ( nodeRef != null )
                addLeafInNode( grid, nodeRef, dso );
                
            dso.recordset.MoveNext();
        }
            
        // desfiltra o dso
        dso.recordset.setFilter('');
        
        modNode = modNode.GetNode(6);
    }    
}

/********************************************************************
Adiciona os nos dos forms ao grid

Parametro:
grid        - referencia ao grid
dso         - referencia ao dso

Retorno:
nenhum
********************************************************************/
function addFormsNode(grid, dso)
{
    var rootNode;
    var modNode; 
    var subModNode;
    var motherRecursoID;    
    
    // referencia ao no raiz que e o Overfly
    rootNode = grid.GetNode(1);
    // referencia ao primeiro no modulo do Overfly
    modNode = rootNode.GetNode(2);

    while (modNode != null)
    {
        // referencia ao primeiro no sub modulo do Overfly
        subModNode = modNode.GetNode(2);
        
        while (subModNode != null)
        {   
            // obtem o id da mae
            motherRecursoID = parseFloat(subModNode.Key);
                
            // move o cursor do dso para o primeiro registro
            dso.recordset.MoveFirst();
                
            // filtra o dso para todos os forms associados ao sub modulo corrente
            dso.recordset.setFilter('RecursoMaeID=' + motherRecursoID.toString() + ' ' + 'AND Nivel = 4');
                
            // loop em todos os forms do sub modulo corrente,
            // preenchendo o grid com os forms do sub modulo corrente
            while (!dso.recordset.EOF)
            {
                nodeRef = addNodeInGrid(grid, dso.recordset['Recurso'].value,
                                    subModNode,
                                    dso.recordset['RecursoID'].value);

                if ( nodeRef != null )
                    addLeafInNode( grid, nodeRef, dso );
                    
                dso.recordset.MoveNext();
            }
                
            // desfiltra o dso
            dso.recordset.setFilter('');
        
            subModNode = subModNode.GetNode(6);
        }
        
        modNode = modNode.GetNode(6);
    }
}

/********************************************************************
Adiciona no ao grid

Parametro:
grid        - referencia ao grid
nodeLabel   - label do no
parentNode  - referencia no qual vai ser criado um no filho
keyNode     - id do componente a associar ao corrente no

Retorno:
referencia ao no adicionado ou null se falha
********************************************************************/
function addNodeInGrid(grid, nodeLabel, parentNode, keyNode)
{
    var i;
    var retVal = null;
    
    // posicionar o grid na linha da folha do no parent
    
    for ( i=1; i<grid.Rows; i++ )
    {
        if ( parseFloat(grid.TextMatrix(i, 9)) == parseFloat(parentNode.Key) )
        {
            grid.Row = i;
            parentNode.AddNode(3, nodeLabel, keyNode.toString());
            // referencia ao no adicionado
            retVal = parentNode.GetNode(3);
            break;
        }    
    }
    
    return retVal;
}

/********************************************************************
Adiciona folha a um no

Parametro:
grid        - referencia ao grid
nodeRef     - referencia ao no que vai receber a folha
dsoInPos    - dso posicionado no registro

Retorno:
nenhum
********************************************************************/
function addLeafInNode( grid, nodeRef, dsoInPos )
{
    var i;
    var dso = dsoInPos;
    var nodeParent;
    
    // a folha
    grid.AddItem(glb_LabelFolha, nodeRef.Row + 1);
    
    grid.Row = nodeRef.Row + 1;
        
    // adiciona os direitos por loop horizontal nas colunas
    // do dso
    for ( i=1;i<=7; i++ )
    {
        if ( dso.recordset.fields[4 + i].value == true )
            grid.TextMatrix(grid.Row, i) = 1;
        else
            grid.TextMatrix(grid.Row, i) = 0;
    }
        
    // adiciona o nivel do componente
    grid.TextMatrix(grid.Row, 8) = 
        dso.recordset['Nivel'].value;
            
    // adiciona o id do componente
    grid.TextMatrix(grid.Row, 9) = 
        dso.recordset['RecursoID'].value;   
        
    // adiciona o id da mae do componente
    grid.TextMatrix(grid.Row, 10) = 
        dso.recordset['RecursoMaeID'].value;       
        
    // adiciona id do contexto do componente
    grid.TextMatrix(grid.Row, 11) = 
        dso.recordset['ContextoID'].value;
        
    // adiciona id do registro do direito do componente
    grid.TextMatrix(grid.Row, 12) = 
        dso.recordset['RegistroID'].value;                             
            
    // adiciona id do estado para do componente
    grid.TextMatrix(grid.Row, 13) = 
        dso.recordset['EstadoParaID'].value;
            
    // linha nao deve ser gravada porque e um no de estado
    // so para compatibilidade da interface
    nodeParent = nodeRef.GetNode(1);
    if ( nodeParent != null)
    {
        if ( dso.recordset['Nivel'].value == 8 )
            if ( parseInt(grid.TextMatrix(nodeParent.Row + 1, 8), 10) == 6 )                                     
                grid.TextMatrix(grid.Row, 14) = '1';
    }    
}

/********************************************************************
Posiciona o dso no registro de nivel 1 que e o Overfly

Parametro:
dso      - referencia ao dso a usar

Retorno:
true se localiza o root e se tem dados nos campos do dso,
caso contrario false
********************************************************************/
function setDsoInRoot(dso)
{
    var retVal = false;
    
    // O dso nao foi preenchido
    if (dso.recordset.Fields.Count == 0)
        return retVal;
    
    // nao tem registros, retorna nao ter direito
    if ( dso.recordset.BOF && dso.recordset.EOF )
        return retVal;
    
    // faremos aqui nossa pesquisa
    dso.recordset.MoveFirst();
    
    dso.recordset.Find('Nivel', '1');
    
    // verifica se o objeto n�o est� no EOF
    if ( !dso.recordset.EOF )
        retVal = true;

    return retVal;
}

/********************************************************************
Insere esqueleto de direitos no banco e atualiza interface.
Depende dos direitos do perfil corrente.
1. Se o perfil corrente nao tem nenhum direito
Depende da opcao selecionada no combo de perfis.
1.1. Se option value = 0 -> insere na tabela de Recursos_Direitos
um esqueleto completo de direitos (ate form) com todos os componentes
sem nenhum direito.
1.2. Se option value != 0 -> insere na tabela de Recursos_Direitos
um clone completo de direitos (ate feature) do perfil selecionado.

2. Se o perfil corrente tem algum direito
Depende da opcao selecionada no combo de perfis.
Faz quatro operacoes.
Examina os registros de direitos da tabela de Recursos_Direitos em relacao
aos registros da tabela de Recursos em duas direcoes:
2.1. Se o perfil corrente tem algum direito sobre um recurso desativado ou
deletado, este direito e eliminado.
2.2. Se existe algum recurso ao qual o perfil nao tem direito associado, este
registro de direito e criado no perfil, sem nenhum direito.
2.3. Examina os registro de direitos do perfil corrente e deleta os registros
de componentes de nivel inferior a form, que nao tenham nenhum direito.
2.4. Remonta a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function sincronizaClicked()
{
    var _retMsg, __retMsgPart;
    var recNivel;
    
    // Perfil sem direitos
    if (glb_bProfileWithOutRights)
    {
        _retMsg = window.top.overflyGen.Confirm('Sincronizar o perfil?');
    
        if ( _retMsg == 0 )
            return null;
        else if ( _retMsg == 1 )
            insertRights();            
        else
            return null;
    }
    // Perfil com direitos
    else if (!glb_bProfileWithOutRights)
    {
        // Combo de Perfis com o Perfil corrente selecionado
        if (selPerfis.value == glb_nPerfilID)
        {
            _retMsg = window.top.overflyGen.Confirm('Sincronizar o perfil?');
    
            if ( _retMsg == 0 )
                return null;
            else if ( _retMsg == 1 )
                sincronizeRights();
            else
               return null;
        }
        // Combo de Perfis com o Perfil corrente nao selecionado
        else
        {
            // se recurso selecionado � nivel >= 1 e <= 5
            recNivel = fg.ValueMatrix(fg.Row, 8);
            
            if( (recNivel >= 1) && (recNivel <= 4) )
            { 
                if ( recNivel == 1 )
                    __retMsgPart = 'o perfil todo';
                else if ( recNivel == 2 )
                    __retMsgPart = 'o m�dulo selecionado';
                else if ( recNivel == 3 )
                    __retMsgPart = 'sub-m�dulo selecionado';
                else if ( recNivel == 4 )
                    __retMsgPart = 'form selecionado';
            
                _retMsg = window.top.overflyGen.Confirm('Clonar '  + __retMsgPart + '?');
    
                if ( _retMsg == 0 )
                    return null;
                else if ( _retMsg == 1 )
                    insertRights();
                else
                    return null;    
            }
            else
            {
                _retMsg = window.top.overflyGen.Confirm('Clonagem s� permitida at� n�vel de form');
    
                if ( _retMsg == 0 )
                    return null;
            }
        }
    }
}

/********************************************************************
1. Se o option value do selPerfis = 0 -> insere na tabela de Recursos_Direitos
um esqueleto completo de direitos (ate form) com todos os componentes
sem nenhum direito.
2. Se o option value do selPerfis!= 0 -> insere na tabela de Recursos_Direitos
um clone completo de direitos (ate feature) do perfil selecionado.

Parametro:
nenhum
Retorno:
nenhum
********************************************************************/
function insertRights()
{
    var nPerfilID;
    var nPerfilCloneID;
    var nNivel = 0;
    var nRecursoID = 0;
    var nRecursoMaeID = 0;

    nPerfilID = glb_nPerfilID;
    nPerfilCloneID = selPerfis.value; 
    
    if (fg.Row >= 1)
    {
        nNivel = fg.ValueMatrix(fg.Row, 8);
        nRecursoID = fg.ValueMatrix(fg.Row, 9);
        nRecursoMaeID = fg.ValueMatrix(fg.Row, 10);
    }

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    // forca o combo de direitos para o perfil corrente
    selOptByValueInSelect(getHtmlId(), selPerfis.id, glb_nPerfilID);
    // seta variavel ultima opcao selecionada no combo
    glb_lastPerfilIDSel = glb_nPerfilID;    

    glb_bProfileWithOutRights = false;

    var strPars = new String();
    strPars = '?nPerfilID=' + escape(nPerfilID);
    strPars += '&nPerfilCloneID=' + escape(nPerfilCloneID);
    strPars += '&nNivel=' + escape(nNivel);
	strPars += '&nRecursoID=' + escape(nRecursoID);
	strPars += '&nRecursoMaeID=' + escape(nRecursoMaeID);

    dsoTreeView.URL = SYS_ASPURLROOT + '/basico/recursos/serverside/insertrights.aspx'+strPars;
    dsoTreeView.ondatasetcomplete = dsoTreeView_DSC;
    dsoTreeView.refresh();
}

/********************************************************************
Se for um perfil que ainda nao tem direitos, insere este perfil como
sendo o primeiro do combo de perfis e seleciona-o

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function insertProfileInCombo()
{
    var oOption = document.createElement("OPTION");
        
    oOption.text = glb_sPerfilFantasia;
    oOption.value = glb_nPerfilID;
    
    selPerfis.add(oOption, 0);
    
    selPerfis.selectedIndex = 0;
}

/********************************************************************
Examina os registros de direitos da tabela de Recursos_Direitos em relacao
aos registros da tabela de Recursos em duas direcoes:
2.1. Se o perfil corrente tem algum direito sobre um recurso desativado ou
deletado, este direito e eliminado.
2.2. Se existe algum recurso ao qual o perfil nao tem direito associado, este
registro de direito e criado no perfil, sem nenhum direito.
2.3. Examina os registro de direitos do perfil corrente e deleta os registros
de componentes de nivel inferior a form, que nao tenham nenhum direito.
2.4. Remonta a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function sincronizeRights()
{
    // se o no corrente e form            
    if (fg.TextMatrix(fg.Row, 8) >= 4)
    {
        // sincroniza e traz os componentes do form
        getFormDetails(1);
    }
    else
        selPerfil_Changed(selPerfis, 1);
}

/********************************************************************
Obtem detalhes do form corrente no servidor e atualiza a interface

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function getFormDetails(nAction)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    var nPerfilID;
    // ID do recurso a partir do qual o perfil deve ser sincronizado
    
    var nRecursoID = 0;
    var nFormID = 0;
    var nContextoID = 0;
    var nConsultar1;
    var nConsultar2;
    var nAlterar1;
    var nAlterar2;
    var nIncluir;
    var nExcluir1;
    var nExcluir2;
    var nNivel; 
    var currNivel;
    var currNode;
    
    if (nAction == null)
        nAction = 0;
            
    // Dados para o servidor
    nPerfilID = selPerfis.value;
    
    nRecursoID = (fg.GetNode(fg.Row)).Key;

    nNivel = parseInt(fg.TextMatrix(fg.Row,8),10);

    // o no corrente e um Form
    if (nNivel == 4)
        nFormID = nRecursoID;
    // o no corrente e algum componente do form
    else
    {
        currNode = fg.GetNode(fg.Row);
        currNivel = parseInt(fg.TextMatrix(fg.Row, 8), 10);
        
        while ( currNivel > 4 )
        {
            currNode = currNode.GetNode(1);
            currNivel = parseInt(fg.TextMatrix((currNode.Row + 1), 8), 10);
            if ( currNivel == 4 )
            {
                nFormID = currNode.Key;
                break;
            }    
        }
    }            
    
    // o no corrente e um subform, obtem o contextoID
    if (nNivel == 6)
    {
        currNode = fg.GetNode(fg.Row);
        currNode = currNode.GetNode(1);
        nContextoID = currNode.Key;            
    }
        
    var strPars = new String();
    strPars = '?nPerfilID='+escape(nPerfilID);
    strPars += '&nRecursoID='+escape(nRecursoID);
    strPars += '&nFormID='+escape(nFormID);
    strPars += '&nContextoID='+escape(nContextoID);
    strPars += '&nAction='+escape(nAction);
    
    dsoTreeView.URL = SYS_ASPURLROOT + '/basico/recursos/serverside/getformdetail.aspx'+strPars;
    dsoTreeView.ondatasetcomplete = getFormDetails_DSC;
    dsoTreeView.refresh();
}

/********************************************************************
Retorno da funcao que obtem detalhes do form corrente no servidor
e atualiza a interface

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function getFormDetails_DSC()
{
    // guarda id do form corrente
    var i;
    var currRecursoID = (fg.GetNode(fg.Row)).Key;
    var currFormID;
    var nNivel;
    var currNode;
    var currNivel;
    
    nNivel = parseInt(fg.TextMatrix(fg.Row,8),10);

    // o no corrente e um Form
    if (nNivel == 4)
        currFormID = currRecursoID;
    // o no corrente e algum componente do form
    else
    {
        currNode = fg.GetNode(fg.Row);
        currNivel = parseInt(fg.TextMatrix(fg.Row, 8), 10);
        
        while ( currNivel > 4 )
        {
            currNode = currNode.GetNode(1);
            currNivel = parseInt(fg.TextMatrix((currNode.Row + 1), 8), 10);
            if ( currNivel == 4 )
            {
                currFormID = currNode.Key;
                break;
            }    
        }
    }           
     
    // carrega dados no dso de edicao dsoEditTable
    loadDataToEdit(currFormID);
    
    // controla se a mudanca de linha do grid e
    // por codigo ou por acao do usuario
    glb_fgChangingRowColByCode = true;
    
    fg.Redraw = 0;
    removeAllFormsDetails(fg, currFormID);
    insertFormDetails(fg, dsoTreeView);
    fg.Redraw = 2;
    
    // retorna foco do grid na folha do no selecionado
    for ( i=0; i< fg.Rows; i++ ) 
    {
        if ( fg.TextMatrix(i, 9) == currRecursoID )
        {
            fg.Row = i;
            // garante a visibilidade do no do form corrente
            fg.TopRow = i - 1;
            break;
        }    
    }
    
    // controla se a mudanca de linha do grid e
    // por codigo ou por acao do usuario
    glb_fgChangingRowColByCode = false;
}

/********************************************************************
Remove detalhes de todos os forms correntes

Parametro:
grid            - referencia ao grid

Retorno:
nenhum
********************************************************************/
function removeAllFormsDetails(grid, currFormID)
{
    var i;
    var rootNode;
    var modNode; 
    var subModNode;
    var formNode;
    
    // referencia ao no raiz que e o Overfly
    rootNode = grid.GetNode(1);
    // referencia ao primeiro no modulo do Overfly
    modNode = rootNode.GetNode(2);

    while (modNode != null)
    {
        // referencia ao primeiro no sub modulo do Overfly
        subModNode = modNode.GetNode(2);
        
        while (subModNode != null)
        {   
            formNode = subModNode.GetNode(2);
            
            while (formNode != null)
            {   
                removeFormDetails(grid, formNode);
                formNode = formNode.GetNode(6);
            }    
               
            subModNode = subModNode.GetNode(6);
        }
        
        modNode = modNode.GetNode(6);
    }
    
    // retorna a linha corrente do grid para a folha do no do form
    for ( i=0; i< fg.Rows; i++ ) 
    {
        if ( fg.TextMatrix(i, 9) == currFormID )
        {
            fg.Row = i;
            break;
        }    
    }
}

/********************************************************************
Remove detalhes do form corrente

Parametro:
grid            - referencia ao grid
currFormNode    - referencia ao no do form corrente

Retorno:
nenhum
********************************************************************/
function removeFormDetails(grid, currFormNode)
{
    var lastChildNode = currFormNode.GetNode(3);
    
    while (lastChildNode != null)
    {
        lastChildNode.RemoveNode();
        lastChildNode = currFormNode.GetNode(3);
    }
}

/********************************************************************
Insere detalhes do form corrente

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function insertFormDetails(grid, dso)
{
    // referencia ao no do form corrente
    var currFormNode;
    currFormNode = grid.GetNode(grid.Row);

    // trap dso zerado
    if ( dso.recordset.BOF && dso.recordset.EOF )
        return null;
            
    insertFormContexts(grid, dso, currFormNode);
    insertContextSubForms(grid, dso, currFormNode);
    insertSubFormsFeatures(grid, dso, currFormNode);
    insertStateMach(grid, dso, currFormNode);
    insertStateMachTransition(grid, dso, currFormNode);
}

/********************************************************************
Insere contextos do form corrente

Parametro:
grid            - referencia ao grid
dso             - referencia ao dso
currFormNode    - referencia ao no do form corrente

Retorno:
nenhum
********************************************************************/
function insertFormContexts(grid, dso, currFormNode)
{
    // contextos    
    // move o cursor do dso para o primeiro registro
    dso.recordset.MoveFirst();
    
    // filtra o dso para todos os modulos associados ao no raiz
    dso.recordset.setFilter('RecursoMaeID=' + currFormNode.Key + ' ' + 'AND Nivel = 5');
    
    // loop em todos os modulos, preenchendo o grid com os modulos
    while (!dso.recordset.EOF)
    {
        nodeRef = addNodeInGrid(grid, dso.recordset['Recurso'].value,
                                currFormNode,
                                dso.recordset['RecursoID'].value);

        if ( nodeRef != null )
            addLeafInNode( grid, nodeRef, dso );
        
        dso.recordset.MoveNext();
    }
}

/********************************************************************
Adiciona os nos dos sub forms ao contexto do form corrente

Parametro:
grid            - referencia ao grid
dso             - referencia ao dso
currFormNode    - referencia ao no do form corrente

Retorno:
nenhum
********************************************************************/
function insertContextSubForms(grid, dso, currFormNode)
{
    var rootNode;
    var modNode; 
    var motherRecursoID;    
    
    // referencia ao no do form que funciona como raiz da insercao
    rootNode = currFormNode;
    // pega referencia ao primeiro contexto do form raiz
    modNode = rootNode.GetNode(2);

    while (modNode != null)
    {    
        // obtem o id da mae que e o contexto
        motherRecursoID = parseFloat(modNode.Key);
            
        // move o cursor do dso para o primeiro registro
        dso.recordset.MoveFirst();
            
        // filtra o dso para todos os sub forms associados ao contexto corrente
        dso.recordset.setFilter('RecursoMaeID=' + motherRecursoID.toString() + ' ' + 'AND Nivel = 6');
            
        // loop em todos os sub forms do contexto corrente,
        // preenchendo o grid com os sub forms do contexto corrente
        while (!dso.recordset.EOF)
        {
            nodeRef = addNodeInGrid(grid, dso.recordset['Recurso'].value,
                                modNode,
                                dso.recordset['RecursoID'].value);

            if ( nodeRef != null )
                addLeafInNode( grid, nodeRef, dso );
                
            dso.recordset.MoveNext();
        }
            
        // desfiltra o dso
        dso.recordset.setFilter('');
        
        modNode = modNode.GetNode(6);
    }    
}

/********************************************************************
Adiciona as features dos sub forms do contexto do form corrente ao grid

Parametro:
grid            - referencia ao grid
dso             - referencia ao dso
currFormNode    - referencia ao no do form corrente

Retorno:
nenhum
********************************************************************/
function insertSubFormsFeatures(grid, dso, currFormNode)
{
    var rootNode;
    var modNode; 
    var subModNode;
    var motherRecursoID;
    var grandMotherRecursoID;    
    
    // referencia ao no do form que funciona como raiz da insercao
    rootNode = currFormNode;
    // pega referencia ao primeiro contexto do form raiz
    modNode = rootNode.GetNode(2);
    
    while (modNode != null)
    {
        grandMotherRecursoID = parseFloat(modNode.Key);
        
        // referencia ao primeiro sub form do contexto
        subModNode = modNode.GetNode(2);
        
        while (subModNode != null)
        {   
            // obtem o id da mae
            motherRecursoID = parseFloat(subModNode.Key);
                
            // move o cursor do dso para o primeiro registro
            dso.recordset.MoveFirst();
                
            // filtra o dso para todos os filtros associados ao sub form corrente
            dso.recordset.setFilter('RecursoMaeID=' + motherRecursoID.toString() + ' ' + 
                                   'AND ContextoID=' + grandMotherRecursoID.toString() + ' ' +
                                   'AND Nivel = 7');
                
            // loop em todos os filtros do sub form corrente,
            // preenchendo o grid com os filtros do sub form corrente
            while (!dso.recordset.EOF)
            {
                nodeRef = addNodeInGrid(grid, dso.recordset['Recurso'].value,
                                    subModNode,
                                    dso.recordset['RecursoID'].value);

                if ( nodeRef != null )
                    addLeafInNode( grid, nodeRef, dso );
                    
                dso.recordset.MoveNext();
            }
                
            // desfiltra o dso
            dso.recordset.setFilter('');
        
            subModNode = subModNode.GetNode(6);
        }
        
        modNode = modNode.GetNode(6);
    }
}

/********************************************************************
Adiciona as maquinas de estado dos sub forms do contexto do form
corrente ao grid

Parametro:
grid            - referencia ao grid
dso             - referencia ao dso
currFormNode    - referencia ao no do form corrente

Retorno:
nenhum
********************************************************************/
function insertStateMach(grid, dso, currFormNode)
{
    var rootNode;
    var modNode; 
    var subModNode;
    var motherRecursoID;
    var grandMotherRecursoID;    
    var lastRecursoID = 0;
        
    // referencia ao no do form que funciona como raiz da insercao
    rootNode = currFormNode;
    // pega referencia ao primeiro contexto do form raiz
    modNode = rootNode.GetNode(2);
    
    while (modNode != null)
    {
        grandMotherRecursoID = parseFloat(modNode.Key);
        
        // referencia ao primeiro sub form do contexto
        subModNode = modNode.GetNode(2);
        
        while (subModNode != null)
        {   
            // obtem o id da mae
            motherRecursoID = parseFloat(subModNode.Key);
                
            // move o cursor do dso para o primeiro registro
            dso.recordset.MoveFirst();
                
            // filtra o dso para todos os estados associados ao sub form corrente
            dso.recordset.setFilter('RecursoMaeID=' + motherRecursoID.toString() + ' ' +
                                   'AND ContextoID=' + grandMotherRecursoID.toString() + ' ' +
                                   'AND Nivel = 8');
                
            // loop em todos os estados do sub form corrente,
            // preenchendo o grid com os estados do sub form corrente
            while (!dso.recordset.EOF)
            {
                nodeRef = addNodeInGrid(grid, dso.recordset['Recurso'].value,
                                    subModNode,
                                    dso.recordset['RecursoID'].value);

                if ( nodeRef != null )
                    addLeafInNode( grid, nodeRef, dso );
                
                lastRecursoID = dso.recordset['RecursoID'].value;    
                    
                while ( !dso.recordset.EOF )    
                {
                    dso.recordset.MoveNext();
                    
                    if (dso.recordset.EOF)
                        break;
                    
                    if ( lastRecursoID != dso.recordset['RecursoID'].value )
                        break;
                }
            }
                
            // desfiltra o dso
            dso.recordset.setFilter('');
        
            subModNode = subModNode.GetNode(6);
        }
        
        modNode = modNode.GetNode(6);
    }
}

/********************************************************************
Adiciona as transicoes de estado do estado corrente da maquinas de estado
dos sub forms do contexto do form
corrente ao grid

Parametro:
grid            - referencia ao grid
dso             - referencia ao dso
currFormNode    - referencia ao no do form corrente

Retorno:
nenhum
********************************************************************/
function insertStateMachTransition(grid, dso, currFormNode)
{
    var rootNode;
    var modNode; 
    var subModNode;
    var infModMode;
    var infMotherRecursoID;
    var motherRecursoID;
    var grandMotherRecursoID;    
    var tempLeafRow;
        
    // referencia ao no do form que funciona como raiz da insercao
    rootNode = currFormNode;
    // pega referencia ao primeiro contexto do form raiz
    modNode = rootNode.GetNode(2);
    
    // loop nos contextos
    while (modNode != null)
    {
        grandMotherRecursoID = parseFloat(modNode.Key);
            
        // referencia ao primeiro sub form do contexto
        subModNode = modNode.GetNode(2);
            
        // loop nos subforms
        while (subModNode != null)
        {   
            // obtem o id da mae
            motherRecursoID = parseFloat(subModNode.Key);
            
            // referencia ao primeiro estado do subform
            // que vem apos os filtros
            infModNode = subModNode.GetNode(2);
                
            if ( infModNode == null )
            {
                subModNode = subModNode.GetNode(6);
                continue;
            }    
            
            tempLeafRow = (infModNode.Row + 1);
            // loop ate encontrar o primeiro estado
            while ( parseInt(grid.TextMatrix(tempLeafRow, 8), 10) != 8 )
            {
                infModNode = infModNode.GetNode(6);
                
                if ( infModNode == null )
                    break;
                
                tempLeafRow = (infModNode.Row + 1);
            }
            
            // loop nos estados
            while (infModNode != null)
            {
                // obtem o id da mae
                infMotherRecursoID = parseFloat(infModNode.Key);
                    
                // move o cursor do dso para o primeiro registro
                dso.recordset.MoveFirst();
                        
                // filtra o dso para todos as transicoes associadas
                // ao estado corrente
                dso.recordset.setFilter('RecursoMaeID=' + motherRecursoID.toString() + ' ' +
                                       'AND ContextoID=' + grandMotherRecursoID.toString() + ' ' +
                                       'AND RecursoID=' + infMotherRecursoID.toString() + ' ' +
                                       'AND Nivel = 8');
                        
                // loop em todos as transicoes do estado corrente,
                // preenchendo o grid com as transicoes do estado corrente
                while (!dso.recordset.EOF)
                {
                    nodeRef = addNodeInGrid(grid, dso.recordset['EstadoPara'].value,
                                        infModNode,
                                        dso.recordset['EstadoParaID'].value);

                    if ( nodeRef != null )
                        addLeafInNode( grid, nodeRef, dso );
                        
                    dso.recordset.MoveNext();
                }
                        
                // desfiltra o dso
                dso.recordset.setFilter('');
                
                infModNode = infModNode.GetNode(6);
            }
            
            subModNode = subModNode.GetNode(6);
        }
            
        modNode = modNode.GetNode(6);
    }
}

// EVENTOS DO GRID **************************************************

var __OldRow = 0;
var __NewRow = 0;
// Controla uso keyboard/mouse -> keyboard: 'K', mouse: 'M' 
var __KeybMouse = null;

/********************************************************************
Usuario deu um duplo clique em uma celula do grid

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_md_DblClick()
{
	if ( fg.Row < 1 )
		return true;
		
    var node = fg.GetNode();
    
	if ( btnDetalha.disabled )	
	{
		try
		{
			node.Expanded = !node.Expanded;
		}	
		catch(e)
		{
			;	
		}	
	}	
	else	
		btnControlBar_Clicked(btnDetalha);	
}

/********************************************************************
Antes de mudar a linha e/ou coluna do grid,
por acao do usuario ou por codigo

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_md_BeforeRowColChange()
{
    if ( glb_fgChangingRowColByCode )
        return true;
        
    __OldRow = fg.Row;    
}

/********************************************************************
Mudou a linha e/ou coluna do grid,
por acao do usuario ou por codigo

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_md_RowColChange()
{
    if ( glb_fgChangingRowColByCode )
        return true;
        
    // Em 29/05/2002 - garante nao mover o foco para a
    // primeira linha, na primeira interacao do usuario
    // com o grid
    if ( (fg.Rows > 2) && (fg.Row == 1) )    
        fg.Row = 2;
}

/********************************************************************
Depois que mudou a linha e/ou coluna do grid, por acao do usuario ou por codigo

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_md_AfterRowColChange()
{
    if ( glb_fgChangingRowColByCode )
        return true;
    
    var direction = -1;
    
    __NewRow = fg.Row;
    
    if ( __NewRow >= (__OldRow + 1) )
    {
        direction = 1;
    }    
    else if ( __NewRow <= (__OldRow - 1) )
    {
        direction = -1;    
    }
/**********************    
    else
    {
        glb_fgChangingRowColByCode = true;
        fg.Row = __OldRow;        
        glb_fgChangingRowColByCode = false;

        return true;    
    }    
*********************/
    
    fg.Redraw = 0;
    
    // forca o grid a assinalar sempre a folha do no
    if ( fg.TextMatrix(__NewRow, 1) == '' )    
    {
        glb_fgChangingRowColByCode = true;
        
        if ( direction == 1 )
        {
            if ( (fg.Row + 1) < fg.Rows )
                fg.Row = fg.Row + 1;
        }
        
        if ( direction == -1 )
        {
            if ( __KeybMouse == 'M' )        
            {
                if ( (fg.Row - 1) > 1 )
                    fg.Row = fg.Row + 1;
            }
            else if ( __KeybMouse == 'K' )
            {
                if ( (fg.Row - 1) > 1 )
                    fg.Row = fg.Row - 1;
            }        
        }
        
        if ( (fg.Row == 1) && (fg.Rows > 2) )
            fg.Row = 2;

        glb_fgChangingRowColByCode = false;
    }
    
    fg.Redraw = 2;
        
    enableDisableInterfaceControls();    
}

function fg_md_KeyDown()
{
    __KeybMouse = 'K';
}

/********************************************************************
Mudou o conteudo de uma celula

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_md_AfterEdit()
{
    // verifica se a linha e um leaf
    // se for e se o nivel (col 8) e 8 nao aceita edicao
    
    if ( parseInt(fg.TextMatrix(fg.Row, 8), 10) == 8 )
    {
        if ( fg.Col > 4 )
        {
            fg.TextMatrix(fg.Row, fg.Col) = 0;
            return true;
        }
    }
    
    // Tem pendencia
    glb_bPendent = true;
    
    // ajusta botoeira
    enableDisableInterfaceControls();
}

/********************************************************************
Botao mouse abaixado

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_md_MouseDown()
{
    __OldRow = fg.Row;
    
    __KeybMouse = 'M';
    
    // Em 05/06/2002 - garante nao mover o foco para linha impar
    if ( (fg.Row == 0) || (fg.Row == 1) || (fg.Row % 2 != 0) )
    {
        glb_fgChangingRowColByCode = true;

        if ( (fg.Row + 1) < fg.Rows ) 
		    fg.Row = fg.Row + 1;
		    
		glb_fgChangingRowColByCode = false;
		    
    }    
    
    // verifica se a linha e um leaf
    // se for e se o nivel (col 8) e 8 nao aceita edicao
    
	
    if ( parseInt(fg.TextMatrix(fg.Row, 8), 10) == 8 )
    {
        if ( fg.Col > 4 )
        {
            fg.TextMatrix(fg.Row, fg.Col) = 0;
            return true;
        }
    }
	enableDisableInterfaceControls();

}

// FINAL DE EVENTOS DO GRID *****************************************

// DSO DE EDICAO DA TABELA DE DIREITOS ******************************

/********************************************************************
Carrega os dados da tabela para edicao da mesma.
Estes dados sao um espelho dos dados atualmente carregados no grid

Parametro:
nFormID     - null se chama do carregamento do treeview ate form
            - valor do id do form selecionado, se chama do
            - carregamento dos componentes do form

Retorno:
nenhum
********************************************************************/
function loadDataToEdit(nFormID)
{
    var dso = dsoEditTable;
    setConnection(dso);
    
    if ( nFormID == null )
    {
        dso.SQL = 'SELECT Consultar1 AS C1,Consultar2 AS C2,Alterar1 AS A1,Alterar2 AS A2, ' +  
                  'Incluir AS I, Excluir1 AS E1,Excluir2 AS E2, RecDireitoID AS RegistroID ' +
                  'FROM Recursos_Direitos  WITH (NOLOCK) ' +
                  'WHERE PerfilID= ' + glb_nPerfilID.toString() + ' AND Nivel<=4 ' +
                  'ORDER BY Nivel, RecDireitoID';
    
        dso.ondatasetcomplete = loadDataToEditModSubModForm_DSC;
    }    
    else
    {
        dso.SQL = 'SELECT Consultar1 AS C1,Consultar2 AS C2,Alterar1 AS A1,Alterar2 AS A2, ' + 
                  'Incluir AS I, Excluir1 AS E1,Excluir2 AS E2, RecDireitoID AS RegistroID ' +  
                  'FROM Recursos_Direitos  WITH (NOLOCK) ' +
                  'WHERE PerfilID= ' + glb_nPerfilID.toString() + ' AND Nivel<=4 ' + 
                  'UNION ALL SELECT DISTINCT a.Consultar1 AS C1, a.Consultar2 AS C2, ' + 
                  'a.Alterar1 AS A1, a.Alterar2 AS A2, ' + 
                  'a.Incluir AS I, a.Excluir1 AS E1,a.Excluir2 AS E2,a.RecDireitoID AS RegistroID ' + 
                  'FROM Recursos_Direitos a  WITH (NOLOCK) ' +
                  'WHERE a.PerfilID= ' + glb_nPerfilID.toString() + ' AND a.Nivel=5 ' + 
                  'AND a.RecursoMaeID=' + nFormID.toString() + ' ' + 
                  'UNION ALL SELECT DISTINCT b.Consultar1 AS C1, b.Consultar2 AS C2, ' + 
                  'b.Alterar1 AS A1, b.Alterar2 AS A2, ' +   
                  'b.Incluir AS I, b.Excluir1 AS E1,b.Excluir2 AS E2,b.RecDireitoID AS RegistroID ' + 
                  'FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b  WITH (NOLOCK) ' +
                  'WHERE a.PerfilID= ' + glb_nPerfilID.toString() + ' AND a.Nivel=5 ' +  
                  'AND a.RecursoMaeID=' + nFormID.toString() + ' AND a.RecursoID=b.RecursoMaeID AND b.PerfilID= ' + glb_nPerfilID.toString() + ' ' +  
                  'AND b.Nivel=6 ' + 
                  'UNION ALL SELECT DISTINCT c.Consultar1 AS C1, c.Consultar2 AS C2, ' + 
                  'c.Alterar1 AS A1, c.Alterar2 AS A2, ' + 
                  'c.Incluir AS I, c.Excluir1 AS E1,c.Excluir2 AS E2,c.RecDireitoID AS RegistroID ' +  
                  'FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b WITH (NOLOCK) , Recursos_Direitos c  WITH (NOLOCK) ' +
                  'WHERE a.PerfilID= ' + glb_nPerfilID.toString() + ' AND a.Nivel=5 ' + 
                  'AND a.RecursoMaeID=' + nFormID.toString() + ' AND a.RecursoID=b.RecursoMaeID AND b.PerfilID= ' + glb_nPerfilID.toString() + ' ' + 
                  'AND b.Nivel=6 AND b.RecursoID=c.RecursoMaeID AND c.PerfilID= ' + glb_nPerfilID.toString() + ' ' + 
                  'AND c.Nivel=7 ' +
                  'UNION ALL SELECT DISTINCT c.Consultar1 AS C1, c.Consultar2 AS C2, ' + 
                  'c.Alterar1 AS A1, c.Alterar2 AS A2, ' + 
                  'c.Incluir AS I, c.Excluir1 AS E1,c.Excluir2 AS E2,c.RecDireitoID AS RegistroID ' +  
                  'FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b WITH (NOLOCK) , Recursos_Direitos c  WITH (NOLOCK) ' +
                  'WHERE a.PerfilID= ' + glb_nPerfilID.toString() + ' AND a.Nivel=5 ' + 
                  'AND a.RecursoMaeID=' + nFormID.toString() + ' AND a.RecursoID=b.RecursoMaeID AND b.PerfilID= ' + glb_nPerfilID.toString() + ' ' + 
                  'AND b.Nivel=6 AND b.RecursoID=c.RecursoMaeID AND c.PerfilID= ' + glb_nPerfilID.toString() + ' ' + 
                  'AND c.Nivel=8';
        
        dso.ondatasetcomplete = loadDataToEditFormDetail_DSC;
    }    
        
    dso.refresh();
}

/********************************************************************
Retorno do servidor do carregamento dos dados da tabela para edicao da mesma.
Estes dados sao um espelho dos dados atualmente carregados no grid
Ocorre quando carrega e monta os componentes do form no treeview

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function loadDataToEditModSubModForm_DSC()
{
    // esta funcao destrava o html contido na janela modal
    lockControlsInModalWin(false);    
    
    // se o perfil corrente tem direitos
    // esta carregando direitos no carregamento do form
    // logo encerra o carregamento do form aqui
    if ( glb_bProfileWithOutRights == false )
    {
        // destrava combo de perfis se for o caso
        if ( selPerfis.length >  0 )
            selPerfis.disabled = false;
            
        // coloca foco no campo apropriado
        if ( fg.disabled == false )
            fg.focus();
    }
    
    enableDisableInterfaceControls();
}    

/********************************************************************
Retorno do servidor do carregamento dos dados da tabela para edicao da mesma.
Estes dados sao um espelho dos dados atualmente carregados no grid
Ocorre quando carrega e monta o treeview ate form

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function loadDataToEditFormDetail_DSC()
{
    // esta funcao destrava o html contido na janela modal
    lockControlsInModalWin(false);    
    
    if ( fg.disabled == false )
        fg.focus();
    
    enableDisableInterfaceControls();
}

/********************************************************************
Salva os dados do treeview na tabela do servidor.

Parametro:
grid        - referencia ao grid
dso         - referencia ao dso de edicao de dados

Retorno:
nenhum

O grid mantem os dados necessarios a manipulacao dos direitos.
    Cada registro tem sempre duas linhas uma de label e outra de direitos.
    Desta forma o grid tem as seguintes colunas:
    0       - label do componente em no
            - label dos direitos em folha
    1 a 7   - bits de direito
    8       - nivel do componente ( de 1 a 7)
    9       - id do componente
    10      - id da mae do componente
    11      - id do contexto (para features (filtros, botoes e relatorios))
    12      - id do registro na tabela de direitos do perfil
    13      - id do estado para
    14      - linha que nao deve ser gravada. 1 nao grava, qualquer outro
              valor ou null grava
********************************************************************/
function saveEdition(grid, dso)
{
    var i;
    
    // loop no grid
    // para cada valor, altera o dso
    
    // se dso nao tem registros, nao faz nada
    if ( dso.recordset.BOF && dso.recordset.EOF )
        return true;
    
    // Nenhuma pendencia
    glb_bPendent = false;
        
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);        
  
    //limpa o array, a string de parametros e datalen
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer=  0;
    glb_strPars = '';
    glb_nDataLen = 0;
  
    for ( i=1; i<grid.Rows; i++ )    
    {
        if ( grid.TextMatrix(i, 12) != null )
            if ( grid.TextMatrix(i, 12) != '' )
                if ( parseInt(grid.TextMatrix(i, 14), 10) != 1 )
                    updateGridLineInArray(grid, dso, i, grid.TextMatrix(i, 12));
    }
    
    //Adiciona �ltima string no array
    if (glb_nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = glb_strPars;
		
    //Salva dados do array no servidor
    if(glb_aSendDataToServer.length>0)
        sendDataToServer();    
    else
        dsoTreeViewComplete_DSC();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoSaveEdition.URL = SYS_ASPURLROOT + '/basico/recursos/serverside/salvaalteracao.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoSaveEdition.ondatasetcomplete = sendDataToServer_DSC;
			dsoSaveEdition.refresh();
		}
		else
		{			
			glb_OcorrenciasTimerInt = window.setInterval('saveEdition_DSC()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		glb_OcorrenciasTimerInt = window.setInterval('dsoTreeViewComplete_DSC()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

/********************************************************************
Adicona uma linha alterada no array, em funcao da correspondente
linha no grid

Parametro:
grid        - referencia ao grid
dso         - referencia ao dso de edicao de dados
line        - linha corrente no grid
registroID  - id do registro na linha corrente do grid

Retorno:
nenhum
********************************************************************/
function updateGridLineInArray(grid, dso, line, registroID)
{
    var i;
    var bGrava=false;
    var bUntil3Col=false;    
    var sFirstString = '';
    
    // move o dso para o primeiro registro
    dso.recordset.MoveFirst();
    
    // localiza o registro no dso
    
    dso.recordset.Find('RegistroID', registroID);

    // verifica se o objeto n�o est� no EOF
    if ( dso.recordset.EOF )
        return null;
    
    // altera os campos do dso
    for(i=0; i<7; i++)
    {
        if ( ( ( grid.TextMatrix(line, (i+1)) == 0) && (dso.recordset.fields[i].value == true) ) ||
             ( (grid.TextMatrix(line, (i+1)) != 0)  && (dso.recordset.fields[i].value == false) ) )
        {
            bGrava = true;
        }                                
        
        // so altera colunas C1, C2, A1, A2 para estados
        if ( parseInt(grid.TextMatrix(line, 8), 10) == 8 )  
        {  
            if ( i == 3 )
            {
                bUntil3Col = true;
                break;
            }
        }
        
        if( (bGrava) && (i>3) )
            break;        
    } 
    
    if (bGrava)
    {           
        nBytesAcum=glb_strPars.length;
			
		if ((nBytesAcum >= glb_nMaxStringSize)||(glb_strPars == ''))
		{
			nBytesAcum = 0;
			
			if (glb_strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = glb_strPars;
				glb_strPars = '';
				glb_nDataLen = 0;
			}
			
			sFirstString = '?';
		}
		else
		    sFirstString = '&';
		
        glb_strPars += sFirstString + 'nRegistroID=' + escape(registroID);
        glb_strPars += '&sC1=' + escape(grid.TextMatrix(line, 1) != 0 ? 1 : 0);
        glb_strPars += '&sC2=' + escape(grid.TextMatrix(line, 2) != 0 ? 1 : 0);
        glb_strPars += '&sA1=' + escape(grid.TextMatrix(line, 3) != 0 ? 1 : 0);
        glb_strPars += '&sA2=' + escape(grid.TextMatrix(line, 4) != 0 ? 1 : 0);
        
        if(!bUntil3Col)
        {
            glb_strPars += '&sI=' + escape(grid.TextMatrix(line, 5) != 0 ? 1 : 0);
            glb_strPars += '&sE1=' + escape(grid.TextMatrix(line, 6) != 0 ? 1 : 0);
            glb_strPars += '&sE2=' + escape(grid.TextMatrix(line, 7) != 0 ? 1 : 0);
        }
        else
        {
            glb_strPars += '&sI=';
            glb_strPars += '&sE1=';
            glb_strPars += '&sE2=';
        }
        
        glb_nDataLen++;
    }
}

/********************************************************************
Retorno do servidor da funcao sendDataToServer
Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function saveEdition_DSC()
{
    if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
    dsoEditTable.refresh();
    
    // esta funcao destrava o html contido na janela modal
    lockControlsInModalWin(false);    
    
    // ajusta botoeira
    enableDisableInterfaceControls();
    
    if ( fg.disabled == false )
        fg.focus();
}

// FINAL DE DSO DE EDICAO DA TABELA DE DIREITOS *********************

/********************************************************************
Replica Direitos de um Recurso para todos os seus filhos, 
existentes no grid.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function replicaDireitoUmNivel()
{
    var currNode;
    var currLeafRow;

    glb_bPendent = true;

    currNode = fg.GetNode(fg.Row);
    if (currNode == null)
        return null;
        
    currLeafRow = currNode.Row + 1;    
    
    fg.Redraw = 0;
    replicaDireitos(fg, currNode, currLeafRow);
    fg.Redraw = 2;
    enableDisableInterfaceControls();
}

/********************************************************************
Replica Direitos de um Recurso para todos os seus descendentes, 
existentes no grid.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function replicaDireitoTodosNiveis()
{
    var i;
    var startNode;
    var startLeafRow;
    var startNivel;
    
    var currLeafRow;
    var currNivel;
        
    glb_bPendent = true;

    startNode = fg.GetNode(fg.Row);
    if (startNode == null)
        return null;
        
    startLeafRow = startNode.Row + 1;    
    startNivel = fg.TextMatrix(startLeafRow, 8 );
    
    fg.Redraw = 0;
    
    currLeafRow = startLeafRow + 2;
    currNivel = fg.TextMatrix(currLeafRow, 8 );
    
    while ( currNivel > startNivel )
    {
        for (i=1; i<=7 ;i++)
        {
            if ( (fg.TextMatrix(startLeafRow,i) == 0) || (fg.TextMatrix(startLeafRow,i).toUpperCase() == 'FALSE') )
                fg.TextMatrix(currLeafRow,i) = 0;
            else
                fg.TextMatrix(currLeafRow,i) = 1;
                
            if ( parseInt(fg.TextMatrix(currLeafRow, 8), 10) == 8 )
                    if ( i == 4 )    
                        break;    
        }

        currLeafRow += 2;
        
        if ( currLeafRow > (fg.Rows -1) )
            break;
            
        currNivel = fg.TextMatrix(currLeafRow, 8 );    
    }
    
    fg.Redraw = 2;
    enableDisableInterfaceControls();
}

/********************************************************************
Replica Direitos de um Recurso para todos os seus filhos imediatos, 
existentes no grid.

Parametro:
grid        - referencia ao grid
currNode    - referencia ao no corrente no grid
currLeafRow - referencia a folha corrente no grid

Retorno:
nenhum
********************************************************************/
function replicaDireitos(grid, currNode, currLeafRow)
{
    var numChildren; 
    var currChild;
    var currLeafChildRow;
    var i,j;
    
    currChild = currNode.GetNode(2);
    if (currChild == null)
        return null;
        
    currLeafChildRow = currChild.Row + 1;

    numChildren = currNode.Children; 

    for (i=0; i<numChildren ;i++)
    {
        if (currChild != null)
        {
            currLeafChildRow = currChild.Row + 1;
                
            for (j=1; j<=7 ;j++)
            {
                if ( (grid.TextMatrix(currLeafRow,j) == 0) || (grid.TextMatrix(currLeafRow,j).toUpperCase() == 'FALSE') )
                    grid.TextMatrix(currLeafChildRow,j) = 0;
                else
                    grid.TextMatrix(currLeafChildRow,j) = 1;
                    
                if ( parseInt(grid.TextMatrix(currLeafChildRow, 8), 10) == 8 )
                    if ( j == 4 )    
                        break;
            }
        }    
        else
            break;
        // Pega o proximo no filho
        currChild = currChild.GetNode(6);
    }
}
