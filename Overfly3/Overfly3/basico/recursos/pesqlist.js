/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form recursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
var dsoPropsPL = new CDatatransport('dsoPropsPL');

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est',
                                 'Recurso', 'Fantasia',
                                 'Tipo');
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	showBtnsEspecControlBar('sup', true, [1, 1, 1, 1]);
	tipsBtnsEspecControlBar('sup', ['Documentos', 'Relatórios', 'Procedimento', 'Manutenção Salarial']);

	setupEspecBtnsControlBar('sup', 'HHHH');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	if(btnClicked == 4)
		openModalManutencaoSalarial();     
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
	if (idElement.toUpperCase() == 'MODALMANUTENCAOSALARIALHTML') {
		if (param1 == 'OK') {
			// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
			return 0;
		}
		else if (param1 == 'CANCEL') {
			// esta funcao fecha a janela modal e destrava a interface
			restoreInterfaceFromModal();
			// escreve na barra de status
			writeInStatusBar('child', 'cellMode', 'Detalhe');
			return 0;
		}
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

/*
 */
function openModalManutencaoSalarial() {
	var nWidth = 837;
	var nHeight = 514;

	// Carregar a modal
	htmlPath = SYS_ASPURLROOT + '/basico/recursos/modalpages/modalmanutencaosalarial.asp';
	showModalWin(htmlPath, new Array(nWidth, nHeight));
}
