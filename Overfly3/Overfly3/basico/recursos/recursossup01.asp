<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="recursossup01Html" name="recursossup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf                    
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/recursos/recursossup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/recursos/especificsup.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="recursossup01Body" name="recursossup01Body" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">        
        
        <p id="lblRecurso" name="lblRecurso" class="lblGeneral">Recurso</p>
        <input type="text" id="txtRecurso" name="txtRecurso" DATASRC="#dsoSup01" DATAFLD="Recurso" class="fldGeneral">
        <p id="lblRecursoFantasia" name="lblRecursoFantasia" class="lblGeneral">Fantasia</p>
        <input type="text" id="txtRecursoFantasia" name="txtRecursoFantasia" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RecursoFantasia">    
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoRecursoID"></select>    
    </div>
    
    <!-- Primeiro div secundario superior - Sistema -->    
    <div id="divSup02_01" name="divSup02_01" class="divExtern">
        <p id="lblHrSistema" name="lblHrSistema" class="lblGeneral">Sistema</p>
        <hr id="hrSistema" id="hrSistema" class="lblGeneral">
        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda Comum</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MoedaID"></select>    
        <p id="lblMoedaAlternativaID" name="lblMoedaAlternativaID" class="lblGeneral">Moeda Alternativa</p>
        <select id="selMoedaAlternativaID" name="selMoedaAlternativaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MoedaAlternativaID"></select>    
        <p id="lblIdiomaID" name="lblIdiomaID" class="lblGeneral">Idioma</p>
        <select id="selIdiomaID" name="selIdiomaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="IdiomaID"></select>
        <p id="lblIdiomaInternacionalID" name="lblIdiomaInternacionalID" class="lblGeneral">Idioma Internacional</p>
        <select id="selIdiomaInternacionalID" name="selIdiomaInternacionalID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="IdiomaInternacionalID"></select>
        <p id="lblLocalidadeID" name="lblLocalidadeID" class="lblGeneral">Localidade</p>
        <select id="selLocalidadeID" name="selLocalidadeID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="LocalidadeID"></select>
		<input type="image" id="btnFindLocalidade" name="btnFindLocalidade" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
        <p id="lblDiasAlerta" name="lblDiasAlerta" class="lblGeneral">Alerta</p>
        <input type="text" id="txtDiasAlerta" name="txtDiasAlerta" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DiasAlerta" title="Dias p/ alerta de paraliza��o do sistema">    
        <p id="lblDiasValidadeCredito" name="lblDiasValidadeCredito" class="lblGeneral">Cr�dito</p>
        <input type="text" id="txtDiasValidadeCredito" name="txtDiasValidadeCredito" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DiasValidadeCredito" title="Dias p/ validade das informa��es de cr�dito">
        <p id="lblNumeroEmails" name="lblNumeroEmails" class="lblGeneral" title="N�mero de emails marketing">Emails</p>
        <input type="text" id="txtNumeroEmails" name="txtNumeroEmails" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NumeroEmails" title="N�mero de emails marketing">

    </div>

    <!-- Segundo div secundario superior - Componente -->    
    <div id="divSup02_02" name="divSup02_02" class="divExtern">
        <p id="lblHrComponente" name="lblHrComponente" class="lblGeneral">Componente</p>
        <hr id="hrComponente" id="hrComponente" class="lblGeneral">
        <p id="lblClassificacaoID" name="lblClassificacaoID" class="lblGeneral">Classifica��o</p>
        <select id="selClassificacaoID" name="selClassificacaoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ClassificacaoID"></select>    
        <p id="lblRecursoMaeID" name="lblRecursoMaeID" class="lblGeneral">Recurso M�e</p>
        <select id="selRecursoMaeID" name="selRecursoMaeID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RecursoMaeID"></select>    
        <p id="lblFocoID" name="lblFocoID" class="lblGeneral">Foco</p>
        <select id="selFocoID" name="selFocoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FocoID"></select>
        <p id="lblRelevanciaID" name="lblRelevanciaID" class="lblGeneral">Relev�ncia</p>
        <select id="selRelevanciaID" name="selRelevanciaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RelevanciaID"></select>
        <p id="lblOrdemForm" name="lblOrdemForm" class="lblGeneral">Ordem</p>
        <input type="text" id="txtOrdemForm" name="txtOrdemForm" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem">
		<p id="lblTabela" name="lblTabela" class="lblGeneral">Tabela</p>
        <input type="text" id="txtTabela" name="txtTabela" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Tabela">
		<p id="lblFormName" name="lblFormName" class="lblGeneral">Form Name</p>
        <input type="text" id="txtFormName" name="txtFormName" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FormName">
        <p id="lblFrameStart" name="lblFrameStart" class="lblGeneral">Frame Start</p>
		<input type="text" id="txtFrameStart" DATASRC="#dsoSup01" DATAFLD="FrameStart" name="txtFrameStart" class="fldGeneral">
		<p id="lblArqStart" name="lblArqStart" class="lblGeneral">File Start</p>
		<input type="text" id="txtArqStart" name="txtArqStart" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ArqStart">
		<p id="lblNomeBotao" name="lblNomeBotao" class="lblGeneral">Nome Bot�o</p>
		<input type="text" id="txtNomeBotao" name="txtNomeBotao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NomeBotao">
		<p id="lblTipoID" name="lblTipoID" class="lblGeneral">Tipo ID</p>
        <select id="selTipoID" name="selTipoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoID"></select>
		<p id="lblFiltro1" name="lblFiltro1" class="lblGeneral">Filtro</p>
		<input type="text" id="txtFiltro1" name="txtFiltro1" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Filtro">
    </div>

    <!-- Terceiro div secundario superior - Contexto -->    
    <div id="divSup02_03" name="divSup02_03" class="divExtern">    
        <p id="lblHrContexto" name="lblHrContexto" class="lblGeneral">Contexto</p>
        <hr id="hrContexto" name="hrContexto" class="lblGeneral">
        <p id="lblRecursoMaeID2" name="lblRecursoMaeID2" class="lblGeneral">Recurso M�e</p>
        <select id="selRecursoMaeID2" name="selRecursoMaeID2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RecursoMaeID"></select>
        <p id="lblFocoID2" name="lblFocoID2" class="lblGeneral">Foco</p>
        <select id="selFocoID2" name="selFocoID2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FocoID"></select>
        <p id="lblRelevanciaID2" name="lblRelevanciaID2" class="lblGeneral">Relev�ncia</p>
        <select id="selRelevanciaID2" name="selRelevanciaID2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RelevanciaID"></select>
        <p id="lblOrdemContexto" name="lblOrdemContexto" class="lblGeneral">Ordem</p>
        <input type="text" id="txtOrdemContexto" name="txtOrdemContexto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem">
        <p id="lblEhDefaultContexto" name="lblEhDefaultContexto" class="lblGeneral">Default</p>
        <input type="checkbox" id="chkEhDefaultContexto" name="chkEhDefaultContexto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhDefault" title="� contexto default?">
        <p id="lblFiltroContexto" name="lblFiltroContexto" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltroContexto" name="txtFiltroContexto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Filtro">
    </div>

    <!-- Quarto div secundario superior - SubForm -->    
    <div id="divSup02_04" name="divSup02_04" class="divExtern">    
        <p id="lblHrSubForm" name="lblHrSubForm" class="lblGeneral">Sub-Form</p>
        <hr id="HrSubForm" name="HrSubForm" class="lblGeneral">
        <p id="lblFocoID4" name="lblFocoID4" class="lblGeneral">Foco</p>
        <select id="selFocoID4" name="selFocoID4" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FocoID"></select>
        <p id="lblRelevanciaID4" name="lblRelevanciaID4" class="lblGeneral">Relev�ncia</p>
        <select id="selRelevanciaID4" name="selRelevanciaID4" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RelevanciaID"></select>
        <p id="lblPrincipal4" name="lblPrincipal4" class="lblGeneral">Principal</p>
        <input type="checkbox" id="chkPrincipal4" name="chkPrincipal4" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Principal" title="� sub-form principal?">
    </div>

    <!-- Quinto div secundario superior - Feature -->    
    <div id="divSup02_05" name="divSup02_05" class="divExtern">        
        <p id="lblHrFeature" name="lblHrFeature" class="lblGeneral">Feature</p>
        <hr id="hrFeature" name="hrFeature" class="lblGeneral">
        <p id="lblClassificacaoID5" name="lblClassificacaoID5" class="lblGeneral">Classifica��o</p>
        <select id="selClassificacaoID5" name="selClassificacaoID5" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ClassificacaoID"></select>
        <p id="lblEhDetalhe" name="lblEhDetalhe" class="lblGeneral">Detalhe</p>
        <input type="checkbox" id="chkEhDetalhe" name="chkEhDetalhe" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhDetalhe" title="� relat�rio de detalhe?">
        <p id="lblFiltroFeature" name="lblFiltroFeature" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltroFeature" name="txtFiltroFeature" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Filtro">
    </div>

    <!-- Sexto div secundario superior - Perfil de Usuario -->    
    <div id="divSup02_06" name="divSup02_06" class="divExtern">        
        <p id="lblHrPerfilUsuario" name="lblHrPerfilUsuario" class="lblGeneral">Perfil de Usu�rio</p>
        <hr id="hrPerfilUsuario" name="hrPerfilUsuario" class="lblGeneral">
        <p  id="lblEhCargo" name="lblEhCargo" class="lblGeneral">Cargo</p>
        <input type="checkbox" id="chkEhCargo" name="chkEhCargo" DATASRC="#dsoSup01" DATAFLD="EhCargo" class="fldGeneral" title="� cargo?"></input>
        <p  id="lblEhCargoSuperior" name="lblEhCargoSuperior" class="lblGeneral">Superior</p>
        <input type="checkbox" id="chkEhCargoSuperior" name="chkEhCargoSuperior" DATASRC="#dsoSup01" DATAFLD="EhCargoSuperior" class="fldGeneral" title="� cargo superior?"></input>

        <p  id="lblPublica" name="lblPublica" class="lblGeneral">Publica</p>
        <input type="checkbox" id="chkPublica" name="chkPublica" DATASRC="#dsoSup01" DATAFLD="Publica" class="fldGeneral" title="Publica perfil na internet?"></input>
        <p  id="lblQualidade" name="lblQualidade" class="lblGeneral">Qualidade</p>
        <input type="checkbox" id="chkQualidade" name="chkQualidade" DATASRC="#dsoSup01" DATAFLD="Qualidade" class="fldGeneral" title="Afeta a qualidade do produto?"></input>
        
        
        <!--  Ocultado no JS  -->
        
<!--  <p id="lblTipoPerfilComercialID" name="lblTipoPerfilComercialID" class="lblGeneral" title="Tipo de perfil comercial para efeito de c�lculo de comiss�es de vendas">Comercial</p>
        <select id="selTipoPerfilComercialID" name="selTipoPerfilComercialID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoPerfilComercialID" title="Tipo de perfil comercial para efeito de c�lculo de comiss�es de vendas"></select>

        <p id="lblTipoPerfilComercialInadimplenciaID" name="lblTipoPerfilComercialInadimplenciaID" class="lblGeneral" title="Tipo de perfil comercial inadimplencia para efeito de c�lculo de inadimpl�ncia">Inadimp</p>
        <select id="selTipoPerfilComercialInadimplenciaID" name="selTipoPerfilComercialInadimplenciaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoPerfilComercialInadimplenciaID" title="Tipo de perfil comercial inadimplencia para efeito de c�lculo de inadimpl�ncia"></select>

        <p id="lblTipoPerfilComercialObsolescenciaID" name="lblTipoPerfilComercialObsolescenciaID" class="lblGeneral" title="Tipo de perfil comercial inadimplencia para efeito de c�lculo de obsolesc�ncia">Obsolesc</p>
        <select id="selTipoPerfilComercialObsolescenciaID" name="selTipoPerfilComercialObsolescenciaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoPerfilComercialObsolescenciaID" title="Tipo de perfil comercial inadimplencia para efeito de c�lculo de obsolesc�ncia"></select>
-->


        <p  id="lblRecebimentoArmazenar" name="lblRecebimentoArmazenar" class="lblGeneral">RA</p>
        <input type="checkbox" id="chkRecebimentoArmazenar" name="chkRecebimentoArmazenar" DATASRC="#dsoSup01" DATAFLD="RecebimentoArmazenar" class="fldGeneral" title="Recebe e-mail quando armazenar as compras?"></input>
        <p  id="lblRecebimentoConserto" name="lblRecebimentoConserto" class="lblGeneral">RC</p>
        <input type="checkbox" id="chkRecebimentoConserto" name="chkRecebimentoConserto" DATASRC="#dsoSup01" DATAFLD="RecebimentoConserto" class="fldGeneral" title="Recebe e-mail quando receber produtos para conserto?"></input>
        <p  id="lblRecebimentoDevolver" name="lblRecebimentoDevolver" class="lblGeneral">RD</p>
        <input type="checkbox" id="chkRecebimentoDevolver" name="chkRecebimentoDevolver" DATASRC="#dsoSup01" DATAFLD="RecebimentoDevolver" class="fldGeneral" title="Recebe e-mail quando receber produtos em devolu��o?"></input>
    </div>

    <!-- Setimo div secundario superior - Estado -->    
    <div id="divSup02_07" name="divSup02_07" class="divExtern">        
        <p id="lblHrEstado" name="lblHrEstado" class="lblGeneral">Estado</p>
        <hr id="hrEstado" name="hrEstado" class="lblGeneral">
        <p id="lblRecursoAbreviado" name="lblRecursoAbreviado" class="lblGeneral" LANGUAGE="javascript">Abrev</p>
        <input type="text" id="txtRecursoAbreviado" name="txtRecursoAbreviado" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RecursoAbreviado">
    </div>

</body>

</html>
