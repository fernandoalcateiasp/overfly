/********************************************************************
recursosinf01.js

Library javascript para o recursosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dso01JoinSup = new CDatatransport('dso01JoinSup');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dso01Grid = new CDatatransport('dso01Grid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb02Grid_01 = new CDatatransport('dsoCmb02Grid_01');
var dsoCmb03Grid_01 = new CDatatransport('dsoCmb03Grid_01');
var dso01GridLkp = new CDatatransport('dso01GridLkp');
var dso02GridLkp = new CDatatransport('dso02GridLkp');
var dso03GridLkp = new CDatatransport('dso03GridLkp');
var dso04GridLkp = new CDatatransport('dso04GridLkp');
var dsoParallelGrid = new CDatatransport('dsoParallelGrid');
var dso01PgGrid = new CDatatransport('dso01PgGrid');
var dsoStateMachineLkp = new CDatatransport('dsoStateMachineLkp');
var dsoFiltroInf = new CDatatransport('dsoFiltroInf');
var dsoCmbRel = new CDatatransport('dsoCmbRel');
var dsoCmbsPropAlt = new CDatatransport('dsoCmbsPropAlt');

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)


FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()


********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();
    
    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    
    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selEducacaoID', '1']]);
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 
    glb_aGridStaticCombos = [ [20010, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dso02GridLkp'   , ''             , ''],
                                       [0, 'dso03GridLkp'   , ''             , ''],
                                       [0, 'dso04GridLkp'   , ''             , '']]],
                              [20012, [[1, 'dsoCmb01Grid_01', 'Recurso', 'RecursoID']]],
                              [20014, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
                              [20016, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dsoStateMachineLkp', ''          , '']]],
                              [20017, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dsoStateMachineLkp', ''          , '']]],
                              [20020, [[0, 'dsoCmb01Grid_01', 'Competencia', 'CompetenciaID']]],
                              [20021, [[0, 'dsoCmb01Grid_01', 'Fantasia', 'PessoaID']]],
                              [20022, [[0, 'dsoCmb03Grid_01', 'Localidade', 'LocalidadeID'],
                                       [1, 'dsoCmb01Grid_01', '*VerbaID|Verba', 'VerbaID'],
									   [2, 'dsoCmb02Grid_01', 'Perfil', 'PerfilID']]],
							  [20284, [[0, 'dsoCmb01Grid_01', 'RecursoID', 'Fantasia'],
							  [20023, [[0, 'dsoCmb01Grid_01', 'Fantasia', 'RecursoID']]],
									   [6, 'dsoCmb02Grid_01', 'NivelSeguranca', 'NivelSegurancaID']]] ];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03',
                               'divInf01_04'],
                               [20008, 20009, 20019,
                               [20010, 20011, 20012, 20013, 20014, 20015, 20016, 20017, 20018, 20020, 20021, 20022, 20284, 20023]]);
        
    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    //@@ Ajustar os divs
    /*** Credito - Div secundario inferior divInf01_03 ***/
    adjustElementsInForm([['lblEducacaoID','selEducacaoID',18,1],
						  ['lblPercentualConclusao','txtPercentualConclusao',6,1],
                          ['lblExperienciaPerfil','txtExperienciaPerfil',5,1],
                          ['lblExperienciaPerfilInferior','txtExperienciaPerfilInferior',5,1]],null,'INF');

}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{

}

function fg_DblClick_Prg()
{
    // Se for pasta relacoes 
    // Captura o id da relacao na coluna zero do grid
    // Manda mensagem
    
    var empresa = getCurrEmpresaData();
    
    if ( (keepCurrFolder() == 20016) && (!fg.Editable) )
        sendJSCarrier(getHtmlId(), 'SHOWRELRECURSOS', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));       
    else if ( (keepCurrFolder() == 20017) && (!fg.Editable) )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESREC', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));       
    else if ( (keepCurrFolder() == 20011) && (!fg.Editable) )
        sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL', 
                  [fg.TextMatrix(keepCurrLineInGrid(), 0), null, null]);

}

function fg_ChangeEdit_Prg()
{

}
function fg_ValidateEdit_Prg()
{

}
function fg_BeforeRowColChange_Prg()
{

}

function fg_EnterCell_Prg()
{

}

function fg_MouseUp_Prg()
{

}

function fg_MouseDown_Prg()
{

}

function fg_BeforeEdit_Prg()
{

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    // Este form tem grids com paginacao
    // Atencao o grid da pasta 20015 (Transi��o de Estados)
    // nao tem paginacao e foi removido do array abaixo
    glb_aFoldersPaging = [20011, 20016, 20017];

    txtPercentualConclusao.setAttribute('minMax', new Array(0, 100), 1);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    if ( cmbID == 'selEducacaoID' )
		adjustLabelsCombos(false);
     
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div
    // Uso avancado, so roda uma vez
    if ( dsoCmbRel.recordset.Fields.Count == 0 )
    {
        glb_lastBtnClicked = btnClicked;
        var formID = window.top.formID;  // variavel global do browser filho
        // passa o id do form por parametro
        var strPars = new String();
        strPars = '?';
        strPars += 'formID=';
        strPars += escape(formID.toString());

        dsoCmbRel.URL = SYS_ASPURLROOT + '/serversidegenEx/tiposrelacoes.aspx'+strPars;
	    dsoCmbRel.ondatasetcomplete = dsoCmbRelComplete_DSC;
	    dsoCmbRel.refresh();
    }    
    else
	    // Mover esta funcao para os finais retornos de operacoes no
	    // banco de dados
	    finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{
    //@@
    
    showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    // pasta de Rel entre Recursos/Rel com Pessoas
    if ( (pastaID == 20016) || (pastaID == 20017) )
    {
        showBtnsEspecControlBar('inf', true, [1,1,1,0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar','Preencher combo','Detalhar Rela��o','']);
    }
    else if ( pastaID == 20011) // Recursos Filhos
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar','Detalhar Recurso','','']);
    }
    else if ( pastaID == 20013) // Direitos
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Configurar Direitos','','','']);
    }
    // Atributos de usuario
    else if (pastaID == 20014)
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Valores dos Atributos','','','']);
    }
	else if (pastaID != null)
    {
        showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    }

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
	    
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{
    //@@

    // Recursos Filhos
    if (folderID == 20011)
    {
        setupEspecBtnsControlBar('inf', 'HHDD');
    }
    // Pasta de Direitos
    else if ( folderID == 20013 )
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Transi��o de Estados 
    else if (folderID == 20015)
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
	// Educacao/Experiencia
    else if (folderID == 20019)
	{
        adjustLabelsCombos(true);
	}
    // Rel entre Recurso, Rel com Pessoas
    else if ( (folderID == 20016) || (folderID == 20017) )
    {
        // relacao nao pode ser incluida se o registro nao estiver ativo
	    nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
	    if ( nEstadoID != 2 )
	    {
	        var strBtns = currentBtnsCtrlBarString('inf');
            var aStrBtns = strBtns.split('');
            aStrBtns[2] = 'D';
            strBtns = aStrBtns.toString();
            re = /,/g;
            strBtns = strBtns.replace(re, '');
            adjustControlBarEx(window,'inf', strBtns);
        }
        
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDHD');
        else
            setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 20014) // Atributos de Usuario
	{
		if ( fg.Rows > 1 )
		{
			if (fg.Row > 0)
			    setupEspecBtnsControlBar('inf', 'HDDD');
        }    
        else    
			setupEspecBtnsControlBar('inf', 'HDDD');
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{
    var empresa = getCurrEmpresaData();
    
    // Rel entre Recursos, Rel com Pessoas
    if ( (folderID == 20016) || (folderID == 20017) )
        setupEspecBtnsControlBar('inf', 'DHDD');

    if (folderID == 20023) 
    {
        if (fg.TextMatrix(nLineInGrid, getColIndexByColKey(fg, 'EmpresaID')).length == 0)
            fg.TextMatrix(nLineInGrid, getColIndexByColKey(fg, 'EmpresaID')) = empresa[0];
    }         
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    var empresa = getCurrEmpresaData();
    
    // Usuario apertou o botao 2 - pasta de Rel Recs/Pes - 20016/20017
    if ( ( (keepCurrFolder() == 20016)||(keepCurrFolder() == 20017) ) 
        && (btnClicked == 2) )
        openModalRelacoes();

    // Configurar direitos
    if ( keepCurrFolder() == 20013 ) 
        openModalProfiles();                

    // Usuario apertou o botao 1 - pasta de Rel Recs/Pes/Recursos filhos - 20015/20016/20011
    if ( ( (keepCurrFolder() == 20016)||(keepCurrFolder() == 20017)||(keepCurrFolder() == 20011) ) 
        && (btnClicked == 1) )
    {
        loadModalOfPagingPesq();
    }

    if ( (keepCurrFolder() == 20011) && (btnClicked == 2) )
    {
        // Zera o argumento de pesquisa
        glb_sArgument = '';
        fg_DblClick_Prg();
    }
    else if ( (keepCurrFolder() == 20016) && (btnClicked == 3) )
        sendJSCarrier(getHtmlId(), 'SHOWRELRECURSOS', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
    else if ( (keepCurrFolder() == 20017) && (btnClicked == 2) )
    {
        // Zera o argumento de pesquisa
        glb_sArgument = '';
        fg_DblClick_Prg();
    }
    else if ( (keepCurrFolder() == 20017) && (btnClicked == 3) )
        sendJSCarrier(getHtmlId(), 'SHOWRELPESREC', new Array(empresa[0], fg.TextMatrix(fg.Row, 0)));
    // Valores de atributos
    else if ( (controlBar == 'INF') && (keepCurrFolder() == 20014) )
    {
		valoresDeAtributos();
    }    
    
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
	// Se for a confirma��o de inclus�o, seta o ID em -1 para que a trigger 
	// da tabela coloque o ID correto no registro.
	//if(controlBar == "INF" && btnClicked == "INFOK") {
	//	fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'RelacaoID')) = "-1";
	//}

    // Para prosseguir a automacao retornar null
    return null;
}
/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Janela de pesquisa para paging no grid de recursos filhos 20011
    if ( ((keepCurrFolder() == 20011)||(keepCurrFolder() == 20016)||(keepCurrFolder() == 20017)) && 
         (idElement.toUpperCase() == 'MODALOFPAGINGINFHTML') )
        execPesqInPaging(param1, param2);
    else if ( ( (keepCurrFolder() == 20016)||(keepCurrFolder() == 20017) ) 
            && (idElement.toUpperCase() == 'MODALRELACOESHTML') )
    {
        if ( param1 == 'OK' )                
        {
            // Escreve ID, Label e TipoRelacaoID no grid de relacoes
            writeSujObjInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    // Janela de Direitos    
    else if ( (keepCurrFolder() == 20013) 
            && (idElement.toUpperCase() == 'MODALPROFILESHTML') )
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            __btn_REFR('inf');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            __btn_REFR('inf');
            return 0;
        }
    }
    // Valores dos atributos
    if (idElement.toUpperCase() == 'MODALVALORESATRIBUTOSHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{

}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.

    // Inicializacao de variavel de grid em modo de paginacao
    if (folderID == 20011) // Recursos Filhos
        glb_scmbKeyPesqSel = 'aRecursoID';
        
    if ( (folderID == 20016)||(folderID == 20017) ) 
        glb_scmbKeyPesqSel = 'aRelacaoID';    

    if ( (folderID != 20016) && (folderID != 20017) )
        return true;

    var aCmbTipoRelacaoText = new Array();
    var aCmbTipoRelacaoValue = new Array();
    var sDefaultRel = '';
    var nRelacaoEntreID;
    var i = 0;
    
    // Preenche o combo de filtros com as relacoes possiveis
    if ( ( folderID == 20016) || (folderID == 20017) ) //Rel entre Recursos/Rel com Pessoas
    {
        cleanupSelInControlBar('inf',2);
        nCurrRegID = getCurrDataInControl('sup','txtRegistroID');
        nCurrTipoRegID = getCurrDataInControl('sup','selTipoRegistroID');
        
        if (folderID == 20016)
           nRelacaoEntreID = 131;
        else if (folderID == 20017)
           nRelacaoEntreID = 132;   
           
        if ( ! ((dsoCmbRel.recordset.BOF) && (dsoCmbRel.recordset.EOF)) )
        {
            dsoCmbRel.recordset.moveFirst();
            if (! dsoCmbRel.recordset.EOF )
            {
                while (!dsoCmbRel.recordset.EOF)
                {
                    if ( (dsoCmbRel.recordset['TipoPossivel'].value == nCurrTipoRegID) &&
                         (dsoCmbRel.recordset['RelacaoEntreID'].value == nRelacaoEntreID) )
                    {
                        if ( ascan(aCmbTipoRelacaoText,dsoCmbRel.recordset['NomePasta'].value,false) < 0 )
                        {
                            aCmbTipoRelacaoText[i] = dsoCmbRel.recordset['NomePasta'].value;
                            aCmbTipoRelacaoValue[i] = [dsoCmbRel.recordset['TipoRelacaoID'].value,
                                                       dsoCmbRel.recordset['Tipo'].value,
                                                       dsoCmbRel.recordset['Header'].value,
                                                       dsoCmbRel.recordset['RelacaoEntreID'].value];

                            if ( (dsoCmbRel.recordset['EhDefault'].value) && (dsoCmbRel.recordset['Tipo'].value == 'OBJETO') )
                                sDefaultRel = (aCmbTipoRelacaoValue[i]).toString();

                            i++;
                        }
                    }
                    dsoCmbRel.recordset.moveNext();
                }
            }
        }

        if (aCmbTipoRelacaoText.length > 0)
        {
            for (i=0; i<aCmbTipoRelacaoText.length; i++)
            {
                addOptionToSelInControlBar('inf', 2, aCmbTipoRelacaoText[i], (aCmbTipoRelacaoValue[i]).toString() );
            }
        }
        
        if (! selOptByValueOfSelInControlBar('inf', 2, sDefaultRel))
            if (aCmbTipoRelacaoValue.length > 0)
                selOptByValueOfSelInControlBar('inf', 2, (aCmbTipoRelacaoValue[0]).toString());
        
        return false;
    }
    
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{
    // este form usa o combo de filtros para outras finalidades
    // desta forma o combo deve ser preenchido aqui e nao
    // na funcao prgInterfaceInf
    if ( !((optValue == 20016) || (optValue == 20017)) )
        refillCmb2Inf(optValue);

    if ( (optValue == 20016) || (optValue == 20017) )
    {
        if ( dsoCmbRel.recordset.Fields.Count == 0 )
        {
            var formID = window.top.formID;  // variavel global do browser filho
            // passa o id do form por parametro
            var strPars = new String();
            strPars = '?';
            strPars += 'formID=';
            strPars += escape(formID.toString());

            dsoCmbRel.URL = SYS_ASPURLROOT + '/serversidegenEx/tiposrelacoes.aspx'+strPars;
	        dsoCmbRel.ondatasetcomplete = dsoCmbRelComplete_1_DSC;
	        dsoCmbRel.refresh();
            // Para a automacao
            return true;
        }
    
    }
    //@@ retornar null para prosseguir a automacao
    return null;
}

function dsoCmbRelComplete_1_DSC() {
    setCurrDSOByCmb1(keepCurrFolder(), true);
}
/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    // Pasta de Colaboradores / LOG Read Only
    if ((folderID == 20018) || (folderID == 20010))
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // As pastas de Relacoes so habilta o botao de alterar
    else if ( (folderID == 20016) || (folderID == 20017))
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // As pastas de Transicao de Estado e Recursos Filhos sao Ready Only 
    else if ( (folderID == 20011) || (folderID == 20015) || (folderID == 20013) )
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // As pastas de Relacoes so habilta o botao de alterar
    else if (folderID == 20284)
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else if (folderID == 20023) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}
/********************************************************************
Funcao criada pelo programador.
Retorno do servidor dos dados do combo de filtros, usado para relacoes.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoCmbRelComplete_DSC() {
    // ao final volta para automacao
    finalOfInfCascade(glb_lastBtnClicked);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Relacoes

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalRelacoes()
{
    var currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
    var aValue = currCmb2Data[1].split(',');
    var htmlPath;
    var strPars = new String();
    var nTipoRelID = aValue[0];
    var sTipoSujObj = aValue[1];
    var sHeader = aValue[2];
    var nSujObjID = getCurrDataInControl('sup', 'txtRegistroID');
    var nRelEntreID = aValue[3];
        
    // mandar os parametros para o servidor
    strPars = '?';
    strPars += 'nTipoRelID=' + escape(nTipoRelID);
    strPars += '&sTipoSujObj=' + escape(sTipoSujObj);
    strPars += '&sHeader=' + escape(sHeader);
    strPars += '&nSujObjID=' + escape(nSujObjID);
    strPars += '&nRelEntreID=' + escape(nRelEntreID);
       
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/recursos/modalpages/modalrelacoes.asp'+strPars;
    showModalWin(htmlPath, new Array(571,284));
}

/********************************************************************
Funcao criada pelo programador.
Escreve Sujeito/Objeto ID da Relacao vindo da janela modal de Relacoes.
Pasta de Relacoes - 20016/20017

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeSujObjInGrid(aSujObj)
{
    // o array aSujObj contem: fldID, Fantasia, TipoRelacaoID
    fg.Redraw = 0;

    // Escreve nos campos
    fg.TextMatrix(keepCurrLineInGrid(),2) = aSujObj[0];
    fg.TextMatrix(keepCurrLineInGrid(),4) = aSujObj[2];
    fg.TextMatrix(keepCurrLineInGrid(),5) = aSujObj[1];
    
    fg.Redraw = 2;
}

function valoresDeAtributos()
{
	var strPars = new String();
	
	var nRecursoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'RecursoID' + '\'' + '].value');
	
    strPars = '?nRecursoID=' + escape(nRecursoID);
    strPars += '&nFormID=' + escape(window.top.formID);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modalgen/modalvaloresatributos.asp'+strPars;
    
    showModalWin(htmlPath, new Array(680,468));
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
        setLabelOfControl(lblEducacaoID, dso01JoinSup.recordset['EducacaoID'].value);
    else
        setLabelOfControl(lblEducacaoID, selEducacaoID.value);
}

// FUNCOES DA JANELA DE PERFIS ======================================

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Perfis

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalProfiles()
{
    var currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
    //var aValue = currCmb2Data[1].split(',');
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 565;
    
    // mandar os parametros para o servidor
    strPars = '?';
    strPars += 'nWidth=' + escape(nWidth.toString());
    strPars += '&nHeight=' + escape(nHeight.toString());
    strPars += '&nPerfilID=' + escape(glb_registroID);
    strPars += '&sPerfilFantasia=' + escape(getCurrDataInControl('sup', 'txtRecursoFantasia'));
           
    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/basico/recursos/modalpages/modalprofiles.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

// FINAL DE FUNCOES DA JANELA DE PERFIS =============================
