/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Recursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
               '(SELECT COUNT(*) FROM _Emails WITH (NOLOCK)  WHERE (dtEnvio IS NULL)) AS TotalEmails, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas  WITH (NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Recursos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM Recursos  WITH (NOLOCK) ' +
               'WHERE ProprietarioID = ' + nID + ' ' +
               'ORDER BY RecursoID DESC';
    else
        sSQL = 'SELECT *, ' +
               '(SELECT COUNT(*) FROM _Emails  WITH (NOLOCK) WHERE (dtEnvio IS NULL)) AS TotalEmails, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas  WITH (NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Recursos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM Recursos  WITH (NOLOCK) ' +
               'WHERE RecursoID = ' + nID + ' ORDER BY RecursoID DESC';

    setConnection(dso);
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
          '(SELECT COUNT(*) FROM _Emails WHERE (dtEnvio IS NULL)) AS TotalEmails, ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM Recursos WHERE RecursoID = 0';
    
    dso.SQL = sql;          
}              
