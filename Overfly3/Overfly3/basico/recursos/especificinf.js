/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Recursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

var empresa = getCurrEmpresaData();

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    var currCmb2Data;
    var aValue;

    //@@
    if ( (folderID == 20008) || (folderID == 20009) || (folderID == 20019) ) // Prop/Altern ou Observacoes ou Eduacaoca/Experiencia
    {
        dso.SQL = 'SELECT a.RecursoID, a.ProprietarioID, a.AlternativoID, a.Observacoes, ' +
			'a.EducacaoID, a.PercentualConclusao, a.ExperienciaPerfil, a.ExperienciaPerfilInferior ' +
				'FROM Recursos a  WITH (NOLOCK) WHERE ' +
        sFiltro + 'a.RecursoID = '+idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a  WITH (NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20011) // Recursos Filhos
    {
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        if ((nTipoRegistroID == 1) || (nTipoRegistroID == 2) )
        {
            glb_currGridTable = 'Recursos';
            glb_aCmbKeyPesq = [['ID','aRecursoID'],['Recurso','aRecursoFantasia']];
        }
        else if( (nTipoRegistroID == 3) || (nTipoRegistroID == 4) )
        {
            glb_currGridTable = 'RelacoesRecursos';
            glb_aCmbKeyPesq = [['ID','NbRecursoID'],['Recurso','VbRecursoFantasia']];
        }
        
        glb_vOptParam1 = nTipoRegistroID;
        // Final da automacao =========================================================

        if ((nTipoRegistroID == 1) || (nTipoRegistroID == 2) )
        {
            // paginacao de grid
            return execPaging(dso, 'RecursoMaeID', idToFind, folderID);
        }
        else if (nTipoRegistroID == 3)
        {
            // paginacao de grid
            return execPaging(dso, 'ObjetoID', idToFind, folderID);
        }
        else if (nTipoRegistroID == 4)
        {
            // paginacao de grid
            return execPaging(dso, 'ObjetoID', idToFind, folderID);
        }
    }
    else if (folderID == 20012) // Wizard
    {
        dso.SQL = 'SELECT a.* FROM Recursos_Wizards a  WITH (NOLOCK) WHERE ' +
        sFiltro + 'a.ContextoID = '+idToFind + ' ' +
        'ORDER BY a.Ordem';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20013) // Direitos
    {
        dso.SQL = 'SELECT DISTINCT a.RecursoID AS RecursoID,b.Recurso,a.Consultar1 AS C1,a.Consultar2 AS C2,a.Alterar1 AS A1,a.Alterar2 AS A2, ' +
                  'a.Incluir AS I,a.Excluir1 AS E1,a.Excluir2 AS E2 ' +
                  'FROM Recursos_Direitos a WITH (NOLOCK) , Recursos b  WITH (NOLOCK) ' +
                  'WHERE  ' + sFiltro + ' a.PerfilID= ' + idToFind + ' ' +
                  'AND a.Nivel=1 AND a.RecursoID=b.RecursoID AND (a.Consultar1=1 OR a.Consultar2=1) ' +
                  'UNION ALL SELECT DISTINCT b.RecursoID AS RecursoID,(' + '\'' + '  ' + '\'' + ' + c.Recurso) AS Recurso,b.Consultar1 AS C1,b.Consultar2 AS C2,b.Alterar1 AS A1,b.Alterar2 AS A2, ' +
                  'b.Incluir AS I,b.Excluir1 AS E1,b.Excluir2 AS E2 ' +
                  'FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b WITH (NOLOCK) , Recursos c  WITH (NOLOCK) ' +
                  'WHERE  ' + sFiltro + ' a.PerfilID= ' + idToFind + ' ' +
                  'AND a.Nivel=1 AND (a.Consultar1=1 OR a.Consultar2=1) ' +
                  'AND a.RecursoID=b.RecursoMaeID AND b.PerfilID= ' + idToFind + ' ' +
                  'AND b.Nivel=2 AND b.RecursoID=c.RecursoID AND (b.Consultar1=1 OR b.Consultar2=1) ' +
                  'UNION ALL SELECT DISTINCT c.RecursoID AS RecursoID,(' + '\'' + '    ' + '\'' + ' + d.Recurso) AS Recurso,c.Consultar1 AS C1,c.Consultar2 AS C2,c.Alterar1 AS A1,c.Alterar2 AS A2, ' +
                  'c.Incluir AS I,c.Excluir1 AS E1,c.Excluir2 AS E2 ' +
                  'FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b WITH (NOLOCK) , Recursos_Direitos c WITH (NOLOCK) , Recursos d WITH (NOLOCK)  ' +
                  'WHERE  ' + sFiltro + ' a.PerfilID= ' + idToFind + ' ' +
                  'AND a.Nivel=1 AND (a.Consultar1=1 OR a.Consultar2=1) ' +
                  'AND a.RecursoID=b.RecursoMaeID AND b.PerfilID= ' + idToFind + ' ' +
                  'AND b.Nivel=2 AND (b.Consultar1=1 OR b.Consultar2=1) ' +
                  'AND b.RecursoID=c.RecursoMaeID AND c.PerfilID= ' + idToFind + ' ' +
                  'AND c.Nivel=3 AND c.RecursoID=d.RecursoID AND (c.Consultar1=1 OR c.Consultar2=1) ' +
                  'UNION ALL SELECT DISTINCT d.RecursoID AS RecursoID,(' + '\'' + '      ' + '\'' + ' + e.Recurso) AS Recurso,d.Consultar1 AS C1,d.Consultar2 AS C2,d.Alterar1 AS A1,d.Alterar2 AS A2, ' +
                  'd.Incluir AS I,d.Excluir1 AS E1,d.Excluir2 AS E2 ' +
                  'FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b WITH (NOLOCK) , Recursos_Direitos c WITH (NOLOCK) , Recursos_Direitos d WITH (NOLOCK) , Recursos e  WITH (NOLOCK) ' +
                  'WHERE  ' + sFiltro + ' a.PerfilID= ' + idToFind + ' ' +
                  'AND a.Nivel=1 AND (a.Consultar1=1 OR a.Consultar2=1) ' +
                  'AND a.RecursoID=b.RecursoMaeID AND b.PerfilID= ' + idToFind + ' ' +
                  'AND b.Nivel=2 AND (b.Consultar1=1 OR b.Consultar2=1) ' +
                  'AND b.RecursoID=c.RecursoMaeID AND c.PerfilID= ' + idToFind + ' ' +
                  'AND c.Nivel=3 AND (c.Consultar1=1 OR c.Consultar2=1) ' +
                  'AND c.RecursoID=d.RecursoMaeID AND d.PerfilID= ' + idToFind + ' ' +
                  'AND d.Nivel=4 AND d.RecursoID=e.RecursoID AND (d.Consultar1=1 OR d.Consultar2=1) ' +
                  'ORDER BY RecursoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20014) // Atributos de Usuario
    {
        dso.SQL = 'SELECT a.* FROM Recursos_Atributos a  WITH (NOLOCK) WHERE ' +
        sFiltro + 'a.PerfilID = '+ idToFind + 
			'ORDER BY (SELECT Ordem FROM TiposAuxiliares_Itens WITH (NOLOCK)  WHERE ItemID=a.AtributoID)';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20020) // Habilidades/Treinamentos
    {
        dso.SQL = 'SELECT a.* FROM Recursos_Competencias a  WITH (NOLOCK) WHERE ' +
        sFiltro + 'a.RecursoID = '+ idToFind + 
			'ORDER BY (SELECT b.TipoCompetenciaID FROM Competencias b WITH (NOLOCK)  WHERE (a.CompetenciaID = b.CompetenciaID)), ' +
				'(SELECT b.Competencia FROM Competencias b  WITH (NOLOCK) WHERE (a.CompetenciaID = b.CompetenciaID))';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 20015) // Transi��o de Estados
    {
        dso.SQL = 'SELECT b.RecursoFantasia AS De, d.RecursoFantasia AS Para, c.EhDefault '+
        'FROM RelacoesRecursos a WITH (NOLOCK) , Recursos b WITH (NOLOCK) , RelacoesRecursos_Estados c WITH (NOLOCK) , Recursos d  WITH (NOLOCK) ' +
        'WHERE ' + sFiltro + 'a.ObjetoID = '+idToFind + 
        'AND a.SujeitoID = b.RecursoID AND a.RelacaoID = c.RelacaoID '+
        'AND c.RecursoID = d.RecursoID '+
        'ORDER BY b.Ordem,d.Ordem' ;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20016) // Rel entre Recursos
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'RelacoesRecursos';
        glb_aCmbKeyPesq = [['ID','aRelacaoID'],[aValue[2],'bRecursoFantasia']];
        glb_vOptParam1 = aValue[1];
        glb_vOptParam2 = aValue[0];
        // Final da automacao =========================================================
        
        if (aValue[1] == 'SUJEITO')
            return execPaging(dso, 'ObjetoID', idToFind, folderID);
        else if (aValue[1] == 'OBJETO')
            return execPaging(dso, 'SujeitoID', idToFind, folderID);
    }
    else if (folderID == 20017) // Rel com Pessoas
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'RelacoesPesRec';
        glb_aCmbKeyPesq = [['ID','aRelacaoID'],[aValue[2],'bFantasia']];
        glb_vOptParam1 = aValue[0];
        // Final da automacao =========================================================

        return execPaging(dso, 'ObjetoID', idToFind, folderID);
    }
    else if (folderID == 20018) // Colaboradores
    {
        dso.SQL = 'SELECT d.Fantasia AS Empresa, c.Fantasia AS Colaborador, a.Ordem, a.Nivel, a.OK, a.Observacao ' +
			'FROM RelacoesPesRec_Perfis a WITH (NOLOCK) , RelacoesPesRec b WITH (NOLOCK) , Pessoas c WITH (NOLOCK) , Pessoas d WITH (NOLOCK)  ' +
			'WHERE (' + sFiltro + ' a.PerfilID = ' + idToFind + ' AND a.RelacaoID = b.RelacaoID AND ' +
				'b.SujeitoID = c.PessoaID AND b.EstadoID=2 AND a.EmpresaID = d.PessoaID)' +
			'ORDER BY d.Fantasia, a.OK DESC, a.Nivel DESC, c.Fantasia ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20022) // Verbas
    {
        dso.SQL = 'SELECT a.* ' +
			'FROM Recursos_Verbas a  WITH (NOLOCK) ' +
			'WHERE (' + sFiltro + ' a.RecursoID = ' + idToFind + ') ' +
			'ORDER BY a.VerbaID ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20021) // Empresas
    {
        dso.SQL = 'SELECT a.* FROM Recursos_Empresas a WITH (NOLOCK)  WHERE ' +
        sFiltro + 'a.RecursoID = '+idToFind + ' ' +
        'ORDER BY (SELECT Fantasia FROM Pessoas  WITH (NOLOCK) WHERE (a.EmpresaID = PessoaID))';
        return 'dso01Grid_DSC';
    }
    
    //Heraldo Grid
    else if (folderID == 20023) // Hist Salario
    {
        dso.SQL = 'SELECT a.* FROM Recursos_HistoricosSalarios a WITH (NOLOCK)  WHERE ' +
        'a.RecursoID = ' + idToFind;
        return 'dso01Grid_DSC';
    }
    
    else if (folderID == 20284) // Senhas
    {
        dso.SQL = 'SELECT a.* FROM Recursos_Senhas a WITH (NOLOCK)  WHERE ' +
        sFiltro + 'a.RecursoID = ' + idToFind + ' ' +
        'ORDER BY a.Tipo';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    var currCmb2Data;
    var aValue;
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH (NOLOCK) , Recursos b WITH (NOLOCK)  ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH (NOLOCK) , Pessoas b WITH (NOLOCK)  ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH (NOLOCK) , TiposAuxiliares_Itens b WITH (NOLOCK)  ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH (NOLOCK) , Recursos b  WITH (NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Wizard
    else if ( pastaID == 20012 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT b.RecursoID AS RecursoID,b.RecursoFantasia AS Recurso ' +
                      'FROM RelacoesRecursos a WITH (NOLOCK) , Recursos b WITH (NOLOCK)  ' +
                      'WHERE a.EstadoID = 2 AND a.TipoRelacaoID = 1 ' +
                      'AND a.ObjetoID = ' + nRegistroID + ' ' +
                      'AND a.SujeitoID = b.RecursoID AND b.EstadoID = 2 ' +
                      'ORDER BY b.RecursoFantasia';
        }
    }        
	// Habilidades/Treinamentos
    else if ( pastaID == 20020 )
    {
		if ( dso.id == 'dsoCmb01Grid_01' )
		{
		    dso.SQL = 'SELECT a.CompetenciaID, (b.ItemAbreviado + SPACE(1) + a.Competencia) AS Competencia ' +
		              'FROM Competencias a WITH (NOLOCK) , TiposAuxiliares_Itens b WITH (NOLOCK)  ' +
		              'WHERE a.EstadoID=2 AND a.TipoCompetenciaID IN (1703,1704) AND a.TipoCompetenciaID = b.ItemID ' +
			          'ORDER BY (b.ItemAbreviado + SPACE(1) + a.Competencia)';
		}
	}
    // Empresas
    else if ( pastaID == 20021 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM RelacoesPesRec a WITH (NOLOCK) , Pessoas b WITH (NOLOCK)  ' +
                      'WHERE a.TipoRelacaoID = 12 AND a.ObjetoID = 999 ' +
                      'AND a.EstadoID = 2 AND a.SujeitoID = b.PessoaID AND b.EstadoID = 2 ' +
                      'ORDER BY b.Fantasia';
        }
    }

    //Heraldo Grid
    
    //Hist Salario
//    else if (pastaID == 20023) {
//    if (dso.id == 'dsoCmb01Grid_01')
//         {
//            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
//                      'FROM RelacoesPesRec a, Pessoas b ' +
//                      'WHERE a.TipoRelacaoID = 12 AND a.ObjetoID = 999 ' +
//                      'AND a.EstadoID = 2 AND a.SujeitoID = b.PessoaID AND b.EstadoID = 2 ' +
//                      'ORDER BY b.Fantasia';
//        }
//    } 
         
    // Atributos
    else if ( pastaID == 20014 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens  WITH (NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 8 ' +
                      'ORDER BY Ordem';
        }
    }    
    // Rel entre Recursos
    else if ( pastaID == 20016 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            if (aValue[1] == 'SUJEITO')
            {
                dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                          'FROM RelacoesRecursos a WITH (NOLOCK) , Recursos b WITH (NOLOCK)  ' +
                          'WHERE a.ObjetoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.SujeitoID = b.RecursoID ';
            }
            else if (aValue[1] == 'OBJETO')
            {
                dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                          'FROM RelacoesRecursos a WITH (NOLOCK) , Recursos b WITH (NOLOCK)  ' +
                          'WHERE a.SujeitoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.ObjetoID = b.RecursoID ';
            }
        }
        else if ( dso.id == 'dsoStateMachineLkp' )
        {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            if (aValue[1] == 'SUJEITO')
            {
                dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                          'FROM RelacoesRecursos a WITH (NOLOCK) , Recursos b WITH (NOLOCK)  ' +
                          'WHERE a.ObjetoID = ' + nRegistroID + 
                          ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.EstadoID = b.RecursoID ';
            }
            else if (aValue[1] == 'OBJETO')
            {
                dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                          'FROM RelacoesRecursos a WITH (NOLOCK) , Recursos b  WITH (NOLOCK) ' +
                          'WHERE a.SujeitoID = ' + nRegistroID +
                          ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                          'AND a.EstadoID = b.RecursoID ';
            }
        }
    }
    else if ( pastaID == 20017 ) // Rel com Pessoas
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM RelacoesPesRec a WITH (NOLOCK) , Pessoas b WITH (NOLOCK)  ' +
                      'WHERE a.ObjetoID = ' + nRegistroID + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                      'AND a.SujeitoID = b.PessoaID ';
        }
        else if ( dso.id == 'dsoStateMachineLkp' )
        {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM RelacoesPesRec a WITH (NOLOCK) , Recursos b WITH (NOLOCK)  ' +
                      'WHERE a.ObjetoID = ' + nRegistroID +
                      ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                      'AND a.EstadoID = b.RecursoID ';
        }
    }        
    else if ( pastaID == 20022 ) // Verbas
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.VerbaID, a.Verba ' +
				'FROM VerbasFopag a  WITH (NOLOCK) ' +
				'WHERE a.EstadoID = 2 AND a.EhProvento = 1 AND a.EhAutomatica = 1';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT a.ItemID AS PerfilID, a.ItemMasculino AS Perfil ' +
				'FROM TiposAuxiliares_Itens a  WITH (NOLOCK) ' +
				'WHERE a.TipoID = 9 AND a.EstadoID = 2';
        }
        else if (dso.id == 'dsoCmb03Grid_01') {
            dso.SQL =   'SELECT SPACE(0) AS Localidade, 0 AS LocalidadeID ' +
                        'UNION ALL ' +
                        'SELECT DISTINCT c.Localidade, c.LocalidadeID ' +
                            'FROM RelacoesPesRec a WITH (NOLOCK) ' +
                                'INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON (b.PessoaID = a.SujeitoID) ' +
                                'INNER JOIN Localidades c WITH(NOLOCK) ON (c.LocalidadeID = b.PaisID) ' +
                            'WHERE a.EstadoID = 2 AND a.TipoRelacaoID = 12';
            }
    }
    else if ( pastaID == 20284) // Senhas
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT 1 AS TipoID, ' + '\'' + 'Overfly' + '\'' + ' AS Tipo ' +
				'UNION SELECT 2 AS TipoID, ' + '\'' + 'WEB' + '\'' + ' AS Tipo ' +
				'ORDER BY TipoID';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT 1 AS NivelSegurancaID, ' + '\'' + '1' + '\'' + ' AS NivelSeguranca ' +
				'UNION SELECT 2 AS NivelSegurancaID, ' + '\'' + '2' + '\'' + ' AS NivelSeguranca ' +
				'UNION SELECT 3 AS NivelSegurancaID, ' + '\'' + '3' + '\'' + ' AS NivelSeguranca ' +
				'UNION SELECT 4 AS NivelSegurancaID, ' + '\'' + '4' + '\'' + ' AS NivelSeguranca ' +
				'ORDER BY NivelSegurancaID';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var aCmbData = null;
    var nClass = 0;


   var ehCargo = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['EhCargo'].value"), 10);

    //@@ Comeca modificar aqui
    if ( nTipoRegistroID == 5 )    // Feature
        aCmbData = getCurrDataSelect(window.top.sup01ID, 'selClassificacaoID5');

	// Este trap e importante
	if ( aCmbData != null )
	    nClass = aCmbData[1];

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    if ( nTipoRegistroID <= 4 )
	{
        vPastaName = window.top.getPastaName(20011);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20011); //Recursos Filhos
    }
    if ( nTipoRegistroID == 3 )
	{
		vPastaName = window.top.getPastaName(20012);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20012); //Wizard
	}    

    if ( nTipoRegistroID == 8 )
	{
		vPastaName = window.top.getPastaName(20015);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20015); //Transi��o de Estados
	}    

    if ( nTipoRegistroID == 6 )
    {
		vPastaName = window.top.getPastaName(20013);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20013); //Direitos

		vPastaName = window.top.getPastaName(20014);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20014); //Atributos

		vPastaName = window.top.getPastaName(20019);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20019); //Educacao/Experiencia

		vPastaName = window.top.getPastaName(20020);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20020); //Habilidades
       
       
       //HEraldo
		vPastaName = window.top.getPastaName(20021);
		
		if ((vPastaName != '') && (ehCargo == 1))
		    addOptionToSelInControlBar('inf', 1, vPastaName, 20021); //Empresas
		    
		    
		    

		vPastaName = window.top.getPastaName(20023);
		if (vPastaName != '' && (ehCargo == 1))
		    addOptionToSelInControlBar('inf', 1, vPastaName, 20023); //Hist Salario

		vPastaName = window.top.getPastaName(20018);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20018); //Colaboradores

		vPastaName = window.top.getPastaName(20022);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20022); //verbas
    }
    
    if ( (nTipoRegistroID == 3) || (nTipoRegistroID == 4) || 
         (nTipoRegistroID == 6) || (nTipoRegistroID == 7) || 
         (nTipoRegistroID == 8) || (nTipoRegistroID == 5) )
    {    
        vPastaName = window.top.getPastaName(20016);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20016); //Rela��es entre Recursos
    }

    if (nTipoRegistroID == 1)
    {
        vPastaName = window.top.getPastaName(20284);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20284); //Senhas

        vPastaName = window.top.getPastaName(20017);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20017); //Rela��es com Pessoas
    }
 	
    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    var currCmb2Data;
    var aValue;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 20012) // Wizard
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1], ['ContextoID', registroID]);

        else if (folderID == 20014) // Atributos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2, 3, 4], ['PerfilID', registroID]);

        else if (folderID == 20020) // Habilidades/Treinamentos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['RecursoID', registroID]);

        else if (folderID == 20021) // Empresas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['RecursoID', registroID]);

        else if (folderID == 20023) // Hist Salario
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['RecursoID', registroID]);
        }
        
        else if (folderID == 20022) // Verbas
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1], ['RecursoID', registroID]);
        }

        else if ((folderID == 20016) || (folderID == 20017)) //Relacoes entre Recursos/Relacoes com Pessoas
        {
            currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
            aValue = currCmb2Data[1].split(',');
            if (aValue[1] == 'SUJEITO')
                currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [2], ['ObjetoID', registroID], [4, 5]);
            else if (aValue[1] == 'OBJETO')
                currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [2], ['SujeitoID', registroID], [4, 5]);
        }

        else if (folderID == 20284) // Senhas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16], ['RecursoID', registroID]);
        
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Recursos Filhos
    else if (folderID == 20011)
    {
        headerGrid(fg,['ID','Est','Recurso','Tipo'], []);
        fillGridMask(fg,currDSO,['RecursoID','RecursoAbreviado','RecursoFantasia','ItemMasculino']
                                     ,['','','','']);
    }    
    // Wizard
    else if (folderID == 20012)
    {
        headerGrid(fg,['Ordem','Sub-Form','ContextoID','RecWizardID'], [2,3]);
        fillGridMask(fg,currDSO,['Ordem','SubFormID','ContextoID','RecWizardID']
                                     ,['99','','','']);
        
    }    
    // Direitos
    else if (folderID == 20013)
    {
        headerGrid(fg,['ID','Recurso','C1','C2','A1','A2','I','E1','E2'], []);
        fillGridMask(fg,currDSO,['RecursoID','Recurso','C1','C2','A1','A2','I','E1','E2']
                                     ,['','','','','','','','','']);
        
    }    
    // Atributos de usuario
    else if (folderID == 20014)
    {
        headerGrid(fg,['Atributo',
                       'M�nimo',
                       'M�ximo',
                       'Var Min',
                       'Var Max',
                       'RecAtributoID'],[5]);
                       
        fillGridMask(fg,currDSO,['AtributoID','ValorMinimo','ValorMaximo','VariacaoMinima','VariacaoMaxima','RecAtributoID'],
                                ['','#999.99','#999.99','999.99','999.99',''],
                                ['','###.00','###.00','###.00','###.00','']);

        alignColsInGrid(fg,[1,2,3,4]);
    }
    // Habilidades/Treinamentos
    else if (folderID == 20020)
    {
        headerGrid(fg,['Compet�ncia',
                       'Nota M�nima',
                       'Observa��o',
                       'RecCompetenciaID'],[3]);
                       
        fillGridMask(fg,currDSO,['CompetenciaID',
								 'NotaMinima',
								 'Observacao',
								 'RecCompetenciaID'],
                                ['','999.99','',''],
                                ['','###.00','','']);

        alignColsInGrid(fg,[1]);
    }
	// Empresas
    else if (folderID == 20021)
    {
        headerGrid(fg,['Empresa', 'RecEmpresaID'], [1]);
        fillGridMask(fg,currDSO,['EmpresaID', 'RecEmpresaID']
                                     ,['','']);
    }
    //Heraldo Grig
    
    //Hist Salario
    else if (folderID == 20023) {
        headerGrid(fg, ['Nivel', 'Vig�ncia', 'Salario', 'EmpresaID','RecHistoricoID'], [3,4]);
        fillGridMask(fg, currDSO, ['Nivel', 'dtVigencia','Salario','EmpresaID', 'RecHistoricoID'],
                                  ['', '', '', '',''], ['','', '', '', '']);
    }    
    
     
    // Transicao de Estados
    else if  (folderID == 20015)  
    {
        headerGrid(fg,['De',
                       'Para',
                       'Default'],[]);
        fillGridMask(fg,currDSO,['De','Para','EhDefault']
                                     ,['','','']);
                                     
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Rel entre Recursos
    else if (folderID == 20016)
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');

        if (aValue[1] == 'SUJEITO')
        {
            headerGrid(fg,['ID',
                           'Est',
                            aValue[2],
                           '_calc_HoldKey_1',
                           'TipoRelacaoID',
                           'SujeitoID',
                           'RelacaoID'],[3,4,5,6]);
                           
            fillGridMask(fg,currDSO,['RelacaoID*',
                                     '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                     '^SujeitoID^dso01GridLkp^RecursoID^RecursoFantasia*',
                                     '_calc_HoldKey_1',
                                     'TipoRelacaoID',
                                     'SujeitoID',
                                     'RelacaoID'],['','','','9','','']);
        }
        else if (aValue[1] == 'OBJETO')
        {
            headerGrid(fg,['ID',
                           'Est',
                            aValue[2],
                           '_calc_HoldKey_1',
                           'TipoRelacaoID',
                           'ObjetoID',
                           'RelacaoID'],[3,4,5,6]);
                           
            fillGridMask(fg,currDSO,['RelacaoID*',
                                     '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                     '^ObjetoID^dso01GridLkp^RecursoID^RecursoFantasia*',
                                     '_calc_HoldKey_1',
                                     'TipoRelacaoID',
                                     'ObjetoID',
                                     'RelacaoID'],['','','','9','','']);
        }
    }
    // Rel com Pessoas
    else if (folderID == 20017) 
    {
        currCmb2Data = getCmbCurrDataInControlBar('inf', 2);
        aValue = currCmb2Data[1].split(',');

        headerGrid(fg,['ID',
                       'Est',
                        aValue[2],
                       '_calc_HoldKey_1',
                       'TipoRelacaoID',
                       'SujeitoID',
                       'RelacaoID'],[3,4,5,6]);
                           
        fillGridMask(fg,currDSO,['RelacaoID*',
                                 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                 '^SujeitoID^dso01GridLkp^PessoaID^Fantasia*',
                                 '_calc_HoldKey_1',
                                 'TipoRelacaoID',
                                 'SujeitoID',
                                 'RelacaoID'],['','','','9','','']);
    }
    // Colaboradores
    else if (folderID == 20018)
    {
        headerGrid(fg,['Empresa',
                       'Colaborador',
					   'Ordem',
                       'N�vel',
					   'OK',
					   'Observa��o'],[]);

        fillGridMask(fg,currDSO,['Empresa',
								 'Colaborador',
								 'Ordem',
								 'Nivel',
								 'OK',
								 'Observacao'],['','','','','','']);
		
		// Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;						 
    }
    // Verbas
    else if (folderID == 20022)
    {
        headerGrid(fg, ['Pa�s',
                       'Verba',
                       'Tipo perfil comercial',                       
					   'Observa��o',
					   'RecVerbaID'],[4]);

        fillGridMask(fg, currDSO, ['PaisID',
                                    'VerbaID',
						            'TipoPerfilComercialID',                                    
						            'Observacao',
						            'RecVerbaID'],['','','','','']);

    }
    // Senhas
    else if (folderID == 20284)
    {
        headerGrid(fg,['Tipo',
			'Hist�rico',
			'Dur m�n',
			'Dur m�x',
			'Tam m�n',
			'Tam m�x',
			'N�vel',
			'Car rep',
			'Seq car',
			'Car doc',
			'Car tel',
			'Aniv',
			'Contr',
			'Tentativas',
			'Tempo',
			'Bloqueios',
			'RecSenhaID'], [16]);

        glb_aCelHint = [[0,1,'N�mero de senhas que ser�o mantidas em hist�rico'],
			[0,2,'Dura��o m�nima da senha (dias)'],
			[0,3,'Dura��o m�xima da senha (dias)'],
			[0,4,'Tamanho m�nimo da senha'],
			[0,5,'Tamanho m�ximo da senha'],
			[0,6,'N�vel de seguran�a da senha'],
			[0,7,'N�mero m�ximo de caracteres repedidos'],
			[0,8,'N�mero m�ximo de caracteres em sequ�ncia'],
			[0,9,'N�mero m�ximo de caracteres em sequ�ncia de documentos'],
			[0,10,'N�mero m�ximo de caracteres em sequ�ncia de telefones'],
			[0,11,'Bloqueia data de anivers�rio?'],
			[0,12,'Solicita contra-senha?'],
			[0,13,'N�mero m�ximo de tentativas antes de bloquear a senha'],
			[0,14,'Tempo de bloqueio da senha (horas)'],
			[0,15,'N�mero de bloqueios antes de suspender a senha']];

        fillGridMask(fg,currDSO,['Tipo*',
			'NumeroHistoricoSenhas',
			'DuracaoMinima',
			'DuracaoMaxima',
			'TamanhoMinimo',
			'TamanhoMaximo',
			'NivelSegurancaSenha',
			'NumeroCaracteresRepetidos',
			'NumeroSequenciaCaracteres',
			'NumeroCaracteresDocumentos',
			'NumeroCaracteresTelefones',
			'BloqueiaDataAniversario',
			'SolicitaContraSenha',
			'NumeroTentativas',
			'TempoBloqueio',
			'NumeroBloqueios',
			'RecSenhaID']
           ,['','99','99','999','99','99','','9','9','9','9','','','9','9','9',''],
            ['','##','##','###','##','##','','#','#','#','#','','','#','#','#','']);
            
		fg.FrozenCols = 1;
    }
}
