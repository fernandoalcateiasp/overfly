using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.basico.recursosEx.serverside
{
    public partial class salvaalteracao : System.Web.UI.OverflyPage
    {
        private string[] registroID;
        private string[] sc1;
        private string[] sc2;
        private string[] sa1;
        private string[] sa2;
        private string[] si;
        private string[] se1;
        private string[] se2;
        private string strSQL;
        private string strUpdates;

        protected string[] nRegistroID
        {
            set
            {
                registroID = value;

                for (int i = 1; i < registroID.Length; i++)
                {
                    if (registroID[i] == null || registroID[i].Length == 0)
						registroID[i] = Constants.STR_EMPTY;
                }
            }
        }

        protected string[] sC1
        {
            set
            {
                sc1 = value;

                for (int i = 1; i < sc1.Length; i++)
                {
                    if (sc1[i] == null || sc1[i].Length == 0)
						sc1[i] = Constants.STR_EMPTY;
                }
            }
        }

        protected string[] sC2
        {
            set
            {
                sc2 = value;

                for (int i = 1; i < sc2.Length; i++)
                {
                    if (sc2[i] == null || sc2[i].Length == 0)
						sc2[i] = Constants.STR_EMPTY;
                }
            }
        }

        protected string[] sA1
        {
            set
            {
                sa1 = value;

                for (int i = 1; i < sa1.Length; i++)
                {
                    if (sa1[i] == null || sa1[i].Length == 0)
						sa1[i] = Constants.STR_EMPTY;
                }
            }
        }

        protected string[] sA2
        {
            set
            {
                sa2 = value;

                for (int i = 1; i < sa2.Length; i++)
                {
                    if (sa2[i] == null || sa2[i].Length == 0)
						sa2[i] = Constants.STR_EMPTY;
                }
            }
        }

        protected string[] sI
        {
            set
            {
                si = value;

                for (int i = 1; i < si.Length; i++)
                {
                    if (si[i] == null || si[i].Length == 0)
						si[i] = Constants.STR_EMPTY;
                }
            }
        }

        protected string[] sE1
        {
            set
            {
                se1 = value;

                for (int i = 1; i < se1.Length; i++)
                {
                    if (se1[i] == null || se1[i].Length == 0)
						se1[i] = Constants.STR_EMPTY;
                }
            }
        }

        protected string[] sE2
        {
            set
            {
                se2 = value;

                for (int i = 1; i < se2.Length; i++)
                {
                    if (se2[i] == null || se2[i].Length == 0)
						se2[i] = Constants.STR_EMPTY;
                }
            }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
			strSQL = "";
			
			for (int i = 0; i < registroID.Length; i++)
            {
                strUpdates = "";

                if (sc1[i] != "")
                {
                    if (strUpdates != "")
                        strUpdates = strUpdates + ",";

                    strUpdates = strUpdates + "Consultar1 = " + sc1[i].ToString();
                }

                if (sc2[i] != "")
                {
                    if (strUpdates != "")
                        strUpdates = strUpdates + ",";

                    strUpdates = strUpdates + "Consultar2 = " + sc2[i].ToString();
                }

                if (sa1[i] != "")
                {
                    if (strUpdates != "")
                        strUpdates = strUpdates + ",";

                    strUpdates = strUpdates + "Alterar1 = " + sa1[i].ToString();
                }

                if (sa2[i] != "")
                {
                    if (strUpdates != "")
                        strUpdates = strUpdates + ",";

                    strUpdates = strUpdates + "Alterar2 = " + sa2[i].ToString();
                }

                if (si[i] != "")
                {
                    if (strUpdates != "")
                        strUpdates = strUpdates + ",";

                    strUpdates = strUpdates + "Incluir = " + si[i].ToString();
                }

                if (se1[i] != "")
                {
                    if (strUpdates != "")
                        strUpdates = strUpdates + ",";

                    strUpdates = strUpdates + "Excluir1 = " + se1[i].ToString();
                }

                if (se2[i] != "")
                {
                    if (strUpdates != "")
                        strUpdates = strUpdates + ",";

                    strUpdates = strUpdates + "Excluir2 = " + se2[i].ToString();
                }

                if (strUpdates != "")
                {
                    strSQL += "UPDATE Recursos_Direitos SET " + strUpdates + " WHERE RecDireitoID= " + registroID[i].ToString() + " ";
                }                
            }
            // Executa o pacote
            int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(strSQL);

            // Gera o resultado para o usuario.
			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT " + rowsAffected + " as Resultado"
                )
            );
        }
    }
}
