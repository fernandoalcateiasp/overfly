using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using OVERFLYSVRCFGLib;
using Overfly3.systemEx.serverside;

namespace Overfly3.basico.recursosEx.serverside
{
    public partial class insertrights : System.Web.UI.OverflyPage
    {
        private Integer perfilID;
        private Integer perfilCloneID;
        private Integer nivel;
        private Integer recursoID;
        private Integer recursoMaeID;
        private string SQL = "";
        private string urlRoot = null;

        protected Integer nPerfilID
        {
			set { perfilID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nPerfilCloneID
        {
			set { perfilCloneID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nNivel
        {
			set { nivel = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nRecursoID
        {
			set { recursoID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nRecursoMaeID
        {
			set { recursoMaeID = (value != null) ? value : Constants.INT_ZERO; }
        }


        public string URLRoot
        {
            get
            {
                if (urlRoot == null)
                {
                    OverflyMTS svrCfg = new OverflyMTS();

                    urlRoot = svrCfg.PagesURLRoot(
                        System.Configuration.ConfigurationManager.AppSettings["application"]
                    );

                    if (!urlRoot.StartsWith("http://www"))
                    {
                        urlRoot = "http://localhost/overfly3";
                    }
                }

                return urlRoot;
            }
        }
    

        private void strSQL()
        {
            SQL = "DELETE FROM Recursos_Direitos WHERE PerfilID=" + perfilID.ToString() + " " +
                  "INSERT INTO Recursos_Direitos (PerfilID,RecursoID,RecursoMaeID, " +
                  "ContextoID,Nivel,Consultar1,Consultar2,Alterar1,Alterar2,Incluir,Excluir1,Excluir2) " +
                  "SELECT " + perfilID.ToString() + " ,a.RecursoID, NULL AS RecursoMaeID,NULL AS ContextoID,1 AS Nivel,0,0,0,0,0,0,0 " +
                  "FROM Recursos a " +
                  "WHERE a.TipoRecursoID=1 AND (a.EstadoID BETWEEN 2 AND 3) " +
                  "UNION ALL " +
                  "SELECT " + perfilID.ToString() + " ,b.RecursoID, b.RecursoMaeID,NULL AS ContextoID,2 AS Nivel,0,0,0,0,0,0,0 " +
                  "FROM Recursos a, Recursos b " +
                  "WHERE a.TipoRecursoID=1 AND (a.EstadoID BETWEEN 2 AND 3) AND a.RecursoID=b.RecursoMaeID " +
                  "AND b.TipoRecursoID=2 AND b.ClassificacaoID=9 " +
                  "AND (b.EstadoID BETWEEN 2 AND 3) " +
                  "UNION ALL " +
                  "SELECT " + perfilID.ToString() + " ,c.RecursoID, c.RecursoMaeID,NULL AS ContextoID,3 AS Nivel,0,0,0,0,0,0,0 " +
                  "FROM Recursos a, Recursos b, Recursos c " +
                  "WHERE a.TipoRecursoID=1 AND (a.EstadoID BETWEEN 2 AND 3) AND a.RecursoID=b.RecursoMaeID " +
                  "AND b.TipoRecursoID=2 AND b.ClassificacaoID=9 " +
                  "AND (b.EstadoID BETWEEN 2 AND 3) AND b.RecursoID=c.RecursoMaeID AND c.TipoRecursoID=2 " +
                  "AND c.ClassificacaoID=10 AND (c.EstadoID BETWEEN 2 AND 3) " +
                  "UNION ALL " +
                  "SELECT " + perfilID.ToString() + " ,d.RecursoID,d.RecursoMaeID,NULL AS ContextoID,4 AS Nivel,0,0,0,0,0,0,0 " +
                  "FROM Recursos a, Recursos b, Recursos c, Recursos d " +
                  "WHERE a.TipoRecursoID=1 AND (a.EstadoID BETWEEN 2 AND 3) AND a.RecursoID=b.RecursoMaeID " +
                  "AND b.TipoRecursoID=2 AND b.ClassificacaoID=9 " +
                  "AND (b.EstadoID BETWEEN 2 AND 3) AND b.RecursoID=c.RecursoMaeID AND c.TipoRecursoID=2 " +
                  "AND c.ClassificacaoID=10 AND (c.EstadoID BETWEEN 2 AND 3) " +
                  "AND c.RecursoID = d.RecursoMaeID AND d.TipoRecursoID=2 AND d.ClassificacaoID=11 AND (d.EstadoID BETWEEN 2 AND 3)";

            int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(SQL);

            // Gera o resultado para o usuario.
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "SELECT " + rowsAffected + " as Resultado"
                    )
            );
        }
        
        private void clonaDireitos()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[5];

            parameters[0] = new ProcedureParameters(
                "@PerfilOrigemID",
                System.Data.SqlDbType.Int,
                perfilCloneID);

            parameters[1] = new ProcedureParameters(
                "@PerfilDestinoID",
                System.Data.SqlDbType.Int,
                perfilID);

            parameters[2] = new ProcedureParameters(
                "@Nivel",
                System.Data.SqlDbType.Int,
                nivel);

            parameters[3] = new ProcedureParameters(
                "@RecursoID",
                System.Data.SqlDbType.Int,
                recursoID);

            parameters[4] = new ProcedureParameters(
                "@RecursoMaeID",
                System.Data.SqlDbType.Int,
                (recursoMaeID != null ? (Object)recursoMaeID : DBNull.Value));

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Clona_Direitos",
                parameters);

        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (perfilCloneID == perfilID)
            {
                strSQL();
            }else
            {
                clonaDireitos();
            }

            WriteResultXML(DataInterfaceObj.getRemoteData("select NULL as Resultado"));
            //Response.Redirect(URLRoot + "/basico/recursos/serverside/loadProfile.aspx?nPerfilID=" + perfilID.ToString());
        }
    }
}