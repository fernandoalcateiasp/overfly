using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.basico.recursosEx.serverside
{
    public partial class loadprofile : System.Web.UI.OverflyPage
    {
        private Integer perfilID; 
        private Integer action;
        private Integer recursoID;

        protected Integer nPerfilID
        {
			set { perfilID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nAction
        {
			set { action = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nRecursoID
        {
			set { recursoID = (value != null) ? value : Constants.INT_ZERO; }
        }

        private void Direitos_Sincroniza()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[2];

            parameters[0] = new ProcedureParameters(
                "@PerfilID",
                System.Data.SqlDbType.Int,
                perfilID);

            parameters[1] = new ProcedureParameters(
                "@RecursoID",
                System.Data.SqlDbType.Int,
                recursoID);

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Direitos_Sincroniza",
                parameters);
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSQL = "";

            strSQL = "SELECT b.RecursoID AS RecursoID, (LTRIM(STR(b.RecursoID)) + SPACE(1) + b.Recurso) AS Recurso, a.Nivel AS Nivel, " +
             "a.RecursoMaeID AS RecursoMaeID, ISNULL(a.ContextoID,0) AS ContextoID, " +
             "a.Consultar1 AS C1, a.Consultar2 AS C2, " +
             "a.Alterar1 AS A1, a.Alterar2 AS A2, " +
             "a.Incluir AS I, a.Excluir1 AS E1,a.Excluir2 AS E2, a.RecDireitoID AS RegistroID, " +
             "ISNULL(a.EstadoParaID,0) AS EstadoParaID, ' ' AS EstadoPara  " +
             "FROM Recursos_Direitos a WITH (NOLOCK) , Recursos b  WITH (NOLOCK) " +
             "WHERE a.PerfilID=" + perfilID.ToString() + " AND a.Nivel<=4 " +
             "AND a.RecursoID=b.RecursoID " +
             "ORDER BY a.Nivel, b.RecursoID";

            if (action.intValue() == 1)
            {
                Direitos_Sincroniza();
            }

            // Gera o resultado para o usuario.
            WriteResultXML(DataInterfaceObj.getRemoteData(strSQL));
        }
    }
}
