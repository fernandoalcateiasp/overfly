﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_manutencaosalarial : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int empresaID = Convert.ToInt32(HttpContext.Current.Request.Params["empresaID"]);
        private int selModo = Convert.ToInt32(HttpContext.Current.Request.Params["selModo"]);
        private string txtInicio = Convert.ToString(HttpContext.Current.Request.Params["txtInicio"]);
        private string txtInicio2 = Convert.ToString(HttpContext.Current.Request.Params["txtInicio2"]);
        private string txtFim = Convert.ToString(HttpContext.Current.Request.Params["txtFim"]);
        private string txtFim2 = Convert.ToString(HttpContext.Current.Request.Params["txtFim2"]);
        private string selCargo = Convert.ToString(HttpContext.Current.Request.Params["selCargo"]);
        private string selNivel = Convert.ToString(HttpContext.Current.Request.Params["selNivel"]);
        int nLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nLinguaLogada"]);
        string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        string controlID = Convert.ToString(HttpContext.Current.Request.Params["controlID"]);
        string txtVigencia = Convert.ToString(HttpContext.Current.Request.Params["txtVigencia"]);
        string txtVigencia2 = Convert.ToString(HttpContext.Current.Request.Params["txtVigencia2"]);
        string txtAjusteSalario = Convert.ToString(HttpContext.Current.Request.Params["txtAjusteSalario"]);
        string txtAjusteSalario2 = Convert.ToString(HttpContext.Current.Request.Params["txtAjusteSalario2"]);
        string controle = Convert.ToString(HttpContext.Current.Request.Params["controle"]);


        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controle)
                {
                    case "listaExcel":
                        listaExcelManutencaoSalarial();
                        break;
                }
            }
        }

        public void listaExcelManutencaoSalarial()
        {
            DateTime DataHoje = DateTime.Now;
            string param = "<Format>dd/mm/yyy</Format>";
            string _data = DataHoje.ToString("dd_MM_yyyy");

            if (nLinguaLogada != 246)
            {
                param = "<Format>mm/dd/yyy</Format>";
                _data = DataHoje.ToString("MM_dd_yyyy");
            }

            string Title = "Manutenção_Salarial_" + sEmpresaFantasia + "_" + _data;



            string strSQL = "SELECT RecHistoricoID, a.RecursoID AS CargoID, " +
                                "dbo.fn_Cargo_Nome(a.RecursoID, c.Nivel, b.EmpresaID, 1) AS Cargo, c.dtVigencia, c.Salario, ";

            // Se estiver no modo manutenção 
            if (selModo == 2)
            {
                strSQL += (txtVigencia != "") ?
                            "'" + txtVigencia2 + "'" + " AS dtVigencia2, " :
                            "NULL AS dtVigencia2, ";

                strSQL += (txtAjusteSalario != "") ?
                            "((1 + (" + txtAjusteSalario2 + "/100.0))*c.Salario) AS Salario2, " :
                            "NULL AS Salario2, ";
            }

            strSQL += "c.Observacao, " +
                        ((txtVigencia != "" || txtAjusteSalario != "") ? "convert(bit, 1) as OK " : "convert(bit, 0) as OK ") +
                    "FROM Recursos a WITH(NOLOCK) " +
                        "INNER JOIN Recursos_Empresas b WITH(NOLOCK) ON a.RecursoID=b.RecursoID " +
                        "INNER JOIN Recursos_HistoricosSalarios c WITH(NOLOCK) ON a.RecursoID=c.RecursoID " +
                    "WHERE " +
                        ((txtInicio != "") ?
                            ("dtVigencia >= '" + txtInicio2 + "' AND ") : ("")) +
                        ((txtFim != "") ?
                            ("dtVigencia <= '" + txtFim2 + "' AND ") : ("")) +
                        ((selCargo != "" && selCargo != "0") ? ("a.RecursoID = " + selCargo + " AND ") : ("")) +
                        ((selNivel != "" && selNivel != "0") ? ("c.Nivel = " + selNivel + " AND ") : ("")) +
                        "b.EmpresaID = " + empresaID;

            // Se for manutenção mostra apenas o último item do histórico.
            if (selModo == 2)
            {
                strSQL += " AND " +
                        "c.RecHistoricoID =  " +
                        "( " +
                            "SELECT TOP 1 RecHistoricoID  " +
                            "FROM Recursos_HistoricosSalarios z WITH (NOLOCK) " +
                            "WHERE z.EmpresaID = c.EmpresaID AND  " +
                                "z.RecursoID = c.RecursoID AND " +
                                "z.Nivel = c.Nivel " +
                            "ORDER BY z.dtVigencia DESC " +
                        ") ";
            }

            strSQL += " ORDER BY Cargo";


            if (controlID == "btnExcel")//SE EXCEL
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                string[,] arrIndexKey = new string[,] { { "" } };

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                int Datateste = 0;
                bool nulo = false;

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, false);

                    Relatorio.CriarObjColunaNaTabela("ID", "CargoID", true, "ID");
                    Relatorio.CriarObjCelulaInColuna("Query", "CargoID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Cargo", "Cargo", true, "Cargo");
                    Relatorio.CriarObjCelulaInColuna("Query", "Cargo", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Vigência", "dtVigencia", true, "Vigência");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtVigencia", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("Salário", "Salario", true, "Salario");
                    Relatorio.CriarObjCelulaInColuna("Query", "Salario", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Observação", "Observacao", true, "Observação");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, 2);
                }
            }
            else//Se Lista
            {
                DataSet dtTeste = DataInterfaceObj.getRemoteData(strSQL);
                WriteResultXML(dtTeste);
            }
        }
    }
}
