using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.basico.recursosEx.serverside
{
    public partial class getformdetail : System.Web.UI.OverflyPage
    {
        private Integer perfilID;
        private Integer recursoID;
        private Integer formID;
        private Integer action;
        private Integer contextoID;

        protected Integer nPerfilID
        {
			set { perfilID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nRecursoID
        {
			set { recursoID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nFormID
        {
			set { formID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nAction
        {
			set { action = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nContextoID
        {
			set { contextoID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSQL = "";
			string strCom;
           
            strSQL = "SELECT DISTINCT b.RecursoID AS RecursoID, (LTRIM(STR(b.RecursoID)) + SPACE(1) + b.Recurso) AS Recurso, a.Nivel AS Nivel, " +
             "a.RecursoMaeID AS RecursoMaeID, ISNULL(a.ContextoID,0) AS ContextoID, " +
             "a.Consultar1 AS C1, a.Consultar2 AS C2, " +
             "a.Alterar1 AS A1, a.Alterar2 AS A2, " +
             "a.Incluir AS I, a.Excluir1 AS E1,a.Excluir2 AS E2, a.RecDireitoID AS RegistroID, ISNULL(a.EstadoParaID,0) AS EstadoParaID, ' ' AS EstadoPara " +
             "FROM Recursos_Direitos a WITH (NOLOCK) , Recursos b  WITH (NOLOCK) " +
             "WHERE a.PerfilID=" + perfilID.ToString() + " AND a.Nivel=5 " +
             "AND a.RecursoMaeID=" + formID.ToString() + " AND a.RecursoID=b.RecursoID " +
             "UNION ALL SELECT DISTINCT c.RecursoID AS RecursoID, (LTRIM(STR(c.RecursoID)) + SPACE(1) + c.Recurso) AS Recurso, b.Nivel AS Nivel, " +
             "b.RecursoMaeID AS RecursoMaeID, ISNULL(b.ContextoID,0) AS ContextoID, " +
             "b.Consultar1 AS C1, b.Consultar2 AS C2, " +
             "b.Alterar1 AS A1, b.Alterar2 AS A2, " +
             "b.Incluir AS I, b.Excluir1 AS E1,b.Excluir2 AS E2, b.RecDireitoID AS RegistroID, ISNULL(b.EstadoParaID,0) AS EstadoParaID, ' ' AS EstadoPara " +
             "FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b WITH (NOLOCK) , Recursos c WITH (NOLOCK)  " +
             "WHERE a.PerfilID=" + perfilID.ToString() + " AND a.Nivel=5 " +
             "AND a.RecursoMaeID=" + formID.ToString() + " AND a.RecursoID=b.RecursoMaeID AND b.PerfilID=" + perfilID.ToString() + " " +
             "AND b.Nivel=6 AND b.RecursoID=c.RecursoID " +
             "UNION ALL SELECT DISTINCT d.RecursoID AS RecursoID, (LTRIM(STR(d.RecursoID)) + SPACE(1) + d.Recurso) AS Recurso, c.Nivel AS Nivel, " +
             "c.RecursoMaeID AS RecursoMaeID, ISNULL(c.ContextoID,0) AS ContextoID, " +
             "c.Consultar1 AS C1, c.Consultar2 AS C2, " +
             "c.Alterar1 AS A1, c.Alterar2 AS A2, " +
             "c.Incluir AS I, c.Excluir1 AS E1,c.Excluir2 AS E2, c.RecDireitoID AS RegistroID, ISNULL(c.EstadoParaID,0) AS EstadoParaID, ' ' AS EstadoPara " +
             "FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b WITH (NOLOCK) , Recursos_Direitos c WITH (NOLOCK) , Recursos d  WITH (NOLOCK) " +
             "WHERE a.PerfilID=" + perfilID.ToString() + " AND a.Nivel=5 " +
             "AND a.RecursoMaeID=" + formID.ToString() + " AND a.RecursoID=b.RecursoMaeID AND b.PerfilID=" + perfilID.ToString() + " " +
             "AND b.Nivel=6 AND b.RecursoID=c.RecursoMaeID AND c.PerfilID=" + perfilID.ToString() + " " +
             "AND c.Nivel=7 AND c.RecursoID=d.RecursoID " +
             "UNION ALL SELECT DISTINCT d.RecursoID AS RecursoID, (LTRIM(STR(d.RecursoID)) + SPACE(1) + d.Recurso) AS Recurso, c.Nivel AS Nivel, " +
             "c.RecursoMaeID AS RecursoMaeID, ISNULL(c.ContextoID,0) AS ContextoID, " +
             "c.Consultar1 AS C1, c.Consultar2 AS C2, " +
             "c.Alterar1 AS A1, c.Alterar2 AS A2, " +
             "c.Incluir AS I, c.Excluir1 AS E1,c.Excluir2 AS E2, c.RecDireitoID AS RegistroID, ISNULL(c.EstadoParaID,0) AS EstadoParaID, (LTRIM(STR(e.RecursoID)) + SPACE(1) + e.Recurso) AS EstadoPara " +
             "FROM Recursos_Direitos a WITH (NOLOCK) , Recursos_Direitos b WITH (NOLOCK) , Recursos_Direitos c WITH (NOLOCK) , Recursos d WITH (NOLOCK) , Recursos e  WITH (NOLOCK) " +
             "WHERE a.PerfilID=" + perfilID.ToString() + " AND a.Nivel=5 " +
             "AND a.RecursoMaeID=" + formID.ToString() + " AND a.RecursoID=b.RecursoMaeID AND b.PerfilID=" + perfilID.ToString() + " " +
             "AND b.Nivel=6 AND b.RecursoID=c.RecursoMaeID AND c.PerfilID=" + perfilID.ToString() + " " +
             "AND c.Nivel=8 AND c.RecursoID=d.RecursoID AND c.EstadoParaID=e.RecursoID " +
             "ORDER BY Nivel, RecursoID";

            if (action.intValue() == 1)
            {
                strCom = "EXEC sp_Direitos_Sincroniza " + perfilID.ToString() + "," +
                                              recursoID.ToString() + "," +
                                              contextoID.ToString();
                if (strCom != "")
                {
                    DataInterfaceObj.ExecuteSQLCommand(strCom);
                }
            }
            // Gera o resultado para o usuario.
            WriteResultXML(DataInterfaceObj.getRemoteData(strSQL));
        }
    }
}