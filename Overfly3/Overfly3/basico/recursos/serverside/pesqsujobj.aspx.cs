using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.basico.recursosEx.serverside
{
    public partial class pesqsujobj : System.Web.UI.OverflyPage
    {
        private Integer tipoRelID;
        private string tipoSujObj;
        private string strtoFind;
        private Integer sujObjID;
        private Integer relEntreID;
        private string stroperator;

        protected Integer nTipoRelID
        {
			set { tipoRelID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected string strToFind
        {
            set
            {
				strtoFind = (value != null) ? value : Constants.STR_EMPTY;

				if ((strtoFind.Substring(0, 1) == "%") || (strtoFind.Substring(0, (strtoFind.Length - 1)) == "%"))
				{
					stroperator = " LIKE ";
				}
				else
				{
					stroperator = " >= ";
				}
			}
        }

        protected string sTipoSujObj
        {
			set { tipoSujObj = (value != null) ? value : Constants.STR_EMPTY; }
        }

        protected Integer nSujObjID
        {
			set { sujObjID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected Integer nRelEntreID
        {
			set { relEntreID = (value != null) ? value : Constants.INT_ZERO; }
        }

        protected string strOperator
        {
            set 
            {
				stroperator = (value != null) ? value : Constants.STR_EMPTY;
			}
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string strSQL = "";

            if (tipoSujObj == "SUJEITO")
            {
                strSQL = "SELECT TOP 100 c.Recurso AS fldName,c.RecursoID AS fldID,c.RecursoFantasia AS Fantasia " +
                        "FROM Recursos a WITH (NOLOCK) , TiposRelacoes_Itens b WITH (NOLOCK) , Recursos c  WITH (NOLOCK) " +
                        "WHERE a.RecursoID = " + sujObjID + " AND a.TipoRecursoID = b.TipoObjetoID " +
                        "AND b.TipoRelacaoID = " + tipoRelID + " AND b.TipoSujeitoID = c.TipoRecursoID " +
                        "AND c.EstadoID = 2 " +
                        "AND c.RecursoID NOT IN (SELECT a.SujeitoID FROM RelacoesRecursos a  WITH (NOLOCK) " +
                        "WHERE a.ObjetoID=" + sujObjID + " AND a.TipoRelacaoID=" + tipoRelID + ") " +
                        "AND c.Recurso " + stroperator + "'" + strtoFind + "' " +
                        "ORDER BY fldName ";
            }else if (tipoSujObj == "OBJETO")
            {
                strSQL = "SELECT TOP 100 c.Recurso AS fldName,c.RecursoID AS fldID,c.RecursoFantasia AS Fantasia " +
                        "FROM Recursos a, WITH (NOLOCK)  TiposRelacoes_Itens b WITH (NOLOCK) , Recursos c  WITH (NOLOCK) " +
                         "WHERE a.RecursoID = " + sujObjID + " AND a.TipoRecursoID = b.TipoSujeitoID " +
                         "AND b.TipoRelacaoID = " + tipoRelID + " AND b.TipoObjetoID = c.TipoRecursoID " +
                         "AND c.EstadoID = 2 " +
                         "AND c.RecursoID NOT IN (SELECT a.ObjetoID FROM RelacoesRecursos a  WITH (NOLOCK) " +
                         "WHERE a.SujeitoID=" + sujObjID + " AND a.TipoRelacaoID=" + tipoRelID + ") " +
                         "AND c.Recurso " + stroperator + "'" + strtoFind + "' " +
                         "ORDER BY fldName ";
            }else if (relEntreID.intValue() == 132)
            {
                strSQL = "SELECT TOP 100 c.Nome AS fldName,c.PessoaID AS fldID,c.Fantasia AS Fantasia, " +
                         "dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 111, 0) AS Documento, g.Localidade " +
                         "AS Cidade, h.CodigoLocalidade2 AS UF, i.CodigoLocalidade2 AS Pais " +
                         "FROM Recursos a WITH (NOLOCK) , TiposRelacoes_Itens b WITH (NOLOCK) , Pessoas c  WITH (NOLOCK) " +
                         "LEFT OUTER JOIN Pessoas_Enderecos f WITH (NOLOCK)  ON (c.PessoaID = f.PessoaID AND (f.Ordem=1)) " +
                         "LEFT OUTER JOIN Localidades g WITH (NOLOCK)  ON f.CidadeID = g.LocalidadeID " +
                         "LEFT OUTER JOIN Localidades h WITH (NOLOCK)  ON f.UFID = h.LocalidadeID " +
                         "LEFT OUTER JOIN Localidades i WITH (NOLOCK)  ON f.PaisID = i.LocalidadeID " +
                         "WHERE a.RecursoID = " + sujObjID + " AND a.TipoRecursoID = b.TipoObjetoID " +
                         "AND b.TipoRelacaoID =  " + tipoRelID + " AND b.TipoSujeitoID = c.TipoPessoaID " +
                         "AND c.EstadoID = 2 " +
                         "AND c.PessoaID NOT IN (SELECT a.SujeitoID FROM RelacoesPesRec a WITH (NOLOCK)  " +
                         "WHERE a.ObjetoID= " + sujObjID + " AND a.TipoRelacaoID=" + tipoRelID + " ) " +
                         "AND c.Nome " + stroperator + "'" + strtoFind + "' " +
                         "ORDER BY fldName ";
            }

            // Gera o resultado para o usuario.
            WriteResultXML(DataInterfaceObj.getRemoteData(strSQL));
        }
    }
}
