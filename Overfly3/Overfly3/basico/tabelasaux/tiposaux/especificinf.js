/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de TiposAuxiliares
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
var glbColObservacoes = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.TipoID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM TiposAuxiliares a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.TipoID = '+idToFind;
        return 'dso01JoinSup' + '_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20201) // Itens
    {
        dso.SQL = 'SELECT a.*, LEFT(CONVERT(VARCHAR(8000),Observacoes),30) + (CASE CONVERT(VARCHAR(8000),Observacoes) WHEN SPACE(0) THEN ' +
			'SPACE(0) ELSE ' + '\'' + '...' + '\'' + 'END) AS ObsAbreviado FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.TipoID = ' + idToFind + ' ' +
                  'ORDER BY a.Ordem';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
       
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Itens
    else if ( pastaID == 20201 )
    {
        if ( dso.id == 'dsoStateMachineLkp' )
        {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM TiposAuxiliares_Itens a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.TipoID = '+ nRegistroID + ' AND a.EstadoID = b.RecursoID';
        }
    }        
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(20201);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20201); //Itens

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 20201) // Itens
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[4], ['TipoID', registroID], [glbColObservacoes]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var lFeminino;
    var lAbreviacao;
    var lNumero;
    var lFiltro;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 20201) // Itens
    {
        lFeminino = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
            "dsoSup01.recordset['Feminino'].value");

        lAbreviacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
            "dsoSup01.recordset['Abreviacao'].value");        

        lNumero = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
            "dsoSup01.recordset['Numero'].value");        

        lFiltro = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
            "dsoSup01.recordset['Filtro'].value");

        var i = 0;
        var j = 0;
        var aHeader = new Array();
        var aFields = new Array();
        var aMask = new Array();
        var aColAligns = new Array();
        var colObsHidden = 0;
        
        aColAligns[j] = i;
        j++;
        
        aHeader[i] = 'ID'+replicate(' ',10);
        aFields[i] = 'ItemID*';
        aMask[i] = '';
        i++;
        
        aHeader[i] = 'Est';
        aFields[i] = '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*';
        aMask[i] = '';
        i++;
        
        aColAligns[j] = i;
        j++;

        aHeader[i] = 'Ordem';
        aFields[i] = 'Ordem';
        aMask[i] = '999';
        i++;

        aHeader[i] = 'Default';
        aFields[i] = 'EhDefault';
        aMask[i] = '';
        i++;

        aHeader[i] = 'Aplicar';
        aFields[i] = 'Aplicar';
        aMask[i] = '';
        i++;

        aHeader[i] = '�tem Masculino'+replicate(' ',15);
        aFields[i] = 'ItemMasculino';
        aMask[i] = '';
        i++;

        if (lFeminino)
        {
            aHeader[i] = '�tem Feminino';
            aFields[i] = 'ItemFeminino';
	        aMask[i] = '';
            i++;
        }
        if (lAbreviacao)
        {
            aHeader[i] = 'Abrev';
            aFields[i] = 'ItemAbreviado';
	        aMask[i] = '';
            i++;
        }
        if (lNumero)
        {
			aColAligns[j] = i;
			j++;

            aHeader[i] = 'N�mero';
            aFields[i] = 'Numero';
	        aMask[i] = '999999999.99';
            i++;
        }
        if (lFiltro)
        {
            aHeader[i] = 'Filtro';
            aFields[i] = 'Filtro';
	        aMask[i] = '';
            i++;
        }
        aHeader[i] = 'Observa��o'+replicate(' ',20);
        aFields[i] = 'Observacao';
        aMask[i] = '';
        i++;

        aHeader[i] = 'Observa��es'+replicate(' ',20);
        aFields[i] = '_calc_ObsAbreviado_30*';
        aMask[i] = '';
        i++;

        glbColObservacoes = i;
        colObsHidden = i;
        aHeader[i] = 'Observa��es';
        aFields[i] = 'Observacoes';
        aMask[i] = '';
        i++;

        aHeader[i] = 'ItemID';
        aFields[i] = 'ItemID';
        aMask[i] = '';
    
        headerGrid(fg,aHeader, [colObsHidden, i]);
        fillGridMask(fg,currDSO,aFields,aMask);

		for (i=1; i<fg.Rows; i++)
		{
			currDSO.recordset.MoveFirst();
			var currID = fg.TextMatrix(i,fg.Cols-1);

			currDSO.recordset.find('ItemID', currID);
			
			if (!currDSO.recordset.EOF)
			{
				if (currDSO.recordset['ObsAbreviado'].value != null)
					fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = currDSO.recordset['ObsAbreviado'].value;
				else
					fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = '';
			}
			else
				fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_ObsAbreviado_30*')) = '';
		}
        
        fg.FrozenCols = 1;

        alignColsInGrid(fg,aColAligns);

        // Permite ordena��o pelo header do grid
        fg.ExplorerBar = 5;
    }    
}
