/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form relpessoasconceitos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
var glb_first = true;
var glb_contexto = 0;
var glb_TimerClick = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload() {
    windowOnLoad_1stPart();

    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Rela��o', 'SujeitoID', 'Sujeito', 'ObjetoID', 'Objeto', 'Propriet�rio', 'Alternativo');
    

    adjustElementsInForm([['lblRefrInf', 'chkRefrInf', 3, 1],
                          ['lblOrdem', 'chkOrdem', 3, 1, -9],
                          ['lblRegistrosVencidos', 'chkRegistrosVencidos', 3, 1, -9],
                          ['lblProprietariosPL', 'selProprietariosPL', 18, 1, -5],
                          ['lblRegistros', 'selRegistros', 6, 1, -3],
                          ['lblMetodoPesq', 'selMetodoPesq', 9, 1, -9],
                          ['lblPesquisa', 'selPesquisa', 14, 1, -4],
                          ['lblArgumento', 'txtArgumento', 12, 1, -4],
                          ['lblFiltro', 'txtFiltro', 50, 1, -4]]);

    windowOnLoad_2ndPart();

    selMetodoPesq.selectedIndex = 3;
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    // seta botao de impressao
    especBtnIsPrintBtn('sup', 1);

    // mostra quatro botoes especificos desabilitados
    nConceitoID = getCmbCurrDataInControlBar('sup', 1);

    // Produtos
    if (nConceitoID[1] == '2131') {
        showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Sujeito', 'Detalhar Objeto', 'Detalhar Lista de Pre�os', 'Kardex', 'Impostos', 'Gerenciamento de Produtos', 'Dados de Fornecedores', 'Lojas Web', 'Vendas Digitais', 'Gerenciar Proprietarios', 'Gerenciar Produtos Cotador']);
    }
    else {
        showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', '', '', '', '']);
    }

    if (fg.Rows > 1) {
        // Produtos
        if (nConceitoID[1] == '2131')
            setupEspecBtnsControlBar('sup', 'HHHHHHHHHHHHHH');
        else
            setupEspecBtnsControlBar('sup', 'HHHHHDHDHDHHD');
    }
    else {
        if (nConceitoID[1] == '2131')
            setupEspecBtnsControlBar('sup', 'HHHDDDDDHDHHHH');
        else
            setupEspecBtnsControlBar('sup', 'HHHDDDDDDDDDD');
    }

    btnGetProps.style.visibility = 'hidden';

    if (glb_first == true) {
        carregaProprietarios();
        glb_contexto = contexto[1];
    } else if (glb_contexto != contexto[1]) {
        carregaProprietarios();
        glb_first = true;
        glb_contexto = contexto[1];
    }

    lblProprietariosPL.title = 'Clique simples: recarrega o combo \n' +
                              'Clique duplo: alterna o combo entre Propriet�rio e Alternativo e recarrega o combo';

    // Proprietario
    lblProprietariosPL.onmouseover = lblProprietariosPL_onmouseover;
    lblProprietariosPL.onmouseout = lblProprietariosPL_onmouseout;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var empresa = getCurrEmpresaData();

    if (controlBar == 'SUP') {
        // Usuario clicou botao documentos
        if (btnClicked == 1) {
            if (fg.Rows > 1) {
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }
            // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();
        else if (btnClicked == 3)
            window.top.openModalControleDocumento('PL', '', 740, null, '22', 'T');
        else if (btnClicked == 4)
            // Manda o id da pessoa a detalhar
            sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], getCellValueByColKey(fg, 'SujeitoID', fg.Row)));
            // Detalhar objeto
        else if (btnClicked == 5)
            // Manda o id do conceito a detalhar
            sendJSCarrier(getHtmlId(), 'SHOWCONCEITO', new Array(empresa[0], getCellValueByColKey(fg, 'ObjetoID', fg.Row)));
            // Lista de pre�os
        else if (btnClicked == 6)
            sendJSCarrier(getHtmlId(), 'SHOWLISTAPRECOS', new Array(empresa[0], dsoSup01.recordset['ObjetoID'].value));
            // Custo
        else if (btnClicked == 7)
            openModalKardex(getCellValueByColKey(fg, 'RelacaoID', fg.Row));
            //Imposto
        else if (btnClicked == 8)
            openModalImpostos();
            //Gerenciamento De Produtos
        else if (btnClicked == 9)
            openmodalgerenciamentoprodutos(btnClicked);
            //Fornecedores
        else if (btnClicked == 10)
            openmodalfornecedores(btnClicked);
            //Lojas Web
        else if (btnClicked == 11)
            openModalLojasWeb();
            //Marketplace
        else if (btnClicked == 12)
            openModalVendasDigitais();
            //Gerenciamento de Proprietarios
        else if (btnClicked == 13)
            openModalGerenciarProprietarios();
        else if (btnClicked == 14)
            openmodalGerenciarProdutosCotador();
    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal do carrinho de compras

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function openModalImpostos() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 770;
    var nHeight = 440;

    var nA1 = getCurrRightValue('SUP', 'A1');

    var nA2 = getCurrRightValue('SUP', 'A2');

    var nDireito = 0;

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Impostos
    //21060 SFS-Grupo Relacao entre PesCon-> A1&&A2 -> Libera bot�o gravar.           
    if ((nA1 == 1) && (nA2 == 1))
        nDireito = 1;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nDireito=' + escape(nDireito);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalimpostospesqlist.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal Gerenciamento de Produtos

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function openmodalgerenciamentoprodutos(btnClicked) {
    var htmlPath;
    var strPars = new String();
    var nWidth = 1002;
    var nHeight = 600;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nModalType=' + escape(btnClicked);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modalgen/modalgerenciamentoprodutos.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal Fornecedores

Parametro:
btnClicked

Retorno:
nenhum
********************************************************************/
function openmodalfornecedores(btnClicked) {
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 570;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nModalType=' + escape(btnClicked);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modalgen/modalfornecedores.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal das lojas da Web

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalLojasWeb() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 570;
    var i;

    var nA1 = getCurrRightValue('SUP', 'B7A1');
    var nA2 = getCurrRightValue('SUP', 'B7A2');

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nA1=' + escape(nA1);
    strPars += '&nA2=' + escape(nA2);

    // o combo de numeros de registros
    for (i = 0; i < selRegistros.length; i++) {
        strPars += '&sCmbText=' + escape(selRegistros.options[i].innerText);
        strPars += '&nCmbValue=' + escape(selRegistros.options[i].value);

        if (i == selRegistros.selectedIndex)
            strPars += '&nCmbOptionSel=' + escape(1);
        else
            strPars += '&nCmbOptionSel=' + escape(0);
    }

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modallojasweb.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Vendas Digitais

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalVendasDigitais() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 570;
    var i;
    var userID = getCurrUserID();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];

    var nA1 = getCurrRightValue('SUP', 'B12A1');
    var nA2 = getCurrRightValue('SUP', 'B12A2');

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nA1=' + escape(nA1);
    strPars += '&nA2=' + escape(nA2);
    strPars += '&nUsuarioID=' + escape(userID);
    strPars += '&nEmpresaID=' + escape(empresaID);

    // o combo de numeros de registros
    for (i = 0; i < selRegistros.length; i++) {
        strPars += '&sCmbText=' + escape(selRegistros.options[i].innerText);
        strPars += '&nCmbValue=' + escape(selRegistros.options[i].value);

        if (i == selRegistros.selectedIndex)
            strPars += '&nCmbOptionSel=' + escape(1);
        else
            strPars += '&nCmbOptionSel=' + escape(0);
    }

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalvendasdigitais.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}



/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
        // Modal de manutencao de produtos
    else if (idElement.toUpperCase() == 'MODALIMPOSTOSPESQLISTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            if (param2 == 'NOREG')
                lockInterface(false);

            // nao mexer
            return 0;
        }
    }
        // Modal de gerenciamento de produtos
    else if (idElement.toUpperCase() == 'MODALGERENCIAMENTOPRODUTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            if (param2 == 'NOREG')
                lockInterface(false);

            // nao mexer
            return 0;
        }
    }

        // Modal de fornecedores
    else if (idElement.toUpperCase() == 'MODALFORNECEDORESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            if (param2 == 'NOREG')
                lockInterface(false);

            // nao mexer
            return 0;
        }
    }

        // Modal de lojas da web
    else if (idElement.toUpperCase() == 'MODALLOJASWEBHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }

        // Modal Vendas Digitais
    else if (idElement.toUpperCase() == 'MODALVENDASDIGITAISHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }

        // Modal de GERENCIAR PROPRIETARIOS
    else if (idElement.toUpperCase() == 'MODALGERENCIARPROPRIETARIOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }

        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    // Modal do Kardex
    if (idElement.toUpperCase() == 'MODALKARDEXHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();


            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    // Modal Geranciar Produtos Cotador
    else if (idElement.toUpperCase() == 'MODALGERENCIARPRODUTOSCOTADORHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch() {

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (fg.disabled == false)
        fg.focus();

    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var empresaCidadeID = empresaData[2];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nEmpresaCidadeID=' + escape(empresaCidadeID);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nIdiomaID=' + escape(getDicCurrLang());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(740, 350));
}

/********************************************************************
Funcao criada pelo programador.
retorna um array com os id's que estao no grid no momento

Parametro:
nenhum

Retorno:
array com os id's que estao no grid no momento
********************************************************************/
function retArrayGrid() {
    if (fg.Rows <= 1)
        return null;

    var aGrid = new Array(fg.Rows - 1);
    var i;

    for (i = 1; i < fg.Rows; i++)
        aGrid[i - 1] = fg.TextMatrix(i, 0);

    return aGrid;
}
function openModalKardex(RegistroID, ProdutoBloqueado) {
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 543;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)    
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nRegistroID=' + escape(RegistroID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalkardex.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}
/********************************************************************
Retorno de usuario clicou o botao do combo de proprietarios

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function btnGetProps_onClick_DSC() {
    var optionStr, optionValue;

    clearComboEx([selProprietariosPL.name]);

    while (!dsoPropsPL.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoPropsPL.recordset['Proprietario'].value;
        oOption.value = dsoPropsPL.recordset['ProprietarioID'].value;
        selProprietariosPL.add(oOption);
        dsoPropsPL.recordset.MoveNext();
    }

    if (glb_first == true) {
        carregaProprietarios();
        glb_first = false;
    }
    else {
        lockInterface(false);
    }
}

function carregaProprietarios() {
    lockInterface(true);
    var cmbContextData = getCmbCurrDataInControlBar('sup', 1);
    var cmbFiltroPadraoData = getCmbCurrDataInControlBar('sup', 2);
    var contextID = cmbContextData[1];
    var FiltroPadraoID = cmbFiltroPadraoData[1];
    var Proprietario = 0;
    var RegistrosVencidos = 0;

    // Registros Vencidos
    if (chkRegistrosVencidos.checked)
        RegistrosVencidos = 1;
    else
        RegistrosVencidos = 0;

    // Parametros para o servidor
    var strPars = new String();
    strPars = '?';

    var subFormID = window.top.subFormID;  // variavel global do browser filho    

    // formID
    strPars += 'nSubFormID=';
    strPars += encodeURIComponent(subFormID.toString());
    // contextoID
    strPars += '&nContextoID=';
    strPars += encodeURIComponent(contextID.toString());
    //FiltroPadraoID
    strPars += '&nFiltroPadraoID=';
    strPars += encodeURIComponent(FiltroPadraoID.toString());
    // RegistrosVencidos
    strPars += '&bRegistrosVencidos=' + encodeURIComponent(RegistrosVencidos);
    // empresaID
    strPars += '&nEmpresaID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_COMBODATA, 'VALUE', null)).toString());
    // userID
    strPars += '&nUsuarioID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_WIDEMSG, '__CURRUSERID', null)).toString());
    strPars += '&bAllList=' + encodeURIComponent(1);
    // Resultado
    strPars += '&bResultado=' + encodeURIComponent((glb_modoComboProprietario == true ? 1 : 0));

    dsoPropsPL.URL = SYS_ASPURLROOT + '/serversidegenEx/cmbspropspl2.aspx' + strPars;
    dsoPropsPL.ondatasetcomplete = btnGetProps_onClick_DSC;
    dsoPropsPL.refresh();
}


function lblProprietariosPL_onClick() {
    glb_TimerClick = window.setInterval('lblProprietariosPL_onClick_Continue()', 10, 'JavaScript');;
}

function lblProprietariosPL_onClick_Continue() {
    if (glb_TimerClick != null) {
        window.clearInterval(glb_TimerClick);
        glb_TimerClick = null;
    }

    carregaProprietarios();
}

function lblProprietariosPL_ondblClick() {
    glb_modoComboProprietario = !glb_modoComboProprietario;

    lblProprietariosPL.innerText = (glb_modoComboProprietario == true ? 'Propriet�rio' : 'Alternativo');

    carregaProprietarios();
}


function lblProprietariosPL_onmouseover() {
    lblProprietariosPL.style.color = 'blue';
    lblProprietariosPL.style.cursor = 'hand';
}

function lblProprietariosPL_onmouseout() {
    lblProprietariosPL.style.color = 'black';
    lblProprietariosPL.style.cursor = 'point';
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal das lojas da Web

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalGerenciarProprietarios() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 570;
    var i;

    var nA1 = getCurrRightValue('SUP', 'B7A1');
    var nA2 = getCurrRightValue('SUP', 'B7A2');

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nA1=' + escape(nA1);
    strPars += '&nA2=' + escape(nA2);

    // o combo de numeros de registros
    for (i = 0; i < selRegistros.length; i++) {
        strPars += '&sCmbText=' + escape(selRegistros.options[i].innerText);
        strPars += '&nCmbValue=' + escape(selRegistros.options[i].value);

        if (i == selRegistros.selectedIndex)
            strPars += '&nCmbOptionSel=' + escape(1);
        else
            strPars += '&nCmbOptionSel=' + escape(0);
    }

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalgerenciarproprietarios.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}


/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Vendas Digitais

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openmodalGerenciarProdutosCotador() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 570;
    var i;
    var userID = getCurrUserID();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];

    var nA1 = getCurrRightValue('SUP', 'B12A1');
    var nA2 = getCurrRightValue('SUP', 'B12A2');

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nA1=' + escape(nA1);
    strPars += '&nA2=' + escape(nA2);
    strPars += '&nUsuarioID=' + escape(userID);
    strPars += '&nEmpresaID=' + escape(empresaID);

    // o combo de numeros de registros
    for (i = 0; i < selRegistros.length; i++) {
        strPars += '&sCmbText=' + escape(selRegistros.options[i].innerText);
        strPars += '&nCmbValue=' + escape(selRegistros.options[i].value);

        if (i == selRegistros.selectedIndex)
            strPars += '&nCmbOptionSel=' + escape(1);
        else
            strPars += '&nCmbOptionSel=' + escape(0);
    }

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalgerenciarprodutoscotador.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth, nHeight));
}