/********************************************************************
modalimpostos.js

Library javascript para o modalimpostos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_bEnableGravar = false;
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoGen02 = new CDatatransport("dsoGen02");
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Eventos de grid
********************************************************************/
function fg_md_CellChanged()
{
    if ( fg.Editable == true )
    {
        if ( (fg.Col == 2) || (fg.Col == 3) && (fg.Row > 0))
            btnOK.disabled = false;
    }
}

function fg_AfterEdit_modalImpostos(Row, Col)
{
	if ( (Col == getColIndexByColKey(fg, 'Aliquota' + glb_sEditable)) ||
		 (Col == getColIndexByColKey(fg, 'BaseCalculo' + glb_sEditable)) )
		fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalimpostosDblClick(grid, Row, Col)
{
    if (Row > 0)
    {
        var sNCM = fg.TextMatrix(Row, getColIndexByColKey(fg, 'NCM*'));
        sNCM = sNCM.substring(0,4);

        strPars = '?sNCM=' + escape(sNCM);
        htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.asp' + strPars;
        window.open(htmlPath);
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Impostos', 1);
    
    loadDataAndTreatInterface();
    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o divFG
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = 40;
    }

	adjustElementsInForm([['lblProdutoSimilar','selProdutoSimilar',30,1,-10,-10]], null, null, true);
	selProdutoSimilar.onchange = selProdutoSimilar_onchange;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFields.offsetTop + divFields.offsetHeight;
        width = modWidth - frameBorder - 2 * ELEM_GAP;            
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }

    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

function selProdutoSimilar_onchange()
{
	glb_bEnableGravar = true;
	setLabelOfControl(lblProdutoSimilar, selProdutoSimilar.value);
	getDataInServer();
}

/********************************************************************
********************************************************************/
function detalhaNCMSuperior()
{
	sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'detalhaNCMSuperior()');
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer()
{
    var strPars = new String();
    var NCM = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getNCM()');
    
    // mandar os parametros para o servidor
    strPars = '?nRelacaoID=' + escape(glb_nRelacaoID);
    strPars += '&nPos=' + escape(glb_nPos);
    strPars += '&nRelacaoSimilarID=' + escape(selProdutoSimilar.value);
    strPars += '&sNCM=' + (NCM != null ? NCM : '');

    dsoGen01.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/impostosdata.aspx' + strPars;
    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC()
{
    fg.Editable = false;
    
    // Produto
    if ( glb_nPos == 0 )
    {
        headerGrid(fg,['NCM',
					   'Tipo',
                       'Imposto',
                       'Aliquota',
                       'Base',
                       '_calc_HoldKey_1',
                       'RelPesConImpostoID'],
                       [5, 6]);
    }
    // Fornecedores
    else if ( glb_nPos == 1 )
    {
        headerGrid(fg,['NCM',
					   'Tipo',
                       'Imposto',
                       'Aliquota',
                       'Base',
                       '_calc_HoldKey_1',
                       'RelPesConImpostoID'],
                       [1, 5, 6]);
    }

    glb_aCelHint = [[0,0,'Clique duas vezes no NCM para abrir o documento NCM']];

    fillGridMask(fg,dsoGen01,['NCM*',
							  'Tipo*',
                              'Imposto*',
                              'Aliquota' + glb_sEditable,
                              'BaseCalculo' + glb_sEditable,
                              '_calc_HoldKey_1',
                              'RelPesConImpostoID'],
                              ['','','','999.99','999.999','',''],
                              ['','','','###.00','###.000','','' ]);
       
    alignColsInGrid(fg,[3,4]);
    
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar as colunas 2 e 3
    if ( fg.Rows > 1 ) 
    {
        if ( glb_sEditable == '' )
        {
            fg.Col = 3;
            fg.Editable = true;
        }
        else if ( glb_sEditable == '*' )
            fg.Col = 1;
    }

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    with (modalimpostosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
    
    if (glb_bEnableGravar)
    {
		glb_bEnableGravar = false;
		btnOK.disabled = false;
	}
	if (glb_sEditable == '*')
	    btnOK.disabled = true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOKClicked()
{
    var i, j;
    var aDataToSave = new Array();
    
    // Verifica colunas RelPesConImpostoID, Aliquota e BaseCalculo
    // do grid
    j = 0;
    for ( i=1; i<fg.Rows; i++)
    {
        fg.TextMatrix(i, 3) = trimAllSpacesInStr(fg.TextMatrix(i, 3));
        //if ( (isNaN(parseFloat(fg.TextMatrix(i, 3)))) ||
        //     (parseFloat(fg.TextMatrix(i, 3)) == 0) )
        if ( isNaN(parseFloat(fg.TextMatrix(i, 3))) )
        {
            fg.TextMatrix(i, 3) = '';
            continue;
        }
        
        fg.TextMatrix(i, 4) = trimAllSpacesInStr(fg.TextMatrix(i, 4));
        //if ( (isNaN(parseFloat(fg.TextMatrix(i, 4)))) ||
        //     (parseFloat(fg.TextMatrix(i, 4)) == 0) )
        if ( isNaN(parseFloat(fg.TextMatrix(i, 4))) )
        {
            fg.TextMatrix(i, 4) = '';
            continue;
        }
            
        /* Colunas que serao gravadas do grid
         colunas: 05 - RelPesConImpostoID
                  02 - Aliquota
                  03 - BaseCalculo
        */
        aDataToSave[j] = new Array(fg.ValueMatrix(i, 6),
                                   fg.ValueMatrix(i, 3),
                                   fg.ValueMatrix(i, 4));
        j++;
    }
    
    if ( aDataToSave.length == 0 )
    {
        if ( window.top.overflyGen.Alert ('Nenhum dado de imposto foi alterado') == 0 )
            return null;
            
        lockControlsInModalWin(false);
        return null;
    }
    
    saveItens(aDataToSave, aDataToSave.length, aDataToSave[0].length);
}

/********************************************************************
Salva itens do grid de impostos
********************************************************************/
function saveItens(aData, aDataLen, aDataElemLen)
{
    var strPars = new String();
    var i, j;
    
    strPars = '?';
    strPars += 'nDataLen=' + escape(aDataLen);
    strPars += '&nDataElemLen=' + escape(aDataElemLen);
    
    for (i=0; i<aDataLen; i++)
    {
        for (j=0; j<aDataElemLen; j++)
            //strPars += '&aData' + j.toString() + '=' + escape(aData[i][j]);
            strPars += '&aData= ' + i.toString() + '/' + escape(aData[i][j]);
    }
    
    // pagina asp no servidor saveitemspedido.asp
    dsoGen02.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/saveimpostos.aspx' + strPars;
    dsoGen02.ondatasetcomplete = saveItens_DSC;
    dsoGen02.refresh();
}

/********************************************************************
Retorno da funcao que salva itens do grid de impostos
********************************************************************/
function saveItens_DSC()
{
    lockControlsInModalWin(false);       
    
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null );
}