/********************************************************************
modallojasweb.js

Library javascript para o modallojasweb.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;

var glb_timerUnloadWin = null;

// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_refreshGrid = null;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_callFillGridTimerInt = null;
var glb_firstColLoja = 0;
var glb_nItensCancelados = 0;
var glb_nItensAtualizados = 0;
var glb_bDivFGIsCollapse = false;
var glb_chkOK = new Array();
var glb_Alterar1;
var glb_ReadOnly;
var glb_nLojaCanal;
var glb_aLojas = new Array();

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbsInterface = new CDatatransport("dsoCmbsInterface");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoDireitosB7 = new CDatatransport("dsoDireitosB7");
var dsoDicasLojasClassGrid = new CDatatransport("dsoDicasLojasClassGrid");
var dsoTiposAuxiliares = new CDatatransport("dsoTiposAuxiliares");
var dsoAlternativoID = new CDatatransport("dsoAlternativoID");
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modallojasweb.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modallojasweb.ASP

js_fg_modallojaswebBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modallojaswebDblClick( grid, Row, Col)
js_modallojaswebKeyPress(KeyAscii)
js_modallojasweb_AfterRowColChange
js_modallojasweb_ValidateEdit()
js_modallojasweb_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // ajusta o body do html
    with (modallojaswebBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
    
	// carrega combos e mostra a janela modal
    fillCmbsData();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Lojas Web', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) 
    {
        frameRect[1] += 28;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    
    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';

    // ajusta o divPesquisa
    elem = window.document.getElementById('divPesquisa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 90;
    }

    // ajusta o divFG - Dica
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.currentStyle.top, 10) +
			parseInt(divPesquisa.currentStyle.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    adjustElementsInForm([['lblRegistros','selRegistros',6,1,-10,-10],
						  ['lblClassificacaoClienteID','selClassificacaoClienteID',22,1,-4],						  
                          ['lblLojaID','selLojaID',22,1],
						  ['lblFiltroID','selFiltroID',17,1,50],
						  ['lblProprietarioID', 'selProprietarioID', 21, 1],
                          ['lblAlternativoID', 'selAlternativoID', 21, 1],
						  ['lblFamiliaID','selFamiliaID',20,2,-10],
						  ['lblMarcaID','selMarcaID',20,2],						  
						  ['lblPesquisa','txtPesquisa',26,2],
						  ['lblOK', 'chkOK', 3, 2, -4],
						  ['lblNaoOK', 'chkNaoOK', 3, 2, -20],
						  ['lblLoja', 'chkLoja', 3, 2, -3],
						  ['lblCanal', 'chkCanal', 3, 2, -4],
						  ['lblDicaLoja', 'chkDicaLoja', 3, 2, -3],
						  ['lblEstoque','chkEstoque',3,2,-5],
						  ['btnListar','btn',btnOK.offsetWidth - 16,2,-5,-4],
						  ['btnGravar','btn',btnOK.offsetWidth - 16,2,2],
                          ['lblDicas2','txtDicas2',122,10,-10,40]], null, null, true);

	btnListar.style.height = btnOK.offsetHeight;
	btnGravar.style.height = btnOK.offsetHeight;
	lblDicas2.style.visibility = 'hidden';
	txtDicas2.style.height = 80;

	lblOK.style.left = parseInt(lblOK.style.left, 10) + 8;
	
	with (lblrdE.style)
	{
		backgroundColor = 'transparent';
		left = selLojaID.offsetLeft + selLojaID.offsetWidth + ELEM_GAP;
		top = lblLojaID.offsetTop;
		width = 10;
	}
		
	with (rdE.style)
	{
		backgroundColor = 'transparent';
		left = lblrdE.offsetLeft - 2;
		top = lblrdE.offsetTop + lblrdE.offsetHeight + ELEM_GAP - 5;
		width = '10pt';
	    height = '10pt';
	}
	rdE.onclick = radioEOu_onclick;
	rdE.checked = true;

	with (lblrdOu.style)
	{
		backgroundColor = 'transparent';
		left = lblrdE.offsetLeft + lblrdE.offsetWidth + ELEM_GAP;
		top = lblrdE.offsetTop;
		width = 10;
	}

	with (rdOu.style)
	{
		backgroundColor = 'transparent';
		left = lblrdOu.offsetLeft - 2;
		top = lblrdOu.offsetTop + lblrdOu.offsetHeight + ELEM_GAP - 5;
		width = '10pt';
	    height = '10pt';
	}
	rdOu.onclick = radioEOu_onclick;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    chkLoja.checked = true;
    chkCanal.checked = true;

    chkOK.checked = false;
    chkNaoOK.checked = true;

    chkOK.onclick = chkOK_onclick;
    chkNaoOK.onclick = chkNaoOK_onclick;
    
    chkLoja.onclick = chkLoja_onclick;
    chkCanal.onclick = chkCanal_onclick;
    
    chkDicaLoja.onclick = chkDicaLoja_onclick;

    txtPesquisa.onkeypress = execPesq_OnKey;

    DicasLojasClassGrid();
    //DicasClassGrid();

    selProprietarioID.onchange = selProprietarioID_onchage;
}

/********************************************************************
Usuario clicou um radio
********************************************************************/
function radioEOu_onclick()
{
	if ( this == rdE )
		rdOu.checked = !rdE.checked;
	else if ( this == rdOu )
		rdE.checked = !rdOu.checked;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnGravar')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnListar')
    {
        buscaLojasListaGrid();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    if (glb_refreshGrid != null)
    {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState(bPar)
{
	var i;
	
	if ( bPar == false)
		btnGravar.disabled = bPar;
	else
		btnGravar.disabled = true;	
}

function fillCmbsData()
{
	var strSQL = '';
	var nEmpresaID = glb_aEmpresaData[0];

    setConnection(dsoCmbsInterface);

    strSQL = 'SELECT 1 AS Indice1, 1 AS Indice2, -1 AS fldID, SPACE(0) AS fldName, NULL AS Atributo1, \'\' AS Atributo2 ' +
		'UNION ALL ' +
			'SELECT 1 AS Indice1, 2 AS Indice2, 0 AS fldID, ' + '\'' + 'Sem Loja' + '\'' + ' AS fldName, NULL AS Atributo1, \'\' AS Atributo2 ' +
		'UNION ALL ' +
			'SELECT 1 AS Indice1, (2 + a.Ordem) AS Indice2, a.ItemID AS fldID, a.ItemMasculino AS fldName, ' +
				'a.ItemMasculino AS Atributo1, ' +
				'CONVERT(VARCHAR(8000), ISNULL(a.Observacoes, \'\')) AS Atributo2 ' +
			'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
			'WHERE (a.EstadoID = 2 AND a.TipoID = 108) ' +
		'UNION ALL ' +
			'SELECT 2 AS Indice1, a.Ordem AS Indice2, b.RecursoID AS fldID, b.RecursoFantasia AS fldName, ' +
				'ISNULL(b.Filtro, SPACE(0)) AS Atributo1, ' +
				'NULL AS Atributo2 ' +
			'FROM RelacoesRecursos a WITH(NOLOCK) ' +
				'INNER JOIN Recursos b WITH(NOLOCK) ON (a.SujeitoID = b.RecursoID) ' +
			'WHERE (a.ObjetoID = 24230 AND a.TipoRelacaoID = 2 AND ' +
				'b.EstadoID = 2 AND b.ClassificacaoID = 12) ' +
		'UNION ALL ' +
			'SELECT DISTINCT 3 AS Indice1, 1 AS Indice2, 0 AS fldID, SPACE(0) AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2  ' +
		'UNION ALL ' +
			'SELECT DISTINCT 3 AS Indice1, 2 AS Indice2, c.ConceitoID AS fldID, c.Conceito AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2  ' +
			'FROM RelacoesPesCon a WITH(NOLOCK) ' +
				'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) ' +
				'INNER JOIN Conceitos c WITH(NOLOCK) ON (b.ProdutoID = c.ConceitoID) ' +
			'WHERE (a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) ' +
		'UNION ALL ' +
			'SELECT DISTINCT 4 AS Indice1, 1 AS Indice2, 0 AS fldID, SPACE(0) AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2  ' +
		'UNION ALL ' +
			'SELECT DISTINCT 4 AS Indice1, 2 AS Indice2, c.ConceitoID AS fldID, c.Conceito AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2 ' +
			'FROM RelacoesPesCon a WITH(NOLOCK) ' +
				'INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) ' +
				'INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MarcaID = c.ConceitoID) ' +
			'WHERE (a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) ' +
		'UNION ALL ' +
			'SELECT DISTINCT 5 AS Indice1, 1 AS Indice2, 0 AS fldID, SPACE(0) AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2  ' +
		'UNION ALL ' +
			'SELECT DISTINCT 5 AS Indice1, 2 AS Indice2, b.PessoaID AS fldID, b.Fantasia AS fldName, ' +
				'NULL AS Atributo1, ' +
				'NULL AS Atributo2 ' +
			'FROM RelacoesPesCon a WITH(NOLOCK) ' +
				'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) ' +
			'WHERE (a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) ' +
        'UNION ALL ' +		
			'SELECT 6 AS Indice1, 1 AS Indice2, -1 AS fldID, SPACE(0) AS fldName, NULL AS Atributo1, NULL AS Atributo2 ' +
		'UNION ALL ' +
			'SELECT 6 AS Indice1, 2 AS Indice2, 0 AS fldID, ' + '\'' + 'Sem Classifica��o' + '\'' + ' AS fldName, NULL AS Atributo1, NULL AS Atributo2 ' +
		'UNION ALL ' +
			'SELECT 6 AS Indice1, (2 + a.Ordem) AS Indice2, a.ItemID AS fldID, a.ItemMasculino AS fldName, ' +
				'a.ItemMasculino AS Atributo1, ' +
				'CONVERT(VARCHAR(8000), a.Observacoes) AS Atributo2 ' +
			'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
			'WHERE (a.EstadoID = 2 AND a.TipoID = 13 AND a.Filtro LIKE ' + '\'' + '%<CLI>%' + '\'' + ') ' +
        'ORDER BY Indice1, Indice2, fldName, fldID '; 			

    if (strSQL != null)
    {
        dsoCmbsInterface.SQL = strSQL;
        dsoCmbsInterface.ondatasetcomplete = dsoCmbsInterface_DSC;
        dsoCmbsInterface.Refresh();
    }
}

function dsoCmbsInterface_DSC()
{
	var optionStr;
	var optionValue;
	var atributo1;
	var atributo2;
	
    while (! dsoCmbsInterface.recordset.EOF )
    {
        optionStr = dsoCmbsInterface.recordset['fldName'].value;
        optionValue = dsoCmbsInterface.recordset['fldID'].value;
		atributo1 = dsoCmbsInterface.recordset['Atributo1'].value;
		atributo2 = dsoCmbsInterface.recordset['Atributo2'].value;

		var oOption = document.createElement("OPTION");
		oOption.text = optionStr;
		oOption.value = optionValue;
		oOption.setAttribute('Atributo1', atributo1, 1);
		oOption.setAttribute('Atributo2', atributo2, 1);

		if (dsoCmbsInterface.recordset['Indice1'].value == 1)
			selLojaID.add(oOption);
		if (dsoCmbsInterface.recordset['Indice1'].value == 2) {
		    if (dsoCmbsInterface.recordset['Indice2'].value == 2)
		        oOption.selected = true;
		    
		    selFiltroID.add(oOption);
		}
		if (dsoCmbsInterface.recordset['Indice1'].value == 3)
			selFamiliaID.add(oOption);
		if (dsoCmbsInterface.recordset['Indice1'].value == 4)
			selMarcaID.add(oOption);
		if (dsoCmbsInterface.recordset['Indice1'].value == 5)
			selProprietarioID.add(oOption);
		if (dsoCmbsInterface.recordset['Indice1'].value == 6)
		    selClassificacaoClienteID.add(oOption);

        dsoCmbsInterface.recordset.MoveNext();
    }

    selProprietarioID_onchage();

	selLojaID.disabled = (selLojaID.options.length == 0);
	selClassificacaoClienteID.disabled = (selClassificacaoClienteID.options.length == 0);	
	selFiltroID.disabled = (selFiltroID.options.length == 0);
	selFamiliaID.disabled = (selFamiliaID.options.length == 0);
	selMarcaID.disabled = (selMarcaID.options.length == 0);
	selProprietarioID.disabled = (selProprietarioID.options.length == 0);
	selOptByValueInSelect(getHtmlId(), 'selProprietarioID', glb_nUserID);

	showExtFrame(window, true);
	
	setupBtnsFromGridState();
	
	window.focus();
    selRegistros.focus();
}

function js_fg_modalLojasWebMouseMove(grid, Button, Shift, X, Y)
{
    //if (glb_GridIsBuilding == false)
    //    js_fg_MouseMove(grid, Button, Shift, X, Y);
    
    var currRow = fg.MouseRow;
    var currCol = fg.MouseCol;
    var sDescricao = '';

    dsoDicasLojasClassGrid.recordset.MoveFirst();

    if ((glb_currCellRow != currRow) || (glb_currCellCol != currCol))
    {
        glb_currCellRow = currRow;
        glb_currCellCol = currCol;

        if ((currRow == 0) && (currCol >= 12))
        {
            dsoDicasLojasClassGrid.recordset.Find('ItemAbreviado',  fg.TextMatrix(currRow, currCol));
            var sItemMasculino = '';

            if ((!dsoDicasLojasClassGrid.recordset.BOF) && (!dsoDicasLojasClassGrid.recordset.EOF)) {
                sItemMasculino = dsoDicasLojasClassGrid.recordset['ItemMasculino'].value;

                sDescricao = sItemMasculino + '\n';
                sDescricao += dsoDicasLojasClassGrid.recordset['Observacoes'].value;
            }

            preecheDica(sDescricao);
            window.status = sItemMasculino;
        }
    }

    //window.status = '';
}

function preecheDica(sTextoDicas)
{
    txtDicas2.value = sTextoDicas;
}

function findCmbIndexByOptionID(oCmb, nOptionID)
{
	var retVal = -1;
	var i = 0;
	
	for (i=0; i<oCmb.options.length; i++)
	{
		if (oCmb.options.item(i).value == nOptionID)
		{
			retVal = i;
			break;
		}
	}
		
	return retVal;
}

function DicasLojasClassGrid() 
{
    setConnection(dsoDicasLojasClassGrid);
    dsoDicasLojasClassGrid.SQL = 'SELECT ItemID, ItemMasculino, ItemAbreviado, Observacoes FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID IN (13, 108)';
    dsoDicasLojasClassGrid.ondatasetcomplete = DicasLojasGrid_DSC;
    dsoDicasLojasClassGrid.Refresh();
}

function DicasLojasGrid_DSC() 
{
    return null;
}
/*
function DicasClassGrid() {
    setConnection(dsoDicasClassGrid);
    dsoDicasClassGrid.SQL = 'SELECT ItemID, ItemMasculino, ItemAbreviado, Observacoes FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID = 13';
    dsoDicasClassGrid.ondatasetcomplete = DicasClassGrid_DSC;
    dsoDicasClassGrid.Refresh();
}

function DicasClassGrid_DSC() {
    return null;
}
*/
function DireitosB7()
{
    var aEmpresa = getCurrEmpresaData();
    
    // Direitos Bot�o 3
    setConnection(dsoDireitosB7);
    dsoDireitosB7.SQL = 'SELECT TOP 1 Direitos.Alterar1, Direitos.Alterar2 ' +
          'FROM RelacoesPesRec RelPesRec WITH(NOLOCK), RelacoesPesRec_Perfis RelPesRecPerfis WITH(NOLOCK), Recursos Perfis WITH(NOLOCK), Recursos Contextos WITH(NOLOCK), Recursos_Direitos Direitos WITH(NOLOCK) ' +
          'WHERE (RelPesRec.SujeitoID IN ((SELECT ' + glb_nUserID + 
                  ' UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = ' + glb_nUserID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
                  'RelPesRec.ObjetoID = 999 AND ' +
            'RelPesRec.TipoRelacaoID = 11 AND RelPesRec.RelacaoID = RelPesRecPerfis.RelacaoID AND  ' +
            'RelPesRecPerfis.EmpresaID = ' + aEmpresa[0] + ' AND RelPesRecPerfis.PerfilID = Perfis.RecursoID AND  ' +
            'Perfis.EstadoID = 2 AND Perfis.RecursoID = Direitos.PerfilID AND Direitos.RecursoMaeID = 21060 AND ' +
            'Direitos.RecursoID = 40003 AND Direitos.ContextoID = Contextos.RecursoID AND Contextos.RecursoID = 2131) ' +
          'ORDER BY Direitos.Alterar1 DESC, Direitos.Alterar2 DESC';
    
    dsoDireitosB7.ondatasetcomplete = dsoDireitosB7_DSC;
    dsoDireitosB7.Refresh();
}

function dsoDireitosB7_DSC()
{
    glb_Alterar1 = dsoDireitosB7.recordset['Alterar1'].value;
    
    if (glb_Alterar1 == 0)
        glb_ReadOnly = '*';
    else
        glb_ReadOnly = '';
}

/********************************************************************
Pesquisa se Enter em campos com esta key
********************************************************************/
function execPesq_OnKey() {
    txtPesquisa.value = trimStr(txtPesquisa.value);

    if ((this.id == 'txtPesquisa') && (event.keyCode != 13))
        fg.Rows = 1;

    if (event.keyCode == 13)
        buscaLojasListaGrid();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	DireitosB7();
	
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}

    lockControlsInModalWin(true);
    
	var strSQL = '';
	
    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    
    var nTipoPesquisa;
    
    if (rdE.checked)
		nTipoPesquisa = 1;
	else 
		nTipoPesquisa = 2;
		
	var nLojaID = selLojaID.value;
	var sLojaID = '';
	var nClassificacaoClienteID = selClassificacaoClienteID.value;
	var sClassificacaoClienteID = '';
	var sFiltro = selFiltroID.options.item(selFiltroID.selectedIndex).getAttribute('Atributo1', 1);
	var nFamiliaID = selFamiliaID.value;
	var sFamiliaID = '';
	var nMarcaID = selMarcaID.value;
	var sMarcaID = '';
	var nProprietarioID = selProprietarioID.value;
	var sProprietarioID = '';
	var nAlternativoID = selAlternativoID.value;
	var sAlternativoID = '';
	var sPesquisa = trimStr(txtPesquisa.value);
	var nLojaCanal = 3;
	
	// sem Loja
	if (nLojaID == -1)
		sLojaID = 'NULL';
	else		
		sLojaID = nLojaID;
	
	// sem Classifica��o
	if (nClassificacaoClienteID == -1)
		sClassificacaoClienteID = 'NULL';
	else		
		sClassificacaoClienteID = nClassificacaoClienteID;

	if (sFiltro != '') {
	    sFiltro += '  AND ';
	}

    if ((chkOK.checked) && (!chkNaoOK.checked))
        sFiltro += 'a.LojaOK = 1';
    else if ((!chkOK.checked) && (chkNaoOK.checked))
        sFiltro += 'a.LojaOK = 0';
	
	if (sFiltro == '')
		sFiltro = 'NULL';
	else		
		sFiltro = '\'' + sFiltro + '\'';
		
	if (nFamiliaID == 0)
		sFamiliaID = 'NULL';
	else		
		sFamiliaID = nFamiliaID;

	if (nMarcaID == 0)
		sMarcaID = 'NULL';
	else		
		sMarcaID = nMarcaID;

	if (nProprietarioID == 0)
		sProprietarioID = 'NULL';
	else		
	    sProprietarioID = nProprietarioID;

	if (nAlternativoID == 0)
	    sAlternativoID = 'NULL';
	else
	    sAlternativoID = nAlternativoID;

	if (sPesquisa == '')
		sPesquisa = 'NULL';
	else		
		sPesquisa = '\'' + sPesquisa + '\'';

    if ((chkLoja.checked) && (!chkCanal.checked))
        nLojaCanal = 1;
    else if ((!chkLoja.checked) && (chkCanal.checked))
        nLojaCanal = 2;
    else
        nLojaCanal = 3;

    glb_nLojaCanal = nLojaCanal;
	
	strSQL = 'EXEC sp_Lojas_Produtos ' +
		glb_aEmpresaData[0] + ', ' + glb_nUserID + ', ' +
		selRegistros.value + ', ' + nTipoPesquisa + ', ' +
		sLojaID + ', ' + sClassificacaoClienteID + ', ' + sFiltro + ', ' + sFamiliaID + ', ' + sMarcaID + ', ' +
		sProprietarioID + ', ' + sAlternativoID + ', ' + sPesquisa + ', ' + (chkEstoque.checked ? '1' : '0') + ', ' + nLojaCanal;

    if (strSQL != null)
    {
		// parametrizacao do dso dsoGrid
		setConnection(dsoGrid);
        dsoGrid.SQL = strSQL;
        dsoGrid.ondatasetcomplete = fillGridData_DSC;
        dsoGrid.Refresh();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
        
    fg.Redraw = 0;
    fg.Rows = 1;

    fg.ExplorerBar = 0;
    
    // Gerenciamento de produtos
    var aColsHeaderGrid = new Array();
    var aColsFillGridMask_1 = new Array();
    var aColsFillGridMask_2 = new Array();
    var aColsFillGridMask_3 = new Array();
    var aColsTotais = new Array();
    
    var i, j;
    var nomeLoja;
    var nPos = 0;
    var nQuantFamilia, nQuantMarca;
    var aFamilia = new Array();
    var aMarca = new Array();
    
    aColsHeaderGrid = ['Familia', 
                   'Marca',
                   'Modelo',
                   'Descri��o',
                   'Estq',
				   'GP',
				   'E',
                   'ID',
                   'OK',
                   'RelacaoID',
                   'EhProprietario',
                   'EhAlternativo'];
                   
	aColsFillGridMask_1 = ['Familia*', 
                   'Marca*',
                   'Modelo*',
                   'Descricao*',
                   'Estoque*',
				   'IniciaisGP*',
				   'Estado*',
                   'ProdutoID*',
                   'chkOK' + glb_ReadOnly,
                   'RelacaoID*',
                   'EhProprietario*',
                   'EhAlternativo*'];
   
	glb_firstColLoja = aColsFillGridMask_1.length;
	fg.ColDataType(getColIndexByColKey(fg, 'chkOK' + glb_ReadOnly)) = 11; // format boolean (checkbox)
                   
    aColsFillGridMask_2 = ['','','','','','','','','','','',''];
	aColsFillGridMask_3 = ['','','','','','','','','','','',''];
	
	aColsHidden = [9,10,11];

    aColsTotais = [[0,'####','C'], [1,'####','C'], [2,'####','C']];
    
    // loop no dso para colunas dinamicas
    for ( i = 0; i < (dsoGrid.recordset.fields.count - 1); i++ )
    {
		nomeLoja = dsoGrid.recordset.fields[i].Name;
		nPos = 1;
		
		if (nomeLoja.substr(0,1) == '#')
		    nomeLoja = nomeLoja.substring(nPos, nomeLoja.length);
		else	
			nomeLoja = '';
		
		if ( nomeLoja.length > 0 )
		{
			aColsHeaderGrid[aColsHeaderGrid.length] = nomeLoja;
			aColsHeaderGrid[aColsHeaderGrid.length] = dsoGrid.recordset.fields[i + 1].Name;
			aColsFillGridMask_1[aColsFillGridMask_1.length] = dsoGrid.recordset.fields[i].Name;
			aColsFillGridMask_1[aColsFillGridMask_1.length] = dsoGrid.recordset.fields[i + 1].Name;
			aColsFillGridMask_2[aColsFillGridMask_2.length] = '';
			aColsFillGridMask_2[aColsFillGridMask_2.length] = '';
			aColsFillGridMask_3[aColsFillGridMask_3.length] = '';
			aColsFillGridMask_3[aColsFillGridMask_3.length] = '';
			aColsHidden[aColsHidden.length] = i + 1; 
		}
    }
    
    for (i = 0; i <= aColsHeaderGrid.length; i++)
    {
        glb_chkOK[i] = false;
    }
    
    // Grid nao tem dados
    if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
    {
		fg.AutoSizeMode = 0;
		fg.AutoSize(0,fg.Cols-1);
		fg.Redraw = 2;
	    
		// Controla se o grid esta em construcao
		glb_GridIsBuilding = false;    
	    
		if (fg.Rows > 2)
			fg.Editable = true;
		
		lockControlsInModalWin(false);

		// coloca foco no grid
		window.focus();
		fg.focus();

		// ajusta estado dos botoes
		setupBtnsFromGridState();
		
		return;
    }

    headerGrid(fg, aColsHeaderGrid, aColsHidden);

    glb_aCelHint = [[0, 1, '']];

    fillGridMask(fg,dsoGrid,
		aColsFillGridMask_1,
		aColsFillGridMask_2,
		aColsFillGridMask_3);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, aColsTotais);

    if (fg.Rows > 2)
    {
		nQuantFamilia = 0;
		nQuantMarca = 0;
		
        for (i = 2; i < fg.Rows; i++)
        {
			if ( ascan(aFamilia, fg.TextMatrix(i, 0), false) == -1 )
			{
				aFamilia[aFamilia.length] = fg.TextMatrix(i, 0);
				nQuantFamilia += 1;
			}

			if ( ascan(aMarca, fg.TextMatrix(i, 1), false) == -1 )
			{
				aMarca[aMarca.length] = fg.TextMatrix(i, 1);
				nQuantMarca += 1;
			}            
        }
        
        fg.TextMatrix(1, 0) = nQuantFamilia;
        fg.TextMatrix(1, 1) = nQuantMarca;
    }

    alignColsInGrid(fg,[4,7]);

    fg.FrozenCols = 3;
    
    fg.MergeCells = 4;
	fg.MergeCol(0) = true;
	fg.MergeCol(1) = true;
	
	fg.Redraw = 0;
    paintReadOnlyCols(fg);
    fg.Redraw = 2;
    
    fg.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg.Rows > 2 )
        fg.Row = 2;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    if (fg.Rows > 2)
        fg.Editable = true;
	
    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 2)
    {
        window.focus();
        fg.focus();
    }
    else
    {
        ;
    }            

    montaHint();

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function chkOK_onclick() 
{
    if ((!chkOK.checked) && (!chkNaoOK.checked))
        chkNaoOK.checked = true;
    /*
    if (chkOK.checked)
        chkNaoOK.checked = false;
    else if (!chkOK.checked)
        chkNaoOK.checked = true;
    */
}

function chkNaoOK_onclick() 
{
    if ((!chkOK.checked) && (!chkNaoOK.checked))
        chkOK.checked = true;
    /*
    if (chkNaoOK.checked)
        chkOK.checked = false;
    else if (!chkNaoOK.checked)
        chkOK.checked = true;
    */
}

function chkLoja_onclick() 
{
    if ((!chkLoja.checked) && (!chkCanal.checked))
        chkCanal.checked = true;
}

function chkCanal_onclick()
{
    if ((!chkLoja.checked) && (!chkCanal.checked))
        chkLoja.checked = true;
}

function chkDicaLoja_onclick()
{
	collapseExtendedDivFG();
}

function collapseExtendedDivFG()
{
    var nRowLine = fg.Row;
    var nTop = parseInt(divPesquisa.currentStyle.top, 10) + parseInt(divPesquisa.currentStyle.height, 10);
	
	fg.Redraw = 0;
	
	if (glb_bDivFGIsCollapse)
	{
		//divControlsGerProd.style.visibility = 'hidden';

	    divFG.style.height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(nTop, 10);
	    fg.style.height = parseInt(divFG.style.height, 10);

		//txtDicas.style.visibility = 'hidden';
	}
	else
	{
		fg.style.height = 337;
		divFG.style.height = 337;
		//txtDicas.style.visibility = 'inherit';
	}

	if ( fg.Rows > 2 )
		fg.TopRow = nRowLine;

	fg.Redraw = 2;
	
	glb_bDivFGIsCollapse = !glb_bDivFGIsCollapse;
}

function saveDataInGrid()
{
	var strPars = '';
	var nBytesAcum = 0;
	var nDataLen = 0;
	var i, j;
	var nExclui = 0;
	var bLojaGrid, bLojaBanco;
	var bEhProprietario = false;
	var bEhAlternativo = false;
	var nCont = 0;
	var nContAnt = 0;
	
	lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
	glb_nItensCancelados = 0;
	glb_nItensAtualizados = 0;

    for (i = 2; i < fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'EhProprietario*', i) != 0)
			bEhProprietario = true;
		else			
			bEhProprietario = false;

		if (getCellValueByColKey(fg, 'EhAlternativo*', i) != 0)
			bEhAlternativo = true;
		else			
			bEhAlternativo = false;

		// Direito
		//DireitoEspecifico
        //Relacao PesCon->Produtos->Modal Lojas Web
        //40007 B7 -> A1&&A2 || (A1||A2)&&Proprietario || A2&&Alternativo -> Grava altera��es do grid.
		if ( (glb_nA1 == 1 && glb_nA2 == 1) ||
			 ((glb_nA1 == 1 || glb_nA2 == 1) && bEhProprietario) ||
			 (glb_nA2 == 1 && bEhAlternativo) )
		{
			for ( j = glb_firstColLoja; j <fg.Cols - 1; j += 2 )
			{
				if ( fg.ValueMatrix(i, j) != 0 )
					bLojaGrid = true;
				else
					bLojaGrid = false;
					
				if ( fg.ValueMatrix(i, j + 1) != 0 )
					bLojaBanco = true;
				else
					bLojaBanco = false;
			
				// N�o grava nesse caso
				if (bLojaGrid == bLojaBanco)
					continue;
				
				nCont = nCont + 1;
				
				glb_nItensAtualizados += 1;

				if (bLojaGrid)
					nExclui = 0;
				else				
					nExclui = 1;
					
				nBytesAcum=strPars.length;
				
				if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
				{
					nBytesAcum = 0;
					if (strPars != '')
					{
						glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
						strPars = '';
						nDataLen = 0;
					}
			  		
  					nDataLen++;
					strPars += '?nRelacaoID=' + escape(getCellValueByColKey(fg, 'RelacaoID*', i));
					strPars += '&nLojaID=' + escape(parseInt(fg.TextMatrix(0, j + 1), 10));
					strPars += '&nExclui=' + escape(nExclui);
					strPars += '&nLojaOK=' + escape(Math.abs(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly))));
					                                
					continue;
					
				}

				nDataLen++;
				strPars += '&nRelacaoID=' + escape(getCellValueByColKey(fg, 'RelacaoID*', i));
				strPars += '&nLojaID=' + escape(parseInt(fg.TextMatrix(0, j + 1), 10));
				strPars += '&nExclui=' + escape(nExclui);
				strPars += '&nLojaOK=' + escape(Math.abs(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly))));
			}	
		}
		else
		{
			glb_nItensCancelados += 1;
		}
	
	    if ((strPars == '') && (nCont == nContAnt) && (glb_chkOK[i] == true))
	    {
    	    glb_nItensAtualizados += 1;
    	    
    	    strPars += '?nRelacaoID=' + escape(getCellValueByColKey(fg, 'RelacaoID*', i));
            strPars += '&nLojaID=' + '0';
			strPars += '&nExclui=' + '0';
			strPars += '&nLojaOK=' + escape(Math.abs(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly))));
	    }
	    else if ((nCont == nContAnt) && (glb_chkOK[i] == true))
	    {
	        glb_nItensAtualizados += 1;
	        
	        strPars += '&nRelacaoID=' + escape(getCellValueByColKey(fg, 'RelacaoID*', i));
            strPars += '&nLojaID=' + '0';
			strPars += '&nExclui=' + '0';
			strPars += '&nLojaOK=' + escape(Math.abs(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly))));
	    }
	    nContAnt = nCont;
	}
	
	if ((nDataLen > 0) || (strPars != ''))
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

	sendDataToServer();
}

function sendDataToServer()
{
	var sMensagem = '';

	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/gravarlojasweb.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			
			sMensagem = glb_nItensAtualizados.toString() + ' registros atualizados.\n' +
				glb_nItensCancelados.toString() + ' registros cancelados.';
			
			if ( window.top.overflyGen.Alert (sMensagem) == 0 )
				return null;

			glb_callFillGridTimerInt = window.setInterval('buscaLojasListaGrid()', 10, 'JavaScript');
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		return null;
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modallojaswebBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modallojaswebDblClick()
{
    //return true;
    var grid = fg;
    var row = fg.row;
    var col = fg.col;

    if ((fg.Row == 2) && (fg.ColDataType(col) == 11)) {
        var bOK;
        //((col == getColIndexByColKey(fg, 'OK')) || (col == getColIndexByColKey(fg, 'ModificacaoOK')))

        if (fg.TextMatrix(2, col) == "True")
            bOk = false;
        else
            bOK = true;

        for (var i = 2; i < grid.Rows; i++) {
            fg.TextMatrix(i, col) = bOK;

            js_modallojasweb_AfterEdit(i, col);
        }
    }

    if (fg.Row < 2)
        return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallojaswebKeyPress(KeyAscii)
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallojasweb_ValidateEdit()
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallojasweb_AfterEdit(Row, Col)
{
    var nType;

    if (fg.Editable)
    {
		if (fg.Row >= 2)
			setupBtnsFromGridState(false);
    }

    if ((Col  == getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly)) && (glb_Alterar1 == 1))
    {
        glb_chkOK[Row] = true;
    }
    
    else if ((Col  == getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly)) && (glb_Alterar1 == 0))
    {
        if (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly)) == 0)
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly)) = -1;
        else
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'ChkOK' + glb_ReadOnly)) = 0;
    }
    
}

/*
function fg_DblClick(grid, row, col) {
    if ((fg.Row == 2) && (fg.ColDataType(col) == 11))
    {
        var bOK;
        //((col == getColIndexByColKey(fg, 'OK')) || (col == getColIndexByColKey(fg, 'ModificacaoOK')))

        if (fg.TextMatrix(2, col) == "True")
            bOk = false;
        else
            bOK = true;

        for (var i = 2; i < grid.Rows; i++)
        {
            fg.TextMatrix(i, col) = bOK;

            js_modallojasweb_AfterEdit(i, col);
        }
    }

    if (fg.Row < 2)
        return true;
}
*/

function buscaLojasListaGrid() {
    
    setConnection(dsoTiposAuxiliares);

    var nLojaCanal;

    if ((chkLoja.checked) && (!chkCanal.checked))
        nLojaCanal = 1;
    else if ((!chkLoja.checked) && (chkCanal.checked))
        nLojaCanal = 2;
    else
        nLojaCanal = 3;

    dsoTiposAuxiliares.SQL = "SELECT a.ItemID, a.ItemAbreviado, a.ItemMasculino " +
                                "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
                                "WHERE (a.EstadoID = 2 AND  " +
    				                                "(((" + nLojaCanal + " = 3) AND ((a.TipoID = 108) OR (a.TipoID = 13 AND a.Filtro LIKE '%<CLI>%'))) " +
    					                                "OR ((" + nLojaCanal + " = 2) AND (a.TipoID = 13 AND a.Filtro LIKE '%<CLI>%')) " +
    					                                "OR ((" + nLojaCanal + " = 1) AND (a.TipoID = 108)))) " +
    			                                "ORDER BY a.TipoID DESC, a.Ordem ";

    dsoTiposAuxiliares.ondatasetcomplete = dsoTiposAuxiliares_DSC;
    dsoTiposAuxiliares.Refresh();
}

function dsoTiposAuxiliares_DSC() {

    while (!dsoTiposAuxiliares.recordset.EOF) {

        glb_aLojas.push(new Array(dsoTiposAuxiliares.recordset['ItemAbreviado'].value, dsoTiposAuxiliares.recordset['ItemMasculino'].value));

        dsoTiposAuxiliares.recordset.moveNext();
    }

    fillGridData();
}

function montaHint() {

    glb_aCelHint = [[0, 1, '']];

    asort(glb_aLojas, 0);

    var nColTipoAuxiliar = getColIndexByColKey(fg, 'EhAlternativo*') + 1;

    for (var i = nColTipoAuxiliar; i < fg.Cols; i++) {
        var sItemAbreviado = fg.TextMatrix(0, i);

        if (sItemAbreviado.indexOf('_') >= 0) {
            continue;
        }

        var sItemMasculino = aseek(glb_aLojas, sItemAbreviado, 0);

        if (sItemMasculino >= 0) {
            sItemMasculino = glb_aLojas[sItemMasculino][1];

            glb_aCelHint.push([0, i, sItemMasculino]);
        }
    }
}

function selProprietarioID_onchage() {

    var nProprietarioID = selProprietarioID.value;
    var strSQL = '';

    setConnection(dsoAlternativoID);
    
    strSQL = "SELECT DISTINCT 0 AS fldID, SPACE(0) AS fldName UNION ALL ";

    if (nProprietarioID == 0) {
        strSQL += "SELECT DISTINCT " +
	                            "b.PessoaID AS fldID, " +
	                            "b.Fantasia AS fldName " +
	                            "FROM RelacoesPesCon a WITH(NOLOCK) " +
	                            	"INNER JOIN Pessoas b WITH(NOLOCK) ON (a.AlternativoID = b.PessoaID) " +
	                            "WHERE (a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) " +
                                "ORDER BY fldName";
    } else {
        strSQL += "SELECT DISTINCT " +
	                            "b.PessoaID AS fldID, " +
	                            "b.Fantasia AS fldName " +
	                            "FROM RelacoesPesCon a WITH(NOLOCK) " +
	                                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.AlternativoID = b.PessoaID) " +
	                            "WHERE (a.TipoRelacaoID = 61 AND a.EstadoID IN (11,12,13,2,14,15)) AND (a.ProprietarioID = " + nProprietarioID.toString() + ") " +
                                 "ORDER BY fldName";
    }
    
    dsoAlternativoID.SQL = strSQL;

    dsoAlternativoID.ondatasetcomplete = dsoAlternativoID_DSC;
    dsoAlternativoID.Refresh();
}

function dsoAlternativoID_DSC() {

    var optionStr;
    var optionValue;

    selAlternativoID.disabled = false;

    clearComboEx(['selAlternativoID']);

    while (!dsoAlternativoID.recordset.EOF) {
        optionStr = dsoAlternativoID.recordset['fldName'].value;
        optionValue = dsoAlternativoID.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        selAlternativoID.add(oOption);

        dsoAlternativoID.recordset.MoveNext();
    }

    selAlternativoID.disabled = (selAlternativoID.options.length == 0);
}

// FINAL DE EVENTOS DE GRID *****************************************