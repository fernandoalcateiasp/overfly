/********************************************************************
modalfornecedor.js

Library javascript para o modalfornecedor.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalfornecedorBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtFornecedor').disabled == false )
        txtFornecedor.focus();

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Fornecedores');

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Fornecedor', 1);

    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divFornecedor
    elem = window.document.getElementById('divFornecedor');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width);
        height = 40;
        
    }
    
    // txtFornecedor
    elem = window.document.getElementById('txtFornecedor');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style)
    {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtFornecedor.onkeypress = txtFornecedor_onKeyPress;
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    elem.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtFornecedor').style.top);
        left = parseInt(document.getElementById('txtFornecedor').style.left) + parseInt(document.getElementById('txtFornecedor').style.width) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divFornecedor').style.top) + parseInt(document.getElementById('divFornecedor').style.height) + ELEM_GAP;
        width = temp + 221;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);
    
    headerGrid(fg,['Fornecedor',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s'], [2]);

    fg.Redraw = 2;
}

function txtFornecedor_onKeyPress()
{
    if ( event.keyCode == 13 )
        btnFindPesquisa_onclick();
}

function txtFornecedor_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    txtFornecedor.value = trimStr(txtFornecedor.value);
    
    changeBtnState(txtFornecedor.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtFornecedor.value);
}

function fg_DblClick()
{
    // Se tem Fornecedor selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , new Array(
                      fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 1)));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strFornecedor)
{
    lockControlsInModalWin(true);
    
    writeInStatusBar('child', 'Listando', 'cellMode' , true);
        
    var strPas = '?';
    strPas += 'strToFind='+escape(strFornecedor);
    strPas += '&nSujeitoID='+escape(glb_nSujeitoID);
    strPas += '&nRelacaoID='+escape(glb_nRelacaoID);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/pesqfornecedor.aspx' + strPas;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    headerGrid(fg,['Fornecedor',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s'], [2]);
    
    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Fantasia',
                             'Documento',
                             'Cidade',
                             'UF',
                             'Pais'],['','','','','','','']);
                           
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
        btnOK.disabled = true;    
        
    writeInStatusBar('child', 'cellMode', 'Fornecedores');    
}
