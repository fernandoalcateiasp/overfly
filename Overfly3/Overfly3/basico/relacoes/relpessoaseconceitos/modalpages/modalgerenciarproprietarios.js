/********************************************************************
modalgerenciarproprietarios.js

Library javascript para o modalgerenciarproprietarios.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_sUltimaPesquisa = '';
var glb_first = true;
var glb_contexto = 0;

// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_refreshGrid = null;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_lblProprietariodblClick = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_callFillGridTimerInt = null;
var glb_Proprietario_Combo = 1;
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbsInterface = new CDatatransport("dsoCmbsInterface");
var dsoCmbsProp = new CDatatransport("dsoCmbsProp");
var dsoCmbsAlterarPara = new CDatatransport("dsoCmbsAlterarPara");
var dsoGrava = new CDatatransport("dsoGrava");
var glb_nContador = 0;
var glb_nQuantidadeProdutos = 0;
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalgerenciarproprietarios.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalgerenciarproprietarios.ASP

js_fg_modalgerenciarproprietariosBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalgerenciarproprietariosDblClick( grid, Row, Col)
js_modalgerenciarproprietariosKeyPress(KeyAscii)
js_modalgerenciarproprietarios_AfterRowColChange
js_modalgerenciarproprietarios_ValidateEdit()
js_modalgerenciarproprietarios_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    // ajusta o body do html
    with (modalgerenciarproprietariosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = -27;

    // configuracao inicial do html
    setupPage();

    // carrega combos e mostra a janela modal
    fillCmbsData();
    fillcmbsProprietarios(glb_Proprietario_Combo);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Gerenciar Propriet�rios');

    secText('Gerenciar Propriet�rios', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 50;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';

    // ajusta o divPesquisa
    elem = window.document.getElementById('divPesquisa');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 1;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP - 10;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 90;
    }

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.currentStyle.top, 10) + 30 +
			parseInt(divPesquisa.currentStyle.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10);
    }

    with (fg.style) {
        left = 1;
        top = 2;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    adjustElementsInForm([
						  ['lblMarca', 'selMarca', 18, 1],
                          ['lblLinha', 'selLinha', 18, 1, -10],
                          ['lblFamilia', 'selFamilia', 18, 1, -10],
                          ['lblProprietarioID', 'selProprietarioID', 18, 1, -10],
                          ['lblPais', 'selPais', 18, 1, -10],
                          ['lblPendencias', 'chkPendencias', 3, 1],
						  ['btnListar', 'btn', btnOK.offsetWidth - 16, 1, 120],
                          ['lblAuto', 'chkAuto', 3, 2, 730],
                          ['lblAlterarPara', 'selAlterarPara', 17, 2],
						  ['btnCorrigir', 'btn', btnOK.offsetWidth - 16, 2, 10]], null, null, true);

    selMarca.style.width = 17 * FONT_WIDTH;
    selMarca.style.height = 100;

    selLinha.style.width = 17 * FONT_WIDTH;
    selLinha.style.height = 100;

    selFamilia.style.width = 17 * FONT_WIDTH;
    selFamilia.style.height = 100;

    selProprietarioID.style.width = 17 * FONT_WIDTH;
    selProprietarioID.style.height = 100;

    selPais.style.width = 17 * FONT_WIDTH;
    selPais.style.height = 100;

    btnListar.style.height = btnOK.offsetHeight;
    btnCorrigir.style.height = btnOK.offsetHeight;


    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    chkAuto.checked = true;
    HideShowAlternar();
    chkAuto.onclick = HideShowAlternar;

    lblProprietarioID.onmouseover = lblProprietariosPL_onmouseover;
    lblProprietarioID.onmouseout = lblProprietariosPL_onmouseout;
    //DicasClassGrid();
}

/********************************************************************
Usuario clicou um radio
********************************************************************
function radioEOu_onclick()
{
	if ( this == rdE )
		rdOu.checked = !rdE.checked;
	else if ( this == rdOu )
		rdE.checked = !rdOu.checked;
}
*/
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnCorrigir') {
        saveDataInGrid();
    }
    else if (controlID == 'btnListar') {
        fillGridData();
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    if (glb_refreshGrid != null) {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState(bPar) {
    var i;

    if (bPar == false)
        btnCorrigir.disabled = bPar;
    else
        btnCorrigir.disabled = true;
}

function fillcmbsProprietarios(Proprietarios) {
    var strSQL = '';
    lockControlsInModalWin(true);

    if (Proprietarios == 1) {
        strSQL =
            "SELECT DISTINCT " +
            "b.PessoaID AS fldID, " +
            "b.Fantasia AS fldName " +
            "FROM RelacoesPesCon a WITH(NOLOCK) " +
            "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) " +
            "WHERE (a.SujeitoID  IN (2, 10, 14, 21, 22, 7) " +
            "AND a.TipoRelacaoID = 61 " +
            "AND a.EstadoID IN (1,11,12,13,2,14,15)) " +
            "ORDER BY fldName, fldID ";
    } else {
        strSQL =
           "SELECT DISTINCT  " +
           "b.PessoaID AS fldID, " +
           "b.Fantasia AS fldName " +
           "FROM RelacoesPesCon a WITH(NOLOCK) " +
           "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.AlternativoID = b.PessoaID) " +
           "WHERE (a.SujeitoID  IN (2, 10, 14, 21, 22, 7) " +
           "AND a.TipoRelacaoID = 61 " +
           "AND a.EstadoID IN (1,11,12,13,2,14,15)) " +
           "ORDER BY fldName, fldID ";

    }
    if (strSQL != null) {
        dsoCmbsProp.SQL = strSQL;
        dsoCmbsProp.ondatasetcomplete = fillcmbsProprietarios_DSC;
        dsoCmbsProp.Refresh();
    }
}


function fillcmbsProprietarios_DSC() {
    var optionStr;
    var optionValue;
    clearComboEx(['selProprietarioID']);
    dsoCmbsProp.recordset.MoveFirst();

    while (!dsoCmbsProp.recordset.EOF) {
        optionStr = dsoCmbsProp.recordset['fldName'].value;
        optionValue = dsoCmbsProp.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selProprietarioID.add(oOption);
        dsoCmbsProp.recordset.MoveNext();
    }

    selProprietarioID.disabled = (selProprietarioID.options.length == 0);
    //selOptByValueInSelect(getHtmlId(), 'selProprietarioID', glb_nUserID);
    showExtFrame(window, true);
    lockControlsInModalWin(false);
    if (glb_lblProprietariodblClick)
        fillcmbsAlternarParaProprietario(glb_Proprietario_Combo);
    window.focus();
}


function fillCmbsData() {
    var strSQL = '';
    var nEmpresaID = glb_aEmpresaData[0];

    setConnection(dsoCmbsInterface);
    // Marca
    strSQL =
        "SELECT DISTINCT 1 AS Indice, " +
                "c.ConceitoID AS fldID, " +
                "c.Conceito AS fldName                 " +
            "FROM RelacoesPesCon a WITH(NOLOCK) " +
                "INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) " +
                "INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MarcaID = c.ConceitoID) " +
            "WHERE ((a.SujeitoID  IN (2, 10, 14, 21, 22, 7)) AND " +
            "(a.TipoRelacaoID = 61) AND " +
            "(a.EstadoID IN (1, 11, 12, 13,2,14,15))) " +
        "UNION ALL " +
        // Linha
        "SELECT DISTINCT 2 AS Indice, " +
                "c.ConceitoID AS fldID, " +
                "c.Conceito AS fldName      " +
            "FROM RelacoesPesCon a WITH(NOLOCK) " +
                "INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) " +
                "INNER JOIN Conceitos c WITH(NOLOCK) ON (b.LinhaProdutoID = c.ConceitoID) " +
            "WHERE ((a.SujeitoID  IN (2, 10, 14, 21, 22, 7)) AND " +
            "(a.TipoRelacaoID = 61) AND  " +
            "(a.EstadoID IN (1, 11,12,13,2,14,15))) " +
        "UNION ALL " +
        // Familia
        "SELECT DISTINCT 3 AS Indice, " +
                "c.ConceitoID AS fldID, " +
                "c.Conceito AS fldName                 " +
            "FROM RelacoesPesCon a WITH(NOLOCK) " +
                "INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) " +
                "INNER JOIN Conceitos c WITH(NOLOCK) ON (b.ProdutoID = c.ConceitoID) " +
            "WHERE ((a.SujeitoID  IN (2, 10, 14, 21, 22, 7)) AND " +// DUDU
            "(a.TipoRelacaoID = 61) AND " +
            "(a.EstadoID IN (1, 11,12,13,2,14,15))) " +
             "UNION ALL " +
        // Pais
        " SELECT DISTINCT 4 AS Indice, " +
            "dbo.fn_Pessoa_Localidade(a.SujeitoID, 1, NULL, NULL) as fldID, " +
            "dbo.fn_Localidade_Dado(dbo.fn_Pessoa_Localidade(a.SujeitoID, 1, NULL, NULL), 1) fldName " +
         "FROM RelacoesPesCon a WITH(NOLOCK) " +
        " WHERE ((a.SujeitoID  IN (2, 10, 14, 21, 22, 7)) AND " +
            "(a.TipoRelacaoID = 61) AND " +
            "(a.EstadoID IN (1, 11,12,13,2,14,15))) " +
        " ORDER BY Indice, fldName, fldID  ";


    if (strSQL != null) {
        dsoCmbsInterface.SQL = strSQL;
        dsoCmbsInterface.ondatasetcomplete = dsoCmbsInterface_DSC;
        dsoCmbsInterface.Refresh();
    }
}

function dsoCmbsInterface_DSC() {
    var optionStr;
    var optionValue;
    var aEmpresa = getCurrEmpresaData();

    while (!dsoCmbsInterface.recordset.EOF) {
        optionStr = dsoCmbsInterface.recordset['fldName'].value;
        optionValue = dsoCmbsInterface.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        if (dsoCmbsInterface.recordset['Indice'].value == 1)
            selMarca.add(oOption);
        else if (dsoCmbsInterface.recordset['Indice'].value == 2)
            selLinha.add(oOption);
        else if (dsoCmbsInterface.recordset['Indice'].value == 3)
            selFamilia.add(oOption);
        else if (dsoCmbsInterface.recordset['Indice'].value == 4) {
            selPais.add(oOption);
            if (oOption.value == aEmpresa[1]) {
                oOption.selected = true;
            }
        }


        dsoCmbsInterface.recordset.MoveNext();
    }
    window.focus();
}

function js_fg_modalgerenciarproprietariosMouseMove() {
    ;
}


function findCmbIndexByOptionID(oCmb, nOptionID) {
    var retVal = -1;
    var i = 0;

    for (i = 0; i < oCmb.options.length; i++) {
        if (oCmb.options.item(i).value == nOptionID) {
            retVal = i;
            break;
        }
    }
    return retVal;
}
/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    if (glb_callFillGridTimerInt != null) {
        window.clearInterval(glb_callFillGridTimerInt);
        glb_callFillGridTimerInt = null;
    }

    lockControlsInModalWin(true);

    var strSQL = '';
    var Marcas = 'NULL';
    var LinhaProdutos = 'NULL';
    var Familias = 'NULL';
    var Proprietarios = 'NULL';
    var soPendencias = 0;
    var Resultado = (glb_Proprietario_Combo == true ? 1 : 0);


    if (selMarca.value != 0) {
        Marcas = "' ";
        for (i = 0; i < selMarca.length; i++) {
            if (selMarca.options[i].selected == true)
                Marcas += (Marcas == '' ? '|' : '|') + selMarca.options[i].value;
        }
        Marcas += (Marcas.length > 0 ? "|'" : '');
    }

    if (selLinha.value != 0) {
        LinhaProdutos = "' ";
        for (i = 0; i < selLinha.length; i++) {
            if (selLinha.options[i].selected == true)
                LinhaProdutos += (LinhaProdutos == '' ? '|' : '|') + selLinha.options[i].value;
        }
        LinhaProdutos += (LinhaProdutos.length > 0 ? "|'" : '');
    }

    if (selFamilia.value != 0) {
        Familias = "' ";
        for (i = 0; i < selFamilia.length; i++) {
            if (selFamilia.options[i].selected == true)
                Familias += (Familias == '' ? '|' : '|') + selFamilia.options[i].value;
        }
        Familias += (Familias.length > 0 ? "|'" : '');
    }


    if (selProprietarioID.value != 0) {
        Proprietarios = "' ";
        for (i = 0; i < selProprietarioID.length; i++) {
            if (selProprietarioID.options[i].selected == true)
                Proprietarios += (Proprietarios == '' ? '|' : '|') + selProprietarioID.options[i].value;
        }
        Proprietarios += (Proprietarios.length > 0 ? "|'" : '');
    }


    if (selPais.value != 0) {
        Pais = "' ";
        for (i = 0; i < selPais.length; i++) {
            if (selPais.options[i].selected == true)
                Pais += (Pais == '' ? '|' : '|') + selPais.options[i].value;
        }
        Pais += (Pais.length > 0 ? "|'" : '');
    }

    soPendencias = (chkPendencias.checked == true ? 1 : 0);
    // zera o grid
    fg.Rows = 1;

    glb_dataGridWasChanged = false;

    strSQL = 'EXEC sp_Produto_ProprietarioListar ' +
		Marcas + ', ' + LinhaProdutos + ', ' +
		Familias + ', ' + Pais + ' , ' +
		Proprietarios + ', ' + soPendencias + ', ' + Resultado;

    if (strSQL != null) {
        // parametrizacao do dso dsoGrid
        setConnection(dsoGrid);
        dsoGrid.SQL = strSQL;
        dsoGrid.ondatasetcomplete = fillGridData_DSC;
        dsoGrid.Refresh();
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {


    startGridInterface(fg);
    fg.FontSize = '8';

    headerGrid(fg, ['Linha',
                    'Tipo',
                    'MarcaID',
                    'Marca',
                    'LinhaPRODUTOID',
                    'Linha',
                    'FamiliaID',
                    'Familia',
                    'PaisID',
                    'Pais',
                    'ColaboradorID',
                    'Colaborador',
                    'Estado',
                    'Cargo',
                    'Produtos',
                    '%',
                    'Distintos',
                    'OK',
                    'ColaboradorOK',
                    'EstadoOK',
                    'CargoOK'], [0, 1, 2, 4, 6, 8, 10, 18, 19, 20]);

    //glb_aCelHint = [[0, 4, 'Mensagem usada somente se estiver presente em uma rela��o']];

    fillGridMask(fg, dsoGrid, ['Linha*',
                                    'Tipo*',
                                    'MarcaID',
                                    'Marca*',
                                    'LinhaProdutoID',
                                    'LinhaProduto*',
                                    'FamiliaID',
                                    'Familia*',
                                    'PaisID',
                                    'Pais*',
                                    'ColaboradorID',
                                    'Colaborador*',
                                    'Estado*',
                                    'Cargo*',
                                    'Produtos*',
                                    'Percentual*',
                                    'ProprietarioDistinto*',
                                    'OK',
                                    'ColaboradorOK',
                                    'EstadoOK',
                                    'CargoOK'],
                                   ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);

    fg.FrozenCols = 12;

    fg.MergeCells = 1;
    fg.MergeCol(1) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(7) = true;
    // Linha de Totais
    gridHasTotalLine(fg, '', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(fg, 'Produtos*'), '######', 'C']]);
    GridLinhaTotal(fg);


    /*  //coloca o tipo das colunas
      with (fg) {
          ColDataType(2) = 11;
      }*/

    paintReadOnlyRows();

    fg.Editable = true;
    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    //fg.ColWidth(5) = parseInt(divFG.currentStyle.width, 10) * 13 - 1200;
    fg.Redraw = 2;
   fg.ExplorerBar = 2;

    lockControlsInModalWin(false);

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function saveDataInGrid() {
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var ManifestoID = '';
    var Mensagem = '';
    var Estado = '';
    var Marcas = 'NULL';
    var LinhaProdutos = 'NULL';
    var Familias = 'NULL';
    var Proprietarios = 'NULL';
    var soPendencias = 0;
    var ProprietariosParaID = 0;
    var Resultado = (glb_Proprietario_Combo == true ? 1 : 0);
    var _retMsg = 1;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;


    if ((glb_nContador < 100) && (glb_nContador >= 50))
        _retMsg = window.top.overflyGen.Confirm('Essa altera��o afetar� : ' + glb_nQuantidadeProdutos + ' Produtos');

    if ((_retMsg == 1) && (glb_nContador < 100)) {
        lockControlsInModalWin(true);

        for (i = 2; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'OK', i) != 0) {

                Marcas = fg.textMatrix(i, getColIndexByColKey(fg, 'MarcaID'));
                LinhaProdutos = fg.textMatrix(i, getColIndexByColKey(fg, 'LinhaProdutoID'));
                Familias = fg.textMatrix(i, getColIndexByColKey(fg, 'FamiliaID'));
                PaisID = fg.textMatrix(i, getColIndexByColKey(fg, 'PaisID'));
                Proprietarios = fg.textMatrix(i, getColIndexByColKey(fg, 'ColaboradorID'));

                if (chkAuto.checked)
                    ProprietariosParaID = '';
                else
                    ProprietariosParaID = selAlterarPara.value;

                nBytesAcum = strPars.length;

                if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                    nBytesAcum = 0;
                    if (strPars != '') {
                        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
                        strPars = '';
                        nDataLen = 0;
                    }
                    strPars = '?UserID=' + escape(glb_nUserID);
                }
                strPars += '&MarcaID=' + escape(Marcas);
                strPars += '&LinhaProdutoID=' + escape(LinhaProdutos);
                strPars += '&FamiliaID=' + escape(Familias);
                strPars += '&PaisID=' + escape(PaisID);
                strPars += '&ProprietarioID=' + escape(Proprietarios);
                strPars += '&ProprietarioParaID=' + escape(ProprietariosParaID);
                strPars += '&Resultado=' + escape(Resultado);

                nDataLen++;
            }
        }

        if (nDataLen > 0)
            glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

        sendDataToServer();
    }
}

function sendDataToServer() {
    var sMensagem = '';

    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/ConsolidarProprietarios.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            lockControlsInModalWin(false);
            glb_callFillGridTimerInt = window.setInterval('fillGridData()', 100, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_callFillGridTimerInt = window.setInterval('fillGridData()', 100, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    lockControlsInModalWin(false);

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        dsoGrava.recordset.MoveFirst();
        if (dsoGrava.recordset['fldResponse'].value != null && dsoGrava.recordset['fldResponse'].value.length > 1) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['fldResponse'].value) == 0)
                return null;
        }
    }
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}


// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerenciarproprietariosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerenciarproprietariosDblClick(grid, Row, Col) {
    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    // trap so colunas editaveis
    if (Col != getColIndexByColKey(grid, 'OK'))
        return true;

    //// trap so header
    //if (Row != 1)
    //    return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 2; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();
    GridLinhaTotal(grid);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerenciarproprietariosKeyPress(KeyAscii) {
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerenciarproprietarios_ValidateEdit() {
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerenciarproprietarios_AfterEdit(Row, Col) {

    var ok = 0;
    if (fg.Editable) {
        if (fg.Row >= 1) {
            for (i = 2; i < fg.Rows; i++) {
                if (getCellValueByColKey(fg, 'OK', i) != 0) {
                    ok += 1;
                }
            }
            if (ok > 0) {
                setupBtnsFromGridState(false);
            }
            else
                setupBtnsFromGridState();

        }
    }

    GridLinhaTotal(fg);
}

// FINAL DE EVENTOS DE GRID *****************************************


function HideShowAlternar() {
    if (chkAuto.checked) {
        lblAlterarpara.style.visibility = 'hidden';
        selAlterarPara.style.visibility = 'hidden';
    }
    else {
        fillcmbsAlternarParaProprietario(glb_Proprietario_Combo);
        lblAlterarpara.style.visibility = 'inherit';
        selAlterarPara.style.visibility = 'inherit';
    }
}
function paintReadOnlyRows() {
    for (var i = 2; i < fg.Rows; i++) {

        if ((fg.textMatrix(i, getColIndexByColKey(fg, 'ColaboradorOK')) == 0)) {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Colaborador*'), i, getColIndexByColKey(fg, 'Colaborador*')) = 0x00ffff;

        }
        if ((fg.textMatrix(i, getColIndexByColKey(fg, 'EstadoOK')) == 0)) {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Estado*'), i, getColIndexByColKey(fg, 'Estado*')) = 0x00ffff;

        }
        if ((fg.textMatrix(i, getColIndexByColKey(fg, 'CargoOK')) == 0)) {
            fg.Cell(6, i, getColIndexByColKey(fg, 'Cargo*'), i, getColIndexByColKey(fg, 'Cargo*')) = 0x00ffff;

        }
        if ((fg.textMatrix(i, getColIndexByColKey(fg, 'ProprietarioDistinto*')) > 1)) {
            fg.Cell(6, i, getColIndexByColKey(fg, 'ProprietarioDistinto*'), i, getColIndexByColKey(fg, 'ProprietarioDistinto*')) = 0x00ffff;

        }
    }
}

function lblProprietariosPL_onClick() {
    glb_TimerClick = window.setInterval('lblProprietariosPL_onClick_Continue()', 10, 'JavaScript');;
}

function lblProprietariosPL_onClick_Continue() {
    if (glb_TimerClick != null) {
        window.clearInterval(glb_TimerClick);
        glb_TimerClick = null;
    }

    fillcmbsProprietarios(glb_Proprietario_Combo);

}

function lblProprietariosPL_ondblClick() {
    glb_Proprietario_Combo = !glb_Proprietario_Combo;
    glb_lblProprietariodblClick = 1;

    lblProprietarioID.innerText = (glb_Proprietario_Combo == true ? 'Propriet�rio' : 'Alternativo');

    fillcmbsProprietarios(glb_Proprietario_Combo);
}


function lblProprietariosPL_onmouseover() {
    lblProprietarioID.style.color = 'blue';
    lblProprietarioID.style.cursor = 'hand';
}

function lblProprietariosPL_onmouseout() {
    lblProprietarioID.style.color = 'black';
    lblProprietarioID.style.cursor = 'point';
}

function setupBtnsFromGridState(bPar) {
    var i;

    if (bPar == false)
        btnCorrigir.disabled = bPar;
    else
        btnCorrigir.disabled = true;
}

function fillcmbsAlternarParaProprietario(Proprietarios) {
    lblProprietarioID.disabled = true;
    lockControlsInModalWin(true);
    var strSQL = '';

    if (Proprietarios == 1) {
        strSQL = "SELECT DISTINCT a.PessoaID as fldId, a.Fantasia as FldName " +
            "FROM Pessoas a WITH(NOLOCK) " +
            "WHERE ((a.ClassificacaoID = 57) AND (dbo.fn_Colaborador_Dado(a.PessoaID, NULL, NULL, 2) = 2) AND " +
            "(dbo.fn_Colaborador_Dado(a.PessoaID, NULL, NULL, 4) IN (612, 632))) " +
            " ORDER BY FldName";
    } else {
        strSQL = "SELECT DISTINCT a.PessoaID as fldId, a.Fantasia as FldName " +
            "FROM Pessoas a WITH(NOLOCK) " +
            "WHERE ((a.ClassificacaoID = 57) AND (dbo.fn_Colaborador_Dado(a.PessoaID, NULL, NULL, 2) = 2) AND " +
            "(dbo.fn_Colaborador_Dado(a.PessoaID, NULL, NULL, 4) IN (632, 657))) " +
            " ORDER BY FldName";
    }
    if (strSQL != null) {
        dsoCmbsAlterarPara.SQL = strSQL;
        dsoCmbsAlterarPara.ondatasetcomplete = fillcmbsAlternarParaProprietario_DSC;
        dsoCmbsAlterarPara.Refresh();
    }
}


function fillcmbsAlternarParaProprietario_DSC() {
    var optionStr;
    var optionValue;
    clearComboEx(['selAlterarPara']);
    dsoCmbsAlterarPara.recordset.MoveFirst();

    while (!dsoCmbsAlterarPara.recordset.EOF) {
        optionStr = dsoCmbsAlterarPara.recordset['FldName'].value;
        optionValue = dsoCmbsAlterarPara.recordset['fldId'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selAlterarPara.add(oOption);
        dsoCmbsAlterarPara.recordset.MoveNext();
    }

    lockControlsInModalWin(false);
    lblProprietarioID.disabled = false;
    selAlterarPara.disabled = (selAlterarPara.options.length == 0);
    //selOptByValueInSelect(getHtmlId(), 'selProprietarioID', glb_nUserID);

    showExtFrame(window, true);
    window.focus();
}
function GridLinhaTotal(grid) {
    glb_nQuantidadeProdutos = 0;
    glb_nContador = 0;

    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'OK')) != 0) {

            glb_nQuantidadeProdutos += grid.ValueMatrix(i, getColIndexByColKey(grid, 'Produtos*'));
            glb_nContador++;
        }
    }

    if (grid.Rows > 1) {
        grid.TextMatrix(1, getColIndexByColKey(grid, 'Produtos*')) = glb_nQuantidadeProdutos;
        grid.TextMatrix(1, getColIndexByColKey(grid, 'Marca*')) = glb_nContador;
    }
}