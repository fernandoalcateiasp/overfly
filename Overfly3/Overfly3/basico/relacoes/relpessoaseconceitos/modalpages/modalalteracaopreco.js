/********************************************************************
modalalteracaopreco.js

Library javascript para o modalalteracaopreco.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_CaracteristicasTimerInt = null;
var glb_RecalculaTimer = null;
var glb_nDOS = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoEmail = CDatatransport("dsoEmail");
var dsoGrid = CDatatransport("dsoGrid");
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalalteracaopreco.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalalteracaopreco.ASP

js_fg_modalalteracaoprecoBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalalteracaoprecoDblClick( grid, Row, Col)
js_modalalteracaoprecoKeyPress(KeyAscii)
js_modalalteracaopreco_AfterRowColChange
js_modalalteracaopreco_ValidateEdit()
js_modalalteracaopreco_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalalteracaoprecoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Forma��o de Pre�o', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        background= 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
    
    btnCalcular.style.top = btnOK.offsetTop;
    btnCalcular.style.left = ELEM_GAP;
    
    btnRefresh.style.top = btnOK.offsetTop;
    btnRefresh.style.left = btnCalcular.offsetLeft + btnCalcular.offsetWidth + ELEM_GAP;
    
    btnExcel.style.top = btnOK.offsetTop;
    btnExcel.style.left = btnRefresh.offsetLeft + btnRefresh.offsetWidth + ELEM_GAP;
    
    btnImprimir.style.top = btnOK.offsetTop;
    btnImprimir.style.left = btnExcel.offsetLeft + btnExcel.offsetWidth + ELEM_GAP;
    
    lblIncideImposto.style.top = btnOK.offsetTop - 5;
    lblIncideImposto.style.left = btnImprimir.offsetLeft + btnImprimir.offsetWidth + ELEM_GAP;
    
    chkIncideImposto.style.top = btnOK.offsetTop + 5;
    chkIncideImposto.style.width = FONT_WIDTH * 3;
    chkIncideImposto.style.left = lblIncideImposto.offsetLeft - 5;
    
    selUF.style.top = chkIncideImposto.offsetTop - 5;
    selUF.style.left = chkIncideImposto.offsetLeft + chkIncideImposto.offsetWidth + ELEM_GAP;
    selUF.style.width = FONT_WIDTH * 5;
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;   
    }
    // codigo privado desta janela
    else if (controlID == 'btnCanc')
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }
    else if (controlID == 'btnCalcular')
    {
		fillGridData(2);
    }
    else if (controlID == 'btnRefresh')
    {
		fillGridData(1);
    }
    else if (controlID == 'btnExcel')
    {
		geraExcel();
    }
    else if (controlID == 'btnImprimir')
    {
		imprimirGrid();
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData(1);
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    
    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
    btnCalcular.disabled = !bHasRowsInGrid;
    btnRefresh.disabled = !bHasRowsInGrid;
    btnExcel.disabled = !bHasRowsInGrid;
    btnImprimir.disabled = !bHasRowsInGrid;
}

/********************************************************************
Solicitar dados do grid ao servidor
nTipo 1: Refersh
      2: Recalcular
********************************************************************/
function fillGridData(nTipo)
{
    if (glb_CaracteristicasTimerInt != null)
    {
        window.clearInterval(glb_CaracteristicasTimerInt);
        glb_CaracteristicasTimerInt = null;
    }

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'SujeitoID' + '\'' + '].value');
    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ObjetoID' + '\'' + '].value');
    var nImpostoIncidencia = (chkIncideImposto.checked ? 1 : 0);
    var nUFVendaID = selUF.value;
    
    var aParams = new Array (-1,-1,-1,-1,-1,-1,-1,-1);
    var strPars = '';
    
    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nProdutoID=' + escape(nProdutoID);
    strPars += '&nImpostoIncidencia=' + escape(nImpostoIncidencia);
    strPars += '&nUFVendaID=' + escape(nUFVendaID);
    
    if (nTipo == 2)
    {
        for (i=1; i<fg.Rows; i++)
        {
            if (! (isNaN(getCellValueByColKey(fg, '_OrdemParametro', i))) )
            {
                if (getCellValueByColKey(fg, '_OrdemParametro', i) > 0)
                {
                    if (getCellValueByColKey(fg, '_EP', i) != 0)
                        aParams[getCellValueByColKey(fg, '_OrdemParametro', i) - 1] = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Percentual'));
                    else if (getCellValueByColKey(fg, '_EV', i) != 0)
                        aParams[getCellValueByColKey(fg, '_OrdemParametro', i) - 1] = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor'));
                }
            }
        }
    }

    for (i=0; i<aParams.length; i++)
    {
        strPars += '&nParam' + (i + 1) + '=' + aParams[i];
    }

    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;

    glb_dataGridWasChanged = false;

    dsoGrid.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/formacaopreco.aspx' + strPars;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    fg.FontSize = '8';
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    headerGrid(fg,['Descri��o',
                   'Perc',
                   'Base Calc',
                   'Moeda',
                   'Valor',
                   '_EP',
                   '_EV',
                   '_OrdemParametro'],[5,6,7]);

    fillGridMask(fg,dsoGrid,['Descricao*',
                             'Percentual',
                             'BaseCalculo*',
                             'Moeda*',
                             'Valor',
                             '_EP',
                             '_EV',
                             '_OrdemParametro'],
                             ['','999.99','999999999.99'  ,'','999999999.99'  ,'','',''],
                             ['','##0.00','###,###,##0.00','','###,###,##0.00','','','']);

	alignColsInGrid(fg,[1,2,4]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

	for (i=1; i<fg.Rows; i++)
	{
	    nCol = getColIndexByColKey(fg, 'Percentual');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;

	    nCol = getColIndexByColKey(fg, 'Valor');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;
    }

    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    //if ( fg.Rows > 1 )
        //fg.Col = 5;
    
    if (fg.Rows > 1)
        fg.Editable = true;

    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        
        for (i=fg.Rows-1; i>0; i--)
        {
            if (!cellIsLocked(i, getColIndexByColKey(fg, 'Percentual')))
            {
                fg.Row = i;
                fg.Col = getColIndexByColKey(fg, 'Percentual');
                break;
            }
            else if (!cellIsLocked(i, getColIndexByColKey(fg, 'Valor')))
            {
                fg.Row = i;
                fg.Col = getColIndexByColKey(fg, 'Valor');
                break;
            }
        }
        
        //fg.focus();
    }
    else
    {
        ;
    }        

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    glb_dataGridWasChanged = false;
    lockControlsInModalWin(true);
	var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
	var strPars = '?nPedidoID=' + escape(nPedidoID) + 
		'&bRecalcula=' + escape(0);
		
    dsoPedido.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/recalculapedido.asp' + strPars;
    dsoPedido.ondatasetcomplete = dsoPedido_DSC;
    dsoPedido.Refresh();
}

function dsoPedido_DSC()
{
    try
    {
        dsoGrid.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg.Rows = 1;
        lockControlsInModalWin(false);
        
		glb_CaracteristicasTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
        return null;
    }    

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
	glb_RecalculaTimer = window.setInterval('recalculaPedido()', 30, 'JavaScript');
	return null;
}

function recalculaPedido()
{
	if (glb_RecalculaTimer != null)
	{
		window.clearInterval(glb_RecalculaTimer);
		glb_RecalculaTimer = null;
	}

	var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
	var strPars = '?nPedidoID=' + escape(nPedidoID) + 
		'&bRecalcula=' + escape(1);
		
    dsoPedido.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/recalculapedido.asp' + strPars;
    dsoPedido.ondatasetcomplete = recalculaPedido_DSC;
    dsoPedido.Refresh();
}

function recalculaPedido_DSC()
{
    lockControlsInModalWin(false);
    
    glb_nDOS = 0;
	fillGridData_DSC();
}

function excluirLinha()
{
	if (fg.Row > 0)
	{
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir esta linha?");
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
	
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();
        dsoGrid.recordset.Find(fg.ColKey(fg.Cols - 1), fg.TextMatrix(fg.Row, fg.Cols - 1));        

        if ( !(dsoGrid.recordset.EOF) )
        {
			fg.RemoveItem(fg.Row);
			dsoGrid.recordset.Delete();
            glb_dataGridWasChanged = true;
            setupBtnsFromGridState();
        }
        saveDataInGrid();
	}
}

function excluirTodos()
{
	if (fg.Row > 0)
	{
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir todas as linhas?");
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
	
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();

        while (true)
        {
			dsoGrid.recordset.Delete();
			
			if (!dsoGrid.recordset.BOF)
				dsoGrid.recordset.MoveFirst();
			else
				break;
		}
        
        fg.Rows = 1;
		glb_dataGridWasChanged = true;
		setupBtnsFromGridState();
		saveDataInGrid();
	}
}

function calcTotItem(nLine)
{
    var nQtd, nValUnit;
    var lIsEditing = (nLine == null);
    nLine = (nLine == null ? fg.Row : nLine);
    
    if ( (fg.Col == 4)&&(lIsEditing) )
        nQtd = parseFloat(fg.EditText);
    else    
        nQtd = parseFloat(fg.TextMatrix(nLine,5));
                
    if ( (fg.Col == 7)&&(lIsEditing) )
        nValUnit = parseFloat(replaceStr(fg.EditText,',','.'));
    else    
        nValUnit = parseFloat(replaceStr(fg.TextMatrix(nLine,8),',','.'));
        
    if ( (! isNaN(nQtd)) && (! isNaN(nValUnit)) )
        fg.TextMatrix(nLine,9) = roundNumber(nQtd*nValUnit, 2);
    else    
        fg.TextMatrix(nLine,9) = '';
        
	gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4,'#########','S'], [8,'###,###,###,###.00','S']]);        
}


// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalalteracaoprecoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

function js_fg_modalalteracaopreco_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
	//Forca celula read-only    
	if (!glb_GridIsBuilding)
	{
		if ( glb_FreezeRolColChangeEvents )
			return true;

		if (cellIsLocked(NewRow, NewCol))
		{
			glb_validRow = OldRow;
			glb_validCol = OldCol;
		}
		else
		    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

function cellIsLocked(nRow, nCol)
{
    var retVal = false;
    
    if (nCol == getColIndexByColKey(fg, 'Percentual'))
    {
        if (getCellValueByColKey(fg, '_EP', nRow) == 0)
            retVal = true;
    }
    else if (nCol == getColIndexByColKey(fg, 'Valor'))
    {
        if (getCellValueByColKey(fg, '_EV', nRow) == 0)
            retVal = true;
    }
    
    return retVal;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalalteracaoprecoDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalteracaoprecoKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalteracaopreco_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalalteracaopreco_AfterEdit(Row, Col)
{
    if (fg.Editable)
    {
		if ((Col == getColIndexByColKey(fg, 'Percentual')) ||
			(Col == getColIndexByColKey(fg, 'Valor')) )
			fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    }
}
// FINAL DE EVENTOS DE GRID *****************************************

function geraExcel()
{
    //Fun��o descontinuada. Modal n�o � mais utilizada. Projeto Overfly 2.0 MTH 28/12/2017
}

function imprimirGrid()
{
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var sMsg = aEmpresaData[6] + '          Forma��o de Pre�o          ' + getCurrDate();

	sMsg += '     ';
	fg.PrintGrid(sMsg, false, 2, 0, 450);

	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}

function getCurrDate()
{
   var d, s = "";
   d = new Date();
   
   if (DATE_SQL_PARAM == 103)
   {
		s += padL(d.getDate().toString(),2,'0') + "/";
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
   }
   else
   {
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
		s += padL(d.getDate().toString(),2,'0') + "/";
   }
   
   s += d.getYear() + " ";
   s += padL(d.getHours().toString(),2,'0') + ":";
   s += padL(d.getMinutes().toString(),2,'0');

   return(s);
}
