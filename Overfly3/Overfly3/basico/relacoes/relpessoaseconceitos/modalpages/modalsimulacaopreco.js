/********************************************************************
modalcodigotaxa.js

Library javascript para o modalcodigotaxa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoAtualizaPrecoMargem = new CDatatransport("dsoAtualizaPrecoMargem");
// IMPLEMENTACAO DAS FUNCOES


/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
        
    // ajusta o body do html
    with (modalSimulacaoPrecoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	// coloca foco no campo Pesquisa
	window.focus();
	txtPreco.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Caption da janela modal
    secText('Simula��o de Pre�o', 1);
        
	var tempLeft = ELEM_GAP;
    var elem;

    // divExtPesquisa
    elem = window.document.getElementById('divExtPesquisa');
    with (elem.style)
    {
        visibility = 'visible';
        backgroundColor = 'transparent';
        width = 300;
        height = 50;
        top = 25;
        left = 0;
        tempLeft = parseInt(left, 10);
    }

    adjustElementsInForm([['lblMoedaEntradaID','selMoedaEntradaID', 7, 1],
		['lblCustoReposicao','txtCustoReposicao', 11, 1],
		['lblCustoBase','txtCustoBase', 11, 1],
		['lblMargemContribuicao','txtMargemContribuicao', 6, 1],
		['lblMargemAjustada','chkMargemAjustada', 3, 1],
		['lblUF','selUF', 6, 1],
		['lblImposto','txtImposto', 6, 1],
		['lblImpostoIncidencia','chkImpostoIncidencia', 3, 1],
		['lblMoedaSaidaID','selMoedaSaidaID', 7, 2],
		['lblPreco','txtPreco', 11, 2],
		['lblMargemContribuicao2','chkMargemContribuicao2', 3, 2],
		['lblMargemPadrao','chkMargemPadrao', 3, 2],
		['lblMargemPublicacao','chkMargemPublicacao', 3, 2],
		['lblMargemMinima','chkMargemMinima', 3, 2]], null, null, true);
				
	selMoedaEntradaID.onchange = selMoedaEntradaID_onchange;

    txtCustoReposicao.maxLength = 10;
    txtCustoReposicao.onfocus = selFieldContent;
    txtCustoReposicao.onkeypress = verifyNumericEnterNotLinked;
    txtCustoReposicao.setAttribute('thePrecision', 11, 1);
    txtCustoReposicao.setAttribute('theScale', 2, 1);
    txtCustoReposicao.setAttribute('minMax', new Array(0, 9999999999));
    txtCustoReposicao.setAttribute('verifyNumPaste', 1);
    txtCustoReposicao.onkeyup = txtCustoReposicao_onkeyup;

    txtCustoBase.maxLength = 10;
    txtCustoBase.onfocus = selFieldContent;
    txtCustoBase.onkeypress = verifyNumericEnterNotLinked;
    txtCustoBase.setAttribute('thePrecision', 11, 1);
    txtCustoBase.setAttribute('theScale', 2, 1);
    txtCustoBase.setAttribute('minMax', new Array(0, 9999999999));
    txtCustoBase.setAttribute('verifyNumPaste', 1);
    txtCustoBase.onkeyup = txtCustoBase_onkeyup;

    txtMargemContribuicao.maxLength = 10;
    txtMargemContribuicao.onfocus = selFieldContent;
    txtMargemContribuicao.onkeypress = verifyNumericEnterNotLinked;
    txtMargemContribuicao.setAttribute('thePrecision', 11, 1);
    txtMargemContribuicao.setAttribute('theScale', 2, 1);
    txtMargemContribuicao.setAttribute('minMax', new Array(0, 9999999999));
    txtMargemContribuicao.setAttribute('verifyNumPaste', 1);
    txtMargemContribuicao.onkeyup = txtMargemContribuicao_onkeyup;

	chkMargemAjustada.onclick = chkMargemAjustada_onclick;
	selMoedaSaidaID.onchange = selMoedaSaidaID_onchange;

    txtPreco.maxLength = 10;
    txtPreco.onfocus = selFieldContent;
    txtPreco.onkeypress = verifyNumericEnterNotLinked;
    txtPreco.setAttribute('thePrecision', 11, 1);
    txtPreco.setAttribute('theScale', 2, 1);
    txtPreco.setAttribute('minMax', new Array(0, 9999999999));
    txtPreco.setAttribute('verifyNumPaste', 1);
    txtPreco.onkeyup = txtPreco_onkeyup;
    
    txtImposto.readOnly = true;
   	chkImpostoIncidencia.onclick = chkImpostoIncidencia_onclick;

	txtMargemContribuicao.value = padNumReturningStr(roundNumber(sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'txtMargemContribuicao.value'),2),2);
	
    var oCmbMoedaEntrada = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'selMoedaEntradaID');
    var oCmbMoedaSaida = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'selMoedaSaidaID');
        
    for (i=0; i<oCmbMoedaEntrada.options.length; i++)
    {
		var oOption = document.createElement("OPTION");
		oOption.text = oCmbMoedaEntrada.options[i].text;
		oOption.value = oCmbMoedaEntrada.options[i].value;
		selMoedaEntradaID.add(oOption);
    }
    
    if (glb_nEstoque > 0)
		selMoedaEntradaID.selectedIndex = oCmbMoedaEntrada.selectedIndex;
	else
    	selOptByValueInSelect(getHtmlId(), 'selMoedaEntradaID', glb_nMoedaFornecedorID);

    for (i=0; i<oCmbMoedaSaida.options.length; i++)
    {
		var oOption = document.createElement("OPTION");
		oOption.text = oCmbMoedaSaida.options[i].text;
		oOption.value = oCmbMoedaSaida.options[i].value;
		selMoedaSaidaID.add(oOption);
    }
	
	selMoedaSaidaID.selectedIndex = oCmbMoedaSaida.selectedIndex;

	selUF.onchange = selUF_onchange;
	selUF_onchange(true);
	
	if (glb_nEstoque > 0)
	{
		txtCustoBase.value = padNumReturningStr(roundNumber(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'txtCustoMedio.value'),2),2);
		atualizaCusto(1);
	}
	else
	{
		txtCustoReposicao.value = padNumReturningStr(roundNumber(glb_nCustoReposicao,2),2);
		atualizaCusto(2);
	}
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		gravaSimulacao();
    }
    else
        sendJSMessage( getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
}

function selMoedaEntradaID_onchange()
{
	atualizaPrecoMargem(1);
}

function selUF_onchange(bCarregamento)
{
	txtImposto.value = padNumReturningStr(roundNumber(selUF.options[selUF.selectedIndex].getAttribute('aliquota', 1),2),2);
	
	if (bCarregamento == undefined)
		atualizaPrecoMargem(1);
}

function selMoedaSaidaID_onchange()
{
	atualizaPrecoMargem(1);
}

/********************************************************************
Atualiza os campos de Custo
nTipo: 1-CustoReposicao
	   2-CustoBase
********************************************************************/
function atualizaCusto(nTipo)
{
	var nCustoReposicao;
	var nCustoBase;
	
	if (nTipo == 1)
	{
		nCustoBase = parseFloat(txtCustoBase.value);
		nCustoBase = (isNaN(nCustoBase) ? 0 : nCustoBase);

		if (glb_nFatorImpostosEntrada > 0)
			nCustoReposicao = nCustoBase/glb_nFatorImpostosEntrada;
		else
			nCustoReposicao = 0;
			
		txtCustoReposicao.value = padNumReturningStr(roundNumber(nCustoReposicao,2),2);						
	}
	else if (nTipo == 2)
	{
		nCustoReposicao = parseFloat(txtCustoReposicao.value);
		nCustoReposicao = (isNaN(nCustoReposicao) ? 0 : nCustoReposicao);
		nCustoBase = nCustoReposicao * glb_nFatorImpostosEntrada;
		txtCustoBase.value = padNumReturningStr(roundNumber(nCustoBase,2),2);						
	}
	
	atualizaPrecoMargem(1);
}

/********************************************************************
Atualiza os campos de Preco ou Margem
nTipo: 1-Preco
	   2-Margem
********************************************************************/
function atualizaPrecoMargem(nTipo)
{
	var EmpresaID = getCurrEmpresaData()[0];
    lockControlsInModalWin(true);
    
    var strPas = '?';
    
    strPas += 'EmpresaID='+escape(EmpresaID);
    strPas += '&ProdutoID='+escape(glb_nProdutoID);
    strPas += '&nMoedaEntradaID='+escape(selMoedaEntradaID.value);
    strPas += '&nCustoBase='+escape(parseFloat(txtCustoBase.value));
    strPas += '&nMargemContribuicao='+escape(parseFloat(txtMargemContribuicao.value));
    strPas += '&nMargemAjustada='+escape(chkMargemAjustada.checked ? 1 : 0);
    strPas += '&nAliquota='+escape(parseFloat(txtImposto.value));
    strPas += '&nImpostosIncidencia='+escape(chkImpostoIncidencia.checked ? 1 : 0);
    strPas += '&nMoedaSaidaID='+escape(selMoedaSaidaID.value);
    strPas += '&nPreco='+escape(parseFloat(txtPreco.value));
    strPas += '&nTipo='+escape(nTipo);
    
    dsoAtualizaPrecoMargem.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/atualizaprecomargem.aspx' + strPas;
    dsoAtualizaPrecoMargem.ondatasetcomplete = dsoAtualizaPrecoMargem_DSC;
    dsoAtualizaPrecoMargem.Refresh();
}

function dsoAtualizaPrecoMargem_DSC()
{
	if (!((dsoAtualizaPrecoMargem.recordset.BOF)&&(dsoAtualizaPrecoMargem.recordset.EOF)))
	{
		dsoAtualizaPrecoMargem.recordset.MoveFirst();
		
		if (dsoAtualizaPrecoMargem.recordset['Tipo'].value == 1)
			txtPreco.value = padNumReturningStr(roundNumber(dsoAtualizaPrecoMargem.recordset['PrecoMargem'].value,2),2);
		else
			txtMargemContribuicao.value = padNumReturningStr(roundNumber(dsoAtualizaPrecoMargem.recordset['PrecoMargem'].value,2),2);
	}
	
    lockControlsInModalWin(false);
}

function txtCustoReposicao_onkeyup()
{
	if (event.keyCode == 13)
		atualizaCusto(2);
}

function txtCustoBase_onkeyup()
{
	if (event.keyCode == 13)
		atualizaCusto(1);
}

function txtMargemContribuicao_onkeyup()
{
	if (event.keyCode == 13)
		atualizaPrecoMargem(1);
}

function chkMargemAjustada_onclick()
{
	atualizaPrecoMargem(1);
}

function chkImpostoIncidencia_onclick()
{
	atualizaPrecoMargem(1);
}

function txtPreco_onkeyup()
{
	if (event.keyCode == 13)
		atualizaPrecoMargem(2);
}

function gravaSimulacao()
{

	if ((trimStr(txtMargemContribuicao.value) == '') ||	isNaN(txtMargemContribuicao.value))
	{
		if ( window.top.overflyGen.Alert('Digite uma MC v�lida') == 0 )
			return null;
		
		return null;
	}

	if ((!chkMargemContribuicao2.checked) && (!chkMargemPadrao.checked) && (!chkMargemPublicacao.checked) && (!chkMargemMinima.checked))
	{
		if ( window.top.overflyGen.Alert('Selecione uma das quatro margens para grava��o') == 0 )
			return null;
		
		return null;
	}

	lockControlsInModalWin(true);
	
	var EmpresaID = getCurrEmpresaData()[0];
    lockControlsInModalWin(true);
    
    var nRelacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,
                                  'dsoSup01.recordset[' + '\'' + 'RelacaoID' + '\'' + '].value');

    var strPas = '?';
    strPas += 'RelacaoID='+escape(nRelacaoID);
    strPas += '&nMargemContribuicao='+escape(parseFloat(txtMargemContribuicao.value));
    strPas += '&bMC='+escape(chkMargemContribuicao2.checked ? 1 : 0);
    strPas += '&bMP='+escape(chkMargemPadrao.checked ? 1 : 0);
    strPas += '&bMPub='+escape(chkMargemPublicacao.checked ? 1 : 0);
    strPas += '&bMMin='+escape(chkMargemMinima.checked ? 1 : 0);
    strPas += '&UsuarioID='+escape(getCurrUserID());
    
    dsoAtualizaPrecoMargem.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/gravasimulacao.aspx' + strPas;
    dsoAtualizaPrecoMargem.ondatasetcomplete = gravaSimulacao_DSC;
    dsoAtualizaPrecoMargem.Refresh();
}

function gravaSimulacao_DSC()
{
    sendJSMessage( getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null);
}
