/********************************************************************
modalpessoas.js

Library javascript para o modalpessoas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES
var dsoPesq = new CDatatransport("dsoPesq");
/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();    
    
    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modalpessoasBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPessoa').disabled == false )
        txtPessoa.focus();

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    if (glb_nTipo == 1)//Concorrente
    // texto da secao01
        secText('Selecionar Concorrente', 1);
    else if (glb_nTipo == 2)//Colaborador
        secText('Selecionar Colaborador', 1);

    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divCidade
    elem = window.document.getElementById('divCidade');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width);
        height = 40;
        
    }
    
    // txtPessoa
    elem = window.document.getElementById('txtPessoa');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style)
    {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    elem.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtPessoa').style.top);
        left = parseInt(document.getElementById('txtPessoa').style.left) + parseInt(document.getElementById('txtPessoa').style.width) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divCidade').style.top) + parseInt(document.getElementById('divCidade').style.height) + ELEM_GAP;
        width = temp + 221;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);
    if (glb_nTipo == 1)
    {
        headerGrid(fg,['Concorrente',
                       'ID',
                       'Fantasia',
                       'Documento',
                       'Cidade',
                       'UF',
                       'Pa�s'], [2]);
    }
    else if (glb_nTipo == 2)
    {
        headerGrid(fg,['Colaborador',
                       'ID',
                       'Fantasia',
                       'Documento',
                       'Cidade',
                       'UF',
                       'Pa�s'], [2]);
    }
    fg.Redraw = 2;
}

function txtPessoa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    txtPessoa.value = trimStr(txtPessoa.value);
    
    changeBtnState(txtPessoa.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtPessoa.value);
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
    // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , new Array(
                  fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 1),glb_nTipo));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strCidade)
{
    lockControlsInModalWin(true);
    
    var strPas = '?';
    strPas += 'strToFind='+escape(strCidade);
    strPas += '&nPessoaID='+escape(glb_nPessoaID);
    strPas += '&nTipo='+escape(glb_nTipo);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/pesqpessoa.aspx'+strPas;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    if (glb_nTipo == 1)
    {
        headerGrid(fg,['Concorrente',
                       'ID',
                       'Fantasia',
                       'Documento',
                       'Cidade',
                       'UF',
                       'Pa�s'], [2]);
    }
    else if (glb_nTipo == 2)
    {
        headerGrid(fg,['Colaborador',
                       'ID',
                       'Fantasia',
                       'Documento',
                       'Cidade',
                       'UF',
                       'Pa�s'], [2]);
    }
    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Fantasia',
                             'Documento',
                             'Cidade',
                             'UF',
                             'Pais'],['','','','','','','']);
                           
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    
    
}
