/********************************************************************
modalvendasdigitais.js

SGP - Cria��o da Modal de Vendas Digitais para gerenciamento de produtos
      enviados para os Marketplaces. 05/11/2015.

Library javascript para o modalvendasdigitais.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_aEmpresaData = null;
var glb_ControlDiv = 0;
var glb_nValue = 0;
var glb_aGridOk = null;
var glb_chkOK = new Array();
var glb_chkDel = new Array();
var glb_nMaxStringSize = 1024 * 1;
var glb_nTimerSendDataToServer = null;
var glb_nTimerToServer = null;
var glb_aSendDataToServer = [];
var glb_nPointToaSendDataToServer = 0;
var glb_nItensAtualizados = 0;
var glb_nMCminima = 0;
var glb_RowEdit = 0;
var glb_RowEdit_Atualiza = 0;
var glb_ColEdit = 0;
var glb_RowFocus = 0;
var glb_nAtualizaValores = 0;
var glb_nDifMC = new Array();
var glb_nDev = 0;
var glb_nControlBtn = 0;
var glb_nControlline = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************
//Eventos de envio ao BD
var dsoServicos = new CDatatransport('dsoServicos');
var dsoGrid = new CDatatransport("dsoGrid");
var dsoGridGerencia = new CDatatransport("dsoGridGerencia");
var dsoGridHistorico = new CDatatransport("dsoGridHistorico");
var dsoGridCategorizar = new CDatatransport("dsoGridCategorizar");
var dsoGridBusca = new CDatatransport("dsoGridBusca");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoPublica = new CDatatransport("dsoPublica");
var dsoAtualiza = new CDatatransport("dsoAtualiza");
var dsoTxtBusca = new CDatatransport("dsoTxtBusca");
var dsoGravaCategoria = new CDatatransport("dsoGravaCategoria");
var dsoPerfil = new CDatatransport('dsoPerfil');
var dsoGridSetup = new CDatatransport("dsoGridSetup");
var dsoSetup = new CDatatransport("dsoSetup"); 
var dsoCmbGrid01 = new CDatatransport("dsoCmbGrid01");
//Filtros de Familia/Marca/Linha
var dsoFamilia = new CDatatransport('dsoFamilia');
var dsoMarca = new CDatatransport('dsoMarca');
var dsoLinha = new CDatatransport('dsoLinha');
var dsoGP = new CDatatransport('dsoGP');
var dsoEstados = new CDatatransport('dsoEstados');
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
startDynamicGrids()                 -- Grid Listagem de produtos para incluir no Mktplace.
startDynamicGrids_DSC()
startDynamicGridsGerenciar()        -- Grid Gerenciamento de Produtos Incluidos nas Vendas Digitais
startDynamicGridsGerenciar_DSC()
startDynamicGridsHistorico()        -- Grid Historico callback Mktplace
startDynamicGridsHistorico_DSC()
preencheFamilia()
preencheFamilia_DSC()
preencheMarca()
preencheMarca_DSC()
preencheLinha()
preencheLinha_DSC()
preencheGP()
preencheGP_DSC()
selFamilia_onchange()
selMarca_onchange()
selLinha_onchange()
selGerenteProduto_onchange()
selDiv_onchange()                   -- Servi�o de mudan�a das Divs
chkDeletado_onclick()
txtBuscaCategoria_onkeypress()
txtBusca_onkeypress()
DataHora(event, obj)                -- Mascara do campo txtdtAgendamento
js_fg_DblClick()                    
paintReadOnlyRow()                  -- Pinta celulas readonly
cellIsLocked()                      -- Verifica se a celula deve estar readonly
getFirstRowColAllowed()             -- Retorna a primeira linha com coluna editavel do grid.
currCellIsLocked()
js_ModalVendasDigitais_BeforeRowColChange()
js_ModalVendasDigitais_AfterEdit()
js_ModalVendasDigitais_BeforeEdit()
saveDataInGrid()
sendDataToServer()
sendDataToServer_DSC()
saveDataInGrid_Gerenciar()
sendDataToServer_Gerenciar()
sendDataToServer_Gerenciar_DSC()
ajustaDivs()                        -- Ajusta as Divs de acordo com a op��o selecionada no botao de controle da modal.
atualizaValores()                   -- Atualiza��o ass�ncrona do Grid.
atualizaValores_DSC()
paramBusca()                        -- Valida se itens da listagem nao fazem parte da mesma familia ou marketplace e zera a pesquisa
copyCategoria()                     -- Copia as categorias da pesquisa para o Grid da listagem
criticAndNormTxtDataTime()          -- Formata campo txt em dateformat para padrao SQL
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    // ajusta o body do html
    with (modalvendasdigitaisBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html    
    setupPage();

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Vendas Digitais - Incluir', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 28;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';

    // ajusta o divControles
    elem = window.document.getElementById('divControles');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP - 10;
        width = modWidth - 2 * ELEM_GAP;
        height = 90;
    }

    // Ajusta elementos da divControles (elementos comuns aos dois divs (Incluir e Gerenciar))
    adjustElementsInForm([['lblSelDiv', 'selDiv', 12, 1, -12],
                          ['lblEmpresa', 'selEmpresa', 15, 1, -5],
                          ['lblPrgMarketing', 'selPrgMarketing', 15, 1, -5],                         
                          ['lblFamilia', 'selFamilia', 18, 1, -5],
                          ['lblMarca', 'selMarca', 18, 1, -5],                          
                          ['lblLinha', 'selLinha', 16, 1, -5],
                          ['lblGerenteProduto', 'selGerenteProduto', 16, 1, -5],
                          ['lblSelEstado', 'selEstado', 12, 2, -12],
                          ['lblBusca', 'txtBusca', 40, 2, -2],                          
                          ['lblDataAgendamento', 'txtDataAgendamento', 16, 2, -5],
                          ['btnListar', 'btn', btnOK.offsetWidth - 16, 1],
                          ['btnGravar', 'btn', btnOK.offsetWidth - 16, 2]], null, null, true);

    // Alinha bot�es � direita
    window.document.getElementById('btnListar').style.left = frameRect[2] - btnListar.offsetWidth - 16 - ELEM_GAP;
    window.document.getElementById('btnGravar').style.left = frameRect[2] - btnGravar.offsetWidth - 16 - ELEM_GAP;

    //Seta tamanho dos combos
    selEmpresa.style.height = 80;
    selGerenteProduto.style.height = 80;
    selFamilia.style.height = 80;
    selMarca.style.height = 80;
    selLinha.style.height = 80;
    selPrgMarketing.style.height = 80;

    selFamilia.onchange = selFamilia_onchange;
    selMarca.onchange = selMarca_onchange;
    selLinha.onchange = selLinha_onchange;
    selGerenteProduto.onchange = selGerenteProduto_onchange;
    selDiv.onchange = selDiv_onchange;
    txtBusca.onkeypress = txtBusca_onkeypress;
    btnGravar.disabled = true;
    
    // ajusta o divGerenciar
    elem = window.document.getElementById('divGerenciar');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = divControles.currentStyle.width;
        height = divControles.currentStyle.height;
        visibility = 'hidden';
    }
    
    // Ajusta elementos da divGerenciar
    /*adjustElementsInForm([['btnIncluir', 'btn', btnOK.offsetWidth - 16, 1, 0, -12],
                          ['lblHistorico', 'chkHistorico', 3, 2, -5],
                          ['lblDeletado', 'chkDeletado', 3, 2, -5]], null, null, true);*/
    adjustElementsInForm([['lblHistorico', 'chkHistorico', 3, 2, -5],
                          ['lblDeletado', 'chkDeletado', 3, 2, -5],
                          ['lblEstoque', 'chkEstoque', 3, 2, -5],
                          ['lblIncluidos', 'chkIncluidos', 3, 2, -5],
                          ['btnMC', 'btn', btnOK.offsetWidth - 16, 1]], null, null, true);

    // Alinha bot�es � direita
    window.document.getElementById('btnMC').style.left = frameRect[2] - btnMC.offsetWidth - 16 - ELEM_GAP;
    //window.document.getElementById('btnMC').style.visibility = 'hidden';

    chkHistorico.onclick = chkHistorico_onclick;
    //chkNaoCategorizados.onclick = chkNaoCategorizados_onclick; //Retirado para forma��o de buscas
    //chkDeletado.onclick = chkDeletado_onclick;
    
    elem = lblSelEstado;
    with (elem.style) {
        left = 350;
    }
    elem = selEstado;
    with (elem.style) {
        left = 350;
    }

    elem = lblBusca;
    with (elem.style) {
        left = 455;
    }
    elem = txtBusca;
    with (elem.style) {
        left = 455;
    }
    elem = lblDataAgendamento;
    with (elem.style) {
        left = 780;
    }
    elem = txtDataAgendamento;
    with (elem.style) {
        left = 780;
    }
    
    elem = btnMC;
    with (elem.style)
    {
        top = 46;
    }

    // ajusta o divCategorizar
    elem = window.document.getElementById('divCategorizar');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = divControles.currentStyle.width;
        height = divControles.currentStyle.height;
        visibility = 'hidden';
    }

    // Ajusta elementos da divCategorizar
    adjustElementsInForm([['lblNaoCategorizados', 'chkNaoCategorizados', 3, 2, -5]], null, null, true);

    // ajusta o divBuscaCategoria
    elem = window.document.getElementById('divBuscaCategoria');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = divControles.currentStyle.width;
        height = divControles.currentStyle.height;
        visibility = 'hidden';
    }

    // Ajusta elementos da divBuscaCategoria
    adjustElementsInForm([['lblBuscaCategoria', 'txtBuscaCategoria', 60, 1,-10],
                          ['btnBuscar', 'btn', 60, 1, 5]], null, null, true);


    // ajusta o divSetup
    elem = window.document.getElementById('divSetup');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = divControles.currentStyle.width;
        height = divControles.currentStyle.height;
        visibility = 'hidden';
    }

    // Ajusta elementos da divBuscaCategoria
    adjustElementsInForm([['btnIncluir', 'btn', 60, 2, -10],
                          ['btnExcluir', 'btn', 60, 2, 5]], null, null, true);


    txtBuscaCategoria.onkeypress = txtBuscaCategoria_onkeypress;

    // ajusta as Divs de Grid
    with (divGrids.style) {
        border = 'none';
        backgroundColor = 'transparent';
        //visibility = 'hidden';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) +
			parseInt(divControles.currentStyle.height, 10) + 10;
        width = modWidth - 2 * ELEM_GAP - 6;
        //height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10) - 20;
        height = "430px";
    }

    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = divGrids.currentStyle.width;
        height = parseInt(divGrids.currentStyle.height,10);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    } 
      
    //Div Historico
    with (divFGHistorico.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        //top = parseInt(divFG.currentStyle.top, 10) +
			//parseInt(divFG.currentStyle.height, 10) + 50;
        width = divGrids.currentStyle.width;
        //Altura inserida apenas para realizar a dinamica dos DivsFG, altura real setada na function ajustaDivs()
        height = 110;
        visibility = 'hidden';
    }

    with (fgHistorico.style) {
        left = 0;
        top = 0;       
    }

    //Div Busca
    with (divFGBusca.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        width = divGrids.currentStyle.width;
        //Altura inserida apenas para realizar a dinamica dos DivsFG, altura real setada na function ajustaDivs()
        height = 110;
        visibility = 'hidden';
    }

    with (fgBusca.style) {
        left = 0;
        top = 0;
    }

    ajustaDivs();

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
    
    showExtFrame(window, true);

    verificaPerfil();    
    preencheFamilia();
    preencheMarca();
    preencheLinha();
    preencheGP();    

}
/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
        
    if (controlID == 'btnListar') {
        lockControlsInModalWin(true);

        glb_ControlDiv = selDiv.value;

        if (glb_ControlDiv == 0)
            startDynamicGrids();
        else if (glb_ControlDiv == 1)
            startDynamicGridsCategorizar();
        else if (glb_ControlDiv == 2)
            startDynamicGridsGerenciar();        
        else if (glb_ControlDiv == 3)
            startDynamicGridsSetup();
    }
    else if (controlID == 'btnGravar')
    {
        if (glb_ControlDiv == 0)
            saveDataInGrid();
        else if (glb_ControlDiv == 1)
            saveDataInGrid_Categorizar();
        else if (glb_ControlDiv == 2)
            saveDataInGrid_Gerenciar();
        else if (glb_ControlDiv == 3)
        {
            if (fg.Rows > 1)
                sendDatatoServer_Setup();
            else {
                if (window.top.overflyGen.Alert('N�o h� registros para altera��o.') == 0)
                    return null;
            }
        }
    }
    else if (controlID == 'btnBuscar')
    {
        lockControlsInModalWin(true);
        startDynamicGridsCategorias();
    }
    else if (controlID == 'btnIncluir')
    {
        lockControlsInModalWin(true);

        glb_nControlBtn = 1;
        startDynamicGridsSetup();
    }
    else if (controlID == 'btnExcluir')
    {
        var _retMsg;

        lockControlsInModalWin(true);

        if (glb_nControlBtn == 1) {
            glb_nControlBtn = 0;
            startDynamicGridsSetup();
        }
        else
        {
            _retMsg = window.top.overflyGen.Confirm('Excluir o registro?');

            if (_retMsg == 1)
            {
                glb_nControlBtn = 2;
                sendDatatoServer_Setup();
            } //Function Aqui exclui registro
            else
            {
                glb_nControlBtn = 0;
                sendDatatoServer_Setup();
            }
        }
    }
    else if (controlID == 'btnMC')
    {
        glb_nControlline = 0;

        AtualizaMC();
    }

    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        // esta funcao fecha a janela modal e destrava a interface
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/*****************************************************
Preenchimento dos Grids da Modal - Inicio
******************************************************/
/*Grid de Listagem Programas Marketing Place*/
function startDynamicGrids()
{
    var strPars = '';
    var sFiltroProduto = '';
    var sFiltroEmpresa = '';
    var sFiltroPrgMarketing = '';
    var sMensagemFiltro = '';
    var sMensagemBuscaFiltro = '';
    var nFiltroBusca = 0;
    var nControleFiltroCombos = 4;
    var nEstoque = 0;
    var nIncluidos = 0;

    if (chkEstoque.checked)
        nEstoque = 1;

    if (chkIncluidos.checked)
        nIncluidos = 1;

    sFiltroProduto = (txtBusca.value == '' ? null : txtBusca.value);

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true && selEmpresa.options[i].value != "") {
            sFiltroEmpresa += '/' + selEmpresa.options[i].value;
        }
    }

    sFiltroEmpresa = (sFiltroEmpresa == '' ? '' : sFiltroEmpresa + '/');

    for (i = 0; i < selPrgMarketing.length; i++) {
        if (selPrgMarketing.options[i].selected == true && selPrgMarketing.options[i].value != "") {
            sFiltroPrgMarketing += '/' + selPrgMarketing.options[i].value;
        }
    }
    sFiltroPrgMarketing = (sFiltroPrgMarketing == '' ? '' : sFiltroPrgMarketing + '/');
   
    if (sFiltroEmpresa == '')
        sMensagemFiltro = 'Empresas';
    if (sFiltroPrgMarketing == '')
    {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Programas de Marketing';
    }
    if (selFamilia.value == 0)
    {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Fam�lia';

        nControleFiltroCombos = nControleFiltroCombos - 1;
    }
    if (selMarca.value == 0)
    {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Marca';

        nControleFiltroCombos = nControleFiltroCombos - 1;
    }

    if (selLinha.value == 0)
        nControleFiltroCombos = nControleFiltroCombos - 1;
    if (selGerenteProduto.value == 0)
        nControleFiltroCombos = nControleFiltroCombos - 1;


    if ((sFiltroProduto != null) && (sFiltroProduto.length >= 3))
        nFiltroBusca = 1;
    /* Trava retirada temporariamente devido a solicita��o do Eduardo Rodrigues
    //if (((((sMensagemFiltro != '') && (sFiltroPrgMarketing == '')) && (nFiltroBusca == 0)) && (nControleFiltroCombos < 1)))// && glb_nUsuarioID != 998)
    if ((((sMensagemFiltro != '') && (nControleFiltroCombos < 1)) && (nFiltroBusca == 0)) || (sFiltroPrgMarketing == '') || (sFiltroEmpresa == ''))
    {

        if ((sFiltroProduto != null) && (sFiltroProduto.length < 3))
            sMensagemBuscaFiltro = 'Filtro de busca inv�lido. \n';
       
        sMensagemFiltro = sMensagemBuscaFiltro + 'Utilize o filtro de ' + sMensagemFiltro + ' ou a Busca para filtrar a listagem.';

        if (window.top.overflyGen.Alert(sMensagemFiltro) == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
    else {
    */
        strPars = '?sEmpresas=' + escape(sFiltroEmpresa);
        strPars += '&nPaisID=' + escape(glb_aEmpresaData[1]);
        strPars += '&nGerenteProdutoID=' + escape((selGerenteProduto.value == 0 ? 0 : selGerenteProduto.value));
        strPars += '&nFamiliaID=' + escape((selFamilia.value == 0 ? 0 : selFamilia.value));
        strPars += '&nMarcaID=' + escape((selMarca.value == 0 ? 0 : selMarca.value));
        strPars += '&nLinhaProdutoID=' + escape((selLinha.value == 0 ? 0 : selLinha.value));
        strPars += '&sProgramasMarketing=' + escape(sFiltroPrgMarketing);
        strPars += '&nServicoID=' + escape(1);
        strPars += '&nRelacaoID=' + escape(0);
        strPars += '&nIncluidos=' + escape(nIncluidos);
        strPars += '&nCategorizado=' + escape(0);
        strPars += '&nEstadoID=' + escape(0);
        strPars += '&nEstoque=' + escape(nEstoque);
        strPars += '&sBusca=' + escape(sFiltroProduto);

        setConnection(dsoGrid);

        //Alterado para preocedure. SGP 26/10/2015
        dsoGrid.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/Serverside/fillprodutosalcateiadigital.aspx' + strPars;
        dsoGrid.ondatasetcomplete = startDynamicGrids_DSC;

        try {
            dsoGrid.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
                return null;

            lockControlsInModalWin(false);
            window.focus();
        }
    //}
}
/*Retorno do Banco Grid de Inclus�o*/
function startDynamicGrids_DSC()
{
    glb_GridIsBuilding = true;
    var i;
    var dso, grid;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';
    
    dso = dsoGrid;
    grid = fg;

    if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)) {
        // move o cursor do dso para o primeiro registro
        dsoGrid.recordset.MoveFirst();
    }

    //a.ProdutoID, a.Est, a.Descricao, a.LinhaProduto, a.Empresa, a.Estoque, b.ProgramaMarketing, a.ProdutoParticipante

    grid.ExplorerBar = 5;
    //startGridInterface(grid, 1, null);
    startGridInterface(grid);
    grid.FontSize = '8';
    grid.Editable = false;
    grid.BorderStyle = 1;

    headerGrid(grid, ['ID',                     //0
                       'Est',                    //1		                                      
                       'Produto',                //2
                       'Linha Produto',          //3
                       'Empresa',                //4
                       'Estoque',                //5
                       'Programa de Marketing',  //6
                       'OK',                     //7
                       'ProdutoParticipante',    //8
                       'ProgramaMarketingID',      //9   
                       'RelacaoID'], [8,9,10]);       //10 


    fillGridMask(grid, dsoGrid, ['ProdutoID*',              //0
                                'Est*',                     //1
                                'Descricao*',               //2
                                'LinhaProduto*',            //3
                                'Empresa*',                 //4
                                'Estoque*',                 //5
                                'ProgramaMarketing*',       //6
                                'OK',                       //7
                                'ProdutoParticipante',      //8
                                'ProgramaMarketingID',      //9
                                'RelacaoID'],              //10
                           ['', '', '', '', '', '', '', '', '', '', ''],
                           ['', '', '', '', '', '', '', '', '', '', '']);


    //alignColsInGrid(grid, [2, 3, 4, 5, 6, 7, 9, 12, 14, 18]);
    //gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

    if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)) {
        dsoGrid.recordset.Filter = '';

        // move o cursor do dso para o primeiro registro
        dsoGrid.recordset.MoveFirst();
    }

    // Coloca tipo de dado nas colunas
    with (grid) {
        ColDataType(7) = 11;
    }

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);
    grid.FrozenCols = 1;
    grid.MergeCells = 4;
    grid.MergeCol(0) = true;
    grid.MergeCol(1) = true;
    grid.MergeCol(2) = true;
    grid.MergeCol(3) = true;
    grid.MergeCol(4) = true;
    grid.MergeCol(5) = true;
    //paintCellsSpecialyReadOnly(grid);

    fg.ColWidth(2) = 405 * 18;

    grid.ExplorerBar = 5;

    if (grid.Rows > 1)
        grid.Row = 1;

    if ((grid.Rows > 0) && grid.Row > 0) {
        grid.TopRow = grid.Row;
    }

    paintReadOnlyRow(grid);

    // se tem linhas no grid, coloca foco no grid
    if (grid.Rows > 1) {
        window.focus();
        grid.focus();

        var aRowCol = getFirstRowColAllowed();

        if (aRowCol != null) {
            grid.Editable = true;
            grid.Row = aRowCol[0];
            grid.Col = aRowCol[1];
        }
        else {
            grid.Editable = false;
            grid.Row = -1;
            grid.Col = -1;
        }
    }

    gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[1, '######', 'C']]);

    grid.Redraw = 2;
    lockControlsInModalWin(false);

    if((glb_nA1 ==true) && (glb_nA2 == true))
        btnGravar.disabled = false;

    glb_GridIsBuilding = false;
}

/*Grid de Gerenciamento para publica��o*/
function startDynamicGridsGerenciar() {
    var strPars = '';
    var sFiltroProduto = '';
    var sFiltroEmpresa = '';
    var sFiltroPrgMarketing = '';
    var sMensagemFiltro = '';
    var sMensagemBuscaFiltro = '';
    var nFiltroBusca = 0;
    var nDel = 0;
    var nControleFiltroCombos = 4;
    var nEstoque = 0;

    if (chkEstoque.checked)
        nEstoque = 1;

    if (chkDeletado.checked)
        nDel = 1;

    sFiltroProduto = (txtBusca.value == '' ? null : txtBusca.value);

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true && selEmpresa.options[i].value != "") {
            sFiltroEmpresa += '/' + selEmpresa.options[i].value;
        }
    }

    sFiltroEmpresa = (sFiltroEmpresa == '' ? '' : sFiltroEmpresa + '/');

    for (i = 0; i < selPrgMarketing.length; i++) {
        if (selPrgMarketing.options[i].selected == true && selPrgMarketing.options[i].value != "") {
            sFiltroPrgMarketing += '/' + selPrgMarketing.options[i].value;
        }
    }
    sFiltroPrgMarketing = (sFiltroPrgMarketing == '' ? '' : sFiltroPrgMarketing + '/');

    if (sFiltroEmpresa == '')
        sMensagemFiltro = 'Empresas';
    if (sFiltroPrgMarketing == '') {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Programas de Marketing';
    }
    if (selFamilia.value == 0) {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Fam�lia';
        nControleFiltroCombos = nControleFiltroCombos - 1;
    }
    if (selMarca.value == 0) {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Marca';
        nControleFiltroCombos = nControleFiltroCombos - 1;
    }

    if (selLinha.value == 0)
        nControleFiltroCombos = nControleFiltroCombos - 1;
    if (selGerenteProduto.value == 0)
        nControleFiltroCombos = nControleFiltroCombos - 1;

    if ((sFiltroProduto != null) && (sFiltroProduto.length >= 3))
        nFiltroBusca = 1;
    /* Trava retirada temporariamente devido a solicita��o do Eduardo Rodrigues
    //if (((((sMensagemFiltro != '') && (sFiltroPrgMarketing == '')) && (nFiltroBusca == 0)) && (nControleFiltroCombos < 1)))// && glb_nUsuarioID != 998)
    if ((((sMensagemFiltro != '') && (nControleFiltroCombos < 1)) && (nFiltroBusca == 0)) || (sFiltroPrgMarketing == '') || (sFiltroEmpresa == ''))
    {
        if ((sFiltroProduto != null) && (sFiltroProduto.length < 3))
            sMensagemBuscaFiltro = 'Filtro de busca inv�lido. \n';

        sMensagemFiltro = sMensagemBuscaFiltro + 'Utilize o filtro de ' + sMensagemFiltro + ' ou a Busca para filtrar a listagem.';

        if (window.top.overflyGen.Alert(sMensagemFiltro) == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
    else {
    */
        strPars = '?sEmpresas=' + escape(sFiltroEmpresa);
        strPars += '&nPaisID=' + escape(glb_aEmpresaData[1]);
        strPars += '&nGerenteProdutoID=' + escape((selGerenteProduto.value == 0 ? 0 : selGerenteProduto.value));
        strPars += '&nFamiliaID=' + escape((selFamilia.value == 0 ? 0 : selFamilia.value));
        strPars += '&nMarcaID=' + escape((selMarca.value == 0 ? 0 : selMarca.value));
        strPars += '&nLinhaProdutoID=' + escape((selLinha.value == 0 ? 0 : selLinha.value));
        strPars += '&sProgramasMarketing=' + escape(sFiltroPrgMarketing);
        strPars += '&nServicoID=' + escape(3);
        strPars += '&nRelacaoID=' + escape(0);
        strPars += '&nIncluidos=' + escape(0);
        strPars += '&nCategorizado=' + escape(nDel);
        strPars += '&nEstadoID=' + escape((selEstado.value == 0 ? 0 : selEstado.value));
        strPars += '&nEstoque=' + escape(nEstoque);
        strPars += '&sBusca=' + escape(sFiltroProduto);

        setConnection(dsoGridGerencia);

        //Alterado para preocedure. SGP 26/10/2015
        dsoGridGerencia.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/Serverside/fillprodutosalcateiadigital.aspx' + strPars;
        dsoGridGerencia.ondatasetcomplete = startDynamicGridsGerenciar_DSC;

        try {
            dsoGridGerencia.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
                return null;

            lockControlsInModalWin(false);
            window.focus();
        }
    //}
}
/*Retorno do Banco Grid de Gerenciamento*/
function startDynamicGridsGerenciar_DSC() {

    glb_GridIsBuilding = true;
    var i;
    var dso, grid;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    dso = dsoGridGerencia;
    grid = fg;

    if (!(dsoGridGerencia.recordset.BOF && dsoGridGerencia.recordset.EOF)) {
        // move o cursor do dso para o primeiro registro
        dsoGridGerencia.recordset.MoveFirst();
    }

    //a.ProdutoID, a.Est, a.Descricao, a.LinhaProduto, a.Empresa, a.Estoque, b.ProgramaMarketing, a.ProdutoParticipante

    grid.ExplorerBar = 5;
    //startGridInterface(grid, 1, null);
    startGridInterface(grid);
    grid.FontSize = '8';
    grid.Editable = false;
    grid.BorderStyle = 1;

    headerGrid(grid, ['ID',                         //0
                        'Est',                      //1		                                      
                        'Produto',                  //2
                        'Linha Produto',            //3
                        'Empresa',                  //4
                        'Estoque',                  //5
                        'Programa de Marketing',    //6
                        'Estado',                   //7
                        'Deletar',                  //8
                        'Pre�o Fabricante',         //9
                        'Pre�o Overfly',            //10
                        'Pre�o Praticado',          //11
                        'MC',                       //12
                        'Agendado para',            //13
                        'Alterado',                 //14
                        'Link do produto',          //15   
                        'URL',                      //16  
                        'EstadoID',                 //17  
                        'RelacaoID',                //18
                        'EmpresaID',                //19
                        'RelPesConVendaDigitalID',  //20
                        'DifMC',                    //21
                        'ProgramaMarketingID',      //22
                        'LoteID'], [16, 17, 18, 19, 20, 21, 22, 23]);   //23


    fillGridMask(grid, dsoGridGerencia, ['ProdutoID*',      //0
                                'Est*',                     //1
                                'Descricao*',               //2
                                'LinhaProduto*',            //3
                                'Empresa*',                 //4
                                'Estoque*',                 //5
                                'ProgramaMarketing*',       //6
                                'Estado*',                  //7
                                'Deletado',                 //8
                                'PrecoFabricante',          //9
                                'PrecoOverfly*',            //10
                                'PrecoPraticado',           //11
                                'MC*',                      //12
                                'dtAgendamento*',           //13
                                'Alterado',                 //14
                                'Link',                     //15
                                'URL_Marketplace',          //16
                                'EstadoID',                 //17
                                'RelacaoID',                //18
                                'EmpresaID',                //19
                                'RelPesConVendaDigitalID',  //20
                                'DifMC*',                   //21
                                'ProgramaMarketingID',      //22
                                'LoteID'],                  //23
                                ['', '', '', '', '', '', '', '', '', '999999999.99', '999999999.99', '999999999.99', '999.99', '99/99/9999 99:99', '', '', '', '', '', '', '', '','',''],
                                ['', '', '', '', '', '', '', '', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '###.00', dTFormat + ' hh:mm', '', '', '', '', '', '', '', '','','']);


    alignColsInGrid(grid, [9,10,11,12]);
    
    if (!(dsoGridGerencia.recordset.BOF && dsoGridGerencia.recordset.EOF)) {
        dsoGridGerencia.recordset.Filter = '';

        // move o cursor do dso para o primeiro registro
        dsoGridGerencia.recordset.MoveFirst();
    }

    // Coloca tipo de dado nas colunas
    with (grid) {
        ColDataType(14) = 11;
    }

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);
    grid.FrozenCols = 3;
    grid.MergeCells = 4;
    grid.MergeCol(0) = true;
    grid.MergeCol(1) = true;
    grid.MergeCol(2) = true;
    grid.MergeCol(3) = true;
    grid.MergeCol(4) = true;
    grid.MergeCol(5) = true;
    //paintCellsSpecialyReadOnly(grid);

    fg.ColWidth(2) = 280 * 18;
    grid.ExplorerBar = 5;

    if (grid.Rows > 1)
        grid.Row = 1;

    if ((grid.Rows > 0) && grid.Row > 0) {
        grid.TopRow = grid.Row;
    }

    gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[1, '######', 'C']]);
  
    grid.Redraw = 2;    
    lockControlsInModalWin(false);
    grid.Editable = true;
    if ((glb_nA1 == true) && (glb_nA2 == true))
        btnGravar.disabled = false;

    glb_GridIsBuilding = false;

}

/*Grid de Historico de publica��o*/
function startDynamicGridsHistorico() {
    var strPars = '';
    var sFiltroProduto = '';

    if (glb_RowEdit > 1)
    {
        sFiltroProduto = (txtBusca.value == '' ? null : txtBusca.value);
        var nRelacaoID = getCellValueByColKey(fg, 'RelacaoID', glb_RowEdit);
        var nProgramaMarketingID = getCellValueByColKey(fg, 'ProgramaMarketingID', glb_RowEdit);

        strPars = '?sEmpresas=null';
        strPars += '&nPaisID=' + escape(glb_aEmpresaData[1]);
        strPars += '&nGerenteProdutoID=' + escape(0);
        strPars += '&nFamiliaID=' + escape(0);
        strPars += '&nMarcaID=' + escape(0);
        strPars += '&nLinhaProdutoID=' + escape(0);
        strPars += '&sProgramasMarketing=/' + escape(nProgramaMarketingID) + '/';
        strPars += '&nServicoID=' + escape(4);
        strPars += '&nRelacaoID=' + escape(nRelacaoID);
        strPars += '&nIncluidos=' + escape(0);
        strPars += '&nCategorizado=' + escape(0);
        strPars += '&nEstadoID=' + escape(0);        
        strPars += '&nEstoque=' + escape(0);
        strPars += '&sBusca=' + escape(sFiltroProduto);

        setConnection(dsoGridHistorico);

        //Alterado para preocedure. SGP 26/10/2015
        dsoGridHistorico.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/Serverside/fillprodutosalcateiadigital.aspx' + strPars;
        dsoGridHistorico.ondatasetcomplete = startDynamicGridsHistorico_DSC;

        try {
            dsoGridHistorico.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
                return null;

            lockControlsInModalWin(false);
            window.focus();
        }
    }
    else
    {
        return null;
    }
}
/*Retorno do Banco Grid de Gerenciamento*/
function startDynamicGridsHistorico_DSC() {

    glb_GridIsBuilding = true;
    var i;
    var dso, grid;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    dso = dsoGridHistorico;
    grid = fgHistorico;

    if (!(dsoGridHistorico.recordset.BOF && dsoGridHistorico.recordset.EOF)) {
        // move o cursor do dso para o primeiro registro
        dsoGridHistorico.recordset.MoveFirst();
    }

    startGridInterface(grid);
    grid.FontSize = '8';
    grid.Editable = false;
    grid.BorderStyle = 1;

    headerGrid(grid, ['Data              Hora',      //0
                       'Colaborador',            //1		                                      
                       'Pre�o Fabricante',       //2
                       'Pre�o Overfly',          //3
                       'Pre�o Praticado',        //4
                       'Estado',
                       'Observac�o'], []);       //7


    fillGridMask(grid, dsoGridHistorico, ['DataHora*',              //0
                                            'Colaborador*',                     //1
                                            'PrecoFabricante*',               //2
                                            'PrecoOverfly*',            //3
                                            'PrecoPraticado*',                 //4
                                            'Estado*',
                                            'Motivo*'],    //8
                                       ['99/99/9999 99:99', '', '999999999.99', '999999999.99', '999999999.99', '', ''],
                                       [dTFormat + ' hh:mm', '', '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '', '']);


    //alignColsInGrid(grid, [2, 3, 4, 5, 6, 7, 9, 12, 14, 18]);
    //gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

    if (!(dsoGridHistorico.recordset.BOF && dsoGridHistorico.recordset.EOF)) {
        dsoGridHistorico.recordset.Filter = '';

        // move o cursor do dso para o primeiro registro
        dsoGridHistorico.recordset.MoveFirst();
    }

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);
    grid.FrozenCols = 1;
   
    //paintCellsSpecialyReadOnly(grid);

   if (grid.Rows > 1)
        grid.Row = 1;

    if ((grid.Rows > 0) && grid.Row > 0) {
        grid.TopRow = grid.Row;
    }

    grid.Redraw = 2;

    lockControlsInModalWin(false);
    glb_GridIsBuilding = false;

}

/*Grid de Categoriza��o para publica��o*/
function startDynamicGridsCategorizar() {
    var strPars = '';
    var sFiltroProduto = '';
    var sFiltroEmpresa = '';
    var sFiltroPrgMarketing = '';
    var sMensagemFiltro = '';
    var sMensagemBuscaFiltro = '';
    var nFiltroBusca = 0;
    var nControleFiltroCombos = 4;
    var nCategorizado = 0;

    if (chkNaoCategorizados.checked)
        nCategorizado = 1;    

    sFiltroProduto = (txtBusca.value == '' ? null : txtBusca.value);

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true && selEmpresa.options[i].value != "") {
            sFiltroEmpresa += '/' + selEmpresa.options[i].value;
        }
    }

    sFiltroEmpresa = (sFiltroEmpresa == '' ? '' : sFiltroEmpresa + '/');

    for (i = 0; i < selPrgMarketing.length; i++) {
        if (selPrgMarketing.options[i].selected == true && selPrgMarketing.options[i].value != "") {
            sFiltroPrgMarketing += '/' + selPrgMarketing.options[i].value;
        }
    }
    sFiltroPrgMarketing = (sFiltroPrgMarketing == '' ? '' : sFiltroPrgMarketing + '/');

    if (sFiltroEmpresa == '')
        sMensagemFiltro = 'Empresas';
    if (sFiltroPrgMarketing == '') {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Programas de Marketing';
    }
    if (selFamilia.value == 0) {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Fam�lia';

        nControleFiltroCombos = nControleFiltroCombos - 1;
    }
    if (selMarca.value == 0) {
        if (sMensagemFiltro != '')
            sMensagemFiltro = sMensagemFiltro + ', ';

        sMensagemFiltro = sMensagemFiltro + 'Marca';

        nControleFiltroCombos = nControleFiltroCombos - 1;
    }

    if (selLinha.value == 0)
        nControleFiltroCombos = nControleFiltroCombos - 1;
    if (selGerenteProduto.value == 0)
        nControleFiltroCombos = nControleFiltroCombos - 1;


    if ((sFiltroProduto != null) && (sFiltroProduto.length >= 3))
        nFiltroBusca = 1;
    /* Trava retirada temporariamente devido a solicita��o do Eduardo Rodrigues
    //if (((((sMensagemFiltro != '') && (sFiltroPrgMarketing == '')) && (nFiltroBusca == 0)) && (nControleFiltroCombos < 1)))// && glb_nUsuarioID != 998)
    if ((((sMensagemFiltro != '') && (nControleFiltroCombos < 1)) && (nFiltroBusca == 0)) || (sFiltroPrgMarketing == '') || (sFiltroEmpresa == ''))
    {

        if ((sFiltroProduto != null) && (sFiltroProduto.length < 3))
            sMensagemBuscaFiltro = 'Filtro de busca inv�lido. \n';

        sMensagemFiltro = sMensagemBuscaFiltro + 'Utilize o filtro de ' + sMensagemFiltro + ' ou a Busca para filtrar a listagem.';

        if (window.top.overflyGen.Alert(sMensagemFiltro) == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
    else {
    */
        strPars = '?sEmpresas=' + escape(sFiltroEmpresa);
        strPars += '&nPaisID=' + escape(glb_aEmpresaData[1]);
        strPars += '&nGerenteProdutoID=' + escape((selGerenteProduto.value == 0 ? 0 : selGerenteProduto.value));
        strPars += '&nFamiliaID=' + escape((selFamilia.value == 0 ? 0 : selFamilia.value));
        strPars += '&nMarcaID=' + escape((selMarca.value == 0 ? 0 : selMarca.value));
        strPars += '&nLinhaProdutoID=' + escape((selLinha.value == 0 ? 0 : selLinha.value));
        strPars += '&sProgramasMarketing=' + escape(sFiltroPrgMarketing);
        strPars += '&nServicoID=' + escape(2);
        strPars += '&nRelacaoID=' + escape(0);
        strPars += '&nIncluidos=' + escape(0);
        strPars += '&nCategorizado=' + escape(nCategorizado);
        strPars += '&nEstadoID=' + escape(0);        
        strPars += '&nEstoque=' + escape(0);
        strPars += '&sBusca=' + escape((sFiltroProduto != '' ? sFiltroProduto : "NULL"));

        setConnection(dsoGridCategorizar);

        //Alterado para procedure. SGP 26/10/2015
        dsoGridCategorizar.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/Serverside/fillprodutosalcateiadigital.aspx' + strPars;
        dsoGridCategorizar.ondatasetcomplete = startDynamicGridsCategorizar_DSC;

        try {
            dsoGridCategorizar.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
                return null;

            lockControlsInModalWin(false);
            window.focus();
        }
    //}
}
/*Retorno do Banco Grid de  Categoriza��o*/
function startDynamicGridsCategorizar_DSC() {

    glb_GridIsBuilding = true;
    var i;
    var dso, grid;
    
    dso = dsoGridCategorizar;
    grid = fg;

    if (!(dsoGridCategorizar.recordset.BOF && dsoGridCategorizar.recordset.EOF)) {
        // move o cursor do dso para o primeiro registro
        dsoGridCategorizar.recordset.MoveFirst();
    }

    //a.ProdutoID, a.Est, a.Descricao, a.LinhaProduto, a.Empresa, a.Estoque, b.ProgramaMarketing, a.ProdutoParticipante

    grid.ExplorerBar = 5;
    //startGridInterface(grid, 1, null);
    startGridInterface(grid);
    grid.FontSize = '8';
    grid.Editable = true;
    grid.BorderStyle = 1;

    headerGrid(grid, ['ID',                                  //0
                       'Produto',                            //1
                       'Programa de Marketing',              //2                       
                       'Categoria',                          //3
                       'OK',                                 //4
                       'Busca',                              //5
                       'CodigoMarketplace',                  //6
                       'FamiliaID',                          //7
                       'MarcaID',                            //8
                       'ProgramaMarketingID'], [5,6,7,8,9]); //9


    fillGridMask(grid, dsoGridCategorizar, ['ProdutoID*',   //0
                                'Descricao*',               //1
                                'ProgramaMarketing*',       //2
                                'Categoria*',               //3
                                'OK',                       //4
                                'Busca*',                   //5
                                'CodigoMarketplace',        //6
                                'FamiliaID',                //7
                                'MarcaID',                  //8
                                'ProgramaMarketingID'],     //9
                           ['', '', '', '', '', '', '', '', '', ''],
                           ['', '', '', '', '', '', '', '', '', '']);

    alignColsInGrid(grid, [6, 7]);
    //gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

    if (!(dsoGridCategorizar.recordset.BOF && dsoGridCategorizar.recordset.EOF)) {
        dsoGridCategorizar.recordset.Filter = '';

        // move o cursor do dso para o primeiro registro
        dsoGridCategorizar.recordset.MoveFirst();
    }

    // Coloca tipo de dado nas colunas
    with (grid) {
        ColDataType(4) = 11;
    }

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);
    grid.FrozenCols = 1;
    grid.MergeCells = 4;
    grid.MergeCol(0) = true;
    grid.MergeCol(1) = true;
    grid.MergeCol(2) = true;
    grid.MergeCol(3) = true;
   
    //paintCellsSpecialyReadOnly(grid);

    grid.ExplorerBar = 5;

    if (grid.Rows > 1)
        grid.Row = 1;

    if ((grid.Rows > 0) && grid.Row > 0) {
        grid.TopRow = grid.Row;
    }

    gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[1, '######', 'C']]);

    grid.Redraw = 2;
    lockControlsInModalWin(false);

    if ((glb_nA1 == true) && (glb_nA2 == true))
        btnGravar.disabled = false;

    glb_GridIsBuilding = false;

}
/*Grid de Busca de Categorias*/
function startDynamicGridsCategorias()
{
    var strPars = '';
    var sFiltroBusca = '';
    var sMensagemBuscaFiltro = '';

    sFiltroBusca = (txtBuscaCategoria.value == '' ? null : txtBuscaCategoria.value);

    if (((sFiltroBusca == '') || (sFiltroBusca == null)) || (sFiltroBusca.length < 2))
    {
        sMensagemBuscaFiltro = 'Filtro de busca inv�lido.';

        if (window.top.overflyGen.Alert(sMensagemBuscaFiltro) == 0)
            return null;

        lockControlsInModalWin(false);
        txtBuscaCategoria.focus();//.focus();
    }
    else {
        var nProgramaMarketingID = getCellValueByColKey(fg, 'ProgramaMarketingID', glb_RowEdit);
        var sBusca = txtBuscaCategoria.value;

        strPars = '?sBusca=' + escape(sBusca);
        strPars += '&nProgramaMarketingID=' + escape(nProgramaMarketingID);

        setConnection(dsoGridBusca);
        dsoGridBusca.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/Serverside/fillcategoriasmarketplace.aspx' + strPars;
        dsoGridBusca.ondatasetcomplete = startDynamicGridsCategorias_DSC;

        try {
            dsoGridBusca.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
                return null;

            lockControlsInModalWin(false);
            txtBuscaCategoria.focus(); //window.focus();
        }
    }
}
/*Retorno do Banco Grid de Busca de Categorias*/
function startDynamicGridsCategorias_DSC() {

    glb_GridIsBuilding = true;
    var i;
    var dso, grid;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    dso = dsoGridBusca;
    grid = fgBusca;

    if (!(dsoGridBusca.recordset.BOF && dsoGridBusca.recordset.EOF)) {
        // move o cursor do dso para o primeiro registro
        dsoGridBusca.recordset.MoveFirst();
    }

    startGridInterface(grid);
    grid.FontSize = '8';
    grid.Editable = false;
    grid.BorderStyle = 1;

    headerGrid(grid, ['�rvore',      //0
                       'CodigoMarketplace'], [1]);       //1


    fillGridMask(grid, dsoGridBusca, ['Arvore*',              //0
                                      'CodigoMarketplace*'],    //1
                                       ['', ''],
                                       ['', '']);


    //alignColsInGrid(grid, [2, 3, 4, 5, 6, 7, 9, 12, 14, 18]);
    //gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

    if (!(dsoGridBusca.recordset.BOF && dsoGridBusca.recordset.EOF)) {
        dsoGridBusca.recordset.Filter = '';

        // move o cursor do dso para o primeiro registro
        dsoGridBusca.recordset.MoveFirst();
    }

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);
    grid.FrozenCols = 1;

    //paintCellsSpecialyReadOnly(grid);
    /*
    if (grid.Rows > 1)
        grid.Row = 1;

    if ((grid.Rows > 0) && grid.Row > 0) {
        grid.TopRow = grid.Row;
    }
    */
    grid.Redraw = 2;

    lockControlsInModalWin(false);
    window.focus();
    txtBuscaCategoria.focus();
    glb_GridIsBuilding = false;

}

/*Grid de SETUP*/
function startDynamicGridsSetup() {
    var sSQL = '';
    
    if (glb_nControlBtn == 0)
        sSQL = "SELECT b.*, a.Localidade FROM Localidades a WITH(NOLOCK) " +
                    "INNER JOIN Localidades_Marketplace b WITH(NOLOCK) ON (b.LocalidadeID = a.LocalidadeID) " +
                    "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = b.TipoParametroID) " +
                    "WHERE ((a.LocalidadeID = " + glb_aEmpresaData[1] + ") AND " +
                    " (c.Filtro LIKE \'%<G>%\')) ";
    else if (glb_nControlBtn == 1)
        sSQL = "SELECT a.LocalidadeID, a.Localidade, 0 as TipoParametroID, 0 AS Valor, 0 AS LocMarketplaceID " +
                    "FROM Localidades a WITH(NOLOCK) " +
                    "WHERE a.LocalidadeID = " + glb_aEmpresaData[1] + " ";

    setConnection(dsoGridSetup);

    //Alterado para preocedure. SGP 26/10/2015
    dsoGridSetup.SQL = sSQL;
    dsoGridSetup.ondatasetcomplete = startDynamicGridsSetup_DSC;

    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbGrid01);
    dsoCmbGrid01.SQL =  'SELECT 0 AS TipoParametroID, \'            \' AS TipoParametro UNION ALL ' +            
                        'SELECT DISTINCT a.ItemID AS TipoParametroID, a.ItemMasculino  AS TipoParametro ' +
	                        'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
		                      'WHERE ((a.Filtro LIKE \'%<G>%\') AND (a.TipoID = 816)) ' +
                            'ORDER BY TipoParametro';
    dsoCmbGrid01.ondatasetcomplete = startDynamicGridsSetup_DSC;
    
    try {
        dsoGridSetup.Refresh();
        dsoCmbGrid01.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}
/*Retorno do Banco Grid de Gerenciamento*/
function startDynamicGridsSetup_DSC() {

    glb_GridIsBuilding = true;
    var i;
    var dso, grid;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    dso = dsoGridSetup;
    grid = fg;

    if (!(dsoGridSetup.recordset.BOF && dsoGridSetup.recordset.EOF)) {
        // move o cursor do dso para o primeiro registro
        dsoGridSetup.recordset.MoveFirst();
    }

    startGridInterface(grid);
    grid.FontSize = '8';
    grid.Editable = false;
    grid.BorderStyle = 1;

    headerGrid(grid, ['Localidade',                 //0
                       'Tipo Setup',
                       'Valor',         //1		                                      
                       'LocalidadeID',              //2
                       'LocMarketplaceID'], [3,4]);//3


    fillGridMask(grid, dsoGridSetup, ['Localidade*',              //0
                                        'TipoParametroID',
                                        'Valor',        //1
                                        'LocalidadeID',            //2
                                        'LocMarketplaceID'],       //3
                                       ['','', '', '', ''],
                                       ['','', '', '', '']);


    //alignColsInGrid(grid, [2, 3, 4, 5, 6, 7, 9, 12, 14, 18]);
    //gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

    insertcomboData(grid, getColIndexByColKey(grid, 'TipoParametroID'), dsoCmbGrid01, 'TipoParametro', 'TipoParametroID');

    if (!(dsoGridSetup.recordset.BOF && dsoGridSetup.recordset.EOF)) {
        dsoGridSetup.recordset.Filter = '';

        // move o cursor do dso para o primeiro registro
        dsoGridSetup.recordset.MoveFirst();
    }

    grid.ColWidth(2) = 18 * 18;

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);
    grid.FrozenCols = 1;
    
    //paintCellsSpecialyReadOnly(grid);

    if (grid.Rows > 1)
        grid.Row = 1;

    if ((grid.Rows > 0) && grid.Row > 0) {
        grid.TopRow = grid.Row;
    }

    grid.Editable = true;
    grid.Redraw = 2;
       

    lockControlsInModalWin(false);
    glb_GridIsBuilding = false;

    if (glb_nControlBtn == 1) {
        
        btnIncluir.disabled = true;
        btnGravar.disabled = false;
    }
    else if (glb_nControlBtn == 0) {
       
        btnIncluir.disabled = false;

        if (fg.Rows > 1)
            btnGravar.disabled = false;
    }

}
/*****************************************************
Preenchimento dos Grids da Modal - Fim
******************************************************/

/*****************************************************
Preenchimento dos Combos Dinamicos de Busca - Inicio
******************************************************/
function verificaPerfil() {

    setConnection(dsoPerfil);

    dsoPerfil.SQL = 'SELECT (CASE WHEN (SELECT COUNT(*) ' +
                        'FROM RelacoesPesRec a WITH(NOLOCK) ' +
                        'INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
                        'WHERE ((b.PerfilID = 500) AND ' +
			                    //'(b.EmpresaID = ' + glb_aEmpresaData[0] + ') AND ' +
			                    '(a.SujeitoID = ' + glb_nUserID + '))) > 0 THEN 1 ELSE 0 END) AS EhDev';

    dsoPerfil.ondatasetcomplete = verificaPerfil_DSC;
    dsoPerfil.Refresh();
}

function verificaPerfil_DSC() {

    glb_nDev = dsoPerfil.recordset['EhDev'].value;

    preencheServicos();
}

function preencheServicos()
{
    var sSelPerfil = ' ';

    setConnection(dsoServicos);

    if (glb_nDev == 1)
        sSelPerfil = ' UNION ALL ' +
                    'SELECT 3 AS ServicoID, \'Setup\' AS Servico ';

    dsoServicos.SQL = 'SELECT 0 AS ServicoID, \'Incluir\' AS Servico ' +
	    'UNION ALL ' +
        'SELECT 1 AS ServicoID, \'Categorizar\' AS Servico ' +
        'UNION ALL ' +
        'SELECT 2 AS ServicoID, \'Gerenciar\' AS Servico ' +
        sSelPerfil +
	    'ORDER BY ServicoID';

    dsoServicos.ondatasetcomplete = preencheServicos_DSC;
    dsoServicos.Refresh();
}

function preencheServicos_DSC() {

    clearComboEx(['selDiv']);

    if (!(dsoServicos.recordset.BOF || dsoServicos.recordset.EOF)) {
        dsoServicos.recordset.moveFirst;

        while (!dsoServicos.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoServicos.recordset['Servico'].value;
            oOption.value = dsoServicos.recordset['ServicoID'].value;
            selDiv.add(oOption);
            dsoServicos.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selDiv', glb_nValue);

        selDiv.disabled = ((selDiv.length <= 1) ? true : false);
    }

}

function preencheFamilia() {
    glb_nValue = selFamilia.value;

    setConnection(dsoFamilia);

    var sWHERE = '';

    /*
    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ');
	*/

    if (selMarca.value != 0)
        sWHERE += (' AND (b.MarcaID = ' + selMarca.value + ') ');
    if (selLinha.value != 0)
        sWHERE += (' AND (c.ConceitoID = ' + selLinha.value + ') ');
    if (selGerenteProduto.value != 0)
        sWHERE += (' AND (a.ProprietarioID = ' + selGerenteProduto.value + ') ');

    dsoFamilia.SQL = 'SELECT 0 AS FamiliaID, \'\' AS Familia ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS FamiliaID, c.Conceito AS Familia ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Familia';

    dsoFamilia.ondatasetcomplete = preencheFamilia_DSC;
    dsoFamilia.Refresh();
}

function preencheFamilia_DSC() {

    clearComboEx(['selFamilia']);

    if (!(dsoFamilia.recordset.BOF || dsoFamilia.recordset.EOF)) {
        dsoFamilia.recordset.moveFirst;

        while (!dsoFamilia.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoFamilia.recordset['Familia'].value;
            oOption.value = dsoFamilia.recordset['FamiliaID'].value;
            selFamilia.add(oOption);
            dsoFamilia.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selFamilia', glb_nValue);

        selFamilia.disabled = ((selFamilia.length <= 1) ? true : false);
    }

}

function preencheMarca() {
    glb_nValue = selMarca.value;

    setConnection(dsoMarca);

    var sWHERE = '';

    /*
    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ');
    */

    if (selFamilia.value != 0)
        sWHERE += (' AND (b.ProdutoID = ' + selFamilia.value + ') ');
    if (selLinha.value != 0)
        sWHERE += (' AND (c.ConceitoID = ' + selLinha.value + ') ');
    if (selGerenteProduto.value != 0)
        sWHERE += (' AND (a.ProprietarioID = ' + selGerenteProduto.value + ') ');

    dsoMarca.SQL = 'SELECT 0 AS MarcaID, \'\' AS Marca ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS MarcaID, c.Conceito AS Marca ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.MarcaID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Marca';

    dsoMarca.ondatasetcomplete = preencheMarca_DSC;
    dsoMarca.Refresh();
}

function preencheMarca_DSC() {
    clearComboEx(['selMarca']);

    if (!(dsoMarca.recordset.BOF || dsoMarca.recordset.EOF)) {
        dsoMarca.recordset.moveFirst;

        while (!dsoMarca.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoMarca.recordset['Marca'].value;
            oOption.value = dsoMarca.recordset['MarcaID'].value;
            selMarca.add(oOption);
            dsoMarca.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selMarca', glb_nValue);

        selMarca.disabled = ((selMarca.length <= 1) ? true : false);
    }
}

function preencheLinha() {
    glb_nValue = selLinha.value;

    setConnection(dsoLinha);

    var sWHERE = '';

    /*
    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ' +
        'AND (b.MarcaID = ' + selMarca.value + ') ');
    */
    if (selFamilia.value != 0)
        sWHERE += (' AND (b.ProdutoID = ' + selFamilia.value + ') ');
    if (selMarca.value != 0)
        sWHERE += (' AND (b.MarcaID = ' + selMarca.value + ') ');
    if (selGerenteProduto.value != 0)
        sWHERE += (' AND (a.ProprietarioID = ' + selGerenteProduto.value + ') ');

    dsoLinha.SQL = 'SELECT 0 AS LinhaID, \'\' AS Linha ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS LinhaID, c.Conceito AS Linha ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.LinhaProdutoID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Linha';

    dsoLinha.ondatasetcomplete = preencheLinha_DSC;
    dsoLinha.Refresh();
}

function preencheLinha_DSC() {
    clearComboEx(['selLinha']);

    if (!(dsoLinha.recordset.BOF || dsoLinha.recordset.EOF)) {
        dsoLinha.recordset.moveFirst;

        while (!dsoLinha.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoLinha.recordset['Linha'].value;;
            oOption.value = dsoLinha.recordset['LinhaID'].value;
            selLinha.add(oOption);
            dsoLinha.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selLinha', glb_nValue);

        selLinha.disabled = ((selLinha.length <= 1) ? true : false);
    }
}

function preencheGP() {
    glb_nValue = selGerenteProduto.value;

    setConnection(dsoGP);

    var sWHERE = '';

    if (selFamilia.value != 0)
        sWHERE += (' AND (b.ProdutoID = ' + selFamilia.value + ') ');

    if (selMarca.value != 0)
        sWHERE += (' AND (b.MarcaID = ' + selMarca.value + ') ');

    if (selLinha.value != 0)
        sWHERE += (' AND (c.ConceitoID = ' + selLinha.value + ') ');

    dsoGP.SQL = 'SELECT 0 AS GerenteProdutoID, \'\' AS Gerente ' +
	    'UNION ' +
	    'SELECT DISTINCT a.ProprietarioID AS GerenteProdutoID, d.Fantasia AS Gerente ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
                'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.LinhaProdutoID) ' +
                'INNER JOIN Pessoas d WITH(NOLOCK) ON (a.ProprietarioID = d.PessoaID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Gerente';

    dsoGP.ondatasetcomplete = preencheGP_DSC;
    dsoGP.Refresh();
}

function preencheGP_DSC() {
    clearComboEx(['selGerenteProduto']);

    if (!(dsoGP.recordset.BOF || dsoGP.recordset.EOF)) {
        dsoGP.recordset.moveFirst;

        while (!dsoGP.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoGP.recordset['Gerente'].value;;
            oOption.value = dsoGP.recordset['GerenteProdutoID'].value;
            selGerenteProduto.add(oOption);
            dsoGP.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selGerenteProduto', glb_nValue);

        selGerenteProduto.disabled = ((selGerenteProduto.length <= 1) ? true : false);
    }
}

function preencheEstados() {
    
    setConnection(dsoEstados);

    var sWHERE = '';

    dsoEstados.SQL = 'SELECT 0 As Ordem, 0 AS EstadoID, \'\' AS Estado ' +
	    'UNION ' +
	    'SELECT a.Ordem, a.EstadoID, b.RecursoFantasia AS Estado ' +
            'FROM (SELECT 1 AS Ordem, 1   AS EstadoID UNION ALL ' +
                       'SELECT 2 AS Ordem, 63  AS EstadoID UNION ALL ' +
                       'SELECT 3 AS Ordem, 11  AS EstadoID UNION ALL ' +
                       'SELECT 4 AS Ordem, 155 AS EstadoID UNION ALL ' +
                       'SELECT 5 AS Ordem, 2   AS EstadoID UNION ALL ' +
                       'SELECT 6 AS Ordem, 160 AS EstadoID UNION ALL ' +
                       'SELECT 7 AS Ordem, 159 AS EstadoID) a ' + 
                     'INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID)' +
               'ORDER BY Ordem';

    dsoEstados.ondatasetcomplete = preencheEstados_DSC;
    dsoEstados.Refresh();
}

function preencheEstados_DSC() {
    clearComboEx(['selEstado']);

    if (!(dsoEstados.recordset.BOF || dsoEstados.recordset.EOF)) {
        dsoEstados.recordset.moveFirst;

        while (!dsoEstados.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoEstados.recordset['Estado'].value;;
            oOption.value = dsoEstados.recordset['EstadoID'].value;
            selEstado.add(oOption);
            dsoEstados.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selEstado', glb_nValue);

        selEstado.disabled = ((selEstado.length <= 1) ? true : false);
    }
}
/*****************************************************
Preenchimento dos Combos Dinamicos de Busca - Fim
******************************************************/

/*****************************************************
Metodos OnChange - Inicio
******************************************************/
function selFamilia_onchange() {
    if (selMarca.value == 0)
        preencheMarca();
    if(selLinha.value == 0)
        preencheLinha();
    if (selGerenteProduto.value == 0)
        preencheGP();
}

function selMarca_onchange() {
    if (selLinha.value == 0)
        preencheLinha();
    if (selGerenteProduto.value == 0)
        preencheGP();
    if (selFamilia.value == 0)
        preencheFamilia();
}

function selLinha_onchange() {
    if (selMarca.value == 0)
        preencheMarca();
    if (selGerenteProduto.value == 0)
        preencheGP();
    if (selFamilia.value == 0)
        preencheFamilia();
}

function selGerenteProduto_onchange() {
    if (selMarca.value == 0)
        preencheMarca();
    if (selLinha.value == 0)
        preencheLinha();
    if (selFamilia.value == 0)
        preencheFamilia();
}

function chkHistorico_onclick() {
    ajustaDivs_Historico();
}

function chkNaoCategorizados_onclick()
{
    btn_onclick(btnListar);
}

function chkDeletado_onclick()
{
    btn_onclick(btnListar);
}

function selDiv_onchange()
{
    glb_nPointToaSendDataToServer = 0;
    glb_nItensAtualizados = 0;
    glb_nMCminima = 0;
    glb_RowEdit = 0;
    glb_RowEdit_Atualiza = 0;
    glb_ColEdit = 0;
    glb_RowFocus = 0;
    glb_nAtualizaValores = 0;
    glb_nDifMC = new Array();
    glb_nControlBtn = 0;
    
    glb_ControlDiv = selDiv.value;
    ajustaDivs();
}

function txtBuscaCategoria_onkeypress() {
    if (event.keyCode == 13) {
        btn_onclick(btnBuscar);        
    }
}

function txtBusca_onkeypress() {
    if (event.keyCode == 13) {
        btn_onclick(btnListar);
    }
}

/*****************************************************
Metodos OnChange - Fim
******************************************************/
//Mascara do campo data de Agendamento
function DataHora(evento, objeto)
{
    var keypress = (window.event) ? event.keyCode : evento.which;
    campo = eval(objeto);

    if (campo.value == '00/00/0000 00:00:00') {
        campo.value = "";
    }

    caracteres = '0123456789';
    separacao1 = '/';
    separacao2 = ' ';
    separacao3 = ':';
    conjunto1 = 2;
    conjunto2 = 5;
    conjunto3 = 10;
    conjunto4 = 13;
    conjunto5 = 16;

    if ((caracteres.search(String.fromCharCode(keypress)) != -1) && campo.value.length < (19)) {
        if (campo.value.length == conjunto1)
            campo.value = campo.value + separacao1;
        else if (campo.value.length == conjunto2)
            campo.value = campo.value + separacao1;
        else if (campo.value.length == conjunto3)
            campo.value = campo.value + separacao2;
        else if (campo.value.length == conjunto4)
            campo.value = campo.value + separacao3;
        else if (campo.value.length == conjunto5)
            campo.value = campo.value + separacao3;
    }
    else
        event.returnValue = false;
}

/********************************************************************
Evento de grid particular desta pagina (GRID)   
********************************************************************/
function js_fg_DblClick(grid, Row, Col) {   

    if ((glb_ControlDiv != 0)) {
        if ((Col == getColIndexByColKey(grid, 'PrecoPraticado')) || (Col == getColIndexByColKey(grid, 'MC*')))
        {
            glb_RowEdit_Atualiza = Row;
            lockControlsInModalWin(true);

            if (glb_ControlDiv == 2)
                atualizaValores(glb_RowEdit_Atualiza);
        }
        else if (Col == getColIndexByColKey(grid, 'Link'))
        {
            var sURL = grid.TextMatrix(Row, getColIndexByColKey(grid, 'URL_Marketplace'));

            if (sURL != "")
                window.open(sURL);
        }
        else if (Col == getColIndexByColKey(grid, 'Deletado') && Row == 1)
        {
            for (i = 2; i < fg.Rows; i++)
            {
                if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'Deletado')) == 0)
                    grid.TextMatrix(i, getColIndexByColKey(grid, 'Deletado')) = -1;
                else
                    grid.TextMatrix(i, getColIndexByColKey(grid, 'Deletado')) = 0;
            }
            
        }
        else if (Col == getColIndexByColKey(grid, 'Alterado') && Row == 1)
        {
            for (i = 2; i < fg.Rows; i++)
            {
                if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'Alterado')) == 0)
                    grid.TextMatrix(i, getColIndexByColKey(grid, 'Alterado')) = -1;
                else
                    grid.TextMatrix(i, getColIndexByColKey(grid, 'Alterado')) = 0;
            }

        }

    }
    else if (glb_ControlDiv == 0)
    { 
        if (Col == getColIndexByColKey(grid, 'OK') && Row == 1)
        {
            for (i = 2; i < fg.Rows; i++)
            {
                if (grid.ValueMatrix(i, getColIndexByColKey(grid, 'OK')) == 0)
                    grid.TextMatrix(i, getColIndexByColKey(grid, 'OK')) = -1;
                else
                    grid.TextMatrix(i, getColIndexByColKey(grid, 'OK')) = 0;
            }
        }
    }
}

function js_fgBusca_DblClick(grid, Row, Col) {
    copyCategoria(Row);
}

function paintReadOnlyRow(grid) {
    var i, j, k;
    var origRow = 0;
    var origCol = 0;
    var EhReadOnly = 0;

    // Colunas read only sao coloridas
    if (grid.Rows > 1) {
        origRow = grid.Row;
        origCol = grid.Col;

        for (i = 1; i < grid.Rows; i++) {
            with (grid) {
                EhReadOnly = getCellValueByColKey(grid, 'ProdutoParticipante', i);

                if (EhReadOnly == true)
                {
                    Select(i, getColIndexByColKey(grid, 'OK'), null, null);
                    FillStyle = 1;
                    CellBackColor = 0XDCDCDC;
                    FillStyle = 0;
                    glb_aGridOk = [grid.id, true];
                }
            }
        }

        grid.Row = origRow;
        grid.Col = origCol;
    }
}

function js_ModalVendasDigitais_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only
    if (!glb_GridIsBuilding) {
       /* if (glb_FreezeRolColChangeEvents)
            return true;*/
        if (currCellIsLocked(grid, NewRow, NewCol))
        {
            if (NewCol == getColIndexByColKey(grid, 'Alterado'))
                grid.TextMatrix(NewRow, NewCol) = 0;

            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);


        //glb_RowEdit = OldRow;
        //GridLinhaTotal(fg);
    }
    //return null;

}

function js_ModalVendasDigitais_AfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {

    if ((glb_ControlDiv != 0)) {
        glb_RowEdit = NewRow;

        if (glb_ControlDiv == 2)
            startDynamicGridsHistorico();
        else if (glb_ControlDiv == 1)
            paramBusca(OldRow, NewRow);
    }
}

function cellIsLocked(grid, nRow, nCol) {
    var retVal = true;

    if ((grid.id == 'fg') && ((nCol == getColIndexByColKey(grid, 'OK')) && (getCellValueByColKey(grid, 'ProdutoParticipante', nRow) == true)))
        retVal = false;
    if ((grid.id == 'fg') && ((nCol == getColIndexByColKey(grid, 'Alterado')) && (getCellValueByColKey(grid, 'MC*', nRow) < 0)))
        retVal = false;

    return retVal;
}

function js_ModalVendasDigitais_AfterEdit(grid, nRow, nCol, OldRow, OldCol, NewRow, NewCol)
{
    var nType;
      
    if ((nCol == getColIndexByColKey(grid, 'PrecoPraticado'))) {

        grid.TextMatrix(nRow, nCol) = treatNumericCell(fg.TextMatrix(nRow, nCol));
        //glb_chkOK[nRow] = true;
        if ((OldCol == getColIndexByColKey(grid, 'PrecoPraticado')) || (NewCol == getColIndexByColKey(grid, 'PrecoPraticado'))) {
            if (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'PrecoPraticado')) > 0)
                grid.TextMatrix(nRow, getColIndexByColKey(grid, 'Alterado')) = -1;
            else
                grid.TextMatrix(nRow, getColIndexByColKey(grid, 'Alterado')) = 0;
            
            glb_RowEdit_Atualiza = nRow;
            glb_nAtualizaValores = 1;
        }
        
    }
    if ((nCol == getColIndexByColKey(grid, 'Deletado'))) {
        //glb_chkOK[nRow] = true;
        if (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'Deletado')) == -1)
            grid.TextMatrix(nRow, getColIndexByColKey(grid, 'Alterado')) = -1;
        if (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'Deletado')) == 0)
            grid.TextMatrix(nRow, getColIndexByColKey(grid, 'Alterado')) = -1;
    }
    if (((OldCol == getColIndexByColKey(grid, 'OK'))) && (glb_ControlDiv == 0)) {
        if (grid.ValueMatrix(OldRow, getColIndexByColKey(grid, 'ProdutoParticipante'))) {
            grid.EditText = 0;
            grid.TextMatrix(OldRow, getColIndexByColKey(grid, 'OK')) = 0;
        }
    }
    if ((NewCol == getColIndexByColKey(grid, 'OK'))) {
        if (grid.ValueMatrix(NewRow, getColIndexByColKey(grid, 'ProdutoParticipante'))) {
            grid.EditText = 0;
            grid.TextMatrix(NewRow, getColIndexByColKey(grid, 'OK')) = 0;
        }
    }
    if ((nCol == getColIndexByColKey(grid, 'PrecoFabricante'))) {
        grid.TextMatrix(nRow, nCol) = treatNumericCell(fg.TextMatrix(nRow, nCol));

        if (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'PrecoFabricante')) > 0)
            grid.TextMatrix(nRow, getColIndexByColKey(grid, 'Alterado')) = -1;
    }
    
    if (glb_nAtualizaValores == 1) {
        atualizaValores(nRow);
    }

    setTotalLine();
    
}

function js_ModalVendasDigitais_BeforeEdit(grid, OldRow, OldCol, NewRow, NewCol, Cancel)
{
    fg.Editable = false;

    var nEstadoID = 0;    

    glb_RowFocus = OldRow;
    glb_ColEdit = OldCol;

    if (glb_ControlDiv == 0) {
        if ((NewCol == getColIndexByColKey(grid, 'OK'))) {
            if (grid.ValueMatrix(NewRow, getColIndexByColKey(grid, 'ProdutoParticipante'))) {
                grid.EditText = 0;
                grid.TextMatrix(OldRow, getColIndexByColKey(grid, 'OK')) = 0;
            }
        }
    }
    if (glb_ControlDiv == 2)
    {
        nEstadoID = fg.ValueMatrix(NewRow, getColIndexByColKey(fg, 'EstadoID'));

        if ((NewCol == getColIndexByColKey(grid, 'Alterado'))) {
            if ((grid.ValueMatrix(NewRow, getColIndexByColKey(grid, 'DifMC*')) < 0) && (nEstadoID != 15))  {
                grid.EditText = 0;
                grid.TextMatrix(OldRow, getColIndexByColKey(grid, 'Alterado')) = 0;
            }
        }
    }
  
    fg.Editable = true;

}

function getFirstRowColAllowed() {
    var i;
    var nValidRow = 0;
    var nValidCol = 0;
    
    if (glb_ControlDiv == 0) {
        for (i = 1; i < fg.Rows; i++)
        {
            // Nao permite alteracao
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ProdutoParticipante')))
                continue;
            else {
                nValidCol = getColIndexByColKey(fg, 'OK');
                nValidRow = i;
                break;
            }
        }
    }
    else if (glb_ControlDiv == 2)
    {
        for (i = 1; i < fg.Rows; i++)
        {
            // Nao permite alteracao
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'MC*')) < 0)
                continue;
            else {
                nValidCol = getColIndexByColKey(fg, 'Alterado');
                nValidRow = i;
                break;
            }
        }
    }

    if ((nValidCol + nValidRow) > 0)
        return new Array(nValidRow, nValidCol);
    else
        return null;
}

function currCellIsLocked(grid, nRow, nCol)
{
    var bRetVal = false;

    if (glb_ControlDiv == 0) {
        if (grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'ProdutoParticipante')))
            bRetVal = true;
    }
    else if (glb_ControlDiv == 2) {
        if (nCol == getColIndexByColKey(grid, 'Alterado'))
            if ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'DifMC*')) < 0) && (fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'EstadoID')) != 15))
            bRetVal = true;
    }

    return bRetVal;
}

function js_ModalVendasDigitais_EnterCell(grid, nRow, nCol, OldRow, OldCol, NewRow, NewCol)
{
    //if (glb_ControlDiv != 2)
    if (glb_GridIsBuilding)
        return null;

    js_fg_EnterCell(grid);  
}
/********************************************************************
Eventos para inserir dados no BD   
********************************************************************/
function saveDataInGrid()
{
    var strPars = '';
    var nBytesAcum = 0;
    var nDataLen = 0;
    var nOK = false;
    var i, j;
    var nExclui = 0;
    var nCont = 0;
    var nContAnt = 0;

    lockControlsInModalWin(true);
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_nItensAtualizados = 0;

    for (i = 1; i < fg.Rows; i++)
    {
        nOK = getCellValueByColKey(fg, 'OK', i);

        if (nOK == "-1")
            glb_chkOK[i] = true;
        else
            glb_chkOK[i] = false;

        if ((strPars == '') && (glb_chkOK[i] == true)) {
            glb_nItensAtualizados += 1;

            strPars += '?nRelacaoID=' + escape(getCellValueByColKey(fg, 'RelacaoID', i));
            strPars += '&nProgramaMarketingID=' + escape(getCellValueByColKey(fg, 'ProgramaMarketingID', i));
            strPars += '&nUsuarioID=' + escape(glb_nUsuarioID);
        }
        else if ((strPars != '') && (glb_chkOK[i] == true)) {
            glb_nItensAtualizados += 1;

            strPars += '&nRelacaoID=' + escape(getCellValueByColKey(fg, 'RelacaoID', i));
            strPars += '&nProgramaMarketingID=' + escape(getCellValueByColKey(fg, 'ProgramaMarketingID', i));
            strPars += '&nUsuarioID=' + escape(glb_nUsuarioID);
        }
    }

    if ((strPars != ''))
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

   sendDataToServer();
}

function sendDataToServer()
{
    var sMensagem = '';

    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/inserirprodutosmarketplace.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            /*
            lockControlsInModalWin(false);

            sMensagem = glb_nItensAtualizados.toString() + ' Registro(s) inserido(s) no Gerenciamento de Marketplace.\n';

            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
            
            lockControlsInModalWin(true);
            */
            startDynamicGrids();
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

/********************************************************************/
function saveDataInGrid_Gerenciar()
{
    var strPars = '';
    var nBytesAcum = 0;
    var nDataLen = 0;
    var nAlterado = false;
    var nDeletado = false;
    var nDifMC = 0;
    var nProdutoID = 0;
    var nProgramaMarketing = 0;
    var nDel;
    var i, j;
    var nExclui = 0;
    var nCont = 0;
    var nContAnt = 0;
    var dateTimeInSQLFormat = null;
    var dataAgendamento;
    var nEstadoID = 0;  
    

    // Analiza o campo txtDataAgendamento
    txtDataAgendamento.value = trimStr(txtDataAgendamento.value);

    if (!((txtDataAgendamento.value == null) || (txtDataAgendamento.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataAgendamento)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataAgendamento.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = '\'' + dateTimeInSQLFormat + '\'';
    else
        dateTimeInSQLFormat = 'NULL';

    lockControlsInModalWin(true);
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_nItensAtualizados = 0;

    for (i = 1; i < fg.Rows; i++)
    {
        nProdutoID = getCellValueByColKey(fg, 'ProdutoID*', i);
        nProgramaMarketing = getCellValueByColKey(fg, 'ProgramaMarketing*', i);
        nAlterado = getCellValueByColKey(fg, 'Alterado', i);
        nDeletado = getCellValueByColKey(fg, 'Deletado', i);
        nDifMC = getCellValueByColKey(fg, 'DifMC*', i);
        nEstadoID = fg.ValueMatrix(i, getColIndexByColKey(fg, 'EstadoID'));

        if (nAlterado == "-1")
            glb_chkOK[i] = 1;
        else
            glb_chkOK[i] = 0;

        if (nDeletado == "-1")
            glb_chkDel[i] = 1;
        else
            glb_chkDel[i] = 0;        

        if (((parseFloat(replaceStr(nDifMC, ',', '.')) < 0) && (glb_chkOK[i] == true)) && (nEstadoID != 15))
        {

            glb_nDifMC[i] = 1;
            glb_nMCminima = 1;

            if (window.top.overflyGen.Alert('Produto: ' + nProdutoID + '\n' + 'ProgramaMarketing: ' + nProgramaMarketing + '\nEst� com margem abaixo da Margem m�nima.') == 0)
                return null;
        }

        else if ((strPars == '') && (glb_chkOK[i] == true) && (glb_nDifMC[i] != 1)) {
            glb_nItensAtualizados += 1;

            strPars += '?nRelPesConVendaDigitalID=' + escape(getCellValueByColKey(fg, 'RelPesConVendaDigitalID', i));
            strPars += '&nPrecoFabricante=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoFabricante'))); 
            strPars += '&nPrecoPraticado=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoPraticado')));
            strPars += '&nDeletado=' + escape(glb_chkDel[i]);
            strPars += '&nUsuarioID=' + escape(glb_nUsuarioID); 
            strPars += '&sDtAgendamento=' + escape(dateTimeInSQLFormat);
        }
        else if ((strPars != '') && (glb_chkOK[i] == true) && (glb_nDifMC[i] != 1)) {
            glb_nItensAtualizados += 1;

            strPars += '&nRelPesConVendaDigitalID=' + escape(getCellValueByColKey(fg, 'RelPesConVendaDigitalID', i));
            strPars += '&nPrecoFabricante=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoFabricante')));
            strPars += '&nPrecoPraticado=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoPraticado')));
            strPars += '&nDeletado=' + escape(glb_chkDel[i]);
            strPars += '&nUsuarioID=' + escape(glb_nUsuarioID);
            strPars += '&sDtAgendamento=' + escape(dateTimeInSQLFormat);
        }
    }

    if ((strPars != '')) 
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;


    sendDataToServer_Gerenciar();
    
}

function sendDataToServer_Gerenciar()
{
    var sMensagem = '';

    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoPublica.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/publicarprodutosmarketplace.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoPublica.ondatasetcomplete = sendDataToServer_Gerenciar_DSC;
            dsoPublica.refresh();
        }
        else {
            /*
            lockControlsInModalWin(false);

            sMensagem = glb_nItensAtualizados.toString() + ' Registro(s) publicado(s) no Marketplace.\n';
            lockControlsInModalWin(true);
           

            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;*/
            if (glb_nMCminima == 0)
                startDynamicGridsGerenciar();
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_Gerenciar_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer_Gerenciar()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

/********************************************************************/
function saveDataInGrid_Categorizar() {
    var strPars = '';
    var nBytesAcum = 0;
    var nDataLen = 0;
    var nOK = false;
    var nProdutoID = 0;
    var nProgramaMarketingID = 0;
    var sCodigoMarketplace = 0;
    var i, j;
   

    lockControlsInModalWin(true);
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_nItensAtualizados = 0;

    for (i = 1; i < fg.Rows; i++)
    {
        nProdutoID = getCellValueByColKey(fg, 'ProdutoID*', i);
        nProgramaMarketingID = getCellValueByColKey(fg, 'ProgramaMarketingID', i);
        sCodigoMarketplace = getCellValueByColKey(fg, 'CodigoMarketplace', i);
        nOK = getCellValueByColKey(fg, 'OK', i);        

        if (nOK == "-1")
            glb_chkOK[i] = 1;
        else
            glb_chkOK[i] = 0;

        if ((strPars == '') && (glb_chkOK[i] == true)) {
            glb_nItensAtualizados += 1;

            strPars += '?nProdutoID=' + escape(nProdutoID);
            strPars += '&nProgramaMarketingID=' + escape(nProgramaMarketingID);
            strPars += '&sCodigoMarketplace=' + escape(sCodigoMarketplace);
        }
        else if ((strPars != '') && (glb_chkOK[i] == true)) {
            glb_nItensAtualizados += 1;

            strPars += '&nProdutoID=' + escape(nProdutoID);
            strPars += '&nProgramaMarketingID=' + escape(nProgramaMarketingID);
            strPars += '&sCodigoMarketplace=' + escape(sCodigoMarketplace);
        }
    }

    if ((strPars != ''))
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

    sendDataToServer_Categorizar();
}

function sendDataToServer_Categorizar() {
    var sMensagem = '';

    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGravaCategoria.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/inserircategoriasmarketplace.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGravaCategoria.ondatasetcomplete = sendDataToServer_Categorizar_DSC;
            dsoGravaCategoria.refresh();
        }
        else {
            /*
            lockControlsInModalWin(false);

            sMensagem = glb_nItensAtualizados.toString() + ' Registro(s) categorizado(s).\n';
            lockControlsInModalWin(true);
            
            //startDynamicGridsCategorias();

            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;*/

            startDynamicGridsCategorizar();
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_Categorizar_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer_Categorizar()', INTERVAL_BATCH_SAVE, 'JavaScript');
}
/*******************************************************************/
function sendDatatoServer_Setup()
{
    var strPars = '';
    var nLocalidadeID = 0;
    var nLocMarketplaceID = 0;
    var nTempoAtualizacao = 0;
    var nTipoParametroID = 0;

    lockControlsInModalWin(true);
    
    nLocalidadeID = getCellValueByColKey(fg, 'LocalidadeID', fg.Row);
    nLocMarketplaceID = getCellValueByColKey(fg, 'LocMarketplaceID', fg.Row);
    nTempoAtualizacao = getCellValueByColKey(fg, 'Valor', fg.Row);
    nTipoParametroID = getCellValueByColKey(fg, 'TipoParametroID', fg.Row);

    strPars += '?nLocalidadeID=' + escape(nLocalidadeID);
    strPars += '&nLocMarketplaceID=' + escape(nLocMarketplaceID);
    strPars += '&nValor=' + escape(nTempoAtualizacao);
    strPars += '&nControlBtn=' + escape(glb_nControlBtn);
    strPars += '&nTipoParametroID=' + escape(nTipoParametroID);

    try {
        dsoSetup.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/setupmarketplace.aspx' + strPars;
        dsoSetup.ondatasetcomplete = sendDatatoServer_Setup_DSC;
        dsoSetup.refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDatatoServer_Setup_DSC()
{
    glb_nControlBtn = 0;
    startDynamicGridsSetup();
}

/*******************************************************************
Eventos para altera��es na modal, ajustes e recalculos.
********************************************************************/
function ajustaDivs() {

    var nHeight = "";

    //Modo Incluir
    if (glb_ControlDiv == 0) {

        nHeight = parseInt(divGrids.currentStyle.height, 10);

        divFG.style.height = nHeight;
        fg.style.height = nHeight;

        elem = lblBusca;
        with (elem.style) {
            left = 350;
        }
        elem = txtBusca;
        with (elem.style) {
            left = 350;
            width = 422;
        }

        lblEstoque.style.visibility = 'inherit';
        chkEstoque.style.visibility = 'inherit';
        lblIncluidos.style.visibility = 'inherit';
        chkIncluidos.style.visibility = 'inherit';
        lblEstoque.style.left = 5;
        chkEstoque.style.left = 0;
        lblIncluidos.style.left = parseInt(lblEstoque.currentStyle.left, 10) + parseInt(lblEstoque.currentStyle.height, 10) + 12;
        chkIncluidos.style.left = parseInt(chkEstoque.currentStyle.left, 10) + parseInt(chkEstoque.currentStyle.height, 10) + 5;

        chkEstoque.checked = true;

        btnGravar.disabled = true;
        divGerenciar.style.visibility = 'hidden';
        //divIncluir.style.visibility = 'inherit';
        divFGHistorico.style.visibility = 'hidden';
        btnGravar.value = 'Gravar';
        secText('Vendas Digitais - Incluir', 1);

        //divIncluir.style.visibility = 'hidden';
        divCategorizar.style.visibility = 'hidden';
        divBuscaCategoria.style.visibility = 'hidden';
        divFGBusca.style.visibility = 'hidden';
        divSetup.style.visibility = 'hidden';

        lblEmpresa.style.visibility = 'inherit';
        lblGerenteProduto.style.visibility = 'inherit';
        lblLinha.style.visibility = 'inherit';
        selEmpresa.style.visibility = 'inherit';
        selGerenteProduto.style.visibility = 'inherit';
        selLinha.style.visibility = 'inherit';
        lblPrgMarketing.style.visibility = 'inherit';
        selPrgMarketing.style.visibility = 'inherit';
        lblFamilia.style.visibility = 'inherit';
        selFamilia.style.visibility = 'inherit';
        lblMarca.style.visibility = 'inherit';
        selMarca.style.visibility = 'inherit';
        lblBusca.style.visibility = 'inherit';
        txtBusca.style.visibility = 'inherit';
        /*
        lblPrgMarketing.style.left = parseInt(selEmpresa.currentStyle.left, 10) + parseInt(selEmpresa.currentStyle.width, 10) + 5;
        selPrgMarketing.style.left = parseInt(selEmpresa.currentStyle.left, 10) + parseInt(selEmpresa.currentStyle.width, 10) + 5;
        lblFamilia.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        selFamilia.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        lblMarca.style.left = parseInt(selFamilia.currentStyle.left, 10) + parseInt(selFamilia.currentStyle.width, 10) + 5;
        selMarca.style.left = parseInt(selFamilia.currentStyle.left, 10) + parseInt(selFamilia.currentStyle.width, 10) + 5;
        lblBusca.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        txtBusca.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        */
        startGridInterface(fg);
        fg.Cols = 0;
        fg.Cols = 1;
        fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
        fg.Redraw = 2;

        startGridInterface(fgHistorico);
        fgHistorico.Cols = 0;
        fgHistorico.Cols = 1;
        fgHistorico.ColWidth(0) = parseInt(divFGHistorico.currentStyle.width, 10) * 18;
        fgHistorico.Redraw = 2;
    }
    else if (glb_ControlDiv == 1) {
        nHeight = "260px";

        divFG.style.height = nHeight;
        fg.style.height = nHeight;

        elem = lblBusca;
        with (elem.style) {
            left = 350;
        }
        elem = txtBusca;
        with (elem.style) {
            left = 350;
            width = 422;
        }

        lblEstoque.style.visibility = 'hidden';
        chkEstoque.style.visibility = 'hidden';
        lblIncluidos.style.visibility = 'hidden';
        chkIncluidos.style.visibility = 'hidden';

        divBuscaCategoria.style.top = parseInt(divFG.style.top, 10) + parseInt(divFG.style.height, 10);
        divBuscaCategoria.style.height = "50px";
        divFGBusca.style.top = parseInt(divBuscaCategoria.style.top, 10) + parseInt(divBuscaCategoria.style.height, 10) + 10;
        divFGBusca.style.height = "110px";
        fgBusca.style.width = parseInt(divFGBusca.style.width, 10);
        fgBusca.style.height = parseInt(divFGBusca.style.height, 10);

        btnGravar.disabled = true;
        //divIncluir.style.visibility = 'hidden';
        divCategorizar.style.visibility = 'inherit';
        divBuscaCategoria.style.visibility = 'inherit';
        divSetup.style.visibility = 'hidden';
        divFGBusca.style.visibility = 'inherit';
        btnGravar.value = 'Gravar';
        secText('Vendas Digitais - Categorizar', 1);

        divGerenciar.style.visibility = 'hidden';
        divFGHistorico.style.visibility = 'hidden';
        fgHistorico.style.visibility = 'hidden';

        //'lblPrgMarketing', 'selPrgMarketing'
        //'lblFamilia', 'selFamilia'
        //'lblMarca', 'selMarca'
        //lblBusca, txtBusca
        lblEmpresa.style.visibility = 'inherit';
        lblGerenteProduto.style.visibility = 'inherit';
        lblLinha.style.visibility = 'inherit';
        selEmpresa.style.visibility = 'inherit';
        selGerenteProduto.style.visibility = 'inherit';
        selLinha.style.visibility = 'inherit';
        lblPrgMarketing.style.visibility = 'inherit';
        selPrgMarketing.style.visibility = 'inherit';
        lblFamilia.style.visibility = 'inherit';
        selFamilia.style.visibility = 'inherit';
        lblMarca.style.visibility = 'inherit';
        selMarca.style.visibility = 'inherit';
        lblBusca.style.visibility = 'inherit';
        txtBusca.style.visibility = 'inherit';
        chkNaoCategorizados.checked = false;
        /*
        lblPrgMarketing.style.left = parseInt(selDiv.currentStyle.left, 10) + parseInt(selDiv.currentStyle.width, 10) + 10;
        selPrgMarketing.style.left = parseInt(selDiv.currentStyle.left, 10) + parseInt(selDiv.currentStyle.width, 10) + 10;
        lblFamilia.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 10;
        selFamilia.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 10;
        lblMarca.style.left = parseInt(selFamilia.currentStyle.left, 10) + parseInt(selFamilia.currentStyle.width, 10) + 10;
        selMarca.style.left = parseInt(selFamilia.currentStyle.left, 10) + parseInt(selFamilia.currentStyle.width, 10) + 10;
        lblNaoCategorizados.style.left = parseInt(selMarca.currentStyle.left, 10) + parseInt(selMarca.currentStyle.width, 10) + 10;
        chkNaoCategorizados.style.left = parseInt(selMarca.currentStyle.left, 10) + parseInt(selMarca.currentStyle.width, 10) + 10;
        lblBusca.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 10;
        txtBusca.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 10;
        */
        txtBuscaCategoria.value = '';

        startGridInterface(fg);
        fg.Cols = 0;
        fg.Cols = 1;
        fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
        fg.Redraw = 2;

        startGridInterface(fgBusca);
        fgBusca.Cols = 0;
        fgBusca.Cols = 1;
        fgBusca.ColWidth(0) = parseInt(divFGBusca.currentStyle.width, 10) * 18;
        fgBusca.Redraw = 2;
    }
    //Modo Gerenciar
    else if (glb_ControlDiv == 2)
    {
        nHeight = parseInt(divGrids.currentStyle.height, 10); //- parseInt(divFGHistorico.currentStyle.height, 10);

        divFG.style.height = nHeight;
        fg.style.height = nHeight;

        elem = lblBusca;
        with (elem.style) {
            left = 450;
        }
        elem = txtBusca;
        with (elem.style) {
            left = 450;
            width = 325;
        }

        preencheEstados();

        lblEstoque.style.visibility = 'inherit';
        chkEstoque.style.visibility = 'inherit';
        lblEstoque.style.left = (parseInt(lblDeletado.currentStyle.left, 10) + parseInt(lblDeletado.currentStyle.height, 10)) + 12;
        chkEstoque.style.left = (parseInt(chkDeletado.currentStyle.left, 10) + parseInt(chkDeletado.currentStyle.height, 10)) + 5;
        lblIncluidos.style.visibility = 'hidden';
        chkIncluidos.style.visibility = 'hidden';

        btnGravar.disabled = true;
        //divIncluir.style.visibility = 'hidden';
        divGerenciar.style.visibility = 'inherit';
        divFGHistorico.style.visibility = 'inherit';
        btnGravar.value = 'Publicar';
        secText('Vendas Digitais - Gerenciar', 1);        

        //divIncluir.style.visibility = 'hidden';
        divCategorizar.style.visibility = 'hidden';
        divBuscaCategoria.style.visibility = 'hidden';
        divSetup.style.visibility = 'hidden';
        divFGBusca.style.visibility = 'hidden';

        divFGHistorico.style.visibility = 'inherit';
        fgHistorico.style.visibility = 'inherit';
        lblEmpresa.style.visibility = 'inherit';
        lblGerenteProduto.style.visibility = 'inherit';
        lblLinha.style.visibility = 'inherit';
        selEmpresa.style.visibility = 'inherit';
        selGerenteProduto.style.visibility = 'inherit';
        selLinha.style.visibility = 'inherit';
        lblPrgMarketing.style.visibility = 'inherit';
        selPrgMarketing.style.visibility = 'inherit';
        lblFamilia.style.visibility = 'inherit';
        selFamilia.style.visibility = 'inherit';
        lblMarca.style.visibility = 'inherit';
        selMarca.style.visibility = 'inherit';
        lblBusca.style.visibility = 'inherit';
        txtBusca.style.visibility = 'inherit';
        /*
        lblPrgMarketing.style.left = parseInt(selEmpresa.currentStyle.left, 10) + parseInt(selEmpresa.currentStyle.width, 10) + 5;
        selPrgMarketing.style.left = parseInt(selEmpresa.currentStyle.left, 10) + parseInt(selEmpresa.currentStyle.width, 10) + 5;
        lblFamilia.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        selFamilia.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        lblMarca.style.left = parseInt(selFamilia.currentStyle.left, 10) + parseInt(selFamilia.currentStyle.width, 10) + 5;
        selMarca.style.left = parseInt(selFamilia.currentStyle.left, 10) + parseInt(selFamilia.currentStyle.width, 10) + 5;
        lblBusca.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        txtBusca.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        */
        chkHistorico.checked = false;
        txtDataAgendamento.value = '';

        startGridInterface(fg);
        fg.Cols = 0;
        fg.Cols = 1;
        fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
        fg.Redraw = 2;

        startGridInterface(fgHistorico);
        fgHistorico.Cols = 0;
        fgHistorico.Cols = 1;
        fgHistorico.ColWidth(0) = parseInt(divFGHistorico.currentStyle.width, 10) * 18;
        fgHistorico.Redraw = 2;
    }
    //Modo SETUP
    else if (glb_ControlDiv == 3) {

        nHeight = parseInt(divGrids.currentStyle.height, 10);

        divFG.style.height = nHeight;
        fg.style.height = nHeight;
        lblEstoque.style.visibility = 'hidden';
        chkEstoque.style.visibility = 'hidden';

        btnGravar.disabled = true;
        btnIncluir.style.left = 790;
        btnExcluir.style.left = 850;

        btnExcluir.disabled = true;

        divGerenciar.style.visibility = 'hidden';
        divSetup.style.visibility = 'inherit';
        //divIncluir.style.visibility = 'inherit';
        divFGHistorico.style.visibility = 'hidden';
        btnGravar.value = 'Gravar';
        secText('Vendas Digitais - Configura��es', 1);

        //divIncluir.style.visibility = 'hidden';
        divCategorizar.style.visibility = 'hidden';
        divBuscaCategoria.style.visibility = 'hidden';
        divFGBusca.style.visibility = 'hidden';

        lblEmpresa.style.visibility = 'hidden';
        lblGerenteProduto.style.visibility = 'hidden';
        lblLinha.style.visibility = 'hidden';
        selEmpresa.style.visibility = 'hidden';
        selGerenteProduto.style.visibility = 'hidden';
        selLinha.style.visibility = 'hidden';
        lblPrgMarketing.style.visibility = 'hidden';
        selPrgMarketing.style.visibility = 'hidden';
        lblFamilia.style.visibility = 'hidden';
        selFamilia.style.visibility = 'hidden';
        lblMarca.style.visibility = 'hidden';
        selMarca.style.visibility = 'hidden';
        lblBusca.style.visibility = 'hidden';
        txtBusca.style.visibility = 'hidden';
        /*
        lblPrgMarketing.style.left = parseInt(selEmpresa.currentStyle.left, 10) + parseInt(selEmpresa.currentStyle.width, 10) + 5;
        selPrgMarketing.style.left = parseInt(selEmpresa.currentStyle.left, 10) + parseInt(selEmpresa.currentStyle.width, 10) + 5;
        lblFamilia.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        selFamilia.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        lblMarca.style.left = parseInt(selFamilia.currentStyle.left, 10) + parseInt(selFamilia.currentStyle.width, 10) + 5;
        selMarca.style.left = parseInt(selFamilia.currentStyle.left, 10) + parseInt(selFamilia.currentStyle.width, 10) + 5;
        lblBusca.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        txtBusca.style.left = parseInt(selPrgMarketing.currentStyle.left, 10) + parseInt(selPrgMarketing.currentStyle.width, 10) + 5;
        */
        startGridInterface(fg);
        fg.Cols = 0;
        fg.Cols = 1;
        fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
        fg.Redraw = 2;

        startGridInterface(fgHistorico);
        fgHistorico.Cols = 0;
        fgHistorico.Cols = 1;
        fgHistorico.ColWidth(0) = parseInt(divFGHistorico.currentStyle.width, 10) * 18;
        fgHistorico.Redraw = 2;
    }
    
}

function ajustaDivs_Historico() 
{ 
    if (chkHistorico.checked) { 
         
        nHeight = "300px";//parseInt(divGrids.currentStyle.height, 10) - parseInt(divFGHistorico.currentStyle.height, 10);
         
        divFG.style.height = nHeight; 
        fg.style.height = nHeight; 
         
        divFGHistorico.style.visibility = 'inherit';
        fgHistorico.style.visibility = 'inherit';
        divFGHistorico.style.top = parseInt(divFG.currentStyle.top, 10) + parseInt(divFG.currentStyle.height, 10) + 10; 
        divFGHistorico.style.height = "120px"; 
        fgHistorico.style.width = parseInt(divFGHistorico.style.width, 10); 
        fgHistorico.style.height = parseInt(divFGHistorico.style.height, 10);        

    } 
    else 
    { 
        nHeight = parseInt(divGrids.currentStyle.height, 10);  
         
        divFG.style.height = nHeight; 
        fg.style.height = nHeight;
        divFGHistorico.style.visibility = 'hidden';
        fgHistorico.style.visibility = 'hidden';
    } 
} 

function atualizaValores(nRow)
{
    /*
    if (glb_nTimerToServer != null) {
        window.clearInterval(glb_nTimerToServer);
        glb_nTimerToServer = null;
    }
    */
    var sSQL;
    var nPrecoListaAjust;
    var nEmpresaID = 0;
    var nProgramaMarketingID = 0;
    var nProdutoID = 0;
    var nPrecoDigitado = 0;

    glb_RowEdit_Atualiza = nRow;

    if (fg.ValueMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'PrecoPraticado')) == 0)
    {
        lockControlsInModalWin(false);
        return null;
    }
    else {
        setConnection(dsoAtualiza);

        nEmpresaID = fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'EmpresaID'));
        nProgramaMarketingID = fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'ProgramaMarketingID'));
        nProdutoID = fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'ProdutoID*'));
        nPrecoDigitado = fg.ValueMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'PrecoPraticado'));
        nLoteID = fg.ValueMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'LoteID'));

        sSQL = "SELECT dbo.fn_Preco_Marketplace( " + nEmpresaID + ", " + nProgramaMarketingID + ", " + nProdutoID + ", " + nPrecoDigitado + ", 2, " + nLoteID + ") AS MargemAjustada, " +
                      "dbo.fn_Preco_Marketplace( " + nEmpresaID + ", " + nProgramaMarketingID + ", " + nProdutoID + ", " + nPrecoDigitado + ", 3, " + nLoteID + ") AS DifMargem, " +
                      "dbo.fn_Preco_Marketplace( " + nEmpresaID + ", " + nProgramaMarketingID + ", " + nProdutoID + ", " + nPrecoDigitado + ", 1, " + nLoteID + ") AS PrecoOverfly ";

        dsoAtualiza.SQL = sSQL;
        dsoAtualiza.ondatasetcomplete = atualizaValores_DSC;
        dsoAtualiza.refresh();
    }

}

function atualizaValores_DSC()
{
    var bgColor;
    var difMC = 0;
    var nMC = 0;
    var nEstadoID = 0;

    glb_GridIsBuilding = true;
    difMC = dsoAtualiza.recordset['DifMargem'].value;
    nMC = dsoAtualiza.recordset['MargemAjustada'].value;
    nEstadoID = fg.ValueMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'EstadoID'));

    if (nMC < -999)
        fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*')) = -999.99;
    else if (nMC > 999)
        fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*')) = 999.99;
    else
        fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*')) = nMC;
    fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'DifMC*')) = dsoAtualiza.recordset['DifMargem'].value;
    fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'PrecoOverfly*')) = dsoAtualiza.recordset['PrecoOverfly'].value;    

    if (glb_nControlline > 0)
        fg.TopRow = Math.max((glb_nControlline - 21), 2);

    if (difMC < 0)
    {
        with (fg) {
            Select(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*'), null, null);
            FillStyle = 1;
            CellBackColor = 0X0000FF; //0XDCDCDC
            FillStyle = 0;           
        }

        if (nEstadoID != 15)
        {
            with (fg)
            {
                Select(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'Alterado'), null, null);
                FillStyle = 1;
                CellBackColor = 0XDCDCDC; //0XFFFFFF
                FillStyle = 0;
                Select(glb_RowFocus, glb_ColEdit, null, null);
            }
        }

              
        bgColor = 0X000000; //0XDC143C
        fg.Cell(7, glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*'), glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*')) = bgColor;

        if (nEstadoID != 15)
            fg.TextMatrix(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'Alterado')) = 0;
    }
    else {
        
        with (fg) {
            Select(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*'), null, null);
            FillStyle = 1;
            CellBackColor = 0XDCDCDC; //0X0000FF
            FillStyle = 0;
        }
        with (fg) {
            Select(glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'Alterado'), null, null);
            FillStyle = 1;
            CellBackColor = 0XFFFFFF; //0XDCDCDC
            FillStyle = 0;
            Select(glb_RowFocus, glb_ColEdit, null, null);
        }
        
        bgColor = 0XDC143C; //0X000000
        fg.Cell(7, glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*'), glb_RowEdit_Atualiza, getColIndexByColKey(fg, 'MC*')) = bgColor;
    }

    fg.ColWidth(getColIndexByColKey(fg, 'MC*')) = 30 * 18;
    
    glb_GridIsBuilding = false;
    //.Editable = true;
    //lockControlsInModalWin(false);

   
    //fg.focus();
    //fg.Col = glb_ColEdit;
    //fg.Row = fg.Row;
    
    if (glb_nControlline > 0)
    {
        glb_nControlline = glb_nControlline + 1;

        AtualizaMC();
    }
}

function paramBusca(OldRow, NewRow)
{
    var nFamiliaAnterior;
    var nFamiliaNova;
    var nMarcaAnterior;
    var nMarcaNova;
    var nProgramaMarketingNovaID;
    var nProgramaMarketingAnteriorID;

    txtBuscaCategoria.value = getCellValueByColKey(fg, 'Busca*', glb_RowEdit);

    nFamiliaAnterior = getCellValueByColKey(fg, 'FamiliaID', OldRow);
    nFamiliaNova = getCellValueByColKey(fg, 'FamiliaID', NewRow);
    nMarcaAnterior = getCellValueByColKey(fg, 'MarcaID', OldRow);
    nMarcaNova = getCellValueByColKey(fg, 'MarcaID', NewRow);
    nProgramaMarketingAnteriorID = getCellValueByColKey(fg, 'ProgramaMarketingID', OldRow);
    nProgramaMarketingNovaID = getCellValueByColKey(fg, 'ProgramaMarketingID', NewRow);

    if ((nFamiliaAnterior != nFamiliaNova) || (nMarcaAnterior != nMarcaNova) || (nProgramaMarketingAnteriorID != nProgramaMarketingNovaID))
    {
        startGridInterface(fgBusca);
        fgBusca.Cols = 0;
        fgBusca.Cols = 1;
        fgBusca.ColWidth(0) = parseInt(divFGBusca.currentStyle.width, 10) * 18;
        fgBusca.Redraw = 2;
    }
}

function copyCategoria(Row)
{
    var copy = getCellValueByColKey(fgBusca, 'Arvore*', Row);
    var mCategoria = getCellValueByColKey(fgBusca, 'CodigoMarketplace*', Row);

    fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'Categoria*')) = copy;
    fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'CodigoMarketplace')) = mCategoria;
    fg.TextMatrix(glb_RowEdit, getColIndexByColKey(fg, 'OK')) = -1;

    fg.ColWidth(3) = fgBusca.ColWidth(0);
}

function setTotalLine() {
    var nAlterados = 0;
    var nTotalPublicacao = 0;

    if (glb_ControlDiv == 0)
    {
        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                nAlterados++;
                //nTotalPublicacao += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoPraticado'));
            }
        }

        if (fg.Rows > 2) {
            fg.TextMatrix(1, getColIndexByColKey(fg, 'LinhaProduto*')) = nAlterados;
            //fg.TextMatrix(1, getColIndexByColKey(fg, 'PrecoPraticado')) = nTotalPublicacao;
        }
    }

    if (glb_ControlDiv == 1)
    {
        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
                nAlterados++;
                //nTotalPublicacao += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoPraticado'));
            }
        }

        if (fg.Rows > 2) {
            fg.TextMatrix(1, getColIndexByColKey(fg, 'ProgramaMarketing*')) = nAlterados;
            //fg.TextMatrix(1, getColIndexByColKey(fg, 'PrecoPraticado')) = nTotalPublicacao;
        }
    }

    if (glb_ControlDiv == 2)
    {
        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Alterado')) != 0) {
                nAlterados++;
                //nTotalPublicacao += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoPraticado'));
            }
        }

        if (fg.Rows > 2) {
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Descricao*')) = nAlterados;
            //fg.TextMatrix(1, getColIndexByColKey(fg, 'PrecoPraticado')) = nTotalPublicacao;
        }
    }
}


/********************************************************************
Funcao criada por programador. SGP
Trima, critica e normaliza (completa hota/min/seg) em campo datetime.
Tambem da mensagens ao usuario e foca o campo se for o caso.
    
O que pode estar digitado no campo
1. Qualquer coisa nao valida
2. So a data valida ou nao
3. A data e a hora valida ou nao
4. A data, a hora e o minuto valida ou nao
5. A data, a hora, o minuto e o segundo valida ou nao

O campo sera completado para dd/mm/yyyy hh:mm:ss
ou para mm/dd/yyyy hh:mm:ss

Parametro:
ctlRef     - referencia ao campo a criticar

Retorno:
true e data/hora validas, caso contrario false
********************************************************************/
function criticAndNormTxtDataTime(ctlRef) {
    var retVal = false;
    var dataAgendamento = '';
    var rExp, aDataAgendamento;

    // referencia ao campo e obrigatoria
    if (ctlRef == null)
        return retVal;

    // trima conteudo do campo ctlRef
    ctlRef.value = trimStr(ctlRef.value);

    // Obtem  texto do label associado ao campo
    var txtLblAss = labelAssociate(ctlRef.id);

    // 1. Campo em branco nao aceita
    if (ctlRef.value == '') {
        if (window.top.overflyGen.Alert('O campo ' + txtLblAss + ' est� em branco...') == 0)
            return null;

        lockControlsInModalWin(false);
        ctlRef.focus();
        return retVal;
    }

    dataCtlRef = ctlRef.value;

    // A funcao chkDataEx valida o campos se ele e uma data valida
    if (chkDataEx(dataCtlRef, true) == false) {
        if (window.top.overflyGen.Alert('O campo ' + txtLblAss + ' n�o � v�lido...') == 0)
            return null;

        lockControlsInModalWin(false);
        ctlRef.focus();
        return retVal;
    }

    // Normaliza o conteudo do campo ctlRef
    ctlRef.value = normalizeDate_DateTime(ctlRef.value);

    if ((ctlRef.value != null) && (ctlRef.value != ''))
        retVal = true;

    return retVal;
}

function AtualizaMC() {
    var nTotalLine = fg.Rows;

    if (glb_nControlline == 0)
        glb_nControlline = 2;

    if (glb_nControlline <= (nTotalLine - 1))
        atualizaValores(glb_nControlline);
    else {
        glb_nControlline = 0;
        return null;
    }
}