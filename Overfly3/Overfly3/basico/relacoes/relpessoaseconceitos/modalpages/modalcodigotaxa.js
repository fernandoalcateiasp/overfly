/********************************************************************
modalcodigotaxa.js

Library javascript para o modalcodigotaxa.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoGen01 = new CDatatransport("dsoGen01");

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // Inicia o carregamento do grid
    fillModalGrid();
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if (fg.Row >= 1 )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' ,
                new Array(fg.TextMatrix(fg.Row, 0), fg.TextMatrix(fg.Row, 1)) );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar C�digo Taxa', 1);
    
    // ajusta elementos da janela
    loadDataAndTreatInterface();
/*
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = ((FONT_WIDTH * 60) + 10);
        height = (MAX_FRAMEHEIGHT_OLD / 2) - ((ELEM_GAP * 6) + 8);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.currentStyle.width, 10);
        height = parseInt(divFG.currentStyle.height, 10) - 1;
    }
*/
}
/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) -
                 ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

function fillModalGrid()
{
    setConnection(dsoGen01);

    dsoGen01.SQL = 'SELECT 0 AS CodigoTaxaID, 0.0000000 AS Cotacao ' + 
                   'UNION ALL SELECT Moedas.CodigoTaxaID, Moedas.Cotacao ' +
	                   'FROM RelacoesPessoas Fornecedores WITH(NOLOCK), RelacoesPessoas_Moedas Moedas WITH(NOLOCK) ' +
	                   'WHERE (Fornecedores.ObjetoID= ' + glb_nFornecedorID + ' AND ' +
	                       'Fornecedores.SujeitoID= ' + glb_nEmpresaID + ' AND ' +
	                       'Fornecedores.TipoRelacaoID=21 AND Fornecedores.RelacaoID=Moedas.RelacaoID) ' +
	                   'ORDER BY CodigoTaxaID';
    
    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.Refresh();
}

function fillModalGrid_DSC()
{
    var i;
    
    startGridInterface(fg);
    
    fg.FrozenCols = 0;
    
    headerGrid(fg,['CT','Taxa'], []);

    fillGridMask(fg,dsoGen01,['CodigoTaxaID','Cotacao'],['',''],['','##,###,###.#######']);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
    
    // ajusta o body do html
    with (modalcodigotaxaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
}

function fg_KeyPress(KeyAscii)
{
    // Enter
    if ( (KeyAscii == 13) && (fg.Row > 0) )
    {
        btn_onclick(btnOK);
    }
}