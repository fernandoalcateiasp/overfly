
<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'Funcao de uso geral, retorna um caracter replicate n vezes
Function replicate(ByVal sString, ByVal nVezes)

    Dim i, sNewString

    sNewString = ""
    
    If (IsNull(sString)) Then
        sString = ""
    End If

    For i=1 To nVezes
        sNewString = sNewString & sString
    Next

    replicate = sNewString

End Function

'Funcao de uso geral, PadL
Function padL(ByVal s, ByVal n, ByVal c)

    If (IsNull(s)) Then
        s = ""
    End If
    
    s = Trim(s)

    padL = replicate(c, n - Len(s)) & s

End Function

'Funcao de uso geral, PadR
Function padR(ByVal s, ByVal n, ByVal c)
    If (IsNull(s)) Then
        s = ""
    End If

    s = Trim(s)
    padR = s & replicate(c, n - Len(s))
End Function
%>

<html id="modalprintHtml" name="modalprintHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalprint.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
   Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_progressbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodalprint.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalprint_relpescon.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, sEmpresaFantasia, nEmpresaCidadeID, nContextoID, nUserID
Dim nIdiomaID, relatorioID, relatorio, EhDefault, relatorioFantasia, dirA1, dirA2, dirA1A2True

nIdiomaID = 0
sCaller = ""
nEmpresaID = 0
sEmpresaFantasia = ""
nEmpresaCidadeID = 0
nContextoID = 0
nUserID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

For i = 1 To Request.QueryString("nIdiomaID").Count    
    nIdiomaID = Request.QueryString("nIdiomaID")(i)
Next

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'sEmpresaFantasia - fantasia da empresa logada
For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

'nEmpresaCidadeID - cidade da empresa logada
For i = 1 To Request.QueryString("nEmpresaCidadeID").Count    
    nEmpresaCidadeID = Request.QueryString("nEmpresaCidadeID")(i)
Next

'nContextoID - ID do contexto
For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

'nUserID - ID do usuario logado
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & sEmpresaFantasia & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaCidadeID = " & nEmpresaCidadeID & ";"
Response.Write vbcrlf

Response.Write "var glb_sCurrDateYYYYMMDD = '" & CStr(Year(Date)) & padL(CStr(Month(Date)), 2, "0") & _
	padL(CStr(Day(Date)), 2, "0") & "';" & vbcrlf
	
Response.Write "var glb_sCurrDateYYYYMMDDhhmmss = '" & CStr(Year(Now)) & padL(CStr(Month(Now)), 2, "0") & _
	padL(CStr(Day(Now)), 2, "0") & padL(CStr(Hour(Now)), 2, "0") & padL(CStr(Minute(Now)), 2, "0") & _
	padL(CStr(Second(Now)), 2, "0") & "';" & vbcrlf
	

'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

Dim rsData, strSQL
Dim rsData1

If ( (sCaller) = "PL" ) Then
    i = 0
Else
    i = 1
End If        

strSQL = "SELECT DISTINCT d.RecursoID AS RelatorioID, dbo.fn_Tradutor(d.Recurso, 246, " & CStr(nIdiomaID) & ", 'A') AS Relatorio, " & _
         "c.EhDefault, d.RecursoFantasia AS RelatorioFantasia, c.Ordem, " & _
         "g.Alterar1 AS A1, g.Alterar2 AS A2 " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _
         "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " & _
         "WHERE a.ObjetoID= " & CStr(nContextoID) & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 " & _
         "AND a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.TipoRecursoID=4 " & _ 
         "AND b.Principal=1 AND b.RecursoID=c.ObjetoID AND c.EstadoID=2 " & _
         "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID " & _
         "AND d.EstadoID=2 AND d.TipoRecursoID=5 AND d.ClassificacaoID=14 AND d.EhDetalhe= " & CStr(i) & " " & _
         "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & CStr(nContextoID) & " )) " & _
         "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
         "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
         "AND e.SujeitoID IN ((SELECT " & CStr(nUserID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(nUserID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND e.RelacaoID=f.RelacaoID " & _
         "AND f.EmpresaID= " & CStr(nEmpresaID) & " AND g.ContextoID= " & CStr(nContextoID) & " " & _
         "AND c.ObjetoID=g.RecursoMaeID AND d.RecursoID=g.RecursoID " & _
         "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " & _
         "ORDER BY c.Ordem"

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
Response.Write "var glb_arrayReports = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf
'------------------
i = 0
Do While (Not rsData.EOF)
        
    relatorioID = rsData.Fields("RelatorioID").Value
    relatorio = rsData.Fields("Relatorio").Value
    EhDefault = rsData.Fields("EhDefault").Value
    relatorioFantasia = rsData.Fields("RelatorioFantasia").Value
    dirA1 = rsData.Fields("A1").Value
    dirA2 = rsData.Fields("A2").Value
    
    If ( dirA1 AND dirA2 ) Then
        dirA1A2True = TRUE        
    Else
        dirA1A2True = FALSE    
    End If
        
    rsData.MoveNext
        
    Do While (Not rsData.EOF)
        If ( CLng(relatorioID) = CLng(rsData.Fields("RelatorioID").Value) ) Then
            'dirX1 e dirX2 e o anterior sempre
            If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
                dirA1A2True = TRUE        
            End If

            dirA1 = dirA1 OR rsData.Fields("A1").Value
            dirA2 = dirA2 OR rsData.Fields("A2").Value
            rsData.MoveNext
        Else
            Exit Do
        End If    
    Loop    
        
    If ( (dirA1) AND (dirA2) ) Then
        If ( NOT (dirA1A2True)  ) Then
            dirA1 = FALSE
            dirA2 = TRUE
        End If
    End If    

    Response.Write "glb_arrayReports[" & CStr(i) & "] = new Array("
    Response.Write Chr(39)
    Response.Write CStr(relatorio)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write CStr(relatorioID)
    Response.Write ","
    Response.Write LCase(CStr(EhDefault))
    Response.Write ","
    Response.Write Chr(39)
    Response.Write CStr(relatorioFantasia)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write LCase(CStr(dirA1))
    Response.Write ","
    Response.Write LCase(CStr(dirA2))
    Response.Write ");"
    i = i + 1

Loop

rsData.Close


Response.Write vbcrlf
Response.Write vbcrlf

'--- Interface dos relatorios do form Pessoas e Conceitos 
'Os estados dos produtos
    
strSQL = "SELECT c.RecursoAbreviado AS Estado, c.RecursoID AS EstadoID, " & _
         "c.RecursoFantasia, b.Ordem AS Ordem " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), RelacoesRecursos b WITH(NOLOCK), Recursos c WITH(NOLOCK) " & _
         "WHERE a.ObjetoID=2131 AND a.SujeitoID=21060 AND a.TipoRelacaoID=1 " & _
         "AND a.EstadoID=2 AND a.MaquinaEstadoID=b.ObjetoID AND b.TipoRelacaoID=3 " & _
         "AND b.EstadoID=2 AND b.SujeitoID=c.RecursoID AND c.EstadoID=2 AND c.RecursoAbreviado <> 'Z' " & _
         "UNION ALL SELECT 'Ag' AS Estado, -1 AS EstadoID, 'Aging' AS RecursoFantasia, 100 AS Ordem "  & _
         "ORDER BY Ordem" 


Set rsData1 = Server.CreateObject("ADODB.Recordset")         

rsData1.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf

Response.Write "</script>"

Response.Write vbcrlf


'--- Final de Interface do Relatorios do form Relacoes Pessoas e Conceitos 

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=oPrinter EVENT=OnPrintFinish>
<!--
 oPrinter_OnPrintFinish();
//-->
</SCRIPT>

</head>

<body id="modalprintBody" name="modalprintBody" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Controle de impressao matricial -->
    <object classid="clsid:1DF93CFF-D5C8-4A6D-983F-19DBAAC698D9" id = "oPrinter" name = "oPrinter" width=0 height=0></object>


    <p id="lblReports" name="lblReports" class="paraNormal"></p>
    <select id="selReports" name="selReports" class="fldGeneral"></select>

<!-- Divs dos relatorios do pessoas e conceitos -->
<%    
'Cria o Div para o relat�rio Relatorio de Estoque
Response.Write vbcrlf
Response.Write "<div id=" & Chr(34) & "divRelatorioEstoque" & Chr(34) & " " & _
               "name="  & Chr(34) & "divRelatorioEstoque" & Chr(34) & " " & _
               "class="  & Chr(34) & "divGeneral"  & Chr(34) & ">"

If ( NOT( rsData1.BOF AND rsData1.EOF) ) Then
    rsData1.MoveFirst
End If
    
'Cria os checkbox de estado
i = 1
While Not rsData1.EOF
    Response.Write vbcrlf   
    Response.Write "<p id=" & Chr(34) & "lblChk_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "name=" & Chr(34) & "lblChk_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
                    rsData1.Fields("Estado").Value & "</p>"
    Response.Write vbcrlf
        
    Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
                   "id=" & Chr(34) & "chk_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "name=" & Chr(34) & "chk_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "title=" & Chr(34) & rsData1.Fields("RecursoFantasia").Value & Chr(34) & " " & _
                   "value=" & Chr(34) & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"
        
    Response.Write vbcrlf
    rsData1.MoveNext
Wend
       
Response.Write vbcrlf    

' Os checkbox dos estoques
' Os proprios vem checados
Dim aEstoques

aEstoques = Array( Array("EPO", "Estoque Pr�prio OK"), Array("EPE", "Estoque Pr�prio Emprestado"), _
    Array("EPD", "Estoque Pr�prio c/ Defeito"), Array("EPC", "Estoque Pr�prio Consertando"), Array("EPT", "Estoque Pr�prio em Tr�nsito"), _
    Array("ETO", "Estoque Terceiros OK"), _
    Array("ETE", "Estoque Terceiros Emprestado"), Array("ETD", "Estoque Terceiros c/ Defeito"), _
    Array("ETC", "Estoque Terceiros Consertando"), Array("EB", "Estoque de Brinde"))

For i=0 To 9
    
    Response.Write "<p id=" & Chr(34) & "lblChk1_ESTOQUE_" & aEstoques(i)(0) & Chr(34) & " " & _
                   "name=" & Chr(34) & "lblChk1_ESTOQUE_" & aEstoques(i)(0) & Chr(34) & " " & _
                   "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
                   aEstoques(i)(0) & "</p>"
    Response.Write vbcrlf
        
    Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
                   "id=" & Chr(34) & "chk1_ESTOQUE_" & aEstoques(i)(0) & Chr(34) & _
                   "name=" & Chr(34) & "chk1_ESTOQUE_" & aEstoques(i)(0) & Chr(34) & _
                   "title=" & Chr(34) & aEstoques(i)(1) & Chr(34) & " " & _
                   "LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & " onclick=" & Chr(34) & "return btn_Estoques1onclick(this) " & Chr(34) & _
                   "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"
        
    Response.Write vbcrlf
Next

%>
<p id="lblMoeda" name="lblMoeda" class="lblGeneral">Moeda</p>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT TOP 1 b.ConceitoID AS fldID, b.SimboloMoeda AS fldName " & _
	"FROM Recursos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) " & _
	"WHERE (a.TipoRecursoID = 1 AND a.EstadoID = 2 AND a.MoedaID = b.ConceitoID) "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

Response.Write "<select id='selMoedas' name='selMoedas' class='fldGeneral' "

If (Not rsData.EOF) Then
	Response.Write "MoedaID = '" & rsData.Fields("fldID").Value & "' " & _
		"SimboloMoeda = '" & rsData.Fields("fldName").Value & "'>" & vbCrlf
Else
	Response.Write ">" & vbCrlf
End If

Response.Write "</select>"

rsData.Close
%>

<%
        
Response.Write "<p id=" & Chr(34) & "lblChkProdutoSeparavel" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblChkProdutoSeparavel" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "PS" & "</p>"

Response.Write vbcrlf   

Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
               "id=" & Chr(34) & "chkProdutoSeparavel" & Chr(34) & " " & _
               "name=" & Chr(34) & "chkProdutoSeparavel" & Chr(34) & " " & _
               "title=" & Chr(34) & "Produto separ�vel? (� separado pela embalagem)" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblChkTemEstoque" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblChkTemEstoque" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Est" & "</p>"

Response.Write vbcrlf
        
Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
               "id=" & Chr(34) & "chkTemEstoque" & Chr(34) & " " & _
               "name=" & Chr(34) & "chkTemEstoque" & Chr(34) & " " & _
               "title=" & Chr(34) & "Somente os produtos que tem em estoque?" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblChkReportaFabricante" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblChkReportaFabricante" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "RF" & "</p>"

Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
               "id=" & Chr(34) & "chkReportaFabricante" & Chr(34) & " " & _
               "name=" & Chr(34) & "chkReportaFabricante" & Chr(34) & " " & _
               "title=" & Chr(34) & "Somente produtos que reportam ao fabricante?" & Chr(34) & " " & _               
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblChkValoracao" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblChkValoracao" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Val" & "</p>"

Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
               "id=" & Chr(34) & "chkValoracao" & Chr(34) & " " & _
               "name=" & Chr(34) & "chkValoracao" & Chr(34) & " " & _
               "title=" & Chr(34) & "Valorizar Estoque?" & Chr(34) & " " & _
               "LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & " onclick=" & Chr(34) & "return btn_Estoques1onclick(this) " & Chr(34) & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"
               
Response.Write "<p id=" & Chr(34) & "lblChkCustoContabil" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblChkCustoContabil" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "CC" & "</p>"

Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
               "id=" & Chr(34) & "chkCustoContabil" & Chr(34) & " " & _
               "name=" & Chr(34) & "chkCustoContabil" & Chr(34) & " " & _
               "title=" & Chr(34) & "1 - Utilizar custo contabil? " & vbcr & "0 - Utilizar custo comercial?" & Chr(34) & " " & _
               "LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & " onclick=" & Chr(34) & "return btn_Estoques1onclick(this) " & Chr(34) & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"
               

Response.Write vbcrlf


Dim aEstoquesReserva

aEstoquesReserva = Array( Array("F", "Estoque F�sico"), Array("Disp", "Estoque Dispon�vel"), _
    Array("RC", "Reserva de Compra"), Array("RCC", "Reserva de Compra Confirmada"), _
    Array("RV", "Reserva de Venda"), Array("RVC", "Reserva de Venda Confirmada"))

For i=0 To 5
    
    Response.Write "<p id=" & Chr(34) & "lblChk1_ESTOQUE_" & aEstoquesReserva(i)(0) & Chr(34) & " " & _
                   "name=" & Chr(34) & "lblChk1_ESTOQUE_" & aEstoquesReserva(i)(0) & Chr(34) & " " & _
                   "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
                   aEstoquesReserva(i)(0) & "</p>"
    Response.Write vbcrlf
        
    Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
                   "id=" & Chr(34) & "chk1_ESTOQUE_" & aEstoquesReserva(i)(0) & Chr(34) & _
                   "name=" & Chr(34) & "chk1_ESTOQUE_" & aEstoquesReserva(i)(0) & Chr(34) & _
                   "title=" & Chr(34) & aEstoquesReserva(i)(1) & Chr(34) & " " & _
                   "LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & " onclick=" & Chr(34) & "return btn_Estoques1onclick(this) " & Chr(34) & _
                   "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"
        
    Response.Write vbcrlf
Next

Response.Write "<p id=" & Chr(34) & "lblDataEstoque" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblDataEstoque" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Data" & "</p>"

Response.Write vbcrlf    

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtDataEstoque" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtDataEstoque" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"        

'Relatorio estoque marco
Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblDeposito" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblDeposito" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Dep�sito" & "</p>"

Response.Write vbcrlf    

Response.Write "<select " & _
               "id=" & Chr(34) & "selDeposito" & Chr(34) & " " & _
               "name=" & Chr(34) & "selDeposito" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></select>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblProprietarios2" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblProprietarios2" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Propriet�rio" & "</p>"

Response.Write vbcrlf    

Response.Write "<select " & _
               "id=" & Chr(34) & "selProprietarios2" & Chr(34) & " " & _
               "name=" & Chr(34) & "selProprietarios2" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></select>"

Response.Write vbcrlf


Response.Write "<p id=" & Chr(34) & "lblFiltro" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblFiltro" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Filtro" & "</p>"

Response.Write vbcrlf    

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtFiltro" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtFiltro" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"        

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblExcel" & Chr(34) & " " & _
                "name=" & Chr(34) & "lblExcel" & Chr(34) & " " & _
                "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">Ex</p>"
Response.Write vbcrlf
    
Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
                "id=" & Chr(34) & "chkExcel" & Chr(34) & " " & _
                "name=" & Chr(34) & "chkExcel" & Chr(34) & " " & _
                "title=" & Chr(34) & "S� excel" & Chr(34) & " " & _
                "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"
    
Response.Write vbcrlf

Response.Write "<p id='lblFabricante' name='lblFabricante' class='lblGeneral'>Fabricante</p>"
Response.Write vbcrlf
Response.Write "<select id='selFabricante' name='selFabricante' class='fldGeneral'></select>"
Response.Write vbcrlf
Response.Write "<p id='lblPadrao' name='lblPadrao' class='lblGeneral'>Padr�o</p>"
Response.Write vbcrlf
Response.Write "<select id='selPadrao' name='selPadrao' class='fldGeneral'></select>"
Response.Write vbcrlf
%>

<p id="lblEmpresa" name="lblEmpresa" class="lblGeneral">Empresas</p>
<select id="selEmpresa" name="selEmpresa" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT d.PessoaID AS fldID, d.Fantasia AS fldName, g.ConceitoID AS MoedaID, g.SimboloMoeda AS SimboloMoeda " & _
	"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Moedas f WITH(NOLOCK), Conceitos g WITH(NOLOCK) " & _
	"WHERE (a.SujeitoID = " & CStr(nUserID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40123 AND " & _
		"c.RecursoMaeID = 21060 AND c.ContextoID = 2131 AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND b.EmpresaID = d.PessoaID AND " & _
		"b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 12 AND e.ObjetoID = 999 AND e.RelacaoID = f.RelacaoID AND " & _
		"f.Faturamento = 1 AND f.MoedaID = g.ConceitoID) " & _
		"GROUP BY d.PessoaID, d.Fantasia, g.ConceitoID, g.SimboloMoeda " & _
		"ORDER BY fldName"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
    
    If (CStr(nEmpresaID) = CStr(rsData.Fields("fldID").Value)) Then
		Response.Write " SELECTED "
    End If

    Response.Write "MoedaID = " & Chr(34) & rsData.Fields("MoedaID").Value & Chr(34) & " " 
    Response.Write "SimboloMoeda = " & Chr(34) & rsData.Fields("SimboloMoeda").Value & Chr(34) & " " 
    
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
</select>


<%
Response.Write "<p id='lblFamilias' name='lblFamilias' class='lblGeneral'>Fam�lias</p>"
Response.Write vbcrlf
Response.Write "<select id='selFamilias' name='selFamilias' class='fldGeneral' MULTIPLE></select>"
Response.Write vbcrlf

%>    
<p id="lblMarca" name="lblMarca" class="lblGeneral">Marcas</p>
<select id="selMarca" name="selMarca" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

'strSQL = "SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
'	"FROM RelacoesPesRec a, RelacoesPesRec_Perfis b, Recursos_Direitos c, Financeiro d, " & _
'	"RelacoesPesCon e, Conceitos f, Conceitos g " & _
'	"WHERE (a.SujeitoID = " & CStr(nUserID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
'		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40008 AND " & _
'		"c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND " & _
'		"b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 61 AND " & _
'		"e.ObjetoID = f.ConceitoID AND f.MarcaID = g.ConceitoID) " & _
'		"GROUP BY g.ConceitoID, g.Conceito " & _
'		"ORDER BY fldName"

'strSQL = "SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
'	"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK), " & _
'	"RelacoesPesCon e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Conceitos g WITH(NOLOCK) " & _
'	"WHERE (a.SujeitoID = " & CStr(nUserID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
'		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40008 AND " & _
'		"c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND " & _
'		"b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 61 AND " & _
'		"e.ObjetoID = f.ConceitoID AND f.MarcaID = g.ConceitoID) " & _
'		"GROUP BY g.ConceitoID, g.Conceito " & _
'		"ORDER BY fldName"

strSQL = "SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
	"FROM RelacoesPesRec a WITH(NOLOCK) " & _
		"INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID " & _
		"INNER JOIN Recursos_Direitos c WITH(NOLOCK) ON b.PerfilID = c.PerfilID " & _
		"INNER JOIN RelacoesPesCon e WITH(NOLOCK) ON b.EmpresaID = e.SujeitoID " & _
		"INNER JOIN  Conceitos f WITH(NOLOCK) ON e.ObjetoID = f.ConceitoID " & _
		"INNER JOIN Conceitos g WITH(NOLOCK) ON f.MarcaID = g.ConceitoID " & _
	"WHERE a.SujeitoID = " & CStr(nUserID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 " & _
		"AND c.RecursoID = 40002 AND c.RecursoMaeID = 21060 AND c.ContextoID IN (2133) " & _
		"AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND e.TipoRelacaoID = 61 " & _
	"GROUP BY g.ConceitoID, g.Conceito " & _
	"ORDER BY fldName"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
</select>

<%    
Response.Write "</div>"
Response.Write vbcrlf

'Cria o Div para o Etiquetas de Produtos
Response.Write vbcrlf

Response.Write "<div id=" & Chr(34) & "divEtiquetasProdutos" & Chr(34) & " " & _
               "name="  & Chr(34) & "divEtiquetasProdutos" & Chr(34) & " " & _
               "class="  & Chr(34) & "divGeneral"  & Chr(34) & ">"

If ( NOT( rsData1.BOF AND rsData1.EOF) ) Then
    rsData1.MoveFirst
End If
    
'Cria os checkbox de estado
i = 1

While Not rsData1.EOF
    Response.Write "<p id=" & Chr(34) & "lblChk3_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "name=" & Chr(34) & "lblChk3_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
                    rsData1.Fields("Estado").Value & "</p>"
    Response.Write vbcrlf
        
    Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
                   "id=" & Chr(34) & "chk3_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "name=" & Chr(34) & "chk3_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "title=" & Chr(34) & rsData1.Fields("RecursoFantasia").Value & Chr(34) & " " & _
                   "value=" & Chr(34) & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"
        
    Response.Write vbcrlf
    rsData1.MoveNext
Wend

Response.Write vbcrlf    

Response.Write "<p id=" & Chr(34) & "lblEtiquetaVertical" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblEtiquetaVertical" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "EV" & "</p>"
               
Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtEtiquetaVertical" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtEtiquetaVertical" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & " " & _
               "title="  & Chr(34) & "Etiquetas Verticais" & Chr(34) & "></input>"

Response.Write vbcrlf    

Response.Write "<p id=" & Chr(34) & "lblEtiquetaHorizontal" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblEtiquetaHorizontal" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "EH" & "</p>"
               
Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtEtiquetaHorizontal" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtEtiquetaHorizontal" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & " " & _
               "title="  & Chr(34) & "Etiquetas Horizontais" & Chr(34) & "></input>"

Response.Write vbcrlf    

Response.Write "<p id=" & Chr(34) & "lblAltura" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblAltura" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Alt" & "</p>"
               
Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtAltura" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtAltura" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & " " & _
               "title="  & Chr(34) & "Altura (linhas)" & Chr(34) & "></input>"
               
Response.Write vbcrlf    

Response.Write "<p id=" & Chr(34) & "lblLargura" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblLargura" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Larg" & "</p>"
               
Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtLargura" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtLargura" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & " " & _
               "title="  & Chr(34) & "Largura (mm)" & Chr(34) & "></input>"

Response.Write vbcrlf    

Response.Write "<p id=" & Chr(34) & "lblMargemSuperior" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblMargemSuperior" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "MS" & "</p>"
               
Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtMargemSuperior" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtMargemSuperior" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & " " & _
               "title="  & Chr(34) & "Margem Superior (linhas)" & Chr(34) & "></input>"

Response.Write vbcrlf    

Response.Write "<p id=" & Chr(34) & "lblMargemEsquerda" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblMargemEsquerda" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "ME" & "</p>"

Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtMargemEsquerda" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtMargemEsquerda" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & " " & _
               "title="  & Chr(34) & "Margem Esquerda (mm)" & Chr(34) & "></input>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblFiltro3" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblFiltro3" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Filtro" & "</p>"

Response.Write vbcrlf

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtFiltro3" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtFiltro3" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"        

Response.Write vbcrlf


Response.Write "</div>"
Response.Write vbcrlf

%>    

<%    
'Cria o Div para o relat�rio Posicao de Produtos
Response.Write vbcrlf
Response.Write "<div id=" & Chr(34) & "divPosicaoProdutos" & Chr(34) & " " & _
               "name="  & Chr(34) & "divPosicaoProdutos" & Chr(34) & " " & _
               "class="  & Chr(34) & "divGeneral"  & Chr(34) & ">"

If ( NOT( rsData1.BOF AND rsData1.EOF) ) Then
    rsData1.MoveFirst
End If
    
'Cria os checkbox de estado
i = 1
While Not rsData1.EOF
    Response.Write vbcrlf   
    Response.Write "<p id=" & Chr(34) & "lblChk4_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "name=" & Chr(34) & "lblChk4_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
                    rsData1.Fields("Estado").Value & "</p>"
    Response.Write vbcrlf
        
    Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
                   "id=" & Chr(34) & "chk4_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "name=" & Chr(34) & "chk4_" & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "title=" & Chr(34) & rsData1.Fields("RecursoFantasia").Value & Chr(34) & " " & _
                   "value=" & Chr(34) & rsData1.Fields("EstadoID").Value & Chr(34) & " " & _
                   "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"
        
    Response.Write vbcrlf
    rsData1.MoveNext
Wend
        
rsData1.Close
Set rsData1 = Nothing

Response.Write vbcrlf

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblEstoqueFisico" & Chr(34) & " " & _
                "name=" & Chr(34) & "lblEstoqueFisico" & Chr(34) & " " & _
                "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
                "F" & "</p>"

Response.Write vbcrlf    

Response.Write "<input type=" & Chr(34) & "checkbox" & Chr(34) & " " & _
                "id=" & Chr(34) & "chkEstoqueFisico" & Chr(34) & " " & _
                "name=" & Chr(34) & "chkEstoqueFisico" & Chr(34) & " " & _
                "title=" & Chr(34) & "Estoque F�sico" & Chr(34) & " " & _
                "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblProprietarios" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblProprietarios" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Propriet�rio" & "</p>"

Response.Write vbcrlf    

Response.Write "<select " & _
               "id=" & Chr(34) & "selProprietarios" & Chr(34) & " " & _
               "name=" & Chr(34) & "selProprietarios" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></select>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblOrdem" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblOrdem" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Ordem" & "</p>"

Response.Write vbcrlf    

Response.Write "<select " & _
               "id=" & Chr(34) & "selOrdem" & Chr(34) & " " & _
               "name=" & Chr(34) & "selOrdem" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></select>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblMoeda2" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblMoeda2" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Moeda" & "</p>"

Response.Write vbcrlf
    
Response.Write "<select id="& Chr(34) & "selMoedas2" & Chr(34) & " " & _
               "name=" & Chr(34) & "selMoedas2" & Chr(34) & " " & _
               "class=" & Chr(34) & "fldGeneral" & Chr(34) & "></select>"

Response.Write vbcrlf

Response.Write "<p id=" & Chr(34) & "lblFiltro4" & Chr(34) & " " & _
               "name=" & Chr(34) & "lblFiltro4" & Chr(34) & " " & _
               "class=" & Chr(34) & "lblGeneral"  & Chr(34) & ">" & _
               "Filtro" & "</p>"

Response.Write vbcrlf    

Response.Write "<input type=" & Chr(34) & "text" & Chr(34) & " " & _
               "id=" & Chr(34) & "txtFiltro4" & Chr(34) & " " & _
               "name=" & Chr(34) & "txtFiltro4" & Chr(34) & " " & _
               "class="  & Chr(34) & "fldGeneral"  & Chr(34) & "></input>"        
%>

<p id="lblMarca2" name="lblMarca2" class="lblGeneral">Marcas</p>
<select id="selMarca2" name="selMarca2" class="fldGeneral" MULTIPLE>
<%

Set rsData = Server.CreateObject("ADODB.Recordset")

'strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
'    "UNION ALL " & _
'    "SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
'	"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos_Direitos c WITH(NOLOCK), " & _
'	"RelacoesPesCon e WITH(NOLOCK), Conceitos f WITH(NOLOCK), Conceitos g WITH(NOLOCK) " & _
'	"WHERE (a.SujeitoID = " & CStr(nUserID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND " & _
'		"a.RelacaoID = b.RelacaoID AND b.PerfilID = c.PerfilID AND c.RecursoID = 40008 AND " & _
'		"c.RecursoMaeID = 28000 AND c.ContextoID IN (9111,9112) AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND " & _
'		"b.EmpresaID = e.SujeitoID AND e.TipoRelacaoID = 61 AND " & _
'		"e.ObjetoID = f.ConceitoID AND f.MarcaID = g.ConceitoID) " & _
'		"GROUP BY g.ConceitoID, g.Conceito " & _
'		"ORDER BY fldName"

strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName UNION ALL " & _
"SELECT g.ConceitoID AS fldID, g.Conceito AS fldName " & _
	"FROM RelacoesPesRec a WITH(NOLOCK) " & _
		"INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID " & _
		"INNER JOIN Recursos_Direitos c WITH(NOLOCK) ON b.PerfilID = c.PerfilID " & _
		"INNER JOIN RelacoesPesCon e WITH(NOLOCK) ON b.EmpresaID = e.SujeitoID " & _
		"INNER JOIN  Conceitos f WITH(NOLOCK) ON e.ObjetoID = f.ConceitoID " & _
		"INNER JOIN Conceitos g WITH(NOLOCK) ON f.MarcaID = g.ConceitoID " & _
	"WHERE a.SujeitoID = " & CStr(nUserID) & " AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 " & _
		"AND c.RecursoID = 40002 AND c.RecursoMaeID = 21060 AND c.ContextoID IN (2133) " & _
		"AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND e.TipoRelacaoID = 61 " & _
	"GROUP BY g.ConceitoID, g.Conceito " & _
	"ORDER BY fldName"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
</select>

<p id="lblFormatoSolicitacao" name="lblFormatoSolicitacao" class="lblGeneral">Formato</p>
<select id="selFormatoSolicitacao" name="selFormatoSolicitacao" class="fldGeneral">
    <option value="2">Excel</option>		
    <option value="1">PDF</option>	
</select>
<%    
Response.Write "</div>"
Response.Write vbcrlf
%>
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal">
        </p>
    </div>

    <iframe id="frmReport" name="frmReport" style="display:none"  frameborder="no"></iframe>
 
</body>

</html>
