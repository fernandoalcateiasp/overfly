<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    Dim EmpresaID
    Dim nProdutoID
    Dim i
        
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
        
    Set objSvrCfg = Nothing
%>

<%'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

EmpresaID = 0

For i = 1 To Request.QueryString("EmpresaID").Count    
    EmpresaID = Request.QueryString("EmpresaID")(i)
Next

nProdutoID = 0

For i = 1 To Request.QueryString("nProdutoID").Count    
    nProdutoID = Request.QueryString("nProdutoID")(i)
Next

Dim rsData, strSQL
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT TOP 1 b.MoedaID, ISNULL(b.CustoReposicao, 0) AS CustoReposicao, " & _
		"dbo.fn_Preco_FatorImpostosEntrada(a.RelacaoID, 0) AS FatorImpostosEntrada, " & _
		"dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 341, NULL, GETDATE(), NULL,375, NULL) AS Estoque " & _
	"FROM RelacoesPesCon a WITH(NOLOCK) LEFT OUTER JOIN RelacoesPesCon_Fornecedores b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " & _
	"WHERE (a.ObjetoID = " & CStr(nProdutoID) & " AND a.SujeitoID = " & CStr(EmpresaID) & " AND a.TipoRelacaoID = 61) " & _
	"ORDER BY b.Ordem "

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
		
Response.Write( "var glb_nMoedaFornecedorID = 0; " )
Response.Write vbcrlf
Response.Write( "var glb_nCustoReposicao = 0; " )
Response.Write vbcrlf
Response.Write( "var glb_nFatorImpostosEntrada = 0; " )
Response.Write vbcrlf
Response.Write( "var glb_nEstoque = 0; " )
Response.Write vbcrlf
Response.Write( "var glb_nProdutoID = " & CStr(nProdutoID) & ";" )
Response.Write vbcrlf

Do While Not rsData.EOF
	If (NOT IsNull(rsData.Fields("MoedaID").Value)) Then
		Response.Write( "glb_nMoedaFornecedorID = " & rsData.Fields("MoedaID").Value & "; " )
		Response.Write vbcrlf
	End If
	
	If (NOT IsNull(rsData.Fields("CustoReposicao").Value)) Then
		Response.Write( "glb_nCustoReposicao = " & rsData.Fields("CustoReposicao").Value & "; " )
		Response.Write vbcrlf
	End If

	Response.Write( "glb_nFatorImpostosEntrada = " & rsData.Fields("FatorImpostosEntrada").Value & "; " )
	Response.Write vbcrlf
	Response.Write( "glb_nEstoque = " & rsData.Fields("Estoque").Value & "; " )
	Response.Write vbcrlf
	rsData.MoveNext
Loop

rsData.Close
Set rsData = Nothing

Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<html id="modalSimulacaoPrecoHtml" name="modalSimulacaoPrecoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
      
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
              
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalsimulacaopreco.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<body id="modalSimulacaoPrecoBody" name="modalSimulacaoPrecoBody" LANGUAGE="javascript" onload="return window_onload()">
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <div id="divExtPesquisa" name="divExtPesquisa" class="divExternPesq">
        <p id="lblMoedaEntradaID" name="lblMoedaEntradaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaEntradaID" name="selMoedaEntradaID" class="fldGeneral"></select>
        <p id="lblCustoReposicao" name="lblCustoReposicao" class="lblGeneral">Custo Reposicao</p>
        <input type="text" id="txtCustoReposicao" name="txtCustoReposicao" class="fldGeneral" title="Custo de reposi��o"></input>
        <p id="lblCustoBase" name="lblCustoBase" class="lblGeneral">Custo Base</p>
        <input type="text" id="txtCustoBase" name="txtCustoBase" class="fldGeneral" title="Custo base"></input>
        <p id="lblMargemContribuicao" name="lblMargemContribuicao" class="lblGeneral">MC</p>
        <input type="text" id="txtMargemContribuicao" name="txtMargemContribuicao" class="fldGeneral" title="Margem de contribui��o"></input>
        <p id="lblMargemAjustada" name="lblMargemAjustada" class="lblGeneral">Ajust</p>
        <input type="checkbox" id="chkMargemAjustada" name="lblMargemAjustada" class="fldGeneral" title="Utiliza margem ajustada?" checked>
        <p id="lblUF" name="lblUF" class="lblGeneral">UF</p>
        <select id="selUF" name="selUF" class="fldGeneral">
		<%
			Set rsData = Server.CreateObject("ADODB.Recordset")
		    
			strSQL = "SELECT d.LocalidadeID, d.CodigoLocalidade2, " & _
					"dbo.fn_Produto_Aliquota(a.PessoaID, " & CStr(nProdutoID) & ", -c.LocalidadeDestinoID, NULL, NULL, NULL, b.ConceitoID, NULL) AS Aliquota, " & _
					"(CASE c.LocalidadeOrigemID WHEN c.LocalidadeDestinoID THEN 1 ELSE 0 END) AS EhDefault " & _
				"FROM Pessoas_Enderecos a WITH(NOLOCK) " & _
				"INNER JOIN Conceitos b WITH(NOLOCK) ON (a.PaisID = b.PaisID) " & _
				"INNER JOIN Conceitos_Aliquotas c WITH(NOLOCK) ON (b.ConceitoID = c.ImpostoID AND a.UFID = c.LocalidadeOrigemID) " & _
				"INNER JOIN Localidades d WITH(NOLOCK) ON (c.LocalidadeDestinoID = d.LocalidadeID) " & _
				"WHERE (a.PessoaID = " & CStr(EmpresaID) & " AND a.Ordem = 1 AND b.TipoConceitoID = 306 AND " & _
				"b.RegraAliquotaID = 333) " & _
				"ORDER BY CodigoLocalidade2"
		    
			rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
		            
			Do While Not rsData.EOF
				Response.Write( "<option value='" & rsData.Fields("LocalidadeID").Value & "' " )
				Response.Write( "aliquota='" & rsData.Fields("Aliquota").Value & "'")
				
				If (rsData.Fields("EhDefault").Value = 1) Then
					Response.Write(" SELECTED " )
				End If
				
    			Response.Write(">" & rsData.Fields("CodigoLocalidade2") & "</option>" )
				rsData.MoveNext
			Loop

			rsData.Close
			Set rsData = Nothing
		%>
        </select>
        <p id="lblImposto" name="lblImposto" class="lblGeneral">Imp</p>
        <input type="text" id="txtImposto" name="txtImposto" class="fldGeneral" title="Imposto"></input>
        <p id="lblImpostoIncidencia" name="lblImpostoIncidencia" class="lblGeneral">Inc</p>
        <input type="checkbox" id="chkImpostoIncidencia" name="chkImpostoIncidencia" class="fldGeneral" title="Imposto incid�ncia">
        <p id="lblMoedaSaidaID" name="lblMoedaSaidaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaSaidaID" name="selMoedaSaidaID" class="fldGeneral"></select>
        <p id="lblPreco" name="lblPreco" class="lblGeneral">Pre�o</p>
        <input type="text" id="txtPreco" name="txtPreco" class="fldGeneral" title="Pre�o"></input>
        <p id="lblMargemContribuicao2" name="lblMargemContribuicao2" class="lblGeneral">MC</p>
        <input type="checkbox" id="chkMargemContribuicao2" name="chkMargemContribuicao2" class="fldGeneral" title="Margem de contribui��o">
        <p id="lblMargemPadrao" name="lblMargemPadrao" class="lblGeneral">MP</p>
        <input type="checkbox" id="chkMargemPadrao" name="chkMargemPadrao" class="fldGeneral" title="Margem padrao">
        <p id="lblMargemPublicacao" name="lblMargemPublicacao" class="lblGeneral">MPub</p>
        <input type="checkbox" id="chkMargemPublicacao" name="chkMargemPublicacao" class="fldGeneral" title="Margem publicacao">
        <p id="lblMargemMinima" name="lblMargemMinima" class="lblGeneral">MM�n</p>
        <input type="checkbox" id="chkMargemMinima" name="chkMargemMinima" class="fldGeneral" title="Margem m�nima">
	</div>		
</body>

</html>
