<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalimpostosHtml" name="modalimpostosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalimpostos.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalimpostos.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "var glb_USERID = 0;"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nRelacaoID, nPos, nDireito

nRelacaoID = 0
nPos = 0
nDireito = 0

For i = 1 To Request.QueryString("nRelacaoID").Count    
    nRelacaoID = Request.QueryString("nRelacaoID")(i)
Next

For i = 1 To Request.QueryString("nPos").Count    
    nPos = Request.QueryString("nPos")(i)
Next

For i = 1 To Request.QueryString("nDireito").Count    
    nDireito = Request.QueryString("nDireito")(i)
Next

Response.Write "var glb_nRelacaoID = " & CStr(nRelacaoID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPos = " & CStr(nPos) & ";"
Response.Write vbcrlf

'Ajusta pelo direitos
'Se o usuario tem direito de incluir na pasta as colunas Aliquotas e Base de Calculo
'sao editaveis
If ( CLng(nDireito) = 1 ) Then

    Response.Write "var glb_sEditable = " & Chr(39) & Chr(39) & ";"

'Se o usuario n�o tem direito de incluir na pasta as colunas Aliquotas e Base de Calculo
'sao read only
Else

    Response.Write "var glb_sEditable = " & Chr(39) & "*" & Chr(39) & ";"

End If

Response.Write vbcrlf

Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    getDataInServer();

    // configuracao inicial do html
    setupPage();   
        
    // mostrar a janela movido para o final de preenchimento do grid
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOKClicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );

}

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=CellChanged>
<!--
 fg_md_CellChanged();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 fg_AfterEdit_modalImpostos(arguments[0], arguments[1]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalimpostosDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
 js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

</head>

<body id="modalimpostosBody" name="modalimpostosBody" LANGUAGE="javascript" onload="return window_onload()">
    
     <div id="divFields" name="divFields" class="divGeneral">
	    <p id="lblProdutoSimilar" name="lblProdutoSimilar" class="lblGeneral">Produto Similar</p>
	    <select id="selProdutoSimilar" name="selProdutoSimilar" class="fldGeneral">
<%
	Dim rsData, strSQL
	Set rsData = Server.CreateObject("ADODB.Recordset")

	' Vindo do sup
	If (CLng(nPos) = 0) Then
		strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
			"UNION ALL SELECT d.RelacaoID AS fldID, c.Conceito AS fldName " & _
			"FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), RelacoesPesCon d WITH(NOLOCK) " & _
			"WHERE (a.RelacaoID = " & CStr(nRelacaoID) & " AND a.ObjetoID = b.ConceitoID AND " & _
				"b.MarcaID = c.MarcaID AND b.ProdutoID = c.ProdutoID AND " & _
				"b.ConceitoID <> c.ConceitoID AND c.ConceitoID = d.ObjetoID AND " & _
				"a.SujeitoID = d.SujeitoID AND d.TipoRelacaoID = 61 AND " & _
				"d.EstadoID NOT IN (1,4,5) AND " & _
				"a.GrupoImpostoEntradaID = d.GrupoImpostoEntradaID AND " & _
				"a.GrupoImpostoSaidaID = d.GrupoImpostoSaidaID) " & _
			"ORDER BY fldName"
	' Vindo do inf
	ElseIf (CLng(nPos) = 1) Then
		strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
			"UNION ALL SELECT f.RelPesConFornecID AS fldID, d.Conceito AS fldName " & _
		"FROM RelacoesPesCon_Fornecedores a WITH(NOLOCK), RelacoesPesCon b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Conceitos d WITH(NOLOCK), RelacoesPesCon e WITH(NOLOCK), " & _
			"RelacoesPesCon_Fornecedores f WITH(NOLOCK) " & _
		"WHERE (a.RelPesconFornecID = " & CStr(nRelacaoID) & " AND a.RelacaoID = b.RelacaoID AND  " & _
			"b.ObjetoID = c.ConceitoID AND c.MarcaID = d.MarcaID AND c.ProdutoID = d.ProdutoID AND " & _
			"c.ConceitoID <> d.ConceitoID AND d.ConceitoID = e.ObjetoID AND " & _
			"b.SujeitoID = e.SujeitoID AND e.TipoRelacaoID = 61 AND " & _
			"e.EstadoID NOT IN (1,4,5) AND e.RelacaoID = f.RelacaoID AND " & _
			"a.FornecedorID = f.FornecedorID AND " & _
			"a.GrupoImpostoID = f.GrupoImpostoID) " & _
		"ORDER BY fldName"
	End If
	
	rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

	While Not rsData.EOF
		Response.Write "<option value = " & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
		Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
		rsData.MoveNext
	Wend

	rsData.Close
	Set rsData = Nothing
%>
	    </select>
     </div>
     
     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal">
        </p>
    </div>    

</body>

</html>
