<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

	strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalalteracaoprecoHtml" name="modalalteracaoprecoHtml">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalalteracaopreco.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
            
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
            
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalalteracaopreco.js" & Chr(34) & "></script>" & vbCrLf
    %>

    <%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, bDireitoAlterar, bDireitoExcluir, sEmpresaUFID, nLocalidadeID

sCaller = ""
bDireitoAlterar = 0
bDireitoExcluir = 0
sEmpresaUFID = ""
nLocalidadeID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("bDireitoAlterar").Count    
    bDireitoAlterar = Request.QueryString("bDireitoAlterar")(i)
Next

For i = 1 To Request.QueryString("bDireitoExcluir").Count    
    bDireitoExcluir = Request.QueryString("bDireitoExcluir")(i)
Next

For i = 1 To Request.QueryString("sEmpresaUFID").Count    
    sEmpresaUFID = Request.QueryString("sEmpresaUFID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
    %>

    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>

    <!-- //@@ Eventos de grid -->
    <!-- fg -->
    <script language="javascript" for="fg" event="ValidateEdit">
<!--
    js_modalalteracaopreco_ValidateEdit();
    //-->
    </script>

    <script language="javascript" for="fg" event="AfterEdit">
<!--
    js_modalalteracaopreco_AfterEdit(arguments[0], arguments[1]);
    //-->
    </script>

    <script language="javascript" for="fg" event="KeyPress">
<!--
    js_modalalteracaoprecoKeyPress(arguments[0]);
    //-->
    </script>

    <script language="javascript" for="fg" event="DblClick">
<!--
    js_fg_modalalteracaoprecoDblClick(fg, fg.Row, fg.Col);
    //-->
    </script>

    <script language="javascript" for="fg" event="BeforeMouseDown">
<!--
    js_fg_modalalteracaoprecoBeforeMouseDown(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    //-->
    </script>

    <script language="javascript" for="fg" event="EnterCell">
<!--
    js_fg_EnterCell(fg);
    //-->
    </script>
    <script language="javascript" for="fg" event="AfterRowColChange">
<!--
    js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    //treatCheckBoxReadOnly2 dsoGrid, fg, OldRow, OldCol, NewRow, NewCol
    //-->
    </script>

    <script language="javascript" for="fg" event="BeforeRowColChange">
<!--
    js_fg_modalalteracaopreco_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
    //-->
    </script>

</head>

<body id="modalalteracaoprecoBody" name="modalalteracaoprecoBody" language="javascript" onload="return window_onload()">
    <!-- Objeto de Impressao em InkJet -->
        
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" viewastext>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>

    <input type="button" id="btnOK" name="btnOK" value="OK" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" language="javascript" onclick="return btn_onclick(this)" class="btns">

    <input type="button" id="btnCalcular" name="btnCalcular" value="Calcular" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnRefresh" name="btnRefresh" value="Refresh" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnExcel" name="btnExcel" value="Excel" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnImprimir" name="btnImprimir" value="Imprimir" language="javascript" onclick="return btn_onclick(this)" class="btns">

    <p id="lblIncideImposto" name="lblIncideImposto" class="lblGeneral">Inc</p>
    <input type="checkbox" id="chkIncideImposto" name="chkIncideImposto" class="fldGeneral" title="Incide imposto (ICMS/Tax) sobre a nota?">

    <select id="selUF" name="selUF" class="fldGeneral">
        <%
		Dim strSQL, rsData
		
		Set rsData = Server.CreateObject("ADODB.Recordset")
		
		' Consulta o UF da empresa logada.
		strSQL = "SELECT Localidades.LocalidadeID " & _
			"FROM " & _
				"Pessoas " & _
				"INNER JOIN Pessoas_Enderecos WITH(NOLOCK) ON (Pessoas.PessoaID = Pessoas_Enderecos.PessoaID) " & _
				"INNER JOIN Localidades WITH(NOLOCK) ON (Pessoas_Enderecos.UFID = Localidades.LocalidadeID) " & _
			"WHERE " & _
				"Pessoas.PessoaID = 1661 AND " & _
				"Localidades.TipoLocalidadeID = 204 AND " & _
				"Localidades.LocalizacaoID = 130 AND " & _
				"Localidades.EstadoID = 2"
		
		rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

		nLocalidadeID = rsData.Fields("LocalidadeID").Value

		rsData.Close
		
		' Consulta os dados do combo.
		strSQL = "SELECT LocalidadeID, CodigoLocalidade2 " & _
			"FROM Localidades WITH(NOLOCK) " & _
			"WHERE (TipoLocalidadeID = 204) AND (LocalizacaoID = 130) AND (EstadoID = 2) "

		rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

		If (Not (rsData.BOF AND rsData.EOF) ) Then
			While Not (rsData.EOF)
				Response.Write( "<option value='" & rsData.Fields("LocalidadeID").Value) & "'"
				
				If (nLocalidadeID = rsData.Fields("LocalidadeID").Value) Then
					Response.Write(" selected>")
				Else
					Response.Write(">")
				End If
				
				Response.Write( rsData.Fields("CodigoLocalidade2").Value & "</option>" )

				rsData.MoveNext()
			WEnd
		End If

		rsData.Close
        %>
    </select>

</body>

</html>
