<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

	strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalkardexHtml" name="modalkardexHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalkardex.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
        
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalkardex.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nRegistroID, rsData,strSQL

sCaller = ""
nRegistroID = 0


For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nRegistroID").Count    
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nRegistroID = " & Chr(39) & CStr(nRegistroID) & Chr(39) & ";"
Response.Write vbcrlf


Set rsData = Server.CreateObject("ADODB.Recordset")
    strSQL =" SELECT ProdutoBloqueado " &_
            " FROM   RelacoesPesCon WITH ( NOLOCK )" &_
            " WHERE  RelacaoID = " + CStr(nRegistroID)

				
    rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

    Response.Write "var glb_bProdutoBloqueado = " & Chr(34) & rsData.Fields("ProdutoBloqueado").Value & Chr(34)
rsData.Close
Set rsData = Nothing

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalkardex_ValidateEdit();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalkardex_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
 js_fg_modalKardexMouseMove();
 js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalkardexKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalkardexDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalkardexBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_modalkardex_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

</head>

<body id="modalkardexBody" name="modalkardexBody" LANGUAGE="javascript" onload="return window_onload()">    
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <p id="lblTipo" name="lblTipo" class="lblGeneral">Tipo</p>
    <select id="selTipo" name="selTipo" class="fldGeneral"  onchange="return cmbOnChange(this)">
     <%
		 Set rsData = Server.CreateObject("ADODB.Recordset")
		    strSQL =" SELECT ItemID AS fldID, ItemMasculino AS fldName " &_
                    " FROM   TiposAuxiliares_Itens WITH ( NOLOCK )" &_
                    " WHERE  ( TipoID = 117 ) AND( EstadoID = 2 ) ORDER BY ITEMID DESC"

							
			rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			While Not rsData.EOF
				Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				If ( CLng(rsData.Fields("fldID").Value) = CLng(1) ) Then
					Response.Write " SELECTED "
				End If

				Response.Write "  TipoID=" & rsData.Fields("fldID").Value & " " & vbCrlf
				Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				rsData.MoveNext
			Wend
		
			rsData.Close
			Set rsData = Nothing
		%>		
    
    </select> 
    
    <p id="lblEstoque" name="lblEstoque" class="lblGeneral">Estoque</p>
    <select id="selEstoque" name="selEstoque" class="fldGeneral"  onchange="return cmbOnChange(this)">
        <option value ="1" SELECTED>Proprio</option>
        <option value ="0">Terceiro</option>
    </select>
       
    <p id="lblLote" name="lblLote" class="lblGeneral">Lote</p>  
    <input type="checkbox" id="chkLote" name="chkLote" class="fldGeneral" title="Somente opera��es com lote ?"></input> 
    
    <p id="lblInicio" name="lblInicio" class="lblGeneral">Inicio</p> 
    <input type="text" id="txtInicio" name="txtInicio" class="fldGeneral" title="Inicio"></input> 
    
    <p id="lblFim" name="lblFim" class="lblGeneral">Fim</p> 
    <input type="text" id="txtFim" name="txtFim" class="fldGeneral" title="Fim"></input> 
    
    <p id="lblConsolidar" name="lblConsolidar" class="lblGeneral">Consolidar</p>  
    <input type="checkbox" id="chkConsolidar" name="chkConsolidar" class="fldGeneral" title="Consolidar opera��es que n�o afetam o custo"></input> 
    
    <input type="button" id="btnListar" name="btnListar" value="Listar" title = "Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <input type="button" id="btnExcel" name="btnExcel" value="Excel" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <input type="button" id="btnDesbloqProduto" name="btnDesbloqProduto" value="Desbloquear Produto" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

    <p id="lblReferenciaCusto" name="lblReferenciaCusto" class="lblGeneral">Referencia de Custo</p> 
    <input type="text" id="txtReferenciaCusto" name="txtReferenciaCusto" class="fldGeneral" title="Referencia Custo"></input> 

    <p id="lblPedidoReferencia" name="lblPedidoReferencia" class="lblGeneral">Pedido Referencia</p> 
    <input type="text" id="txtPedidoReferencia" name="txtPedidoReferencia" class="fldGeneral" title="Pedido Referencia"></input> 

    <p id="lblMoeda" name="lblMoeda" class="lblGeneral">$</p> 
    <input type="text" id="txtMoeda" name="txtMoeda" class="fldGeneral" title="Moeda"></input> 
        
    <p id="lblValorUnitario" name="lblValorUnitario" class="lblGeneral">Valor Unitario</p> 
    <input type="text" id="txtValorUnitario" name="txtValorUnitario" class="fldGeneral" title="Valor Unitario"></input> 
    
    <p id="lblDespesaUnitaria" name="lblDespesaUnitaria" class="lblGeneral">Despesa Unitaria</p> 
    <input type="text" id="txtDespesaUnitaria" name="txtDespesaUnitaria" class="fldGeneral" title="Despesa Unitaria"></input> 
    
    <p id="lblTaxa" name="lblTaxa" class="lblGeneral">Taxa</p> 
    <input type="text" id="txtTaxa" name="txtTaxa" class="fldGeneral" title="Taxa"></input> 
    
    <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p> 
    <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral" title="Observa��o"></input> 

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>
</body>

</html>
