<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modallojaswebHtml" name="modallojaswebHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modallojasweb.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modallojasweb.js" & Chr(34) & "></script>" & vbCrLf
%>


<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller
Dim sCmbText, nCmbValue, nCmbOptionSel, nA1, nA2

sCaller = ""
sCmbText = ""
nCmbValue = 0
nCmbOptionSel = 0
nA1 = 0
nA2 = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nA1").Count    
    nA1 = Request.QueryString("nA1")(i)
Next

Response.Write "var glb_nA1 = " & CStr(nA1) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nA2").Count    
    nA2 = Request.QueryString("nA2")(i)
Next

Response.Write "var glb_nA2 = " & CStr(nA2) & ";"
Response.Write vbcrlf

ReDim sCmbText(CInt(Request.QueryString("sCmbText").Count) - 1)

For i = 1 To CInt(Request.QueryString("sCmbText").Count)
    sCmbText(i - 1) = Request.QueryString("sCmbText")(i)
Next
    
ReDim nCmbValue(CInt(Request.QueryString("nCmbValue").Count) - 1)

For i = 1 To CInt(Request.QueryString("nCmbValue").Count)
    nCmbValue(i - 1) = Request.QueryString("nCmbValue")(i)
Next    
    
ReDim nCmbOptionSel(CInt(Request.QueryString("nCmbOptionSel").Count) - 1)

For i = 1 To CInt(Request.QueryString("nCmbOptionSel").Count)
    nCmbOptionSel(i - 1) = CInt(Request.QueryString("nCmbOptionSel")(i))
Next


Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modallojasweb_ValidateEdit();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modallojasweb_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modallojaswebKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
    js_fg_modallojaswebDblClick();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modallojaswebBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 fg_AfterRowColChange();
 //-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
    js_fg_modalLojasWebMouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR="fg" EVENT="AfterSort">
<!--
 fg_AfterSort();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR="fg" EVENT="BeforeSort">
<!--
 fg_BeforeSort();
//-->
</SCRIPT>

</head>

<body id="modallojaswebBody" name="modallojaswebBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <div id="divPesquisa" name="divPesquisa" class="divGeneral">
		<p id="lblRegistros" name="lblRegistros" class="lblGeneral">Registros</p>
	    <select id="selRegistros" name="selRegistros" class="fldGeneral">
	    <%
			For i = 0 To CInt(Request.QueryString("sCmbText").Count) - 1
				Response.Write("<option value=")
				Response.Write(nCmbValue(i))

				If (nCmbOptionSel(i) = 1) Then
					Response.Write(" selected")
				End If
				
				Response.Write(">")
				Response.Write(sCmbText(i)) & "</option>"
				Response.Write vbcrlf
			Next		

	    %>
	    </select>	    
	    <p id="lblClassificacaoClienteID" name="lblClassificacaoClienteID" class="lblGeneral">Classifica��o cliente</p>
	    <select id="selClassificacaoClienteID" name="selClassificacaoClienteID" class="fldGeneral"></select>
	    <p id="lblLojaID" name="lblLojaID" class="lblGeneral">Loja</p>
	    <select id="selLojaID" name="selLojaID" class="fldGeneral"></select>
		<!-- Radio buttons -->
		<p id="lblrdE" name="lblrdE" class="lblGeneral">E</p>
        <input type="radio" id="rdE" name="rdE" class="fldGeneral" title="Loja e Filtro..."></input>
        <p id="lblrdOu" name="lblrdOu" class="lblGeneral">Ou</p>
        <input type="radio" id="rdOu" name="rdOu" class="fldGeneral" title="Loja ou Filtro..."></input>
		
	    <p id="lblFiltroID" name="lblFiltroID" class="lblGeneral">Filtro</p>
	    <select id="selFiltroID" name="selFiltroID" class="fldGeneral"></select>
	    <p id="lblFamiliaID" name="lblFamiliaID" class="lblGeneral">Fam�lia</p>
	    <select id="selFamiliaID" name="selFamiliaID" class="fldGeneral"></select>
	    <p id="lblMarcaID" name="lblMarcaID" class="lblGeneral">Marca</p>
	    <select id="selMarcaID" name="selMarcaID" class="fldGeneral"></select>
	    <p id="lblProprietarioID" name="lblProprietarioID" class="lblGeneral">Propriet�rio</p>
	    <select id="selProprietarioID" name="selProprietarioID" class="fldGeneral"></select>
        <p id="lblAlternativoID" name="lblAlternativoID" class="lblGeneral">Alternativo</p>
	    <select id="selAlternativoID" name="selAlternativoID" class="fldGeneral"></select>
        <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Pesquisa</p>
        <input type="text" id="txtPesquisa" name="txtPesquisa" class="fldGeneral"></input>

        <p id="lblOK" name="lblOK" class="lblGeneral">OK</p>
        <input type="checkbox" id="chkOK" name="chkOK" class="fldGeneral" title="Somente produtos com OK selecionado"></input>
        <p id="lblNaoOK" name="lblNaoOK" class="lblGeneral"></p>
        <input type="checkbox" id="chkNaoOK" name="chkNaoOK" class="fldGeneral" title="Somente produtos com OK n�o selecionado"></input>

        <p id="lblLoja" name="lblLoja" class="lblGeneral">Lojas</p>
        <input type="checkbox" id="chkLoja" name="chkLoja" class="fldGeneral" title="Lojas?"></input>
        <p id="lblCanal" name="lblCanal" class="lblGeneral">Canais</p>
        <input type="checkbox" id="chkCanal" name="chkCanal" class="fldGeneral" title="Canais?"></input>
        <p id="lblDicaLoja" name="lblDicaLoja" class="lblGeneral">Dicas</p>
        <input type="checkbox" id="chkDicaLoja" name="chkDicaLoja" class="fldGeneral" title="Dicas de lojas e canais?"></input>
                <p id="lblEstoque" name="lblEstoque" class="lblGeneral">Est</p>
        <input type="checkbox" id="chkEstoque" name="chkEstoque" class="fldGeneral" title="S� produtos com estoque?"></input>
        <p id="lblDicas2" name="lblDicas2" class="lblGeneral" title="">Dicas</p>
		<textarea id="txtDicas2" name="txtDicas2" class="fldGeneral" title="Dicas da loja"></textarea>
		<input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        
    </div>    
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
</body>

</html>
