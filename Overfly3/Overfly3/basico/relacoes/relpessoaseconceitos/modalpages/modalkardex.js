/********************************************************************

Library javascript para o modalkardex.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_CaracteristicasTimerInt = null;
var glb_RecalculaTimer = null;
var glb_List = 0;
var glb_nDOS = 0;
var glb_Desbloqueio = false;
var glb_controlID = 'btnListar';
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var dsoDetalhe = new CDatatransport("dsoDetalhe");
var dsoGrid = new CDatatransport("dsoGrid");
var dsoEMail = new CDatatransport("dsoEMail");
var dsoProduto = new CDatatransport("dsoProduto");
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalkardex.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalkardex.ASP

js_fg_modalkardexBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalkardexDblClick( grid, Row, Col)
js_modalkardexKeyPress(KeyAscii)
js_modalkardex_AfterRowColChange
js_modalkardex_ValidateEdit()
js_modalkardex_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()    
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalkardexBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;
    modalFrame.style.left = 0;
    
    
    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Kardex');

    secText('Kardex', 1);

    adjustElementsInForm([['lblTipo', 'selTipo', 20, 2, 0, -23],
                          ['lblEstoque', 'selEstoque', 10, 2],
                          ['lblLote', 'chkLote', 3, 2],
                          ['lblInicio', 'txtInicio', 10, 2],
                          ['lblFim', 'txtFim', 10, 2],
                          ['lblConsolidar', 'chkConsolidar', 3, 2],
                          ['btnListar', 'btnListar', 50, 2, -10],
                          ['btnExcel', 'btnExcel', 50, 2, 10],
                          ['btnDesbloqProduto', 'btnDesbloqProduto', 110, 2, 10]], null, null, true);
    
    lblTipo.style.width = '100px';
    selTipo.onchange = selTipo_onchange;
    selEstoque.onchange = selEstoque_onchange;

    
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
                          
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;  
 
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP + 50;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    lblReferenciaCusto.style.top = parseInt(btnOK.currentStyle.top, 10) - 5;
    lblReferenciaCusto.style.left = 10;
    
    txtReferenciaCusto.style.top = parseInt(lblReferenciaCusto.currentStyle.top, 10) + 15;
    txtReferenciaCusto.style.left = parseInt(lblReferenciaCusto.style.left, 10) + ELEM_GAP - 10;
    txtReferenciaCusto.style.width = FONT_WIDTH * 7 + 80;

    lblPedidoReferencia.style.top = parseInt(lblReferenciaCusto.currentStyle.top, 10);
    lblPedidoReferencia.style.left = parseInt(lblReferenciaCusto.style.left, 10) + ELEM_GAP + 135;

    txtPedidoReferencia.style.top = parseInt(lblPedidoReferencia.currentStyle.top, 10) + 15;
    txtPedidoReferencia.style.left = parseInt(lblPedidoReferencia.style.left, 10) + ELEM_GAP - 10;
    txtPedidoReferencia.style.width = FONT_WIDTH * 7 + 50;

    lblMoeda.style.top = parseInt(lblReferenciaCusto.currentStyle.top, 10);
    lblMoeda.style.left = parseInt(txtPedidoReferencia.style.left, 10) + ELEM_GAP + 105;

    txtMoeda.style.top = parseInt(lblMoeda.currentStyle.top, 10) + 15;
    txtMoeda.style.left = parseInt(lblMoeda.style.left, 10) + ELEM_GAP-10;
    txtMoeda.style.width = FONT_WIDTH * 7;

    lblValorUnitario.style.top = parseInt(lblReferenciaCusto.currentStyle.top, 10);
    lblValorUnitario.style.left = parseInt(lblMoeda.style.left, 10) + ELEM_GAP + 55;

    txtValorUnitario.style.top = parseInt(txtMoeda.currentStyle.top, 10);
    txtValorUnitario.style.left = parseInt(lblValorUnitario.style.left, 10);
    txtValorUnitario.style.width = FONT_WIDTH * 7 + 30;

    lblDespesaUnitaria.style.top = parseInt(lblValorUnitario.currentStyle.top, 10);
    lblDespesaUnitaria.style.left = parseInt(lblValorUnitario.style.left, 10) + ELEM_GAP + 85;

    txtDespesaUnitaria.style.top = parseInt(txtValorUnitario.currentStyle.top, 10);
    txtDespesaUnitaria.style.left = parseInt(lblDespesaUnitaria.style.left, 10) + ELEM_GAP - 10;
    txtDespesaUnitaria.style.width = FONT_WIDTH * 7 + 30;

    lblTaxa.style.top = parseInt(lblDespesaUnitaria.currentStyle.top, 10);
    lblTaxa.style.left = parseInt(lblDespesaUnitaria.style.left, 10) + ELEM_GAP + 85;

    txtTaxa.style.top = parseInt(txtDespesaUnitaria.currentStyle.top, 10);
    txtTaxa.style.left = parseInt(lblTaxa.style.left, 10) + ELEM_GAP - 10;
    txtTaxa.style.width = FONT_WIDTH * 7 + 10;

    lblObservacao.style.top = parseInt(lblTaxa.currentStyle.top, 10);
    lblObservacao.style.left = parseInt(lblTaxa.style.left, 10) + ELEM_GAP + 65;

    txtObservacao.style.top = parseInt(txtTaxa.currentStyle.top, 10);
    txtObservacao.style.left = parseInt(lblObservacao.style.left, 10) + ELEM_GAP - 10;
    txtObservacao.style.width = FONT_WIDTH * 7 + 200;


    txtInicio.onkeypress = verifyDateTimeNotLinked;
    txtInicio.setAttribute('verifyNumPaste', 1);
    txtInicio.setAttribute('thePrecision', 10, 1);
    txtInicio.setAttribute('theScale', 0, 1);
    txtInicio.setAttribute('minMax', new Array(0, 999999999), 1);
    txtInicio.onfocus = selFieldContent;
    txtInicio.maxLength = 10;

    txtFim.onkeypress = verifyDateTimeNotLinked;
    txtFim.setAttribute('verifyNumPaste', 1);
    txtFim.setAttribute('thePrecision', 10, 1);
    txtFim.setAttribute('theScale', 0, 1);
    txtFim.setAttribute('minMax', new Array(0, 999999999), 1);
    txtFim.onfocus = selFieldContent;
    txtFim.maxLength = 10;

    selTipo_onchange();
    chkLote.onclick = fillGridData;      
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
    
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        ;
    }
    // codigo privado desta janela
    else if (controlID == 'btnCanc') {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, glb_Desbloqueio);
    }
    else if (controlID == 'btnExcel') {
        glb_controlID = 'btnExcel';
        fillGridData();
    }
    else if (controlID == 'btnListar') 
    {
        fillGridData();
        glb_List = 1;
    }
    else if (controlID == 'btnDesbloqProduto') {
        desbloquearProduto();
    }
    
    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, a  pos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}
/********************************************************************
A�oes do combo
********************************************************************/
function cmbOnChange(cmb) {
    ;
}
/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    
    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
}

/********************************************************************
Solicitar dados do grid ao servidor
nTipo 1: Refersh
      2: Recalcular
********************************************************************/
function fillGridData() {
    var sSQL;
    var nTipo, nDataInicio, nDataFim, bLote, bConsolidar, bEstoqueProprio;
    var sLinguaLogada = getDicCurrLang();
    glb_aEmpresaData = getCurrEmpresaData();
    
    //INICIO DOS PARAMETROS
    
    // TIPO DE REGISTROS: GERENCIAL|CONTABIL
    nTipo = parseInt((selTipo.options[selTipo.selectedIndex].value), 10);
    // DATA INICIO E FIM
    nDataInicio = (txtInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtInicio.value, true) + '\'');
    nDataFim = (txtFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtFim.value, true) + '\'');
    // Vai querer Lote
    bLote = (chkLote.checked ? 1 : 0);
    // Resultado consolidado ?
    bConsolidar = (chkConsolidar.checked ? 1 : 0);

    bEstoqueProprio = selEstoque.value;

    if (!verificaData(txtInicio.value))
        return null;
    
    if (!verificaData(txtFim.value))
        return null;


    var strParameters = 'controle=' + 'listaExcel' + '&sEmpresaFantasia=' + glb_aEmpresaData[3] + '&glb_nRegistroID=' + glb_nRegistroID + '&nTipo=' + nTipo +
                    '&nDataInicio=' + nDataInicio + '&nDataFim=' + nDataFim + '&bLote=' + bLote + '&bConsolidar=' + bConsolidar +
                    '&bEstoqueProprio=' + bEstoqueProprio + '&sLinguaLogada=' + sLinguaLogada + '&glb_controlID=' + glb_controlID;

    if (glb_controlID == 'btnExcel') {

        lockControlsInModalWin(true);
        var frameReport = document.getElementById("frmReport");
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/ReportsGrid_Kardex.aspx?' + strParameters;
        lockControlsInModalWin(false);

        glb_controlID = 'btnListar';
    }
    else {
        setConnection(dsoGrid);
        dsoGrid.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/ReportsGrid_Kardex.aspx?' + strParameters;
        dsoGrid.ondatasetcomplete = fillGridData_DSC;
        dsoGrid.Refresh();
    }
}
/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{       
    showExtFrame(window, true);
    HiddenColumn = [];
    Colunas = [];
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    fg.FontSize = '8';
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    if ((chkLote.checked) && (selTipo.value == 374))
        HiddenColumn = [8, 9, 18, 19];
    else if ((!(chkLote.checked)) && (selTipo.value == 374))
        HiddenColumn = [0, 8, 9, 18, 19];
    else if (chkLote.checked)
        HiddenColumn = [ 18, 19];    
    else
        HiddenColumn = [0, 18, 19];

        
    
    headerGrid(fg,['Lote',
                   'Data',
                   'Pedido',
                   'NF',
                   'CFOP',
                   'Pessoa',
                   'M�todo',
                   'Moeda',
                   'Estoque',
                   'Deposito',
                   'Quant Pedido',
                   'Custo Unitario Pedido',
                   'Custo Total Pedido',
                   'Pagamento Pedido',
                   'Quant Total',
                   'Custo Unitario',
                   'Custo Total',
                   'Pagamento Medio',
                   'Operacao',
                   'RelPesConKardexID'], HiddenColumn);

        fillGridMask(fg, dsoGrid, ['Lote',
                               'Data',
                               'Pedido',
                               'NotaFiscal',
                               'CFOP',
                               'Pessoa',
                               'Metodo',
                               'Moeda',
                               'Estoque',
                               'Deposito',
                               'QuantidadePedido',
                               'CustoUnPedido',
                               'CustoTotalPedido',
                               'PagtoPed',                              
                               'QuantidadeTotal',
                               'CustoUnMed',
                               'CustoMedio',
                               'PagamentoMedio',
                               'Operacao',
                               'RelPesConKardexID'],
                               ['', '99/99/9999', '', '', '', '', '', '', '', '', '', '999999999.99', '999999999.99', '99/99/9999', '999999999.99', '', '999999999.99', '99/99/9999', '', ''],
                               ['', dTFormat, '', '', '', '', '', '', '', '', '', '###,###,##0.00', '###,###,##0.00', dTFormat, '', '###,###,##0.00', '###,###,##0.00', dTFormat, '', '']);

        alignColsInGrid(fg, [2, 3, 11, 12, 15, 16]);


    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1, false, 0);
    fg.ColWidth(4) = 600;
    fg.BorderStyle = 1;
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

	/*for (i=1; i<fg.Rows; i++)
	{
	    nCol = getColIndexByColKey(fg, 'Percentual');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;

	    nCol = getColIndexByColKey(fg, 'Valor');
	    if (cellIsLocked(i, nCol))
            fg.Cell(6, i, nCol, i, nCol) = 0XDCDCDC;
    }*/

    fg.Redraw = 2;
        
    if (fg.Rows > 1)
        fg.Editable = true;

    lockControlsInModalWin(false);

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        PreencheCampos(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesConKardexID')));
        fg.focus();
        if (glb_bProdutoBloqueado == "True")
            btnDesbloqProduto.style.visibility = 'inherit';
        else
            btnDesbloqProduto.style.visibility = 'hidden';
    }
    else
    {
        btnDesbloqProduto.style.visibility = 'hidden';
    }        

    // ajusta estado dos botoes
    setupBtnsFromGridState();
    selTipo.focus();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    glb_dataGridWasChanged = false;
    lockControlsInModalWin(true);
	var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
	var strPars = '?nPedidoID=' + escape(nPedidoID) + 
		'&bRecalcula=' + escape(0);
		
    dsoPedido.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/recalculapedido.asp' + strPars;
    dsoPedido.ondatasetcomplete = dsoPedido_DSC;
    dsoPedido.Refresh();
}

function dsoPedido_DSC()
{
    try
    {
        dsoGrid.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg.Rows = 1;
        lockControlsInModalWin(false);
        
		glb_CaracteristicasTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
        return null;
    }    

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
	glb_RecalculaTimer = window.setInterval('recalculaPedido()', 30, 'JavaScript');
	return null;
}

function recalculaPedido()
{
	if (glb_RecalculaTimer != null)
	{
		window.clearInterval(glb_RecalculaTimer);
		glb_RecalculaTimer = null;
	}

	var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtRegistroID.value');
	var strPars = '?nPedidoID=' + escape(nPedidoID) + 
		'&bRecalcula=' + escape(1);
		
    dsoPedido.URL = SYS_ASPURLROOT + '/modcomercial/submodpedidos/frmpedidos/serverside/recalculapedido.asp' + strPars;
    dsoPedido.ondatasetcomplete = recalculaPedido_DSC;
    dsoPedido.Refresh();
}

function recalculaPedido_DSC()
{
    lockControlsInModalWin(false);
    
    glb_nDOS = 0;
	fillGridData_DSC();
}

function excluirLinha()
{
	if (fg.Row > 0)
	{
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir esta linha?");
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
	
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();
        dsoGrid.recordset.Find(fg.ColKey(fg.Cols-1) , fg.TextMatrix(fg.Row, fg.Cols-1));

        if ( !(dsoGrid.recordset.EOF) )
        {
			fg.RemoveItem(fg.Row);
			dsoGrid.recordset.Delete();
            glb_dataGridWasChanged = true;
            setupBtnsFromGridState();
        }
        saveDataInGrid();
	}
}

function excluirTodos()
{
	if (fg.Row > 0)
	{
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir todas as linhas?");
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	        return null;
	
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();

        while (true)
        {
			dsoGrid.recordset.Delete();
			
			if (!dsoGrid.recordset.BOF)
				dsoGrid.recordset.MoveFirst();
			else
				break;
		}
        
        fg.Rows = 1;
		glb_dataGridWasChanged = true;
		setupBtnsFromGridState();
		saveDataInGrid();
	}
}

function calcTotItem(nLine)
{
    var nQtd, nValUnit;
    var lIsEditing = (nLine == null);
    nLine = (nLine == null ? fg.Row : nLine);
    
    if ( (fg.Col == 4)&&(lIsEditing) )
        nQtd = parseFloat(fg.EditText);
    else    
        nQtd = parseFloat(fg.TextMatrix(nLine,5));
                
    if ( (fg.Col == 7)&&(lIsEditing) )
        nValUnit = parseFloat(replaceStr(fg.EditText,',','.'));
    else    
        nValUnit = parseFloat(replaceStr(fg.TextMatrix(nLine,8),',','.'));
        
    if ( (! isNaN(nQtd)) && (! isNaN(nValUnit)) )
        fg.TextMatrix(nLine,9) = roundNumber(nQtd*nValUnit, 2);
    else    
        fg.TextMatrix(nLine,9) = '';
        
	gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4,'#########','S'], [8,'###,###,###,###.00','S']]);        
}


// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalkardexBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

function js_fg_modalkardex_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (cellIsLocked(grid, NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);

    }
}

function cellIsLocked(nRow, nCol)
{
    var retVal = false;
    
    if (nCol == getColIndexByColKey(fg, 'Percentual'))
    {
        if (getCellValueByColKey(fg, '_EP', nRow) == 0)
            retVal = true;
    }
    else if (nCol == getColIndexByColKey(fg, 'Valor'))
    {
        if (getCellValueByColKey(fg, '_EV', nRow) == 0)
            retVal = true;
    }
    
    return retVal;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalkardexDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalkardexKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalkardex_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalkardex_AfterEdit(Row, Col)
{
    if (fg.Editable)
    {
		if ((Col == getColIndexByColKey(fg, 'Percentual')) ||
			(Col == getColIndexByColKey(fg, 'Valor')) )
			fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    }
}
// FINAL DE EVENTOS DE GRID *****************************************

/********************************************************************
Evento de grid particular desta pagina
Preenche campos na interface.
********************************************************************/
function js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {
    var RelPesConKardexID;
    
    if ((fg.Rows > 0) && (OldRow != NewRow)) {
 //       lockControlsInModalWin(true);
        RelPesConKardexID = fg.ValueMatrix(NewRow, getColIndexByColKey(fg, 'RelPesConKardexID'));
        PreencheCampos(RelPesConKardexID);
 //       lockControlsInModalWin(false);
    }
}
function PreencheCampos(NewRow) {
    var sSQL;
    sSQL = ' SELECT  a.RelPesConKardexID, ISNULL(ee.PedidoID, SPACE(\'\'\)) AS PedidoReferenciaID, ' +
                    'ISNULL(g.ItemAbreviado, SPACE(\'\'\)) AS ReferenciaCustoID, ' +
                    'ISNULL(f.SimboloMoeda, SPACE(\'\'\))  AS SimboloMoeda, ' +
                    'ISNULL(d.ValorUnitario, SPACE(\'\'\)) AS ValorUnitario, ' +
                    'ISNULL(c.ValorDespesasTotal, 0) AS ValorDespesasTotal, ' +
                    'ISNULL(d.TaxaMoeda, SPACE(\'\'\)) AS TaxaMoeda, ' +
                    'ISNULL(e.Observacao, SPACE(\'\'\)) AS Observacao ' +
                'FROM RelacoesPesCon_Kardex a WITH(NOLOCK) ' +
                   'INNER JOIN dbo.Pedidos_Itens_Kardex c WITH(NOLOCK) ON (a.PedIteKardexID = c.PedIteKardexID) ' +
                   'INNER JOIN dbo.Pedidos_Itens d WITH(NOLOCK) ON (c.PedItemID = d.PedItemID) ' +
                   'LEFT OUTER JOIN dbo.Pedidos_Itens dd WITH(NOLOCK) ON (dd.PedItemID = d.PedItemReferenciaID) ' +
                   'LEFT OUTER JOIN dbo.Pedidos ee WITH(NOLOCK) ON (dd.PedidoID = ee.PedidoID) ' +
                   'INNER JOIN dbo.Pedidos e WITH(NOLOCK) ON (e.PedidoID = d.PedidoID) ' +
                   'INNER JOIN dbo.Operacoes h WITH(NOLOCK) ON (e.TransacaoID = h.OperacaoID) ' +
                   'LEFT OUTER JOIN dbo.TiposAuxiliares_Itens g WITH(NOLOCK) ON (h.ReferenciaCustoID = g.ItemID) ' +
                   'INNER JOIN dbo.Conceitos f WITH(NOLOCK) ON (a.MoedaID = f.ConceitoID) ' +
                'WHERE   a.RelPesConKardexID = ' + NewRow;

    setConnection(dsoDetalhe);
    dsoDetalhe.SQL = sSQL;
    dsoDetalhe.ondatasetcomplete = PreencheCampos_Set;
    dsoDetalhe.Refresh();
}

function PreencheCampos_Set () {

    if (!(dsoDetalhe.recordset.BOF && dsoDetalhe.recordset.EOF)) {
        txtReferenciaCusto.value = dsoDetalhe.recordset['ReferenciaCustoID'].value;
        txtPedidoReferencia.value = (dsoDetalhe.recordset['PedidoReferenciaID'].value == 0 ? txtPedidoReferencia.value = '' : dsoDetalhe.recordset['PedidoReferenciaID'].value);
        txtMoeda.value = dsoDetalhe.recordset['SimboloMoeda'].value;
        txtValorUnitario.value = dsoDetalhe.recordset['ValorUnitario'].value;
        txtDespesaUnitaria.value = (dsoDetalhe.recordset['ValorDespesasTotal'].value == 0 ? txtDespesaUnitaria.value = '' : dsoDetalhe.recordset['ValorDespesasTotal'].value); 
        txtTaxa.value = dsoDetalhe.recordset['TaxaMoeda'].value;
        txtObservacao.value = dsoDetalhe.recordset['Observacao'].value;
    }
    else {
        txtReferenciaCusto.value = '';
        txtPedidoReferencia.value = '';
        txtMoeda.value = '';
        txtValorUnitario.value = '';
        txtDespesaUnitaria.value = '';
        txtTaxa.value = '';
        txtObservacao.value = '';
    }

}

function selTipo_onchange()
{
    lblTipo.innerText = "Tipo " + selTipo.options[selTipo.selectedIndex].getAttribute("TipoID", 1);

    if (selTipo.value == 374)
    {
        chkLote.style.visibility = 'hidden';
        lblLote.style.visibility = 'hidden';

        chkLote.checked = 0;
    }
    else if (selTipo.value == 375)
    {
        chkLote.style.visibility = 'inherit';
        lblLote.style.visibility = 'inherit';
    }

    lista();
}

function selEstoque_onchange()
{
    fg.Rows = 1;
}

function js_fg_modalKardexMouseMove() 
{
    var currRow = fg.MouseRow;
    var currCol = fg.MouseCol;
    var sDescricao = '';

    if ((currCol = getColIndexByColKey(fg, 'CFOP')) && (currRow >= 1)) 
    {
        sDescricao = fg.TextMatrix(currRow, getColIndexByColKey(fg, 'Operacao'));

        glb_aCelHint = [[currRow, currCol, sDescricao]];
    }
}
function verificaData(sData) 
{
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        lockControlsInModalWin(false);
        return false;
    }
    return true;
}

function lista() 
{
    fillGridData();
    return true;
}


function desbloquearProduto() {
    var RelPesConKardexID = fg.ValueMatrix(1, getColIndexByColKey(fg, 'RelPesConKardexID'));
    var strPars = '?nRelPesConKardexID=' + escape(RelPesConKardexID);
    dsoProduto.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/desbloqueiaproduto.aspx' + strPars;
    dsoProduto.ondatasetcomplete = desbloquearProduto_DSC;
    dsoProduto.Refresh();     
}

function desbloquearProduto_DSC()
{
    if (window.top.overflyGen.Alert('Produto Desbloqueado.') == 0)
        return true;

    glb_Desbloqueio = true;

    return true;
}

function reports_onreadystatechange() {

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
        (frameReport.contentWindow.document.readyState == 'interactive') ||
        (frameReport.contentWindow.document.readyState == 'complete')) {

        lockControlsInModalWin(false);
    }
}