/********************************************************************
modalpesqnumserie.js

Library javascript para o modalpesqnumserie.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalpesqnumserieBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // configuracao inicial do html
    setupPage();   
        
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // estado do botao OK
    txtPesquisa_ondigit(txtPesquisa);
    
	// coloca foco no campo Pesquisa
    txtPesquisa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Caption da janela modal
    secText('C�digo de Barra', 1);
        
	var tempLeft = ELEM_GAP;
    var elem;
    
    // divExtPesquisa
    elem = window.document.getElementById('divExtPesquisa');
    with (elem.style)
    {
        visibility = 'visible';
        backgroundColor = 'transparent';
        width = 300;
        height = 50;
        top = 50;
        left = 19;
        tempLeft = parseInt(left);
    }

    // divPesquisa
    elem = window.document.getElementById('divPesquisa');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        width = FONT_WIDTH * 20;
        height = 40;
        top = 10;
        left = ((205 - 150)/ 2);
        tempLeft = parseInt(left, 10) +  parseInt(width, 10) +  ELEM_GAP;
    }

    // txtPesquisa
    elem = window.document.getElementById('txtPesquisa');
    elem.disabled = false;
    with (elem.style)
    {
        width = FONT_WIDTH * 25 + 3;
        elem.maxLength = 25;
    }
    
    txtPesquisa.onkeyup = txtPesquisa_ondigit;
    txtPesquisa.onkeypress = txtPesquisa_onkeypress;
    txtPesquisa.onfocus = selFieldContent;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
		btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Pressiona botao OK se pressionado Enter neste campo
********************************************************************/
function txtPesquisa_onkeypress()
{
    if ( event.keyCode == 13 )
        btnOK_Clicked();
}

/********************************************************************
Trava destrava btnOK em funcao de texto
********************************************************************/
function txtPesquisa_ondigit(ctl)
{
	if ( ctl == null )
		ctl = this;
		
	if ( ctl.value.length != 0 )
		btnOK.disabled = false;
	else
		btnOK.disabled = true;	
}

/********************************************************************
Usuario clicou btnOK
********************************************************************/
function btnOK_Clicked()
{
	txtPesquisa.value = trimStr(txtPesquisa.value);
	
	if ( txtPesquisa.value.length == 0 )
	{
		if ( window.top.overflyGen.Alert ('C�digo de em branco!') == 0 )
		    return null;
		
		lockControlsInModalWin(false);
		
		txtPesquisa.focus();
		
		btnOK.disabled = true;
	}
	else
		// 1. O usuario clicou o botao OK
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , txtPesquisa.value);
        
}        