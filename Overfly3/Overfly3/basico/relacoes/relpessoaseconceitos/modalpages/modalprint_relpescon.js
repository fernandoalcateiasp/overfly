/********************************************************************
modalprint_relpescon.js

Library javascript para o modalprint.asp
relacoes entre pessoas e conceitos - pesqlist
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_DepositoSelecionado = 'NULL';
var glb_strParameters = '';

var dsoProprietarios = new CDatatransport("dsoProprietarios");
var dsoMoedas = new CDatatransport("dsoMoedas");
var dsoEMail = new CDatatransport("dsoEMail");
var dsoCmbs01 = new CDatatransport("dsoCmbs01");
var dsoCmbs02 = new CDatatransport("dsoCmbs02");
var dsoCmbs03 = new CDatatransport("dsoCmbs03");
var dsoCmbs04 = new CDatatransport("dsoCmbs04");
var dsoPrint01 = new CDatatransport("dsoPrint01");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    adjustDivRelatorioEstoque()
    adjustDivPosicaoProdutos()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    glb_bUseProgressBar = true;
    asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();
    // ajusta o body do html
    with (modalprintBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // parametrizacao do dso dsoMoedas (designado para selMoedas)
    setConnection(dsoMoedas);

    dsoMoedas.SQL = 'SELECT 1 AS Indice, c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, CONVERT(varchar,1) AS Ordem ' +
        'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
        'WHERE a.ObjetoID=999 AND a.SujeitoID= ' + glb_nEmpresaID + ' AND a.TipoRelacaoID=12 AND a.RelacaoID=b.RelacaoID  ' +
        'AND b.Faturamento=1 AND b.MoedaID=c.ConceitoID  ' +
        'UNION ALL  ' +
        'SELECT 1 AS Indice, b.ConceitoID AS fldID, b.SimboloMoeda AS fldName, CONVERT(varchar,2) AS Ordem  ' +
        'FROM Recursos a WITH(NOLOCK), Conceitos b WITH(NOLOCK)  ' +
        'WHERE a.RecursoID=999 AND a.MoedaID=b.ConceitoID';

    dsoMoedas.ondatasetcomplete = dsoMoedas_DSC;
    dsoMoedas.Refresh();

    // Visualizacao da janela movida para dsoMoedas_DSC()
}

function dsoMoedas_DSC() {
    loadselMoedas();
    selEmpresa_onchange();
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();

    if (selReports.disabled == false)
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Funcao disparada pelo programador.
Carrega o combo de moedas
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function loadselMoedas() {
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selMoedas2];
    var aDSOsDunamics = [dsoMoedas];

    clearComboEx(['selMoedas2']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        oldDataSrc = aCmbsDynamics[i].dataSrc;
        oldDataFld = aCmbsDynamics[i].dataFld;
        aCmbsDynamics[i].dataSrc = '';
        aCmbsDynamics[i].dataFld = '';

        if (!((aDSOsDunamics[i].recordset.BOF) && (aDSOsDunamics[i].recordset.EOF)))
            aDSOsDunamics[i].recordset.MoveFirst();

        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].dataSrc = oldDataSrc;
        aCmbsDynamics[i].dataFld = oldDataFld;
    }

    // trava/destrava os combos de moedas
    if (selMoedas2.options.length == 0) {
        //selMoedas.disabled = true;
        selMoedas2.disabled = true;

    }
    else {
        //selMoedas.disabled = false;
        selMoedas2.disabled = false;
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id)
        btnCanc.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
        // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id) {
        pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/********************************************************************
Informa ao sistema o termino da impressao
********************************************************************/
function oPrinter_OnPrintFinish() {
    winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Relat�rios', 1);
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree;
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;

    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o paragrafo relatorios
    lblReports.innerText = 'Relat�rio';
    with (lblReports.style) {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // ajusta combo de relatorios
    with (selReports.style) {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // ajusta o divRelatorioEstoque
    with (divRelatorioEstoque.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divEtiquetasProdutos
    with (divEtiquetasProdutos.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o divPosicaoProdutos
    with (divPosicaoProdutos.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // O estado do botao btnOK
    btnOK_Status();

    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface() {
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;

    selReports.onchange = selReports_Change;

    // carrega a lista de relatorios
    loadReportsInList();

    // linka os divs nos relatorios
    linkDivsAndReports();

    // ajusta os divs
    adjustDivRelatorioEstoque();
    adjustDivEtiquetasProdutos();
    adjustDivPosicaoProdutos();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList() {
    var i;

    if (glb_arrayReports != null) {
        for (i = 0; i < glb_arrayReports.length; i++) {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);

            if (glb_arrayReports[i][2])
                selReports.selectedIndex = i;
        }

        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports() {
    divEtiquetasProdutos.setAttribute('report', 40122, 1);
    divRelatorioEstoque.setAttribute('report', 40123, 1);
    divPosicaoProdutos.setAttribute('report', 40125, 1);
}

/********************************************************************
Ajusta os elementos labels e checkbox do divPosicaoProdutos
********************************************************************/
function adjustDivPosicaoProdutos() {
    var i;
    var elem;
    var hGap = 0;
    var labelLeft;
    var lastChk_x, lastChk_cx, lastChk_y;
    var lineY;

    // ajusta e alinha os labels
    for (i = 0; i < (divPosicaoProdutos.children.length) ; i++) {
        elem = divPosicaoProdutos.children[i];

        if ((elem.tagName).toUpperCase() == 'P') {
            with (elem.style) {
                left = hGap;
                top = 0;
                width = (elem.innerText).length * FONT_WIDTH;
                height = 16;
                hGap = hGap + parseInt(width, 10) + (2 * ELEM_GAP);
                labelLeft = parseInt(left, 10);
            }

            elem.onclick = invertChkBox;
        }
    }

    // ajusta, alinha e checa os checkbox    
    for (i = 0; i < (divPosicaoProdutos.children.length) ; i++) {
        elem = divPosicaoProdutos.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if ((elem.type).toUpperCase() == 'CHECKBOX') {
                with (elem.style) {
                    backgroundColor = 'transparent';
                    left = parseInt((elem.previousSibling).currentStyle.left, 10) - 5;
                    top = parseInt((elem.previousSibling).currentStyle.top, 10) +
                          parseInt((elem.previousSibling).currentStyle.height, 10);
                    width = 3 * FONT_WIDTH;
                    height = 3 * FONT_WIDTH;

                    lastChk_x = parseInt(left);
                    lastChk_y = parseInt(top);
                    lastChk_cx = parseInt(width);
                }

                if (!((elem.value == 1) || (elem.value == 11) || (elem.value == 4) || (elem.value == -1)))
                    elem.checked = true;

                elem.onclick = chkBox_Clicked;
            }
        }
    }

    lblEstoqueFisico.style.left = lastChk_x - 5;
    chkEstoqueFisico.style.left = lastChk_x - 10;

    adjustElementsInForm([['lblProprietarios', 'selProprietarios', 30, 2, -11, -2],
                          ['lblOrdem', 'selOrdem', 14.6, 3, -11, -2],
                          ['lblMoeda2', 'selMoedas2', 14.6, 3]], null, null, true);
    adjustElementsInForm([['lblFiltro4', 'txtFiltro4', 50.6, 4, -11]], null, null, true);
    adjustElementsInForm([['lblFormatoSolicitacao', 'selFormatoSolicitacao', 10, 5, -11, 0]], null, null, true);


    lblMarca2.style.left = 310;
    selMarca2.style.left = 310;
    selMarca2.style.top = 20;
    selMarca2.style.width = 14 * FONT_WIDTH;
    selMarca2.style.height = 118;

    clearComboEx(['selProprietarios', 'selOrdem', 'selProprietarios2']);

    // parametrizacao do dso dsoProprietarios (designado para selProprietarios)
    setConnection(dsoProprietarios);

    dsoProprietarios.SQL =
        'SELECT ' +
            'SPACE(0) AS Colaborador, 0 AS ColaboradorID ' +
        'UNION ALL ' +
        'SELECT DISTINCT ' +
            'Colaboradores.Fantasia AS Colaborador, PessoaID AS ColaboradorID ' +
        'FROM ' +
            'Pessoas Colaboradores WITH(NOLOCK), RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
        'WHERE ' +
            'ProdutosEmpresa.TipoRelacaoID = 61 AND ProdutosEmpresa.SujeitoID=' + glb_nEmpresaID + ' AND ProdutosEmpresa.ProprietarioID=Colaboradores.PessoaID ' +
        'ORDER BY ' +
            'Colaborador';

    dsoProprietarios.ondatasetcomplete = adjustDivPosicaoProdutos_DSC;
    dsoProprietarios.Refresh();
}

function adjustDivPosicaoProdutos_DSC() {
    if (!((dsoProprietarios.recordset.BOF) && (dsoProprietarios.recordset.EOF))) {
        while (!dsoProprietarios.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoProprietarios.recordset['Colaborador'].value;
            oOption.value = dsoProprietarios.recordset['ColaboradorID'].value;
            selProprietarios.add(oOption);

            var oOption2 = document.createElement("OPTION");
            oOption2.text = dsoProprietarios.recordset['Colaborador'].value;
            oOption2.value = dsoProprietarios.recordset['ColaboradorID'].value;
            selProprietarios2.add(oOption2);

            dsoProprietarios.recordset.MoveNext();
        }
        selProprietarios.disabled = false;
        selProprietarios2.disabled = false;
    }
    else {
        selProprietarios.disabled = true;
        selProprietarios2.disabled = true;
    }

    var oOption = document.createElement("OPTION");
    oOption.text = 'ID';
    oOption.value = 0;
    selOrdem.add(oOption);

    var oOption = document.createElement("OPTION");
    oOption.text = 'Produto';
    oOption.value = 1;
    selOrdem.add(oOption);

    selOrdem.selectedIndex = 0;

    return null;
}

/********************************************************************
Ajusta os elementos labels e checkbox do divRelatorioEstoque
********************************************************************/
function adjustDivRelatorioEstoque() {
    var i;
    var elem;
    var hGap = 0;
    var labelLeft;
    var lastChk_x, lastChk_cx, lastChk_y;
    var lineY;

    // Por default os seguintes controle entram escondidos
    lblChkValoracao.style.visibility = 'hidden';
    chkValoracao.style.visibility = 'hidden';

    lblChkCustoContabil.style.visibility = 'hidden';
    chkCustoContabil.style.visibility = 'hidden';
    chkCustoContabil.checked = true;

    lblMoeda.style.visibility = 'hidden';
    selMoedas.style.visibility = 'hidden';

    lblDataEstoque.style.visibility = 'hidden';
    txtDataEstoque.style.visibility = 'hidden';

    adjustElementsInForm([['lblExcel', 'chkExcel', 3, 1, -10, -10],
						  ['lblFabricante', 'selFabricante', 13, 1, -7],
						  ['lblPadrao', 'selPadrao', 16, 1]], null, null, true);

    lblEmpresa.style.left = 290;
    lblEmpresa.style.top = -3;
    selEmpresa.style.left = 290;
    selEmpresa.style.top = 12;
    selEmpresa.style.width = 17 * FONT_WIDTH;
    selEmpresa.style.height = 118;
    selEmpresa.onchange = selEmpresa_onchange;
    selEmpresa.disabled = true;

    lblFamilias.style.left = selEmpresa.offsetLeft + selEmpresa.offsetWidth + ELEM_GAP;
    lblFamilias.style.top = -3;
    selFamilias.style.left = selEmpresa.offsetLeft + selEmpresa.offsetWidth + ELEM_GAP;
    selFamilias.style.top = 12;
    selFamilias.style.width = 20 * FONT_WIDTH;
    selFamilias.style.height = 118;

    lblMarca.style.left = selFamilias.offsetLeft + selFamilias.offsetWidth + ELEM_GAP;
    lblMarca.style.top = -3;
    selMarca.style.left = selFamilias.offsetLeft + selFamilias.offsetWidth + ELEM_GAP;
    selMarca.style.top = 12;
    selMarca.style.width = 14 * FONT_WIDTH;
    selMarca.style.height = 118;

    selFabricante.onchange = selFabricante_OnChange;
    selPadrao.onchange = selPadrao_OnChange;
    chkExcel.onclick = chkExcel_onclick;

    chkExcel_onclick();

    var nTop = 37;
    // ajusta e alinha os labels
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if (((elem.tagName).toUpperCase() == 'P') && (elem.id != lblFabricante.id) &&
			 (elem.id != lblPadrao.id) && (elem.id != lblFamilias.id) && (elem.id != lblExcel.id) &&
			 (elem.id != lblEmpresa.id) && (elem.id != lblMarca.id)) {
            with (elem.style) {
                left = hGap;
                top = nTop;
                width = (elem.innerText).length * FONT_WIDTH;
                height = 16;
                hGap = hGap + parseInt(width, 10) + (2 * ELEM_GAP);
                labelLeft = parseInt(left, 10);
            }

            elem.onclick = invertChkBox;
        }
    }

    // ajusta, alinha e checa os checkbox    
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') &&
                 (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                 (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                 (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                 (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
                 (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                 (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                 (elem.id.toUpperCase() != 'CHKEXCEL') &&
                 (elem.id.indexOf('_ESTOQUE_') < 0)) {
                with (elem.style) {
                    backgroundColor = 'transparent';
                    left = parseInt((elem.previousSibling).currentStyle.left, 10) - 5;
                    top = parseInt((elem.previousSibling).currentStyle.top, 10) +
					      parseInt((elem.previousSibling).currentStyle.height, 10);
                    width = 3 * FONT_WIDTH;
                    height = 3 * FONT_WIDTH;

                    lastChk_x = parseInt(left);

                    lastChk_y = parseInt(top);
                    lastChk_cx = parseInt(width);
                }

                if (!((elem.value == 1) || (elem.value == 11) || (elem.value == 4) || (elem.value == -1)))
                    elem.checked = true;

                elem.onclick = chkBox_Clicked;
            }
        }
    }

    adjustElementsInForm([['lblChk1_ESTOQUE_EPO', 'chk1_ESTOQUE_EPO', 3, 2, -10, 38],
                          ['lblChk1_ESTOQUE_EPE', 'chk1_ESTOQUE_EPE', 3, 2, -6],
                          ['lblChk1_ESTOQUE_EPD', 'chk1_ESTOQUE_EPD', 3, 2, -6],
                          ['lblChk1_ESTOQUE_EPC', 'chk1_ESTOQUE_EPC', 3, 2, -6],
                          ['lblChk1_ESTOQUE_EPT', 'chk1_ESTOQUE_EPT', 3, 2, -6],
                          ['lblChk1_ESTOQUE_ETO', 'chk1_ESTOQUE_ETO', 3, 2, -6],
                          ['lblChk1_ESTOQUE_ETE', 'chk1_ESTOQUE_ETE', 3, 2, -6],
                          ['lblChk1_ESTOQUE_ETD', 'chk1_ESTOQUE_ETD', 3, 2, -6],
                          ['lblChk1_ESTOQUE_ETC', 'chk1_ESTOQUE_ETC', 3, 2, -6],
                          ['lblChk1_ESTOQUE_EB', 'chk1_ESTOQUE_EB', 3, 2, -6]], null, null, true);

    adjustElementsInForm([['lblChk1_ESTOQUE_F', 'chk1_ESTOQUE_F', 3, 3, -11, 37],
						  ['lblChk1_ESTOQUE_Disp', 'chk1_ESTOQUE_Disp', 3, 3, -6],
						  ['lblChk1_ESTOQUE_RC', 'chk1_ESTOQUE_RC', 3, 3, -6],
						  ['lblChk1_ESTOQUE_RCC', 'chk1_ESTOQUE_RCC', 3, 3, -6],
						  ['lblChk1_ESTOQUE_RV', 'chk1_ESTOQUE_RV', 3, 3, -6],
						  ['lblChk1_ESTOQUE_RVC', 'chk1_ESTOQUE_RVC', 3, 3, -6],
						  ['lblChkTemEstoque', 'chkTemEstoque', 3, 3, -6],
                          ['lblChkProdutoSeparavel', 'chkProdutoSeparavel', 3, 3, -6],
                          ['lblChkReportaFabricante', 'ChkReportaFabricante', 3, 3, -6],
						  ['lblChkValoracao', 'chkValoracao', 3, 3, -6],
						  ['lblChkCustoContabil', 'chkCustoContabil', 3, 3, -6],
                          ['lblMoeda', 'selMoedas', 8, 3],
                          ['lblDataEstoque', 'txtDataEstoque', 19, 3],
                          ['lblDeposito', 'selDeposito', 21, 3]], null, null, true);

    adjustElementsInForm([['lblProprietarios2', 'selProprietarios2', 21, 2, -10, 123],
		['lblFiltro', 'txtFiltro', 67.6, 2]], null, null, true);

    // campo txtDataEstoque digitacao maxima: 12/12/2002 12:12:12
    txtDataEstoque.maxLength = 19;
    // e seleciona conteudo quando em foco
    txtDataEstoque.onfocus = selFieldContent;
    // e so aceita caracteres coerentes com data
    txtDataEstoque.onkeypress = verifyDateTimeNotLinked;

    setConnection(dsoCmbs01);
    glb_nDynamicCmbs = 4;

    dsoCmbs01.SQL = 'SELECT 0 AS fldID, SPACE(20) AS fldName, 0 AS EhDefault ' +
                    'UNION ALL ' +
                    'SELECT DISTINCT c.PessoaID AS fldID, c.Fantasia AS fldName, 0 AS EhDefault ' +
                    'FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Pessoas c WITH(NOLOCK) ' +
                    'WHERE a.SujeitoID=' + glb_nEmpresaID + ' AND a.ObjetoID=b.ConceitoID AND ' +
                    '(a.EstadoID=2 OR a.EstadoID>=6) AND b.FabricanteID=c.PessoaID ' +
                    'ORDER BY fldName';

    dsoCmbs01.ondatasetcomplete = relatorioEstoque_DSC;
    dsoCmbs01.Refresh();

    setConnection(dsoCmbs02);

    dsoCmbs02.SQL = 'SELECT ConceitoID AS fldID, Conceito AS fldName, 0 AS EhDefault ' +
                    'FROM Conceitos WITH(NOLOCK) ' +
                    'WHERE (EstadoID=2 AND TipoConceitoID=302) ' +
                    'ORDER BY Conceito';

    dsoCmbs02.ondatasetcomplete = relatorioEstoque_DSC;
    dsoCmbs02.Refresh();

    setConnection(dsoCmbs03);

    dsoCmbs03.SQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName, EhDefault AS EhDefault ' +
                    'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                    'WHERE (EstadoID=2 AND TipoID=410 ) ' +//AND Filtro LIKE \'%<EST>%\' AND ItemID NOT IN (CASE WHEN  (dbo.fn_Pessoa_Localidade(' + glb_nEmpresaID + ',1,NULL,NULL) <> 130) THEN 716 ELSE 0 END)) ' +
                    'ORDER BY EhDefault DESC, ItemMasculino';

    dsoCmbs03.ondatasetcomplete = relatorioEstoque_DSC;
    dsoCmbs03.Refresh();

    setConnection(dsoCmbs04);
        
    dsoCmbs04.SQL = 'SELECT 0 AS fldID, ' + '\'' + 'Todos' + '\'' + 'AS fldName, 1 AS Indice ' +
		'UNION ALL ' +
		'SELECT a.PessoaID, a.Fantasia, 2 AS Indice ' +
		'FROM Pessoas a WITH(NOLOCK) ' +
		'WHERE (a.PessoaID = ' + glb_nEmpresaID + ') ' +
		'UNION ALL ' +
		'SELECT b.PessoaID, b.Fantasia, 3 AS Indice ' +
		'FROM RelacoesPessoas a WITH (NOLOCK) ' +
		'INNER JOIN Pessoas b WITH (NOLOCK) ON (a.ObjetoID = b.PessoaID) ' +
		'WHERE (a.SujeitoID = ' + glb_nEmpresaID + ' AND a.TipoRelacaoID = 28 AND a.EstadoID = 2 AND b.EstadoID = 2) ' +
		'ORDER BY Indice, fldName';

    dsoCmbs04.ondatasetcomplete = relatorioEstoque_DSC;
    dsoCmbs04.Refresh();
}

function chkExcel_onclick() {
    if (chkExcel.checked) {
        selPadrao.selectedIndex = 0;
    }

    var n = 0;

    if (!chkExcel.checked) {
        for (i = 0; i < selEmpresa.length; i++) {
            selEmpresa.options[i].selected = (selEmpresa.options[i].value == glb_nEmpresaID);

            if (selEmpresa.options[i].value == glb_nEmpresaID)
                n = i;
        }
    }

    selEmpresa.disabled = !chkExcel.checked;
}

function relatorioEstoque_DSC() {
    glb_nDynamicCmbs--;
    var i;

    if (glb_nDynamicCmbs > 0)
        return null;

    var aDSOs = [dsoCmbs01, dsoCmbs02, dsoCmbs03, dsoCmbs04];
    var aCmbs = [selFabricante, selFamilias, selPadrao, selDeposito];
    clearComboEx(['selFabricante', 'selFamilias', 'selPadrao', 'selDeposito']);
    for (i = 0; i < aCmbs.length; i++) {
        while (!aDSOs[i].recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = aDSOs[i].recordset['fldName'].value;
            oOption.value = aDSOs[i].recordset['fldID'].value;

            if (i != 3) {
                if (aDSOs[i].recordset['EhDefault'].value == true)
                    oOption.selected = true;
            }
            else {
                if (aDSOs[i].recordset['fldName'].value == 'Todos')
                    oOption.selected = true;
            }

            aCmbs[i].add(oOption);
            aDSOs[i].recordset.MoveNext();
        }
    }
}

function selFabricante_OnChange() {

    if (this.value == 33968 && selPadrao.value == 702) {

        chk1_ESTOQUE_EPE.checked = false;
        chk1_ESTOQUE_EPD.checked = false;
        chk1_ESTOQUE_EPC.checked = false;
        chkTemEstoque.checked = false;
  
        //Selecionar a empresa Alcateia RJ ou Alcateia Fil RJ
        for (i = 0; i < selEmpresa.options.length; i++) {

            if ((selEmpresa.options.item(i).value == 10) || (selEmpresa.options.item(i).value == 14)){

                selEmpresa.options.item(i).selected = true;
            }
        }
    }

    if (chkExcel.checked)
        return;

    if (this.value == 10002) {
        selOptByValueInSelect(getHtmlId(), 'selPadrao', 702);
        selPadrao_OnChange(selPadrao.value);
    }
    else if ((this.value != 10002) && (selPadrao.value == 702))
        selPadrao.selectedIndex = 0;
    else if (this.value == 33968)
    {
        selOptByValueInSelect(getHtmlId(), 'selPadrao', 702);

        chkExcel.checked = true;

        chk_1.checked = true;
        chk_12.checked = true;
        chk_13.checked = true;
        chk_2.checked = true;
        chk_14.checked = true;
        chk_4.checked = true;
        chk_15.checked = true;
        chk_3.checked = true;
        chk_4.checked = true;


        chk1_ESTOQUE_EPO.checked = true;
        chk1_ESTOQUE_EPE.checked = false;
        chk1_ESTOQUE_EPD.checked = false;
        chk1_ESTOQUE_EPC.checked = false;
        chk1_ESTOQUE_EPT.checked = false;
        chk1_ESTOQUE_ETO.checked = false;
        chk1_ESTOQUE_ETE.checked = false;

        btn_Estoques1onclick(chk1_ESTOQUE_EPO);
    }
        
}

function selPadrao_OnChange(sOrigem) {
    document.getElementById('chk1_ESTOQUE_EPT').style.visibility = 'hidden';
    //document.getElementById('lblPadrao').style.visibility = 'hidden';
    lblChk1_ESTOQUE_EPT.style.visibility = "hidden";

    chk1_ESTOQUE_EPO.disabled = false;
    chk1_ESTOQUE_EPE.disabled = false;
    chk1_ESTOQUE_EPD.disabled = false;
    chk1_ESTOQUE_EPC.disabled = false;
    chk1_ESTOQUE_EPT.disabled = false;
    chk1_ESTOQUE_ETO.disabled = false;
    chk1_ESTOQUE_ETE.disabled = false;
    chk1_ESTOQUE_ETD.disabled = false;
    chk1_ESTOQUE_ETC.disabled = false;
    chk1_ESTOQUE_EB.disabled = false;
    chk_1.disabled = false;
    chk_2.disabled = false;
    chk_3.disabled = false;
    chk_4.disabled = false;
    //chk_11.disabled = false;
    chk_12.disabled = false;
    chk_13.disabled = false;
    chk_14.disabled = false;
    chk_15.disabled = false;
    chkExcel.disabled = false;
    chkTemEstoque.disabled = false;
    selFabricante.disabled = false;
    selMarca.disabled = false;
    chk1_ESTOQUE_F.disabled = false;
    chk1_ESTOQUE_Disp.disabled = false;
    chk1_ESTOQUE_RC.disabled = false;
    chk1_ESTOQUE_RCC.disabled = false;
    chk1_ESTOQUE_RV.disabled = false;
    chk1_ESTOQUE_RVC.disabled = false;
    chkTemEstoque.disabled = false;
    chkProdutoSeparavel.disabled = false;
    chkValoracao.disabled = false;
    chkCustoContabil.disabled = false;
    selDeposito.disabled = false;
    selProprietarios2.disabled = false;

    if ((this.value == 701) || (sOrigem == 701)) {
        document.getElementById('chk1_ESTOQUE_EPT').style.visibility = 'visible';
        lblChk1_ESTOQUE_EPT.style.visibility = "visible";
    }
    else if ((this.value == 702) || (sOrigem == 702)) {
        selOptByValueInSelect(getHtmlId(), 'selFabricante', 10002);

        // Os estados do produto

        for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
            elem = divRelatorioEstoque.children[i];

            if ((elem.tagName).toUpperCase() == 'INPUT') {
                if (((elem.type).toUpperCase() == 'CHECKBOX') &&
					(elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
					(elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
					(elem.id.toUpperCase() != 'CHKVALORACAO') &&
					(elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
					(elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
					(elem.id.indexOf('_ESTOQUE_') < 0)) {
                    if (!(elem.value == 5))
                        elem.checked = true;
                }
            }
        }

        chk1_ESTOQUE_EPO.checked = true;
        chk1_ESTOQUE_EPE.checked = true;
        chk1_ESTOQUE_EPD.checked = true;
        chk1_ESTOQUE_EPC.checked = true;
        chk1_ESTOQUE_EPT.checked = false;


        btn_Estoques1onclick(chk1_ESTOQUE_EPO);
    }
    else if ((this.value == 703) || (sOrigem == 703)) {
        chk1_ESTOQUE_EPO.checked = true;
        chk1_ESTOQUE_EPE.checked = false;
        chk1_ESTOQUE_EPD.checked = false;
        chk1_ESTOQUE_EPC.checked = false;
        chk1_ESTOQUE_EPT.checked = false;
        chk1_ESTOQUE_ETO.checked = false;
        chk1_ESTOQUE_ETE.checked = false;
        chk1_ESTOQUE_ETD.checked = false;
        chk1_ESTOQUE_ETC.checked = false;
        chk1_ESTOQUE_EB.checked = false;
        chk_1.checked = true;
        chk_2.checked = true;
        chk_3.checked = true;
        //chk_11.checked = true;
        chk_12.checked = true;
        chk_13.checked = true;
        chk_14.checked = true;
        chk_15.checked = true;
        
        chkTemEstoque.checked = true;

        /*for (i = 0; i < selFamilias.length; i++) {
            if ((selFamilias.options(i).value == 2703) ||
				(selFamilias.options(i).value == 2713) ||
				(selFamilias.options(i).value == 2753))
                selFamilias.options(i).selected = true;
            else}*/

        selEmpresa.disabled = true;
        selFamilias.options(i).selected = false;
        
        //selOptByValueInSelect(getHtmlId(), 'selFabricante', 10003);
        btn_Estoques1onclick(chk1_ESTOQUE_EPO);
    }
    else if ((this.value == 710) || (sOrigem == 710)) {
        chkTemEstoque.disabled = true;
    }
    else if ((this.value == 951) || (sOrigem == 951)) {
        chk1_ESTOQUE_EPO.checked = true;
        chk1_ESTOQUE_EPE.checked = false;
        chk1_ESTOQUE_EPD.checked = false;
        chk1_ESTOQUE_EPC.checked = false;
        chk1_ESTOQUE_EPT.checked = false;
        chk1_ESTOQUE_ETO.checked = false;
        chk1_ESTOQUE_ETE.checked = false;
        chk1_ESTOQUE_ETD.checked = false;
        chk1_ESTOQUE_ETC.checked = false;
        chk1_ESTOQUE_EB.checked = false;
        chk_1.checked = false;
        chk_2.checked = true;
        chk_3.checked = true;
        chk_4.checked = true;
        //chk_11.checked = false;
        chk_12.checked = true;
        chk_13.checked = true;
        chk_14.checked = true;
        chk_15.checked = true;
        chkExcel.checked = true;
        chkTemEstoque.checked = true;
        selMarca.selected = false;

        chk1_ESTOQUE_EPO.disabled = true;
        chk1_ESTOQUE_EPE.disabled = true;
        chk1_ESTOQUE_EPD.disabled = true;
        chk1_ESTOQUE_EPT.disabled = true;
        chk1_ESTOQUE_EPC.disabled = true;
        chk1_ESTOQUE_ETO.disabled = true;
        chk1_ESTOQUE_ETE.disabled = true;
        chk1_ESTOQUE_ETD.disabled = true;
        chk1_ESTOQUE_ETC.disabled = true;
        chk1_ESTOQUE_EB.disabled = true;
        chk_1.disabled = true;
        chk_2.disabled = true;
        chk_3.disabled = true;
        chk_4.disabled = true;
        //chk_11.disabled = true;
        chk_12.disabled = true;
        chk_13.disabled = true;
        chk_14.disabled = true;
        chk_15.disabled = true;
        chkExcel.disabled = true;
        chkTemEstoque.disabled = true;
        selFabricante.disabled = true;
        selMarca.disabled = true;
        chk1_ESTOQUE_F.disabled = true;
        chk1_ESTOQUE_Disp.disabled = true;
        chk1_ESTOQUE_RC.disabled = true;
        chk1_ESTOQUE_RCC.disabled = true;
        chk1_ESTOQUE_RV.disabled = true;
        chk1_ESTOQUE_RVC.disabled = true;
        chkTemEstoque.disabled = true;
        chkProdutoSeparavel.disabled = true;
        chkValoracao.disabled = true;
        chkCustoContabil.disabled = true;
        selDeposito.disabled = true;
        selProprietarios2.disabled = true;

        selOptByValueInSelect(getHtmlId(), 'selFabricante', 15395);
        btn_Estoques1onclick(chk1_ESTOQUE_EPO);
    }
    else if ((this.value == 952) || (sOrigem == 952)) {
        chk1_ESTOQUE_EPO.checked = true;
        chk1_ESTOQUE_EPE.checked = false;
        chk1_ESTOQUE_EPD.checked = false;
        chk1_ESTOQUE_EPC.checked = false;
        chk1_ESTOQUE_EPT.checked = false;
        chk1_ESTOQUE_ETO.checked = false;
        chk1_ESTOQUE_ETE.checked = false;
        chk1_ESTOQUE_ETD.checked = false;
        chk1_ESTOQUE_ETC.checked = false;
        chk1_ESTOQUE_EB.checked = false;
        chk_1.checked = false;
        chk_2.checked = true;
        chk_3.checked = true;
        chk_4.checked = true;
        //chk_11.checked = true;
        chk_12.checked = true;
        chk_13.checked = true;
        chk_14.checked = true;
        chk_15.checked = true;
        chkExcel.checked = true;
        //chkTemEstoque.checked = true;
        //selMarca.selected = false;

        chk1_ESTOQUE_EPO.disabled = true;
        chk1_ESTOQUE_EPE.disabled = true;
        chk1_ESTOQUE_EPD.disabled = true;
        chk1_ESTOQUE_EPC.disabled = true;
        chk1_ESTOQUE_EPT.disabled = true;
        chk1_ESTOQUE_ETO.disabled = true;
        chk1_ESTOQUE_ETE.disabled = true;
        chk1_ESTOQUE_ETD.disabled = true;
        chk1_ESTOQUE_ETC.disabled = true;
        chk1_ESTOQUE_EB.disabled = true;
        chk_1.disabled = true;
        chk_2.disabled = true;
        chk_3.disabled = true;
        chk_4.disabled = true;
        //chk_11.disabled = true;
        chk_12.disabled = true;
        chk_13.disabled = true;
        chk_14.disabled = true;
        chk_15.disabled = true;
        chkExcel.disabled = true;
        chkTemEstoque.disabled = true;
        chk1_ESTOQUE_F.disabled = true;
        chk1_ESTOQUE_Disp.disabled = true;
        chk1_ESTOQUE_RC.disabled = true;
        chk1_ESTOQUE_RCC.disabled = true;
        chk1_ESTOQUE_RV.disabled = true;
        chk1_ESTOQUE_RVC.disabled = true;
        chkTemEstoque.disabled = true;
        chkProdutoSeparavel.disabled = true;
        chkValoracao.disabled = true;
        chkCustoContabil.disabled = true;
        selDeposito.disabled = true;
        selProprietarios2.disabled = true;
        selEmpresa.disabled = false;

        btn_Estoques1onclick(chk1_ESTOQUE_EPO);
    }
    else if ((this.value == 953) || (sOrigem == 953)) {
        chk1_ESTOQUE_EPO.checked = true;
        chk1_ESTOQUE_EPE.checked = false;
        chk1_ESTOQUE_EPD.checked = false;
        chk1_ESTOQUE_EPC.checked = false;
        chk1_ESTOQUE_EPT.checked = false;
        chk1_ESTOQUE_ETO.checked = false;
        chk1_ESTOQUE_ETE.checked = false;
        chk1_ESTOQUE_ETD.checked = false;
        chk1_ESTOQUE_ETC.checked = false;
        chk1_ESTOQUE_EB.checked = false;
        chk_1.checked = false;
        chk_2.checked = true;
        chk_3.checked = true;
        chk_4.checked = false;
        //chk_11.checked = true;
        chk_12.checked = true;
        chk_13.checked = true;
        chk_14.checked = true;
        chk_15.checked = true;
        chkExcel.checked = true;
        chkTemEstoque.checked = true;
        //selMarca.selected = false;

        chk1_ESTOQUE_EPO.disabled = false;
        chk1_ESTOQUE_EPE.disabled = false;
        chk1_ESTOQUE_EPD.disabled = false;
        chk1_ESTOQUE_EPC.disabled = false;
        chk1_ESTOQUE_EPT.disabled = false;
        chk1_ESTOQUE_ETO.disabled = false;
        chk1_ESTOQUE_ETE.disabled = false;
        chk1_ESTOQUE_ETD.disabled = false;
        chk1_ESTOQUE_ETC.disabled = false;
        chk1_ESTOQUE_EB.disabled = true;
        chk_1.disabled = false;
        chk_2.disabled = false;
        chk_3.disabled = false;
        chk_4.disabled = false;
        //chk_11.disabled = false;
        chk_12.disabled = false;
        chk_13.disabled = false;
        chk_14.disabled = false;
        chk_15.disabled = false;
        chkExcel.disabled = true;
        chkTemEstoque.disabled = true;
        chk1_ESTOQUE_F.disabled = true;
        chk1_ESTOQUE_Disp.disabled = true;
        chk1_ESTOQUE_RC.disabled = true;
        chk1_ESTOQUE_RCC.disabled = true;
        chk1_ESTOQUE_RV.disabled = true;
        chk1_ESTOQUE_RVC.disabled = true;
        chkTemEstoque.disabled = true;
        chkProdutoSeparavel.disabled = true;
        chkValoracao.disabled = true;
        chkCustoContabil.disabled = true;
        selDeposito.disabled = false;
        selProprietarios2.disabled = false;
        selEmpresa.disabled = false;

        selOptByValueInSelect(getHtmlId(), 'selMarca', 3093);
        btn_Estoques1onclick(chk1_ESTOQUE_EPO);
    }
    else if ((this.value == 716) || (sOrigem == 716)) {
        document.getElementById('chk1_ESTOQUE_EPT').style.visibility = 'visible';
        lblChk1_ESTOQUE_EPT.style.visibility = "visible";

        selMoedas.disabled = true;

        lblDataEstoque.style.visibility = 'visible';
        txtDataEstoque.style.visibility = 'visible';

        lblMoeda.style.visibility = 'visible';
        selMoedas.style.visibility = 'visible';

        document.getElementById('chkReportaFabricante').style.visibility = 'visible';
        lblChkReportaFabricante.style.visibility = "visible";

        document.getElementById('chkValoracao').style.visibility = 'visible';
        lblChkValoracao.style.visibility = "visible";

        document.getElementById('chkCustoContabil').style.visibility = 'visible';
        lblChkCustoContabil.style.visibility = "visible";

        chk1_ESTOQUE_EPO.checked = true;
        chk1_ESTOQUE_EPE.checked = true;
        chk1_ESTOQUE_EPD.checked = true;
        chk1_ESTOQUE_EPC.checked = true;
        chk1_ESTOQUE_EPT.checked = true;
        chk1_ESTOQUE_ETO.checked = false;
        chk1_ESTOQUE_ETE.checked = false;
        chk1_ESTOQUE_ETD.checked = false;
        chk1_ESTOQUE_ETC.checked = false;
        chk1_ESTOQUE_EB.checked = false;
        chk_1.checked = true;
        chk_2.checked = true;
        chk_3.checked = true;
        chk_4.checked = true;
        //chk_11.checked = true;
        chk_12.checked = true;
        chk_13.checked = true;
        chk_14.checked = true;
        chk_15.checked = true;
        chkExcel.checked = true;
        chkTemEstoque.checked = true;
        chkValoracao.checked = true;
        chkReportaFabricante.checked = false;
        //selMarca.selected = false;

        chk1_ESTOQUE_EPO.disabled = true;
        chk1_ESTOQUE_EPE.disabled = true;
        chk1_ESTOQUE_EPD.disabled = true;
        chk1_ESTOQUE_EPC.disabled = true;
        chk1_ESTOQUE_EPT.disabled = true;
        chk1_ESTOQUE_ETO.disabled = true;
        chk1_ESTOQUE_ETE.disabled = true;
        chk1_ESTOQUE_ETD.disabled = true;
        chk1_ESTOQUE_ETC.disabled = true;
        chk1_ESTOQUE_EB.disabled = true;
        chk_1.disabled = true;
        chk_2.disabled = true;
        chk_3.disabled = true;
        chk_4.disabled = true;
        //chk_11.disabled = true;
        chk_12.disabled = true;
        chk_13.disabled = true;
        chk_14.disabled = true;
        chk_15.disabled = true;
        chkExcel.disabled = true;
        chkTemEstoque.disabled = true;
        chk1_ESTOQUE_F.disabled = true;
        chk1_ESTOQUE_Disp.disabled = true;
        chk1_ESTOQUE_RC.disabled = true;
        chk1_ESTOQUE_RCC.disabled = true;
        chk1_ESTOQUE_RV.disabled = true;
        chk1_ESTOQUE_RVC.disabled = true;
        chkTemEstoque.disabled = true;
        chkProdutoSeparavel.disabled = true;
        chkValoracao.disabled = true;
        chkCustoContabil.disabled = true;
        selDeposito.disabled = false;
        selProprietarios2.disabled = false;
        selEmpresa.disabled = false;
        chkValoracao.disabled = true;
        chkReportaFabricante.disabled = true;

        //            selOptByValueInSelect(getHtmlId(), 'selMarca', 3093);
        //            btn_Estoques1onclick(chk1_ESTOQUE_EPO);


    }
}

function btn_Estoques1onclick(chkObject) {
    var i = 0;
    var sValoracaoVisibility = '';
    var sDataEstoqueVisibility = '';
    var sChkProdutoSeparavelVisibility = 'inherit';

    var aCmbsEstoques = new Array(chk1_ESTOQUE_EPO,
                                  chk1_ESTOQUE_EPE,
                                  chk1_ESTOQUE_EPD,
                                  chk1_ESTOQUE_EPC,
                                  chk1_ESTOQUE_EPT,
                                  chk1_ESTOQUE_ETO,
                                  chk1_ESTOQUE_ETE,
                                  chk1_ESTOQUE_ETD,
                                  chk1_ESTOQUE_ETC,
								  chk1_ESTOQUE_EB);

    for (i = 0; i < aCmbsEstoques.length; i++) {
        if (aCmbsEstoques[i].checked) {
            sValoracaoVisibility = 'inherit';
            sChkProdutoSeparavelVisibility = 'hidden';

            if ((i <= 4) && (sDataEstoqueVisibility != 'hidden'))
                sDataEstoqueVisibility = 'inherit';
            else
                sDataEstoqueVisibility = 'hidden';
        }
        else if (sValoracaoVisibility != 'inherit')
            sValoracaoVisibility = 'hidden';
    }

    sDataEstoqueVisibility = (sDataEstoqueVisibility == '' ? 'hidden' : sDataEstoqueVisibility);

    lblChkValoracao.style.visibility = sValoracaoVisibility;
    chkValoracao.style.visibility = sValoracaoVisibility;

    lblChkCustoContabil.style.visibility = sValoracaoVisibility;
    chkCustoContabil.style.visibility = sValoracaoVisibility;

    lblChkProdutoSeparavel.style.visibility = sChkProdutoSeparavelVisibility;
    chkProdutoSeparavel.style.visibility = sChkProdutoSeparavelVisibility;
    if (sChkProdutoSeparavelVisibility == 'hidden')
        chkProdutoSeparavel.checked = false;

    if (sValoracaoVisibility == 'hidden') {
        chkValoracao.checked = false;
        chkCustoContabil.checked = true;
    }

    if ((chkValoracao.style.visibility == 'inherit') &&
         (chkValoracao.checked)) {
        lblMoeda.style.visibility = 'inherit';
        selMoedas.style.visibility = 'inherit';
    }
    else {
        lblMoeda.style.visibility = 'hidden';
        selMoedas.style.visibility = 'hidden';
    }

    lblDataEstoque.style.visibility = sDataEstoqueVisibility;
    txtDataEstoque.style.visibility = sDataEstoqueVisibility;
    if (sDataEstoqueVisibility == 'hidden')
        txtDataEstoque.value = '';

}

/********************************************************************
Ajusta os elementos labels e checkbox do divEtiquetasProdutos
********************************************************************/
function adjustDivEtiquetasProdutos() {
    var i;
    var elem;
    var hGap = 0;
    var labelLeft;
    var lastChk_x, lastChk_cx, lastChk_y;

    // ajusta e alinha os labels
    for (i = 0; i < (divEtiquetasProdutos.children.length) ; i++) {
        elem = divEtiquetasProdutos.children[i];

        if ((elem.tagName).toUpperCase() == 'P') {
            with (elem.style) {
                left = hGap;
                top = 0;
                width = (elem.innerText).length * FONT_WIDTH;
                height = 16;
                hGap = hGap + parseInt(width, 10) + (2 * ELEM_GAP);
                labelLeft = parseInt(left, 10);
            }

            elem.onclick = invertChkBox;
        }
    }

    // ajusta, alinha e checa os checkbox    
    for (i = 0; i < (divEtiquetasProdutos.children.length) ; i++) {
        elem = divEtiquetasProdutos.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if ((elem.type).toUpperCase() == 'CHECKBOX') {
                with (elem.style) {
                    backgroundColor = 'transparent';
                    left = parseInt((elem.previousSibling).currentStyle.left, 10) - 5;
                    top = parseInt((elem.previousSibling).currentStyle.top, 10) +
                          parseInt((elem.previousSibling).currentStyle.height, 10);
                    width = 3 * FONT_WIDTH;
                    height = 3 * FONT_WIDTH;

                    lastChk_x = parseInt(left);
                    lastChk_y = parseInt(top);
                    lastChk_cx = parseInt(width);
                }

                if (!((elem.value == 1) || (elem.value == 11) || (elem.value == 4) || (elem.value == -1)))
                    elem.checked = true;

                elem.onclick = chkBox_Clicked;
            }
        }
    }

    var aElem = new Array;

    txtEtiquetaVertical.maxLength = 2;
    txtEtiquetaHorizontal.maxLength = 2;
    txtAltura.maxLength = 2;
    txtLargura.maxLength = 2;
    txtMargemSuperior.maxLength = 2;
    txtMargemEsquerda.maxLength = 2;

    txtEtiquetaVertical.value = 10;
    txtEtiquetaHorizontal.value = 2;
    txtAltura.value = 6;
    txtLargura.value = 70;
    txtMargemSuperior.value = 2;
    txtMargemEsquerda.value = 4;

    aElem[aElem.length] = ['lblEtiquetaVertical', 'txtEtiquetaVertical', 4, 2, -10];
    aElem[aElem.length] = ['lblEtiquetaHorizontal', 'txtEtiquetaHorizontal', 4, 2];
    aElem[aElem.length] = ['lblAltura', 'txtAltura', 4, 2];
    aElem[aElem.length] = ['lblLargura', 'txtLargura', 4, 2];
    aElem[aElem.length] = ['lblMargemSuperior', 'txtMargemSuperior', 4, 2];
    aElem[aElem.length] = ['lblMargemEsquerda', 'txtMargemEsquerda', 4, 2];
    //aElem[aElem.length] = ['lblFiltro3', 'txtFiltro3', 34, 4, -10];

    adjustElementsInForm(aElem, null, null, true);
    adjustElementsInForm([['lblFiltro3', 'txtFiltro3', 34, 3, -10]], null, null, true);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    // O estado do botao btnOK
    btnOK_Status();

    window.focus();
    if (ctl.disabled == false)
        ctl.focus();

    return true;
}

/********************************************************************
Usuario clicou um check box
********************************************************************/
function chkBox_Clicked() {
    ctl = this;

    // O estado do botao btnOK
    btnOK_Status();

    window.focus();
    if (ctl.disabled == false)
        ctl.focus();
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change() {
    // mostra o div correspondente ao relatorio
    showDivByReportID();

    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if (this.disabled == false)
        this.focus();

    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status() {
    var btnOKStatus = true;
    var i, elem;

    if (selReports.selectedIndex != -1) {
        if ((selReports.value == 40122) || (selReports.value == 40123) ||
             (selReports.value == 40125))
            btnOKStatus = false;
    }

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked() {
    if ((selReports.value == 40122) && (glb_sCaller == 'PL'))
        etiquetasProdutos();
    else if ((selReports.value == 40123) && (glb_sCaller == 'PL'))
        relatorioEstoque();
    else if ((selReports.value == 40125) && (glb_sCaller == 'PL'))
        relatorioPosicaoProdutos();
    else
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID() {
    var i, coll, attr, currRep;

    attr = null;
    currRep = 0;

    currRep = selReports.value;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (attr == currRep)
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr() {
    var i, coll, attr, retVal;

    attr = null;
    retVal = 0;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else {
            if (coll[i].style.visibility == 'visible') {
                retVal = attr;
                break;
            }
        }
    }
    return retVal;
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights() {
    if (selReports.options.length != 0)
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;

    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        attr = coll[i].getAttribute('report', 1);

        // nao faz nada se nao e div de relatorio
        if (attr == null)
            continue;
        else
            coll[i].style.visibility = 'hidden';
    }

    // desabilita o combo de relatorios
    selReports.disabled = true;

    // desabilita o botao OK
    btnOK.disabled = true;

    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[3];

    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 30);

    elem = document.getElementById('selReports');

    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);

    // a altura livre    
    modHeight -= topFree;

    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt';
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';

    // acrescenta o elemento
    window.document.body.appendChild(elem);

    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);

    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;

    return null;
}

/********************************************************************
Impressao de Etiquetas de Produtos (ID=40122)
********************************************************************/
function etiquetasProdutos() {
    var sMsgError = '';
    if (isNaN(parseInt(txtEtiquetaVertical.value)))
        sMsgError = 'O campo ' + lblEtiquetaVertical.innerText + ' n�o cont�m um n�mero v�lido.';
    else if (isNaN(parseInt(txtEtiquetaHorizontal.value)))
        sMsgError = 'O campo ' + lblEtiquetaHorizontal.innerText + ' n�o cont�m um n�mero v�lido.';
    else if (isNaN(parseInt(txtAltura.value)))
        sMsgError = 'O campo ' + lblAltura.innerText + ' n�o cont�m um n�mero v�lido.';
    else if (isNaN(parseInt(txtLargura.value)))
        sMsgError = 'O campo ' + lblLargura.innerText + ' n�o cont�m um n�mero v�lido.';
    else if (isNaN(parseInt(txtMargemSuperior.value)))
        sMsgError = 'O campo ' + lblMargemSuperior.innerText + ' n�o cont�m um n�mero v�lido.';
    else if (isNaN(parseInt(txtMargemEsquerda.value)))
        sMsgError = 'O campo ' + lblMargemEsquerda.innerText + ' n�o cont�m um n�mero v�lido.';

    if (sMsgError != '') {
        if (window.top.overflyGen.Alert(sMsgError) == 0)
            return null;
        lockControlsInModalWin(false);
        pb_StopProgressBar(true);
        return true;
    }

    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';

    sFiltro = trimStr(txtFiltro.value);
    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divEtiquetasProdutos.children.length) ; i++) {
        elem = divEtiquetasProdutos.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked)) {
                if (bFirst) {
                    bFirst = false;
                    sFiltroEstados = ' AND (a.EstadoID=' + elem.value + ' ';
                }
                else {
                    sFiltroEstados += ' OR a.EstadoID=' + elem.value + ' ';
                }
            }
        }
    }

    if (!bFirst)
        sFiltroEstados += ')';
    else
        sFiltroEstados += ' AND (a.EstadoID=0)';

    setConnection(dsoPrint01);

    dsoPrint01.SQL = 'SELECT COUNT(*) AS NumeroEtiquetas ' +
                     'FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                     'WHERE a.TiporelacaoID=61 AND a.SujeitoID= ' + glb_nEmpresaID + ' ' +
                     'AND a.ObjetoID=b.ConceitoID ' +
                      sFiltroEstados + ' ' + sFiltro;

    dsoPrint01.ondatasetcomplete = etiquetasProdutos_DSC;
    dsoPrint01.Refresh();
}

function etiquetasProdutos_DSC() {
    var nNumeroEtiquetas = 0;

    if (!dsoPrint01.recordset.EOF) {
        nNumeroEtiquetas = dsoPrint01.recordset['NumeroEtiquetas'].value;

        if (nNumeroEtiquetas == 0) {
            if (window.top.overflyGen.Alert('Nemhuma etiqueta a ser impressa') == 0)
                return null;

            lockControlsInModalWin(false);

            pb_StopProgressBar(true);
            return true;
        }
        else {
            var _retMsg = window.top.overflyGen.Confirm('Imprimir' + ' ' + nNumeroEtiquetas + ' etiquetas?');

            if (_retMsg == 0) {
                pb_StopProgressBar(true);
                return null;
            }
            else if (_retMsg == 1)
                __timerInt = window.setInterval('imprimeEtiquetasProdutos()', 50, 'JavaScript');
            else {
                pb_StopProgressBar(true);
                lockControlsInModalWin(false);
                return true;
            }
        }
    }
    else {
        if (window.top.overflyGen.Alert('Nemhuma etiqueta a ser impressa') == 0)
            return null;

        pb_StopProgressBar(true);
        lockControlsInModalWin(false);
        return true;
    }
}

function imprimeEtiquetasProdutos() {
    if (__timerInt != null) {
        window.clearInterval(__timerInt);
        __timerInt = null;
    }

    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';

    sFiltro = trimStr(txtFiltro.value);
    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divEtiquetasProdutos.children.length) ; i++) {
        elem = divEtiquetasProdutos.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked)) {
                if (bFirst) {
                    bFirst = false;
                    sFiltroEstados = ' AND (a.EstadoID=' + elem.value + ' ';
                }
                else {
                    sFiltroEstados += ' OR a.EstadoID=' + elem.value + ' ';
                }
            }
        }
    }

    if (!bFirst)
        sFiltroEstados += ')';
    else
        sFiltroEstados += ' AND (a.EstadoID=0)';

    setConnection(dsoPrint01);

    dsoPrint01.SQL = 'SELECT b.ConceitoID AS ProdutoID, b.Conceito as Produto ' +
                     'FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                     'WHERE a.TiporelacaoID=61 AND a.SujeitoID= ' + glb_nEmpresaID + ' ' +
                     'AND a.ObjetoID=b.ConceitoID ' +
                      sFiltroEstados + ' ' + sFiltro + ' ' +
                     'ORDER BY b.ConceitoID';

    dsoPrint01.ondatasetcomplete = imprimeEtiquetasProdutos_DSC;
    dsoPrint01.Refresh();
}

function imprimeEtiquetasProdutos_DSC() {
    var nEtiquetasVerticais;
    var nEtiquetasHorizontais;
    var nAltura;   // numero de linhas possiveis da etiqueta (cpp)
    var nLargura; // largura da etiqueta (mm)
    var nLinhas = 5;   // numero de linhas utilizaveis da etiqueta
    var nMargemSuperior;  // numero de linha a pular na primeira etiqueta
    var nMargemEsquerda;  // distancia da margem esquerda (mm)

    nEtiquetasVerticais = parseInt(txtEtiquetaVertical.value, 10);
    nEtiquetasHorizontais = parseInt(txtEtiquetaHorizontal.value, 10);
    nAltura = parseInt(txtAltura.value, 10);
    nLargura = parseInt(txtLargura.value, 10);
    nMargemSuperior = parseInt(txtMargemSuperior.value, 10);
    nMargemEsquerda = parseInt(txtMargemEsquerda.value, 10);

    var aCelula = new Array();
    var i, j, k, m, n;

    for (i = 0; i <= nLinhas; i++)
        aCelula[i] = new Array();

    // seta os parametros de inicializacao da impressora
    oPrinter.DefaultPrinterNFParams(6, 70, 12, 'C', false);
    oPrinter.ParallelPort = 1;
    oPrinter.ResetPrinter();
    oPrinter.ResetTextForPrint();

    dsoPrint01.recordset.MoveFirst;

    while (!dsoPrint01.recordset.EOF) {
        oPrinter.MoveToNewLine(nMargemSuperior);
        j = 0;
        while ((!dsoPrint01.recordset.EOF) && (j < nEtiquetasVerticais)) {
            k = 0;
            while ((!dsoPrint01.recordset.EOF) && (k < nEtiquetasHorizontais)) {
                aCelula[0][k] = dsoPrint01.recordset['ProdutoID'].value;
                aCelula[1][k] = '';
                aCelula[2][k] = dsoPrint01.recordset['Produto'].value;
                dsoPrint01.recordset.MoveNext();
                k++;
            }

            // Impressao dos Dados
            for (m = 0; m < nLinhas; m++) {
                for (n = 0; n < k; n++) {
                    oPrinter.SendTextForPrint(nMargemEsquerda + n * nLargura, aCelula[m][n], (m == 0 ? 'E' : 'N'), (m == 0));
                }
                oPrinter.MoveToNewLine(1);
            }
            oPrinter.MoveToNewLine(nAltura - m);
            j++;
        }
        oPrinter.FormFeed();
    }

    pb_StopProgressBar(true);
    oPrinter.PrintText();
}

/********************************************************************
Impressao do relatorio de RelatorioEstoque (ID=40123)
********************************************************************/
function relatorioEstoque() {
    glb_DepositoSelecionado = (selDeposito.selectedIndex == 0 ? 'NULL' : selDeposito.value);

    // Generico
    if (selPadrao.value == 701)
        relatorioEstoqueGenerico();
        // Intel		
    else if (selPadrao.value == 702)
        relatorioEstoqueIntel();
    else if (selPadrao.value == 703)
        relatorioEstoqueMicrosoft();
    else if (selPadrao.value == 709)
        relatorioEstoqueSeagate();
    else if (selPadrao.value == 710)
        relatorioEstoqueKingston();
    else if (selPadrao.value == 951)
        relatorioEstoque3com();
    else if (selPadrao.value == 952)
        relatorioEstoqueNVidia();
    else if (selPadrao.value == 953)
        relatorioEstoqueHP();
        //Relat�rio de SPED - FRG 28/01/2015.    
    else if (selPadrao.value == 716)
        relatorioSPED();

}

function relatorioEstoqueGenerico() {
    var dirA1;
    var dirA2;
    var i;
    var dateTimeInSQLFormat = null;
    var dataCustoEstoque;
    var TipoCustoEstoque;

    var _selEmpresa = '';
    var _selFamilias = '';
    var _selMarca = '';
    var _elemValue1 = '';


    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataEstoque)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(' + '\'' + dateTimeInSQLFormat + '\'' + ', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    var sFiltroFabricante = (selFabricante.value == 0 ? '' : ' AND Produtos.FabricanteID=' + selFabricante.value);

    var sFiltroEmpresa = '';
    var nItensSelected = 0;
    var sFiltroProprietario = '';

    if (selProprietarios2.selectedIndex > 0)
        sFiltroProprietario = ' AND ProdutosEmpresa.ProprietarioID = ' + selProprietarios2.value + ' ';

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true) {
            nItensSelected++;
            sFiltroEmpresa += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'ProdutosEmpresa.SujeitoID=' + selEmpresa.options[i].value;

            _selEmpresa += (nItensSelected == 1 ? selEmpresa.options[i].value : ':' + selEmpresa.options[i].value);
        }
    }

    sFiltroEmpresa += (nItensSelected > 0 ? ')' : '');

    if (sFiltroEmpresa == '') {
        if (window.top.overflyGen.Alert('Selecione uma empresa') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    var sFiltroFamilia = '';
    nItensSelected = 0;

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');


    var sFiltroMarca = '';
    nItensSelected = 0;

    for (i = 0; i < selMarca.length; i++) {
        if (selMarca.options[i].selected == true) {
            nItensSelected++;
            sFiltroMarca += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.MarcaID=' + selMarca.options[i].value;

            _selMarca += (nItensSelected == 1 ? selMarca.options[i].value : ':' + selMarca.options[i].value);
        }
    }

    sFiltroMarca += (nItensSelected > 0 ? ')' : '');

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Generico)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word
    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';
    var sEstadosCheckeds = '';
    var Count = 0;
    var bAging = false;

    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                 (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                 (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                 (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                 (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
                 (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                 (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                 (elem.id.toUpperCase() != 'CHKEXCEL') &&
                 (elem.id.indexOf('_ESTOQUE_') < 0)) {

                Count++;

                if (elem.value == -1)
                    bAging = true;
                else
                {

                    if (bFirst) {
                        bFirst = false;

                        //sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                        sFiltroEstados = ' AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                        sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;

                        _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                    }
                    else {
                        // sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                        sFiltroEstados += ' OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                        sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;

                        _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                    }
                }
            }
        }
    }

    if (!bFirst)
        sFiltroEstados += ')';
    else
        sFiltroEstados += ' AND (ProdutosEmpresa.EstadoID=0)';

    if (bAging) {
        _elemValue1 += ":-1";
        sFiltroEstados += '  AND (dbo.fn_Produto_Aging(ProdutosEmpresa.RelacaoID, NULL) = 1) ';
    }
    var aCmbsEstoques =
        [chk1_ESTOQUE_EPO,
         chk1_ESTOQUE_EPE,
         chk1_ESTOQUE_EPD,
         chk1_ESTOQUE_EPC,
         chk1_ESTOQUE_EPT,
         chk1_ESTOQUE_ETO,
         chk1_ESTOQUE_ETE,
         chk1_ESTOQUE_ETD,
         chk1_ESTOQUE_ETC,
         chk1_ESTOQUE_EB,
         chk1_ESTOQUE_F,
         chk1_ESTOQUE_Disp,
         chk1_ESTOQUE_RC,
         chk1_ESTOQUE_RCC,
         chk1_ESTOQUE_RV,
         chk1_ESTOQUE_RVC];

    var sSQLMasterSelect, sSQLMasterFrom, sSQLMasterWhere1, sSQLMasterWhere2, sSQLMasterGroupBy, sSQLMasterOrderBy;

    sSQLMasterSelect = 'SELECT DISTINCT ' +
            'Conceitos2.Conceito AS ConceitoAbstrato2, ' +
            'Conceitos2.ConceitoID AS _Conceito2ID, NULL AS Gap1, NULL AS Gap2 ';

    var bLocalizacao = true;

    var chkCmbEstoques = '';

    for (i = 0; i < aCmbsEstoques.length; i++) {
        if (aCmbsEstoques[i].checked) {
            bLocalizacao = false;

            chkCmbEstoques += (i == 0 ? 'checked' : ':' + 'checked');
        }
        else {
            chkCmbEstoques += (i == 0 ? 'unchecked' : ':' + 'unchecked');
        }
    }

    sSQLMasterFrom = 'FROM ' +
            'Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), ' +
            'RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), ' +
            'RelacoesPesCon ProdutosEmpresa  WITH(NOLOCK) ';

    sSQLMasterWhere1 =
        'WHERE (' +
            '(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND ' +
            '(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND ' +
            'RelConceitos1.EstadoID=2 AND RelConceitos1.SujeitoID=Conceitos2.ConceitoID) AND ' +
            '(Conceitos2.TipoConceitoID=301 AND Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND ' +
            '(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND ' +
            'RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND ' +
            '(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND ';

    sSQLMasterWhere2 =
            'Conceitos3.TipoConceitoID=302 AND Conceitos3.EstadoID=2) AND ' +
            '(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND ' +
            '(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ' +
            'ProdutosEmpresa.TipoRelacaoID=61) AND ' +
            '(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID = Produtos.MarcaID) ';

    if (chkProdutoSeparavel.checked)
        sSQLMasterWhere2 += ' AND Produtos.ProdutoSeparavel = 1 ';

    if (chkReportaFabricante.checked)
        sSQLMasterWhere2 += ' AND ProdutosEmpresa.ReportaFabricante = 1 ';

    sSQLMasterWhere2 += sFiltroFabricante + sFiltroEmpresa + sFiltroProprietario + sFiltroFamilia + sFiltroMarca + sFiltroEstados + ' ' + sFiltro + ') ';

    sSQLMasterGroupBy = 'GROUP BY Conceitos2.Conceito, Conceitos2.ConceitoID ';

    sSQLMasterOrderBy = 'ORDER BY Conceitos2.Conceito, Conceitos2.ConceitoID';

    var aSQLQuantEstoquesDetail =
        ['dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,341,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPO ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,342,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPE ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,343,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPD ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,344,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPC ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,345,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPT ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,346,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS ETO ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,347,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS ETE ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,348,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS ETD ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,349,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS ETC ',
         'dbo.fn_Produto_EstoqueBrinde(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,4) AS EB ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,341,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS F__ ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,356,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS Dis ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,351,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS RC_ ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,352,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS RCC ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,353,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS RV_ ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,354,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS RVC '];
    var aDynamicEstoque =
        ['EPO Numeric(10),',
         'EPE Numeric(10),',
         'EPD Numeric(10),',
         'EPC Numeric(10),',
         'EPT Numeric(10),',
         'ETO Numeric(10),',
         'ETE Numeric(10),',
         'ETD Numeric(10),',
         'ETC Numeric(10),',
         'EB_ Numeric(10),',
         'F__ Numeric(10),',
         'Dis Numeric(10),',
         'RC_ Numeric(10),',
         'RCC Numeric(10),',
         'RV_ Numeric(10),',
         'RVC Numeric(10),'];

    var sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailOrderBy;

    sSQLDetailSelect = 'SELECT DISTINCT ' +
            'Produtos.ConceitoID AS ID, Estados.RecursoAbreviado AS Est, ' +
            'Produtos.Conceito AS Produto, CONVERT(VARCHAR(126),dbo.fn_Produto_Descricao2(Produtos.ConceitoID, NULL, 11)) AS Descricao, Conceitos2.ConceitoID AS _Conceito2ID ';

    for (i = 0; i < aCmbsEstoques.length; i++)
        if (aCmbsEstoques[i].checked)
            sSQLDetailSelect += ', ' + aSQLQuantEstoquesDetail[i];

    // 'ProdutosEmpresa.CustoMedio * dbo.fn_Preco_Cotacao(ProdutosEmpresa.MoedaEntradaID, ' + selMoedas.value + ', NULL, GETDATE()) AS Custo, ' +

    //Alterado para pegar estoque na data.
    if ((dateTimeInSQLFormat != 'NULL') && (chkCustoContabil.checked)) {
        TipoCustoEstoque = 2;
        dataCustoEstoque = dateTimeInSQLFormat;
    }
    else if ((dateTimeInSQLFormat == 'NULL') && (chkCustoContabil.checked)) {
        TipoCustoEstoque = 1;
        dataCustoEstoque = 'GETDATE()';
    }
    else if ((dateTimeInSQLFormat != 'NULL') && (!chkCustoContabil.checked)) {
        TipoCustoEstoque = 4;
        dataCustoEstoque = dateTimeInSQLFormat;
    }
    else if ((dateTimeInSQLFormat == 'NULL') && (!chkCustoContabil.checked)) {
        TipoCustoEstoque = 3;
        dataCustoEstoque = 'GETDATE()';
    }

    if (bLocalizacao)
        sSQLDetailSelect += ', dbo.fn_Produto_Localizacoes(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, NULL, 10, 1) AS Localizacao ';
    else if (chkValoracao.checked)
        sSQLDetailSelect += ', 0 AS Quant, ' +
            'dbo.fn_Produto_Custo(ProdutosEmpresa.RelacaoID, ' + dataCustoEstoque + ', ' + selMoedas.value + ', NULL, ' + TipoCustoEstoque + ') AS Custo, ' +
            '0 AS TotalCusto, ' +
            'dbo.fn_Preco_Preco(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,NULL,NULL,NULL,NULL,' + selMoedas.value + ',GetDate(),NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL) AS Preco, ' +
            '0 AS TotalPreco ';
    else
        sSQLDetailSelect += ', 0 AS Quant ';

    sSQLDetailSelect += ', ProdutosEmpresa.CodigoAnterior, Familias.Conceito AS Familia, ' +
		'Fabricantes.Fantasia AS Fabricante, Marcas.Conceito AS Marca, Produtos.Modelo, Produtos.PartNumber, Produtos.Serie ';

    if (dateTimeInSQLFormat != 'NULL')
        sSQLDetailSelect += ', ' + dateTimeInSQLFormat + ' AS Data ';
    else
        sSQLDetailSelect += ', GETDATE() AS Data ';

    sSQLDetailSelect += ', Empresas.PessoaID AS EmpresaID, Empresas.Fantasia AS Empresa, ' +
		'Proprietarios.Fantasia AS Proprietario, 1 AS _Registro, ProdutosEmpresa.Observacao, ' +
		'CONVERT(VARCHAR(10), ProdutosEmpresa.dtControle, ' + DATE_SQL_PARAM + ') AS dtControle, ' +
		'dbo.fn_Produto_PesosMedidas(ProdutosEmpresa.ObjetoID, 1, 5) AS Cubagem, ' +
	    'dbo.fn_Produto_PesosMedidas(ProdutosEmpresa.ObjetoID, 1, 1) AS Peso, ' +
	    'dbo.fn_Produto_TransfereFilial(Produtos.ConceitoID,Empresas.PessoaID)'; //Inclus�o de nova coluna no relat�rio - FSM 05/03/2012

    sSQLDetailFrom = 'FROM ' +
            'Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), ' +
            'RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), ' +
            'RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Recursos Estados WITH(NOLOCK), Recursos Sistema WITH(NOLOCK), Pessoas Fabricantes WITH(NOLOCK), Pessoas Empresas WITH(NOLOCK), Pessoas Proprietarios WITH(NOLOCK) ';

    sSQLDetailWhere1 = 'WHERE (' +
            '(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND ' +
            '(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND ' +
            'RelConceitos1.EstadoID=2) AND ' +
            '(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND ' +
            'Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND ' +
            '(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND ' +
            'RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND ' +
            '(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ';

    sSQLDetailWhere2 =
            'Conceitos3.EstadoID=2) AND ' +
            '(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND ' +
            '(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
            'ProdutosEmpresa.EstadoID=Estados.RecursoID AND Sistema.RecursoID=999) AND ' +
            '(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) AND ' +
            '(Fabricantes.PessoaID=Produtos.FabricanteID) AND ' +
            '(Empresas.PessoaID=ProdutosEmpresa.SujeitoID) AND ' +
            '(Proprietarios.PessoaID=ProdutosEmpresa.ProprietarioID)';

    if (chkProdutoSeparavel.checked)
        sSQLDetailWhere2 += ' AND Produtos.ProdutoSeparavel = 1 ';

    if (chkReportaFabricante.checked)
        sSQLDetailWhere2 += ' AND ProdutosEmpresa.ReportaFabricante = 1 ';

    sSQLDetailWhere2 += sFiltroFabricante + sFiltroEmpresa + sFiltroProprietario + sFiltroFamilia + sFiltroMarca + sFiltroEstados + ' ' + sFiltro + ') ';

    sSQLDetailOrderBy = 'ORDER BY Produtos.ConceitoID, Produtos.Conceito, Conceitos2.ConceitoID ';

    sTmpTableSelect = 'CREATE TABLE #TempTable ' +
	    '( ' +
		'ID NUMERIC(10),' +
		'Est VARCHAR(1),' +
		'Produto VARCHAR(40),' +
		'Descricao VARCHAR(126), ' +
		'_Conceito2ID NUMERIC(10),';

    for (i = 0; i < aCmbsEstoques.length; i++)
        if (aCmbsEstoques[i].checked)
            sTmpTableSelect += aDynamicEstoque[i];

    if (bLocalizacao) {
        sTmpTableSelect += 'Localizacao VARCHAR(200),';
    }
    else if (chkValoracao.checked) {
        sTmpTableSelect += 'Quant NUMERIC(10), ' +
	        'Custo NUMERIC(11,2), ' +
	        'TotalCusto NUMERIC(12,2), ' +
	        'Preco NUMERIC(11,2), ' +
	        'TotalPreco NUMERIC(12,2),';
    }
    else
        sTmpTableSelect += 'Quant NUMERIC(10),';

    sTmpTableSelect += 'CodigoAnterior VARCHAR(10),' +
					   'Familia VARCHAR(25),' +
					   'Fabricante VARCHAR(20),' +
					   'Marca VARCHAR(25),' +
                       'Modelo VARCHAR(18),' +
                       'PartNumber VARCHAR(20),' +
                       'Serie VARCHAR(40),' +
                       'Data DATETIME,' +
                       'EmpresaID NUMERIC(10),' +
                       'Empresa VARCHAR(20),' +
                       'Proprietario VARCHAR(20),' +
                       '_Registro INTEGER, ' +
                       'Observacao VARCHAR(25), ' +
                       'dtControle VARCHAR(10), ' +
                       'Cubagem NUMERIC(13,8), ' +
                       'Peso NUMERIC(11, 2), ' +
                       'Transferivel BIT)'; //Inclus�o de nova coluna no relat�rio - FSM 05/03/2012

    sTmpTableSelect += 'INSERT INTO #TempTable ' + sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 +
		sSQLDetailWhere2 + sSQLDetailOrderBy + ' ';

    if (!bLocalizacao) {
        sTmpTableSelect += 'UPDATE #TempTable SET Quant = ( ';

        var nQtdEstoquesCheckeds = 0;
        var isFirst = true;
        for (i = 0; i < aCmbsEstoques.length; i++)
            if (aCmbsEstoques[i].checked) {
                sTmpTableSelect += (isFirst ? '' : '+') + (aDynamicEstoque[i]).substr(0, 3);
                isFirst = false;
                nQtdEstoquesCheckeds++;
            }

        if (!isFirst)
            sTmpTableSelect += ') ';
    }

    sTmpTableSelect += ' UPDATE #TempTable SET Cubagem = Cubagem * Quant, Peso = Peso * Quant ';

    if (chkValoracao.checked)
        sTmpTableSelect += ' UPDATE #TempTable SET TotalCusto = Quant * Custo, TotalPreco = Quant * Preco ';

    sTmpTableSelect += 'SELECT * FROM #TempTable ';
    if (chkTemEstoque.checked)
        sTmpTableSelect += 'WHERE (Quant <> 0) ';

    sTmpTableSelect += 'DROP TABLE #TempTable';

    var strSQLParsed = 'EXEC sp_RelatorioEstoque ' + '\'' + dupCharInString(sTmpTableSelect, '\'') + '\'';

    strSQLParsed = (strSQLParsed == null ? '' : strSQLParsed);

    var lUseBold = true;
    var nFontTitleSize = 8;
    var lUseBoldSubDetail = false;
    var nLeft = 0;
    var nTopHeader2 = 0;
    var sParams = '';

    if (sFiltroProprietario != '')
        sParams += translateTerm('Propriet�rio', null) + ':' + selProprietarios2.options(selProprietarios2.selectedIndex).innerText + '   ';

    if (sEstadosCheckeds != '')
        sParams += sEstadosCheckeds + '   ';

    if (trimStr(txtDataEstoque.value) != '')
        sParams += translateTerm('Data', null) + ':' + txtDataEstoque.value + '   ';

    if (selDeposito.selectedIndex > 0)
        sParams += 'Dep�sito: ' + selDeposito.options[selDeposito.selectedIndex].innerText + '   ';

    if (chkValoracao.checked)
        sParams += translateTerm('Moeda', null) + ':' + selMoedas.options[selMoedas.selectedIndex].innerText;

    if (sParams != '') {
        nTopHeader2 = 14;
    }

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    var _chkProdutoSeparavel = '';
    var _chkReportaFabricante = '';
    var _chkCustoContabil = '';
    var _chkValoracao = '';
    var _chkTemEstoque = '';
    var _chkExcel = '';

    var _chk1_ESTOQUE_0 = '';
    var _chk1_ESTOQUE_1 = '';
    var _chk1_ESTOQUE_2 = '';
    var _chk1_ESTOQUE_3 = '';
    var _chk1_ESTOQUE_4 = '';
    var _chk1_ESTOQUE_5 = '';
    var _chk1_ESTOQUE_6 = '';
    var _chk1_ESTOQUE_7 = '';
    var _chk1_ESTOQUE_8 = '';
    var _chk1_ESTOQUE_9 = '';
    var _chk1_ESTOQUE_10 = '';
    var _chk1_ESTOQUE_11 = '';
    var _chk1_ESTOQUE_12 = '';
    var _chk1_ESTOQUE_13 = '';
    var _chk1_ESTOQUE_14 = '';
    var _chk1_ESTOQUE_15 = '';

    var _selMoedas = '';
    var _DATE_SQL_PARAM = DATE_SQL_PARAM;
    var _nQtdEstoquesCheckeds = nQtdEstoquesCheckeds;
    var _glb_sEmpresaFantasia = glb_sEmpresaFantasia;

    if (chkProdutoSeparavel.checked)
        _chkProdutoSeparavel = 'checked';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    if (chkCustoContabil.checked)
        _chkCustoContabil = 'checked';

    if (chkValoracao.checked)
        _chkValoracao = 'checked';

    if (chkTemEstoque.checked)
        _chkTemEstoque = 'checked';

    if (chkExcel.checked)
        _chkExcel = 'checked';



    if (aCmbsEstoques[0].checked)
        _chk1_ESTOQUE_0 = 'checked';

    if (aCmbsEstoques[1].checked)
        _chk1_ESTOQUE_1 = 'checked';

    if (aCmbsEstoques[2].checked)
        _chk1_ESTOQUE_2 = 'checked';

    if (aCmbsEstoques[3].checked)
        _chk1_ESTOQUE_3 = 'checked';

    if (aCmbsEstoques[4].checked)
        _chk1_ESTOQUE_4 = 'checked';

    if (aCmbsEstoques[5].checked)
        _chk1_ESTOQUE_5 = 'checked';

    if (aCmbsEstoques[6].checked)
        _chk1_ESTOQUE_6 = 'checked';

    if (aCmbsEstoques[7].checked)
        _chk1_ESTOQUE_7 = 'checked';

    if (aCmbsEstoques[8].checked)
        _chk1_ESTOQUE_8 = 'checked';

    if (aCmbsEstoques[9].checked)
        _chk1_ESTOQUE_9 = 'checked';

    if (aCmbsEstoques[10].checked)
        _chk1_ESTOQUE_10 = 'checked';

    if (aCmbsEstoques[11].checked)
        _chk1_ESTOQUE_11 = 'checked';

    if (aCmbsEstoques[12].checked)
        _chk1_ESTOQUE_12 = 'checked';

    if (aCmbsEstoques[13].checked)
        _chk1_ESTOQUE_13 = 'checked';

    if (aCmbsEstoques[14].checked)
        _chk1_ESTOQUE_14 = 'checked';

    if (aCmbsEstoques[15].checked)
        _chk1_ESTOQUE_15 = 'checked';



    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFabricante=' + selFabricante.value +
                    '&selProprietarios2Index=' + selProprietarios2.selectedIndex + '&selProprietarios2Value=' + selProprietarios2.value +
                    '&selEmpresa=' + _selEmpresa + '&selFamilias=' + _selFamilias + '&selMarca=' + _selMarca + '&sFiltro=' + sFiltro + '&chkCmbEstoques=' + chkCmbEstoques +
                    '&chkProdutoSeparavel=' + _chkProdutoSeparavel + '&chkReportaFabricante=' + _chkReportaFabricante + '&glb_DepositoSelecionado=' + glb_DepositoSelecionado +
                    '&chkCustoContabil=' + _chkCustoContabil + '&chkValoracao=' + _chkValoracao + '&chkTemEstoque=' + _chkTemEstoque + '&chkExcel=' + _chkExcel + '&sParams=' + sParams +
                    '&selMoedas=' + selMoedas.value + '&DATE_SQL_PARAM=' + DATE_SQL_PARAM + '&nQtdEstoquesCheckeds=' + _nQtdEstoquesCheckeds + '&glb_sEmpresaFantasia=' + _glb_sEmpresaFantasia +
                    '&elem=' + _elemValue1 + '&aCmbsEstoques_0=' + _chk1_ESTOQUE_0 + '&aCmbsEstoques_1=' + _chk1_ESTOQUE_1 + '&aCmbsEstoques_2=' + _chk1_ESTOQUE_2 +
                    '&aCmbsEstoques_3=' + _chk1_ESTOQUE_3 + '&aCmbsEstoques_4=' + _chk1_ESTOQUE_4 + '&aCmbsEstoques_5=' + _chk1_ESTOQUE_5 + '&aCmbsEstoques_6=' + _chk1_ESTOQUE_6 +
                    '&aCmbsEstoques_7=' + _chk1_ESTOQUE_7 + '&aCmbsEstoques_8=' + _chk1_ESTOQUE_8 + '&aCmbsEstoques_9=' + _chk1_ESTOQUE_9 + '&aCmbsEstoques_10=' + _chk1_ESTOQUE_10 +
                    '&aCmbsEstoques_11=' + _chk1_ESTOQUE_11 + '&aCmbsEstoques_12=' + _chk1_ESTOQUE_12 + '&aCmbsEstoques_13=' + _chk1_ESTOQUE_13 + '&aCmbsEstoques_14=' + _chk1_ESTOQUE_14 +
                    '&aCmbsEstoques_15=' + _chk1_ESTOQUE_15 + "&Padrao=Generico";;

    var frameReport = document.getElementById("frmReport");

    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;

    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/Estoque.aspx?' + strParameters;
}

//function reports_onreadystatechange() 
//{
//    if ((document.readyState == 'loaded') ||
//    (document.readyState == 'interactive') ||
//    (document.readyState == 'complete')) 
//    {
//        //winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
//        pb_StopProgressBar(true);
//        lockControlsInModalWin(false);
//    }
//}

function reports_onreadystatechange() {

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
    (frameReport.contentWindow.document.readyState == 'interactive') ||
    (frameReport.contentWindow.document.readyState == 'complete')) {
        //winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
        //        pb_StopProgressBar(true);
        //        lockControlsInModalWin(false);

        //        var strParameters = "DATE_SQL_PARAM=" + DATE_SQL_PARAM + "&glb_nNotaFiscalID=" + glb_nNotaFiscalID +
        //                        "&nEmpresaData1=" + nEmpresaData[7] + "&nEmpresaData2=" + nEmpresaData[8] +
        //                        "&bCustomsWarehouse=" + bCustomsWarehouse + "&via2=true";

        //        if (glbContRel == 1) {

        //            glbContRel = 2;

        //            window.document.location = SYS_PAGESURLROOT + '/PrintJet/Invoice.aspx?' + strParameters + "&via2=true";

        //           
        //        }

        //        window.print();

        //        lockControlsInModalWin(false);

        pb_StopProgressBar(true);
        lockControlsInModalWin(false);

    }
}

function relatorioEstoqueIntel() {
    var dirA1;
    var dirA2;
    var i;

    var _selEmpresa = '';
    var _selFamilias = '';
    var _selMarca = '';
    var _elemValue1 = '';
    var Count = 0;
    var chkCmbEstoques = '';

    var dateTimeInSQLFormat = null;

    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataEstoque)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(' + '\'' + dateTimeInSQLFormat + '\'' + ', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';


    var sFiltroFabricante = (selFabricante.value == 0 ? '' : ' AND Produtos.FabricanteID =' + selFabricante.value);

    var sFiltroFamilia = '';
    var nItensSelected = 0;
    var sFiltroProprietario = '';

    if (selProprietarios2.selectedIndex > 0)
        sFiltroProprietario = ' AND ProdutosEmpresa.ProprietarioID = ' + selProprietarios2.value + ' ';

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Intel)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word


    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';
    var sEstadosCheckeds = '';


    var sClausulaIN = '';

    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                 (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                 (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                 (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                 (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
                 (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                 (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                 (elem.id.toUpperCase() != 'CHKEXCEL') &&
                 (elem.id.indexOf('_ESTOQUE_') < 0)) {
                if (i == 1)
                    sClausulaIN = elem.value;  //+ '', '';
                else
                    sClausulaIN += ', ' + elem.value;  //+ '', '';

                Count++;

                if (bFirst) {
                    bFirst = false;


                    //Old 
                    //sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    // sFiltroEstados = ' AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' +  dateTimeInSQLFormat +')= ' + elem.value + ' ';
                    // sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;


                    sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
                else {

                    //Old
                    // sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    //sFiltroEstados += ' OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    //sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;                   

                    sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);

                }
            }
        }
    }

    //Old
    /* if (!bFirst)
         sFiltroEstados += ')';
     else */
    if (bFirst)
        sFiltroEstados += ' AND (ProdutosEmpresa.EstadoID=0)';

    var aCmbsEstoques =
        [chk1_ESTOQUE_EPO,
         chk1_ESTOQUE_EPE,
         chk1_ESTOQUE_EPD,
         chk1_ESTOQUE_EPC,
         chk1_ESTOQUE_EPT,
         chk1_ESTOQUE_ETO,
         chk1_ESTOQUE_ETE,
         chk1_ESTOQUE_ETD,
         chk1_ESTOQUE_ETC];

    var aSQLQuantEstoquesDetail =
        ['dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349,Depositos.ObjetoID,' + dateTimeInSQLFormat + ',NULL,375, -1) '];

    var aDynamicEstoque =
        ['_EPO Numeric(10),',
         '_EPE Numeric(10),',
         '_EPD Numeric(10),',
         '_EPC Numeric(10),',
         '_EPT Numeric(10),',
         '_ETO Numeric(10),',
         '_ETE Numeric(10),',
         '_ETD Numeric(10),',
         '_ETC Numeric(10),'];

    var sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

    sSQLDetailSelect = 'SELECT DISTINCT ProdutosEmpresa.SujeitoID AS EmpresaID, ' +
		'CONVERT(VARCHAR(10), dbo.fn_Data_Zero(' + dateTimeInSQLFormat + '), 112) AS [A01], Produtos.ConceitoID AS [A03] ';

    var bHasEstoqueSelected = false;
    var strSumEstoques = '';
    for (i = 0; i < aCmbsEstoques.length; i++) {
        if (aCmbsEstoques[i].checked) {
            if (!bHasEstoqueSelected)
                strSumEstoques += ', ';
            else
                strSumEstoques += ' + ';

            strSumEstoques += aSQLQuantEstoquesDetail[i];
            bHasEstoqueSelected = true;

            chkCmbEstoques += (i == 0 ? 'checked' : ':' + 'checked');
        }
        else {
            chkCmbEstoques += (i == 0 ? 'unchecked' : ':' + 'unchecked');
        }
    }

    var sA04 = ', 0 AS [A04]';
    var sA041 = ', 0 AS [A041]';

    sA04 = strSumEstoques + ' AS [A04] ';

    sA041 = ', ISNULL((SELECT SUM(Itens.Quantidade) FROM Pedidos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK), Operacoes Transacoes WITH(NOLOCK) ' +
		'WHERE (Pedidos.EmpresaID = ProdutosEmpresa.SujeitoID AND Pedidos.PedidoID=Itens.PedidoID AND ' +
		'Pedidos.TransacaoID=Transacoes.OperacaoID AND Transacoes.MetodoCustoID=372 AND Pedidos.DepositoID = Depositos.ObjetoID AND ' +
		'Pedidos.EstadoID BETWEEN 23 AND 28 AND Itens.ProdutoID=Produtos.ConceitoID)), 0) AS [A041],' +


		'dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, dbo.fn_Data_Fuso(' + dateTimeInSQLFormat + ', NULL,16659)) AS [RelacaoEstadoID]';

    sSQLDetailSelect += sA04 + sA041 + ', Produtos.Modelo AS [A02], ' +
		'(CASE WHEN Depositos.ObjetoID = Depositos.SujeitoID THEN ' +
	        '(SELECT TOP 1 a.Codigo ' +
		        'FROM RelacoesPessoas a WITH(NOLOCK) ' +
		        'WHERE (a.TipoRelacaoID = 21 AND a.SujeitoID = ProdutosEmpresa.SujeitoID AND a.ObjetoID = 10002)) ' +
	        'ELSE (SELECT TOP 1 CodFab.Codigo ' +
			        'FROM RelacoesPessoas_CodigosFabricante CodFab WITH(NOLOCK) ' +
			'WHERE CodFab.RelacaoID = Depositos.RelacaoID AND CodFab.FabricanteID = 10002) END) AS [A06] ';

    sSQLDetailFrom = 'FROM Conceitos Produtos WITH(NOLOCK) ' +
		                'INNER JOIN Conceitos Familias WITH(NOLOCK) ON Familias.ConceitoID = Produtos.ProdutoID ' +
		                'INNER JOIN Conceitos Marcas WITH(NOLOCK) ON Marcas.ConceitoID=Produtos.MarcaID ' +
		                'INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON Produtos.ConceitoID = ProdutosEmpresa.ObjetoID ' +
		                'INNER JOIN (SELECT RelPesDeposito.RelacaoID, RelPesDeposito.SujeitoID, RelPesDeposito.ObjetoID ' +
						                'FROM RelacoesPessoas RelPesDeposito WITH(NOLOCK) ' +
						                'WHERE TipoRelacaoID = 28 AND EstadoID = 2' +
					                    'UNION ALL ' +
					                'SELECT NULL AS RelacaoID, EmpSistema.SujeitoID AS SujeitoID, EmpSistema.SujeitoID AS ObjetoID ' +
						                'FROM RelacoesPesRec EmpSistema WITH(NOLOCK) ' +
						                'WHERE (EmpSistema.TipoRelacaoID = 12  ' +
									        'AND EmpSistema.ObjetoID = 999  ' +
									        'AND EmpSistema.EstadoID = 2)) Depositos ON ProdutosEmpresa.SujeitoID = Depositos.SujeitoID ';

    sSQLDetailWhere1 = 'WHERE Produtos.TipoConceitoID=303 AND Marcas.TipoConceitoID = 304 AND Familias.TipoConceitoID = 302 ' +
            ' AND ProdutosEmpresa.TipoRelacaoID=61 ';

    if (glb_nEmpresaID == 3)
        sSQLDetailWhere1 += ' AND ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' ';
    else
        sSQLDetailWhere1 += ' AND ProdutosEmpresa.SujeitoID <> 3 ';

    if (chkProdutoSeparavel.checked)
        sSQLDetailWhere1 += ' AND Produtos.ProdutoSeparavel = 1 ';

    if (chkReportaFabricante.checked)
        sSQLDetailWhere1 += ' AND ProdutosEmpresa.ReportaFabricante = 1 ';

    if (glb_DepositoSelecionado != 'NULL')
        sSQLDetailWhere1 += ' AND Depositos.ObjetoID = ' + glb_DepositoSelecionado + ' ';

    sSQLDetailWhere1 += sFiltroProprietario + sFiltroFabricante + sFiltroFamilia + sFiltroEstados + ' ' + sFiltro + ' ';

    sSQLDetailGroupBy = ' GROUP BY ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, Produtos.Modelo, ' +
                        ' Depositos.SujeitoID, Depositos.ObjetoID, ProdutosEmpresa.RelacaoID, Depositos.RelacaoID ';



    sSQLDetailOrderBy = 'ORDER BY EmpresaID, A03, A02 ';

    sTmpTableSelect =
        'CREATE TABLE #TempTable ' +
	    '( ' +
	    '_IDTMP int IDENTITY,' +
		'EmpresaID NUMERIC(10),' +
		'A01 VARCHAR(10),' +
		'A03 NUMERIC(10),' +
		'A04 NUMERIC(10),' +
		'A041 NUMERIC(10),' +
		'RelacaoEstadoID NUMERIC(10),' +
		'A02 VARCHAR(18), ' +
		'A06 VARCHAR(10)) ';


    sTmpTableSelect += 'INSERT INTO #TempTable (EmpresaID, A01, A03, A04, A041, RelacaoEstadoID, A02, A06) ' +
		sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailGroupBy + sSQLDetailOrderBy + ' ';

    if (chkTemEstoque.checked)
        sTmpTableSelect += 'SELECT _IDTMP, ' +
        'EmpresaID,' +
		'A01,' +
		'A03,' +
		'A04,' +
		'A041,' +
		'A02, ' +
		'A06 FROM #TempTable WHERE [A04] > 0  AND RelacaoEstadoID IN (' + sClausulaIN + ')';
    else
        sTmpTableSelect += 'SELECT _IDTMP, ' +
        'EmpresaID, ' +
        'A01,' +
        'A03,' +
        'A04,' +
        'A041,' +
        'A02, ' +
        'A06 FROM #TempTable WHERE RelacaoEstadoID IN (' + sClausulaIN + ')';



    sTmpTableSelect += 'DROP TABLE #TempTable';

    var strSQLParsed = 'EXEC sp_RelatorioEstoque ' + '\'' + dupCharInString(sTmpTableSelect, '\'') + '\'';

    strSQLParsed = (strSQLParsed == null ? '' : strSQLParsed);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    var _chkProdutoSeparavel = '';
    var _chkReportaFabricante = '';
    var _chkTemEstoque = '';

    if (chkProdutoSeparavel.checked)
        _chkProdutoSeparavel = 'checked';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    if (chkTemEstoque.checked)
        _chkTemEstoque = 'checked';

    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFabricante=' + selFabricante.value +
                    '&selProprietarios2Index=' + selProprietarios2.selectedIndex + '&selProprietarios2Value=' + selProprietarios2.value +
                    '&selEmpresa=' + _selEmpresa + '&selFamilias=' + _selFamilias + '&selMarca=' + _selMarca + '&sFiltro=' + sFiltro + '&chkCmbEstoques=' + chkCmbEstoques +
                    '&chkProdutoSeparavel=' + _chkProdutoSeparavel + '&chkReportaFabricante=' + _chkReportaFabricante + '&glb_DepositoSelecionado=' + glb_DepositoSelecionado +
                    '&chkTemEstoque=' + _chkTemEstoque +
                    '&elem=' + _elemValue1 + '&glb_nEmpresaID=' + glb_nEmpresaID + "&Padrao=Intel";


    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/Estoque.aspx?' + strParameters;
}

function relatorioPosicaoProdutos() {

    var sFiltroEstados = '';
    var sFiltroProprietarios = 'NULL';
    var sFiltroMarca = '';
    var sFiltroEstoque = '';
    var sFiltro = '';
    var i;

    if (parseInt(selProprietarios.value, 10) != 0)
        sFiltroProprietarios = selProprietarios.value;

    for (i = 0; i < selMarca2.length; i++) {
        if ((selMarca2.options[i].selected == true) && (selMarca2.options[i].value != 0)) {

            sFiltroMarca += (sFiltroMarca == '' ? (' b.MarcaID IN ( ' + selMarca2.options[i].value) : (', ' + selMarca2.options[i].value));
        }
    }

    sFiltroMarca += (sFiltroMarca.length > 0 ? ' )' : '');

    if (chkEstoqueFisico.checked)
        sFiltroEstoque = '(dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,341,NULL,NULL,NULL,375, -1) > 0)';

    sFiltro = trimStr(txtFiltro4.value);

    if ((sFiltro != '') && (sFiltroMarca != '')){
        sFiltro = sFiltro + ' AND ' + sFiltroMarca;
    }
    else if (sFiltroMarca != '') {
        sFiltro = sFiltroMarca;

        //sFiltro = '\'' + '(' + sFiltro + ')' + '\'';
    }

    if (sFiltroEstoque != '') {
        if (sFiltro.length > 0)
            sFiltro += ' AND ' + sFiltroEstoque;
        else
            sFiltro = sFiltroEstoque;
    }

    if (sFiltro != '') {
        sFiltro = "'"+ sFiltro+ "'";
    }
    else
        sFiltro = 'NULL';

    
    var strParams = '';
    var elem = null;

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divPosicaoProdutos.children.length) ; i++) {
        elem = divPosicaoProdutos.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked))
                sFiltroEstados += (sFiltroEstados == '' ? ('/' + elem.value) : ('/' + elem.value));
        }
    }

    if (sFiltroEstados != '')
        sFiltroEstados = '\'' + sFiltroEstados + '/' + '\'';
    else
        sFiltroEstados = 'NULL';
       

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40125 Posicao de Produtos
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word


    // Monta string de parametros

    for (i = 0; i < divPosicaoProdutos.children.length; i++) {
        elem = divPosicaoProdutos.children[i];

        // Campos Textos
        if (((elem.tagName).toUpperCase() == 'INPUT') && ((elem.type).toUpperCase() == 'TEXT') &&
             ((elem.id).toUpperCase() != 'TXTFILTRO')) {
            if ((elem.value != null) && (trimStr(elem.value) != '')) {
                strParams += (elem.previousSibling).innerText;
                strParams += ':';
                strParams += trimStr(elem.value);
                strParams += '   ';
            }
        }
        else if ((elem.tagName).toUpperCase() == 'SELECT') {
            if (elem.selectedIndex != -1) {
                if ((elem.options(elem.selectedIndex).innerText != null) && (trimStr(elem.options(elem.selectedIndex).innerText) != '')) {
                    if ((elem.previousSibling).innerText != 'Formato') {
                        strParams += (elem.previousSibling).innerText;
                        strParams += ':';
                        strParams += trimStr(elem.options(elem.selectedIndex).innerText);
                        strParams += '   ';
                    }

                }
            }
        }
    }

    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + 
                    "&sFiltroEstados=" + sFiltroEstados + "&sFiltroProprietarios=" + sFiltroProprietarios + "&selOrdem=" + selOrdem.selectedIndex + "&selMoedas2=" + selMoedas2.value +
                    "&sFiltro=" + sFiltro + "&strParams=" + strParams + '&Formato=' + selFormatoSolicitacao.value;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/basico/relacoes/relpessoaseconceitos/serverside/Reports_relpessoaseconceitos.aspx?" + strParameters + "&via2=false";

    glbContRel = 1;


}

function selEmpresa_onchange() {
    var nMoedaSistemaID = selMoedas.getAttribute('MoedaID', 1);
    var sMoedaSistema = selMoedas.getAttribute('SimboloMoeda', 1);
    var nMoedaID = 0;
    var sMoedaID = '';
    var nMoeda2ID = 0;
    var i = 0;
    var j = 0;
    var bInsere = false;

    clearComboEx([selMoedas.id]);

    if (nMoedaSistemaID != null) {
        var oOption = document.createElement("OPTION");
        oOption.text = sMoedaSistema;
        oOption.value = nMoedaSistemaID;
        selMoedas.add(oOption);
    }

    for (i = 0; i < selEmpresa.options.length; i++) {
        bInsere = true;

        if (selEmpresa.options.item(i).selected) {
            nMoedaID = selEmpresa.options.item(i).getAttribute('MoedaID', 1);
            sMoedaID = selEmpresa.options.item(i).getAttribute('SimboloMoeda', 1);

            if ((lookUpValueInCmb(selMoedas, nMoedaID) == -1) &&
				 (nMoedaID != nMoedaSistemaID)) {
                for (j = 0; j < selEmpresa.options.length; j++) {
                    if (selEmpresa.options.item(j).selected) {
                        nMoeda2ID = selEmpresa.options.item(j).getAttribute('MoedaID', 1);

                        if (nMoeda2ID != nMoedaID)
                            bInsere = false;
                    }
                }

                if (bInsere) {
                    var oOption = document.createElement("OPTION");
                    oOption.text = sMoedaID;
                    oOption.value = nMoedaID;
                    oOption.selected = true;
                    selMoedas.add(oOption);
                }
            }
        }
    }

}

function relatorioEstoqueMicrosoft() {
    var dirA1;
    var dirA2;
    var i;

    var _selFamilias = '';
    var _selMarca = '';
    var _elemValue1 = '';
    var Count = 0;
    var chkCmbEstoques = '';

    var dateTimeInSQLFormat = null;

    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataEstoque)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(' + '\'' + dateTimeInSQLFormat + '\'' + ', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';

    var sFiltroFabricante = (selFabricante.value == 0 ? '' : ' AND Produtos.FabricanteID=' + selFabricante.value);
    var sFiltroFamilia = '';
    var nItensSelected = 0;
    var sFiltroProprietario = '';

    //Quando o Fabricante escolhido for Microsoft US, trazer tamb�m o fabricante Macrosoft Corp ID 91575.
    if (selFabricante.value == 10003)
        sFiltroFabricante = ' AND Produtos.FabricanteID IN (' + selFabricante.value + ', 91575)';

    if (selProprietarios2.selectedIndex > 0)
        sFiltroProprietario = ' AND ProdutosEmpresa.ProprietarioID = ' + selProprietarios2.value + ' ';

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Microsoft)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word

    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';
    var sEstadosCheckeds = '';

    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                 (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                 (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                 (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                 (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
                 (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                 (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                 (elem.id.toUpperCase() != 'CHKEXCEL') &&
                 (elem.id.indexOf('_ESTOQUE_') < 0)) {
                Count++;

                if (bFirst) {
                    bFirst = false;

                    //sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    sFiltroEstados = ' AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
                else {
                    // sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    sFiltroEstados += ' OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
            }
        }
    }

    if (!bFirst)
        sFiltroEstados += ')';
    else
        sFiltroEstados += ' AND (ProdutosEmpresa.EstadoID=0)';

    var aCmbsEstoques =
        [chk1_ESTOQUE_EPO,
         chk1_ESTOQUE_EPE,
         chk1_ESTOQUE_EPD,
         chk1_ESTOQUE_EPC,
         chk1_ESTOQUE_EPT,
         chk1_ESTOQUE_ETO,
         chk1_ESTOQUE_ETE,
         chk1_ESTOQUE_ETD,
         chk1_ESTOQUE_ETC];

    var aSQLQuantEstoquesDetail =
        ['dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) '];

    var aDynamicEstoque =
        ['_EPO Numeric(10),',
         '_EPE Numeric(10),',
         '_EPD Numeric(10),',
         '_EPC Numeric(10),',
         '_EPT Numeric(10),',
         '_ETO Numeric(10),',
         '_ETE Numeric(10),',
         '_ETD Numeric(10),',
         '_ETC Numeric(10),'];

    var sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

    var bHasEstoqueSelected = false;
    var strSumEstoques = '';

    for (i = 0; i < aCmbsEstoques.length; i++) {
        if (aCmbsEstoques[i].checked) {
            if (!bHasEstoqueSelected)
                strSumEstoques += ', ';
            else
                strSumEstoques += ' + ';

            strSumEstoques += aSQLQuantEstoquesDetail[i];
            bHasEstoqueSelected = true;

            chkCmbEstoques += (i == 0 ? 'checked' : ':' + 'checked');
        }
        else {
            chkCmbEstoques += (i == 0 ? 'unchecked' : ':' + 'unchecked');
        }

    }

    if (strSumEstoques == '') {
        if (window.top.overflyGen.Alert('Selecione pelo menos um estoque.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    sSQLDetailSelect = 'SELECT Empresas.Fantasia, ' + '\'' + 'ALBZ' + '\'' + ', CHAR(79), ' +
		'\'' + glb_sCurrDateYYYYMMDD + '\'' + ', SPACE(0), SPACE(0), SPACE(0), SPACE(0), ' +
		'SPACE(0), SPACE(0), SPACE(0), Produtos.PartNumber, SPACE(0), SPACE(0), Produtos.Modelo, SPACE(0) ' +
		strSumEstoques + ', SPACE(0), SPACE(0), SPACE(0), SPACE(0), Familias.Conceito ';

    sSQLDetailFrom = 'FROM ' +
            'Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), ' +
            'RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), ' +
            'RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Recursos Estados WITH(NOLOCK), Recursos Sistema WITH(NOLOCK), Pessoas Empresas WITH(NOLOCK) ';

    sSQLDetailWhere1 = 'WHERE (' +
            '(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND ' +
            '(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND ' +
            'RelConceitos1.EstadoID=2) AND ' +
            '(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND ' +
            'Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND ' +
            '(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND ' +
            'RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND ' +
            '(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ';

    var sFiltroEmpresa = ' AND ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' ';

    sSQLDetailWhere2 =
            'Conceitos3.EstadoID=2) AND ' +
            '(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND ' +
            '(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 ' +
			sFiltroEmpresa + sFiltroProprietario + ' AND ProdutosEmpresa.SujeitoID=Empresas.PessoaID AND ' +
            'ProdutosEmpresa.EstadoID=Estados.RecursoID AND Sistema.RecursoID=999) AND ' +
            '(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) ';

    if (chkProdutoSeparavel.checked)
        sSQLDetailWhere2 += ' AND Produtos.ProdutoSeparavel = 1 ';

    if (chkReportaFabricante.checked)
        sSQLDetailWhere2 += ' AND ProdutosEmpresa.ReportaFabricante = 1 ';

    if (chkTemEstoque.checked)
        sSQLDetailWhere2 += ' AND (' + (strSumEstoques).substr(1) + ') > 0 ';

    sSQLDetailWhere2 += sFiltroFabricante + sFiltroFamilia + sFiltroEstados + ' ' + sFiltro + ') ';

    sSQLDetailGroupBy = '';

    sSQLDetailOrderBy = '';

    sTmpTableSelect =
        'CREATE TABLE #TempTable ' +
	    '( ' +
	    '_IDTMP INTEGER IDENTITY, ' +
		'[Sell From Global Name] VARCHAR(80), ' +
		'[Source ID] VARCHAR(8), ' +
		'[Resend Indicator] VARCHAR(1), ' +
		'[Report End Date] VARCHAR(8), ' +
		'[Ship From Global Name] VARCHAR(80), ' +
		'[Ship From External ID] VARCHAR(20), ' +
		'[Ship From Address Line 1] VARCHAR(80), ' +
		'[Ship From Address Line 2] VARCHAR(80), ' +
		'[Ship From City Name] VARCHAR(80), ' +
		'[Ship From State/Province] VARCHAR(80), ' +
		'[Ship From Postal Code] VARCHAR(20), ' +
		'[MS Part Number] VARCHAR(17), ' +
		'[MS UPC] VARCHAR(12), ' +
		'[Source Product ID] VARCHAR(30), ' +
		'[Source Product Description] VARCHAR(80), ' +
		'[Quantity In Float] INTEGER, ' +
		'[Quantity On Hand] INTEGER, ' +
		'[Committed Quantity] INTEGER, ' +
		'[On Order Quantity] INTEGER, ' +
		'[Returns Quantity] INTEGER, ' +
		'[Backorder Quantity] INTEGER, ' +
		'[MS Familia] VARCHAR(25))';

    sTmpTableSelect += 'INSERT INTO #TempTable ([Sell From Global Name],[Source ID],[Resend Indicator],' +
		'[Report End Date],[Ship From Global Name],[Ship From External ID],[Ship From Address Line 1],' +
		'[Ship From Address Line 2],[Ship From City Name],[Ship From State/Province],[Ship From Postal Code],' +
		'[MS Part Number],[MS UPC],[Source Product ID],[Source Product Description],[Quantity In Float],' +
		'[Quantity On Hand],[Committed Quantity],[On Order Quantity],[Returns Quantity],[Backorder Quantity], [MS Familia]) ' +
		sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailWhere2 + sSQLDetailGroupBy + sSQLDetailOrderBy + ' ';

    sTmpTableSelect += 'SELECT * FROM #TempTable ';
    sTmpTableSelect += 'DROP TABLE #TempTable';

    var strSQLParsed = 'EXEC sp_RelatorioEstoque ' + '\'' + dupCharInString(sTmpTableSelect, '\'') + '\'';

    strSQLParsed = (strSQLParsed == null ? '' : strSQLParsed);
    
    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    var _chkProdutoSeparavel = '';
    var _chkReportaFabricante = '';
    var _chkTemEstoque = '';

    if (chkProdutoSeparavel.checked)
        _chkProdutoSeparavel = 'checked';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    if (chkTemEstoque.checked)
        _chkTemEstoque = 'checked';

    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFabricante=' + selFabricante.value +
                    '&selProprietarios2Index=' + selProprietarios2.selectedIndex + '&selProprietarios2Value=' + selProprietarios2.value +
                    '&selFamilias=' + _selFamilias + '&sFiltro=' + sFiltro + '&chkCmbEstoques=' + chkCmbEstoques +
                    '&chkProdutoSeparavel=' + _chkProdutoSeparavel + '&chkReportaFabricante=' + _chkReportaFabricante + '&glb_DepositoSelecionado=' + glb_DepositoSelecionado +
                    '&chkTemEstoque=' + _chkTemEstoque + '&glb_sCurrDateYYYYMMDD=' + glb_sCurrDateYYYYMMDD +
                    '&elem=' + _elemValue1 + '&glb_nEmpresaID=' + glb_nEmpresaID + '&Padrao=Microsoft';


    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/Estoque.aspx?' + strParameters;
}

function relatorioEstoque3com() {
    var dirA1;
    var dirA2;
    var i;

    var _selFamilias = '';

    var dateTimeInSQLFormat = null;

    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataEstoque)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(\'' + dateTimeInSQLFormat + '\', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';

    var sFiltroFamilia = '';
    var nItensSelected = 0;

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao 3Com)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word

    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';
    var sEstadosCheckeds = '';
    var sReportaFabricante = '';

    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    if (chkReportaFabricante.checked)
        sReportaFabricante = 'AND ProdutosEmpresa.ReportaFabricante = 1 ';

    var s3com_variavel = 'SET NOCOUNT ON DECLARE	@dtFim DATETIME, @Fabricante VARCHAR(8000), @EmpresaRelatorio INT, @MoedaID INT, @Nome VARCHAR(35), @Ecam VARCHAR(8), ' +
			'@UF_Abreviado VARCHAR(5), @Cep VARCHAR(12), @Pais VARCHAR(5), @Contato VARCHAR(35), @Telefone VARCHAR(20), ' +
			'@UF VARCHAR(30), @Sql VARCHAR(8000) ' +
			'SET @dtFim = ISNULL(' + dateTimeInSQLFormat + ',GETDATE()) ';

    var s3com_table = 'CREATE TABLE #table3Com_Inv (YourCompanyName  VARCHAR(35), YourCompanyECAMBillTo  VARCHAR(35), ' +
						'YourCompanyBillToName  VARCHAR(35), StateCompany  VARCHAR(35), YourCompanyBilltoZip  VARCHAR(35), YourCompanyBilltoCountryCode  VARCHAR(35), ' +
						'ContactNameCompany  VARCHAR(35), ContactPhone  VARCHAR(35), YourCompanyECAMShipTo  VARCHAR(35), YourCompanyShipToName  VARCHAR(50), ' +
						'StatecompanyShip  VARCHAR(35), YourCompanyShipToZip  VARCHAR(35), YourCompanyShipToCountryCode  VARCHAR(35), ' +
						'UnitPrice  NUMERIC(11,2), ProductNumber VARCHAR(50), CurrencyCode  VARCHAR(35), EffectiveInventoryDate  VARCHAR(35),  ' +
						'QtyonHand  INT, QtyonOrder  INT, QtyStockTransfer  INT, ReportEndDate  VARCHAR(35)) ';

    var s3com_inserttable = 'INSERT INTO #table3Com_Inv (ProductNumber, UnitPrice, CurrencyCode, EffectiveInventoryDate, QtyonHand, QtyonOrder, QtyStockTransfer, ReportEndDate) ' +
					    'SELECT	DISTINCT Produtos.PartNumber,  ' +
						'dbo.fn_Produto_Custo(ProdutosEmpresa.RelacaoID,GetDate(),541, NULL,1), ' +
						'(SELECT SimboloMoeda FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = 541 ), CONVERT(VARCHAR(8),@dtFim,112), ' +
						'dbo.fn_Produto_Estoque(2,Produtos.ConceitoID,341,NULL,@dtFim,NULL,375, -1) +  ' +
						'dbo.fn_Produto_Estoque(7,Produtos.ConceitoID,341,NULL,@dtFim,NULL,375, -1) + ' +
						'dbo.fn_Produto_Estoque(10,Produtos.ConceitoID,341,NULL,@dtFim,NULL,375, -1) + ' +
						'/*RCC Alcateia-Abano por data*/ ' +
						'dbo.fn_Produto_EstoqueInTransit(2, Produtos.ConceitoID, @dtFim,1,2) + ' +
						'dbo.fn_Produto_EstoqueInTransit(10, Produtos.ConceitoID, @dtFim,1,2) As ONHand, ' +
						'dbo.fn_Produto_EstoqueInTransit(7, Produtos.ConceitoID, @dtFim,1,2) AS [RCC Allplus por data, ONOrder], ' +
								'dbo.fn_Produto_Estoque(7,Produtos.ConceitoID,343,NULL,@dtFim,NULL,375, -1) AS StockTransfer,	CONVERT(VARCHAR(10),@dtFim,112) ' +
						'FROM	Conceitos Produtos WITH(NOLOCK) ' +
									'INNER JOIN RelacoesPescon ProdutosEmpresa WITH(NOLOCK) ON Produtos.ConceitoID = ProdutosEmpresa.ObjetoID ' +
						'WHERE	Produtos.TipoConceitoID = 303 AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, @dtFim) NOT IN (1,5,11)) ' +
									'AND ProdutosEmpresa.TipoRelacaoID = 61	AND ProdutosEmpresa.SujeitoID = 7 AND Produtos.FabricanteID = 15395 ';

    s3com_inserttable += sFiltroFamilia + sReportaFabricante + sFiltro;

    //S� considera o que tem estoque
    var s3com_soestoque = 'DELETE #table3Com_Inv WHERE Qtyonhand = 0 AND QtyonOrder = 0 AND QtyStockTransfer = 0 ';

    //Adiciona dados da Allplus
    var s3com_dadosallplus = 'SELECT @Nome = a.Nome, @Ecam = \'63253858\',@UF_Abreviado = c.CodigoLocalidade2, @Cep = b.CEP, @Pais = d.CodigoLocalidade2, ' +
                                '@Contato = f.Fantasia, @Telefone = dbo.fn_Pessoa_Telefone(f.PessoaID, 119, 120),@UF = c.Localidade ' +
                            'FROM	Pessoas a WITH(NOLOCK) ' +
                                'INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON a.PessoaID = b.PessoaID ' +
                                'INNER JOIN Localidades c WITH(NOLOCK) ON b.UFID = c.LocalidadeID ' +
                                'INNER JOIN Localidades d WITH(NOLOCK) ON b.PaisID = d.LocalidadeID ' +
                                'INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON a.PessoaID = e.SujeitoID ' +
                                'INNER JOIN Pessoas f WITH(NOLOCK) ON e.ProprietarioID = f.PessoaID ' +
                            'WHERE	a.PessoaID = 7 ' +
                                'AND b.EndFaturamento = 1 AND b.Ordem = 1 ' +
                                'AND e.TipoRelacaoID = 21 ' +
                                'AND e.ObjetoID = 15395 /*3Com*/ ' +
                            'UPDATE #table3Com_Inv SET YourCompanyName = @Nome, YourCompanyECAMBillTo = @Ecam, YourCompanyBillToName = @Nome, ' +
                                'StateCompany = @UF_Abreviado, YourCompanyBilltoZip = @Cep, YourCompanyBilltoCountryCode = @Pais, ' +
                                'ContactNameCompany = @Contato, ContactPhone = @Telefone, YourCompanyECAMShipTo = @Ecam, ' +
                                'YourCompanyShipToName = @Nome, StatecompanyShip = @UF,YourCompanyShipToZip = @CEP, ' +
                                'YourCompanyShipToCountryCode = @Pais ';

    var s3com_selecttable = 'SELECT YourCompanyName AS [Your Company Name], \'\' AS [Number], YourCompanyECAMBillTo AS [Your Company ECAM Bill To #], ' +
            'YourCompanyBillToName AS [Your Company Bill To Name], \'\' AS [Address 1], \'\' AS [Address 2], \'\' AS [City], ' +
            'StateCompany AS [State], YourCompanyBilltoZip AS [Your Company Bill to Zip], \'\' AS [Country], YourCompanyBilltoCountryCode AS [Your Company Bill to Country Code], ' +
            'ContactNameCompany AS [Contact Name], ContactPhone AS [Contact Phone], YourCompanyECAMShipTo AS [Your Company ECAM Ship To #], YourCompanyShipToName AS [Your Company Ship To Name], ' +
            '\'\' AS [Ship to Address 1], \'\' AS [Ship To Address 2], \'\' AS [City], StatecompanyShip AS [State], ' +
            'YourCompanyShipToZip AS [Your Company Ship To Zip], \'\' AS [Country Name], YourCompanyShipToCountryCode AS [Your Company Ship To Country Code], ' +
            '\'\' AS [Contact Name], \'\' AS [Contact Phone], \'\' AS [Reseller Number], \'\' AS [Reseller Bill to #], ' +
            '\'\' AS [Reseller Bill to Name], \'\' AS [Reseller Bill to Address 1], \'\' AS [Reseller Bill to Address 2], ' +
            '\'\' AS [City], \'\' AS [State], \'\' AS [Reseller Bill to Zip], \'\' AS [Reseller Bill to Country Code], ' +
            '\'\' AS [Country], \'\' AS [Contact Name], \'\' AS [Contact Phone], \'\' AS [Ship to Number], ' +
            '\'\' AS [Reseller Ship to Name], \'\' AS [Ship to Address 1], \'\' AS [Ship to Address 2], \'\' AS [City], ' +
            '\'\' AS [State], \'\' AS [Reseller Ship to Zip], \'\' AS [Reseller Ship to Country Code], \'\' AS [Country], ' +
            '\'\' AS [Contact Name], \'\' AS [Contact Phone], \'\' AS [Sales Region], \'\' AS [Sales Territory], ' +
            '\'\' AS [End-User Bill to #], \'\' AS [End-User Bill To Name], \'\' AS [Bill to Address 1], \'\' AS [Bill to Address 2], ' +
            '\'\' AS [City], \'\' AS [State], \'\' AS [Zip], \'\' AS [Country Code], \'\' AS [Country], ' +
            '\'\' AS [End-User Ship To #], \'\' AS [Ship to Name], \'\' AS [Ship to Address 1], \'\' AS [Ship to Address 2], ' +
            '\'\' AS [City], \'\' AS [State], \'\' AS [Zip], \'\' AS [Country Code], \'\' AS [Country], ' +
            '\'\' AS [Contact Name], \'\' AS [Contact Phone], \'\' AS [SPQ/Promotion ID], \'\' AS [Bundle ID], ' +
            'ProductNumber AS [3COM Product Number], \'\' AS [Customer Product Number],  UnitPrice AS [Unit Price],  CurrencyCode AS [Currency Code], ' +
            '\'\' AS [UOM      1 = EA],  EffectiveInventoryDate AS [Effective Inventory Date],  QtyonHand AS [Qty on Hand],  QtyonOrder AS [Qty on Order], ' +
            '\'\' AS [Qty Committed], \'\' AS [Qty in Float], \'\' AS [Qty on Back Order],  QtyStockTransfer AS [Qty Stock Transfer],  ' +
            '\'\' AS [Ship Date], \'\' AS [Invoice Date],  ReportEndDate AS [Report End Date], \'\' AS [Qty Sold ], \'\' AS [Qty Returned], ' +
            '\'\' AS [Qty Shipped], \'\' AS [Invoice/Credit Note Number] FROM #table3Com_Inv ' +
            'DROP TABLE #table3Com_Inv SET NOCOUNT OFF';

    var s3com_sql = s3com_variavel + s3com_table + s3com_inserttable + s3com_soestoque + s3com_dadosallplus + s3com_selecttable;

    s3com_sql = (s3com_sql == null ? '' : s3com_sql);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    var _chkReportaFabricante = '';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFamilias=' + _selFamilias +
                    '&sFiltro=' + sFiltro + '&chkReportaFabricante=' + _chkReportaFabricante + '&Padrao=3com';
}

function relatorioEstoqueNVidia() {
    var dirA1;
    var dirA2;
    var i;
    var sFiltro = '';
    var sFiltroFamilia = '';
    var sFiltroMarca = '';
    var sFiltroEmpresa = '';
    var sFiltroFabricante = '';
    var sReportaFabricante = '';
    var nItensSelected = 0;
    var dateTimeInSQLFormat = null;
    var sNVidia_variavel = '';
    var sNVidia_table = '';
    var sNVidia_inserttable = '';
    var sNVidia_selecttable = '';
    var sNVidia_sql = '';

    var _selEmpresa = '';
    var _selFamilias = '';
    var _selMarca = '';

    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataEstoque)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(\'' + dateTimeInSQLFormat + '\', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');

    nItensSelected = 0;

    for (i = 0; i < selMarca.length; i++) {
        if (selMarca.options[i].selected == true) {
            nItensSelected++;
            sFiltroMarca += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.MarcaID=' + selMarca.options[i].value;

            _selMarca += (nItensSelected == 1 ? selMarca.options[i].value : ':' + selMarca.options[i].value);
        }
    }

    sFiltroMarca += (nItensSelected > 0 ? ')' : '');

    nItensSelected = 0;

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true) {
            nItensSelected++;
            sFiltroEmpresa += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'ProdutosEmpresa.SujeitoID=' + selEmpresa.options[i].value;

            _selEmpresa += (nItensSelected == 1 ? selEmpresa.options[i].value : ':' + selEmpresa.options[i].value);
        }
    }

    sFiltroEmpresa += (nItensSelected > 0 ? ')' : '');


    sFiltro = trimStr(txtFiltro.value);

    sFiltroFabricante = (selFabricante.value == 0 ? '' : ' AND Produtos.FabricanteID=' + selFabricante.value + ' ');

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    //    if(ckReportaFabricante.checked)
    //        sReportaFabricante = ' AND ProdutosEmpresa.ReportaFabricante = 1 ';

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao 3Com)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word

    sNVidia_variavel = 'SET NOCOUNT ON ' +
            'DECLARE @Separador VARCHAR(1), @MarcaID INT, @EmpresaID INT, @dtEstoque DATETIME, @Cotacao NUMERIC(15,7) ' +
            'SET @Separador = CHAR(44) ' +
            'SET @dtEstoque = ISNULL(' + dateTimeInSQLFormat + ',GETDATE()) ' +
            'SET @EmpresaID = ' + glb_nEmpresaID + ' ' +
            'SET @MarcaID = 3261 ' +
            'SET @Cotacao = dbo.fn_Preco_Cotacao(541, 647, -@EmpresaID, @dtEstoque) ';

    sNVidia_table = 'CREATE TABLE #tempTable3 (EmpresaID INT, ReporterID VARCHAR(10), DataEstoque DATETIME, Modelo VARCHAR(18), Descricao VARCHAR(256), ' +
		            'Quantidade INT, Fabricante VARCHAR(40), MoedaID INT, CustoAtual NUMERIC(11,2), CustoMedio NUMERIC(11,2),CustomerBackLog INT, ' +
		            'POBackLog INT) ';

    sNVidia_inserttable = 'INSERT INTO #tempTable3 ' +
	        'SELECT	ProdutosEmpresa.SujeitoID, CASE ProdutosEmpresa.SujeitoID WHEN 2 THEN \'1242392\' WHEN 7 THEN \'1368406\' END, ' +
			    '@dtEstoque, Produtos.Modelo, Produtos.Conceito + SPACE(1) + Produtos.Descricao, ' +
			    'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, 343, NULL, @dtEstoque,NULL,375, -1), Fabricantes.Fantasia, 647, ' +
			    'CASE ProdutosEmpresa.MoedaEntradaID WHEN 647 THEN ProdutosEmpresa.CustoMedio WHEN 541 THEN ProdutosEmpresa.CustoMedio * @Cotacao END, ' +
			    'CASE ProdutosEmpresa.MoedaEntradaID WHEN 647 THEN ProdutosEmpresa.CustoMedio WHEN 541 THEN ProdutosEmpresa.CustoMedio * @Cotacao END, ' +
			    'CASE WHEN ProdutosEmpresa.SujeitoID = 7 THEN dbo.fn_Produto_EstoqueInTransit(ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, @dtEstoque, 0,1) ELSE 0 END, ' +
			    'CASE WHEN ProdutosEmpresa.SujeitoID = 7 THEN dbo.fn_Produto_EstoqueInTransit(ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, @dtEstoque, 1,1) ELSE 0 END ' +
		    'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
				    'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID = Produtos.ConceitoID) ' +
				    'INNER JOIN Pessoas Fabricantes WITH(NOLOCK) ON (Produtos.FabricanteID = Fabricantes.PessoaID) ' +
		    'WHERE	(ProdutosEmpresa.TipoRelacaoID = 61) ' +
		            ' AND (ProdutosEmpresa.EstadoID NOT IN (1,5)) ';

    if (sFiltroEmpresa == '')
        sNVidia_inserttable += ' AND (ProdutosEmpresa.SujeitoID = @EmpresaID) ';

    sNVidia_inserttable += sFiltroEmpresa + sFiltroFamilia + sFiltroMarca + sFiltroFabricante + sReportaFabricante + sFiltro;

    sNVidia_selecttable = '/*Header*/ ' +
                    'SELECT	\'ReporterID\' + @Separador + ' +
		                    '\'Inventory Date\' + @Separador +  ' +
		                    '\'Product Number\' + @Separador +  ' +
		                    '\'Product Description\' + @Separador + ' +
		                    '\'Quantity On Hand\' + @Separador +  ' +
		                    '\'Manufacturer\' + @Separador +  ' +
		                    '\'Current Cost\' + @Separador +  ' +
		                    '\'Average Cost\' + @Separador +  ' +
		                    '\'Currency of Sale\' + @Separador +  ' +
		                    '\'Customer Back Log\' + @Separador +  ' +
		                    '\'PO Back Log\' + @Separador AS Registro ' +
                    'UNION ALL ' +
                    '/*Detalhe*/ ' +
                    'SELECT 	a.ReporterID + @Separador + /*[ReporterID]*/ ' +
		                    'CONVERT(VARCHAR(4),DATEPART(yyyy, a.DataEstoque)) + ' +
			                    'dbo.fn_Pad(CONVERT(VARCHAR(2),DATEPART(mm, a.DataEstoque)), 2, \'0\', \'L\') +  ' +
			                    'dbo.fn_Pad(CONVERT(VARCHAR(2),DATEPART(dd, a.DataEstoque)), 2, \'0\', \'L\') + @Separador + /*[Inventory Date]*/ ' +
		                    'ISNULL(CONVERT(VARCHAR(10), a.Modelo),SPACE(0)) + @Separador + /*[Product Number]*/ ' +
		                    'ISNULL(a.Descricao,SPACE(0)) + @Separador + /*[Product Description]*/ ' +
		                    'CONVERT(VARCHAR(10), a.Quantidade) + @Separador + /*[Quantity on Hand]*/ ' +
		                    'a.Fabricante + @Separador + /*[Manufacturer]*/ ' +
		                    'ISNULL(CONVERT(VARCHAR(13), a.CustoAtual),0) + @Separador + /*[Current Cost]*/ ' +
		                    'ISNULL(CONVERT(VARCHAR(13), a.CustoMedio),0) + @Separador + /*[Average Cost]*/ ' +
		                    'CASE a.MoedaID ' +
			                    'WHEN 541 THEN \'USD\' ' +
			                    'WHEN 647 THEN \'BRL\' END + @Separador + /*[Currency of Sale]*/ ' +
		                    'CONVERT(VARCHAR(10), a.CustomerBackLog) + @Separador + /*[Customer Back Log]*/ ' +
		                    'CONVERT(VARCHAR(10), a.POBackLog) + @Separador AS Registro /*[PO Back Log]*/ ' +
	                    'FROM #tempTable3 a ' +
                    'DROP TABLE #tempTable3 ' +
                    'SET NOCOUNT OFF ';

    sNVidia_sql = sNVidia_variavel + sNVidia_table + sNVidia_inserttable + sNVidia_selecttable;

    sNVidia_sql = (sNVidia_sql == null ? '' : sNVidia_sql);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    var _chkReportaFabricante = '';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFabricante=' + selFabricante.value +
                    '&selEmpresa=' + _selEmpresa + '&selFamilias=' + _selFamilias + '&selMarca=' + _selMarca + '&sFiltro=' + sFiltro +
                    '&chkReportaFabricante=' + _chkReportaFabricante + '&glb_nEmpresaID=' + glb_nEmpresaID + '&Padrao=NVidia';

    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/Estoque.aspx?' + strParameters;

}

function relatorioEstoqueSeagate() {
    var dirA1;
    var dirA2;
    var i;

    var _elemValue1 = '';
    var chkCmbEstoques = '';
    var _selEmpresa = '';
    var _selFamilias = '';
    var _selMarca = '';
    var Count = 0;

    var dateTimeInSQLFormat = null;

    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataEstoque)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(' + '\'' + dateTimeInSQLFormat + '\'' + ', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';

    var sFiltroFabricante = (selFabricante.value == 0 ? '' : ' AND Produtos.FabricanteID=' + selFabricante.value);
    var sFiltroProprietario = '';

    if (selProprietarios2.selectedIndex > 0)
        sFiltroProprietario = ' AND ProdutosEmpresa.ProprietarioID = ' + selProprietarios2.value + ' ';

    var sFiltroFamilia = '';
    var nItensSelected = 0;

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Seagate)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word

    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';
    var sEstadosCheckeds = '';

    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                 (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                 (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                 (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                 (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
                 (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                 (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                 (elem.id.toUpperCase() != 'CHKEXCEL') &&
                 (elem.id.indexOf('_ESTOQUE_') < 0)) {

                Count++;

                if (bFirst) {
                    bFirst = false;

                    //sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    sFiltroEstados = ' AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
                else {
                    // sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    sFiltroEstados += ' OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
            }
        }
    }

    if (!bFirst)
        sFiltroEstados += ')';
    else
        sFiltroEstados += ' AND (ProdutosEmpresa.EstadoID=0)';

    var aCmbsEstoques =
        [chk1_ESTOQUE_EPO,
         chk1_ESTOQUE_EPE,
         chk1_ESTOQUE_EPD,
         chk1_ESTOQUE_EPC,
         chk1_ESTOQUE_EPT,
         chk1_ESTOQUE_ETO,
         chk1_ESTOQUE_ETE,
         chk1_ESTOQUE_ETD,
         chk1_ESTOQUE_ETC];

    var aSQLQuantEstoquesDetail =
        ['dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) '];

    var aDynamicEstoque =
        ['_EPO Numeric(10),',
         '_EPE Numeric(10),',
         '_EPD Numeric(10),',
         '_EPC Numeric(10),',
         '_EPT Numeric(10),',
         '_ETO Numeric(10),',
         '_ETE Numeric(10),',
         '_ETD Numeric(10),',
         '_ETC Numeric(10),'];

    var sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

    var bHasEstoqueSelected = false;
    var strSumEstoques = '';
    for (i = 0; i < aCmbsEstoques.length; i++) {
        if (aCmbsEstoques[i].checked) {
            if (!bHasEstoqueSelected)
                strSumEstoques += ', ';
            else
                strSumEstoques += ' + ';

            strSumEstoques += aSQLQuantEstoquesDetail[i];
            bHasEstoqueSelected = true;

            chkCmbEstoques += (i == 0 ? 'checked' : ':' + 'checked');
        }
        else {
            chkCmbEstoques += (i == 0 ? 'unchecked' : ':' + 'unchecked');
        }
    }

    if (strSumEstoques == '') {
        if (window.top.overflyGen.Alert('Selecione pelo menos um estoque.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    sSQLDetailSelect = 'SELECT Empresas.Nome' +
		', ' + '\'' + glb_sCurrDateYYYYMMDDhhmmss + '\'' +
		', Empresas.PessoaID ' +
		', ' + '\'' + 'OH' + '\'' +
		', Paises.CodigoLocalidade2 ' +
		', CONVERT(VARCHAR(8), ' + dateTimeInSQLFormat + ', 112) ' +
		', Produtos.Modelo ' +
		strSumEstoques + ' ' +
		', ISNULL((SELECT SUM(Itens.Quantidade) FROM Pedidos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK), Operacoes Transacoes WITH(NOLOCK) ' +
			'WHERE (Pedidos.EmpresaID = ProdutosEmpresa.SujeitoID AND Pedidos.PedidoID=Itens.PedidoID AND ' +
			'Pedidos.TipoPedidoID = 601 AND Pedidos.TransacaoID=Transacoes.OperacaoID AND ' +
			'Transacoes.MetodoCustoID=372 AND ' +
			'Pedidos.EstadoID BETWEEN 23 AND 28 AND Itens.ProdutoID=Produtos.ConceitoID)), 0) AS [In-transit to Location Code] ' + ' ';

    sSQLDetailFrom = 'FROM ' +
            'Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), ' +
            'RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), ' +
            'RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Recursos Estados WITH(NOLOCK), Recursos Sistema WITH(NOLOCK), Pessoas Empresas WITH(NOLOCK), ' +
            'Pessoas_Enderecos Enderecos WITH(NOLOCK), Localidades Paises WITH(NOLOCK) ';

    sSQLDetailWhere1 = 'WHERE (' +
            '(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND ' +
            '(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND ' +
            'RelConceitos1.EstadoID=2) AND ' +
            '(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND ' +
            'Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND ' +
            '(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND ' +
            'RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND ' +
            '(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ';

    var sFiltroEmpresa = '';
    nItensSelected = 0;

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true) {
            nItensSelected++;
            sFiltroEmpresa += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'ProdutosEmpresa.SujeitoID=' + selEmpresa.options[i].value;

            _selEmpresa += (nItensSelected == 1 ? selEmpresa.options[i].value : ':' + selEmpresa.options[i].value);
        }
    }

    sFiltroEmpresa += (nItensSelected > 0 ? ')' : '');

    if (sFiltroEmpresa == '') {
        if (window.top.overflyGen.Alert('Selecione uma empresa') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    sSQLDetailWhere2 =
            'Conceitos3.EstadoID=2) AND ' +
            '(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND ' +
            '(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 ' +
			sFiltroEmpresa + sFiltroProprietario + ' AND ProdutosEmpresa.SujeitoID=Empresas.PessoaID AND ' +
            'Empresas.PessoaID = Enderecos.PessoaID AND Enderecos.Ordem = 1 AND Enderecos.PaisID = Paises.LocalidadeID AND ' +
            'ProdutosEmpresa.EstadoID=Estados.RecursoID AND Sistema.RecursoID=999) AND ' +
            '(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) ';

    if (chkProdutoSeparavel.checked)
        sSQLDetailWhere2 += ' AND Produtos.ProdutoSeparavel = 1 ';

    if (chkReportaFabricante.checked)
        sSQLDetailWhere2 += ' AND ProdutosEmpresa.ReportaFabricante = 1 ';

    sSQLDetailWhere2 += sFiltroFabricante + sFiltroFamilia + sFiltroEstados + ' ' + sFiltro + ') ';

    sSQLDetailGroupBy = '';

    sSQLDetailOrderBy = '';

    sTmpTableSelect =
        'CREATE TABLE #TempTable ' +
	    '( ' +
	    '_IDTMP INTEGER IDENTITY, ' +
		'[Distributor Name] VARCHAR(80), ' +
		'[Report Date /Time] VARCHAR(14), ' +
		'[Distributor Location Identifier] VARCHAR(2), ' +
		'[Inventory Type Code] VARCHAR(2), ' +
		'[Country Code] VARCHAR(2), ' +
		'[Inventory Date] VARCHAR(8), ' +
		'[Seagate Model Number] VARCHAR(18), ' +
		'[Quantity] VARCHAR(7), ' +
		'[In-transit to Location Code] VARCHAR(8)) ';

    sTmpTableSelect += 'INSERT INTO #TempTable ([Distributor Name],[Report Date /Time],[Distributor Location Identifier],' +
		'[Inventory Type Code],[Country Code],[Inventory Date],[Seagate Model Number],' +
		'[Quantity],[In-transit to Location Code]) ' +
		sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailWhere2 + sSQLDetailGroupBy + sSQLDetailOrderBy + ' ';

    if (chkTemEstoque.checked)
        sTmpTableSelect += ' DELETE FROM #TempTable WHERE (([Quantity] = 0) AND ([In-transit to Location Code] = 0)) ';

    sTmpTableSelect += 'SELECT * FROM #TempTable ORDER BY [Distributor Location Identifier], [Seagate Model Number] ';

    sTmpTableSelect += 'DROP TABLE #TempTable';

    var strSQLParsed = 'EXEC sp_RelatorioEstoque ' + '\'' + dupCharInString(sTmpTableSelect, '\'') + '\'';

    strSQLParsed = (strSQLParsed == null ? '' : strSQLParsed);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    var _chkProdutoSeparavel = '';
    var _chkReportaFabricante = '';
    var _chkTemEstoque = '';

    if (chkProdutoSeparavel.checked)
        _chkProdutoSeparavel = 'checked';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    if (chkTemEstoque.checked)
        _chkTemEstoque = 'checked';

    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFabricante=' + selFabricante.value +
                    '&selProprietarios2Index=' + selProprietarios2.selectedIndex + '&selProprietarios2Value=' + selProprietarios2.value +
                    '&selEmpresa=' + _selEmpresa + '&selFamilias=' + _selFamilias + '&sFiltro=' + sFiltro + '&chkCmbEstoques=' + chkCmbEstoques +
                    '&chkProdutoSeparavel=' + _chkProdutoSeparavel + '&chkReportaFabricante=' + _chkReportaFabricante + '&glb_DepositoSelecionado=' + glb_DepositoSelecionado +
                    '&chkTemEstoque=' + _chkTemEstoque + '&glb_sCurrDateYYYYMMDDhhmmss=' + glb_sCurrDateYYYYMMDDhhmmss +
                    '&elem=' + _elemValue1 + '&glb_nEmpresaID=' + glb_nEmpresaID + "&Padrao=Seagate";


    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/Estoque.aspx?' + strParameters;

}

function relatorioEstoqueKingston() {
    var dirA1;
    var dirA2;
    var i;
    var dateTimeInSQLFormat = null;

    var Count = 0;

    var _elemValue1 = '';
    var chkCmbEstoques = '';
    var _selEmpresa = '';
    var _selFamilias = '';
    var _selMarca = '';

    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataEstoque)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(' + '\'' + dateTimeInSQLFormat + '\'' + ', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';

    var sFiltroFabricante = (selFabricante.value == 0 ? '' : ' AND Produtos.FabricanteID=' + selFabricante.value);
    var sFiltroProprietario = '';

    if (selProprietarios2.selectedIndex > 0)
        sFiltroProprietario = ' AND ProdutosEmpresa.ProprietarioID = ' + selProprietarios2.value + ' ';

    var sFiltroFamilia = '';
    var nItensSelected = 0;

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Kingston)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word

    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';
    var sEstadosCheckeds = '';

    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                 (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                 (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                 (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                 (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
                 (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                 (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                 (elem.id.toUpperCase() != 'CHKEXCEL') &&
                 (elem.id.indexOf('_ESTOQUE_') < 0)) {
                Count++;

                if (bFirst) {
                    bFirst = false;

                    //sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    sFiltroEstados = ' AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
                else {
                    // sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    sFiltroEstados += ' OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
            }
        }
    }

    if (!bFirst)
        sFiltroEstados += ')';
    else
        sFiltroEstados += ' AND (ProdutosEmpresa.EstadoID=0)';

    var aCmbsEstoques =
        [chk1_ESTOQUE_EPO,
         chk1_ESTOQUE_EPE,
         chk1_ESTOQUE_EPD,
         chk1_ESTOQUE_EPC,
         chk1_ESTOQUE_EPT,
         chk1_ESTOQUE_ETO,
         chk1_ESTOQUE_ETE,
         chk1_ESTOQUE_ETD,
         chk1_ESTOQUE_ETC];

    var aSQLQuantEstoquesDetail =
        ['dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) '];

    var aDynamicEstoque =
        ['_EPO Numeric(10),',
         '_EPE Numeric(10),',
         '_EPD Numeric(10),',
         '_EPC Numeric(10),',
         '_EPT Numeric(10),',
         '_ETO Numeric(10),',
         '_ETE Numeric(10),',
         '_ETD Numeric(10),',
         '_ETC Numeric(10),'];

    var sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

    var bHasEstoqueSelected = false;
    var strSumEstoques = '';
    for (i = 0; i < aCmbsEstoques.length; i++) {
        if (aCmbsEstoques[i].checked) {
            if (!bHasEstoqueSelected)
                strSumEstoques += ', ';
            else
                strSumEstoques += ' + ';

            strSumEstoques += aSQLQuantEstoquesDetail[i];
            bHasEstoqueSelected = true;

            chkCmbEstoques += (i == 0 ? 'checked' : ':' + 'checked');
        }
        else {
            chkCmbEstoques += (i == 0 ? 'unchecked' : ':' + 'unchecked');
        }
    }

    if (strSumEstoques == '') {
        if (window.top.overflyGen.Alert('Selecione pelo menos um estoque.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    sSQLDetailSelect = 'SELECT Produtos.PartNumber ' +
		strSumEstoques + ', dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,354,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS committed, ' +
		'SPACE(0) AS [on order], ' +
		'CONVERT(INTEGER, dbo.fn_Produto_Totaliza(' + glb_nEmpresaID + ', ProdutosEmpresa.ObjetoID, ' +
			'dbo.fn_Data_Dia(GETDATE(), 3, 0, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, 0, 0, 0), 1)) AS [sales-month to date], ' +
		'CONVERT(INTEGER, dbo.fn_Produto_Totaliza(' + glb_nEmpresaID + ', ProdutosEmpresa.ObjetoID, ' +
			'dbo.fn_Data_Dia(GETDATE(), 3, -1, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -1, 0, 0), 1)) AS [sales-1 month ago], ' +
		'CONVERT(INTEGER, dbo.fn_Produto_Totaliza(' + glb_nEmpresaID + ', ProdutosEmpresa.ObjetoID, ' +
			'dbo.fn_Data_Dia(GETDATE(), 3, -2, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -2, 0, 0), 1)) AS [sales-2 month ago], ' +
		'CONVERT(INTEGER, dbo.fn_Produto_Totaliza(' + glb_nEmpresaID + ', ProdutosEmpresa.ObjetoID, ' +
			'dbo.fn_Data_Dia(GETDATE(), 3, -3, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -3, 0, 0), 1)) AS [sales-3 month ago] ';

    /*
            'CONVERT(INTEGER, dbo.fn_Produto_Totaliza(' + glb_nEmpresaID + ', ProdutosEmpresa.ObjetoID, dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3,  0, 1, 0)), dbo.fn_Data_Zero(GETDATE()), 1)) AS [sales-month to date],' +
            'CONVERT(INTEGER, dbo.fn_Produto_Totaliza(' + glb_nEmpresaID + ', ProdutosEmpresa.ObjetoID, dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -1, 1, 0)), dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -1, 0, 0)), 1)) AS [sales-1 month ago],' +
            'CONVERT(INTEGER, dbo.fn_Produto_Totaliza(' + glb_nEmpresaID + ', ProdutosEmpresa.ObjetoID, dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -2, 1, 0)), dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -2, 0, 0)), 1)) AS [sales-2 month ago],' +
            'CONVERT(INTEGER, dbo.fn_Produto_Totaliza(' + glb_nEmpresaID + ', ProdutosEmpresa.ObjetoID, dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -3, 1, 0)), dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -3, 0, 0)), 1)) AS [sales-3 month ago] ';
    */

    sSQLDetailFrom = 'FROM ' +
            'Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), ' +
            'RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), ' +
            'RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Recursos Sistema WITH(NOLOCK) ';

    sSQLDetailWhere1 = 'WHERE (' +
            '(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND ' +
            '(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND ' +
            'RelConceitos1.EstadoID=2) AND ' +
            '(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND ' +
            'Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND ' +
            '(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND ' +
            'RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND ' +
            '(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ';

    var sFiltroEmpresa = '';
    nItensSelected = 0;

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true) {
            nItensSelected++;
            sFiltroEmpresa += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'ProdutosEmpresa.SujeitoID=' + selEmpresa.options[i].value;

            _selEmpresa += (nItensSelected == 1 ? selEmpresa.options[i].value : ':' + selEmpresa.options[i].value);
        }
    }

    sFiltroEmpresa += (nItensSelected > 0 ? ')' : '');

    if (sFiltroEmpresa == '') {
        if (window.top.overflyGen.Alert('Selecione uma empresa') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    sSQLDetailWhere2 =
            'Conceitos3.EstadoID=2) AND ' +
            '(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND ' +
            '(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 ' +
			sFiltroEmpresa + sFiltroProprietario + ' AND Sistema.RecursoID=999) AND ' +
            '(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) ';

    if (chkProdutoSeparavel.checked)
        sSQLDetailWhere2 += ' AND Produtos.ProdutoSeparavel = 1 ';

    if (chkReportaFabricante.checked)
        sSQLDetailWhere2 += ' AND ProdutosEmpresa.ReportaFabricante = 1 ';

    sSQLDetailWhere2 += sFiltroFabricante + sFiltroFamilia + sFiltroEstados + ' ' + sFiltro + ') ';

    sSQLDetailGroupBy = '';

    sSQLDetailOrderBy = '';

    sTmpTableSelect =
        'CREATE TABLE #TempTable ' +
	    '( ' +
	    '_IDTMP INTEGER IDENTITY, ' +
		'[part number] VARCHAR(20), ' +
		'[stock on hand] VARCHAR(7), ' +
		'[committed ] VARCHAR(7), ' +
		'[on order] VARCHAR(1), ' +
		'[sales- month to date] VARCHAR(7), ' +
		'[sales- 1 month ago] VARCHAR(7), ' +
		'[sales- 2 month ago] VARCHAR(7), ' +
		'[sales- 3 month ago] VARCHAR(7)) ';

    sTmpTableSelect += 'INSERT INTO #TempTable ([part number],[stock on hand],[committed ],' +
		'[on order],[sales- month to date],[sales- 1 month ago],[sales- 2 month ago],[sales- 3 month ago]) ' +
		sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailWhere2 + sSQLDetailGroupBy + sSQLDetailOrderBy + ' ';

    if (chkTemEstoque.checked)
        sTmpTableSelect += ' DELETE FROM #TempTable WHERE (([Quantity] = 0) AND ([In-transit to Location Code] = 0)) ';

    sTmpTableSelect += 'SELECT * FROM #TempTable ORDER BY [part number] ';

    sTmpTableSelect += 'DROP TABLE #TempTable';

    var strSQLParsed = 'EXEC sp_RelatorioEstoque ' + '\'' + dupCharInString(sTmpTableSelect, '\'') + '\'';

    strSQLParsed = (strSQLParsed == null ? '' : strSQLParsed);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    var _chkProdutoSeparavel = '';
    var _chkReportaFabricante = '';
    var _chkTemEstoque = '';

    if (chkProdutoSeparavel.checked)
        _chkProdutoSeparavel = 'checked';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    if (chkTemEstoque.checked)
        _chkTemEstoque = 'checked';

    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFabricante=' + selFabricante.value +
                    '&selProprietarios2Index=' + selProprietarios2.selectedIndex + '&selProprietarios2Value=' + selProprietarios2.value +
                    '&selEmpresa=' + _selEmpresa + '&selFamilias=' + _selFamilias + '&selMarca=' + _selMarca + '&sFiltro=' + sFiltro + '&chkCmbEstoques=' + chkCmbEstoques +
                    '&chkProdutoSeparavel=' + _chkProdutoSeparavel + '&chkReportaFabricante=' + _chkReportaFabricante + '&glb_DepositoSelecionado=' + glb_DepositoSelecionado +
                    '&chkTemEstoque=' + _chkTemEstoque + '&glb_sCurrDateYYYYMMDDhhmmss=' + glb_sCurrDateYYYYMMDDhhmmss +
                    '&elem=' + _elemValue1 + '&glb_nEmpresaID=' + glb_nEmpresaID + "&Padrao=Kingston";


    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/Estoque.aspx?' + strParameters;


}

function relatorioEstoqueHP() {
    var dirA1;
    var dirA2;
    var i;
    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';
    var sEstadosCheckeds = '';
    var sFiltroFabricante = '';
    var sFiltroProprietario = '';
    var sFiltroFamilia = '';
    var nItensSelected = 0;
    var sFiltroEmpresa = '';
    var sFiltroMarca = '';
    var sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

    var Count = 0;

    var _elemValue1 = '';
    var chkCmbEstoques = '';
    var _selEmpresa = '';
    var _selFamilias = '';
    var _selMarca = '';

    var dateTimeInSQLFormat = null;

    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        if (criticAndNormTxtDataTime(txtDataEstoque)) {
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(' + '\'' + dateTimeInSQLFormat + '\'' + ', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';

    sFiltroFabricante = (selFabricante.value == 0 ? '' : ' AND Produtos.FabricanteID=' + selFabricante.value);
    sFiltroProprietario = '';

    if (selProprietarios2.selectedIndex > 0)
        sFiltroProprietario = ' AND ProdutosEmpresa.ProprietarioID = ' + selProprietarios2.value + ' ';

    sFiltroFamilia = '';
    nItensSelected = 0;

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');

    sFiltroMarca = '';
    nItensSelected = 0;

    for (i = 0; i < selMarca.length; i++) {
        if (selMarca.options[i].selected == true) {
            nItensSelected++;
            sFiltroMarca += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.MarcaID=' + selMarca.options[i].value;

            _selMarca += (nItensSelected == 1 ? selMarca.options[i].value : ':' + selMarca.options[i].value);
        }
    }

    sFiltroMarca += (nItensSelected > 0 ? ')' : '');

    sFiltroEstados = ' ';
    bFirst = true;
    sFiltro = '';
    sEstadosCheckeds = '';

    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                 (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                 (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                 (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                 (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
                 (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                 (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                 (elem.id.toUpperCase() != 'CHKEXCEL') &&
                 (elem.id.indexOf('_ESTOQUE_') < 0)) {

                Count++;

                if (bFirst) {
                    bFirst = false;

                    sFiltroEstados = ' AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
                else {
                    sFiltroEstados += ' OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
            }
        }
    }

    if (!bFirst)
        sFiltroEstados += ')';
    else
        sFiltroEstados += ' AND (ProdutosEmpresa.EstadoID=0)';

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Seagate)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word

    var aCmbsEstoques =
        [chk1_ESTOQUE_EPO,
         chk1_ESTOQUE_EPE,
         chk1_ESTOQUE_EPD,
         chk1_ESTOQUE_EPC,
         chk1_ESTOQUE_EPT,
         chk1_ESTOQUE_ETO,
         chk1_ESTOQUE_ETE,
         chk1_ESTOQUE_ETD,
         chk1_ESTOQUE_ETC];

    var aSQLQuantEstoquesDetail =
        ['dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) '];

    var aDynamicEstoque =
        ['_EPO Numeric(10),',
         '_EPE Numeric(10),',
         '_EPD Numeric(10),',
         '_EPC Numeric(10),',
         '_EPT Numeric(10),',
         '_ETO Numeric(10),',
         '_ETE Numeric(10),',
         '_ETD Numeric(10),',
         '_ETC Numeric(10),'];

    var bHasEstoqueSelected = false;
    var strSumEstoques = '';
    for (i = 0; i < aCmbsEstoques.length; i++) {
        if (aCmbsEstoques[i].checked) {
            if (!bHasEstoqueSelected)
                strSumEstoques += ', ';
            else
                strSumEstoques += ' + ';

            strSumEstoques += aSQLQuantEstoquesDetail[i];
            bHasEstoqueSelected = true;

            chkCmbEstoques += (i == 0 ? 'checked' : ':' + 'checked');
        }
        else {
            chkCmbEstoques += (i == 0 ? 'unchecked' : ':' + 'unchecked');
        }
    }

    if (strSumEstoques == '') {
        if (window.top.overflyGen.Alert('Selecione pelo menos um estoque.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    sSQLDetailSelect = 'SELECT \'\' AS Circular ' +
	    ',Produtos.Modelo AS [Descri��o] ' +
	    ',Produtos.PartNumber AS [C�d HP] ' +
	    ',0 AS Rebate' +
	    strSumEstoques + ' AS [Qtde.] ' +
	    ',0 AS [Rebate Total] ' +
	    ',\'\' AS PL ' +
	    ',\'\' AS [ID/OPG] ' +
	    ', CONVERT(VARCHAR(10), ISNULL(' + dateTimeInSQLFormat + ',GETDATE()), ' + DATE_SQL_PARAM + ') AS [Data de estoque] ';

    sSQLDetailFrom = 'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
                        'INNER JOIN Conceitos Produtos WITH(NOLOCK) ON ProdutosEmpresa.ObjetoID = Produtos.ConceitoID ' +
                        'INNER JOIN Conceitos Familias WITH(NOLOCK) ON Produtos.ProdutoID = Familias.ConceitoID ' +
                        'INNER JOIN Conceitos Marcas WITH(NOLOCK) ON Marcas.ConceitoID=Produtos.MarcaID ';

    sSQLDetailWhere1 = 'WHERE (ProdutosEmpresa.TipoRelacaoID=61) AND ' +
                        '(Produtos.EstadoID = 2 AND Familias.EstadoID = 2 AND Marcas.EstadoID = 2) ';

    if (chkProdutoSeparavel.checked)
        sSQLDetailWhere1 += ' AND (Produtos.ProdutoSeparavel = 1) ';

    if (chkReportaFabricante.checked)
        sSQLDetailWhere1 += ' AND (ProdutosEmpresa.ReportaFabricante = 1) ';

    sFiltroEmpresa = '';
    nItensSelected = 0;

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true) {
            nItensSelected++;
            sFiltroEmpresa += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'ProdutosEmpresa.SujeitoID=' + selEmpresa.options[i].value;

            _selEmpresa += (nItensSelected == 1 ? selEmpresa.options[i].value : ':' + selEmpresa.options[i].value);
        }
    }

    sFiltroEmpresa += (nItensSelected > 0 ? ')' : '');

    if (sFiltroEmpresa == '') {
        if (window.top.overflyGen.Alert('Selecione uma empresa') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    sSQLDetailWhere2 = sFiltroEmpresa + sFiltroProprietario + sFiltroFabricante + sFiltroFamilia + sFiltroMarca + sFiltroEstados + ' ' + sFiltro;

    var strSQLParsed = sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailWhere2;

    var sTmpTableSelect =
        'CREATE TABLE #TempTable ' +
	    '( ' +
		'Circular VARCHAR(1),' +
		'[Descri��o] VARCHAR(64),' +
		'[C�d HP] VARCHAR(64),' +
		'[Rebate] VARCHAR(1),' +
		'[Qtde.] NUMERIC(10),' +
		'[Rebate Total] VARCHAR(1), ' +
		'[PL] VARCHAR(1),' +
		'[ID/OPG] VARCHAR(1),' +
		'[Data de Estoque] VARCHAR(11)) ';

    sTmpTableSelect += 'INSERT INTO #TempTable  ' + strSQLParsed;

    sTmpTableSelect += 'SELECT * FROM #TempTable WHERE [Qtde.] > 0 ORDER BY [Descri��o]';
    sTmpTableSelect += 'DROP TABLE #TempTable';

    var strSQLParsed = 'EXEC sp_RelatorioEstoque ' + '\'' + dupCharInString(sTmpTableSelect, '\'') + '\'';

    strSQLParsed = (strSQLParsed == null ? '' : strSQLParsed);

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    var _chkProdutoSeparavel = '';
    var _chkReportaFabricante = '';
    var _chkTemEstoque = '';

    if (chkProdutoSeparavel.checked)
        _chkProdutoSeparavel = 'checked';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    if (chkTemEstoque.checked)
        _chkTemEstoque = 'checked';

    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFabricante=' + selFabricante.value +
                    '&selProprietarios2Index=' + selProprietarios2.selectedIndex + '&selProprietarios2Value=' + selProprietarios2.value + '&DATE_SQL_PARAM=' + DATE_SQL_PARAM +
                    '&selEmpresa=' + _selEmpresa + '&selFamilias=' + _selFamilias + '&selMarca=' + _selMarca + '&sFiltro=' + sFiltro + '&chkCmbEstoques=' + chkCmbEstoques +
                    '&chkProdutoSeparavel=' + _chkProdutoSeparavel + '&chkReportaFabricante=' + _chkReportaFabricante + '&glb_DepositoSelecionado=' + glb_DepositoSelecionado +
                    '&chkTemEstoque=' + _chkTemEstoque + '&glb_sCurrDateYYYYMMDDhhmmss=' + glb_sCurrDateYYYYMMDDhhmmss +
                    '&elem=' + _elemValue1 + '&glb_nEmpresaID=' + glb_nEmpresaID + "&Padrao=HP";


    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/Estoque.aspx?' + strParameters;
}

function relatorioSPED() {
    var dirA1;
    var dirA2;
    var i;
    var dateTimeInSQLFormat = null;
    var dataCustoEstoque;
    var TipoCustoEstoque;
    var PeriodoSolcitado = '';

    var Count = 0;

    var _elemValue1 = '';
    var chkCmbEstoques = '';
    var _selEmpresa = '';
    var _selFamilias = '';
    var _selMarca = '';

    // Analiza o campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    if (!((txtDataEstoque.value == null) || (txtDataEstoque.value == ''))) {
        txtDataEstoque.value += ' 23:59:59';

        if (criticAndNormTxtDataTime(txtDataEstoque))
        {       
            dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);
            PeriodoSolcitado = normalizeDate_DateTime(txtDataEstoque.value, true);

            if (dateTimeInSQLFormat == null)
                return null;
        }
        else
            return null;
    }

    if (dateTimeInSQLFormat != null)
        dateTimeInSQLFormat = 'dbo.fn_Data_Fuso(' + '\'' + dateTimeInSQLFormat + '\'' + ', NULL,' + glb_nEmpresaCidadeID + ')';
    else
        dateTimeInSQLFormat = 'NULL';

    dirA1 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 4);
    dirA2 = valueOfFieldInArray(glb_arrayReports, 1, selReports.value, 5);

    var sFiltroFabricante = (selFabricante.value == 0 ? '' : ' AND Produtos.FabricanteID=' + selFabricante.value);

    var sFiltroEmpresa = '';
    var nItensSelected = 0;
    var sFiltroProprietario = '';

    if (selProprietarios2.selectedIndex > 0)
        sFiltroProprietario = ' AND ProdutosEmpresa.ProprietarioID = ' + selProprietarios2.value + ' ';

    for (i = 0; i < selEmpresa.length; i++) {
        if (selEmpresa.options[i].selected == true) {
            nItensSelected++;
            sFiltroEmpresa += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'ProdutosEmpresa.SujeitoID=' + selEmpresa.options[i].value;

            _selEmpresa += (nItensSelected == 1 ? selEmpresa.options[i].value : ':' + selEmpresa.options[i].value);
        }
    }

    sFiltroEmpresa += (nItensSelected > 0 ? ')' : '');

    if (sFiltroEmpresa == '') {
        if (window.top.overflyGen.Alert('Selecione uma empresa') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    var sFiltroFamilia = '';
    nItensSelected = 0;

    for (i = 0; i < selFamilias.length; i++) {
        if (selFamilias.options[i].selected == true) {
            nItensSelected++;
            sFiltroFamilia += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.ProdutoID=' + selFamilias.options[i].value;

            _selFamilias += (nItensSelected == 1 ? selFamilias.options[i].value : ':' + selFamilias.options[i].value);
        }
    }

    sFiltroFamilia += (nItensSelected > 0 ? ')' : '');


    var sFiltroMarca = '';
    nItensSelected = 0;

    for (i = 0; i < selMarca.length; i++) {
        if (selMarca.options[i].selected == true) {
            nItensSelected++;
            sFiltroMarca += (nItensSelected == 1 ? ' AND (' : ' OR ') + 'Produtos.MarcaID=' + selMarca.options[i].value;

            _selMarca += (nItensSelected == 1 ? selMarca.options[i].value : ':' + selMarca.options[i].value);
        }
    }

    sFiltroMarca += (nItensSelected > 0 ? ')' : '');

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Print
    //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Generico)
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel
    //desabilitado-> Word

    var sFiltroEstados = ' ';
    var bFirst = true;
    var sFiltro = '';
    var sEstadosCheckeds = '';

    sFiltro = trimStr(txtFiltro.value);

    if (sFiltro != '')
        sFiltro = ' AND (' + sFiltro + ') ';

    // loop nos checkbox de estados para montar a string de filtro
    for (i = 0; i < (divRelatorioEstoque.children.length) ; i++) {
        elem = divRelatorioEstoque.children[i];

        if ((elem.tagName).toUpperCase() == 'INPUT') {
            if (((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                 (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                 (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                 (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                 (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&
                 (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                 (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                 (elem.id.toUpperCase() != 'CHKEXCEL') &&
                 (elem.id.indexOf('_ESTOQUE_') < 0)) {

                Count++;

                if (bFirst) {
                    bFirst = false;

                    //sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    sFiltroEstados = ' AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
                else {
                    // sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    sFiltroEstados += ' OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;

                    _elemValue1 += (Count == 1 ? elem.value : ':' + elem.value);
                }
            }
        }
    }

    if (!bFirst)
        sFiltroEstados += ')';
    else
        sFiltroEstados += ' AND (ProdutosEmpresa.EstadoID=0)';

    var aCmbsEstoques =
        [chk1_ESTOQUE_EPO,
         chk1_ESTOQUE_EPE,
         chk1_ESTOQUE_EPD,
         chk1_ESTOQUE_EPC,
         chk1_ESTOQUE_EPT,
         chk1_ESTOQUE_ETO,
         chk1_ESTOQUE_ETE,
         chk1_ESTOQUE_ETD,
         chk1_ESTOQUE_ETC,
         chk1_ESTOQUE_EB,
         chk1_ESTOQUE_F,
         chk1_ESTOQUE_Disp,
         chk1_ESTOQUE_RC,
         chk1_ESTOQUE_RCC,
         chk1_ESTOQUE_RV,
         chk1_ESTOQUE_RVC];


    var bLocalizacao = true;

    for (i = 0; i < aCmbsEstoques.length; i++) {
        if (aCmbsEstoques[i].checked) {
            bLocalizacao = false;

            chkCmbEstoques += (i == 0 ? 'checked' : ':' + 'checked');
        }
        else {
            chkCmbEstoques += (i == 0 ? 'unchecked' : ':' + 'unchecked');
        }
    }

    if ((dateTimeInSQLFormat == 'NULL') && (chkCustoContabil.checked)) {
        dateTimeInSQLFormat = "GetDate()";        
    }


    var sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailOrderBy;


    sSQLDetailSelect = 'SELECT DISTINCT ' +
                       'Empresas.Fantasia AS [Empresa],' +
				       'ProdutosEmpresa.ObjetoID,' +
				       'dbo.fn_Produto_Descricao2(ProdutosEmpresa.ObjetoID,647,11),' +
				       'Familias.Conceito,' +
				       'Marcas.Conceito,' +
				       'Produtos.Modelo,' +
				       'NCM.Conceito,' +
				       '0,' +
				       'dbo.fn_produto_custo(produtosempresa.relacaoid,' + dateTimeInSQLFormat + ', 647, NULL, 2),' +
				       '0,' +
				       'AliquotaICMS.Aliquota,' +
				       'dbo.fn_TipoAuxiliar_Item(produtos.UnidadeID,2), ' +
				        dateTimeInSQLFormat + ',' +
				       '(SELECT TOP 1  dbo.fn_DivideZero(b.ValorCustoTotal, b.Quantidade) ' +
				 		'FROM RelacoesPesCon_Kardex aa WITH(NOLOCK) ' +
				 			'INNER JOIN Pedidos_Itens_Kardex b WITH(NOLOCK) on b.PedIteKardexID = aa.PedIteKardexID ' +
				 			'INNER JOIN Pedidos_Itens bb WITH(NOLOCK) ON (b.PedItemID = bb.PedItemID) ' +
				 			'INNER JOIN Pedidos cc WITH(NOLOCK) ON (bb.PedidoID = cc.PedidoID) ' +
				 			'LEFT OUTER JOIN NotasFiscais dd WITH(NOLOCK) ON (cc.NotaFiscalID = dd.NotaFiscalID) ' +
				 			'INNER JOIN Operacoes ee WITH(NOLOCK) ON (cc.TransacaoID = ee.OperacaoID) ' +
				 	   'WHERE ((aa.RelacaoID = produtosempresa.RelacaoID) AND ' +
				 		      '(aa.dtkardex <=' + dateTimeInSQLFormat + ') AND ' +
				 			  '(ee.OperacaoID IN (111)) AND (aa.TipoCustoID = 374)) ' +
				        'ORDER BY aa.RelPesConKardexID DESC), ' +
				       '(SELECT TOP 1  aa.dtKardex ' +
				 		'FROM RelacoesPesCon_Kardex aa WITH(NOLOCK) ' +
				 			'INNER JOIN Pedidos_Itens_Kardex b WITH(NOLOCK) on b.PedIteKardexID = aa.PedIteKardexID ' +
				 			'INNER JOIN Pedidos_Itens bb WITH(NOLOCK) ON (b.PedItemID = bb.PedItemID) ' +
				 			'INNER JOIN Pedidos cc WITH(NOLOCK) ON (bb.PedidoID = cc.PedidoID) ' +
				 			'LEFT OUTER JOIN NotasFiscais dd WITH(NOLOCK) ON (cc.NotaFiscalID = dd.NotaFiscalID) ' +
				 			'INNER JOIN Operacoes ee WITH(NOLOCK) ON (cc.TransacaoID = ee.OperacaoID) ' +
				 	   'WHERE ((aa.RelacaoID = produtosempresa.RelacaoID) AND ' +
				 		      '(aa.dtkardex <=' + dateTimeInSQLFormat + ') AND ' +
				 			  '(ee.OperacaoID IN (111)) AND (aa.TipoCustoID = 374)) ' +
				        'ORDER BY aa.RelPesConKardexID DESC), ' +
			           '(SELECT TOP 1  cc.PedidoID ' +
				 		'FROM RelacoesPesCon_Kardex aa WITH(NOLOCK) ' +
				 			'INNER JOIN Pedidos_Itens_Kardex b WITH(NOLOCK) on b.PedIteKardexID = aa.PedIteKardexID ' +
				 			'INNER JOIN Pedidos_Itens bb WITH(NOLOCK) ON (b.PedItemID = bb.PedItemID) ' +
				 			'INNER JOIN Pedidos cc WITH(NOLOCK) ON (bb.PedidoID = cc.PedidoID) ' +
				 			'LEFT OUTER JOIN NotasFiscais dd WITH(NOLOCK) ON (cc.NotaFiscalID = dd.NotaFiscalID) ' +
				 			'INNER JOIN Operacoes ee WITH(NOLOCK) ON (cc.TransacaoID = ee.OperacaoID) ' +
				 	   'WHERE ((aa.RelacaoID = produtosempresa.RelacaoID) AND ' +
				 		      '(aa.dtkardex <=' + dateTimeInSQLFormat + ') AND ' +
				 			  '(ee.OperacaoID IN (111)) AND (aa.TipoCustoID = 374)) ' +
				        'ORDER BY aa.RelPesConKardexID DESC), ' +
				'ISNULL((SELECT TOP 1 dbo.fn_Preco_Cotacao(aa.MoedaID, 647, NULL,' + dateTimeInSQLFormat + ') * aa.CustoReposicao ' +
									'FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) ' +
									'WHERE (aa.RelacaoID =  produtosempresa.RelacaoID)), 0) ';

    sSQLDetailFrom = 'FROM ' +
                'conceitos conceitos1 WITH(nolock),' +
                'relacoesconceitos relconceitos1 WITH(nolock),' +
                'conceitos conceitos2 WITH(nolock),' +
                'relacoesconceitos relconceitos2 WITH(nolock),' +
                'conceitos conceitos3 WITH(nolock),' +
                'conceitos produtos WITH(nolock),' +
                'conceitos familias WITH(nolock),' +
                'conceitos marcas WITH(nolock),' +
                'relacoespescon produtosempresa WITH(nolock),' +
                'recursos estados WITH(nolock),' +
                'recursos sistema WITH(nolock),' +
                'pessoas fabricantes WITH(nolock),' +
                'pessoas empresas WITH(nolock),' +
                'pessoas proprietarios WITH(nolock),' +
				'Conceitos NCM WITH(NOLOCK),' +
				'RelacoesPesCon_Impostos AliquotaICMS WITH(NOLOCK) ';

    sSQLDetailWhere1 = 'WHERE (' +
            '(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND ' +
            '(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND ' +
            'RelConceitos1.EstadoID=2) AND ' +
            '(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND ' +
            'Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND ' +
            '(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND ' +
            'RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND ' +
            '(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ';

    sSQLDetailWhere2 =
            'Conceitos3.EstadoID=2) AND ' +
            '(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND ' +
            '(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
            'ProdutosEmpresa.EstadoID=Estados.RecursoID AND Sistema.RecursoID=999) AND ' +
            '(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) AND ' +
            '(Fabricantes.PessoaID=Produtos.FabricanteID) AND ' +
            '(Empresas.PessoaID=ProdutosEmpresa.SujeitoID) AND ' +
            '(Proprietarios.PessoaID=ProdutosEmpresa.ProprietarioID) AND ' +
            '(NCM.ConceitoID = produtosempresa.ClassificacaoFiscalID) AND ' +
			'((AliquotaICMS.RelacaoID = produtosempresa.RelacaoID) AND ' +
			' (AliquotaICMS.ImpostoID = 9) AND (AliquotaICMS.TipoID = 335)) ';

    if (chkProdutoSeparavel.checked)
        sSQLDetailWhere1 += ' AND Produtos.ProdutoSeparavel = 1 ';

    if (chkReportaFabricante.checked)
        sSQLDetailWhere1 += ' AND ProdutosEmpresa.ReportaFabricante = 1 ';

    sSQLDetailWhere1 += sSQLDetailWhere2 + sFiltroFabricante + sFiltroEmpresa + sFiltroProprietario + sFiltroFamilia + sFiltroMarca + sFiltroEstados + sFiltro + ') ';

    sSQLDetailOrderBy = 'ORDER BY  Empresas.Fantasia,ProdutosEmpresa.ObjetoID ';

    var aSQLQuantEstoquesDetail =
        ['dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,341,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPO ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,342,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPE ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,343,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPD ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,344,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPC ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,345,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS EPT ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,346,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS ETO ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,347,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS ETE ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,348,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS ETD ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,349,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS ETC ',
         'dbo.fn_Produto_EstoqueBrinde(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,4) AS EB ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,341,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, -1) AS F__ ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,356,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS Dis ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,351,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS RC_ ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,352,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS RCC ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,353,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS RV_ ',
         'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,354,' + glb_DepositoSelecionado + ',' + dateTimeInSQLFormat + ',NULL,375, NULL) AS RVC '];
    var aDynamicEstoque =
        ['V_EPO Numeric(10),',
         'V_EPE Numeric(10),',
         'V_EPD Numeric(10),',
         'V_EPC Numeric(10),',
         'V_EPT Numeric(10),',
         'V_ETO Numeric(10),',
         'V_ETE Numeric(10),',
         'V_ETD Numeric(10),',
         'V_ETC Numeric(10),',
         'V_EB_ Numeric(10),',
         'V_F__ Numeric(10),',
         'V_Dis Numeric(10),',
         'V_RC_ Numeric(10),',
         'V_RCC Numeric(10),',
         'V_RV_ Numeric(10),',
         'V_RVC Numeric(10),'];


    for (i = 0; i < aCmbsEstoques.length; i++)
        if (aCmbsEstoques[i].checked)
            sSQLDetailSelect += ', ' + aSQLQuantEstoquesDetail[i];

    // 'ProdutosEmpresa.CustoMedio * dbo.fn_Preco_Cotacao(ProdutosEmpresa.MoedaEntradaID, ' + selMoedas.value + ', NULL, GETDATE()) AS Custo, ' +

    //    //Alterado para pegar estoque na data.
    //    if ((dateTimeInSQLFormat != 'NULL') && (chkCustoContabil.checked)) {
    //        TipoCustoEstoque = 2;
    //        dataCustoEstoque = dateTimeInSQLFormat;
    //    }
    //    else if ((dateTimeInSQLFormat == 'NULL') && (chkCustoContabil.checked)) {
    //        TipoCustoEstoque = 1;
    //        dataCustoEstoque = 'GetDate()';
    //    }
    //    else if ((dateTimeInSQLFormat != 'NULL') && (!chkCustoContabil.checked)) {
    //        TipoCustoEstoque = 4;
    //        dataCustoEstoque = dateTimeInSQLFormat;
    //    }
    //    else if ((dateTimeInSQLFormat == 'NULL') && (!chkCustoContabil.checked)) {
    //        TipoCustoEstoque = 3;
    //        dataCustoEstoque = 'GetDate()';
    //    }

    sTmpTableSelect = 'CREATE TABLE #TempTable ' +
		'( ' +
	    'Empresa VARCHAR(100), ' +
	    'ProdutoID INT,' +
	    'Produto VARCHAR(500),' +
	    'Familia VARCHAR(100),' +
	    'Marca VARCHAR(100),' +
	    'Modelo VARCHAR(100),' +
	    'NCM VARCHAR(50),' +
	    'Quantidade INT,' +
	    'CustoContabil NUMERIC(11,2),' +
	    'CustoContabilTotal  NUMERIC(11,2),' +
	    'AliquotaICMS NUMERIC(5,2),' +
	    'UnidadeMedida VARCHAR(50),' +
	    'Data DATETIME,' +
	    'V_CustoUltimaCompraKardex NUMERIC(11,2),' +
	    'V_dtUltimaCompraKardex DATETIME,' +
	    'V_PedidoID INT,' +
	    'V_CustoReposicao NUMERIC(11,2),' +
	    'V_epo  INT,' +
	    'V_epe INT,' +
	    'V_epd INT,' +
	    'V_epc INT,' +
	    'V_ept INT)';

    sTmpTableSelect += ' INSERT INTO #TempTable ' + sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailOrderBy + ' ';

    if (!bLocalizacao) {
        sTmpTableSelect += ' UPDATE #TempTable SET Quantidade = ( ';

        var nQtdEstoquesCheckeds = 0;
        var isFirst = true;
        for (i = 0; i < aCmbsEstoques.length; i++) {
            if (aCmbsEstoques[i].checked) {
                sTmpTableSelect += (isFirst ? '' : '+') + (aDynamicEstoque[i]).substr(0, 5);
                isFirst = false;
                nQtdEstoquesCheckeds++;
            }
        }

        if (!isFirst)
            sTmpTableSelect += ') ';
    }

    //    sTmpTableSelect += ' UPDATE #TempTable SET Cubagem = Cubagem * Quant, Peso = Peso * Quant ';

    if (chkValoracao.checked)
        sTmpTableSelect += ' UPDATE #TempTable SET CustoContabilTotal = Quantidade * CustoContabil';

    sTmpTableSelect += ' SELECT * FROM #TempTable ';

    if (chkTemEstoque.checked)
        sTmpTableSelect += ' WHERE (Quantidade > 0) ';

    sTmpTableSelect += ' DROP TABLE #TempTable';

    var strSQLParsed = 'EXEC sp_RelatorioEstoque ' + '\'' + dupCharInString(sTmpTableSelect, '\'') + '\'';

    strSQLParsed = (strSQLParsed == null ? '' : strSQLParsed);

    var lUseBold = true;
    var nFontTitleSize = 8;
    var lUseBoldSubDetail = false;
    var nLeft = 0;
    var nTopHeader2 = 0;
    var sParams = '';

    if (sFiltroProprietario != '')
        sParams += translateTerm('Propriet�rio', null) + ':' + selProprietarios2.options(selProprietarios2.selectedIndex).innerText + '   ';

    if (sEstadosCheckeds != '')
        sParams += sEstadosCheckeds + '   ';

    if (trimStr(txtDataEstoque.value) != '')
        sParams += translateTerm('Data', null) + ':' + txtDataEstoque.value + '   ';

    if (selDeposito.selectedIndex > 0)
        sParams += 'Dep�sito: ' + selDeposito.options[selDeposito.selectedIndex].innerText + '   ';

    if (chkValoracao.checked)
        sParams += translateTerm('Moeda', null) + ':' + selMoedas.options[selMoedas.selectedIndex].innerText;

    if (sParams != '') {
        nTopHeader2 = 14;
    }

    //pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);

    if (chkExcel.checked) {
        pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    }

    var _chkProdutoSeparavel = '';
    var _chkReportaFabricante = '';
    var _chkCustoContabil = '';
    var _chkTemEstoque = '';
    var _chkTemEstoque = '';
    var _chkValoracao = '';

    if (chkProdutoSeparavel.checked)
        _chkProdutoSeparavel = 'checked';

    if (chkReportaFabricante.checked)
        _chkReportaFabricante = 'checked';

    if (chkTemEstoque.checked)
        _chkTemEstoque = 'checked';

    if (chkCustoContabil.checked)
        _chkCustoContabil = 'checked';

    if (chkTemEstoque.checked)
        _chkTemEstoque = 'checked';

    if (chkValoracao.checked)
        _chkValoracao = 'checked';

    strParameters = 'dateTimeInSQLFormat=' + dateTimeInSQLFormat + '&glb_nEmpresaCidadeID=' + glb_nEmpresaCidadeID + '&selFabricante=' + selFabricante.value +
                    '&selProprietarios2Index=' + selProprietarios2.selectedIndex + '&selProprietarios2Value=' + selProprietarios2.value + '&glb_sEmpresaFantasia=' + glb_sEmpresaFantasia +
                    '&selEmpresa=' + _selEmpresa + '&selFamilias=' + _selFamilias + '&selMarca=' + _selMarca + '&sFiltro=' + sFiltro + '&chkCmbEstoques=' + chkCmbEstoques +
                    '&chkProdutoSeparavel=' + _chkProdutoSeparavel + '&chkReportaFabricante=' + _chkReportaFabricante + '&glb_DepositoSelecionado=' + glb_DepositoSelecionado +
                    '&elem=' + _elemValue1 + '&chkValoracao=' + _chkValoracao + '&chkTemEstoque=' + _chkTemEstoque + '&glb_nEmpresaID=' + glb_nEmpresaID + "&Padrao=SPED" + '&glb_PeriodoSolicitado=' + PeriodoSolcitado;

    var frameReport = document.getElementById("frmReport");

    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;

    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/Estoque.aspx?' + strParameters;
}