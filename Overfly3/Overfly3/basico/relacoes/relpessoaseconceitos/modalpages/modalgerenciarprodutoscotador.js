/********************************************************************
modalgerenciarprodutoscotador.js

TSA - A cria��o da Modal de Gerenciamento Produtos Cotador se deve para o gerenciamento dos produtos
        que n�o possuem cadastro no overfly e precisam ser cotados para os clientes.

Library javascript para modalgerenciarprodutoscotador.asp
********************************************************************/

//VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_dataGridWasChanged = false;  // Controla se algum dado do grid foi alterado
var glb_refreshGrid = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_nTimerSendDataToServer = null;
var glb_nTimerToServer = null;
var glb_aSendDataToServer = [];
var glb_nPointToaSendDataToServer = 0;
var glb_nCurrFormID = null;
var glb_nCurrSubFormID = null;
var glb_bModoAtualizacao;
var glb_bInclusaoProduto = false;
var glb_nFormID = window.top.formID;
var glb_nEmpresaData = getCurrEmpresaData();

//Eventos de envio ao BD ************************************************
var dsoMarca = new CDatatransport('dsoMarca');
var dsoTiposArquivos = new CDatatransport('dsoTiposArquivos');
var dsoNCM = new CDatatransport('dsoNCM');
var dsoNCMGrid = new CDatatransport('dsoNCMGrid');
var dsoGridAtualizacoes = new CDatatransport(dsoGridAtualizacoes);
var dsoGridProdutos = new CDatatransport(dsoGridProdutos);
var dsoCmbGridProdOrigem = new CDatatransport(dsoCmbGridProdOrigem);
var dsoCmbGridProdTipo = new CDatatransport(dsoCmbGridProdTipo);
var dsoCmbGridProdMoeda = new CDatatransport(dsoCmbGridProdMoeda);
var dsoCmbGridProdEstado = new CDatatransport(dsoCmbGridProdEstado);
var dsoCmbGridTaxaConv = new CDatatransport(dsoCmbGridTaxaConv);
var dsoGridProdutosInclusao = new CDatatransport(dsoGridProdutosInclusao);
var dsoGridDescontos = new CDatatransport(dsoGridDescontos);
var dsoGridLogs = new CDatatransport(dsoGridLogs);
var dsoGridTaxaConv = new CDatatransport(dsoGridTaxaConv);

/********INDICE DAS FUNCOES *********************************************
window_onload()
setupPage()
btn_onclick(ctl)
btnCanc_onclick()
preencheMarca()
preencheMarca_DSC()
preencheTipoArquivo()
preencheTipoArquivo_DSC()
preencheNCM(bFiltro)
preencheNCM_DSC()
selMarca_onchange()
selModo_onchange()
selTipoArquivo_onchange()
selOrigem_onchange()
selEstado_onchange()
selNCM_onchange()
fillGridDataAtualizacoes()
fillGridDataProdutos(bListOk, bNewItem)
fillGridDataDescontos(nCorpProdutoID)
fillGridDataLogs(nCorpProdutoID)
fillGridDataAtualizacoes_DSC()
fillGridDataProdutos_DSC()
fillGridDataDescontos_DSC()
fillGridDataLogs_DSC()
importarArquivo()
Download()
saveDataInGridAtualizacoes()
saveDataInGridProdutos()
saveDataInGrid_DSC()
afterEdit_gridAtualizacoes(Row, Col)
afterEdit_gridProdutos(Row, Col)
startDynamicCmbsGridsProdutos()
startDynamicCmbsGridsProdutos_DSC()
criticAndNormTxtDataTime(ctlRef, sCampo)
pintaCelula()
Argumento_onkeypress()
**********************************************************************/

// IMPLEMENTACAO DAS FUNCOES *********************************************

//Configura o html *******************************************************
function window_onload() {
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_nCurrFormID = window.top.formID;
    glb_nCurrSubFormID = window.top.subFormID;
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;


    // ajusta o body do html
    with (modalgerenciarprodutoscotadorBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html    
    setupPage();

    return glb_nCurrFormID;
}

//Configuracao inicial do html *******************************************
function setupPage() {
    secText('Gerenciamento Cotador', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var iframe;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 28;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';

    // ajusta o divControles
    //elem = window.document.getElementById('');
    with (divControles.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP - 10;
        width = modWidth - 2 * ELEM_GAP;
        height = 90;
    }

    // Ajusta elementos da divControles (elementos comuns aos dois divs (Incluir e Gerenciar))
    adjustElementsInForm([['lblModo', 'selModo', 13, 1, -10],
                          ['lblMarca', 'selMarca', 10, 1],
                          ['lblTipoArquivo', 'selTipoArquivo', 15, 1],
                          ['btnUpload', 'btn', btnOK.offsetWidth - 16, 1,10],
                          ['btnDownload', 'btn', btnOK.offsetWidth - 16, 1, 7],
                          ['btnVoltar', 'btn', btnOK.offsetWidth - 16, 1, -140],
                          ['btnListar', 'btn', btnOK.offsetWidth - 16, 1, - 10],
                          ['btnGravar', 'btn', btnOK.offsetWidth - 16, 1],
                          ['lblOrigem', 'selOrigem', 12, 1, -320],
                          ['lblEstado', 'selEstado', 15, 1],
                          ['lblNCM', 'selNCM', 15, 1],
                          ['lblArgumento', 'txtArgumento', 20, 1],
                          ['btnNovo', 'btn', btnOK.offsetWidth - 16, 1, 33]], null, null, true);

    // Alinha bot�es � direita
    window.document.getElementById('btnListar').style.left = frameRect[2] - btnListar.offsetWidth - 16 - ELEM_GAP - 72;
    window.document.getElementById('btnGravar').style.left = frameRect[2] - btnGravar.offsetWidth - 16 - ELEM_GAP;

    divUpload.style.visibility = 'hidden';
    btnVoltar.style.visibility = 'hidden';
    //selTipoArquivo.style.visibility = 'hidden';
    //lblTipoArquivo.style.visibility = 'hidden';
    selOrigem.style.visibility = 'hidden';
    lblOrigem.style.visibility = 'hidden';
    lblEstado.style.visibility = 'hidden';
    selEstado.style.visibility = 'hidden';
    lblNCM.style.visibility = 'hidden';
    selNCM.style.visibility = 'hidden';
    lblArgumento.style.visibility = 'hidden';
    txtArgumento.style.visibility = 'hidden';
    btnNovo.style.visibility = 'hidden';

    iframe = document.getElementById("I1");
    selMarca.onchange = selMarca_onchange;
    selModo.onchange = selModo_onchange;
    selTipoArquivo.onchange = selTipoArquivo_onchange;
    selEstado.onchange = selEstado_onchange;
    selOrigem.onchange = selOrigem_onchange;
    selNCM.onchange = selNCM_onchange;
    txtArgumento.maxLength = 100;
    btnGravar.disabled = true;
    btnDownload.disabled = true;

    //ajusta divUpload
    with (divUpload.style) {
        border = 'solid';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = btnVoltar.offsetLeft + 62;
        top = parseInt(selModo.style.top, 10) - 12;
        width = 400;
        height = 90;
    }

    // ajusta as Divs de Grid
    with (divGrids.style) {
        border = 'none';
        backgroundColor = 'transparent';
        //visibility = 'hidden';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) +
			parseInt(divControles.currentStyle.height, 10) -37;
        width = modWidth - 2 * ELEM_GAP - 6;
        //height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10) - 20;
        height = "477px";
    }

    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = divGrids.currentStyle.width;
        height = parseInt(divGrids.currentStyle.height,10);
    }

    with (divFGDetalhes.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = divGrids.currentStyle.width;
        height = parseInt(divGrids.currentStyle.height, 10);
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = (parseInt(divFG.style.height, 10));
    }

    with (fgDetalhes.style) {
        left = 0;
        top = 346;
        width = parseInt(divFG.style.width, 10);
        height = 134;
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
    
    startGridInterface(fgDetalhes);
    fgDetalhes.Cols = 0;
    fgDetalhes.Cols = 1;
    fgDetalhes.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fgDetalhes.Redraw = 2;
    // Esconde div
    fgDetalhes.style.visibility = 'hidden';

    glb_bModoAtualizacao = false;
    preencheMarca();
    preencheTipoArquivo();
    preencheNCM(false);
    startDynamicCmbsGridsProdutos();
    selModo_onchange();

    showExtFrame(window, true);
}

//Metodos onclick ********************************************************
function btn_onclick(ctl) {

    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    if (controlID == 'btnListar')
    {
        glb_bInclusaoProduto = false;

        //Reajusta altura
        fg.style.height = (parseInt(divFG.style.height, 10));

        //Atualiza��es
        if (glb_bModoAtualizacao)
            fillGridDataAtualizacoes();
        else
            fillGridDataProdutos(false, false);
    }
    else if (controlID == 'btnGravar')
    {
        //Atualiza��es
        if (glb_bModoAtualizacao) {
            saveDataInGridAtualizacoes();
        }
            //Produtos
        else
        {
            saveDataInGridProdutos();
        }
    }
    else if (controlID == 'btnUpload')
    {
            importarArquivo();
    }
    else if (controlID == 'btnVoltar') {
        divUpload.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';
        btnUpload.style.visibility = 'inherit';
        btnDownload.style.visibility = 'inherit';
    }
    else if (controlID == 'btnDownload')
    {
        Download();
    }
    else if (controlID == 'btnNovo')
    {
        glb_bInclusaoProduto = true;

        //preencheNCM(false);

        fillGridDataProdutos(false, true);
    }
    else
    {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '')
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        // esta funcao fecha a janela modal e destrava a interface
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

function btnCanc_onclick() {

    if(glb_bModoAtualizacao)
    {
        fillGridDataAtualizacoes();
        document.getElementById("I1").src = "";
        divUpload.style.visibility = 'hidden';
        divUpload.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';
        btnUpload.style.visibility = 'inherit';
        btnDownload.style.visibility = 'inherit';
    }
}

//Preenchimento dos Combos Dinamicos *************************************
function preencheMarca() {
    nMarcaID = selMarca.value;

    setConnection(dsoMarca);

    var sWHERE = '';

    dsoMarca.SQL = "SELECT 0 AS MarcaID, \'\' AS Marca " +
	    "UNION " +
	    "SELECT DISTINCT a.ConceitoID AS MarcaID, a.Conceito AS Marca " +
    	    "FROM Conceitos a WITH(NOLOCK) " +
            "WHERE a.TipoConceitoID = 304 AND a.Observacoes LIKE '%<CotadorCorporativo>%' " + sWHERE +
        'ORDER BY Marca';

    dsoMarca.ondatasetcomplete = preencheMarca_DSC;
    dsoMarca.Refresh();
}

function preencheMarca_DSC() {
    clearComboEx(['selMarca']);

    if (!(dsoMarca.recordset.BOF || dsoMarca.recordset.EOF))
    {
        dsoMarca.recordset.moveFirst;

        while (!dsoMarca.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoMarca.recordset['Marca'].value;
            oOption.value = dsoMarca.recordset['MarcaID'].value;
            selMarca.add(oOption);
            dsoMarca.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selMarca', nMarcaID);

        selMarca.disabled = ((selMarca.length <= 1) ? true : false);
    }
}

function preencheTipoArquivo() {

    var sFiltro = '';

    sFiltro = 'TipoID = 61 AND EstadoID = 2';

    setConnection(dsoTiposArquivos);

    dsoTiposArquivos.SQL = 'SELECT ItemID, ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ' + sFiltro;
    dsoTiposArquivos.ondatasetcomplete = preencheTipoArquivo_DSC;
    dsoTiposArquivos.Refresh();
}

function preencheTipoArquivo_DSC() {

    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = 0;
    oOption.selected = true;
    selTipoArquivo.add(oOption);

    while (!dsoTiposArquivos.recordset.EOF) {
        var optionStr = dsoTiposArquivos.recordset['ItemMasculino'].value;
        var optionValue = dsoTiposArquivos.recordset['ItemID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        // Forms que dependiam de direitos para gerenciar imagens agora tem a regra agregada � glb_permiteAlterarImagem fazendo a mesma tarefa
		// Form publica��o n�o permite tinha op��o documentos bloqueada, portanto permite insers�o apenas de imagem
        if (((optionValue != 1451) || (optionValue == 1451 && glb_permiteAlterarImagem)) && ((optionValue != 1454) || (optionValue == 1454 && (glb_nCurrFormID != 4240)))) 
		{
            selTipoArquivo.add(oOption);
        }

        dsoTiposArquivos.recordset.MoveNext();
    }
}

function preencheNCM(bFiltro)
{
    var sFiltro = '';

    selNCM.disabled = true;

    if (bFiltro) {
        setConnection(dsoNCM);

        var nMarcaID = selMarca.value;
        var nOrigemID = selOrigem.value;
        var nEstadoID = selEstado.value;

        if (nMarcaID > 0)
            sFiltro += " AND a.MarcaID = " + nMarcaID + " ";

        if (nOrigemID > 0)
            sFiltro += " AND a.OrigemID " + (nOrigemID == 2 ? "IN (2701, 2706)" : "NOT IN (2701, 2706)") + " ";

        if (nEstadoID > 0)
            sFiltro += "AND a.EstadoID = " + nEstadoID + " ";

        if (sFiltro.length > 0) {
            sQuery = "SELECT 0 [NCMID], \'\' [NCM] " +
                     "UNION " +
                     "SELECT DISTINCT a.NCMID, b.Conceito [NCM] " +
                        "FROM CorpProdutos a WITH(NOLOCK) " +
                            "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.NCMID) " +
                        "WHERE (a.NCMID IS NOT NULL) " + sFiltro +
                        "ORDER BY [NCM] ";

            dsoNCM.SQL = sQuery;
            dsoNCM.ondatasetcomplete = preencheNCM_DSC;
            dsoNCM.Refresh();
        }
    }
    else
        preencheNCMGrid();
}

function preencheNCM_DSC() {
    clearComboEx(['selNCM']);

    if (!(dsoNCM.recordset.BOF || dsoNCM.recordset.EOF))
	{
        dsoNCM.recordset.moveFirst;

        while (!dsoNCM.recordset.EOF)
        {
            var oOption = document.createElement("OPTION");

            oOption.text = dsoNCM.recordset['NCM'].value;
            oOption.value = dsoNCM.recordset['NCMID'].value;
            selNCM.add(oOption);
            dsoNCM.recordset.MoveNext();
        }

        selNCM.disabled = ((selNCM.length <= 1) ? true : false);
    }
}

function preencheNCMGrid()
{
    var sQuery;

    setConnection(dsoNCMGrid);

    sQuery = "SELECT 0 [NCMID], \'\' [NCM] " +
             "UNION " +
             "SELECT DISTINCT a.ConceitoID, a.Conceito [NCM] " +
                "FROM Conceitos a WITH(NOLOCK) " +
                "WHERE a.TipoConceitoID = 309 AND a.Observacoes LIKE '%<CotadorCorporativo>%' " +
                "ORDER BY [NCM] ";

    dsoNCMGrid.SQL = sQuery;
    dsoNCMGrid.ondatasetcomplete = preencheNCMGrid_DSC;
    dsoNCMGrid.Refresh();
}

function preencheNCMGrid_DSC()
{
    ;
}

//Metodos OnChange ********************************************************
function selMarca_onchange()
{
    if (glb_bModoAtualizacao) 
    {
        importarArquivo();

        divUpload.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';
        btnUpload.style.visibility = 'inherit';
        btnDownload.style.visibility = 'inherit';
    }
    else {
        preencheNCM(true);
        CmbGridTaxaConv();
    }

    // zera o grid
    fg.Rows = 1;
}

function selModo_onchange()
{
    //Reajusta altura
    fg.style.height = (parseInt(divFG.style.height, 10));

    //Atualiza��es
    if (selModo.value == 0)
    {
        btnUpload.style.visibility = 'inherit';
        btnDownload.style.visibility = 'inherit';
        lblTipoArquivo.style.visibility = 'inherit';
        selTipoArquivo.style.visibility = 'inherit';

        lblOrigem.style.visibility = 'hidden';
        selOrigem.style.visibility = 'hidden';
        lblEstado.style.visibility = 'hidden';
        selEstado.style.visibility = 'hidden';
        lblNCM.style.visibility = 'hidden';
        selNCM.style.visibility = 'hidden';
        lblArgumento.style.visibility = 'hidden';
        txtArgumento.style.visibility = 'hidden';
        btnNovo.style.visibility = 'hidden';

        //Reajusta altura
        fg.style.height = (parseInt(divFG.style.height, 10));

        //esconde div e grid de detalhes
        divFGDetalhes.style.visibility = 'hidden';
        fgDetalhes.style.visibility = 'hidden';

        glb_bModoAtualizacao = true;
        btnDownload.disabled = true;
    }
    //Produtos
    else
    {
        divUpload.style.visibility = 'hidden';
        btnUpload.style.visibility = 'hidden';
        btnDownload.style.visibility = 'hidden';
        lblTipoArquivo.style.visibility = 'hidden';
        selTipoArquivo.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';


        lblOrigem.style.visibility = 'inherit';
        selOrigem.style.visibility = 'inherit';
        lblEstado.style.visibility = 'inherit';
        selEstado.style.visibility = 'inherit';
        lblNCM.style.visibility = 'inherit';
        selNCM.style.visibility = 'inherit';
        lblArgumento.style.visibility = 'inherit';
        txtArgumento.style.visibility = 'inherit';
        btnNovo.style.visibility = 'inherit';

        glb_bModoAtualizacao = false;
    }

    selModo.focus();
    fg.Rows = 1;
}

function selTipoArquivo_onchange()
{
    var bUpload = (divUpload.style.visibility == 'inherit' ? true : false);

    importarArquivo();

    if (!bUpload)
    {
        divUpload.style.visibility = 'hidden';
        btnVoltar.style.visibility = 'hidden';
        btnUpload.style.visibility = 'inherit';
        btnDownload.style.visibility = 'inherit';
    }

    selTipoArquivo.focus();

    // zera o grid
    fg.Rows = 1;
}

function selOrigem_onchange()
{
    if (!glb_bModoAtualizacao)
        preencheNCM(true);

    // zera o grid
    fg.Rows = 1;
}

function selEstado_onchange()
{
    if (!glb_bModoAtualizacao)
        preencheNCM(true);

    // zera o grid
    fg.Rows = 1;
}

function selNCM_onchange()
{
    // zera o grid
    fg.Rows = 1;
}

/***Metodos criados pelo programador *************************************/

//Solicitar dados dos grids ao servidor ************************************
function fillGridDataAtualizacoes() {
    var sfiltro = "";
    var nMarcaID = selMarca.value;
    var nTipoArquivoID = selTipoArquivo.value;

    if (nMarcaID > 0) {

        btnOK.disabled = true;
        lockControlsInModalWin(true);

        // zera o grid
        fg.Rows = 1;


        if (nTipoArquivoID > 0)
            sfiltro =  " AND a.TipoArquivoID = " + nTipoArquivoID;

        // parametrizacao do dso dsoGridAtualizacoes
        setConnection(dsoGridAtualizacoes);

        dsoGridAtualizacoes.SQL = "SELECT a.CotAtualizacaoID, (SELECT TOP 1 b.DocumentoID " +
                                                        "FROM [OVERFLYBASE_EXT2].Overfly_Documentos.dbo.Documentos b WITH(NOLOCK) " +
                                                        "WHERE (b.RegistroID = ISNULL(a.CotAtualizacaoID,NULL) AND b.FormID = 2130 AND b.SubformID = 40014)) [DocumentoID], " +
                                            "dbo.fn_TipoAuxiliar_Item(a.TipoArquivoID, 0) [Tipo], 'Cisco' [Marca], a.Arquivo, dbo.fn_Numero_Formata(ISNULL(a.Registros, 0), 0, 1, NULL) [Registros], " +
                                            " dbo.fn_Numero_Formata(ISNULL(a.Inclusao, 0), 0, 1, NULL) [Inclusao], " +
                                            " dbo.fn_Numero_Formata(ISNULL(a.Alteracao, 0), 0, 1, NULL) [Alteracao], " +
                                            " dbo.fn_Numero_Formata(ISNULL(a.Erros, 0), 0, 1, NULL) [Erros], " +
                                            "(CASE a.StatusID WHEN 0 THEN 'Pendente' WHEN 1 THEN 'Processando' WHEN 2 THEN 'Processado' WHEN 3 THEN 'Arquivo invalido' END) [Status], " +
                                            "a.dtProcessamento, dbo.fn_Pessoa_Fantasia(a.UsuarioID, 0) [Usuario], a.Descricao " +
	                                    "FROM Cotador_Atualizacoes a WITH(NOLOCK) " +
	                                    "WHERE a.MarcaID = " + nMarcaID + " " + sfiltro + " " +
                                        "ORDER BY a.dtProcessamento";

        dsoGridAtualizacoes.ondatasetcomplete = fillGridDataAtualizacoes_DSC;
        dsoGridAtualizacoes.Refresh();
    }
    else
    {
        if (window.top.overflyGen.Alert('Selecione uma marca !') == 0)
            return null;
    }
}

function fillGridDataProdutos(bListOk, bNewItem) {
    var sFiltro = "";
    var nMarcaID = selMarca.value;
    var nOrigemID = selOrigem.value;
    var nEstadoID = selEstado.value;
    var nNCM = selNCM.value;
    var sArgumento = txtArgumento.value;
    var sSelect = "";
    var sOrderBy = "";
    var bModalTravada;

    if (nMarcaID > 0)
    {
        btnOK.disabled = true;

        bModalTravada = modalWinIsLocked();

        if (!bModalTravada)
            lockControlsInModalWin(true);

        // zera o grid
        fg.Rows = 1;

        setConnection(dsoGridProdutos);

        if (!glb_bInclusaoProduto) {
            if (bNewItem) {
                sSelect = "TOP 1 CorpProdutoID, Produto [PartNumber], EstadoID, " +
                            "DescricaoProduto, OrigemID, " +
                            "TipoProdutoID, NCMID," +
                            "(SELECT SimboloMoeda " +
                                "FROM Conceitos WITH(NOLOCK) " +
                                "WHERE ConceitoID = MoedaID) [Moeda], PrecoLista, DescontoPadrao, ProdutoSubstituto, ProdutoVerTambem, dtVendaFim, Observacao, " +
                            "ISNULL(dbo.fn_CotadorAtualizacoes(CorpProdutoID), SPACE(0)) [Atualizacoes], CONVERT(BIT, 0) [DescontoEspecial], TaxaConversaoID, UsuarioID ";

                sOrderBy = "ORDER BY CorpProdutoID DESC";
            }
            else {
                if (bListOk) {
                    sSelect = "CorpProdutoID, Produto [PartNumber], EstadoID, " +
                                "DescricaoProduto, OrigemID, " +
                                "TipoProdutoID, NCMID, " +
                                "(SELECT SimboloMoeda " +
                                    "FROM Conceitos WITH(NOLOCK) " +
                                    "WHERE ConceitoID = MoedaID) [Moeda], PrecoLista, DescontoPadrao, ProdutoSubstituto, ProdutoVerTambem, dtVendaFim, Observacao, " +
                                "ISNULL(dbo.fn_CotadorAtualizacoes(CorpProdutoID), SPACE(0)) [Atualizacoes]," +
                                "(SELECT CONVERT(BIT, COUNT(1)) " +
                                    "FROM CorpDescontos aa WITH(NOLOCK) " +
                                    "WHERE aa.CorpProdutoID = CorpProdutos.CorpProdutoID) [DescontoEspecial], TaxaConversaoID, UsuarioID ";

                    sOrderBy = "ORDER BY Produto";
                }
                else
                    sSelect = "COUNT(1) [Resgistros] ";

                if (nOrigemID > 0)
                    sFiltro += " AND OrigemID " + (nOrigemID == 2 ? "IN (2701, 2706)" : "NOT IN (2701, 2706)") + " ";

                if (nEstadoID > 0)
                    sFiltro += "AND EstadoID = " + nEstadoID + " ";

                if (nNCM > 0) {
                    sFiltro += "AND NCMID = " + nNCM + " ";
                }

                if (sArgumento.length > 0)
                    sFiltro += "AND " + "Produto + DescricaoProduto LIKE '%" + sArgumento + "%' ";
            }

            dsoGridProdutos.SQL = "SELECT " + sSelect +
                                    "FROM CorpProdutos WITH(NOLOCK) " +
                                    "WHERE MarcaID = " + nMarcaID + " " + sFiltro +
                                    sOrderBy;
        }
        else {
            dsoGridProdutos.SQL = "SELECT 0 [CorpProdutoID], SPACE(30) [PartNumber], 1 [EstadoID], SPACE(30) [DescricaoProduto], SPACE(59) [OrigemID], SPACE(0) [TipoProdutoID], " +
                                            "SPACE(25) [NCMID], SPACE(0) [Moeda], SPACE(0) [PrecoLista], SPACE(0) [DescontoPadrao], SPACE(0) [ProdutoSubstituto], SPACE(0) [ProdutoVerTambem], " +
                                            "SPACE(0) [dtVendaFim], SPACE(0) [Observacao], SPACE(0) [Atualizacoes], CONVERT(BIT, 0) [DescontoEspecial], SPACE(0) [TaxaConversaoID], SPACE(0) [UsuarioID] ";
        }

        dsoGridProdutos.ondatasetcomplete = fillGridDataProdutos_DSC;
        dsoGridProdutos.Refresh();
    }
    else
    {
        if (window.top.overflyGen.Alert('Selecione uma marca !') == 0)
            return null;
    }
}

function fillGridDataDescontos(nCorpProdutoID) {

        btnOK.disabled = true;
        lockControlsInModalWin(true);

        // zera o grid
        fgDetalhes.Rows = 1;

        setConnection(dsoGridDescontos);

        dsoGridDescontos.SQL = "SELECT b.CorpDescontoID, ISNULL(a.Descricao, SPACE(0)) [Descricao], b.DescontoPercentual, " +
			                            "/*(c.PrecoLista * (1 - (dbo.fn_DivideZero(b.DescontoPercentual,100)))) [ValorDesconto]*/ b.DescontoValor, " +
			                            "b.dtInclusao, b.dtInicioDesconto, b.dtFimDesconto, dbo.fn_Pessoa_Fantasia(a.UsuarioID, 0) [Usuario] " +
                                    "FROM Cotador_Atualizacoes a WITH(NOLOCK) " +
                                        "INNER JOIN CorpDescontos b WITH(NOLOCK) ON (b.CotAtualizacaoID = a.CotAtualizacaoID) " +
                                        "INNER JOIN CorpProdutos c WITH(NOLOCK) ON (c.CorpProdutoID = b.CorpProdutoID) " +
                                    "WHERE c.CorpProdutoID = " + nCorpProdutoID;

        dsoGridDescontos.ondatasetcomplete = fillGridDataDescontos_DSC;
        dsoGridDescontos.Refresh();
}

function fillGridDataLogs(nCorpProdutoID) {

    btnOK.disabled = true;
    lockControlsInModalWin(true);

    // zera o grid
    fgDetalhes.Rows = 1;

    setConnection(dsoGridLogs);

    dsoGridLogs.SQL = "SELECT a.LOGID, dbo.fn_Data_Fuso(a.Data, NULL, " + glb_nEmpresaData[2] + ") [DataFuso], dbo.fn_Pessoa_Fantasia(a.UsuarioID, 0) [Colaborador], " +
		                            "dbo.fn_TipoAuxiliar_Item(a.EventoID, 2) [Evento], c.RecursoAbreviado [Estado], b.Campo, (CASE a.EventoID WHEN 28 THEN SPACE(1) ELSE b.Conteudo END) [Conteudo], " +
		                            "dbo.fn_Log_Mudanca(b.LogDetalheID) [Mudanca] " +
	                            "FROM LOGs a WITH(NOLOCK) " +
		                            "INNER JOIN LOGs_Detalhes b WITH(NOLOCK) ON (b.LOGID = a.LOGID) " +
		                            "INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = a.EstadoID) " +
	                            "WHERE a.FormID = " + glb_nFormID + " AND SubFormID = 40014 AND  a.RegistroID = " + nCorpProdutoID + " " +
	                            "ORDER BY a.SubFormID, a.Data DESC ";

    dsoGridLogs.ondatasetcomplete = fillGridDataLogs_DSC;
    dsoGridLogs.Refresh();
}

//Retorno do servidor de solicita��o dos dados dos grids ********************
function fillGridDataAtualizacoes_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm:ss';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm:ss';

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '9';
    fg.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['CotAtualizacaoID',//0
                    'DocumentoID',//1
                    'Tipo',//2
                    'Marca',//3
                    'Arquivo',//4
                    'Reg',//5
                    'Inc',//6
                    'Alt',//7
                    'Erros',//8
                    'Status',//9
                    'Data',//10
                    'Usu�rio',//11
                    'Descri��o         '], [0, 1, 2, 3]);//12

    glb_aCelHint = [[0, 5, 'Registros'],
                    [0, 6, 'Inclus�es'],
                    [0, 7, 'Altera��es'],
                    [0, 10, 'Data de processamento']];

    fillGridMask(fg, dsoGridAtualizacoes, ['CotAtualizacaoID*',
                               'DocumentoID*',
                               'Tipo*',
				               'Marca*',
				               'Arquivo*',
				               'Registros*',
                               'Inclusao*',
				               'Alteracao*',
				               'Erros*',
				               'Status*',
				               'dtProcessamento*',
				               'Usuario*',
                               'Descricao'],
							 ['', '', '', '', '', '', '', '', '', '', '99/99/9999', '', ''],
							 ['', '',  '', '', '', '', '', '', '', '', dTFormat, '', '']);

    fg.Redraw = 0;

    // alinha as colunas a direita
    alignColsInGrid(fg, [0, 1, 5, 6, 7, 8]);

    //Congela Coluna
    fg.FrozenCols = 5;

    //fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 2);

    fg.ExplorerBar = 5;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        fg.Editable = true;
        btnDownload.disabled = false;
        window.focus();
        fg.focus();
    }

    btnGravar.disabled = true;

    /*
    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    */

    fg.Redraw = 2;
}

function fillGridDataProdutos_DSC()
{
    if (dsoGridProdutos.recordset.fields.count > 1)
    {
        var dTFormat = '';

        if (DATE_FORMAT == "DD/MM/YYYY")
            dTFormat = 'dd/mm/yyyy';
        else if (DATE_FORMAT == "MM/DD/YYYY")
            dTFormat = 'mm/dd/yyyy';

        fg.Redraw = 0;
        fg.Editable = false;
        startGridInterface(fg);

        fg.FontSize = '9';
        fg.FrozenCols = 0;

        // Controla se o grid esta em construcao
        glb_GridIsBuilding = true;

        headerGrid(fg, ['CorpProdutoID',//0
				        'PartNumber',//1
				        'Est',//2
				        'Produto',//3
				        'Origem',//4
				        'Tipo',//5
				        'NCM',//6
				        'Moeda',//7
				        'Pre�o',//8
				        'Desconto',//9
				        'Substituto',//10
				        'Ver Tamb�m',//11
				        'CVP Fim',//12
				        'Observa��o',//13
				        'Atualizacoes', //14
				        'DE', //15
				        'TaxaConv', //16
				        'UsuarioID'], [0, glb_bInclusaoProduto ? 7 : 0, 14, glb_bInclusaoProduto ? 15 : 0, 17]);//17

        glb_aCelHint = [[0, 2, 'Estado do Produto'],
                        [0, 5, 'Tipo de produto'],
                        [0, 6, 'Classifica��o fiscal'],
                        [0, 10, 'Produto Substituto'],
                        [0, 12, 'Data limite para venda'],
                        [0, 15, 'Desconto Especial']];

        fillGridMask(fg, dsoGridProdutos, ['CorpProdutoID*',
                                            'PartNumber' + (!glb_bInclusaoProduto ? '*' : ''),
                                            'EstadoID',
                                            'DescricaoProduto' + (!glb_bInclusaoProduto ? '*' : ''),
                                            'OrigemID',
                                            'TipoProdutoID',
                                            'NCMID',
                                            'Moeda' + (!glb_bInclusaoProduto ? '*' : ''),
                                            'PrecoLista',
                                            'DescontoPadrao',
                                            'ProdutoSubstituto',
                                            'ProdutoVerTambem',
                                            'dtVendaFim' + (!glb_bInclusaoProduto ? '*' : ''),
                                            'Observacao',
                                            'Atualizacoes*',
                                            'DescontoEspecial',
                                            'TaxaConversaoID',
                                            'UsuarioID'],
                                 ['', '', '', '', '', '', '', '', '99999999.99', '99.99', '', '', '99/99/9999', '', '', '', '', ''],
                                 ['', '', '', '', '', '', '', '', '###,###,##0.00', '##.00', '', '', dTFormat, '', '', '', '', '']);

        // alinha as colunas a direita
        alignColsInGrid(fg, [0, 8, 9]);

        for (i = 1; i < fg.Rows; i++) {

            // pinta de cinza
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'EstadoID')) == 2 || fg.ValueMatrix(i, getColIndexByColKey(fg, 'EstadoID')) == 4) {
                fg.Cell(6, i, getColIndexByColKey(fg, 'OrigemID'), i, getColIndexByColKey(fg, 'OrigemID')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'TipoProdutoID'), i, getColIndexByColKey(fg, 'TipoProdutoID')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'NCMID'), i, getColIndexByColKey(fg, 'NCMID')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'PrecoLista'), i, getColIndexByColKey(fg, 'PrecoLista')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'DescontoPadrao'), i, getColIndexByColKey(fg, 'DescontoPadrao')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'ProdutoSubstituto'), i, getColIndexByColKey(fg, 'ProdutoSubstituto')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'ProdutoVerTambem'), i, getColIndexByColKey(fg, 'ProdutoVerTambem')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'Observacao'), i, getColIndexByColKey(fg, 'Observacao')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'TaxaConversaoID'), i, getColIndexByColKey(fg, 'TaxaConversaoID')) = 0XDCDCDC;
                fg.Cell(6, i, getColIndexByColKey(fg, 'UsuarioID'), i, getColIndexByColKey(fg, 'UsuarioID')) = 0XDCDCDC;
            }
        }

        if (glb_bInclusaoProduto)
        {
            insertcomboData(fg, getColIndexByColKey(fg, 'Moeda'), dsoCmbGridProdMoeda, 'Moeda', 'MoedaID');
        }

        insertcomboData(fg, getColIndexByColKey(fg, 'EstadoID'), dsoCmbGridProdEstado, 'Estado', 'EstadoID');
        insertcomboData(fg, getColIndexByColKey(fg, 'NCMID'), dsoNCMGrid, 'NCM', 'NCMID');
        insertcomboData(fg, getColIndexByColKey(fg, 'OrigemID'), dsoCmbGridProdOrigem, 'Origem', 'OrigemID');
        insertcomboData(fg, getColIndexByColKey(fg, 'TipoProdutoID'), dsoCmbGridProdTipo, 'Tipo', 'TipoProdutoID');
        insertcomboData(fg, getColIndexByColKey(fg, 'TaxaConversaoID'), dsoCmbGridTaxaConv, 'TaxaConversao', 'TaxaConversaoID');

        fg.Redraw = 0;

        //Congela Coluna
        fg.FrozenCols = 3;

        fg.ExplorerBar = 5;

        // Controla se o grid esta em construcao
        glb_GridIsBuilding = false;

        // se tem linhas no grid, coloca foco no grid
        if (fg.Rows > 1)
        {
            pintaCelula();
            fg.Editable = true;
            window.focus();
            fg.focus();
        }

        fg.Redraw = 2;
    }
    else
    {
        var nRegistros = dsoGridProdutos.recordset.aRecords[0][0];

        if (nRegistros > 300)
        {
            if (window.top.overflyGen.Alert('Pesquisa retornou mais de 300 registros. Favor especificar melhor sua busca.') == 0)
                return null;
        }
        else
        {
            fillGridDataProdutos(true, false);
            return null;
        }
    }

    //Reajusta altura
    fg.style.height = (parseInt(divFG.style.height, 10));

    //Reajuste de largura
    fg.style.width = (parseInt(divFG.style.Width, 10));

    // Define tamanho das coluna do grid
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    //esconde div e grid de detalhes
    divFGDetalhes.style.visibility = 'hidden';
    fgDetalhes.style.visibility = 'hidden';

    lockControlsInModalWin(false);
    btnGravar.disabled = true;

    if (glb_bInclusaoProduto)
        btnNovo.disabled = true;
    else
        btnNovo.disabled = false;
}

function fillGridDataDescontos_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fgDetalhes.Redraw = 0;
    fgDetalhes.Editable = false;
    startGridInterface(fgDetalhes);

    fgDetalhes.FontSize = '9';
    fgDetalhes.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fgDetalhes, ['CorpDescontoID',//0
							'Descri��o',//1
							'% Desconto',//2
							'Valor Desconto',//3
							'Inclus�o',//4
							'Vig�ncia In�cio',//5
							'Vig�ncia Fim',//6
							'Usu�rio'], [0]);//7

    glb_aCelHint = [[0, 1, 'Tipo de Desconto'],
                    [0, 2, 'Percentual de Desconto'],
                    [0, 5, 'In�cio de vig�ncia'],
                    [0, 6, 'Fim de vig�ncia']];

    fillGridMask(fgDetalhes, dsoGridDescontos, ['CorpDescontoID*',
                                                    'Descricao*',
                                                    'DescontoPercentual*',
                                                    'DescontoValor*',
                                                    'dtInclusao*',
                                                    'dtInicioDesconto*',
                                                    'dtFimDesconto*',
                                                    'Usuario*'],
												 ['', '', '99.99', '99999999.99', '99/99/9999', '99/99/9999', '99/99/9999', ''],
												 ['', '', '##.00', '###,###,##0.00', dTFormat, dTFormat, dTFormat,  '']);

    fgDetalhes.AutoSizeMode = 0;
    fgDetalhes.AutoSize(0, fgDetalhes.Cols - 1);

    alignColsInGrid(fgDetalhes, [2, 3]);

    //Permite ordena��o de grid
    fgDetalhes.ExplorerBar = 5;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fgDetalhes.Rows > 1)
    {
        //Reajusta altura
        fg.style.height = (parseInt(divFG.style.height, 10) - 140);

        divFGDetalhes.style.visibility = 'inherit';
        fgDetalhes.style.visibility = 'inherit';

        window.focus();
        fgDetalhes.focus();
    }
    else
    {
        //Reajusta altura
        fg.style.height = (parseInt(divFG.style.height, 10));

        divFGDetalhes.style.visibility = 'hidden';
        fgDetalhes.style.visibility = 'hidden';

        if (window.top.overflyGen.Alert('N�o h� hist�rico de descontos para este item.') == 0)
        {
            return null;
        }
    }

    lockControlsInModalWin(false);

    fgDetalhes.Redraw = 2;
}

function fillGridDataLogs_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fgDetalhes.Redraw = 0;
    fgDetalhes.Editable = false;
    startGridInterface(fgDetalhes);

    fgDetalhes.FontSize = '9';
    fgDetalhes.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fgDetalhes, ['LOGID',//0
							   'Data           Hora     ',
							   'Colaborador',
							   'Evento',
							   'Est',
							   'Campo',
							   'De',
							   'Para'], [0]);

    fillGridMask(fgDetalhes, dsoGridLogs, ['LOGID*',
                                            'DataFuso*',
                                            'Colaborador*',
                                            'Evento*',
                                            'Estado*',
                                            'Campo*',
                                            'Conteudo*',
                                            'Mudanca*'],
							 ['', '99/99/9999', '', '', '', '', '', ''],
							 ['', dTFormat + ' hh:mm:ss', '', '', '', '' , '', '']);

    fgDetalhes.AutoSizeMode = 0;
    fgDetalhes.AutoSize(0, fgDetalhes.Cols - 1);

    //Permite ordena��o de grid
    fgDetalhes.ExplorerBar = 5;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fgDetalhes.Rows > 1)
    {
        //Reajusta altura
        fg.style.height = (parseInt(divFG.style.height, 10) - 140);
        
        divFGDetalhes.style.visibility = 'inherit';
        fgDetalhes.style.visibility = 'inherit';
        window.focus();
        fgDetalhes.focus();
    }

    else
    {
        //Reajusta altura
        fg.style.height = (parseInt(divFG.style.height, 10));

        if (window.top.overflyGen.Alert('N�o h� hist�rico de altera��es para este item.') == 0)
        {
            divFGDetalhes.style.visibility = 'hidden';
            fgDetalhes.style.visibility = 'hidden';
            return null;
        }
    }

    lockControlsInModalWin(false);

    fgDetalhes.Redraw = 2;
}

//Uploads de arquivos *****************************************************
function importarArquivo() {

    divUpload.style.visibility = 'inherit';
    btnVoltar.style.visibility = 'inherit';
    //selTipoArquivo.style.visibility = 'inherit';
    //lblTipoArquivo.style.visibility = 'inherit';
    btnUpload.style.visibility = 'hidden';
    btnDownload.style.visibility = 'hidden';

    var iframe = document.getElementById("I1");
    //Documento
    var nTipoArquivoID = 1454;
    var nTipoArquivoCotadorID = selTipoArquivo.value;
    var nMarcaID = selMarca.value;

    strPars = '?nUserID=' + escape(glb_nUserID);
    strPars += '&nTipoArquivoID=' + escape(nTipoArquivoID);
    strPars += '&nTipoArquivoCotadorID=' + escape(nTipoArquivoCotadorID);
    strPars += '&nMarcaID=' + escape(nMarcaID);
    strPars += '&nFormID=' + escape(glb_nCurrFormID);
    strPars += '&nSubFormID=' + escape(40014);

    window.open(SYS_ASPURLROOT + '/serversidegenEx/importararquivoscotador.aspx' + strPars, 'I1');
}

//Downloads de arquivos
function Download() {

    var strPars;
    var nFileId;
    nFileId = fg.textMatrix(fg.row, getColIndexByColKey(fg, 'DocumentoID*'));
    strPars = '?nFileID=' + escape(nFileId);
    window.location = SYS_ASPURLROOT + '/serversidegenEx/arquivodownload.aspx' + strPars;
}

//Salva as linhas alteradas nos grids ***************************************
function saveDataInGridAtualizacoes() {
    var i = 0;
    glb_dataGridWasChanged = false;
    lockControlsInModalWin(true);

    try {
        dsoGridAtualizacoes.SubmitChanges();
    }
    catch (e) {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887) {
            if (window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0)
                return null;
        }

        fg.Rows = 1;
        lockControlsInModalWin(false);

        glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(false)', 50, 'JavaScript');
        return null;
    }

    dsoGridAtualizacoes.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGridAtualizacoes.Refresh();
}

function saveDataInGridProdutos() {
    var i = 0;
    glb_dataGridWasChanged = false;
    lockControlsInModalWin(true);

    if (!glb_bInclusaoProduto)
    {
        try
        {
            dsoGridProdutos.SubmitChanges();
        }
        catch (e)
        {
            // Numero de erro qdo o registro foi alterado ou removido por outro usuario
            if (e.number == -2147217887)
            {
                if (window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0)
                    return null;
            }

            fg.Rows = 1;
            lockControlsInModalWin(false);

            glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(false)', 50, 'JavaScript');
            return null;
        }

        dsoGridProdutos.ondatasetcomplete = saveDataInGrid_DSC;
        dsoGridProdutos.Refresh();
    }
    //Novo item
    else
    {
        var strPars = '';
        var sPartNumber;
        var nEstadoID;
        var sProduto;
        var nMarcaID;
        var nOrigemID;
        var nTipoProdutoID;
        var nNCMID;
        var nMoedaID;
        var nPrecoLista;
        var nDesconto;
        var sSubstituto;
        var sVerTambem;
        var sCVPFim;
        var sObservacao;
        var nUsuarioID;
        var sMensagem;
        var nTaxaConversaoID;

        lockControlsInModalWin(true);

        sPartNumber = getCellValueByColKey(fg, 'PartNumber', fg.Row);
        nEstadoID = getCellValueByColKey(fg, 'EstadoID', fg.Row);
        sProduto = getCellValueByColKey(fg, 'DescricaoProduto', fg.Row);
        nMarcaID = selMarca.value;
        nOrigemID = getCellValueByColKey(fg, 'OrigemID', fg.Row);
        nTipoProdutoID = getCellValueByColKey(fg, 'TipoProdutoID', fg.Row);
        nNCMID = getCellValueByColKey(fg, 'NCMID', fg.Row);
        nMoedaID = ((nOrigemID == 2701 || nOrigemID == 2706) ? 541 : 647);
        nPrecoLista = getCellValueByColKey(fg, 'PrecoLista', fg.Row);
        nDesconto = getCellValueByColKey(fg, 'DescontoPadrao', fg.Row);
        sSubstituto = getCellValueByColKey(fg, 'ProdutoSubstituto', fg.Row);
        sVerTambem = getCellValueByColKey(fg, 'ProdutoVerTambem', fg.Row);
        sCVPFim = getCellValueByColKey(fg, 'dtVendaFim', fg.Row);
        sObservacao = getCellValueByColKey(fg, 'Observacao', fg.Row);
        nUsuarioID = getCurrUserID();
        sMensagem = '';
        nTaxaConversaoID = getCellValueByColKey(fg, 'TaxaConversaoID', fg.Row);

        if ((sPartNumber == null) || (sPartNumber.length == 0))
            sMensagem += 'O campo PartNumber deve ser preenchido.' + '\n';

        if ((sProduto == null) || (sProduto.length == 0))
            sMensagem += 'O campo Produto deve ser preenchido.' + '\n';

        if (nOrigemID.length == 0)
            sMensagem += 'O campo Origem deve ser preenchido.' + '\n';

        if (nTipoProdutoID.length == 0)
            sMensagem += 'O campo Tipo deve ser preenchido.' + '\n';

        if ((nNCMID == 0) && (nTipoProdutoID != 1134) && (nTipoProdutoID != 1133))
            sMensagem += 'O campo NCM deve ser preenchido.' + '\n';

        if ((nPrecoLista == null) || (nPrecoLista.length == 0) || ((nPrecoLista <= 0) && (nTipoProdutoID == 1132)))
        {
            if (nTipoProdutoID == 1132)
                sMensagem += 'O campo Pre�o deve ser preechido e maior que zero.';
            else
                sMensagem += 'O campo Pre�o deve ser preechido.';
        }
        else
            nPrecoLista = nPrecoLista.replace(",", ".");

        if ((nDesconto == null) || (nDesconto.length == 0))
            nDesconto = null;
        else
            nDesconto = nDesconto.replace(",", ".");

        if ((sSubstituto == null) || (sSubstituto.length == 0))
            sSubstituto = null;

        if ((sObservacao == null) || (sObservacao.length == 0))
            sObservacao = null;

        if (sMensagem.length > 0)
        {
            if (window.top.overflyGen.Alert(sMensagem) == 1)
            {
                lockControlsInModalWin(false);
                return null;
            }
        }

        if (nNCMID == 0)
            nNCMID = '';

        if (sCVPFim != '') {
            if (criticAndNormTxtDataTime(sCVPFim, 'CVP Fim'))
                sCVPFim = normalizeDate_DateTime(sCVPFim, true);
            else {
                lockControlsInModalWin(false);
                return null;
            }
        }
        

        strPars += '?sPartNumber=' + escape(sPartNumber);
        strPars += '&nEstadoID=' + escape(nEstadoID);
        strPars += '&sProduto=' + escape(sProduto);
        strPars += '&nMarcaID=' + escape(nMarcaID);
        strPars += '&nOrigemID=' + escape(nOrigemID);
        strPars += '&nTipoProdutoID=' + escape(nTipoProdutoID);
        strPars += '&nNCMID=' + escape(nNCMID);
        strPars += '&nMoedaID=' + escape(nMoedaID);
        strPars += '&nPrecoLista=' + escape(nPrecoLista);
        strPars += '&nDesconto=' + escape(nDesconto);
        strPars += '&sSubstituto=' + escape(sSubstituto);
        strPars += '&sVerTambem=' + escape(sVerTambem);
        strPars += '&sCVPFim=' + escape(sCVPFim);
        strPars += '&sObservacao=' + escape(sObservacao);
        strPars += '&nUsuarioID=' + escape(nUsuarioID);
        strPars += '&nTaxaConversaoID=' + escape(nTaxaConversaoID);

        try
        {
            dsoGridProdutosInclusao.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/gravarprodutocotador.aspx' + strPars;
            dsoGridProdutosInclusao.ondatasetcomplete = saveDataInGrid_DSC;
            dsoGridProdutosInclusao.refresh();
        }
        catch (e)
        {
            if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            {
                lockControlsInModalWin(false);
                return null;
            }
        }
    }
}

//Retorno do servidor, apos salvar as linhas alteradas nos grids ************
function saveDataInGrid_DSC() {

    if (glb_bInclusaoProduto)
    {
        var msg = "";

        try{
            msg = dsoGridProdutosInclusao.recordset.fldresp.value;
        }
        catch(e){
            msg = "";
        }

        if (msg.indexOf("Taxa de convers�o deve ser utilizada apenas para Servi�o") >= 0 ) {
            window.top.overflyGen.Alert("Taxa de convers�o deve ser utilizada apenas para Servi�o");
            glb_bInclusaoProduto = false;
        }
        else if (msg.indexOf("Produto em duplicidade.") >= 0) {
            window.top.overflyGen.Alert("Produto em duplicidade.");
            glb_bInclusaoProduto = false;
        }
        else {
            glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(true)', 50, 'JavaScript');

            //zera filros
            selNCM.selectedIndex = -1;
            selEstado.selectedIndex = -1;
            selOrigem.selectedIndex = -1;
            txtArgumento.value = "";

            glb_bInclusaoProduto = false;
            btnNovo.disabled = false;
        }
    }
    else
        glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(false)', 50, 'JavaScript');

    lockControlsInModalWin(false);
}

function afterEdit_gridAtualizacoes(Row, Col) {
    var nType;
    var sColunaAtual = '';

    if (fg.Editable)
    {
        sColunaAtual = replaceStr(fg.ColKey(Col), '*', '');

        // Altera o campo do dso com o novo valor
        dsoGridAtualizacoes.recordset.moveFirst();

        dsoGridAtualizacoes.recordset.Find('CotAtualizacaoID', fg.TextMatrix(Row, getColIndexByColKey(fg, 'CotAtualizacaoID')));

        if (!(dsoGridAtualizacoes.recordset.EOF))
        {
            if ((dsoGridAtualizacoes.recordset[sColunaAtual].value) != (fg.TextMatrix(Row, Col)))
            {
                dsoGridAtualizacoes.recordset[sColunaAtual].value = fg.TextMatrix(Row, Col);
                glb_dataGridWasChanged = true;
            }
        }

        btnGravar.disabled = false;
    }
}

function afterEdit_gridProdutos(Row, Col) {
    var nType;
    var nUsuarioID = getCurrUserID();
    var sColunaAtual = '';

    if (fg.Editable)
    {
        sColunaAtual = replaceStr(fg.ColKey(Col), '*', '');

        //Trata campos numeircs
        if ((fg.col == getColIndexByColKey(fg, 'PrecoLista')) || (fg.col == getColIndexByColKey(fg, 'DescontoPadrao')))
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

        // Altera o campo do dso com o novo valor
        dsoGridProdutos.recordset.moveFirst();

        dsoGridProdutos.recordset.Find('CorpProdutoID', fg.TextMatrix(Row, getColIndexByColKey(fg, 'CorpProdutoID')));

        //Grava UsuarioID
        if ((fg.TextMatrix(Row, getColIndexByColKey(fg, 'UsuarioID'))) != nUsuarioID)
        {
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'UsuarioID')) = nUsuarioID;

            dsoGridProdutos.recordset[fg.ColKey(getColIndexByColKey(fg, 'UsuarioID'))].value = nUsuarioID;
        }

        //N�o permite grava��o do DE
        if ((dsoGridProdutos.recordset[fg.ColKey(getColIndexByColKey(fg, 'DescontoEspecial'))].value) != (fg.TextMatrix(Row, getColIndexByColKey(fg, 'DescontoEspecial'))))
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'DescontoEspecial')) = dsoGridProdutos.recordset[fg.ColKey(getColIndexByColKey(fg, 'DescontoEspecial'))].value;

        if (!(dsoGridProdutos.recordset.EOF))
        {
            //Trata campos text
            if ((fg.col == getColIndexByColKey(fg, 'ProdutoSubstituto')) || (fg.col == getColIndexByColKey(fg, 'Observacao')) || (fg.col == getColIndexByColKey(fg, 'ProdutoVerTambem')))
            {
                if ((dsoGridProdutos.recordset[sColunaAtual].value) != (fg.TextMatrix(Row, Col)))
                {
                    dsoGridProdutos.recordset[sColunaAtual].value = fg.TextMatrix(Row, Col);
                    glb_dataGridWasChanged = true;
                }
            }
            else
            {
                if ((dsoGridProdutos.recordset[sColunaAtual].value) != (fg.ValueMatrix(Row, Col)))
                {
                    dsoGridProdutos.recordset[sColunaAtual].value = fg.ValueMatrix(Row, Col);
                    glb_dataGridWasChanged = true;
                }
            }
        }


        if (Col != getColIndexByColKey(fg, 'DescontoEspecial'))
            btnGravar.disabled = false;
    }
}

//Inclus�o de itens no grid de produtos
function startDynamicCmbsGridsProdutos()
{
    // parametrizacao do dso CmbGridProdutos
    setConnection(dsoCmbGridProdOrigem);

    dsoCmbGridProdOrigem.SQL = "SELECT DISTINCT a.ItemID AS OrigemID, a.Observacao AS Origem " +
	                                "FROM TiposAuxiliares_Itens a WITH(NOLOCK)"  +
	                                "WHERE (TipoID = 114 AND EstadoID = 2) " +
	                                "ORDER BY Origem "; 

    dsoCmbGridProdOrigem.ondatasetcomplete = startDynamicCmbsGridsProdutos_DSC;
    dsoCmbGridProdOrigem.Refresh();

    // parametrizacao do dso CmbGridProdTipo
    setConnection(dsoCmbGridProdTipo);

    dsoCmbGridProdTipo.SQL = "SELECT DISTINCT a.ItemID AS TipoProdutoID, a.ItemMasculino AS Tipo " +
                                "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " +
                                "WHERE (a.TipoID = 60 AND a.EstadoID = 2)  " +
	                            "ORDER BY Tipo ";

    dsoCmbGridProdTipo.ondatasetcomplete = startDynamicCmbsGridsProdutos_DSC;
    dsoCmbGridProdTipo.Refresh();

    // parametrizacao do dso CmbGridProdMoeda
    setConnection(dsoCmbGridProdMoeda);

    dsoCmbGridProdMoeda.SQL = "SELECT 0 AS MoedaID, SPACE(0) AS Moeda " +
                              "UNION ALL " +
                              "SELECT ConceitoID AS MoedaID, ConceitoAbreviado AS Moeda " +
                                "FROM Conceitos WITH(NOLOCK) " +
                                "WHERE ConceitoID IN (541, 647)" ;

    dsoCmbGridProdMoeda.ondatasetcomplete = startDynamicCmbsGridsProdutos_DSC;
    dsoCmbGridProdMoeda.Refresh();

    setConnection(dsoCmbGridProdEstado);

    dsoCmbGridProdEstado.SQL = "SELECT RecursoID AS EstadoID, RecursoAbreviado AS Estado " +
                                    "FROM Recursos " +
                                    "WHERE TipoRecursoID = 7 AND EstadoID = 2 AND RecursoID IN (1, 2, 4) " +
                                    "ORDER BY EstadoID ";

    dsoCmbGridProdEstado.ondatasetcomplete = startDynamicCmbsGridsProdutos_DSC;
    dsoCmbGridProdEstado.Refresh();

    CmbGridTaxaConv();
}

function CmbGridTaxaConv()
{
    // parametrizacao do dso CmbGridTaxaConv
    setConnection(dsoCmbGridTaxaConv);

    dsoCmbGridTaxaConv.SQL = "DECLARE @TaxaConvTable TABLE(TaxaConversaoID INT, TaxaConversao VARCHAR(20)) " +
                             "DECLARE @TagValor VARCHAR(100), @Indice INT, @TaxaConversaoID INT, @TaxaConversao VARCHAR(20) " +
                             "SELECT @Indice = 1, @TagValor = '1' " +
                             "WHILE @TagValor <> '' " +
                             "BEGIN " +
                                "SET @TagValor = NULL " +
                                "SELECT @TagValor = dbo.fn_TagValor(Observacoes, 'TaxaConversao' , '<', '|', @Indice) " +
                                    "FROM Conceitos WITH(NOLOCK) " +
                                    "WHERE TipoConceitoID = 304 AND Observacoes LIKE '%<CotadorCorporativo>%'" + (selMarca.value == '' ? ' ' : "AND ConceitoID = " + selMarca.value + " ") +

                                "SELECT	@TaxaConversaoID = SUBSTRING(@TagValor,0,CHARINDEX(';',@TagValor,0)), " +
                                       "@TaxaConversao = SUBSTRING(@TagValor,CHARINDEX(';',@TagValor,0)+1,LEN(@TagValor)) " +

                                "IF(@TaxaConversao <> '') " +
                                    "INSERT INTO @TaxaConvTable(TaxaConversaoID, TaxaConversao) VALUES(@TaxaConversaoID, CONVERT(VARCHAR(20),CONVERT(NUMERIC(11,2),@TaxaConversao))) " +

                                "SET @Indice += 1 " +
                              "END " +
                              "SELECT 0 AS TaxaConversaoID, ' ' AS TaxaConversao " +
                              "UNION ALL " +
                              "SELECT TaxaConversaoID, TaxaConversao AS TaxaConversao FROM @TaxaConvTable";


    dsoCmbGridTaxaConv.ondatasetcomplete = startDynamicCmbsGridsProdutos_DSC;
    dsoCmbGridTaxaConv.Refresh();
}

function startDynamicCmbsGridsProdutos_DSC()
{
    ;
}

//Trata campos datetime ****************************************************
function criticAndNormTxtDataTime(ctlRef, sCampo) {
    var retVal = false;

    // trima conteudo do campo ctlRef
    ctlRef = trimStr(ctlRef);

    // 1. Campo em branco nao aceita
    if ((ctlRef == '') || (ctlRef == null)) {
        if (window.top.overflyGen.Alert('O campo ' + sCampo + ' est� em branco...') == 0)
            return null;

        lockControlsInModalWin(false);
        return retVal;
    }

    dataCtlRef = ctlRef;

    // A funcao chkDataEx valida o campos se ele e uma data valida
    if (chkDataEx(dataCtlRef, true) == false) {
        if (window.top.overflyGen.Alert('O campo ' + sCampo + ' n�o � v�lido...') == 0)
            return null;

        lockControlsInModalWin(false);
        return retVal;
    }

    // Normaliza o conteudo do campo ctlRef
    ctlRef = normalizeDate_DateTime(ctlRef);

    if ((ctlRef != null) && (ctlRef != ''))
        retVal = true;

    return retVal;
}

//Fun��o que pinta as c�lulas do Grid **************************************
function pintaCelula()
{
    var nCorLaranja = 0X60A4F4;

    for (var contador = 1; contador < fg.Rows; contador++)
    {
        var sCampos = fg.TextMatrix(contador, getColIndexByColKey(fg, 'Atualizacoes*'));
        var sCampo;

        if (sCampos.length > 0)
        {
            if ((sCampos.indexOf(";")) == 0)
            {
                sCampo = (sCampos + (!glb_bInclusaoProduto ? '*' : ''));
                fg.Cell(6, contador, getColIndexByColKey(fg, sCampo), contador, getColIndexByColKey(fg, sCampo)) = nCorLaranja;
            }
            //faz split
            else
            {
                while (sCampos.length > 0)
                {
                    sCampo = ((sCampos.substring(0, sCampos.indexOf(";") > 0 ? sCampos.indexOf(";") : sCampos.length)) + (!glb_bInclusaoProduto ? '*' : ''));
                    fg.Cell(6, contador, getColIndexByColKey(fg, sCampo), contador, getColIndexByColKey(fg, sCampo)) = nCorLaranja;
                    sCampos = (sCampos.replace(sCampos.substring(0, sCampos.indexOf(";") > 0 ? sCampos.indexOf(";") + 1 : sCampos.length), ""));
                }
            }
        }
    }
}

//Digitar no txtArgumento ***************************************************
function Argumento_onkeypress() {
    // zera o grid
    fg.Rows = 1;

    if (event.keyCode == 13)
    {
        fillGridDataProdutos(false, false);
    }
}

//Eventos do grid *********************************************************
function js_modalcotador_AfterEdit(Row, Col)
{
    //Atualiza��es
    if (glb_bModoAtualizacao)
        afterEdit_gridAtualizacoes(Row, Col);
    //Produtos
    else
        afterEdit_gridProdutos(Row, Col);
}

function js_modalcotador_AfterRowColChange(oldRow, oldCol, newRow, newCol) {
    fg_AfterRowColChange();

    if (divFGDetalhes.style.visibility == 'hidden');
    {
        //Reajusta altura
        fg.style.height = (parseInt(divFG.style.height, 10));

        //esconde div e grid de detalhes
        divFGDetalhes.style.visibility = 'hidden';
        fgDetalhes.style.visibility = 'hidden';
    }
}

function js_modalcotadorBeforeEdit(grid, row, col)
{
    if (getColIndexByColKey(fg, 'PartNumber' + (!glb_bInclusaoProduto ? '*' : '')) == col)
        grid.EditMaxLength = 18;
    else if (getColIndexByColKey(fg, 'DescricaoProduto' + (!glb_bInclusaoProduto ? '*' : '')) == col)
        grid.EditMaxLength = 80;
    else if (getColIndexByColKey(fg, 'NCM') == col)
        grid.EditMaxLength = 25;
    else if (getColIndexByColKey(fg, 'ProdutoSubstituto') == col)
        grid.EditMaxLength = 18;
    else if (getColIndexByColKey(fg, 'Observacao') == col)
        grid.EditMaxLength = 100;
}

function js_modalcotador_ValidateEdit() {
    ;
}

function js_modalcotadorKeyPress(KeyAscii) {
    ;
}

function js_fg_modalcotadorDblClick(grid, Row, Col)
{
    if (!glb_bModoAtualizacao)
    {    
        if (fg.Rows > 1)
        {
            var nCorpProdutoID = fg.textMatrix(Row, getColIndexByColKey(fg, 'CorpProdutoID*'));

            if (getColIndexByColKey(fg, 'DescontoEspecial') == Col)
                fillGridDataDescontos(nCorpProdutoID);
            else
                fillGridDataLogs(nCorpProdutoID);
        }
    }
}

function js_fg_modalcotadorBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    return true;
}

function fg_MouseDown_cotador(nRow, nCol)
{
    ;
}

function js_fg_modalcotadorMouseMove(grid, Button, Shift, X, Y)
{
    ;
}

function js_modalgerenciarprodutoscotador_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    if (selModo.value == 1)
    {
        var nColCorpProdutoID = getColIndexByColKey(fg, 'CorpProdutoID*');
        var nColPartNumber = getColIndexByColKey(fg, 'PartNumber' + (!glb_bInclusaoProduto ? '*' : ''));
        var nColDescricaoProduto = getColIndexByColKey(fg, 'DescricaoProduto' + (!glb_bInclusaoProduto ? '*' : ''));
        var nColOrigemID = getColIndexByColKey(fg, 'OrigemID');
        var nColTipoProdutoID = getColIndexByColKey(fg, 'TipoProdutoID');
        var nColNCMID = getColIndexByColKey(fg, 'NCMID');
        var nColMoeda = getColIndexByColKey(fg, 'Moeda' + (!glb_bInclusaoProduto ? '*' : ''));
        var nColPrecoLista = getColIndexByColKey(fg, 'PrecoLista');
        var nColDescontoPadrao = getColIndexByColKey(fg, 'DescontoPadrao');
        var nColProdutoSubstituto = getColIndexByColKey(fg, 'ProdutoSubstituto');
        var nColProdutoVerTambem = getColIndexByColKey(fg, 'ProdutoVerTambem');
        var nColdtVendaFim = getColIndexByColKey(fg, 'dtVendaFim' + (!glb_bInclusaoProduto ? '*' : ''));
        var nColObservacao = getColIndexByColKey(fg, 'Observacao');
        var nColAtualizacoes = getColIndexByColKey(fg, 'Atualizacoes');
        var nColTaxaConversaoID = getColIndexByColKey(fg, 'TaxaConversaoID');
        var nColUsuarioID = getColIndexByColKey(fg, 'UsuarioID');

        if (!glb_GridIsBuilding)
        {
            if (glb_FreezeRolColChangeEvents)
                return true;

            if (((fg.ValueMatrix(NewRow, getColIndexByColKey(fg, 'EstadoID')) == 2 || fg.ValueMatrix(NewRow, getColIndexByColKey(fg, 'EstadoID')) == 4)) &&
                 ((NewCol == nColCorpProdutoID) || (NewCol == nColPartNumber) || (NewCol == nColDescricaoProduto) || (NewCol == nColOrigemID) || (NewCol == nColTipoProdutoID) || (NewCol == nColNCMID) ||
                  (NewCol == nColMoeda) || (NewCol == nColPrecoLista) || (NewCol == nColDescontoPadrao) || (NewCol == nColProdutoSubstituto) || (NewCol == nColProdutoVerTambem) ||
                  (NewCol == nColdtVendaFim) || (NewCol == nColObservacao) || (NewCol == nColAtualizacoes) || (NewCol == nColTaxaConversaoID) ||
                  (NewCol == nColUsuarioID)))
            {
                glb_validRow = OldRow;
                glb_validCol = OldCol;
            }
            else
                js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
        }
    }
    else
        js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
}

//Refresh no grid com ele listado *******************************************
function refreshParamsAndDataAndShowModalWin(bNewItem)
{
    if (glb_refreshGrid != null)
    {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }

    // mostra a janela modal
    if (glb_bModoAtualizacao)
        fillGridDataAtualizacoes();
    else
        fillGridDataProdutos(false, bNewItem);

    showExtFrame(window, true);
}