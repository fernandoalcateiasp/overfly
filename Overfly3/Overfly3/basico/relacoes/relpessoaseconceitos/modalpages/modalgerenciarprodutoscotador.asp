<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    

    Set objSvrCfg = Nothing

%>

<html id="modalgerenciarprodutoscotadorHtml" name="modalgerenciarprodutoscotadorHtml">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalgerenciarprodutoscotador.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/modalpages/modalgerenciarprodutoscotador.js" & Chr(34) & "></script>" & vbCrLf
    %>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller
Dim sCmbText, nCmbValue, nCmbOptionSel, nA1, nA2, nUsuarioID, nEmpresaID

sCaller = ""
sCmbText = ""
nCmbValue = 0
nCmbOptionSel = 0
nA1 = 0
nA2 = 0
nUsuarioID = 0
nEmpresaID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nA1").Count    
    nA1 = Request.QueryString("nA1")(i)
Next

For i = 1 To Request.QueryString("nA2").Count    
    nA2 = Request.QueryString("nA2")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write "var glb_nUsuarioID = " & Chr(39) & CStr(nUsuarioID) & Chr(39) & ";"

Response.Write "var glb_nA1 = " & CStr(nA1) & ";"

Response.Write "var glb_nA2 = " & CStr(nA2) & ";"

Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf
%>


<script id="wndJSProc" language="javascript">
</script>

<script language="javascript" for="fg" event="ValidateEdit">
    js_modalcotador_ValidateEdit();
</script>

<script language="javascript" for="fg" event="KeyPress">
    js_modalcotadorKeyPress(arguments[0]);
</script>

<script language="javascript" for="fg" event="AfterEdit">
    js_modalcotador_AfterEdit(arguments[0], arguments[1]);
</script>

<script language="javascript" for="fg" event="AfterRowColChange">
    js_modalcotador_AfterRowColChange(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
    js_modalcotadorBeforeEdit(fg, fg.Row, fg.Col);
</SCRIPT>

<script language="javascript" for="fg" event="DblClick">
    js_fg_modalcotadorDblClick(fg, fg.Row, fg.Col);
</script>

<script language="javascript" for="fg" event="BeforeMouseDown">
    js_fg_modalcotadorBeforeMouseDown(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</script>

<script language="javascript" for="fg" event="BeforeRowColChange">
    js_modalgerenciarprodutoscotador_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</script>

<script language="javascript" for="fg" event="AfterSort">
    fg_AfterSort();
</script>

<script language="javascript" for="fg" event="BeforeSort">
    fg_BeforeSort();
</script>

<script language="javascript" for="fg" event="MouseDown">
    fg_MouseDown_cotador(fg.Row, fg.Col);
</script>

<script language="javascript" for="fg" event="MouseMove">
    js_fg_modalcotadorMouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
</script>

<script language="javascript" for="fg" event="EnterCell">
    js_fg_EnterCell(fg);
</script>

</head>

<!--
<script language="javascript" for="fg" event="EnterCell">
    js_fg_EnterCell(fg);
</script>

<script LANGUAGE=javascript FOR=fg EVENT=DblClick>
    js_fg_DblClick(fg, fg.Row, fg.Col);
</script>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
    js_ModalGerenciarProdutosCotador_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</SCRIPT>
<script language="javascript" for="fg" event="AfterEdit">
    js_ModalGerenciarProdutosCotador_AfterEdit(arguments[0], arguments[1]);
</script>
<script language="javascript" for="fg" event="BeforeEdit">
    js_ModalGerenciarProdutosCotador_AfterEdit(arguments[0], arguments[1]);
</script>
<script language="javascript" for="fg" event="EnterCell">
    js_fg_EnterCell(fg);
</script>
-->


<body id="modalgerenciarprodutoscotadorBody" name="modalgerenciarprodutoscotadorBody" language="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>

    <div id="divControles" name="divControles" class="divGeneral">
        <!-- Componentes para comuns nos divs -->
        <p id="lblModo" name="lblModo" class="lblGeneral">Modo</p>
        <select id="selModo" name="selModo" class="fldGeneral" title="Arquivos de atualizações ou produtos do cotador.">
			<option value="1">Produtos</option>
            <option value="0">Atualizações</option>
        </select>
        <p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
        <select id="selMarca" name="selMarca" class="fldGeneral"></select>
        <p id="lblTipoArquivo" name="lblTipoArquivo" class="lblGeneral">Tipo Arquivo</p>
        <select id="selTipoArquivo" name="selTipoArquivo"  class="fldGeneral"></select>
        <p id="lblOrigem" name="lblOrigem" class="lblGeneral">Origem</p>
        <select id="selOrigem" name="selOrigem" class="fldGeneral">
            <option value="0"></option>
            <option value="1">Nacional</option>
			<option value="2">Importado</option>
        </select>
        <p id="lblEstado" name="lblEstado" class="lblGeneral">Estado</p>
        <select id="selEstado" name="selEstado" class="fldGeneral" title="Qual estado do produto ?">
            <option value="0"></option>
            <option value="1">Cadastrado</option>
			<option value="2">Ativo</option>
            <option value="4">Desativo</option>
        </select>
        <p id="lblNCM" name="lblNCM" class="lblGeneral">NCM</p>
        <select id="selNCM" name="selNCM" class="fldGeneral"></select>
        <!-- Argumento da pesquisa -->
        <p id="lblArgumento" name="lblArgumento" class="lblGeneral">Argumento</p>
        <input type="text" id="txtArgumento" name="txtArgumento" class="fldGeneral" onkeypress="Argumento_onkeypress()"></input>

        <!--Div de Upload-->
        <div id="divUpload" name="divUpload" class="divGeneral">
            <iframe src ="" id="I1" frameborder="0"  name="I1" height="45" width="400" SCROLLING="NO" ></iframe>         
        </div>

        <input type="button" id="btnUpload" name="btnUpload" value="Upload" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnDownload" name="btnDownload" value="Download" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnNovo" name="btnNovo" value="Novo" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnListar" name="btnListar" value="Listar" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnGravar" name="btnGravar" value="Gravar" language="javascript" onclick="return btn_onclick(this)" class="btns" />
    </div>

    <!-- Divs de grid -->
    <div id="divGrids" name="divGrids" class="divGeneral">
        <div id="divFG" name="divFG" class="divGeneral">
            <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" viewastext></object>
            <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
            <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
            <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
        </div>
        <div id="divFGDetalhes" name="divFGDetalhes" class="divGeneral">
            <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgDetalhes" name="fgDetalhes" viewastext></object>
            <img id="hr_L_FGBorder2" name="hr_L_FGBorder2" class="lblGeneral"></img>
            <img id="hr_R_FGBorder2" name="hr_R_FGBorder2" class="lblGeneral"></img>
            <img id="hr_B_FGBorder2" name="hr_B_FGBorder2" class="lblGeneral"></img>
        </div>
    </div>

    <input type="button" id="btnOK" name="btnOK" value="Gravar" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" language="javascript" onclick="return btn_onclick(this)" class="btns">
</body>

</html>