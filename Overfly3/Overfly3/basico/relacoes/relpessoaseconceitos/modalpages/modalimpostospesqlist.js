/********************************************************************
modalimpostospesqlist.js

Library javascript para o modalimpostospesqlist.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;
var glb_getServerData = 0;
var glb_refreshGrid = null;
var glb_timerUnloadWin = null;

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;
var glb_callFillGridTimerInt = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbImpostos = new CDatatransport("dsoCmbImpostos");
var dsoCmbMoeda = new CDatatransport("dsoCmbMoeda");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoDireitos = new CDatatransport("dsoDireitos");
/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
fieldExists(dso, fldName)

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalimpostospesqlist.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalimpostospesqlist.ASP

js_fg_modalimpostospesqlistBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalimpostospesqlistDblClick( grid, Row, Col)
js_modalimpostospesqlistKeyPress(KeyAscii)
js_modalimpostospesqlist_AfterRowColChange
js_modalimpostospesqlist_ValidateEdit()
js_modalimpostospesqlist_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // ajusta o body do html
    with (modalimpostospesqlistBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Impostos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    
    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';

    // ajusta o divCidade
    elem = window.document.getElementById('divPesquisa');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 40;
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.currentStyle.top, 10) +
			parseInt(divPesquisa.currentStyle.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;    
        //height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    adjustElementsInForm([['lblImpostoID','selImpostoID',10,1,-10,-10],
						  ['lblFornecedor','chkFornecedor',3,1],
						  ['lblEntrada','chkEntrada',3,1],
						  ['lblSaida','chkSaida',3,1],
						  ['lblAsstec','chkAsstec',3,1],
						  ['btnListar','btn',btnOK.offsetWidth,1,0,-4],
						  ['btnGravar','btn',btnOK.offsetWidth,1,2]], null, null, true);

	btnListar.style.height = btnOK.offsetHeight;
	btnGravar.style.height = btnOK.offsetHeight;
	
	chkFornecedor.onclick = setupBtnsFromGridState;
	chkEntrada.onclick = setupBtnsFromGridState;
	chkSaida.onclick = setupBtnsFromGridState;
	chkAsstec.onclick = setupBtnsFromGridState;
	setupBtnsFromGridState();
	
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;

    glb_getServerData = 1;
    
    setConnection(dsoCmbMoeda);
    dsoCmbMoeda.SQL = 'SELECT c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, c.SimboloMoeda AS Ordem ' +
        'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
        'WHERE a.SujeitoID = ' + aEmpresa[0] + ' ' +
        'AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 ' +
        'AND a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID';
    
    dsoCmbMoeda.ondatasetcomplete = dsoServerData_DSC;
    dsoCmbMoeda.Refresh();
}

/********************************************************************
Retorno do servidor
********************************************************************/
function dsoServerData_DSC()
{
    glb_getServerData--;

    if (glb_getServerData != 0)
        return null;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnGravar')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnListar')
    {
		fillGridData();    
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    if (glb_refreshGrid != null)
    {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }

    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    //fillGridData();
    fillCmbImpostos();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
	var i;
    btnListar.disabled = !(chkFornecedor.checked || chkEntrada.checked || chkSaida.checked || chkAsstec.checked);
	btnGravar.disabled = true;
	
	if (glb_sReadOnly != '*')
	{
		for (i=1; i<fg.Rows; i++)
		{
			if (getCellValueByColKey(fg, 'OK', i) != 0)
			{
				btnGravar.disabled = false;
				break;
			}	
		}
	}
}

function fillCmbImpostos()
{
	var sProdutos = selectString();
    // parametrizacao do dso dsoGrid
    setConnection(dsoCmbImpostos);

    strSQL = 'SELECT 0 AS Indice, 0 AS ConceitoID, SPACE(0) AS Imposto ' +
		'UNION ALL ' +
		'SELECT DISTINCT 1 AS Indice, c.ConceitoID, c.Imposto ' +
		'FROM RelacoesPesCon a WITH(NOLOCK), RelacoesPesCon_Impostos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
		'WHERE (a.RelacaoID IN ' + sProdutos + ' AND a.RelacaoID = b.RelacaoID AND  ' +
		'b.ImpostoID = c.ConceitoID) ' +
		'UNION ALL ' +
		'SELECT DISTINCT 1 AS Indice, d.ConceitoID, d.Imposto  ' +
		'FROM RelacoesPesCon a WITH(NOLOCK), RelacoesPesCon_Fornecedores b WITH(NOLOCK), RelacoesPesCon_Impostos c WITH(NOLOCK), Conceitos d WITH(NOLOCK) ' +
		'WHERE (a.RelacaoID IN ' + sProdutos + ' AND a.RelacaoID = b.RelacaoID AND  ' +
		'b.RelPesConFornecID = c.RelPesConFornecID AND c.ImpostoID = d.ConceitoID) ' +
		'ORDER BY Indice, Imposto ';

    if (strSQL != null)
    {
        dsoCmbImpostos.SQL = strSQL;
        dsoCmbImpostos.ondatasetcomplete = dsoCmbImpostos_DSC;
        dsoCmbImpostos.Refresh();
    }
}

function dsoCmbImpostos_DSC()
{
	var optionStr;
	var optionValue;
	
	clearComboEx(['selImpostoID']);

    while (! dsoCmbImpostos.recordset.EOF )
    {
        optionStr = dsoCmbImpostos.recordset['Imposto'].value;
		optionValue = dsoCmbImpostos.recordset['ConceitoID'].value;

		if (findCmbIndexByOptionID(selImpostoID, optionValue) == -1)
		{
			var oOption = document.createElement("OPTION");
			oOption.text = optionStr;
			oOption.value = optionValue;
			selImpostoID.add(oOption);
        }

        dsoCmbImpostos.recordset.MoveNext();
    }

	selImpostoID.disabled = (selImpostoID.options.length == 0);
	
	showExtFrame(window, true);
}

function findCmbIndexByOptionID(oCmb, nOptionID)
{
	var retVal = -1;
	var i = 0;
	
	for (i=0; i<oCmb.options.length; i++)
	{
		if (oCmb.options.item(i).value == nOptionID)
		{
			retVal = i;
			break;
		}
	}
		
	return retVal;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_callFillGridTimerInt != null)
	{
		window.clearInterval(glb_callFillGridTimerInt);
		glb_callFillGridTimerInt = null;
	}

    lockControlsInModalWin(true);
    
	var strSQL = '';
	
	var sFiltroImposto = '';

    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	var sProdutos = selectString();

	if (selImpostoID.value != 0)
		sFiltroImposto = ' AND x.ConceitoID = ' + selImpostoID.value + ' ';

	if (chkFornecedor.checked)
	{
		strSQL += 'SELECT 1 AS Indice, d.RelPesConImpostoID AS RelPesConImpostoID, f.ConceitoID AS ProdutoID, i.Conceito AS NCM, h.RecursoAbreviado, f.Conceito AS Produto, ' +
				'c.Fantasia AS Tipo, g.Conceito AS GrupoImposto, x.Imposto AS Imposto, d.Aliquota AS Aliquota, d.BaseCalculo AS BaseCalculo, 0 AS OK ' +
			'FROM RelacoesPesCon a WITH(NOLOCK), RelacoesPesCon_Fornecedores b WITH(NOLOCK), Pessoas c WITH(NOLOCK), ' +
				'RelacoesPesCon_Impostos d WITH(NOLOCK), Conceitos x WITH(NOLOCK), Conceitos f WITH(NOLOCK), Conceitos g WITH(NOLOCK), Recursos h WITH(NOLOCK), Conceitos i WITH(NOLOCK) ' +
			'WHERE (a.RelacaoID IN ' + sProdutos + ' AND a.RelacaoID = b.RelacaoID AND b.FornecedorID = c.PessoaID AND ' +
				'b.RelPesConFornecID = d.RelPesConFornecID AND d.ImpostoID = x.ConceitoID AND ' +
				'a.ObjetoID = f.ConceitoID AND b.GrupoImpostoID = g.ConceitoID AND a.EstadoID = h.RecursoID AND i.ConceitoID = a.ClassificacaoFiscalID ' + sFiltroImposto + ')';
    }

	if (chkEntrada.checked)
	{
		if (strSQL != '')
			strSQL += ' UNION ALL ';

		strSQL += 'SELECT 2 AS Indice, b.RelPesConImpostoID AS RelPesConImpostoID, d.ConceitoID AS ProdutoID, h.Conceito AS NCM, g.RecursoAbreviado, d.Conceito AS Produto, ' +
				'f.ItemMasculino AS Tipo, e.Conceito AS GrupoImposto, x.Imposto AS Imposto, b.Aliquota AS Aliquota, b.BaseCalculo AS BaseCalculo, 0 AS OK ' +
			'FROM RelacoesPesCon a WITH(NOLOCK), RelacoesPesCon_Impostos b WITH(NOLOCK), Conceitos x WITH(NOLOCK), Conceitos d WITH(NOLOCK), Conceitos e WITH(NOLOCK), TiposAuxiliares_Itens f WITH(NOLOCK), Recursos g WITH(NOLOCK), Conceitos h WITH(NOLOCK) ' +
			'WHERE (a.RelacaoID IN ' + sProdutos + ' AND a.RelacaoID = b.RelacaoID AND b.ImpostoID = x.ConceitoID AND ' +
				'a.ObjetoID = d.ConceitoID AND a.GrupoImpostoEntradaID = e.ConceitoID AND b.TipoID = 334 AND ' +
				'b.TipoID = f.ItemID AND a.EstadoID = g.RecursoID AND h.ConceitoID = a.ClassificacaoFiscalID ' + sFiltroImposto + ')';
    }

	if (chkSaida.checked)
	{
		if (strSQL != '')
			strSQL += ' UNION ALL ';

		strSQL += 'SELECT 3 AS Indice, b.RelPesConImpostoID AS RelPesConImpostoID, d.ConceitoID AS ProdutoID, h.Conceito AS NCM, g.RecursoAbreviado, d.Conceito AS Produto, ' +
				'f.ItemMasculino AS Tipo, e.Conceito AS GrupoImposto, x.Imposto AS Imposto, b.Aliquota AS Aliquota, b.BaseCalculo AS BaseCalculo, 0 AS OK ' +
			'FROM RelacoesPesCon a WITH(NOLOCK), RelacoesPesCon_Impostos b WITH(NOLOCK), Conceitos x WITH(NOLOCK), Conceitos d WITH(NOLOCK), Conceitos e WITH(NOLOCK), TiposAuxiliares_Itens f WITH(NOLOCK), Recursos g WITH(NOLOCK), Conceitos h WITH(NOLOCK) ' +
			'WHERE (a.RelacaoID IN ' + sProdutos + ' AND a.RelacaoID = b.RelacaoID AND b.ImpostoID = x.ConceitoID AND ' +
				'a.ObjetoID = d.ConceitoID AND a.GrupoImpostoSaidaID = e.ConceitoID AND b.TipoID = 335 AND ' +
				'b.TipoID = f.ItemID AND a.EstadoID = g.RecursoID AND h.ConceitoID = a.ClassificacaoFiscalID ' + sFiltroImposto + ')';
    }

	if (chkAsstec.checked)
	{
		if (strSQL != '')
			strSQL += ' UNION ALL ';

		strSQL += 'SELECT 4 AS Indice, b.RelPesConImpostoID AS RelPesConImpostoID, d.ConceitoID AS ProdutoID, h.Conceito AS NCM, g.RecursoAbreviado, d.Conceito AS Produto, ' +
				'f.ItemMasculino AS Tipo, e.Conceito AS GrupoImposto, x.Imposto AS Imposto, b.Aliquota AS Aliquota, b.BaseCalculo AS BaseCalculo, 0 AS OK ' +
			'FROM RelacoesPesCon a WITH(NOLOCK), RelacoesPesCon_Impostos b WITH(NOLOCK), Conceitos x WITH(NOLOCK), Conceitos d WITH(NOLOCK), Conceitos e WITH(NOLOCK), TiposAuxiliares_Itens f WITH(NOLOCK), Recursos g WITH(NOLOCK), Conceitos h WITH(NOLOCK) ' +
			'WHERE (a.RelacaoID IN ' + sProdutos + ' AND a.RelacaoID = b.RelacaoID AND b.ImpostoID = x.ConceitoID AND ' +
				'a.ObjetoID = d.ConceitoID AND a.GrupoImpostoEntradaID = e.ConceitoID AND b.TipoID = 336 AND ' +
				'b.TipoID = f.ItemID AND a.EstadoID = g.RecursoID AND h.ConceitoID = a.ClassificacaoFiscalID ' + sFiltroImposto + ')';
    }

	strSQL += ' ORDER BY ProdutoID, Indice, Tipo ';

    if (strSQL != null)
    {
        dsoGrid.SQL = strSQL;
        dsoGrid.ondatasetcomplete = fillGridData_DSC;
        dsoGrid.Refresh();
    }
}

/********************************************************************
Obtem string de select semelhante ao do pesqlist

Retorno:
string de select
********************************************************************/
function selectString()
{
    var i = 0;
    var strSQL = '';
    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');

    if (aGrid == null)
        return null;
    
    strSQL = '(';

	for (i=0; i<aGrid.length; i++)
	{
		strSQL = strSQL + aGrid[i];
	    
		if (i != (aGrid.length-1))
			strSQL = strSQL + ',';
	}

	strSQL = strSQL + ')';
	
	return strSQL;
}

/********************************************************************
Fecha a janela se mostra-la
********************************************************************/
function unloadThisWin()
{
    if (glb_timerUnloadWin != null)
    {
        window.clearInterval(glb_timerUnloadWin);
        glb_timerUnloadWin = null;
    }
   
    if ( window.top.overflyGen.Alert('Nenhum registro dispon�vel.') == 0 )
        return null;
            
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, 'NOREG' );
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
    {
        glb_timerUnloadWin = window.setInterval('unloadThisWin()', 50, 'JavaScript');
        
        return null;
    }
    
    showExtFrame(window, true);
    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');
    var i, j;
    var bgColorGreen, bgColorYellow, bgColorRed;
    
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    fg.Editable = false;
    fg.Redraw = 0;
    fg.Rows = 1;

    fg.ExplorerBar = 0;

    // Gerenciameto de Produtos
    headerGrid(fg,['ID', 
                   'E',
                   'Produto',
                   'NCM',
				   'Tipo',
                   'Grupo Imposto',
                   'Imposto',
                   'Al�quota',
                   'Base C�lc',
                   'RelPesConImpostoID',
                   'OK'],[9,10]);

    glb_aCelHint = [[0,3,'Clique duas vezes no NCM para abrir o documento NCM']];

    fillGridMask(fg,dsoGrid,['ProdutoID*',
					'RecursoAbreviado*',
					'Produto*',
					'NCM*',
                    'Tipo*',
                    'GrupoImposto*',
                    'Imposto*',
                    'Aliquota' + glb_sReadOnly,
                    'BaseCalculo' + glb_sReadOnly,
                    'RelPesConImpostoID',
                    'OK'],
                    ['','','','','','','','999.99','999.999','',''],
                    ['','','','','','','','###.00','###.000','','']);

    alignColsInGrid(fg,[0,6,7]);

    fg.FrozenCols = 6;
    
    fg.MergeCells = 4;
	fg.MergeCol(0) = true;
	fg.MergeCol(1) = true;
	fg.MergeCol(2) = true;
	fg.MergeCol(3) = true;
	fg.MergeCol(4) = true;
	fg.MergeCol(5) = true;
	
	fg.Redraw = 0;
    paintReadOnlyCols(fg);
    fg.Redraw = 2;
    fg.Col = 6;
    fg.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg.Rows > 1 )
        fg.Row = 1;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    if (fg.Rows > 1)
        fg.Editable = true;
	
    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function saveDataInGrid()
{
	var strPars = '';
	var nBytesAcum = 0;
	var nDataLen = 0;
	
	lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;

    lockControlsInModalWin(true);
    
    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
					strPars = '';
					nDataLen = 0;
				}
  				
  				nDataLen++;
				strPars += '?nRelPesConImpostoID=' + escape(getCellValueByColKey(fg, 'RelPesConImpostoID', i));
				strPars += '&nAliquota=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aliquota')));
				strPars += '&nBaseCalculo=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')));
				continue;
			}

			nDataLen++;
			strPars += '&nRelPesConImpostoID=' + escape(getCellValueByColKey(fg, 'RelPesConImpostoID', i));
			strPars += '&nAliquota=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Aliquota')));
			strPars += '&nBaseCalculo=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'BaseCalculo')));
		}	
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;

	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/gravarimpostos.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_callFillGridTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		return null;
	}
}

function sendDataToServer_DSC()
{
	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}


/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);
    
    glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(false)', 50, 'JavaScript');
    
    // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
}

/********************************************************************
Verifica se um dado campo existe em um dado dso

Parametros:
dso         - referencia ao dso
fldName     - nome do campo a verificar a existencia
                      
Retorno:
true se o campo existe, caso contrario false
********************************************************************/
function fieldExists(dso, fldName)
{
    var retVal = false;
    var i;
    
    for ( i=0; i< dso.recordset.Fields.Count; i++ )
    {
        if ( dso.recordset.Fields(i).Name == fldName )
            retVal = true;
    }
    
    return retVal;
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalimpostospesqlistBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalimpostospesqlistDblClick(grid, Row, Col)
{
    if (Row > 0)
    {
        var sNCM = fg.TextMatrix(Row, getColIndexByColKey(fg, 'NCM*'));
        sNCM = sNCM.substring(0,4);

        strPars = '?sNCM=' + escape(sNCM);
        htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serversideEx/imagemncm.aspx' + strPars;
        window.open(htmlPath);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalimpostospesqlistKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalimpostospesqlist_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalimpostospesqlist_AfterEdit(Row, Col)
{
    var nType;
    
    if (fg.Editable)
    {
		if (fg.Row > 0)
			fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;

		setupBtnsFromGridState();
    }
}

function fg_AfterEdit_modalImpostos(Row, Col)
{
	if ( (Col == getColIndexByColKey(fg, 'Aliquota' + glb_sReadOnly)) ||
		 (Col == getColIndexByColKey(fg, 'BaseCalculo' + glb_sReadOnly)) )
		fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
}
// FINAL DE EVENTOS DE GRID *****************************************
