/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de relpessoasconceitos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister) {
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    var sSQL;
    //@@
    var nUserID = getCurrUserID();
    var sAuditoria = '';

    if (glb_btnCtlSup == 'SUPREFR')
        sAuditoria = 'dbo.fn_Produto_Auditoria(RelacaoID, NULL, 1, GETDATE())';
    else
        sAuditoria = 'NULL';

    var sSQL2 = 'dbo.fn_Produto_FatorInternacao(RelacaoID, 1, GETDATE()) AS FatorInternacaoEntrada_Calc, ' +
                  'dbo.fn_Produto_FatorInternacao(RelacaoID, 2, GETDATE()) AS FatorInternacaoSaida_Calc, ' +
                  'CONVERT(VARCHAR, dtMediaPagamento, ' + DATE_SQL_PARAM + ') as V_dtMediaPagamento, ' +
                  'DATEDIFF(dd,GETDATE(),dtMediaPagamento) as DiasPagamento, ' +
				  '(SELECT TOP 1 c.SimboloMoeda FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
					'WHERE(a.SujeitoID = RelacoesPesCon.SujeitoID AND a.TipoRelacaoID = 12 AND a.ObjetoID = 999 AND a.RelacaoID = b.RelacaoID AND b.Faturamento = 1 AND b.MoedaID = c.ConceitoID) ' +
					'ORDER BY b.Ordem) AS MoedaEmpresa_Calc, ' +
                  'dbo.fn_Preco_Preco(SujeitoID,ObjetoID,NULL,NULL,NULL,NULL,NULL,GetDate(),1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL) as Preco_Calc,' +
                  'dbo.fn_Preco_Preco(SujeitoID,ObjetoID,NULL,NULL,MargemPublicacao,NULL,NULL,GetDate(),1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL) as PrecoWeb_Calc,' +
                  'dbo.fn_Preco_Preco(SujeitoID,ObjetoID,NULL,NULL,0,NULL,NULL,GetDate(),1,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL) AS ValorTransferencia_Calc, ' +
                  'CONVERT(VARCHAR, dtPrevisaoDisponibilidade, ' + DATE_SQL_PARAM + ') as V_dtPrevisaoDisponibilidade, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,341,NULL,NULL,NULL,375, -1) AS EstoqueFisico_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,356,NULL,NULL,NULL,375, NULL) AS EstoqueDisponivel_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,351,NULL,NULL,NULL,375, NULL) AS EstoqueRC_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,352,NULL,NULL,NULL,375, NULL) AS EstoqueRCC_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,353,NULL,NULL,NULL,375, NULL) AS EstoqueRV_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,354,NULL,NULL,NULL,375, NULL) AS EstoqueRVC_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,341,NULL,NULL,NULL,375, -2) AS EstoqueLoteFis_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,356,NULL,NULL,NULL,375, -2) AS EstoqueLoteDisp_Calc, ' +
				  sAuditoria + ' AS Auditoria, ' +
                  'ISNULL((dbo.fn_Produto_Descricao2(ObjetoID, ' + empresaData[8] + ', 11)), SPACE(0)) AS DescricaoCompleta, ' +
                  'CONVERT(VARCHAR, dbo.fn_Produto_Movimento_Kardex(SujeitoID, ObjetoID, NULL, 341), ' + DATE_SQL_PARAM + ') AS dtMovimentacao, ' +
                  '(CASE WHEN OrigemID IN (2701, 2706) THEN 1  ELSE 0 END ) AS NacImp, ' +
				  'ISNULL((SELECT a.TemProducao FROM RelacoesPesRec a WITH(NOLOCK) WHERE (RelacoesPesCon.SujeitoID = a.SujeitoID AND a.TipoRelacaoID = 12 AND a.ObjetoID = 999)), 0) AS TemProducao, ' +
				  '(SELECT ClassificacaoID FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = RelacoesPesCon.ObjetoID) AS ClassificacaoID, ';

    if (newRegister == true) {
        sSQL = 'SELECT TOP 1 *, ' + sSQL2 +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=RelacoesPesCon.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM RelacoesPesCon WITH(NOLOCK) ' +
               'WHERE ProprietarioID = ' + nID;

        // ID Calculado pela Trigger
        if (window.top.registroID_Type == 16)
            sSQL += ' AND ' + glb_sFldTipoRegistroName + ' = ' + glb_RelPesConTipoRegID + ' ';

        sSQL += 'ORDER BY RelacaoID DESC';
    }
    else {
        sSQL = 'SELECT *, ' + sSQL2 +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
	           'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=RelacoesPesCon.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               '(SELECT TOP 1 CONVERT (NUMERIC(11,2), dbo.fn_DivideZero(a.CustoTotal, a.QuantidadeTotal)) ' +
               'FROM dbo.RelacoesPesCon_Kardex a WITH(NOLOCK) ' +
               'WHERE a.RelacaoID =  ' + nID + '  AND TipoCustoID = 375 AND (dbo.fn_Estoque_EhProprio(EstoqueEspecificoID) = 1) ' +
               'ORDER BY dtKardex DESC ) AS CustoMedioGerencial, ' +
               '( SELECT TOP 1 CONVERT (NUMERIC(11,2), dbo.fn_DivideZero(a.CustoTotal, a.QuantidadeTotal )) ' +
               'FROM dbo.RelacoesPesCon_Kardex a WITH(NOLOCK) ' +
               ' WHERE a.RelacaoID =  ' + nID + ' AND TipoCustoID = 374 AND (dbo.fn_Estoque_EhProprio(EstoqueEspecificoID) = 1) ORDER BY dtKardex DESC) AS CustoContMedio ' +
               'FROM RelacoesPesCon WITH(NOLOCK) ' +
               'WHERE RelacaoID = ' + nID + ' ORDER BY RelacaoID DESC ';
    }
    setConnection(dso);

    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();

    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso) {
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    var sSQL2 = 'dbo.fn_Produto_FatorInternacao(RelacaoID, 1, GETDATE()) AS FatorInternacao_Calc, ' +
                  'dbo.fn_Produto_FatorInternacao(RelacaoID, 2, GETDATE()) AS FatorInternacaoSaida_Calc, ' +
                  'CONVERT(VARCHAR, dtMediaPagamento, ' + DATE_SQL_PARAM + ') as V_dtMediaPagamento, ' +
                  'DATEDIFF(dd,GETDATE(),dtMediaPagamento) as DiasPagamento, ' +
				  '(SELECT TOP 1 c.SimboloMoeda FROM RelacoesPesRec a, RelacoesPesRec_Moedas b, Conceitos c ' +
					'WHERE(a.SujeitoID = RelacoesPesCon.SujeitoID AND a.TipoRelacaoID = 12 AND a.ObjetoID = 999 AND a.RelacaoID = b.RelacaoID AND b.Faturamento = 1 AND b.MoedaID = c.ConceitoID) ' +
					'ORDER BY b.Ordem) AS MoedaEmpresa_Calc, ' +
                  'dbo.fn_Preco_Preco(SujeitoID,ObjetoID,NULL,NULL,NULL,NULL,NULL,GetDate(),1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL,NULL, NULL) as Preco_Calc,' +
                  'dbo.fn_Preco_Preco(SujeitoID,ObjetoID,NULL,NULL,MargemPublicacao,NULL,NULL,GetDate(),1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL,NULL, NULL) as PrecoWeb_Calc,' +
                  'dbo.fn_Preco_Preco(SujeitoID,ObjetoID,NULL,NULL,0,NULL,NULL,GetDate(),1,NULL,NULL,NULL,0,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL,NULL, NULL) AS ValorTransferencia_Calc, ' +
                  'CONVERT(VARCHAR, dtPrevisaoDisponibilidade, ' + DATE_SQL_PARAM + ') as V_dtPrevisaoDisponibilidade, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,341,NULL,NULL,NULL,375, -1) AS EstoqueFisico_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,356,NULL,NULL,NULL,375, NULL) AS EstoqueDisponivel_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,351,NULL,NULL,NULL,375, NULL) AS EstoqueRC_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,352,NULL,NULL,NULL,375, NULL) AS EstoqueRCC_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,353,NULL,NULL,NULL,375, NULL) AS EstoqueRV_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,354,NULL,NULL,NULL,375, NULL) AS EstoqueRVC_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,356,NULL,NULL,NULL,375, -1) AS EstoqueLoteFis_Calc, ' +
                  'dbo.fn_Produto_Estoque(SujeitoID,ObjetoID,356,NULL,NULL,NULL,375, -1) AS EstoqueLoteDisp_Calc, ' +
                  'NULL AS Auditoria, ' +
                  'CONVERT(VARCHAR, dbo.fn_Produto_Movimento_Kardex(SujeitoID, ObjetoID, NULL, 341), 103) as dtMovimentacao, ' +
                  '(CASE WHEN OrigemID IN (2701, 2706) THEN \'Importado\'  ELSE \'Nacional\' END ) AS NacImp, ' +
                  'ISNULL((dbo.fn_Produto_Descricao2(ObjetoID, ' + empresaData[8] + ', 11)), SPACE(0)) AS DescricaoCompleta, ' +
				  'ISNULL((SELECT a.TemProducao FROM RelacoesPesRec a WHERE (RelacoesPesCon.SujeitoID = a.SujeitoID AND a.TipoRelacaoID = 12 AND a.ObjetoID = 999)), 0) AS TemProducao, ' +
				  '(SELECT ClassificacaoID FROM Conceitos WHERE ConceitoID = RelacoesPesCon.ObjetoID) AS ClassificacaoID, ';

    setConnection(dso);

    var sql;

    sql = 'SELECT *, ' + sSQL2 +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM RelacoesPesCon WHERE RelacaoID = 0';

    dso.SQL = sql;
}
