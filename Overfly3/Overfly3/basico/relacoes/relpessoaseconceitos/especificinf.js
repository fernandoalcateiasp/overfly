/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de relpessoasconceitos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
var glb_sNumeroSerie = '0';

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    var nRelPesConFornecID;

    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    glb_bCheckInvert = false;

    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.RelacaoID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM RelacoesPesCon a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT a.LOGID, a.FormID, a.SubFormID, a.RegistroID, a.Data, a.UsuarioID, a.EventoID, a.EstadoID, ISNULL(a.Motivo, SPACE(1)) AS Motivo, ' +
                 'dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso, b.Campo, (CASE a.EventoID WHEN 28 THEN SPACE(1) ELSE b.Conteudo END) AS Conteudo, ' +
                 'dbo.fn_Log_Mudanca(b.LogDetalheID) AS Mudanca ' +
           'FROM LOGs a WITH(NOLOCK) ' +
             'LEFT OUTER JOIN LOGs_Detalhes b WITH(NOLOCK) ON b.LOGID=a.LOGID ' +
           'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' AND a.FormID=' + glb_nFormID + ' ' +
           'ORDER BY a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21064) // Estoques
    {
        // glb_dtPesConAtEstoque
        // Esta variavel, definida e setada no inf.js
        // define se o usuario:
        // entrou na pasta ou clicou refresh na pasta
        //  -> glb_dtPesConAtEstoque == null
        // clicou botao especifico para atualizar estoque
        //  -> glb_dtPesConAtEstoque == mm/dd/yyyy hh:mm:ss

        var sData = 'NULL';
        if (glb_dtPesConAtEstoque != null)
            sData = 'dbo.fn_Data_Fuso(' + '\'' + glb_dtPesConAtEstoque + '\'' + ', NULL,' + nEmpresaData[2] + ')';

        var sSQLEstoques = 'CONVERT(VARCHAR, dbo.fn_Data_Fuso(GETDATE(), NULL, ' + nEmpresaData[2] + '), ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
		                   'dbo.fn_Pad(CONVERT(VARCHAR(2), DATEPART(hh , dbo.fn_Data_Fuso(GETDATE(), NULL, ' + nEmpresaData[2] + '))), 2, CHAR(48), CHAR(76)) + CHAR(58) + ' +
		                   'dbo.fn_Pad(CONVERT(VARCHAR(2), DATEPART(mi , dbo.fn_Data_Fuso(GETDATE(), NULL, ' + nEmpresaData[2] + '))), 2, CHAR(48), CHAR(76)) + CHAR(58) + ' +
		                   'dbo.fn_Pad(CONVERT(VARCHAR(2), DATEPART(ss , dbo.fn_Data_Fuso(GETDATE(), NULL, ' + nEmpresaData[2] + '))), 2, CHAR(48), CHAR(76)) AS DataEstoque, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,341,NULL,' + sData + ',NULL,375, -1) AS EstoquePO, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,342,NULL,' + sData + ',NULL,375, -1) AS EstoquePE, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,343,NULL,' + sData + ',NULL,375, -1) AS EstoquePD, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,344,NULL,' + sData + ',NULL,375, -1) AS EstoquePC, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,345,NULL,' + sData + ',NULL,375, -1) AS EstoquePT, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,361,NULL,' + sData + ',NULL,375, -1) AS EstoqueProprio, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,346,NULL,NULL,NULL,375, -1) AS EstoqueTO, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,347,NULL,' + sData + ',NULL,375, -1) AS EstoqueTE, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,348,NULL,NULL,NULL,375, -1) AS EstoqueTD, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,349,NULL,NULL,NULL,375, -1) AS EstoqueTC, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,362,NULL,NULL,NULL,375, -1) AS EstoqueTerceiros, ' +
                           'dbo.fn_Produto_Estoque(a.SujeitoID,a.ObjetoID,363,NULL,NULL,NULL,375, -1) AS EstoqueTotal, ' +
                           'dbo.fn_Produto_EstoqueBrinde(a.SujeitoID,a.ObjetoID,4) AS EstoqueBrinde, ' +
                           'a.Localizacao, ';

        var sSQLDates = 'CONVERT(VARCHAR, a.dtInicio, ' + DATE_SQL_PARAM + ') as V_dtInicio, ' +
                        'CONVERT(VARCHAR, a.dtControle, ' + DATE_SQL_PARAM + ') as V_dtControle,' +
                        'DATEDIFF(dd,GETDATE(),a.dtControle) as DiasControle, ' +
                        'CONVERT(VARCHAR, a.dtFim, ' + DATE_SQL_PARAM + ') as V_dtFim,' +
                        'DATEDIFF(dd,GETDATE(),a.dtFim) as DiasFim ';

        dso.SQL = 'SELECT ' + sSQLEstoques + ' a.dtInicio, a.dtControle, a.dtFim, ' +
                  'a.dtMediaPagamento, a.dtPrevisaoDisponibilidade, ' +
                  'a.EstoqueSemNS, a.Controle, a.EstoqueControle, a.LiberaBrinde, a.AjusteBrinde, a.PrazoObsoletoExtra, ' + sSQLDates +
                  'FROM RelacoesPesCon a WITH(NOLOCK) WHERE ' +
                  sFiltro + 'a.RelacaoID = ' + idToFind;

        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 21075) // Localizacoes de Estoque
    {
        dso.SQL = 'SELECT a.* ' +
        'FROM RelacoesPesCon_LocalizacoesEstoque a WITH(NOLOCK) ' +
        'WHERE ' + sFiltro + ' a.RelacaoID = ' + idToFind + ' ORDER BY a.Ordem';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 21072) // Gerenciamento de produtos
    {
        var sSQLDates = ' ,CONVERT(VARCHAR, a.dtInicio, ' + DATE_SQL_PARAM + ') as V_dtInicio, ' +
                        'CONVERT(VARCHAR, a.dtControle, ' + DATE_SQL_PARAM + ') as V_dtControle,' +
                        'DATEDIFF(dd,GETDATE(),a.dtControle) as DiasControle, ' +
                        'CONVERT(VARCHAR, a.dtFim, ' + DATE_SQL_PARAM + ') as V_dtFim,' +
                        'CONVERT(VARCHAR, dbo.fn_Produto_Movimento_Kardex(a.SujeitoID,a.ObjetoID,1,341), ' + DATE_SQL_PARAM + ') as dtUltimaEntrada,' +
                        'CONVERT(VARCHAR, dbo.fn_Produto_Movimento_Kardex(a.SujeitoID,a.ObjetoID,2,341), ' + DATE_SQL_PARAM + ') as dtUltimaSaida,' +
                        'DATEDIFF(dd, dbo.fn_Produto_Movimento_Kardex(a.SujeitoID,a.ObjetoID,2,341),GETDATE() ) as DiasSaida,' +
                        'DATEDIFF(dd,GETDATE(),a.dtFim ) as DiasFim ';

        dso.SQL = 'SELECT a.RelacaoID, a.MargemContribuicao, a.MargemPadrao, a.MargemPublicacao, MargemMinima, ' +
                  'dbo.fn_Preco_MargemContribuicao(a.SujeitoID, a.ObjetoID,NULL,NULL,NULL,NULL,NULL,NULL,NULL, GETDATE(),1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MRci,' +
                  'dbo.fn_Preco_MargemContribuicao(a.SujeitoID, a.ObjetoID,NULL,NULL,0,NULL,NULL,NULL,NULL, GETDATE(),1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL) AS MRciWeb,' +
				  'a.PrecoMinimo, a.PrazoTroca, a.PrazoGarantia, a.PrazoAsstec, a.Observacao, ' +
				  'dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, ' +
						'dbo.fn_Data_Dia(GETDATE(), 3, -3, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -3, 0, 0), 1) AS Vendas3, ' +
				  'dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, ' +
						'dbo.fn_Data_Dia(GETDATE(), 3, -2, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -2, 0, 0), 1) AS Vendas2, ' +
				  'dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, ' +
						'dbo.fn_Data_Dia(GETDATE(), 3, -1, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -1, 0, 0), 1) AS Vendas1, ' +
				  'dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, ' +
						'dbo.fn_Data_Dia(GETDATE(), 3, 0, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, 0, 0, 0), 1) AS Vendas0, ' +
				  'dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, ' +
						'dbo.fn_Data_Dia(GETDATE(), 3, 0, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, 0, 0, 0), 2) AS MediaVendas13, ' +
				  'a.PrevisaoVendas, a.MultiploVenda, a.MultiploCompra, a.PrazoEstoque, ' +
				  'dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 11) AS PrevisaoVendasDiaria, ' +
				  'CONVERT(NUMERIC(10), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 12)) AS PrazoReposicao1, ' +
				  'CONVERT(NUMERIC(10), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 17)) AS PrazoPagamento1, ' +
				  'CONVERT(NUMERIC(10), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 13)) AS PrazoMinimo, ' +
				  'CONVERT(NUMERIC(10), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 14)) AS EstoqueMinimo, ' +
                  'dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 358, NULL, NULL,NULL,375, NULL) AS Estoque, ' +
				  'CONVERT(NUMERIC(10), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 15)) AS PrazoEstoqueAjustado, ' +
				  'CONVERT(NUMERIC(10), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 16)) AS LoteCompra, ' +
				  'dbo.fn_Produto_EstoqueWeb(a.SujeitoID, a.ObjetoID, 356, NULL, GETDATE(), NULL) AS EstoqueWeb,' +
                  'CONVERT(NUMERIC(10), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 30)) AS ComprarExcesso, ' +
                  'a.PublicaPreco, a.Publica, a.PercentualPublicacaoEstoque, ' +
                  'a.dtInicio, a.dtControle, a.dtFim, a.Reportafabricante, a.LojaOK, a.FichaTecnica,' +
                  'a.dtUltimoCusto, a.UsuarioID ' + sSQLDates +
                  'FROM RelacoesPesCon a WITH(NOLOCK) WHERE ' +
                  sFiltro + 'a.RelacaoID = ' + idToFind;

        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 21065) // Fornecedores
    {



        dso.SQL = 'SELECT a.* ' +  //, dbo.fn_Produto_FatorInternacao(-a.RelPesConFornecID, 1, GETDATE()) AS FIE ' +                
        'FROM RelacoesPesCon_Fornecedores a WITH(NOLOCK) ' +
        'WHERE ' + sFiltro + ' a.RelacaoID = ' + idToFind + ' ORDER BY a.Ordem';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 21066) // Custo Medio
    {
        // Ordem Invertida
        glb_bCheckInvert = true;

        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        // Paginacao Inferior
        glb_currGridTable = 'RelacoesPesCon_Custos_Antiga';
        glb_aCmbKeyPesq = [['Data', 'aDtCusto'], ['Pedido', 'fPedidoID'],
                           ['Pessoa', 'gFantasia']];

        // paginacao de grid
        return execPaging(dso, 'RelacaoID', idToFind, folderID);
    }
    else if (folderID == 21067) // Pedidos
    {
        glb_bCheckInvert = true;
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        // Paginacao Inferior
        glb_currGridTable = 'Pedidos_Itens';
        glb_aCmbKeyPesq = [['Data', 'bdtMovimento'], ['Pedido', 'cPedidoID'],
                           ['Pessoa', 'VfFantasia']];
        // paginacao de grid
        return execPaging(dso, 'RelacaoID', idToFind, folderID);
    }
    else if (folderID == 21068) // Movimentos
    {
        // Ordem Invertida
        glb_bCheckInvert = true;

        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        // Paginacao Inferior
        glb_currGridTable = 'RelacoesPesCon_Kardex';
        glb_aCmbKeyPesq = [['Data', 'bdtMovimento'], ['Pedido', 'bPedidoID']];
        // paginacao de grid
        return execPaging(dso, 'RelacaoID', idToFind, folderID);
    }
    else if (folderID == 21069) // Numero de Serie
    {
        var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'SujeitoID\'].value');
        var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ObjetoID\'].value');

        dso.SQL = 'SELECT b.ConceitoID AS ProdutoID, a.NumeroSerie AS NumeroSerie, g.ItemAbreviado AS Estoque, ' +
                '(SELECT TOP 1 dtMovimento FROM Pedidos_Itens WITH(NOLOCK) WHERE PedidoID = d.PedidoID AND ProdutoID = b.ConceitoID) AS dtMovimento, ' +
                'd.PedidoID, f.Operacao AS Transacao, e.Fantasia, b.Conceito AS ProdutoFantasia ' +
            'FROM NumerosSerie a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ProdutoID) ' +
                'INNER JOIN NumerosSerie_Movimento c WITH(NOLOCK) ON (c.NumeroSerieID = a.NumeroSerieID) ' +
                    'INNER JOIN Pedidos d WITH(NOLOCK) ON (d.PedidoID = c.PedidoID) ' +
                        'INNER JOIN Pessoas e WITH(NOLOCK) ON (e.PessoaID = d.PessoaID) ' +
                        'INNER JOIN Operacoes f WITH(NOLOCK) ON (f.OperacaoID = d.TransacaoID) ' +
                    'LEFT OUTER JOIN TiposAuxiliares_Itens g WITH(NOLOCK) ON (g.ItemID = c.EstoqueID) ' +
            'WHERE ' + sFiltro + 'a.ProdutoID = ' + nProdutoID + ' AND ' +
                'LTRIM(RTRIM(a.NumeroSerie))=' + '\'' + glb_sNumeroSerie + '\'' + ' AND  ' +
                'a.ProdutoID= ' + nProdutoID + ' AND d.EmpresaID=' + nEmpresaID + ' ' +
            'ORDER BY dtMovimento';

        return 'dso01Grid_DSC';

        glb_sNumeroSerie = '0';
    }
    else if (folderID == 21070) // Preco dos Concorrentes
    {
        var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'SujeitoID\'].value');
        var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ObjetoID\'].value');


        dso.SQL = 'SELECT a.*, ' +
                  'LTRIM(STR(ROUND(((((a.Preco)/(dbo.fn_Preco_Preco( ' + nEmpresaID + ',' + nProdutoID + ',NULL,NULL,NULL,NULL,NULL,GetDate(),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL)))-1)*100),2),5,2)) AS Variacao, ' +
                  'LTRIM(STR(dbo.fn_Preco_MargemContribuicao( ' + nEmpresaID + ',' + nProdutoID + ',NULL,NULL,a.Preco,NULL,NULL,NULL,NULL,GetDate(),NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),5,2)) AS Margem ' +
                  'FROM RelacoesPesCon_Precos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21073) // Equipes
    {
        dso.SQL = 'SELECT a.* ' +
            'FROM RelacoesPesCon_Equipes a WITH(NOLOCK) WHERE ' +
			sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE a.EquipeID=PessoaID)';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21074) // Campanhas
    {
        var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'SujeitoID\'].value');
        var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ObjetoID\'].value');

        dso.SQL = 'SELECT d.ItemMasculino AS Tipo, b.dtEmissao, ' +
				'dbo.fn_CampanhaProduto_Datas(a.CamProdutoID, 1) AS dtInicio, ' +
				'dbo.fn_CampanhaProduto_Datas(a.CamProdutoID, 2) AS dtFim, ' +
				'b.CampanhaID, e.RecursoAbreviado AS Estado, ' +
				'b.Campanha, b.Codigo, f.Fantasia AS Fornecedor, ' +
				'a.Quantidade, g.SimboloMoeda,  ' +
				'dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 3) AS ValorCampanha, ' +
				'dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 7) AS SaldoQuantidade, ' +
				'dbo.fn_CampanhaProduto_Totais(a.CamProdutoID, NULL, 8) AS SaldoValor ' +
			'FROM Campanhas_Produtos a WITH(NOLOCK) ' +
			    'INNER JOIN Campanhas b WITH(NOLOCK) ON (a.CampanhaID = b.CampanhaID) ' +
			    'INNER JOIN Campanhas_Empresas c WITH(NOLOCK) ON (b.CampanhaID = c.CampanhaID) ' +
			    'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (b.TipoCampanhaID = d.ItemID) ' +
			    'INNER JOIN Recursos e WITH(NOLOCK) ON (b.EstadoID = e.RecursoID) ' +
				'LEFT OUTER JOIN Pessoas f WITH(NOLOCK) ON (b.FornecedorID = f.PessoaID) ' +
				'INNER JOIN Conceitos g WITH(NOLOCK) ON (b.MoedaID = g.ConceitoID) ' +
			'WHERE (a.ProdutoID = ' + nProdutoID + ' AND ' +
				'c.EmpresaID = ' + nEmpresaID + ' AND b.EstadoID=41) ' +
			'ORDER BY d.ItemMasculino, b.dtEmissao, b.CampanhaID';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 21076) // Lojas web
    {
        dso.SQL = 'SELECT a.* ' +
            'FROM RelacoesPesCon_Lojas a WITH(NOLOCK) WHERE ' +
			sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY (SELECT ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE a.LojaID=ItemID)';
        return 'dso01Grid_DSC';
    }
        //@@
    else if (folderID == 21080) // Fiscal
    {
        dso.SQL = 'SELECT a.relacaoid , a.GrupoImpostoEntradaID, a.MoedaEntradaID, a.GrupoImpostoSaidaID, a.MoedaSaidaID, ' +
                  'a.ClassificacaoFiscalID, a.OrigemID, a.UsuarioID, a.ProdutoImportado, a.NBSID, a.CestID ' +
                  'FROM RelacoesPesCon a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY RelacaoID DESC';
        return 'dso01JoinSup_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    var nSujeitoID;
    var nEmpresaData = getCurrEmpresaData();
    var nRelPesConFornecID;
    setConnection(dso);

    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
        // Fornecedores
    else if (pastaID == 21065) {
        if (dso.id == 'dsoCmb01Grid_01') {
            nSujeitoID = getCurrDataInControl('sup', 'selSujeitoID');
            nSujeitoID = (nSujeitoID == null ? 0 : nSujeitoID);
            dso.SQL = 'SELECT a.ConceitoID , a.Conceito ' +
                      'FROM Conceitos a WITH(NOLOCK), Pessoas_Enderecos b WITH(NOLOCK) ' +
                      'WHERE a.TipoConceitoID = 307 AND a.EstadoID=2 AND b.PessoaID = ' + nSujeitoID + ' AND ' +
                      'a.PaisID = b.PaisID AND b.Ordem=1';
        }
        else if ((dso.id == 'dsoCmb01Grid_02') || (dso.id == 'dsoCmb01Grid_03')) {
            dso.SQL = 'SELECT c.ConceitoID, c.SimboloMoeda ' +
                      'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
                      'WHERE a.SujeitoID = ' + nEmpresaData[0] + ' ' +
                      'AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 ' +
                      'AND a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID ' +
                      'ORDER BY c.ConceitoAbreviado';

        }
        else if (dso.id == 'dsoCmb01Grid_04') {
            dso.SQL = 'SELECT 0 AS ConceitoID, SPACE(1) AS Conceito ' +
                      'UNION ALL ' +
                      'SELECT a.ConceitoID, a.Conceito ' +
                      'FROM Conceitos a WITH(NOLOCK) ' +
                      'WHERE a.EstadoID IN (2,4) AND a.TipoConceitoID = 309 AND a.PaisID = ' + nEmpresaData[1] + ' ' +
                      'ORDER BY Conceito';

        }
        else if (dso.id == 'dsoCmb01Grid_05') {
            dso.SQL = 'SELECT 0 AS PortoOrigemID, SPACE(1) AS PortoOrigem ' +
                      'UNION ALL ' +
                      'SELECT DISTINCT a.PessoaID AS PortoOrigemID, a.Fantasia AS PortoOrigem ' +
                      'FROM Pessoas a WITH(NOLOCK) ' +
                        'INNER JOIN Importacao b WITH(NOLOCK) ON (b.PortoOrigemID = a.PessoaID) ' +
                      'WHERE a.EstadoID = 2 ' +
                      'ORDER BY PortoOrigem';

        }
        else if (dso.id == 'dsoCmb01Grid_06') {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(1) AS fldName UNION ALL ' +
                        'SELECT ItemID AS fldID, ItemMasculino AS fldName ' +
                            'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                            'WHERE TipoID = 405 and EstadoID = 2 and filtro like \'%<I>%\' ' +
                            'ORDER BY fldName';

        }

        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.PessoaID, a.Fantasia ' +
                      'FROM Pessoas a WITH(NOLOCK), RelacoesPesCon_Fornecedores b WITH(NOLOCK) ' +
                      'WHERE a.PessoaID = b.FornecedorID AND b.RelacaoID = ' +
                      nRegistroID;
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT FornecedoresProduto.RelPesConFornecID, Moedas.Cotacao ' +
	                    'FROM RelacoesPesCon_Fornecedores FornecedoresProduto WITH(NOLOCK), RelacoesPessoas Fornecedores WITH(NOLOCK), ' +
	                        'RelacoesPessoas_Moedas Moedas WITH(NOLOCK) ' +
	                    'WHERE (FornecedoresProduto.RelacaoID= ' + nRegistroID + ' AND FornecedoresProduto.FornecedorID=Fornecedores.ObjetoID AND ' +
    	                    'Fornecedores.SujeitoID= ' + nEmpresaData[0] + ' AND Fornecedores.TipoRelacaoID=21 AND Fornecedores.RelacaoID=Moedas.RelacaoID AND ' +
		                    'Moedas.CodigoTaxaID=FornecedoresProduto.CodigoTaxaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT Fornecedores.RelPesConFornecID, ' +
                            'dbo.fn_Preco_Preco(PesCon.SujeitoID, PesCon.ObjetoID, NULL, Fornecedores.CustoReposicao, 0, NULL, PesCon.MoedaEntradaID, GETDATE(), 1, 0, 0, 1, PesCon.SujeitoID, NULL, ' +
                                'NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL) AS Preco ' +
	                    'FROM RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ' +
                            'INNER JOIN RelacoesPesCon PesCon WITH(NOLOCK) ON (PesCon.RelacaoID = Fornecedores.RelacaoID) ' +
	                    'WHERE (Fornecedores.RelacaoID = ' + nRegistroID + ')';
        }
    }
        // Custo Medio
        /* Virou pagina��o
        else if ( pastaID == 21066 )
        {
            if (dso.id == 'dso01GridLkp')
            {
                dso.SQL =  'SELECT a.MoedaCustoID, a.MoedaPedidoID, b.SimboloMoeda, c.SimboloMoeda AS SimboloMoeda2 ' +
                           'FROM RelacoesPesCon_Custos a WITH(NOLOCK) ' +
                            'LEFT OUTER JOIN Conceitos b WITH(NOLOCK) ON (a.MoedaCustoID=b.ConceitoID) ' +
                            'LEFT OUTER JOIN Conceitos c WITH(NOLOCK) ON (a.MoedaPedidoID=c.ConceitoID) ' +
                           'WHERE (a.RelacaoID = ' + nRegistroID + ')';
            }
            else if (dso.id == 'dso02GridLkp')
            {
                dso.SQL =  'SELECT b.PedItemID, b.PedidoID, dbo.fn_RelPesConCusto_Encomenda(a.RelPesConCustoID) AS EhEncomenda ' +
                           'FROM RelacoesPesCon_Custos a WITH(NOLOCK), Pedidos_Itens b WITH(NOLOCK) ' +
                           'WHERE a.RelacaoID = ' + nRegistroID + ' AND a.PedItemID=b.PedItemID ';
            }
            else if (dso.id == 'dso03GridLkp')
            {
                dso.SQL =  'SELECT a.RelPesConCustoID, b.ItemAbreviado ' +
                           'FROM RelacoesPesCon_Custos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                           'WHERE a.RelacaoID = ' + nRegistroID + ' AND a.MetodoCustoID=b.ItemID ';
            }
            else if (dso.id == 'dso04GridLkp')
            {
                dso.SQL =  'SELECT a.RelPesConCustoID, b.CFOPID AS CFOPID, d.Fantasia AS Pessoa ' +
                           'FROM RelacoesPesCon_Custos a WITH(NOLOCK) ' +
                                'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.PedItemID = a.PedItemID) ' +
                                'INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) ' +
                                'INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = c.PessoaID) ' +
                           'WHERE a.RelacaoID = ' + nRegistroID;
            }
        }*/
        // Preco dos concorrentes
    else if (pastaID == 21070) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.PessoaID,a.Fantasia FROM Pessoas a WITH(NOLOCK), RelacoesPesCon_Precos b WITH(NOLOCK) WHERE b.ConcorrenteID=a.PessoaID AND b.RelacaoID = ' + nRegistroID;
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT a.PessoaID,a.Fantasia FROM Pessoas a WITH(NOLOCK), RelacoesPesCon_Precos b WITH(NOLOCK) WHERE b.ColaboradorID=a.PessoaID AND b.RelacaoID = ' + nRegistroID;
        }
    }
        // Fornecedores
    else if (pastaID == 21073) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT a.PessoaID AS EquipeID, a.Fantasia Equipe ' +
                      'FROM Pessoas a WITH(NOLOCK) ' +
                      'WHERE a.EstadoID=2 AND a.TipoPessoaID = 53 ' +
                      'ORDER BY a.Fantasia';
        }
        else if (dso.id == 'dso01GridLkp') {
            var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'SujeitoID\'].value');
            var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ObjetoID\'].value');

            dso.SQL = 'SELECT a.RelPesConEquipeID, dbo.fn_Produto_EstoqueEquipe(' + nEmpresaID + ',' + nProdutoID + ', a.EquipeID, 3) AS Reserva, ' +
						'dbo.fn_Produto_EstoqueEquipe(' + nEmpresaID + ',' + nProdutoID + ', a.EquipeID, 4) AS Disponivel ' +
				'FROM RelacoesPesCon_Equipes a WITH(NOLOCK) WHERE ' +
				'a.RelacaoID = ' + nRegistroID;
        }
    }
        // Localizacoes de Estoque
    else if (pastaID == 21075) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT a.LocalizacaoID, dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) AS Codigo ' +
                      'FROM LocalizacoesEstoque a WITH(NOLOCK) ' +
                      'WHERE (a.EstadoID = 2 AND a.EmpresaID = ' + nEmpresaData[0] + ' AND ' +
						'dbo.fn_LocalizacaoEstoque_Util(a.LocalizacaoID) = 1) ' +
                      'ORDER BY dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1)';

        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.RelPesConLocalizacaoID, ISNULL(b.ReposicaoDiaria, 0) AS ReposicaoDiaria, ' +
						'dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, NULL, NULL, NULL, 0, 1) AS QuantidadeCaixasTotal, ' +
						'dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, 1, NULL, NULL, 0, 1) AS QuantidadeCaixasFila, ' +
						'dbo.fn_LocalizacaoEstoque_ProdutosTotais(b.LocalizacaoID, c.ObjetoID, 1, NULL,    1, 0, 1) AS QuantidadeCaixasPilha ' +
                      'FROM RelacoesPesCon_LocalizacoesEstoque a WITH(NOLOCK), LocalizacoesEstoque b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK) ' +
                      'WHERE (a.RelacaoID = ' + nRegistroID + ' AND a.LocalizacaoID = b.LocalizacaoID AND ' +
						'a.RelacaoID = c.RelacaoID)';
        }
    }
        // Lojas web
    else if (pastaID == 21076) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT a.ItemID, a.ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                      'WHERE (a.TipoID = 108 AND a.EstadoID = 2) ' +
					  'ORDER BY a.ItemMasculino';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    if (nTipoRegistroID == 61) {
        vPastaName = window.top.getPastaName(21064);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21064); //Estoques

        vPastaName = window.top.getPastaName(21075);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21075); //Localiza��es de Estoque

        vPastaName = window.top.getPastaName(21072);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21072); //Gerenciamento de produtos

        vPastaName = window.top.getPastaName(21073);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21073); //Equipes

        vPastaName = window.top.getPastaName(21074);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21074); //Campanhas

        vPastaName = window.top.getPastaName(21065);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21065); //Fornecedores

        vPastaName = window.top.getPastaName(21066);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21066); //Custo Medio

        vPastaName = window.top.getPastaName(21067);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21067); //Pedidos

        vPastaName = window.top.getPastaName(21068);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21068); //Movimentos

        vPastaName = window.top.getPastaName(21069);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21069); //Numeros de Serie

        vPastaName = window.top.getPastaName(21070);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21070); //Preco dos concorrentes

        vPastaName = window.top.getPastaName(21076);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21076); //Lojas web

        vPastaName = window.top.getPastaName(21080);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 21080); //Fiscal


    }

    // Nao mexer - Inicio de automacao ============================== 	
    // Tenta selecionar novamente a pasta anterior selecionada no combo
    // Se nao mais existe a pasta anteriormente selecionada no combo
    // seleciona a pasta default

    stripFoldersByRight(currSelection);

    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 21065) // Fornecedores
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['RelacaoID', registroID], [26]);
        else if (folderID == 21066) // Custo Medio
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['RelacaoID', registroID]);
        else if (folderID == 21070) // Concorrentes
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2, 5, 7, 8], ['RelacaoID', registroID], [7, 8]);
        else if (folderID == 21073) // Equipes
        {
            if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Percentual')) <= 0)
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Percentual')) = '0';

            if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Quantidade')) <= 0)
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Quantidade')) = '0';

            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['RelacaoID', registroID]);
        }
        else if (folderID == 21075) // Localizacoes de Estoque
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['RelacaoID', registroID]);
        else if (folderID == 21076) // Lojas web
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['RelacaoID', registroID]);

        if (currLine < 0)
            return false;
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var oElem;

    fg.FrozenCols = 0;

    if (folderID == 20010) // LOG
    {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'Campo',
                       'De',
                       'Para',
                       'LOGID'], [9]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'Campo',
                                 'Conteudo',
                                 'Mudanca',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '', '', '', '']);

        alignColsInGrid(fg, [9]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 21065) // Fornecedores
    {
        var Movimentacao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'dtMovimentacao\'].value');
        var Imp = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'NacImp\'].value');
        var ClassificacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ClassificacaoID\'].value');

        var trava = 1;
        var nEmpresaData = getCurrEmpresaData();

        if (nEmpresaData[1] == 130) {

            if ((ClassificacaoID == 315) && (Imp == 1)) {
                    trava = 0;
            }
        }

        //Temporario, para analise da trava. LYF 06/12/2017
        travaCusto = 0;

        headerGrid(fg, ['Ordem',
					   'ID',
                       'Fornecedor',
                       'Grupo Imposto',
                       'Moeda',
                       'Custo FOB',
                       'Val Custo FOB',
                       'Custo Reposi��o',
                       'FIE Aut',
                       'FIE',
                       'Pre�o',
                       'FIS',
                       'Porto Origem',
                       'Via Transporte',
                       'Moeda',
                       'Reb Cli',
                       'CT',
                       'Taxa',
                       'Class Fiscal',
                       'PP',
                       'PR',
                       'Troca',
                       'Gar',
                       'Asstec',
                       'AP',
                       'AT',
                       'C�digo',
                       'Observa��o',
                       'ColaboradorID',
                       'RelPesConFornecID'], [29]);

        glb_aCelHint = [[0, 6, 'Validade do Custo FOB (em dias)'],
                        [0, 8, 'FIE autom�tico?'],
                        [0, 9, 'Fator de Interna��o de Entrada'],
                        [0, 10, 'Pre�o'],
                        [0, 11, 'Fator de Interna��o de Sa�da'],
                        [0, 15, 'Rebate Cliente'],
                        [0, 16, 'C�digo da Taxa da Moeda'],
                        [0, 19, 'Prazo de Pagamento (em dias corridos)'],
                        [0, 20, 'Prazo (em dias �teis) que levamos para comprar e o produto estar dispon�vel para vendas'],
                        [0, 21, 'Prazo de Troca (em meses)'],
                        [0, 22, 'Prazo de Garantia (em meses)'],
                        [0, 23, 'Prazo de Assist�ncia T�cnica (em meses)'],
                        [0, 24, 'Assist�ncia T�cnica Pr�pria'],
                        [0, 24, 'Assist�ncia T�cnica Terceirizada'],
                        [0, 26, 'C�digo do Produto no Fornecedor']];

        fillGridMask(fg, currDSO, ['Ordem',
					   'FornecedorID*',
                       '^FornecedorID^dso01GridLkp^PessoaID^Fantasia*',
                       'GrupoImpostoID',
                       'MoedaID',
                       'CustoFOB',
                       'ValidadeCustoFOB',
                       'CustoReposicao*',
                       //'_CALC_FIE_6*',
                       'FIEAutomatico*',
                       'FatorInternacaoEntrada' + (trava == 1 ? '*' : ''),
                       '^RelPesConFornecID^dso03GridLkp^RelPesConFornecID^Preco*',
                       'FatorInternacaoSaida*',
                       'PortoOrigemID',
                       'ViaTransporteID',
                       'MoedaRebateID',
					   'RebateCliente',
                       'CodigoTaxaID*',
                       '^RelPesConFornecID^dso02GridLkp^RelPesConFornecID^Cotacao*',
                       'ClassificacaoFiscalID',
                       'PrazoPagamento',
                       'PrazoReposicao',
                       'PrazoTroca',
                       'PrazoGarantia',
                       'PrazoAsstec',
                       'AsstecProprio',
                       'AsstecTerceirizado',
                       'Codigo',
                       'Observacao',
                       'ColaboradorID',
                       'RelPesConFornecID'],
                       ['99', '', '', '', '', '999999999.99', '', '99999999.99', '', '99.9999', '999999999.99', '99.9999', '', '', '', '999999999.99', '', '', '', '999', '999', '999', '999', '999', '', '', '', '', '', ''],
                       ['99', '', '', '', '', '###,###,###.00', '', '###,###,###.00', '', '##.0000', '###,###,###.00', '##.0000', '', '', '', '###,###,###.00', '', '##,###,###.0000000', '', '999', '999', '999', '999', '999', '', '', '', '', '', '']);

        alignColsInGrid(fg, [0, 5, 6, 7, 9, 10, 11, 15, 16, 17, 19, 20, 21, 22, 23]);
        fg.FrozenCols = 3;

        //  setCalcFieldsFornecedores();
    }
    else if (folderID == 21066) // Custo Medio
    {
        var sReadOnly = '*';

        headerGrid(fg, ['Data',
                       'M�todo',
                       'Enc',
                       'QE',
                       'QP',
                       'QT',
                       'Moeda',
                       'CE',
                       'CP',
                       'CM',
                       'DE',
                       'DP',
                       'DM',
                       'PE',
                       'PP',
                       'PM',
                       'Moeda',
                       'CCE',
                       'CCP',
                       'CCM',
                       'Pedido',
                       'CFOP',
                       'Pessoa',
                       'Unit�rio',
                       'Taxa',
					   'Observa��o',
                       'RelPesConCustoID'], [26]);

        // array de celulas com hint
        glb_aCelHint = [[0, 3, 'Quantidade do Estoque'],
                        [0, 4, 'Quantidade do Pedido'],
                        [0, 5, 'Quantidade Total'],
                        [0, 7, 'Custo do Estoque'],
                        [0, 8, 'Custo do Pedido'],
                        [0, 9, 'Custo M�dio'],
                        [0, 10, 'Despesa do Estoque'],
                        [0, 11, 'Depesa do Pedido'],
                        [0, 12, 'Despesa M�dia'],
                        [0, 13, 'Pagamento do Estoque'],
                        [0, 14, 'Pagamento do Pedido'],
                        [0, 15, 'Pagamento M�dio'],
                        [0, 17, 'Custo Cont�bil do Estoque'],
                        [0, 18, 'Custo Cont�bil do Pedido'],
                        [0, 20, 'Custo Cont�bil M�dio']];

        fillGridMask(fg, currDSO, ['Data*',
                                 'Metodo*',
                                 'Encomenda*',
                                 'QuantidadeEstoque*',
                                 'QuantidadePedido*',
                                 'QuantidadeTotal*',
                                 'Moeda*',
                                 'CustoEstoque*',
                                 'CustoPedido*',
                                 'CustoMedio*',
                                 'DespesaEstoque*',
                                 'DespesaPedido*',
                                 'DespesaMedia*',
                                 'dtPagamentoEstoque*',
                                 'dtPagamentoPedido*',
                                 'dtMediaPagamento*',
                                 'Moeda1*',
                                 'CustoContabilEstoque*',
                                 'CustoContabilPedido*',
                                 'CustoContabilMedio*',
                                 'PedidoID*',
                                 'CFOPID*',
                                 'Pessoa*',
                                 'Unitario*',
			 				     'Taxa*',
                                 'Observacao',
                                 'RelPesConCustoID'],
                                 ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 [dTFormat + ' hh:mm', '', '', '', '', '', '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00',
                                 dTFormat, dTFormat, dTFormat, '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '', '', '', '###,###,###.00', '###,###,##.0000000', '', '']);

        alignColsInGrid(fg, [3, 4, 5, 7, 8, 9, 10, 11, 12, 17, 18, 19, 20, 23, 24]);
        fg.FrozenCols = 2;
    }
    else if (folderID == 21067) // Pedidos
    {
        headerGrid(fg, ['Data',
                       'Pedido',
                       'Est',
                       'Pessoa',
                       'CFOP',
                       'Quant',
                       'Moeda',
                       'Valor',
					   'Contribui��o',
					   'MC',
                       'Vendedor',
                       'Refer�ncia',
                       'Observa��o'], []);

        fillGridMask(fg, currDSO, ['dtMovimento',
                                 'PedidoID',
                                 'Estado',
                                 'Pessoa',
                                 'Operacao',
                                 'Quantidade',
                                 'Moeda',
                                 'ValorUnitario',
  								 'Contribuicao',
								 'Margem',
                                 'Vendedor',
                                 'PedidoReferencia',
                                 'Observacao'],
                                 ['', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 [dTFormat, '', '', '', '', '', '', '###,###,###.00', '###,###,###.00', '###.00', '', '', '']);

        alignColsInGrid(fg, [1, 5, 7, 8, 9, 11]);
        fg.FrozenCols = 2;
    }
    else if (folderID == 21068) // Movimentos
    {
        headerGrid(fg, ['Data           Hora',
                       'Pedido',
                       'Asstec',
                       'Transa��o',
                       'Estoque',
                       'Dep�sito',
                       'Quantidade'], []);

        fillGridMask(fg, currDSO, ['dtMovimento',
                                 'PedidoID',
                                 'AsstecID',
                                 'Transacao',
                                 'Estoque',
                                 'Deposito',
                                 'Quantidade'],
                                 ['', '', '', '', '', '', ''],
                                 [dTFormat + ' hh:mm:ss', '', '', '', '', '', '']);

        alignColsInGrid(fg, [6]);
        fg.FrozenCols = 2;
    }
    else if (folderID == 21069) // Numero de Serie
    {
        headerGrid(fg, ['ID',
                       'Numero S�rie   ',
                       'Estoque',
                       'Data',
                       'Pedido',
                       'Transa��o',
                       'Pessoa',
                       'ProdutoFantasia'], [7]);

        fillGridMask(fg, currDSO, ['ProdutoID',
                                 'NumeroSerie',
                                 'Estoque',
                                 'dtMovimento',
                                 'PedidoID',
                                 'Transacao',
                                 'Fantasia',
                                 'ProdutoFantasia'],
                                 ['', '', '', '', '', '', '', ''],
                                 ['', '', '', dTFormat, '', '', '', '']);
        alignColsInGrid(fg, [0, 4]);
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

    }
    else if (folderID == 21070) // Preco dos concorrentes
    {
        headerGrid(fg, ['Concorrente',
                       'Data',
                       'Pre�o',
                       'Varia��o',
                       'MC',
                       'Colaborador',
                       'Observa��o',
                       'ConcorrenteID',
                       'ColaboradorID',
                       'RelPesConConcorID'], [7, 8, 9]);

        fillGridMask(fg, currDSO, ['^ConcorrenteID^dso01GridLkp^PessoaID^Fantasia*',
                                 'Data',
                                 'Preco',
                                 '_Calc_Variacao_5*',
                                 '_Calc_Margem_5*',
                                 '^ColaboradorID^dso02GridLkp^PessoaID^Fantasia*',
                                 'Observacao',
                                 'ConcorrenteID',
                                 'ColaboradorID',
                                 'RelPesConConcorID'],
                                 ['', '99/99/9999', '999999999.99',
                                  '', '', '', '', '', '', ''],
                                 ['', dTFormat, '###,###,###.00',
                                  '', '', '', '', '', '', '']);

        setCalcFieldsPrecoConcorrentes();
    }
    else if (folderID == 21073) // Equipes
    {
        headerGrid(fg, ['Equipe',
                       'Perc',
                       'Quant',
                       'RV',
                       'Dispon�vel',
                       'RelPesConEquipeID'], [5]);

        fillGridMask(fg, currDSO, ['EquipeID',
								 'Percentual',
								 'Quantidade',
								 '^RelPesConEquipeID^dso01GridLkp^RelPesConEquipeID^Reserva*',
								 '^RelPesConEquipeID^dso01GridLkp^RelPesConEquipeID^Disponivel*',
								 'RelPesConEquipeID'],
                                 ['', '999', '999999', '999999', '999999', ''],
                                 ['', '###', '######', '######', '######', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1, '######', 'S'], [2, '##########', 'S'],
			[3, '##########', 'S'], [4, '##########', 'S']]);
        alignColsInGrid(fg, [1, 2, 3, 4]);
    }
    else if (folderID == 21074) // Campanhas
    {
        headerGrid(fg, ['Tipo',
                       'Emiss�o',
                       'In�cio',
                       'Fim',
                       'ID',
                       'Est',
                       'Campanha',
                       'C�digo',
                       'Fornecedor',
                       'Quant',
                       '$',
                       'Valor',
                       'Saldo Quant',
                       'Saldo Valor'], []);

        fillGridMask(fg, currDSO, ['Tipo',
								 'dtEmissao',
								 'dtInicio',
								 'dtFim',
								 'CampanhaID',
								 'Estado',
								 'Campanha',
								 'Codigo',
								 'Fornecedor',
								 'Quantidade',
								 'SimboloMoeda',
								 'ValorCampanha',
								 'SaldoQuantidade',
								 'SaldoValor'],
                                 ['', '99/99/9999', '99/99/9999', '99/99/9999', '', '', '', '', '', '999999', '999999999.99',
								  '999999999.99', '999999999.99', '999999999.99'],
                                 ['', dTFormat, dTFormat, dTFormat, '', '', '', '', '', '######', '###,###,##0.00',
								  '###,###,##0.00', '###,###,##0.00', '###,###,##0.00']);

        alignColsInGrid(fg, [4, 7, 9, 11, 12, 13]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 21075) // Localizacoes de Estoque
    {
        headerGrid(fg, ['Ordem',
					   'Localiza��o',
					   'RD',
					   'Cx Total',
					   'Cx Fila',
					   'Cx Pilha',
                       'RelPesConLocalizacaoID'], [6]);

        glb_aCelHint = [[0, 2, 'Reposi��o di�ria?'],
						[0, 3, 'Quantidade de caixas total'],
						[0, 4, 'Quantidade de caixas por fila'],
						[0, 5, 'Quantidade de caixas por pilha']];

        fillGridMask(fg, currDSO, ['Ordem',
								 'LocalizacaoID',
								 '^RelPesConLocalizacaoID^dso01GridLkp^RelPesConLocalizacaoID^ReposicaoDiaria*',
								 '^RelPesConLocalizacaoID^dso01GridLkp^RelPesConLocalizacaoID^QuantidadeCaixasTotal*',
								 '^RelPesConLocalizacaoID^dso01GridLkp^RelPesConLocalizacaoID^QuantidadeCaixasFila*',
								 '^RelPesConLocalizacaoID^dso01GridLkp^RelPesConLocalizacaoID^QuantidadeCaixasPilha*',
								 'RelPesConLocalizacaoID'],
								['999', '', '', '', '', '', ''],
								['', '', '', '', '', '', '']);

        alignColsInGrid(fg, [0, 3, 4, 5]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

    }
    else if (folderID == 21076) // Lojas web
    {
        headerGrid(fg, ['Loja',
                       'RelPesConLojaID'], [1]);

        fillGridMask(fg, currDSO, ['LojaID',
								 'RelPesConLojaID'],
                                 ['', ''],
                                 ['', '']);

        alignColsInGrid(fg, []);
    }
}
