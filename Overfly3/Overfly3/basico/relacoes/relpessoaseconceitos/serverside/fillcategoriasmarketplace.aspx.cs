using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.Attributes;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class fillcategoriasmarketplace : System.Web.UI.OverflyPage
	{
        private string busca;
        private Integer ProgramaMarketingID;

		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(ListaCategorias());
		}

        protected DataSet ListaCategorias()
		{
            // Roda a procedure sp_ProdutoMargemMinima_Pesquisar
			ProcedureParameters[] procParams = new ProcedureParameters[2];
            
            procParams[0] = new ProcedureParameters(
                "@Busca",
                System.Data.SqlDbType.VarChar,
                busca != "null" ? (Object)busca : DBNull.Value);

            procParams[1] = new ProcedureParameters(
               "@ProgramaMarketingID",
               System.Data.SqlDbType.Int,
               ProgramaMarketingID != null ? (Object)ProgramaMarketingID.ToString() : DBNull.Value);

			return DataInterfaceObj.execQueryProcedure(
                "sp_MarketplaceCategoria_Busca",
				procParams);
		}



        public Integer nProgramaMarketingID
        {
            get { return ProgramaMarketingID; }
            set { ProgramaMarketingID = (value.intValue() == 0 ? null : value); }
        }

   
        public string sBusca
		{
			get { return busca; }
            set { busca = (value != null ? value : null); }
		}
	}
}
