using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class inserirprodutosmarketplace : System.Web.UI.OverflyPage
    {
        private Integer[] RelacaoID;

        public Integer[] nRelacaoID
        {
            get { return RelacaoID; }
            set { RelacaoID = value; }
        }
        private Integer[] ProgramaMarketingID;

        public Integer[] nProgramaMarketingID
        {
            get { return ProgramaMarketingID; }
            set { ProgramaMarketingID = value; }
        }

        private Integer[] UsuarioID;

        public Integer[] nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
       

        protected int Executar()
        {
            int fldresp;
            string sql = "";
            int nDataLen = RelacaoID.Length;

            for (int i = 0; i < nDataLen; i++)
            {
                sql = sql + " INSERT INTO RelacoesPesCon_VendaDigital (RelacaoID, ProgramaMarketingID, EstadoID, Deletado, PrecoFabricante, PrecoPraticado, dtAgendamento, UsuarioID) " +
                                " SELECT " + RelacaoID[i].ToString() + ", " + ProgramaMarketingID[i].ToString() + ", 1, 0, 0.00, 0.00, NULL, " + UsuarioID[i].ToString() + " ";
            }

            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            return fldresp;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = Executar();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + fldresp + " as fldresp"
                )
            );
        }
    }
}
