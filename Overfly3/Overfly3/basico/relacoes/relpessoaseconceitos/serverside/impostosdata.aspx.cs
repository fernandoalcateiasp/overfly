using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class impostosdata : System.Web.UI.OverflyPage
    {

        public string strSQL, sAliquota, sBaseCalculo;


        private Integer RelacaoID;
        public Integer nRelacaoID
        {
            get { return RelacaoID; }
            set { RelacaoID = value; }
        }
        private Integer RelacaoSimilarID;
        public Integer nRelacaoSimilarID
        {
            get { return RelacaoSimilarID; }
            set { RelacaoSimilarID = value; }
        }
        private Integer Pos;
        public Integer nPos
        {
            get { return Pos; }
            set { Pos = value; }
        }

        private string NCM;
        public string sNCM
        {
            get { return NCM; }
            set { NCM = value; }
        }


        protected string  Executar()
        {

            sAliquota = "a.Aliquota ";
            sBaseCalculo = "a.BaseCalculo ";

            if (Pos.intValue() == 0)
            {
                if ((RelacaoSimilarID.intValue()) != 0)
                {
                    sAliquota = "(SELECT TOP 1 aa.Aliquota " +
                            "FROM RelacoesPesCon_Impostos aa WITH(NOLOCK) " +
                            "WHERE(aa.RelacaoID = " + RelacaoSimilarID.ToString() + " AND " +
                                "aa.TipoID = a.TipoID AND aa.ImpostoID = a.ImpostoID)) AS Aliquota";

                    sBaseCalculo = "(SELECT TOP 1 aa.BaseCalculo " +
                            "FROM RelacoesPesCon_Impostos aa WITH(NOLOCK) " +
                            "WHERE(aa.RelacaoID = " + RelacaoSimilarID.ToString() + " AND " +
                                "aa.TipoID = a.TipoID AND aa.ImpostoID = a.ImpostoID)) AS BaseCalculo";
                }

                strSQL += "SELECT a.RelPesConImpostoID, '" + NCM.ToString() + "' AS NCM, RTRIM(LTRIM(b.ItemMasculino)) AS Tipo, " +
                         "c.Imposto AS Imposto, " + sAliquota.ToString() + ", " + sBaseCalculo.ToString() + " " +
                         "FROM RelacoesPesCon_Impostos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " +
                         "WHERE a.RelacaoID=" + RelacaoID.ToString() + " AND a.TipoID=b.ItemID AND a.ImpostoID=c.ConceitoID " +
                         "ORDER BY a.TipoID, Imposto";
            }

            else if ((Pos.intValue() == 1))
            {
                if (RelacaoSimilarID.intValue() != 0)
                {

                    sAliquota = "(SELECT TOP 1 aa.Aliquota " +
                                    "FROM RelacoesPesCon_Impostos aa WITH(NOLOCK) " +
                                    "WHERE(aa.RelPesConFornecID = " + (nRelacaoSimilarID.ToString()) + " AND " +
                                        "aa.ImpostoID = a.ImpostoID)) AS Aliquota";

                    sBaseCalculo = "(SELECT TOP 1 aa.BaseCalculo " +
                                    "FROM RelacoesPesCon_Impostos aa WITH(NOLOCK) " +
                                    "WHERE(aa.RelPesConFornecID = " + (nRelacaoSimilarID.ToString()) + " AND " +
                                        "aa.ImpostoID = a.ImpostoID)) AS BaseCalculo";
                }

                strSQL = "SELECT a.RelPesConImpostoID, '" + NCM.ToString() + "' AS NCM, NULL AS Tipo, b.Imposto AS Imposto, " + sAliquota.ToString() + ", " + sBaseCalculo.ToString() + " " +
                     "FROM RelacoesPesCon_Impostos a WITH(NOLOCK), Conceitos b WITH(NOLOCK) " +
                     "WHERE a.RelPesConFornecID=" + (nRelacaoID.ToString()) + " AND a.ImpostoID=b.ConceitoID " +
                     "ORDER BY Imposto";
            }
            return (strSQL);
        }
        protected override void PageLoad(object sender, EventArgs e)
        {

            WriteResultXML(DataInterfaceObj.getRemoteData(Executar()));
        }
    }
}
