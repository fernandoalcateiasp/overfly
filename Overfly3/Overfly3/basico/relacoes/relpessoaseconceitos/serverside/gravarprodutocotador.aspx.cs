using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class gravarprodutocotador : System.Web.UI.OverflyPage
    {
        private String PartNumber;

        public String sPartNumber
        {
            get { return PartNumber; }
            set { PartNumber = value; }
        }

        private Integer EstadoID;

        public Integer nEstadoID
        {
            get { return EstadoID; }
            set { EstadoID = value; }
        }
        
        private String Produto;

        public String sProduto
        {
            get { return Produto; }
            set { Produto = value; }
        }

        private Integer MarcaID;

        public Integer nMarcaID
        {
            get { return MarcaID; }
            set { MarcaID = value; }
        }
        
        private Integer OrigemID;

        public Integer nOrigemID
        {
            get { return OrigemID; }
            set { OrigemID = value; }
        }

        private Integer TipoProdutoID;

        public Integer nTipoProdutoID
        {
            get { return TipoProdutoID; }
            set { TipoProdutoID = value; }
        }

        private String NCMID;

        public String nNCMID
        {
            get { return NCMID; }
            set { NCMID = value; }
        }

        private Integer MoedaID;

        public Integer nMoedaID
        {
            get { return MoedaID; }
            set { MoedaID = value; }
        }

        private String PrecoLista;

        public String nPrecoLista
        {
            get { return PrecoLista; }
            set { PrecoLista = value; }
        }

        private String Desconto;

        public String nDesconto
        {
            get { return Desconto; }
            set { Desconto = (value != null ? value : "NULL"); }
        }

        private String Substituto;

        public String sSubstituto
        {
            get { return Substituto; }
            set { Substituto = (value != null ? value : "NULL"); }
        }

        private String VerTambem;

        public String sVerTambem
        {
            get { return VerTambem; }
            set { VerTambem = (value != null ? value : "NULL"); }
        }

        private String CVPFim;

        public String sCVPFim
        {
            get { return CVPFim; }
            set { CVPFim = value; }
        }

        private String Observacao;

        public String sObservacao
        {
            get { return Observacao; }
            set { Observacao = (value != null ? value : "NULL"); }
        }

        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private Integer TaxaConversaoID;
        public Integer nTaxaConversaoID
        {
            get { return TaxaConversaoID; }
            set { TaxaConversaoID = value; }
        }

        protected string Executar()
        {
            string fldresp;
            string sql = "";

            if (Substituto != "null")
                Substituto = "'" + Substituto + "'";

            if (VerTambem != "null")
                VerTambem = "'" + VerTambem + "'";

            if (Observacao != "null")
                Observacao = "'" + Observacao + "'";

            if (NCMID.Length == 0)
                NCMID = "NULL";
            else
                NCMID = "'" + NCMID + "'";

            sql = "INSERT INTO CorpProdutos (Produto, EstadoID, DescricaoProduto, MarcaID, OrigemID, TipoProdutoID, NCMID, MoedaID, PrecoLista, DescontoPadrao, " +
                                    "ProdutoSubstituto, ProdutoVerTambem, dtVendaFim, Observacao, UsuarioID, TaxaConversaoID) " +
                        " SELECT '" + PartNumber + "' , " + EstadoID.ToString() + ", '" + Produto + "', " + MarcaID.ToString() + ", " + OrigemID.ToString() + ", " + TipoProdutoID.ToString() + "," +
                                    NCMID + ", " + MoedaID.ToString() + ", " + PrecoLista + ", " + Desconto + ", " + Substituto + ", " + VerTambem + ", " +
                                    (CVPFim == "" ? "null" : "'" + CVPFim + "'") +
                                    ", " + Observacao + ", " + UsuarioID.ToString() + ", " + (TaxaConversaoID == null ? "NULL" : TaxaConversaoID.ToString());

            try
            {
                fldresp = (DataInterfaceObj.ExecuteSQLCommand(sql)).ToString();                
            }
            catch(System.Exception e)
            {
                fldresp = e.Message;               
            }

            return fldresp;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string fldresp = Executar();

            WriteResultXML(DataInterfaceObj.getRemoteData("select '" + fldresp + "' as fldresp"));
        }
    }
}