﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class ConsolidarProprietarios : System.Web.UI.OverflyPage
    {

        private string RegistrosProcessaodos;
        private Integer[] marcaID;

        public Integer[] MarcaID
        {
            get { return marcaID; }
            set { marcaID = value; }
        }
        private Integer[] linhaProdutoID;

        public Integer[] LinhaProdutoID
        {
            get { return linhaProdutoID; }
            set { linhaProdutoID = value; }
        }
        private Integer[] familiaID;

        public Integer[] FamiliaID
        {
            get { return familiaID; }
            set { familiaID = value; }
        }
        private Integer[] paisID;

        public Integer[] PaisID
        {
            get { return paisID; }
            set { paisID = value; }
        }
        private Integer[] proprietarioID;

        public Integer[] ProprietarioID
        {
            get { return proprietarioID; }
            set { proprietarioID = value; }
        }
        private Integer[] proprietarioParaID;

        public Integer[] ProprietarioParaID
        {
            get { return proprietarioParaID; }
            set { proprietarioParaID = value; }
        }
        private string[] resultado;

        public string[] Resultado
        {
            get { return resultado; }
            set { resultado = value; }
        }
        private Integer[] UsuarioID;

        public Integer[] UserID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            Consolida();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                    "select '" + RegistrosProcessaodos + "' as fldResponse"));

        }

        // Roda a procedure sp_ManifestoNFe_Exporta
        private void Consolida()
        {
            int datalen = MarcaID.Length;

            for (int i = 0; i < datalen; i++)
            {
                try
                {
                    ProcedureParameters[] procParams = new ProcedureParameters[8];
                    procParams[0] = new ProcedureParameters(
                        "@MarcaID",
                        System.Data.SqlDbType.Int,
                        marcaID[i] != null ? marcaID[i] : new Integer(0));

                    procParams[1] = new ProcedureParameters(
                        "@LinhaProdutoID",
                        System.Data.SqlDbType.Int,
                        linhaProdutoID[i] != null ? linhaProdutoID[i] : new Integer(0));

                    procParams[2] = new ProcedureParameters(
                        "@FamiliaID",
                        System.Data.SqlDbType.Int,
                        familiaID[i] != null ? familiaID[i] : new Integer(0));

                    procParams[3] = new ProcedureParameters(
                        "@PaisID",
                        System.Data.SqlDbType.Int,
                        paisID[i] != null ? paisID[i] : new Integer(0));

                    procParams[4] = new ProcedureParameters(
                        "@ProprietarioID",
                        System.Data.SqlDbType.Int,
                        proprietarioID[i] != null ? proprietarioID[i] : new Integer(0));

                    procParams[5] = new ProcedureParameters(
                        "@ProprietarioParaID",
                        System.Data.SqlDbType.Int,
                        proprietarioParaID[i] != null ? proprietarioParaID[i] : new Integer(0));

                    procParams[6] = new ProcedureParameters(
                        "@Resultado",
                        System.Data.SqlDbType.Bit,
                        resultado[i] != null ? (resultado[i].ToString() == "0" ? false : true ): false);

                    procParams[7] = new ProcedureParameters(
                        "@RegistrosProcessados",
                        SqlDbType.Int,
                        DBNull.Value,
                        ParameterDirection.InputOutput);

                    DataInterfaceObj.execNonQueryProcedure(
                        "sp_Produto_ProprietarioConsolidar",
                        procParams);

                    // Obtém o resultado da execução.
                    RegistrosProcessaodos = procParams[7].Data.ToString();

                }
                catch (System.Exception exception)
                {

                    //erros += "#ERROR#OverflyPage (" + this.GetType().Name + "):" + exception.Message + " \r\n ";
                    RegistrosProcessaodos += exception.Message + " Erro ! \r\n ";
                }
            }
        }
    }
}