using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class setupmarketplace : System.Web.UI.OverflyPage
    {
        private Integer LocalidadeID;

        public Integer nLocalidadeID
        {
            get { return LocalidadeID; }
            set { LocalidadeID = value; }
        }
        private Integer LocMarketplaceID;

        public Integer nLocMarketplaceID
        {
            get { return LocMarketplaceID; }
            set { LocMarketplaceID = value; }
        }

        private Integer Valor;

        public Integer nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }

        private Integer ControlBtn;

        public Integer nControlBtn
        {
            get { return ControlBtn; }
            set { ControlBtn = value; }
        }


        private Integer TipoParametroID;

        public Integer nTipoParametroID
        {
            get { return TipoParametroID; }
            set { TipoParametroID = value; }
        }

        protected int Executar()
        {
            int fldresp;
            string sql = "";


            if ((ControlBtn.intValue() == 0))
            {
                sql = sql + " UPDATE Localidades_Marketplace " +
                                " SET Valor = " + Valor.ToString() +
                                " WHERE LocMarketplaceID = " + LocMarketplaceID.ToString() + " ";
            }

            else if ((ControlBtn.intValue() == 1))
            {
                sql = sql + " INSERT INTO Localidades_Marketplace (LocalidadeID, TipoParametroID, Valor) " +
                                " SELECT " + LocalidadeID.ToString() + ", " + TipoParametroID.ToString() + ", " + Valor.ToString() + " ";
            }

            else if ((ControlBtn.intValue() == 2))
            {
                sql = sql + " DELETE FROM Localidades_Marketplace " +
                                " WHERE LocMarketplaceID = " + LocMarketplaceID.ToString() + " ";
            }

            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            return fldresp;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = Executar();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + fldresp + " as fldresp"
                )
            );
        }
    }
}
