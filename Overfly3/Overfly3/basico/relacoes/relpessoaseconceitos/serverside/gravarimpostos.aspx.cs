using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class gravarimpostos : System.Web.UI.OverflyPage
    {
        private Integer[] relPesconImpostoID;

        public Integer[] nRelPesconImpostoID
        {
            get { return relPesconImpostoID; }
            set { relPesconImpostoID = value; }
        }

        private Integer[] Aliquota;

        public Integer[] nAliquota
        {
            get { return Aliquota; }
            set { Aliquota = value; }
        }
        private Integer[] BaseCalculo;

        public Integer[] nBaseCalculo
        {
            get { return BaseCalculo; }
            set { BaseCalculo = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp;
            string sql = "";
            int nDataLen = relPesconImpostoID.Length;

            for (int i = 0; i < nDataLen; i++)
            {
                sql += " UPDATE RelacoesPesCon_Impostos SET Aliquota=" + Aliquota[i].ToString() + ", " +
                  "BaseCalculo=" + BaseCalculo[i].ToString() + " " +
                  "WHERE (RelPesConImpostoID = " + relPesconImpostoID[i].ToString() + ") ";

            }
            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + fldresp + " as fldresp"
                )
            );
        }
    }
}
