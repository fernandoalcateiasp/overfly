using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class verificacaorelacao : System.Web.UI.OverflyPage
    {
        private int response;
        private string mensagem;        

        private Integer RelacaoID;

        public Integer nRelacaoID
        {
            get { return RelacaoID; }
            set { RelacaoID = value; }
        }
        private Integer EstadoDeID;

        public Integer nEstadoDeID
        {
            get { return EstadoDeID; }
            set { EstadoDeID = value; }
        }
        private Integer EstadoParaID;

        public Integer nEstadoParaID
        {
            get { return EstadoParaID; }
            set { EstadoParaID = value; }
        }

        protected void ConceitoVerifica()
        {
            ProcedureParameters[] param = new ProcedureParameters[5];

            param[0] = new ProcedureParameters(
                "@RelacaoID",
                SqlDbType.Int,
                RelacaoID.intValue());

            param[1] = new ProcedureParameters(
                "@EstadoDeID",
                SqlDbType.Int,
                nEstadoDeID.intValue());

            param[2] = new ProcedureParameters(
                "@EstadoParaID",
                SqlDbType.Int,
                nEstadoParaID.intValue());

            param[3] = new ProcedureParameters(
                "@Resultado",
                SqlDbType.Int,
                DBNull.Value,
                ParameterDirection.InputOutput);

            param[4] = new ProcedureParameters(
                "@Mensagem",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);

            param[4].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure("sp_RelPesCon_Verifica", param);

            // Obt�m o resultado da execu��o.
            response = int.Parse(param[3].Data.ToString());
            mensagem = param[4].Data.ToString();
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            ConceitoVerifica();

            WriteResultXML(
                DataInterfaceObj.getRemoteData("select " + response + " as Resultado, '" + mensagem + "' as Mensagem")
            );

        }
    }
}