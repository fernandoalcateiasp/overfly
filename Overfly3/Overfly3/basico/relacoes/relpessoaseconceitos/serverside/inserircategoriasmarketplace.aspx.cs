using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;
using Overfly3.Attributes;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class inserircategoriasmarketplace : System.Web.UI.OverflyPage
    {
        private Integer[] ProdutoID = Constants.INT_EMPTY_SET;
        private Integer[] ProgramaMarketingID = Constants.INT_EMPTY_SET;
        private String[] CodigoMarketplace;

        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = CategoriasVendasDigitais();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + fldresp + " as fldresp"
                )
            );
        }

        protected int CategoriasVendasDigitais()
        {
            int fldresp;
            string sql = "";
            int nDataLen = ProdutoID.Length;

            for (int i = 0; i < nDataLen; i++)
            {
                sql = sql + "EXEC sp_Produto_VendasDigitais_CategoriaGravar " + ProdutoID[i].ToString() + ", " + ProgramaMarketingID[i].ToString() + ", '" + CodigoMarketplace[i] + "' ";
            }

            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            return fldresp;

        }


        public Integer[] nProdutoID
        {
            get { return ProdutoID; }
            set { ProdutoID = value; }
        }

        public Integer[] nProgramaMarketingID
        {
            get { return ProgramaMarketingID; }
            set { ProgramaMarketingID = value; }
        }

        public String[] sCodigoMarketplace
        {
            get { return CodigoMarketplace; }
            set { CodigoMarketplace = value; }
        }
      
    }
}
