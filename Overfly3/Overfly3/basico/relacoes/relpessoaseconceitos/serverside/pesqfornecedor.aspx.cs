using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class pesqfornecedor : System.Web.UI.OverflyPage
    {

        public string strOperator, strSQL;

        private string sstrtofind;

        public string strToFind
        {
            get { return sstrtofind; }
            set { sstrtofind = value; }
        }
        private Integer SujeitoID;

        public Integer nSujeitoID
        {
            get { return SujeitoID; }
            set { SujeitoID = value; }
        }
        private Integer RelacaoID;

        public Integer nRelacaoID
        {
            get { return RelacaoID; }
            set { RelacaoID = value; }
        }


        protected string Executar()
        {
            if ((sstrtofind.Substring(0, 1) == "%") || (sstrtofind.Substring(0, (sstrtofind.Length - 1)) == "%"))
            {
                strOperator = " LIKE ";
            }
            else
            {
                strOperator = " >= ";
            }

            strSQL = "SELECT TOP 100 b.Nome AS fldName,b.PessoaID AS fldID,b.Fantasia AS Fantasia, " +
             "dbo.fn_Pessoa_Documento(b.PessoaID, NULL, NULL, 111, 111, 0) AS Documento, f.Localidade " +
             "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " +
             "FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " +
             "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
             "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
             "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
             "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
             "WHERE a.EstadoID = 2 AND a.TipoRelacaoID = 21 AND a.SujeitoID = " + SujeitoID.ToString() + " AND a.ObjetoID = b.PessoaID " +
             "AND a.ObjetoID NOT IN (SELECT FornecedorID FROM RelacoesPesCon_Fornecedores WHERE RelacaoID = " + RelacaoID.ToString() + " AND FornecedorID IS NOT NULL) " +
             "AND b.Nome " + strOperator + "'" + sstrtofind + "' " +
             "ORDER BY fldName ";

            return strSQL;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string ssql = Executar();

            WriteResultXML(DataInterfaceObj.getRemoteData(ssql));
        }
    }
}
