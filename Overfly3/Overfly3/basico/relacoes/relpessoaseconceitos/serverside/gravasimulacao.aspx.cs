using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class gravasimulacao : System.Web.UI.OverflyPage
    {
        private Integer RelacaoID;

        public Integer nRelacaoID
        {
            get { return RelacaoID; }
            set { RelacaoID = value; }
        }
        private Integer MargemContribuicao;

        public Integer nMargemContribuicao
        {
            get { return MargemContribuicao; }
            set { MargemContribuicao = value; }
        }
        private Integer MC;

        public Integer nMC
        {
            get { return MC; }
            set { MC = value; }
        }
        private Integer MP;

        public Integer nMP
        {
            get { return MP; }
            set { MP = value; }
        }
        private Integer Mpub;

        public Integer nMpub
        {
            get { return Mpub; }
            set { Mpub = value; }
        }
        private Integer MMin;

        public Integer nMMin
        {
            get { return MMin; }
            set { MMin = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }


        protected int Executar()
        {
            int fldresp;
            string sql = "UPDATE RelacoesPesCon SET  ";
            string strUpdates = "";

            if (MC.intValue() == 1)
            {
                strUpdates += "MargemContribuicao = " + MargemContribuicao.ToString();
            }

            if (MP.intValue() == 1)
            {
                if (strUpdates != "")
                {
                    strUpdates += ", ";
                }

                strUpdates += "MargemPadrao = " + MargemContribuicao.ToString();
            }

            if (Mpub.intValue() == 1)
            {
                if (strUpdates != "")
                {
                    strUpdates += ", ";
                }

                strUpdates += "MargemPublicacao = " + MargemContribuicao.ToString();
            }
            if (MMin.intValue() == 1)
            {
                if (strUpdates != "")
                {
                    strUpdates += ", ";
                }

                strUpdates += "MargemMinima = " + MargemContribuicao.ToString();
            }

            strUpdates += ", UsuarioID = " + UsuarioID.ToString();

            sql += strUpdates + "WHERE RelacaoID = " + RelacaoID.ToString();


            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            return fldresp;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = Executar();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + fldresp + " as Resultado"
                )
            );
        }
    }
}
