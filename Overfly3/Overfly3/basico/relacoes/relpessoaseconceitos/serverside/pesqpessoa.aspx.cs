using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class pesqpessoa : System.Web.UI.OverflyPage
    {

        public string strSQL;


        private Integer PessoaID;

        public Integer nPessoaID
        {
            get { return PessoaID; }
            set { PessoaID = value; }
        }
        private Integer Tipo;

        public Integer nTipo
        {
            get { return Tipo; }
            set { Tipo = value; }
        }
        private string StrtoFind;

        public string strtoFind
        {
            get { return StrtoFind; }
            set { StrtoFind = value; }
        }
        protected string Executar()
        {
            if (Tipo.intValue() == 1)
            {


                strSQL = "SELECT TOP 100 a.Nome AS fldName,a.PessoaID AS fldID,a.Fantasia AS Fantasia, " +
                         "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 111, 0) AS Documento, e.Localidade " +
                         "AS Cidade, f.CodigoLocalidade2 AS UF, g.CodigoLocalidade2 AS Pais " +
                         "FROM Pessoas a WITH(NOLOCK) " +
                         "LEFT OUTER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON (a.PessoaID = d.PessoaID AND (d.Ordem=1)) " +
                         "LEFT OUTER JOIN Localidades e WITH(NOLOCK) ON d.CidadeID = e.LocalidadeID " +
                         "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON d.UFID = f.LocalidadeID " +
                         "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON d.PaisID = g.LocalidadeID " +
                         "WHERE a.EstadoID = 2 AND a.TipoPessoaID = 52 " +
                         "AND a.Nome >= '" + StrtoFind + "' " +
                         "ORDER BY fldName ";


            }
            else if (Tipo.intValue() == 2)
            {
                strSQL = "SELECT TOP 100 a.Nome AS fldName,a.PessoaID AS fldID,a.Fantasia AS Fantasia, " +
                    "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 101, 101, 0) AS Documento, e.Localidade " +
                    "AS Cidade, f.CodigoLocalidade2 AS UF, g.CodigoLocalidade2 AS Pais " +
                    "FROM Pessoas a WITH(NOLOCK) " +
                    "LEFT OUTER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON (a.PessoaID = d.PessoaID AND (d.Ordem=1)) " +
                    "LEFT OUTER JOIN Localidades e WITH(NOLOCK) ON d.CidadeID = e.LocalidadeID " +
                    "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON d.UFID = f.LocalidadeID " +
                    "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON d.PaisID = g.LocalidadeID " +
                    "WHERE a.EstadoID = 2 AND a.TipoPessoaID = 51 " +
                    "AND a.Nome >= '" + StrtoFind + "' " +
                    "ORDER BY fldName ";
            }
            return strSQL;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            string ssql = Executar();

            WriteResultXML(DataInterfaceObj.getRemoteData(ssql));
        }
    }
}
