using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class atualizaprecomargem : System.Web.UI.OverflyPage
    {

        private Integer moedaEntradaID = new Integer(0);

        public Integer nMoedaEntradaID
        {
            get { return moedaEntradaID; }
            set { moedaEntradaID = value; }
        }
        private Integer custoBase = new Integer(0);

        public Integer nCustoBase
        {
            get { return custoBase; }
            set { custoBase = value; }
        }
        private Integer margemContribuicao = new Integer(0);

        public Integer nMargemContribuicao
        {
            get { return margemContribuicao; }
            set { margemContribuicao = value; }
        }
        private Integer margemAjustada = new Integer(0);

        public Integer nMargemAjustada
        {
            get { return margemAjustada; }
            set { margemAjustada = value; }
        }
        private Integer aliquota = new Integer(0);

        public Integer nAliquota
        {
            get { return aliquota; }
            set { aliquota = value; }
        }
        private Integer impostosIncidencia = new Integer(0);

        public Integer nImpostosIncidencia
        {
            get { return impostosIncidencia; }
            set { impostosIncidencia = value; }
        }
        private Integer moedaSaida = new Integer(0);

        public Integer nMoedaSaida
        {
            get { return moedaSaida; }
            set { moedaSaida = value; }
        }
        private Integer empresaID = new Integer(0);

        public Integer nEmpresaID
        {
            get { return empresaID; }
            set { empresaID = value; }
        }
        private Integer produtoID = new Integer(0);

        public Integer nProdutoID
        {
            get { return produtoID; }
            set { produtoID = value; }
        }
        private Integer tipo = new Integer(0);

        public Integer nTipo
        {
            get { return tipo; }
            set { tipo = value; }
        }
        private Integer preco = new Integer(0);

        public Integer nPreco
        {
            get { return preco; }
            set { preco = value; }
        }



        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "";

            /*string sql = "SELECT OperacaoID AS fldID, OperacaoID AS fldName " +
                     "FROM Operacoes ";

            if (grupo.Equals("CFOP"))
            {
                sql += "WHERE EstadoID=2 AND TipoOperacaoID = 651 ";
            }
            else if (grupo.Equals("TRANSACOES"))
            {
                sql += "WHERE EstadoID=2 AND TipoOperacaoID = 652 ";
            }

            sql += "AND Nivel = " + (nivel.intValue() - 1) + " ORDER BY fldName";*/

            if (tipo.intValue() == 1)
            {
                // FUNCAO DE PRE�O PASSADO TODOS NULOS ESSA MODAL N�O � UTILIZADA.
                sql = "SELECT dbo.fn_Preco_Preco(a.SujeitoID, a.ObjetoID, NULL, " +
                    "( " + custoBase.ToString() + " * dbo.fn_Preco_Cotacao(" + moedaEntradaID.ToString() + ", a.MoedaEntradaID, NULL, GETDATE())) , " +
                     margemContribuicao.ToString() + "," + aliquota.ToString() + "," + moedaSaida.ToString() + ", GETDATE(), " + margemAjustada.ToString() + ", " + impostosIncidencia.ToString() +
                    " , 1, 1, NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL, NULL, NULL) AS PrecoMargem, " + tipo.ToString() + " AS Tipo " +
                    "FROM RelacoesPesCon a WITH(NOLOCK) " +
                    "WHERE (a.ObjetoID = " + produtoID.ToString() + " AND a.SujeitoID = " + empresaID.ToString() + " AND a.TipoRelacaoID = 61)";


                /*	dbo.fn_Preco_Preco
                 * @EmpresaID INT,  -- Empresa do produto -
                 * @ProdutoID INT,  -- Produto a ter o seu pre�o calculado-
                 * @ProdutoAlternativoID INT,	-- Produto alternativo-
                 * @Custo NUMERIC(11,2),  -- Custo do produto. Default: custo do cadastro de produto-
                 * @MargemContribuicao NUMERIC(5,2),  -- MC para a forma��o do pre�o. Default: MC do cadastro do produto-
                 * @AliquotaImposto NUMERIC(5,2),  -- Aliquota do imposto de sa�da (ICMS) para calcular o pre�o. Se for nulo ser� tratado pela fun��o FatorImpostosSaida-
                 * @MoedaSaidaID INT,  -- ID da moeda de sa�da do pre�o. Default: moeda de sa�da do cadastro do produto-
                 * @dtAtualizacao DATETIME,  -- Data de atualiza��o do custo e das cota��es das moedas-
                 * @Atualiza BIT,  -- Atualiza o custo (TaxaEstoque, dtPagamento)? Default: 0-
                 * @ImpostoIncidencia BIT,  -- Utiliza o grupo de imposto alternativo? Default: 0-
                 * @IncluiImpostoNaoIncluso BIT,  -- Calcula com o imposto n�o incluso (IPI)? Default: 0-
                 * @MantemImpostoIncluso  BIT,  -- Calcula com o imposto incluso (ICMS)? Default: 1-
                 * @PessoaID INT, -- Pessoa para a qual o pre�o sera calculado.-
                 * @CanalID INT, -- Utilizar sem pessoa-
                 * @EhContribuinte BIT,  -- Utilizar sem pessoa-
                 * @SimplesNacional BIT,  -- Utilizar sem pessoa-
                 * @UFID INT, -- Utilizar sem pessoa-
                 * @CidadeID INT, -- Utilizar sem pessoa-
                 * @CNAE INT, -- Utilizar sem pessoa
                 * @TipoPessoaID INT, -- Utilizar sem pessoa
                 * @FinalidadeID INT,
                 * @CFOPID INT,
                 * @PedItemID INT,
                 * @LoteID INT,
                 * @IncluiFrete BIT
                 */

            }
            else if (tipo.intValue() == 2)
            {
                sql = "SELECT dbo.fn_Preco_Preco(a.SujeitoID, a.ObjetoID, NULL, " +
                  "( " + custoBase.ToString() + " * dbo.fn_Preco_Cotacao(" + moedaEntradaID.ToString() + ", a.MoedaEntradaID, NULL, GETDATE())) , " +
                   margemContribuicao.ToString() + "," + aliquota.ToString() + "," + moedaSaida.ToString() + ", GETDATE(), " + margemAjustada.ToString() + ", " + impostosIncidencia.ToString() +
                  " , 1, 1, NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL,NULL, NULL) AS PrecoMargem, " + tipo.ToString() + " AS Tipo " +
                  "FROM RelacoesPesCon a WITH(NOLOCK) " +
                 "WHERE (a.ObjetoID = " + produtoID.ToString() + " AND a.SujeitoID = " + empresaID.ToString() + " AND a.TipoRelacaoID = 61)";
            }
            WriteResultXML(DataInterfaceObj.getRemoteData(sql));
        }
    }
}
