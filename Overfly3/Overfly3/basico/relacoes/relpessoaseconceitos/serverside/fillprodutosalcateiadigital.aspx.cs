using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.Attributes;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class fillprodutosalcateiadigital : System.Web.UI.OverflyPage
	{
        private string empresas;
   		private string prgmarketing;
        private string busca;
        private Integer PaisID;
        private Integer GerenteProdutoID;
        private Integer FamiliaID;
        private Integer MarcaID;
        private Integer LinhaProdutoID;
        private Integer ServicoID;
        private Integer RelacaoID;
        private Integer Estoque;
        private Integer Categorizado;
        private Integer Incluidos;
        private Integer EstadoID;

		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(ProdutoVendasDigitais());
		}

		protected DataSet ProdutoVendasDigitais()
		{
            // Roda a procedure sp_ProdutoMargemMinima_Pesquisar
			ProcedureParameters[] procParams = new ProcedureParameters[15];

            procParams[0] = new ProcedureParameters(
               "@RelacaoID",
               System.Data.SqlDbType.Int,
               RelacaoID != null ? (Object)RelacaoID.ToString() : DBNull.Value);

            procParams[1] = new ProcedureParameters(
                "@PaisID",
                System.Data.SqlDbType.Int, 
                PaisID != null ? (Object)PaisID.ToString() : DBNull.Value);
            
            procParams[2] = new ProcedureParameters(
                "@IdiomaID",
				System.Data.SqlDbType.Int, DBNull.Value);

			procParams[3] = new ProcedureParameters(
                "@Empresa",
				System.Data.SqlDbType.VarChar,
                empresas != "null" ? (Object)empresas : DBNull.Value);
                
			procParams[4] = new ProcedureParameters(
                "@GerenteProdutoID",
                System.Data.SqlDbType.Int,
                GerenteProdutoID != null ? (Object)GerenteProdutoID.ToString() : DBNull.Value);

            procParams[5] = new ProcedureParameters(
                "@FamiliaID",
                 System.Data.SqlDbType.Int,
                 FamiliaID != null ? (Object)FamiliaID.ToString() : DBNull.Value);

            procParams[6] = new ProcedureParameters(
                "@MarcaID",
                System.Data.SqlDbType.Int,
                MarcaID != null ? (Object)MarcaID.ToString() : DBNull.Value);

            procParams[7] = new ProcedureParameters(
                "@LinhaProdutoID",
                System.Data.SqlDbType.Int,
                LinhaProdutoID != null ? (Object)LinhaProdutoID.ToString() : DBNull.Value);

            procParams[8] = new ProcedureParameters(
                "@ProgramaMarketing",
				System.Data.SqlDbType.VarChar,
                prgmarketing != "null" ? (Object)prgmarketing : DBNull.Value);

            procParams[9] = new ProcedureParameters(
               "@ProdutoParticipante",
               System.Data.SqlDbType.Bit,
               Incluidos != null ? (Object)Incluidos : DBNull.Value);

            procParams[10] = new ProcedureParameters(
               "@Categorizado",
               System.Data.SqlDbType.Bit,
               Categorizado != null ? (Object)Categorizado : DBNull.Value);

            procParams[11] = new ProcedureParameters(
                "@EstadoMarketplaceID",
                System.Data.SqlDbType.Int,
                EstadoID != null ? (Object)EstadoID.ToString() : DBNull.Value);

            procParams[12] = new ProcedureParameters(
              "@ComEstoque",
              System.Data.SqlDbType.Bit,
              Estoque != null ? (Object)Estoque : DBNull.Value);

            procParams[13] = new ProcedureParameters(
                "@Busca",
                System.Data.SqlDbType.VarChar,
                busca != "null" ? (Object)busca : DBNull.Value);

            procParams[14] = new ProcedureParameters(
                "@TipoResultado",
                System.Data.SqlDbType.Int,
                ServicoID != null ? (Object)ServicoID.ToString() : DBNull.Value);

			return DataInterfaceObj.execQueryProcedure(
                "sp_Produto_VendasDigitais",
				procParams);
		}

        
       	public string sEmpresas
		{
			get { return empresas; }
            set { empresas = (value != null ? value : ""); }
		}

        public Integer nPaisID
        {
            get { return PaisID; }
            set { PaisID = (value.intValue() == 0 ? null : value); }
        }

        public Integer nGerenteProdutoID
        {
            get { return GerenteProdutoID; }
            set { GerenteProdutoID = (value.intValue() == 0 ? null : value); }
        }

        public Integer nFamiliaID
        {
            get { return FamiliaID; }
            set { FamiliaID = (value.intValue() == 0 ? null : value); }
        }

        public Integer nMarcaID
        {
            get { return MarcaID; }
            set { MarcaID = (value.intValue() == 0 ? null : value); }
        }

        public Integer nLinhaProdutoID
        {
            get { return LinhaProdutoID; }
            set { LinhaProdutoID = (value.intValue() == 0 ? null : value); }
        }

        public Integer nServicoID
        {
            get { return ServicoID; }
            set { ServicoID = (value.intValue() == 0 ? null : value); }
        }

        public Integer nRelacaoID
        {
            get { return RelacaoID; }
            set { RelacaoID = (value.intValue() == 0 ? null : value); }
        }

        public Integer nCategorizado
        {
            get { return Categorizado; }
            set { Categorizado = (value.intValue() == 0 ? null : value); }
        }

        public Integer nIncluidos
        {
            get { return Incluidos; }
            set { Incluidos = value; }
        }

        public Integer nEstadoID
        {
            get { return EstadoID; }
            set { EstadoID = (value.intValue() == 0 ? null : value); }
        }

        public Integer nEstoque
        {
            get { return Estoque; }
            set { Estoque = (value.intValue() == 0 ? null : value); }
        }

        public string sProgramasMarketing
		{
            get { return prgmarketing; }
            set { prgmarketing = (value != null ? value : ""); }
		}

        public string sBusca
		{
			get { return busca; }
            set { busca = (value != null ? value : null); }
		}
	}
}
