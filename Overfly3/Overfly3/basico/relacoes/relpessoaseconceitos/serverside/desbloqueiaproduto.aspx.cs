using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class desbloqueiaproduto : System.Web.UI.OverflyPage
	{
		
		private java.lang.Boolean falso = new java.lang.Boolean(false);

        private Integer relPesConKardexID;

        public Integer nRelPesConKardexID
        {
            get { return relPesConKardexID; }
            set { relPesConKardexID = value; }
        }

        // Roda a procedure sp_Pedido_CustoMedioItem;
		protected DataSet DesbloqueiaProduto()
		{
			ProcedureParameters[] procParams = new ProcedureParameters[2];

			procParams[0] = new ProcedureParameters(
                "@RelPesConKardexID",
				System.Data.SqlDbType.Int,
				relPesConKardexID.ToString());

			procParams[1] = new ProcedureParameters(
                "@DesbloquearProduto",
				System.Data.SqlDbType.Bit,
				1);

			DataInterfaceObj.execNonQueryProcedure(
                "sp_Pedido_CustoMedioItem",
				procParams);

			return DataInterfaceObj.getRemoteData(
                "select Space(0) as fldresp"
			);
		}
		
		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DesbloqueiaProduto());
		}
	}
}
