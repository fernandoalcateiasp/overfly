﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class ReportsGrid_Kardex : System.Web.UI.OverflyPage
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        private string sRegistroID = Convert.ToString(HttpContext.Current.Request.Params["glb_nRegistroID"]);
        private string sTipo = Convert.ToString(HttpContext.Current.Request.Params["nTipo"]);
        private string sDataInicio = Convert.ToString(HttpContext.Current.Request.Params["nDataInicio"]);
        private string sDataFim = Convert.ToString(HttpContext.Current.Request.Params["nDataFim"]);
        private string sLote = Convert.ToString(HttpContext.Current.Request.Params["bLote"]);
        private string sConsolidar = Convert.ToString(HttpContext.Current.Request.Params["bConsolidar"]);
        private string sEstoqueProprio = Convert.ToString(HttpContext.Current.Request.Params["bEstoqueProprio"]);
        private string sLinguaLogada = Convert.ToString(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string glb_controlID = Convert.ToString(HttpContext.Current.Request.Params["glb_controlID"]);

        protected override void PageLoad(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string controle = Request.Params["controle"].ToString();

                switch (controle)
                {
                    case "listaExcel":
                        Kardex();
                        break;
                }

            }
        }
    
        public void Kardex()
        {
            string FormatoData = "NULL";
            string bExcel = "0";
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString("dd_MM_yyy ");
            string Title = "Kardex - " + sEmpresaFantasia + " - " + _data;

            if (glb_controlID == "btnExcel")
            {
                bExcel = "1";

                if (sLinguaLogada == "246")
                {
                    FormatoData = "103";                   
                }
                else
                {
                    FormatoData = "101";
                    _data = DataHoje.ToString("MM_dd_yyy ");
                }


            }

            string strSQL = "EXEC dbo.sp_RelacoesPesCon_Custos " +
                            sRegistroID + "," + // RelacaoID
                            sTipo + "," + // TipoCustoID
                            sDataInicio + "," + // DataInicio
                            sDataFim + "," + // DataFim
                            sLote + "," + // Lote
                            sConsolidar + "," + // Consolidado
                            sEstoqueProprio + "," + //@EstoqueProprio BIT
                            bExcel + "," + //@Excel BIT
                            FormatoData; //Formato da data

            if (glb_controlID == "btnExcel")//SE EXCEL
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };
                string[,] arrIndexKey = new string[,] { { "" } };

                Relatorio.CriarRelatório(Title, arrSqlQuery, "BRA", "Excel");

                int Datateste = 0;
                bool nulo = false;

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    //HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    //Cria Tabela           
                    Relatorio.CriarObjTabela("0", "0", "Query1", true, true, true);
                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, 2);
                }
            }
            else
            {
                DataSet dtTeste = DataInterfaceObj.getRemoteData(strSQL);
                WriteResultXML(dtTeste);
            }                                    
        }
    }
}