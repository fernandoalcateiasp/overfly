﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_relpessoaseconceitos : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sFiltroEstados = Convert.ToString(HttpContext.Current.Request.Params["sFiltroEstados"]);
        private string sFiltroProprietarios = Convert.ToString(HttpContext.Current.Request.Params["sFiltroProprietarios"]);
        private string selOrdem = Convert.ToString(HttpContext.Current.Request.Params["selOrdem"]);
        private string selMoedas2 = Convert.ToString(HttpContext.Current.Request.Params["selMoedas2"]);
        private string sFiltro = Convert.ToString(HttpContext.Current.Request.Params["sFiltro"]);
        private string strParams = Convert.ToString(HttpContext.Current.Request.Params["strParams"]);
        private string DATE_SQL_PARAM = HttpContext.Current.Request.Params["DATE_SQL_PARAM"];


        string SQLImagem = string.Empty;
        string SQLDetail1 = string.Empty;
        string SQLDetail2 = string.Empty;
        string SQLDetail3 = string.Empty;
        string SQLDetail4 = string.Empty;

        int Datateste = 0;
        bool nulo = false;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "40125":
                        relatorioPosicaoProdutos();
                        break;
                }

            }
        }

        public void relatorioPosicaoProdutos()
        {
            string Title = "Posição de Produtos";

            string strSQL = "EXEC sp_Produto_Posicao " + nEmpresaID + ", " + sFiltroEstados + ", " + sFiltroProprietarios + ", " + 
                            selOrdem + ", " + selMoedas2 + ", " + sFiltro;

            var Header_Body = (formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (formato == 1)
            {
                Relatorio.CriarRelatório("Extrato de Cliente", arrSqlQuery, "BRA", "Landscape", "2");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    //Cabeçalho
                    Relatorio.CriarObjLabel("Fixo", "", sEmpresaFantasia, "0.2", "0.5", "12", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Posição de Produtos", "12", "0.5", "12", "Blue", "B", "Header", "6", "");
                    Relatorio.CriarObjLabelData("Now", "", "", "23.795", "0.5", "12", "black", "B", "Header", "4", "Horas");

                    Relatorio.CriarObjLabel("Fixo", "", strParams, "0.2", "1", "6", "Black", "B", "Header", "28", "");
                    Relatorio.CriarObjLinha("0.2", "1.3", "28", "", "Header");

                    Relatorio.CriarObjLabel("Fixo", "", "Identificação", "1.5", "1.37", "6", "Black", "B", "Header", "2", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Datas", "7.2", "1.37", "6", "Black", "B", "Header", "2", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Estoque", "12.3", "1.37", "6", "Black", "B", "Header", "2", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Plano de Compras", "16.3", "1.37", "6", "Black", "B", "Header", "2", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Plano de Vendas", "21.2", "1.37", "6", "Black", "B", "Header", "2", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Vendas", "26.8", "1.37", "6", "Black", "B", "Header", "2", "");

                    Relatorio.CriarObjLinha("0.2", "1.6", "4.5", "", "Header");
                    Relatorio.CriarObjLinha("4.9", "1.6", "4.6", "", "Header");
                    Relatorio.CriarObjLinha("10.1", "1.6", "4.4", "", "Header");
                    Relatorio.CriarObjLinha("14.8", "1.6", "4.6", "", "Header");
                    Relatorio.CriarObjLinha("19.6", "1.6", "5.8", "", "Header");
                    Relatorio.CriarObjLinha("25.8", "1.6", "3", "", "Header");

                    Relatorio.CriarObjLabel("Fixo", "", "ID", "0.2", "1.65", "6", "Black", "B", "Header", "0.9", "");
                    Relatorio.CriarObjLabel("Fixo", "", "E", "1.1", "1.65", "6", "Black", "B", "Header", "0.3", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Produto", "1.4", "1.65", "6", "Black", "B", "Header", "2.9", "");
                    Relatorio.CriarObjLabel("Fixo", "", "GP", "4.3", "1.65", "6", "Black", "B", "Header", "0.7", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Início", "5", "1.65", "6", "Black", "B", "Header", "1.15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Controle", "6.15", "1.65", "6", "Black", "B", "Header", "1.15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Fim", "7.3", "1.65", "6", "Black", "B", "Header", "1.15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Pagto", "8.45", "1.65", "6", "Black", "B", "Header", "1.15", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Disp", "9.5", "1.65", "6", "Black", "B", "Header", "1", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Total", "10.5", "1.65", "6", "Black", "B", "Header", "1.4", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Exc", "11.9", "1.65", "6", "Black", "B", "Header", "1.1", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Total", "13", "1.65", "6", "Black", "B", "Header", "1.4", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "RC", "14.4", "1.65", "6", "Black", "B", "Header", "0.7", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "PR", "15.1", "1.65", "6", "Black", "B", "Header", "0.7", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "PE", "15.8", "1.65", "6", "Black", "B", "Header", "0.7", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "PP", "16.5", "1.65", "6", "Black", "B", "Header", "0.7", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Compr", "17.2", "1.65", "6", "Black", "B", "Header", "0.9", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Custo", "18.1", "1.65", "6", "Black", "B", "Header", "1.1", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "MC", "19.2", "1.65", "6", "Black", "B", "Header", "0.8", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "MP", "20", "1.65", "6", "Black", "B", "Header", "0.8", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "MR", "20.8", "1.65", "6", "Black", "B", "Header", "0.8", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "PV", "21.6", "1.65", "6", "Black", "B", "Header", "0.8", "Right"); 
                    Relatorio.CriarObjLabel("Fixo", "", "Faturamento", "22.4", "1.65", "6", "Black", "B", "Header", "1.5", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "Contribuição", "23.9", "1.65", "6", "Black", "B", "Header", "1.5", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "M3", "25.4", "1.65", "6", "Black", "B", "Header", "0.9", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "M2", "26.3", "1.65", "6", "Black", "B", "Header", "0.9", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "M1", "27.2", "1.65", "6", "Black", "B", "Header", "0.9", "Right");
                    Relatorio.CriarObjLabel("Fixo", "", "M0", "28.1", "1.65", "6", "Black", "B", "Header", "0.8", "Right");

                    //Tabela com os dados
                    Relatorio.CriarObjTabela("0.2", "0", arrIndexKey, false, true);
                 
                        Relatorio.CriarObjColunaNaTabela(" ", "PID", false, "0.9", "Default", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "PID", "DetailGroup", "Null", "Null", 0, true, "5.5");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Totais", "TotalGroup", "", "DetailGroup", 8, true, "5.5");

                        Relatorio.CriarObjColunaNaTabela(" ", "E", false, "0.3", "Default", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "E", "DetailGroup", "Null", "Null", 0, true, "5.5");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5");

                        Relatorio.CriarObjColunaNaTabela(" ", "Produto", false, "2.9", "Default", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, true, "5.5");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5");

                        Relatorio.CriarObjColunaNaTabela(" ", "GP", false, "0.7", "Default", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "GP", "DetailGroup", "Null", "Null", 0, true, "5.5");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5");

                        Relatorio.CriarObjColunaNaTabela(" ", "Inicio", false, "1.15", "Default", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Inicio", "DetailGroup", "Null", "Null", 0, true, "5.5");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5");

                        Relatorio.CriarObjColunaNaTabela(" ", "Controle", false, "1.15", "Default", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Controle", "DetailGroup", "Null", "Null", 0, true, "5.5");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5");

                        Relatorio.CriarObjColunaNaTabela(" ", "Fim", false, "1.15", "Default", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fim", "DetailGroup", "Null", "Null", 0, true, "5.5");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5");

                        Relatorio.CriarObjColunaNaTabela(" ", "Pagto", false, "1.15", "Default", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pagto", "DetailGroup", "Null", "Null", 0, true, "5.5");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5");

                        Relatorio.CriarObjColunaNaTabela(" ", "Disp", false, "1", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Disp", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Disp", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "TotalDisp", false, "1.4", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalDisp", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalDisp", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "Exc", false, "1.1", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Exc", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Exc", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "TotalExc", false, "1.4", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalExc", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalExc", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "RC", false, "0.7", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "RC", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "RC", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "PR", false, "0.7", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "PR", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 3, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "PE", false, "0.7", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "PE", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "PP", false, "0.7", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "PP", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "Compr", false, "0.9", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Compr", "DetailGroup", "Null", "Null", 0, true, "6", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Compr", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "TotalComp", false, "1.1", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalComp", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalComp", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "MC", false, "0.8", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "MC", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "MC", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0.00</Format>",false,"Default", true);

                        Relatorio.CriarObjColunaNaTabela(" ", "MP", false, "0.8", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "MP", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "MP", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0.00</Format>", false, "Default", true);

                        Relatorio.CriarObjColunaNaTabela(" ", "MR", false, "0.8", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "MR", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "MR", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0.00</Format>", false, "Default", true);

                        Relatorio.CriarObjColunaNaTabela(" ", "PV", false, "0.8", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "PV", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "PV", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "TotalFat", false, "1.5", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalFat", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalFat", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "Contribuicao", false, "1.5", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "Contribuicao", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Contribuicao", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "M3", false, "0.9", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "M3", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "M3", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "M2", false, "0.9", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "M2", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "M2", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "M1", false, "0.9", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "M1", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "M1", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela(" ", "M0", false, "0.8", "Right", "1","0");
                        Relatorio.CriarObjCelulaInColuna("Query", "M0", "DetailGroup", "Null", "Null", 0, true, "5.5", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "M0", "TotalGroup", "", "DetailGroup", 0, true, "5.5", "<Format>#,0</Format>");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, formato);
                }
            }

            else if (formato == 2) // Se Excel
            {

                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                { 
                    
                    //Cria Tabela           
                    Relatorio.CriarObjTabela("0.00", "0", arrIndexKey, true,true);

                        Relatorio.CriarObjColunaNaTabela("ID", "PID", true, "PID", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "PID", "DetailGroup", "Null", "Null");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Totais", "TotalGroup", "", "DetailGroup", 8, true);

                        Relatorio.CriarObjColunaNaTabela("E", "E", true, "E", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "E", "DetailGroup", "Null", "Null");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("Produto", "Produto", true, "Produto", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("GP", "GP", true, "GP", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "GP", "DetailGroup", "Null", "Null");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("Início", "Inicio", true, "Inicio", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Inicio", "DetailGroup", "Null", "Null");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("Controle", "Controle", true, "Controle", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Controle", "DetailGroup", "Null", "Null");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("Fim", "Fim", true, "Fim", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Fim", "DetailGroup", "Null", "Null");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("Pagto", "Pagto", true, "Pagto", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pagto", "DetailGroup", "Null", "Null");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("Custo", "Custo", true, "Custo", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Custo", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Custo", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela("Disp", "Disp", true, "Disp", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Disp", "DetailGroup", "Null", "Null",0,false,"0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Disp", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("Total Disp", "TotalDisp", true, "Total Disp", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalDisp", "DetailGroup", "Null", "Null",0,false,"0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalDisp", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela("Exc", "Exc", true, "Exc", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Exc", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Exc", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("Total Exc", "TotalExc", true, "Total Exc", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalExc", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalExc", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela("RC", "RC", true, "RC", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "RC", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "RC", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("PR", "PR", true, "PR", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "PR", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 3, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("PE", "PE", true, "PE", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "PE", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("PP", "PP", true, "PP", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "PP", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("Compr", "Compr", true, "Compr", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Compr", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Compr", "TotalGroup", "", "DetailGroup", 0, true);

                        Relatorio.CriarObjColunaNaTabela("Total Comp", "TotalComp", true, "Total Comp", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalComp", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalComp", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela("MC", "MC", true, "MC", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "MC", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "MC", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>", false, "Default", true);

                        Relatorio.CriarObjColunaNaTabela("MP", "MP", true, "MP", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "MP", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "MP", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>", false, "Default", true);

                        Relatorio.CriarObjColunaNaTabela("MR", "MR", true, "MR", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "MR", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "MR", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>", false, "Default", true);

                        Relatorio.CriarObjColunaNaTabela("PV", "PV", true, "PV", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "PV", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "PV", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("Preco", "Preco", true, "Preco", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Preco", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Fixo", " ", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela("Total Fat", "TotalFat", true, "Total Fat", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalFat", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "TotalFat", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela("Contribuição", "Contribuicao", true, "Contribuicao", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "Contribuicao", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0.00</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "Contribuicao", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0.00</Format>");

                        Relatorio.CriarObjColunaNaTabela("M3", "M3", true, "M3", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "M3", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "M3", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("M2", "M2", true, "M2", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "M2", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "M2", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("M1", "M1", true, "M1", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "M1", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "M1", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                        Relatorio.CriarObjColunaNaTabela("M0", "M0", true, "M0", "Default");
                        Relatorio.CriarObjCelulaInColuna("Query", "M0", "DetailGroup", "Null", "Null", 0, false, "0", "<Format>#,0</Format>");
                        Relatorio.CriarObjCelulaInColuna("Query", "M0", "TotalGroup", "", "DetailGroup", 0, true, "0", "<Format>#,0</Format>");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, formato);
                }              
            }            
        }
    }
}