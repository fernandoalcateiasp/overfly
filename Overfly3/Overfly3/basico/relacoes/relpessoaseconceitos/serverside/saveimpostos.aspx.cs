using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class saveimpostos : System.Web.UI.OverflyPage
    {
        private static Integer zero = new Integer(0);

        private Integer dataLen;
        private Integer dataElemLen;
        private char[] marks = new char[] { (char)47 };
        private Object[][] data;

        public Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value != null ? value : zero; }
        }

        public Integer nDataElemLen
        {
            get { return dataElemLen; }
            set { dataElemLen = value != null ? value : zero; }
        }

        public Object[] aData
        {
            set
            {

                if (value != null)
                {
                    int i = 0;
                    data = new Object[dataLen.intValue()][];
                    for (int j = 0; j < (dataLen.intValue()); j++)
                    {
                        data[j] = new Object[dataElemLen.intValue()];
                        int l = 0;
                        while (l < dataElemLen.intValue())
                        {
                            //values = value[j].ToString().Split(marks);
                            data[j][l] = value[i].ToString().Split(marks)[1]; //values[j][i];
                            i++;
                            l++;
                        }
                    }
                }
                else
                {
                    data = new Object[0][];
                }
            }
        }

        protected string GetSql()
        {
            string sql = "";

            for (int i = 0; i < dataLen.intValue(); i++)
            {
                sql +=
                    " UPDATE RelacoesPesCon_Impostos SET " +
                        " Aliquota=" + data[i][1].ToString() + ", " +
                        " BaseCalculo=" + data[i][2].ToString() + " " +
                    " WHERE RelPesConImpostoID = " + data[i][0].ToString();
            }

            return sql;
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            DataInterfaceObj.ExecuteSQLCommand(GetSql());

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select 0 as fldErrorNumber, '' as fldErrorText"
                )
            );
        }
       
    }
}
