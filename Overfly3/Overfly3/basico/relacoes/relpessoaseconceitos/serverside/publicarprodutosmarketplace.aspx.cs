using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;


namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class publicarprodutosmarketplace : System.Web.UI.OverflyPage
    {
        private Integer[] RelPesConVendaDigitalID = Constants.INT_EMPTY_SET;

        public Integer[] nRelPesConVendaDigitalID
        {
            get { return RelPesConVendaDigitalID; }
            set { RelPesConVendaDigitalID = value; }
        }

        private string[] PrecoFabricante = Constants.STR_EMPTY_SET;

        protected string[] nPrecoFabricante
        {
            get { return PrecoFabricante; }
            set { PrecoFabricante = value; }
        }

        private string[] PrecoPraticado = Constants.STR_EMPTY_SET;

        protected string[] nPrecoPraticado
        {
            get { return PrecoPraticado; }
            set { PrecoPraticado = value; }
        }

        private Integer[] deletado = Constants.INT_EMPTY_SET;

        public Integer[] nDeletado
        {
            get { return deletado; }
            set { deletado = value; }
        }
        
        private Integer[] UsuarioID = Constants.INT_EMPTY_SET;

        public Integer[] nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }

        private string[] dtAgendamento = Constants.STR_EMPTY_SET;

        public string[] sDtAgendamento
        {
            get { return dtAgendamento; }
            set { dtAgendamento = value; }
        }
        
        protected int Executar()
        {
            int fldresp;
            string sql = "";
            int nDataLen = RelPesConVendaDigitalID.Length;

            for (int i = 0; i < nDataLen; i++)
            {
                sql = sql + "UPDATE RelacoesPesCon_VendaDigital " +
                            "SET PrecoFabricante= " + PrecoFabricante[i].ToString() + ", PrecoPraticado= " + PrecoPraticado[i].ToString() + 
                            ", dtAgendamento= " + (PrecoPraticado[i].ToString() != "0" ? (dtAgendamento[i].ToString() != "NULL" ? dtAgendamento[i].ToString() : "GETDATE()") : "NULL") +
                                ", Deletado= " + deletado[i].ToString() + ", UsuarioID= " + UsuarioID[i].ToString() + " " +
                            "WHERE RelPesConVendaDigitalID = " + RelPesConVendaDigitalID[i].ToString() + " ";
            }

            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            return fldresp;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = Executar();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + fldresp + " as fldresp"
                )
            );
        }
    }
}
