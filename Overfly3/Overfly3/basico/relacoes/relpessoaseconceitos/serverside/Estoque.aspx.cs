﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Overfly3.PrintJet
{
    public partial class Estoque : System.Web.UI.Page
    {

        ClsReport Relatorio = new ClsReport();

        protected void Page_Load(object sender, EventArgs e)
        {
            string Padrao = HttpContext.Current.Request.QueryString["Padrao"];

            if (Padrao == "Generico")
            {
                relatorioEstoqueGenerico();
            }
            else if (Padrao == "Intel")
            {
                relatorioEstoqueIntel();
            }
            else if (Padrao == "Microsoft")
            {
                relatorioEstoqueMicrosoft();
            }
            else if (Padrao == "3com")
            {
                relatorioEstoque3com();
            }
            else if (Padrao == "NVidia")
            {
                relatorioEstoqueNVidia();
            }
            else if (Padrao == "Seagate")
            {
                relatorioEstoqueSeagate();
            }
            else if (Padrao == "Kingston")
            {
                relatorioEstoqueKingston();
            }
            else if (Padrao == "HP")
            {
                relatorioEstoqueHP();
            }
            else if (Padrao == "SPED")
            {
                relatorioSPED();
            }
        }

        private void relatorioEstoqueGenerico()
        {
            int i;
            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            string dataCustoEstoque="";
            string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            int TipoCustoEstoque = 0;
            int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            string selEmpresa = HttpContext.Current.Request.QueryString["selEmpresa"];
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            string selMarca = HttpContext.Current.Request.QueryString["selMarca"];


            if (dateTimeInSQLFormat == "NULL")

            {
                dateTimeInSQLFormat = "NULL";
            }


            var sFiltroFabricante = (selFabricanteValue == 0 ? "" : " AND Produtos.FabricanteID=" + selFabricanteValue);

            var sFiltroEmpresa = "";
            var nItensSelected = 0;
            var sFiltroProprietario = "";
            

            string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            string[] _selMarca = new string[] { };
            string[] _elem_Value = new string[] { };

            if (selProprietarios2_selectedIndex > 0)
                sFiltroProprietario = " AND ProdutosEmpresa.ProprietarioID = " + selProprietarios2_value + " ";


            char[] DelimitChar1 = { ':' };

            if (selEmpresa != "")
                _selEmpresa = selEmpresa.Split(DelimitChar1);


            for (i = 0; i < _selEmpresa.Length; i++)
            {

                nItensSelected++;
                sFiltroEmpresa += (nItensSelected == 1 ? " AND (" : " OR ") + "ProdutosEmpresa.SujeitoID=" + _selEmpresa[i].ToString();

            }

            sFiltroEmpresa += (nItensSelected > 0 ? ")" : "");

            var sFiltroFamilia = "";
            nItensSelected = 0;

            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < _selFamilias.Length; i++)
            {

                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + _selFamilias[i].ToString();

            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");


            var sFiltroMarca = "";
            nItensSelected = 0;

            if (selMarca != "")
                _selMarca = selMarca.Split(DelimitChar1);

            for (i = 0; i < _selMarca.Length; i++)
            {

                nItensSelected++;
                sFiltroMarca += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.MarcaID=" + _selMarca[i].ToString();

            }

            sFiltroMarca += (nItensSelected > 0 ? ")" : "");


            var sFiltroEstados = " ";
            var bFirst = true;
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            var sEstadosCheckeds = HttpContext.Current.Request.QueryString["sEstadosCheckeds"];
            var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            var chkCustoContabil = HttpContext.Current.Request.QueryString["chkCustoContabil"];
            var chkValoracao = HttpContext.Current.Request.QueryString["chkValoracao"];
            var chkTemEstoque = HttpContext.Current.Request.QueryString["chkTemEstoque"];
            var chkExcel = HttpContext.Current.Request.QueryString["chkExcel"];
            var sParams = HttpContext.Current.Request.QueryString["sParams"];
            var selMoedas = HttpContext.Current.Request.QueryString["selMoedas"];
            var DATE_SQL_PARAM = HttpContext.Current.Request.QueryString["DATE_SQL_PARAM"];
            var glb_sEmpresaFantasia = HttpContext.Current.Request.QueryString["glb_sEmpresaFantasia"];

            var aCmbsEstoques_0 = HttpContext.Current.Request.QueryString["aCmbsEstoques_0"];
            var aCmbsEstoques_1 = HttpContext.Current.Request.QueryString["aCmbsEstoques_1"];
            var aCmbsEstoques_2 = HttpContext.Current.Request.QueryString["aCmbsEstoques_2"];
            var aCmbsEstoques_3 = HttpContext.Current.Request.QueryString["aCmbsEstoques_3"];
            var aCmbsEstoques_4 = HttpContext.Current.Request.QueryString["aCmbsEstoques_4"];
            var aCmbsEstoques_5 = HttpContext.Current.Request.QueryString["aCmbsEstoques_5"];
            var aCmbsEstoques_6 = HttpContext.Current.Request.QueryString["aCmbsEstoques_6"];
            var aCmbsEstoques_7 = HttpContext.Current.Request.QueryString["aCmbsEstoques_7"];
            var aCmbsEstoques_8 = HttpContext.Current.Request.QueryString["aCmbsEstoques_8"];
            var aCmbsEstoques_9 = HttpContext.Current.Request.QueryString["aCmbsEstoques_9"];
            var aCmbsEstoques_10 = HttpContext.Current.Request.QueryString["aCmbsEstoques_10"];
            var aCmbsEstoques_11 = HttpContext.Current.Request.QueryString["aCmbsEstoques_11"];
            var aCmbsEstoques_12 = HttpContext.Current.Request.QueryString["aCmbsEstoques_12"];
            var aCmbsEstoques_13 = HttpContext.Current.Request.QueryString["aCmbsEstoques_13"];
            var aCmbsEstoques_14 = HttpContext.Current.Request.QueryString["aCmbsEstoques_14"];
            var aCmbsEstoques_15 = HttpContext.Current.Request.QueryString["aCmbsEstoques_15"];

            string[,] ArrCmbChecked = new string[,]
            {
                {aCmbsEstoques_0, "EPO"},
                {aCmbsEstoques_1, "EPE"},
                {aCmbsEstoques_2, "EPD"},
                {aCmbsEstoques_3, "EPC"},
                {aCmbsEstoques_4, "EPT"},
                {aCmbsEstoques_5, "ETO"},
                {aCmbsEstoques_6, "ETE"},
                {aCmbsEstoques_7, "ETD"},
                {aCmbsEstoques_8, "ETC"},
                {aCmbsEstoques_9, "EB_"},
                {aCmbsEstoques_10,"F"},
                {aCmbsEstoques_11, "Dis"},
                {aCmbsEstoques_12, "RC"},
                {aCmbsEstoques_13, "RCC"},
                {aCmbsEstoques_14, "RV"},
                {aCmbsEstoques_15, "RVC"},
            };



            string sTmpTableSelect = string.Empty;
            var bAging = false;

            if (elem_Value != "")
                _elem_Value = elem_Value.Split(DelimitChar1);

            // loop nos checkbox de estados para montar a string de filtro
            for (i = 0; i < (_elem_Value.Length); i++)
            {
                if (_elem_Value[i] == "-1")
                    bAging = true;
                else
                {
                    if (bFirst)
                    {
                        bFirst = false;

                        sFiltroEstados = " AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";

                    }
                    else
                    {
                        sFiltroEstados += " OR (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " )";
                    }
                }
            }


            if (!bFirst)
                sFiltroEstados += ')';
            else
                sFiltroEstados += " AND (ProdutosEmpresa.EstadoID=0)";


            if (bAging)
                sFiltroEstados += " AND (dbo.fn_Produto_Aging(ProdutosEmpresa.RelacaoID, NULL) = 1) ";
          

            //var aCmbsEstoques = 
            //    [chk1_ESTOQUE_EPO,
            //     chk1_ESTOQUE_EPE,
            //     chk1_ESTOQUE_EPD,
            //     chk1_ESTOQUE_EPC,
            //     chk1_ESTOQUE_EPT,
            //     chk1_ESTOQUE_ETO,
            //     chk1_ESTOQUE_ETE,
            //     chk1_ESTOQUE_ETD,
            //     chk1_ESTOQUE_ETC,
            //     chk1_ESTOQUE_EB,
            //     chk1_ESTOQUE_F,
            //     chk1_ESTOQUE_Disp,
            //     chk1_ESTOQUE_RC,
            //     chk1_ESTOQUE_RCC,
            //     chk1_ESTOQUE_RV,
            //     chk1_ESTOQUE_RVC];

            string sSQLMasterSelect, sSQLMasterFrom, sSQLMasterWhere1, sSQLMasterWhere2, sSQLMasterGroupBy, sSQLMasterOrderBy;

            sSQLMasterSelect = "SELECT DISTINCT " +
                    "Conceitos2.Conceito AS ConceitoAbstrato2, " +
                    "Conceitos2.ConceitoID AS TesteID, NULL AS Gap1, NULL AS Gap2 ";


            var bLocalizacao = true;

            string[] _chkCmbEstoques = chkCmbEstoques.Split(DelimitChar1);

            for (i = 0; i < _chkCmbEstoques.Length; i++)
            {
                if (_chkCmbEstoques[i].ToString() == "checked")
                    bLocalizacao = false;
            }

            sSQLMasterFrom = "FROM " +
                    "Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), " +
                    "RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), " +
                    "RelacoesPesCon ProdutosEmpresa  WITH(NOLOCK) ";

            sSQLMasterWhere1 =
                "WHERE (" +
                    "(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND " +
                    "(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND " +
                    "RelConceitos1.EstadoID=2 AND RelConceitos1.SujeitoID=Conceitos2.ConceitoID) AND " +
                    "(Conceitos2.TipoConceitoID=301 AND Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND " +
                    "(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND " +
                    "RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND " +
                    "(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND ";

            sSQLMasterWhere2 =
                    "Conceitos3.TipoConceitoID=302 AND Conceitos3.EstadoID=2) AND " +
                    "(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND " +
                    "(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND " +
                    "ProdutosEmpresa.TipoRelacaoID=61) AND " +
                    "(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID = Produtos.MarcaID) ";

            if (chkProdutoSeparavel == "checked")
                sSQLMasterWhere2 += " AND Produtos.ProdutoSeparavel = 1 ";

            if (chkReportaFabricante == "checked")
                sSQLMasterWhere2 += " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            sSQLMasterWhere2 += sFiltroFabricante + sFiltroEmpresa + sFiltroProprietario + sFiltroFamilia + sFiltroMarca + sFiltroEstados + " " + sFiltro + ") ";

            sSQLMasterGroupBy = "GROUP BY Conceitos2.Conceito, Conceitos2.ConceitoID ";

            sSQLMasterOrderBy = "ORDER BY Conceitos2.Conceito, Conceitos2.ConceitoID";

            string MasterQuerySQL = sSQLMasterSelect + sSQLMasterFrom + sSQLMasterWhere1 + sSQLMasterWhere2 + sSQLMasterGroupBy + sSQLMasterOrderBy;


            string[] aSQLQuantEstoquesDetail = new string[]
            {   
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,341," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPO ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,342," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPE ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,343," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPD ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,344," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPC ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,345," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPT ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,346," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS ETO ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,347," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS ETE ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,348," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS ETD ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,349," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS ETC ",
                "dbo.fn_Produto_EstoqueBrinde(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,4) AS EB ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,341," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS F ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,356," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS Dis ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,351," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS RC ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,352," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS RCC ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,353," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS RV ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,354," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS RVC "
            };

            string[] aDynamicEstoque = new string[] 
            {   
                "EPO Numeric(10),",
                "EPE Numeric(10),",
                "EPD Numeric(10),",
                "EPC Numeric(10),",
                "EPT Numeric(10),",
                "ETO Numeric(10),",
                "ETE Numeric(10),",
                "ETD Numeric(10),",
                "ETC Numeric(10),",
                "EB_ Numeric(10),",
                "F   Numeric(10),",
                "Dis Numeric(10),",
                "RC Numeric(10),",
                "RCC Numeric(10),",
                "RV Numeric(10),",
                "RVC Numeric(10),"
            };

            string sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailOrderBy;

            sSQLDetailSelect = "SELECT DISTINCT " +
                    "Produtos.ConceitoID AS ID, Estados.RecursoAbreviado AS Est, " +
                    "Produtos.Conceito AS Produto, CONVERT(VARCHAR(126),dbo.fn_Produto_Descricao2(Produtos.ConceitoID, NULL, 11)) AS Descricao, Conceitos2.ConceitoID AS TesteID ";

            for (i = 0; i < _chkCmbEstoques.Length; i++)
                if (_chkCmbEstoques[i].ToString() == "checked")
                    sSQLDetailSelect += ", " + aSQLQuantEstoquesDetail[i];


            //Alterado para pegar estoque na data.
            if ((dateTimeInSQLFormat != "NULL") && (chkCustoContabil == "checked"))
            {
                TipoCustoEstoque = 2;
                dataCustoEstoque = dateTimeInSQLFormat;
            }
            else if ((dateTimeInSQLFormat == "NULL") && (chkCustoContabil == "checked"))
            {
                TipoCustoEstoque = 1;
                dataCustoEstoque = "GetDate()";
            }
            else if ((dateTimeInSQLFormat != "NULL") && (chkCustoContabil != "checked"))
            {
                TipoCustoEstoque = 4;
                dataCustoEstoque = dateTimeInSQLFormat;
            }
            else if ((dateTimeInSQLFormat == "NULL") && (chkCustoContabil != "checked"))
            {
                TipoCustoEstoque = 3;
                dataCustoEstoque = "GetDate()";
            }

            if (bLocalizacao)
                sSQLDetailSelect += ", dbo.fn_Produto_Localizacoes(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, NULL, 10, 1) AS Localizacao ";
            else if (chkValoracao == "checked")
                sSQLDetailSelect += ", 0 AS Quant, " +
                    "dbo.fn_Produto_Custo(ProdutosEmpresa.RelacaoID, " + dataCustoEstoque + ", " + selMoedas + ", NULL, " + TipoCustoEstoque + ") AS Custo, " +
                    "0 AS TotalCusto, " +
                    "dbo.fn_Preco_Preco(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,NULL,NULL,NULL,NULL," + selMoedas + ",GetDate(),NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL,NULL, NULL) AS Preco, " +
                    "0 AS TotalPreco ";
            else
                sSQLDetailSelect += ", 0 AS Quant ";

            sSQLDetailSelect += ", ProdutosEmpresa.CodigoAnterior, Familias.Conceito AS Familia, " +
                "Fabricantes.Fantasia AS Fabricante, Marcas.Conceito AS Marca, Produtos.Modelo, Produtos.PartNumber, Produtos.Serie ";

            if (dateTimeInSQLFormat != "NULL")
                sSQLDetailSelect += ", " + dateTimeInSQLFormat + " AS Data ";
            else
                sSQLDetailSelect += ", GETDATE() AS Data ";

            sSQLDetailSelect += ", Empresas.PessoaID AS EmpresaID, Empresas.Fantasia AS Empresa, " +
                "Proprietarios.Fantasia AS Proprietario, DATEDIFF(dd,GETDATE(),ProdutosEmpresa.dtMediaPagamento) as DiasPagamento, " +
                "1 AS _Registro, ProdutosEmpresa.Observacao, " +
                "CONVERT(VARCHAR(10), ProdutosEmpresa.dtControle, " + DATE_SQL_PARAM + ") AS dtControle, " +
                "dbo.fn_Produto_PesosMedidas(ProdutosEmpresa.ObjetoID, 1, 5) AS Cubagem, " +
                "dbo.fn_Produto_PesosMedidas(ProdutosEmpresa.ObjetoID, 1, 1) AS Peso, " +
                "dbo.fn_Produto_TransfereFilial(Produtos.ConceitoID,Empresas.PessoaID)"; //Inclusão de nova coluna no relatório - FSM 05/03/2012

            sSQLDetailFrom = "FROM " +
                    "Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), " +
                    "RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), " +
                    "RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Recursos Estados WITH(NOLOCK), Recursos Sistema WITH(NOLOCK), Pessoas Fabricantes WITH(NOLOCK), Pessoas Empresas WITH(NOLOCK), Pessoas Proprietarios WITH(NOLOCK) ";

            sSQLDetailWhere1 = "WHERE (" +
                    "(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND " +
                    "(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND " +
                    "RelConceitos1.EstadoID=2) AND " +
                    "(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND " +
                    "Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND " +
                    "(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND " +
                    "RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND " +
                    "(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ";

            sSQLDetailWhere2 =
                    "Conceitos3.EstadoID=2) AND " +
                    "(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND " +
                    "(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 AND " +
                    "ProdutosEmpresa.EstadoID=Estados.RecursoID AND Sistema.RecursoID=999) AND " +
                    "(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) AND " +
                    "(Fabricantes.PessoaID=Produtos.FabricanteID) AND " +
                    "(Empresas.PessoaID=ProdutosEmpresa.SujeitoID) AND " +
                    "(Proprietarios.PessoaID=ProdutosEmpresa.ProprietarioID)";

            if (chkProdutoSeparavel == "checked")
                sSQLDetailWhere2 += " AND Produtos.ProdutoSeparavel = 1 ";

            if (chkReportaFabricante == "checked")
                sSQLDetailWhere2 += " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            sSQLDetailWhere2 += sFiltroFabricante + sFiltroEmpresa + sFiltroProprietario + sFiltroFamilia + sFiltroMarca + sFiltroEstados + " " + sFiltro + ") ";

            sSQLDetailOrderBy = "ORDER BY Produtos.ConceitoID, Produtos.Conceito, Conceitos2.ConceitoID ";

            sTmpTableSelect = "CREATE TABLE #TempTable " +
                "( " +
                "ID NUMERIC(10)," +
                "Est VARCHAR(1)," +
                "Produto VARCHAR(40)," +
                "Descricao VARCHAR(126), " +
                "TesteID NUMERIC(10),";

            for (i = 0; i < _chkCmbEstoques.Length; i++)
                if (_chkCmbEstoques[i].ToString() == "checked")
                    sTmpTableSelect += aDynamicEstoque[i];

            if (bLocalizacao)
            {
                sTmpTableSelect += "Localizacao VARCHAR(200),";
            }
            else if (chkValoracao == "checked")
            {
                sTmpTableSelect += "Quant NUMERIC(10), " +
                    "Custo NUMERIC(11,2), " +
                    "TotalCusto NUMERIC(12,2), " +
                    "Preco NUMERIC(11,2), " +
                    "TotalPreco NUMERIC(12,2),";
            }
            else
                sTmpTableSelect += "Quant NUMERIC(10),";

            sTmpTableSelect += "CodigoAnterior VARCHAR(10)," +
                               "Familia VARCHAR(25)," +
                               "Fabricante VARCHAR(20)," +
                               "Marca VARCHAR(25)," +
                               "Modelo VARCHAR(18)," +
                               "PartNumber VARCHAR(20)," +
                               "Serie VARCHAR(40)," +
                               "Data DATETIME," +
                               "EmpresaID NUMERIC(10)," +
                               "Empresa VARCHAR(20)," +
                               "Proprietario VARCHAR(20)," +
                               "DiasPagamento INT," +
                               "_Registro INTEGER, " +
                               "Observacao VARCHAR(25), " +
                               "dtControle VARCHAR(10), " +
                               "Cubagem NUMERIC(13,8), " +
                               "Peso NUMERIC(11, 2), " +
                               "Transferivel BIT)"; //Inclusão de nova coluna no relatório - FSM 05/03/2012

            sTmpTableSelect += "INSERT INTO #TempTable " + sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 +
                sSQLDetailWhere2 + sSQLDetailOrderBy + " ";

            var nQtdEstoquesCheckeds = 0;

            if (!bLocalizacao)
            {
                sTmpTableSelect += "UPDATE #TempTable SET Quant = ( ";


                var isFirst = true;
                for (i = 0; i < _chkCmbEstoques.Length; i++)
                    if (_chkCmbEstoques[i].ToString() == "checked")
                    {
                        sTmpTableSelect += (isFirst ? "" : "+") + (aDynamicEstoque[i]).Substring(0, 3);
                        isFirst = false;
                        nQtdEstoquesCheckeds++;
                    }

                if (!isFirst)
                    sTmpTableSelect += ") ";
            }

            if (!bLocalizacao)
                sTmpTableSelect += " UPDATE #TempTable SET Cubagem = Cubagem * Quant, Peso = Peso * Quant ";

            if (chkValoracao == "checked")
                sTmpTableSelect += " UPDATE #TempTable SET TotalCusto = Quant * Custo, TotalPreco = Quant * Preco ";

            sTmpTableSelect += "SELECT * FROM #TempTable ";
            if (chkTemEstoque == "checked")
                sTmpTableSelect += "WHERE (Quant <> 0) ";

            sTmpTableSelect += "DROP TABLE #TempTable";

            var strSQLParsed = "EXEC sp_RelatorioEstoque " + "'" + dupCharInString(sTmpTableSelect, "'") + "'";

            strSQLParsed = (strSQLParsed == null ? "" : strSQLParsed);

            var Landscape = ((nQtdEstoquesCheckeds > 11) || ((chkValoracao == "checked") && (nQtdEstoquesCheckeds > 2)) || (bLocalizacao));


            var lUseBold = true;
            var nFontTitleSize = 8;
            var lUseBoldSubDetail = false;
            var nLeft = 0;
            var nTopHeader2 = 0;

            string PosX_Land1 = "8.255";
            string PosX_Land2 = "17.04855";
            string TamLinha = "20.6";


            if (sParams != "")
            {

            }

            string _Landspcape = "Portrait";

            if (Landscape)
            {
                _Landspcape = "Landscape";
                PosX_Land1 = "12.065";
                PosX_Land2 = "24.98605";
                TamLinha = "29.5";
            }


            string[,] arrQuery = new string[,] 
            {
                {"Query1", MasterQuerySQL},
                {"Query2", strSQLParsed}
            };

            string[,] arrQueryExcel = new string[,] 
            {
                {"Query2", strSQLParsed}
            };


            string[,] arrIndexKey = new string[,] 
            {
                {"Query2", "TesteID", "TesteID"},
            };


            if (chkExcel == "checked")
            {

                Relatorio.CriarRelatório("RelEstoque_Generico_Excel", arrQueryExcel, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, "0", "0", "9", "Black", "B", "Body", "5");
                Relatorio.CriarObjLabel("Fixo", "", "Relatório de Estoque", "12.065", "0", "9", "#0113a0", "B", "Body", "5");
                //Relatorio.CriarObjLabel("Fixo", "", "Relatório de Estoque", "1.2065", "0", "9", "#0113a0", "B", "Body", "5");
                Relatorio.CriarObjLabelData("Now", "", "", "20.5", "0", "9", "black", "B", "Body", "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "2.05", "0", "9", "black", "B", "Body", "3.72187", "Horas");

                Relatorio.CriarObjTabela("0", "3.9688", "Query2", true, true,true);
                //Relatorio.CriarObjTabela("0", "0.39688", "Query2", true, true, true);


                Relatorio.TabelaEnd();

                Relatorio.CriarPDF_Excel("RelatorioEstoque", 2);

                return;
            }
            else
            {

            }


            Relatorio.CriarRelatório("RelatorioEstoque", arrQuery, "BRA", _Landspcape, "2.41993", "0", "0");

            Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, "0.2", "0.34396", "12", "Black", "B", "Header", "5");
            //Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, "0.02", "0.034396", "12", "Black", "B", "Header", "5");
            Relatorio.CriarObjLabel("Fixo", "", "Relatório de Estoque", PosX_Land1, "0.34396", "12", "#0113a0", "B", "Header", "5");
            //Relatorio.CriarObjLabel("Fixo", "", "Relatório de Estoque", PosX_Land1, "0.034396", "12", "#0113a0", "B", "Header", "5");
            Relatorio.CriarObjLabelData("Now", "", "", PosX_Land2, "0.34396", "11", "black", "B", "Header", "3.72187", "Horas");
            //Relatorio.CriarObjLabelData("Now", "", "", PosX_Land2, "0.034396", "11", "black", "B", "Header", "3.72187", "Horas");
            Relatorio.CriarObjLabel("Fixo", "", sParams, "0.2", "0.89077", "9", "Black", "B", "Header", "5");
            //Relatorio.CriarObjLabel("Fixo", "", sParams, "0.02", "0.089077", "9", "Black", "B", "Header", "5");

            Relatorio.CriarObjLinha("0.2", "0.85549", TamLinha, "", "Header");
            //Relatorio.CriarObjLinha("0.02", "0.085549", TamLinha, "", "Header");

            Relatorio.CriarObjTabela("0.2", "0.0", arrIndexKey, false, true);
            //Relatorio.CriarObjTabela("0.02", "0.0", arrIndexKey, false, true);

            Relatorio.CriarObjColunaNaTabela("ID", "ID", true, "ID");
            Relatorio.CriarObjCelulaInColuna("Query", "ConceitoAbstrato2", "ParentGroup", "TesteID", "", 3, true);
            Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup");
            Relatorio.CriarObjCelulaInColuna("Fixo", "Totais", "TotalGroup", "TesteID", "DetailGroup", 0, true);
            Relatorio.CriarObjCelulaInColuna("Fixo", "Totais Gerais", "TotalGroup", "TesteID", "ParentGroup", 3, true);

            Relatorio.CriarObjColunaNaTabela("E", "Est", true, "Est");
            Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "ParentGroup", "TesteID");
            Relatorio.CriarObjCelulaInColuna("Query", "Est", "DetailGroup");
            Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "TesteID", "DetailGroup", 0, true);
            Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "TesteID", "ParentGroup", 0, true);

            Relatorio.CriarObjColunaNaTabela("Produto", "Produto", true, "Produto");
            Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "ParentGroup", "TesteID");
            Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup");
            Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "TesteID", "DetailGroup", 0, true);
            Relatorio.CriarObjCelulaInColuna("Fixo", "Merge", "TotalGroup", "TesteID", "ParentGroup", 0, true);

            if (bLocalizacao)
            {

            }
            else
            {

                if (aCmbsEstoques_0 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("EPO", "EPO", true, "EPO");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPO", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPO", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "EPO", "TotalGroup", "TesteID", "ParentGroup", 0, true);

                }

                if (aCmbsEstoques_1 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("EPE", "EPE", true, "EPE");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPE", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPE", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "EPE", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_2 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("EPD", "EPD", true, "EPD");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPD", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPD", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "EPD", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_3 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("EPC", "EPC", true, "EPC");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPC", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPC", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "EPC", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_4 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("EPT", "EPT", true, "EPT");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPT", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "EPT", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "EPT", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_5 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("ETO", "ETO", true, "ETO");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ETO", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "ETO", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "ETO", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_6 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("ETE", "ETE", true, "ETE");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ETE", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "ETE", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "ETE", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_7 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("ETD", "ETD", true, "ETD");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ETD", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "ETD", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "ETD", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_8 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("ETC", "ETC", true, "ETC");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ETC", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "ETC", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "ETC", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_9 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("EB", "EB_", true, "EB_");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "EB_", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "EB_", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "EB_", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_10 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("F", "F", true, "F");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "F", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "F", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "F", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_11 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("Dis", "Dis", true, "Dis");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "Dis", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "Dis", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "Dis", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_12 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("RC", "RC", true, "RC");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "RC", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "RC", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "RC", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_13 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("RCC", "RCC", true, "RCC");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "RCC", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "RCC", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "RCC", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_14 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("RV", "RV", true, "RV");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "RV", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "RV", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "RV", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }

                if (aCmbsEstoques_15 == "checked")
                {
                    Relatorio.CriarObjColunaNaTabela("RVC", "RVC", true, "RVC");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "RVC", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "RVC", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "RVC", "TotalGroup", "TesteID", "ParentGroup", 0, true);
                }


                Relatorio.CriarObjColunaNaTabela("Quant", "Quant", true, "Quant");
                Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                Relatorio.CriarObjCelulaInColuna("Query", "Quant", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "Quant", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                Relatorio.CriarObjCelulaInColuna("Query", "Quant", "TotalGroup", "TesteID", "ParentGroup", 0, true);

                if (chkValoracao == "checked")
                {

                    Relatorio.CriarObjColunaNaTabela("Custo", "Custo", true, "Custo");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "Custo", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "TesteID", "ParentGroup", 0, true);

                    Relatorio.CriarObjColunaNaTabela("Total", "TotalCusto", true, "TotalCusto");
                    Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalCusto", "DetailGroup");
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalCusto", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                    Relatorio.CriarObjCelulaInColuna("Query", "TotalCusto", "TotalGroup", "TesteID", "ParentGroup", 0, true);

                }


                Relatorio.CriarObjColunaNaTabela("Observação", "Observacao", true, "Observacao");
                Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                Relatorio.CriarObjCelulaInColuna("Fixo", "", "TotalGroup", "TesteID", "ParentGroup", 0, true);

                Relatorio.CriarObjColunaNaTabela("Controle", "dtControle", true, "dtControle");
                Relatorio.CriarObjCelulaInColuna("Query", "", "ParentGroup", "TesteID");
                Relatorio.CriarObjCelulaInColuna("Query", "dtControle", "DetailGroup");
                Relatorio.CriarObjCelulaInColuna("Query", "", "TotalGroup", "TesteID", "DetailGroup", 0, true);
                Relatorio.CriarObjCelulaInColuna("Query", "", "TotalGroup", "TesteID", "ParentGroup", 0, true);
            }

            Relatorio.TabelaEnd();            

            Relatorio.CriarPDF_Excel("RelatorioEstoque", 1);


        }

        private void relatorioEstoqueIntel()
        {
            //var dirA1;
            //var dirA2;
            int i;
            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            string selEmpresa = HttpContext.Current.Request.QueryString["selEmpresa"];
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            string selMarca = HttpContext.Current.Request.QueryString["selMarca"];
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            var sEstadosCheckeds = HttpContext.Current.Request.QueryString["sEstadosCheckeds"];
            var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            var chkTemEstoque = HttpContext.Current.Request.QueryString["chkTemEstoque"];
            var glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_nEmpresaID"]);

            string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            string[] _selMarca = new string[] { };
            string[] _elem_Value = new string[] { };

            string sTmpTableSelect = string.Empty;

            //var dateTimeInSQLFormat = null;

            // Analiza o campo txtDataEstoque
            //txtDataEstoque.value = trimStr(txtDataEstoque.value);

            //if ( !((txtDataEstoque.value == null) || (txtDataEstoque.value == '')) )
            //{
            //    if ( criticAndNormTxtDataTime(txtDataEstoque) )
            //    {
            //        dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            //        if (dateTimeInSQLFormat == null)
            //            return null;
            //    }
            //    else
            //        return null;
            //}

            char[] DelimitChar1 = { ':' };

            //if (dateTimeInSQLFormat != "NULL")
            //    dateTimeInSQLFormat = "dbo.fn_Data_Fuso('" + dateTimeInSQLFormat + "', NULL," + glb_nEmpresaCidadeID + ")";
            //else
            //    dateTimeInSQLFormat = "NULL";


            //if (dateTimeInSQLFormat != "")
            //    dateTimeInSQLFormat = "dbo.fn_Data_Fuso(" + "\"" + dateTimeInSQLFormat + "\"" + ", NULL," + glb_nEmpresaCidadeID + ")";
            //else
            //    dateTimeInSQLFormat = "NULL";


            if (dateTimeInSQLFormat == "NULL")
            //dateTimeInSQLFormat = "dbo.fn_Data_Fuso('" + dateTimeInSQLFormat + "', NULL," + glb_nEmpresaCidadeID + ")";
            {
                dateTimeInSQLFormat = "NULL";
            }


            var sFiltroFabricante = (selFabricanteValue == 0 ? "" : " AND Produtos.FabricanteID =" + selFabricanteValue);

            var sFiltroFamilia = "";
            var nItensSelected = 0;
            var sFiltroProprietario = "";

            if (selProprietarios2_selectedIndex > 0)
                sFiltroProprietario = " AND ProdutosEmpresa.ProprietarioID = " + selProprietarios2_value + " ";

            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < _selFamilias.Length; i++)
            {
                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + selFamilias[i].ToString();

            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");

            var sFiltroEstados = " ";
            var bFirst = true;
            //var sFiltro = "";
            //var sEstadosCheckeds = "";


            var sClausulaIN = "";

            //sFiltro = trimStr(txtFiltro.value);

            //if (sFiltro != "")
            //    sFiltro = " AND (" + sFiltro + ") ";

            if (elem_Value != "")
                _elem_Value = elem_Value.Split(DelimitChar1);

            for (i = 0; i < (_elem_Value.Length); i++)
            {

                // loop nos checkbox de estados para montar a string de filtro
                //for ( i=0; i<(divRelatorioEstoque.children.length); i++ )
                //{
                //    elem = divRelatorioEstoque.children[i];

                //    if ( (elem.tagName).toUpperCase() == 'INPUT' )
                //    {
                //        if ( ((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                //             (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                //             (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                //             (elem.id.toUpperCase() != 'CHKVALORACAO') &&                 
                //             (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&                                  
                //             (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                //             (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                //             (elem.id.toUpperCase() != 'CHKEXCEL') &&
                //             (elem.id.indexOf('_ESTOQUE_') < 0) )
                //        {

                if (i == 0)
                    sClausulaIN = _elem_Value[i];  //+ '', '';
                else
                    sClausulaIN += ", " + _elem_Value[i];  //+ '', '';

                if (bFirst)
                {
                    bFirst = false;

                    //Old 
                    //sFiltroEstados = ' AND (ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    // sFiltroEstados = ' AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' +  dateTimeInSQLFormat +')= ' + elem.value + ' ';
                    // sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;


                    //sEstadosCheckeds = translateTerm('Estados', null) + ':' + (elem.previousSibling).innerText;
                }
                else
                {

                    //Old
                    // sFiltroEstados += ' OR ProdutosEmpresa.EstadoID=' + elem.value + ' ';
                    //sFiltroEstados += ' OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, ' + dateTimeInSQLFormat + ')= ' + elem.value + ' ';
                    //sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;                   

                    //sEstadosCheckeds += ' ' + (elem.previousSibling).innerText;


                }
            }

            if (bFirst)
                sFiltroEstados += " AND (ProdutosEmpresa.EstadoID=0)";


            //var aCmbsEstoques = 
            //    [chk1_ESTOQUE_EPO,
            //     chk1_ESTOQUE_EPE,
            //     chk1_ESTOQUE_EPD,
            //     chk1_ESTOQUE_EPC,
            //     chk1_ESTOQUE_EPT,
            //     chk1_ESTOQUE_ETO,
            //     chk1_ESTOQUE_ETE,
            //     chk1_ESTOQUE_ETD,
            //     chk1_ESTOQUE_ETC];

            string[] aSQLQuantEstoquesDetail =
                {"dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349,Depositos.ObjetoID," + dateTimeInSQLFormat + ",NULL,375, -1) "};

            //var aDynamicEstoque = 
            //    ['_EPO Numeric(10),',
            //     '_EPE Numeric(10),',
            //     '_EPD Numeric(10),',
            //     '_EPC Numeric(10),',
            //     '_EPT Numeric(10),',
            //     '_ETO Numeric(10),',
            //     '_ETE Numeric(10),',
            //     '_ETD Numeric(10),',
            //     '_ETC Numeric(10),'];

            string sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

            sSQLDetailSelect = "SELECT DISTINCT ProdutosEmpresa.SujeitoID AS EmpresaID, " +
                "CONVERT(VARCHAR(10), dbo.fn_Data_Zero(" + dateTimeInSQLFormat + "), 112) AS [A01], Produtos.ConceitoID AS [A03] ";

            var bHasEstoqueSelected = false;
            var strSumEstoques = "";

            string[] _chkCmbEstoques = chkCmbEstoques.Split(DelimitChar1);

            for (i = 0; i < _chkCmbEstoques.Length; i++)
            {
                if (_chkCmbEstoques[i] == "checked")
                {
                    if (!bHasEstoqueSelected)
                        strSumEstoques += ", ";
                    else
                        strSumEstoques += " + ";

                    strSumEstoques += aSQLQuantEstoquesDetail[i];
                    bHasEstoqueSelected = true;
                }
            }

            var sA04 = ", 0 AS [A04]";
            var sA041 = ", 0 AS [A041]";

            sA04 = strSumEstoques + " AS [A04] ";

            sA041 = ", ISNULL((SELECT SUM(Itens.Quantidade) FROM Pedidos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK), Operacoes Transacoes WITH(NOLOCK) " +
                "WHERE (Pedidos.EmpresaID = ProdutosEmpresa.SujeitoID AND Pedidos.PedidoID=Itens.PedidoID AND " +
                "Pedidos.TransacaoID=Transacoes.OperacaoID AND Transacoes.MetodoCustoID=372 AND Pedidos.DepositoID = Depositos.ObjetoID AND " +
                "Pedidos.EstadoID BETWEEN 23 AND 28 AND Itens.ProdutoID=Produtos.ConceitoID)), 0) AS [A041]," +


                "dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, dbo.fn_Data_Fuso(" + dateTimeInSQLFormat + ", NULL,16659)) AS [RelacaoEstadoID]";

            sSQLDetailSelect += sA04 + sA041 + ", Produtos.Modelo AS [A02], " +
                "(CASE WHEN Depositos.ObjetoID = Depositos.SujeitoID THEN " +
                    "(SELECT TOP 1 a.Codigo " +
                        "FROM RelacoesPessoas a WITH(NOLOCK) " +
                        "WHERE (a.TipoRelacaoID = 21 AND a.SujeitoID = ProdutosEmpresa.SujeitoID AND a.ObjetoID = 10002)) " +
                    "ELSE (SELECT TOP 1 CodFab.Codigo " +
                            "FROM RelacoesPessoas_CodigosFabricante CodFab WITH(NOLOCK) " +
                    "WHERE CodFab.RelacaoID = Depositos.RelacaoID AND CodFab.FabricanteID = 10002) END) AS [A06] ";

            sSQLDetailFrom = "FROM Conceitos Produtos WITH(NOLOCK) " +
                                "INNER JOIN Conceitos Familias WITH(NOLOCK) ON Familias.ConceitoID = Produtos.ProdutoID " +
                                "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON Marcas.ConceitoID=Produtos.MarcaID " +
                                "INNER JOIN RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ON Produtos.ConceitoID = ProdutosEmpresa.ObjetoID " +
                                "INNER JOIN (SELECT RelPesDeposito.RelacaoID, RelPesDeposito.SujeitoID, RelPesDeposito.ObjetoID " +
                                                "FROM RelacoesPessoas RelPesDeposito WITH(NOLOCK) " +
                                                "WHERE TipoRelacaoID = 28 AND EstadoID = 2" +
                                                "UNION ALL " +
                                            "SELECT NULL AS RelacaoID, EmpSistema.SujeitoID AS SujeitoID, EmpSistema.SujeitoID AS ObjetoID " +
                                                "FROM RelacoesPesRec EmpSistema WITH(NOLOCK) " +
                                                "WHERE (EmpSistema.TipoRelacaoID = 12  " +
                                                    "AND EmpSistema.ObjetoID = 999  " +
                                                    "AND EmpSistema.EstadoID = 2)) Depositos ON ProdutosEmpresa.SujeitoID = Depositos.SujeitoID ";

            sSQLDetailWhere1 = "WHERE Produtos.TipoConceitoID=303 AND Marcas.TipoConceitoID = 304 AND Familias.TipoConceitoID = 302 " +
                    " AND ProdutosEmpresa.TipoRelacaoID=61 ";

            if (glb_nEmpresaID == 3)
                sSQLDetailWhere1 += " AND ProdutosEmpresa.SujeitoID = " + glb_nEmpresaID + " ";
            else
                sSQLDetailWhere1 += " AND ProdutosEmpresa.SujeitoID <> 3 ";

            if (chkProdutoSeparavel == "checked")
                sSQLDetailWhere1 += " AND Produtos.ProdutoSeparavel = 1 ";

            if (chkReportaFabricante == "checked")
                sSQLDetailWhere1 += " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            if (glb_DepositoSelecionado != "NULL")
                sSQLDetailWhere1 += " AND Depositos.ObjetoID = " + glb_DepositoSelecionado + " ";

            sSQLDetailWhere1 += sFiltroProprietario + sFiltroFabricante + sFiltroFamilia + sFiltroEstados + " " + sFiltro + " ";


            //Old
            /*sSQLDetailGroupBy = " GROUP BY ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, Produtos.Modelo, " +
                " Depositos.SujeitoID, Depositos.ObjetoID, Depositos.RelacaoID ";

            sSQLDetailOrderBy = "ORDER BY EmpresaID, A03, A02 "; */


            sSQLDetailGroupBy = " GROUP BY ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, Produtos.Modelo, " +
                                " Depositos.SujeitoID, Depositos.ObjetoID, ProdutosEmpresa.RelacaoID, Depositos.RelacaoID ";



            sSQLDetailOrderBy = "ORDER BY EmpresaID, A03, A02 ";


            //Old
            /*sTmpTableSelect = 
                'CREATE TABLE #TempTable ' +
                '( ' +
                'IDTMP_ int IDENTITY,' +
                'EmpresaID NUMERIC(10),' +
                'A01 VARCHAR(10),' +
                'A03 NUMERIC(10),' +
                'A04 NUMERIC(10),' +
                'A041 NUMERIC(10),' +
                'A02 VARCHAR(18), ' +
                'A06 VARCHAR(10)) '; */

            sTmpTableSelect =
                "CREATE TABLE #TempTable " +
                "( " +
                "IDTMP_ int IDENTITY," +
                "EmpresaID NUMERIC(10)," +
                "A01 VARCHAR(10)," +
                "A03 NUMERIC(10)," +
                "A04 NUMERIC(10)," +
                "A041 NUMERIC(10)," +
                "RelacaoEstadoID NUMERIC(10)," +
                "A02 VARCHAR(18), " +
                "A06 VARCHAR(10)) ";


            //Old
            //sTmpTableSelect += 'INSERT INTO #TempTable (EmpresaID, A01, A03, A04, A041, A02, A06) ' +

            sTmpTableSelect += "INSERT INTO #TempTable (EmpresaID, A01, A03, A04, A041, RelacaoEstadoID, A02, A06) " +
                sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailGroupBy + sSQLDetailOrderBy + " ";

            //Old
            //if (chkTemEstoque.checked)
            //   sTmpTableSelect += "SELECT * FROM #TempTable WHERE [A04] > 0 ";

            if (chkTemEstoque == "checked")
                sTmpTableSelect += "SELECT IDTMP_, " +
                "EmpresaID," +
                "A01," +
                "A03," +
                "A04," +
                "A041," +
                "A02, " +
                "A06 FROM #TempTable WHERE [A04] > 0  AND RelacaoEstadoID IN (" + sClausulaIN + ")";
            else
                //Old
                // sTmpTableSelect += "SELECT * FROM #TempTable ";

                sTmpTableSelect += "SELECT IDTMP_, " +
                "EmpresaID, " +
                "A01," +
                "A03," +
                "A04," +
                "A041," +
                "A02, " +
                "A06 FROM #TempTable WHERE RelacaoEstadoID IN (" + sClausulaIN + ")";

            sTmpTableSelect += "DROP TABLE #TempTable";

            var strSQLParsed = "EXEC sp_RelatorioEstoque " + "'" + dupCharInString(sTmpTableSelect, "'") + "'";

            strSQLParsed = (strSQLParsed == null ? "" : strSQLParsed);

            string[,] arrQuerys = new string[,] 
            {
                {"Query1", strSQLParsed}
            };

            Relatorio.CriarRelatório("RelEstPadraoIntel", arrQuerys, "BRA", "Excel");

            Relatorio.CriarObjTabela("0", "0", "Query1", false, true);
            /*
            Relatorio.CriarObjColunaNaTabela("EmpresaID", "EmpresaID", true, "EmpresaID");
            Relatorio.CriarObjCelulaInColuna("Query", "EmpresaID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("A01", "A01", true, "A01");
            Relatorio.CriarObjCelulaInColuna("Query", "A01", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("A03", "A03", true, "A03");
            Relatorio.CriarObjCelulaInColuna("Query", "A03", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("A04", "A04", true, "A04");
            Relatorio.CriarObjCelulaInColuna("Query", "A04", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("A02", "A02", true, "A02");
            Relatorio.CriarObjCelulaInColuna("Query", "A02", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("A06", "A06", true, "A06");
            Relatorio.CriarObjCelulaInColuna("Query", "A06", "DetailGroup");*/

            Relatorio.TabelaEnd();

            Relatorio.CriarPDF_Excel("RelEstoquePadraoIntel", 2);
        }

        private void relatorioEstoqueMicrosoft()
        {
            //var dirA1;
            //var dirA2;
            int i;

            //var dateTimeInSQLFormat = null;

            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            string selMarca = HttpContext.Current.Request.QueryString["selMarca"];
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            var sEstadosCheckeds = HttpContext.Current.Request.QueryString["sEstadosCheckeds"];
            var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            var chkTemEstoque = HttpContext.Current.Request.QueryString["chkTemEstoque"];
            var glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_nEmpresaID"]);
            var glb_sCurrDateYYYYMMDD = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_sCurrDateYYYYMMDD"]);

            string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            string[] _selMarca = new string[] { };
            string[] _elem_Value = new string[] { };

            string sTmpTableSelect = string.Empty;

            char[] DelimitChar1 = { ':' };

            // Analiza o campo txtDataEstoque
            //txtDataEstoque.value = trimStr(txtDataEstoque.value);

            //if ( !((txtDataEstoque.value == null) || (txtDataEstoque.value == '')) )
            //{
            //    if ( criticAndNormTxtDataTime(txtDataEstoque) )
            //    {
            //        dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            //        if (dateTimeInSQLFormat == null)
            //            return null;
            //    }
            //    else
            //        return null;
            //}


            if (dateTimeInSQLFormat == "NULL")
            //dateTimeInSQLFormat = "dbo.fn_Data_Fuso('" + dateTimeInSQLFormat + "', NULL," + glb_nEmpresaCidadeID + ")";
            {
                dateTimeInSQLFormat = "NULL";
            }

            var sFiltroFabricante = (selFabricanteValue == 0 ? "" : " AND Produtos.FabricanteID=" + selFabricanteValue);
            var sFiltroFamilia = "";
            var nItensSelected = 0;
            var sFiltroProprietario = "";

            //Quando o Fabricante escolhido for Microsoft US, trazer também o fabricante Macrosoft Corp ID 91575.
            if (selFabricanteValue == 10003)
                sFiltroFabricante = "AND Produtos.FabricanteID IN (" + selFabricanteValue + ", 91575)";

            if (selProprietarios2_selectedIndex > 0)
                sFiltroProprietario = " AND ProdutosEmpresa.ProprietarioID = " + selProprietarios2_value + " ";


            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < _selFamilias.Length; i++)
            {
                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + _selFamilias[i].ToString();

            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");


            //DireitoEspecifico
            //Relacao PesCon->Produtos->Modal Print
            //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Microsoft)
            //A1||A2 -> Impressao
            //A2 -> Zebrar
            //A1&&A2 -> Excel
            //desabilitado-> Word


            var sFiltroEstados = " ";
            var bFirst = true;
            //var sFiltro = "";
            //var sEstadosCheckeds = "";

            //sFiltro = trimStr(txtFiltro.value);

            //if (sFiltro != "")
            //    sFiltro = " AND (" + sFiltro + ") ";

            // loop nos checkbox de estados para montar a string de filtro

            if (elem_Value != "")
                _elem_Value = elem_Value.Split(DelimitChar1);

            for (i = 0; i < (_elem_Value.Length); i++)
            {

                //for ( i=0; i<(divRelatorioEstoque.children.length); i++ )
                //{
                //    elem = divRelatorioEstoque.children[i];

                //    if ( (elem.tagName).toUpperCase() == 'INPUT' )
                //    {
                //        if ( ((elem.type).toUpperCase() == 'CHECKBOX') && (elem.checked) &&
                //             (elem.id.toUpperCase() != 'CHKLOCALIZACAO') &&
                //             (elem.id.toUpperCase() != 'CHKREPORTAFABRICANTE') &&
                //             (elem.id.toUpperCase() != 'CHKVALORACAO') &&
                //             (elem.id.toUpperCase() != 'CHKCUSTOCONTABIL') &&                 
                //             (elem.id.toUpperCase() != 'CHKPRODUTOSEPARAVEL') &&
                //             (elem.id.toUpperCase() != 'CHKTEMESTOQUE') &&
                //             (elem.id.toUpperCase() != 'CHKEXCEL') &&
                //             (elem.id.indexOf('_ESTOQUE_') < 0) )
                //        {
                if (bFirst)
                {
                    bFirst = false;

                    //sFiltroEstados = " AND (ProdutosEmpresa.EstadoID=" + elem.value + " ";
                    sFiltroEstados = " AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";
                    //sEstadosCheckeds = translateTerm("Estados", null) + ":" + (elem.previousSibling).innerText;
                }
                else
                {
                    // sFiltroEstados += " OR ProdutosEmpresa.EstadoID=" + elem.value + " ";
                    sFiltroEstados += " OR ( dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + ") ";
                    //sEstadosCheckeds += " " + (elem.previousSibling).innerText;
                }
            }

            if (!bFirst)
                sFiltroEstados += ")";
            else
                sFiltroEstados += " AND (ProdutosEmpresa.EstadoID=0)";

            //var aCmbsEstoques = 
            //    [chk1_ESTOQUE_EPO,
            //     chk1_ESTOQUE_EPE,
            //     chk1_ESTOQUE_EPD,
            //     chk1_ESTOQUE_EPC,
            //     chk1_ESTOQUE_EPT,
            //     chk1_ESTOQUE_ETO,
            //     chk1_ESTOQUE_ETE,
            //     chk1_ESTOQUE_ETD,
            //     chk1_ESTOQUE_ETC];

            string[] aSQLQuantEstoquesDetail = new string[]
            {
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) "
            };

            //var aDynamicEstoque = 
            //    ['_EPO Numeric(10),',
            //     '_EPE Numeric(10),',
            //     '_EPD Numeric(10),',
            //     '_EPC Numeric(10),',
            //     '_EPT Numeric(10),',
            //     '_ETO Numeric(10),',
            //     '_ETE Numeric(10),',
            //     '_ETD Numeric(10),',
            //     '_ETC Numeric(10),'];

            string sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

            var bHasEstoqueSelected = false;
            var strSumEstoques = "";

            string[] _chkCmbEstoques = chkCmbEstoques.Split(DelimitChar1);

            for (i = 0; i < _chkCmbEstoques.Length; i++)
            {
                if (_chkCmbEstoques[i] == "checked")
                {
                    if (!bHasEstoqueSelected)
                        strSumEstoques += ", ";
                    else
                        strSumEstoques += " + ";

                    strSumEstoques += aSQLQuantEstoquesDetail[i];
                    bHasEstoqueSelected = true;
                }
            }

            //if (strSumEstoques == "")
            //{
            //    if (window.top.overflyGen.Alert("Selecione pelo menos um estoque.") == 0)
            //        return null;

            //    lockControlsInModalWin(false);
            //    return null;
            //}

            sSQLDetailSelect = "SELECT Empresas.Fantasia, " + "'" + "ALBZ" + "'" + ", CHAR(79), " +
                "'" + glb_sCurrDateYYYYMMDD + "'" + ", SPACE(0), SPACE(0), SPACE(0), SPACE(0), " +
                "SPACE(0), SPACE(0), SPACE(0), Produtos.PartNumber, SPACE(0), SPACE(0), Produtos.Modelo, SPACE(0) " +
                strSumEstoques + ", SPACE(0), SPACE(0), SPACE(0), SPACE(0), Familias.Conceito ";

            sSQLDetailFrom = "FROM " +
                    "Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), " +
                    "RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), " +
                    "RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Recursos Estados WITH(NOLOCK), Recursos Sistema WITH(NOLOCK), Pessoas Empresas WITH(NOLOCK) ";

            sSQLDetailWhere1 = "WHERE (" +
                    "(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND " +
                    "(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND " +
                    "RelConceitos1.EstadoID=2) AND " +
                    "(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND " +
                    "Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND " +
                    "(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND " +
                    "RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND " +
                    "(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ";

            var sFiltroEmpresa = " AND ProdutosEmpresa.SujeitoID = " + glb_nEmpresaID + " ";

            sSQLDetailWhere2 =
                    "Conceitos3.EstadoID=2) AND " +
                    "(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND " +
                    "(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 " +
                    sFiltroEmpresa + sFiltroProprietario + " AND ProdutosEmpresa.SujeitoID=Empresas.PessoaID AND " +
                    "ProdutosEmpresa.EstadoID=Estados.RecursoID AND Sistema.RecursoID=999) AND " +
                    "(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) ";

            if (chkProdutoSeparavel == "checked")
                sSQLDetailWhere2 += " AND Produtos.ProdutoSeparavel = 1 ";

            if (chkReportaFabricante == "checked")
                sSQLDetailWhere2 += " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            if (chkTemEstoque == "checked")
                sSQLDetailWhere2 += " AND (" + (strSumEstoques).Substring(1) + ") > 0 ";

            sSQLDetailWhere2 += sFiltroFabricante + sFiltroFamilia + sFiltroEstados + " " + sFiltro + ") ";

            sSQLDetailGroupBy = "";

            sSQLDetailOrderBy = "";

            sTmpTableSelect =
                "CREATE TABLE #TempTable " +
                "( " +
                "IDTMP_ INTEGER IDENTITY, " +
                "[Sell_From_Global_Name] VARCHAR(80), " +
                "[Source_ID] VARCHAR(8), " +
                "[Resend_Indicator] VARCHAR(1), " +
                "[Report_End_Date] VARCHAR(8), " +
                "[Ship_From_Global_Name] VARCHAR(80), " +
                "[Ship_From_External_ID] VARCHAR(20), " +
                "[Ship_From_Address_Line_1] VARCHAR(80), " +
                "[Ship_From_Address_Line_2] VARCHAR(80), " +
                "[Ship_From_City_Name] VARCHAR(80), " +
                "[Ship_From_State_Province] VARCHAR(80), " +
                "[Ship_From_Postal_Code] VARCHAR(20), " +
                "[MS_Part_Number] VARCHAR(MAX), " +
                "[MS_UPC] VARCHAR(12), " +
                "[Source_Product_ID] VARCHAR(30), " +
                "[Source_Product_Description] VARCHAR(80), " +
                "[Quantity_In_Float] INTEGER, " +
                "[Quantity_On_Hand] INTEGER, " +
                "[Committed_Quantity] INTEGER, " +
                "[On_Order_Quantity] INTEGER, " +
                "[Returns_Quantity] INTEGER, " +
                "[Backorder_Quantity] INTEGER, " +
                "[MS_Familia] VARCHAR(25))";

            sTmpTableSelect += "INSERT INTO #TempTable ([Sell_From_Global_Name],[Source_ID],[Resend_Indicator]," +
                "[Report_End_Date],[Ship_From_Global_Name],[Ship_From_External_ID],[Ship_From_Address_Line_1]," +
                "[Ship_From_Address_Line_2],[Ship_From_City_Name],[Ship_From_State_Province],[Ship_From_Postal_Code]," +
                "[MS_Part_Number],[MS_UPC],[Source_Product_ID],[Source_Product_Description],[Quantity_In_Float]," +
                "[Quantity_On_Hand],[Committed_Quantity],[On_Order_Quantity],[Returns_Quantity],[Backorder_Quantity], [MS_Familia]) " +
                sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailWhere2 + sSQLDetailGroupBy + sSQLDetailOrderBy + " ";

            sTmpTableSelect += "SELECT * FROM #TempTable ";
            sTmpTableSelect += "DROP TABLE #TempTable";

            var strSQLParsed = "EXEC sp_RelatorioEstoque " + "'" + dupCharInString(sTmpTableSelect, "'") + "'";

            strSQLParsed = (strSQLParsed == null ? "" : strSQLParsed);

            string[,] arrQuerys = new string[,] 
            {
                {"Query1", strSQLParsed}
            };

            Relatorio.CriarRelatório("RelEstPadraoMicrosoft", arrQuerys, "BRA", "Excel");

            Relatorio.CriarObjTabela("0", "0", "Query1", false, false);
          
            Relatorio.CriarObjColunaNaTabela("Sell From Global Name", "Sell_From_Global_Name", true, "Sell_From_Global_Name");
            Relatorio.CriarObjCelulaInColuna("Query", "Sell_From_Global_Name", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Source ID", "Source_ID", true, "Source_ID");
            Relatorio.CriarObjCelulaInColuna("Query", "Source_ID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Resend Indicator", "Resend_Indicator", true, "Resend_Indicator");
            Relatorio.CriarObjCelulaInColuna("Query", "Resend_Indicator", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Report End Date", "Report_End_Date", true, "Report_End_Date");
            Relatorio.CriarObjCelulaInColuna("Query", "Report_End_Date", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Ship From Global Name", "Ship_From_Global_Name", true, "Ship_From_Global_Name");
            Relatorio.CriarObjCelulaInColuna("Query", "Ship_From_Global_Name", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Ship From External ID", "Ship_From_External_ID", true, "Ship_From_External_ID");
            Relatorio.CriarObjCelulaInColuna("Query", "Ship_From_External_ID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Ship From Address Line 1", "Ship_From_Address_Line_1", true, "Ship_From_Address_Line_1");
            Relatorio.CriarObjCelulaInColuna("Query", "Ship_From_Address_Line_1", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Ship From Address Line 2", "Ship_From_Address_Line_2", true, "Ship_From_Address_Line_2");
            Relatorio.CriarObjCelulaInColuna("Query", "Ship_From_Address_Line_2", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Ship From City Name", "Ship_From_City_Name", true, "Ship_From_City_Name");
            Relatorio.CriarObjCelulaInColuna("Query", "Ship_From_City_Name", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Ship From State/Province", "Ship_From_State_Province", true, "Ship_From_State_Province");
            Relatorio.CriarObjCelulaInColuna("Query", "Ship_From_State_Province", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Ship From Postal Code", "Ship_From_Postal_Code", true, "Ship_From_Postal_Code");
            Relatorio.CriarObjCelulaInColuna("Query", "Ship_From_Postal_Code", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("MS Part Number", "MS_Part_Number", true, "MS_Part_Number");
            Relatorio.CriarObjCelulaInColuna("Query", "MS_Part_Number", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("MS UPC", "MS_UPC", true, "MS_UPC");
            Relatorio.CriarObjCelulaInColuna("Query", "MS_UPC", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Source Product ID", "Source_Product_ID", true, "Source_Product_ID");
            Relatorio.CriarObjCelulaInColuna("Query", "Source_Product_ID", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Source Product Description", "Source_Product_Description", true, "Source_Product_Description");
            Relatorio.CriarObjCelulaInColuna("Query", "Source_Product_Description", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Quantity In Float", "Quantity_In_Float", true, "Quantity_In_Float");
            Relatorio.CriarObjCelulaInColuna("Query", "Quantity_In_Float", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Quantity On Hand", "Quantity_On_Hand", true, "Quantity_On_Hand");
            Relatorio.CriarObjCelulaInColuna("Query", "Quantity_On_Hand", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Committed Quantity", "Committed_Quantity", true, "Committed_Quantity");
            Relatorio.CriarObjCelulaInColuna("Query", "Committed_Quantity", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("On Order Quantity", "On_Order_Quantity", true, "On_Order_Quantity");
            Relatorio.CriarObjCelulaInColuna("Query", "On_Order_Quantity", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Returns Quantity", "Returns_Quantity", true, "Returns_Quantity");
            Relatorio.CriarObjCelulaInColuna("Query", "Returns_Quantity", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Backorder Quantity", "Backorder_Quantity", true, "Backorder_Quantity");
            Relatorio.CriarObjCelulaInColuna("Query", "Backorder_Quantity", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("MS Familia", "MS_Familia", true, "MS_Familia");
            Relatorio.CriarObjCelulaInColuna("Query", "MS_Familia", "DetailGroup");
            
            Relatorio.TabelaEnd();

            Relatorio.CriarPDF_Excel("RelEstPadraoMicrosoft", 2);

        }

        private void relatorioEstoque3com()
        {
            //var dirA1;
            //var dirA2;
            int i;

            //var dateTimeInSQLFormat = null;

            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            //int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            //int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            //int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            //string selMarca = HttpContext.Current.Request.QueryString["selMarca"];
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            //var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            //var sEstadosCheckeds = HttpContext.Current.Request.QueryString["sEstadosCheckeds"];
            //var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            //var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            //var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            //var chkTemEstoque = HttpContext.Current.Request.QueryString["chkTemEstoque"];
            //var glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_nEmpresaID"]);
            //var glb_sCurrDateYYYYMMDD = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_sCurrDateYYYYMMDD"]);

            //string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            //string[] _selMarca = new string[] { };
            //string[] _elem_Value = new string[] { };

            //string sTmpTableSelect = string.Empty;

            char[] DelimitChar1 = { ':' };

            // Analiza o campo txtDataEstoque
            //txtDataEstoque.value = trimStr(txtDataEstoque.value);

            //if ( !((txtDataEstoque.value == null) || (txtDataEstoque.value == '')) )
            //{
            //    if ( criticAndNormTxtDataTime(txtDataEstoque) )
            //    {
            //        dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            //        if (dateTimeInSQLFormat == null)
            //            return null;
            //    }
            //    else
            //        return null;
            //}


            if (dateTimeInSQLFormat == "NULL")
            //dateTimeInSQLFormat = "dbo.fn_Data_Fuso('" + dateTimeInSQLFormat + "', NULL," + glb_nEmpresaCidadeID + ")";
            {
                dateTimeInSQLFormat = "NULL";
            }

            var sFiltroFamilia = "";
            var nItensSelected = 0;

            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < _selFamilias.Length; i++)
            {

                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + _selFamilias[i].ToString();

            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");


            //DireitoEspecifico
            //Relacao PesCon->Produtos->Modal Print
            //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao 3Com)
            //A1||A2 -> Impressao
            //A2 -> Zebrar
            //A1&&A2 -> Excel
            //desabilitado-> Word

            //var sFiltroEstados = " ";    
            var bFirst = true;
            //var sFiltro = "";
            //var sEstadosCheckeds = "";
            var sReportaFabricante = "";

            //sFiltro = trimStr(txtFiltro.value);

            //if (sFiltro != "")
            //    sFiltro = " AND (" + sFiltro + ") ";

            if (chkReportaFabricante == "checked")
                sReportaFabricante = "AND ProdutosEmpresa.ReportaFabricante = 1 ";

            var s3com_variavel = "SET NOCOUNT ON DECLARE	@dtFim DATETIME, @Fabricante VARCHAR(8000), @EmpresaRelatorio INT, @MoedaID INT, @Nome VARCHAR(35), @Ecam VARCHAR(8), " +
                    "@UF_Abreviado VARCHAR(5), @Cep VARCHAR(12), @Pais VARCHAR(5), @Contato VARCHAR(35), @Telefone VARCHAR(20), " +
                    "@UF VARCHAR(30), @Sql VARCHAR(8000) " +
                    "SET @dtFim = ISNULL(" + dateTimeInSQLFormat + ",GETDATE()) ";

            var s3com_table = "CREATE TABLE #table3Com_Inv (YourCompanyName  VARCHAR(35), YourCompanyECAMBillTo  VARCHAR(35), " +
                                "YourCompanyBillToName  VARCHAR(35), StateCompany  VARCHAR(35), YourCompanyBilltoZip  VARCHAR(35), YourCompanyBilltoCountryCode  VARCHAR(35), " +
                                "ContactNameCompany  VARCHAR(35), ContactPhone  VARCHAR(35), YourCompanyECAMShipTo  VARCHAR(35), YourCompanyShipToName  VARCHAR(50), " +
                                "StatecompanyShip  VARCHAR(35), YourCompanyShipToZip  VARCHAR(35), YourCompanyShipToCountryCode  VARCHAR(35), " +
                                "UnitPrice  NUMERIC(11,2), ProductNumber VARCHAR(50), CurrencyCode  VARCHAR(35), EffectiveInventoryDate  VARCHAR(35),  " +
                                "QtyonHand  INT, QtyonOrder  INT, QtyStockTransfer  INT, ReportEndDate  VARCHAR(35)) ";

            var s3com_inserttable = "INSERT INTO #table3Com_Inv (ProductNumber, UnitPrice, CurrencyCode, EffectiveInventoryDate, QtyonHand, QtyonOrder, QtyStockTransfer, ReportEndDate) " +
                                "SELECT	DISTINCT Produtos.PartNumber,  " +
                                "dbo.fn_Produto_Custo(ProdutosEmpresa.RelacaoID,GetDate(),541, NULL,1), " +
                                "(SELECT SimboloMoeda FROM Conceitos WITH(NOLOCK) WHERE ConceitoID = 541 ), CONVERT(VARCHAR(8),@dtFim,112), " +
                                "dbo.fn_Produto_Estoque(2,Produtos.ConceitoID,341,NULL,@dtFim,NULL,375, -1) +  " +
                                "dbo.fn_Produto_Estoque(7,Produtos.ConceitoID,341,NULL,@dtFim,NULL,375, -1) + " +
                                "dbo.fn_Produto_Estoque(10,Produtos.ConceitoID,341,NULL,@dtFim,NULL,375, -1) + " +
                                "/*RCC Alcateia-Abano por data*/ " +
                                "dbo.fn_Produto_EstoqueInTransit(2, Produtos.ConceitoID, @dtFim,1,2) + " +
                                "dbo.fn_Produto_EstoqueInTransit(10, Produtos.ConceitoID, @dtFim,1,2) As ONHand, " +
                                "dbo.fn_Produto_EstoqueInTransit(7, Produtos.ConceitoID, @dtFim,1,2) AS [RCC Allplus por data, ONOrder], " +
                                        "dbo.fn_Produto_Estoque(7,Produtos.ConceitoID,343,NULL,@dtFim,NULL,375, -1) AS StockTransfer,	CONVERT(VARCHAR(10),@dtFim,112) " +
                                "FROM	Conceitos Produtos WITH(NOLOCK) " +
                                            "INNER JOIN RelacoesPescon ProdutosEmpresa WITH(NOLOCK) ON Produtos.ConceitoID = ProdutosEmpresa.ObjetoID " +
                                "WHERE	Produtos.TipoConceitoID = 303 AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, @dtFim) NOT IN (1,5,11)) " +
                                            "AND ProdutosEmpresa.TipoRelacaoID = 61	AND ProdutosEmpresa.SujeitoID = 7 AND Produtos.FabricanteID = 15395 ";

            s3com_inserttable += sFiltroFamilia + sReportaFabricante + sFiltro;

            //Só considera o que tem estoque
            var s3com_soestoque = "DELETE #table3Com_Inv WHERE Qtyonhand = 0 AND QtyonOrder = 0 AND QtyStockTransfer = 0 ";

            //Adiciona dados da Allplus
            var s3com_dadosallplus = "SELECT @Nome = a.Nome, @Ecam = \"63253858\",@UF_Abreviado = c.CodigoLocalidade2, @Cep = b.CEP, @Pais = d.CodigoLocalidade2, " +
                                        "@Contato = f.Fantasia, @Telefone = dbo.fn_Pessoa_Telefone(f.PessoaID, 119, 120),@UF = c.Localidade " +
                                    "FROM	Pessoas a WITH(NOLOCK) " +
                                        "INNER JOIN Pessoas_Enderecos b WITH(NOLOCK) ON a.PessoaID = b.PessoaID " +
                                        "INNER JOIN Localidades c WITH(NOLOCK) ON b.UFID = c.LocalidadeID " +
                                        "INNER JOIN Localidades d WITH(NOLOCK) ON b.PaisID = d.LocalidadeID " +
                                        "INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON a.PessoaID = e.SujeitoID " +
                                        "INNER JOIN Pessoas f WITH(NOLOCK) ON e.ProprietarioID = f.PessoaID " +
                                    "WHERE	a.PessoaID = 7 " +
                                        "AND b.EndFaturamento = 1 AND b.Ordem = 1 " +
                                        "AND e.TipoRelacaoID = 21 " +
                                        "AND e.ObjetoID = 15395 /*3Com*/ " +
                                    "UPDATE #table3Com_Inv SET YourCompanyName = @Nome, YourCompanyECAMBillTo = @Ecam, YourCompanyBillToName = @Nome, " +
                                        "StateCompany = @UF_Abreviado, YourCompanyBilltoZip = @Cep, YourCompanyBilltoCountryCode = @Pais, " +
                                        "ContactNameCompany = @Contato, ContactPhone = @Telefone, YourCompanyECAMShipTo = @Ecam, " +
                                        "YourCompanyShipToName = @Nome, StatecompanyShip = @UF,YourCompanyShipToZip = @CEP, " +
                                        "YourCompanyShipToCountryCode = @Pais ";

            var s3com_selecttable = "SELECT YourCompanyName AS [Your Company Name], \"\" AS [Number], YourCompanyECAMBillTo AS [Your Company ECAM Bill To #], " +
                    "YourCompanyBillToName AS [Your Company Bill To Name], \"\" AS [Address 1], \"\" AS [Address 2], \"\" AS [City], " +
                    "StateCompany AS [State], YourCompanyBilltoZip AS [Your Company Bill to Zip], \"\" AS [Country], YourCompanyBilltoCountryCode AS [Your Company Bill to Country Code], " +
                    "ContactNameCompany AS [Contact Name], ContactPhone AS [Contact Phone], YourCompanyECAMShipTo AS [Your Company ECAM Ship To #], YourCompanyShipToName AS [Your Company Ship To Name], " +
                    "\"\" AS [Ship to Address 1], \"\" AS [Ship To Address 2], \"\" AS [City], StatecompanyShip AS [State], " +
                    "YourCompanyShipToZip AS [Your Company Ship To Zip], \"\" AS [Country Name], YourCompanyShipToCountryCode AS [Your Company Ship To Country Code], " +
                    "\"\" AS [Contact Name], \"\" AS [Contact Phone], \"\" AS [Reseller Number], \"\" AS [Reseller Bill to #], " +
                    "\"\" AS [Reseller Bill to Name], \"\" AS [Reseller Bill to Address 1], \"\" AS [Reseller Bill to Address 2], " +
                    "\"\" AS [City], \"\" AS [State], \"\" AS [Reseller Bill to Zip], \"\" AS [Reseller Bill to Country Code], " +
                    "\"\" AS [Country], \"\" AS [Contact Name], \"\" AS [Contact Phone], \"\" AS [Ship to Number], " +
                    "\"\" AS [Reseller Ship to Name], \"\" AS [Ship to Address 1], \"\" AS [Ship to Address 2], \"\" AS [City], " +
                    "\"\" AS [State], \"\" AS [Reseller Ship to Zip], \"\" AS [Reseller Ship to Country Code], \"\" AS [Country], " +
                    "\"\" AS [Contact Name], \"\" AS [Contact Phone], \"\" AS [Sales Region], \"\" AS [Sales Territory], " +
                    "\"\" AS [End-User Bill to #], \"\" AS [End-User Bill To Name], \"\" AS [Bill to Address 1], \"\" AS [Bill to Address 2], " +
                    "\"\" AS [City], \"\" AS [State], \"\" AS [Zip], \"\" AS [Country Code], \"\" AS [Country], " +
                    "\"\" AS [End-User Ship To #], \"\" AS [Ship to Name], \"\" AS [Ship to Address 1], \"\" AS [Ship to Address 2], " +
                    "\"\" AS [City], \"\" AS [State], \"\" AS [Zip], \"\" AS [Country Code], \"\" AS [Country], " +
                    "\"\" AS [Contact Name], \"\" AS [Contact Phone], \"\" AS [SPQ/Promotion ID], \"\" AS [Bundle ID], " +
                    "ProductNumber AS [3COM Product Number], \"\" AS [Customer Product Number],  UnitPrice AS [Unit Price],  CurrencyCode AS [Currency Code], " +
                    "\"\" AS [UOM      1 = EA],  EffectiveInventoryDate AS [Effective Inventory Date],  QtyonHand AS [Qty on Hand],  QtyonOrder AS [Qty on Order], " +
                    "\"\" AS [Qty Committed], \"\" AS [Qty in Float], \"\" AS [Qty on Back Order],  QtyStockTransfer AS [Qty Stock Transfer],  " +
                    "\"\" AS [Ship Date], \"\" AS [Invoice Date],  ReportEndDate AS [Report End Date], \"\" AS [Qty Sold ], \"\" AS [Qty Returned], " +
                    "\"\" AS [Qty Shipped], \"\" AS [Invoice/Credit Note Number] FROM #table3Com_Inv " +
                    "DROP TABLE #table3Com_Inv SET NOCOUNT OFF";

            var s3com_sql = s3com_variavel + s3com_table + s3com_inserttable + s3com_soestoque + s3com_dadosallplus + s3com_selecttable;

            s3com_sql = (s3com_sql == null ? "" : s3com_sql);

        }

        private void relatorioEstoqueNVidia()
        {
            //var dirA1;
            //var dirA2;

            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            string selEmpresa = HttpContext.Current.Request.QueryString["selEmpresa"];
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            string selMarca = HttpContext.Current.Request.QueryString["selMarca"];
            var glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_nEmpresaID"]);
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);

            //string dataCustoEstoque = "";
            //string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            //int TipoCustoEstoque = 0;
            //int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            //int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            //int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            //string selEmpresa = HttpContext.Current.Request.QueryString["selEmpresa"];
            //string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            //string selMarca = HttpContext.Current.Request.QueryString["selMarca"];

            //string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            //string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            //int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            //int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            //int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            //string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            //string selMarca = HttpContext.Current.Request.QueryString["selMarca"];
            //var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            //var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            //var sEstadosCheckeds = HttpContext.Current.Request.QueryString["sEstadosCheckeds"];
            //var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            //var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            //var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            //var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            //var chkTemEstoque = HttpContext.Current.Request.QueryString["chkTemEstoque"];
            //var glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_nEmpresaID"]);
            //var glb_sCurrDateYYYYMMDD = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_sCurrDateYYYYMMDD"]);

            string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            string[] _selMarca = new string[] { };
            string[] _elem_Value = new string[] { };

            string sTmpTableSelect = string.Empty;

            char[] DelimitChar1 = { ':' };


            int i;
            //var sFiltro = "";
            var sFiltroFamilia = "";
            var sFiltroMarca = "";
            var sFiltroEmpresa = "";
            var sFiltroFabricante = "";
            var sReportaFabricante = "";
            var nItensSelected = 0;
            //var dateTimeInSQLFormat = null;
            var sNVidia_variavel = "";
            var sNVidia_table = "";
            var sNVidia_inserttable = "";
            var sNVidia_selecttable = "";
            var sNVidia_sql = "";

            // Analiza o campo txtDataEstoque
            //txtDataEstoque.value = trimStr(txtDataEstoque.value);

            //if ( !((txtDataEstoque.value == null) || (txtDataEstoque.value == '')) )
            //{
            //    if ( criticAndNormTxtDataTime(txtDataEstoque) )
            //    {
            //        dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            //        if (dateTimeInSQLFormat == null)
            //            return null;
            //    }
            //    else
            //        return null;
            //}

            if (dateTimeInSQLFormat == "NULL")
            //dateTimeInSQLFormat = "dbo.fn_Data_Fuso('" + dateTimeInSQLFormat + "', NULL," + glb_nEmpresaCidadeID + ")";
            {
                dateTimeInSQLFormat = "NULL";
            }

            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < selFamilias.Length; i++)
            {

                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + selFamilias[i].ToString();

            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");

            nItensSelected = 0;

            if (selMarca != "")
                _selMarca = selMarca.Split(DelimitChar1);

            for (i = 0; i < _selMarca.Length; i++)
            {

                nItensSelected++;
                sFiltroMarca += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.MarcaID=" + selMarca[i].ToString();

            }

            sFiltroMarca += (nItensSelected > 0 ? ")" : "");

            nItensSelected = 0;

            if (selEmpresa != "")
                _selEmpresa = selEmpresa.Split(DelimitChar1);


            for (i = 0; i < _selEmpresa.Length; i++)
            {

                nItensSelected++;
                sFiltroEmpresa += (nItensSelected == 1 ? " AND (" : " OR ") + "ProdutosEmpresa.SujeitoID=" + selEmpresa[i].ToString();

            }

            sFiltroEmpresa += (nItensSelected > 0 ? ")" : "");


            //sFiltro = trimStr(txtFiltro.value);

            sFiltroFabricante = (selFabricanteValue == 0 ? "" : " AND Produtos.FabricanteID=" + selFabricanteValue + " ");

            //if (sFiltro != "")
            //    sFiltro = " AND (" + sFiltro + ") ";

            if (chkReportaFabricante == "checked")
                sReportaFabricante = " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            //DireitoEspecifico
            //Relacao PesCon->Produtos->Modal Print
            //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao 3Com)
            //A1||A2 -> Impressao
            //A2 -> Zebrar
            //A1&&A2 -> Excel
            //desabilitado-> Word

            sNVidia_variavel = "SET NOCOUNT ON " +
                    "DECLARE @Separador VARCHAR(1), @MarcaID INT, @EmpresaID INT, @dtEstoque DATETIME, @Cotacao NUMERIC(15,7) " +
                    "SET @Separador = CHAR(44) " +
                    "SET @dtEstoque = ISNULL(" + dateTimeInSQLFormat + ",GETDATE()) " +
                    "SET @EmpresaID = " + glb_nEmpresaID + " " +
                    "SET @MarcaID = 3261 " +
                    "SET @Cotacao = dbo.fn_Preco_Cotacao(541, 647, -@EmpresaID, @dtEstoque) ";

            sNVidia_table = "CREATE TABLE #tempTable3 (EmpresaID INT, ReporterID VARCHAR(10), DataEstoque DATETIME, Modelo VARCHAR(18), Descricao VARCHAR(256), " +
                            "Quantidade INT, Fabricante VARCHAR(40), MoedaID INT, CustoAtual NUMERIC(11,2), CustoMedio NUMERIC(11,2),CustomerBackLog INT, " +
                            "POBackLog INT) ";

            sNVidia_inserttable = "INSERT INTO #tempTable3 " +
                    "SELECT	ProdutosEmpresa.SujeitoID, CASE ProdutosEmpresa.SujeitoID WHEN 2 THEN '1242392' WHEN 7 THEN '1368406' END, " +
                        "@dtEstoque, Produtos.Modelo, Produtos.Conceito + SPACE(1) + Produtos.Descricao, " +
                        "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, 343, NULL, @dtEstoque,NULL,375, -1), Fabricantes.Fantasia, 647, " +
                        "CASE ProdutosEmpresa.MoedaEntradaID WHEN 647 THEN ProdutosEmpresa.CustoMedio WHEN 541 THEN ProdutosEmpresa.CustoMedio * @Cotacao END, " +
                        "CASE ProdutosEmpresa.MoedaEntradaID WHEN 647 THEN ProdutosEmpresa.CustoMedio WHEN 541 THEN ProdutosEmpresa.CustoMedio * @Cotacao END, " +
                        "CASE WHEN ProdutosEmpresa.SujeitoID = 7 THEN dbo.fn_Produto_EstoqueInTransit(ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, @dtEstoque, 0,1) ELSE 0 END, " +
                        "CASE WHEN ProdutosEmpresa.SujeitoID = 7 THEN dbo.fn_Produto_EstoqueInTransit(ProdutosEmpresa.SujeitoID, Produtos.ConceitoID, @dtEstoque, 1,1) ELSE 0 END " +
                    "FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) " +
                            "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON (ProdutosEmpresa.ObjetoID = Produtos.ConceitoID) " +
                            "INNER JOIN Pessoas Fabricantes WITH(NOLOCK) ON (Produtos.FabricanteID = Fabricantes.PessoaID) " +
                    "WHERE	(ProdutosEmpresa.TipoRelacaoID = 61) " +
                            " AND (ProdutosEmpresa.EstadoID NOT IN (1,5)) ";

            if (sFiltroEmpresa == "")
                sNVidia_inserttable += " AND (ProdutosEmpresa.SujeitoID = @EmpresaID) ";

            sNVidia_inserttable += sFiltroEmpresa + sFiltroFamilia + sFiltroMarca + sFiltroFabricante + sReportaFabricante + sFiltro;

            sNVidia_selecttable = "/*Header*/ " +
                            "SELECT	'ReporterID' + @Separador + " +
                                    "'Inventory Date' + @Separador +  " +
                                    "'Product Number' + @Separador +  " +
                                    "'Product Description' + @Separador + " +
                                    "'Quantity On Hand' + @Separador +  " +
                                    "'Manufacturer' + @Separador +  " +
                                    "'Current Cost' + @Separador +  " +
                                    "'Average Cost' + @Separador +  " +
                                    "'Currency of Sale' + @Separador +  " +
                                    "'Customer Back Log' + @Separador +  " +
                                    "'PO Back Log' + @Separador AS Registro " +
                            "UNION ALL " +
                            "/*Detalhe*/ " +
                            "SELECT 	a.ReporterID + @Separador + /*[ReporterID]*/ " +
                                    "CONVERT(VARCHAR(4),DATEPART(yyyy, a.DataEstoque)) + " +
                                        "dbo.fn_Pad(CONVERT(VARCHAR(2),DATEPART(mm, a.DataEstoque)), 2, '0', 'L') +  " +
                                        "dbo.fn_Pad(CONVERT(VARCHAR(2),DATEPART(dd, a.DataEstoque)), 2, '0', 'L') + @Separador + /*[Inventory Date]*/ " +
                                    "ISNULL(CONVERT(VARCHAR(10), a.Modelo),SPACE(0)) + @Separador + /*[Product Number]*/ " +
                                    "ISNULL(a.Descricao,SPACE(0)) + @Separador + /*[Product Description]*/ " +
                                    "CONVERT(VARCHAR(10), a.Quantidade) + @Separador + /*[Quantity on Hand]*/ " +
                                    "a.Fabricante + @Separador + /*[Manufacturer]*/ " +
                                    "ISNULL(CONVERT(VARCHAR(13), a.CustoAtual),0) + @Separador + /*[Current Cost]*/ " +
                                    "ISNULL(CONVERT(VARCHAR(13), a.CustoMedio),0) + @Separador + /*[Average Cost]*/ " +
                                    "CASE a.MoedaID " +
                                        "WHEN 541 THEN 'USD' " +
                                        "WHEN 647 THEN 'BRL' END + @Separador + /*[Currency of Sale]*/ " +
                                    "CONVERT(VARCHAR(10), a.CustomerBackLog) + @Separador + /*[Customer Back Log]*/ " +
                                    "CONVERT(VARCHAR(10), a.POBackLog) + @Separador AS Registro /*[PO Back Log]*/ " +
                                "FROM #tempTable3 a " +
                            "DROP TABLE #tempTable3 " +
                            "SET NOCOUNT OFF ";

            sNVidia_sql = sNVidia_variavel + sNVidia_table + sNVidia_inserttable + sNVidia_selecttable;

            sNVidia_sql = (sNVidia_sql == null ? "" : sNVidia_sql);


            string[,] arrQuerys = new string[,] 
            {
                {"Query1", sNVidia_sql}
            };

            Relatorio.CriarRelatório("RelEstPadraoNVidia", arrQuerys, "BRA", "Excel");

            Relatorio.CriarObjTabela("0", "0", "Query1", false, true);

            Relatorio.CriarObjColunaNaTabela("Registro", "Registro", true, "Registro");
            Relatorio.CriarObjCelulaInColuna("Query", "Registro", "DetailGroup");

            Relatorio.TabelaEnd();

            Relatorio.CriarPDF_Excel("RelEstPadraoNVidia", 2);

        }

        private void relatorioEstoqueSeagate()
        {
            //var dirA1;
            //var dirA2;
            int i;

            //var dateTimeInSQLFormat = null;

            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            var chkTemEstoque = HttpContext.Current.Request.QueryString["chkTemEstoque"];
            var glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_nEmpresaID"]);
            var glb_sCurrDateYYYYMMDDhhmmss = HttpContext.Current.Request.QueryString["glb_sCurrDateYYYYMMDDhhmmss"];
            string selEmpresa = HttpContext.Current.Request.QueryString["selEmpresa"];

            string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            string[] _elem_Value = new string[] { };

            string sTmpTableSelect = string.Empty;

            char[] DelimitChar1 = { ':' };

            //var dateTimeInSQLFormat = null;

            //// Analiza o campo txtDataEstoque
            //txtDataEstoque.value = trimStr(txtDataEstoque.value);

            //if ( !((txtDataEstoque.value == null) || (txtDataEstoque.value == '')) )
            //{
            //    if ( criticAndNormTxtDataTime(txtDataEstoque) )
            //    {
            //        dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            //        if (dateTimeInSQLFormat == null)
            //            return null;
            //    }
            //    else
            //        return null;
            //}


            if (dateTimeInSQLFormat == "NULL")
            //dateTimeInSQLFormat = "dbo.fn_Data_Fuso('" + dateTimeInSQLFormat + "', NULL," + glb_nEmpresaCidadeID + ")";
            {
                dateTimeInSQLFormat = "NULL";
            }

            var sFiltroFabricante = (selFabricanteValue == 0 ? "" : " AND Produtos.FabricanteID=" + selFabricanteValue);
            var sFiltroProprietario = "";

            if (selProprietarios2_selectedIndex > 0)
                sFiltroProprietario = " AND ProdutosEmpresa.ProprietarioID = " + selProprietarios2_value + " ";

            var sFiltroFamilia = "";
            var nItensSelected = 0;

            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < _selFamilias.Length; i++)
            {
                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + selFamilias[i].ToString();

            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");

            //DireitoEspecifico
            //Relacao PesCon->Produtos->Modal Print
            //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Seagate)
            //A1||A2 -> Impressao
            //A2 -> Zebrar
            //A1&&A2 -> Excel
            //desabilitado-> Word

            var sFiltroEstados = " ";
            var bFirst = true;
            //var sFiltro = "";
            //var sEstadosCheckeds = "";

            //sFiltro = trimStr(txtFiltro.value);

            //if (sFiltro != "")
            //    sFiltro = " AND (" + sFiltro + ") ";

            if (elem_Value != "")
                _elem_Value = elem_Value.Split(DelimitChar1);

            // loop nos checkbox de estados para montar a string de filtro
            for (i = 0; i < (_elem_Value.Length); i++)
            {

                //for ( i=0; i<(divRelatorioEstoque.children.length); i++ )
                //{
                //    elem = divRelatorioEstoque.children[i];

                //    if ( (elem.tagName).toUpperCase() == "INPUT" )
                //    {
                //        if ( ((elem.type).toUpperCase() == "CHECKBOX") && (elem.checked) &&
                //             (elem.id.toUpperCase() != "CHKLOCALIZACAO") &&
                //             (elem.id.toUpperCase() != "CHKREPORTAFABRICANTE") &&
                //             (elem.id.toUpperCase() != "CHKVALORACAO") &&
                //             (elem.id.toUpperCase() != "CHKCUSTOCONTABIL") &&                 
                //             (elem.id.toUpperCase() != "CHKPRODUTOSEPARAVEL") &&
                //             (elem.id.toUpperCase() != "CHKTEMESTOQUE") &&
                //             (elem.id.toUpperCase() != "CHKEXCEL") &&
                //             (elem.id.indexOf("_ESTOQUE_") < 0) )
                //        {
                if (bFirst)
                {
                    bFirst = false;

                    //sFiltroEstados = " AND (ProdutosEmpresa.EstadoID=" + elem.value + " ";
                    sFiltroEstados = " AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";
                    //sEstadosCheckeds = translateTerm("Estados", null) + ":" + (elem.previousSibling).innerText;
                }
                else
                {
                    // sFiltroEstados += " OR ProdutosEmpresa.EstadoID=" + elem.value + " ";
                    sFiltroEstados += " OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";
                    //sEstadosCheckeds += " " + (elem.previousSibling).innerText;
                }

            }

            if (!bFirst)
                sFiltroEstados += ")";
            else
                sFiltroEstados += " AND (ProdutosEmpresa.EstadoID=0)";

            //var aCmbsEstoques = 
            //    [chk1_ESTOQUE_EPO,
            //     chk1_ESTOQUE_EPE,
            //     chk1_ESTOQUE_EPD,
            //     chk1_ESTOQUE_EPC,
            //     chk1_ESTOQUE_EPT,
            //     chk1_ESTOQUE_ETO,
            //     chk1_ESTOQUE_ETE,
            //     chk1_ESTOQUE_ETD,
            //     chk1_ESTOQUE_ETC];

            string[] aSQLQuantEstoquesDetail = new string[] 
            {
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) "
            };

            //var aDynamicEstoque = 
            //    ["_EPO Numeric(10),",
            //     "_EPE Numeric(10),",
            //     "_EPD Numeric(10),",
            //     "_EPC Numeric(10),",
            //     "_EPT Numeric(10),",
            //     "_ETO Numeric(10),",
            //     "_ETE Numeric(10),",
            //     "_ETD Numeric(10),",
            //     "_ETC Numeric(10),"];

            string sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

            var bHasEstoqueSelected = false;
            var strSumEstoques = "";

            string[] _chkCmbEstoques = chkCmbEstoques.Split(DelimitChar1);

            for (i = 0; i < _chkCmbEstoques.Length; i++)
            {
                if (_chkCmbEstoques[i].ToString() == "checked")
                {
                    if (!bHasEstoqueSelected)
                        strSumEstoques += ", ";
                    else
                        strSumEstoques += " + ";

                    strSumEstoques += aSQLQuantEstoquesDetail[i];
                    bHasEstoqueSelected = true;
                }
            }

            //if (strSumEstoques == "")
            //{
            //    if (window.top.overflyGen.Alert("Selecione pelo menos um estoque.") == 0)
            //        return null;

            //    lockControlsInModalWin(false);
            //    return null;
            //}

            sSQLDetailSelect = "SELECT Empresas.Nome" +
                ", " + "'" + glb_sCurrDateYYYYMMDDhhmmss + "'" +
                ", Empresas.PessoaID " +
                ", " + "'" + "OH" + "'" +
                ", Paises.CodigoLocalidade2 " +
                ", CONVERT(VARCHAR(8), " + dateTimeInSQLFormat + ", 112) " +
                ", Produtos.Modelo " +
                strSumEstoques + " " +
                ", ISNULL((SELECT SUM(Itens.Quantidade) FROM Pedidos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK), Operacoes Transacoes WITH(NOLOCK) " +
                    "WHERE (Pedidos.EmpresaID = ProdutosEmpresa.SujeitoID AND Pedidos.PedidoID=Itens.PedidoID AND " +
                    "Pedidos.TipoPedidoID = 601 AND Pedidos.TransacaoID=Transacoes.OperacaoID AND " +
                    "Transacoes.MetodoCustoID=372 AND " +
                    "Pedidos.EstadoID BETWEEN 23 AND 28 AND Itens.ProdutoID=Produtos.ConceitoID)), 0) AS [In-transit to Location Code] " + " ";

            sSQLDetailFrom = "FROM " +
                    "Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), " +
                    "RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), " +
                    "RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Recursos Estados WITH(NOLOCK), Recursos Sistema WITH(NOLOCK), Pessoas Empresas WITH(NOLOCK), " +
                    "Pessoas_Enderecos Enderecos WITH(NOLOCK), Localidades Paises WITH(NOLOCK) ";

            sSQLDetailWhere1 = "WHERE (" +
                    "(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND " +
                    "(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND " +
                    "RelConceitos1.EstadoID=2) AND " +
                    "(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND " +
                    "Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND " +
                    "(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND " +
                    "RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND " +
                    "(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ";

            var sFiltroEmpresa = "";
            nItensSelected = 0;

            if (selEmpresa != "")
                _selEmpresa = selEmpresa.Split(DelimitChar1);


            for (i = 0; i < _selEmpresa.Length; i++)
            {

                {
                    nItensSelected++;
                    sFiltroEmpresa += (nItensSelected == 1 ? " AND (" : " OR ") + "ProdutosEmpresa.SujeitoID=" + _selEmpresa[i].ToString();
                }
            }

            sFiltroEmpresa += (nItensSelected > 0 ? ")" : "");

            //if (sFiltroEmpresa == "")
            //{
            //    if ( window.top.overflyGen.Alert("Selecione uma empresa") == 0 )
            //        return null;

            //    lockControlsInModalWin(false);
            //    return null;    
            //}

            sSQLDetailWhere2 =
                    "Conceitos3.EstadoID=2) AND " +
                    "(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND " +
                    "(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 " +
                    sFiltroEmpresa + sFiltroProprietario + " AND ProdutosEmpresa.SujeitoID=Empresas.PessoaID AND " +
                    "Empresas.PessoaID = Enderecos.PessoaID AND Enderecos.Ordem = 1 AND Enderecos.PaisID = Paises.LocalidadeID AND " +
                    "ProdutosEmpresa.EstadoID=Estados.RecursoID AND Sistema.RecursoID=999) AND " +
                    "(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) ";

            if (chkProdutoSeparavel == "checked")
                sSQLDetailWhere2 += " AND Produtos.ProdutoSeparavel = 1 ";

            if (chkReportaFabricante == "checked")
                sSQLDetailWhere2 += " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            sSQLDetailWhere2 += sFiltroFabricante + sFiltroFamilia + sFiltroEstados + " " + sFiltro + ") ";

            sSQLDetailGroupBy = "";

            sSQLDetailOrderBy = "";

            sTmpTableSelect =
                "CREATE TABLE #TempTable " +
                "( " +
                "IDTMP_ INTEGER IDENTITY, " +
                "[Distributor_Name] VARCHAR(80), " +
                "[Report_Date_Time] VARCHAR(14), " +
                "[Distributor_Location_Identifier] VARCHAR(2), " +
                "[Inventory_Type_Code] VARCHAR(2), " +
                "[Country_Code] VARCHAR(2), " +
                "[Inventory_Date] VARCHAR(8), " +
                "[Seagate_Model_Number] VARCHAR(18), " +
                "[Quantity] VARCHAR(7), " +
                "[In_transit_to_Location_Code] VARCHAR(8)) ";

            sTmpTableSelect += "INSERT INTO #TempTable ([Distributor_Name],[Report_Date_Time],[Distributor_Location_Identifier]," +
                "[Inventory_Type_Code],[Country_Code],[Inventory_Date],[Seagate_Model_Number]," +
                "[Quantity],[In_transit_to_Location_Code]) " +
                sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailWhere2 + sSQLDetailGroupBy + sSQLDetailOrderBy + " ";

            if (chkTemEstoque == "checked")
                sTmpTableSelect += " DELETE FROM #TempTable WHERE (([Quantity] = 0) AND ([In_transit_to_Location_Code] = 0)) ";

            sTmpTableSelect += "SELECT * FROM #TempTable ORDER BY [Distributor_Location_Identifier], [Seagate_Model_Number] ";

            sTmpTableSelect += "DROP TABLE #TempTable";

            var strSQLParsed = "EXEC sp_RelatorioEstoque " + "'" + dupCharInString(sTmpTableSelect, "'") + "'";

            strSQLParsed = (strSQLParsed == null ? "" : strSQLParsed);

            string[,] arrQuerys = new string[,] 
            {
                {"Query1", strSQLParsed}
            };

            Relatorio.CriarRelatório("RelEstPadraoSeagate", arrQuerys, "BRA", "Excel");

            Relatorio.CriarObjTabela("0", "0", "Query1", false, true);

            Relatorio.CriarObjColunaNaTabela("Distributor Name", "Distributor_Name", true, "Distributor_Name");
            Relatorio.CriarObjCelulaInColuna("Query", "Distributor_Name", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Report Date /Time", "Report_Date_Time", true, "Report_Date_Time");
            Relatorio.CriarObjCelulaInColuna("Query", "Report_Date_Time", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Distributor Location Identifier", "Distributor_Location_Identifier", true, "Distributor_Location_Identifier");
            Relatorio.CriarObjCelulaInColuna("Query", "Distributor_Location_Identifier", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Inventory Type Code", "Inventory_Type_Code", true, "Inventory_Type_Code");
            Relatorio.CriarObjCelulaInColuna("Query", "Inventory_Type_Code", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Country Code", "Country_Code", true, "Country_Code");
            Relatorio.CriarObjCelulaInColuna("Query", "Country_Code", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Inventory Date", "Inventory_Date", true, "Inventory_Date");
            Relatorio.CriarObjCelulaInColuna("Query", "Inventory_Date", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Seagate Model Number", "Seagate_Model_Number", true, "Seagate_Model_Number");
            Relatorio.CriarObjCelulaInColuna("Query", "Seagate_Model_Number", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Quantity", "Quantity", true, "Quantity");
            Relatorio.CriarObjCelulaInColuna("Query", "Quantity", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("In-transit to Location Code", "In_transit_to_Location_Code", true, "In_transit_to_Location_Code");
            Relatorio.CriarObjCelulaInColuna("Query", "In_transit_to_Location_Code", "DetailGroup");

            Relatorio.TabelaEnd();

            Relatorio.CriarPDF_Excel("RelEstoquePadraoSeagate", 2);

        }

        private void relatorioEstoqueKingston()
        {
            //var dirA1;
            //var dirA2;
            //var i;
            //var dateTimeInSQLFormat = null;

            int i;
            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            string dataCustoEstoque = "";
            string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            int TipoCustoEstoque = 0;
            int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            string selEmpresa = HttpContext.Current.Request.QueryString["selEmpresa"];
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            string selMarca = HttpContext.Current.Request.QueryString["selMarca"];
            var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            var glb_nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.QueryString["glb_nEmpresaID"]);
            var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            var chkTemEstoque = HttpContext.Current.Request.QueryString["chkTemEstoque"];
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];

            string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            string[] _elem_Value = new string[] { };

            string sTmpTableSelect = string.Empty;

            char[] DelimitChar1 = { ':' };

            // Analiza o campo txtDataEstoque
            //txtDataEstoque.value = trimStr(txtDataEstoque.value);

            //if ( !((txtDataEstoque.value == null) || (txtDataEstoque.value == "")) )
            //{
            //    if ( criticAndNormTxtDataTime(txtDataEstoque) )
            //    {
            //        dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            //        if (dateTimeInSQLFormat == null)
            //            return null;
            //    }
            //    else
            //        return null;
            //}


            if (dateTimeInSQLFormat == "NULL")
            //dateTimeInSQLFormat = "dbo.fn_Data_Fuso('" + dateTimeInSQLFormat + "', NULL," + glb_nEmpresaCidadeID + ")";
            {
                dateTimeInSQLFormat = "NULL";
            }

            var sFiltroFabricante = (selFabricanteValue == 0 ? "" : " AND Produtos.FabricanteID=" + selFabricanteValue);
            var sFiltroProprietario = "";

            if (selProprietarios2_selectedIndex > 0)
                sFiltroProprietario = " AND ProdutosEmpresa.ProprietarioID = " + selProprietarios2_value + " ";

            var sFiltroFamilia = "";
            var nItensSelected = 0;


            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < _selFamilias.Length; i++)
            {
                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + _selFamilias[i].ToString();

            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");


            //DireitoEspecifico
            //Relacao PesCon->Produtos->Modal Print
            //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Kingston)
            //A1||A2 -> Impressao
            //A2 -> Zebrar
            //A1&&A2 -> Excel
            //desabilitado-> Word

            var sFiltroEstados = " ";
            var bFirst = true;
            //var sFiltro = "";
            var sEstadosCheckeds = "";

            //sFiltro = trimStr(txtFiltro.value);

            //if (sFiltro != "")
            //    sFiltro = " AND (" + sFiltro + ") ";

            if (elem_Value != "")
                _elem_Value = elem_Value.Split(DelimitChar1);

            // loop nos checkbox de estados para montar a string de filtro
            for (i = 0; i < (_elem_Value.Length); i++)
            {

                // loop nos checkbox de estados para montar a string de filtro
                //for ( i=0; i<(divRelatorioEstoque.children.length); i++ )
                //{
                //    elem = divRelatorioEstoque.children[i];

                //    if ( (elem.tagName).toUpperCase() == "INPUT" )
                //    {
                //        if ( ((elem.type).toUpperCase() == "CHECKBOX") && (elem.checked) &&
                //             (elem.id.toUpperCase() != "CHKLOCALIZACAO") &&
                //             (elem.id.toUpperCase() != "CHKREPORTAFABRICANTE") &&
                //             (elem.id.toUpperCase() != "CHKVALORACAO") &&
                //             (elem.id.toUpperCase() != "CHKCUSTOCONTABIL") &&                 
                //             (elem.id.toUpperCase() != "CHKPRODUTOSEPARAVEL") &&
                //             (elem.id.toUpperCase() != "CHKTEMESTOQUE") &&
                //             (elem.id.toUpperCase() != "CHKEXCEL") &&
                //             (elem.id.indexOf("_ESTOQUE_") < 0) )
                //        {
                if (bFirst)
                {
                    bFirst = false;

                    //sFiltroEstados = " AND (ProdutosEmpresa.EstadoID=" + elem.value + " ";
                    sFiltroEstados = " AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";
                    //sEstadosCheckeds = translateTerm("Estados", null) + ":" + (elem.previousSibling).innerText;
                }
                else
                {
                    // sFiltroEstados += " OR ProdutosEmpresa.EstadoID=" + elem.value + " ";
                    sFiltroEstados += " OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";
                    //sEstadosCheckeds += " " + (elem.previousSibling).innerText;
                }

            }

            if (!bFirst)
                sFiltroEstados += ")";
            else
                sFiltroEstados += " AND (ProdutosEmpresa.EstadoID=0)";

            //var aCmbsEstoques = 
            //    [chk1_ESTOQUE_EPO,
            //     chk1_ESTOQUE_EPE,
            //     chk1_ESTOQUE_EPD,
            //     chk1_ESTOQUE_EPC,
            //     chk1_ESTOQUE_EPT,
            //     chk1_ESTOQUE_ETO,
            //     chk1_ESTOQUE_ETE,
            //     chk1_ESTOQUE_ETD,
            //     chk1_ESTOQUE_ETC];

            string[] aSQLQuantEstoquesDetail = new string[]
            {
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) "
            };

            //var aDynamicEstoque = 
            //    ["_EPO Numeric(10),",
            //     "_EPE Numeric(10),",
            //     "_EPD Numeric(10),",
            //     "_EPC Numeric(10),",
            //     "_EPT Numeric(10),",
            //     "_ETO Numeric(10),",
            //     "_ETE Numeric(10),",
            //     "_ETD Numeric(10),",
            //     "_ETC Numeric(10),"];

            string sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

            var bHasEstoqueSelected = false;
            var strSumEstoques = "";

            string[] _chkCmbEstoques = chkCmbEstoques.Split(DelimitChar1);

            for (i = 0; i < _chkCmbEstoques.Length; i++)
            {
                if (_chkCmbEstoques[i] == "checked")
                {
                    if (!bHasEstoqueSelected)
                        strSumEstoques += ", ";
                    else
                        strSumEstoques += " + ";

                    strSumEstoques += aSQLQuantEstoquesDetail[i];
                    bHasEstoqueSelected = true;
                }
            }

            //if (strSumEstoques == "")
            //{
            //    if (window.top.overflyGen.Alert("Selecione pelo menos um estoque.") == 0)
            //        return null;

            //    lockControlsInModalWin(false);
            //    return null;
            //}

            sSQLDetailSelect = "SELECT Produtos.PartNumber " +
                strSumEstoques + ", dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,354," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS committed, " +
                "SPACE(0) AS [on order], " +
                "CONVERT(INTEGER, dbo.fn_Produto_Totaliza(" + glb_nEmpresaID + ", ProdutosEmpresa.ObjetoID, " +
                    "dbo.fn_Data_Dia(GETDATE(), 3, 0, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, 0, 0, 0), 1)) AS [sales-month to date], " +
                "CONVERT(INTEGER, dbo.fn_Produto_Totaliza(" + glb_nEmpresaID + ", ProdutosEmpresa.ObjetoID, " +
                    "dbo.fn_Data_Dia(GETDATE(), 3, -1, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -1, 0, 0), 1)) AS [sales-1 month ago], " +
                "CONVERT(INTEGER, dbo.fn_Produto_Totaliza(" + glb_nEmpresaID + ", ProdutosEmpresa.ObjetoID, " +
                    "dbo.fn_Data_Dia(GETDATE(), 3, -2, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -2, 0, 0), 1)) AS [sales-2 month ago], " +
                "CONVERT(INTEGER, dbo.fn_Produto_Totaliza(" + glb_nEmpresaID + ", ProdutosEmpresa.ObjetoID, " +
                    "dbo.fn_Data_Dia(GETDATE(), 3, -3, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -3, 0, 0), 1)) AS [sales-3 month ago] ";

            /*
                    "CONVERT(INTEGER, dbo.fn_Produto_Totaliza(" + glb_nEmpresaID + ", ProdutosEmpresa.ObjetoID, dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3,  0, 1, 0)), dbo.fn_Data_Zero(GETDATE()), 1)) AS [sales-month to date]," +
                    "CONVERT(INTEGER, dbo.fn_Produto_Totaliza(" + glb_nEmpresaID + ", ProdutosEmpresa.ObjetoID, dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -1, 1, 0)), dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -1, 0, 0)), 1)) AS [sales-1 month ago]," +
                    "CONVERT(INTEGER, dbo.fn_Produto_Totaliza(" + glb_nEmpresaID + ", ProdutosEmpresa.ObjetoID, dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -2, 1, 0)), dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -2, 0, 0)), 1)) AS [sales-2 month ago]," +
                    "CONVERT(INTEGER, dbo.fn_Produto_Totaliza(" + glb_nEmpresaID + ", ProdutosEmpresa.ObjetoID, dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -3, 1, 0)), dbo.fn_Data_Zero(dbo.fn_Data_Dia(GETDATE(), 3, -3, 0, 0)), 1)) AS [sales-3 month ago] ";
            */

            sSQLDetailFrom = "FROM " +
                    "Conceitos Conceitos1 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), " +
                    "RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos3 WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), Conceitos Marcas WITH(NOLOCK), " +
                    "RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Recursos Sistema WITH(NOLOCK) ";

            sSQLDetailWhere1 = "WHERE (" +
                    "(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND " +
                    "(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND " +
                    "RelConceitos1.EstadoID=2) AND " +
                    "(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND " +
                    "Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND " +
                    "(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND " +
                    "RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND " +
                    "(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ";

            var sFiltroEmpresa = "";
            nItensSelected = 0;

            if (selEmpresa != "")
                _selEmpresa = selEmpresa.Split(DelimitChar1);


            for (i = 0; i < _selEmpresa.Length; i++)
            {
                nItensSelected++;
                sFiltroEmpresa += (nItensSelected == 1 ? " AND (" : " OR ") + "ProdutosEmpresa.SujeitoID=" + _selEmpresa[i].ToString();

            }

            sFiltroEmpresa += (nItensSelected > 0 ? ")" : "");

            //if (sFiltroEmpresa == "")
            //{
            //    if ( window.top.overflyGen.Alert("Selecione uma empresa") == 0 )
            //        return null;

            //    lockControlsInModalWin(false);
            //    return null;    
            //}

            sSQLDetailWhere2 =
                    "Conceitos3.EstadoID=2) AND " +
                    "(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND " +
                    "(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 " +
                    sFiltroEmpresa + sFiltroProprietario + " AND Sistema.RecursoID=999) AND " +
                    "(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) ";

            if (chkProdutoSeparavel == "checked")
                sSQLDetailWhere2 += " AND Produtos.ProdutoSeparavel = 1 ";

            if (chkReportaFabricante == "checked")
                sSQLDetailWhere2 += " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            sSQLDetailWhere2 += sFiltroFabricante + sFiltroFamilia + sFiltroEstados + " " + sFiltro + ") ";

            sSQLDetailGroupBy = "";

            sSQLDetailOrderBy = "";

            sTmpTableSelect =
                "CREATE TABLE #TempTable " +
                "( " +
                "IDTMP_ INTEGER IDENTITY, " +
                "[part_number] VARCHAR(20), " +
                "[stock_on_hand] VARCHAR(7), " +
                "[committed] VARCHAR(7), " +
                "[on_order] VARCHAR(1), " +
                "[sales_month_to_date] VARCHAR(7), " +
                "[sales_1_month_ago] VARCHAR(7), " +
                "[sales_2_month_ago] VARCHAR(7), " +
                "[sales_3_month_ago] VARCHAR(7)) ";

            sTmpTableSelect += "INSERT INTO #TempTable ([part_number],[stock_on_hand],[committed]," +
                "[on_order],[sales_month_to_date],[sales_1_month_ago],[sales_2_month_ago],[sales_3_month_ago]) " +
                sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailWhere2 + sSQLDetailGroupBy + sSQLDetailOrderBy + " ";

            sTmpTableSelect += "SELECT * FROM #TempTable ORDER BY [part_number] ";

            sTmpTableSelect += "DROP TABLE #TempTable";

            var strSQLParsed = "EXEC sp_RelatorioEstoque " + "'" + dupCharInString(sTmpTableSelect, "'") + "'";

            strSQLParsed = (strSQLParsed == null ? "" : strSQLParsed);

            string[,] arrQuerys = new string[,] 
            {
                {"Query1", strSQLParsed}
            };

            Relatorio.CriarRelatório("RelEstPadraoKingston", arrQuerys, "BRA", "Excel");

            Relatorio.CriarObjTabela("0", "0", "Query1", false, false, false);

            Relatorio.CriarObjColunaNaTabela("part number", "part_number", true, "part_number");
            Relatorio.CriarObjCelulaInColuna("Query", "part_number", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("stock on hand", "stock_on_hand", true, "stock_on_hand");
            Relatorio.CriarObjCelulaInColuna("Query", "stock_on_hand", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("committed", "committed", true, "committed");
            Relatorio.CriarObjCelulaInColuna("Query", "committed", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("on order", "on_order", true, "on_order");
            Relatorio.CriarObjCelulaInColuna("Query", "on_order", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("sales- month to date", "sales_month_to_date", true, "sales_month_to_date");
            Relatorio.CriarObjCelulaInColuna("Query", "sales_month_to_date", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("sales- 1 month ago", "sales_1_month_ago", true, "sales_1_month_ago");
            Relatorio.CriarObjCelulaInColuna("Query", "sales_1_month_ago", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("sales- 2 month ago", "sales_2_month_ago", true, "sales_2_month_ago");
            Relatorio.CriarObjCelulaInColuna("Query", "sales_2_month_ago", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("sales- 3 month ago", "sales_3_month_ago", true, "sales_3_month_ago");
            Relatorio.CriarObjCelulaInColuna("Query", "sales_3_month_ago", "DetailGroup");

            Relatorio.TabelaEnd();

            Relatorio.CriarPDF_Excel("RelEstoquePadraoKingston", 2);
        }

        private void relatorioEstoqueHP()
        {
            //var dirA1;Frelatorio
            //var dirA2;

            int i;
            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            string dataCustoEstoque = "";
            string glb_nEmpresaCidadeID = HttpContext.Current.Request.QueryString["glb_nEmpresaCidadeID"];
            int TipoCustoEstoque = 0;
            int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            string selEmpresa = HttpContext.Current.Request.QueryString["selEmpresa"];
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            string selMarca = HttpContext.Current.Request.QueryString["selMarca"];
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            var DATE_SQL_PARAM = HttpContext.Current.Request.QueryString["DATE_SQL_PARAM"];

            string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            string[] _selMarca = new string[] { };
            string[] _elem_Value = new string[] { };

            //string sTmpTableSelect = string.Empty;

            char[] DelimitChar1 = { ':' };

            var sFiltroEstados = " ";
            var bFirst = true;
            //var sFiltro = "";
            var sEstadosCheckeds = "";
            var sFiltroFabricante = "";
            var sFiltroProprietario = "";
            var sFiltroFamilia = "";
            var nItensSelected = 0;
            var sFiltroEmpresa = "";
            var sFiltroMarca = "";

            string sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailGroupBy, sSQLDetailOrderBy;

            //var dateTimeInSQLFormat = null;

            // Analiza o campo txtDataEstoque
            //txtDataEstoque.value = trimStr(txtDataEstoque.value);

            //if ( !((txtDataEstoque.value == null) || (txtDataEstoque.value == "")) )
            //{
            //    if ( criticAndNormTxtDataTime(txtDataEstoque) )
            //    {
            //        dateTimeInSQLFormat = normalizeDate_DateTime(txtDataEstoque.value, true);

            //        if (dateTimeInSQLFormat == null)
            //            return null;
            //    }
            //    else
            //        return null;
            //}


            if (dateTimeInSQLFormat == "NULL")
            //dateTimeInSQLFormat = "dbo.fn_Data_Fuso('" + dateTimeInSQLFormat + "', NULL," + glb_nEmpresaCidadeID + ")";
            {
                dateTimeInSQLFormat = "NULL";
            }

            sFiltroFabricante = (selFabricanteValue == 0 ? "" : " AND Produtos.FabricanteID=" + selFabricanteValue);
            sFiltroProprietario = "";

            if (selProprietarios2_selectedIndex > 0)
                sFiltroProprietario = " AND ProdutosEmpresa.ProprietarioID = " + selProprietarios2_value + " ";

            sFiltroFamilia = "";
            nItensSelected = 0;

            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < _selFamilias.Length; i++)
            {
                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + _selFamilias[i].ToString();

            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");

            sFiltroMarca = "";
            nItensSelected = 0;

            if (selMarca != "")
                _selMarca = selMarca.Split(DelimitChar1);

            for (i = 0; i < _selMarca.Length; i++)
            {
                nItensSelected++;
                sFiltroMarca += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.MarcaID=" + _selMarca[i].ToString();

            }

            sFiltroMarca += (nItensSelected > 0 ? ")" : "");

            sFiltroEstados = " ";
            bFirst = true;
            sFiltro = "";
            sEstadosCheckeds = "";

            //sFiltro = trimStr(txtFiltro.value);

            //if (sFiltro != "")
            //    sFiltro = " AND (" + sFiltro + ") ";

            if (elem_Value != "")
                _elem_Value = elem_Value.Split(DelimitChar1);

            // loop nos checkbox de estados para montar a string de filtro
            for (i = 0; i < (_elem_Value.Length); i++)
            {


                // loop nos checkbox de estados para montar a string de filtro
                //for ( i=0; i<(divRelatorioEstoque.children.length); i++ )
                //{
                //    elem = divRelatorioEstoque.children[i];

                //    if ( (elem.tagName).toUpperCase() == "INPUT" )
                //    {
                //        if ( ((elem.type).toUpperCase() == "CHECKBOX") && (elem.checked) &&
                //             (elem.id.toUpperCase() != "CHKLOCALIZACAO") &&
                //             (elem.id.toUpperCase() != "CHKREPORTAFABRICANTE") &&
                //             (elem.id.toUpperCase() != "CHKVALORACAO") &&
                //             (elem.id.toUpperCase() != "CHKCUSTOCONTABIL") &&                 
                //             (elem.id.toUpperCase() != "CHKPRODUTOSEPARAVEL") &&
                //             (elem.id.toUpperCase() != "CHKTEMESTOQUE") &&
                //             (elem.id.toUpperCase() != "CHKEXCEL") &&
                //             (elem.id.indexOf("_ESTOQUE_") < 0) )
                //        {
                if (bFirst)
                {
                    bFirst = false;

                    sFiltroEstados = " AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";
                    //sEstadosCheckeds = translateTerm("Estados", null) + ":" + (elem.previousSibling).innerText;
                }
                else
                {
                    sFiltroEstados += " OR dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";
                    //sEstadosCheckeds += " " + (elem.previousSibling).innerText;
                }

            }

            if (!bFirst)
                sFiltroEstados += ")";
            else
                sFiltroEstados += " AND (ProdutosEmpresa.EstadoID=0)";

            //DireitoEspecifico
            //Relacao PesCon->Produtos->Modal Print
            //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Seagate)
            //A1||A2 -> Impressao
            //A2 -> Zebrar
            //A1&&A2 -> Excel
            //desabilitado-> Word

            //var aCmbsEstoques = 
            //    [chk1_ESTOQUE_EPO,
            //     chk1_ESTOQUE_EPE,
            //     chk1_ESTOQUE_EPD,
            //     chk1_ESTOQUE_EPC,
            //     chk1_ESTOQUE_EPT,
            //     chk1_ESTOQUE_ETO,
            //     chk1_ESTOQUE_ETE,
            //     chk1_ESTOQUE_ETD,
            //     chk1_ESTOQUE_ETC];

            string[] aSQLQuantEstoquesDetail = new string[]
            {
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,341," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,342," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,343," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,344," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,345," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,346," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,347," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,348," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) ",
                "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,Produtos.ConceitoID,349," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) "
            };

            //var aDynamicEstoque =    
            //    ["_EPO Numeric(10),",
            //     "_EPE Numeric(10),",
            //     "_EPD Numeric(10),",
            //     "_EPC Numeric(10),",
            //     "_EPT Numeric(10),",
            //     "_ETO Numeric(10),",
            //     "_ETE Numeric(10),",
            //     "_ETD Numeric(10),",
            //     "_ETC Numeric(10),"];    

            var bHasEstoqueSelected = false;
            var strSumEstoques = "";


            string[] _chkCmbEstoques = chkCmbEstoques.Split(DelimitChar1);

            for (i = 0; i < _chkCmbEstoques.Length; i++)
            {
                if (_chkCmbEstoques[i] == "checked")
                {
                    if (!bHasEstoqueSelected)
                        strSumEstoques += ", ";
                    else
                        strSumEstoques += " + ";

                    strSumEstoques += aSQLQuantEstoquesDetail[i];
                    bHasEstoqueSelected = true;
                }
            }

            //if (strSumEstoques == "")
            //{
            //    if (window.top.overflyGen.Alert("Selecione pelo menos um estoque.") == 0)
            //        return null;

            //    lockControlsInModalWin(false);
            //    return null;
            //}

            sSQLDetailSelect = "SELECT '''' AS Circular " +
                ",Produtos.Modelo AS [Descrição] " +
                ",Produtos.PartNumber AS [Cód_HP] " +
                ",0 AS Rebate" +
                strSumEstoques + " AS [Qtde] " +
                ",0 AS [Rebate_Total] " +
                ",'''' AS PL " +
                ",'''' AS [ID_OPG] " +
                ", CONVERT(VARCHAR(10), ISNULL(" + dateTimeInSQLFormat + ",GETDATE()), " + DATE_SQL_PARAM + ") AS [Data_de_estoque] ";

            sSQLDetailFrom = "FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) " +
                                "INNER JOIN Conceitos Produtos WITH(NOLOCK) ON ProdutosEmpresa.ObjetoID = Produtos.ConceitoID " +
                                "INNER JOIN Conceitos Familias WITH(NOLOCK) ON Produtos.ProdutoID = Familias.ConceitoID " +
                                "INNER JOIN Conceitos Marcas WITH(NOLOCK) ON Marcas.ConceitoID=Produtos.MarcaID ";

            sSQLDetailWhere1 = "WHERE (ProdutosEmpresa.TipoRelacaoID=61) AND " +
                                "(Produtos.EstadoID = 2 AND Familias.EstadoID = 2 AND Marcas.EstadoID = 2) ";

            if (chkProdutoSeparavel == "checked")
                sSQLDetailWhere1 += " AND (Produtos.ProdutoSeparavel = 1) ";

            if (chkReportaFabricante == "checked")
                sSQLDetailWhere1 += " AND (ProdutosEmpresa.ReportaFabricante = 1) ";

            sFiltroEmpresa = "";
            nItensSelected = 0;

            if (selEmpresa != "")
                _selEmpresa = selEmpresa.Split(DelimitChar1);


            for (i = 0; i < _selEmpresa.Length; i++)
            {
                nItensSelected++;
                sFiltroEmpresa += (nItensSelected == 1 ? " AND (" : " OR ") + "ProdutosEmpresa.SujeitoID=" + _selEmpresa[i].ToString();

            }

            sFiltroEmpresa += (nItensSelected > 0 ? ")" : "");

            //if (sFiltroEmpresa == "")
            //{
            //    if ( window.top.overflyGen.Alert("Selecione uma empresa") == 0 )
            //        return null;

            //    lockControlsInModalWin(false);
            //    return null;    
            //}

            sSQLDetailWhere2 = sFiltroEmpresa + sFiltroProprietario + sFiltroFabricante + sFiltroFamilia + sFiltroMarca + sFiltroEstados + " " + sFiltro;

            var strSQLParsed = sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailWhere2;

            var sTmpTableSelect =
                "CREATE TABLE #TempTable " +
                "( " +
                "Circular VARCHAR(1)," +
                "[Descrição] VARCHAR(64)," +
                "[Cód_HP] VARCHAR(64)," +
                "[Rebate] VARCHAR(1)," +
                "[Qtde] NUMERIC(10)," +
                "[Rebate_Total] VARCHAR(1), " +
                "[PL] VARCHAR(1)," +
                "[ID_OPG] VARCHAR(1)," +
                "[Data_de_Estoque] VARCHAR(11)) ";

            sTmpTableSelect += "INSERT INTO #TempTable  " + strSQLParsed;

            sTmpTableSelect += "SELECT * FROM #TempTable WHERE [Qtde] > 0 ORDER BY [Descrição]";
            sTmpTableSelect += "DROP TABLE #TempTable";

            strSQLParsed = "EXEC sp_RelatorioEstoque " + "'" + dupCharInString(sTmpTableSelect, "'") + "'";

            strSQLParsed = (strSQLParsed == null ? "" : strSQLParsed);

            string[,] arrQuerys = new string[,] 
            {
                {"Query1", strSQLParsed}
            };

            Relatorio.CriarRelatório("RelEstPadraoHP", arrQuerys, "BRA", "Excel");

            Relatorio.CriarObjTabela("0", "0", "Query1", false, true);

            Relatorio.CriarObjColunaNaTabela("Circular", "Circular", true, "Circular");
            Relatorio.CriarObjCelulaInColuna("Query", "Circular", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Descrição", "Descrição", true, "Descrição");
            Relatorio.CriarObjCelulaInColuna("Query", "Descrição", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Cód HP", "Cód_HP", true, "Cód_HP");
            Relatorio.CriarObjCelulaInColuna("Query", "Cód_HP", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Rebate", "Rebate", true, "Rebate");
            Relatorio.CriarObjCelulaInColuna("Query", "Rebate", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Qtde.", "Qtde", true, "Qtde");
            Relatorio.CriarObjCelulaInColuna("Query", "Qtde", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Rebate Total", "Rebate_Total", true, "Rebate_Total");
            Relatorio.CriarObjCelulaInColuna("Query", "Rebate_Total", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("PL", "PL", true, "PL");
            Relatorio.CriarObjCelulaInColuna("Query", "PL", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("ID/OPG", "ID_OPG", true, "ID_OPG");
            Relatorio.CriarObjCelulaInColuna("Query", "ID_OPG", "DetailGroup");

            Relatorio.CriarObjColunaNaTabela("Data de Estoque", "Data_de_Estoque", true, "Data_de_Estoque");
            Relatorio.CriarObjCelulaInColuna("Query", "Data_de_Estoque", "DetailGroup");

            Relatorio.TabelaEnd();

            Relatorio.CriarPDF_Excel("RelEstoquePadraoHP", 2);

        }

        private void relatorioSPED()
        {
            int i;
            string dateTimeInSQLFormat = HttpContext.Current.Request.QueryString["dateTimeInSQLFormat"];
            int selFabricanteValue = Convert.ToInt32(HttpContext.Current.Request.QueryString["selFabricante"]);
            int selProprietarios2_selectedIndex = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Index"]);
            int selProprietarios2_value = Convert.ToInt32(HttpContext.Current.Request.QueryString["selProprietarios2Value"]);
            string selEmpresa = HttpContext.Current.Request.QueryString["selEmpresa"];
            string selFamilias = HttpContext.Current.Request.QueryString["selFamilias"];
            string selMarca = HttpContext.Current.Request.QueryString["selMarca"];
            var sFiltro = HttpContext.Current.Request.QueryString["sFiltro"];
            var elem_Value = HttpContext.Current.Request.QueryString["elem"];
            var glb_DepositoSelecionado = HttpContext.Current.Request.QueryString["glb_DepositoSelecionado"];
            var chkCmbEstoques = HttpContext.Current.Request.QueryString["chkCmbEstoques"];
            var chkProdutoSeparavel = HttpContext.Current.Request.QueryString["chkProdutoSeparavel"];
            var chkReportaFabricante = HttpContext.Current.Request.QueryString["chkReportaFabricante"];
            var chkValoracao = HttpContext.Current.Request.QueryString["chkValoracao"];
            var chkTemEstoque = HttpContext.Current.Request.QueryString["chkTemEstoque"];
            var glb_sEmpresaFantasia = HttpContext.Current.Request.QueryString["glb_sEmpresaFantasia"];
            string PeriodoSolcitado = HttpContext.Current.Request.QueryString["glb_PeriodoSolicitado"];

            string[] _selEmpresa = new string[] { };
            string[] _selFamilias = new string[] { };
            string[] _selMarca = new string[] { };
            string[] _elem_Value = new string[] { };

            string sTmpTableSelect = string.Empty;

            char[] DelimitChar1 = { ':' };

            if (dateTimeInSQLFormat == "NULL")
            {
                dateTimeInSQLFormat = "NULL";
            }

            var sFiltroFabricante = (selFabricanteValue == 0 ? "" : " AND Produtos.FabricanteID=" + selFabricanteValue);

            var sFiltroEmpresa = "";
            var nItensSelected = 0;
            var sFiltroProprietario = "";

            if (selProprietarios2_selectedIndex > 0)
                sFiltroProprietario = " AND ProdutosEmpresa.ProprietarioID = " + selProprietarios2_value + " ";

            if (selEmpresa != "")
                _selEmpresa = selEmpresa.Split(DelimitChar1);


            for (i = 0; i < _selEmpresa.Length; i++)
            {
                nItensSelected++;
                sFiltroEmpresa += (nItensSelected == 1 ? " AND (" : " OR ") + "ProdutosEmpresa.SujeitoID=" + _selEmpresa[i].ToString();

            }

            sFiltroEmpresa += (nItensSelected > 0 ? ")" : "");

            var sFiltroFamilia = "";
            nItensSelected = 0;

            if (selFamilias != "")
                _selFamilias = selFamilias.Split(DelimitChar1);

            for (i = 0; i < _selFamilias.Length; i++)
            {
                nItensSelected++;
                sFiltroFamilia += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.ProdutoID=" + _selFamilias[i].ToString();
            }

            sFiltroFamilia += (nItensSelected > 0 ? ")" : "");


            var sFiltroMarca = "";
            nItensSelected = 0;

            if (selMarca != "")
                _selMarca = selMarca.Split(DelimitChar1);

            for (i = 0; i < _selMarca.Length; i++)
            {
                nItensSelected++;
                sFiltroMarca += (nItensSelected == 1 ? " AND (" : " OR ") + "Produtos.MarcaID=" + _selMarca[i].ToString();

            }

            sFiltroMarca += (nItensSelected > 0 ? ")" : "");

            //DireitoEspecifico
            //Relacao PesCon->Produtos->Modal Print
            //21060 SFS Grupo RelPesCon -> Relatorio: 40123 Relatorio de Estoque (Padrao Generico)
            //A1||A2 -> Impressao
            //A2 -> Zebrar
            //A1&&A2 -> Excel
            //desabilitado-> Word

            var sFiltroEstados = " ";
            var bFirst = true;
            var sEstadosCheckeds = "";

            if (elem_Value != "")
                _elem_Value = elem_Value.Split(DelimitChar1);

            // loop nos checkbox de estados para montar a string de filtro
            for (i = 0; i < (_elem_Value.Length); i++)
            {
                if (bFirst)
                {
                    bFirst = false;

                    sFiltroEstados = " AND (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ";
                }
                else
                {
                    sFiltroEstados += " OR (dbo.fn_RelPesCon_Estado(ProdutosEmpresa.RelacaoID, " + dateTimeInSQLFormat + ")= " + _elem_Value[i] + " ) ";
                }

            }

            if (!bFirst)
                sFiltroEstados += ")";
            else
                sFiltroEstados += " AND (ProdutosEmpresa.EstadoID=0)";

            var bLocalizacao = true;

            string[] _chkCmbEstoques = chkCmbEstoques.Split(DelimitChar1);

            for (i = 0; i < _chkCmbEstoques.Length; i++)
            {
                if (_chkCmbEstoques[i] == "checked")
                {
                    bLocalizacao = false;
                }
            }

            string sSQLDetailSelect, sSQLDetailFrom, sSQLDetailWhere1, sSQLDetailWhere2, sSQLDetailOrderBy;


            sSQLDetailSelect = "SELECT DISTINCT " +
                               "Empresas.Fantasia AS [Empresa]," +
                               "ProdutosEmpresa.ObjetoID," +
                               "dbo.fn_Produto_Descricao2(ProdutosEmpresa.ObjetoID,647,11)," +
                               "Familias.Conceito," +
                               "Marcas.Conceito," +
                               "Produtos.Modelo," +
                               "(CASE WHEN (produtosempresa.SujeitoID IN(10,14) AND (produtosempresa.ClassificacaoFiscalID IS NULL)) " +
                               "THEN '99999999' " +
                               "ELSE (SELECT  NCM.ClassificacaoFiscal  FROM Conceitos NCM WITH(NOLOCK) WHERE NCM.ConceitoID = produtosempresa.ClassificacaoFiscalID) END) AS [NCM], " +
                               "0," +
                               "  (CASE WHEN (CASE WHEN (dbo.Fn_produto_custo(produtosempresa.relacaoid," + dateTimeInSQLFormat + ", 647,NULL, 2) = '0.00') " +
                               " THEN (SELECT TOP 1 b.ValorUnitario " +
                                            "FROM Pedidos a WITH(NOLOCK) " +
                                            "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) " +
                                            "WHERE ((a.EmpresaID = produtosempresa.SujeitoID) AND  " +
                                            "	  (b.ProdutoID = produtosempresa.ObjetoID) AND  " +
                                            "	  (a.EstadoID >= 28) AND " +
                                            "	  (b.dtMovimento <=" + dateTimeInSQLFormat + ") AND " +
                                            "	  (a.TipoPedidoID = 601))   " +
                                            "ORDER BY b.dtMovimento DESC)  " +
                        "ELSE dbo.Fn_produto_custo(produtosempresa.relacaoid," + dateTimeInSQLFormat + ", 647,NULL, 2) END) IS NULL   " +
                        "THEN (SELECT TOP 1 (CASE WHEN (b.MoedaID <> 647)  " +
                                                    "THEN (dbo.fn_Preco_Cotacao(541,647,NULL," + dateTimeInSQLFormat + ") *  b.CustoReposicao) " +
                                                    "ELSE b.CustoReposicao END) " +
                                    "FROM RelacoesPesCon a WITH(NOLOCK) " +
                                    "	INNER JOIN RelacoesPesCon_Fornecedores b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " +
                                    "WHERE ((a.SujeitoID = produtosempresa.SujeitoID) AND (a.ObjetoID = produtosempresa.ObjetoID )) " +
                                    "ORDER BY b.RelPesConFornecID ASC) " +
                        "ELSE (CASE WHEN (dbo.Fn_produto_custo(produtosempresa.relacaoid," + dateTimeInSQLFormat + ", 647,NULL, 2) <= '0.00')  " +
                        "THEN (SELECT TOP 1 b.ValorUnitario  " +
                                            "FROM Pedidos a WITH(NOLOCK)  " +
                                            "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (a.PedidoID = b.PedidoID) " +
                                            "WHERE ((a.EmpresaID = produtosempresa.SujeitoID) AND  " +
                                            "	  (b.ProdutoID = produtosempresa.ObjetoID) AND  " +
                                            "	  (a.EstadoID >= 28) AND " +
                                            "	  (b.dtMovimento <= " + dateTimeInSQLFormat + ") AND " +
                                                  "(a.TipoPedidoID = 601))   " +
                                            "ORDER BY b.dtMovimento DESC)  " +
                        "ELSE dbo.Fn_produto_custo(produtosempresa.relacaoid," + dateTimeInSQLFormat + ", 647,NULL, 2) END) END), " +
                               "0," +
                               "AliquotaICMS.Aliquota," +
                               "dbo.fn_TipoAuxiliar_Item(produtos.UnidadeID,2), " +
                                dateTimeInSQLFormat + "," +
                               "(SELECT TOP 1  dbo.fn_DivideZero(b.ValorCustoTotal, b.Quantidade) " +
                                "FROM RelacoesPesCon_Kardex aa WITH(NOLOCK) " +
                                    "INNER JOIN Pedidos_Itens_Kardex b WITH(NOLOCK) on b.PedIteKardexID = aa.PedIteKardexID " +
                                    "INNER JOIN Pedidos_Itens bb WITH(NOLOCK) ON (b.PedItemID = bb.PedItemID) " +
                                    "INNER JOIN Pedidos cc WITH(NOLOCK) ON (bb.PedidoID = cc.PedidoID) " +
                                    "LEFT OUTER JOIN NotasFiscais dd WITH(NOLOCK) ON (cc.NotaFiscalID = dd.NotaFiscalID) " +
                                    "INNER JOIN Operacoes ee WITH(NOLOCK) ON (cc.TransacaoID = ee.OperacaoID) " +
                               "WHERE ((aa.RelacaoID = produtosempresa.RelacaoID) AND " +
                                      "(aa.dtkardex <=" + dateTimeInSQLFormat + ") AND " +
                                      "(ee.OperacaoID IN (111)) AND (aa.TipoCustoID = 374)) " +
                                "ORDER BY aa.RelPesConKardexID DESC), " +
                               "(SELECT TOP 1  aa.dtKardex " +
                                "FROM RelacoesPesCon_Kardex aa WITH(NOLOCK) " +
                                    "INNER JOIN Pedidos_Itens_Kardex b WITH(NOLOCK) on b.PedIteKardexID = aa.PedIteKardexID " +
                                    "INNER JOIN Pedidos_Itens bb WITH(NOLOCK) ON (b.PedItemID = bb.PedItemID) " +
                                    "INNER JOIN Pedidos cc WITH(NOLOCK) ON (bb.PedidoID = cc.PedidoID) " +
                                    "LEFT OUTER JOIN NotasFiscais dd WITH(NOLOCK) ON (cc.NotaFiscalID = dd.NotaFiscalID) " +
                                    "INNER JOIN Operacoes ee WITH(NOLOCK) ON (cc.TransacaoID = ee.OperacaoID) " +
                               "WHERE ((aa.RelacaoID = produtosempresa.RelacaoID) AND " +
                                      "(aa.dtkardex <=" + dateTimeInSQLFormat + ") AND " +
                                      "(ee.OperacaoID IN (111)) AND (aa.TipoCustoID = 374)) " +
                                "ORDER BY aa.RelPesConKardexID DESC), " +
                               "(SELECT TOP 1  cc.PedidoID " +
                                "FROM RelacoesPesCon_Kardex aa WITH(NOLOCK) " +
                                    "INNER JOIN Pedidos_Itens_Kardex b WITH(NOLOCK) on b.PedIteKardexID = aa.PedIteKardexID " +
                                    "INNER JOIN Pedidos_Itens bb WITH(NOLOCK) ON (b.PedItemID = bb.PedItemID) " +
                                    "INNER JOIN Pedidos cc WITH(NOLOCK) ON (bb.PedidoID = cc.PedidoID) " +
                                    "LEFT OUTER JOIN NotasFiscais dd WITH(NOLOCK) ON (cc.NotaFiscalID = dd.NotaFiscalID) " +
                                    "INNER JOIN Operacoes ee WITH(NOLOCK) ON (cc.TransacaoID = ee.OperacaoID) " +
                               "WHERE ((aa.RelacaoID = produtosempresa.RelacaoID) AND " +
                                      "(aa.dtkardex <=" + dateTimeInSQLFormat + ") AND " +
                                      "(ee.OperacaoID IN (111)) AND (aa.TipoCustoID = 374)) " +
                                "ORDER BY aa.RelPesConKardexID DESC), " +
                        "ISNULL((SELECT TOP 1 dbo.fn_Preco_Cotacao(aa.MoedaID, 647, NULL," + dateTimeInSQLFormat + ") * aa.CustoReposicao " +
                                            "FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) " +
                                            "WHERE (aa.RelacaoID =  produtosempresa.RelacaoID)), 0) , " +
                                            "TiposAuxiliares.ItemMasculino AS Classificacao  ";
            sSQLDetailFrom = "FROM " +
                        "conceitos conceitos1 WITH(nolock)," +
                        "relacoesconceitos relconceitos1 WITH(nolock)," +
                        "conceitos conceitos2 WITH(nolock)," +
                        "relacoesconceitos relconceitos2 WITH(nolock)," +
                        "conceitos conceitos3 WITH(nolock)," +
                        "conceitos produtos WITH(nolock)," +
                        "conceitos familias WITH(nolock)," +
                        "conceitos marcas WITH(nolock)," +
                        "relacoespescon produtosempresa WITH(nolock)," +
                        "recursos estados WITH(nolock)," +
                        "recursos sistema WITH(nolock)," +
                        "pessoas fabricantes WITH(nolock)," +
                        "pessoas empresas WITH(nolock)," +
                        "pessoas proprietarios WITH(nolock)," +
                        "RelacoesPesCon_Impostos AliquotaICMS WITH(NOLOCK), " +
                        "TiposAuxiliares_Itens TiposAuxiliares WITH(nolock) ";

            sSQLDetailWhere1 = "WHERE (" +
                    "(Conceitos1.TipoConceitoID=301 AND Conceitos1.EstadoID=2 AND Conceitos1.Nivel=1) AND " +
                    "(Conceitos1.ConceitoID=RelConceitos1.ObjetoID AND RelConceitos1.TipoRelacaoID=41 AND " +
                    "RelConceitos1.EstadoID=2) AND " +
                    "(TiposAuxiliares.ItemID = Produtos.ClassificacaoID ) AND " +
                    "(RelConceitos1.SujeitoID=Conceitos2.ConceitoID AND Conceitos2.TipoConceitoID=301 AND " +
                    "Conceitos2.EstadoID=2 AND Conceitos2.Nivel=2) AND " +
                    "(Conceitos2.ConceitoID=RelConceitos2.ObjetoID AND RelConceitos2.TipoRelacaoID=41 AND " +
                    "RelConceitos2.EstadoID=2 AND RelConceitos2.EhDefault=1) AND " +
                    "(RelConceitos2.SujeitoID=Conceitos3.ConceitoID AND Conceitos3.TipoConceitoID=302 AND ";


            sSQLDetailWhere2 =
                    "Conceitos3.EstadoID=2) AND " +
                    "(Conceitos3.ConceitoID=Produtos.ProdutoID AND Produtos.TipoConceitoID=303) AND " +
                    "(Produtos.ConceitoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.TipoRelacaoID=61 AND " +
                    "ProdutosEmpresa.EstadoID=Estados.RecursoID AND Sistema.RecursoID=999) AND " +
                    "(Familias.ConceitoID = Produtos.ProdutoID AND Marcas.ConceitoID=Produtos.MarcaID) AND " +
                    "(Fabricantes.PessoaID=Produtos.FabricanteID) AND " +
                    "(Empresas.PessoaID=ProdutosEmpresa.SujeitoID) AND " +
                    "(Proprietarios.PessoaID=ProdutosEmpresa.ProprietarioID) AND " +
                    "((AliquotaICMS.RelacaoID = produtosempresa.RelacaoID) AND " +
                    " (AliquotaICMS.ImpostoID = 9) AND (AliquotaICMS.TipoID = 335)) ";

            if (chkProdutoSeparavel == "checked")
                sSQLDetailWhere1 += " AND Produtos.ProdutoSeparavel = 1 ";

            if (chkReportaFabricante == "checked")
                sSQLDetailWhere1 += " AND ProdutosEmpresa.ReportaFabricante = 1 ";

            sSQLDetailWhere1 += sSQLDetailWhere2 + sFiltroFabricante + sFiltroEmpresa + sFiltroProprietario + sFiltroFamilia + sFiltroMarca + sFiltroEstados + sFiltro + ") ";

            sSQLDetailOrderBy = "ORDER BY  Empresas.Fantasia,ProdutosEmpresa.ObjetoID ";

            string[] aSQLQuantEstoquesDetail = new string[]
            {
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,341," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPO ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,342," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPE ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,343," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPD ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,344," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPC ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,345," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS EPT ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,346," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS ETO ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,347," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS ETE ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,348," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS ETD ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,349," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS ETC ",
                 "dbo.fn_Produto_EstoqueBrinde(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,4) AS EB ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,341," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, -1) AS F ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,356," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS Dis ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,351," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS RC ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,352," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS RCC ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,353," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS RV ",
                 "dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID,ProdutosEmpresa.ObjetoID,354," + glb_DepositoSelecionado + "," + dateTimeInSQLFormat + ",NULL,375, NULL) AS RVC "
            };

            string[] aDynamicEstoque = new string[]
            {
                 "V_EPO Numeric(10),",
                 "V_EPE Numeric(10),",
                 "V_EPD Numeric(10),",
                 "V_EPC Numeric(10),",
                 "V_EPT Numeric(10),",
                 "V_ETO Numeric(10),",
                 "V_ETE Numeric(10),",
                 "V_ETD Numeric(10),",
                 "V_ETC Numeric(10),",
                 "V_EB_ Numeric(10),",
                 "V_F Numeric(10),",
                 "V_Dis Numeric(10),",
                 "V_RC Numeric(10),",
                 "V_RCC Numeric(10),",
                 "V_RV Numeric(10),",
                 "V_RVC Numeric(10),"
            };


            for (i = 0; i < _chkCmbEstoques.Length; i++)
            {
                if (_chkCmbEstoques[i] == "checked")
                    sSQLDetailSelect += ", " + aSQLQuantEstoquesDetail[i];
            }

            sTmpTableSelect = "CREATE TABLE #TempTable " +
                "( " +
                "Empresa VARCHAR(100), " +
                "ProdutoID INT," +
                "Produto VARCHAR(500)," +
                "Familia VARCHAR(100)," +
                "Marca VARCHAR(100)," +
                "Modelo VARCHAR(100)," +
                "NCM VARCHAR(50)," +
                "Quantidade INT," +
                "CustoContabil NUMERIC(11,2)," +
                "CustoContabilTotal  NUMERIC(11,2)," +
                "AliquotaICMS NUMERIC(5,2)," +
                "UnidadeMedida VARCHAR(50)," +
                "Data DATETIME," +
                "V_CustoUltimaCompraKardex NUMERIC(11,2)," +
                "V_dtUltimaCompraKardex DATETIME," +
                "V_PedidoID INT," +
                "V_CustoReposicao NUMERIC(11,2)," +
                "Classificacao VARCHAR(100), " +
                "V_epo  INT," +
                "V_epe INT," +
                "V_epd INT," +
                "V_epc INT," +
                "V_ept INT )";

            sTmpTableSelect += " INSERT INTO #TempTable " + sSQLDetailSelect + sSQLDetailFrom + sSQLDetailWhere1 + sSQLDetailOrderBy + " ";

            if (!bLocalizacao)
            {
                sTmpTableSelect += " UPDATE #TempTable SET Quantidade = ( ";

                var nQtdEstoquesCheckeds = 0;
                var isFirst = true;

                for (i = 0; i < _chkCmbEstoques.Length; i++)
                {
                    if (_chkCmbEstoques[i] == "checked")
                    {
                        sTmpTableSelect += (isFirst ? "" : "+") + (aDynamicEstoque[i]).Substring(0, 5);
                        isFirst = false;
                        nQtdEstoquesCheckeds++;
                    }
                }

                if (!isFirst)
                    sTmpTableSelect += ") ";
            }

            if (chkValoracao == "checked")
                sTmpTableSelect += " UPDATE #TempTable SET CustoContabilTotal = Quantidade * CustoContabil";

            sTmpTableSelect += " SELECT * FROM #TempTable ";

            if (chkTemEstoque == "checked")
                sTmpTableSelect += " WHERE (Quantidade > 0) ";

            sTmpTableSelect += " DELETE FROM #temptable WHERE ncm IS NULL OR custocontabil IS NULL ";

            sTmpTableSelect += " DROP TABLE #TempTable";

            var strSQLParsed = "EXEC sp_RelatorioEstoque " + "'" + dupCharInString(sTmpTableSelect, "'") + "'";

            strSQLParsed = (strSQLParsed == null ? "" : strSQLParsed);

            var lUseBold = true;
            var nFontTitleSize = 8;
            var lUseBoldSubDetail = false;
            var nLeft = 0;
            var nTopHeader2 = 0;
            var sParams = "";

            string[,] arrQuerys = new string[,] 
            {
                {"Query1", strSQLParsed}
            };

            if (PeriodoSolcitado == null)
                PeriodoSolcitado = DateTime.Today.ToString();

            var PosX_Land1 = "4.98979";
            //var PosX_Land1 = "0.498979";
            var PosX_Land2 = "21.28271";
            //var PosX_Land2 = "2.128271";
            var Sped = "SPED - " + PeriodoSolcitado;

            Relatorio.CriarRelatório("RelEstPadraoSPED", arrQuerys, "BRA", "Excel");

            Relatorio.CriarObjLabel("Fixo", "", glb_sEmpresaFantasia, "0", "0", "9", "Black", "B", "Body", "5");
            Relatorio.CriarObjLabel("Fixo", "", Sped, PosX_Land1, "0", "9", "#0113a0", "B", "Body", "5");
            Relatorio.CriarObjLabelData("Now", "", "", PosX_Land2, "0", "9", "black", "B", "Body", "3.72187", "Horas");

            Relatorio.CriarObjTabela("0", "0.39688", "Query1", true, true, true);
            //Relatorio.CriarObjTabela("0", "0.039688", "Query1", true, true, true);

            Relatorio.TabelaEnd();

            Relatorio.CriarPDF_Excel("RelEstoquePadraoSPED", 2);
            
        }

        private string dupCharInString(string _string, string _char)
        {
            var _charRet = _char + _char;

            _string = _string.Replace(_char, _charRet);

            return _string;
        }
    }
}