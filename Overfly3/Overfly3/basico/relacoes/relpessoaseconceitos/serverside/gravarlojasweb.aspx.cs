using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;

namespace Overfly3.basico.relacoes.relpessoaseconceitosEx.serverside
{
    public partial class gravarlojasweb : System.Web.UI.OverflyPage
    {
        private Integer[] RelacaoID;

        public Integer[] nRelacaoID
        {
            get { return RelacaoID; }
            set { RelacaoID = value; }
        }
        private Integer[] LojaID;

        public Integer[] nLojaID
        {
            get { return LojaID; }
            set { LojaID = value; }
        }
        private Integer[] Exclui;

        public Integer[] nExclui
        {
            get { return Exclui; }
            set { Exclui = value; }
        }
        private Integer[] LojaOK;

        public Integer[] nLojaOK
        {
            get { return LojaOK; }
            set { LojaOK = value; }
        }

        protected int Executar()
        {
            int fldresp;
            string sql = "";
            int nDataLen = RelacaoID.Length;

            for (int i = 0; i < nDataLen; i++)
            {
                if (LojaID[i].intValue() != 0)
                {
                    sql = sql + "EXEC sp_RelPesCon_Lojas_Atualiza " +
                               RelacaoID[i].ToString() + ", " +
                               LojaID[i].ToString() + ", " +
                               Exclui[i].ToString() + " ";
                }
            }

            for (int i = 0; i < nDataLen; i++)
            {
                sql = sql + " UPDATE RelacoesPesCon SET LojaOK  = " + LojaOK[i].ToString() + " WHERE RelacaoID = " + RelacaoID[i].ToString();
            }

            fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

            return fldresp;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = Executar();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select " + fldresp + " as fldresp"
                )
            );
        }
    }
}
