<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- ID do arquivo -->
<!-- //@@ -->
<html id="relpessoasconceitosinf01Html" name="relpessoasconceitosinf01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf  
   
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_consultaserasa.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailinf.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf
     
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf      
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/relpessoasconceitosinf01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/especificinf.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</SCRIPT>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 fg_KeyPress(arguments[0]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPressEdit>
<!--
 js_fg_KeyPressEdit(arguments[0], arguments[1], arguments[2]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell(fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseUp>
<!--
 fg_MouseUp();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseDown>
<!--
 fg_MouseDown();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 fg_BeforeEdit();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
 fg_DblClick();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ChangeEdit>
<!--
 fg_ChangeEdit();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 fg_ValidateEdit();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
 js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

</head>

<!-- //@@ -->
<body id="relpessoasconceitosinf01Body" name="relpessoasconceitosinf01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle de impressao matricial -->
    <object classid="clsid:1DF93CFF-D5C8-4A6D-983F-19DBAAC698D9" id = "oPrinter" name = "oPrinter" width=0 height=0></object>
           
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">  
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- LANGUAGE="javascript" onchange="return optChangedInCmb(this)", que esta no inf01.js" -->
    <!-- //@@ Todos os controles cujo onclick e invocado convergem para -->
    <!-- a funcao onClickControl(cmb), que esta no inf01.js" -->
    
    <!-- Primeiro div inferior - Observacoes -->
    <!-- O div com o controle abaixo e comum a todos os forms -->
    <div id="divInf01_01" name="divInf01_01" class="divExtern">
        <textarea id="txtObservacoes" name="txtObservacoes" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Observacoes"></textarea>
    </div>
    
    <!-- Segundo div inferior - Prop Alter -->
    <!-- O div com os controles abaixo e comum a todos os forms -->
    <div id="divInf01_02" name="divInf01_02" class="divExtern">
        <p  id="lblProprietario" name="lblProprietario" class="lblGeneral">Propriet�rio</p>
        <select id="selProprietario" name="selProprietario" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Proprietarioid"></select>
	    <!-- //@@ ajustar path abaixo-->
	    <input type="image" id="btnFindProprietario" name="btnFindProprietario" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">
        <p id="lblAlternativo" name="lblAlternativo" class="lblGeneral">Alternativo</p>
        <select id="selAlternativo" name="selAlternativo" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Alternativoid"></select>
	    <!-- //@@ ajustar path abaixo-->
	    <input type="image" id="btnFindAlternativo" name="btnFindAlternativo" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">
    </div>
        
    <div id="divInf01_03" name="divInf01_03" class="divExtern">
	    <!--ESTOQUES-->
        <p id="lblDataEstoque" name="lblDataEstoque" class="lblGeneral">Data Estoque</p>
        <input type="text" id="txtDataEstoque" name="txtDataEstoque" class="fldGeneral"></input>
        <p id="lblEstoquePO" name="lblEstoquePO" class="lblGeneral">EPO</p>
        <input type="text" id="txtEstoquePO" name="txtEstoquePO" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoquePO" title="Estoque Pr�prio OK"></input>
        <p id="lblEstoquePE" name="lblEstoquePE" class="lblGeneral">EPE</p>
        <input type="text" id="txtEstoquePE" name="txtEstoquePE" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoquePE"  title="Estoque Pr�prio Emprestado"></input>
        <p id="lblEstoquePD" name="lblEstoquePD" class="lblGeneral">EPD</p>
        <input type="text" id="txtEstoquePD" name="txtEstoquePD" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoquePD" title="Estoque Pr�prio c/ Defeito"></input>
        <p id="lblEstoquePC" name="lblEstoquePC" class="lblGeneral">EPC</p>
        <input type="text" id="txtEstoquePC" name="txtEstoquePC" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoquePC" title="Estoque Pr�prio Consertando"></input>
        <p id="lblEstoquePT" name="lblEstoquePT" class="lblGeneral">EPT</p>
        <input type="text" id="txtEstoquePT" name="txtEstoquePT" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoquePT" title="Estoque Pr�prio em Tr�nsito"></input>
        <p id="lblEstoqueProprio" name="lblEstoqueProprio" class="lblGeneral">Pr�prio</p>
        <input type="text" id="txtEstoqueProprio" name="txtEstoqueProprio" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueProprio" title="Estoque Pr�prio"></input>
        <p id="lblEstoqueTO" name="lblEstoqueTO" class="lblGeneral">ETO</p>
        <input type="text" id="txtEstoqueTO" name="txtEstoqueTO" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueTO" title="Estoque de Terceiros OK"></input>
        <p id="lblEstoqueTE" name="lblEstoqueTE" class="lblGeneral">ETE</p>
        <input type="text" id="txtEstoqueTE" name="txtEstoqueTE" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueTE"  title="Estoque de Terceiros Emprestado"></input>
        <p id="lblEstoqueTD" name="lblEstoqueTD" class="lblGeneral">ETD</p>
        <input type="text" id="txtEstoqueTD" name="txtEstoqueTD" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueTD" title="Estoque de Terceiros c/ Defeito"></input>
        <p id="lblEstoqueTC" name="lblEstoqueTC" class="lblGeneral">ETC</p>
        <input type="text" id="txtEstoqueTC" name="txtEstoqueTC" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueTC" title="Estoque de Terceiros Consertando"></input>
        <p id="lblEstoqueTerceiros" name="lblEstoqueTerceiros" class="lblGeneral">Terceiros</p>
        <input type="text" id="txtEstoqueTerceiros" name="txtEstoqueTerceiros" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueTerceiros" title="Estoque de Terceiros"></input>
        <p id="lblEstoqueTotal" name="lblEstoqueTotal" class="lblGeneral">Total</p>
        <input type="text" id="txtEstoqueTotal" name="txtEstoqueTotal" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueTotal" title="Estoque Total"></input>
        <p id="lblLocalizacao" name="lblLocalizacao" class="lblGeneral">Localiza��o</p>
        <input type="text" id="txtLocalizacao" name="txtLocalizacao" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Localizacao" ></input>
        <p id="lblEstoqueSemNS" name="lblEstoqueSemNS" class="lblGeneral" title="Estoque sem n�mero de s�rie">SNS</p>
        <input type="text" id="txtEstoqueSemNS" name="txtEstoqueSemNS" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueSemNS" title="Estoque sem n�mero de s�rie"></input>
        <p id="lblEstoqueControle" name="lblEstoqueControle" class="lblGeneral">Controle</p>
        <input type="text" id="txtEstoqueControle" name="txtEstoqueControle" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueControle" title="Estoque de Controle"></input>
        <p id="lblControle" name="lblControle" class="lblGeneral">C</p>
        <input type="checkbox" id="chkControle" name="chkControle" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Controle" title="Libera a venda mesmo que o Estoque Dispon�vel fique menor que o Estoque Controle?"></input>
        <p id="lblLiberaBrinde" name="lblLiberaBrinde" class="lblGeneral">LB</p>
        <input type="checkbox" id="chkLiberaBrinde" name="chkLiberaBrinde" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="LiberaBrinde" title="Libera sa�da de brinde sem saldo?"></input>
        <p id="lblAjusteBrinde" name="lblAjusteBrinde" class="lblGeneral">Ajuste Brinde</p>
        <input type="text" id="txtAjusteBrinde" name="txtAjusteBrinde" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="AjusteBrinde" title=""></input>
        <p id="lblEstoqueBrinde" name="lblEstoqueBrinde" class="lblGeneral">Brinde</p>
        <input type="text" id="txtEstoqueBrinde" name="txtEstoqueBrinde" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="EstoqueBrinde" title="Estoque de Brinde"></input>
        <p id="lblPrazoObsoletoExtra" name="lblPrazoObsoletoExtra" class="lblGeneral">Obs Extra</p>
        <input type="text" id="txtPrazoObsoletoExtra" name="txtPrazoObsoletoExtra" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="PrazoObsoletoExtra" title="Prazo extra para obsolesc�ncia do produto"></input>
    </div>

    <div id="divInf01_04" name="divInf01_04" class="divExtern">
	    <!--GERENCIAMENTO DE PRODUTOS-->
        <p id="lblMargemContribuicao" name="lblMargemContribuicao" class="lblGeneral">MC</p>
        <input type="text" id="txtMargemContribuicao" name="txtMargemContribuicao" DATASRC="#dso01JoinSup" DATAFLD="MargemContribuicao" class="fldGeneral" title="Margem de Contribui��o"></input>
        <p id="lblMargemPadrao" name="lblMargemPadrao" class="lblGeneral">MP</p>
        <input type="text" id="txtMargemPadrao" name="txtMargemPadrao" DATASRC="#dso01JoinSup" DATAFLD="MargemPadrao" class="fldGeneral" title="Margem de Padr�o"></input>
        <p id="lblMargemPublicacao" name="lblMargemPublicacao" class="lblGeneral">MPub</p>
		<input type="text" id="txtMargemPublicacao" name="txtMargemPublicacao" DATASRC="#dso01JoinSup" DATAFLD="MargemPublicacao" class="fldGeneral" title="Margem de Publica��o"></input>        
        <p id="lblMargemMinima" name="lblMargemMinima" class="lblGeneral">MM�n</p>
        <input type="text" id="txtMargemMinima" name="txtMargemMinima" DATASRC="#dso01JoinSup" DATAFLD="MargemMinima" class="fldGeneral" title="Margem m�nima"></input>
        <p id="lblMargemRealCI" name="lblMargemRealCI" class="lblGeneral">MR.ci</p>
        <input type="text" id="txtMargemRealCI" name="txtMargemRealCI" DATASRC="#dso01JoinSup" DATAFLD="MRci" class="fldGeneral" title="Margem Real ci"></input>
        <p id="lblPrecoMinimo" name="lblPrecoMinimo" class="lblGeneral">PMin</p>
	    <input type="text" id="txtPrecoMinimo" name="txtPrecoMinimo" DATASRC="#dso01JoinSup" DATAFLD="PrecoMinimo" class="fldGeneral" title="Pre�o Minimo">
	    <p id="lblPrazoTroca" name="lblPrazoTroca" class="lblGeneral">Troca</p>
	    <input type="text" id="txtPrazoTroca" name="txtPrazoTroca" DATASRC="#dso01JoinSup" DATAFLD="PrazoTroca" class="fldGeneral" title="Prazo de Troca (em meses)">
	    <p id="lblPrazoGarantia" name="lblPrazoGarantia" class="lblGeneral">Gar</p>
	    <input type="text" id="txtPrazoGarantia" name="txtPrazoGarantia" DATASRC="#dso01JoinSup" DATAFLD="PrazoGarantia" class="fldGeneral" title="Prazo de Garantia (em meses)">
	    <p id="lblPrazoAsstec" name="lblPrazoAsstec" class="lblGeneral">Asstec</p>
	    <input type="text" id="txtPrazoAsstec" name="txtPrazoAsstec" DATASRC="#dso01JoinSup" DATAFLD="PrazoAsstec" class="fldGeneral" title="Prazo de Assist�ncia T�cnica (em meses)">
	    <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
	    <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dso01JoinSup" DATAFLD="Observacao" class="fldGeneral">
        <p id="lblPublica" name="lblPublica" class="lblGeneral">PI</p>
        <input type="checkbox" id="chkPublica" name="chkPublica" DATASRC="#dso01JoinSup" DATAFLD="Publica" class="fldGeneral" title="Publica o produto na internet?"></input>
        <p id="lblPublicaPreco" name="lblPublicaPreco" class="lblGeneral">PP</p>
        <input type="checkbox" id="chkPublicaPreco" name="chkPublicaPreco" DATASRC="#dso01JoinSup" DATAFLD="PublicaPreco" class="fldGeneral" title="Publica o pre�o do produto na internet?"></input>
	    <p id="lblPercentualPublicacaoEstoque" name="lblPercentualPublicacaoEstoque" class="lblGeneral">PE %</p>
	    <input type="text" id="txtPercentualPublicacaoEstoque" name="txtPercentualPublicacaoEstoque" DATASRC="#dso01JoinSup" DATAFLD="PercentualPublicacaoEstoque" class="fldGeneral" title="Percentual de publica��o do estoque do produto na internet?">
	    <p id="lblReportaFabricante" name="lblReportaFabricante" class="lblGeneral">RF</p>
        <input type="checkbox" id="chkReportaFabricante" name="chkReportaFabricante" DATASRC="#dso01JoinSup" DATAFLD="Reportafabricante" class="fldGeneral" title="Reporta vendas e estoque para o fabricante?"></input>        
        <p id="lblBNDES" name="lblBNDES" class="lblGeneral">BNDES</p>
        <input type="checkbox" id="chkBNDES" name="chkBNDES" DATASRC="#dso01JoinSup" DATAFLD="BNDES" class="fldGeneral" title="Produto cadastrado no BNDES?"></input>        
        <p id="lblLojaOK" name="lblLojaOK" class="lblGeneral">Loja</p>
        <input type="checkbox" id="chkLojaOK" name="chkLojaOK" DATASRC="#dso01JoinSup" DATAFLD="LojaOK" class="fldGeneral" title="Loja OK?"></input>
        <p id="lblFichaTecnica" name="lblFichaTecnica" class="lblGeneral">FT</p>
        <input type="checkbox" id="chkFichaTecnica" name="chkFichaTecnica" DATASRC="#dso01JoinSup" DATAFLD="FichaTecnica" class="fldGeneral" title="Inclui produto na Ficha T�cnica da Proposta?"></input>
	    <p id="lblVendas0" name="lblVendas0" class="lblGeneral">VM0</p>
	    <input type="text" id="txtVendas0" name="txtVendas0" DATASRC="#dso01JoinSup" DATAFLD="Vendas0" class="fldGeneral" title="Vendas do m�s corrente">
	    <p id="lblVendas1" name="lblVendas1" class="lblGeneral">VM1</p>
	    <input type="text" id="txtVendas1" name="txtVendas1" DATASRC="#dso01JoinSup" DATAFLD="Vendas1" class="fldGeneral" title="Vendas do m�s anterior">
	    <p id="lblMediaVendas13" name="lblMediaVendas13" class="lblGeneral">MV13</p>
	    <input type="text" id="txtMediaVendas13" name="txtMediaVendas13" DATASRC="#dso01JoinSup" DATAFLD="MediaVendas13" class="fldGeneral" title="M�dia das vendas do m�s 1 ao 3">
	    <p id="lblPrevisaoVendas" name="lblPrevisaoVendas" class="lblGeneral">PV</p>
	    <input type="text" id="txtPrevisaoVendas" name="txtPrevisaoVendas" DATASRC="#dso01JoinSup" DATAFLD="PrevisaoVendas" class="fldGeneral" title="Previs�o de Vendas">
	    <p id="lblMultiploVenda" name="lblMultiploVenda" class="lblGeneral">MV</p>
	    <input type="text" id="txtMultiploVenda" name="txtMultiploVenda" DATASRC="#dso01JoinSup" DATAFLD="MultiploVenda" class="fldGeneral" title="M�ltiplo de Venda">
	    <p id="lblPrazoEstoque" name="lblPrazoEstoque" class="lblGeneral">PE</p>
	    <input type="text" id="txtPrazoEstoque" name="txtPrazoEstoque" DATASRC="#dso01JoinSup" DATAFLD="PrazoEstoque" class="fldGeneral" title="Prazo de Estoque (em dias �teis)">
	    <p id="lblPrazoEstoqueAjustado" name="lblPrazoEstoqueAjustado" class="lblGeneral">PEA</p>
	    <input type="text" id="txtPrazoEstoqueAjustado" name="txtPrazoEstoqueAjustado" DATASRC="#dso01JoinSup" DATAFLD="PrazoEstoqueAjustado" class="fldGeneral" title="Prazo de Estoque ajustado (em dias �teis)">
	    <p id="lblEstoque" name="lblEstoque" class="lblGeneral">Estoque</p>
	    <input type="text" id="txtEstoque" name="txtEstoque" DATASRC="#dso01JoinSup" DATAFLD="Estoque" class="fldGeneral" title="Estoque F�sico - RVC + RC">
	    <p id="lblLoteCompra" name="lblLoteCompra" class="lblGeneral">LC</p>
	    <input type="text" id="txtLoteCompra" name="txtLoteCompra" DATASRC="#dso01JoinSup" DATAFLD="LoteCompra" class="fldGeneral" title="Lote de Compra (PVD * PE)">
	    <p id="lblEstoqueWeb" name="lblEstoqueWeb" class="lblGeneral">Est Web</p>
	    <input type="text" id="txtEstoqueWeb" name="txtEstoqueWeb" DATASRC="#dso01JoinSup" DATAFLD="EstoqueWeb" class="fldGeneral" title="Estoque publicado na web">
	    <p id="lblVendas2" name="lblVendas2" class="lblGeneral">VM2</p>
	    <input type="text" id="txtVendas2" name="txtVendas2" DATASRC="#dso01JoinSup" DATAFLD="Vendas2" class="fldGeneral" title="Vendas do m�s retrasado">
	    <p id="lblVendas3" name="lblVendas3" class="lblGeneral">VM3</p>
	    <input type="text" id="txtVendas3" name="txtVendas3" DATASRC="#dso01JoinSup" DATAFLD="Vendas3" class="fldGeneral" title="Vendas do m�s re-retrasado">
	    <p id="lblPrevisaoVendasDiaria" name="lblPrevisaoVendasDiaria" class="lblGeneral">PVD</p>
	    <input type="text" id="txtPrevisaoVendasDiaria" name="txtPrevisaoVendasDiaria" DATASRC="#dso01JoinSup" DATAFLD="PrevisaoVendasDiaria" class="fldGeneral" title="Previs�o de Vendas Di�ria (PV / 20)">
	    <p id="lblMultiploCompra" name="lblMultiploCompra" class="lblGeneral">MC</p>
	    <input type="text" id="txtMultiploCompra" name="txtMultiploCompra" DATASRC="#dso01JoinSup" DATAFLD="MultiploCompra" class="fldGeneral" title="M�ltiplo de Compra">
	    <p id="lblPrazoReposicao1" name="lblPrazoReposicao1" class="lblGeneral">PR1</p>
	    <input type="text" id="txtPrazoReposicao1" name="txtPrazoReposicao1" DATASRC="#dso01JoinSup" DATAFLD="PrazoReposicao1" class="fldGeneral" title="Prazo de Reposi��o do Fornecedor 1 (em dias �teis)">
	    <p id="lblPrazoPagamento1" name="lblPrazoPagamento1" class="lblGeneral">PP1</p>
	    <input type="text" id="txtPrazoPagamento1" name="txtPrazoPagamento1" DATASRC="#dso01JoinSup" DATAFLD="PrazoPagamento1" class="fldGeneral" title="Prazo de Pagamento do Fornecedor 1">
	    <p id="lblPrazoMinimo" name="lblPrazoMinimo" class="lblGeneral">PM</p>
	    <input type="text" id="txtPrazoMinimo" name="txtPrazoMinimo" DATASRC="#dso01JoinSup" DATAFLD="PrazoMinimo" class="fldGeneral" title="Prazo p/ Estoque M�nimo (em dias �teis) (PR1 + PS)">
	    <p id="lblEstoqueMinimo" name="lblEstoqueMinimo" class="lblGeneral">EM</p>
	    <input type="text" id="txtEstoqueMinimo" name="txtEstoqueMinimo" DATASRC="#dso01JoinSup" DATAFLD="EstoqueMinimo" class="fldGeneral" title="Estoque M�nimo (PVD * PM)">
	    <p id="lblComprarExcesso" name="lblComprarExcesso" class="lblGeneral">Comprar</p>
	    <input type="text" id="txtComprarExcesso" name="txtComprarExcesso" class="fldGeneral" title="Quantidade a comprar">
        <p id="lblCicloVidaProduto" name="lblCicloVidaProduto" class="lblGeneral">CVP - Ciclo de Vida do Produto</p>
        <hr id="hrCicloVidaProduto" name="hrCicloVidaProduto" class="lblGeneral">
	    <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">In�cio</p>
	    <input type="text" id="txtdtInicio" name="txtdtInicio" DATASRC="#dso01JoinSup" DATAFLD="V_dtInicio" class="fldGeneral" title="In�cio do Ciclo de Vida do Produto">
	    <p id="lbldtControle" name="lbldtControle" class="lblGeneral">Controle</p>
	    <input type="text" id="txtdtControle" name="txtdtControle" DATASRC="#dso01JoinSup" DATAFLD="V_dtControle" class="fldGeneral" title="Data de Controle do CVP do Produto">
	    <p id="lblDiasControle" name="lblDiasControle" class="lblGeneral">Dias</p>
	    <input type="text" id="txtDiasControle" name="txtDiasControle" DATASRC="#dso01JoinSup" DATAFLD="DiasControle" class="fldGeneral" title="N�mero de dias que faltam p/ a data de Controle">
	    <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Fim</p>
	    <input type="text" id="txtdtFim" name="txtdtFim" DATASRC="#dso01JoinSup" DATAFLD="V_dtFim" class="fldGeneral" title="Final do Ciclo de Vida do Produto">
	    <p id="lblDiasFim" name="lblDiasFim" class="lblGeneral">Dias</p>
        <input type="text" id="txtDiasFim" name="txtDiasFim" DATASRC="#dso01JoinSup" DATAFLD="DiasFim" class="fldGeneral" title="N�mero de dias que faltam p/ o fim do CVP do Produto">	    
	    <p id="lbldtUltimaEntrada" name="lbldtUltimaEntrada" class="lblGeneral">�ltima Entrada</p>
        <input type="text" id="txtdtUltimaEntrada" name="txtdtUltimaEntrada" DATASRC="#dso01JoinSup" DATAFLD="dtUltimaEntrada" class="fldGeneral" title="Data da �ltima entrada do produto">
	    <p id="lbldtUltimaSaida" name="lbldtUltimaSaida" class="lblGeneral">�ltima Sa�da</p>
        <input type="text" id="txtdtUltimaSaida" name="txtdtUltimaSaida" DATASRC="#dso01JoinSup" DATAFLD="dtUltimaSaida" class="fldGeneral" title="Data da �ltima sa�da do produto">
	    <p id="lblDiasSaida" name="lblDiasSaida" class="lblGeneral">Dias</p>
        <input type="text" id="txtDiasSaida" name="txtDiasSaida" DATASRC="#dso01JoinSup" DATAFLD="DiasSaida" class="fldGeneral" title="N�mero de dias da �ltima sa�da do produto">
    </div>
        
    <div id="divInf01_05" name="divInf01_05" class="divExtern">
	    <!--FISCAL-->
        <p id="lblGrupoImpostoEntradaID" name="lblGrupoImpostoEntradaID" class="lblGeneral">Impostos de Entrada</p>
        <select id="selGrupoImpostoEntradaID" name="selGrupoImpostoEntradaID" DATASRC="#dso01JoinSup" DATAFLD="GrupoImpostoEntradaID" class="fldGeneral"></select>
        <p id="lblMoedaEntradaID" name="lblMoedaEntradaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaEntradaID" name="selMoedaEntradaID" DATASRC="#dso01JoinSup" DATAFLD="MoedaEntradaID" class="fldGeneral"></select>
        <p id="lblGrupoImpostoSaidaID" name="lblGrupoImpostoSaidaID" class="lblGeneral">Impostos de Sa�da</p>
        <select id="selGrupoImpostoSaidaID" name="selGrupoImpostoSaidaID" DATASRC="#dso01JoinSup" DATAFLD="GrupoImpostoSaidaID" class="fldGeneral"></select>
        <p id="lblMoedaSaidaID" name="lblMoedaSaidaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaSaidaID" name="selMoedaSaidaID" DATASRC="#dso01JoinSup" DATAFLD="MoedaSaidaID" class="fldGeneral"></select>
	    <p id="lblClassificacaoFiscalID" name="lblClassificacaoFiscalID" class="lblGeneral" title="Classifica��o Fiscal">Class Fiscal</p>
	    <select id="selClassificacaoFiscalID" name="selClassificacaoFiscalID" DATASRC="#dso01JoinSup" DATAFLD="ClassificacaoFiscalID" class="fldGeneral"></select>
	    <p id="lblOrigemID" name="lblOrigemID" class="lblGeneral">Origem</p>
        <select id="selOrigemID" name="selOrigemID" DATASRC="#dso01JoinSup" DATAFLD="OrigemID" class="fldGeneral"></select>
        <p id="lblProdutoImportado" name="lblProdutoImportado" class="lblGeneral">Imp</p>
        <input type="checkbox" id="chkProdutoImportado" name="chkProdutoImportado" DATASRC="#dso01JoinSup" DATAFLD="ProdutoImportado" class="fldGeneral" title="O produto � importado?"></input>
        <p id="lblNBSID" name="lblNBSID" class="lblGeneral">NBS</p>
        <select id="selNBSID" name="selNBSID" DATASRC="#dso01JoinSup" DATAFLD="NBSID" class="fldGeneral"></select>
        <p id="lblCEST" name="lblCEST" class="lblGeneral">CEST</p>
        <select id="selCEST" name="selCEST" DATASRC="#dso01JoinSup" DATAFLD="CestID" class="fldGeneral"></select>
    </div>

    <div id="divInf01_06" name="divInf01_06" class="divExtern">
        <OBJECT CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </OBJECT>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>
        
</body>

</html>
