/********************************************************************
relpessoasconceitosinf01.js

Library javascript para o relpessoasconceitosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// variavel de timer
var glb_PesConInfTimerVar = null;
// variavel controladora de execucao de atualizacao de estoques
var glb_dtPesConAtEstoque = null;
// variavel de window setInterval
var __winTimerHandler = null;
var glb_timerInfRefrInterval = null;

//Combo Din�mico
var glb_CounterCmbsDynamics = 0;
var glb_BtnFromFramWork;

var dso01JoinSup = new CDatatransport('dso01JoinSup');
var dsoGravaCustoMedio = new CDatatransport('dsoGravaCustoMedio');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dso01Grid = new CDatatransport('dso01Grid');
var dso02Grid = new CDatatransport('dso02Grid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb01Grid_02 = new CDatatransport('dsoCmb01Grid_02');
var dsoCmb01Grid_03 = new CDatatransport('dsoCmb01Grid_03');
var dsoCmb01Grid_04 = new CDatatransport('dsoCmb01Grid_04');
var dsoCmb01Grid_05 = new CDatatransport('dsoCmb01Grid_05');
var dsoCmb01Grid_06 = new CDatatransport('dsoCmb01Grid_06');
var dso01GridLkp = new CDatatransport('dso01GridLkp');
var dso02GridLkp = new CDatatransport('dso02GridLkp');
var dso03GridLkp = new CDatatransport('dso03GridLkp');
var dso04GridLkp = new CDatatransport('dso04GridLkp');
var dsoParallelGrid = new CDatatransport('dsoParallelGrid');
var dso01PgGrid = new CDatatransport('dso01PgGrid');
var dsoFiltroInf = new CDatatransport('dsoFiltroInf');
var dsoCmbsPropAlt = new CDatatransport('dsoCmbsPropAlt');
var OBJECT1 = new CDatatransport('OBJECT1');
// FINAL DE VARIAVEIS GLOBAIS ***************************************
/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()
    
********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    // Ze em 17/03/08
    dealWithGrid_Load();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    glb_aDsosToCloneFieldsEtc = ['dso01JoinSup'];

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 

    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', '']]],
                              [21065, [[3, 'dsoCmb01Grid_01', 'Conceito', 'ConceitoID'],
                                       [4, 'dsoCmb01Grid_02', 'SimboloMoeda', 'ConceitoID'],
                                       [12, 'dsoCmb01Grid_05', 'PortoOrigem', 'PortoOrigemID'],
                                       [13, 'dsoCmb01Grid_06', 'fldName', 'fldID'],
                                       [14, 'dsoCmb01Grid_03', 'SimboloMoeda', 'ConceitoID'],
                                       [18, 'dsoCmb01Grid_04', 'Conceito', 'ConceitoID'],
                                       
                                       [0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', '']]],
                              [21070, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', '']]],
							  [21073, [[0, 'dsoCmb01Grid_01', 'Equipe', 'EquipeID'],
									   [0, 'dso01GridLkp', '', '']]],
							  [21075, [[1, 'dsoCmb01Grid_01', 'Codigo', 'LocalizacaoID'],
									   [0, 'dso01GridLkp', '', '']]],
							  [21076, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]]];

    glb_aStaticCombos = ([['selGrupoImpostoEntradaID', '1'],
                          ['selGrupoImpostoSaidaID', '1'],
                          ['selMoedaEntradaID', '2'],
                          ['selMoedaSaidaID', '2'],
                          ['selClassificacaoFiscalID', '3'],
                          ['selOrigemID', '4'],
                          ['selNBSID', '5']]);


    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03',
                               'divInf01_04',
                               'divInf01_05',
                               'divInf01_06'],
                               [20008, 20009, 21064, 21072, 21080,
                               [20010, 21065, 21066, 21067, 21068, 21069, 21070, 21073, 21074, 21075, 21076]]);

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

function setupPage() {
    //@@ Ajustar os divs
    // Nada a codificar
    adjustDivs('inf', [[1, 'divInf01_01'],
                      [1, 'divInf01_02'],
                      [1, 'divInf01_03'],
                      [1, 'divInf01_04'],
                      [1, 'divInf01_05'],
                      [1, 'divInf01_06']]);

    adjustElementsInForm([['lblProprietario', 'selProprietario', 23, 1],
                          ['btnFindProprietario', '', 21, 1],
                          ['lblAlternativo', 'selAlternativo', 23, 1],
                          ['btnFindAlternativo', '', 21, 1]], null, 'INF');

    adjustElementsInForm([['lblMargemContribuicao', 'txtMargemContribuicao', 6, 1, -2],
                          ['lblMargemPadrao', 'txtMargemPadrao', 6, 1],
                          ['lblMargemPublicacao', 'txtMargemPublicacao', 6, 1],
                          ['lblMargemMinima', 'txtMargemMinima', 6, 1],
                          ['lblMargemRealCI', 'txtMargemRealCI', 6, 1],
                          ['lblPrecoMinimo', 'txtPrecoMinimo', 9, 1],
                          ['lblPrazoTroca', 'txtPrazoTroca', 4, 1],
                          ['lblPrazoGarantia', 'txtPrazoGarantia', 4, 1],
                          ['lblPrazoAsstec', 'txtPrazoAsstec', 4, 1],
						  ['lblObservacao', 'txtObservacao', 25, 1],
                          ['lblPublica', 'chkPublica', 3, 1],
                          ['lblPublicaPreco', 'chkPublicaPreco', 3, 1, -10],
                          ['lblPercentualPublicacaoEstoque', 'txtPercentualPublicacaoEstoque', 4, 1, -10],
                          ['lblReportaFabricante', 'chkReportaFabricante', 4, 1, -10],
                          ['lblBNDES', 'chkBNDES', 3, 1, -10],
                          ['lblVendas0', 'txtVendas0', 6, 2],
                          ['lblVendas1', 'txtVendas1', 6, 2],
                          ['lblMediaVendas13', 'txtMediaVendas13', 6, 2],
                          ['lblPrevisaoVendas', 'txtPrevisaoVendas', 6, 2],
                          ['lblMultiploVenda', 'txtMultiploVenda', 4, 2],
                          ['lblPrazoEstoque', 'txtPrazoEstoque', 4, 2],
                          ['lblPrazoEstoqueAjustado', 'txtPrazoEstoqueAjustado', 4, 2],
						  ['lblEstoque', 'txtEstoque', 6, 2],
						  ['lblLoteCompra', 'txtLoteCompra', 6, 2],
                          ['lblLojaOK', 'chkLojaOK', 3, 2, 155],
						  ['lblFichaTecnica', 'chkFichaTecnica', 3, 2, -5],
						  ['lblEstoqueWeb', 'txtEstoqueWeb', 6, 2, 25],
                          ['lblVendas2', 'txtVendas2', 6, 3],
                          ['lblVendas3', 'txtVendas3', 6, 3],
						  ['lblPrevisaoVendasDiaria', 'txtPrevisaoVendasDiaria', 6, 3, 60],
						  ['lblMultiploCompra', 'txtMultiploCompra', 4, 3],
						  ['lblPrazoReposicao1', 'txtPrazoReposicao1', 4, 3],
						  ['lblPrazoPagamento1', 'txtPrazoPagamento1', 4, 3],
						  ['lblPrazoMinimo', 'txtPrazoMinimo', 4, 3],
						  ['lblEstoqueMinimo', 'txtEstoqueMinimo', 6, 3],
						  ['lblComprarExcesso', 'txtComprarExcesso', 6, 3],
					      ['lblCicloVidaProduto', 'hrCicloVidaProduto', 110, 4],
                          ['lbldtInicio', 'txtdtInicio', 10, 5],
                          ['lbldtControle', 'txtdtControle', 10, 5],
                          ['lblDiasControle', 'txtDiasControle', 5, 5],
                          ['lbldtFim', 'txtdtFim', 10, 5],
                          ['lblDiasFim', 'txtDiasFim', 5, 5],
                          ['lbldtUltimaEntrada', 'txtdtUltimaEntrada', 10, 5],
                          ['lbldtUltimaSaida', 'txtdtUltimaSaida', 10, 5],
                          ['lblDiasSaida', 'txtDiasSaida', 6, 5]], null, 'INF');

    txtPercentualPublicacaoEstoque.setAttribute('minMax', new Array(0, 100), 1);
    lblReportaFabricante.style.left = parseInt(lblReportaFabricante.style.left, 10) + 5;

    adjustElementsInForm([['lblDataEstoque', 'txtDataEstoque', 17, 1],
                          ['lblEstoquePO', 'txtEstoquePO', 6, 1],
                          ['lblEstoquePE', 'txtEstoquePE', 6, 1, -2],
                          ['lblEstoquePD', 'txtEstoquePD', 6, 1, -2],
                          ['lblEstoquePC', 'txtEstoquePC', 6, 1, -2],
                          ['lblEstoquePT', 'txtEstoquePT', 6, 1, -2],
                          ['lblEstoqueProprio', 'txtEstoqueProprio', 6, 1, -2],
                          ['lblEstoqueTO', 'txtEstoqueTO', 6, 1, -2],
                          ['lblEstoqueTE', 'txtEstoqueTE', 6, 1, -2],
                          ['lblEstoqueTD', 'txtEstoqueTD', 6, 1, -2],
                          ['lblEstoqueTC', 'txtEstoqueTC', 6, 1, -2],
                          ['lblEstoqueTerceiros', 'txtEstoqueTerceiros', 6, 1, -2],
                          ['lblEstoqueTotal', 'txtEstoqueTotal', 6, 1, -8],
                          ['lblLocalizacao', 'txtLocalizacao', 20, 2, -2],
                          ['lblEstoqueSemNS', 'txtEstoqueSemNS', 6, 2, -2],
                          ['lblEstoqueControle', 'txtEstoqueControle', 6, 2, -2],
                          ['lblControle', 'chkControle', 3, 2],
						  ['lblLiberaBrinde', 'chkLiberaBrinde', 3, 2, -2],
						  ['lblAjusteBrinde', 'txtAjusteBrinde', 6, 2, -2],
						  ['lblEstoqueBrinde', 'txtEstoqueBrinde', 6, 2, -9],
						  ['lblPrazoObsoletoExtra', 'txtPrazoObsoletoExtra', 6, 2, -2]], null, 'INF');

    adjustElementsInForm([['lblGrupoImpostoEntradaID', 'selGrupoImpostoEntradaID', 22, 1],
                          ['lblMoedaEntradaID', 'selMoedaEntradaID', 7, 1],
                          ['lblGrupoImpostoSaidaID', 'selGrupoImpostoSaidaID', 22, 2],
                          ['lblMoedaSaidaID', 'selMoedaSaidaID', 7, 2],
                          ['lblClassificacaoFiscalID', 'selClassificacaoFiscalID', 17, 3, -2],
                          ['lblOrigemID', 'selOrigemID', 5, 3, -2],
                          ['lblProdutoImportado', 'chkProdutoImportado', 3, 3, -3],
                          ['lblNBSID', 'selNBSID', 12, 4, -2],
                          ['lblCEST', 'selCEST', 12, 4, -2]], null, 'INF');
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg() {
}

function fg_DblClick_Prg() {
    var empresa = getCurrEmpresaData();

    // Custo Medio
    if (keepCurrFolder() == 21066)
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 1), 10)));
        // Pedidos
    else if (keepCurrFolder() == 21067)
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 1), 10)));

        // Movimentos
    else if (keepCurrFolder() == 21068)
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 1), 10)));

    else if (keepCurrFolder() == 21069)
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 4), 10)));

    if ((keepCurrFolder() == 21065) && (fg.Editable == false) &&
         (fg.Rows > 1))
        openModalImpostos(1);
}

function fg_ChangeEdit_Prg() {

}
function fg_ValidateEdit_Prg() {

}

function fg_BeforeRowColChange_Prg() {

}

function fg_EnterCell_Prg() {

}

function fg_MouseUp_Prg() {

}

function fg_MouseDown_Prg() {

}

function fg_BeforeEdit_Prg() {

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD() {

}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@
    // Paginacao Inferior
    glb_aFoldersPaging = [21066, 21067, 21068];

    // Ajusta cor dos labels (Verde/Azul)
    lblProdutoImportado.style.color = 'green';
    lblNBSID.style.color = 'green';
    lblCEST.style.color = 'green';
    lblGrupoImpostoEntradaID.style.color = 'green';
    lblMoedaEntradaID.style.color = 'green';
    lblGrupoImpostoSaidaID.style.color = 'green';
    lblMoedaSaidaID.style.color = 'green';
    lblOrigemID.style.color = 'green';
    lblClassificacaoFiscalID.style.color = 'green';
    lblMargemContribuicao.style.color = 'green';
    lblMargemPadrao.style.color = 'green';
    lblMargemPublicacao.style.color = 'green';
    lblMargemMinima.style.color = 'green';
    lblPrecoMinimo.style.color = 'green';
    lblPrazoTroca.style.color = 'green';
    lblPrazoGarantia.style.color = 'green';
    lblPrazoAsstec.style.color = 'green';
    lblObservacao.style.color = 'green';
    lblPublica.style.color = 'green';
    lblPublicaPreco.style.color = 'green';
    lblPercentualPublicacaoEstoque.style.color = 'green';
    lblReportaFabricante.style.color = 'green';
    lblBNDES.style.color = 'green';
    lblBNDES.style.color = 'green';
    lblLojaOK.style.color = 'green';
    lblFichaTecnica.style.color = 'green';
    lblMultiploVenda.style.color = 'green';
    lblMultiploCompra.style.color = 'green';
    lblPrazoEstoque.style.color = 'green';
    lbldtInicio.style.color = 'green';
    lbldtControle.style.color = 'blue';
    lbldtFim.style.color = 'blue';

    lblDataEstoque.style.color = 'blue';
    lblEstoquePO.style.color = 'blue';
    lblEstoquePE.style.color = 'blue';
    lblEstoquePD.style.color = 'blue';
    lblEstoquePC.style.color = 'blue';
    lblEstoquePT.style.color = 'blue';
    lblEstoqueProprio.style.color = 'blue';
    lblEstoqueTE.style.color = 'blue';

    // Campos para valor numerico que tem minimo e maximo
    txtPrevisaoVendas.setAttribute('minMax', new Array(0, 999999), 1);
    txtMultiploVenda.setAttribute('minMax', new Array(0, 9999), 1);
    txtMultiploCompra.setAttribute('minMax', new Array(0, 9999), 1);
    txtPrazoEstoque.setAttribute('minMax', new Array(0, 999), 1);

    txtMargemContribuicao.setAttribute('minMax', new Array(-100, 100), 1);
    txtMargemPadrao.setAttribute('minMax', new Array(-100, 100), 1);
    txtMargemPublicacao.setAttribute('minMax', new Array(-100, 100), 1);
    txtMargemMinima.setAttribute('minMax', new Array(-100, 100), 1);

    txtAjusteBrinde.setAttribute('minMax', new Array(-99999, 99999), 1);

    // campo txtDataEstoque digitacao maxima: 12/12/2002 12:12:12
    txtDataEstoque.maxLength = 19;
    // e seleciona conteudo quando em foco
    txtDataEstoque.onfocus = selFieldContent;
    // e so aceita caracteres coerentes com data
    txtDataEstoque.onkeypress = verifyDateTimeNotLinked;
}
/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

    // Estoques/Data
    // Preenche campo txtDataEstoque apenas se o botao refresh
    // foi clicado pelo usuario ou a pasta se tornou
    // a pasta corrente
    if ((keepCurrFolder() == 21064) && (btnClicked == 'INFREFR')) {
        if (glb_dtPesConAtEstoque == null)
            txtDataEstoque.value = dso01JoinSup.recordset.Fields["DataEstoque"].value;
        else
            glb_dtPesConAtEstoque = null;
    }

    if ((keepCurrFolder() == 21080) && ((btnClicked == 'INFDET') || (btnClicked == 'INFREFR')))
    {
        glb_BtnFromFramWork = btnClicked;

        startDynamicCmbs();
    }

    // Mover esta funcao para os finais retornos de operacoes no
    // banco de dados
    finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    showBtnsEspecControlBar('inf', false, [1, 1, 1, 1]);

    // nenhum botao de impressao
    especBtnIsPrintBtn('inf', 0);

    // Pasta de estoques tem botao adicional
    if (pastaID == 21064) {
        txtDataEstoque.disabled = false;
        txtDataEstoque.readOnly = false;

        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Atualiza Estoques', '', '', '']);
    }
        // Pasta de fornecedores tem botao adicional
    else if (pastaID == 21065) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 0]);
        tipsBtnsEspecControlBar('inf', ['Preencher Fornecedor', 'Preencher CT', 'Impostos', '']);
    }
        // Custo Medio
    else if (pastaID == 21066) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Pedido', '', '', '']);
    }
        // Paginacao Inferior
    else if (pastaID == 21067) {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar', 'Detalhar Pedido', '', '']);
    }
        // Movimentos
    else if (pastaID == 21068) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 0]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar', 'Detalhar Pedido', 'Detalhar Asstec', '']);
    }
        // Numeros de serie
    else if (pastaID == 21069) {
        // mostra tres botoes especificos
        // sendo o primeiro de impressao
        especBtnIsPrintBtn('inf', 1);
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 0]);
        tipsBtnsEspecControlBar('inf', ['Imprimir', 'Pesquisa', 'Detalhar Pedido', '']);
    }
    else if (pastaID == 21070) {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Preencher combo de concorrente', 'Preencher combo de colaborador', '', '']);
    }
        // Pasta Gerenciamento de produtos
    else if (pastaID == 21072) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 0]);
        tipsBtnsEspecControlBar('inf', ['Procedimento', 'Tabela de Vari�veis de Produto', 'Simular Pre�os', '']);

        var nMRciWeb = dso01JoinSup.recordset['MRciWeb'].value;
        var nComprarExcesso = dso01JoinSup.recordset['ComprarExcesso'].value;

        txtMargemRealCI.title = 'Margem Real Web: ' + padNumReturningStr(roundNumber(nMRciWeb, 2), 2) + '%';

        if (nComprarExcesso >= 0) {
            txtComprarExcesso.value = nComprarExcesso;
            txtComprarExcesso.title = 'Quantidade a comprar';
            lblComprarExcesso.innerText = 'Comprar';
            setFldComprar('green');
        }
        else {
            txtComprarExcesso.value = Math.abs(nComprarExcesso);
            txtComprarExcesso.title = 'Quantidade em excesso';
            lblComprarExcesso.innerText = 'Excesso';
            setFldComprar('red');
        }
    }

    else if (pastaID == 21080) {
        if (!(btnClicked == 'INFINCL')) {
            setLabelGrupoImpostoEntrada(dso01JoinSup.recordset['GrupoImpostoEntradaID'].value);
            setLabelGrupoImpostoSaida(dso01JoinSup.recordset['GrupoImpostoSaidaID'].value);
            setLabelNBSID(dso01JoinSup.recordset['NBSID'].value);
            setLabelClassificacaoFiscal(dso01JoinSup.recordset['ClassificacaoFiscalID'].value);
        }
    }

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ((btnClicked.toUpperCase()).indexOf('SUP') >= 0)
        // usario clicou botao no control bar sup
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
    else
        // usario clicou botao no control bar inf
        sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID) {
    //@@
    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')
          || (btnClicked == 'SUPOK') || (btnClicked == 'SUPEST')
          || (btnClicked == 'INFREFR') || (btnClicked == 'INFOK')
          || (btnClicked == 'INFCANC')) {
        // botao 4 do sup depende do contexto    
        var contextoData = getCmbCurrDataInControlBar('sup', 1);

        if (contextoData[1] == 2131)
            setupEspecBtnsControlBar('sup', 'HHHHHHHHH');
        else
            setupEspecBtnsControlBar('sup', 'HHHHHDDDD');
    }
    else
        setupEspecBtnsControlBar('sup', 'DDDDDDDDD');

    // Cadastar novo produto
    if (btnClicked == 'SUPINCL')
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'newProduct()');

    /* Virou pagina��o
    if (folderID == 21066)
    {
        fg.Redraw = 0;
        fg.ColWidth(getColIndexByColKey(fg, '^PedItemID^dso02GridLkp^PedItemID^EhEncomenda*')) = 430;
        fg.Redraw = 2;
    }*/

    // Pasta de Gerenciamento de produtos d� refresh em cima
    // so se o refresh do pesqlist estiver habilitado
    if ((folderID == 21072) && (btnClicked == 'INFOK')) {
        var bChecked = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'chkRefrInf.checked');

        if (bChecked)
            __winTimerHandler = window.setInterval('refreshSup()', 10, 'JavaScript');
    }

    // Pasta de estoques tem botao adicional
    // sempre habilitado
    if (folderID == 21064)
        setupEspecBtnsControlBar('inf', 'HDDD');

    // Gerenciamento de produtos
    if (folderID == 21072) {
        setupEspecBtnsControlBar('inf', 'HHHDD');
    }

    // Fornecedores
    if (folderID == 21065) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'DDHD');
        else
            setupEspecBtnsControlBar('inf', 'DDHD');
    }

    // Paginacao Inferior
    // Custo Medio
    if (folderID == 21066) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }

    // Pedidos
    if (folderID == 21067) {
        // tres botoes especificos
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHDD');
        else
            setupEspecBtnsControlBar('inf', 'HDDD');
    }

    // Movimentos
    if (folderID == 21068) {
        // tres botoes especificos
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHHD');
        else
            setupEspecBtnsControlBar('inf', 'HDDD');
    }

    // Numero de serie
    if (folderID == 21069) {
        // tres botoes especificos
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHHD');
        else
            setupEspecBtnsControlBar('inf', 'DHDD');
    }
    if (folderID == 21072) {
        setlabels();

    }

    if ((folderID == 21080) && (btnClicked == 'INFOK')) {
        __winTimerHandler = window.setInterval('refreshSup()', 10, 'JavaScript');
    }

    if ((btnClicked == 'SUPEST') || (btnClicked == 'SUPOK') || (btnClicked == 'SUPREFR') || (btnClicked == 'SUPALT'))
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'showHideByTipoRegistro()');
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso) {
    // Habilita o botas especifico (1)
    if (folderID == 21065) // Fornecedores
    {
        setupEspecBtnsControlBar('inf', 'HDDD');
    }
    else if (folderID == 21070) // Preco dos Concorrentes
    {
        setupEspecBtnsControlBar('inf', 'HHDD');
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    // Estoques
    if (keepCurrFolder() == 21064) {
        if (btnClicked == 1)
            // Critica e normaliza o conteudo do campo txtDataEstoque
            if (criticAndNormTxtDataEstoque() == true) {
                // atualiza o estoque conforme datetime
                // definido pelo usuario
                __btn_REFR('inf');
            }
    }
        // Fornecedores
    else if (keepCurrFolder() == 21065) {
        if (btnClicked == 1)
            openModalFornecedor();
        else if (btnClicked == 2)
            openModalCodigoTaxa();
        else if (btnClicked == 3)
            openModalImpostos(1);
    }

        // Paginacao Inferior
        // Custo Medio
    else if ((keepCurrFolder() == 21066) && (btnClicked == 1)) {
        var empresa = getCurrEmpresaData();
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 1), 10)));
    }
        // Pedidos
    else if ((keepCurrFolder() == 21067) && (btnClicked == 1))
        loadModalOfPagingPesq();

    else if ((keepCurrFolder() == 21068) && (btnClicked == 1))
        loadModalOfPagingPesq();

    else if ((keepCurrFolder() == 21067) && (btnClicked == 2)) {
        var empresa = getCurrEmpresaData();
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 1), 10)));
    }
    else if ((keepCurrFolder() == 21068) && (btnClicked == 2)) {
        var empresa = getCurrEmpresaData();
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 1), 10)));
    }
    else if ((keepCurrFolder() == 21068) && (btnClicked == 3)) {
        var empresa = getCurrEmpresaData();
        sendJSCarrier(getHtmlId(), 'SHOWASSTEC', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 2), 10)));
    }

        // Usuario apertou o botao 1 - pasta de Numeros de serie - 21069
    else if ((keepCurrFolder() == 21069) && (btnClicked == 1))
        imprimeCodigoBarra();

        // Usuario apertou o botao 2 - pasta de Numeros de serie - 21069
    else if ((keepCurrFolder() == 21069) && (btnClicked == 2))
        openModalNumSeriePesq();

        // Usuario apertou o botao 3 - pasta de Numeros de serie - 21069
    else if ((keepCurrFolder() == 21069) && (btnClicked == 3)) {
        var empresa = getCurrEmpresaData();
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], parseInt(fg.TextMatrix(fg.Row, 4), 10)));
    }
        // Usuario apertou o botao 1 ou 2 - pasta de Preco dos Concorrentes - 21070
    else if ((keepCurrFolder() == 21070) && ((btnClicked == 1) || (btnClicked == 2)))
        openModalPessoas(btnClicked);

        // Usuario apertou o botao 1 - pasta de Gerenciamento de produtos
    else if (keepCurrFolder() == 21072) {
        if (btnClicked == 1)
            window.top.openModalControleDocumento('S', '', 740, null, '222', 'T');
        else if (btnClicked == 2)
            window.top.openModalControleDocumento('S', '', 74001, null, null, 'T');
        else if (btnClicked == 3)
            openModalSimulacaoPreco();
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    if (btnClicked == 'INFOK') {
        // Coloca ID do usuario logado no campo UsuarioID
        // ao gravar registro da pasta de Gerenciamento de produtos
        if (keepCurrFolder() == 21072) {
            dso01JoinSup.recordset.Fields['UsuarioID'].value = getCurrUserID();
        }

        if (keepCurrFolder() == 21066) // custo medio
        {
            grava_customedio();
            return true;
        }

        if (keepCurrFolder() == 21080) // fiscal
        {
            dso01JoinSup.recordset.Fields['UsuarioID'].value = getCurrUserID();
            setLabelGrupoImpostoEntrada();
            setLabelGrupoImpostoSaida();
            setLabelNBSID(0);
            setLabelClassificacaoFiscal(0);
        }
        if (keepCurrFolder() == 21065) //Fornecedor
        {
            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'ColaboradorID')) = getCurrUserID();
        }
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    if (idElement.toUpperCase() == 'MODALIMPOSTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }

        return null;
    }

    // Fornecedores
    if (keepCurrFolder() == 21065) {
        if (idElement.toUpperCase() == 'MODALFORNECEDORHTML') {
            if (param1 == 'OK') {
                writeFornecedorInGrid(param2);
                // esta funcao fecha a janela modal e destrava a interface
                // movida para a funcao acima
                //restoreInterfaceFromModal();
                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Altera��o');
                return 0;
            }
            else if (param1 == 'CANCEL') {
                // esta funcao fecha a janela modal e destrava a interface
                restoreInterfaceFromModal();

                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Altera��o');
                return 0;
            }
        }
        else if (idElement.toUpperCase() == 'MODALCODIGOTAXAHTML') {
            if (param1 == 'OK') {
                writeCodigoTaxaInGrid(param2);
                // esta funcao fecha a janela modal e destrava a interface
                restoreInterfaceFromModal();
                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Altera��o');
                return 0;
            }
            else if (param1 == 'CANCEL') {
                // esta funcao fecha a janela modal e destrava a interface
                restoreInterfaceFromModal();

                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Altera��o');
                return 0;
            }
        }
    }
    else if (keepCurrFolder() == 21070 && (idElement.toUpperCase() == 'MODALPESSOASHTML')) {
        if (param1 == 'OK') {
            // Escreve Colaborador ou Concorrente no grid de Preco dos Concorrentes
            writePessoaInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

        // Paginacao Inferior - Custo Medio
    else if ((keepCurrFolder() == 21066) && (idElement.toUpperCase() == 'MODALOFPAGINGINFHTML'))
        execPesqInPaging(param1, param2);

        // Paginacao Inferior - Pedidos
    else if ((keepCurrFolder() == 21067) && (idElement.toUpperCase() == 'MODALOFPAGINGINFHTML'))
        execPesqInPaging(param1, param2);

        // Paginacao Inferior - Movimentos
    else if ((keepCurrFolder() == 21068) && (idElement.toUpperCase() == 'MODALOFPAGINGINFHTML'))
        execPesqInPaging(param1, param2);

    else if (keepCurrFolder() == 21069 && (idElement.toUpperCase() == 'MODALPESQNUMSERIEHTML')) {
        if (param1 == 'OK') {
            restoreInterfaceFromModal();

            // param2 e o numero de serie

            glb_PesConInfTimerVar = window.setInterval('refreshGridNumSerie(' + '\'' + param2 + '\'' + ')', 10, 'JavaScript');

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALSIMULACAOPRECOHTML') {
        if (param1 == 'OK') {
            // Escreve Colaborador ou Concorrente no grid de Preco dos Concorrentes
            //writePessoaInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');

            glb_timerInfRefrInterval = window.setInterval('forceInfRefr()', 20, 'JavaScript');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid(folderID) {
    // Habilita o botas especifico (1,2)
    if (folderID == 21065) // Fornecedores
    {
        setupEspecBtnsControlBar('inf', 'HHDD');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid(folderID) {
    if (folderID == 21072) {
        setupEspecBtnsControlBar('inf', 'HHDD');

        // campos read-only
        txtMargemRealCI.readOnly = true;
        txtVendas0.readOnly = true;
        txtVendas1.readOnly = true;
        txtVendas2.readOnly = true;
        txtVendas3.readOnly = true;
        txtMediaVendas13.readOnly = true;
        txtPrevisaoVendasDiaria.readOnly = true;
        txtPrazoReposicao1.readOnly = true;
        txtPrazoPagamento1.readOnly = true;
        txtPrazoMinimo.readOnly = true;
        txtEstoqueMinimo.readOnly = true;
        txtEstoque.readOnly = true;
        txtPrazoEstoqueAjustado.readOnly = true;
        txtLoteCompra.readOnly = true;
        txtEstoqueWeb.readOnly = true;
        txtComprarExcesso.readOnly = true;
        txtDiasControle.readOnly = true;
        txtDiasFim.readOnly = true;
        txtdtUltimaEntrada.readOnly = true;
        txtdtUltimaSaida.readOnly = true;
        txtDiasSaida.readOnly = true;
    }
    else if (folderID == 21064) {
        // campos read-only
        txtEstoquePO.readOnly = true;
        txtEstoquePE.readOnly = true;
        txtEstoquePD.readOnly = true;
        txtEstoquePC.readOnly = true;
        txtEstoquePT.readOnly = true;
        txtEstoqueTO.readOnly = true;
        txtEstoqueTE.readOnly = true;
        txtEstoqueTD.readOnly = true;
        txtEstoqueTC.readOnly = true;
        txtEstoqueProprio.readOnly = true;
        txtEstoqueTerceiros.readOnly = true;
        txtEstoqueTotal.readOnly = true;
        txtEstoqueBrinde.readOnly = true;
        // Campos com Direitos Especiais
        direitoCampos();
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID) {
    // Inicializacao de variavel de grid em modo de paginacao

    // Paginacao Inferior
    if (folderID == 21066) // Custo Medio
        glb_scmbKeyPesqSel = 'adtCusto';

    if (folderID == 21067) // Pedidos
        glb_scmbKeyPesqSel = 'bdtMovimento';

    else if (folderID == 21068) // Movimentos
        glb_scmbKeyPesqSel = 'bdtMovimento';


    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder) {
    showBtnsEspecControlBar('inf', false, [1, 1, 1, 1]);

    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID) {

    //@@
    // Pasta de LOG Read Only
    if ((folderID == 20010) || (folderID == 21074)) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
        // Custo Medio, Pedidos, Movimentos, NumerosSerie
    else if ((folderID == 21066) || (folderID == 21067) || (folderID == 21068) || (folderID == 21069)) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de fornecedores para a pasta de fornecedores - 21065

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalFornecedor() {
    var htmlPath;
    var strPars = new String();
    var nSujeitoID = getCurrDataInControl('sup', 'selSujeitoID');
    var nRelacaoID = getCurrDataInControl('sup', 'txtRegistroID');

    // mandar os parametros para o servidor
    strPars = '?';
    strPars += 'nSujeitoID=';
    strPars += escape(nSujeitoID);
    strPars += '&nRelacaoID=';
    strPars += escape(nRelacaoID);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalfornecedor.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
    writeInStatusBar('child', 'cellMode', 'Fornecedores', true);
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de pesquisa para a pasta de Numeros de serie - 21069

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalNumSeriePesq() {
    var htmlPath;
    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalpesqnumserie.asp';
    showModalWin(htmlPath, new Array(280, 126));
    writeInStatusBar('child', 'cellMode', 'Pesquisa', true);
}

/********************************************************************
Funcao criada pelo programador.
Trata o codigo de barra da modal de pesquisa do mesmo

Parametro:
codigoBarra		- codigo de barra que retornou da pesquisa

Retorno:
nenhum
********************************************************************/
function refreshGridNumSerie(codigoBarra) {
    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset[' + '\'' + 'ObjetoID' + '\'' + '].value');

    if (glb_PesConInfTimerVar != null) {
        window.clearInterval(glb_PesConInfTimerVar);
        glb_PesConInfTimerVar = null;
    }

    // foi digitado o c�digo de barra
    if ((parseInt(codigoBarra.substr(0, 5), 10) == nProdutoID))
        glb_sNumeroSerie = codigoBarra.substr(5, codigoBarra.length);
    else
        glb_sNumeroSerie = codigoBarra;

    __btn_REFR('inf');
}

/********************************************************************
Funcao criada pelo programador.
Escreve Fornecedor e ID do Fornecedor vindo da janela modal de fornecedores.
Pasta de fornecedores - 21065

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeFornecedorInGrid(aFornecedor) {
    // o array aFornecedor contem: Fornecedor, FornecedorID

    fg.Redraw = 0;
    // Escreve nos campos
    fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, '^FornecedorID^dso01GridLkp^PessoaID^Fantasia*')) = aFornecedor[0];
    fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'FornecedorID*')) = aFornecedor[1];
    fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'ColaboradorID')) = getCurrUserID();

    fg.Redraw = 2;

    // esta funcao fecha a janela modal e destrava a interface
    restoreInterfaceFromModal();

    // ajusta barra de botoes
    setupEspecBtnsControlBar('inf', 'HHDD');
}

/********************************************************************
Funcao criada pelo programador.
Escreve Codigo Taxa e a Taxa vindo da janela modal de CodigoTaxa.
Pasta de fornecedores - 21065

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeCodigoTaxaInGrid(aCodigoTaxa) {
    // o array aCodigoTaxa contem: CodigoTaxaID, Taxa

    fg.Redraw = 0;
    // Escreve nos campos

    fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'CodigoTaxaID*')) = aCodigoTaxa[0];
    fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, '^RelPesConFornecID^dso02GridLkp^RelPesConFornecID^Cotacao*')) = aCodigoTaxa[1];

    fg.Redraw = 2;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de Pesquisa de Concorrente/Colaborador para a pasta de 
Preco dos concorrentes - 21070

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPessoas(nTipo) {
    var htmlPath;
    var strPars = new Array();
    var nPessoaID = getCurrDataInControl('sup', 'selSujeitoID');
    nPessoaID = (nPessoaID == null ? 0 : nPessoaID);

    // mandar os parametros para o servidor
    strPars = '?';
    strPars += 'nPessoaID=';
    strPars += escape(nPessoaID);
    strPars += '&nTipo=' + escape(nTipo);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalpessoas.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
}

/********************************************************************
Funcao criada pelo programador.
Escreve Concorrente/Colaborador e ID  vindo da janela modal de pessoas.
Pasta de Preco dos Concorrentes - 21070

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writePessoaInGrid(aPessoa) {
    // o array aPessoa contem: Pessoa, PessoaID, 
    //Tipo : 1 - Quando Concorrente
    //Tipo : 2 - Quando Colaborador

    fg.Redraw = 0;
    // Escreve nos campos
    if (aPessoa[2] == 1) {
        fg.TextMatrix(keepCurrLineInGrid(), 0) = aPessoa[0];
        fg.TextMatrix(keepCurrLineInGrid(), 7) = aPessoa[1];
    }
    else if (aPessoa[2] == 2) {
        fg.TextMatrix(keepCurrLineInGrid(), 5) = aPessoa[0];
        fg.TextMatrix(keepCurrLineInGrid(), 8) = aPessoa[1];
    }
    fg.Redraw = 2;
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos calculados (Variacao e Margem).
Pasta de Preco dos Concorrentes - 21070

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function setCalcFieldsPrecoConcorrentes() {
    var i;
    for (i = 1; i < fg.Rows; i++) {
        dso01Grid.recordset.MoveFirst();
        dso01Grid.recordset.Find('RelPesConConcorID' , fg.TextMatrix(i, 9));
        if (!dso01Grid.recordset.EOF) {
            fg.TextMatrix(i, 3) = dso01Grid.recordset['Variacao'].value;
            fg.TextMatrix(i, 4) = dso01Grid.recordset['Margem'].value;
        }
        else {
            fg.TextMatrix(i, 3) = '';
            fg.TextMatrix(i, 4) = '';
        }
    }
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos calculados DataFuso.
Pasta de Custo Medio

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function setCalcFieldsCustoMedio() {
    var i;
    for (i = 1; i < fg.Rows; i++) {
        dso01Grid.recordset.MoveFirst();
        dso01Grid.recordset.Find('RelPesConCustoID', fg.TextMatrix(i, fg.Cols - 1));
        if ((!dso01Grid.recordset.EOF) && (dso01Grid.recordset['DataFuso'].value != null))
            fg.TextMatrix(i, 0) = dso01Grid.recordset['DataFuso'].value;
        else
            fg.TextMatrix(i, 0) = '';
    }
}

function imprimeCodigoBarra() {
    var empresaData = getCurrEmpresaData();
    var empresaFantasia = empresaData[3];
    var nPrintBarcode = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrPrinterBarCode()');
    var nLabelGap = sendJSMessage(getHtmlId(), JS_WIDEMSG, 'LABELBARCODE_GAP', null);

    // Reseta o buffer de impressao
    oPrinter.ResetPrinterBarLabel(1, 9, 9);

    oPrinter.InsertBarLabelData(empresaFantasia.substr(0, 13),
                                removeCharFromString(fg.TextMatrix(fg.Row, 7), '"'),
                                fg.TextMatrix(fg.Row, 0) + fg.TextMatrix(fg.Row, 1));

    oPrinter.PrintBarLabels(nPrintBarcode, true, LABELBARCODE_HEIGHT, nLabelGap);
}

/********************************************************************
Funcao criada pelo programador.
Abre modal com impostos na pasta de compra e venda

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalImpostos(nPos) {
    var htmlPath;
    var strPars = new String();
    var nBC1 = 0;
    var nBC2 = 0;
    var sBC1;
    var sBC2;

    var sSubFrmCaller = (nPos == 0 ? 'SUP' : 'INF');

    sBC1 = (sSubFrmCaller == 'SUP' ? 'B6C1' : 'B3C1');

    sBC2 = (sSubFrmCaller == 'SUP' ? 'B6C2' : 'B3C2');

    nBC1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(\'' + sSubFrmCaller + '\',\'' + sBC1 + '\')'));

    nBC2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(\'' + sSubFrmCaller + '\',\'' + sBC2 + '\')'));

    var nDireito = 0;

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Modal Impostos (Produtos/Fornecedores)
    //Botao 3, 21065 SFI-Grid Fornecedores / Botao 5, 21060 SFS Relacao PesCon-> C1&&C2 -> Permite edi��o das colunas: 'Aliquota', 'Base'
    if ((nBC1 == 1) && (nBC2 == 1))
        nDireito = 1;

    // mandar os parametros para o servidor
    if (nPos == 0)
        strPars = '?nRelacaoID=' + escape(getCurrDataInControl('sup', 'txtRegistroID'));
    else if (nPos == 1) {
        if (fg.Row < 1)
            return null;

        strPars = '?nRelacaoID=' + escape(fg.ValueMatrix(fg.Row, fg.Cols - 1));
    }

    strPars += '&nPos=' + escape(nPos);
    strPars += '&nDireito=' + escape(nDireito);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalimpostos.asp' + strPars;

    if (nPos == 0)
        showModalWin(htmlPath, new Array(492, 271));
    else if (nPos == 1)
        showModalWin(htmlPath, new Array(240, 271));
}

/********************************************************************
Funcao criada pelo programador.
Abre modal com C�digos de Taxa

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCodigoTaxa() {
    if (fg.Row < 1)
        return null;

    var nEmpresaData = getCurrEmpresaData();
    var nEmpresaID = nEmpresaData[0];
    var htmlPath;
    var strPars = new String();

    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nFornecedorID=' + escape(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'FornecedorID*')));

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalcodigotaxa.asp' + strPars;

    showModalWin(htmlPath, new Array(0, 0));
}

/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos com direitos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function direitoCampos() {
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
    var bReadOnly;

    //DireitoEspecifico
    //Relacao PesCon->Produtos->Estoques
    //21060 SFS-RelacaoPesCon-> A1&&A2 -> Permide edi��o dos campos:'txtEstoqueSemNS','txtEstoqueControle','txtPrazoObsoletoExtra','chkControle','chkLiberaBrinde','txtAjusteBrinde'
    if ((nA1 == 1) && (nA2 == 1))
        bReadOnly = false;
    else
        bReadOnly = true;

    txtEstoqueSemNS.readOnly = bReadOnly;
    txtEstoqueControle.readOnly = bReadOnly;
    txtPrazoObsoletoExtra.readOnly = bReadOnly;
    chkControle.disabled = bReadOnly;
    chkLiberaBrinde.disabled = bReadOnly;
    txtAjusteBrinde.readOnly = bReadOnly;

}

/********************************************************************
Funcao criada pelo programador.
Trima, critica e normaliza (completa hota/min/seg) o campo txtDataEstoque.
Tambem da mensagens ao usuario e foca o campo se for o caso.
    
O que pode estar digitado no campo
1. Qualquer coisa nao valida
2. So a data valida ou nao
3. A data e a hora valida ou nao
4. A data, a hora e o minuto valida ou nao
5. A data, a hora, o minuto e o segundo valida ou nao

O campo sera completado para dd/mm/yyyy hh:mm:ss
ou para mm/dd/yyyy hh:mm:ss

Parametro:
nenhum

Retorno:
true e data/hora validas, caso contrario false
********************************************************************/
function criticAndNormTxtDataEstoque() {
    var retVal = false;
    var dataEstoque = '';
    var rExp, aDataEstoque;

    // trima conteudo do campo txtDataEstoque
    txtDataEstoque.value = trimStr(txtDataEstoque.value);

    // Obtem  texto do label associado ao campo
    var txtLblAss = labelAssociate('txtDataEstoque');

    // 1. Campo em branco nao aceita
    if (txtDataEstoque.value == '') {
        if (window.top.overflyGen.Alert('O campo ' + txtLblAss + ' est� em branco...') == 0)
            return null;

        txtDataEstoque.focus();
        return retVal;
    }

    dataEstoque = txtDataEstoque.value;

    // A funcao chkDataEx valida o campos se ele e uma data valida
    if (chkDataEx(dataEstoque, true) == false) {
        if (window.top.overflyGen.Alert('O campo ' + txtLblAss + ' n�o � v�lido...') == 0)
            return null;

        txtDataEstoque.focus();
        return retVal;
    }

    // Normaliza o conteudo do campo txtDataEstoque
    txtDataEstoque.value = normalizeDate_DateTime(txtDataEstoque.value);

    if ((txtDataEstoque.value != null) && (txtDataEstoque.value != ''))
        glb_dtPesConAtEstoque = normalizeDate_DateTime(txtDataEstoque.value, true);

    if (glb_dtPesConAtEstoque == null) {
        if (window.top.overflyGen.Alert('O campo ' + txtLblAss + ' n�o � v�lido...') == 0)
            return null;

        txtDataEstoque.focus();
        return retVal;
    }
    else
        retVal = true;

    return retVal;

}
function refreshSup() {
    if (__winTimerHandler != null) {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                  '__btn_REFR(' + '\'' + 'sup' + '\'' + ')');
}

/********************************************************************
Funcao criada pelo programador.
Ajusta caracteristicas graficas do campo comprar

Parametro:
sColor - Cor

Retorno:
nenhum
********************************************************************/
function setFldComprar(sColor) {
    with (txtComprarExcesso.style) {
        fontWeight = 'bold';
        fontSize = '10pt';
        color = sColor;
        //backgroundColor = 'yellow';        
    }
}

function openModalSimulacaoPreco() {
    var empresaData = getCurrEmpresaData();
    var nProdutoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                'dsoSup01.recordset[' + '\'' + 'ObjetoID' + '\'' + '].value');
    var htmlPath;
    var strPars = new String();
    strPars = '?EmpresaID=' + escape(empresaData[0]);
    strPars += '&nProdutoID=' + escape(nProdutoID);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalsimulacaopreco.asp' + strPars;

    showModalWin(htmlPath, new Array(517, 182));
}

function forceInfRefr() {
    if (glb_timerInfRefrInterval != null) {
        window.clearInterval(glb_timerInfRefrInterval);
        glb_timerInfRefrInterval = null;
    }

    __btn_REFR('inf');
}

function grava_customedio() {


    var nRelPesConCustoID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesConCustoID'));
    var strPars = new String();

    strPars = '?nRelPesConCustoID=' + escape(nRelPesConCustoID);
    strPars += '&sObservacao=' + escape(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Observacao')));

    dsoGravaCustoMedio.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/gravacustomedio.aspx' + strPars;
    dsoGravaCustoMedio.ondatasetcomplete = dsoGravaCustoMedio_DSC;
    dsoGravaCustoMedio.refresh();

}


function dsoGravaCustoMedio_DSC() {
    lockInterface(false);
    __btn_REFR('inf');
}
/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    if (cmbID == 'selGrupoImpostoEntradaID')
        setLabelGrupoImpostoEntrada(cmb.value);
    else if (cmbID == 'selGrupoImpostoSaidaID')
        setLabelGrupoImpostoSaida(cmb.value);
    else if (cmbID == 'selClassificacaoFiscalID')
        setLabelClassificacaoFiscal(cmb.value);
    else if (cmbID == 'selOrigemID')
        setLabelOrigem(cmb.value);
    else if (cmbID == 'selNBSID')
        setLabelNBSID(cmb.value);

}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblGrupoImpostoEntradaID, de acordo com o ID do
Grupo de Imposto de Entrada

Parametro:
nID   ID do Grupo de Imposto de Entrada

Retorno:
nenhum
********************************************************************/

function setLabelGrupoImpostoEntrada(nID) {
    if (nID == null)
        nID = '';

    var sLabel = 'Imposto Entrada ' + nID;
    lblGrupoImpostoEntradaID.style.width = sLabel.length * FONT_WIDTH;
    lblGrupoImpostoEntradaID.innerText = sLabel;
}

function setLabelGrupoImpostoSaida(nID) {
    if (nID == null)
        nID = '';

    var sLabel = 'Imposto Saida ' + nID;
    lblGrupoImpostoSaidaID.style.width = sLabel.length * FONT_WIDTH;
    lblGrupoImpostoSaidaID.innerText = sLabel;
}

function setLabelNBSID(nNBSID) {
    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.setFilter('Indice=5 AND fldID=' + nNBSID);

        if (!dsoEstaticCmbs.recordset.EOF) {
            lblNBSID.title = dsoEstaticCmbs.recordset['Descricao'].value;
            selNBSID.title = dsoEstaticCmbs.recordset['Descricao'].value;
        }
        else {
            lblNBSID.title = '';
            selNBSID.title = '';
        }

        dsoEstaticCmbs.recordset.setFilter('');
    }
}


function setLabelClassificacaoFiscal(nClassificacaoFiscalID) {
    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.setFilter('Indice=3 AND fldID=' + nClassificacaoFiscalID);

        if (!dsoEstaticCmbs.recordset.EOF) {
            lblClassificacaoFiscalID.title = dsoEstaticCmbs.recordset['Descricao'].value;
            selClassificacaoFiscalID.title = dsoEstaticCmbs.recordset['Descricao'].value;
        }
        else {
            lblClassificacaoFiscalID.title = '';
            selClassificacaoFiscalID.title = '';
        }

        dsoEstaticCmbs.recordset.setFilter('');
    }
}

function setLabelOrigem(nOrigemID) {
    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.setFilter('Indice=7 AND fldID=' + nOrigemID);

        if (!dsoEstaticCmbs.recordset.EOF) {
            lblOrigemID.title = dsoEstaticCmbs.recordset['Descricao'].value;
            selOrigemID.title = dsoEstaticCmbs.recordset['Descricao'].value;
        }
        else {
            lblCodigoListaServicoID.title = '';
            selCodigoListaServicoID.title = '';
        }

        dsoEstaticCmbs.recordset.setFilter('');
    }
}
/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selSujeitoID, selObjetoID, 
    //  selGrupoImpostoEntradaID,selGrupoImpostoSaidaID,
    //  selMoedaEntradaID, selMoedaSaidaID)
    // e verificacao de estoque do produto

    glb_CounterCmbsDynamics = 1;

    var nClassificacaoFiscalID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'ClassificacaoFiscalID' + '\'' + '].value');

    // parametrizacao do dso dsoCmbDynamic01 (designado para selCEST)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL =   'SELECT SPACE(0) AS fldID, SPACE (0) AS fldName ' +
                            'UNION ALL ' +
                            'SELECT a.ConCestID AS fldID, a.Cest AS fldName ' +
                                'FROM Conceitos_Cest a WITH(NOLOCK) ' +
                                'WHERE a.ConceitoID =  ' + nClassificacaoFiscalID;

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

}

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selCEST];
    var aDSOsDunamics = [dsoCmbDynamic01];
    var aCmbBar = new Array();
    var iCount = 0;
    var folderID = 21080;

    clearComboEx(['selCEST']);

    var cestIndex = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selCEST.selectedIndex');

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        // atualiza a data corrente e calcula os campos calculados
        for (iCount = 0; iCount <= aCmbsDynamics.length - 1; iCount++) {
            oldDataSrc = aCmbsDynamics[iCount].dataSrc;
            oldDataFld = aCmbsDynamics[iCount].dataFld;
            aCmbsDynamics[iCount].dataSrc = '';
            aCmbsDynamics[iCount].dataFld = '';


            if (!((aDSOsDunamics[iCount].recordset.BOF) && (aDSOsDunamics[iCount].recordset.EOF)))
                aDSOsDunamics[iCount].recordset.MoveFirst();

            while (!aDSOsDunamics[iCount].recordset.EOF) {
                optionStr = aDSOsDunamics[iCount].recordset['fldName'].value;
                optionValue = aDSOsDunamics[iCount].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[iCount].add(oOption);
                aDSOsDunamics[iCount].recordset.MoveNext();
            }

            aCmbsDynamics[iCount].dataSrc = oldDataSrc;
            aCmbsDynamics[iCount].dataFld = oldDataFld;
        }

        // volta para a automacao
        finalOfInfCascade(glb_BtnFromFramWork);

    }
    selCEST.selectedIndex = cestIndex;
    treatsControlsBars('INFOK', folderID, null, null);
    return null;
}