/********************************************************************
relpessoasconceitossup01.js

Library javascript para o relpessoasconceitossup01.asp
********************************************************************/
// VARIAVEIS GLOBAIS ************************************************
// controla o retorno do servidor dos dados de combos dinamicos
// (selSujeitoID e selObjetoID)
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFramWork;
// guarda id do combo de pula a preencher
var glb_cmbLupaID;
// Guarda o tipo do registro selecionado para ser usado no select
// que traz o registro devolta na gravacao, pois este form tem
// Identity calculado por trigger
var glb_RelPesConTipoRegID = null;
// Estoque de produto
var glb_ProdutoEstoque = 0;
var glb_TimerPedidoVar = null;

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoCmbDynamic02 = new CDatatransport('dsoCmbDynamic02');
var dsoCmbDynamic03 = new CDatatransport('dsoCmbDynamic03');
var dsoDynamicDate = new CDatatransport('dsoDynamicDate');
var dsoCmbsLupa = new CDatatransport('dsoCmbsLupa');
var dsoVerificacao = new CDatatransport('dsoVerificacao');
var dsoProdStock = new CDatatransport('dsoProdStock');
var dsoPerfilUsuario = new CDatatransport('dsoPerfilUsuario');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )

FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);


    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    // esperando variavel global da empresa logada para montar os demais
    // combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selGrupoImpostoEntradaID', '2'],
                          ['selGrupoImpostoSaidaID', '2'],
                          ['selMoedaEntradaID', '3'],
                          ['selMoedaSaidaID', '3'],
                          ['selCodigoTaxaID', '4'],
                          ['selClassificacaoFiscalID', '5'],
                          ['selServicoID', '6'],
                          ['selOrigemID', '7'],
                          ['selNBSID', '8']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/relpessoasconceitosinf01.asp',
                              SYS_PAGESURLROOT + '/basico/relacoes/relpessoaseconceitos/relpessoasconceitospesqlist.asp');


    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS


    linkDivsAndSubForms('divSup01_01', ['divSup02_01', 'divSup02_02', 'divSup02_03'], [61, 63, 62]);

    windowOnLoad_2ndPart();


    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RelacaoID';


    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoRelacaoID';
}

function setupPage() {
    adjustDivs('sup', [[1, 'divSup01_01'],
                      [2, 'divSup02_01'],
                      [2, 'divSup02_02'],
                      [2, 'divSup02_03']]);

    /*********** RELACAO ENTRE PESSOAS E CONCEITO DIV 1 - divSup01_01 ****************/
    adjustElementsInForm([['lblregistroid', 'txtregistroid', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1, -2],
                          ['lblTipoRegistroID', 'selTipoRegistroID', 20, 1, -4],
                          ['lblSujeitoID', 'selSujeitoID', 20, 1, -2],
                          ['btnFindSujeito', 'btn', 21, 1],
                          ['lblObjetoID', 'selObjetoID', 25, 1, -2],
                          ['btnFindObjeto', 'btn', 21, 1],
                          ['lblCodigoAnterior', 'txtCodigoAnterior', 9, 1, -3]]);

    /******************* Produto DIV 2 - divSup02_01 ***************************/
    adjustElementsInForm([['lblGrupoImpostoEntradaID', 'selGrupoImpostoEntradaID', 22, 2],
                          ['lblMoedaEntradaID', 'selMoedaEntradaID', 7, 2],
                          ['lblCustoMedio', 'txtCustoMedio', 11, 2],
                          ['lblFatorInternacaoEntrada', 'txtFatorInternacaoEntrada', 6, 2],
                          ['lblFatorInternacaoSaida', 'txtFatorInternacaoSaida', 6, 2],
                          ['lbldtMediaPagamento', 'txtdtMediaPagamento', 10, 2],
                          ['lblDiasPagamento', 'txtDiasPagamento', 4, 2, -7],
                          ['lblMoedaEmpresa', 'txtMoedaEmpresa', 5, 2, -5],
                          ['lblCustoContabilMedio', 'txtCustoContabilMedio', 11, 2, -5],
                          ['lblPercentualCustoMedio', 'txtPercentualCustoMedio', 8, 2, -11],//Campo Novo                          
                          ['lblGrupoImpostoSaidaID', 'selGrupoImpostoSaidaID', 22, 3],
                          ['lblMoedaSaidaID', 'selMoedaSaidaID', 7, 3],
                          ['lblPreco', 'txtPreco', 11, 3],
                          ['lblValorTransferencia', 'txtValorTransferencia', 11, 3],
                          ['lblCodigoTaxaID', 'selCodigoTaxaID', 5, 3, -22],
//                        ['lblFatorInternacao', 'txtFatorInternacao', 6, 3, -5],
                          ['lblNBSID', 'selNBSID', 17, 3, -2],
                          ['lblCEST', 'selCEST', 17, 3, -2],
                          ['lblCategoria', 'selCategoria', 5, 4],
                          //['lblOrigem','selOrigem',5,4,-2],
                          ['lblOrigemID', 'selOrigemID', 5, 4, -2],
                          ['lblClassificacaoFiscalID', 'selClassificacaoFiscalID', 17, 4, -2],
                          ['lblProdutoImportado', 'chkProdutoImportado', 3, 4, -3],
                          ['lblCompraAutomatica', 'chkCompraAutomatica', 3, 4, -3],
                          ['lblBloqueiaProduto', 'chkBloqueiaProduto', 3, 4, -3],
                          ['lblProdutoBloqueado', 'chkProdutoBloqueado', 3, 4, -3],
                          ['lblTipoGatilhamento', 'selTipoGatilhamento', 18, 4, -3],
                          ['lblNumeroSerieNF', 'chkNumeroSerieNF', 3, 4, -3],
                          ['lblArredondaPreco', 'chkArredondaPreco', 3, 4, -3],
                          ['lblFichaTecnica', 'chkFichaTecnica', 3, 4, -3],
                          ['lblPublica', 'chkPublica', 3, 4, -3],
                          ['lblPublicaPreco', 'chkPublicaPreco', 3, 4, -3],
                          ['lblPercentualPublicacaoEstoque', 'txtPercentualPublicacaoEstoque', 4, 4, -3],
                          ['lblReportaFabricante', 'chkReportaFabricante', 3, 4, -5],
                          ['lblBNDES', 'chkBNDES', 3, 4, -7],
                          ['lblEstoqueFisico', 'txtEstoqueFisico', 6, 5, -1, -8],
                          ['lblEstoqueDisponivel', 'txtEstoqueDisponivel', 6, 5, -1],
                          ['lblReservaC', 'txtReservaC', 6, 5, -14],
                          ['lblReservaCC', 'txtReservaCC', 6, 5, -1],
                          ['lblReservaV', 'txtReservaV', 6, 5, -1],
                          ['lblReservaVC', 'txtReservaVC', 6, 5, -1],
                          ['lblLoteFis', 'txtLoteFis', 6, 5, -1],
                          ['lblLoteDisp', 'txtLoteDisp', 6, 5, -1],
                          ['lblPrevisaoDisponibilidade', 'txtdtPrevisaoDisponibilidade', 10, 5, -1],
                          ['lblObservacao', 'txtObservacao', 25, 5, -13],
                          ['lblFIE_Excecao', 'txtFIE_Excecao', 6, 5]]);

    txtPercentualPublicacaoEstoque.setAttribute('minMax', new Array(0, 100), 1);

    /************************** MARCA DIV 2 - divSup02_02 *****************************/
    adjustElementsInForm([['lblOrdem', 'txtOrdem', 3, 2],
                          ['lblEhProprietario', 'chkEhProprietario', 3, 2]], ['lblHrMarca', 'hrMarca']);

    /************************** FABRICANTE DIV 3 - divSup02_03 *****************************/
    adjustElementsInForm([
                          ['lblAgrupa', 'chkAgrupa', 3, 2]]);

}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    if (param1 == 'SHOWRELPESCON') {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;


        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();


        if (param2[0] != empresa[0])
            return null;

        window.top.focus();


        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);


        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.

    // labels Verdes
    lblCodigoAnterior.style.color = 'green';
    lblGrupoImpostoEntradaID.style.color = 'green';
    lblMoedaEntradaID.style.color = 'green';
    lblGrupoImpostoSaidaID.style.color = 'green';
    lblMoedaSaidaID.style.color = 'green';
    lblCodigoTaxaID.style.color = 'green';
    lblOrigemID.style.color = 'green';
    lblCategoria.style.color = 'green';
    lblClassificacaoFiscalID.style.color = 'green';
    lblNBSID.style.color = 'green';
    lblCEST.style.color = 'green';
    lblServicoID.style.color = 'green';
    lblCompraAutomatica.style.color = 'green';
    lblProdutoImportado.style.color = 'green';
    lblBloqueiaProduto.style.color = 'green';
    lblTipoGatilhamento.style.color = 'green';
    lblNumeroSerieNF.style.color = 'green';
    lblArredondaPreco.style.color = 'green';
    lblPublica.style.color = 'green';
    lblPublicaPreco.style.color = 'green';
    lblPercentualPublicacaoEstoque.style.color = 'green';
    lblFichaTecnica.style.color = 'green';
    lblReportaFabricante.style.color = 'green';
    lblBNDES.style.color = 'green';
    lblPrevisaoDisponibilidade.style.color = 'green';
    lblObservacao.style.color = 'green';
    lblPercentualCustoMedio.style.color = 'green';
    lblFIE_Excecao.style.color = 'green';

    chkProdutoBloqueado.disabled = true;
    chkProdutoImportado.disabled = true;

    /*Por enquanto s� impressora fiscal ter� o bit ligado automaticamente pela trigguer. DCS*/
    chkNumeroSerieNF.disabled = true;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    // e verifica se o produto tem estoque
    glb_BtnFromFramWork = btnClicked;
    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
        startDynamicCmbs();

    else
        // volta para a automacao    
        finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) {
    // seta botao de impressao
    especBtnIsPrintBtn('sup', 1);

    // mostra quatro botoes especificos
    showBtnsEspecControlBar('sup', false, [1, 1, 1, 1, 1, 1, 1, 1]);
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Sujeito', 'Detalhar Objeto', 'Detalhar Lista de Pre�os', 'Kardex', 'Impostos']);

    if (btnClicked == 'SUPINCL') {
        // funcao que controla campos read-only
        controlReadOnlyFields();


        // Habilita desabilita campo frete chkFrete
        direitoCampos();
    }


    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    clearAndLockCmbsDynamics(btnClicked);

    // Ajusta os labels de sujeito e objeto
    // Apenas nos forms de relacoes
    changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID', 'lblObjetoID', null);


    if (!(btnClicked == 'SUPINCL')) {
        setLabelRelacao(dsoSup01.recordset['TipoRelacaoID'].value);
        setLabelGrupoImpostoEntrada(dsoSup01.recordset['GrupoImpostoEntradaID'].value);
        setLabelGrupoImpostoSaida(dsoSup01.recordset['GrupoImpostoSaidaID'].value);
        setLabelTipoGatilhamento(dsoSup01.recordset['TipoGatilhamento'].value);
        setLabelClassificacaoFiscal(dsoSup01.recordset['ClassificacaoFiscalID'].value);
        setLabelServico(dsoSup01.recordset['ServicoID'].value);
        setLabelOrigem(dsoSup01.recordset['OrigemID'].value);
        setLabelNBS(dsoSup01.recordset['NBSID'].value);
    }


    showHideByTipoRegistro();

    // Problema de cadastro
    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR'))
        auditoria();

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;


    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario


    if (cmbID == 'selTipoRegistroID') {
        clearComboEx(['selSujeitoID', 'selObjetoID']);
        selSujeitoID.disabled = true;
        selObjetoID.disabled = true;
        lockBtnLupa(btnFindSujeito, true);
        lockBtnLupa(btnFindObjeto, true);

        if (cmbID.selectedIndex != -1)
            lockBtnLupa(btnFindSujeito, false);


        setLabelRelacao(cmb.value);


        if ((cmb.value == 61) && (txtRegistroID.value == ''))
            chkReportaFabricante.checked = true;
    }
    else if (cmbID == 'selSujeitoID') {
        clearComboEx(['selObjetoID']);
        selObjetoID.disabled = true;
        lockBtnLupa(btnFindObjeto, true);


        if (cmb.selectedIndex != -1)
            // destrava o botao de lupa do objeto
            lockBtnLupa(btnFindObjeto, false);
    }
    else if (cmbID == 'selGrupoImpostoEntradaID')
        setLabelGrupoImpostoEntrada(cmb.value);

    else if (cmbID == 'selGrupoImpostoSaidaID')
        setLabelGrupoImpostoSaida(cmb.value);

    else if (cmbID == 'selTipoGatilhamento')
        setLabelTipoGatilhamento(cmb.value);
    else if (cmbID == 'selClassificacaoFiscalID')
        setLabelClassificacaoFiscal(cmb.value);
    else if (cmbID == 'selServicoID')
        setLabelServico(cmb.value);
    else if (cmbID == 'selOrigemID')
        setLabelOrigem(cmb.value);
    else if (cmbID == 'selNBSID')
        setLabelNBS(cmb.value);

    // Ajusta os labels de sujeito e objeto
    // Apenas nos forms de relacoes
    if ((cmbID == 'selTipoRegistroID') || (cmbID == 'selSujeitoID') || (cmbID == 'selObjetoID'))
        changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID',
                           'lblObjetoID', 'selTipoRegistroID');

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID')
        if (adjustSupInterface() == null)
            showHideByTipoRegistro(true);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    // Ativo ou Pre-Lancamento

    if (dsoSup01.recordset['TipoRelacaoID'].value == 61) {
        verifyRelacaoInServer(currEstadoID, newEstadoID);
        return true;
    }

    //@@ prossegue a automacao
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    //@@
    var cmbID = '';
    var nTipoRelacaoID = '';
    var nRegExcluidoID = '';


    if (btnClicked.id == 'btnFindSujeito') {
        nTipoRelacaoID = selTipoRegistroID.value;
        nRegExcluidoID = '';
        showModalRelacoes(window.top.formID, 'S', 'selSujeitoID', 'SUJ', getLabelNumStriped(lblSujeitoID.innerText), nTipoRelacaoID, nRegExcluidoID);
        return null;
    }
    else if (btnClicked.id == 'btnFindObjeto') {
        nTipoRelacaoID = selTipoRegistroID.value;
        nRegExcluidoID = selSujeitoID.value;

        showModalRelacoes(window.top.formID, 'S', 'selObjetoID', 'OBJ', getLabelNumStriped(lblObjetoID.innerText), nTipoRelacaoID, nRegExcluidoID);
        return null;

    }
    // Nao mexer - Inicio de automacao ==============================
    // Invoca janela modal
    loadModalOfLupa(cmbID, null);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs() {
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selSujeitoID, selObjetoID, selCEST)
    // e verificacao de estoque do produto

    glb_CounterCmbsDynamics = 3;

    // Dados da Empresa Logada (EmpresaID,EmpresaPaisID,EmpresaCidadeID)
    var nEmpresaData = getCurrEmpresaData();
    var nProdutoID = dsoSup01.recordset['ObjetoID'].value;

    // Estoque do produto
    var aCmbBar = new Array();


    aCmbBar = getCmbCurrDataInControlBar('sup', 1);


    if ((aCmbBar[2].toUpperCase() == 'PRODUTOS') && (nProdutoID != null)) {
        glb_CounterCmbsDynamics += 1;

        // parametrizacao do dso dsoProdStock
        setConnection(dsoProdStock);

        dsoProdStock.SQL = 'SELECT dbo.fn_Produto_Estoque(' + nEmpresaData[0] + ', ' +
			nProdutoID + ', 361, NULL, GETDATE(), NULL, 375, -1) AS ProdStock';
        dsoProdStock.ondatasetcomplete = dsoCmbDynamic_DSC;
        dsoProdStock.Refresh();
    }

    // parametrizacao do dso dsoCmbDynamic01 (designado para selSujeitoID)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT PessoaID AS fldID, Fantasia AS fldName ' +
                          'FROM Pessoas ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['SujeitoID'].value;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    // parametrizacao do dso dsoCmbDynamic02 (designado para selObjetoID)
    setConnection(dsoCmbDynamic02);

    dsoCmbDynamic02.SQL = 'SELECT ConceitoID AS fldID, Conceito AS fldName ' +
                          'FROM Conceitos ' +
                          'WHERE ConceitoID = ' + dsoSup01.recordset['ObjetoID'].value;
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();

    // parametrizacao do dso dsoCmbDynamic03 (designado para selCEST)
    setConnection(dsoCmbDynamic03);

    dsoCmbDynamic03.SQL = 'SELECT SPACE(0) AS fldID, SPACE (0) AS fldName ' +
                            'UNION ALL ' +
                            'SELECT a.ConCestID AS fldID, a.Cest AS fldName ' +
                                'FROM Conceitos_Cest a WITH(NOLOCK) ' +
                                'WHERE a.ConceitoID =  ' + dsoSup01.recordset['ClassificacaoFiscalID'].value;

    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic03.Refresh();

}

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selSujeitoID, selObjetoID, selCEST];
    var aDSOsDunamics = [dsoCmbDynamic01, dsoCmbDynamic02, dsoCmbDynamic03];
    var aCmbBar = new Array();
    var iCount = 0;

    clearComboEx(['selSujeitoID', 'selObjetoID', 'selCEST']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        // atualiza a data corrente e calcula os campos calculados
        for (iCount = 0; iCount <= aCmbsDynamics.length - 1; iCount++) {
            oldDataSrc = aCmbsDynamics[iCount].dataSrc;
            oldDataFld = aCmbsDynamics[iCount].dataFld;
            aCmbsDynamics[iCount].dataSrc = '';
            aCmbsDynamics[iCount].dataFld = '';


            if (!((aDSOsDunamics[iCount].recordset.BOF) && (aDSOsDunamics[iCount].recordset.EOF)))
                aDSOsDunamics[iCount].recordset.MoveFirst();

            while (!aDSOsDunamics[iCount].recordset.EOF) {
                optionStr = aDSOsDunamics[iCount].recordset['fldName'].value;
                optionValue = aDSOsDunamics[iCount].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[iCount].add(oOption);
                aDSOsDunamics[iCount].recordset.MoveNext();
            }

            aCmbsDynamics[iCount].dataSrc = oldDataSrc;
            aCmbsDynamics[iCount].dataFld = oldDataFld;
        }


        // Estoque do produto
        aCmbBar = getCmbCurrDataInControlBar('sup', 1);

        if (aCmbBar[2].toUpperCase() == 'PRODUTOS') {
            if (dsoSup01.recordset['ObjetoID'].value != null) {
                glb_ProdutoEstoque = dsoProdStock.recordset.Fields[0].value;
            }
            else
                glb_ProdutoEstoque = 0;
        }

        // volta para a automacao
        finalOfSupCascade(glb_BtnFromFramWork);

    }
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    if (controlBar == 'SUP') {
        var empresa = getCurrEmpresaData();

        // Documentos
        if (btnClicked == 1) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
            // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();
            // Procedimento
        else if (btnClicked == 3)
            window.top.openModalControleDocumento('S', '', 740, null, '22', 'T');
            // Detalhar sujeito
        else if (btnClicked == 4) {
            // Manda o id da pessoa a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], selSujeitoID.value));
        }
            // Detalhar objeto
        else if (btnClicked == 5) {
            // Manda o id do conceito a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWCONCEITO', new Array(empresa[0], selObjetoID.value));
        }
            // Lista de pre�os
        else if (btnClicked == 6)
            sendJSCarrier(getHtmlId(), 'SHOWLISTAPRECOS', new Array(empresa[0], dsoSup01.recordset['ObjetoID'].value));
            /*// Formacao de preco
            else if (btnClicked == 8)
                openModalFormacaoPreco();*/ /*VAI SAIR DAQUI!*/
            // Kardex
        else if (btnClicked == 7)
            openModalKardex(txtRegistroID.value);
            // Impostos
        else if (btnClicked == 8)
            sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'openModalImpostos(0)');

    }
}




function getNCM() {
    var NCM = selClassificacaoFiscalID.options(selClassificacaoFiscalID.selectedIndex).innerText;

    if (NCM != null && NCM.length > 0) {
        return NCM;

    }
    else {
        return null;

    }
}

function getNBS() {
    var NBS = selNBSID.options(selNBSID.selectedIndex).innerText;

    if (NBS != null && NBS.length > 0) {
        return NBS;
    }
    else {
        return null;
    }
}


/********************************************************************
********************************************************************/
function detalhaNCMSuperior() {
    var NCM = getNCM();


    if (NCM != null) {
        strPars = '?sNCM=' + NCM.substr(0, 4);
        htmlPath = SYS_ASPURLROOT + '/pcm/conceitos/serverside/imagemncm.asp' + strPars;
        window.open(htmlPath);
    } else {
        alert("O imposto escolhido n�o � uma classifica��o fiscal.");


    }

}

function openModalFormacaoPreco() {
    var htmlPath;
    var strPars = new String();
    var nWidth = 466;
    var nHeight = 470;


    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)    
    strPars = '?sCaller=' + escape('S');
    strPars += '&bDireitoAlterar=' + escape(1);
    strPars += '&bDireitoExcluir=' + escape(1);
    strPars += '&sEmpresaUFID=' + getCurrEmpresaData()[4];

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalalteracaopreco.asp' + strPars;


    showModalWin(htmlPath, new Array(nWidth, nHeight));
}

function openModalKardex(RegistroID) {
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 543;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)    
    strPars = '?sCaller=' + escape('S');
    strPars += '&nRegistroID=' + escape(RegistroID);
    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalkardex.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {

    // Identity Calculado
    if ((controlBar == 'SUP') && (btnClicked == 'SUPOK'))
        glb_RelPesConTipoRegID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;

    if (btnClicked == 'SUPINCL') {
        setLabelRelacao();
        setLabelGrupoImpostoEntrada();
        setLabelGrupoImpostoSaida();
        setLabelTipoGatilhamento();
        setLabelClassificacaoFiscal(0);
        setLabelNBS(0);
        setLabelServico(0);
        setLabelOrigem(0);
        Lockedlabels(1);
    }

    if (btnClicked == 'SUPALT')
        Lockedlabels(dsoSup01.recordset['EstadoID'].value);

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');


            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;

        }
    }
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de relacoes
    if (idElement.toUpperCase() == 'MODALRELACOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();


            fillFieldsByRelationModal(param2);


            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');


            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;

        }
    }
    // Modal de alteracao de preco
    if (idElement.toUpperCase() == 'MODALALTERACAOPRECOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal do Kardex
    if (idElement.toUpperCase() == 'MODALKARDEXHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();


            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            if (param2)
                glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            if (param2)
                glb_TimerPedidoVar = window.setInterval('refreshSup()', 10, 'JavaScript');

            // nao mexer
            return 0;
        }



    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    // funcao que controla campos read-only
    controlReadOnlyFields();

    // Habilita desabilita campo frete chkFrete
    direitoCampos();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {
    // seta botao de impressao
    // especBtnIsPrintBtn('sup', 1);

    // mostra quatro botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Sujeito', 'Detalhar Objeto', 'Detalhar Lista de Pre�os', 'Kardex', 'Impostos']);
}

/********************************************************************
Funcao criada pelo programador.
Limpa e trava os combos dinamicos quando o usuario clicar em inclusao

Parametro:
btnClicked  -> Ultimo botao clicado da barra superior

Retorno:
nenhum
********************************************************************/
function clearAndLockCmbsDynamics(btnClicked) {
    if (btnClicked == 'SUPINCL') {
        clearComboEx(['selSujeitoID', 'selObjetoID']);
        selSujeitoID.disabled = true;
        selObjetoID.disabled = true;


        // trava o botao de lupa do sujeito
        lockBtnLupa(btnFindSujeito, true);


        // trava o botao de lupa do objeto
        lockBtnLupa(btnFindObjeto, true);

    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio


    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&nIdiomaID=' + escape(getDicCurrLang());

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/modalpages/modalprint.asp' + strPars;
    showModalWin(htmlPath, new Array(346, 200 + 34));
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields() {
    treatCmbTipoGatilhamento();


    txtFatorInternacaoEntrada.readOnly = true;
    txtFatorInternacaoSaida.readOnly = true;
    txtDiasPagamento.readOnly = true;
    txtMoedaEmpresa.readOnly = true;
    txtPreco.readOnly = true;
    txtValorTransferencia.readOnly = true;
    txtEstoqueFisico.readOnly = true;
    txtEstoqueDisponivel.readOnly = true;
    txtReservaC.readOnly = true;
    txtReservaCC.readOnly = true;
    txtReservaV.readOnly = true;
    txtReservaVC.readOnly = true;
    Lockedlabels(dsoSup01.recordset['EstadoID'].value);

    txtFIE_ExcecaoDireitos();
}

/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos com direitos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function direitoCampos() {
    var nE1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E1' + '\'' + ')'));

    var nE2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E2' + '\'' + ')'));

    var nUserID = getCurrUserID();

    var bReadOnly2;

    bReadOnly2 = true;

    txtCustoMedio.readOnly = bReadOnly2;
    txtdtMediaPagamento.readOnly = bReadOnly2;
    txtCustoContabilMedio.readOnly = bReadOnly2;
}

/********************************************************************
Verificacoes da Relacao
********************************************************************/
function verifyRelacaoInServer(currEstadoID, newEstadoID) {
    var nRelacaoID = dsoSup01.recordset['RelacaoID'].value;
    var strPars = new String();

    strPars = '?nRelacaoID=' + escape(nRelacaoID);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaseconceitos/serverside/verificacaorelacao.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyRelacaoInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes da Relacao
********************************************************************/
function verifyRelacaoInServer_DSC() {
    var sMensagem = (dsoVerificacao.recordset['Mensagem'].value == null ? '' : dsoVerificacao.recordset['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset['Resultado'].value == null ? 0 : dsoVerificacao.recordset['Resultado'].value);

    if (nResultado == 0) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        stateMachSupExec('OK');
    }
    else if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            stateMachSupExec('OK');
        else
            stateMachSupExec('CANC');
    }
    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        stateMachSupExec('CANC');
    }
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblTipoRegistroID, de acordo com o ID do
Tipo de Registro

Parametro:
nID   ID do Tipo de Registro

Retorno:
nenhum
********************************************************************/
function setLabelRelacao(nID) {
    if (nID == null)
        nID = '';


    var sLabel = 'Rela��o ' + nID;
    lblTipoRegistroID.style.width = sLabel.length * FONT_WIDTH;
    lblTipoRegistroID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblGrupoImpostoEntradaID, de acordo com o ID do
Grupo de Imposto de Entrada

Parametro:
nID   ID do Grupo de Imposto de Entrada

Retorno:
nenhum
********************************************************************/
function setLabelGrupoImpostoEntrada(nID) {
    if (nID == null)
        nID = '';


    var sLabel = 'Imposto Entrada ' + nID;
    lblGrupoImpostoEntradaID.style.width = sLabel.length * FONT_WIDTH;
    lblGrupoImpostoEntradaID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblGrupoImpostoSaidaID, de acordo com o ID do
Grupo de Imposto de Saida

Parametro:
nID   ID do Grupo de Imposto de Saida

Retorno:
nenhum
********************************************************************/
function setLabelGrupoImpostoSaida(nID) {
    if (nID == null)
        nID = '';


    var sLabel = 'Imposto Saida ' + nID;
    lblGrupoImpostoSaidaID.style.width = sLabel.length * FONT_WIDTH;
    lblGrupoImpostoSaidaID.innerText = sLabel;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label do controle lblGrupoImpostoEntradaID, de acordo com o ID do
Grupo de Imposto de Entrada

Parametro:
nID   ID do Grupo de Imposto de Entrada

Retorno:
nenhum
********************************************************************/
function setLabelTipoGatilhamento(nID) {
    if (nID == null)
        nID = '';


    var sLabel = 'Gatilha ' + nID;
    lblTipoGatilhamento.style.width = sLabel.length * FONT_WIDTH;
    lblTipoGatilhamento.innerText = sLabel;
}

function setLabelClassificacaoFiscal(nClassificacaoFiscalID) {
    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.setFilter('Indice=5 AND fldID=' + nClassificacaoFiscalID);


        if (!dsoEstaticCmbs.recordset.EOF) {
            lblClassificacaoFiscalID.title = dsoEstaticCmbs.recordset['Descricao'].value;
            selClassificacaoFiscalID.title = dsoEstaticCmbs.recordset['Descricao'].value;
        }

        else {
            lblClassificacaoFiscalID.title = '';
            selClassificacaoFiscalID.title = '';
        }


        dsoEstaticCmbs.recordset.setFilter('');
    }
}

function setLabelNBS(nNBSID) {
    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.setFilter('Indice=8 AND fldID=' + nNBSID);

        if (!dsoEstaticCmbs.recordset.EOF) {
            lblNBSID.title = dsoEstaticCmbs.recordset['Descricao'].value;
            selNBSID.title = dsoEstaticCmbs.recordset['Descricao'].value;
        }
        else {
            lblNBSID.title = '';
            selNBSID.title = '';
        }

        dsoEstaticCmbs.recordset.setFilter('');
    }
}



function setLabelServico(nServicoID) {
    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.setFilter('Indice=6 AND fldID=' + nServicoID);

        if (!dsoEstaticCmbs.recordset.EOF) {
            lblServicoID.title = dsoEstaticCmbs.recordset['Descricao'].value;
            selServicoID.title = dsoEstaticCmbs.recordset['Descricao'].value;
        }

        else {
            lblServicoID.title = '';
            selServicoID.title = '';
        }


        dsoEstaticCmbs.recordset.setFilter('');
    }
}

function setLabelOrigem(nOrigemID) {
    if (!((dsoEstaticCmbs.recordset.BOF) && (dsoEstaticCmbs.recordset.EOF))) {
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.setFilter('Indice=7 AND fldID=' + nOrigemID);

        if (!dsoEstaticCmbs.recordset.EOF) {
            lblOrigemID.title = dsoEstaticCmbs.recordset['Descricao'].value;
            selOrigemID.title = dsoEstaticCmbs.recordset['Descricao'].value;
        }
        else {
            lblOrigemID.title = '';
            selOrigemID.title = '';
        }

        dsoEstaticCmbs.recordset.setFilter('');
    }
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
            aData[0] - quem e na relacao ('SUJ' ou 'OBJ')
            aData[1] - ID do tipo da relacao
            aData[2] - ID do campo do subform corrente
            aData[3] - ID selecionado no grid da modal
            aData[4] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData) {
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;


    elem = window.document.getElementById(aData[2]);


    if (elem == null)
        return;


    clearComboEx([elem.id]);


    oldDataSrc = elem.dataSrc;
    oldDataFld = elem.dataFld;
    elem.dataSrc = '';
    elem.dataFld = '';

    // Preencher o combo selSujeitoID
    if (aData[2] == 'selSujeitoID')
        dsoSup01.recordset.Fields['SujeitoID'].value = aData[3];
        // Preencher o combo selObjetoID
    else if (aData[2] == 'selObjetoID')
        dsoSup01.recordset.Fields['ObjetoID'].value = aData[3];

    oOption = document.createElement("OPTION");
    oOption.text = aData[4];
    oOption.value = aData[3];
    elem.add(oOption);
    elem.selectedIndex = 0;


    elem.dataSrc = oldDataSrc;
    elem.dataFld = oldDataFld;

    if (aData.length > 5)
        addSexOrServ = aData[5];


    optChangedInCmbSujOrObj(elem, addSexOrServ);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
cmb     - referencia do combo
addSexOrServ  - ID do sexo ou descricao do servico

Retorno:
nenhum
********************************************************************/
function optChangedInCmbSujOrObj(cmb, addSexOrServ) {
    var cmbID = cmb.id;


    if (cmbID == 'selSujeitoID') {
        clearComboEx(['selObjetoID']);
        selObjetoID.disabled = true;
        lockBtnLupa(btnFindObjeto, true);


        if (cmb.selectedIndex != -1) {
            // destrava o botao de lupa do objeto
            lockBtnLupa(btnFindObjeto, false);

        }
    }

    if ((cmbID == 'selSujeitoID') || (cmbID == 'selObjetoID'))
        changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID',
                           'lblObjetoID', 'selTipoRegistroID',
                           selSujeitoID.value, selObjetoID.value);
}

/********************************************************************
Criada pelo programador
           
Parametros: 
true esta em modo de edicao

Retorno:
nenhum
********************************************************************/
function showHideByTipoRegistro(bEditing) {
    var nTipoRegistroID, bTemProducao;


    bTemProducao = dsoSup01.recordset['TemProducao'].value;

    if (bEditing == true)
        nTipoRegistroID = selTipoRegistroID.value;
    else
        nTipoRegistroID = dsoSup01.recordset['TipoRelacaoID'].value;


    lblCodigoAnterior.style.visibility = 'hidden';
    txtCodigoAnterior.style.visibility = 'hidden';


    if (nTipoRegistroID == 61) {
        lblCodigoAnterior.style.visibility = 'inherit';
        txtCodigoAnterior.style.visibility = 'inherit';

        var nPrecoWeb = dsoSup01.recordset['PrecoWeb_Calc'].value;
        txtPreco.title = 'Pre�o Web: ' + padNumReturningStr(roundNumber(nPrecoWeb, 2), 2);
        selObjetoID.title = dsoSup01.recordset['DescricaoCompleta'].value;
        lblObjetoID.title = dsoSup01.recordset['DescricaoCompleta'].value;
    }
    else {
        selObjetoID.title = '';
        lblObjetoID.title = '';
    }


    if (dsoSup01.recordset['ClassificacaoID'].value != 315) {
        lblServicoID.style.visibility = 'hidden';
        selServicoID.style.visibility = 'hidden';
        lblClassificacaoFiscalID.style.visibility = 'inherit';
        selClassificacaoFiscalID.style.visibility = 'inherit';
    }
    else {
        lblClassificacaoFiscalID.style.visibility = 'hidden';
        selClassificacaoFiscalID.style.visibility = 'hidden';
        lblServicoID.style.top = lblClassificacaoFiscalID.style.top;
        lblServicoID.style.left = lblClassificacaoFiscalID.style.left;
        selServicoID.style.top = selClassificacaoFiscalID.style.top;
        selServicoID.style.left = selClassificacaoFiscalID.style.left;
        selServicoID.style.width = (parseInt(selClassificacaoFiscalID.style.width) - 10);

        lblServicoID.style.visibility = 'inherit';
        selServicoID.style.visibility = 'inherit';
    }
}

function auditoria() {
    if (dsoSup01.recordset['Auditoria'].value != null) {
        if (window.top.overflyGen.Alert('Inconsist�ncia(s)' + SYS_SPACE + 'de cadastro:' + dsoSup01.recordset['Auditoria'].value) == 0)
            return null;
        return false;
    }
}

/********************************************************************
Usuario vai incluir um novo produto, inicia o tipo gatilhamento com 3
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function newProduct() {
    var aCmbBar = new Array();


    aCmbBar = getCmbCurrDataInControlBar('sup', 1);


    if (aCmbBar[2].toUpperCase() == 'PRODUTOS') {
        selTipoGatilhamento.disabled = false;
        setValueInControlLinked(selTipoGatilhamento, dsoSup01, 3);

    }
}

/********************************************************************
Trava destrava o combo tipo de gatilhamento
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function treatCmbTipoGatilhamento() {
    if (glb_ProdutoEstoque > 0)
        selTipoGatilhamento.disabled = true;
    else
        selTipoGatilhamento.disabled = false;
}
/********************************************************************
Funcao criada pelo programador.
Da refresh no sup

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function Lockedlabels(EstadoID) {

    if ((EstadoID == 1) || (EstadoID == 11)) {
        selGrupoImpostoEntradaID.disabled = false;
        selMoedaEntradaID.disabled = false;
        selOrigemID.disabled = false;
        selClassificacaoFiscalID.disabled = false;
        chkProdutoImportado.disabled = false;
        selGrupoImpostoSaidaID.disabled = false;
        selMoedaSaidaID.disabled = false;
        selCEST.disabled == false
    }
    else {
        selGrupoImpostoEntradaID.disabled = true;
        selMoedaEntradaID.disabled = true;
        selOrigemID.disabled = true;
        selClassificacaoFiscalID.disabled = true;
        chkProdutoImportado.disabled = true;
        selGrupoImpostoSaidaID.disabled = true;
        selMoedaSaidaID.disabled = true;
        selCEST.disabled == true;
    }
}

/********************************************************************
Funcao criada pelo programador.
Da refresh no sup

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function refreshSup() {
    if (glb_TimerPedidoVar != null) {
        window.clearInterval(glb_TimerPedidoVar);
        glb_TimerPedidoVar = null;
    }

    __btn_REFR('sup');
}


function txtFIE_ExcecaoDireitos() {
    setConnection(dsoPerfilUsuario);

    dsoPerfilUsuario.SQL = 'SELECT DISTINCT c.PerfilID ' +
                                'FROM Pessoas a WITH(NOLOCK) ' +
                                    'INNER JOIN RelacoesPesRec b WITH(NOLOCK) ON (b.SujeitoID = a.PessoaID) ' +
                                    'INNER JOIN RelacoesPesRec_Perfis c WITH(NOLOCK) ON (c.RelacaoID = b.RelacaoID) ' +
                                'WHERE a.PessoaID = ' + glb_USERID + ' AND a.EstadoID = 2 ' +
                                    'AND b.TipoRelacaoID = 11 AND b.EstadoID = 2 AND b.ObjetoID = 999';

    dsoPerfilUsuario.ondatasetcomplete = dsoPerfilUsuario_DSC;
    dsoPerfilUsuario.Refresh();
}

function dsoPerfilUsuario_DSC() {
    var perfilFIE_Excecao = false;

    // Apenas perfis Gerente Log�stica e Coordenador de mporta��o potem alterar campo FIE Exce��o
    if (!((dsoPerfilUsuario.recordset.BOF) && (dsoPerfilUsuario.recordset.EOF))) {

        dsoPerfilUsuario.recordset.MoveFirst();
        
        var nPerfilID = 0;

        while (!dsoPerfilUsuario.recordset.BOF) {

            if (dsoPerfilUsuario.recordset['PerfilID'].value == null)
                break;
            else
                nPerfilID = parseInt(dsoPerfilUsuario.recordset['PerfilID'].value);

            if ((nPerfilID == 500) || (nPerfilID == 619) || (nPerfilID == 643)) {
                perfilFIE_Excecao = true;
                break;
            }

            dsoPerfilUsuario.recordset.MoveNext();
        }
    }

    if (perfilFIE_Excecao)
        txtFIE_Excecao.readOnly = false;
    else
        txtFIE_Excecao.readOnly = true;
}