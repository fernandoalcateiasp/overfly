<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="relpessoasconceitossup01Html" name="relpessoasconceitossup01Html">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/relpessoasconceitossup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaseconceitos/especificsup.js" & Chr(34) & "></script>" & vbCrLf
    %>

    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>

</head>

<!-- //@@ -->
<body id="relpessoasconceitossup01Body" name="relpessoasconceitossup01Body" language="javascript" onload="return window_onload()">

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">

    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->

    <!-- Div principal superior -->
    <!--RELACAO ENTRE PESSOAS E CONCEITOS-->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" datasrc="#dsoStateMachine" datafld="Estado" name="txtEstadoID" class="fldGeneral">
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Rela��o</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" datasrc="#dsoSup01" datafld="TipoRelacaoID" class="fldGeneral"></select>
        <p id="lblSujeitoID" name="lblSujeitoID" class="lblGeneral">Sujeito</p>
        <select id="selSujeitoID" name="selSujeitoID" datasrc="#dsoSup01" datafld="SujeitoID" class="fldGeneral"></select>
        <input type="image" id="btnFindSujeito" name="btnFindSujeito" class="fldGeneral" title="Preencher combo" width="24" height="23">
        <p id="lblObjetoID" name="lblObjetoID" class="lblGeneral">Objeto</p>
        <select id="selObjetoID" name="selObjetoID" datasrc="#dsoSup01" datafld="ObjetoID" class="fldGeneral"></select>
        <input type="image" id="btnFindObjeto" name="btnFindObjeto" class="fldGeneral" title="Preencher combo" width="24" height="23">
        <p id="lblCodigoAnterior" name="lblCodigoAnterior" class="lblGeneral">C�digo Anterior</p>
        <input type="text" id="txtCodigoAnterior" name="txtCodigoAnterior" datasrc="#dsoSup01" datafld="CodigoAnterior" class="fldGeneral" title="C�digo do produto no sistema anterior">
    </div>

    <!--PRODUTO-->
    <div id="divSup02_01" name="divSup02_01" class="divExtern">
        <p id="lblGrupoImpostoEntradaID" name="lblGrupoImpostoEntradaID" class="lblGeneral">Impostos de Entrada</p>
        <select id="selGrupoImpostoEntradaID" name="selGrupoImpostoEntradaID" datasrc="#dsoSup01" datafld="GrupoImpostoEntradaID" class="fldGeneral"></select>
        <p id="lblMoedaEntradaID" name="lblMoedaEntradaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaEntradaID" name="selMoedaEntradaID" datasrc="#dsoSup01" datafld="MoedaEntradaID" class="fldGeneral"></select>
        <p id="lblCustoMedio" name="lblCustoMedio" class="lblGeneral">Custo M�dio</p>
        <input type="text" id="txtCustoMedio" name="txtCustoMedio" datasrc="#dsoSup01" datafld="CustoMedioGerencial" class="fldGeneral"></input>

        <p id="lblFatorInternacaoEntrada" name="lblFatorInternacaoEntrada" class="lblGeneral">FIE</p>
        <input type="text" id="txtFatorInternacaoEntrada" name="txtFatorInternacaoEntrada" datasrc="#dsoSup01" datafld="FatorInternacaoEntrada_Calc" class="fldGeneral" title="Fator Interna��o de Entrada"></input>

        <p id="lblFatorInternacaoSaida" name="lblFatorInternacaoSaida" class="lblGeneral">FIS</p>
        <input type="text" id="txtFatorInternacaoSaida" name="txtFatorInternacaoSaida" datasrc="#dsoSup01" datafld="FatorInternacaoSaida_Calc" class="fldGeneral" title="Fator de Interna��o de Sa�da"></input>

<!--
        <p id="lblDespesaMedia" name="lblDespesaMedia" class="lblGeneral">Despesa M�dia</p>
        <input type="text" id="txtDespesaMedia" name="txtDespesaMedia" datasrc="#dsoSup01" datafld="DespesaMedia" class="fldGeneral"></input>
-->

        <p id="lbldtMediaPagamento" name="lbldtMediaPagamento" class="lblGeneral">M�dia Pagament</p>
        <input type="text" id="txtdtMediaPagamento" name="txtdtMediaPagamento" datasrc="#dsoSup01" datafld="V_dtMediaPagamento" class="fldGeneral" title="Data m�dia de pagamento do Produto">
        <p id="lblDiasPagamento" name="lblDiasPagamento" class="lblGeneral">Dias</p>
        <input type="text" id="txtDiasPagamento" name="txtDiasPagamento" datasrc="#dsoSup01" datafld="DiasPagamento" class="fldGeneral" title="N�mero de dias que faltam p/ pagar o Produto"></input>

        <p id="lblMoedaEmpresa" name="lblMoedaEmpresa" class="lblGeneral">Moeda</p>
        <input type="text" id="txtMoedaEmpresa" name="txtMoedaEmpresa" datasrc="#dsoSup01" datafld="MoedaEmpresa_Calc" class="fldGeneral"></input>
        <p id="lblCustoContabilMedio" name="lblCustoContabilMedio" class="lblGeneral">Custo Cont M�dio</p>
        <input type="text" id="txtCustoContabilMedio" name="txtCustoContabilMedio" datasrc="#dsoSup01" datafld="CustoContMedio" class="fldGeneral"></input>
        <p id="lblPercentualCustoMedio" name="lblPercentualCustoMedio" class="lblGeneral">Custo %</p>
        <input type="text" id="txtPercentualCustoMedio" name="txtPercentualCustoMedio" datasrc="#dsoSup01" datafld="PercentualAlteracaoCusto" class="fldGeneral"></input>
        <p id="lblGrupoImpostoSaidaID" name="lblGrupoImpostoSaidaID" class="lblGeneral">Impostos de Sa�da</p>
        <select id="selGrupoImpostoSaidaID" name="selGrupoImpostoSaidaID" datasrc="#dsoSup01" datafld="GrupoImpostoSaidaID" class="fldGeneral"></select>
        <p id="lblMoedaSaidaID" name="lblMoedaSaidaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaSaidaID" name="selMoedaSaidaID" datasrc="#dsoSup01" datafld="MoedaSaidaID" class="fldGeneral"></select>
        <p id="lblPreco" name="lblPreco" class="lblGeneral">Pre�o</p>
        <input type="text" id="txtPreco" name="txtPreco" datasrc="#dsoSup01" datafld="Preco_Calc" class="fldGeneral"></input>
        <p id="lblValorTransferencia" name="lblValorTransferencia" class="lblGeneral">Valor Transfer�ncia</p>
        <input type="text" id="txtValorTransferencia" name="txtValorTransferencia" datasrc="#dsoSup01" datafld="ValorTransferencia_Calc" class="fldGeneral"></input>
        <p id="lblCodigoTaxaID" name="lblCodigoTaxaID" class="lblGeneral">CT</p>
        <select id="selCodigoTaxaID" name="selCodigoTaxaID" datasrc="#dsoSup01" datafld="CodigoTaxaID" class="fldGeneral" title="C�digo da Taxa da Moeda"></select>
        <p id="lblCategoria" name="lblCategoria" class="lblGeneral">Cat</p>
        <select id="selCategoria" name="selCategoria" datasrc="#dsoSup01" datafld="Categoria" class="fldGeneral" title="Categoria ABC (ISO 9000)">
            <option value="A">A</option>
            <option value="B">B</option>
            <option value="C">C</option>
        </select>
        <!-- 
	    <p id="lblOrigem" name="lblOrigem" class="lblGeneral">Origem</p>
	    <select id="selOrigem" name="selOrigem" DATASRC="#dsoSup01" DATAFLD="Origem" class="fldGeneral" title="">
			<option value="0">0</option>
			<option value="1">1</option>
			<option value="2">2</option>
	    </select>
  	    -->
        <p id="lblOrigemID" name="lblOrigemID" class="lblGeneral">Origem</p>
        <select id="selOrigemID" name="selOrigemID" datasrc="#dsoSup01" datafld="OrigemID" class="fldGeneral"></select>

        <p id="lblClassificacaoFiscalID" name="lblClassificacaoFiscalID" class="lblGeneral" title="Classifica��o Fiscal">Class Fiscal</p>
        <select id="selClassificacaoFiscalID" name="selClassificacaoFiscalID" class="fldGeneral" datasrc="#dsoSup01" datafld="ClassificacaoFiscalID" title="Classifica��o Fiscal">
        </select>
        <p id="lblNBSID" name="lblNBSID" class="lblGeneral" title="NBS">NBS</p>
        <select id="selNBSID" name="selNBSID" class="fldGeneral" datasrc="#dsoSup01" datafld="NBSID" title="NBS"></select>
        <p id="lblCEST" name="lblCEST" class="lblGeneral" title="CEST">CEST</p>
        <select id="selCEST" name="selCEST" class="fldGeneral" datasrc="#dsoSup01" datafld="CestID" title="CEST"></select>
        <p id="lblServicoID" name="lblServicoID" class="lblGeneral" title="Servi�o">Servi�o</p>
        <select id="selServicoID" name="selServicoID" class="fldGeneral" datasrc="#dsoSup01" datafld="ServicoID" title="Servi�o">
        </select>
        <p id="lblProdutoImportado" name="lblProdutoImportado" class="lblGeneral">Imp</p>
        <input type="checkbox" id="chkProdutoImportado" name="chkProdutoImportado" datasrc="#dsoSup01" datafld="ProdutoImportado" class="fldGeneral" title="O produto � importado?"></input>
        <p id="lblCompraAutomatica" name="lblCompraAutomatica" class="lblGeneral">CA</p>
        <input type="checkbox" id="chkCompraAutomatica" name="chkCompraAutomatica" datasrc="#dsoSup01" datafld="CompraAutomatica" class="fldGeneral" title="Faz Compra Autom�tica?"></input>
        <p id="lblBloqueiaProduto" name="lblBloqueiaProduto" class="lblGeneral">BP</p>
        <input type="checkbox" id="chkBloqueiaProduto" name="chkBloqueiaProduto" datasrc="#dsoSup01" datafld="BloqueiaProduto" class="fldGeneral" title="Bloqueia produto em fun��o do custo m�dio?"></input>
        <p id="lblProdutoBloqueado" name="lblProdutoBloqueado" class="lblGeneral">PB</p>
        <input type="checkbox" id="chkProdutoBloqueado" name="chkProdutoBloqueado" datasrc="#dsoSup01" datafld="ProdutoBloqueado" class="fldGeneral" title="Produto est� bloqueado em fun��o do custo m�dio?"></input>

        <!--TIPO GATILAHMENTO
        0 - N�o gatilha nem na entrada nem na sa�da, 
		1 - S� gatilha na sa�da,
		2 - Gatilha na saida e descobre a entrada, 
		3 - Gatilha na entrada e gatilha na saida"
		valor default: 3
		o campo aceita NULL na tabela
		o combo trava quando o produto tem estoque
		-->
        <p id="lblTipoGatilhamento" name="lblTipoGatilhamento" class="lblGeneral"></p>
        <select id="selTipoGatilhamento" name="selTipoGatilhamento" class="fldGeneral" datasrc="#dsoSup01" datafld="TipoGatilhamento" title="Tipo de Gatilhamento">
            <option value="0">N�o</option>
            <option value="1">S� saida</option>
            <option value="2">Saida infere entrada</option>
            <option value="3">Entrada e saida</option>
        </select>
        <p id="lblNumeroSerieNF" name="lblNumeroSerieNF" class="lblGeneral" title="Imprime n�meros de s�rie na nota fiscal?">NS NF</p>
        <input type="checkbox" id="chkNumeroSerieNF" name="chkNumeroSerieNF" datasrc="#dsoSup01" datafld="NumeroSerieNF" class="fldGeneral" title="Imprime n�meros de s�rie na nota fiscal?"></input>
        <p id="lblArredondaPreco" name="lblArredondaPreco" class="lblGeneral">AP</p>
        <input type="checkbox" id="chkArredondaPreco" name="chkArredondaPreco" datasrc="#dsoSup01" datafld="ArredondaPreco" class="fldGeneral" title="Arredonda pre�o?"></input>
        <p id="lblFichaTecnica" name="lblFichaTecnica" class="lblGeneral">FT</p>
        <input type="checkbox" id="chkFichaTecnica" name="chkFichaTecnica" datasrc="#dsoSup01" datafld="FichaTecnica" class="fldGeneral" title="Inclui produto na Ficha T�cnica da Proposta?"></input>
        <p id="lblPublica" name="lblPublica" class="lblGeneral">PI</p>
        <input type="checkbox" id="chkPublica" name="chkPublica" datasrc="#dsoSup01" datafld="Publica" class="fldGeneral" title="Publica o produto na internet?"></input>
        <p id="lblPublicaPreco" name="lblPublicaPreco" class="lblGeneral">PP</p>
        <input type="checkbox" id="chkPublicaPreco" name="chkPublicaPreco" datasrc="#dsoSup01" datafld="PublicaPreco" class="fldGeneral" title="Publica o pre�o do produto na internet?"></input>
        <p id="lblPercentualPublicacaoEstoque" name="lblPercentualPublicacaoEstoque" class="lblGeneral">PE %</p>
        <input type="text" id="txtPercentualPublicacaoEstoque" name="txtPercentualPublicacaoEstoque" datasrc="#dsoSup01" datafld="PercentualPublicacaoEstoque" class="fldGeneral" title="Percentual de publica��o do estoque do produto na internet?">
        <p id="lblReportaFabricante" name="lblReportaFabricante" class="lblGeneral">RF</p>
        <input type="checkbox" id="chkReportaFabricante" name="chkReportaFabricante" datasrc="#dsoSup01" datafld="ReportaFabricante" class="fldGeneral" title="Reporta vendas e estoque para o fabricante?"></input>
        <p id="lblBNDES" name="lblBNDES" class="lblGeneral">BNDES</p>
        <input type="checkbox" id="chkBNDES" name="chkBNDES" datasrc="#dsoSup01" datafld="BNDES" class="fldGeneral" title="Produto cadastrado no BNDES?"></input>
        <p id="lblEstoqueFisico" name="lblEstoqueFisico" class="lblGeneral">F�sico</p>
        <input type="text" id="txtEstoqueFisico" name="txtEstoqueFisico" class="fldGeneral" datasrc="#dsoSup01" datafld="EstoqueFisico_Calc" title="Estoque F�sico"></input>
        <p id="lblEstoqueDisponivel" name="lblEstoqueDisponivel" class="lblGeneral">Dispon�vel</p>
        <input type="text" id="txtEstoqueDisponivel" name="txtEstoqueDisponivel" class="fldGeneral" datasrc="#dsoSup01" datafld="EstoqueDisponivel_Calc" title="Estoque Dispon�vel"></input>
        <p id="lblReservaC" name="lblReservaC" class="lblGeneral">RC</p>
        <input type="text" id="txtReservaC" name="txtReservaC" class="fldGeneral" datasrc="#dsoSup01" datafld="EstoqueRC_Calc" title="Reserva de Compra"></input>
        <p id="lblReservaCC" name="lblReservaCC" class="lblGeneral">RCC</p>
        <input type="text" id="txtReservaCC" name="txtReservaCC" class="fldGeneral" datasrc="#dsoSup01" datafld="EstoqueRCC_Calc" title="Reserva de Compra Confirmada"></input>
        <p id="lblReservaV" name="lblReservaV" class="lblGeneral">RV</p>
        <input type="text" id="txtReservaV" name="txtReservaV" class="fldGeneral" datasrc="#dsoSup01" datafld="EstoqueRV_Calc" title="Reserva de Venda"></input>
        <p id="lblReservaVC" name="lblReservaVC" class="lblGeneral">RVC</p>
        <input type="text" id="txtReservaVC" name="txtReservaVC" class="fldGeneral" datasrc="#dsoSup01" datafld="EstoqueRVC_Calc" title="Reserva de Venda Confirmada"></input>
        <p id="lblLoteFis" name="lblLoteFis" class="lblGeneral">Lote Fis</p>
        <input type="text" id="txtLoteFis" name="txtLoteFis" class="fldGeneral" datasrc="#dsoSup01" datafld="EstoqueLoteFis_Calc" title="Lote F�sico"></input>
        <p id="lblLoteDisp" name="lblLoteDisp" class="lblGeneral">Lote Disp</p>
        <input type="text" id="txtLoteDisp" name="txtLoteDisp" class="fldGeneral" datasrc="#dsoSup01" datafld="EstoqueLoteDisp_Calc" title="Lote Dispon�vel"></input>
        <p id="lblPrevisaoDisponibilidade" name="lblPrevisaoDisponibilidade" class="lblGeneral">Disponibilidade</p>
        <input type="text" id="txtdtPrevisaoDisponibilidade" name="txtdtPrevisaoDisponibilidade" datasrc="#dsoSup01" datafld="V_dtPrevisaoDisponibilidade" class="fldGeneral" title="Previs�o de disponibilidade do produto para vendas">
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" datasrc="#dsoSup01" datafld="Observacao" class="fldGeneral">
        <p id="lblFIE_Excecao" name="lblFIE_Excecao" class="lblGeneral">FIE Ex</p>
        <input type="text" id="txtFIE_Excecao" name="txtFIE_Excecao" datasrc="#dsoSup01" datafld="FIE_Excecao" class="fldGeneral" title="Fator Interna��o de Entrada de Exce��o"></input>
    </div>

    <!--MARCA-->
    <div id="divSup02_02" name="divSup02_02" class="divExtern">
        <p id="lblHrMarca" name="lblHrMarca" class="lblGeneral">Marca</p>
        <hr id="hrMarca" name="hrMarca" class="lblGeneral">
        <p id="lblOrdem" name="lblOrdem" class="lblGeneral">Ordem</p>
        <input type="text" id="txtOrdem" name="txtOrdem" datasrc="#dsoSup01" datafld="Ordem" class="fldGeneral" title="� propriet�rio da marca?">
        <p id="lblEhProprietario" name="lblEhProprietario" class="lblGeneral">Propriet�rio</p>
        <input type="checkbox" id="chkEhProprietario" name="chkEhProprietario" datasrc="#dsoSup01" datafld="EhProprietario" class="fldGeneral" title="� propriet�rio da marca?">
    </div>

       <!--Fabricante-->
    <div id="divSup02_03" name="divSup02_03" class="divExtern">
        <p id="lblAgrupa" name="lblAgrupa" class="lblGeneral">Agrupa</p>
        <input type="checkbox" id="chkAgrupa" name="chkAgrupa" datasrc="#dsoSup01" datafld="Agrupa" class="fldGeneral" title="Agrupa esta familia em relatorio de totaliza��o de vendas, quando tiver pouca relevancia"></input>        
    </div>
</body>

</html>
