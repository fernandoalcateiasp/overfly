<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="relacoesrecssup01Html" name="relacoesrecssup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf            
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relentrerecs/relacoesrecssup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relentrerecs/especificsup.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="relacoesrecssup01Body" name="relacoesrecssup01Body" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">        
        
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoRelacaoID"></select>    
	    <p id="lblSujeitoID" name="lblSujeitoID" class="lblGeneral">Sujeito</p>
	    <select id="selSujeitoID" name="selSujeitoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="SujeitoID"></select>
        <input type="image" id="btnFindSujeito" name="btnFindSujeito" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
        <input type="image" id="btnFindObjeto" name="btnFindObjeto" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
	    <p id="lblObjetoID" name="lblObjetoID" class="lblGeneral">Objeto</p>
	    <select id="selObjetoID" name="selObjetoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ObjetoID"></select>
	</div>

    <!-- Primeiro div secundario superior - Sub-Form -->    
    <div id="divSup02_01" name="divSup02_01" class="divExtern">
		<p id="lblHrSubForm" name="lblHrSubForm" class="lblGeneral">Sub-Form</p>
		<hr id="hrSubForm" name="hrSubForm" class="lblGeneral"></hr>
	    <p id="lblMaquinaEstadoID" name="lblMaquinaEstadoID" class="lblGeneral">M�quina de Estado</p>
		<select id="selMaquinaEstadoID" name="selMaquinaEstadoID" DATASRC="#dsoSup01" DATAFLD="MaquinaEstadoID" class="fldGeneral"></select>
    </div>

    <!-- Segundo div secundario superior - Feature -->    
    <div id="divSup02_02" name="divSup02_02" class="divExtern">
		<p id="lblHrFeature" name="lblHrFeature" class="lblGeneral">Feature</p>
		<hr id="hrFeature" name="hrFeature" class="lblGeneral"></hr>
	    <p id="lblOrdem" name="lblOrdem" class="lblGeneral">Ordem</p>
	    <input type="text" id="txtOrdem" name="txtOrdem" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem"></input>
	    <p id="lblEhDefault"  name="lblEhDefault" class="lblGeneral">Default</p>
	    <input type="checkbox" id="chkEhDefault"  name="chkEhDefault" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhDefault" title="� feature default?"></input>
    </div>

    <!-- Terceiro div secundario superior - Estado -->    
    <div id="divSup02_03" name="divSup02_03" class="divExtern">    
 		<p id="lblHrEstado" name="lblHrEstado" class="lblGeneral">Estado</p>
		<hr id="hrEstado" name="hrEstado" class="lblGeneral"></hr>
	    <p id="lblOrdem_2" name="lblOrdem_2" class="lblGeneral">Ordem</p>
	    <input type="text" id="txtOrdem_2" name="txtOrdem_2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem"></input>
        <p id="lblRelevancia" name="lblRelevancia" class="lblGeneral">Relev�ncia</p>
	    <input type="text" id="txtRelevancia" name="txtRelevancia" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Relevancia"></input>
        <p id="lblPrazo" name="lblPrazo" class="lblGeneral">Prazo</p>
	    <input type="text" id="txtPrazo" name="txtPrazo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Prazo"></input>
    </div>

    <!-- Quarto div secundario superior - Perfil -->    
    <div id="divSup02_04" name="divSup02_04" class="divExtern">    
 		<p id="lblHrPerfil" name="lblHrPerfil" class="lblGeneral">Perfil</p>
		<hr id="hrPerfil" name="hrPerfil" class="lblGeneral"></hr>
	    <p id="lblOrdem_3" name="lblOrdem_3" class="lblGeneral">Ordem</p>
	    <input type="text" id="txtOrdem_3" name="txtOrdem_3" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem"></input>
	    <p id="lblEhSuperior"  name="lblEhSuperior" class="lblGeneral">Superior</p>
	    <input type="checkbox" id="chkEhSuperior"  name="chkEhSuperior" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhDefault" title="� perfil superior?"></input>
    </div>

</body>

</html>