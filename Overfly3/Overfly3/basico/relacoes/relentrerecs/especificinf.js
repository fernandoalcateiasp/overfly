/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Recursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.RelacaoID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM RelacoesRecursos a  WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a  WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20034) // Contextos Aplicaveis
    {
        dso.SQL = 'SELECT a.* FROM RelacoesRecursos_Contextos a  WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+idToFind;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20035) // Estados Transitivos
    {
        dso.SQL = 'SELECT a.* FROM RelacoesRecursos_Estados a  WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+idToFind;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20037) // Empresas
    {
        dso.SQL = 'SELECT a.* FROM RelacoesRecursos_Empresas a  WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+idToFind + 
        ' ORDER BY a.EmpresaID ';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    var nObjetoID;
    var nSujeitoID;
    setConnection(dso);
    
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK) , Recursos b  WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK) , Pessoas b  WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK) , TiposAuxiliares_Itens b  WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK) , Recursos b  WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Contextos Aplicaveis
    else if ( pastaID == 20034 )
    {
        // Busca o ID do campo superior 'selObjetoID'
        nObjetoID = getCurrDataInControl('sup', 'selObjetoID');

        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.RecursoID, a.RecursoFantasia ' +
                      'FROM Recursos a WITH(NOLOCK) , RelacoesRecursos b  WITH(NOLOCK) ' +
                      'WHERE b.EstadoID = 2 AND b.TipoRelacaoID = 1 ' +
                      'AND b.SujeitoID = ' + nObjetoID + 
                      'AND b.ObjetoID = a.RecursoID AND a.EstadoID = 2 ' +
                      'ORDER BY a.RecursoFantasia';
        }
    }        
    // Estados Transitivos
    else if ( pastaID == 20035 )
    {
        // Busca os ID dos campos superiores 'selSujeitoID'/'selObjetoID'
        nSujeitoID = getCurrDataInControl('sup', 'selSujeitoID');
        nObjetoID = getCurrDataInControl('sup', 'selObjetoID');

        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.RecursoID, a.RecursoFantasia ' +
                      'FROM Recursos a WITH(NOLOCK) , RelacoesRecursos b  WITH(NOLOCK) ' +
                      'WHERE b.EstadoID = 2 AND b.TipoRelacaoID = 3 ' +
                      'AND b.ObjetoID = ' + nObjetoID + ' ' + 
                      'AND b.SujeitoID <> ' + nSujeitoID + ' ' +
                      'AND b.SujeitoID = a.RecursoID ' +
                      'ORDER BY a.RecursoFantasia';
        }
    }        
    // Empresas
    else if ( pastaID == 20037 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.RelacaoID, b.PessoaID, b.Fantasia ' +
                      'FROM RelacoesPesRec a WITH(NOLOCK) , Pessoas b  WITH(NOLOCK) ' +
                      'WHERE a.TipoRelacaoID = 12 AND a.ObjetoID = 999 ' +
                      'AND a.EstadoID = 2 AND a.SujeitoID = b.PessoaID AND b.EstadoID = 2 ' +
                      'ORDER BY b.Fantasia';
        }
    }        
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    if ( nTipoRegistroID == 2 )
	{
        vPastaName = window.top.getPastaName(20034);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20034); //Contextos Aplicaveis
    }

    if ( nTipoRegistroID == 3 )
	{
        vPastaName = window.top.getPastaName(20035);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20035); //Estados Transitivos
    }

    if ( nTipoRegistroID == 4 )
	{
        vPastaName = window.top.getPastaName(20037);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20037); //Empresas
    }
 	
    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 20034) // Contextos Aplicaveis
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID]);
        else if (folderID == 20035) // Estados Transitivos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID]);
        else if (folderID == 20037) // Empresas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 20034)
    {
        headerGrid(fg,['Contexto', 'RelacaoID', 'RelRecContextoID'], [1,2]);
        fillGridMask(fg,currDSO,['RecursoID', 'RelacaoID', 'RelRecContextoID']
                                     ,['','','']);
    }    
    else if (folderID == 20035)
    {
        headerGrid(fg,['Para', 'Default', 'US', 'Motivo', 'RelacaoID', 'RelRecEstadoID'], [4,5]);
        
        glb_aCelHint = [[0,1,'Esta � a transi��o de estado default?'],
						[0,2,'Esta transi��o de estado � de uso do sistema?'],
                        [0,3,'Esta transi��o de estado tem motivo?']];
        
        fillGridMask(fg,currDSO,['RecursoID', 'EhDefault', 'UsoSistema', 'TemMotivo', 'RelacaoID', 'RelRecEstadoID']
                                     ,['','','','','','']);
    }
    else if (folderID == 20037)
    {
        headerGrid(fg,['Empresa', 'RelacaoID', 'RelRecEmpresaID'], [1,2]);
        fillGridMask(fg,currDSO,['EmpresaID','RelacaoID', 'RelRecEmpresaID']
                                     ,['','','']);
    }    
}
