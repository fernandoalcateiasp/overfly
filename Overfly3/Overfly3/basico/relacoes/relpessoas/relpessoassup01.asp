<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="relpessoassup01Html" name="relpessoassup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/relpessoassup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/especificsup.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="relpessoassup01Body" name="relpessoassup01Body" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->

    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <!-- //@@ Inicia a partir daqui -->
        <p id="lblTipoRelacaoID" name="lblTipoRelacaoID" class="lblGeneral">Rela��o</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" DATASRC="#dsoSup01" DATAFLD="TipoRelacaoID" class="fldGeneral"></select>
        <p id="lblSujeitoID" name="lblSujeitoID" class="lblGeneral">Sujeito</p>
        <select id="selSujeitoID" name="selSujeitoID" DATASRC="#dsoSup01" DATAFLD="SujeitoID" class="fldGeneral"></select>
        <input type="image" id="btnFindSujeito" name="btnFindSujeito" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
        <p id="lblObjetoID" name="lblObjetoID" class="lblGeneral">Objeto</p>
        <select id="selObjetoID" name="selObjetoID" DATASRC="#dsoSup01" DATAFLD="ObjetoID" class="fldGeneral"></select>
        <input type="image" id="btnFindObjeto" name="btnFindObjeto" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
    </div>
    
    <!-- Primeiro div secundario superior - Clientelia -->    
    <div id="divSup02_01" name="divSup02_01" class="divExtern">        
        <p id="lblClassificacaoPessoa" name="lblClassificacaoPessoa" class="lblGeneral" title="Classifica��o do sujeito em rela��o ao mercado">Classifica��o</p>
        <input type="text" id="txtClassificacaoPessoa" name="txtClassificacaoPessoa" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ClassificacaoPessoa" title="Classifica��o do sujeito em rela��o ao mercado">           
        <p id="lblEmpresaColigada" name="lblEmpresaColigada" class="lblGeneral">EC</p>
        <input type="checkbox" id="chkEmpresaColigada" name="chkEmpresaColigada" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EmpresaColigada" title="O cliente � empresa coligada do fornecedor?">
        <p id="lblClassificacaoID" name="lblClassificacaoID" class="lblGeneral" title="Classifica��o comercial do parceiro">C</p>
        <select id="selClassificacaoID" name="selClassificacaoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ClassificacaoID" title="Classifica��o comercial do parceiro"></select>    
        <p id="lblConceitoID" name="lblConceitoID" class="lblGeneral" title="Conceito financeiro do parceiro">Conceito</p>
        <select id="selConceitoID" name="selConceitoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ConceitoID" title="Conceito financeiro do parceiro"></select>
        <p id="lblUsuarioCredito" name="lblUsuarioCredito" class="lblGeneral">Usu�rio</p>
        <input type="text" id="txtUsuarioCredito" name="txtUsuarioCredito" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="UsuarioCredito">
        <p id="lblListaPreco" name="lblListaPreco" class="lblGeneral">Lista</p>
        <select id="selListaPreco" name="selListaPreco" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ListaPreco">
            <option value="0">0</option>
            <option value="1">1</option>
		    <option value="2">2</option>
		    <option value="3">3</option>
		    <option value="4">4</option>
		    <option value="5">5</option>
        </select>    
        <p id="lblFrete" name="lblFrete" class="lblGeneral">Frete</p>
        <input type="checkbox" id="chkFrete" name="chkFrete" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Frete" title="� frete pago?">
        <p id="lblMailing" name="lblMailing" class="lblGeneral">Mailing</p>
        <input type="checkbox" id="chkMailing" name="chkMailing" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Mailing" title="Recebe mala direta?">
        <p id="lblEmails" name="lblEmails" class="lblGeneral">Emails</p>
        <input type="checkbox" id="chkEmails" name="chkEmails" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Emails" title="Recebe e-mails de ofertas e promo��es?">
        <p id="lblNaoRebate" name="lblNaoRebate" class="lblGeneral">Reb</p>
        <input type="checkbox" id="chkNaoRebate" name="chkNaoRebate" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NaoRebate" title="N�o aplica rebate?">
		<p id="lblCodigo" name="lblCodigo" class="lblGeneral">C�digo</p>
        <input type="text" id="txtCodigo" name="txtCodigo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Codigo" title="C�digo da empresa no parceiro">
		<p id="lblCodigoAnterior" name="lblCodigoAnterior" class="lblGeneral">C�digo Anterior</p>
        <input type="text" id="txtCodigoAnterior" name="txtCodigoAnterior" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CodigoAnterior" title="C�digo anterior do parceiro no sistema">
        <p id="lblCodigoCRM" name="lblCodigoCRM" class="lblGeneral">C�digo CRM</p>
        <input type="text" id="txtCodigoCRM" name="txtCodigoCRM" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CodigoCRM" title="C�digo CRM">
		<p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
		<p id="lblClienteDesde" name="lblClienteDesde" class="lblGeneral">Cliente Desde</p>
        <input type="text" id="txtClienteDesde" name="txtClienteDesde" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_ClienteDesde">
		<p id="lblAnos" name="lblAnos" class="lblGeneral">Anos</p>
        <input type="text" id="txtAnos" name="txtAnos" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Anos" title="Tempo(em anos) que � cliente">
		<p id="lblPedidos" name="lblPedidos" class="lblGeneral">Pedidos</p>
        <input type="text" id="txtPedidos" name="txtPedidos" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Pedidos" title="N�mero de Pedidos">
		<p id="lblFaturamentoAcumulado" name="lblFaturamentoAcumulado" class="lblGeneral">Fat Acum US$</p>
        <input type="text" id="txtFaturamentoAcumulado" name="txtFaturamentoAcumulado" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FaturamentoAcumulado" title="Faturamento Acumulado">
		<p id="lblFaturamentoMensal" name="lblFaturamentoMensal" class="lblGeneral">Fat Mensal US$</p>
        <input type="text" id="txtFaturamentoMensal" name="txtFaturamentoMensal" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FaturamentoMensal" title="Faturamento por m�s">
		<p id="lblFaturamentoPedido" name="lblFaturamentoPedido" class="lblGeneral">Fat/Pedido US$</p>
        <input type="text" id="txtFaturamentoPedido" name="txtFaturamentoPedido" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FaturamentoPedido" title="Faturamento por Pedido">
		<p id="lblContribuicaoAcumulada" name="lblContribuicaoAcumulada" class="lblGeneral">Contr Acum US$</p>
        <input type="text" id="txtContribuicaoAcumulada" name="txtContribuicaoAcumulada" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ContribuicaoAcumulada" title="Contribui��o Acumulada">
		<p id="lblContribuicaoAcumuladaRecebida" name="lblContribuicaoAcumuladaRecebida" class="lblGeneral">Contr Acum Rec US$</p>
        <input type="text" id="txtContribuicaoAcumuladaRecebida" name="txtContribuicaoAcumuladaRecebida" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ContribuicaoAcumuladaRecebida" title="Contribui��o Acumulada Recebida">
		<p id="lblMaiorAtraso" name="lblMaiorAtraso" class="lblGeneral">Atraso</p>
        <input type="text" id="txtMaiorAtraso" name="txtMaiorAtraso" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MaiorAtraso" title="Maior atraso(em dias)">
		<p id="lbldtMaiorCompra" name="lbldtMaiorCompra" class="lblGeneral">Maior Compra</p>
        <input type="text" id="txtdtMaiorCompra" name="txtdtMaiorCompra" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="dtMaiorCompra" title="Data da maior compra">
		<p id="lblValorMaiorCompra" name="lblValorMaiorCompra" class="lblGeneral">Valor US$</p>
        <input type="text" id="txtValorMaiorCompra" name="txtValorMaiorCompra" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ValorMaiorCompra" title="Valor da maior compra">
		<p id="lbldtUltimaCompra" name="lbldtUltimaCompra" class="lblGeneral">�ltima Compra</p>
        <input type="text" id="txtdtUltimaCompra" name="txtdtUltimaCompra" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtUltimaCompra" title="Data da �ltima compra">
		<p id="lblDiasUltimaCompra" name="lblDiasUltimaCompra" class="lblGeneral">Dias</p>
        <input type="text" id="txtDiasUltimaCompra" name="txtDiasUltimaCompra" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="diasUltimaCompra" title="Dias da �ltima compra">
		<p id="lblValorUltimaCompra" name="lblValorUltimaCompra" class="lblGeneral">Valor US$</p>
        <input type="text" id="txtValorUltimaCompra" name="txtValorUltimaCompra" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ValorUltimaCompra" title="Valor da �ltima compra">
		<p id="lbldtMaiorAcumulo" name="lbldtMaiorAcumulo" class="lblGeneral">Maior Ac�mulo</p>
        <input type="text" id="txtdtMaiorAcumulo" name="txtdtMaiorAcumulo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="dtMaiorAcumulo" title="Data do maior ac�mulo">
		<p id="lblValorMaiorAcumulo" name="lblValorMaiorAcumulo" class="lblGeneral">Valor US$</p>
        <input type="text" id="txtValorMaiorAcumulo" name="txtValorMaiorAcumulo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ValorMaiorAcumulo" title="Valor do maior ac�mulo">
    </div>

    <!-- Segundo div secundario superior - Sociedade-->    
    <div id="divSup02_02" name="divSup02_02" class="divExtern">        
        <p id="lblHrSociedade" name="lblHrSociedade" class="lblGeneral">Sociedade</p>
        <hr id="hrSociedade" name="hrSociedade" class="lblGeneral">
		<p id="lblParticipacao_02" name="lblParticipacao_02" class="lblGeneral">Participa��o</p>
        <input type="text" id="txtParticipacao_08" name="txtParticipacao_02" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Participacao">
		<p id="lbldtAdmissao_02" name="lbldtAdmissao_02" class="lblGeneral">Admiss�o</p>
        <input type="text" id="txtdtAdmissao_02" name="txtdtAdmissao_02" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtAdmissao">
        <p id="lblContratoSocial_02" name="lblContratoSocial_02" class="lblGeneral">CS</p>
        <input type="checkbox" id="chkContratoSocial" name="chkContratoSocial" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ContratoSocial" title="Est� no contrato social?">
	</div>    

    <!-- Terceiro div secundario superior - Empregat�cio -->    
    <div id="divSup02_03" name="divSup02_03" class="divExtern">    
        <p id="lblHrEmpregaticios" name="lblHrEmpregaticios" class="lblGeneral">Empregat�cio</p>
        <hr id="hrEmpregaticios" name="hrEmpregaticios" class="lblGeneral">
        <p id="lblFuncionarioDireto" name="lblFuncionarioDireto" class="lblGeneral">FD</p>
        <input type="checkbox" id="chkFuncionarioDireto" name="chkFuncionarioDireto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FuncionarioDireto" title="Funcion�rio direto?">
		<p id="lbldtAdmissao_03" name="lbldtAdmissao_03" class="lblGeneral">Admiss�o</p>
        <input type="text" id="txtdtAdmissao_03" name="txtdtAdmissao_03" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtAdmissao">
		<p id="lbldtDesligamento_03" name="lbldtDesligamento_03" class="lblGeneral">Desligamento</p>
        <input type="text" id="txtdtDesligamento_03" name="txtdtDesligamento_03" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtDesligamento">
		<p id="lblTipoFopagID" name="lblTipoFopagID" class="lblGeneral">Tipo Fopag</p>
        <select id="SelTipoFopagID" name="SelTipoFopagID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoFopagID"></select>
        <p id="lblCodigoCRM_03" name="lblCodigoCRM_03" class="lblGeneral">C�digo CRM</p>
        <input type="text" id="txtCodigoCRM_03" name="txtCodigoCRM_03" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CodigoCRM" title="C�digo CRM">
        
        <!--Heraldo -->
        
        <p id="lbldtVigencia_03" name="lbldtVigencia_03" class="lblGeneral">Vig�ncia</p>
        <input type="text" id="txtdtdtVigencia_03" name="txtdtdtVigencia_03" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtVigencia">
        
        
        <p id="lblDepartamentoID_01" name="lblDepartamentoID_01" class="lblGeneral">Departamento</p>
        <select id="selDepartamentoID_01" name="selDepartamentoID_01" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DepartamentoID"></select>


 <p id="lblCargoEmpregID_03" name="lblCargoEmpregID_03" class="lblGeneral">Cargo</p>
        <select id="selCargoEmpregID_03" name="selCargoEmpregID_03" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CargoID"></select>    

        <!--Heraldo -->
        <p id="lblNivel_01" name="lblNivel_01" class="lblGeneral">Nivel</p>
        <select id="selNivel_01" name="selNivel_01" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Nivel"></select>
        
        
       
		<p id="lblSalario" name="lblSalario" class="lblGeneral">S�lario</p>
        <input type="text" id="txtSalario" name="txtSalario" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Salario">
		
		<p id="lblObservacao_03" name="lblObservacao_03" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_03" name="txtObservacao_03" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
		
		<p id="lblSalarioCarteira" name="lblSalarioCarteira" class="lblGeneral">S�lario Carteira</p>
        <input type="text" id="txtSalarioCarteira" name="txtSalarioCarteira" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="SalarioCarteira">
		<p id="lblPesoPremio" name="lblPesoPremio" class="lblGeneral">Peso Pr�mio</p>
        <input type="text" id="txtPesoPremio" name="txtPesoPremio" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PesoPremio" title="Percentual para efeito de c�lculo de pr�mio">
		<p id="lblPercentual401K" name="lblPercentual401K" class="lblGeneral">401K</p>
        <input type="text" id="txtPercentual401K" name="txtPercentual401K" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Percentual401K" title="Percentual para c�lculo da verba 401K">
		
    </div>

    <!-- Quarto div secundario superior - Funcional -->    
    <div id="divSup02_04" name="divSup02_04" class="divExtern">    
        <p id="lblHrFuncional" name="lblHrFuncional" class="lblGeneral">Funcional</p>
        <hr id="hrFuncional" name="hrFuncional" class="lblGeneral">
        <p id="lblEmpresaID_04" name="lblEmpresaID_04" class="lblGeneral">Empresa</p>
        <select id="selEmpresaID_04" name="selEmpresaID_04" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EmpresaID"></select>    
        <p id="lblEstadoID_04" name="lblEstadoID_03" class="lblGeneral">Est</p>
        <select id="selEstadoID_04" name="selEstadoID_04" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EstadoProgramaID"></select>    
		<p id="lbldtAdmissao_04" name="lbldtAdmissao_04" class="lblGeneral">Admiss�o</p>
        <input type="text" id="txtdtAdmissao_04" name="txtdtAdmissao_04" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtAdmissao">
		<p id="lbldtDesligamento_04" name="lbldtDesligamento_04" class="lblGeneral">Desligamento</p>
        <input type="text" id="txtdtDesligamento_04" name="txtdtDesligamento_04" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtDesligamento">
        
        
         <!--Heraldo Alves --> 
        
<!--        <p id="lblDepartamentoID_01" name="lblDepartamentoID_01" class="lblGeneral">Departamento</p>
        <select id="selDepartamentoID_01" name="selDepartamentoID_01" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DepartamentoID"></select>
-->         
       <!-- <p id="lblNivel" name="lblNivel" class="lblGeneral">Nivel</p>
        <select id="selNivel" name="selNivel" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Nivel"></select>  -->      
         
         
        <!--Heraldo -->
       <!-- <p id="lblNivel_02" name="lblNivel_02" class="lblGeneral">Nivel</p>
        <select id="selNivel_02" name="selNivel_02" class="fldGeneral"></select>-->
         
         
        
        <p id="lblCargoID_04" name="lblCargoID_04" class="lblGeneral">Cargo</p>
        <select id="selCargoID_04" name="selCargoID_04" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="CargoID"></select>    
        <p id="lblResponsavel_04" name="lblResponsavel_04" class="lblGeneral">Resp</p>
        <input type="checkbox" id="chkResponsavel_04" name="chkResponsavel_04" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Responsavel" title="� respons�vel pelo departamento?">
		<p id="lblObservacao_04" name="lblObservacao_04" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_04" name="txtObservacao_04" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
    </div>

    <!-- Quinto div secundario superior - Departamental -->    
    <div id="divSup02_05" name="divSup02_05" class="divExtern"> 
           
       <!-- <p id="lblHrDepartamental" name="lblHrDepartamental" class="lblGeneral">Departamental</p>
        <hr id="hrDepartamental" name="hrDepartamental" class="lblGeneral">-->
        
        <!--<p id="lblEmpresaID_05" name="lblEmpresaID_05" class="lblGeneral">Empresa</p>
        <select id="selEmpresaID_05" name="selEmpresaID_05" DATASRC="#dsoSup01" DATAFLD="EmpresaID" class="fldGeneral"></select>-->
    </div>

    <!-- Sexto div secundario superior - Patrocinio a Prg de Mkt -->    
    <div id="divSup02_06" name="divSup02_06" class="divExtern">        
        <p id="lblHrPatrocinioMkt" name="lblHrPatrocinioMkt" class="lblGeneral">Patroc�nio a Programas de Mkt</p>
        <hr id="hrPatrocinioMkt" name="hrPatrocinioMkt" class="lblGeneral">
		<p id="lblParticipacao_06" name="lblParticipacao_06" class="lblGeneral">Participa��o</p>
        <input type="text" id="txtParticipacao_06" name="txtParticipacao_06" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Participacao">
		<p id="lblObservacao_06" name="lblObservacao_06" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_06" name="txtObservacao_06" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
    </div>

    <!-- Setimo div secundario superior - Participacao em Prg de Mkt -->    
    <div id="divSup02_07" name="divSup02_07" class="divExtern">        
        <p id="lblHrParticipacaoMkt" name="lblHrParticipacaoMkt" class="lblGeneral">Participa��o em Programas de Mkt</p>
        <hr id="hrParticipacaoMkt" name="hrParticipacaoMkt" class="lblGeneral">
		<p id="lblIdentificador_07" name="lblIdentificador_07" class="lblGeneral">ID</p>
        <input type="text" id="txtIdentificador_07" name="txtIdentificador_07" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Identificador">
		<p id="lbldtFiliacao_07" name="lbldtFiliacao_07" class="lblGeneral">Data Filia��o</p>
        <input type="text" id="txtdtFiliacao_07" name="txtdtFiliacao_07" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtAdmissao">
        <p id="lblFiliadorID_07" name="lblFiliadorID_07" class="lblGeneral">Filiador</p>
        <select id="selFiliadorID_07" name="selFiliadorID_07" DATASRC="#dsoSup01" DATAFLD="FiliadorID" class="fldGeneral"></select>
        <input type="image" id="btnFindFiliador" name="btnFindFiliador" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
		<p id="lblObservacao_07" name="lblObservacao_07" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_07" name="txtObservacao_07" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
    </div>

    <!-- Oitavo div secundario superior - Afinidades-->    
    <div id="divSup02_08" name="divSup02_08" class="divExtern">        
        <p id="lblHrAfinidade" name="lblHrAfinidade" class="lblGeneral">Afinidade</p>
        <hr id="hrAfinidade" name="hrAfinidade" class="lblGeneral">
        <p id="lblTipoAfinidadeID_08" name="lblTipoAfinidadeID_08" class="lblGeneral">Afinidade</p>
        <select id="selTipoAfinidadeID_08" name="selTipoAfinidadeID_08" DATASRC="#dsoSup01" DATAFLD="TipoAfinidadeID" class="fldGeneral"></select>
        <p id="lblConsanguinea" name="lblConsanguinea" class="lblGeneral">Cons</p>
        <input type="checkbox" id="chkConsanguinea" name="chkConsanguinea" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Consanguinea" title="� rela��o consanguinea?">
		<p id="lblGrau" name="lblGrau" class="lblGeneral">Grau</p>
        <input type="text" id="txtGrau" name="txtGrau" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Grau">
        <p id="lblTipoCompatibilidadeID" name="lblTipoCompatibilidadeID" class="lblGeneral">Compatibilidade</p>
        <select id="selTipoCompatibilidadeID" name="selTipoCompatibilidadeID" DATASRC="#dsoSup01" DATAFLD="TipoCompatibilidadeID" class="fldGeneral"></select>
        
        <p id="lblDependenteSalarioFamilia" name="lblDependenteSalarioFamilia" class="lblGeneral">SF</p>
        <input type="checkbox" id="chkDependenteSalarioFamilia" name="chkDependenteSalarioFamilia" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DependenteSalarioFamilia" title="Dependente para efeito de Sal�rio Fam�lia?">
        
        <p id="lblDependenteImpostoRenda" name="lblDependenteImpostoRenda" class="lblGeneral">IR</p>
        <input type="checkbox" id="chkDependenteImpostoRenda" name="chkDependenteImpostoRenda" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DependenteImpostoRenda" title="Dependente para efeito de Imposto de Renda?">
		<p id="lblEmpresaID" name="lblEmpresaID" class="lblGeneral">Empresa</p>
        <select id="selEmpresaID" name="selEmpresaID" DATASRC="#dsoSup01" DATAFLD="EmpresaID" class="fldGeneral"></select>
		<p id="lblObservacao_08" name="lblObservacao_08" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_08" name="txtObservacao_08" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>

    <!-- Nono div secundario superior - Asstec-->
    <div id="divSup02_09" name="divSup02_09" class="divExtern">
        <p id="lblHrAsstec" name="lblHrAsstec" class="lblGeneral">Asstec</p>
        <hr id="hrAsstec" name="hrAsstec" class="lblGeneral">
        <p id="lblObservacao_09" name="lblObservacao_09" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_09" name="txtObservacao_09" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>

    <!-- D�cimo div secundario superior - Transporte-->
    <div id="divSup02_10" name="divSup02_10" class="divExtern">
        <p id="lblHrTransporte" name="lblHrTransporte" class="lblGeneral">Transporte</p>
        <hr id="hrTransporte" name="hrTransporte" class="lblGeneral">
        <p id="lblOrdem_10" name="lblOrdem_10" class="lblGeneral">Ordem</p>
        <input type="text" id="txtOrdem_10" name="txtOrdem_10" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem">
        <p id="lblObservacao_10" name="lblObservacao_10" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_10" name="txtObservacao_10" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>

    <!-- D�cimo Primeiro div secundario superior - Banco-->
    <div id="divSup02_11" name="divSup02_11" class="divExtern">
        <p id="lblHrBanco" name="lblHrBanco" class="lblGeneral">Banco</p>
        <hr id="hrBanco" name="hrBanco" class="lblGeneral">
        <p id="lblOrdem_11" name="lblOrdem_11" class="lblGeneral">Ordem</p>
        <input type="text" id="txtOrdem_11" name="txtOrdem_11" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem">
        <p id="lblBanco" name="lblBanco" class="lblGeneral">Banco</p>
        <input type="text" id="txtBanco" name="txtBanco" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Banco">
        <p id="lblAgencia" name="lblAgencia" class="lblGeneral">Ag�ncia</p>
        <input type="text" id="txtAgencia" name="txtAgencia" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Agencia">
        <p id="lblObservacao_11" name="lblObservacao_11" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_11" name="txtObservacao_11" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>

    <!-- D�cimo Segundo div secundario superior - Servicos-->
    <div id="divSup02_12" name="divSup02_12" class="divExtern">
        <p id="lblHrServicos" name="lblHrServicos" class="lblGeneral">Servi�os</p>
        <hr id="hrServicos" name="hrServicos" class="lblGeneral">
        <p id="lblServico" name="lblServico" class="lblGeneral">Servi�o</p>
        <input type="text" id="txtServico" name="txtServico" class="fldGeneral">
        <p id="lblObservacao_12" name="lblObservacao_12" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao_12" name="txtObservacao_12" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Observacao">
	</div>

    <!-- D�cimo Terceiro div secundario superior - Grupo de Trabalho-->
    <div id="divSup02_13" name="divSup02_13" class="divExtern">        
        <p id="lblHrGrupoTrabalho" name="lblHrGrupoTrabalho" class="lblGeneral">Grupo de Trabalho</p>
        <hr id="hrGrupoTrabalho" name="hrGrupoTrabalho" class="lblGeneral">
        <p id="lblOrdem_12" name="lblOrdem_12" class="lblGeneral">Ordem</p>
        <input type="text" id="txtOrdem_12" name="txtOrdem_12" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem">
	</div>

    <!-- D�cimo Quarto div secundario superior - Deposito-->    
    <div id="divSup02_14" name="divSup02_14" class="divExtern">        
        <p id="lblHrDeposito" name="lblHrDeposito" class="lblGeneral">Dep�sito</p>
        <hr id="hrDeposito" name="hrDeposito" class="lblGeneral">
        <p id="lblEhDefault" name="lblEhDefault" class="lblGeneral">Default</p>
		<input type="checkbox" id="chkEhDefault" name="chkEhDefault" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhDefault" title="Este dep�sito � o default para a empresa?">
	</div>
	
	<!-- D�cimo quinto div secundario superior - Filia��o-->    
    <div id="divSup02_15" name="divSup02_15" class="divExtern">        
        <p id="lblHrFiliacao" name="lblHrFiliacao" class="lblGeneral">Filia��o</p>
        <hr id="hrFiliacao" name="hrFiliacao" class="lblGeneral">
        <p id="lblContabilidadeCentralizada" name="lblContabilidadeCentralizada" class="lblGeneral">CC</p>
		<input type="checkbox" id="chkContabilidadeCentralizada" name="chkContabilidadeCentralizada" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ContabilidadeCentralizada" title="A contabilidade � centralizada?">
		<p id="lblTransferePedidos" name="lblTransferePedidos" class="lblGeneral">TP</p>
		<input type="checkbox" id="chkTransferePedidos" name="chkTransferePedidos" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TransferePedidos" title="Transfer�ncia do pedido de venda � autom�tica da matriz para filial?">
	</div>
	
	<!-- D�cimo Sexto div secundario superior - Local de entrega-->    
    <div id="divSup02_16" name="divSup02_16" class="divExtern">        
        <p id="lblHrLocalEntrega" name="lblHrLocalEntrega" class="lblGeneral">Local de entrega</p>
        <hr id="hrLocalEntrega" name="hrLocalEntrega" class="lblGeneral">
        <p id="lblEhDefault_16" name="lblEhDefault_16" class="lblGeneral">Default</p>
		<input type="checkbox" id="chkEhDefault_16" name="chkEhDefault_16" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhDefault" title="Este local de entrega � o default para a empresa?">
	</div>
	
</body>

</html>
