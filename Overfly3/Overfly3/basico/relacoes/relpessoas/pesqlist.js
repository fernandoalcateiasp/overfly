/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form relpessoas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_sUltimaPesquisa = '';
var glb_first = true;
var glb_contexto = 0;
// Dados da listagem da pesquisa 
var dsoListData01 = new CDatatransport("dsoListData01");
// Dados dos combos de contexto e filtros 
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
// Dados dos proprietarios para o pesqlist .URL 
var dsoPropsPL = new CDatatransport("dsoPropsPL");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload() {
    var contexto = getCmbCurrDataInControlBar('sup', 1);

    windowOnLoad_1stPart();

    // Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Rela��o', 'SujeitoID', 'Sujeito', 'ObjetoID', 'Objeto', 'C', 'I', 'E', 'Dias', 'Cr�dito', 'Proprietario', 'Alternativo', 'nSujeitoEmpresaSistema', 'nObjetoEmpresaSistema');
    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', '', '', '', '', '', '', '###,###,##0.00', '', '', '', '');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked() {
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var sBtn5Caption = 'Alterar cr�dito';
    var sBtn5Able = 'D';
    var sBtn8Caption = 'Hist�ricos Funcionais';
    var sBtn8Able = 'D';


    // Se contexto funcional
    if (contexto[1] == 1223)
        sBtn5Caption = 'Funcion�rios';
    //rodrigo  22/06/2011 -- usar o bot�o 8 
    if (contexto[1] == 1221)
        sBtn8Caption = 'Convidar Usu�rio';

    if ((fg.Row > 0) || (contexto[1] == 1223))
        sBtn5Able = 'H';

    //rodrigo  22/06/2011 -- usar o bot�o 8 
    if (fg.Row > 0)
        sBtn8Able = 'H';

    fg.ColHidden(fg.Cols - 2) = true;
    fg.ColHidden(fg.Cols - 1) = true;

    fg.ColHidden(getColIndexByColKey(fg, 'C')) = (contexto[1] != 1221);
    fg.ColHidden(getColIndexByColKey(fg, 'I')) = (contexto[1] != 1221);
    fg.ColHidden(getColIndexByColKey(fg, 'E')) = (contexto[1] != 1221);
    fg.ColHidden(getColIndexByColKey(fg, 'diasUltimaCompra')) = (contexto[1] != 1221);
    fg.ColHidden(getColIndexByColKey(fg, 'LimiteCredito')) = (contexto[1] != 1221);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Sujeito', 'Detalhar Objeto', sBtn5Caption, 'Resumo', sBtn8Caption, 'CRM Sincroniza��o']);

    if ((fg.Rows > 1) && (fg.Row > 0))
        setupEspecBtnsControlBar('sup', 'HHHDD' + sBtn5Able + 'H' + sBtn8Able + 'H');
    else
        setupEspecBtnsControlBar('sup', 'HHHDD' + sBtn5Able + 'D' + sBtn8Able + 'H');

    btnGetProps.style.visibility = 'hidden';

    if (glb_first == true) {
        carregaProprietarios();
        glb_contexto = contexto[1];
    } else if (glb_contexto != contexto[1]) {
        carregaProprietarios();
        glb_first = true;
        glb_contexto = contexto[1];
    }

    lblProprietariosPL.title = 'Clique simples: recarrega o combo \n' +
                              'Clique duplo: alterna o combo entre Propriet�rio e Alternativo e recarrega o combo';

    // Proprietario
    lblProprietariosPL.onmouseover = lblProprietariosPL_onmouseover;
    lblProprietariosPL.onmouseout = lblProprietariosPL_onmouseout;

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var contexto = getCmbCurrDataInControlBar('sup', 1);

    if (controlBar == 'SUP') {
        // usuario clicou botao documentos
        if (btnClicked == 1) {
            if (fg.Rows > 1) {
                __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
            }
            else {
                window.top.overflyGen.Alert('Selecione um registro.');
            }
        }

            // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();

        else if (btnClicked == 3)
            window.top.openModalControleDocumento('PL', '', 720, null, '21', 'T');

        else if (btnClicked == 6) {
            if (contexto[1] == 1223)
                openModalFuncionarios();
            else
                openModalAlteracaoCredito();
        }
        else if (btnClicked == 7) {
            if (fg.Row < 1)
                return null;

            var nCurrPessoaID = getCellValueByColKey(fg, 'RelacaoID', fg.Row);
            window.top.openModalHTML(nCurrPessoaID, 'Resumo da Rela��o', 'PL', 8);
        }
        else if (btnClicked == 8) {
            //rodrigo 22/06/2011 - Abrir a modal convidar usuario somente no contexto clientes       
            if (contexto[1] == 1221)
                openModalConvidarUsuario();
            else
                openModalHistoricosFuncionais();
        }
        else if (btnClicked == 9) {
            openModalCRMSincronizacao();
        }
    }
}

function openModalAlteracaoCredito() {
    var htmlPath, nEmpresaID, nPessoaID, nSujeitoEmpresaSistema, nObjetoEmpresaSistema;
    var nFaturamentoDiretoColigada = 0;
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];

    nSujeitoEmpresaSistema = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'nSujeitoEmpresaSistema'));
    nObjetoEmpresaSistema = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'nObjetoEmpresaSistema'));

    if (nObjetoEmpresaSistema != 0) {
        nEmpresaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ObjetoID'));
        nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));
    }
    else if (nSujeitoEmpresaSistema != 0) {
        nEmpresaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));
        nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ObjetoID'));
    }
    else {
        nFaturamentoDiretoColigada = 1;
        nEmpresaID = empresaID;
        nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));
    }

    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalalteracaocredito.asp';

    // Passar parametro se veio do inf ou do sup
    var strPars = new String();
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nUsuarioID=' + escape(getCurrUserID());
    strPars += '&nRelacaoID=' + escape(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'RelacaoID')));
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nFaturamentoDiretoColigada=' + escape(nFaturamentoDiretoColigada);

    showModalWin((htmlPath + strPars), new Array(0, 0));
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Modal de impressao
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');

            // nao mexer
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALFUNCIONARIOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALHISTORICOSFUNCIONAISHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALALTERACAOCREDITOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
        // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
        // Modal convidar Usuario Rodrigo 21/06/2011
    else if (idElement.toUpperCase() == 'MODALCONVIDARUSUARIOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            //glb_nCallModal = window.setInterval('enviaEMailSenha(' + (param2 == true ? 'true' : 'false') + ')', 50, 'JavaScript');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal CRM Sincroniza��o
    else if (idElement.toUpperCase() == 'MODALCRMSINCRONIZACAOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch() {

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    // por questoes esteticas, coloca foco no grid de pesquisa
    if (fg.disabled == false)
        fg.focus();

    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(390, 480));
}

/********************************************************************
Recebe input de janela modal para discar pelo carrier.
Definir e implementar no pesqlist, no sup e no inf do form.
********************************************************************/
function dialByModalPage(sDDD, sNumero, nPessoaID) {
    sDDD = sDDD.toString();
    sNumero = sNumero.toString();
    nPessoaID = parseInt(nPessoaID, 10);

    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

function openModalFuncionarios() {
    var htmlPath;
    var strPars = new String();

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalfuncionarios.asp' + strPars;
    showModalWin(htmlPath, new Array(775, 460));
}

function openModalHistoricosFuncionais() {
    var htmlPath;
    var strPars = new String();

    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];

    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);
    strPars += '&nEmpresaID=' + escape(empresaID);

    // carregar modal -
    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalhistoricosfuncionais.asp' + strPars;
    showModalWin(htmlPath, new Array(940, 510));
}

function openModalConvidarUsuario() {

    var htmlPath;

    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalconvidarusuario.asp';


    // Passar o id do label do combo que vai preencher
    // para configurar a janela
    // e o id do combo que vai preencher, que volta com a mensagem
    // para saber o combo a preencher
    var strPars = new String();

    var nSujeitoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SujeitoID'));


    strPars = '?nSujeitoID=' + escape(nSujeitoID);

    showModalWin((htmlPath + strPars), new Array(700, 200));

    return null;

}

function openModalCRMSincronizacao() {

    var htmlPath;

    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalcrmsincronizacao.asp';

    showModalWin(htmlPath, new Array(990, 540));

    return null;
}

/********************************************************************
Retorno de usuario clicou o botao do combo de proprietarios

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function btnGetProps_onClick_DSC() {
    var optionStr, optionValue;

    clearComboEx([selProprietariosPL.name]);

    while (!dsoPropsPL.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoPropsPL.recordset['Proprietario'].value;
        oOption.value = dsoPropsPL.recordset['ProprietarioID'].value;
        selProprietariosPL.add(oOption);
        dsoPropsPL.recordset.MoveNext();
    }

    if (glb_first == true) {
        carregaProprietarios();
        glb_first = false;
    }
    else {
        lockInterface(false);
    }
}

function carregaProprietarios() {
    lockInterface(true);
    var cmbContextData = getCmbCurrDataInControlBar('sup', 1);
    var cmbFiltroPadraoData = getCmbCurrDataInControlBar('sup', 2);
    var contextID = cmbContextData[1];
    var FiltroPadraoID = cmbFiltroPadraoData[1];
    var Proprietario = 0;
    var RegistrosVencidos = 0;

    // Registros Vencidos
    if (chkRegistrosVencidos.checked)
        RegistrosVencidos = 1;
    else
        RegistrosVencidos = 0;

    // Parametros para o servidor
    var strPars = new String();
    strPars = '?';

    var subFormID = window.top.subFormID;  // variavel global do browser filho    

    // formID
    strPars += 'nSubFormID=';
    strPars += encodeURIComponent(subFormID.toString());
    // contextoID
    strPars += '&nContextoID=';
    strPars += encodeURIComponent(contextID.toString());
    //FiltroPadraoID
    strPars += '&nFiltroPadraoID=';
    strPars += encodeURIComponent(FiltroPadraoID.toString());
    // RegistrosVencidos
    strPars += '&bRegistrosVencidos=' + encodeURIComponent(RegistrosVencidos);
    // empresaID
    strPars += '&nEmpresaID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_COMBODATA, 'VALUE', null)).toString());
    // userID
    strPars += '&nUsuarioID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_WIDEMSG, '__CURRUSERID', null)).toString());
    strPars += '&bAllList=' + encodeURIComponent(1);
    // Resultado
    strPars += '&bResultado=' + encodeURIComponent((glb_modoComboProprietario == true ? 1 : 0));

    dsoPropsPL.URL = SYS_ASPURLROOT + '/serversidegenEx/cmbspropspl2.aspx' + strPars;
    dsoPropsPL.ondatasetcomplete = btnGetProps_onClick_DSC;
    dsoPropsPL.refresh();
}


function lblProprietariosPL_onClick() {
    glb_TimerClick = window.setInterval('lblProprietariosPL_onClick_Continue()', 10, 'JavaScript');;
}

function lblProprietariosPL_onClick_Continue() {
    if (glb_TimerClick != null) {
        window.clearInterval(glb_TimerClick);
        glb_TimerClick = null;
    }

    carregaProprietarios();
}

function lblProprietariosPL_ondblClick() {
    glb_modoComboProprietario = !glb_modoComboProprietario;

    lblProprietariosPL.innerText = (glb_modoComboProprietario == true ? 'Propriet�rio' : 'Alternativo');

    carregaProprietarios();
}


function lblProprietariosPL_onmouseover() {
    lblProprietariosPL.style.color = 'blue';
    lblProprietariosPL.style.cursor = 'hand';
}

function lblProprietariosPL_onmouseout() {
    lblProprietariosPL.style.color = 'black';
    lblProprietariosPL.style.cursor = 'point';
}
