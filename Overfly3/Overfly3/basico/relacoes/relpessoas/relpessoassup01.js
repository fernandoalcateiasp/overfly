/********************************************************************
relpessoassup01.js

Library javascript para o relpessoassup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// controla o retorno do servidor dos dados de combos dinamicos
// (selSujeitoID e selObjetoID)
var glb_CounterCmbsDynamics = 0;

// guarda o botao clicado enviado pelo framework
var glb_BtnFromFramWork;

// guarda id do combo de lupa a preencher
var glb_cmbLupaID;
var glb_cmbID;

// Controla se sujeito ou objeto sao empresas do sistema
var glb_SujOrObjFromSystem = false;

var glb_refreshSupTimer = null;
var glb_fillPS = null;
var glb_LastBtnSupClicked = null;

// Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
// Dados dos combos dinamicos (selSujeitoID ,selObjetoID .SQL 
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
var dsoCmbDynamic03 = new CDatatransport("dsoCmbDynamic03");
var dsoCmbDynamic04 = new CDatatransport("dsoCmbDynamic04");
var dsoCmbDynamic05 = new CDatatransport("dsoCmbDynamic05");
var dsoCmbDynamic06 = new CDatatransport("dsoCmbDynamic06");
var dsoCmbDynamic07 = new CDatatransport("dsoCmbDynamic07");
// Dados do combo de Financiamento Padrao .SQL 
var dsoCmbFinanPad = new CDatatransport("dsoCmbFinanPad");
// Dados dos combos de Lupa .SQL 
var dsoCmbsLupa = new CDatatransport("dsoCmbsLupa");
// Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
// Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// //@@ Os dsos sao definidos de acordo com o form 
// Verificacao da Relacao .URL 
var dsoVerificacao = new CDatatransport("dsoVerificacao");
// Verificacao do limite de credito .URL
var dsoVerificacaoLimite = new CDatatransport("dsoVerificacaoLimite");
//Heraldo
var dsoCamposEmpregaticio = new CDatatransport("dsoCamposEmpregaticio");
//Heraldo
var dsoCamposNivel = new CDatatransport("dsoCamposNivel");
var dsoEmpresa = new CDatatransport("dsoEmpresa");

var glb_nCmbDynamicsMode = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
putSpecialAttributesInControls()
prgServerSup(btnClicked)
prgInterfaceSup(btnClicked)
optChangedInCmb(cmb)
btnLupaClicked(btnClicked)
finalOfSupCascade(btnClicked)
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
supInitEditMode()
formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
stateMachOpened( currEstadoID )
stateMachBtnOK( currEstadoID, newEstadoID )
stateMachClosed( oldEstadoID, currEstadoID, bSavedID )

FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
carrierArrived(idElement, idBrowser, param1, param2)
carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {



    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);

    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selClassificacaoID', '4'],
                          ['selConceitoID', '5'],
                          ['selTipoAfinidadeID_08', '9'],
                          ['selTipoCompatibilidadeID', '10'],
                          ['SelTipoFopagID', '11']]);

    windowOnLoad_1stPart();

    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/relpessoasinf01.asp',
                              SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/relpessoaspesqlist.asp');

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS

    linkDivsAndSubForms('divSup01_01', ['divSup02_01',
                                         'divSup02_02',
                                         'divSup02_03',
                                         'divSup02_04',
                                         'divSup02_05',
                                         'divSup02_06',
                                         'divSup02_07',
                                         'divSup02_08',
                                         'divSup02_09',
                                         'divSup02_10',
                                         'divSup02_11',
                                         'divSup02_12',
                                         'divSup02_13',
                                         'divSup02_14',
                                         'divSup02_15',
                                         'divSup02_16'],
                                        [21, 29, 31, 32, 33, 35, 36, 37, 22, 23, 24, 25, 34, 28, 30, 38]);

    windowOnLoad_2ndPart();

    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RelacaoID';

    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoRelacaoID';
}

function setupPage() {



    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup', [[1, 'divSup01_01'],
                      [2, 'divSup02_01'],
                      [2, 'divSup02_02'],
                      [2, 'divSup02_03'],
                      [2, 'divSup02_04'],
                      [2, 'divSup02_05'],
                      [2, 'divSup02_06'],
                      [2, 'divSup02_07'],
                      [2, 'divSup02_08'],
                      [2, 'divSup02_09'],
                      [2, 'divSup02_10'],
                      [2, 'divSup02_11'],
		              [2, 'divSup02_12'],
		              [2, 'divSup02_13'],
		              [2, 'divSup02_14'],
		              [2, 'divSup02_15'],
		              [2, 'divSup02_16']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
                          ['lblEstadoID', 'txtEstadoID', 2, 1],
                          ['lblTipoRelacaoID', 'selTipoRegistroID', 20, 1],
                          ['lblSujeitoID', 'selSujeitoID', 20, 1],
                          ['btnFindSujeito', 'btn', 24, 1],
                          ['lblObjetoID', 'selObjetoID', 20, 1],
                          ['btnFindObjeto', 'btn', 24, 1]]);

    //@@ *** Clientelia - Div secundario superior divSup02_01 ***/
    adjustElementsInForm([['lblEmpresaColigada', 'chkEmpresaColigada', 3, 2],
                          ['lblClassificacaoPessoa', 'txtClassificacaoPessoa', 15, 2, -10],
                          ['lblClassificacaoID', 'selClassificacaoID', 6, 2],
                          ['lblConceitoID', 'selConceitoID', 11, 2],
                          ['lblUsuarioCredito', 'txtUsuarioCredito', 13, 2],
                          ['lblListaPreco', 'selListaPreco', 5, 3],
                          ['lblFrete', 'chkFrete', 3, 3],
                          ['lblMailing', 'chkMailing', 3, 3, -1],
                          ['lblEmails', 'chkEmails', 3, 3, -4],
                          ['lblNaoRebate', 'chkNaoRebate', 3, 3, -4],
                          ['lblCodigo', 'txtCodigo', 10, 3, -4],
                          ['lblCodigoAnterior', 'txtCodigoAnterior', 10, 3],
                          ['lblCodigoCRM', 'txtCodigoCRM', 10, 3, -11],
                          ['lblObservacao', 'txtObservacao', 30, 3, -3],
                          ['lblClienteDesde', 'txtClienteDesde', 10, 4],
                          ['lblAnos', 'txtAnos', 5, 4],
                          ['lblPedidos', 'txtPedidos', 5, 4],
                          ['lblFaturamentoAcumulado', 'txtFaturamentoAcumulado', 12, 4],
                          ['lblFaturamentoMensal', 'txtFaturamentoMensal', 12, 4],
                          ['lblFaturamentoPedido', 'txtFaturamentoPedido', 12, 4],
                          ['lblContribuicaoAcumulada', 'txtContribuicaoAcumulada', 12, 4],
                          ['lblContribuicaoAcumuladaRecebida', 'txtContribuicaoAcumuladaRecebida', 12, 4],
                          ['lblMaiorAtraso', 'txtMaiorAtraso', 5, 4, -5],
                          ['lbldtMaiorCompra', 'txtdtMaiorCompra', 10, 5, 0, -4],
                          ['lblValorMaiorCompra', 'txtValorMaiorCompra', 12, 5],
                          ['lbldtUltimaCompra', 'txtdtUltimaCompra', 10, 5],
                          ['lblDiasUltimaCompra', 'txtDiasUltimaCompra', 5, 5],
                          ['lblValorUltimaCompra', 'txtValorUltimaCompra', 12, 5],
                          ['lbldtMaiorAcumulo', 'txtdtMaiorAcumulo', 10, 5],
                          ['lblValorMaiorAcumulo', 'txtValorMaiorAcumulo', 12, 5]]);

    //@@ *** Sociedade - Div secundario superior divSup02_02 ***/
    adjustElementsInForm([['lblParticipacao_02', 'txtParticipacao_02', 7, 2],
                          ['lbldtAdmissao_02', 'txtdtAdmissao_02', 10, 2],
                          ['lblContratoSocial_02', 'chkContratoSocial', 3, 2]],
                          ['lblHrSociedade', 'hrSociedade']);


    //@@ *** Empregat�cio - Div secundario superior divSup02_03 ***/
    adjustElementsInForm([['lblFuncionarioDireto', 'chkFuncionarioDireto', 3, 2],
						  ['lbldtAdmissao_03', 'txtdtAdmissao_03', 10, 2, -7],
                          ['lbldtDesligamento_03', 'txtdtDesligamento_03', 10, 2],
                          ['lblTipoFopagID', 'SelTipoFopagID', 12, 2],
                          ['lblCodigoCRM_03', 'txtCodigoCRM_03', 10, 2],


    //Linha 3
                          ['lbldtVigencia_03', 'txtdtdtVigencia_03', 10, 3],
                          ['lblDepartamentoID_01', 'selDepartamentoID_01', 25, 3],
                          ['lblCargoEmpregID_03', 'selCargoEmpregID_03', 25, 3],
                          ['lblNivel_01', 'selNivel_01', 5, 3],
                          ['lblSalario', 'txtSalario', 10, 3],
                          ['lblObservacao_03', 'txtObservacao_03', 30, 3],

    //Linha 4
                          ['lblSalarioCarteira', 'txtSalarioCarteira', 10, 4],
                          ['lblPesoPremio', 'txtPesoPremio', 10, 4, -15],
                          ['lblPercentual401K', 'txtPercentual401K', 6, 4]],

                          ['lblHrEmpregaticios', 'hrEmpregaticios']);

    //@@ *** Funcional - Div secundario superior divSup02_04 ***/
    adjustElementsInForm([['lblEmpresaID_04', 'selEmpresaID_04', 20, 2],
                          ['lblEstadoID_04', 'selEstadoID_04', 5, 2],
                          ['lbldtAdmissao_04', 'txtdtAdmissao_04', 10, 2],
                          ['lbldtDesligamento_04', 'txtdtDesligamento_04', 10, 2],

    //['lblDepartamentoID_01', 'selDepartamentoID_01', 15, 2],

                          ['lblCargoID_04', 'selCargoID_04', 30, 2],
                          ['lblResponsavel_04', 'chkResponsavel_04', 3, 2],
                          ['lblObservacao_04', 'txtObservacao_04', 30, 3]],
                          ['lblHrFuncional', 'hrFuncional']);

    //@@ *** Departamental - Div secundario superior divSup02_05 ***/
//    adjustElementsInForm([['lblEmpresaID_05', 'selEmpresaID_05', 20, 2]],
//                          ['lblHrDepartamental', 'hrDepartamental']);

    //@@ *** Patrocinio a Prg Mkt - Div secundario superior divSup02_06 ***/
    adjustElementsInForm([['lblParticipacao_06', 'txtParticipacao_06', 7, 2],
                          ['lblObservacao_06', 'txtObservacao_06', 30, 2]],
                          ['lblHrPatrocinioMkt', 'hrPatrocinioMkt']);

    //@@ *** Participa��o em Prg Mkt - Div secundario superior divSup02_07 ***/
    adjustElementsInForm([['lblIdentificador_07', 'txtIdentificador_07', 10, 2],
                          ['lbldtFiliacao_07', 'txtdtFiliacao_07', 10, 2],
                          ['lblFiliadorID_07', 'selFiliadorID_07', 20, 2],
                          ['btnFindFiliador', 'btn', 21, 2],
                          ['lblObservacao_07', 'txtObservacao_07', 30, 2]],
                          ['lblHrParticipacaoMkt', 'hrParticipacaoMkt']);


    //@@ *** Afinidade - Div secundario superior divSup02_08 ***/
    adjustElementsInForm([['lblTipoAfinidadeID_08', 'selTipoAfinidadeID_08', 11, 2],
                          ['lblConsanguinea', 'chkConsanguinea', 3, 2],
                          ['lblGrau', 'txtGrau', 3, 2],
                          ['lblTipoCompatibilidadeID', 'selTipoCompatibilidadeID', 13, 2],
                          ['lblDependenteSalarioFamilia', 'chkDependenteSalarioFamilia', 3, 2],
                          ['lblDependenteImpostoRenda', 'chkDependenteImpostoRenda', 3, 2],
                          ['lblEmpresaID', 'selEmpresaID', 10, 2],
                          ['lblObservacao_08', 'txtObservacao_08', 30, 2]],
                          ['lblHrAfinidade', 'hrAfinidade']);

    //@@ *** Asstec - Div secundario superior divSup02_09 ***/
    adjustElementsInForm([['lblObservacao_09', 'txtObservacao_09', 30, 2]],
                          ['lblHrAsstec', 'hrAsstec']);

    //@@ *** Transporte - Div secundario superior divSup02_10 ***/
    adjustElementsInForm([['lblOrdem_10', 'txtOrdem_10', 4, 2],
                          ['lblObservacao_10', 'txtObservacao_10', 30, 2]],
                          ['lblHrTransporte', 'hrTransporte']);

    //@@ *** Banco - Div secundario superior divSup02_11 ***/
    adjustElementsInForm([['lblOrdem_11', 'txtOrdem_11', 4, 2],
                          ['lblBanco', 'txtBanco', 9, 2],
                          ['lblAgencia', 'txtAgencia', 7, 2],
                          ['lblObservacao_11', 'txtObservacao_11', 30, 2]],
                          ['lblHrBanco', 'hrBanco']);

    //@@ *** Servi�os - Div secundario superior divSup02_12 ***/
    adjustElementsInForm([['lblServico', 'txtServico', 20, 2],
                          ['lblObservacao_12', 'txtObservacao_12', 30, 2]],
                          ['lblHrServicos', 'hrServicos']);

    //@@ *** Grupo de Trabalho - Div secundario superior divSup02_13 ***/
    adjustElementsInForm([['lblOrdem_12', 'txtOrdem_12', 4, 2]],
                          ['lblHrGrupoTrabalho', 'hrGrupoTrabalho']);

    //@@ *** Deposito - Div secundario superior divSup02_14 ***/
    adjustElementsInForm([['lblEhDefault', 'chkEhDefault', 3, 2]],
                          ['lblHrDeposito', 'hrDeposito']);

    //@@ *** Filia��o - Div secundario superior divSup02_15 ***/
    adjustElementsInForm([['lblContabilidadeCentralizada', 'chkContabilidadeCentralizada', 3, 2],
                          ['lblTransferePedidos', 'chkTransferePedidos', 3, 2]],
                          ['lblHrFiliacao', 'hrFiliacao']);

    //@@ *** Deposito - Div secundario superior divSup02_16 ***/
    adjustElementsInForm([['lblEhDefault_16', 'chkEhDefault_16', 3, 2]],
                          ['lblHrLocalEntrega', 'hrLocalEntrega']);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    if (param1 == 'SHOWRELPESSOAS') {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;

        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();

        if (param2[0] != empresa[0])
            return null;

        window.top.focus();

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);

        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@

    // Campos sempre readonly
    txtUsuarioCredito.disabled = true;
    txtServico.disabled = true;
    selTipoAfinidadeID_08.disabled = true;
    txtBanco.disabled = true;
    txtAgencia.disabled = true;
    txtClassificacaoPessoa.disabled = true;
    txtCodigoCRM.disabled = false;
    txtCodigoCRM_03.disable = false;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked) {
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    if ((btnClicked == 'SUPDET') ||
         (btnClicked == 'SUPREFR') ||
         (btnClicked == 'SUPANT') ||
         (btnClicked == 'SUPSEG')) {
        startDynamicCmbs(2);
    }
    else
    // volta para a automacao
        finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    // Ativo e Relacao de Clientelia
    // if( (newEstadoID == 2) && (dsoSup01.recordset['TipoRelacaoID'].value == 21) ) 
    //{
    verifyRelPessoasInServer(currEstadoID, newEstadoID);
    return true;

    //}

    //@@ prossegue a automacao
    //return false;    
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {
    controlFornecedorCliente();
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    clearAndLockCmbsDynamics(btnClicked);

    // Ajusta os labels de sujeito e objeto
    // Apenas nos forms de relacoes
    changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID', 'lblObjetoID', null);
    setLabelOfControl(lblFiliadorID_07, selFiliadorID_07.value);
    showHideControls();

    if (btnClicked == 'SUPINCL') {
        // funcao que controla campos read-only
        controlReadOnlyFields();

        // Habilita desabilita campo frete chkFrete
        direitoCampos();

        // Habilita desabilita campos Cliente/Fornecedor
        controlFornecedorCliente(true);
    }
    else {
        // Ajusta o texto (masc/fem) do combo de afinidades
        if (getCurrTipoRegID() == 37)
            fillCmbAfinBySex(dsoSup01.recordset['Sexo'].value);

        // Preenche o campo calculado de servico
        if (getCurrTipoRegID() == 25) // Servico
            setServico(dsoSup01.recordset['Servico'].value);

        // Habilita desabilita campos Cliente/Fornecedor
        controlFornecedorCliente();
    }

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
cmb     - referencia do combo

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario
    if (cmbID == 'selTipoRegistroID') {
        clearComboEx(['selSujeitoID', 'selObjetoID']);
        clearComboEx(['selEmpresaID']);

        selSujeitoID.disabled = true;
        selObjetoID.disabled = true;
        selEmpresaID.disabled = true;

        lockBtnLupa(btnFindSujeito, true);
        lockBtnLupa(btnFindObjeto, true);
        selFiliadorID_07.disabled = true;

        if (cmbID.selectedIndex != -1)
            lockBtnLupa(btnFindSujeito, false);

        if (cmb.value == 25)
            txtServico.value = '';

        changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID',
                           'lblObjetoID', 'selTipoRegistroID',
                           selSujeitoID.value, selObjetoID.value);
        setLabelOfControl(lblFiliadorID_07, selFiliadorID_07.value);
    }
    else if (cmbID == 'selCargoEmpregID_03') {
        fillCmbNivel(1);
    }

    adjustLabelsCombos(false);

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selTipoRegistroID') {
        adjustSupInterface();

        // Habilita desabilita campos Cliente/Fornecedor
        controlFornecedorCliente(true);
    }
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {
    //@@
    var cmbID = '';
    var nTipoRelacaoID = '';
    var nRegExcluidoID = '';

    if (btnClicked.id == 'btnFindSujeito') {
        nTipoRelacaoID = selTipoRegistroID.value;
        nRegExcluidoID = '';
        showModalRelacoes(window.top.formID, 'S', 'selSujeitoID', 'SUJ', getLabelNumStriped(lblSujeitoID.innerText), nTipoRelacaoID, nRegExcluidoID);
        return null;
    }
    else if (btnClicked.id == 'btnFindObjeto') {
        nTipoRelacaoID = selTipoRegistroID.value;
        nRegExcluidoID = selSujeitoID.value;

        showModalRelacoes(window.top.formID, 'S', 'selObjetoID', 'OBJ', getLabelNumStriped(lblObjetoID.innerText), nTipoRelacaoID, nRegExcluidoID);
        return null;
    }
    else if (btnClicked.id == 'btnFindFiliador') {
        showModalRelacoes(window.top.formID, 'S', 'selFiliadorID_07', '', getLabelNumStriped(lblFiliadorID_07.innerText), nTipoRelacaoID, nRegExcluidoID);
        return null;
    }

    // Nao mexer - Inicio de automacao ==============================
    // Invoca janela modal
    loadModalOfLupa(cmbID, null);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: mode 1: Edicao
2: Paginacao

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs(nMode) {
    glb_nCmbDynamicsMode = nMode;
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selSujeitoID, selObjetoID,selFiliadorID_07)
    glb_CounterCmbsDynamics = 6;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selSujeitoID)
    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT PessoaID AS fldID,Fantasia AS fldName, Sexo, ' +
                              '(CASE WHEN ((SELECT COUNT(*) FROM RelacoesPesRec WITH(NOLOCK) ' +
                                               'WHERE ((SujeitoID= ' + dsoSup01.recordset['SujeitoID'].value + ' OR ' +
                                                       'SujeitoID= ' + dsoSup01.recordset['ObjetoID'].value + ') AND ' +
                                  'ObjetoID=999 AND TipoRelacaoID=12))>0) ' +
                                         'THEN 1 ELSE 0 END) AS EmpresaSistema ' +
                          'FROM Pessoas WITH(NOLOCK) ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['SujeitoID'].value;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    // parametrizacao do dso dsoCmbDynamic02 (designado para selObjetoID)
    setConnection(dsoCmbDynamic02);

    dsoCmbDynamic02.SQL = 'SELECT PessoaID AS fldID,Fantasia AS fldName,Sexo,Observacao AS Servico ' +
                          'FROM Pessoas WITH(NOLOCK) ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['ObjetoID'].value;
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();

    // parametrizacao do dso dsoCmbDynamic03 (designado para selFiliadorID_07)
    setConnection(dsoCmbDynamic03);

    dsoCmbDynamic03.SQL = 'SELECT PessoaID AS fldID,Fantasia AS fldName ' +
              'FROM Pessoas WITH(NOLOCK) ' +
              'WHERE PessoaID = ' + dsoSup01.recordset['FiliadorID'].value;
    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic03.Refresh();
    setConnection(dsoCmbDynamic05);
    dsoCmbDynamic05.SQL = "SELECT  a.Fantasia AS fldName, a.PessoaID AS fldID " +
        "FROM dbo.Pessoas a WITH(NOLOCK) " +
        "INNER JOIN Pessoas_Empresas b WITH(NOLOCK) ON b.PessoaID=a.PessoaID " +
        "WHERE ((a.TipoPessoaID=54) AND (a.EstadoID=2) AND (b.EmpresaID=" + dsoSup01.recordset['ObjetoID'].value + ")) " +
        "ORDER BY b.CodigoProvedor";

    dsoCmbDynamic05.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic05.Refresh();

    setConnection(dsoCmbDynamic06);
    dsoCmbDynamic06.SQL = "SELECT a.RecursoFantasia AS fldName, a.RecursoID AS fldID, b.NivelMaximo " +
        "FROM Recursos a WITH(NOLOCK) " +
	        "INNER JOIN dbo.Recursos_Empresas b WITH(NOLOCK) ON b.RecursoID = a.RecursoID " +
        "WHERE ((a.EstadoID = 2) AND (a.TipoRecursoID=6) AND (a.EhCargo=1) AND (b.EmpresaID=" + dsoSup01.recordset['ObjetoID'].value + ")) " +
        "ORDER BY b.CodigoProvedor";

    dsoCmbDynamic06.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic06.Refresh();

    // parametrizacao do dso dsoCmbDynamic07 (designado para selEmpresaID)
    dsoCmbDynamic07.SQL = 'SELECT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
                          'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ObjetoID) ' +
                          'WHERE ((TipoRelacaoID = 31) AND (a.SujeitoID = ' + dsoSup01.recordset['ObjetoID'].value + '))';

    dsoCmbDynamic07.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic07.Refresh();
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selSujeitoID, selObjetoID, selFiliadorID_07, selDepartamentoID_01, selCargoEmpregID_03, selEmpresaID];
    var aDSOsDunamics = [dsoCmbDynamic01, dsoCmbDynamic02, dsoCmbDynamic03, dsoCmbDynamic05, dsoCmbDynamic06, dsoCmbDynamic07];

    // Inicia o carregamento de combos dinamicos (selSujeitoID, selObjetoID, selFiliadorID_07
    //
    clearComboEx(['selSujeitoID', 'selObjetoID', 'selFiliadorID_07', 'selDepartamentoID_01', 'selCargoEmpregID_03', 'selEmpresaID']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i <= 5; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';

            while (!aDSOsDunamics[i].recordset.EOF) {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
                optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;

                if (i == 5) {
                    oOption.setAttribute("NivelMaximo", aDSOsDunamics[i].recordset['NivelMaximo'].value, 1);
                }

                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }

            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        if (glb_nCmbDynamicsMode == 2) {
            // Combo de Sujeito
            // Controla se sujeito ou objeto sao empresas do sistema
            if (aDSOsDunamics[0].recordset.Fields.Count != 0) {
                if (aDSOsDunamics[0].recordset.RecordCount() != 0) {
                    aDSOsDunamics[0].recordset.MoveFirst();
                    if ((aDSOsDunamics[0].recordset['EmpresaSistema'].value) == 1)
                        glb_SujOrObjFromSystem = true;
                    else
                        glb_SujOrObjFromSystem = false;
                }
            }
        }

        adjustLabelsCombos(true);

        //fillCmbNivel(2);
        fillCmbNivel(glb_nCmbDynamicsMode);

        if (glb_nCmbDynamicsMode == 2) {
            controlFornecedorCliente();

            // volta para a automacao
            finalOfSupCascade(glb_BtnFromFramWork);
        }
        else {
            alert('lockfalse');
            lockInterface(false);
        }
    }
    return null;
}


/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros: nMode 1: Edicao
2: Paginacao


Retorno:
nenhum
********************************************************************/
function fillCmbNivel(nMode) {

    if (nMode == 2)
        selOptByValueInSelect(getHtmlId(), 'selCargoEmpregID_03', dsoSup01.recordset['CargoID'].value);

    //var nNivelMaximo = (nMode == 1 ? selCargoEmpregID_03.options.item(selCargoEmpregID_03.selectedIndex).getAttribute('NivelMaximo', 1) :
    //    dsoSup01.recordset['Nivel'].value);

    var nNivelMaximo = 0;

    if (selCargoEmpregID_03.selectedIndex >= 0)
        nNivelMaximo = selCargoEmpregID_03.options.item(selCargoEmpregID_03.selectedIndex).getAttribute('NivelMaximo', 1);

    clearComboEx(['selNivel_01']);

    for (i = 1; i <= nNivelMaximo; i++) {
        optionStr = i;
        optionValue = i;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selNivel_01.add(oOption);
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    if (controlBar == 'SUP') {
        var empresa = getCurrEmpresaData();

        // Documentos
        if ((controlBar == 'SUP') && (btnClicked == 1)) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
        // usuario clicou botao imprimir
        else if (btnClicked == 2)
            openModalPrint();
        else if (btnClicked == 3) {
            window.top.openModalControleDocumento('S', '', 720, null, '21', 'T');
        }
        // Detalhar sujeito
        else if (btnClicked == 4) {
            // Manda o id da pessoa a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], selSujeitoID.value));
        }
        // Detalhar objeto
        else if (btnClicked == 5) {
            // Manda o id da pessoa a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], selObjetoID.value));
        }
        else if (btnClicked == 6) {
            openModalAlteracaoCredito();
        }
        else if (btnClicked == 7) {
            window.top.openModalHTML(dsoSup01.recordset['RelacaoID'].value, 'Resumo da Rela��o', 'S', 8);
        }
        //rodrigo
        else if (btnClicked == 8) {
            openModalConvidarUsuario();
        }
    }
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    // Para prosseguir a automacao retornar null
    glb_LastBtnSupClicked = btnClicked;

    if (btnClicked == 'SUPALT') {
		;
	}
    else if (btnClicked == 'SUPOK') {
 		;
    }
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) 
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    // Modal de relacoes
    if (idElement.toUpperCase() == 'MODALRELACOESHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            fillFieldsByRelationModal(param2);

            if ((param2[0] == 'OBJ') && (param2[1] == 37))
                preencherEmpresa();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de impressao
    else if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALALTERACAOCREDITOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            glb_refreshSupTimer = window.setInterval('refreshSup()', 10, 'JavaScript');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal convidar Usuario Rodrigo 21/06/2011
    else if (idElement.toUpperCase() == 'MODALCONVIDARUSUARIOHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            //glb_nCallModal = window.setInterval('enviaEMailSenha(' + (param2 == true ? 'true' : 'false') + ')', 50, 'JavaScript');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode() {
    if (selTipoRelacaoID = 37) // Afinidades
    {
        if (selSujeitoID.selectedIndex != -1)
            selTipoAfinidadeID_08.disabled = false;
        else
            selTipoAfinidadeID_08.disabled = true;
    }

    if (selTipoRelacaoID = 18) // Participacao em Programa de Mkt
    {
        if (selFiliadorID_07.options.length != 0)
            selFiliadorID_07.disabled = false;
        else
            selFiliadorID_07.disabled = true;
    }

    // Habilita desabilita campos Cliente/Fornecedor
    controlFornecedorCliente(null, true);

    // funcao que controla campos read-only
    controlReadOnlyFields();

    direitoCampos();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad() {
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Sujeito', 'Detalhar Objeto', 'Alterar cr�dito', 'Resumo', 'Convidar Usu�rio']);
}

/********************************************************************
Funcao criada pelo programador.
Limpa e trava os combos dinamicos quando o usuario clicar em inclusao

Parametro:
btnClicked  -> Ultimo botao clicado da barra superior

Retorno:
nenhum
********************************************************************/
function clearAndLockCmbsDynamics(btnClicked) {
    if (btnClicked == 'SUPINCL') {
        clearComboEx(['selSujeitoID', 'selObjetoID']);
        selSujeitoID.disabled = true;
        selObjetoID.disabled = true;
        selFiliadorID_07.disabled = true;

        // trava o botao de lupa do sujeito
        lockBtnLupa(btnFindSujeito, true);

        // trava o botao de lupa do objeto
        lockBtnLupa(btnFindObjeto, true);
    }
}

/********************************************************************
Funcao criada pelo programador.
Preenche o combo de afinidades se for o caso.
Ou seja se o sujeito da relacao tiver sexo diferente do sexo
registrado no controle selTipoAfinidadeID_08

Parametros:
sexID   - opcional id do sexo ou null

Retorno:
nenhum
********************************************************************/
function fillCmbAfinBySex(sexID) {
    // Trap. So executa se o tipo de registro == 37
    if (getCurrTipoRegID() != 37)
        return null;

    // repreenche combo de afinidades, em funcao do sexo do sujeito
    // default para masculino
    if (sexID == null)
        sexID = 91;

    if (sexID != selTipoAfinidadeID_08.getAttribute('currSujSex', 1))
        reloadCmbTipoAfin(sexID);
}

/********************************************************************
Funcao criada pelo programador.
Atende a funcao fillCmbAfinBySex()
Preenche o combo de afinidades se for o caso.
Ou seja se o sujeito da relacao tiver sexo diferente do sexo
registrado no controle selTipoAfinidadeID_08

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function reloadCmbTipoAfin(sexID) {
    var theDSO = dsoEstaticCmbs;

    selTipoAfinidadeID_08.setAttribute('currSujSex', sexID, 1);

    clearComboEx(['selTipoAfinidadeID_08']);

    var optionStr, optionValue;

    var bmk = theDSO.recordset.Bookmark();

    theDSO.recordset.setFilter('Indice = 9');

    theDSO.recordset.MoveFirst();

    while (!theDSO.recordset.EOF) {
        var oOption = document.createElement("OPTION");

        if (sexID == 92)
            oOption.text = theDSO.recordset['fldNameAlt'].value;
        else
            oOption.text = theDSO.recordset['fldName'].value;

        oOption.value = theDSO.recordset['fldID'].value;

        selTipoAfinidadeID_08.add(oOption);

        theDSO.recordset.MoveNext();
    }

    theDSO.recordset.setFilter('');

    if (bmk != null)
        theDSO.recordset.gotoBookmark(bmk);

    return true;
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o campo de servico.

Parametros:
servStr - opcional, null ou descricao do servico

Retorno:
nenhum
********************************************************************/
function setServico(servStr) {
    if (servStr == null)
        serStr = '';

    txtServico.value = servStr;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint() {
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();

    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars += '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);
    strPars += '&nContextoID=' + escape(contexto[1]);
    strPars += '&nUserID=' + escape(userID);
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/modalpages/modalprint.asp' + strPars;

    showModalWin(htmlPath, new Array(346, 230));
}


/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos com direitos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function direitoCampos() {
    // Habilita desabilita campo frete chkFrete
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));

    var bReadOnly;

    //DireitoEspecifico
    //Relacao Pesssoas->Clientes->SUP
    //20140 SFS-Grupo Relacao entre Pessoas-> A1&&A2 -> Permite setar check box de frete.

    if ((nA1 == 1) && (nA2 == 1))
        bReadOnly = false;
    else
        bReadOnly = true;

    chkFrete.disabled = bReadOnly;
    
    selClassificacaoID.disabled = bReadOnly;
    selListaPreco.disabled = bReadOnly;
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
aData[0] - quem e na relacao ('SUJ' ou 'OBJ')
aData[1] - ID do tipo da relacao
aData[2] - ID do campo do subform corrente
aData[3] - ID selecionado no grid da modal
aData[4] - texto correspondente ao ID selecionado no grid da modal
            
os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData) {
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;

    elem = window.document.getElementById(aData[2]);

    if (elem == null)
        return;

    clearComboEx([elem.id]);

    oldDataSrc = elem.dataSrc;
    oldDataFld = elem.dataFld;
    elem.dataSrc = '';
    elem.dataFld = '';

    // Preencher o combo selSujeitoID
    if (aData[2] == 'selSujeitoID')
        dsoSup01.recordset['SujeitoID'].value = aData[3];
    // Preencher o combo selObjetoID
    else if (aData[2] == 'selObjetoID')
        dsoSup01.recordset['ObjetoID'].value = aData[3];
    // Preencher o combo selFiliadorID_07
    else if (aData[2] == 'selFiliadorID_07')
        dsoSup01.recordset['FiliadorID'].value = aData[3];

    oOption = document.createElement("OPTION");
    oOption.text = aData[4];
    oOption.value = aData[3];
    elem.add(oOption);
    elem.selectedIndex = 0;

    elem.dataSrc = oldDataSrc;
    elem.dataFld = oldDataFld;

    if (aData.length > 5)
        addSexOrServ = aData[5];

    optChangedInCmbSujOrObj(elem, addSexOrServ);

    if ((aData[2] == 'selObjetoID') && (selTipoRegistroID.value == 31))
        startDynamicCmbs(1);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
cmb     - referencia do combo
addSexOrServ  - ID do sexo ou descricao do servico

Retorno:
nenhum
********************************************************************/
function optChangedInCmbSujOrObj(cmb, addSexOrServ) {
    var cmbID = cmb.id;

    if (cmbID == 'selSujeitoID') {
        clearComboEx(['selObjetoID']);
        selObjetoID.disabled = true;
        lockBtnLupa(btnFindObjeto, true);

        if (cmb.selectedIndex != -1) {
            // destrava o botao de lupa do objeto
            lockBtnLupa(btnFindObjeto, false);
            // destrava o combo de afinidades
            selTipoAfinidadeID_08.disabled = false;
        }

        // Preenche o combo de afinidades
        lockInterface(true);
        fillCmbAfinBySex(addSexOrServ);
        lockInterface(false);

    }
    else if (cmbID == 'selObjetoID') {
        // Preenche o campo calculado de servico
        if (getCurrTipoRegID() == 25) // Servico
            setServico(addSexOrServ);
    }

    if ((cmbID == 'selSujeitoID') || (cmbID == 'selObjetoID'))
        changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID',
                           'lblObjetoID', 'selTipoRegistroID',
                           selSujeitoID.value, selObjetoID.value);
    else if (cmbID == 'selFiliadorID_07')
        setLabelOfControl(lblFiliadorID_07, selFiliadorID_07.value);
}

function adjustLabelsCombos(bPaging) {
    if (bPaging) {
        setLabelOfControl(lblTipoRelacaoID, dsoSup01.recordset['TipoRelacaoID'].value);
        setLabelOfControl(lblClassificacaoID, dsoSup01.recordset['ClassificacaoID'].value);
        setLabelOfControl(lblConceitoID, dsoSup01.recordset['ConceitoID'].value);
        setLabelOfControl(lblUsuarioCredito, dsoSup01.recordset['UsuarioCreditoID'].value);
        setLabelOfControl(lblCargoEmpregID_03, dsoSup01.recordset['CargoID'].value);
        setLabelOfControl(lblCargoID_04, dsoSup01.recordset['CargoID'].value);
        //Heraldo
        setLabelOfControl(lblDepartamentoID_01, dsoSup01.recordset['DepartamentoID'].value);
        //Incluido para mostrar o ID do Tipo da Fopag. BJBN
        setLabelOfControl(lblTipoFopagID, dsoSup01.recordset['TipoFopagID'].value);
        //Incluido para mostrar o ID da Empresa. BJBN
        setLabelOfControl(lblEmpresaID, dsoSup01.recordset['EmpresaID'].value);


    }
    else {
        setLabelOfControl(lblTipoRelacaoID, selTipoRegistroID.value);
        setLabelOfControl(lblClassificacaoID, selClassificacaoID.value);
        setLabelOfControl(lblConceitoID, selConceitoID.value);
        setLabelOfControl(lblCargoEmpregID_03, selCargoEmpregID_03.value);
        setLabelOfControl(lblCargoID_04, selCargoID_04.value);
        //Heraldo
        setLabelOfControl(lblDepartamentoID_01, selDepartamentoID_01.value);
        //Incluido para mostrar o ID do Tipo da Fopag. BJBN
        setLabelOfControl(lblTipoFopagID, SelTipoFopagID.value);
        //Incluido para mostrar o ID da Empresa. BJBN
        setLabelOfControl(lblEmpresaID, SelTipoFopagID.value);


    }

}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Read Only

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields() {
    txtClassificacaoPessoa.readOnly = true;
    txtUsuarioCredito.readOnly = true;
    txtdtMaiorCompra.readOnly = true;
    txtValorMaiorCompra.readOnly = true;
    txtFaturamentoAcumulado.readOnly = true;
    txtAnos.readOnly = true;
    txtFaturamentoMensal.readOnly = true;
    txtPedidos.readOnly = true;
    txtFaturamentoPedido.readOnly = true;
    txtMaiorAtraso.readOnly = true;
    txtdtUltimaCompra.readOnly = true;
    txtDiasUltimaCompra.readOnly = true;
    txtValorUltimaCompra.readOnly = true;
    txtdtMaiorAcumulo.readOnly = true;
    txtValorMaiorAcumulo.readOnly = true;
    txtContribuicaoAcumulada.readOnly = true;
    txtContribuicaoAcumuladaRecebida.readOnly = true;
    chkEmpresaColigada.disabled = true;
    selConceitoID.disabled = true;
    txtCodigoCRM.readOnly = false;
    txtCodigoCRM_03.readOnly = false;
}

/********************************************************************
Funcao criada pelo programador.
Controla os campos Fornecedor/Cliente

Parametro:
bHidden

Retorno:
nenhum
********************************************************************/
function controlFornecedorCliente(bHidden, bEditMode) {
    var sVisibilityCliente = 'hidden';
    var sVisibilityFornecedor = 'hidden';
    var nSujeitoID;
    var nObjetoID;
    var nSujeitoEmpresaSistema;
    var nObjetoEmpresaSistema;
    var aEmpresaData = getCurrEmpresaData();
    var nEmpresaID;

    bEditMode = (bEditMode == true ? true : false);

    if (!bHidden) {
        nEmpresaID = aEmpresaData[0];
        nSujeitoID = dsoSup01.recordset['SujeitoID'].value;
        nObjetoID = dsoSup01.recordset['ObjetoID'].value;
        nSujeitoEmpresaSistema = dsoSup01.recordset['nSujeitoEmpresaSistema'].value;
        nObjetoEmpresaSistema = dsoSup01.recordset['nObjetoEmpresaSistema'].value;

        // Cliente
        if ((nSujeitoEmpresaSistema == 0) && (nObjetoEmpresaSistema >= 1)) {
            sVisibilityCliente = 'inherit';
            sVisibilityFornecedor = 'hidden';
        }
        // Fornecedor
        else if ((nSujeitoEmpresaSistema >= 1) && (nObjetoEmpresaSistema >= 0)) {
            sVisibilityCliente = 'hidden';
            sVisibilityFornecedor = 'inherit';
        }
        else if ((nSujeitoEmpresaSistema >= 1) && (nObjetoEmpresaSistema >= 1)) {
            if (nEmpresaID == nObjetoID) {
                sVisibilityCliente = 'inherit';
                sVisibilityFornecedor = 'hidden';
            }
            else if (nEmpresaID == nSujeitoID) {
                sVisibilityCliente = 'hidden';
                sVisibilityFornecedor = 'inherit';
            }
        }
    }

    lbldtMaiorCompra.style.visibility = sVisibilityCliente;
    txtdtMaiorCompra.style.visibility = sVisibilityCliente;

    lblValorMaiorCompra.style.visibility = sVisibilityCliente;
    txtValorMaiorCompra.style.visibility = sVisibilityCliente;

    lblFaturamentoAcumulado.style.visibility = sVisibilityCliente;
    txtFaturamentoAcumulado.style.visibility = sVisibilityCliente;

    lblFaturamentoMensal.style.visibility = sVisibilityCliente;
    txtFaturamentoMensal.style.visibility = sVisibilityCliente;

    lblPedidos.style.visibility = sVisibilityCliente;
    txtPedidos.style.visibility = sVisibilityCliente;

    lblFaturamentoPedido.style.visibility = sVisibilityCliente;
    txtFaturamentoPedido.style.visibility = sVisibilityCliente;

    lblMaiorAtraso.style.visibility = sVisibilityCliente;
    txtMaiorAtraso.style.visibility = sVisibilityCliente;

    lbldtUltimaCompra.style.visibility = sVisibilityCliente;
    txtdtUltimaCompra.style.visibility = sVisibilityCliente;

    lblDiasUltimaCompra.style.visibility = sVisibilityCliente;
    txtDiasUltimaCompra.style.visibility = sVisibilityCliente;

    lblValorUltimaCompra.style.visibility = sVisibilityCliente;
    txtValorUltimaCompra.style.visibility = sVisibilityCliente;

    lbldtMaiorAcumulo.style.visibility = sVisibilityCliente;
    txtdtMaiorAcumulo.style.visibility = sVisibilityCliente;

    lblValorMaiorAcumulo.style.visibility = sVisibilityCliente;
    txtValorMaiorAcumulo.style.visibility = sVisibilityCliente;

    lblContribuicaoAcumulada.style.visibility = sVisibilityCliente;
    txtContribuicaoAcumulada.style.visibility = sVisibilityCliente;

    lblContribuicaoAcumuladaRecebida.style.visibility = sVisibilityCliente;
    txtContribuicaoAcumuladaRecebida.style.visibility = sVisibilityCliente;
}
/********************************************************************
Recebe input de janela modal para discar pelo carrier.
Definir e implementar no pesqlist, no sup e no inf do form.
********************************************************************/
function dialByModalPage(sDDD, sNumero, nPessoaID) {
    sDDD = sDDD.toString();
    sNumero = sNumero.toString();
    nPessoaID = parseInt(nPessoaID, 10);

    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

function openModalAlteracaoCredito() {
    var htmlPath, nEmpresaID, nPessoaID, nSujeitoEmpresaSistema, nObjetoEmpresaSistema;
    var nFaturamentoDiretoColigada = 0;
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];

    nSujeitoEmpresaSistema = dsoSup01.recordset['nSujeitoEmpresaSistema'].value;
    nObjetoEmpresaSistema = dsoSup01.recordset['nObjetoEmpresaSistema'].value;

    if (nObjetoEmpresaSistema == 1) {
        nEmpresaID = dsoSup01.recordset['ObjetoID'].value;
        nPessoaID = dsoSup01.recordset['SujeitoID'].value;
    }
    else if (nSujeitoEmpresaSistema == 1) {
        nEmpresaID = dsoSup01.recordset['SujeitoID'].value;
        nPessoaID = dsoSup01.recordset['ObjetoID'].value;
    }
    else {
        nFaturamentoDiretoColigada = 1;
        nEmpresaID = empresaID;
        nPessoaID = dsoSup01.recordset['SujeitoID'].value;
    }

    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalalteracaocredito.asp';

    // Passar parametro se veio do inf ou do sup
    var strPars = new String();
    strPars = '?sCaller=' + escape('S');
    strPars += '&nUsuarioID=' + escape(getCurrUserID());
    strPars += '&nRelacaoID=' + escape(dsoSup01.recordset['RelacaoID'].value);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nFaturamentoDiretoColigada=' + escape(nFaturamentoDiretoColigada);

    showModalWin((htmlPath + strPars), new Array(0, 0));
}

function getCmbConceitoData() {
    var i = 0;
    var aCmb = new Array();

    for (i = 0; i < selConceitoID.options.length; i++) {
        aCmb[i] = new Array();
        aCmb[i][0] = selConceitoID.options[i].text;
        aCmb[i][1] = selConceitoID.options[i].value;
    }

    return aCmb;
}

/********************************************************************
Funcao criada pelo programador.
Da refresh no sup

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function refreshSup() {
    if (glb_refreshSupTimer != null) {
        window.clearInterval(glb_refreshSupTimer);
        glb_refreshSupTimer = null;
    }

    __btn_REFR('sup');
}

function showHideControls() {
    // Se afinidades
    if (dsoSup01.recordset['TipoRelacaoID'].value == 37) {
        lblDependenteSalarioFamilia.visibility = 'inherit';
        chkDependenteSalarioFamilia.visibility = 'inherit';
        lblDependenteImpostoRenda.visibility = 'inherit';
        chkDependenteImpostoRenda.visibility = 'inherit';
        lblEmpresaID.visibility = 'inherit';
        selEmpresaID.visibility = 'inherit';
    }
    else {
        lblDependenteSalarioFamilia.visibility = 'hidden';
        chkDependenteSalarioFamilia.visibility = 'hidden';
        lblDependenteImpostoRenda.visibility = 'hidden';
        chkDependenteImpostoRenda.visibility = 'hidden';
        lblEmpresaID.visibility = 'hidden';
        selEmpresaID.visibility = 'hidden';
    }
}

/********************************************************************
Funcao criada pelo programador.
Verificacoes da Localidade

Parametros:
nCurrEstadoID ID do estado atual
nNewEstadoID  ID do estado novo

Retorno:
nenhum
********************************************************************/
function verifyRelPessoasInServer(nCurrEstadoID, nNewEstadoID) {
    var nRelacaoID = dsoSup01.recordset['RelacaoID'].value;
    var strPars = new String();

    strPars = '?nRelacaoID=' + escape(nRelacaoID);
    strPars += '&nCurrEstadoID=' + escape(nCurrEstadoID);
    strPars += '&nNewEstadoID=' + escape(nNewEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/verificacaorelpessoas.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyRelPessoasInServer_DSC;
    dsoVerificacao.refresh();
}

function verifyRelPessoasInServer_DSC() {

    var sMensagem = (dsoVerificacao.recordset['Mensagem'].value == null ? '' : dsoVerificacao.recordset['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset['Resultado'].value == null ? 0 : dsoVerificacao.recordset['Resultado'].value);

    if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            stateMachSupExec('OK');
        else
            stateMachSupExec('CANC');
    }
    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('OK');
    }
    else {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('CANC');
    }
}

//Rodrigo bot�o 8
function openModalConvidarUsuario() {

    var htmlPath;

    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalconvidarusuario.asp';


    // Passar o id do label do combo que vai preencher
    // para configurar a janela
    // e o id do combo que vai preencher, que volta com a mensagem
    // para saber o combo a preencher
    var strPars = new String();
    var nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value");

    strPars = '?nSujeitoID=' + escape(nSujeitoID);

    showModalWin((htmlPath + strPars), new Array(700, 200));


    return null;

}

function preencherEmpresa()
{
    dsoEmpresa.SQL = 'SELECT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
                          'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ObjetoID) ' +
                          'WHERE ((TipoRelacaoID = 31) AND (a.SujeitoID = ' + selObjetoID.value + '))';

    dsoEmpresa.ondatasetcomplete = preencherEmpresa_DSC;
    dsoEmpresa.Refresh();
}

function preencherEmpresa_DSC()
{
    var optionStr, optionValue;

    clearComboEx(['selEmpresaID']);

    dsoEmpresa.recordset.moveFirst();

    while (!dsoEmpresa.recordset.EOF) {
        optionStr = dsoEmpresa.recordset['fldName'].value;
        optionValue = dsoEmpresa.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        selEmpresaID.add(oOption);

        dsoEmpresa.recordset.moveNext();

        selEmpresaID.disabled = false;
    }
}