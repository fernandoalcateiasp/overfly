/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Relacoes Entre Pessoas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
-> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
registro o filtro deve ser por proprietarioID e nao pelo identity
do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister) {
    var sSQL;

    //@@
    var nUserID = getCurrUserID();
    var sSQL2 = '';
	var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
	var nContextoID = aItemSelected[1];    
	//alert(nContextoID);
    

    if (glb_LastBtnSupClicked == 'SUPREFR') {
        sSQL2 += 'CONVERT(VARCHAR, dbo.fn_Pessoa_Data(SujeitoID, ObjetoID, NULL, 3, GETDATE()-10000, GETDATE()), ' + DATE_SQL_PARAM + ') AS dtMaiorCompra, ' +
			(nContextoID == 1221 ? 'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 5, GETDATE()-10000, GETDATE(), 1)' : 'NULL') + ' AS ValorMaiorCompra, ' +
			(nContextoID == 1221 ? 'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 1, GETDATE()-10000, GETDATE(), 1)' : 'NULL') + ' AS FaturamentoAcumulado, ' +
			'CONVERT(VARCHAR, ClienteDesde, ' + DATE_SQL_PARAM + ' ) AS V_ClienteDesde, ' +
			(nContextoID == 1221 ? '(CASE WHEN ClienteDesde IS NULL THEN NULL ELSE CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 11, GETDATE()-10000, GETDATE(), 1)) END)' : 'NULL') + ' AS Anos, ' +
			(nContextoID == 1221 ? 'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 13, GETDATE()-10000, GETDATE(), 1)' : 'NULL') + ' AS FaturamentoMensal, ' +
			(nContextoID == 1221 ? 'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 12, GETDATE()-10000, GETDATE(), 1)' : 'NULL') + ' AS FaturamentoPedido, ' +
			(nContextoID == 1221 ? 'CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 9, GETDATE()-10000, GETDATE(), 1))' : 'NULL') + ' AS MaiorAtraso, ' +
			(nContextoID == 1221 ? 'CONVERT(VARCHAR, dbo.fn_Pessoa_Data(SujeitoID, ObjetoID, NULL, 2, GETDATE()-10000, GETDATE()), ' + DATE_SQL_PARAM + ' )' : 'NULL') + ' AS V_dtUltimaCompra, ' +
            (nContextoID == 1221 ? 'CONVERT(INT, dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 18, GETDATE()-10000, GETDATE(), 0))' : 'NULL') + ' AS diasUltimaCompra, ' +
			(nContextoID == 1221 ? 'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 4, GETDATE()-10000, GETDATE(), 1)' : 'NULL') + ' AS ValorUltimaCompra, ' +
			(nContextoID == 1221 ? 'CONVERT(VARCHAR, dbo.fn_Pessoa_Data(SujeitoID, ObjetoID, NULL, 4, GETDATE()-10000, GETDATE()), ' + DATE_SQL_PARAM + ' )' : 'NULL') + ' AS dtMaiorAcumulo, ' +
			(nContextoID == 1221 ? 'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 6, GETDATE()-10000, GETDATE(), 1)' : 'NULL') + ' AS ValorMaiorAcumulo, ' +
			(nContextoID == 1221 ? 'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 2, GETDATE()-10000, GETDATE(), 1)' : 'NULL') + ' AS ContribuicaoAcumulada, ' +
			(nContextoID == 1221 ? 'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 3, GETDATE()-10000, GETDATE(), 1)' : 'NULL') + ' AS ContribuicaoAcumuladaRecebida, ' +
			'(SELECT COUNT(*) FROM RelacoesPesRec WITH(NOLOCK) WHERE (TipoRelacaoID = 12 AND SujeitoID = RelacoesPessoas.SujeitoID AND ObjetoID = 999 AND EstadoID = 2)) AS nSujeitoEmpresaSistema, ' +
			'(SELECT COUNT(*) FROM RelacoesPesRec WITH(NOLOCK) WHERE (TipoRelacaoID = 12 AND SujeitoID = RelacoesPessoas.ObjetoID AND ObjetoID = 999 AND EstadoID = 2)) AS nObjetoEmpresaSistema, ';
    }
    else {
        sSQL2 += 'NULL AS dtMaiorCompra, ' +
			'NULL AS ValorMaiorCompra, ' +
			'NULL AS FaturamentoAcumulado, ' +
			'CONVERT(VARCHAR, ClienteDesde, ' + DATE_SQL_PARAM + ' ) AS V_ClienteDesde, ' +
			'(CASE WHEN ClienteDesde IS NULL THEN NULL ELSE CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 11, GETDATE()-10000, GETDATE(), 1)) END) AS Anos, ' +
			'NULL AS FaturamentoMensal, ' +
			'NULL AS FaturamentoPedido, ' +
			'NULL AS MaiorAtraso, ' +
			'NULL AS V_dtUltimaCompra, ' +
			'NULL AS diasUltimaCompra, ' +
			'NULL AS ValorUltimaCompra, ' +
			'NULL AS dtMaiorAcumulo, ' +
			'NULL AS ValorMaiorAcumulo, ' +
			'NULL AS ContribuicaoAcumulada, ' +
			'NULL AS ContribuicaoAcumuladaRecebida, ' +
			'(SELECT COUNT(*) FROM RelacoesPesRec WITH(NOLOCK) WHERE (TipoRelacaoID = 12 AND SujeitoID = RelacoesPessoas.SujeitoID AND ObjetoID = 999 AND EstadoID = 2)) AS nSujeitoEmpresaSistema, ' +
			'(SELECT COUNT(*) FROM RelacoesPesRec WITH(NOLOCK) WHERE (TipoRelacaoID = 12 AND SujeitoID = RelacoesPessoas.ObjetoID AND ObjetoID = 999 AND EstadoID = 2)) AS nObjetoEmpresaSistema, ';
    }

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' + sSQL2 +
               '(SELECT ItemMasculino ' +
                    'FROM Pessoas WITH(NOLOCK) ' +
		                'INNER JOIN TiposAuxiliares_Itens Itens WITH(NOLOCK) ON Pessoas.ClassificacaoID = Itens.ItemID ' +
                    'WHERE Pessoas.PessoaID = RelacoesPessoas.SujeitoID) AS ClassificacaoPessoa, ' +
               (nContextoID == 1221 ? 'CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 10, GETDATE()-10000, GETDATE(), 1))' : '0') + ' AS Pedidos, ' +
			   (nContextoID == 1221 ? 'dbo.fn_Pessoa_Fantasia(UsuarioCreditoID, 0)' : 'SPACE(1)') + ' As UsuarioCredito, ' +
               'CONVERT(VARCHAR, dtAdmissao, ' + DATE_SQL_PARAM + ') as V_dtAdmissao,' +
               'CONVERT(VARCHAR, dtDesligamento, ' + DATE_SQL_PARAM + ') as V_dtDesligamento,' +
               'CONVERT(VARCHAR, dtVigencia, ' + DATE_SQL_PARAM + ') as V_dtVigencia,' +
               (nContextoID == 1221 ? '(SELECT TOP 1 a.Financiamento FROM FinanciamentosPadrao a WITH(NOLOCK) WHERE (a.FinanciamentoID = dbo.fn_RelacoesPessoas_FinanciamentoPadrao(-RelacaoID, NULL, NULL)))' : 'NULL') + ' AS FinanciamentoDefault, ' +
			   (nContextoID == 1221 ? '(SELECT TOP 1 a.ItemAbreviado FROM TiposAuxiliares_Itens a WITH(NOLOCK) WHERE (a.ItemID = dbo.fn_RelacoesPessoas_FormaPagamento(-RelacaoID, NULL, NULL, NULL, NULL)))' : 'NULL') + ' AS FormaPagamentoDefault, ' +
			   'Sexo = (SELECT TOP 1 Sexo FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = RelacoesPessoas.SujeitoID) ), ' +
			   'Servico = (SELECT TOP 1 Observacao FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = RelacoesPessoas.ObjetoID) ), ' +
			   'Banco = (SELECT TOP 1 b.Codigo FROM Pessoas a WITH(NOLOCK), Bancos b WITH(NOLOCK) WHERE (a.PessoaID = RelacoesPessoas.ObjetoID AND a.BancoID = b.BancoID) ), ' +
			   'Agencia = (SELECT TOP 1 ISNULL(Agencia, SPACE(0)) + ISNULL(CHAR(45) + AgenciaDV, SPACE(0)) FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = RelacoesPessoas.ObjetoID) ), ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=RelacoesPessoas.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM RelacoesPessoas WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = ' + nID + ' ' +
               'ORDER BY RelacaoID DESC';
    else
        sSQL = 'SELECT *, ' + sSQL2 +
               '(SELECT ItemMasculino ' +
                     'FROM Pessoas WITH(NOLOCK) ' +
		                 'INNER JOIN TiposAuxiliares_Itens Itens WITH(NOLOCK) ON Pessoas.ClassificacaoID = Itens.ItemID ' +
                     'WHERE Pessoas.PessoaID = RelacoesPessoas.SujeitoID) AS ClassificacaoPessoa, ' +
               (nContextoID == 1221 ? 'CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 10, GETDATE()-10000, GETDATE(), 1))' : '0') + ' AS Pedidos, ' +
			   (nContextoID == 1221 ? 'dbo.fn_Pessoa_Fantasia(UsuarioCreditoID, 0)' : 'SPACE(1)') + ' As UsuarioCredito, ' +
               'CONVERT(VARCHAR, dtAdmissao, ' + DATE_SQL_PARAM + ') as V_dtAdmissao,' +
               'CONVERT(VARCHAR, dtDesligamento, ' + DATE_SQL_PARAM + ') as V_dtDesligamento,' +
               'CONVERT(VARCHAR, dtVigencia, ' + DATE_SQL_PARAM + ') as V_dtVigencia,' +
               (nContextoID == 1221 ? '(SELECT TOP 1 a.Financiamento FROM FinanciamentosPadrao a WITH(NOLOCK) WHERE (a.FinanciamentoID = dbo.fn_RelacoesPessoas_FinanciamentoPadrao(-RelacaoID, NULL, NULL)))' : 'NULL') + ' AS FinanciamentoDefault, ' +
			   (nContextoID == 1221 ? '(SELECT TOP 1 a.ItemAbreviado FROM TiposAuxiliares_Itens a WITH(NOLOCK) WHERE (a.ItemID = dbo.fn_RelacoesPessoas_FormaPagamento(-RelacaoID, NULL, NULL, NULL, NULL)))' : 'NULL') + ' AS FormaPagamentoDefault, ' +
			   'Sexo = (SELECT TOP 1 Sexo FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = RelacoesPessoas.SujeitoID) ), ' +
			   'Servico = (SELECT TOP 1 Observacao FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = RelacoesPessoas.ObjetoID) ), ' +
			   'Banco = (SELECT TOP 1 b.Codigo FROM Pessoas a WITH(NOLOCK), Bancos b WITH(NOLOCK) WHERE (a.PessoaID = RelacoesPessoas.ObjetoID AND a.BancoID = b.BancoID) ), ' +
			   'Agencia = (SELECT TOP 1 ISNULL(Agencia, SPACE(0)) + ISNULL(CHAR(45) + AgenciaDV, SPACE(0)) FROM Pessoas WHERE (PessoaID = RelacoesPessoas.ObjetoID) ), ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
    	       'WHEN (SELECT COUNT (*) FROM RelacoesPessoas ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=RelacoesPessoas.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM RelacoesPessoas WITH(NOLOCK) ' +
               'WHERE RelacaoID = ' + nID + ' ORDER BY RelacaoID DESC';

    setConnection(dso);

    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();

    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso) {
    setConnection(dso);

    var sql;

    var sSQL2 = 'CONVERT(VARCHAR, dbo.fn_Pessoa_Data(SujeitoID, ObjetoID, NULL, 3, GETDATE()-10000, GETDATE()), ' + DATE_SQL_PARAM + ') AS dtMaiorCompra, ' +
		'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 5, GETDATE()-10000, GETDATE(), 1) AS ValorMaiorCompra, ' +
		'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 1, GETDATE()-10000, GETDATE(), 1) AS FaturamentoAcumulado, ' +
		'CONVERT(VARCHAR, ClienteDesde, ' + DATE_SQL_PARAM + ' ) AS V_ClienteDesde, ' +
		'(CASE WHEN ClienteDesde IS NULL THEN NULL ELSE CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 11, GETDATE()-10000, GETDATE(), 1)) END) AS Anos, ' +
		'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 13, GETDATE()-10000, GETDATE(), 1) AS FaturamentoMensal, ' +
		'CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 10, GETDATE()-10000, GETDATE(), 1)) AS Pedidos, ' +
		'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 12, GETDATE()-10000, GETDATE(), 1) AS FaturamentoPedido, ' +
		'CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 9, GETDATE()-10000, GETDATE(), 1)) AS MaiorAtraso, ' +
		'CONVERT(VARCHAR, dbo.fn_Pessoa_Data(SujeitoID, ObjetoID, NULL, 2, GETDATE()-10000, GETDATE()), ' + DATE_SQL_PARAM + ' ) AS V_dtUltimaCompra, ' +
		'CONVERT(INT, dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 18, GETDATE()-10000, GETDATE(), 0)) AS diasUltimaCompra, ' +
		'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 4, GETDATE()-10000, GETDATE(), 1) AS ValorUltimaCompra, ' +
		'CONVERT(VARCHAR, dbo.fn_Pessoa_Data(SujeitoID, ObjetoID, NULL, 4, GETDATE()-10000, GETDATE()), ' + DATE_SQL_PARAM + ' ) AS dtMaiorAcumulo, ' +
		'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 6, GETDATE()-10000, GETDATE(), 1) AS ValorMaiorAcumulo, ' +
		'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 2, GETDATE()-10000, GETDATE(), 1) AS ContribuicaoAcumulada, ' +
		'dbo.fn_Pessoa_Totaliza(SujeitoID, ObjetoID, 3, GETDATE()-10000, GETDATE(), 1) AS ContribuicaoAcumuladaRecebida, ' +
		'(SELECT COUNT(*) FROM RelacoesPesRec  WHERE (TipoRelacaoID = 12 AND SujeitoID = RelacoesPessoas.SujeitoID AND ObjetoID = 999 AND EstadoID = 2)) AS nSujeitoEmpresaSistema, ' +
		'(SELECT COUNT(*) FROM RelacoesPesRec  WHERE (TipoRelacaoID = 12 AND SujeitoID = RelacoesPessoas.ObjetoID AND ObjetoID = 999 AND EstadoID = 2)) AS nObjetoEmpresaSistema, ';

    sql = 'SELECT *, ' + sSQL2 +
          '(SELECT ItemMasculino ' +
                        'FROM Pessoas  ' +
		                        'INNER JOIN TiposAuxiliares_Itens Itens  ON Pessoas.ClassificacaoID = Itens.ItemID ' +
                    'WHERE Pessoas.PessoaID = RelacoesPessoas.SujeitoID) AS ClassificacaoPessoa, ' +
		  'dbo.fn_Pessoa_Fantasia(UsuarioCreditoID, 0) As UsuarioCredito, ' +
          'CONVERT(VARCHAR, dtAdmissao, ' + DATE_SQL_PARAM + ') as V_dtAdmissao,' +
          'CONVERT(VARCHAR, dtDesligamento, ' + DATE_SQL_PARAM + ') as V_dtDesligamento,' +
          'CONVERT(VARCHAR, dtVigencia, ' + DATE_SQL_PARAM + ') as V_dtVigencia,' +
          '(SELECT TOP 1 a.Financiamento FROM FinanciamentosPadrao a  WHERE (a.FinanciamentoID = dbo.fn_RelacoesPessoas_FinanciamentoPadrao(-RelacaoID, NULL, NULL))) AS FinanciamentoDefault, ' +
		  '(SELECT TOP 1 a.ItemAbreviado FROM TiposAuxiliares_Itens a WHERE (a.ItemID = dbo.fn_RelacoesPessoas_FormaPagamento(-RelacaoID, NULL, NULL, NULL, NULL))) AS FormaPagamentoDefault, ' +
    	  'Sexo = (SELECT TOP 1 Sexo FROM Pessoas  WHERE (PessoaID = RelacoesPessoas.SujeitoID) ), ' +
		  'Servico = (SELECT TOP 1 Observacao FROM Pessoas  WHERE (PessoaID = RelacoesPessoas.ObjetoID) ), ' +
 		  'Banco = (SELECT TOP 1 b.Codigo FROM Pessoas a , Bancos b WITH(NOLOCK) WHERE (a.PessoaID = RelacoesPessoas.ObjetoID AND a.BancoID = b.BancoID) ), ' +
		  'Agencia = (SELECT TOP 1 ISNULL(Agencia, SPACE(0)) + ISNULL(CHAR(45) + AgenciaDV, SPACE(0)) FROM Pessoas  WHERE (PessoaID = RelacoesPessoas.ObjetoID) ), ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM RelacoesPessoas  WHERE RelacaoID = 0';

    dso.SQL = sql;
}              
