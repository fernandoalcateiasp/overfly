﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_relpessoas : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string sEmpresas = Convert.ToString(HttpContext.Current.Request.Params["sEmpresas"]);
        private string sParceiroID = Convert.ToString(HttpContext.Current.Request.Params["sParceiroID"]);
        private string sLinguaLogada = Convert.ToString(HttpContext.Current.Request.Params["sLinguaLogada"]);
        private string sDtInicio = Convert.ToString(HttpContext.Current.Request.Params["sDtInicio"]);
        private string sDtFim = Convert.ToString(HttpContext.Current.Request.Params["sDtFim"]);
        private string sProgramasMarketing = Convert.ToString(HttpContext.Current.Request.Params["sProgramasMarketing"]);
        private string sFiltro2 = Convert.ToString(HttpContext.Current.Request.Params["sFiltro2"]);
        string DATE_SQL_PARAM = HttpContext.Current.Request.Params["DATE_SQL_PARAM"];
        string sFornecedor = Convert.ToString(HttpContext.Current.Request.Params["nFornecedor"]);
        string sTodasEmpresas = Convert.ToString(HttpContext.Current.Request.Params["bTodasEmpresas"]);
        string sClientes = Convert.ToString(HttpContext.Current.Request.Params["nClientes"]);
        string sglb_sCaller = Convert.ToString(HttpContext.Current.Request.Params["glb_sCaller"]);
        string seguradora = Convert.ToString(HttpContext.Current.Request.Params["seguradora"]);
        string marca = Convert.ToString(HttpContext.Current.Request.Params["marca"]);
        string valorSolicitadoMinimo = Convert.ToString(HttpContext.Current.Request.Params["valorSolicitadoMinimo"]);
        string valorSolicitadoMaximo = Convert.ToString(HttpContext.Current.Request.Params["valorSolicitadoMaximo"]);
        string valorAprovadoMinimo = Convert.ToString(HttpContext.Current.Request.Params["valorAprovadoMinimo"]);
        string valorAprovadoMaximo = Convert.ToString(HttpContext.Current.Request.Params["valorAprovadoMaximo"]);
        string dataSolicitacaoInicio = Convert.ToString(HttpContext.Current.Request.Params["dataSolicitacaoInicio"]);
        string dataSolicitacaoFim = Convert.ToString(HttpContext.Current.Request.Params["dataSolicitacaoFim"]);
        string dataCancelamentoInicio = Convert.ToString(HttpContext.Current.Request.Params["dataCancelamentoInicio"]);
        string dataCancelamentoFim = Convert.ToString(HttpContext.Current.Request.Params["dataCancelamentoFim"]);
        string dataVencimentoInicio = Convert.ToString(HttpContext.Current.Request.Params["dataVencimentoInicio"]);
        string dataVencimentoFim = Convert.ToString(HttpContext.Current.Request.Params["dataVencimentoFim"]);
        string selAprovacao = Convert.ToString(HttpContext.Current.Request.Params["selAprovacao"]);
        Boolean chkPedidosPL = Convert.ToBoolean(HttpContext.Current.Request.Params["chkPedidosPL"]);
        Boolean chkPedidos = Convert.ToBoolean(HttpContext.Current.Request.Params["chkPedidos"]);
        Boolean chkProdutosPL = Convert.ToBoolean(HttpContext.Current.Request.Params["chkProdutosPL"]);
        Boolean chkProdutos = Convert.ToBoolean(HttpContext.Current.Request.Params["chkProdutos"]);
        Boolean chkFinanceirosPL = Convert.ToBoolean(HttpContext.Current.Request.Params["chkFinanceirosPL"]);
        Boolean chkFinanceiros = Convert.ToBoolean(HttpContext.Current.Request.Params["chkFinanceiros"]);
        Boolean chkObservacoesPL = Convert.ToBoolean(HttpContext.Current.Request.Params["chkObservacoesPL"]);
        Boolean chkObservacoes = Convert.ToBoolean(HttpContext.Current.Request.Params["chkObservacoes"]);
        


        string SQLImagem = string.Empty;
        string SQLDetail1 = string.Empty;
        string SQLDetail2 = string.Empty;
        string SQLDetail3 = string.Empty;
        string SQLDetail4 = string.Empty;

        int Datateste = 0;
        bool nulo = false;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "40101":
                        RelatorioDeExtratoDeCliente();
                        break;

                    case "40103":
                        extratoFinanceiro();
                        break;

                    case "40105":
                        programasMarketing();
                        break;

                    case "40109":
                        RelatorioSeguroDeCredito();
                        break;
                }

            }
        }

        public void extratoFinanceiro()
        {
            // Excel
            int Formato = 2;
            string Title = "Extrato Financeiro";
            string _Formato = "BRA";

            string[] ListColum1 = { "Empresa",
                                        "Financeiro",
                                        "Historico_Padrão",
                                        "Duplicata",
                                        "Emissão",
                                        "Vencimento",
                                        "Saldo",
                                        "Pagamento",
                                        "Valor",
                                        "Valor_Aplicado"};

            string[,] ListLabels = { {"Empresa","Company" },
                                        { "Financeiro","Entry" },
                                        { "Historico Padrão","Transaction" },
                                        { "Duplicata", "Invoice" },
                                        { "Emissão","Date" },
                                        { "Vencimento","Due Date" },
                                        { "Saldo", "Balance" },
                                        { "Pagamento", "Payment" },
                                        { "Valor","Amount" },
                                        { "Valor Aplicado", "Applied Amount"}};

            string strSQL = "EXEC sp_RelacoesPessoas_ExtratoFinanceiro " + sEmpresas + ", " + sParceiroID + ", 246, " + sLinguaLogada + ", " + sDtInicio + ", " + sDtFim;

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if(sLinguaLogada != "246")
            {
                Title = "Financial Statement";
                _Formato = "USA";
            }

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {


                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, _Formato, "Excel");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                    Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                    //Cria Tabela           
                    Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true);

                    int j = 0;
                    string Alinhamento = "Default";

                    if (sLinguaLogada == "245")
                    {
                        j = 1;
                    }

                    //Insere Colunas na tabela
                    for (int i = 0; i < ListColum1.Length; i++)
                    {
                        if (ListColum1[i] == "Saldo" || ListColum1[i] == "Valor" || ListColum1[i] == "Valor_Aplicado")
                        {
                            Alinhamento = "Right";
                        }

                        Relatorio.CriarObjColunaNaTabela(ListLabels[i, j], ListColum1[i], true, ListColum1[i], Alinhamento);
                        Relatorio.CriarObjCelulaInColuna("Query", ListColum1[i], "DetailGroup");
                    }

                    Relatorio.TabelaEnd();
                }
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }

        public void programasMarketing()
        {
            // Excel
            int Formato = 2;
            string Title = "Programa de Marketing";

            string strSQL = "SELECT " +
                                "RelPessoas.RelacaoID, Estados.RecursoAbreviado AS Estado, Pessoas.PessoaID, Pessoas.Fantasia, Programas.Fantasia AS Programa, RelPessoas.Identificador, " +
                                "RelPessoas.dtAdmissao, Paises.CodigoLocalidade2 AS Pais, Enderecos.CEP, UFs.CodigoLocalidade2 AS UF, Cidades.Localidade AS Cidade, Enderecos.Bairro, " +
                                "Enderecos.Endereco, Enderecos.Numero, Enderecos.Complemento " +
                            "FROM " +
                                "RelacoesPessoas RelPessoas WITH(NOLOCK) " +
                                    "INNER JOIN Recursos Estados WITH(NOLOCK) ON(Estados.RecursoID = RelPessoas.EstadoID) " +
                                    "INNER JOIN Pessoas WITH(NOLOCK) ON(Pessoas.PessoaID = RelPessoas.SujeitoID) " +
                                    "INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON(Enderecos.PessoaID = Pessoas.PessoaID) " +
                                    "INNER JOIN Localidades Paises WITH(NOLOCK) ON(Paises.LocalidadeID = Enderecos.PaisID) " +
                                    "INNER JOIN Localidades UFs WITH(NOLOCK) ON(UFs.LocalidadeID = Enderecos.UFID) " +
                                    "INNER JOIN Localidades Cidades WITH(NOLOCK) ON(Cidades.LocalidadeID = Enderecos.CidadeID) " +
                                    "INNER JOIN Pessoas Programas WITH(NOLOCK) ON(Programas.PessoaID = RelPessoas.ObjetoID) " +
                            "WHERE " +
                                "RelPessoas.TipoRelacaoID = 36 AND RelPessoas.EstadoID <> 5 ";

            if (sProgramasMarketing != "NULL")
            {
                strSQL += " AND (Programas.PessoaID = " + sProgramasMarketing + ") ";
            }

            if (sFiltro2 != "NULL")
            {
                strSQL += " AND (" + sFiltro2 + ") ";
            }

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "9", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }

        private void RelatorioDeExtratoDeCliente()
        {
         
            SQLImagem = "SELECT '' AS Vazio";

            SQLDetail1 = "EXEC sp_Relatorio_ExtratoCliente " +
                        sFornecedor + ", " +
                        sTodasEmpresas + ", " +
                        sClientes + ", " +
                        DATE_SQL_PARAM + ", " +
                        " 1, " +
                        sDtInicio + ", " +
                        sDtFim + ", " +
                        " null";

            SQLDetail2 = "EXEC sp_Relatorio_ExtratoCliente " +
                        sFornecedor + ", " +
                        sTodasEmpresas + ", " +
                        sClientes + ", " +
                        DATE_SQL_PARAM + ", " +
                        " 2, " +
                        sDtInicio + ", " +
                        sDtFim + ", " +
                        " null";

            SQLDetail3 = "EXEC sp_Relatorio_ExtratoCliente " +
                        sFornecedor + ", " +
                        sTodasEmpresas + ", " +
                        sClientes + ", " +
                        DATE_SQL_PARAM + ", " +
                        " 3, " +
                        sDtInicio + ", " +
                        sDtFim + ", " +
                        " null";

            SQLDetail4 = "EXEC sp_Relatorio_ExtratoCliente " +
                        sFornecedor + ", " +
                        sTodasEmpresas + ", " +
                        sClientes + ", " +
                        DATE_SQL_PARAM + ", " +
                        " 4, " +
                        sDtInicio + ", " +
                        sDtFim + ", " +
                        " null";

            printExtratoDeCliente();
            

        }

        private void printExtratoDeCliente()
        {

            ClsReport Relatorio = new ClsReport();

            DataSet Ds = new DataSet();

            string[,] arrQuerys;

            arrQuerys = new string[,] {
                                        {"sqlImagem", SQLImagem},
                                        {"sqlDetail1", SQLDetail1},
                                        {"sqlDetail2", SQLDetail2},
                                        {"sqlDetail3", SQLDetail3},
                                        {"sqlDetail4", SQLDetail4}
                                      };

            if (formato == 1)
            {

            string Left = "0.2";
            double espLinha = 0.4;
            double topHeader1 = 0;
            double topHeader2 = 0.4;
            string LeftCabecalho1 = "3";
            string LeftCabecalho2 = "12";
            string LeftCabecalho3 = "15";


            Relatorio.CriarRelatório("Extrato de Cliente", arrQuerys, "BRA", "Portrait", "1.6");

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];

            //Verifica se a Query está vazia e retorna mensagem de erro
            Datateste = dsFin.Tables.Count;


            if ((dsFin.Tables["SQLDetail1"].Rows.Count == 0) && (dsFin.Tables["SQLDetail2"].Rows.Count == 0)&& (dsFin.Tables["SQLDetail3"].Rows.Count == 0) && (dsFin.Tables["SQLDetail4"].Rows.Count == 0))
            {
                nulo = true;
            }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {

                    string sEndereco = dsFin.Tables["sqlDetail1"].Rows[0]["Endereco"].ToString();

                    Relatorio.CriarObjImage("Query", "sqlImagem", "Vazio", "0", "0", "Body", "5.09104", "1.74215");

                    Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, Left, "0.5", "12", "Black", "B", "Header", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Extrato de Cliente", "8", "0.5", "12", "Blue", "B", "Header", "6", "");
                    Relatorio.CriarObjLabelData("Now", "", "", "16.795", "0.5", "11", "black", "B", "Header", "3.72187", "Horas");
                    Relatorio.CriarObjLinha(Left, "1", "20", "", "Header");

                    Relatorio.CriarObjLabel("Fixo", "", "Dados Cadastrais:", Left, topHeader1.ToString(), "10", "Black", "B", "Body", "6", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Código: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "PessoaID", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "6", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Nome: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Nome", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "10", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Fantasia: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Fantasia", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "7", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Pessoa: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Pessoa", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "6", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Endereço: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Endereco", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "10", "");                    

                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "CEPCidadeUF", LeftCabecalho1, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "7", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Contato: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Contato", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "7", "");

                    Relatorio.CriarObjLabel("Fixo", "", "CNPJ: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "PessoaDocumento1", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "7", "");

                    Relatorio.CriarObjLabel("Fixo", "", "IE: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "PessoaDocumento2", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "7", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Telefones: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "PessoaTelefones", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "7", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Faxes: ", Left, (topHeader1 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "PessoaFaxes", LeftCabecalho1, topHeader1.ToString(), "10", "Black", "L", "Body", "7", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Transportadora: ", LeftCabecalho2, topHeader2.ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Transportadora", LeftCabecalho3, topHeader2.ToString(), "10", "Black", "L", "Body", "6", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Grupo: ", LeftCabecalho2, (topHeader2 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Conceito: ", LeftCabecalho2, (topHeader2 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Cadastro: ", LeftCabecalho2, (topHeader2 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Data Cadastro: ", LeftCabecalho2, (topHeader2 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Atualização: ", LeftCabecalho2, (topHeader2 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Fixo", "", "Movimento: ", LeftCabecalho2, (topHeader2 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Pedidos: ", LeftCabecalho2, (topHeader2 += espLinha).ToString(), "10", "Black", "L", "Body", "6", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Pedidos", LeftCabecalho3, topHeader2.ToString(), "10", "Black", "L", "Body", "6", "");

                    Relatorio.CriarObjLabel("Fixo", "", "Acumulado US$: ", LeftCabecalho2, (topHeader2 += espLinha).ToString(), "10", "Black", "L", "Body", "4", "");
                    Relatorio.CriarObjLabel("Query", "sqlDetail1", "Acumulado", LeftCabecalho3, topHeader2.ToString(), "10", "Black", "L", "Body", "4", "");

                    Relatorio.CriarObjLinha(Left, (topHeader1 += espLinha).ToString(), "20", "", "Body");

                    //Pedidos
                    if (((sglb_sCaller == "PL") && chkPedidosPL) || ((sglb_sCaller == "S") && chkPedidos))
                    {

                        Relatorio.CriarObjLabel("Fixo", "", "Pedidos", Left, (topHeader1 += 0.2).ToString(), "11", "Black", "B", "Body", "6", "");

                        //Tabela Pedidos
                        Relatorio.CriarObjTabela("0.2", (topHeader1 += 0.5).ToString(), "sqlDetail2", true);

                        //Coluna1 Pedidos
                        Relatorio.CriarObjColunaNaTabela("#", "ROW", true, "#", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ROW", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna2 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Data", "Data", true, "Data", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna3 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Pedido", "ID", true, "ID", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna4 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Est", "Estado", true, "Estado", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna5 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Pessoa", "Pessoa", true, "Pessoa", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna6 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Nota", "NotaFiscal", true, "Nota", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna7 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Data Nota", "DataNF", true, "Data Nota", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "DataNF", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna8 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Transação", "Transacao", false, "3", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna9 Pedidos
                        Relatorio.CriarObjColunaNaTabela("$", "Moeda", true, "$", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna10 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Valor", "Total", true, "Total", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Total", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna11 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Vendedor", "Vendedor", false, "3", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Vendedor", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna12 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "Empresa", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.TabelaEnd();

                        Relatorio.CriarObjLinha(Left, (topHeader1 += espLinha).ToString(), "20", "", "Body");

                    }

                    //Produtos
                    if (((sglb_sCaller == "PL") && chkProdutosPL) || ((sglb_sCaller == "S") && chkProdutos))
                    {


                        //Tabela Produtos
                        Relatorio.CriarObjTabela("0.2", (topHeader1 += 0.5).ToString(), "sqlDetail3", true);

                        //Coluna1 Produtos
                        Relatorio.CriarObjColunaNaTabela("#", "ROW", true, "#", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ROW", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna2 Produtos
                        Relatorio.CriarObjColunaNaTabela("Data", "Data", true, "Data", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna3 Produtos
                        Relatorio.CriarObjColunaNaTabela("Pedido", "ID", true, "ID", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");


                        //Coluna4 Produtos
                        Relatorio.CriarObjColunaNaTabela("Nota", "NotaFiscal", true, "Nota", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna5 Produtos
                        Relatorio.CriarObjColunaNaTabela("Nat", "CFOP", true, "Nat", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "CFOP", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna6 Produtos
                        Relatorio.CriarObjColunaNaTabela("Produto", "Produto", false, "4.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna7 Produtos
                        Relatorio.CriarObjColunaNaTabela("Qtde", "Quantidade", true, "Qtde", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna8 Produtos
                        Relatorio.CriarObjColunaNaTabela("Valor", "ValUnitario", true, "Valor", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValUnitario", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna9 Produtos
                        Relatorio.CriarObjColunaNaTabela("Valor $", "Valor", true, "Valor $", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna10 Produtos
                        Relatorio.CriarObjColunaNaTabela("Vendedor", "Vendedor", false, "4", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Vendedor", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.TabelaEnd();

                        Relatorio.CriarObjLinha(Left, (topHeader1 += espLinha).ToString(), "20", "", "Body");
                    }

                    //Financeiro
                    if (((sglb_sCaller == "PL") && chkFinanceirosPL) || ((sglb_sCaller == "S") && chkFinanceiros))
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "Financeiro", Left, (topHeader1 += 0.2).ToString(), "11", "Black", "B", "Body", "6", "");

                        //Tabela Financeiro
                        Relatorio.CriarObjTabela("0.2", (topHeader1 += 0.5).ToString(), "sqlDetail4", true);

                        //Coluna1 Financeiro
                        Relatorio.CriarObjColunaNaTabela("#", "Row", true, "#", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Row", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna2 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Venc", "Vencimento", true, "Venc", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Vencimento", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna3 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Atraso", "Atraso", true, "Atraso", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Atraso", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna4 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Tipo", "Tipo", true, "Tipo", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna5 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Forma", "FormaPagamento", true, "Forma", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "FormaPagamento", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna6 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Prazo", "Prazo", true, "Prazo", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Prazo", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna7 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Pessoa", "Pessoa", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna8 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", true, "Empresa $", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna9 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Financ", "ID", true, "Financ", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna10 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Duplicata", "Duplicata", true, "Duplicata", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna11 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Pedido", "Pedido", true, "Pedido", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna12 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Transação", "Transacao", false, "2.2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna13 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Saldo", "SaldoDevedor", true, "SaldoDevedor", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoDevedor", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna14 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Saldo Atual", "SaldoAtualizado", true, "Saldo Atual", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoAtualizado", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.TabelaEnd();

                        Relatorio.CriarObjLinha(Left, (topHeader1 += espLinha).ToString(), "20", "", "Body");

                    }

                    //Observações / Anotações

                    if (((sglb_sCaller == "PL") && chkObservacoesPL) || ((sglb_sCaller == "S") && chkObservacoes))
                    {
                        Relatorio.CriarObjLabel("Fixo", "", "Observações / Anotações", Left, (topHeader1 += 0.2).ToString(), "11", "Black", "B", "Body", "7", "");
                        Relatorio.CriarObjLabel("Query", "sqlDetail1", "Observacoes", Left, (topHeader1 += 0.5).ToString(), "10", "Black", "L", "Body", "20", "");
                    }

                    Relatorio.CriarPDF_Excel("Extrato de Cliente", 1);
                }
            }
            else if (formato == 2)
            {
                Relatorio.CriarRelatório("Extrato de clientes", arrQuerys, "BRA", "Excel");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if ((dsFin.Tables["SQLDetail1"].Rows.Count == 0) && (dsFin.Tables["SQLDetail2"].Rows.Count == 0) && (dsFin.Tables["SQLDetail3"].Rows.Count == 0) && (dsFin.Tables["SQLDetail4"].Rows.Count == 0))
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {                    
                    Relatorio.CriarObjTabela("0", "0", "sqlDetail1", true);

                    Relatorio.CriarObjColunaNaTabela("Código", "PessoaID", false, "2", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "PessoaID", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Nome", "Nome", false, "2", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Nome", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Fantasia", "Fantasia", false, "2.5", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Fantasia", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Pessoa", "Pessoa", false, "2.5", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Endereço", "Endereco", false, "2.5", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Endereco", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Contato", "Contato", false, "2", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Contato", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("CNPJ", "PessoaDocumento1", false, "3", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "PessoaDocumento1", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("IE", "PessoaDocumento2", false, "1.5", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "PessoaDocumento2", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Telefones", "PessoaTelefones", false, "2", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "PessoaTelefones", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Faxes", "PessoaFaxes", false, "3", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "PessoaFaxes", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Transportadora", "Transportadora", false, "2", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Transportadora", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Pedidos", "Pedidos", false, "2", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Pedidos", "DetailGroup", "Null", "Null", 0, false, "8");

                    Relatorio.CriarObjColunaNaTabela("Acumulado US$", "Acumulado", false, "1.5", "Default", "8");
                    Relatorio.CriarObjCelulaInColuna("Query", "Acumulado", "DetailGroup", "Null", "Null", 0, false, "8");

                    if (((sglb_sCaller == "PL") && chkObservacoesPL) || ((sglb_sCaller == "S") && chkObservacoes))
                    {
                        Relatorio.CriarObjColunaNaTabela("Observações / Anotações", "Observacoes", false, "10", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Observacoes", "DetailGroup", "Null", "Null", 0, false, "8");
                    }

                    Relatorio.TabelaEnd();

                    //Pedidos
                    if (((sglb_sCaller == "PL") && chkPedidosPL) || ((sglb_sCaller == "S") && chkPedidos))
                    {
                        //Tabela Pedidos
                        Relatorio.CriarObjTabela("0", "0.5", "sqlDetail2", true);

                        //Coluna2 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Data", "Data", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna3 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Pedido", "ID", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna4 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Est", "Estado", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Estado", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna5 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Pessoa", "Pessoa", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna6 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Nota", "NotaFiscal", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna7 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Data Nota", "DataNF", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "DataNF", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna8 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Transação", "Transacao", false, "3", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna9 Pedidos
                        Relatorio.CriarObjColunaNaTabela("$", "Moeda", false, "1.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Moeda", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna10 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Valor", "Total", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Total", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna11 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Vendedor", "Vendedor", false, "3", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Vendedor", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna12 Pedidos
                        Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.TabelaEnd();

                    }

                    //Produtos
                    if (((sglb_sCaller == "PL") && chkProdutosPL) || ((sglb_sCaller == "S") && chkProdutos))
                    {

                        //Tabela Produtos
                        Relatorio.CriarObjTabela("0", "1", "sqlDetail3", true);

                        //Coluna2 Produtos
                        Relatorio.CriarObjColunaNaTabela("Data", "Data", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Data", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna3 Produtos
                        Relatorio.CriarObjColunaNaTabela("Pedido", "ID", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");


                        //Coluna4 Produtos
                        Relatorio.CriarObjColunaNaTabela("Nota", "NotaFiscal", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "NotaFiscal", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna5 Produtos
                        Relatorio.CriarObjColunaNaTabela("Nat", "CFOP", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "CFOP", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna6 Produtos
                        Relatorio.CriarObjColunaNaTabela("Produto", "Produto", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Produto", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna7 Produtos
                        Relatorio.CriarObjColunaNaTabela("Qtde", "Quantidade", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Quantidade", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna8 Produtos
                        Relatorio.CriarObjColunaNaTabela("Valor", "ValUnitario", false, "3", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ValUnitario", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna9 Produtos
                        Relatorio.CriarObjColunaNaTabela("Valor $", "Valor", false, "1.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Valor", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna10 Produtos
                        Relatorio.CriarObjColunaNaTabela("Vendedor", "Vendedor", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Vendedor", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.TabelaEnd();

                    }

                    //Financeiro
                    if (((sglb_sCaller == "PL") && chkFinanceirosPL) || ((sglb_sCaller == "S") && chkFinanceiros))
                    {
 
                        //Tabela Financeiro
                        Relatorio.CriarObjTabela("0", "1.5", "sqlDetail4", true);

                        //Coluna2 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Venc", "Vencimento", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Vencimento", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna3 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Atraso", "Atraso", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Atraso", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna4 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Tipo", "Tipo", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Tipo", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna5 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Forma", "FormaPagamento", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "FormaPagamento", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna6 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Prazo", "Prazo", false, "2.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Prazo", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna7 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Pessoa", "Pessoa", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pessoa", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna8 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Empresa", "Empresa", false, "3", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Empresa", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna9 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Financ", "ID", false, "1.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "ID", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna10 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Duplicata", "Duplicata", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna11 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Pedido", "Pedido", false, "3", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Pedido", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna12 Financeiro
                        Relatorio.CriarObjColunaNaTabela("Transação", "Transacao", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "Transacao", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna13 Financeiro
                        Relatorio.CriarObjColunaNaTabela("SaldoDevedor", "SaldoDevedor", false, "2", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoDevedor", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna14 Financeiro
                        Relatorio.CriarObjColunaNaTabela("SaldoAtualizado", "SaldoAtualizado", false, "1.5", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "SaldoAtualizado", "DetailGroup", "Null", "Null", 0, false, "8");

                        //Coluna15 Financeiro
                        Relatorio.CriarObjColunaNaTabela("SeuPedido", "SeuPedido", false, "10", "Default", "8");
                        Relatorio.CriarObjCelulaInColuna("Query", "SeuPedido", "DetailGroup", "Null", "Null", 0, false, "8");

                        Relatorio.TabelaEnd();

                    }
                }

                Relatorio.CriarPDF_Excel("Extrato de Cliente", 2);
            }
        }
        public void RelatorioSeguroDeCredito()
        {
            // Excel
            int Formato = 2;
            string Title = "Seguros de Crédito";

            string strSQL = "EXEC sp_Relatorio_SeguroDeCredito " +
                            seguradora + ", " +
                            marca + ", " +
                            valorSolicitadoMinimo + ", " +
                            valorSolicitadoMaximo + ", " +
                            valorAprovadoMinimo + ", " +
                            valorAprovadoMaximo + ", " +
                            dataSolicitacaoInicio + ", " +
                            dataSolicitacaoFim + ", " +
                            dataCancelamentoInicio + ", " +
                            dataCancelamentoFim + ", " +
                            dataVencimentoInicio + ", " +
                            dataVencimentoFim + ", " +
                            selAprovacao + ", " +
                            nEmpresaID + ", " +
                            DATE_SQL_PARAM;

            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query está vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Não há dados a serem impressos.');window.parent.pb_StopProgressBar(true);window.parent.lockControlsInModalWin(false);</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                { 

                    Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "2.51332", "");
                    Relatorio.CriarObjLabel("Fixo", "", Title, "5.13834", "0.0", "11", "#0113a0", "B", Header_Body, "7.51332", "");
                    Relatorio.CriarObjLabelData("Now", "", "", "13.0", "0.0", "11", "black", "B", Header_Body, "3.92187", "Horas");

                    //Cria Tabela           
                    Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                    Relatorio.TabelaEnd();
                }
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}