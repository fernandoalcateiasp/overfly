using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;


namespace Overfly3.basico.relacoes.relpessoas.serverside
{
	/*public partial class verificacaorelpessoas : System.Web.UI.OverflyPage
	{
		private Integer RelacaoID;
		protected Integer nRelacaoID
		{
			get { return RelacaoID; }
			set { RelacaoID = value; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT dbo.fn_RelPessoas_Verifica( " + nRelacaoID + ") AS Verificacao"
			));
		}
	}*/

    ////////////////////////
    public partial class verificacaorelpessoas : System.Web.UI.OverflyPage
    {
        private static Integer zero = new Integer(0);
        private Integer relacaoID = zero;
        private Integer currEstadoID = zero;
        private Integer newEstadoID = zero;

        // Variaveis de saida.
        private string response;
        private string mensagem;

        protected Integer nRelacaoID
        {
            set { relacaoID = value != null ? value : zero; }
        }

        protected Integer nCurrEstadoID
        {
            set { currEstadoID = value != null ? value : zero; }
        }

        protected Integer nNewEstadoID
        {
            set { newEstadoID = value != null ? value : zero; }
        }


        protected void RelacaoPessoaVerifica()
        {
            ProcedureParameters[] procparam = new ProcedureParameters[5];

            procparam[0] = new ProcedureParameters(
                "@RelacaoID",
                SqlDbType.Int,
                relacaoID.ToString());

            procparam[1] = new ProcedureParameters(
                "@EstadoDeID",
                SqlDbType.Int,
                currEstadoID.ToString());

            procparam[2] = new ProcedureParameters(
               "@EstadoParaID",
               SqlDbType.Int,
               newEstadoID.ToString());

            procparam[3] = new ProcedureParameters(
                "@Resultado",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procparam[3].Length = 1;

            procparam[4] = new ProcedureParameters(
                "@Mensagem",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procparam[4].Length = 8000;

            // Executa a procedure.
            DataInterfaceObj.execNonQueryProcedure(
                "sp_RelacoesPessoas_Verifica", procparam
            );

            // Obt�m o resultado da execu��o.
            response = procparam[3].Data.ToString();
            mensagem = procparam[4].Data.ToString();
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            RelacaoPessoaVerifica();
            //updatePedidos();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select '" + response + "' as Resultado, '" + mensagem + "' as Mensagem"
                )
            );
        }
    }

}
