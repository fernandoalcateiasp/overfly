using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.PrintJet
{
	public partial class historicofuncional : System.Web.UI.OverflyPage
	{
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey; 
        private string controlID = Convert.ToString(HttpContext.Current.Request.Params["controlID"]);
        private int empresaID = Convert.ToInt32(HttpContext.Current.Request.Params["empresaID"]);
        private int nLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nLinguaLogada"]);
        private string sEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["sEmpresaFantasia"]);
        private int selCargo = Convert.ToInt32(HttpContext.Current.Request.Params["selCargo"]);
        private int selCargoIndex = Convert.ToInt32(HttpContext.Current.Request.Params["selCargoIndex"]);
        private string txtInicio = Convert.ToString(HttpContext.Current.Request.Params["txtInicio"]);
        private string txtInicio2 = Convert.ToString(HttpContext.Current.Request.Params["txtInicio2"]);
        private string txtFim = Convert.ToString(HttpContext.Current.Request.Params["txtFim"]);
        private string txtFim2 = Convert.ToString(HttpContext.Current.Request.Params["txtFim2"]);
        private int selModo = Convert.ToInt32(HttpContext.Current.Request.Params["selModo"]);
        private int selColaborador = Convert.ToInt32(HttpContext.Current.Request.Params["selColaborador"]);
        private int selColaboradorIndex = Convert.ToInt32(HttpContext.Current.Request.Params["selColaboradorIndex"]);
        private int selDepartamento = Convert.ToInt32(HttpContext.Current.Request.Params["selDepartamento"]);
        private int selDepartamentoIndex = Convert.ToInt32(HttpContext.Current.Request.Params["selDepartamentoIndex"]);
        private int selNivel = Convert.ToInt32(HttpContext.Current.Request.Params["selNivel"]);
        private int selNivelIndex = Convert.ToInt32(HttpContext.Current.Request.Params["selNivelIndex"]);
        private string sDtVigencia = Convert.ToString(HttpContext.Current.Request.Params["sDtVigencia"]);
        private string controle = Convert.ToString(HttpContext.Current.Request.Params["controle"]);

        protected override void PageLoad(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {
                switch (controle)
                {
                    case "listaExcel":
                        listaExcelHistoricoFuncionais();
                        break;

                    case "gravaDados":
                        gravaHistoricoFuncionais();
                        break;
                }
            }
        }
        public void listaExcelHistoricoFuncionais()
        {
            DateTime DataHoje = DateTime.Now;
            string param = "<Format>dd/mm/yyy</Format>";
            string _data = DataHoje.ToString("dd_MM_yyyy");

            if (nLinguaLogada != 246)
            {
                param = "<Format>mm/dd/yyy</Format>";
                _data = DataHoje.ToString("MM_dd_yyyy");
            }

            string sFiltro = "";

            /*Selec�o dos filtros*/
            if ((txtInicio != "") && (selModo == 1))
            {
                sFiltro += "AND a.dtVigencia >= " + "'" + txtInicio2 + "'";
            }

            if ((txtFim != "") && (selModo == 1))
            {
                sFiltro += " AND a.dtVigencia <= " + "'" + txtFim2 + "'";
            }

            if (selColaboradorIndex > 0)
                sFiltro += " AND c.PessoaID = " + selColaborador;

            if (selDepartamentoIndex > 0)
                sFiltro += " AND d.PessoaID = " + selDepartamento;

            if ((selCargoIndex > 0) && (selNivelIndex < 1))
            {
                if (selModo == 1)
                {
                    sFiltro += " AND a.CargoID = " + selCargo;
                }
                else
                {
                    sFiltro += " AND b.CargoID = " + selCargo;
                }
            }

            if ((selCargoIndex > 0) && (selNivelIndex > 0))
            {
                if (selModo == 1)
                {
                    sFiltro += " AND a.CargoID = " + selCargo + " AND a.Nivel = " + selNivel;
                }
                else
                {
                    sFiltro += " AND b.CargoID = " + selCargo + " AND b.Nivel = " + selNivel;
                }
            }

            string Title = "Historico_Funcionais_"+ sEmpresaFantasia + "_" + _data;

            string strSQL = "";

            if (selModo == 1)
            {
                strSQL = "SELECT b.RelacaoID, c.PessoaID AS ColaboradorID, c.Fantasia AS Colaborador, b.dtAdmissao, a.dtVigencia, a.Salario, d.PessoaID AS DepartamentoID, d.Fantasia AS Departamento, " +
                            "a.CargoID, Dbo.Fn_Cargo_Nome(a.CargoID, a.Nivel, b.ObjetoID, 1) AS Cargo, a.Nivel, NULL AS dtVigencia2, NULL AS Salario2, a.Observacao, CONVERT(BIT, 0) AS OK, dbo.fn_Colaborador_Alteracoes(a.RelPesHistoricoID) AS Alteracoes " +
                           "FROM RelacoesPessoas_HistoricosFuncionais a WITH(NOLOCK) " +
                           "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON b.RelacaoID=a.RelacaoID " +
                           "INNER JOIN Pessoas c WITH(NOLOCK) ON b.SujeitoID=c.PessoaID " +
                           "INNER JOIN Pessoas d WITH(NOLOCK) ON d.PessoaID=a.DepartamentoID " +
                           "WHERE b.TipoRelacaoID=31 " + sFiltro + " AND b.ObjetoID=" + empresaID + " AND c.EstadoID=2 " +
                           "ORDER BY Colaborador, dtVigencia DESC";
            }
            else
            {
                strSQL = "SELECT b.RelacaoID, c.PessoaID AS ColaboradorID, c.Fantasia AS Colaborador, b.dtAdmissao, b.dtVigencia, b.Salario, b.DepartamentoID, d.Fantasia AS Departamento, b.CargoID, Dbo.Fn_Cargo_Nome(b.CargoID, b.Nivel, b.ObjetoID, 1) AS Cargo,b.Nivel, " +
                           sDtVigencia + " AS dtVigencia2, dbo.fn_Cargo_Salario(b.CargoID, b.Nivel, b.ObjetoID, NULL) AS Salario2, " +
                           "convert(varchar(30),'') AS Observacao, 0 AS OK, SPACE(0) AS Alteracoes " +
                          "FROM RelacoesPessoas b WITH(NOLOCK) " +
                          "INNER JOIN Pessoas c WITH(NOLOCK) ON b.SujeitoID=c.PessoaID " +
                          "INNER JOIN Pessoas d WITH(NOLOCK) ON b.DepartamentoID=d.PessoaID " +
                          "WHERE b.TipoRelacaoID=31 AND b.EstadoID= 2 " + sFiltro + " AND b.ObjetoID=" + empresaID + " AND c.EstadoID=2 " +
                          "ORDER BY c.Fantasia";
            }

            if (controlID == "btnExcel")//SE EXCEL
            {
                arrSqlQuery = new string[,] { { "Query1", strSQL } };

                string[,] arrIndexKey = new string[,] { { "" } };

                Relatorio.CriarRelat�rio(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                int Datateste = 0;
                bool nulo = false;

                DataSet dsFin = new DataSet();

                dsFin = (DataSet)HttpContext.Current.Session["Query"];

                //Verifica se a Query est� vazia e retorna mensagem de erro
                Datateste = dsFin.Tables.Count;

                if (dsFin.Tables["Query1"].Rows.Count == 0)
                {
                    nulo = true;
                }

                if (nulo)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                    HttpContext.Current.Response.End();
                    return;
                }
                else
                {
                    Relatorio.CriarObjTabela("0.00", "0.00", "Query1", true, false);

                    Relatorio.CriarObjColunaNaTabela("ColaboradorID", "ColaboradorID", true, "ColaboradorID");
                    Relatorio.CriarObjCelulaInColuna("Query", "ColaboradorID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Colaborador", "Colaborador", true, "Colaborador");
                    Relatorio.CriarObjCelulaInColuna("Query", "Colaborador", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Admiss�o", "dtAdmissao", true, "Admiss�o");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtAdmissao", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("Vig�ncia", "dtVigencia", true, "Vig�ncia");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtVigencia", "DetailGroup", Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("Sal�rio", "Salario", true, "Sal�rio");
                    Relatorio.CriarObjCelulaInColuna("Query", "Salario", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("DepartamentoID", "DepartamentoID", true, "DepartamentoID");
                    Relatorio.CriarObjCelulaInColuna("Query", "DepartamentoID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Departamento", "Departamento", true, "Departamento");
                    Relatorio.CriarObjCelulaInColuna("Query", "Departamento", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("CargoID", "CargoID", true, "CargoID");
                    Relatorio.CriarObjCelulaInColuna("Query", "CargoID", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Cargo", "Cargo", true, "Cargo");
                    Relatorio.CriarObjCelulaInColuna("Query", "Cargo", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Nivel", "Nivel", true, "Nivel");
                    Relatorio.CriarObjCelulaInColuna("Query", "Nivel", "DetailGroup");

                    Relatorio.CriarObjColunaNaTabela("Observa��o", "Observacao", true, "Observa��o");
                    Relatorio.CriarObjCelulaInColuna("Query", "Observacao", "DetailGroup");

                    Relatorio.TabelaEnd();

                    Relatorio.CriarPDF_Excel(Title, 2);
                }
            }
            else//Se Lista
            {
                DataSet dtTeste = DataInterfaceObj.getRemoteData(strSQL);
                WriteResultXML(dtTeste);
            }
        }

        //Par�metros para m�todo de gravar altera��es na Grid
        private Integer dataLen = Constants.INT_ZERO;
        public Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value; }
        }

        private Integer[] relacaoID = Constants.INT_EMPTY_SET;
        public Integer[] RelacaoID
        {
            get { return relacaoID; }
            set { relacaoID = value; }
        }

        private Integer[] departamentoID = Constants.INT_EMPTY_SET;
        public Integer[] DepartamentoID
        {
            get { return departamentoID; }
            set { departamentoID = value; }
        }

        private Integer[] cargoID = Constants.INT_EMPTY_SET;
        public Integer[] CargoID
        {
            get { return cargoID; }
            set { cargoID = value; }
        }

        private Integer[] nivel = Constants.INT_EMPTY_SET;
        public Integer[] Nivel
        {
            get { return nivel; }
            set { nivel = value; }
        }

        private string[] salario = Constants.STR_EMPTY_SET;
        public string[] Salario
        {
            get { return salario; }
            set { salario = value; }
        }

        private string[] DTVigencia = Constants.STR_EMPTY_SET;
        public string[] dtVigencia
        {
            get { return DTVigencia; }
            set { DTVigencia = value; }
        }

        private string[] observacao = Constants.STR_EMPTY_SET;
        public string[] Observacao
        {
            get { return observacao; }
            set { observacao = value; }
        }

        protected string SQL
        {
            get
            {
                string sql = "";

                sql = DepartamentoID[0].ToString();
                sql = CargoID[0].ToString();
                sql = Nivel[0].ToString();
                sql = Salario[0];
                sql = dtVigencia[0];
                sql = Observacao[0];
                sql = RelacaoID[0].ToString();

                sql = "";

                for (int i = 0; i < nDataLen.intValue(); i++)
                {
                    sql += " UPDATE RelacoesPessoas SET " +
                        "DepartamentoID=" + DepartamentoID[i].ToString() + ", " +
                        "CargoID=" + CargoID[i].ToString() + ", " +
                        "Nivel=" + Nivel[i].ToString() + ", " +
                        "Salario=" + Salario[i].ToString() + ", " +
                        "DtVigencia=" + dtVigencia[i] + ", " +
                        "Observacao=" + Observacao[i] + " " +
                        "WHERE RelacaoID=" + RelacaoID[i].ToString();
                }
                return sql;
            }
        }
        //Gravar altera��es na grid
        public void gravaHistoricoFuncionais()
        {
            DataInterfaceObj.ExecuteSQLCommand(SQL);
            // DataInterfaceObj.getRemoteData("select '' as Resultado");
            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT '' AS Resultado"));
        }
	}
}
