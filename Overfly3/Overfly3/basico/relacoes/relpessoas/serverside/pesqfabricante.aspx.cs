using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.relacoes.relpessoas.serverside
{
	public partial class pesqfabricante : System.Web.UI.OverflyPage
	{
		private string toFind;
		protected string strToFind
		{
			get { return toFind; }
			set { toFind = value; }
		}

		private Integer relacaoID;
		protected Integer nRelacaoID
		{
			get { return relacaoID; }
			set { relacaoID = value; }
		}

		protected string strOperator
		{
			get
			{
				strToFind = strToFind.Trim();

				if (strToFind.StartsWith("%") || strToFind.EndsWith("%"))
				{
					return " LIKE ";
				}

				return " >= ";
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT TOP 100 a.Nome AS fldName, a.PessoaID AS fldID, a.Fantasia AS Fantasia, " +
				"dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 111, 0) AS Documento, f.Localidade " + 
				"AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " +
				"FROM Pessoas a WITH(NOLOCK) " +
				"INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.ClassificacaoID = b.ItemID " +
				"LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " +
				"LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " +
				"LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " +
				"LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " +
				"WHERE a.EstadoID = 2 AND b.Filtro LIKE '%<FAB>%' " +
				"AND a.PessoaID NOT IN (SELECT FabricanteID FROM RelacoesPessoas_CodigosFabricante WITH(NOLOCK) WHERE RelacaoID = " + nRelacaoID + ") " +
				"AND a.Nome " + strOperator + " '" + strToFind + "' " +
				"ORDER BY fldName "
			));
		}
	}
}
