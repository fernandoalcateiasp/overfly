using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.relacoes.relpessoas.serverside
{
	public partial class inclcontatos : System.Web.UI.OverflyPage
	{
		private Integer relacaoID;
		protected Integer nRelacaoID
		{
			get { return relacaoID; }
			set { relacaoID = value; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.execNonQueryProcedure(
				"sp_RelPessoasContatos_Replica",
				new ProcedureParameters[] { new ProcedureParameters("@RelacaoID", SqlDbType.Int, relacaoID.intValue()) }
			);

			WriteResultXML(DataInterfaceObj.getRemoteData("select 'Mensagem gen�rica' as fldMsg"));
		}
	}
}
