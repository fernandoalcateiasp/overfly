using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.relacoes.relpessoas.serverside
{
	public partial class vincularelacao : System.Web.UI.OverflyPage
	{
		private Integer ParceiroID;
		protected Integer nParceiroID
		{
			get { return ParceiroID; }
			set { ParceiroID = value; }
		}

		private Integer EmpresaID;
		protected Integer nEmpresaID
		{
			get { return EmpresaID; }
			set { EmpresaID = value; }
		}

		private Integer UsuarioID;
		protected Integer nUsuarioID
		{
			get { return UsuarioID; }
			set { UsuarioID = value; }
		}

		private Integer TipoAtualizacao;
		protected Integer nTipoAtualizacao
		{
			get { return TipoAtualizacao; }
			set { TipoAtualizacao = value; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.execNonQueryProcedure("sp_RelPessoas_Validade",
				new ProcedureParameters[] {
					new ProcedureParameters("@ParceiroID", SqlDbType.Int, nParceiroID.intValue()),
					new ProcedureParameters("@EmpresaID", SqlDbType.Int, nEmpresaID.intValue()),
					new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUsuarioID.intValue()),
					new ProcedureParameters("@TipoAtualizacao", SqlDbType.Int, nTipoAtualizacao.intValue())
				}
			);

			WriteResultXML(DataInterfaceObj.getRemoteData("select 'Mensagem gen�rica' as fldMsg"));
		}
	}
}
