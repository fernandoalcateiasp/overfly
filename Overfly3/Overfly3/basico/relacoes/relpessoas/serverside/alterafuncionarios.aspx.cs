using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.basico.relacoes.relpessoas.serverside
{
	public partial class alterafuncionarios : System.Web.UI.OverflyPage
	{
		private Integer dataLen = Constants.INT_ZERO;
		public Integer nDataLen
		{
			get { return dataLen; }
			set { dataLen = value; }
		}

		private Integer[] relacaoID = Constants.INT_EMPTY_SET;
		public Integer[] RelacaoID
		{
			get { return relacaoID; }
			set { relacaoID = value; }
		}

		private Integer[] cargoID = Constants.INT_EMPTY_SET;
		public Integer[] CargoID
		{
			get { return cargoID; }
			set { cargoID = value; }
		}

		private Integer[] superiorDiretoID = Constants.INT_EMPTY_SET;
		public Integer[] SuperiorDiretoID
		{
			get { return superiorDiretoID; }
			set { superiorDiretoID = value; }
		}

		private Integer[] superiorID = Constants.INT_EMPTY_SET;
		public Integer[] SuperiorID
		{
			get { return superiorID; }
			set { superiorID = value; }
		}

		private Integer[] funcionarioDireto = Constants.INT_EMPTY_SET;
		public Integer[] FuncionarioDireto
		{
			get { return funcionarioDireto; }
			set { funcionarioDireto = value; }
		}

		private java.lang.Double[] salario = Constants.DBL_EMPTY_SET;
		public java.lang.Double[] Salario
		{
			get { return salario; }
			set { salario = value; }
		}

		private java.lang.Double[] salarioCarteira = Constants.DBL_EMPTY_SET;
		public java.lang.Double[] SalarioCarteira
		{
			get { return salarioCarteira; }
			set { salarioCarteira = value; }
		}

		private java.lang.Double[] pesoPremio = Constants.DBL_EMPTY_SET;
		public java.lang.Double[] PesoPremio
		{
			get { return pesoPremio; }
			set { pesoPremio = value; }
		}

		private string[] admissao = Constants.STR_EMPTY_SET;
		public string[] dtAdmissao
		{
			get { return admissao; }
			set { admissao = value; }
		}

		private string[] observacao = Constants.STR_EMPTY_SET;
		public string[] Observacao
		{
			get { return observacao; }
			set { observacao = value; }
		}

		protected string SQL
		{
			get
			{
				string sql = "";

				for (int i = 0; i < nDataLen.intValue(); i++)
				{
					sql += " UPDATE RelacoesPessoas SET " +
						"CargoID=" + CargoID[i].intValue() + ", " +
						"ProprietarioID=" + SuperiorDiretoID[i].intValue() + ", " +
						"AlternativoID=" + SuperiorID[i].intValue() + ", " +
						"FuncionarioDireto=" + FuncionarioDireto[i].intValue() + ", " +
						"Salario=" + Salario[i].doubleValue() + ", " +
						"SalarioCarteira=" + SalarioCarteira[i].doubleValue() + ", " +
						"PesoPremio=" + PesoPremio[i].doubleValue() + ", " +
						"dtAdmissao=" + dtAdmissao[i] + ", " +
						"Observacao=" + Observacao[i] + " " +
						"WHERE RelacaoID=" + RelacaoID[i].intValue();
				}

				return sql;
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			DataInterfaceObj.ExecuteSQLCommand(SQL);

			WriteResultXML(DataInterfaceObj.getRemoteData("select '' as Resultado"));
		}
	}
}
