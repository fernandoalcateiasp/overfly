using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.basico.relacoes.relpessoas.serverside
{
	public partial class sendmailxls : System.Web.UI.OverflyPage
	{
		private string E_ID;
		protected string sE_ID
		{
			get { return E_ID; }
			set { E_ID = value; }
		}

		private string EAlt_ID;
		protected string sEAlt_ID
		{
			get { return EAlt_ID; }
			set { EAlt_ID = value; }
		}

		private string E_Fant;
		protected string sE_Fant
		{
			get { return E_Fant; }
			set { E_Fant = value; }
		}

		private string E_sMail;
		protected string sE_sMail
		{
			get { return E_sMail; }
			set { E_sMail = value; }
		}

		private Integer E_CurrLang = Constants.INT_ZERO;
		protected Integer nE_CurrLang
		{
			get { return E_CurrLang; }
			set { E_CurrLang = (value != null) ? value : Constants.INT_ZERO; }
		}

		private Integer E_SysLang = Constants.INT_ZERO;
		protected Integer nE_SysLang
		{
			get { return E_SysLang; }
			set { E_SysLang = (value != null) ? value : Constants.INT_ZERO; }
		}

		private Integer DateFormat = Constants.INT_ZERO;
		protected Integer nDateFormat
		{
			get { return DateFormat; }
			set { DateFormat = (value != null) ? value : Constants.INT_ZERO; }
		}

		// Termos
		protected string term_001
		{
			get
			{
				return (nE_CurrLang.intValue() == 245) ?
					"Invalid filter." : "Filtro inv�lido.";
			}
		}

		// Formato de Data Para Excel
		protected string strDateFormat
		{
			get
			{
				return (nE_CurrLang.intValue() == 245) ?
					"mm/dd/yyyy" : "dd/mm/yyyy";
			}
		}

		private Integer EmpresaSelectedID = Constants.INT_ZERO;
		protected Integer nEmpresaSelectedID
		{
			get { return EmpresaSelectedID;  }
			set { EmpresaSelectedID = (value != null) ? value : Constants.INT_ZERO; }
		}

		private Integer ParceiroID = Constants.INT_ZERO;
		protected Integer nParceiroID
		{
			get { return ParceiroID; }
			set { ParceiroID = (value != null) ? value : Constants.INT_ZERO; }
		}

		private string UsuarioID = Constants.STR_EMPTY;
		protected string nUsuarioID
		{
			get { return UsuarioID; }
			set { UsuarioID = (value != null) ? value : Constants.STR_EMPTY; }
		}

		protected void Email_Cobranca()
		{
			OVERFLYSVRCFGLib.OverflyMTSClass dbd = new OVERFLYSVRCFGLib.OverflyMTSClass();

			DataInterfaceObj.execNonQueryProcedure("sp_Email_Cobranca",
				new ProcedureParameters[] {
					new ProcedureParameters("@EmpresaID", SqlDbType.Int, nEmpresaSelectedID.intValue()),
					new ProcedureParameters("@PessoaID", SqlDbType.Int, nParceiroID.intValue()),
					new ProcedureParameters("@UsuarioID", SqlDbType.Int, nUsuarioID),
					new ProcedureParameters("@Dominio", SqlDbType.VarChar, dbd.DatabaseDominium(DataInterfaceObj.ApplicationName), 300),
					new ProcedureParameters("@Aplicacao", SqlDbType.VarChar, DataInterfaceObj.ApplicationName, 30),
					new ProcedureParameters("@TipoEmail", SqlDbType.Int, 2)
				}
			);
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			Email_Cobranca();

			WriteResultXML(DataInterfaceObj.getRemoteData("SELECT 'e-Mail Enviado' AS Msg"));
		}
	}
}
