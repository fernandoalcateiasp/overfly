using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;
using Overfly3.systemEx.serverside;

namespace Overfly3.basico.relacoes.relpessoas.serverside
{
	public partial class verificacaocredito : System.Web.UI.OverflyPage
	{
		private Integer EmpresaID;
		protected Integer nEmpresaID
		{
			get { return EmpresaID; }
			set { EmpresaID = value; }
		}

		private Integer UserID;
		protected Integer nUserID
		{
			get { return UserID; }
			set { UserID = value; }
		}

		private java.lang.Double LimiteCredito;
		protected java.lang.Double nLimiteCredito
		{
			get { return LimiteCredito; }
			set { LimiteCredito = value; }
		}

		private Integer PessoaID;
		protected Integer nPessoaID
		{
			get { return PessoaID; }
			set { PessoaID = value; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			string sql = "SELECT dbo.fn_Direitos_Verifica(";
			double limite;

			sql += UserID + ", ";
			sql += EmpresaID + ", ";
			sql += PessoaID + ", 36, ";

			limite = LimiteCredito.doubleValue();
			sql += limite + ", GETDATE()) AS OK, ";

			sql += "CONVERT(VARCHAR(12), ISNULL(dbo.fn_Direitos_Valor(" + UserID + ", ";
			sql += EmpresaID + ", ";
			sql += PessoaID + ", 36, 0, GETDATE()), 0)) AS ValorMaximo ";

			WriteResultXML(DataInterfaceObj.getRemoteData(sql));
		}
	}
}
