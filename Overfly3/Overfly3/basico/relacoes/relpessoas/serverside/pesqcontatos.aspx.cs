using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.relacoes.relpessoas.serverside
{
	public partial class pesqcontatos : System.Web.UI.OverflyPage
	{
		private string toFind;
		protected string strToFind
		{
			get { return toFind; }
			set { toFind = value; }
		}

		private Integer relacaoID;
		protected Integer nRelacaoID
		{
			get { return relacaoID; }
			set { relacaoID = value; }
		}

		protected string strOperator
		{
			get
			{
				strToFind = strToFind.Trim();
		    
				if(strToFind.StartsWith("%") || strToFind.EndsWith("%"))
				{
					return " LIKE ";
				}

				return " >= ";
			}
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			WriteResultXML(DataInterfaceObj.getRemoteData(
				"SELECT TOP 100 a.Fantasia AS fldName,a.PessoaID AS fldID,a.Fantasia AS Fantasia, " +
				"dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 101, 101, 0) AS Documento ,e.Localidade AS Cidade, " +
				"f.CodigoLocalidade2 AS UF,g.CodigoLocalidade2 AS Pais " +
                "FROM Pessoas a WITH(NOLOCK)  " +
                "LEFT OUTER JOIN Pessoas_Enderecos d WITH(NOLOCK) ON (a.PessoaID=d.PessoaID AND (d.Ordem=1)) " +
                "LEFT OUTER JOIN Localidades e WITH(NOLOCK) ON (d.CidadeID=e.LocalidadeID) " +
                "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON (d.UFID=f.LocalidadeID) " +
                "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON (d.PaisID=g.LocalidadeID) " +
				"WHERE (a.TipoPessoaID = 51 AND a.EstadoID = 2 AND " +
				"a.PessoaID NOT IN (SELECT ContatoID FROM RelacoesPessoas_Contatos WITH(NOLOCK) WHERE (RelacaoID = " + nRelacaoID + ")) AND " +
				"a.Fantasia " + strOperator + "'" + strToFind + "' )" + 
				"ORDER BY fldName "
			));
		}
	}
}
