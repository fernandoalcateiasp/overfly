/********************************************************************
modalfabricante.js

Library javascript para o modalfabricante.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport("dsoPesq");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalfabricantebody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtFabricante').disabled == false )
        txtFabricante.focus();

    // Escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Fabricantes');

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Fabricante', 1);

    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divFabricante
    elem = window.document.getElementById('divFabricante');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width);
        height = 40;
        
    }
    
    // txtFabricante
    elem = window.document.getElementById('txtFabricante');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style)
    {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtFabricante.onkeypress = txtFabricante_onKeyPress;
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    elem.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtFabricante').style.top);
        left = parseInt(document.getElementById('txtFabricante').style.left) + parseInt(document.getElementById('txtFabricante').style.width) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divFabricante').style.top) + parseInt(document.getElementById('divFabricante').style.height) + ELEM_GAP;
        width = temp + 221;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);
    
    headerGrid(fg,['Fabricante',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s'], [2]);

    fg.Redraw = 2;
}

function txtFabricante_onKeyPress()
{
    if ( event.keyCode == 13 )
        btnFindPesquisa_onclick();
}

function txtFabricante_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    txtFabricante.value = trimStr(txtFabricante.value);
    
    changeBtnState(txtFabricante.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtFabricante.value);
}

function fg_DblClick()
{
    // Se tem Fabricante selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , new Array(
                      fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 1)));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strFabricante)
{
    lockControlsInModalWin(true);
    
    writeInStatusBar('child', 'Listando', 'cellMode' , true);
        
    var strPas = '?';
    strPas += 'strToFind='+escape(strFabricante);    
    strPas += '&nRelacaoID='+escape(glb_nRelacaoID);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/pesqfabricante.aspx'+strPas;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC()
{
    startGridInterface(fg);
    headerGrid(fg,['Fabricante',
                   'ID',
                   'Fantasia',
                   'Documento',
                   'Cidade',
                   'UF',
                   'Pa�s'], [2]);
    
    fillGridMask(fg,dsoPesq,['fldName',
                             'fldID',
                             'Fantasia',
                             'Documento',
                             'Cidade',
                             'UF',
                             'Pais'],['','','','','','','']);
                           
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }    
    else
        btnOK.disabled = true;    
        
    writeInStatusBar('child', 'cellMode', 'Fabricantes');    
}
