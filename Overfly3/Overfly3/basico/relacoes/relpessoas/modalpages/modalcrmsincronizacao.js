/********************************************************************
modalcrmsincronizacao.js

Library javascript para o modalcrmsincronizacao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_eventAfter = false;

// Gravacao de dados do grid .RDS

//Eventos de envio ao BD
var dsoGrid = new CDatatransport("dsoGrid");
var dsoGridDetalhes = new CDatatransport("dsoGridDetalhes");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;

    // ajusta o body do html
    with (modalcrmsincronizacaoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // configuracao inicial do html
    setupPage();   
        
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	// coloca foco no campo Pesquisa
    txtInicio.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('CRM Sincroniza��o', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 25;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    adjustElementsInForm([['lblInicio', 'txtInicio', 15, 1, -5],
                          ['lblFim', 'txtFim', 15, 1],
                          ['lblErro', 'chkErro', 3, 1],
                          ['lblEvento', 'selEvento', 12, 1, -5],
                          ['lblSistema', 'selSistema', 12, 1],
                          ['lblID', 'txtID', 15, 1],
                          ['lblCampo', 'selCampo', 16, 1],
                          ['btnOK', 'btn', btnOK.offsetWidth, 1, 125, (lblInicio.offsetHeight + 10)]], null, null, true);

    // ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10);
        width = modWidth - ELEM_GAP;
        height = parseInt(divMod01.currentStyle.height, 10) + txtInicio.offsetTop + txtInicio.offsetHeight;
    }

    // ajusta as Divs de Grid
    with (divGrids.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFields.currentStyle.top, 10) +
			parseInt(divFields.currentStyle.height, 10) - 10;
        width = modWidth - 25;
        height = modHeight - divFields.offsetHeight - 2 * ELEM_GAP;
    }

    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 5;
        top = 0;
        width = parseInt(divGrids.currentStyle.width) - ELEM_GAP;
        height = (parseInt(divGrids.currentStyle.height, 10) / 2) - ELEM_GAP;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;


    //Div Detalhes
    with (divFGDetalhes.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 5;
        top = parseInt(divFG.currentStyle.top) + parseInt(divFG.currentStyle.height) + ELEM_GAP;
        width = parseInt(divGrids.currentStyle.width) - ELEM_GAP;
        height = (parseInt(divGrids.currentStyle.height, 10) / 2) - ELEM_GAP;
    }

    with (fgDetalhes.style) {
        left = 0;
        top = 0;
        width = parseInt(divFGDetalhes.style.width, 10);
        height = parseInt(divFGDetalhes.style.height, 10);
    }

    startGridInterface(fgDetalhes);
    fgDetalhes.Cols = 0;
    fgDetalhes.Cols = 1;
    fgDetalhes.ColWidth(0) = parseInt(divFGDetalhes.currentStyle.width, 10) * 18;
    fgDetalhes.Redraw = 2;

    // Configura bot�es OK e Cancelar
    btnOK.disabled = false;
    btnOK.innerText = 'Listar';
    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';

    // Configura demais componentes da modal
    chkErro.checked = true;
    txtInicio.onkeypress = onkeypress_enter;
    txtInicio.maxLength = 16;
    txtFim.onkeypress = onkeypress_enter;
    txtFim.maxLength = 16;
    txtID.onkeypress = onkeypress_enter;

    //var data = new Date();
    //txtInicio.value = data.getDate().toString() + '/' + (data.getMonth() + 1).toString() + '/' + data.getFullYear().toString();

    txtInicio.value = getCurrDate(null, -1);
    txtFim.value = getCurrDate();
}


/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    if (ctl.id == btnOK.id )
    {
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);

}

/********************************************************************
Usuario clicou btnOK
********************************************************************/
function btnOK_Clicked() 
{
    if (txtInicio.value == '' || txtFim.value == '')
        window.top.overflyGen.Alert('Informe o per�odo desejado.');
    else
        listar();
}

/*****************************************************
Preenchimento do Grid de registros da sincroniza��o
******************************************************/
function listar()
{
    //lockControlsInModalWin(true);

    setConnection(dsoGrid);

    var sSQL = "SELECT DISTINCT a.SincronizacaoID, a.dtData, a.dtSincronizacao, a.Erro, a.ResultadoSincronizacao, /*c.ItemAbreviado AS Evento,*/ " +
                       "/*d.ItemMasculino AS Sistema,*/ a.PessoaID, a.CodigoCRM, a.CadOK, l.RecursoAbreviado AS Estado, e.ItemAbreviado AS TipoPessoa, " +
                       "a.Fantasia, a.Nome, f.Fantasia AS Proprietario, g.ItemMasculino AS Classificacao, h.Fantasia AS Empresa, " +
                       "a.DocumentoNumero, a.TelefoneDDD, a.TelefoneNumero, a.CelularDDD, a.CelularNumero, a.Site, a.Email, " +
                       "i.Localidade AS Pais, a.CEP, j.CodigoLocalidade2 AS UF, k.Localidade AS Cidade, a.Bairro, a.Endereco, " +
                       "a.Numero, a.Complemento " +
                   "FROM CRMSincronizacao a WITH(NOLOCK) " +
                       "INNER JOIN CRMSincronizacao_Detalhes b WITH(NOLOCK) ON (b.SincronizacaoID = a.SincronizacaoID) " +
                       "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = a.EventoID) " +
                       "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (d.ItemID = a.SistemaID) " +
                       "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = a.TipoPessoaID) " +
                       "INNER JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID = a.ProprietarioID) " +
                       "INNER JOIN TiposAuxiliares_Itens g WITH(NOLOCK) ON (g.ItemID = a.ClassificacaoID) " +
                       "LEFT JOIN Pessoas h WITH(NOLOCK) ON (h.PessoaID = a.EmpresaID) " +
                       "INNER JOIN Localidades i WITH(NOLOCK) ON (i.LocalidadeID = a.PaisID) " +
                       "INNER JOIN Localidades j WITH(NOLOCK) ON (j.LocalidadeID = a.UFID) " +
                       "INNER JOIN Localidades k WITH(NOLOCK) ON (k.LocalidadeID = a.CidadeID) " +
                       "INNER JOIN Recursos l WITH(NOLOCK) ON (l.RecursoID = a.EstadoID) " +
                   "WHERE (1=1) AND a.dtData BETWEEN '" + normalizeDate_DateTime(txtInicio.value, true) + "' AND '" + normalizeDate_DateTime(txtFim.value, true) + "' ";
    
    if (chkErro.checked)
        sSQL += "AND (a.Erro = 1) ";

    if (selEvento.value > 0)
        sSQL += "AND (b.EventoID = " + selEvento.value + ") ";

    if (selSistema.value > 0)
        sSQL += "AND (b.SistemaID = " + selSistema.value + ") ";

    if (txtID.value != "")
        sSQL += "AND ((a.PessoaID = " + txtID.value + ") OR (a.CodigoCRM = " + txtID.value + ") OR (a.EmpresaID = " + txtID.value + ")) ";

    if (selCampo.value > 0)
        sSQL += "AND (b.Campo = '" + selCampo.options(selCampo.selectedIndex).text + "') ";

    dsoGrid.SQL = sSQL;
    dsoGrid.ondatasetcomplete = listar_DSC;

    try {
        dsoGrid.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

function listar_DSC()
{
    glb_GridIsBuilding = true;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var dso = dsoGrid;
    var grid = fg;

    // Move o cursor do dso para o primeiro registro
    if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)) {
        dsoGrid.recordset.MoveFirst();
    }

    grid.ExplorerBar = 5;
    startGridInterface(grid);
    grid.FontSize = '8';
    grid.Editable = false;
    grid.BorderStyle = 1;

    headerGrid(grid, ['ID',
                      'Data',
                      'Sincroniza��o',
                      'Erro',
                      'Resultado',
                      //'Evento',
                      //'Sistema',
                      'PessoaID',
                      'CodigoCRM',
                      'CadOK',
                      'Estado',
                      'Tipo Pessoa',
                      'Fantasia',
                      'Nome',
                      'Propriet�rio',
                      'Classifica��o',
                      'Empresa',
                      'Documento Federal',
                      'Telefone DDD',
                      'Telefone Numero',
                      'Celular DDD',
                      'Celular Numero',
                      'Site',
                      'Email',
                      'Pa�s',
                      'CEP',
                      'UF',
                      'Cidade',
                      'Bairro',
                      'Endere�o',
                      'N�mero',
                      'Complemento'], []);

    fillGridMask(grid, dsoGrid, ['SincronizacaoID',
                                 'dtData',
                                 'dtSincronizacao',
                                 'Erro',
                                 'ResultadoSincronizacao',
                                 //'Evento',
                                 //'Sistema',
                                 'PessoaID',
                                 'CodigoCRM',
                                 'CadOK',
                                 'Estado',
                                 'TipoPessoa',
                                 'Fantasia',
                                 'Nome',
                                 'Proprietario',
                                 'Classificacao',
                                 'Empresa',
                                 'DocumentoNumero',
                                 'TelefoneDDD',
                                 'TelefoneNumero',
                                 'CelularDDD',
                                 'CelularNumero',
                                 'Site',
                                 'Email',
                                 'Pais',
                                 'CEP',
                                 'UF',
                                 'Cidade',
                                 'Bairro',
                                 'Endereco',
                                 'Numero',
                                 'Complemento'],
                                 ['', '99/99/9999 99:99', '99/99/9999 99:99', '', '', /*'', '',*/ '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm', dTFormat + ' hh:mm', '', '', /*'', '',*/ '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);

    //alignColsInGrid(grid, [2, 3, 4, 5, 6, 7, 9, 12, 14, 18]);
    gridHasTotalLine(grid, 'Totais', 0xC0C0C0, null, true, [[0, '######', 'C']]);

    if (fg.Rows > 1) {
        var i, nRegistros = 0;

        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'SincronizacaoID')) != 0) {
                nRegistros += 1;
            }
        }

        if (nRegistros > 0)
            fg.TextMatrix(1, 0) = nRegistros;
    }

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);
    
    fg.ColWidth(getColIndexByColKey(fg, 'ResultadoSincronizacao')) = 3000;

    grid.Redraw = 2;
    
    if (grid.Rows > 1){
        grid.Row = 2;
        listarDetalhes(fg.TextMatrix(grid.Row, getColIndexByColKey(fg, 'SincronizacaoID')));
    }
    else
        lockControlsInModalWin(false);

    glb_GridIsBuilding = false;
}

/*****************************************************
Preenchimento do Grid de detalhes dos registros da sincroniza��o
******************************************************/
function listarDetalhes(nSincronizacaoID) {
    setConnection(dsoGridDetalhes);

    var sSQL = "SELECT SinDetalheID, b.ItemAbreviado AS Evento, c.ItemMasculino AS Sistema, Campo, Valor, Erro, ResultadoSincronizacao " +
                    "FROM CRMSincronizacao_Detalhes a WITH(NOLOCK) " +
                        "INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.EventoID) " +
                        "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = a.SistemaID) " +
                   "WHERE (a.SincronizacaoID = " + nSincronizacaoID + ") ";

    /*if (chkErro.checked)
        sSQL += "AND (a.Erro = 1) ";*/

    if (selEvento.value > 0)
        sSQL += "AND (a.EventoID = " + selEvento.value + ") ";

    if (selSistema.value > 0)
        sSQL += "AND (a.SistemaID = " + selSistema.value + ") ";

    if (selCampo.value > 0)
        sSQL += "AND (a.Campo = '" + selCampo.options(selCampo.selectedIndex).text + "') ";

    dsoGridDetalhes.SQL = sSQL;
    dsoGridDetalhes.ondatasetcomplete = listarDetalhes_DSC;

    try {
        dsoGridDetalhes.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
            return null;

        lockControlsInModalWin(false);
        window.focus();
    }
}

function listarDetalhes_DSC() {
    glb_GridIsBuilding = true;

    var dso = dsoGridDetalhes;
    var grid = fgDetalhes;

    // Move o cursor do dso para o primeiro registro
    if (!(dso.recordset.BOF && dso.recordset.EOF)) {
        dso.recordset.MoveFirst();
    }

    //grid.ExplorerBar = 5;
    startGridInterface(grid);
    grid.FontSize = '8';
    grid.Editable = false;
    grid.BorderStyle = 1;

    headerGrid(grid, ['Evento',
                      'Sistema',
                      'Campo',
                      'Valor Anterior',
                      'Erro',
                      'Resultado',
                      'SinDetalheID'], [6]);

    fillGridMask(grid, dso, ['Evento',
                             'Sistema',
                             'Campo',
                             'Valor',
                             'Erro',
                             'ResultadoSincronizacao',
                             'SinDetalheID'],
                             ['', '', '', '', '', '', ''],
                             ['', '', '', '', '', '', '']);

    grid.AutoSizeMode = 0;
    grid.AutoSize(0, grid.Cols - 1);

    fg.ColWidth(getColIndexByColKey(fg, 'ResultadoSincronizacao')) = 3000;

    grid.Redraw = 2;

    if (grid.Rows > 1)
        grid.Row = 1;

    lockControlsInModalWin(false);
    glb_GridIsBuilding = false;
}

/********************************************************************
Busca data atual no formato brasileiro
********************************************************************/
function getCurrDate(hour, day) {
    var d, s = "";
    d = new Date();

    hour = ((hour != null) ? (d.getHours() + (hour)) : d.getHours());
    day = ((day != null) ? (d.getDate() + (day)) : d.getDate());


    if (DATE_SQL_PARAM == 103) {
        s += padL(day.toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(day.toString(), 2, '0') + "/";
    }

    s += d.getYear() + " ";
    s += padL(hour.toString(), 2, '0') + ":";

    s += padL(d.getMinutes().toString(), 2, '0');

    return (s);
}

/********************************************************************
Lista registros quando usuario pressiona Enter
********************************************************************/
function onkeypress_enter() {
    if (event.keyCode == 13) {
        listar();
    }
    else
        return verifyDateTimeNotLinked;
}

/********************************************************************
Faz listagem de detalhes do registro quando muda de linha
********************************************************************/
function js_ModalCrmSincronizacao_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    if (!glb_GridIsBuilding)
        if ((NewRow != 1) && (!glb_eventAfter))
            listarDetalhes(fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'SincronizacaoID')));

    glb_eventAfter = false;
}

/********************************************************************
Impede de mudar sele��o para linha de totais
********************************************************************/
function js_ModalCrmSincronizacao_AfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol) {
    if (!glb_GridIsBuilding)
        if ((grid.Rows > 1) && (NewRow == 1)) {
            glb_eventAfter = true;
            grid.Row = OldRow;
        }
}