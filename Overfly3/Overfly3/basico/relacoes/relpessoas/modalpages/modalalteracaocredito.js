/********************************************************************
modalalteracaocredito.js

Library javascript para o modalalteracaocredito.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;

// Gravacao de dados do grid .RDS
var dsoGravacao = new CDatatransport("dsoGravacao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
sels_onchange()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	txtLimiteCredito.readOnly = true;
		 
    window_onload_1stPart();

	fillControlsData();
	
	// configuracao inicial do html
    setupPage();   
	
	getDataServer();
}

function getDataServer()
{
	var nLimiteCredito = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['LimiteCredito'].value");
	
	if (isNaN(nLimiteCredito))
		nLimiteCredito = 0;
	
	setConnection(dsoGravacao);
	dsoGravacao.SQL = 'SELECT b.RelacaoID, b.EmpresaColigada, b.ConceitoID, FinanciamentoPadraoID, FormaPagamentoID, NivelPagamento, UsuarioID ' +
        'FROM RelacoesPessoas b WITH(NOLOCK) ' +
		'WHERE RelacaoID = ' + glb_nRelacaoID;

	dsoGravacao.ondatasetcomplete = getDataServer_DSC;
	dsoGravacao.Refresh();
}

function getDataServer_DSC()
{	
    // ajusta o body do html
    with (modalalteracaocreditoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    updateHTMLFields();

    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Altera��o de Cr�dito', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
        
    // reajusta dimensoes e reposiciona a janela
	redimAndReposicionModalWin(355, 180, false);
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    adjustElementsInForm([['lblConceitoID','selConceitoID',10,1,-10,-10],
						  ['lblLimiteCredito','txtLimiteCredito',11,1,-8],
						  ['lblFinanciamentoPadraoID','selFinanciamentoPadraoID',14,2,-10],
						  ['lblFormaPagamentoID','selFormaPagamentoID',7,2],
						  ['lblNivelPagamento','selNivelPagamento',5,2],
						  ['lblEmpresaColigada','chkEmpresaColigada',3,2]], null, null, true);
	
	selConceitoID.onchange = sels_onchange;
	
	// ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = selNivelPagamento.offsetTop + selNivelPagamento.offsetHeight + 2;
    }
}

/********************************************************************
Usuario teclou Enter em campo texto
********************************************************************/
function txt_onkeydown()
{
	if ( event.keyCode == 13 )	
		;
}

/********************************************************************
Usuario clicou em checkbox
********************************************************************/
function chks_onclick()
{
	;
}

/********************************************************************
Usuario alterou selecao de combo
********************************************************************/
function sels_onchange()
{
	adjustLabelCombos(this);

	if (this == selConceitoID)
	{
		adjustBtnsStatus();
	}
}

function adjustLabelCombos(elem)
{
	;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		lockControlsInModalWin(true);
		updateRecordFromHTMLFields();
		
		try
		{			
			if (dsoGravacao.recordset['FinanciamentoPadraoID'].value == 0)
				dsoGravacao.recordset['FinanciamentoPadraoID'].value = null;
				
			if (dsoGravacao.recordset['FormaPagamentoID'].value == 0)
				dsoGravacao.recordset['FormaPagamentoID'].value = null;

			dsoGravacao.recordset['UsuarioID'].value = glb_nUsuarioID;
			
			dsoGravacao.ondatasetcomplete = gravacao_DSC;
		    dsoGravacao.SubmitChanges();   
		}
		catch(e)
		{
		    // Numero de erro qdo o registro foi alterado ou removido por outro usuario
		    // ou operacao de gravacao abortada no banco
		    if (e.number == -2147217887)
		        if ( window.top.overflyGen.Alert('Erro ao gravar.') == 0 )
		            return null;

			// Simula o botao OK para fechar a janela e forcar um refresh no sup
			sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
			return null;
		}
    }
    else if (controlID == 'btnCanc')
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null); 
    }
}

function gravacao_DSC()
{
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Preenche os campos de dado
********************************************************************/
function fillControlsData()
{
    lockControlsInModalWin(true);
    
	var i=0;
	var aCmbConceito = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbConceitoData()');
	var aCmbFormaPagamento = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbFormaPagamento()');
	var aCmbNivelPagamento = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbNivelPagamento()');
    var oOption;

	for (i=0; i<aCmbConceito.length; i++)
	{
        oOption = document.createElement("OPTION");
        oOption.text = aCmbConceito[i][0];
        oOption.value = aCmbConceito[i][1];
        selConceitoID.add(oOption);
	}

	txtLimiteCredito.value = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'txtLimiteCredito.value');
		
	var nB5C2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrRightValue(\'SUP\',\'B5C2\')');

	lockControlsInModalWin(false);
	
	if (glb_nFaturamentoDiretoColigada==0)
	{	
	    selConceitoID.disabled = ((glb_nNivelPagamentoMaximo != 4) || (selConceitoID.options.length == 0));
	    selFinanciamentoPadraoID.disabled = (selFinanciamentoPadraoID.options.length == 0);
	    selFormaPagamentoID.disabled = (selFormaPagamentoID.options.length == 0);
	    selNivelPagamento.disabled = (selNivelPagamento.options.length == 0);
	    chkEmpresaColigada.disabled = ((glb_nNivelPagamentoMaximo != 4) && (nB5C2 == 0));
    }	        
    else
    {
        selConceitoID.disabled = true;
	    selFinanciamentoPadraoID.disabled = true;
	    selFormaPagamentoID.disabled = true;
	    selNivelPagamento.disabled = true;
	    chkEmpresaColigada.disabled = ((glb_nNivelPagamentoMaximo != 4) && (nB5C2 == 0));
    }
}

function adjustBtnsStatus()
{
	;
}
