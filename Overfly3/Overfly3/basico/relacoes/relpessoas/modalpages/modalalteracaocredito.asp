
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Function ReplaceInText(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceInText = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function
%>

<html id="modalalteracaocreditoHtml" name="modalalteracaocreditoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/modalpages/modalalteracaocredito.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/modalpages/modalalteracaocredito.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nUsuarioID, nRelacaoID, nEmpresaID, nPessoaID, nFaturamentoDiretoColigada 

sCaller = ""
nUsuarioID = 0
nRelacaoID = 0
nEmpresaID = 0
nPessoaID = 0
nFaturamentoDiretoColigada = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

For i = 1 To Request.QueryString("nRelacaoID").Count    
    nRelacaoID = Request.QueryString("nRelacaoID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nPessoaID").Count    
    nPessoaID = Request.QueryString("nPessoaID")(i)
Next

For i = 1 To Request.QueryString("nFaturamentoDiretoColigada").Count    
    nFaturamentoDiretoColigada = Request.QueryString("nFaturamentoDiretoColigada")(i)
Next


Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nUsuarioID = " & CStr(nUsuarioID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nRelacaoID = " & CStr(nRelacaoID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPessoaID = " & CStr(nPessoaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nFaturamentoDiretoColigada = " & CStr(nFaturamentoDiretoColigada) & ";"
Response.Write vbcrlf

Dim rsData, rsData2, rsData3, strSQL
Set rsData = Server.CreateObject("ADODB.Recordset")
Set rsData2 = Server.CreateObject("ADODB.Recordset")
Set rsData3 = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT TOP 1 ISNULL(a.NivelPagamento,1) AS NivelPagamento, " & _
	"ISNULL(dbo.fn_Direitos_Valor(" & CStr(nUsuarioID) & ", " & CStr(nEmpresaID) & ", NULL, 47, 0, GETDATE()),1) AS NivelPagamentoMaximo " & _
	"FROM RelacoesPessoas a WITH(NOLOCK) " & _
	"WHERE (a.RelacaoID = " & CStr(nRelacaoID) & ")" 

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write "var glb_nNivelPagamentoMaximo = " & CStr(rsData.Fields("NivelPagamentoMaximo").Value) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<body id="modalalteracaocreditoBody" name="modalalteracaocreditoBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <!-- Div dos campos -->
    <div id="divFields" name="divFields" class="divGeneral">
		<p id="lblConceitoID" name="lblConceitoID" class="lblGeneral">Conceito</p>
        <select id="selConceitoID" name="selConceitoID" class="fldGeneral" DATASRC="#dsoGravacao" DATAFLD="ConceitoID"></select>

		<p id="lblLimiteCredito" name="lblLimiteCredito" class="lblGeneral">Limite de Cr�dito</p>
        <input type="text" id="txtLimiteCredito" name="txtLimiteCredito" class="fldGeneral" DATASRC="#dsoGravacao" DATAFLD="LimiteCredito">
        
        <p id="lblFinanciamentoPadraoID" name="lblFinanciamentoPadraoID" class="lblGeneral">Financ Padr�o</p>
        <select id="selFinanciamentoPadraoID" name="selFinanciamentoPadraoID" class="fldGeneral" DATASRC="#dsoGravacao" DATAFLD="FinanciamentoPadraoID">
<%
			strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem " & _
						  "UNION ALL SELECT b.FinanciamentoID AS fldID,b.Financiamento AS fldName, b.Ordem AS Ordem " & _
                          "FROM FinanciamentosPadrao a WITH(NOLOCK), " &_
                                "FinanciamentosPadrao b WITH(NOLOCK) ON (b.NumeroParcelas <= a.NumeroParcelas) " &_
                                "Pessoas_Creditos c WITH(NOLOCK) ON (a.FinanciamentoID = c.FinanciamentoLimiteID) " & _
                          "WHERE b.EstadoID = 2 AND  " & _
                          "AND ((dbo.fn_Financiamento_PMP(b.FinanciamentoID,NULL,NULL,NULL))<= (dbo.fn_Financiamento_PMP(a.FinanciamentoID,NULL,NULL,NULL))) " & _
                          "AND b.FinanciamentoID<>1 AND c.PessoaID=" & CStr(nPessoaID) & "  " & _
                          "ORDER BY Ordem"

			rsData2.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
			
			If (Not (rsData2.BOF AND rsData2.EOF) ) Then

				While Not (rsData2.EOF)

					Response.Write( "<option value='" & rsData2.Fields("fldID").Value & "'")
					Response.Write( ">" & rsData2.Fields("fldName").Value & "</option>" )
					rsData2.MoveNext()

				WEnd

			End If

			rsData2.Close
			Set rsData2 = Nothing
			
%>        
        </select>

        <p id="lblFormaPagamentoID" name="lblFormaPagamentoID" class="lblGeneral">Forma</p>
        <select id="selFormaPagamentoID" name="selFormaPagamentoID" class="fldGeneral" DATASRC="#dsoGravacao" DATAFLD="FormaPagamentoID">
<%
    Set rsData3 = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem " & _
			 "UNION ALL Select DISTINCT a.ItemID as fldID, a.ItemAbreviado as fldName , LTRIM(RTRIM(STR(a.ordem))) AS Ordem " & _
             "From TiposAuxiliares_Itens a WITH(NOLOCK) " & _
             "Where (a.TipoID = 804 AND a.EstadoID = 2 AND a.Filtro LIKE '%(1221)%') " & _
             "Order By Ordem "

    rsData3.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
	While (Not rsData3.EOF)
	    Response.Write "<option value =" & Chr(34) & rsData3.Fields("fldID").Value & Chr(34) & " "

		If (CInt(rsData3.Fields("fldID").Value) = 1033) Then
			Response.Write " selected "
		End If

		Response.Write ">" & rsData3.Fields("fldName").Value & "</option>" & vbCrlf
	    rsData3.MoveNext
	Wend
	
	rsData3.Close
	Set rsData3 = Nothing
	
%>
        </select>

        <p id="lblNivelPagamento" name="lblNivelPagamento" class="lblGeneral">N�vel</p>
        <select id="selNivelPagamento" name="selNivelPagamento" class="fldGeneral" DATASRC="#dsoGravacao" DATAFLD="NivelPagamento">
		<%
			If ( Not(rsData.BOF AND rsData.EOF)) Then
			
			
				For i = 1 To CInt(rsData.Fields("NivelPagamentoMaximo").Value)
					Response.Write( "<option value='" & CStr(i) & "'" )
					Response.Write( ">" & CStr(i) & "</option>" )
				Next
				
				If (CInt(rsData.Fields("NivelPagamentoMaximo").Value) < CInt(rsData.Fields("NivelPagamento").Value)) Then
					Response.Write( "<option value='" & CStr(rsData.Fields("NivelPagamento").Value) & "'" )
					Response.Write( ">" & CStr(rsData.Fields("NivelPagamento").Value) & "</option>" )
				End If

			End If

			rsData.Close
			Set rsData = Nothing
		%>
		</select>
		
        <p id="lblEmpresaColigada" name="lblEmpresaColigada" class="lblGeneral" title="O cliente � empresa coligada do fornecedor?">EC</p>
        <input type="checkbox" id="chkEmpresaColigada" name="chkEmpresaColigada" class="fldGeneral" title="O cliente � empresa coligada do fornecedor?" DATASRC="#dsoGravacao" DATAFLD="EmpresaColigada"></input>
		
    </div>
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
</body>

</html>
