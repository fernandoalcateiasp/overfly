/********************************************************************
modalemailcontato.js

Library javascript para o modalemailcontato.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nUserID = 0;

// Gravacao de dados do grid .RDS
var dsoGravacao = new CDatatransport("dsoGravacao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
btn_onclick(ctl)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

	// configuracao inicial do html
    setupPage();   
	
	getDataServer();
}

function getDataServer()
{
    setConnection(dsoGravacao);
    
    var nMailIndex = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'getColIndexByColKey(fg, ' + '\'' + 'EMail*' + '\'' + ')');
    
    if (nMailIndex == -1)
        nMailIndex = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'getColIndexByColKey(fg, ' + '\'' + 'EMail' + '\'' + ')');
    
    var bEhContato = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'getCellValueByColKey(fg,' + '\'' + 'EhContato*' + '\'' + ', fg.Row)');
    var sEmailAnterior = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'fg.TextMatrix(fg.Row,' + nMailIndex + ')');
    
    txtEMail1.value = sEmailAnterior;
    txtEMail2.value = sEmailAnterior;
    
    if (bEhContato == 1)
    {
        var nContatoID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'fg.TextMatrix(fg.Row, getColIndexByColKey(fg, ' + '\'' + 'ContatoID' + '\'' + '))');

	    dsoGravacao.SQL = 'SELECT PesURLID, PessoaID, TipoURLID, Ordem, URL ' +
		    'FROM Pessoas_URLs WITH(NOLOCK) ' +
		    'WHERE PessoaID = ' + nContatoID + ' AND URL=' + '\'' + sEmailAnterior + '\'';
    }
    else
    {
        var nProspectID = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'fg.TextMatrix(fg.Row, getColIndexByColKey(fg, ' + '\'' + 'ProspectID' + '\'' + '))');
	    dsoGravacao.SQL = 'SELECT ProspectID, EMail ' +
		    'FROM Prospect WITH(NOLOCK) ' +
		    'WHERE ProspectID = ' + nProspectID;
    }


	dsoGravacao.ondatasetcomplete = getDataServer_DSC;
	dsoGravacao.Refresh();
}

function getDataServer_DSC()
{	
    // ajusta o body do html
    with (modalemailcontatoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Altera��o de e-mail', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
        
    // reajusta dimensoes e reposiciona a janela
	//redimAndReposicionModalWin(295, 180, false);
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    adjustElementsInForm([['lblEMail1','txtEMail1',42,1,-10,-10],
						  ['lblEMail2','txtEMail2',42,2,-10]], null, null, true);

    txtEMail1.onpaste = mail_onpaste;
    txtEMail2.onpaste = mail_onpaste;
    
	// ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = txtEMail2.offsetTop + txtEMail2.offsetHeight + 2;
    }
}

function mail_onpaste()
{
    event.returnValue = false;
}

/********************************************************************
Usuario teclou Enter em campo texto
********************************************************************/
function txt_onkeydown()
{
	if ( event.keyCode == 13 )	
		;
}

/********************************************************************
Usuario clicou em checkbox
********************************************************************/
function chks_onclick()
{
	;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        if (trimStr(txtEMail1.value) == '')
        {
	        if ( window.top.overflyGen.Alert('E-mail inv�lido.') == 0 )
	            return null;
	            
	        return null;
        }

        if (txtEMail1.value != txtEMail2.value)
        {
	        if ( window.top.overflyGen.Alert('Os dois campos de e-mail devem ter o mesmo conte�do.') == 0 )
	            return null;
	            
	        return null;
        }

		lockControlsInModalWin(true);
		try
		{			
            var bEhContato = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'getCellValueByColKey(fg,' + '\'' + 'EhContato*' + '\'' + ', fg.Row)');

            if (bEhContato == 1)
            {
                if ((dsoGravacao.recordset.BOF) && (dsoGravacao.recordset.EOF))
                {
                    var nContatoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ContatoID'));
                    dsoSup01.recordset.AddNew();
                    
                    dsoGravacao.recordset['PessoaID'].value = nContatoID;
                    dsoGravacao.recordset['TipoURLID'].value = 124;
                    dsoGravacao.recordset['Ordem'].value = 1;
                    dsoGravacao.recordset['URL'].value = txtEMail1.value;
                }
                else
                    dsoGravacao.recordset['URL'].value = txtEMail1.value;
            }
            else
                dsoGravacao.recordset['EMail'].value = txtEMail1.value;

            dsoGravacao.ondatasetcomplete = gravacao_DSC;
            dsoGravacao.SubmitChanges();
			//dsoGravacao.Refresh();
		}
		catch(e)
		{
		    // Numero de erro qdo o registro foi alterado ou removido por outro usuario
		    // ou operacao de gravacao abortada no banco
		    if (e.number == -2147217887)
		        if ( window.top.overflyGen.Alert('Erro ao gravar.') == 0 )
		            return null;

			// Simula o botao OK para fechar a janela e forcar um refresh no sup
			sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
			return null;
		}
    }
    else if (controlID == 'btnCanc')
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null); 
    }
}

function gravacao_DSC()
{
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
}
