/********************************************************************
modalconvidarusuario.js

Library javascript para o modalconvidarusuario.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Gravacao de dados do grid .RDS
var dsoCmbPerfil = new CDatatransport("dsoCmbPerfil");
var dsoGravacao = new CDatatransport("dsoGravacao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalconvidarusuarioBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // configuracao inicial do html
    setupPage();   
        
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	// coloca foco no campo Pesquisa
    txtEmail.focus();
    alimentaPerfil();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    secText('Convidar Usu�rio', 1);

    // ajusta elementos da janela
    var elem, elem1;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;

    // reajusta dimensoes e reposiciona a janela
    //redimAndReposicionModalWin(295, 180, false);

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    adjustElementsInForm([['lblEmail', 'txtEMail', 80, 1],
                          ['lblNome', 'txtNome', 40, 2],
						  ['lblPerfil', 'selPerfilID', 30, 2]], null, null, true);

    txtEMail.onpaste = mail_onpaste;

    // ajusta o divFields
    elem = divFields;
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP + 10;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = txtNome.offsetTop + txtEmail.offsetHeight + selPerfilID.offsetHeight + 2;
    }
    
}

function mail_onpaste() {
    event.returnValue = false;
}

function alimentaPerfil() 
{
    lockControlsInModalWin(true);
    setConnection(dsoCmbPerfil);
    dsoCmbPerfil.SQL = 'SELECT ItemID, ItemMasculino FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID = 27';
    dsoCmbPerfil.ondatasetcomplete = alimentaPerfil_DSC;
    dsoCmbPerfil.Refresh();

}

function alimentaPerfil_DSC() 
{
    clearComboEx(['selPerfilID']);

    if (!(dsoCmbPerfil.recordset.BOF || dsoCmbPerfil.recordset.EOF)) {
        dsoCmbPerfil.recordset.moveFirst;

        while (!dsoCmbPerfil.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCmbPerfil.recordset['ItemMasculino'].value; ;
            oOption.value = dsoCmbPerfil.recordset['ItemID'].value;
            selPerfilID.add(oOption);
            dsoCmbPerfil.recordset.MoveNext();
        }
    }

    lockControlsInModalWin(false);

    selPerfilID.disabled = ((selPerfilID.length > 1) ? false : true);
}

function gravaConvite() {

    var strPars = new String();
    //var sujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value");
    var objetoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");
    
    lockControlsInModalWin(true);
    
    setConnection(dsoGravacao);
    
    strPars = '?sNome=' + escape(txtNome.value);
    strPars += '&sEmail=' + escape(txtEmail.value);
    strPars += '&nPerfilID=' + escape(selPerfilID.value);
    strPars += '&sRelacaoIDs=' + escape(glb_nSujeitoID) + ';';
    strPars += '&sCnpj=' + escape(glb_sCnpj);
    strPars += '&sEstadoDe=IN_06';

    dsoGravacao.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/convidarusuario.aspx' + strPars;
    
    dsoGravacao.ondatasetcomplete = gravaConvite_DSC;
    dsoGravacao.Refresh();
}

function gravaConvite_DSC() {

    lockControlsInModalWin(false);
    window.top.overflyGen.Alert('Convite realizado com sucesso!!!!');
    
    
}


/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        if (glb_sCnpj != 0)
            btnOK_Clicked();
        else 
        {
            window.top.overflyGen.Alert('Erro ao adicionar contato para essa rela��o. Sujeito n�o tem CNPJ cadastrado.');
            lockControlsInModalWin(false);
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);
        }
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);

    

}

/********************************************************************
Pressiona botao OK se pressionado Enter neste campo
********************************************************************/
function selPerfilID_onkeypress()
{
    if (event.keyCode == 13)
       btnOK_Clicked();
}

/********************************************************************
Usuario clicou btnOK
********************************************************************/
function btnOK_Clicked() 
{
    
    if (txtNome.value == '' || txtEmail.value == '')
        window.top.overflyGen.Alert('Informe o Email e Nome.');
    else
        verifica_email();

}

function verifica_email() 
{
    var email = txtEmail.value;

    if (email.indexOf('@') > 0 && (email.indexOf('.com') > 0 || email.indexOf('.br') > 0))
        gravaConvite();
    else 
    {
        window.top.overflyGen.Alert('Digite um e-mail valido!!');
        lockControlsInModalWin(false);
        txtEmail.focus();
    }
}
      