<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    Dim rsData, strSQL
    
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalhistoricosfuncionaisHtml" name="modalhistoricosfuncionaisHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/modalpages/modalhistoricosfuncionais.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/modalpages/modalhistoricosfuncionais.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, sCurrDateFormat
Dim nEmpresaID

sCaller = ""
nEmpresaID = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"

Response.Write vbcrlf

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

 Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";" & vbcrlf


Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalhistoricosfuncionais_ValidateEdit(fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalhistoricosfuncionais_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalhistoricosfuncionaisKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
<!--
 js_modalhistoricosfuncionais_BeforeEdit(fg, fg.Row, fg.Col)
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_fg_modalhistoricosfuncionaisDblClick (fg, fg.Row, fg.Col);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalhistoricosfuncionaisBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
 js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Para celulas checkbox que sao readonly
treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//Atualizar linha de mensagens
js_fg_AfterRowColmodalhistoricosfuncionais (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 //Atencao programador:
 //Setimo parametro abaixo = true, permite ao usuario mudar de linha em grid
 //clicando em celula read only.
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>

<script ID="vbFunctions" LANGUAGE="vbscript">
<!--

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

//-->
</script>

</head>

<body id="modalhistoricosfuncionaisBody" name="modalhistoricosfuncionaisBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    <!-- Div Controles -->
    <div id="divControls" name="divControls" class="divGeneral">
        <p id="lblModo" name="lblModo" class="lblGeneral">Modo</p>
         <select id="selModo" name="selModo" class="fldGeneral">
			<option value="1">Consulta</option>
			<option value="2">Manuten��o</option>
		 </select>
        <p id="lblColaborador" name="lblColaborador" class="lblGeneral">Colaborador</p>
        <select id="selColaborador" name="selColaborador" class="fldGeneral">
	   <%Set rsData = Server.CreateObject("ADODB.Recordset")
			 strSQL = "SELECT 0 as fldID, SPACE(0) AS fldName "& _
			     "UNION " & _
			    "SELECT a.PessoaID AS fldID, a.Fantasia AS fldName " & _
			    "FROM Pessoas a WITH(NOLOCK) " & _
				"INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON ( b.SujeitoID = a.PessoaID ) " & _
				"WHERE b.ObjetoID = " & nEmpresaID & " and ( b.TipoRelacaoID = 31 ) AND ( a.EstadoID = 2 ) AND ( b.EstadoID = 2 ) " & _
				"ORDER BY fldName ASC "				
				
			    rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			    While Not rsData.EOF
				 Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
				
				 If ( CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaID) ) Then
					Response.Write " SELECTED "
				 End If

				 Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				 rsData.MoveNext
			    Wend

			rsData.Close
			Set rsData = Nothing%>
        </select>
        <p id="lblDepartamento" name="lblDepartamento" class="lblGeneral">Departamento</p>
        <select id="selDepartamento" name="selDepartamento" class="fldGeneral">
	   <%Set rsData = Server.CreateObject("ADODB.Recordset")
             strSQL = "SELECT 0 as fldID, SPACE(0) AS fldName " & _
			     "UNION " & _
                "SELECT a.PessoaID AS fldID, a.Fantasia AS fldName " & _
                "FROM Pessoas a WITH(NOLOCK) " & _
                "INNER JOIN Pessoas_Empresas b WITH(NOLOCK) ON a.PessoaID = b.PessoaID " & _
                "WHERE a.TipoPessoaID=54 AND b.EmpresaID = " & nEmpresaID & " and a.EstadoID = 2 "
                
			    rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			    While Not rsData.EOF
				 Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)

				 Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				 rsData.MoveNext
			    Wend
			rsData.Close
			Set rsData = Nothing%>
        </select> 
        <p id="lblCargo" name="lblCargo" class="lblGeneral">Cargo</p>
        <select id="selCargo" name="selCargo" class="fldGeneral">
	   <%Set rsData = Server.CreateObject("ADODB.Recordset")
           strSQL = "SELECT 0 as fldID, SPACE(0) AS fldName, 0 as NivelMaximo " & _
			     "UNION " & _
                 "SELECT a.RecursoID AS fldID, a.RecursoFantasia AS fldName, b.NivelMaximo " & _
                "FROM Recursos a WITH(NOLOCK) " & _
                "INNER JOIN Recursos_Empresas b WITH(NOLOCK) ON a.RecursoID=b.RecursoID " & _
                "WHERE (a.TipoRecursoID=6 AND a.EhCargo=1 AND b.EmpresaID=" & nEmpresaID & ")" 
     
                rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

			    While Not rsData.EOF
				 Response.Write "<option value ='" & rsData.Fields("fldID").Value & "' "
				 Response.Write "nivelMaximo ='" & rsData.Fields("NivelMaximo").Value & "' "
    			 Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
				 rsData.MoveNext
			    Wend

			rsData.Close
			Set rsData = Nothing%>
        </select>
        <p id="lblNivel" name="lblNivel" class="lblGeneral">Nivel</p>
        <select id="selNivel" name="lblNivel" class="fldGeneral">
        </select>
            <p id="lblInicio" name="lblInicio" class="lblGeneral">Inicio</p>
            <input type="text" id="txtInicio" name="txtInicio" class="fldGeneral" title="Inicio">
            <p id="lblFim" name="lblFim" class="lblGeneral">Fim</p>
            <input type="text" id="txtFim" name="txtFim" class="fldGeneral" title="Fim">
            <p id="lblVigencia" name="lblVigencia" class="lblGeneral">Vig�ncia</p>
            <input type="text" id="txtVigencia" name="txtVigencia" class="fldGeneral" title="Vig�ncia">
    </div>    

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    
            
	<input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnExcel" name="btnExcel" value="Excel" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>
</body>

</html>
