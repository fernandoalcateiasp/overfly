/********************************************************************
modalcriteriosavaliacao.js

Library javascript para o modalcriteriosavaliacao.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_dataGridWasChanged = false;

// Controla retornos de servidor
var glb_nServerReturned = 0;
var glb_ServerTimer = null;

// Controla se houve gravacao para dar refresh no inf
var glb_dataSaved = false;

// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
// RDS para gravacao
var dsoGrava = new CDatatransport("dsoGrava");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO MODALCRITERIOSAVALIACAO.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO MODALCRITERIOSAVALIACAO.ASP

js_fg_modalcriteriosavaliacaoBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalcriteriosavaliacaoDblClick( grid, Row, Col)
js_modalcriteriosavaliacaoKeyPress(KeyAscii)
js_modalcriteriosavaliacao_AfterRowColChange
js_modalcriteriosavaliacao_ValidateEdit()
js_modalcriteriosavaliacao_AfterEdit(Row, Col)
js_modalcriteriosavaliacao_BeforeEdit(grid, row, col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	with (btnCanc)
    {
		style.visibility = 'visible';
    }
    
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalcriteriosavaliacaoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
    
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Crit�rios de Avalia��o', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    //centerBtnInModal(btnOK);
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    // codigo privado desta janela
    else
    {
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, glb_dataSaved ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    
    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if ( glb_ServerTimer != null )
	{
		window.clearInterval(glb_ServerTimer);
		glb_ServerTimer = null;
	}
	
    lockControlsInModalWin(true);
    
    // zera o grid
    fg.Rows = 1;
    
    glb_dataGridWasChanged = false;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);
    
    // parametrizacao do dso dsoGrava
    setConnection(dsoGrava);

    dsoGrid.SQL = 
        'SELECT RelPesRAFCriterioID, RelPesRAFID, CriterioID,' +
			'Peso, Pontuacao, Dividendo, Divisor, QAFID, Comentarios,' +
			'(SELECT ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE (a.CriterioID = ItemID)) AS Criterio, ' +
			'(SELECT Aplicar FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE (a.CriterioID = ItemID)) AS ReadOnly ' +
		'FROM RelacoesPessoas_RAF_Criterios a WITH(NOLOCK) ' +
		'WHERE (a.RelPesRAFID = ' + glb_nRelPesRAFID + ')' + 
		'ORDER BY CriterioID';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    dsoGrava.SQL = 
        'SELECT a.RelPesRAFCriterioID, a.RelPesRAFID, a.CriterioID, ' +
        'a.Peso, a.Pontuacao, a.Dividendo, a.Divisor, ' +
        'a.QAFID, a.Comentarios ' +
		'FROM RelacoesPessoas_RAF_Criterios a WITH(NOLOCK) ' +
		'WHERE (a.RelPesRAFID = ' + glb_nRelPesRAFID + ')' +
		'ORDER BY CriterioID';

    dsoGrava.ondatasetcomplete = fillGridData_DSC;
    
    glb_nServerReturned = 0;
    
    dsoGrava.Refresh();
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nServerReturned++;
	
	if ( glb_nServerReturned < 2 )
		return true;

    showExtFrame(window, true);

    var dTFormat = '';
    var i;
    var aLinesState = new Array();
    var bTempValue;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
			
    headerGrid(fg,['Crit�rio',
                   'Peso',
                   'Pontua��o',
                   'Dividendo',
                   'Divisor',
                   'QAF',
                   'Coment�rios',
                   'ReadOnly',
                   'CriterioID',
                   'RelPesRAFCriterioID'],[7, 8, 9]);
                       
    fillGridMask(fg,dsoGrid,['Criterio*',
							 'Peso*', 
							 'Pontuacao', 
							 'Dividendo*', 
							 'Divisor*', 
							 'QAFID*', 
							 'Comentarios',
							 'ReadOnly',
							 'CriterioID',
							 'RelPesRAFCriterioID'],
                              ['','','999.99','','','','','',''],
                              ['','','###.00','','','','','','']);

	// Seta linhas readonly
	// Cuidado quando o grid tiver linha de totalizacao
	if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) )
	{
		if ( aLinesState.length == 0 )
			aLinesState[0] = true;
		
		for ( i=1; i< fg.Rows; i++ )
		{
			if ( getCellValueByColKey(fg, 'ReadOnly', i) != 0 )
				bTempValue = true;
			else
				bTempValue = false;
			
			aLinesState[aLinesState.length] = bTempValue;
		}

		setLinesState(fg, aLinesState);
	}
    
	alignColsInGrid(fg,[1,2,3,4,5]);
                              
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.ColWidth(6) = 60 * FONT_WIDTH * 15;
    
    fg.FrozenCols = 1;
    
    fg.Redraw = 2;
    
	// seleciona a primeira linha nao readonly
    // se a mesma existe
    for ( i=1; i<fg.Rows; i++ )
    {
		if ( !(getCellValueByColKey(fg, 'ReadOnly', i) != 0) )
		{
			fg.Row = i;
			break;
		}
    }
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 2 e a 6
    if ( fg.Rows > 1 )
        fg.Col = 2;    
    
    if (fg.Rows > 1)
        fg.Editable = true;

    lockControlsInModalWin(false);
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    glb_dataGridWasChanged = false;
    
    lockControlsInModalWin(true);
    
    try
    {
        dsoGrava.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }

        lockControlsInModalWin(false);
        
        setupBtnsFromGridState();
    }    

    dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrava.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);
    
    glb_dataSaved = true;
    
	glb_ServerTimer = window.setInterval('fillGridData()', 50, 'JavaScript');
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcriteriosavaliacaoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalcriteriosavaliacaoDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcriteriosavaliacaoKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcriteriosavaliacao_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcriteriosavaliacao_AfterEdit(Row, Col)
{
    var nType;
    var dso, sColKey;
    
    if (fg.Editable)
    {
		if (Col == 2)
		{
			if (fg.ValueMatrix(Row, Col) > 100)
			{
				if ( window.top.overflyGen.Alert ('Pontua��o deve ser entre 0 e 100!') == 0 )
                    return null;
                    
				fg.TextMatrix(Row, Col) = '';
			}
			
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
        }    

        // Altera o campo do dso com o novo valor
        dso = dsoGrava;
        
        if ( !(dso.recordset.BOF && dso.recordset.EOF) )
        {
			dso.recordset.MoveFirst();
			dso.recordset.Find('RelPesRAFCriterioID', fg.TextMatrix(Row, 9));
        
			if ( !(dso.recordset.EOF) )
			{
				sColKey = fg.ColKey(Col);
				
				if ( sColKey.substr(sColKey.length - 1, 1) == '*' )
					sColKey = sColKey.substr(0, sColKey.length - 1);
				
			    nType = dso.recordset[sColKey].type;
			    
			    // Se decimal , numerico , int, bigint ou boolean
			    if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
			        dso.recordset[sColKey].value = fg.ValueMatrix(Row, Col);
			    else    
			        dso.recordset[sColKey].value = fg.TextMatrix(Row, Col);

			    glb_dataGridWasChanged = true;
			    setupBtnsFromGridState();
			}
        }    
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalcriteriosavaliacao_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
    
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

// FINAL DE EVENTOS DE GRID *****************************************
