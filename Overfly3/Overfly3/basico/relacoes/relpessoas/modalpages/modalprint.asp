
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalprintHtml" name="modalprintHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/modalpages/modalprint.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_progressbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodalprint.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/modalpages/modalprint_relpessoas.js" & Chr(34) & "></script>" & vbCrLf
%>


<%
'Script de variaveis globais

Dim i, sCaller, nEmpresaID, sEmpresaFantasia, nContextoID, nUserID, sCurrDateFormat
Dim relatorioID, relatorio, EhDefault, relatorioFantasia, dirA1, dirA2, dirA1A2True

sCaller = ""
nEmpresaID = 0
sEmpresaFantasia = ""
nContextoID = 0
nUserID = 0
sCurrDateFormat = ""

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

'nEmpresaID - ID da empresa logada
For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

'sEmpresaFantasia - fantasia da empresa logada
For i = 1 To Request.QueryString("sEmpresaFantasia").Count    
    sEmpresaFantasia = Request.QueryString("sEmpresaFantasia")(i)
Next

'nContextoID - ID do contexto
For i = 1 To Request.QueryString("nContextoID").Count    
    nContextoID = Request.QueryString("nContextoID")(i)
Next

'nUserID - ID do usuario logado
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

'Formato corrente de data
For i = 1 To Request.QueryString("sCurrDateFormat").Count
    sCurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nEmpresaID = " & nEmpresaID & ";"
Response.Write vbcrlf

Response.Write "var glb_sEmpresaFantasia = " & Chr(39) & sEmpresaFantasia & Chr(39) & ";"
Response.Write vbcrlf

If (sCurrDateFormat = "DD/MM/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
    Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
    Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
Else
    Response.Write "var glb_dCurrDate = '';" & vbcrlf
End If

'Monta array dos relatorios
Response.Write "// Array dos relatorios, com seus ids, nomes"
Response.Write vbcrlf

Dim rsData, strSQL
Dim rsData1

If ( (sCaller) = "PL" ) Then
    i = 0
Else
    i = 1
End If        

strSQL = "SELECT DISTINCT d.RecursoID AS RelatorioID, d.Recurso AS Relatorio, " & _
         "c.EhDefault, d.RecursoFantasia AS RelatorioFantasia, c.Ordem, " & _
         "g.Alterar1 AS A1, g.Alterar2 AS A2 " & _
         "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _
         "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " & _
         "WHERE a.ObjetoID= " & CStr(nContextoID) & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 " & _
         "AND a.SujeitoID=b.RecursoID AND b.EstadoID=2 AND b.TipoRecursoID=4 " & _ 
         "AND b.Principal=1 AND b.RecursoID=c.ObjetoID AND c.EstadoID=2 " & _
         "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID " & _
         "AND d.EstadoID=2 AND d.TipoRecursoID=5 AND d.ClassificacaoID=14 AND d.EhDetalhe= " & CStr(i) & " " & _
         "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & CStr(nContextoID) & " )) " & _
         "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
         "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
         "AND e.SujeitoID IN ((SELECT " & CStr(nUserID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(nUserID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND e.RelacaoID=f.RelacaoID " & _
         "AND f.EmpresaID= " & CStr(nEmpresaID) & " AND g.ContextoID= " & CStr(nContextoID) & " " & _
         "AND c.ObjetoID=g.RecursoMaeID AND d.RecursoID=g.RecursoID " & _
         "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " & _
         "ORDER BY c.Ordem"

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Response.Write vbcrlf
Response.Write "var glb_arrayReports = new Array();"
Response.Write vbcrlf
Response.Write vbcrlf
'------------------
i = 0
Do While (Not rsData.EOF)
        
    relatorioID = rsData.Fields("RelatorioID").Value
    relatorio = rsData.Fields("Relatorio").Value
    EhDefault = rsData.Fields("EhDefault").Value
    relatorioFantasia = rsData.Fields("RelatorioFantasia").Value
    dirA1 = rsData.Fields("A1").Value
    dirA2 = rsData.Fields("A2").Value
    
    If ( dirA1 AND dirA2 ) Then
        dirA1A2True = TRUE        
    Else
        dirA1A2True = FALSE    
    End If
        
    rsData.MoveNext
        
    Do While (Not rsData.EOF)
        If ( CLng(relatorioID) = CLng(rsData.Fields("RelatorioID").Value) ) Then
            'dirX1 e dirX2 e o anterior sempre
            If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
                dirA1A2True = TRUE        
            End If

            dirA1 = dirA1 OR rsData.Fields("A1").Value
            dirA2 = dirA2 OR rsData.Fields("A2").Value
            rsData.MoveNext
        Else
            Exit Do
        End If    
    Loop    
        
    If ( (dirA1) AND (dirA2) ) Then
        If ( NOT (dirA1A2True)  ) Then
            dirA1 = FALSE
            dirA2 = TRUE
        End If
    End If    

    Response.Write "glb_arrayReports[" & CStr(i) & "] = new Array("
    Response.Write Chr(39)
    Response.Write CStr(relatorio)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write CStr(relatorioID)
    Response.Write ","
    Response.Write LCase(CStr(EhDefault))
    Response.Write ","
    Response.Write Chr(39)
    Response.Write CStr(relatorioFantasia)
    Response.Write Chr(39)
    Response.Write ","
    Response.Write LCase(CStr(dirA1))
    Response.Write ","
    Response.Write LCase(CStr(dirA2))
    Response.Write ");"
    i = i + 1

Loop

rsData.Close
Set rsData = Nothing

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"

Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=oPrinter EVENT=OnPrintFinish>
<!--
 oPrinter_OnPrintFinish();
//-->
</SCRIPT>

</head>

<body id="modalprintBody" name="modalprintBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Controle de impressao matricial -->
    <object classid="clsid:1DF93CFF-D5C8-4A6D-983F-19DBAAC698D9" id = "oPrinter" name = "oPrinter" width=0 height=0></object>
    
    <p id="lblReports" name="lblReports" class="paraNormal"></p>
    <select id="selReports" name="selReports" class="fldGeneral"></select>

    <div id="divEtiquetasMalaDireta" name="divEtiquetasMalaDireta" class="divGeneral">
        <p id="lblEtiquetaVertical" name="lblEtiquetaVertical" class="lblGeneral">EV</p>
        <input type="text" id="txtEtiquetaVertical" name="txtEtiquetaVertical" class="fldGeneral" title="Etiquetas Verticais"></input>
        <p id="lblEtiquetaHorizontal" name="lblEtiquetaHorizontal" class="lblGeneral">EH</p>
        <input type="text" id="txtEtiquetaHorizontal" name="txtEtiquetaHorizontal" class="fldGeneral" title="Etiquetas Horizontais"></input>
        <p id="lblAltura" name="lblAltura" class="lblGeneral">Alt</p>
        <input type="text" id="txtAltura" name="txtAltura" class="fldGeneral" title="Altura (linhas)"></input>
        <p id="lblLargura" name="lblLargura" class="lblGeneral">Larg</p>
        <input type="text" id="txtLargura" name="txtLargura" class="fldGeneral" title="Largura (mm)"></input>
        <p id="lblMargemSuperior" name="lblMargemSuperior" class="lblGeneral">MS</p>
        <input type="text" id="txtMargemSuperior" name="txtMargemSuperior" class="fldGeneral" title="Margem Superior (linhas)"></input>
        <p id="lblMargemEsquerda" name="lblMargemEsquerda" class="lblGeneral">ME</p>
        <input type="text" id="txtMargemEsquerda" name="txtMargemEsquerda" class="fldGeneral" title="Margem Esquerda (mm)"></input>
        <p id="lblCEPInicio" name="lblCEPInicio" class="lblGeneral">CEP Inicial</p>
        <input type="text" id="txtCEPInicio" name="txtCEPInicio" class="fldGeneral"></input>
        <p id="lblCEPFim" name="lblCEPFim" class="lblGeneral">CEP Final</p>
        <input type="text" id="txtCEPFim" name="txtCEPFim" class="fldGeneral"></input>
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral"></input>
    </div>
    <div id="divExtratoFinanceiro" name="divExtratoFinanceiro" class="divGeneral">        
        <p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
		<select id="selEmpresas" name="selEmpresas" class="fldGeneral" multiple></select>
        <p id="lblDataInicio2" name="lblDataInicio2" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio2" name="txtDataInicio2" class="fldGeneral"></input>
        <p id="lblDataFim2" name="lblDataFim2" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim2" name="txtDataFim2" class="fldGeneral"></input>
    </div>

    <div id="divExtratoCliente" name="divExtratoCliente" class="divGeneral">
        <p id="lblEmpresa" name="lblEmpresa" class="lblGeneral">Fornecedor</p>
        <input type="checkbox" id="chkEmpresa" name="chkEmpresa" class="fldGeneral" title="Todas as empresas do sistema?"></input>
        <p id="lblDados" name="lblDados" class="lblGeneral">Dados</p>
        <input type="checkbox" id="chkDados" name="chkDados" class="fldGeneral" title="Dados Cadastrais?"></input>
        <p id="lblPedidos" name="lblPedidos" class="lblGeneral">Pedidos</p>
        <input type="checkbox" id="chkPedidos" name="chkPedidos" class="fldGeneral" title="Pedidos?"></input>
        <p id="lblProdutos" name="lblProdutos" class="lblGeneral">Produtos</p>
        <input type="checkbox" id="chkProdutos" name="chkProdutos" class="fldGeneral" title="Produtos?"></input>
        <p id="lblFinanceiros" name="lblFinanceiros" class="lblGeneral">Financeiros</p>
        <input type="checkbox" id="chkFinanceiros" name="chkFinanceiros" class="fldGeneral" title="Financeiros?"></input>
        <p id="lblObservacoes" name="lblObservacoes" class="lblGeneral">Obs</p>
        <input type="checkbox" id="chkObservacoes" name="chkObservacoes" class="fldGeneral" title="Observa��es?"></input>
        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral"></input>
        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral"></input>
        <p id="lblFormatoSolicitacao" name="lblFormatoSolicitacao" class="lblGeneral">Formato</p>
        <select id="selFormatoSolicitacao" name="selFormatoSolicitacao" class="fldGeneral">
            <option value="2">Excel</option>		
            <option value="1">PDF</option>	
        </select>
    </div>

    <div id="divExtratoClientePL" name="divExtratoClientePL" class="divGeneral">
        <p id="lblEmpresaPL" name="lblEmpresaPL" class="lblGeneral">Empresa</p>
        <input type="checkbox" id="chkEmpresaPL" name="chkEmpresaPL" class="fldGeneral" title="Apenas dados da empresa logada?"></input>
        <p id="lblDadosPL" name="lblDadosPL" class="lblGeneral">Dados</p>
        <input type="checkbox" id="chkDadosPL" name="chkDadosPL" class="fldGeneral" title="Dados Cadastrais?"></input>
        <p id="lblPedidosPL" name="lblPedidosPL" class="lblGeneral">Pedidos</p>
        <input type="checkbox" id="chkPedidosPL" name="chkPedidosPL" class="fldGeneral" title="Pedidos?"></input>
        <p id="lblProdutosPL" name="lblProdutosPL" class="lblGeneral">Produtos</p>
        <input type="checkbox" id="chkProdutosPL" name="chkProdutosPL" class="fldGeneral" title="Produtos?"></input>
        <p id="lblFinanceirosPL" name="lblFinanceirosPL" class="lblGeneral">Financeiros</p>
        <input type="checkbox" id="chkFinanceirosPL" name="chkFinanceirosPL" class="fldGeneral" title="Financeiros?"></input>
        <p id="lblObservacoesPL" name="lblObservacoesPL" class="lblGeneral">Obs</p>
        <input type="checkbox" id="chkObservacoesPL" name="chkObservacoesPL" class="fldGeneral" title="Observa��es?"></input>
        <p id="lblDataInicioPL" name="lblDataInicioPL" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicioPL" name="txtDataInicioPL" class="fldGeneral"></input>
        <p id="lblDataFimPL" name="lblDataFimPL" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFimPL" name="txtDataFimPL" class="fldGeneral"></input>

		<p id="lblChave" name="lblChave" class="lblGeneral">Chave</p>
		<select id="selChave" name="selChave" class="fldGeneral">
			<option value="cnpj">Doc. Federal</option>
			<option value="id">ID</option>
		</select>
        <p id="lblValorChave" name="lblValorChave" class="lblGeneral">Valor</p>
        <input type="text" id="txtValorChave" name="txtValorChave" class="fldGeneral"/>
		<input type="button" id="btnInserir" name="btnInserir" value="Inserir" LANGUAGE="javascript" onclick="btnInserir_Clicked()" class="btns"/>
		<input type="button" id="btnRemover" name="btnRemover" value="Remover" LANGUAGE="javascript" onclick="btnRemover_Clicked()" class="btns"/>        
        
		<div id="divGridClientes" name="divGridClientes" class="divExtern" LANGUAGE=javascript>
			<!-- Grid dos clientes -->
			<object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgClientes" name="fgClientes" viewastext></object>
			<img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"/>
			<img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"/>
			<img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"/>
        </div>
    </div>

	<div id="divProgramasMarketing" name="divProgramasMarketing" class="divGeneral">
		<p id="lblProgramasMarketing" name="lblProgramasMarketing" class="lblGeneral">Programas de Marketing</p>
		<select id="selProgramasMarketing" name="selProgramasMarketing" class="fldGeneral"></select>
        <p id="lblFiltro2" name="lblFiltro2" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro2" name="txtFiltro2" class="fldGeneral"></input>
    </div>

    <div id="divSeguroDeCredito" name="divSeguroDeCredito" class="divGeneral">
		<p id="lblSeguradora" name="lblSeguradora" class="lblGeneral">Seguradora</p>
		<select id="selSeguradora" name="selSeguradora" class="fldGeneral">
			<option value="0"> </option>
            <%
	            Set rsData = Server.CreateObject("ADODB.Recordset")

                strSQL = "SELECT DISTINCT Seguros.SeguradoraID AS fldID, Seguradoras.Fantasia AS fldName " & _
                    "FROM RelacoesPessoas Relacoes WITH(NOLOCK) " & _
                        "INNER JOIN Creditos Seguros WITH(NOLOCK) ON Seguros.ParceiroID = Relacoes.SujeitoID " & _
                        "INNER JOIN Pessoas Seguradoras WITH(NOLOCK) ON Seguradoras.PessoaID = Seguros.SeguradoraID " & _
                        "INNER JOIN (SELECT " & CStr(nEmpresaID) & " AS EmpresaID " & _
                        "UNION ALL " & _
                        "SELECT EmpresaAlternativaID " & _
                            "FROM FopagEmpresas WITH(NOLOCK) " & _
                            "WHERE (EmpresaID = " & CStr(nEmpresaID) & " AND FuncionarioID IS NULL)) EmpresaGrupo ON Relacoes.ObjetoID = EmpresaGrupo.EmpresaID " & _
                    "WHERE Seguradoras.TipoPessoaId = 52 AND Relacoes.TipoRelacaoID = 21 AND Seguros.SeguradoraID IS NOT NULL AND Seguros.EstadoID = 75"

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	            If (Not (rsData.BOF AND rsData.EOF) ) Then

		            While Not (rsData.EOF)

			            Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			            Response.Write( rsData.Fields("fldName").Value & "</option>" )

			            rsData.MoveNext()

		            WEnd

	            End If

	            rsData.Close
	            set rsData = Nothing
            %>		
		</select>

		<p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
		<select id="selMarca" name="selMarca" class="fldGeneral">
			<option value="0"> </option>
            <%
	            Set rsData = Server.CreateObject("ADODB.Recordset")

	            strSQL = "SELECT a.ConceitoID as fldID, a.Conceito as fldName " & _
                    "FROM	Conceitos a WITH(NOLOCK) " & _
                    "WHERE	a.EstadoID = 2 AND a.TipoConceitoID = 304 " & _
                    "ORDER BY Conceito"

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	            If (Not (rsData.BOF AND rsData.EOF) ) Then

		            While Not (rsData.EOF)

			            Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
			            Response.Write( rsData.Fields("fldName").Value & "</option>" )

			            rsData.MoveNext()

		            WEnd

	            End If

	            rsData.Close
	            set rsData = Nothing
            %>		
		</select>

        <p id="lblValorSolicitadoMinimo" name="lblValorSolicitadoMinimo" class="lblGeneral">Valor Solicitado M�nimo</p>
        <input type="text" id="txtValorSolicitadoMinimo" name="txtValorSolicitadoMinimo" class="fldGeneral"/>
        <p id="lblValorSolicitadoMaximo" name="lblValorSolicitadoMaximo" class="lblGeneral">Valor Solicitado M�ximo</p>
        <input type="text" id="txtValorSolicitadoMaximo" name="txtValorSolicitadoMaximo" class="fldGeneral"/>
        
        <p id="lblValorAprovadoMinimo" name="lblValorAprovadoMinimo" class="lblGeneral">Valor Aprovado M�nimo</p>
        <input type="text" id="txtValorAprovadoMinimo" name="txtValorAprovadoMinimo" class="fldGeneral"/>
        <p id="lblValorAprovadoMaximo" name="lblValorAprovadoMaximo" class="lblGeneral">Valor Aprovado M�ximo</p>
        <input type="text" id="txtValorAprovadoMaximo" name="txtValorAprovadoMaximo" class="fldGeneral"/>
        

        <p id="lblDataSolicitacaoInicio" name="lblDataSolicitacaoInicio" class="lblGeneral">Solicita��o In�cio</p>
        <input type="text" id="txtDataSolicitacaoInicio" name="txtDataSolicitacaoInicio" class="fldGeneral"/>
        <p id="lblDataSolicitacaoFim" name="lblDataSolicitacaoFim" class="lblGeneral">Solicita��o Fim</p>
        <input type="text" id="txtDataSolicitacaoFim" name="txtDataSolicitacaoFim" class="fldGeneral"/>
        
        <p id="lblDataVencimentoInicio" name="lblDataVencimentoInicio" class="lblGeneral">Vencimento In�cio</p>
        <input type="text" id="txtDataVencimentoInicio" name="txtDataVencimentoInicio" class="fldGeneral"/>
        <p id="lblDataVencimentoFim" name="lblDataVencimentoFim" class="lblGeneral">Vencimento Fim</p>
        <input type="text" id="txtDataVencimentoFim" name="txtDataVencimentoFim" class="fldGeneral"/>
        
        <p id="lblDataCancelamentoInicio" name="lblDataCancelamentoInicio" class="lblGeneral">Cancelamento In�cio</p>
        <input type="text" id="txtDataCancelamentoInicio" name="txtDataCancelamentoInicio" class="fldGeneral"/>
        <p id="lblDataCancelamentoFim" name="lblDataCancelamentoFim" class="lblGeneral">Cancelamento Fim</p>
        <input type="text" id="txtDataCancelamentoFim" name="txtDataCancelamentoFim" class="fldGeneral"/>

		<p id="lblAprovacao" name="lblAprovacao" class="lblGeneral">Aprova��o</p>
		<select id="selAprovacao" name="selAprovacao" class="fldGeneral">
			<option value="NULL">Todos</option>
			<option value="0">Aprovados</option>
			<option value="1">Recusados</option>
		</select>
    </div>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal">
        </p>
    </div>    

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>

</body>

</html>
