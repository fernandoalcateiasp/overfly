/********************************************************************
modalprint_relpessoas.js

Library javascript para o modalprint.asp
Relacoes entre Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// variavel de timer
var __timerInt;

//  Dados de impressao matricial .SQL 
var dsoPrint01 = new CDatatransport("dsoPrint01");
//  dso usado para envia e-mails 
var dsoEMail = new CDatatransport("dsoEMail");
//  dso usado para preenchimento de combo 
var dsoCmbs01 = new CDatatransport("dsoCmbs01");
//  Controle de impressao matricial 
var dsoEmpresas = new CDatatransport("dsoEmpresas");
//  Inclus�o de dados no grid de filtro para o relatorio do pesqlist 
var dsoGridClientes = new CDatatransport("dsoGridClientes");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linksDivAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    btnInserir_Clicked()
    inserir_DSC()
    btnRemover_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
	
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    if ( selReports.disabled == false )
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();

	btnRemover_Status();        
	btnOK_Status();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    
    if (ctl.id == btnOK.id )
    {
        btnOK.focus();
    }
    else if (ctl.id == btnCanc.id )
    {
        btnCanc.focus();
    }
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
	}
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Relat�rios', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style)
    {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta combo de relatorios
    with (selReports.style)
    {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta o Div de Etiquetas de Mala Direta
    with (divEtiquetasMalaDireta.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o Div de Extrato de Cliente
    with (divExtratoCliente.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
	
    // ajusta o Div de Extrato de Cliente do PesqList
    with (divExtratoClientePL.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // Ajusta o div de seguro de cr�dito
    with (divSeguroDeCredito.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }
	
    // ajusta o Div de Extrato Financeiro
    with (divExtratoFinanceiro.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // ajusta o Div de Extrato de Cliente
    with (divProgramasMarketing.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = vGap + ELEM_GAP;
        width = widthFree - (2 * ELEM_GAP);
        height = topFree + heightFree - parseInt(top, 10) - ELEM_GAP;
    }

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // O estado do botao btnOK
    btnOK_Status();
    
    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // linka os divs nos relatorios
    linkDivsAndReports();
    
    // ajusta o divEtiquetasEmbalagem
    adjustDivEtiquetasMalaDireta();

    // ajusta o divExtratoCliente
    adjustDivExtratoCliente();
    
    // ajusta o divExtratoCliente
    adjustDivExtratoClientePL();
    
    // ajusta o divExtratoCliente
    adjustDivSeguroDeCredito();
    
    // ajusta o divExtratoFinanceiro
    adjustDivExtratoFinanceiro();
    
    // ajusta o divProgramasMarketing
    adjustDivProgramasMarketing();

    // restringe digitacao em campos numericos
    setMaskAndLengthToInputs();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    if ( glb_arrayReports != null )
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
    divExtratoCliente.setAttribute('report', 40101 , 1);
    divExtratoClientePL.setAttribute('report', 40108 , 1);
    divSeguroDeCredito.setAttribute('report', 40109, 1);
    divEtiquetasMalaDireta.setAttribute('report', 40102 , 1);
    divProgramasMarketing.setAttribute('report', 40105 , 1);
    divExtratoFinanceiro.setAttribute('report', 40103 , 1);
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
	adjustDivExtratoClientePL();
	
    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if ( this.disabled == false )
        this.focus();
    
    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    var i, elem;
    
    if ( selReports.selectedIndex != -1 )
    {
        // Extrato de Cliente
        if (selReports.value == 40101)
            btnOKStatus = false;
        else if (selReports.value == 40108) 
            btnOKStatus = fgClientes.Rows <= 1;
        else if (selReports.value == 40109) 
            btnOKStatus = false;
        else if (selReports.value == 40102)
            btnOKStatus = false;
        else if (selReports.value == 40103)
            btnOKStatus = false;
        else if (selReports.value == 40105)
            btnOKStatus = false;        
    }
    
    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Seta o status do botao remover em funcao do numero de linhas do 
grid de clientes. Se nao houver linhas no grid, o botao deve 
ficar desabilitado.
********************************************************************/
function btnRemover_Status()
{
	btnRemover.disabled = (fgClientes.Rows - 1) < 1;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    // Extrato de Cliente
    if ( (selReports.value == 40101) && (glb_sCaller == 'S') )
        RelatorioDeExtratoDeCliente();    
    else if ( (selReports.value == 40108) && (glb_sCaller == 'PL') )
        extratoClientePL();
    else if ( (selReports.value == 40109) && (glb_sCaller == 'PL') )
        seguroDeCredito();
    else if ( (selReports.value == 40102) && (glb_sCaller == 'PL') )
        etiquetaMalaDireta();
	else if ( (selReports.value == 40105) && (glb_sCaller == 'PL') )
        programasMarketing();
    else if ( (selReports.value == 40103) && (glb_sCaller == 'S') )
        extratoFinanceiro();
    else        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
}

/********************************************************************
Consulta na base o cliente solicitado pelo usuario para inseri-lo
no grid. O cliente informado no campo valor sera inserido se o grid
nao possui-lo.
********************************************************************/
function btnInserir_Clicked()
{
	if(txtValorChave.value == null || txtValorChave.value == "")
	{
		window.top.overflyGen.Alert("Inclua um cliente no campo valor");
		return;
	}

	for(row = 1; row < fgClientes.Rows; row++)
	{
		var doc;
		
		if(selChave.value == "id")
		{
			doc = fgClientes.TextMatrix(row, 0); // Coluna ID
		}
		else
		{
			doc = fgClientes.TextMatrix(row, 1); // Doc. Federal
		}

		if(txtValorChave.value == doc)
		{
			window.top.overflyGen.Alert("Este cliente j� foi incluido.");
			return;
		}
	}
	
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	
    setConnection(dsoGridClientes);

    dsoGridClientes.SQL = 
		"SELECT " +
			"Pessoas.PessoaID as ID, Documentos.Numero as DocFed, Pessoas.Nome, Pessoas.Fantasia, " +
			"dbo.fn_Empresa_Sistema(Pessoas.PessoaID) as EmpresaDoSistema " +
		"FROM " +
			"Pessoas WITH(NOLOCK) " +
				"INNER JOIN Pessoas_documentos Documentos WITH(NOLOCK) ON Documentos.PessoaID = Pessoas.PessoaID " +
				"INNER JOIN RelacoesPessoas Relacoes WITH(NOLOCK) ON Relacoes.SujeitoID = Pessoas.PessoaID " +
				"INNER JOIN TiposAuxiliares_Itens TipoDocumento WITH(NOLOCK) ON TipoDocumento.ItemID = Documentos.TipoDocumentoID " +
		"WHERE " +
				"TipoDocumento.EstadoID = 2 " +
			"AND Pessoas.TipoPessoaID IN (51, 52) " +
			"AND Documentos.TipoDocumentoID IN (101, 111) " +
			"AND Relacoes.TipoRelacaoID = 21 " +

			"AND Relacoes.ObjetoID =  " + aEmpresaData[0] +

			((selChave.value == "id") ? " AND Pessoas.PessoaID = " : " AND Documentos.Numero = ") +
			"'" + txtValorChave.value + "' " + 

		"ORDER BY " +
			"Pessoas.PessoaID, Documentos.Numero ";
		
    dsoGridClientes.ondatasetcomplete = inserir_DSC;
    dsoGridClientes.Refresh();
    
    // Elimina os dados digitados para inclusao de outro.
    txtValorChave.value = "";
}

function inserir_DSC()
{
    if(dsoGridClientes.recordset.EOF)
    {
        window.top.overflyGen.Alert('Esta pessoa n�o est� cadastrada no sistema.');
        return;
    }
    
    // Nao permite inclus�o de empresas do grupo
    if(dsoGridClientes.recordset["EmpresaDoSistema"].value == 1)
    {
        window.top.overflyGen.Alert('Uma empresa do grupo n�o pode fazer parte deste relat�rio.');
        return;
    }
    
	fgClientes.Redraw = 2;
	
	var lineData = dsoGridClientes.recordset["ID"].value + '\t' +
		dsoGridClientes.recordset["DocFed"].value + '\t' +
		dsoGridClientes.recordset["Nome"].value + '\t' +
		dsoGridClientes.recordset["Fantasia"].value + '\t';
	
    fgClientes.AddItem(lineData);
	    
    fgClientes.AutoSizeMode = 0;
    fgClientes.AutoSize(0,fgClientes.Cols-1);
    
    fgClientes.Redraw = 2;

    fgClientes.Row = fgClientes.Rows-1;

	btnRemover_Status();
	btnOK_Status();
}

/********************************************************************
Remove a linha corrente do grid.
********************************************************************/
function btnRemover_Clicked()
{
	var currentLine = fgClientes.Row;
	
	if(currentLine == 0)
	{
		return;
	}
	
	fgClientes.RemoveItem(currentLine);
	
	if(fgClientes.Rows == 2)
	{
		fgClientes.Row = 1;
	}
	else if(fgClientes.Rows > 2)
	{
		if(fgClientes.Rows == currentLine)
		{
			fgClientes.Row = currentLine - 1;
		}
		else
		{
			fgClientes.Row = currentLine;
		}
	}
	
	btnRemover_Status();
	btnOK_Status();
}
/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Ajusta os elementos labels e checkbox do divEtiquetasMalaDireta
********************************************************************/
function adjustDivEtiquetasMalaDireta()
{
    var i;
    var elem;
    var hGap = 0;
    var labelLeft;
    var labelTop;
    var labelWidth;
    var labelFloor;
    var lineY = 0;
    
    // lblEtiquetaVertical
    elem = lblEtiquetaVertical;
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = hGap;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtEtiquetaVertical
    elem = txtEtiquetaVertical;
    with (elem.style)
    {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblEtiquetaHorizontal
    elem = lblEtiquetaHorizontal;
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtEtiquetaHorizontal
    elem = txtEtiquetaHorizontal;
    with (elem.style)
    {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblAltura
    elem = lblAltura;
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtAltura
    elem = txtAltura;
    with (elem.style)
    {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblLargura
    elem = lblLargura;
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtLargura
    elem = txtLargura;
    with (elem.style)
    {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblMargemSuperior
    elem = lblMargemSuperior;
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtMargemSuperior
    elem = txtMargemSuperior;
    with (elem.style)
    {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblMargemEsquerda
    elem = lblMargemEsquerda;
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = lineY;
        width = 4 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }

    // txtMargemEsquerda
    elem = txtMargemEsquerda;
    with (elem.style)
    {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    hGap = 0;
    lineY = parseInt(txtMargemEsquerda.currentStyle.top, 10) +
            parseInt(txtMargemEsquerda.currentStyle.height, 10) +
            ELEM_GAP;

    // lblCEPInicio
    elem = lblCEPInicio;
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = hGap;
        top = lineY;
        width = 9 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
        labelTop = parseInt(top, 10);
    }
    
    // txtCEPInicio
    elem = txtCEPInicio;
    with (elem.style)
    {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // lblCEPFim
    elem = lblCEPFim;
    with (elem.style)
    {
        backgroundColor = 'transparent';
        left = hGap + ELEM_GAP;
        top = labelTop;
        width = 9 * FONT_WIDTH;
        height = 16;
        hGap = parseInt(left, 10) + parseInt(width, 10);
        labelLeft = parseInt(left, 10);
        labelWidth = parseInt(width, 10);
        labelFloor = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // txtCEPFim
    elem = txtCEPFim;
    with (elem.style)
    {
        left = labelLeft;
        top = labelFloor;
        width = labelWidth;
        height = 24;
    }

    // ajusta e alinha o label lblFiltro e o campo texto txtFiltro
	adjustPesqElements(lblCEPInicio, txtCEPInicio, lblFiltro, txtFiltro);
	
	// valores Default nos campos
    txtEtiquetaVertical.value = 10;
    txtEtiquetaHorizontal.value = 3;
    txtAltura.value = 6;
    txtLargura.value = 70;
    txtMargemSuperior.value = 2;
    txtMargemEsquerda.value = 4;
}

/********************************************************************
Ajusta os elementos labels e checkbox do divExtratoCliente
********************************************************************/
function adjustDivExtratoCliente()
{
    txtDataInicio.maxLength = 10;
    txtDataFim.maxLength = 10;

    adjustElementsInForm([['lblEmpresa','chkEmpresa',3,1,-10],
						  ['lblDados','chkDados',3,1],
                          ['lblPedidos','chkPedidos',3,1],
                          ['lblProdutos','chkProdutos',3,1],
                          ['lblFinanceiros','chkFinanceiros',3,1],
                          ['lblObservacoes','chkObservacoes',3,1],
                          ['lblDataInicio','txtDataInicio',13,2,-8,0],
                          ['lblDataFim', 'txtDataFim', 13, 2, -8, 0],
                          ['lblFormatoSolicitacao', 'selFormatoSolicitacao', 10, 2, -8, 0]], null, null, true);
                          
    chkFinanceiros.checked = true;
    chkObservacoes.checked = true;
}

/********************************************************************
Ajusta os elementos labels e checkbox do divExtratoClientePL
********************************************************************/
function adjustDivExtratoClientePL()
{
    txtDataInicio.maxLength = 10;
    txtDataFim.maxLength = 10;

    adjustElementsInForm([['lblEmpresaPL','chkEmpresaPL',3,1,-10],
						  ['lblDadosPL','chkDadosPL',3,1],
                          ['lblPedidosPL','chkPedidosPL',3,1],
                          ['lblProdutosPL','chkProdutosPL',3,1],
                          ['lblFinanceirosPL','chkFinanceirosPL',3,1],
                          ['lblObservacoesPL','chkObservacoesPL',3,1],
                          ['lblDataInicioPL','txtDataInicioPL',13,2,-8,0],
                          ['lblDataFimPL','txtDataFimPL',13,2,-8,0],
                          ['lblChave', 'selChave',14,3,-9,0],
                          ['lblValorChave', 'txtValorChave',14,3],
                          ['btnInserir','btn',48,3],
                          ['btnRemover','btn',48,3,-288,180 + ELEM_GAP]],null,null,true);

	btnRemover.style.width = btnOK.currentStyle.width;
	                          
    chkFinanceirosPL.checked = true;
    chkObservacoesPL.checked = true;
    
	with (divGridClientes.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		
		left = ELEM_GAP;
		top = parseInt(txtValorChave.currentStyle.top, 10) + parseInt(txtValorChave.currentStyle.height, 10) + ELEM_GAP;
		width = divExtratoClientePL.currentStyle.width;
		height = 142;
	}
    
	with(fgClientes.style)
	{
		left = 0;
		top = parseInt(txtValorChave.currentStyle.top, 10) + parseInt(txtValorChave.currentStyle.height, 10) + ELEM_GAP;
		width = divGridClientes.currentStyle.width;
		height = divGridClientes.currentStyle.height;
	}
	
	startGridClientes();
}

function startGridClientes()
{
    startGridInterface(fgClientes);
	fgClientes.Redraw = 0;

	fgClientes.Editable = false;

	fgClientes.FontSize = '8';
	fgClientes.FrozenCols = 0;
	headerGrid(fgClientes,['ID','Doc. Federal','Nome','Fantasia'], []);
	
	fgClientes.AutoSizeMode = 0;

	fgClientes.Redraw = 2;
}

/********************************************************************
Ajusta os elementos labels e checkbox do divExtratoClientePL
********************************************************************/
function adjustDivSeguroDeCredito()
{
    txtDataSolicitacaoInicio.maxLength  = 10;
    txtDataSolicitacaoInicio.onkeypress = verifyDateTimeNotLinked;
    
    txtDataSolicitacaoFim.maxLength     = 10;
    txtDataSolicitacaoFim.onkeypress = verifyDateTimeNotLinked;
    
    txtDataCancelamentoInicio.maxLength = 10;
    txtDataCancelamentoInicio.onkeypress = verifyDateTimeNotLinked;
    
    txtDataCancelamentoFim.maxLength    = 10;
    txtDataCancelamentoFim.onkeypress = verifyDateTimeNotLinked;

    txtValorSolicitadoMinimo.onkeypress = verifyNumericEnterNotLinked;     
    //txtValorSolicitadoMinimo.onkeyup = txtPesquisa_onKeyPress;   
	txtValorSolicitadoMinimo.setAttribute('verifyNumPaste', 1);
	txtValorSolicitadoMinimo.setAttribute('thePrecision', 11, 1);
	txtValorSolicitadoMinimo.setAttribute('theScale', 2, 1);
	txtValorSolicitadoMinimo.setAttribute('minMax', new Array(0, 999999999.99), 1);
	//txtValorSolicitadoMinimo.onfocus = selFieldContent;
    txtValorSolicitadoMinimo.maxLength = 11;

    txtValorSolicitadoMaximo.onkeypress = verifyNumericEnterNotLinked;
    //txtValorSolicitadoMaximo.onkeyup = txtPesquisa_onKeyPress;
	txtValorSolicitadoMaximo.setAttribute('verifyNumPaste', 1);
	txtValorSolicitadoMaximo.setAttribute('thePrecision', 11, 1);
	txtValorSolicitadoMaximo.setAttribute('theScale', 2, 1);
	txtValorSolicitadoMaximo.setAttribute('minMax', new Array(0, 999999999.99), 1);
	//txtValorSolicitadoMaximo.onfocus = selFieldContent;
    txtValorSolicitadoMaximo.maxLength = 11;

    txtValorAprovadoMinimo.onkeypress = verifyNumericEnterNotLinked;
    //txtValorAprovadoMinimo.onkeyup = txtPesquisa_onKeyPress;
	txtValorAprovadoMinimo.setAttribute('verifyNumPaste', 1);
	txtValorAprovadoMinimo.setAttribute('thePrecision', 11, 1);
	txtValorAprovadoMinimo.setAttribute('theScale', 2, 1);
	txtValorAprovadoMinimo.setAttribute('minMax', new Array(0, 999999999.99), 1);
	//txtValorAprovadoMinimo.onfocus = selFieldContent;
    txtValorAprovadoMinimo.maxLength = 11;

    txtValorAprovadoMaximo.onkeypress = verifyNumericEnterNotLinked;
    //txtValorAprovadoMaximo.onkeyup = txtPesquisa_onKeyPress;
	txtValorAprovadoMaximo.setAttribute('verifyNumPaste', 1);
	txtValorAprovadoMaximo.setAttribute('thePrecision', 11, 1);
	txtValorAprovadoMaximo.setAttribute('theScale', 2, 1);
	txtValorAprovadoMaximo.setAttribute('minMax', new Array(0, 999999999.99), 1);
	//txtValorAprovadoMaximo.onfocus = selFieldContent;
    txtValorAprovadoMaximo.maxLength = 11;

    adjustElementsInForm([['lblSeguradora', 'selSeguradora', 20,1],
						  ['lblMarca', 'selMarca', 20,1],
						  ['lblValorSolicitadoMinimo','txtValorSolicitadoMinimo',13,2],
						  ['lblValorSolicitadoMaximo','txtValorSolicitadoMaximo',13,2,-10,0],
						  ['lblValorAprovadoMinimo','txtValorAprovadoMinimo',13,3],
						  ['lblValorAprovadoMaximo','txtValorAprovadoMaximo',13,3],
						  
						  ['lblDataSolicitacaoInicio','txtDataSolicitacaoInicio',10,4,0,5],
						  ['lblDataSolicitacaoFim','txtDataSolicitacaoFim',10,4,-10,0],   
						  ['lblDataVencimentoInicio','txtDataVencimentoInicio',10,5],
						  ['lblDataVencimentoFim','txtDataVencimentoFim',10,5],
						  ['lblDataCancelamentoInicio','txtDataCancelamentoInicio',10,6],
						  ['lblDataCancelamentoFim','txtDataCancelamentoFim',10,6,-10,0],
						  
						  ['lblAprovacao', 'selAprovacao', 10,7]],null,null,true);
}

/********************************************************************
Ajusta os elementos labels e checkbox do divExtratoCliente
********************************************************************/
function adjustDivExtratoFinanceiro()
{
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    
    setConnection(dsoEmpresas);

    dsoEmpresas.SQL = 'SELECT DISTINCT d.PessoaID AS fldID, d.Fantasia AS fldName ' +
				    'FROM	RelacoesPesRec a WITH(NOLOCK) ' +
					    'INNER JOIN RelacoesPesRec_Perfis b WITH(NOLOCK) ON a.RelacaoID = b.RelacaoID ' +
					    'INNER JOIN Recursos_Direitos c WITH(NOLOCK) ON b.PerfilID = c.PerfilID ' +
					    'INNER JOIN Pessoas d WITH(NOLOCK) ON b.EmpresaID = d.PessoaID ' +
					    'INNER JOIN RelacoesPesRec e WITH(NOLOCK) ON b.EmpresaID = e.SujeitoID ' +  
				    'WHERE (a.SujeitoID =  ' + nUserID + ' AND a.TipoRelacaoID = 11 AND a.ObjetoID = 999 AND e.EstaEmOperacao=1 AND c.RecursoID = 40103 ' + 
					    'AND c.RecursoMaeID = 20140 AND c.ContextoID = 1221 AND (c.Alterar1 = 1 OR c.Alterar2 = 1) AND e.TipoRelacaoID = 12 ' + 
					    'AND e.ObjetoID = 999) ' +   
				    'ORDER BY fldName '; 

    dsoEmpresas.ondatasetcomplete = adjustDivExtratoFinanceiro_DSC;
    dsoEmpresas.refresh();	  
 
}
/********************************************************************
Ajusta os elementos labels e checkbox do divExtratoCliente
********************************************************************/
function adjustDivExtratoFinanceiro_DSC()
{
    var optionStr = '';
    var optionValue = '';

    clearComboEx(['selEmpresas']);
	
    while (! dsoEmpresas.recordset.EOF )
    {
        optionStr = dsoEmpresas.recordset['fldName'].value;
		optionValue = dsoEmpresas.recordset['fldID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selEmpresas.add(oOption);
        dsoEmpresas.recordset.MoveNext();
    }

    txtDataInicio2.maxLength = 10;
    txtDataInicio2.onkeypress = verifyDateTimeNotLinked;        	
    txtDataFim2.maxLength = 10;
    txtDataFim2.onkeypress = verifyDateTimeNotLinked;        	
    
    txtDataInicio2.value = glb_dCurrDate;
    txtDataFim2.value = glb_dCurrDate;

    adjustElementsInForm([['lblEmpresas','selEmpresas',14,1,-10],						  
                          ['lblDataInicio2','txtDataInicio2',10,1],
                          ['lblDataFim2','txtDataFim2',10,1]],null,null,true);
                          
    selEmpresas.style.height = 70;               
}

/********************************************************************
Ajusta os elementos labels e checkbox do DivProgramasMarketing
********************************************************************/
function adjustDivProgramasMarketing()
{
    adjustElementsInForm([['lblProgramasMarketing','selProgramasMarketing',20,1,-10,-10],
						  ['lblFiltro2','txtFiltro2',30,2,-10]],null,null,true);

    setConnection(dsoCmbs01);

    dsoCmbs01.SQL = 'SELECT 0 AS Indice, 0 AS fldID, ' + '\'' + 'TODOS' + '\'' + ' AS fldName ' +
					'UNION ALL ' +
					'SELECT 1 AS Indice, PessoaID AS fldID, Fantasia AS fldName ' +
					'FROM Pessoas WITH(NOLOCK) ' +
					'WHERE (TipoPessoaID=55 AND EstadoID=2) ' +
					'ORDER BY Indice, fldName';

    dsoCmbs01.ondatasetcomplete = adjustDivProgramasMarketing_DSC;
    dsoCmbs01.Refresh();
}

function adjustDivProgramasMarketing_DSC()
{
    var i;
    var aDSOs = [dsoCmbs01];
    var aCmbs = [selProgramasMarketing];
    clearComboEx(['selProgramasMarketing']);
    
    for (i=0; i<aCmbs.length; i++)
    {
        while (! aDSOs[i].recordset.EOF)
        {
            var oOption = document.createElement("OPTION");
            oOption.text = aDSOs[i].recordset['fldName'].value;
            oOption.value = aDSOs[i].recordset['fldID'].value;

            aCmbs[i].add(oOption);
            aDSOs[i].recordset.MoveNext();
        }
    }
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs()
{
 
    txtEtiquetaVertical.onkeypress = verifyNumericEnterNotLinked;
    txtEtiquetaHorizontal.onkeypress = verifyNumericEnterNotLinked;
    txtAltura.onkeypress = verifyNumericEnterNotLinked;
    txtLargura.onkeypress = verifyNumericEnterNotLinked;
    txtMargemSuperior.onkeypress = verifyNumericEnterNotLinked;
    txtMargemEsquerda.onkeypress = verifyNumericEnterNotLinked;
    txtCEPInicio.onkeypress = verifyNumericEnterNotLinked;
    txtCEPFim.onkeypress = verifyNumericEnterNotLinked;

    txtEtiquetaVertical.setAttribute('verifyNumPaste', 1);
    txtEtiquetaHorizontal.setAttribute('verifyNumPaste', 1);
    txtAltura.setAttribute('verifyNumPaste', 1);
    txtLargura.setAttribute('verifyNumPaste', 1);
    txtMargemSuperior.setAttribute('verifyNumPaste', 1);
    txtMargemEsquerda.setAttribute('verifyNumPaste', 1);
    txtCEPInicio.setAttribute('verifyNumPaste', 1);
    txtCEPFim.setAttribute('verifyNumPaste', 1);

    txtEtiquetaVertical.setAttribute('thePrecision', 2, 1);
    txtEtiquetaHorizontal.setAttribute('thePrecision', 2, 1);
    txtAltura.setAttribute('thePrecision', 2, 1);
    txtLargura.setAttribute('thePrecision', 3, 1);
    txtMargemSuperior.setAttribute('thePrecision', 2, 1);
    txtMargemEsquerda.setAttribute('thePrecision', 2, 1);
    txtCEPInicio.setAttribute('thePrecision', 9, 1);
    txtCEPFim.setAttribute('thePrecision', 9, 1);
    
    txtEtiquetaVertical.setAttribute('theScale', 0, 1);
    txtEtiquetaHorizontal.setAttribute('theScale', 0, 1);
    txtAltura.setAttribute('theScale', 0, 1);
    txtLargura.setAttribute('theScale', 0, 1);
    txtMargemSuperior.setAttribute('theScale', 0, 1);
    txtMargemEsquerda.setAttribute('theScale', 0, 1);
    txtCEPInicio.setAttribute('theScale', 0, 1);
    txtCEPFim.setAttribute('theScale', 0, 1);

    txtEtiquetaVertical.setAttribute('minMax', new Array(0, 99), 1);
    txtEtiquetaHorizontal.setAttribute('minMax', new Array(0, 99), 1);
    txtAltura.setAttribute('minMax', new Array(0, 99), 1);
    txtLargura.setAttribute('minMax', new Array(0, 999), 1);
    txtMargemSuperior.setAttribute('minMax', new Array(0, 99), 1);
    txtMargemEsquerda.setAttribute('minMax', new Array(0, 99), 1);
    txtCEPInicio.setAttribute('minMax', new Array(0, 999999999), 1);
    txtCEPFim.setAttribute('minMax', new Array(0, 999999999), 1);
    
    txtEtiquetaVertical.onfocus = selFieldContent;
    txtEtiquetaHorizontal.onfocus = selFieldContent;
    txtAltura.onfocus = selFieldContent;
    txtLargura.onfocus = selFieldContent;
    txtMargemSuperior.onfocus = selFieldContent;
    txtMargemEsquerda.onfocus = selFieldContent;
    txtCEPInicio.onfocus = selFieldContent;
    txtCEPFim.onfocus = selFieldContent;
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
    if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}

function oPrinter_OnPrintFinish()
{
    winModalPrint_Timer = window.setInterval('restoreModalPrint()', 10, 'JavaScript');
}

/********************************************************************
Criado pelo programador
Impressao de Etiqueta de Mala Direta

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function etiquetaMalaDireta()
{
    var sCEPInicio = trimStr(txtCEPInicio.value);
    var sCEPFim = trimStr(txtCEPFim.value);
    var sFiltro = '';

    sFiltro = trimStr(txtFiltro.value);
    if (sFiltro!= '')
        sFiltro = ' AND (' + sFiltro;

    if(sCEPInicio!='')
    {
        sFiltro += ( sFiltro == '' ? ' AND (' : ' AND ' );
        sFiltro += 'Enderecos.CEP>= '  + '\'' + sCEPInicio + '\'';
    }    
        
    if(sCEPFim!='')
    {
        sFiltro += ( sFiltro == '' ? ' AND (' : ' AND ' );
        sFiltro += 'Enderecos.CEP<= ' + '\'' + sCEPFim + '\'';
    }
    
    sFiltro += ( sFiltro == '' ? '' : ') ' );
    
    setConnection(dsoPrint01);

    dsoPrint01.SQL = 
        'SELECT ' +
           'COUNT (*) AS NumeroEtiquetas ' +
        'FROM RelacoesPessoas RelPessoas WITH(NOLOCK) ' +
           'INNER JOIN RelacoesPessoas_Contatos RelPessoasContatos WITH(NOLOCK) ON (RelPessoas.RelacaoID=RelPessoasContatos.RelacaoID) ' +
           'INNER JOIN Pessoas Contatos WITH(NOLOCK) ON (RelPessoasContatos.ContatoID=Contatos.PessoaID) ' +
           'INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (RelPessoas.SujeitoID=Clientes.PessoaID) ' +
           'INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Clientes.PessoaID=Enderecos.PessoaID) ' +
           'INNER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) ' +
           'LEFT OUTER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID=UFs.LocalidadeID) ' +
           'LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) ' +
        'WHERE ' +
           'RelPessoas.ObjetoID= ' + glb_nEmpresaID + ' AND RelPessoas.TipoRelacaoID=21 AND RelPessoas.EstadoID = 2 AND ' +
           'Contatos.EstadoID = 2 AND ' +
           'RelPessoasContatos.Mailing=1 AND Clientes.EstadoID = 2 AND Enderecos.Ordem=1 ' +
           ' ' + sFiltro + ' ';

		if (sFiltro.indexOf('RelPessoasContatos') == -1)
		{
			dsoPrint01.SQL = dsoPrint01.SQL + 'UNION ALL ' +
			'SELECT ' +
			   'COUNT(*) AS NumeroEtiquetas ' +
			'FROM ' +
			   'RelacoesPessoas RelPessoas WITH(NOLOCK) ' +
			   'INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (RelPessoas.SujeitoID=Clientes.PessoaID) ' +
			   'INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Clientes.PessoaID=Enderecos.PessoaID) ' +
			   'INNER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) ' +
			   'LEFT OUTER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID=UFs.LocalidadeID) ' +
			   'LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) ' +
			'WHERE ' +
			   'RelPessoas.ObjetoID= ' + glb_nEmpresaID + ' AND RelPessoas.TipoRelacaoID=21 AND RelPessoas.EstadoID = 2 AND RelPessoas.Mailing=1 AND ' +
			   'Clientes.EstadoID = 2 AND Enderecos.Ordem=1 ' + sFiltro;
		}
		else
			dsoPrint01.SQL = dsoPrint01.SQL + 'UNION ALL SELECT 0 AS NumeroEtiquetas ';
		
    dsoPrint01.ondatasetcomplete = etiquetaMalaDireta_DSC;
    dsoPrint01.Refresh();
}

function etiquetaMalaDireta_DSC()
{
    var nNumeroEtiquetas=0;
    
    if (!dsoPrint01.recordset.EOF)
    {
        nNumeroEtiquetas = dsoPrint01.recordset['NumeroEtiquetas'].value;
        dsoPrint01.recordset.MoveNext();
        nNumeroEtiquetas += dsoPrint01.recordset['NumeroEtiquetas'].value;
            
        if (nNumeroEtiquetas == 0)
        {
            if ( window.top.overflyGen.Alert ('Nemhuma etiqueta a ser impressa') == 0 )
                return null;
                
            pb_StopProgressBar(true);
            lockControlsInModalWin(false);
            return true;                        
        }
        else
        {
            var _retMsg = window.top.overflyGen.Confirm('Imprimir' + ' ' + nNumeroEtiquetas + ' etiquetas?');
    
            if ( _retMsg == 0 )
            {
				pb_StopProgressBar(true);
                return null;
            }    
            else if ( _retMsg == 1 )
                __timerInt = window.setInterval('imprimeEtiquetaMalaDireta()', 50, 'JavaScript');
            else
            {
				pb_StopProgressBar(true);
                lockControlsInModalWin(false);
                return true;                        
            }
        }
    }
    else 
    {
        if ( window.top.overflyGen.Alert ('Nemhuma etiqueta a ser impressa') == 0 )
            return null;
		
		pb_StopProgressBar(true);            
            
        lockControlsInModalWin(false);
        return true;                        
    }
}

function imprimeEtiquetaMalaDireta()
{
    if ( __timerInt != null )
    {
        window.clearInterval(__timerInt);
        __timerInt = null;
    }    

    var sCEPInicio = trimStr(txtCEPInicio.value);
    var sCEPFim = trimStr(txtCEPFim.value);
    var sFiltro = '';

    sFiltro = trimStr(txtFiltro.value);
    if (sFiltro!= '')
        sFiltro = ' AND (' + sFiltro;

    if(sCEPInicio!='')
    {
        sFiltro += ( sFiltro == '' ? ' AND (' : ' AND ' );
        sFiltro += 'Enderecos.CEP>= '  + '\'' + sCEPInicio + '\'';
    }    
        
    if(sCEPFim!='')
    {
        sFiltro += ( sFiltro == '' ? ' AND (' : ' AND ' );
        sFiltro += 'Enderecos.CEP<= ' + '\'' + sCEPFim + '\'';
    }
    
    sFiltro += ( sFiltro == '' ? '' : ') ' );
        
    setConnection(dsoPrint01);
	dsoPrint01.SQL = 
		'SELECT ' +
			'Contatos.Nome AS Contato, UPPER(Clientes.Nome) AS Pessoa, ' +
			'(CASE ISNULL(Paises.EnderecoInvertido, 0) WHEN 1 THEN ' +
			'(ISNULL(Enderecos.Numero + SPACE(1),SPACE(0)) + Enderecos.Endereco) ' +
			'ELSE (Enderecos.Endereco + ISNULL(SPACE(1) + Enderecos.Numero,SPACE(0))) END) AS Endereco, ' +
			'ISNULL(Enderecos.Complemento,SPACE(0)) AS Complemento,Enderecos.Bairro AS Bairro, ISNULL(Enderecos.CEP,SPACE(0)) AS CEP, ' +
			'ISNULL(Cidades.Localidade,SPACE(0)) AS Cidade, UFs.CodigoLocalidade2 AS UF ' +
		'FROM ' +
			'RelacoesPessoas RelPessoas WITH(NOLOCK) ' +
			'INNER JOIN RelacoesPessoas_Contatos RelPessoasContatos WITH(NOLOCK) ON (RelPessoas.RelacaoID=RelPessoasContatos.RelacaoID) ' +
			'INNER JOIN Pessoas Contatos WITH(NOLOCK) ON (RelPessoasContatos.ContatoID=Contatos.PessoaID) ' +
			'INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (RelPessoas.SujeitoID=Clientes.PessoaID) ' +
			'INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Clientes.PessoaID=Enderecos.PessoaID) ' +
			'INNER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) ' +
			'LEFT OUTER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID=UFs.LocalidadeID) ' +
			'LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) ' +
		'WHERE ' +
			'RelPessoas.ObjetoID= ' + glb_nEmpresaID + ' AND RelPessoas.TipoRelacaoID=21 AND RelPessoas.EstadoID = 2 AND ' +
			'Contatos.EstadoID = 2 AND Clientes.EstadoID = 2 AND RelPessoasContatos.Mailing=1 AND ' +
			'Enderecos.Ordem=1 ' + sFiltro + ' ';
	        
	if (sFiltro.indexOf('RelPessoasContatos') == -1)
	{
		dsoPrint01.SQL = dsoPrint01.SQL + 'UNION ALL ' +
		'SELECT ' +
			'SPACE(0) AS Contato, UPPER(Clientes.Nome) AS Pessoa, ' +
			'(CASE ISNULL(Paises.EnderecoInvertido, 0) WHEN 1 THEN ' +
			'(ISNULL(Enderecos.Numero + SPACE(1),SPACE(0)) + Enderecos.Endereco) ' +
			'ELSE (Enderecos.Endereco + ISNULL(SPACE(1) + Enderecos.Numero,SPACE(0))) END) AS Endereco, ' +
			'ISNULL(Enderecos.Complemento,SPACE(0)) AS Complemento, ISNULL(Enderecos.Bairro,SPACE(0)) AS Bairro, ' +
			'ISNULL(Enderecos.CEP,SPACE(0)) AS CEP, ISNULL(Cidades.Localidade,SPACE(0)) AS Cidade, UFs.CodigoLocalidade2 AS UF ' +
		'FROM ' +
			'RelacoesPessoas RelPessoas WITH(NOLOCK) ' +
			'INNER JOIN Pessoas Clientes WITH(NOLOCK) ON (RelPessoas.SujeitoID=Clientes.PessoaID) ' +
			'INNER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Clientes.PessoaID=Enderecos.PessoaID) ' +
			'INNER JOIN Localidades Cidades WITH(NOLOCK) ON (Enderecos.CidadeID=Cidades.LocalidadeID) ' +
			'LEFT OUTER JOIN Localidades UFs WITH(NOLOCK) ON (Enderecos.UFID=UFs.LocalidadeID) ' +
			'LEFT OUTER JOIN Localidades Paises WITH(NOLOCK) ON (Enderecos.PaisID=Paises.LocalidadeID) ' +
		'WHERE RelPessoas.ObjetoID= ' + glb_nEmpresaID + ' AND RelPessoas.TipoRelacaoID=21 AND RelPessoas.EstadoID = 2 AND RelPessoas.Mailing=1 AND ' +
			'Clientes.EstadoID = 2 AND Enderecos.Ordem=1 ' + sFiltro + ' ' +
		'ORDER BY CEP';
	}

    dsoPrint01.ondatasetcomplete = imprimeEtiquetaMalaDireta_DSC;
    dsoPrint01.Refresh();
}

function imprimeEtiquetaMalaDireta_DSC()
{
    var nEtiquetasVerticais = 10;
    var nEtiquetasHorizontais = 3; 
    var nAltura = 6;   // numero de linhas possiveis da etiqueta (cpp)
    var nLargura = 70; // largura da etiqueta (mm)
    var nLinhas = 5;   // numero de linhas utilizaveis da etiqueta
    var nMargemSuperior = 2;  // numero de linha a pular na primeira etiqueta
    var nMargemEsquerda = 4;  // distancia da margem esquerda (mm)
    var nAjuste;  // ajusta a linha de impressao
    var nLineBold = -1;


    if(txtEtiquetaVertical.value!='')
        nEtiquetasVerticais = parseInt(txtEtiquetaVertical.value,10);
    if(txtEtiquetaHorizontal.value!='')
        nEtiquetasHorizontais = parseInt(txtEtiquetaHorizontal.value,10);
    if(txtAltura.value!='')
        nAltura = parseInt(txtAltura.value,10);
    if(txtLargura.value!='')
        nLargura = parseInt(txtLargura.value,10);
    if(txtMargemSuperior.value!='')
        nMargemSuperior = parseInt(txtMargemSuperior.value,10);
    if(txtEtiquetaVertical.value!='')
        nMargemEsquerda = parseInt(txtMargemEsquerda.value,10);

    var aCelula = new Array();
    var i,j,k,m,n,p;
    
    for (i=0;i<nLinhas;i++)
        aCelula[i] = new Array();

    var sComplementoBairro;
    var bEstouro;

    // seta os parametros de inicializacao da impressora
    oPrinter.DefaultPrinterNFParams(6,70,10,'C',false);
    oPrinter.ParallelPort = 1;
    oPrinter.ResetPrinter();
    oPrinter.ResetTextForPrint();
    
    dsoPrint01.recordset.MoveFirst();

    i=0;
    while (! dsoPrint01.recordset.EOF)
    {
        oPrinter.MoveToNewLine(nMargemSuperior);
        j=0;
        while ((! dsoPrint01.recordset.EOF) && (j<nEtiquetasVerticais))
        {
            k=0;
            while ((!dsoPrint01.recordset.EOF) && (k<nEtiquetasHorizontais))
            {
                aCelula[3][k]='';
                aCelula[4][k]='';
                nAjuste=0;
                
                if (dsoPrint01.recordset['Contato'].value !='')
                {
                    aCelula[0][k]=trimCarriageReturnAtEnd(dsoPrint01.recordset['Contato'].value);
                    nLineBold = 1;
                }
                else
                {
                    nAjuste++;
                    nLineBold = -1;
                }
                
				aCelula[1-nAjuste][k]=trimCarriageReturnAtEnd(dsoPrint01.recordset['Pessoa'].value);
                aCelula[2-nAjuste][k]=(trimCarriageReturnAtEnd(dsoPrint01.recordset['Endereco'].value)).substr(0,44);
                sComplementoBairro=trimStr((trimCarriageReturnAtEnd(dsoPrint01.recordset['Complemento'].value))+' '+(trimCarriageReturnAtEnd(dsoPrint01.recordset['Bairro'].value)));
                
                if (sComplementoBairro!='')
                    aCelula[3-nAjuste][k]=sComplementoBairro;
                else
                    nAjuste++;
                
                aCelula[4-nAjuste][k]=(trimCarriageReturnAtEnd(dsoPrint01.recordset['CEP'].value))+' '+(dsoPrint01.recordset['Cidade'].value)+' '+(trimCarriageReturnAtEnd(dsoPrint01.recordset['UF'].value));

                // Trap para campos com mais de 40 caracteres
                bEstouro=false;
                if (k==2)
                {
                    for(p=0;p<nLinhas;p++)
                    {
                        if(aCelula[p][k].length>40)
                        {
                            bEstouro=true;
                            break;
                        }
                    }               
                }
                if (bEstouro)
                    break;
                else
                {
                    dsoPrint01.recordset.MoveNext();
                    k++;
                    i++;
                }
            }

            // Impressao dos Dados
            for(m=0;m<nLinhas;m++)
            {
                for(n=0;n<k;n++)
                    oPrinter.SendTextForPrint(nMargemEsquerda + n*nLargura, aCelula[m][n],'C',(m==nLineBold));

                oPrinter.MoveToNewLine(1);
            }
            oPrinter.MoveToNewLine(nAltura-m);
            j++;
            if (dsoPrint01.recordset.EOF)
            {
                oPrinter.MoveToNewLine(2);
                oPrinter.SendTextForPrint(nMargemEsquerda, '***  ' + i + ' etiquetas impressas  ***','C',false);                
            }

        }
        oPrinter.FormFeed();
    }    

    pb_StopProgressBar(true);
    oPrinter.PrintText();
}
function extratoFinanceiro()
{
    var sEmpresas = '';    
    var sDtInicio = '';
    var sDtFim = '';    
    var sParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['SujeitoID'].value");    
    var sLinguaLogada = getDicCurrLang();
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    // var formato = selFormato.value;
    var formato = 2;
    
    for (i=0; i<selEmpresas.length; i++)
    {
        if ( selEmpresas.options[i].selected == true )
            sEmpresas += (sEmpresas == '' ? '/' : '') + selEmpresas.options[i].value + '/';            
    }    
    
    sEmpresas = (sEmpresas == '' ? 'NULL' : '\'' + sEmpresas + '\'');

    if (!chkDataEx(txtDataInicio2.value)) 
    {
        if ( window.top.overflyGen.Alert('Data de In�cio � Inv�lida') == 0 )
            return null;
        lockControlsInModalWin(false);
        return null;
    }    
    else if (!chkDataEx(txtDataFim2.value)) 
    {
        if ( window.top.overflyGen.Alert('Data de Fim � Inv�lida') == 0 )
            return null;
        lockControlsInModalWin(false);
        return null;
    }   	

    sDtInicio = (txtDataInicio2.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDataInicio2.value, true) + '\'');
    sDtFim = (txtDataFim2.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDataFim2.value, true) + '\'');
    
    sParceiroID = (sParceiroID == null ? 'NULL' : sParceiroID);

    var frameReport = document.getElementById("frmReport");
    
    
    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&sEmpresas=" + sEmpresas + "&sParceiroID=" + sParceiroID + "&sLinguaLogada=" + sLinguaLogada + "&sDtInicio=" + sDtInicio + "&sDtFim=" + sDtFim;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/serverside/Reports_relpessoas.aspx?' + strParameters;

    glbContRel = 1;
    
}

function extratoClientePL()
{
    // Recuperar os clientes selecionados do grid    
    var nClientes = '\';'; 

    // Recupera a lista de clientes selecionados pelo usu�rio
    for(linha = 1; linha < fgClientes.Rows; nClientes += fgClientes.TextMatrix(linha++, 0) + ';');
    nClientes += '\'';
    
    RelatorioDeExtratoDeCliente(nClientes);
}

function RelatorioDeExtratoDeCliente(nClientes)
{
    var nFornecedor;
    var nRelacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RelacaoID'].value");
	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	
	var nSujeitoEmpresaSistema = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['nSujeitoEmpresaSistema'].value");
	var nObjetoEmpresaSistema = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['nObjetoEmpresaSistema'].value");
	
	var sDtInicio = '\'' + txtDataInicio.value + '\'';
	var sDtFim = '\'' + txtDataFim.value + '\'';
	

    //DireitoEspecifico
    //Relacao Pesssoas->Clientes->Sup->Modal Print
    //20140 SFS Grupo-Relacao entre Pessoas -> Relatorio: 40101 Extrato de Cliente
    //A1||A2 -> Impressao
    //A2 -> Zebrar
    //A1&&A2 -> Excel, Word    

    // Monta as consultas para o relat�rio.    
    var bTodasEmpresas = '0';    
    
    if(glb_sCaller == 'PL')
    {
        bTodasEmpresas = (chkEmpresaPL.checked ? '0' : '1');
        nFornecedor = aEmpresaData[0]; //Empresa logada
    }
    else if(glb_sCaller == 'S')       
    {
        bTodasEmpresas = (chkEmpresa.checked ? '1' : '0');
       
        if ((nObjetoEmpresaSistema == 0) && (nSujeitoEmpresaSistema == 1))
        {
            nFornecedor = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value");
            nClientes = '\';' + sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value") + ';\'';   
        }       
        else if ((nObjetoEmpresaSistema == 0) && (nSujeitoEmpresaSistema == 0))
        {
            nFornecedor = aEmpresaData[0];
            nClientes = '\';' + sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value") + ';\'';
        }
        else
        {
            nFornecedor = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");
            nClientes = '\';' + sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value") + ';\'';
        }
    }

    strParameters = 'DATE_SQL_PARAM=' + DATE_SQL_PARAM + '&RelatorioID=' + selReports.value + '&nFornecedor=' + nFornecedor +
                    '&bTodasEmpresas=' + bTodasEmpresas + '&nClientes=' + nClientes + '&sDtInicio=' + sDtInicio + '&sDtFim=' + sDtFim +
                    '&glb_sEmpresaFantasia=' + glb_sEmpresaFantasia + '&glb_sCaller=' + glb_sCaller + '&chkPedidosPL=' + chkPedidosPL.checked +
                    '&chkPedidos=' + chkPedidos.checked + '&chkProdutosPL=' + chkProdutosPL.checked + '&chkProdutos=' + chkProdutos.checked +
                    '&chkFinanceirosPL=' + chkFinanceirosPL.checked + '&chkFinanceiros=' + chkFinanceiros.checked + '&chkObservacoesPL=' + chkObservacoesPL.checked +
                    '&chkObservacoes=' + chkObservacoes.checked +'&Formato=' + selFormatoSolicitacao.value;


    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/basico/relacoes/relpessoas/serverside/Reports_relpessoas.aspx?" + strParameters + "&via2=false";

    glbContRel = 1;

}

function seguroDeCredito() 
{
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

    if (txtDataSolicitacaoInicio.value != null && txtDataSolicitacaoInicio.value != '' && !chkDataEx(txtDataSolicitacaoInicio.value)) 
    {
        if ( window.top.overflyGen.Alert('Data inicial da solicita��o est� inv�lida') == 0 )
            return null;
        lockControlsInModalWin(false);
        return null;
    }    
    else if (txtDataSolicitacaoFim.value != null && txtDataSolicitacaoFim.value != '' && !chkDataEx(txtDataSolicitacaoFim.value)) 
    {
        if ( window.top.overflyGen.Alert('Data final da solicita��o est� inv�lida') == 0 )
            return null;
        lockControlsInModalWin(false);
        return null;
    }   	

    if (txtDataVencimentoInicio.value != null && txtDataVencimentoInicio.value != '' && !chkDataEx(txtDataVencimentoInicio.value)) 
    {
        if ( window.top.overflyGen.Alert('Data inicial de vencimento est� inv�lida') == 0 )
            return null;
        lockControlsInModalWin(false);
        return null;
    }    
    else if (txtDataVencimentoFim.value != null && txtDataVencimentoFim.value != '' && !chkDataEx(txtDataVencimentoFim.value)) 
    {
        if ( window.top.overflyGen.Alert('Data final de vencimento est� inv�lida') == 0 )
            return null;
        lockControlsInModalWin(false);
        return null;
    }   	

    if (txtDataCancelamentoInicio.value != null && txtDataCancelamentoInicio.value != '' && !chkDataEx(txtDataCancelamentoInicio.value)) 
    {
        if ( window.top.overflyGen.Alert('Data inicial do cancelamento est� inv�lida') == 0 )
            return null;
        lockControlsInModalWin(false);
        return null;
    }    
    else if (txtDataCancelamentoFim.value != null && txtDataCancelamentoFim.value != '' && !chkDataEx(txtDataCancelamentoFim.value)) 
    {
        if ( window.top.overflyGen.Alert('Data final do cancelamento est� inv�lida') == 0 )
            return null;
        lockControlsInModalWin(false);
        return null;
    }   	

    var seguradora            = (selSeguradora.value == '0' ? 'NULL' : selSeguradora.value);
    var marca                 = (selMarca.value == '0' ? 'NULL' : selMarca.value);
    var valorSolicitadoMinimo = (txtValorSolicitadoMinimo.value == '' ? 'NULL' : txtValorSolicitadoMinimo.value);
    var valorSolicitadoMaximo = (txtValorSolicitadoMaximo.value == '' ? 'NULL' : txtValorSolicitadoMaximo.value);
    var valorAprovadoMinimo   = (txtValorAprovadoMinimo.value == '' ? 'NULL' : txtValorAprovadoMinimo.value);
    var valorAprovadoMaximo   = (txtValorAprovadoMaximo.value == '' ? 'NULL' : txtValorAprovadoMaximo.value);

    var dataSolicitacaoInicio  = (txtDataSolicitacaoInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDataSolicitacaoInicio.value, true) + '\'');
    var dataSolicitacaoFim     = (txtDataSolicitacaoFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDataSolicitacaoFim.value, true) + '\'');
    var dataVencimentoInicio   = (txtDataVencimentoInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDataVencimentoInicio.value, true) + '\'');
    var dataVencimentoFim      = (txtDataVencimentoFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDataVencimentoFim.value, true) + '\'');
    var dataCancelamentoInicio = (txtDataCancelamentoInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDataCancelamentoInicio.value, true) + '\'');
    var dataCancelamentoFim    = (txtDataCancelamentoFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtDataCancelamentoFim.value, true) + '\'');


    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    // var formato = selFormato.value;
    var formato = 2;

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&seguradora=" + seguradora + "&marca=" + marca + "&valorSolicitadoMinimo=" + valorSolicitadoMinimo + "&valorSolicitadoMaximo=" + valorSolicitadoMaximo +
                        "&valorAprovadoMinimo=" + valorAprovadoMinimo + "&valorAprovadoMaximo=" + valorAprovadoMaximo + "&dataSolicitacaoInicio=" + dataSolicitacaoInicio + 
                        "&dataSolicitacaoFim=" + dataSolicitacaoFim + "&dataCancelamentoInicio=" + dataCancelamentoInicio + "&dataCancelamentoFim=" + dataCancelamentoFim + 
                        "&dataVencimentoInicio=" + dataVencimentoInicio + "&dataVencimentoFim=" + dataVencimentoFim + "&selAprovacao=" + selAprovacao.value +
                        '&DATE_SQL_PARAM=' + DATE_SQL_PARAM;


    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = SYS_PAGESURLROOT + "/basico/relacoes/relpessoas/serverside/Reports_relpessoas.aspx?" + strParameters + "&via2=false";

    glbContRel = 1;
       
}

function programasMarketing()
{
    var sProgramasMarketing = '';
    var sFiltro2 = '';
    var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    // var formato = selFormato.value;
    var formato = 2;
    
    sProgramasMarketing = (selProgramasMarketing.selectedIndex >= 1 ? sProgramasMarketing = selProgramasMarketing.value : sProgramasMarketing = 'NULL');
    
    sFiltro2 = (txtFiltro2.value == '' ? 'NULL' : txtFiltro2.value);

    /*
    var strSQL = 'SELECT RelPessoas.RelacaoID, Estados.RecursoAbreviado AS Estado, Pessoas.PessoaID, Pessoas.Fantasia, ' +
			'Programas.Fantasia AS Programa, RelPessoas.Identificador, RelPessoas.dtAdmissao, '+
			'Paises.CodigoLocalidade2 AS Pais, Enderecos.CEP, UFs.CodigoLocalidade2 AS UF, ' +
			'Cidades.Localidade AS Cidade, Enderecos.Bairro, Enderecos.Endereco, Enderecos.Numero, Enderecos.Complemento ' +
		'FROM RelacoesPessoas RelPessoas WITH(NOLOCK), Recursos Estados WITH(NOLOCK), Pessoas WITH(NOLOCK), ' +
			'Pessoas_Enderecos Enderecos WITH(NOLOCK), Localidades Paises WITH(NOLOCK), Localidades UFs WITH(NOLOCK), Localidades Cidades WITH(NOLOCK), Pessoas Programas WITH(NOLOCK) ' +
		'WHERE (RelPessoas.TipoRelacaoID = 36 AND RelPessoas.EstadoID <> 5 AND ' +
			'RelPessoas.EstadoID = Estados.RecursoID AND RelPessoas.SujeitoID = Pessoas.PessoaID AND ' +
			'Pessoas.PessoaID = Enderecos.PessoaID AND Enderecos.PaisID = Paises.LocalidadeID AND ' +
			'Enderecos.UFID = UFs.LocalidadeID AND Enderecos.CidadeID = Cidades.LocalidadeID AND ' +
			'RelPessoas.ObjetoID = Programas.PessoaID) ';
			
	if (selProgramasMarketing.selectedIndex > 1)
		strSQL += ' AND (Programas.PessoaID = ' + selProgramasMarketing.value + ') ';
		
	if (trimStr(txtFiltro2.value) != '')
		strSQL += ' AND (' + txtFiltro2.value + ') ';

	strSQL += 'ORDER BY Pessoas.Fantasia';
    */
    var strParameters = "RelatorioID=" + selReports.value + "&glb_nEmpresaID=" + aEmpresaData[0] + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia + "&Formato=" + formato +
                        "&sProgramasMarketing=" + sProgramasMarketing + "&sFiltro2=" + sFiltro2;

	pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
	lockControlsInModalWin(true);
	window.document.onreadystatechange = reports_onreadystatechange;
	window.document.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/serverside/Reports_relpessoas.aspx?' + strParameters;

}

function verificaData(sData)
{
	var sData = trimStr(sData);
	var bDataIsValid = true;

	if (sData != '')
		bDataIsValid = chkDataEx(sData);

	if ( !bDataIsValid )
	{
        if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
            return null;
        
        lockControlsInModalWin(false);
		return false;
	}
	return true;
}
