/********************************************************************
modalhistoricosfuncionais.js

Library javascript para o modalhistoricosfuncionais.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_TodosMarcados = false;
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_Nivel = null; 
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;

// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoCargos = new CDatatransport("dsoCargos");
var dsoColaborador = new CDatatransport("dsoColaborador");

var dsoDepartamentos = new CDatatransport("dsoDepartamentos");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
btnBarClicked(controlBar, btnClicked)
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalhistoricosfuncionaisBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalhistoricosfuncionaisDblClick(grid, Row, Col)
js_modalhistoricosfuncionaisKeyPress(KeyAscii)
js_modalhistoricosfuncionais_ValidateEdit()
js_modalhistoricosfuncionais_BeforeEdit(grid, row, col)
js_modalhistoricosfuncionais_AfterEdit(Row, Col)
js_fg_AfterRowColmodalhistoricosfuncionais (grid, OldRow, OldCol, NewRow, NewCol)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalhistoricosfuncionaisBody)
     {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
     }
     glb_nDSOs = 3;

    /*DSC Funcionarios*/
    setConnection(dsoDepartamentos);

    dsoDepartamentos.SQL = 'SELECT a.PessoaID AS DepartamentoID, a.Fantasia AS Departamento ' +
                           'FROM Pessoas a WITH(NOLOCK) ' +
                           'INNER JOIN Pessoas_Empresas b WITH(NOLOCK) ON a.PessoaID = b.PessoaID ' +
                           'WHERE a.TipoPessoaID=54 AND b.EmpresaID = ' + glb_nEmpresaID + ' and a.EstadoID = 2';

    dsoDepartamentos.ondatasetcomplete = dsoCargos_DSC;
    dsoDepartamentos.Refresh();

    setConnection(dsoCargos);

    dsoCargos.SQL = 'SELECT a.RecursoID AS CargoID, a.RecursoFantasia AS Cargo, b.NivelMaximo AS Nivel,' +
        '(SELECT MAX(aa.NivelMaximo) ' +
		'FROM Recursos_Empresas aa WITH(NOLOCK) ' +
		'WHERE aa.EmpresaID = b.EmpresaID) AS NivelMaximo ' +
		'FROM Recursos a WITH(NOLOCK) ' +
		'INNER JOIN Recursos_Empresas b WITH(NOLOCK) ' +
		'ON a.RecursoID = b.RecursoID AND b.EmpresaID = ' + glb_nEmpresaID + ' ' +
		'WHERE ((a.TipoRecursoID = 6) AND (a.EstadoID=2) AND (a.EhCargo=1)) ' +
		'ORDER BY b.CodigoProvedor ';

    dsoCargos.ondatasetcomplete = dsoCargos_DSC;
	dsoCargos.Refresh();

    setConnection(dsoColaborador);

	dsoColaborador.SQL = 'SELECT DISTINCT b.PessoaID, b.Fantasia ' +
		'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), Recursos c WITH(NOLOCK) ' +
		'WHERE ((a.TipoRelacaoID = 31) AND (a.EstadoID = 2) AND  ' +
		'(a.SujeitoID = b.PessoaID) AND (a.CargoID = c.RecursoID) AND ' +
		'(c.EhCargoSuperior = 1)) ' +
		'ORDER BY b.Fantasia';

    dsoColaborador.ondatasetcomplete = dsoCargos_DSC;
	dsoColaborador.Refresh();
}

function dsoCargos_DSC()
{
	glb_nDSOs--;

	if (glb_nDSOs > 0)
		return null;

    glb_bFirstFill = true;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

// EVENTOS DO PRINTJET **************************************


/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Hist�ricos Funcionais', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 1;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
		backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = btnOK.offsetLeft + btnOK.offsetWidth + ELEM_GAP;    
        height = 85;
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    adjustElementsInForm([['btnListar', 'btnListar', 60, 3, 721, -20],
                          ['btnOK', 'btnOK', 60, 3, 10, 0],
                          ['btnExcel', 'btnExcel', 60, 3, 10, 00],
                          ['lblModo', 'selModo', 12, 1, -10, -10],
                          ['lblColaborador', 'selColaborador', 20, 1],
	                      ['lblDepartamento', 'selDepartamento', 20, 1],
	                      ['lblCargo', 'selCargo', 20, 1],
	                      ['lblNivel', 'selNivel', 5, 1],
	                      ['lblInicio', 'txtInicio', 10, 2, -10],
	                      ['lblFim', 'txtFim', 10, 2],
	                      ['lblVigencia', 'txtVigencia', 10, 2]], null, null, true);

    lblVigencia.style.top = lblInicio.offsetTop;
    lblVigencia.style.left = lblInicio.offsetLeft;

    txtVigencia.style.top = txtInicio.offsetTop;
    txtVigencia.style.left = txtInicio.offsetLeft;

    selModo.onchange = selModo_onchange;
    selModo_onchange();

    selCargo.onchange = selCargo_onchange;
    selCargo_onchange();                   

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if ((controlID == 'btnListar') || (controlID == 'btnExcel')) {
        fillGridData(controlID);
    }
    // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
	showExtFrame(window, true);
	
	window.focus();

    // preenche o grid da janela modal
   // fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
	var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");

	if (selModo.value == 2) {
	    btnOK.disabled = ((!bHasRowsInGrid) || (nEstadoID == 66));
	}
}

function setTotalColumns()
{
	return null;
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nValue = 0;
    var nCounter = 0;
    
	if (bHasRowsInGrid)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
			{
				nValue += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
				nCounter++;
			}				
		}
	}
	
	fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = nValue;
	fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = nCounter;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(controlID) {
    
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
	}

	var empresaData = getCurrEmpresaData();
	var nLinguaLogada = getDicCurrLang();
    var sDadosAvaliacao = '';
	var aGrid = null;
	var i = 0;

	/*Selec�o dos filtros*/
	if ((trimStr(txtInicio.value) != "")&&(selModo.value==1)) {
	    if (chkDataEx(txtInicio.value) == false) {
	        window.top.overflyGen.Alert('Data inv�lida.');
	        return null;
	    }
	}

	if ((trimStr(txtFim.value) != "")&&(selModo.value==1)) {
	    if (chkDataEx(txtFim.value) == false) {
	        window.top.overflyGen.Alert('Data inv�lida.');
	        return null;
	    }
	}

	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
   
    var sDtVigencia = 'NULL';

    if ((trimStr(txtVigencia.value) != "") && (selModo.value == 2)) {
        if (chkDataEx(txtVigencia.value) == false) {
            window.top.overflyGen.Alert('Data inv�lida.');
           return null;
       }
       sDtVigencia = txtVigencia.value;
       sDtVigencia = "'" + normalizeDate_DateTime(sDtVigencia, true) + "'";
    }

    strPars = '?selCargo=' + selCargo.value;
    strPars += '&empresaID=' + empresaData[0];
    strPars += '&selCargoIndex=' + selCargo.selectedIndex;
    strPars += '&txtInicio=' + txtInicio.value;
    strPars += '&txtInicio2=' + normalizeDate_DateTime(txtInicio.value, true);
    strPars += '&txtFim=' + txtFim.value;
    strPars += '&txtFim2=' + normalizeDate_DateTime(txtFim.value, true);
    strPars += '&selModo=' + selModo.value;   
    strPars += '&selColaborador=' + selColaborador.value;
    strPars += '&selColaboradorIndex=' + selColaborador.selectedIndex;
    strPars += '&selDepartamento=' + selDepartamento.value;
    strPars += '&selDepartamentoIndex=' + selDepartamento.selectedIndex;
    strPars += '&selNivel=' + selNivel.value;
    strPars += '&selNivelIndex=' + selNivel.selectedIndex;
    strPars += '&sDtVigencia=' + sDtVigencia;
    strPars += '&controle=' + 'listaExcel';
    strPars += '&sEmpresaFantasia=' + empresaData[3];
    strPars += '&nLinguaLogada=' + nLinguaLogada;
    strPars += '&controlID=' + controlID;

    if (controlID == 'btnExcel') {
        lockControlsInModalWin(true);
        var frameReport = document.getElementById("frmReport");
        frameReport.contentWindow.document.onreadystatechange = reports_onreadystatechange;
        frameReport.contentWindow.location = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/serverside/historicofuncional.aspx' + strPars;       
    }
    else {
        // zera o grid
        fg.Rows = 1;

        // parametrizacao do dso dsoGrid
        setConnection(dsoGrid);

        dsoGrid.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/historicofuncional.aspx' + strPars;
        dsoGrid.ondatasetcomplete = fillGridData_DSC;

        try {
            dsoGrid.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido.') == 0)
                return null;

            lockControlsInModalWin(false);
            window.focus();
        }
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {

    var sReadOnly = (selModo.value == 1 ? '*' : '');
    var aHiddenColumns = (selModo.value == 1 ? [8, 9, 11, 12, 13] : [12, 13]);
    
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

  // dsoGrid.SQL = 'SELECT b.PessoaID AS ColaboradorID, b.Fantasia AS Colaborador, a.dtAdmissao, a.dtVigencia, a.Salario, a.DepartamentoID, a.CargoID, a.Nivel, NULL AS dtVigencia2, 0.00 AS Salario2, ' +
     //             'SPACE(0) AS Observacao, 0 AS OK, SPACE(0) AS Alteracoes ' +


    headerGrid(fg, ['ID',
				   'Colaborador',
				   'Admiss�o',
				   'Vig�ncia',
				   'Sal�rio',
				   'Departamento',
				   'Cargo',
				   'N�vel',
				   'Vig�ncia',
				   'Sal�rio',
				   'Observa��o',
				   'OK',
                   'Altera��es',
                   'RelacaoID'], aHiddenColumns);
    
    //glb_aCelHint = [[0,3,'�rea comercial'],
	//			[0,4,'Funcion�rio direto?']];

    fillGridMask(fg, dsoGrid, ['ColaboradorID*',
							  'Colaborador*',
							  'dtAdmissao*',
							  'dtVigencia*',
							  'Salario*',
							  'DepartamentoID' + sReadOnly,
							  'CargoID' + sReadOnly,
							  'Nivel' + sReadOnly,
							  'dtVigencia2' + sReadOnly,
							  'Salario2' + sReadOnly,
							  'Observacao' + sReadOnly,
							  'OK' + sReadOnly,
							  'Alteracoes' + sReadOnly,
							  'RelacaoID'],
							 ['', '', '99/99/9999', '99/99/9999', '999999999.99',   '', '', '', '99/99/9999', '999999999.99', '', '', ''],
							 ['', '', dTFormat,      dTFormat,    '###,###,##0.00', '', '', '',   dTFormat, '###,###,##0.00', '', '', '']);

    insertcomboData(fg, getColIndexByColKey(fg, 'CargoID' + sReadOnly), dsoCargos, 'Cargo', 'CargoID');
    insertcomboData(fg, getColIndexByColKey(fg, 'DepartamentoID' + sReadOnly), dsoDepartamentos, 'Departamento', 'DepartamentoID');

    var sComboNivel = "#";
    var sComboNivelMax = "";
    var nComboNivelCount;
    
    dsoCargos.recordset.MoveFirst();
    
    if (!dsoCargos.recordset.EOF) {
        nComboNivelMax = dsoCargos.recordset['NivelMaximo'].value;

        for (nComboNivelCount = 1; nComboNivelCount <= nComboNivelMax; nComboNivelCount++) {
            sComboNivel += (sComboNivel != '#' ? '|' : '') + nComboNivelCount + ';' + nComboNivelCount; 
        }
    }

    fg.ColComboList(getColIndexByColKey(fg, 'Nivel' + sReadOnly)) = sComboNivel;

    fg.ColDataType(getColIndexByColKey(fg, 'OK' + sReadOnly)) = 11;

	alignColsInGrid(fg,[0,4,7,9]);
	
    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;

    fg.Redraw = 0;
    fg.FrozenCols = 4;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    paintCellsSpecialyReadOnly();

    if (selModo.value == 2) {
        var bgColorYellow;
        var bgColorYellow = 0x00ffff;
        fg.Redraw = 0;

        for (y = 1; y <= (fg.Rows - 1); y++) {
            if (fg.valueMatrix(y, getColIndexByColKey(fg, 'Salario*')) != fg.valueMatrix(y, getColIndexByColKey(fg, 'Salario2' + sReadOnly)))

                fg.Cell(6, y, getColIndexByColKey(fg, 'Salario*'), y, getColIndexByColKey(fg, 'Salario*')) = bgColorYellow;
        }
        fg.Redraw = 2;
    }

    if (selModo.value == 1) {
    
        var vAlteracoes;
    
        var bgColorYellow;
        var bgColorYellow = 0x00ffff;
        fg.Redraw = 0;

           for (y = 1; y <= (fg.Rows - 1); y++) {

              vAlteracoes = (fg.TextMatrix(y, getColIndexByColKey(fg, 'Alteracoes' + sReadOnly)));

              if (vAlteracoes.lastIndexOf('Salario') > 0) {
                  fg.Cell(6, y, getColIndexByColKey(fg, 'Salario*'), y, getColIndexByColKey(fg, 'Salario*')) = bgColorYellow;
              }

               if (vAlteracoes.lastIndexOf('Departamento') > 0) {
                   fg.Cell(6, y, getColIndexByColKey(fg, 'DepartamentoID' + sReadOnly), y, getColIndexByColKey(fg, 'DepartamentoID' + sReadOnly)) = bgColorYellow;
              }

              if (vAlteracoes.lastIndexOf('Cargo') > 0) {
                  fg.Cell(6, y, getColIndexByColKey(fg, 'CargoID' + sReadOnly), y, getColIndexByColKey(fg, 'CargoID' + sReadOnly)) = bgColorYellow;
              }

              if (vAlteracoes.lastIndexOf('Nivel') > 0) {
                  fg.Cell(6, y, getColIndexByColKey(fg, 'Nivel' + sReadOnly), y, getColIndexByColKey(fg, 'Nivel' + sReadOnly)) = bgColorYellow;
              }
          }

         fg.Redraw = 2;
     }
        
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if ((fg.Rows > 1)&&(selModo.value==2)) {
       
		fg.Editable = true;
        window.focus();
        fg.focus();
        fg.Col = 1;
    }                
    else {
        fg.Editable = false;
        window.focus();
        fg.focus();
    }

    if (fg.Rows > 1) {
        fg.Row = 1;
        fg.TopRow = 1;
    }

    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	var VigenciaAtual;
	
    lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
    
    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0) {

		    var VigenciaAtual = fg.TextMatrix(i, getColIndexByColKey(fg, 'dtVigencia*'));
		    VigenciaAtual = "'" + normalizeDate_DateTime(VigenciaAtual, true) + "'";
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?StartPackge=' + escape(0);
			}

			nDataLen++;
			strPars += '&RelacaoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'RelacaoID')));
			strPars += '&DepartamentoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'DepartamentoID')));
			strPars += '&CargoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'CargoID')));
			strPars += '&Nivel=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Nivel')));
			strPars += '&Salario=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Salario2')));
			strPars += '&controle=' + 'gravaDados';
			
			if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtVigencia2')) == 0)
			    strPars += '&dtVigencia=' + VigenciaAtual;
			else
			    strPars += '&dtVigencia=' + escape('\'' + putDateInMMDDYYYY2(fg.TextMatrix(i, getColIndexByColKey(fg, 'dtVigencia2'))) + '\'');
			    
			if (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao'))) == '')
				strPars += '&Observacao=' + escape('NULL');
			else
				strPars += '&Observacao=' + escape('\'' + fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao')) + '\'');
		}
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
		    dsoGrava.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/historicofuncional.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData(\'btnListar\')', 10, 'JavaScript');
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData(\'btnListar\')', 10, 'JavaScript');
	}
}

function sendDataToServer_DSC()
{
	if ( !(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		dsoGrava.recordset.MoveFirst();
		if ( (dsoGrava.recordset['Resultado'].value != null) && 
			 (dsoGrava.recordset['Resultado'].value != '') )
		{
			if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
				return null;
				
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData(\'btnListar\')', 10, 'JavaScript');
			return null;
		}
	}

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData(\'btnListar\')', 10, 'JavaScript');
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalhistoricosfuncionaisBeforeMouseDown(grid, Row, Col) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalhistoricosfuncionaisDblClick(grid, Row, Col) {

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    if (Col != getColIndexByColKey(fg, "OK"))
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 1; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            break;
        }
    }

    for (i = 1; i < grid.Rows; i++) {
        if (bFill)
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }

    setTotalColumns();

    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();

    // Se n�o for manuten��o, n�o trata
    if (selModo.value != '2') return;

    // Inverte o valor de todos marcados.
    glb_TodosMarcados = !glb_TodosMarcados;

    // Se o click foi no label da coluna OK, linha zero e coluna 11,
    // inverte a
    if (Col == getColIndexByColKey(fg, "OK")) {
        for (l = 1; l < fg.Rows; l++) {
            fg.TextMatrix(l, getColIndexByColKey(fg, "OK")) = glb_TodosMarcados ? 1 : 0;
        }
        fg.Row = 1;
    }
}


/********************************************************************
Exibe niveis de Cargo
********************************************************************/
function selCargo_onchange() {
    
    var nivel = 1;
    clearComboEx(['selNivel']);

    optionStr = '';
    optionValue = 0;
    var oOption = document.createElement("OPTION");
    oOption.text = optionStr;
    oOption.value = optionValue;
    selNivel.add(oOption);    
    
    for (nivel = 1; nivel <= selCargo.options(selCargo.selectedIndex).nivelMaximo; nivel++)
    {
         optionStr = nivel;
         optionValue = nivel;
         var oOption = document.createElement("OPTION");
         oOption.text = optionStr;
         oOption.value = optionValue;
         selNivel.add(oOption);    
     }
}

/********************************************************************
Controla campos (Inicio, Fim, Vig�ncia)
********************************************************************/
function selModo_onchange() {

    //Desabilita o Bot�o Gravar qdo for Consulta
    if (selModo.value == 1) {
        btnOK.disabled = true;
    }

    //Limpa linhas do Grid 
    fg.Rows = 1;
    
    if (selModo.value == 1) {
        /* Esconde Vig�ncia - Exibe In�cio e Fim*/
        lblVigencia.style.visibility = 'hidden';
        txtVigencia.style.visibility = 'hidden';

        lblInicio.style.visibility = 'inherit';
        txtInicio.style.visibility = 'inherit';

        lblFim.style.visibility = 'inherit';
        txtFim.style.visibility = 'inherit';
    } else {
       /* Esconde Inicio e Fim - Exibe Vig�ncia*/
        lblInicio.style.visibility = 'hidden';
        txtInicio.style.visibility = 'hidden';

        lblFim.style.visibility = 'hidden';
        txtFim.style.visibility = 'hidden';

        lblVigencia.style.visibility = 'inherit';
        txtVigencia.style.visibility = 'inherit'; 
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalhistoricosfuncionaisKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalhistoricosfuncionais_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalhistoricosfuncionais_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else
        grid.EditMaxLength = 0;

    if (col == getColIndexByColKey(fg, 'Nivel')) {
        glb_Nivel = fg.TextMatrix(row, col);

    }
}


/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalhistoricosfuncionais_AfterEdit(Row, Col) {
    var bLigarOK = false;
    
	if ((fg.Editable) && (fg.EditText != ''))
	{
		if (Col == getColIndexByColKey(fg, 'Salario2'))
		{
		    fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
		    bLigarOK = true;
		}
		else if (Col == getColIndexByColKey(fg, 'dtVigencia2'))
		{
			if (chkDataEx(fg.EditText) == false)
			{
				if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
					return null;

	            fg.TextMatrix(Row, Col) = '';
	            return null;
	        }
	        bLigarOK = true;
       }
       else if (Col == getColIndexByColKey(fg, 'Nivel')) 
       {
              dsoCargos.recordset.MoveFirst();
              dsoCargos.recordset.Find('CargoID', fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CargoID')));
              
              if (!dsoCargos.recordset.EOF) {
                  if (fg.EditText > dsoCargos.recordset['Nivel'].value) {
                      if (window.top.overflyGen.Alert('N�vel superior ao n�vel m�ximo (' + dsoCargos.recordset['Nivel'].value + ') permitido para este Cargo') == 0)
                          return null;
                      
                      fg.TextMatrix(Row, Col) = glb_Nivel;
                      return null;
                  }
              }
              bLigarOK = true;
       }
   }

   if (bLigarOK)
       fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;	
        
	setupBtnsFromGridState();
	setTotalColumns();

}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalhistoricosfuncionais(grid, OldRow, OldCol, NewRow, NewCol) {
	if (glb_GridIsBuilding)
		return false;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************

function reports_onreadystatechange() {

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
        (frameReport.contentWindow.document.readyState == 'interactive') ||
        (frameReport.contentWindow.document.readyState == 'complete')) {

        lockControlsInModalWin(false);
    }
}
