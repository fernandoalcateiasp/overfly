<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalcrmsincronizacaoHtml" name="modalcrmsincronizacaoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoas/modalpages/modalcrmsincronizacao.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- Eventos de grid -->
<!-- fg -->

<script LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
    js_ModalCrmSincronizacao_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
    js_ModalCrmSincronizacao_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
</SCRIPT>

</head>

<body id="modalcrmsincronizacaoBody" name="modalcrmsincronizacaoBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <!-- Div dos campos -->
    <div id="divFields" name="divFields" class="divGeneral">
        
        <p id="lblInicio" name="lblInicio" class="lblGeneral">Inicio</p>
        <input type="text" id="txtInicio" name="txtInicio" class="fldGeneral">
        
        <p id="lblFim" name="lblFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtFim" name="txtFim" class="fldGeneral">

        <p id="lblErro" name="lblErro" class="lblGeneral">Erro</p>
        <input type="checkbox" id="chkErro" name="chkErro" class="fldGeneral" title="Listar registros com erros?" />

        <p id="lblEvento" name="lblEvento" class="lblGeneral">Evento</p> 
        <select id="selEvento" name="selEvento" class="fldGeneral">
        <%
	        Dim rsData, strSQL
	        Set rsData = Server.CreateObject("ADODB.Recordset")

                strSQL =  "SELECT 0 AS ItemID, '' AS ItemMasculino " & _
                          "UNION " & _
                          "SELECT ItemID, ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE (TipoID = 6)"

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            Do While Not rsData.EOF
                Response.Write( "<option value='" & CStr(rsData.Fields("ItemID").Value) & "'" )

    	        Response.Write( ">" & rsData.Fields("ItemMasculino") & "</option>" )
                rsData.MoveNext
            Loop

            rsData.Close
            Set rsData = Nothing
        %>
        </select>

        <p id="lblSistema" name="lblSistema" class="lblGeneral">Sistema</p> 
        <select id="selSistema" name="selSistema" class="fldGeneral" >
        <%
	        'Dim rsData, strSQL
	        Set rsData = Server.CreateObject("ADODB.Recordset")

                strSQL =  "SELECT 0 AS ItemID, '' AS ItemMasculino " & _
                          "UNION " & _
                          "SELECT ItemID, ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ((TipoID = 402) AND (ItemID IN (605, 609)))"

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

            Do While Not rsData.EOF
                Response.Write( "<option value='" & CStr(rsData.Fields("ItemID").Value) & "'" )

    	        Response.Write( ">" & rsData.Fields("ItemMasculino") & "</option>" )
                rsData.MoveNext
            Loop

            rsData.Close
            Set rsData = Nothing
        %>
        </select>

        <p id="lblID" name="lblID" class="lblGeneral">ID</p>
        <input type="text" id="txtID" name="txtID" class="fldGeneral">

        <p id="lblCampo" name="lblCampo" class="lblGeneral">Campo</p> 
        <select id="selCampo" name="selCampo" class="fldGeneral" >
        <%
	        Dim i
	        Set rsData = Server.CreateObject("ADODB.Recordset")

                strSQL =  "SELECT '' AS Campo " & _
                          "UNION " & _
                          "SELECT DISTINCT Campo FROM CRMSincronizacao_Detalhes WITH(NOLOCK) WHERE (Campo IS NOT NULL) ORDER BY Campo"

            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

           i = 0

            Do While Not rsData.EOF
                Response.Write( "<option value='" & CStr(i) & "'" )

    	        Response.Write( ">" & rsData.Fields("Campo") & "</option>" )
                
                i = i + 1

                rsData.MoveNext
            Loop

            rsData.Close
            Set rsData = Nothing
        %>
        </select>
    </div>
    
    <!-- Divs de grid -->
    <div id="divGrids" name="divGrids" class="divGeneral">
        <div id="divFG" name="divFG" class="divGeneral">
            <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" viewastext></object>
            <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
            <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
            <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
        </div>
        <div id="divFGDetalhes" name="divFGDetalhes" class="divGeneral">
            <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fgDetalhes" name="fgDetalhes" viewastext>
            </object>
            <img id="hr_L_FGBorder2" name="hr_L_FGBorder2" class="lblGeneral"></img>
            <img id="hr_R_FGBorder2" name="hr_R_FGBorder2" class="lblGeneral"></img>
            <img id="hr_B_FGBorder2" name="hr_B_FGBorder2" class="lblGeneral"></img>
        </div>
    </div>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
</body>

</html>
