/********************************************************************
modalfuncionarios.js

Library javascript para o modalfuncionarios.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_nDSOs = 0;
var glb_OcorrenciasTimerInt = null;
var glb_bFirstFill = false;
// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;

// Dados do grid .RDS
var dsoGrid = new CDatatransport("dsoGrid");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoCargos = new CDatatransport("dsoCargos");
var dsoSuperior = new CDatatransport("dsoSuperior");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalfuncionariosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modalfuncionariosDblClick(grid, Row, Col)
js_modalfuncionariosKeyPress(KeyAscii)
js_modalfuncionarios_ValidateEdit()
js_modalfuncionarios_BeforeEdit(grid, row, col)
js_modalfuncionarios_AfterEdit(Row, Col)
js_fg_AfterRowColmodalfuncionarios (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalfuncionariosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

	glb_nDSOs = 2;

    setConnection(dsoCargos);

	dsoCargos.SQL = 'SELECT a.RecursoID AS CargoID, a.RecursoFantasia AS Cargo ' +
		'FROM Recursos a WITH(NOLOCK) ' +
		'WHERE ((a.TipoRecursoID = 6) AND (a.EstadoID=2) AND (a.EhCargo=1)) ' +
		'ORDER BY Cargo';

    dsoCargos.ondatasetcomplete = dsoCargos_DSC;
	dsoCargos.Refresh();

    setConnection(dsoSuperior);

	dsoSuperior.SQL = 'SELECT DISTINCT b.PessoaID, b.Fantasia ' +
		'FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), Recursos c WITH(NOLOCK) ' +
		'WHERE ((a.TipoRelacaoID = 31) AND (a.EstadoID = 2) AND  ' +
		'(a.SujeitoID = b.PessoaID) AND (a.CargoID = c.RecursoID) AND ' +
		'(c.EhCargoSuperior = 1)) ' +
		'ORDER BY b.Fantasia';

    dsoSuperior.ondatasetcomplete = dsoCargos_DSC;
	dsoSuperior.Refresh();
}

function dsoCargos_DSC()
{
	glb_nDSOs--;

	if (glb_nDSOs > 0)
		return null;

    glb_bFirstFill = true;

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Funcion�rios', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
	
    // Ajusta o divControls
    with (divControls.style)
    {
		border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = btnOK.offsetLeft + btnOK.offsetWidth + ELEM_GAP;    
        height = 37;
    }
    
    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = modHeight - parseInt(top, 10) - (ELEM_GAP + 6);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = divControls.offsetTop + ELEM_GAP;
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		title = '';
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

    with (btnListar)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnListar.currentStyle.width, 10) - 4;
    }
    
	adjustElementsInForm([['lblFuncionariosDiretos','chkFuncionariosDiretos',3,1,-10,-10],
		['lblFuncionariosIndiretos','chkFuncionariosIndiretos',3,1,-24],
		['lblPessoalComercial','chkPessoalComercial',3,1],
		['lblPessoalAdministrativo','chkPessoalAdministrativo',3,1,-20]], null, null, true);

	chkFuncionariosDiretos.onclick = chkFuncionariosDiretos_onclick;
	chkFuncionariosIndiretos.onclick = chkFuncionariosDiretos_onclick;

	chkPessoalComercial.onclick =  chkPessoalComercial_onclick;
	chkPessoalAdministrativo.onclick =  chkPessoalComercial_onclick;

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

function chkFuncionariosDiretos_onclick()
{
	fg.Rows = 1;

	if ((this.id == chkFuncionariosDiretos.id) && (!chkFuncionariosDiretos.checked))
		chkFuncionariosIndiretos.checked = true;
	else if ((this.id == chkFuncionariosIndiretos.id) && (!chkFuncionariosIndiretos.checked))
		chkFuncionariosDiretos.checked = true;	
}

function chkPessoalComercial_onclick()
{
	fg.Rows = 1;
	
	if ((this.id == chkPessoalComercial.id) && (!chkPessoalComercial.checked))
		chkPessoalAdministrativo.checked = true;
	else if ((this.id == chkPessoalAdministrativo.id) && (!chkPessoalAdministrativo.checked))
		chkPessoalComercial.checked = true;	
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnListar')
    {
		fillGridData();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
	showExtFrame(window, true);
	
	window.focus();

    // preenche o grid da janela modal
    fillGridData();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
	var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
	
	btnOK.disabled = ((!bHasRowsInGrid) || (nEstadoID == 66));
}

function setTotalColumns()
{
	return null;
    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nValue = 0;
    var nCounter = 0;
    
	if (bHasRowsInGrid)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) != 0)
			{
				nValue += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor*'));
				nCounter++;
			}				
		}
	}
	
	fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor*')) = nValue;
	fg.TextMatrix(1, getColIndexByColKey(fg, 'FormaPagamento*')) = nCounter;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_OcorrenciasTimerInt != null)
    {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }
    
    var sDadosAvaliacao = '';
	var aGrid = null;
	var i = 0;
	var sFiltro = '';

	if (!(chkFuncionariosDiretos.checked && chkFuncionariosIndiretos.checked))
	{
		if (chkFuncionariosDiretos.checked)
			sFiltro += ' AND d.FuncionarioDireto = 1 ';
		else if (chkFuncionariosIndiretos.checked)
			sFiltro += ' AND d.FuncionarioDireto = 0 ';
	}

	if (!(chkPessoalComercial.checked && chkPessoalAdministrativo.checked))
	{
		if (chkPessoalComercial.checked)
			sFiltro += ' AND e.TipoPerfilComercialID IS NOT NULL ';
		else if (chkPessoalAdministrativo.checked)
			sFiltro += ' AND e.TipoPerfilComercialID IS NULL ';
	}

	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;

    // zera o grid
    fg.Rows = 1;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	dsoGrid.SQL = 'SELECT ISNULL(f.ItemMasculino, ' + '\'' + 'Administrativo' + '\'' + ') AS Grupo, ' +
			'e.RecursoID AS CargoID, d.ProprietarioID, d.AlternativoID, c.PessoaID AS FuncionarioID, c.Fantasia AS Funcionario, d.dtAdmissao, ' +
			'd.FuncionarioDireto, CONVERT(BIT, e.TipoPerfilComercialID) AS Comercial, d.Salario, d.SalarioCarteira, d.PesoPremio, ' +
			'd.RelacaoID, d.Observacao, 0 AS OK ' +
		'FROM Pessoas c WITH(NOLOCK) ' +
		    'INNER JOIN RelacoesPessoas d WITH(NOLOCK) ON (c.PessoaID=d.SujeitoID) ' +
		    'INNER JOIN Recursos e WITH(NOLOCK) ON (d.CargoID = e.RecursoID) ' +
		    'LEFT OUTER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (e.TipoPerfilComercialID = f.ItemID) ' +
		'WHERE d.ObjetoID=' + glb_aEmpresaData[0] + ' AND d.TipoRelacaoID=31 AND ' +
			'd.EstadoID = 2 ' + sFiltro +
		'ORDER BY ISNULL(f.ItemID, 9999), Funcionario';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    
    try
    {
		dsoGrid.Refresh();
	}	
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Filtro inv�lido.') == 0 )
            return null;

		lockControlsInModalWin(false);        
        window.focus();
	}
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
	glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

	if (glb_bFirstFill)
	{
		glb_bFirstFill = false;
		showExtFrame(window, true);
		
		window.focus();
	}

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    headerGrid(fg,['Grupo',
				   'ID',
				   'Funcion�rio',
				   'Coml',
				   'Dir',
				   'Admiss�o',
				   'Cargo',
				   'Gestor imediato',
				   'Gestor',
				   'Sal�rio',
				   'Sal�rio Carteira',
				   'Peso Pr�mio',
				   'Observa��o',
				   'OK',
                   'RelacaoID'], [13,14]);

    glb_aCelHint = [[0,3,'�rea comercial'],
					[0,4,'Funcion�rio direto?']];

    fillGridMask(fg,dsoGrid,['Grupo*',
							 'FuncionarioID*',
							 'Funcionario*',
							 'Comercial*',
							 'FuncionarioDireto',
							 'dtAdmissao',
							 'CargoID',
							 'ProprietarioID',
							 'AlternativoID',
							 'Salario',
							 'SalarioCarteira',
							 'PesoPremio',
							 'Observacao',
							 'OK',
							 'RelacaoID'],
							 ['','','','','','99/99/9999','','','','999999999.99','999999999.99','999999999.99','','',''],
							 ['','','','','',dTFormat    ,'','','','###,###,##0.00','###,###,##0.00','###,###,##0.00','','','']);

	insertcomboData(fg, getColIndexByColKey(fg, 'CargoID'), dsoCargos, 'Cargo', 'CargoID');
	insertcomboData(fg, getColIndexByColKey(fg, 'ProprietarioID'), dsoSuperior, 'Fantasia', 'PessoaID');
	insertcomboData(fg, getColIndexByColKey(fg, 'AlternativoID'), dsoSuperior, 'Fantasia', 'PessoaID');

	alignColsInGrid(fg,[1,9,10,11]);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;

    fg.Redraw = 0;
    fg.FrozenCols = 4;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
	paintCellsSpecialyReadOnly();
	    
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
		fg.Editable = true;
        window.focus();
        fg.focus();
        fg.Col = 1;
    }                
    else
    {
        ;
    }            

    fg.Redraw = 2;
    
    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;
	
    lockControlsInModalWin(true);
	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
    
    for (i=1; i<fg.Rows; i++)
    {
		if (getCellValueByColKey(fg, 'OK', i) != 0)
		{
			nBytesAcum=strPars.length;
			
			if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
			{
				nBytesAcum = 0;
				if (strPars != '')
				{
					glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
					strPars = '';
					nDataLen = 0;
				}
				
				strPars += '?StartPackge=' + escape(0);
			}

			nDataLen++;
			strPars += '&RelacaoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'RelacaoID')));
			strPars += '&CargoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'CargoID')));
			strPars += '&SuperiorDiretoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ProprietarioID')));
			strPars += '&SuperiorID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'AlternativoID')));
			strPars += '&FuncionarioDireto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'FuncionarioDireto')) == 0 ? '0' : '1');
			strPars += '&Salario=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Salario')));
			strPars += '&SalarioCarteira=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'SalarioCarteira')));
			strPars += '&PesoPremio=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PesoPremio')));

			if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'dtAdmissao')) == 0)
				strPars += '&dtAdmissao=' + escape('NULL');
			else
				strPars += '&dtAdmissao=' + escape('\'' + putDateInMMDDYYYY2(fg.TextMatrix(i, getColIndexByColKey(fg, 'dtAdmissao'))) + '\'');

			if (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao'))) == '')
				strPars += '&Observacao=' + escape('NULL');
			else
				strPars += '&Observacao=' + escape('\'' + fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao')) + '\'');
		}
	}
	
	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{
			dsoGrava.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/alterafuncionarios.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
			dsoGrava.refresh();
		}
		else
		{
			lockControlsInModalWin(false);
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
		}	
	}
	catch(e)
	{
		if ( window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0 )
			return null;

		lockControlsInModalWin(false);
		glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
	}
}

function sendDataToServer_DSC()
{
	if ( !(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		dsoGrava.recordset.MoveFirst();
		if ( (dsoGrava.recordset['Resultado'].value != null) && 
			 (dsoGrava.recordset['Resultado'].value != '') )
		{
			if ( window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0 )
				return null;
				
			glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  				
			return null;
		}
	}

	glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC()
{
    lockControlsInModalWin(false);

	glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	paintReadOnlyCols(fg);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalfuncionariosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalfuncionariosDblClick(grid, Row, Col)
{
    if (glb_PassedOneInDblClick == true)
    {    
        glb_PassedOneInDblClick = false;
        return true;
    }
    
    // trap (grid esta vazio)
    if (grid.Rows <= 1)    
        return true;
    
    // trap so colunas editaveis
    if ( getColIndexByColKey(grid, 'OK') != Col)
        return true;
        
    var i;
    var bFill = true;
    
    grid.Editable = false;
    grid.Redraw = 0;
    lockControlsInModalWin(true);
    
    glb_PassedOneInDblClick = true;
    
    // limpa coluna se tem um check box checado
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( grid.ValueMatrix(i, Col) != 0 )
        {
            bFill = false;
            break;
        }
    }
    
    for ( i=2; i<grid.Rows; i++ )
    {
        if ( bFill )
            grid.TextMatrix(i, Col) = 1;
        else
            grid.TextMatrix(i, Col) = 0;
    }
    
    setTotalColumns();
    
    lockControlsInModalWin(false);
    grid.Editable = true;
    grid.Redraw = 2;
    setupBtnsFromGridState();
    window.focus();
    grid.focus();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfuncionariosKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfuncionarios_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfuncionarios_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
	
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfuncionarios_AfterEdit(Row, Col)
{
	if ((fg.Editable) && (fg.EditText != ''))
	{
		fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;
		if ( (Col == getColIndexByColKey(fg, 'Salario')) ||
			(Col == getColIndexByColKey(fg, 'SalarioCarteira')) ||
			(Col == getColIndexByColKey(fg, 'PesoPremio')) )
		{
			fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
		}
		else if (Col == getColIndexByColKey(fg, 'dtAdmissao'))
		{
			if (chkDataEx(fg.EditText) == false)
			{
				if ( window.top.overflyGen.Alert('Data inv�lida.') == 0 )
					return null;

				fg.TextMatrix(Row, Col) = '';
			}
		}
	}
	
	setupBtnsFromGridState();
	setTotalColumns();
}

/********************************************************************
Evento de grid particular desta pagina.
Ajusta estados da botoeira da interface.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColmodalfuncionarios(grid, OldRow, OldCol, NewRow, NewCol)
{
	if (glb_GridIsBuilding)
		return false;

	setupBtnsFromGridState();
}

// FINAL DE EVENTOS DE GRID *****************************************
