/********************************************************************
modalpesqfinanceiro.js

Library javascript para o modalpesqfinanceiro.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalpesqfinanceiroBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // configuracao inicial do html
    setupPage();   
        
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	// coloca foco no campo Pesquisa
    txtFiltro.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Caption da janela modal
    secText('Pesquisa Financeiro', 1);
        
	var tempLeft = ELEM_GAP;
    var elem;
    
    // divExtPesquisa
    elem = window.document.getElementById('divExtPesquisa');
    with (elem.style)
    {
        visibility = 'visible';
        backgroundColor = 'transparent';
        width = 300;
        height = 50;
        top = 50;
        left = 19;
        tempLeft = parseInt(left);
    }

    // txtFiltro
    elem = window.document.getElementById('txtFiltro');
    elem.disabled = false;
    with (elem.style)
    {
        width = FONT_WIDTH * 25 + 3;
        elem.maxLength = 25;
    }

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblAbertos','chkAbertos',3,1, -10, -10],
                          ['lblFiltro','txtFiltro',35,1]], null, null, true);

	txtFiltro.value = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sFiltroFinanceiro');
	
	chkAbertos.checked = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bFinanceirosAbertos');
	chkAbertos.onchange = chkAbertos_onchange;
	chkAbertos.onclick = chkAbertos_onchange;
	
    txtFiltro.onkeypress = txtFiltro_onkeypress;
    txtFiltro.onfocus = selFieldContent;
}

/********************************************************************
onchange do controle chkAbertos
********************************************************************/
function chkAbertos_onchange()
{
	if (chkAbertos.checked)
		sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bFinanceirosAbertos = true');
	else
		sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bFinanceirosAbertos = false');
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
		btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Pressiona botao OK se pressionado Enter neste campo
********************************************************************/
function txtFiltro_onkeypress()
{
    if ( event.keyCode == 13 )
        btnOK_Clicked();
}

/********************************************************************
Usuario clicou btnOK
********************************************************************/
function btnOK_Clicked()
{
	txtFiltro.value = trimStr(txtFiltro.value);
	
	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , txtFiltro.value);
}  
      