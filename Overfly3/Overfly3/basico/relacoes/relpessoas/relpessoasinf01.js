/********************************************************************
relpessoasinf01.js

Library javascript para o relpessoasinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_bFinanceirosAbertos = true;
var glb_sFiltroFinanceiro = '';

var glb_nCallModal = null;

var glb_timerRelPessoasInf = null;

var glb_Row = null;
var glb_RowPlano = null;
var glb_Altera = null;
var glb_AlteraValor = true;
var glb_AlteraQuantidade = true;

// Dados do registro corrente .SQL (no minimo: regID, observacoes, propID e alternativoID) 
var dso01JoinSup = new CDatatransport("dso01JoinSup");
// Dados de combos estaticos do form .URL 
var dsoSendMailXLS = new CDatatransport("dsoSendMailXLS");
// Dados de combos estaticos do form .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Dados de combos dinamicos do form .URL 
var dsoDynamicCmbs = new CDatatransport("dsoDynamicCmbs");
// Dados de um grid .SQL
var dso01Grid = new CDatatransport("dso01Grid");
var dsoWeb = new CDatatransport("dsoWeb");
var dsoCmb01Grid_01 = new CDatatransport("dsoCmb01Grid_01");
var dsoCmb01Grid_02 = new CDatatransport("dsoCmb01Grid_02");
var dsoCmb01Grid_03 = new CDatatransport("dsoCmb01Grid_03");
var dsoCmb01Grid_04 = new CDatatransport("dsoCmb01Grid_04");
var dso01GridLkp = new CDatatransport("dso01GridLkp");
var dso02GridLkp = new CDatatransport("dso02GridLkp");
var dso03GridLkp = new CDatatransport("dso03GridLkp");
var dso04GridLkp = new CDatatransport("dso04GridLkp");
var dsoGridProspects = new CDatatransport("dsoGridProspects");
var dsoPesqProspect = new CDatatransport("dsoPesqProspect");
// Descricao dos estados atuais dos registros do grid corrente .SQL         
var dsoStateMachineLkp = new CDatatransport("dsoStateMachineLkp");
// Dados do combo de filtros da barra inferior .SQL         
var dsoFiltroInf = new CDatatransport("dsoFiltroInf");
// Dados dos combos de proprietario e alternativo         
var dsoCmbsPropAlt = new CDatatransport("dsoCmbsPropAlt");
// Dados diversos
var dsoGeneralUse = new CDatatransport("dsoGeneralUse");
var dsoBancoDefault = new CDatatransport("dsoBancoDefault");
var dsoPlano = new CDatatransport("dsoPlano");
var dsoCmbEmailContato = new CDatatransport("dsoCmbEmailContato");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
adjustElementsInDivsNonSTD()
putSpecialAttributesInControls()
optChangedInCmb(cmb)
btnLupaClicked(btnClicked)
prgServerInf(btnClicked)
prgInterfaceInf(btnClicked, pastaID, dso)
finalOfInfCascade(btnClicked)
treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
addedLineInGrid(folderID, grid, nLineInGrid, dso)
btnBarClicked(controlBar, btnClicked)
btnBarNotEspecClicked(controlBar, btnClicked)
modalInformForm(idElement, param1, param2)
btnAltPressedInGrid( folderID )
btnAltPressedInNotGrid( folderID )
fillCmbFiltsInfAutomatic(folderID)
selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
frameIsAboutToApplyRights(folderID)
    
FUNCOES DA MAQUINA DE ESTADO:
stateMachOpened( currEstadoID )
stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
fg_AfterRowColChange_Prg()
fg_DblClick_Prg()
fg_ChangeEdit_Prg()
fg_ValidateEdit_Prg()
fg_BeforeRowColChange_Prg()
fg_EnterCell_Prg()
fg_MouseUp_Prg()
fg_MouseDown_Prg()
fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload() {
    // Ze em 17/03/08
    dealWithGrid_Load();

    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;

    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 
    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [0, 'dso03GridLkp', '', ''],
                                       [0, 'dso04GridLkp', '', '']]],
                              [20158, [[13, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
                                       [15, 'dsoCmb01Grid_02', 'Email', 'PesURLID'],
                                       [0, 'dso01GridLkp', '', ''],
                                       [0, 'dso02GridLkp', '', ''],
                                       [4, 'dsoCmb01Grid_04', 'ItemMasculino', 'ItemID'],
                                       [5, 'dsoCmb01Grid_03', 'ItemMasculino', 'ItemID']]],
                              [20159, [[2, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
                                       [3, 'dsoCmb01Grid_02', 'Fantasia', 'PessoaID'],
                                       [4, 'dsoCmb01Grid_03', 'Fantasia', 'PessoaID'],
                                       [5, 'dsoCmb01Grid_04', 'ItemMasculino', 'ItemID'],
                                       [0, 'dsoStateMachineLkp', '', '']]],
                              [20160, [[1, 'dsoCmb01Grid_01', 'SimboloMoeda', 'ConceitoID'],
                                       [2, 'dsoCmb01Grid_02', 'SimboloMoeda', 'ConceitoID']]],
							  [20161, [[0, 'dso01GridLkp', '', '']]],
						      [20162, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
                                       [0, 'dsoStateMachineLkp', '', '']]],
							  [20163, [[0, 'dso01GridLkp', '', '']]],
							  [20164, [[2, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
									   [0, 'dso01GridLkp', '', '']]],
							  [20165, [[0, 'dsoCmb01Grid_01', 'Fantasia', 'PessoaID'],
									   [1, 'dsoCmb01Grid_02', 'Conceito', 'ConceitoID'],
									   [4, 'dsoCmb01Grid_03', 'SimboloMoeda', 'ConceitoID'],
									   [0, 'dso01GridLkp', '', '']]],
							  [20166, [[2, 'dsoCmb01Grid_01', 'Verba|*Plano', 'RelPesRecBeneficioID'],
									   [0, 'dso01GridLkp', '', '']]],
                              [20167, [[0, 'dso01GridLkp', '', '']]],
                              [20168, [[0, 'dsoCmb01Grid_01', 'Fantasia', 'PessoaID']]],
                              [20169, [[1, 'dsoCmb01Grid_01', 'Fantasia', 'PessoaID'],
                                       [2, 'dsoCmb01Grid_02', 'Cargo', 'RecursoID']]],
                              [32036, [[1, 'dsoCmb01Grid_01', 'ItemFeminino', 'ItemID']]],
                              [20014, [[0, 'dsoCmb01Grid_01', 'Atributo', 'ItemID'],
                                       [0, 'dso01GridLkp', '', '']]]];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03'],
                               [20008, 20009,
                               [20010, 20158, 20159, 20160, 20161, 20162, 20163, 20164, 20165, 20166, 20167, 20283, 20168, 20169, 32036, 20014]]); // Adicionado pasta Hist. Funcional (20169). HABS

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

function setupPage() {
    //@@ Ajustar os divs
    // Nada a codificar
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg() {
    if (keepCurrLineInGrid() == -1)
        return null;

    // Visitas, consultar Observacoes
    if (keepCurrFolder() == 20159)
        setupEspecBtnsControlBar('inf', 'HDDD');
}

function fg_DblClick_Prg() {
    var empresa = getCurrEmpresaData();

    if ((keepCurrFolder() == 20158) && (!fg.Editable))
        sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], fg.TextMatrix(fg.Row, 1)));
}

function js_fg_DblClick_Prg(grid, row, col) {
    if (keepCurrFolder() == 20158)
        openSiteWeb();
}

function openSiteWeb() {
    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");
    var sUsuario = parseInt(getCurrUserID(), 10); ;  
    var nContatoid = getCellValueByColKey(fg, 'ContatoID*', fg.Row);
    var sPagina = 'vitrine.aspx';
    var nParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value");
 
  
    strPars = '?ContatoID=' + escape(nContatoid) +
		'&Pagina=' + escape(sPagina) +
		'&ParceiroID=' + escape(nParceiroID) +
		'&PessoaID=' + null +
		'&nUserID=' + escape(sUsuario) +
		'&EmpresaID=' + escape(nEmpresaID);

    dsoWeb.URL = SYS_ASPURLROOT + '/serversidegenEx/rashtoopensite.aspx' + strPars;
    dsoWeb.ondatasetcomplete = openSiteWeb_DCS;
    dsoWeb.Refresh();
}

//Alterada para gera��o de hashcode
function openSiteWeb_DCS() {
    var sSite;

    if (!((dsoWeb.recordset.BOF) || (dsoWeb.recordset.BOF))) {
     
       var sHash = dsoWeb.recordset['sHashCode'].value;

        if (dsoWeb.recordset['sHashCode'].value == null || sHash == "") {

            window.top.overflyGen.Alert('Contato n�o possui acesso ao Site.');
                    return null;
        }
        if (sHash != null) {
            sSite = 'http://www.alcateia.com.br/alcateiav3/web/aspx/Login.aspx?hc=' + sHash;

            window.open(sSite);
            return null;
        }

    }
}

function fg_ChangeEdit_Prg() {

}
function fg_ValidateEdit_Prg() {

}

function js_modalitens_AfterEdit(Row, Col) {

    //Rodrigo
    if ((keepCurrFolder() == 20166) && glb_Altera == true) {
        var PlanoID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'RelPesRecBeneficioID'));

        glb_RowPlano = Row;
        if (PlanoID > 0)
            colunareadonly(PlanoID);
        
    }
    
}

function js_fg_BeforeRowColChange_Prg(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //RRF - BENEFICIOS
    if (keepCurrFolder() == 20166) {
        //Forca celula read-only
        if (!glb_GridIsBuilding) {
            var PlanoID = grid.TextMatrix(NewRow, getColIndexByColKey(fg, 'RelPesRecBeneficioID'));
            glb_RowPlano = NewRow;

            if (PlanoID > 0) {
                colunareadonly(PlanoID);
                if ((glb_AlteraValor && NewCol == getColIndexByColKey(grid, 'Valor')) || (glb_AlteraQuantidade && NewCol == getColIndexByColKey(grid, 'Quantidade')) ||
                    ((NewCol != getColIndexByColKey(grid, 'Valor')) && (NewCol != getColIndexByColKey(grid, 'Quantidade')))) {
                    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
                }
            }
        }
    }
    else
        js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
}

function fg_EnterCell_Prg() {

}

function fg_MouseUp_Prg() {

}

function fg_MouseDown_Prg() {

}

function fg_BeforeEdit_Prg() {

}


function js_fg_AfterRowColChange_Prg(grid, OldRow, OldCol, NewRow, NewCol, Cancel) 
{
    js_fg_AfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
}


// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD() {
    //@@
    // Este form nao tem este caso    
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls() {
    //@@
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario


    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if (cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

    // Mover esta funcao para os finais retornos de operacoes no
    // banco de dados
    finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso) {
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    showBtnsEspecControlBar('inf', false, [1, 1, 1, 1]);
    tipsBtnsEspecControlBar('inf', ['', '', '', '']);

    // Proprietario
    if (pastaID == 20009) {
        showHideValidadeDias(btnClicked);
    }
    // Contatos
    if (pastaID == 20158) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Preencher Contato',
			'Detalhar Contato',
			'Incluir contatos de outras empresas',
			'Enviar e-mail da senha p/ contato',
			'Enviar e-mail da PSC p/ contato',
			'Enviar e-mail do RRC p/ contato',
			'Enviar e-mail do QAF p/ contato']);
    }
    // Financeiro
    else if (pastaID == 20161) {
        showBtnsEspecControlBar('inf', true, [1, 1, 1, 1]);
        tipsBtnsEspecControlBar('inf', ['Pesquisar', 'Detalhar Financeiro', 'Detalhar Pedido', 'Enviar E-mail de Posi��o Financeira']);
    }
    // Seguros de Cr�dito
    else if (pastaID == 20165) {
        showBtnsEspecControlBar('inf', true, [0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '', '']);
    }
    // Visitas
    else if (pastaID == 20159) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Observa��es', '', '', '']);
    }
    // RAF
    else if (pastaID == 20163) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Crit�rios de Avalia��o', '', '', '']);
    }
    // CodigosFabricante
    if (pastaID == 20167) {
        showBtnsEspecControlBar('inf', true, [1, 0, 0, 0, 0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Preencher fabricante', '', '', '', '', '', '']);
    }
    // Contatos2
    else if (pastaID == 20283) {
        showBtnsEspecControlBar('inf', true, [1, 1, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['Alterar E-mail', 'Detalhar prospect/contato', '', '']);
    }

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

function showHideValidadeDias(btnClicked) {
    var nTipoRelacaoID = 0;
    nTipoRelacaoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
        "dsoSup01.recordset['TipoRelacaoID'].value"), 10);

    showBtnsEspecControlBar('inf', true, [1, 0, 0, 0]);
    tipsBtnsEspecControlBar('inf', ['Vincular Rela��o', '', '', '']);

    lbldtValidade.style.visibility = 'hidden';
    txtdtValidade.style.visibility = 'hidden';

    lblDiasValidade.style.visibility = 'hidden';
    txtDiasValidade.style.visibility = 'hidden';


    //Usuario clicou bot�o de "altera��o"
    if ((nTipoRelacaoID == 21) && (btnClicked == null)) {
        lbldtValidade.style.visibility = 'inherit';
        txtdtValidade.style.visibility = 'hidden';

        lblDiasValidade.style.visibility = 'inherit';
        txtDiasValidade.style.visibility = 'hidden';
    }
    //Usuario clicou outro bot�o diferente de "altera��o"
    else if ((nTipoRelacaoID == 21) && (btnClicked != null)) {
        lbldtValidade.style.visibility = 'inherit';
        txtdtValidade.style.visibility = 'inherit';

        lblDiasValidade.style.visibility = 'inherit';
        txtDiasValidade.style.visibility = 'inherit';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked) {
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ((btnClicked.toUpperCase()).indexOf('SUP') >= 0)
    // usario clicou botao no control bar sup
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
    else
    // usario clicou botao no control bar inf
        sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================

}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID) {
    //@@
    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC')) {
    
        var contexto = getCmbCurrDataInControlBar('sup', 1);
        var nContextoID = contexto[1];

        if (nContextoID == 1223)
            setupEspecBtnsControlBar('sup', 'HHHHHDHD');
        else
            setupEspecBtnsControlBar('sup', 'HHHHHHHH');   
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'adjustLabelsCombos(true)');
    }
    else if (btnClicked == 'SUPINCL')
        setupEspecBtnsControlBar('sup', 'HDHDDD');
    else
        setupEspecBtnsControlBar('sup', 'HHHDDD');

    if (btnClicked == 'SUPEST')
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'controlFornecedorCliente()');

    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset["EstadoID"].value');

    // Proprietario
    if (folderID == 20009) {
        var nUserID = parseInt(getCurrUserID(), 10);
        var nProprietarioID = parseInt(dso01JoinSup.recordset["ProprietarioID"].value, 0);
        var nDiasValidade = parseInt(dso01JoinSup.recordset["DiasValidade"].value, 0);

        if (nDiasValidade < 0)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }

    // Contatos
    if (folderID == 20158) {
        var nSujeitoEmpresaSistema;
        var nObjetoEmpresaSistema;
        var nSujeitoID;
        var aEmpresaID = getCurrEmpresaData();
        var sBotao5 = 'D';
        var sBotao6 = 'D';

        if (fg.Rows > 1) {
            nSujeitoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['nSujeitoEmpresaSistema'].value"), 10);

            nObjetoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['nObjetoEmpresaSistema'].value"), 10);

            nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['SujeitoID'].value");

            nObjetoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['ObjetoID'].value");

            if (((nSujeitoEmpresaSistema == 0) && (nObjetoEmpresaSistema >= 1)) ||
				 ((nSujeitoEmpresaSistema >= 1) && (nObjetoEmpresaSistema >= 1) && (aEmpresaID[0] == nObjetoID))) {
                sBotao5 = 'H';
            }

            if (((nSujeitoEmpresaSistema >= 1) && (nObjetoEmpresaSistema == 0)) ||
				 ((nSujeitoEmpresaSistema >= 1) && (nObjetoEmpresaSistema >= 1) && (aEmpresaID[0] == nSujeitoID))) {
                sBotao6 = 'H';
            }

            if (nEstadoID == 2)
                setupEspecBtnsControlBar('inf', 'DHHH' + sBotao5 + 'H' + sBotao6);
            else
                setupEspecBtnsControlBar('inf', 'DHHD' + sBotao5 + 'H' + sBotao6);
        }
        else
            setupEspecBtnsControlBar('inf', 'DDHDDDD');
    }

    // Visitas
    else if ((folderID == 20159) && (fg.Rows > 1))
        setupEspecBtnsControlBar('inf', 'HDDD');

    // Financeiro
    else if (folderID == 20161) {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHHH');
        else
            setupEspecBtnsControlBar('inf', 'HDDH');
    }

    //Seguros de Credito
    else if (folderID == 20165)
        setupEspecBtnsControlBar('inf', 'DDDD');

    else if (folderID == 20163) // RAF
    {
        if ((fg.Rows > 1) && (fg.Row > 0))
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }

    else if (folderID == 20167) // Codigos fabricante
        setupEspecBtnsControlBar('inf', 'DDDD');

    else if (folderID == 20283) // Contatos2
    {
        if (fg.Row > 0)
            setupEspecBtnsControlBar('inf', 'HHDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso) {
    // habilita o primeiro botao especifico para grid de contatos
    if (folderID == 20158)
        setupEspecBtnsControlBar('inf', 'HDDDDD');

    // habilita o primeiro botao especifico para grid de visitas
    // Observacoes
    else if (folderID == 20159)
        setupEspecBtnsControlBar('inf', 'HDDD');

    else if (folderID == 20167)
        setupEspecBtnsControlBar('inf', 'HDDDDD');

    else if (folderID == 20283) {
        var nMailIndex = getColIndexByColKey(fg, 'EMail*');

        if (nMailIndex == -1)
            nMailIndex = getColIndexByColKey(fg, 'EMail');

        fg.ColKey(nMailIndex) = 'EMail';
    }

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened(currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID) {

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    var empresa = getCurrEmpresaData();

    if (keepCurrFolder() == 20158) {
        if ((btnClicked >= 3) && (btnClicked <= 7) && (fg.Rows > 1)) {
            if (getCellValueByColKey(fg, '^ContatoID^dso01GridLkp^PessoaID^EstadoID*', fg.Row) != 2) {
                if (window.top.overflyGen.Alert('O contato deve estar ativo para permitir esta opera��o.') == 0)
                    return null;

                return false;
            }
        }
    }

    if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20009)) {
        vinculaRelacao();
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20158)) {
        var strPars = '';
        var nRelacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RelacaoID'].value");

        // mandar os parametros para o servidor
        strPars = '?nRelacaoID=' + escape(nRelacaoID);

        // carregar modal - nao faz operacao de banco no carregamento
        htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalContatos.asp' + strPars;
        showModalWin(htmlPath, new Array(457, 284));
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 20158)) {
        var empresa = getCurrEmpresaData();

        sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], fg.TextMatrix(fg.Row, 1)));
    }
    else if ((controlBar == 'INF') && (btnClicked == 3) && (keepCurrFolder() == 20158)) {
        inclContatosOutrasEmpresas();
    }
    else if ((controlBar == 'INF') && (btnClicked == 4) && (keepCurrFolder() == 20158)) {
        openModalConfirmaAlteracaoSenha();
    }
    else if ((controlBar == 'INF') && (btnClicked == 5) && (keepCurrFolder() == 20158)) {
        emailPesquisaSatisfacao();
    }
    else if ((controlBar == 'INF') && (btnClicked == 6) && (keepCurrFolder() == 20158)) {
        emailRRC();
    }
    else if ((controlBar == 'INF') && (btnClicked == 7) && (keepCurrFolder() == 20158)) {
        emailQuestionarioAvaliacaoFornecedor();
    }
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20159)) {
        // funcao do frame work
        // abre janela modal para Observacoes
        startObsWindow_inf(fg, dso01Grid, 'Observacoes', 7);
    }
    // Pesquisa Financeiro
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20161)) {
        openModalPesqFinanceiro();
    }
    else if ((controlBar == 'INF') && (btnClicked == 2) && (keepCurrFolder() == 20161)) {
        sendJSCarrier(getHtmlId(), 'SHOWFINANCEIRO', new Array(empresa[0], fg.TextMatrix(fg.Row, 2)));
    }
    else if ((controlBar == 'INF') && (btnClicked == 3) && (keepCurrFolder() == 20161)) {
        sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(empresa[0], fg.TextMatrix(fg.Row, 5)));
    }
    else if ((controlBar == 'INF') && (btnClicked == 4) && (keepCurrFolder() == 20161)) {
        sendMailXLS();
    }
    // RAF
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20163)) {
        openModalCriteriosAvaliacao();
    }
    // Codigos Fabricante
    else if ((controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20167)) {
        openModalFabricante();
    }
    // RAF
    else if ((controlBar == 'INF') && (keepCurrFolder() == 20283)) {
        if (btnClicked == 1) {
            openModalEmailContato();
        }
        else if (btnClicked == 2) {
            if (getCellValueByColKey(fg, 'EhContato*', fg.Row) == 1)
                sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ContatoID'))));
            else
                sendJSCarrier(getHtmlId(), 'SHOWPROSPECT', new Array(empresa[0], fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'prospectID'))));
        }
    }
}

function emailRRC() {
    lockInterface(true);
    var strPars = new String();

    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");
    var nClienteID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value");
    var nContatoID = getCellValueByColKey(fg, 'ContatoID*', fg.Row);

    if ((nContatoID == null) || (nContatoID == '')) {
        if (window.top.overflyGen.Alert('Preencha o contato.') == 0)
            return null;

        return null;
    }

    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nParceiroID=' + escape(nClienteID);
    strPars += '&nContatoID=' + escape(nContatoID);
    strPars += '&nTipo=' + escape(2);

    setConnection(dsoGeneralUse);

    dsoGeneralUse.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/emailquestionario.aspx' + strPars;
    dsoGeneralUse.ondatasetcomplete = emailRRC_DSC;
    dsoGeneralUse.refresh();
}

function emailRRC_DSC() {
    lockInterface(false);

    if (!(dsoGeneralUse.BOF && dsoGeneralUse.EOF)) {
        dsoGeneralUse.recordset.MoveFirst();
        if (window.top.overflyGen.Alert(dsoGeneralUse.recordset['fldResponse'].value) == 0)
            return null;
    }
}


/********************************************************************
Envia e-mail da situacao financira com planilha em formato XLS atachada

Parametros: Nenhum
Retornos: Nenhum
********************************************************************/
function sendMailXLS() {
    lockInterface(true);
    var nEmpresaSelectedID, nParceiroID, nUsuarioID;

    var nSujeitoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		"dsoSup01.recordset['nSujeitoEmpresaSistema'].value"), 10);

    var nObjetoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
		"dsoSup01.recordset['nObjetoEmpresaSistema'].value"), 10);

    if ((nSujeitoEmpresaSistema >= 1) && (nObjetoEmpresaSistema == 0)) {
        nParceiroID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['ObjetoID'].value"), 10);

        nEmpresaSelectedID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['SujeitoID'].value"), 10);
    }
    else {
        nParceiroID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['SujeitoID'].value"), 10);

        nEmpresaSelectedID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['ObjetoID'].value"), 10);
    }

    nUsuarioID = getCurrUserID();

    var strPars = new String();
    strPars = '?nEmpresaSelectedID=' + escape(nEmpresaSelectedID);
    strPars += '&nParceiroID=' + escape(nParceiroID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);

    setConnection(dsoSendMailXLS);

    dsoSendMailXLS.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/sendmailxls.aspx' + strPars;
    dsoSendMailXLS.ondatasetcomplete = dsoSendMailXLS_DSC;
    dsoSendMailXLS.refresh();
}

function dsoSendMailXLS_DSC() {
    lockInterface(false);

    if (window.top.overflyGen.Alert('Arquivo enviado para o cliente e para o seu e-mail') == 0)
        return null;
}

function openModalPesqFinanceiro() {
    // carregar modal - nao faz operacao de banco no carregamento
    var htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalpesqfinanceiro.asp';
    showModalWin(htmlPath, new Array(380, 170));
}

function openModalCriteriosAvaliacao() {
    // carregar modal - nao faz operacao de banco no carregamento
    var strPars = new String();
    var htmlPath;
    var nVar = '';

    strPars = '?sCaller=' + escape('I');

    var nVar = getCellValueByColKey(fg, 'RelPesRAFID', fg.Row);
    strPars += '&nRelPesRAFID=' + escape(nVar);

    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalcriteriosavaliacao.asp' + strPars;

    showModalWin(htmlPath, new Array(770, 250));
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {

    if (btnClicked == 'INFALT') {
        if (keepCurrFolder() == 20158) {
            if (getCellValueByColKey(fg, '^ContatoID^dso01GridLkp^PessoaID^EstadoID*', fg.Row) == 1) {
                if (window.top.overflyGen.Alert('O contato deve ser ativado para permitir altera��es.') == 0)
                    return null;

                return false;
            }

            refillCmbEmailContato(getCellValueByColKey(fg, 'ContatoID*', fg.Row));
        }
    }

    if (btnClicked == 'INFINCL') {
        if (keepCurrFolder() == 20158) {
            refillCmbEmailContato(null, true);
        }
    }

    if (btnClicked == 'INFCANC') {
        if (keepCurrFolder() == 20158) {
            refillCmbEmailContato(null);
        }
    }

    if ((keepCurrFolder() == 20283) && (fg.Row > 0)) {
        if (getCellValueByColKey(fg, 'EhContato*', fg.Row) == 1) {
            if ((btnClicked == 'INFALT') || (btnClicked == 'INFEXCL')) {
                if (window.top.overflyGen.Alert('Opera��o n�o permitida para contato.') == 0)
                    return null;

                return false;
            }
        }

        if (btnClicked == 'INFOK') {
            return pesqProspect();
        }

    }

    if ((keepCurrFolder() == 20166) && (fg.Row > 0)) 
    {
        fg.Col = getColIndexByColKey(fg, 'RelPesRecBeneficioID');
    }
    
    
    // Para prosseguir a automacao retornar null
    return null;
}

function pesqProspect() {
    setConnection(dsoPesqProspect);
    var sNome = getCellValueByColKey(fg, 'Nome', fg.Row);
    var sEMail = getCellValueByColKey(fg, 'EMail*', fg.Row);
    var sProspectID = getCellValueByColKey(fg, 'ProspectID', fg.Row);

    if ((sNome != '') && (sEMail != '') && ((sProspectID == null) || (sProspectID == ''))) {
        setConnection(dsoPesqProspect);

        dsoPesqProspect.SQL = 'SELECT (SELECT TOP 1 PessoaID FROM Pessoas_URLs WITH(NOLOCK) WHERE URL=' + '\'' + sEMail + '\'' + ') AS PessoaEmail,' +
			'(SELECT TOP 1 PessoaID FROM Pessoas WITH(NOLOCK) WHERE Nome =' + '\'' + sNome + '\'' + ') AS PessoaNome,' +
			'(SELECT TOP 1 ProspectID FROM Prospect WITH(NOLOCK) WHERE EMail =' + '\'' + sEMail + '\'' + ') AS ProspectEmail,' +
			'(SELECT TOP 1 ProspectID FROM Prospect WITH(NOLOCK) WHERE Nome =' + '\'' + sNome + '\'' + ') AS ProspectNome';

        dsoPesqProspect.ondatasetcomplete = dsoPesqProspect_DSC;
        dsoPesqProspect.Refresh();
        return false;
    }
    else
        return null;
}

function dsoPesqProspect_DSC() {
    var sError = '';
    var PessoaID1 = dsoPesqProspect.recordset['PessoaEmail'].value;
    var PessoaID2 = dsoPesqProspect.recordset['PessoaNome'].value;
    var ProspectID1 = dsoPesqProspect.recordset['ProspectEmail'].value;
    var ProspectID2 = dsoPesqProspect.recordset['ProspectNome'].value;

    if ((PessoaID1 != null) || (PessoaID2 != null))
        sError = 'Ja existe cadastro desta pessoa. Deseja Detalhar?.';
    else if ((ProspectID1 != null) || (ProspectID2 != null))
        sError = 'Ja existe cadastro deste prospect. Deseja Detalhar?.';

    if (sError != '') {
        var _retMsg = window.top.overflyGen.Confirm(sError);

        if (_retMsg == 1) {
            if (PessoaID1 != null)
                sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(getCurrEmpresaData()[0], PessoaID1));
            else if (PessoaID2 != null)
                sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(getCurrEmpresaData()[0], PessoaID2));
            else if (ProspectID1 != null)
                sendJSCarrier(getHtmlId(), 'SHOWPROSPECT', new Array(getCurrEmpresaData()[0], ProspectID1));
            else if (ProspectID2 != null)
                sendJSCarrier(getHtmlId(), 'SHOWPROSPECT', new Array(getCurrEmpresaData()[0], ProspectID2));
        }
    }
    else
        saveRegister_1stPart();
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de fabricantes para a pasta Codigos fabricante - 21067

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalFabricante() {
    var htmlPath;
    var strPars = new String();
    var nRelacaoID = getCurrDataInControl('sup', 'txtRegistroID');

    // mandar os parametros para o servidor    
    strPars = '?nRelacaoID=' + escape(nRelacaoID);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalfabricante.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
    writeInStatusBar('child', 'cellMode', 'Fabricantes', true);
}

/********************************************************************
Funcao criada pelo programador.
Escreve fabricante e ID do fabricante vindo da janela modal de fabricantes.
Pasta de Codigos fabricante - 21067

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeFabricanteInGrid(aFabricante) {
    // o array aFabricante contem: fabricante, fabricanteID

    fg.Redraw = 0;
    // Escreve nos campos
    fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, '^FabricanteID^dso01GridLkp^PessoaID^Fantasia*')) = aFabricante[0];
    fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'FabricanteID*')) = aFabricante[1];

    fg.Redraw = 2;

    // esta funcao fecha a janela modal e destrava a interface
    restoreInterfaceFromModal();

    // ajusta barra de botoes
    setupEspecBtnsControlBar('inf', 'HDDD');
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2) {
    // Pasta de Contatos
    if ((keepCurrFolder() == 20158) && (idElement.toUpperCase() == 'MODALCONTATOSHTML')) {
        if (param1 == 'OK') {
            // Escreve ID, Fantasia no grid de contatos
            writeContatoInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    if (keepCurrFolder() == 20167) {
        if (idElement.toUpperCase() == 'MODALFABRICANTEHTML') {
            if (param1 == 'OK') {
                writeFabricanteInGrid(param2);
                // esta funcao fecha a janela modal e destrava a interface
                // movida para a funcao acima
                //restoreInterfaceFromModal();
                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Altera��o');
                return 0;
            }
            else if (param1 == 'CANCEL') {
                // esta funcao fecha a janela modal e destrava a interface
                restoreInterfaceFromModal();

                // escreve na barra de status
                writeInStatusBar('child', 'cellMode', 'Altera��o');
                return 0;
            }
        }
    }
    // Pasta de Visitas
    else if (keepCurrFolder() == 20159 && (idElement.toUpperCase() == 'MODALOBSERVACAOHTML')) {
        if (param1 == 'OK') {
            // Escreve UF, cidade e CEP no grid de enderecos
            writeObservacoesInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Pasta de Financeiro
    else if (keepCurrFolder() == 20161 && (idElement.toUpperCase() == 'MODALPESQFINANCEIROHTML')) {
        if (param1 == 'OK') {
            glb_sFiltroFinanceiro = param2;
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // Forca um refresh no inf
            __btn_REFR('inf');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal confirmacao de alteracao de senha do contato
    else if (keepCurrFolder() == 20158 && (idElement.toUpperCase() == 'MODALCONFIRMAALTERACAOSENHAHTML')) {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            glb_nCallModal = window.setInterval('enviaEMailSenha(' + (param2 == true ? 'true' : 'false') + ')', 50, 'JavaScript');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal RAF
    else if (keepCurrFolder() == 20163 && (idElement.toUpperCase() == 'MODALCRITERIOSAVALIACAOHTML')) {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            if (param2 == true) {
                glb_timerRelPessoasInf = window.setInterval('forceINFrefresh()', 50, 'javascript');
            }

            return 0;
        }
    }
    else if (keepCurrFolder() == 20283 && (idElement.toUpperCase() == 'MODALEMAILCONTATOHTML')) {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            glb_timerRelPessoasInf = window.setInterval('forceINFrefresh()', 50, 'javascript');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid(folderID) {
    // Visitas
    if ((folderID == 20159) && (fg.Rows > 1)) {
        if (fg.Editable)
            setupEspecBtnsControlBar('inf', 'HDDD');
        else
            setupEspecBtnsControlBar('inf', 'DDDD');
    }
    // Contatos
    else if (folderID == 20158) {
        if (fg.Editable)
            setupEspecBtnsControlBar('inf', 'HDDDD');
    }
    // Codigos fabricantes
    else if (folderID == 20167) {
        if (fg.Editable)
            setupEspecBtnsControlBar('inf', 'HDDDD');
    }
    else if (folderID == 20283) {
        var nMailIndex = getColIndexByColKey(fg, 'EMail*');

        if (nMailIndex == -1)
            nMailIndex = getColIndexByColKey(fg, 'EMail');

        fg.ColKey(nMailIndex) = 'EMail*';
    }
    else if (folderID == 20166)
        glb_Altera = true;
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid(folderID) {
    if (folderID == 20009) {
        txtDiasValidade.readOnly = true;
        txtdtProprietario.readOnly = true;

        // Campos com Direitos Especiais
        direitoCampos();
        showHideValidadeDias();
    }

}
/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID) {

    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder) {

    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID) {

var nParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");

    // Pasta de LOG Read Only
    if ((folderID == 20010) || (folderID == 20161) || (folderID == 20163)) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }

    //Pasta Atributos Read Only quando n�o for AppleID
    if (folderID == 20014 && nParceiroID != 223483) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    
    // As pastas de Relacoes so habilita o botao de incluir
    else if ((folderID == 20123) || (folderID == 20124) || (folderID == 20125)) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }

    // Seguros de credito
    else if (folderID == 20165) {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[3] = true;
    }

    else if (folderID == 20169) {
        // trava as barras do inf para a pasta acima definida
        if (getCurrUserID() != 1142 && getCurrUserID () != 1820)
        {
            glb_BtnsIncAltEstExcl[0] = true;
            glb_BtnsIncAltEstExcl[2] = true;
            glb_BtnsIncAltEstExcl[3] = true;
        }
    }
    else {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao criada pelo programador.
Escreve Observacoes vindo da janela modal de Observacao.
Pasta de Visitas - 20159

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeObservacoesInGrid(sObservacoes) {
    fg.Redraw = 0;
    // Escreve no campo
    fg.TextMatrix(keepCurrLineInGrid(), 7) = sObservacoes;

    fg.Redraw = 2;
}

/********************************************************************
Funcao criada pelo programador.
Escreve fabricante e ID do fabricante vindo da janela modal de fabricantes.
Pasta de fabricantes - 21065

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function writeContatoInGrid(aContato) {
    // o array aContato contem: PessoaID, Fantasia

    fg.Redraw = 0;
    // Escreve nos campos
    // Fantasia
    fg.TextMatrix(keepCurrLineInGrid(), 2) = aContato[0];
    // ID
    fg.TextMatrix(keepCurrLineInGrid(), 1) = aContato[1];

    fg.Redraw = 2;

    refillCmbEmailContato(aContato[1]);
}

function refillCmbEmailContato(nContatoID, bClear) {

    if (bClear)
        fg.ColComboList(getColIndexByColKey(fg, 'PesURLID')) = '#;';
    else if (nContatoID != null)
        dsoCmbEmailContato.SQL = 'SELECT a.PesURLID, a.URL as Email ' +
                      'FROM dbo.Pessoas_URLs a WITH (NOLOCK) ' +
                      'WHERE a.PessoaID = ' + nContatoID;
    else {
        nRegistroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RelacaoID'].value");
        dsoCmbEmailContato.SQL = 'SELECT b.PesURLID, b.URL as Email ' +
                      ' FROM RelacoesPessoas_Contatos a WITH (NOLOCK) ' +
	                  ' INNER JOIN dbo.Pessoas_URLs b WITH (NOLOCK) ON (b.PessoaID = a.ContatoID) ' +
                      ' WHERE a.RelacaoID = ' + nRegistroID;
    }

    if (!bClear) {
        dsoCmbEmailContato.ondatasetcomplete = refillCmbEmailContato_DSC;
        dsoCmbEmailContato.refresh();
    }
}

function refillCmbEmailContato_DSC() {
    insertcomboData(fg, getColIndexByColKey(fg, 'PesUrlID'), dsoCmbEmailContato, 'Email', 'PesURLID');
}

function emailPesquisaSatisfacao() {
    if ((fg.ValueMatrix(fg.Row, 12) == 0) || (fg.ValueMatrix(fg.Row, 12) == false)) {
        if (window.top.overflyGen.Alert('Contato sem acesso � Web.') == 0)
            return null;

        window.focus();
        fg.focus();
        return true;
    }

    lockInterface(true);
    var strPars = new String();
    var nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value");
    var nObjetoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");

    strPars = '?nEmpresaID=' + escape(nObjetoID);
    strPars += '&nParceiroID=' + escape(nSujeitoID);
    strPars += '&nContatoID=' + escape(getCellValueByColKey(fg, 'ContatoID*', fg.Row));
    strPars += '&nTipo=' + escape(0);

    dsoGeneralUse.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/emailquestionario.aspx' + strPars;
    dsoGeneralUse.ondatasetcomplete = enviaEMailPesquisaSatisfacao_DSC;
    dsoGeneralUse.refresh();
}

function enviaEMailPesquisaSatisfacao_DSC() {
    lockInterface(false);

    if (!(dsoGeneralUse.BOF && dsoGeneralUse.EOF)) {
        dsoGeneralUse.recordset.MoveFirst();
        if (window.top.overflyGen.Alert(dsoGeneralUse.recordset['fldResponse'].value) == 0)
            return null;

        // Forca um refresh no inf
        __btn_REFR('inf');
    }
}

function emailQuestionarioAvaliacaoFornecedor() {
    lockInterface(true);
    var strPars = new String();
    var nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value");
    var nObjetoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");

    strPars = '?nEmpresaID=' + escape(nSujeitoID);
    strPars += '&nParceiroID=' + escape(nObjetoID);
    strPars += '&nContatoID=' + escape(getCellValueByColKey(fg, 'ContatoID*', fg.Row));
    strPars += '&nTipo=' + escape(1);

    setConnection(dsoGeneralUse);

    dsoGeneralUse.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/emailquestionario.aspx' + strPars;
    dsoGeneralUse.ondatasetcomplete = emailQuestionarioAvaliacaoFornecedor_DSC;
    dsoGeneralUse.refresh();
}

function emailQuestionarioAvaliacaoFornecedor_DSC() {
    lockInterface(false);

    if (!(dsoGeneralUse.BOF && dsoGeneralUse.EOF)) {
        dsoGeneralUse.recordset.MoveFirst();
        if (window.top.overflyGen.Alert(dsoGeneralUse.recordset['fldResponse'].value) == 0)
            return null;
    }
}

function openModalConfirmaAlteracaoSenha() {
    if ((fg.ValueMatrix(fg.Row, 12) == 0) || (fg.ValueMatrix(fg.Row, 12) == false)) {
        if (window.top.overflyGen.Alert('Contato sem acesso � Web.') == 0)
            return null;

        window.focus();
        fg.focus();
        return true;
    }

    var htmlPath;

    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalconfirmaalteracaosenha.asp';

    // Veio do sup ou do inf?
    var htmlId = (getHtmlId()).toUpperCase();

    // Passar o id do label do combo que vai preencher
    // para configurar a janela
    // e o id do combo que vai preencher, que volta com a mensagem
    // para saber o combo a preencher
    var strPars = new String();
    strPars = '';

    showModalWin((htmlPath + strPars), new Array(205, 126));

    return null;
}

function enviaEMailSenha(bChangeSenha) {
    if (glb_nCallModal != null) {
        window.clearInterval(glb_nCallModal);
        glb_nCallModal = null;
    }

    lockInterface(true);
    var strPars = new String();

    var nObjetoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");

    strPars = '?nFromID=' + escape(getCurrUserID());
    strPars += '&nRelPesContatoID=' + escape(getCellValueByColKey(fg, 'RelPesContatoID', fg.Row));
    strPars += '&nContatoID=' + escape(getCellValueByColKey(fg, 'ContatoID*', fg.Row));
    strPars += '&bChangeSenha=' + escape(bChangeSenha ? 1 : 0);
    //Rodrigo Nova senha
    strPars += '&nPesURLID=' + escape(getCellValueByColKey(fg, 'PesURLID', fg.Row));

    setConnection(dsoGeneralUse);

    dsoGeneralUse.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/emailsenha.aspx' + strPars;
    dsoGeneralUse.ondatasetcomplete = enviaEMailSenha_DSC;
    dsoGeneralUse.refresh();
}

function enviaEMailSenha_DSC() {
    lockInterface(false);

    if (!(dsoGeneralUse.BOF && dsoGeneralUse.EOF)) {
        dsoGeneralUse.recordset.MoveFirst();
        if (window.top.overflyGen.Alert(dsoGeneralUse.recordset['fldResponse'].value) == 0)
            return null;

        // Forca um refresh no inf
        __btn_REFR('inf');
    }
}

function inclContatosOutrasEmpresas() {
    lockInterface(true);
    var strPars = new String();

    var nRelacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['RelacaoID'].value");

    strPars = '?nRelacaoID=' + escape(nRelacaoID);

    setConnection(dsoGeneralUse);

    dsoGeneralUse.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/inclcontatos.aspx' + strPars;
    dsoGeneralUse.ondatasetcomplete = inclContatosOutrasEmpresas_DSC;
    dsoGeneralUse.refresh();
}

function inclContatosOutrasEmpresas_DSC() {
    lockInterface(false);
    __btn_REFR('inf');
}

/********************************************************************
Funcao criada pelo programador.
Controla a habilitacao dos campos com direitos

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function direitoCampos() {
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
    var bReadOnly;

    //DireitoEspecifico
    //Relacao Pesssoas->Clientes->Propritario
    //20009 SFI-Grupo Proprietarios -> A1&&A2 -> Libera altera��o do campo Validade.
    if ((nA1 == 1) && (nA2 == 1))
        bReadOnly = false;
    else
        bReadOnly = true;

    txtdtValidade.readOnly = bReadOnly;
}
/********************************************************************
Funcao criada pelo programador.
Vincula a Relacao para o Usuario

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function vinculaRelacao() {
    lockInterface(true);

    var strPars = new String();
    var nParceiroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value");
    var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ObjetoID'].value");
    var nUsuarioID = getCurrUserID();
    var nTipoAtualizacao = 4;

    // mandar os parametros para o servidor
    strPars = '?nParceiroID=' + escape(nParceiroID);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
    strPars += '&nTipoAtualizacao=' + escape(nTipoAtualizacao);

    setConnection(dsoGeneralUse);

    dsoGeneralUse.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoas/serverside/vincularelacao.aspx' + strPars;
    dsoGeneralUse.ondatasetcomplete = vinculaRelacao_DSC;
    dsoGeneralUse.refresh();
}

function vinculaRelacao_DSC() {
    lockInterface(false);
    __btn_REFR('inf');
}

function forceINFrefresh() {
    if (glb_timerRelPessoasInf != null) {
        window.clearInterval(glb_timerRelPessoasInf);
        glb_timerRelPessoasInf = null;
    }

    __btn_REFR('inf');
}

function openModalEmailContato() {
    var htmlPath;

    htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoas/modalpages/modalemailcontato.asp';

    // Veio do sup ou do inf?
    var htmlId = (getHtmlId()).toUpperCase();

    // Passar o id do label do combo que vai preencher
    // para configurar a janela
    // e o id do combo que vai preencher, que volta com a mensagem
    // para saber o combo a preencher
    var strPars = new String();
    strPars = '?sCaller=' + escape('I');

    showModalWin((htmlPath + strPars), new Array(365, 126));

    return null;
}

function colunareadonly(PlanoID) {
    setConnection(dsoPlano);
    dsoPlano.SQL = 'SELECT ValorTitular, VerbaID FROM RelacoesPesRec_Beneficios WITH(NOLOCK) WHERE RelPesRecBeneficioID = ' + PlanoID;
    dsoPlano.ondatasetcomplete = colunareadonly_DSC;
    dsoPlano.Refresh();

}

function colunareadonly_DSC() {

    var nColValor = getColIndexByColKey(fg, 'Valor');
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');

    glb_AlteraValor = true;
    glb_AlteraQuantidade = true;
    fg.Cell(6, glb_RowPlano, nColValor, glb_RowPlano, nColValor) = 0XFFFFFF; // branco
    fg.Cell(6, glb_RowPlano, nColQuantidade, glb_RowPlano, nColQuantidade) = 0XFFFFFF; // branco

    if (dsoPlano.recordset['VerbaID'].value != 232601) //vale transporte
    {
        fg.Cell(6, glb_RowPlano, nColValor, glb_RowPlano, nColValor) = 0XDCDCDC;
        glb_AlteraValor = false;
        fg.TextMatrix(glb_RowPlano, nColValor) = "";

        fg.Cell(6, glb_RowPlano, nColQuantidade, glb_RowPlano, nColQuantidade) = 0XDCDCDC;
        glb_AlteraQuantidade = false;
        fg.TextMatrix(glb_RowPlano, nColValor) = "";
    }

    else if (dsoPlano.recordset['ValorTitular'].value != 0) 
    {
        fg.Cell(6, glb_RowPlano, nColValor, glb_RowPlano, nColValor) = 0XDCDCDC;
        glb_AlteraValor = false;
        fg.TextMatrix(glb_RowPlano, nColValor) = "";
    }

    else 
    {
        if ((fg.TextMatrix(glb_RowPlano, nColValor) == "") && (dso01Grid.recordset.aRecords[glb_RowPlano - 1][nColValor] > 0))
        {
                fg.TextMatrix(glb_RowPlano, nColValor) = dso01Grid.recordset.aRecords[glb_RowPlano - 1][nColValor];
        }
    }
}
