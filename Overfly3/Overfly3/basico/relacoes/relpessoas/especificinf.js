/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Relacoes Entre Pessoas
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
glb_nFormID = window.top.formID;
glb_nCurrSubFormID = window.top.subFormID;
glb_prospectTimer = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.RelacaoID,a.ProprietarioID,a.AlternativoID, ' +
            'a.dtValidade, CONVERT(VARCHAR, a.dtValidade, ' + DATE_SQL_PARAM + ') AS V_dtValidade, ' +
            'a.dtProprietario, CONVERT(VARCHAR, a.dtProprietario, ' + DATE_SQL_PARAM + ') AS V_dtProprietario, ' +
            'DATEDIFF(dd, GETDATE(), ISNULL(a.dtValidade, GETDATE())) AS DiasValidade, ' +
            'a.Observacoes, a.UsuarioID FROM RelacoesPessoas a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT a.LOGID, a.FormID, a.SubFormID, a.RegistroID, a.Data, a.UsuarioID, a.EventoID, a.EstadoID, ISNULL(a.Motivo, SPACE(1)) AS Motivo, ' +
				        'dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso, b.Campo, (CASE a.EventoID WHEN 28 THEN SPACE(1) ELSE b.Conteudo END) AS Conteudo, ' +
				        'dbo.fn_Log_Mudanca(b.LogDetalheID) AS Mudanca ' +
		          'FROM LOGs a WITH(NOLOCK) ' +
			        'LEFT OUTER JOIN LOGs_Detalhes b WITH(NOLOCK) ON b.LOGID=a.LOGID ' +
		          'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' AND a.FormID=' + glb_nFormID + ' ' +
		          'ORDER BY a.SubFormID, a.Data DESC';

        return 'dso01Grid_DSC';
    }

    
    else if (folderID == 20158) // Contatos
    {
        dso.SQL = 'SELECT a.* ' +
                        'FROM RelacoesPessoas_Contatos a WITH(NOLOCK) ' +
                        'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ContatoID = b.PessoaID) ' +
                        ' WHERE ' + sFiltro + 'a.RelacaoID = ' + idToFind + ' ' +
                        'ORDER BY a.Ordem';
                        
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20167) // Codigos fabricante
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPessoas_CodigosFabricante a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = ' + idToFind + ' ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20161) // Financeiro
    {
        var nSujeitoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['nSujeitoEmpresaSistema'].value"), 10);

        var nObjetoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['nObjetoEmpresaSistema'].value"), 10);

        var nPessoaID, nEmpresaID;

        if ((nSujeitoEmpresaSistema >= 1) && (nObjetoEmpresaSistema == 0)) {
            nPessoaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['ObjetoID'].value"), 10);

            nEmpresaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['SujeitoID'].value"), 10);

        }
        else {
            nPessoaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['SujeitoID'].value"), 10);

            nEmpresaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['ObjetoID'].value"), 10);
        }

        dso.SQL = 'SELECT k.dtVencimento, dbo.fn_Financeiro_Totais(k.FinanceiroID, GETDATE(), 40) AS Atraso, ' +
					'k.FinanceiroID,c.RecursoAbreviado AS Estado,d.ItemMasculino AS TipoFinanceiro, ' +
					'k.PedidoID AS Pedido, ISNULL(dbo.fn_Tradutor(j.Operacao, 246, dbo.fn_Pais_idioma(dbo.fn_Pessoa_Localidade(b.EmpresaID, 1, NULL, NULL)), '  + '\'' + 'A' + '\'), SPACE(0)) AS Transacao, ' +
                    'e.Fantasia AS Pessoa, k.Duplicata, k.dtEmissao, ' +
					'g.ItemAbreviado AS FormaPagamento, k.PrazoPagamento, h.SimboloMoeda AS Moeda, k.Valor, ' +
					'dbo.fn_Financeiro_Posicao(k.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, ' +
					'dbo.fn_Financeiro_Posicao(k.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, i.Fantasia AS Empresa, k.Observacao ' +
                'FROM Financeiro k WITH(NOLOCK) ' +
                        'INNER JOIN Recursos c WITH(NOLOCK) ON k.EstadoID = c.RecursoID ' +
                        'INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON k.TipoFinanceiroID = d.ItemID ' +
                        'INNER JOIN Pessoas e WITH(NOLOCK) ON k.PessoaID = e.PessoaID ' +
					    'INNER JOIN TiposAuxiliares_Itens g WITH(NOLOCK) ON k.FormaPagamentoID = g.ItemID ' +
					    'INNER JOIN Conceitos h WITH(NOLOCK) ON k.MoedaID = h.ConceitoID ' +
					    'INNER JOIN Pessoas i WITH(NOLOCK) ON k.EmpresaID = i.PessoaID ' +
                        'LEFT OUTER JOIN Pedidos b WITH(NOLOCK) ON k.PedidoID = b.PedidoID ' +
                        'LEFT OUTER JOIN Operacoes j WITH(NOLOCK) ON b.TransacaoID = j.OperacaoID ' +
                'WHERE ' + sFiltro + ' ' +
					    'k.EmpresaID = ' + nEmpresaID + ' AND k.ParceiroID = ' + nPessoaID + ' AND ' +
					    '(b.PedidoID IS NULL OR b.EstadoID >= 29) ' +
					    (glb_sFiltroFinanceiro != '' ? ' AND ' : '') + 
					    glb_sFiltroFinanceiro +
					    (glb_bFinanceirosAbertos == true ? ' AND k.EstadoID NOT IN (1,5,48)' : '') + ' ' +
				'ORDER BY k.dtVencimento DESC';
	    
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20162) // Contas
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPessoas_Contas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY a.TipoContaID, a.dtAbertura, a.RelPesContaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20159) // Visitas
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPessoas_Visitas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY a.RelPesVisitaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20160) // Moedas
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPessoas_Moedas a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY a.CodigoTaxaID, a.RelPesMoedaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20163) // RAF
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPessoas_RAF a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY a.dtData DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20164) // Classificacoes
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPessoas_Classificacoes a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY a.dtData DESC';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 20166) // Beneficios
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPessoas_Beneficios a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + 'a.RelacaoID = ' + idToFind + ' ' +
                  'ORDER BY (	SELECT Verba FROM RelacoesPesRec_Beneficios zz  WITH(NOLOCK) ' +
						 'INNER JOIN VerbasFopag z WITH(NOLOCK) ON zz.VerbaID = z.VerbaID ' +
						 'WHERE zz.RelPesRecBeneficioID = a.RelPesRecBeneficioID), ' +
			             '(SELECT Fantasia FROM RelacoesPesRec_Beneficios zz  WITH(NOLOCK) ' +
						 'INNER JOIN Pessoas z WITH(NOLOCK) ON zz.PrestadorID = z.PessoaID ' +
						 'WHERE zz.RelPesRecBeneficioID = a.RelPesRecBeneficioID), ' +
			             '(SELECT Plano FROM RelacoesPesRec_Beneficios zz  WITH(NOLOCK) ' +
						 'WHERE zz.RelPesRecBeneficioID = a.RelPesRecBeneficioID), ' +
						 'a.Valor, a.Quantidade';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 20283) // contatos2
    {
        glb_currDSO = dso01Grid;
        dso = dso01Grid;

        var nSujeitoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['SujeitoID'].value"), 10);

        dso.SQL = 'SELECT ' + '\'' + 'Prospect' + '\'' + ' AS Tipo, a.Nome, a.Fantasia, a.Email, a.TelefoneDDD, a.TelefoneNumero, a.CelularDDD, ' +
            'a.CelularNumero, a.Observacao, a.EstadoID, a.TipoPessoaID, a.ClassificacaoID, a.EmpresaID, a.Mailing, a.Emails, a.PaisID, ' +
            'a.prospectID, CONVERT(BIT,0) AS EhContato, a.ParceiroID, 0 AS ContatoID, a.ProprietarioID, a.AlternativoID, a.UsuarioID ' +
			'FROM prospect a WITH(NOLOCK) ' +
			'WHERE ' + sFiltro + 'a.ParceiroID = ' + nSujeitoID + ' AND ' +
				'((SELECT COUNT(*) ' +
				'FROM RelacoesPessoas_Contatos WITH(NOLOCK) ' +
				'WHERE RelacaoID=' + idToFind + ' AND ContatoID=a.PessoaID) = 0) ' +
			'UNION ALL ' +
			'SELECT ' + '\'' + 'Contato' + '\'' + ' AS Tipo, b.Nome, b.Fantasia, dbo.fn_Pessoa_URL(a.ContatoID,124,NULL) AS Email, ' +
				'c.DDD AS TelefoneDDD, c.Numero AS TelefoneNumero, d.DDD AS CelularDDD, d.Numero AS CelularNumero, a.Observacao, NULL, NULL, ' +
				'NULL, NULL, NULL, NULL, NULL, NULL, CONVERT(BIT,1) AS EhContato, 0 AS ParceiroID, a.ContatoID, NULL AS ProprietarioID, NULL AS AlternativoID, NULL AS UsuarioID ' +
			'FROM RelacoesPessoas_Contatos a WITH(NOLOCK) inner join Pessoas b WITH(NOLOCK) on a.ContatoID=b.PessoaID ' +
				'left outer join Pessoas_Telefones c WITH(NOLOCK) on a.ContatoID=c.PessoaID AND c.TipoTelefoneID=119 AND c.Ordem=1 ' +
				'left outer join Pessoas_Telefones d WITH(NOLOCK) on a.ContatoID=d.PessoaID AND d.TipoTelefoneID=121 AND d.Ordem=1 ' +
			'WHERE a.RelacaoID=' + idToFind + ' ' +
			'ORDER BY Tipo DESC, Nome';

        setConnection(dsoGridProspects);

        dsoGridProspects.SQL = 'SELECT ' + '\'' + 'Prospect' + '\'' + ' AS Tipo, a.Nome, a.Fantasia, a.Email, a.TelefoneDDD, a.TelefoneNumero, ' +
		    'a.CelularDDD, a.CelularNumero, a.Observacao, a.EstadoID, a.TipoPessoaID, a.ClassificacaoID, a.EmpresaID, a.Mailing, a.Emails, ' +
		    'a.PaisID, CONVERT(BIT, 0) AS EhContato, a.ProspectID, a.ParceiroID, 0 AS ContatoID, a.ProprietarioID, a.AlternativoID, a.UsuarioID ' +
			'FROM prospect a WITH(NOLOCK) ' +
			'WHERE a.ParceiroID = ' + nSujeitoID + ' AND ' +
				'((SELECT COUNT(*) ' +
				'FROM RelacoesPessoas_Contatos b WITH(NOLOCK) ' +
				'WHERE b.RelacaoID=' + idToFind + ' AND b.ContatoID=a.PessoaID) = 0) ' +
				'ORDER BY a.Nome';

        dsoGridProspects.ondatasetcomplete = teste_DSC;
        dsoGridProspects.Refresh();

        return 'dso01Grid_DSC';
    }
    else if (folderID == 20169) // Hist. Funcional
    {
        dso.SQL = 'SELECT a.*, ISNULL(dbo.fn_Colaborador_Alteracoes(a.RelPesHistoricoID), \'\') AS Alteracoes ' +
                    'FROM RelacoesPessoas_HistoricosFuncionais a WITH(NOLOCK) ' +
                    'WHERE a.RelacaoID = ' + idToFind + ' ' +
                    'ORDER BY a.dtVigencia DESC';
        return 'dso01Grid_DSC';

    }
    else if (folderID == 32036) // F�rias
    {
        var nSujeitoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value"), 10);

        dso.SQL = 'SELECT a.*, dbo.fn_Colaborador_Dado(-a.RelPesFeriasID, NULL, NULL, 11) AS Saldo ' +
	              'FROM RelacoesPessoas_Ferias a WITH(NOLOCK) ' +
		          'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
	              'WHERE (a.RelacaoID = ' + idToFind + ') ' +
	              'ORDER BY a.dtData, a.TipoFeriasID, a.Dias ';
        return 'dso01Grid_DSC';
    }    
    //HERALDO GRID
    else if (folderID == 20168) // EMPRESA
    {
    
        dso.SQL = 'SELECT a.* '+
                  'FROM RelacoesPessoas_Empresas a WITH(NOLOCK)'+
                  'INNER JOIN dbo.RelacoesPessoas b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID)'+
                  'WHERE (a.RelacaoID = ' + idToFind + ' AND b.TipoRelacaoID = 33)';                  

        return 'dso01Grid_DSC';
    }
    else if (folderID == 20014) { //Atributos
        var nPessoaID;
        
        nPessoaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['SujeitoID'].value"), 10);

        dso.SQL = 'SELECT a.* ' +
                  'FROM Atributos a WITH(NOLOCK) ' +
                  'WHERE a.FormID IN (1210,1220) AND a.SubFormID IN (20100,20140) AND a.RegistroID = ' + nPessoaID;
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);

    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = contexto[1];
   
    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }

    else if (pastaID == 20158) // Contatos
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=27 ' +
                      'ORDER BY Ordem';
        }
        if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT b.PesURLID, b.URL as Email ' +
                      ' FROM RelacoesPessoas_Contatos a WITH (NOLOCK) ' +
	                  ' INNER JOIN dbo.Pessoas_URLs b WITH (NOLOCK) ON (b.PessoaID = a.ContatoID) ' +
                      ' WHERE a.RelacaoID = ' + nRegistroID;
        }
        if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=28 ' +
                      'ORDER BY Ordem';
        }

        if (dso.id == 'dsoCmb01Grid_04') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=22 ' +
                      'ORDER BY Ordem';
        }

        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.PessoaID, a.Fantasia, a.EstadoID, c.RecursoAbreviado AS Est ' +
                      'FROM Pessoas a WITH(NOLOCK), RelacoesPessoas_Contatos b WITH(NOLOCK), Recursos c WITH(NOLOCK) ' +
                      'WHERE b.ContatoID=a.PessoaID AND a.EstadoID=c.RecursoID AND b.RelacaoID = ' + nRegistroID;
        }
        //Rodrigo WEB - 20/06/2011
        if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PesURLID, b.URL as Email, CONVERT(BIT, ISNULL(b.Ativado, 0)) as Ativado ' +
                      ' FROM RelacoesPessoas_Contatos a WITH (NOLOCK) ' +
	                  ' INNER JOIN dbo.Pessoas_URLs b WITH (NOLOCK) ON (b.PesURLID = a.PesURLID) ' +
                      ' WHERE a.RelacaoID = ' + nRegistroID;
        }
    }
    else if (pastaID == 20167) // Codigos fabricante
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.PessoaID, a.Fantasia ' +
                      'FROM Pessoas a privada1290  WITH(NOLOCK) ' +
                      'INNER JOIN TiposAuxiliares_Itens  b WITH(NOLOCK) ON a.ClassificacaoID = b.ItemID ' +
                      'WHERE a.EstadoID = 2 AND b.Filtro Like \'%<FAB>%\' ' +
                      'ORDER BY Fantasia';
        }
    }
    else if (pastaID == 20162) // Contas
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 902 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoStateMachineLkp') {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM RelacoesPessoas_Contas a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.RelacaoID = ' + nRegistroID + ' ' +
                      'AND a.EstadoID = b.RecursoID';
        }
    }
    else if (pastaID == 20159) // Visitas
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 37 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT a.Fantasia,a.PessoaID ' +
                      'FROM Pessoas a WITH(NOLOCK), RelacoesPessoas_Contatos b WITH(NOLOCK) ' +
                      'WHERE a.PessoaID=b.ContatoID ' +
			          'AND b.RelacaoID = ' + nRegistroID;
        }
        else if (dso.id == 'dsoCmb01Grid_03') {
            dso.SQL = 'SELECT a.Fantasia,a.PessoaID ' +
                      'FROM Pessoas a WITH(NOLOCK), RelacoesPessoas_Contatos b WITH(NOLOCK) ' +
                      'WHERE a.PessoaID=b.ContatoID AND b.RelacaoID = ' + nRegistroID;
        }
        else if (dso.id == 'dsoCmb01Grid_04') {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=38 ' +
			          'ORDER BY ItemMasculino';
        }
        else if (dso.id == 'dsoStateMachineLkp') {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM RelacoesPessoas_Visitas a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.RelacaoID = ' + nRegistroID + ' ' +
                      'AND a.EstadoID = b.RecursoID';
        }
    }
    else if (pastaID == 20160) // Moedas
    {
        var nEmpresaData = getCurrEmpresaData();
        var nEmpresaID = nEmpresaData[0];

        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT c.ConceitoID, c.SimboloMoeda ' +
                      'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
                      'WHERE a.SujeitoID = ' + nEmpresaID + ' AND a.ObjetoID=999 ' +
                      'AND a.RelacaoID = b.RelacaoID AND b.MoedaID=c.ConceitoID ' +
                      'ORDER BY b.Ordem';
        }
        else if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT c.ConceitoID, c.SimboloMoeda ' +
                      'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
                      'WHERE a.SujeitoID = ' + nEmpresaID + ' AND a.ObjetoID=999 ' +
                      'AND a.RelacaoID = b.RelacaoID AND b.Faturamento = 1 AND b.MoedaID=c.ConceitoID ' +
                      'ORDER BY b.Ordem';
        }
    }
    else if (pastaID == 20161) // Financeiro
    {
        if (dso.id == 'dso01GridLkp') {
            var nSujeitoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['nSujeitoEmpresaSistema'].value"), 10);

            var nObjetoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['nObjetoEmpresaSistema'].value"), 10);

            var nPessoaID, nEmpresaID;

            if ((nSujeitoEmpresaSistema >= 1) && (nObjetoEmpresaSistema == 0)) {
                nPessoaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
					"dsoSup01.recordset['ObjetoID'].value"), 10);

                nEmpresaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
					"dsoSup01.recordset['SujeitoID'].value"), 10);

            }
            else {
                nPessoaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
					"dsoSup01.recordset['SujeitoID'].value"), 10);

                nEmpresaID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
					"dsoSup01.recordset['ObjetoID'].value"), 10);

            }

            dso.SQL = 'SELECT dbo.fn_Pessoa_Posicao(' + nPessoaID + ', ' + nEmpresaID + ', 9, GETDATE(), 0, NULL, NULL, NULL, NULL) AS SaldoDevedor, ' +
                      'dbo.fn_Pessoa_Posicao(' + nPessoaID + ', ' + nEmpresaID + ', 8, GETDATE(), 0, NULL, NULL, NULL, NULL) AS SaldoAtualizado ' +
					  'FROM Pessoas a WITH(NOLOCK) ' +
                      'WHERE a.PessoaID = ' + nPessoaID;
        }
    }
    else if (pastaID == 20163) // RAF
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.PessoaID, a.Fantasia ' +
	                   'FROM Pessoas a WITH(NOLOCK), RelacoesPessoas_RAF b WITH(NOLOCK) ' +
	                   'WHERE b.ColaboradorID=a.PessoaID AND b.RelacaoID = ' + nRegistroID;
        }
    }
    else if (pastaID == 20164) // Classificacoes
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=29 ' +
                      'ORDER BY Ordem';
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT DISTINCT a.PessoaID, a.Fantasia ' +
	                   'FROM Pessoas a WITH(NOLOCK), RelacoesPessoas_Classificacoes b WITH(NOLOCK) ' +
	                   'WHERE b.UsuarioID=a.PessoaID AND b.RelacaoID = ' + nRegistroID;
        }
    }
    else if (pastaID == 20166) // Beneficios
    {
        var nSujeitoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['SujeitoID'].value"), 10);

        var nObjetoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
			"dsoSup01.recordset['ObjetoID'].value"), 10);

        if (dso.id == 'dsoCmb01Grid_01') {
            if (nTipoRegistroID == 31)
                dso.SQL = "SELECT b.RelPesRecBeneficioID, c.VerbaID, c.Verba, CASE c.VerbaID WHEN 232601 THEN b.Plano + \' (R$ \'+CONVERT(VARCHAR,b.ValorTitular) + \')\' ELSE b.Plano END AS Plano " +
                    "FROM RelacoesPesRec a WITH(NOLOCK) " +
                    "INNER JOIN RelacoesPesRec_Beneficios b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " +
                    "INNER JOIN VerbasFopag c WITH(NOLOCK) ON (b.VerbaID = c.VerbaID) " +
                    "WHERE (a.TipoRelacaoID = 12) AND (a.SujeitoID = " + nObjetoID + ") " +
                    "ORDER BY c.VerbaID, b.Plano";

            else if (nTipoRegistroID == 37)
                dso.SQL = "SELECT b.RelPesRecBeneficioID, c.VerbaID, c.Verba, b.Plano " +
                    "FROM RelacoesPesRec a WITH(NOLOCK) " +
                    "INNER JOIN RelacoesPesRec_Beneficios b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " +
                    "INNER JOIN VerbasFopag c WITH(NOLOCK) ON (b.VerbaID = c.VerbaID) " +
                    "WHERE (a.TipoRelacaoID = 12) AND (a.SujeitoID = (SELECT TOP 1 aa.ObjetoID " +
                                                                        "FROM RelacoesPessoas aa WITH(NOLOCK) " +
                                                                        "WHERE ((aa.TipoRelacaoID = 31) AND (aa.EstadoID NOT IN (1, 5)) AND " +
                                                                        "(aa.SujeitoID = " + nObjetoID + ")))) " +
                    "ORDER BY c.VerbaID, b.Plano";
        }
        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = "SELECT b.RelPesRecBeneficioID, c.VerbaID, c.Verba, b.Plano, b.ValorDependente, b.ValorTitular, " +
                "e.Fantasia AS Prestador " +
                "FROM RelacoesPesRec a WITH(NOLOCK) " +
                "INNER JOIN RelacoesPesRec_Beneficios b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " +
                "INNER JOIN VerbasFopag c WITH(NOLOCK) ON (b.VerbaID = c.VerbaID) " +
                "INNER JOIN RelacoesPessoas_Beneficios d WITH(NOLOCK) ON (d.RelPesRecBeneficioID = b.RelPesRecBeneficioID) " +
                "INNER JOIN Pessoas e WITH(NOLOCK) ON e.PessoaID=b.PrestadorID " +
                "WHERE d.RelacaoID= " + nRegistroID + " " +
                "ORDER BY c.VerbaID, b.Plano";
        }
    }
    else if (pastaID == 20169) // Hist. Funcional
    {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = "SELECT a.PessoaID, a.Fantasia AS Departamento " +
	                    "FROM Pessoas a WITH(NOLOCK) " +
		                    "INNER JOIN RelacoesPessoas_HistoricosFuncionais b WITH(NOLOCK) ON (b.DepartamentoID = a.PessoaID) " +
	                    "WHERE (b.RelacaoID = " + nRegistroID + ")";
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = "SELECT a.RecursoID, a.RecursoFantasia AS Cargo " +
	                    "FROM Recursos a WITH(NOLOCK) " +
		                    "INNER JOIN RelacoesPessoas_HistoricosFuncionais b WITH(NOLOCK) ON (b.CargoID = a.RecursoID) " +
	                    "WHERE (b.RelacaoID = " + nRegistroID + ")";
        }

        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = "SELECT a.PessoaID AS PessoaID, a.Fantasia AS Fantasia " +
	                    "FROM Pessoas a WITH(NOLOCK) " +
	                    "WHERE a.TipoPessoaID = 54";
        }

        if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = "SELECT a.RecursoID AS RecursoID, a.RecursoFantasia AS Cargo " +
	                    "FROM Recursos a WITH(NOLOCK) " +
	                    "WHERE (a.TipoRecursoID = 6) AND (a.EhCargo = 1)";
        }
    }

    else if (pastaID == 32036) // F�rias
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = "SELECT ItemID, ItemFeminino " +
	                    "FROM TiposAuxiliares_itens WITH(NOLOCK) " +
	                    "WHERE (EstadoID = 2 AND TipoID = 32) " +
	                    "ORDER BY Ordem";
        }
    }

    //HERALDO GRID

    else if (pastaID == 20168) // EMPRESAS
    {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = "SELECT b.PessoaID AS PessoaID, b.Fantasia " +
                        " FROM RelacoesPesRec a WITH(NOLOCK) " +
	                        " INNER JOIN Pessoas b  WITH(NOLOCK) ON  (a.SujeitoID = b.PessoaID) " +
                      " WHERE (a.TipoRelacaoID = 12 AND a.ObjetoID = 999 AND a.EstadoID = 2 AND b.EstadoID = 2) " +
                      " ORDER BY b.Fantasia ";
        }
    }
    else if (pastaID == 20014) // Atributos
    {
        if (dso.id == 'dsoCmb01Grid_01' && nContextoID == 1224) {
            dso.SQL = 'SELECT a.ItemID, a.ItemMasculino AS Atributo ' +
                      'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                      'WHERE a.TipoID = 56 AND a.EstadoID = 2 AND a.Filtro LIKE  ' + '\'' + '%<' + nContextoID + '>%' + '\'' + ' ' +
                      'ORDER BY ItemID';
        }

        else if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT a.ItemID, a.ItemMasculino AS Atributo ' +
                      'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                      'WHERE a.TipoID = 56 AND a.EstadoID = 2 AND a.Filtro LIKE \'%<20100>%\'';
        }

        else if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.ItemMasculino AS Atributo, c.Fantasia, c.PessoaID ' +
                      'FROM Atributos a WITH(NOLOCK) ' +
                      'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.TipoAtributoID) ' +
                      'INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.UsuarioID) ' +
                      'INNER JOIN RelacoesPessoas d WITH(NOLOCK) ON (d.SujeitoID = a.RegistroID) ' +
                      'WHERE a.FormID IN (1210,1220) AND a.SubFormID IN (20100,20140) AND d.RelacaoID = ' + nRegistroID;
        }
    }
}

/********************************************************************
Funcao criada pelo programador.
Nao adiciona a pasta de moedas se sujeito ou objeto nao sao empresas
do sistema
           
Parametros: 
nenhum

Retorno:
true        - adiciona a pasta
false       - nao adiciona nao adiciona
********************************************************************/
function empresaSistema()
{
    var sujOrObjFromSystem;
        
    // Controla se sujeito ou objeto sao empresas do sistema
    sujOrObjFromSystem = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_SujOrObjFromSystem');    
        
    return sujOrObjFromSystem;
}

/********************************************************************

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	var nSujeitoEmpresaSistema;
	var nObjetoEmpresaSistema;
	var nSujeitoID;
    var aEmpresaData = getCurrEmpresaData();
	var nEmpresaID = aEmpresaData[0];

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG


    if (nTipoRegistroID == 33){  
        vPastaName = window.top.getPastaName(20168);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 20168); //EMPRESAS
	}

	if (nTipoRegistroID == 36)
	{
	    vPastaName = window.top.getPastaName(20158);
	    if (vPastaName != '')
	        addOptionToSelInControlBar('inf', 1, vPastaName, 20158); //Contatos
	}

    if ( (nTipoRegistroID >= 21) && (nTipoRegistroID <= 25) && (empresaSistema() == true) )
    {
        vPastaName = window.top.getPastaName(20158);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20158); //Contatos

        vPastaName = window.top.getPastaName(20283);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20283); //Contatos2

		if (nTipoRegistroID == 21)
		{
		    vPastaName = window.top.getPastaName(20161);
		    if (vPastaName != '')
		    	addOptionToSelInControlBar('inf', 1, vPastaName, 20161); //Financeiro

			nSujeitoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
				"dsoSup01.recordset['nSujeitoEmpresaSistema'].value"), 10);

			nObjetoEmpresaSistema = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
				"dsoSup01.recordset['nObjetoEmpresaSistema'].value"), 10);

			nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
				"dsoSup01.recordset['SujeitoID'].value");

			if ( ((nSujeitoEmpresaSistema >=1) && (nObjetoEmpresaSistema == 0)) ||
				 ((nSujeitoEmpresaSistema >=1) && (nObjetoEmpresaSistema >= 1) && (nEmpresaID == nSujeitoID)) )
			{
				vPastaName = window.top.getPastaName(20163);
				if (vPastaName != '')
					addOptionToSelInControlBar('inf', 1, vPastaName, 20163); //RAF
			}

		}
		else if (nTipoRegistroID == 24)
		{
			vPastaName = window.top.getPastaName(20162);
			if (vPastaName != '')
				addOptionToSelInControlBar('inf', 1, vPastaName, 20162); //Contas
		}
	}
	
	if ( ((nTipoRegistroID == 21) && (empresaSistema() == true)) || (nTipoRegistroID == 31) )
	{
		    vPastaName = window.top.getPastaName(20164);
		    if (vPastaName != '')
		    	addOptionToSelInControlBar('inf', 1, vPastaName, 20164); //Classificacoes
	}	    	
	if ( (nTipoRegistroID == 28) && (empresaSistema() == true) )
	{
        vPastaName = window.top.getPastaName(20158);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20158); //Contatos
	    	
        vPastaName = window.top.getPastaName(20167);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20167); //Codigos fabricante
	}
	
	if (nTipoRegistroID == 21)
	{
        vPastaName = window.top.getPastaName(20159);
        if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 20159); //Visitas

        if ( empresaSistema() )
        {
            vPastaName = window.top.getPastaName(20160);
            if (vPastaName != '')
	        	addOptionToSelInControlBar('inf', 1, vPastaName, 20160); //Moedas
        }
    }

    if ((nTipoRegistroID == 31) || (nTipoRegistroID == 37)) {
        vPastaName = window.top.getPastaName(20166);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 20166); //Beneficios

        vPastaName = window.top.getPastaName(20169);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 20169); //Hist. Funcionais
    }

    vPastaName = window.top.getPastaName(20014);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20014); //Atributos

    vPastaName = window.top.getPastaName(32036);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 32036); // Pasta F�rias
    
//    //HERALDO GRID
//        vPastaName = window.top.getPastaName(20168);
//        if (vPastaName != '')
//            addOptionToSelInControlBar('inf', 1, vPastaName, 20168); //Empresa
//    }



//    //HERALDO GRID

////    if (nTipoRegistroID == 29){
//        vPastaName = window.top.getPastaName(20168);
//        if (vPastaName != '')
//            addOptionToSelInControlBar('inf', 1, vPastaName, 20168); //Empresa

//    }

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 20158) // Contatos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['RelacaoID', registroID]);
        else if (folderID == 20159) // Visitas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 2, 3, 4], ['RelacaoID', registroID]);
        else if (folderID == 20160) // Moedas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2, 3, 4, 5], ['RelacaoID', registroID]);
        else if (folderID == 20162) // Contas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 3], ['RelacaoID', registroID]);
        else if (folderID == 20163) // RAF
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['RelacaoID', registroID]);
        else if (folderID == 20164) // Classificacoes
        {
            // Registro novo
            if (getCellValueByColKey(fg, 'RelPesClassificacaoID', fg.Row) == '')
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'UsuarioID')) = getCurrUserID();

            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['RelacaoID', registroID]);
        }
        else if (folderID == 20166) // Beneficios
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2], ['RelacaoID', registroID]);
        else if (folderID == 20167) // Codigos fabricante
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 2], ['RelacaoID', registroID]);
        else if (folderID == 20283) // Contatos2
        {
            var nSujeitoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['SujeitoID'].value"), 10);
            var nObjetoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				"dsoSup01.recordset['ObjetoID'].value"), 10);
            var nEmpresaData = getCurrEmpresaData();
            var tmpDso = glb_currDSO;

            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID')) = 2;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoPessoaID')) = 51;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ClassificacaoID')) = 66;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EmpresaID')) = nObjetoID;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Mailing')) = 0;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Emails')) = 1;
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PaisID')) = nEmpresaData[1];

            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ProprietarioID')) = getCurrUserID();
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AlternativoID')) = getCurrUserID();
            fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'UsuarioID')) = getCurrUserID();

            // Jogada de mestre
            fg.ColHidden(getColIndexByColKey(fg, 'EhContato*')) = true;
            fg.ColHidden(getColIndexByColKey(fg, 'Tipo*')) = true;
            glb_currDSO = dsoGridProspects;
            currLine = criticLineGridAndFillDsoEx(fg, dsoGridProspects, fg.Row, [1, 2, 3], ['ParceiroID', nSujeitoID], [10, 11, 12, 13, 14, 15, 16]);

            //glb_currDSO = tmpDso;
            fg.ColHidden(getColIndexByColKey(fg, 'EhContato')) = false;
            fg.ColHidden(getColIndexByColKey(fg, 'Tipo*')) = false;

        }
        else if (folderID == 20169) // Hist. Funcionais
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['RelacaoID', registroID]);
        else if (folderID == 32036) // F�rias
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2], ['RelacaoID', registroID], [3]);
        else if (folderID == 20168) // Empresas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [], ['RelacaoID', registroID]);
        else if (folderID == 20014) { // Atributos
            // Registro novo
            if (getCellValueByColKey(fg, 'AtributoID', fg.Row) == '') {
                var nSujeitoID = parseInt(sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
				                    "dsoSup01.recordset['SujeitoID'].value"), 10);
                
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Data')) = getCurrDate();
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'UsuarioID')) = getCurrUserID();
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FormID')) = glb_nFormID;
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SubFormID')) = glb_nCurrSubFormID;
                fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RegistroID')) = nSujeitoID;
            }

            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['AtributoID', registroID], [5, 6, 7, 8, 9]);
        }

        if (currLine < 0)
            return false;
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    var sWebReadOnly = '';
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    if (folderID == 20010) // LOG
    {
        headerGrid(fg, ['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'Campo',
                       'De',
                       'Para',
                       'LOGID'], [9]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'Campo',
                                 'Conteudo',
                                 'Mudanca',
                                 'LOGID'],
                                 ['', '99/99/9999', '', '', '', '', '', '', '', ''],
                                 ['', dTFormat + ' hh:mm:ss', '', '', '', '', '', '', '', '']);

        alignColsInGrid(fg, [9]);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

 
    
    else if (folderID == 20158) // Contatos
    {
        sWebReadOnly = '';

        if (!((currDSO.recordset.BOF) && (currDSO.recordset.EOF))) {
            currDSO.recordset.moveFirst();

            // Verifica se tem pelo menos um master
            while (!currDSO.recordset.EOF) {
                if ((currDSO.recordset['PerfilID'].value == 140) &&
					(currDSO.recordset['Web'].value == 1) &&
					(currDSO.recordset['Senha'].value != null) &&
					(currDSO.recordset['Senha'].value != '')) {
                    sWebReadOnly = '*';
                    break;
                }

                currDSO.recordset.moveNext();
            }

            currDSO.recordset.moveFirst();
        }

        // Remover apos correcoes no cadastro da Ikeda
        sWebReadOnly = '';

        headerGrid(fg, ['Ordem',
                       'ID',
                       'Contato',
                       'Est',
                       'Cargo',
                       'Area',
					   'Corp',
                       'Com',
                       'Fin',
                       'Tec',
                       'Mailing',
                       'Emails',
                       'Web',
                       'Perfil',
                       'Senha*',
                       'E-mail',
                       'Ativado',
                       'PSC',
                       'C�digo CRM',
                       'Observa��o',
                       'EstadoID',
                       'RelPesContatoID'], [14, 20, 21]);

        glb_aCelHint = [[0, 6, 'Recebe informa��es corporativas?'],
						[0, 7, 'Recebe informa��es comerciais? (Proposta Comercial, Email de Faturamento)'],
                        [0, 8, 'Recebe informa��es financeiras? (Email de Posi��o Financeira)'],
                        [0, 9, 'Recebe informa��es t�cnicas?'],
                        [0, 10, 'Recebe mala direta?'],
                        [0, 11, 'Recebe e-mails marketing?'],
                        [0, 12, 'Acessa a Web?'],
                        [0, 16, 'E-mail esta ativo?'],
                        [0, 17, '�ltimo e-mail de PSC']];


        fillGridMask(fg, currDSO, ['Ordem*',
                                 'ContatoID*',
                                 '^ContatoID^dso01GridLkp^PessoaID^Fantasia*',
                                 '^ContatoID^dso01GridLkp^PessoaID^Est*',
                                 'CargoID',
                                 'AreaID',
                                 'Corporativo',
                                 'Comercial',
                                 'Financeiro',
                                 'Tecnico',
                                 'Mailing',
  							     'Emails',
								 'Web' + sWebReadOnly,
								 'PerfilID' + sWebReadOnly,
								 'Senha*',
								 'PesURLID',
								 '^PesURLID^dso02GridLkp^PesURLID^Ativado*',
								 'dtUltimoEmailPSC*',
                                 'CodigoCRM',
								 'Observacao',
                                 '^ContatoID^dso01GridLkp^PessoaID^EstadoID*',
                                 'RelPesContatoID'],
                                 ['99', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '99/99/9999', '', '', '', ''],
                                 ['##', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', dTFormat, '', '', '', '']);
        fg.FrozenCols = 1;
    }
    else if (folderID == 20159) // Visitas
    {
        headerGrid(fg, ['Data',
                       'Est',
                       'Tipo',
                       'Visitante',
                       'Visitado',
                       'Resultado',
                       'Quilometragem',
                       'Observa��es',
                       'RelPesVisitaID'], [8]);

        fillGridMask(fg, currDSO, ['dtVisita',
                                 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                 'TipoVisitaID',
                                 'VisitanteID',
                                 'VisitadoID',
                                 'ResultadoID',
                                 'Quilometragem',
                                 'Observacoes*',
                                 'RelPesVisitaID'],
                                 ['99/99/9999', '', '', '', '', '', '999.9', '', ''],
                                 [dTFormat, '', '', '', '', '', '###.0', '', '']);
        fg.FrozenCols = 1;
    }
    else if (folderID == 20160) // Moedas
    {
        headerGrid(fg, ['CT',
                       'De',
                       'Para',
                       'In�cio',
                       'Fim',
                       'Cota��o',
                       'RelPesMoedaID'], [6]);

        fillGridMask(fg, currDSO, ['CodigoTaxaID',
                                 'MoedaDeID',
                                 'MoedaParaID',
                                 'dtInicio',
                                 'dtFim',
                                 'Cotacao',
                                 'RelPesMoedaID'],
                                 ['99', '', '', '99/99/9999', '99/99/9999', '99999999.9999999', ''],
                                 ['##', '', '', dTFormat, dTFormat, '##,###,###.0000000', '']);
        fg.FrozenCols = 1;
        alignColsInGrid(fg, [0, 5]);
    }
    else if (folderID == 20161) // Financeiro
    {
        headerGrid(fg, ['Vencimento',
					   'Atraso',
                       'Financeiro',
                       'Est',
                       'Tipo',
					   'Pedido',
					   'Transa��o',
                       'Pessoa',
                       'Duplicata',
                       'Emiss�o',
                       'Forma',
                       'Prazo',
                       'Moeda',
                       'Valor',
                       'Saldo',
                       'Saldo Atual',
                       'Observa��o'], []);

        fillGridMask(fg, currDSO, ['dtVencimento',
								 'Atraso',
                                 'FinanceiroID',
                                 'Estado',
                                 'TipoFinanceiro',
                                 'Pedido',
                                 'Transacao',
                                 'Pessoa',
                                 'Duplicata',
                                 'dtEmissao',
                                 'FormaPagamento',
                                 'PrazoPagamento',
                                 'Moeda',
                                 'Valor',
                                 'SaldoDevedor',
                                 'SaldoAtualizado',
                                 'Observacao'],
                                 ['99/99/9999', '9999', '', '', '', '', '', '', '', '99/99/9999', '', '', '', '', '999999999.99', '999999999.99', ''],
                                 [dTFormat, '9999', '', '', '', '', '', '', '', dTFormat, '', '', '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '']);
        fg.FrozenCols = 3;
        alignColsInGrid(fg, [1, 2, 5, 11, 13, 14, 15]);
        gridHasTotalLine(fg, 'Totais (US$)', 0xC0C0C0, null, true, [[14, '###,###,###,###.00', 'S'],
                                                                    [15, '###,###,###,###.00', 'S']]);
    }
    else if (folderID == 20162) // Contas
    {
        headerGrid(fg, ['Tipo',
                       'Conta',
                       'DV',
                       'Est',
                       'Abertura',
                       'Limite',
                       'Pub',
                       'Observa��o',
                       'RelPesContaID'], [8]);

        glb_aCelHint = [[0, 6, 'Publica a conta na internet?']];

        fillGridMask(fg, currDSO, ['TipoContaID',
                                 'Conta',
                                 'ContaDV',
                                 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
                                 'dtAbertura',
                                 'Limite',
                                 'Publica',
                                 'Observacao',
                                 'RelPesContaID'],
                                 ['', '', '', '', '99/99/9999', '999999999.99', '', '', ''],
                                 ['', '', '', '', dTFormat, '###,###,###.00', '', '', '']);
        fg.FrozenCols = 1;
        alignColsInGrid(fg, [5]);
    }
    else if (folderID == 20163) // RAF
    {
        headerGrid(fg, ['Data',
                       'Pontua��o',
                       'Class',
					   'Colaborador',
                       'ColaboradorID',
                       'RelPesRAFID'], [4, 5]);

        glb_aCelHint = [[0, 2, 'Classifica��o']];

        fillGridMask(fg, currDSO, ['dtData*',
								 'Pontuacao*',
								 'Classificacao*',
								 '^ColaboradorID^dso01GridLkp^PessoaID^Fantasia*',
								 'ColaboradorID',
								 'RelPesRAFID'],
                                 ['99/99/9999', '999.99', '', '', '', ''],
                                 [dTFormat, '###.00', '', '', '', '']);
        fg.FrozenCols = 1;
        alignColsInGrid(fg, [1]);
    }
    else if (folderID == 20164) // Classificacoes
    {
        headerGrid(fg, ['Data',
                       'Origem',
                       'Classifica��o',
					   'Usu�rio',
                       'UsuarioID',
                       'RelPesClassificacaoID'], [4, 5]);

        glb_aCelHint = [[0, 1, '0-Volume de compra, 1-Pesquisa de segmenta��o']];

        fillGridMask(fg, currDSO, ['dtData',
								 'Origem',
								 'ClassificacaoID',
								 '^UsuarioID^dso01GridLkp^PessoaID^Fantasia*',
								 'UsuarioID',
								 'RelPesClassificacaoID'],
                                 ['99/99/9999', '9', '', '', ''],
                                 [dTFormat, '#', '', '', '']);
        fg.FrozenCols = 1;
        alignColsInGrid(fg, [1]);
    }

    else if (folderID == 20166) // Beneficios
    {
        headerGrid(fg, ['Verba',
					   'Prestador',
					   'Plano',
					   'Valor',
					   'Quantidade',
					   'N�mero do Cart�o',
                       'Observa��o',
                       'VerbaID',
                       'ValorTitular',
                       'RelPesBeneficioID'], [7,8,9]);
        
        fillGridMask(fg, currDSO, ['^RelPesRecBeneficioID^dso01GridLkp^RelPesRecBeneficioID^Verba*',
                                 '^RelPesRecBeneficioID^dso01GridLkp^RelPesRecBeneficioID^Prestador*',
                                 'RelPesRecBeneficioID',
                                 'Valor',
                                 'Quantidade',
                                 'NumeroCartao',
                                 'Observacao',
                                 '^RelPesRecBeneficioID^dso01GridLkp^RelPesRecBeneficioID^VerbaID*',
                                 '^RelPesRecBeneficioID^dso01GridLkp^RelPesRecBeneficioID^ValorTitular*',
                                 'RelPesBeneficioID'],
                                 ['', '', '', '999999999.99', '9999', '', '', '', '', ''],
                                 ['', '', '', '###,###,###.00', '####', '', '', '', '', '']);

        //pinta celulas readonly
        currDSO.recordset.moveFirst();

        var i;
        var nColValor = getColIndexByColKey(fg, 'Valor');
        var nColQuantidade = getColIndexByColKey(fg, 'Quantidade');
        
        for (i = 1; i < fg.Rows; i++) 
        {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, '^RelPesRecBeneficioID^dso01GridLkp^RelPesRecBeneficioID^VerbaID*')) != 232601) 
            {
                fg.Cell(6, i, nColQuantidade, i, nColQuantidade) = 0XDCDCDC; // cinza
                fg.Cell(6, i, nColValor, i, nColValor) = 0XDCDCDC; // cinza
            }
            else if (fg.ValueMatrix(i, getColIndexByColKey(fg, '^RelPesRecBeneficioID^dso01GridLkp^RelPesRecBeneficioID^ValorTitular*')) != 0)
            {
                fg.Cell(6, i, nColValor, i, nColValor) = 0XDCDCDC; // cinza
            }
        }

        alignColsInGrid(fg, [5]);
    }
    else if (folderID == 20167) // Codigos fabricante
    {
        headerGrid(fg, ['ID',
                       'Fabricante',
                       'C�digo',
                       'Observa��o',
                       'RelPesCodigoID'], [4]);

        fillGridMask(fg, currDSO, ['FabricanteID*',
                                 '^FabricanteID^dso01GridLkp^PessoaID^Fantasia*',
                                 'Codigo',
                                 'Observacao',
                                 'RelPesCodigoID'],
                                 ['', '', '', '', ''],
                                 ['', '', '', '', '']);
        fg.FrozenCols = 1;
        alignColsInGrid(fg, [0]);
    }
    else if (folderID == 20283) // contatos2
    {
        // Acabou de gravar
        if (currDSO == dsoGridProspects) {
            glb_prospectTimer = window.setInterval('refillGrid()', 200, 'JavaScript');
            return;
        }

        headerGrid(fg, ['Tipo',
					   'Nome',
                       'Fantasia',
                       'E-mail',
					   'DDD',
                       'Telefone      ',
                       'DDD',
                       'Celular       ',
                       'Observa��o',
                       'Cont',
						'EstadoID',
						'TipoPessoaID',
						'ClassificacaoID',
						'EmpresaID',
						'Mailing',
						'Emails',
						'PaisID',
						'prospectID',
						'ProprietarioID',
						'AlternativoID',
						'UsuarioID',
						'ContatoID'], [9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21]);

        fillGridMask(fg, currDSO, ['Tipo*',
								 'Nome',
								 'Fantasia',
								 'Email*',
								 'TelefoneDDD',
								 'TelefoneNumero',
								 'CelularDDD',
								 'CelularNumero',
								 'Observacao',
								 'EhContato*',
								 'EstadoID',
								 'TipoPessoaID',
								 'ClassificacaoID',
								 'EmpresaID',
								 'Mailing',
								 'Emails',
								 'PaisID',
								 'prospectID',
						         'ProprietarioID',
						         'AlternativoID',
						         'UsuarioID',
						         'ContatoID'],
                                 ['', '', '', '', '9999', '99999999', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 ['', '', '', '', '####', '########', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);


        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
    }

    //HERALDO GRID
    if (folderID == 20168) // EMPRESA
    {
        headerGrid(fg, ['Empresa',
                        'RelPesEmpresaID'],[1]);

        fillGridMask(fg, currDSO, ['EmpresaID',
                                   'RelPesEmpresaID'],
                                  ['', ''],
                                  ['', '']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }    
        
    else if (folderID == 20169) // Hist. Funcional
    {
        headerGrid(fg, ['Vig�ncia',
                        'Departamento',
                        'Cargo',
                        'N�vel',
                        'Salario',
                        'Observa��o',
                        'OK',
                        'Alteracoes',
                        'RelPesHistoricoID'], [7, 8]);

        var sReadOnly = '*';
        
        if (nUserID == 1142 || nUserID == 1820)// Ids da Simone Mazoco e Keila Silva para editar todos os campos.
            sReadOnly = '';
            
        fillGridMask(fg, currDSO, [
                    'dtVigencia' + sReadOnly,
                    'DepartamentoID' + sReadOnly,
                    'CargoID' + sReadOnly,
                    'Nivel' + sReadOnly,
                    'Salario' + sReadOnly,
                    'Observacao' + sReadOnly,
                    'OK' + sReadOnly,
                    'Alteracoes' + sReadOnly,
                    'RelPesHistoricoID' ],
                   ['', '', '', '', '999999999.99', '', '', '', ''],
                   ['', '', '', '', '###,###,###.00', '', '', '', '']);
        
        alignColsInGrid(fg, [3, 4]);

        currDSO.recordset.moveFirst();

        var i = 1;
        
        //Linhas
        while (!currDSO.recordset.EOF) 
        {
            var j = 2;
            var caracter = '';
            var nCaracteres = 1;
            var sCampo = currDSO.recordset['Alteracoes'].value;
            var bgColorYellow = 0x00ffff;
            var colunaGrid = '';
            var colunaGridCompare = '';

            //Colunas
            for (y = 0; y <= (fg.cols - 1); y++) 
            {
                //Pega nome da coluna do grid.
                colunaGrid = fg.ColKey(y);
                //Verifica se tem * e retira.
                if (colunaGrid.substr((colunaGrid.length - 1), 1) == '*')
                    colunaGridCompare = colunaGrid.substr(0, (colunaGrid.length - 1));

                //Trata campos lookup
                if (colunaGrid.substr(0, 1) == '^') 
                {
                    if (colunaGrid.substr((colunaGrid.length - 1), 1) == '*') 
                        j = 2;

                    else
                        j = 1;
                    
                    nCaracteres = 0;
                    caracter = '';
                    
                    while (caracter != '^')
                    {
                        caracter = colunaGrid.substr(colunaGrid.length - j, 1);

                        nCaracteres++;
                        j++;
                    }
                    if (colunaGrid.substr((colunaGrid.length - 1), 1) == '*')
                        colunaGridCompare = colunaGrid.substr((colunaGrid.length - nCaracteres), (nCaracteres - 1));
                    else
                        colunaGridCompare = colunaGrid.substr((colunaGrid.length - nCaracteres), nCaracteres);
                }

                //Verifica se a coluna esta na variavel. se tiver pinta o grid.
                if (sCampo.search('/' + colunaGridCompare + '/') != -1)
                    fg.Cell(6, i, getColIndexByColKey(fg, colunaGrid), i, getColIndexByColKey(fg, colunaGrid)) = bgColorYellow;
            }

            currDSO.recordset.MoveNext();
            i++;
        }
    }

    else if (folderID == 32036) // F�rias
    {
        headerGrid(fg, ['Data',
                        'Situa��o',
                        'Dias',
                        'Saldo',
                        'Observa��o',
                        'RelacaoID',
                        'RelPesFeriasID'], [5,6]);
            
        fillGridMask(fg, currDSO, ['dtData',
                                   'TipoFeriasID',
                                   'dias',
                                   'Saldo*',
                                   'Observacao',
                                   'RelacaoID',
                                   'RelPesFeriasID'],
                                   ['99/99/9999', '', '#9999', '', '', '', ''],
                                   [dTFormat, '', '', '#####', '', '', '']);

        //glb_aCelHint = [[0, 1, '']];

        // Pinta campos negativos com vermelho
        var nColDias = getColIndexByColKey(fg, 'Dias');
        var nColSaldo = getColIndexByColKey(fg, 'Saldo*');
        var nVerde = 0X90EE90;
        var nAmarelo = 0X8CE6F0;
        var nVermelho = 0X0000FF;
        
        for (i = 1; i < fg.Rows; i++) 
        {
            if (fg.ValueMatrix(i, nColDias) < 0) 
            {
                fg.Select(i, nColDias, i, nColDias);
                fg.FillStyle = 1;
                fg.CellForeColor = nVermelho;
            }
            
            if (fg.ValueMatrix(i, nColSaldo) < 0)
            {
                fg.Select(i, nColSaldo, i, nColSaldo);
                fg.FillStyle = 1;
                fg.CellForeColor = nVermelho;
            }

            if (fg.ValueMatrix(i, nColSaldo) == 30)
            {
                fg.Cell(6, i, nColSaldo, i, nColSaldo) = nVerde;
            }

            if ((fg.ValueMatrix(i, nColSaldo) < 0) || (fg.ValueMatrix(i, nColSaldo) > 30))
            {
                fg.Cell(6, i, nColSaldo, i, nColSaldo) = nAmarelo;
            }
        }

        if (fg.Rows > 1)
            fg.Row = 1;

        alignColsInGrid(fg, [2, 3]);
        
        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

    else if (folderID == 20014) // Atributos
    {
        headerGrid(fg, ['Atributo',
                        'Valor',
                        'Data',
                        'Usu�rio',
                        'Observa��o',
                        'FormID',
                        'SubFormID',
                        'RegistroID',
                        'Data',
                        'Usuario',
                        'AtributoID'], [5,6,7,8,9,10]);

        fillGridMask(fg, currDSO, ['TipoAtributoID',
                                  'Valor',
                                  'Data*',
                                  '^UsuarioID^dso01GridLkp^PessoaID^Fantasia*',
                                  'Observacao',
                                  'FormID',
                                  'SubFormID',
                                  'RegistroID',
                                  'Data',
                                  'UsuarioID',
                                  'AtributoID'],
                                  ['','', '', '', '', '', '', '', '', '', ''],
                                  ['','', '', '', '', '', '', '', '', '', '']);

        //alignColsInGrid(fg, [2, 3, 4, 5, 6]);
    }
}

function teste_DSC()
{
	;
}

function refillGrid()
{
	if (glb_prospectTimer != null)
	{
	    window.clearInterval(glb_prospectTimer);
	    glb_prospectTimer = null;
	}
	
	__btn_REFR('inf');
}

function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear();

    return (s);
}