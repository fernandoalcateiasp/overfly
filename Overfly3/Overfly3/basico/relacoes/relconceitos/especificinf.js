/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Recursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.RelacaoID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM RelacoesConceitos a WITH(NOLOCK) WHERE '+
        sFiltro + 'a.RelacaoID = '+idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21040) // Empresas
    {
        dso.SQL = 'SELECT a.* FROM RelacoesConceitos_Empresas a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+idToFind;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21041) // Incidencia de Impostos
    {
        dso.SQL = 'SELECT a.* FROM RelacoesConceitos_Impostos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+idToFind;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21042) // Cotacoes
    {
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'RelacoesConceitos_Cotacoes';
        glb_aCmbKeyPesq = [['Data','DadtCotacao'],['Compra','NaCotacaoCompra'],
                           ['Venda','NaCotacaoVenda']];
        // Final da automacao =========================================================
        
        // paginacao de grid
        return execPaging(dso, 'RelacaoID', idToFind, folderID);
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
       
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Empresas
    else if ( pastaID == 21040 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.PessoaID, a.Fantasia ' +
                      'FROM Pessoas a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK) ' +
                      'WHERE b.EstadoID=2 AND b.TipoRelacaoID = 12 ' +
                      'AND b.ObjetoID=999 AND b.SujeitoID=a.PessoaID ' +
                      'ORDER BY a.Fantasia';
        }
    }

    // Incidencia de Impostos
    else if ( pastaID == 21041 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.ConceitoID, a.Imposto ' +
                      'FROM Conceitos a WITH(NOLOCK) ' +
                      'WHERE a.EstadoID=2 AND a.TipoConceitoID = 306 ' +
                      'ORDER BY a.Imposto';
        }
    }

    // Cotacoes
    else if (pastaID == 21042) 
    {
        if (dso.id == 'dso01GridLkp') 
        {
            dso.SQL = 'SELECT DISTINCT a.PessoaID AS UsuarioID, a.Fantasia AS Usuario ' +
                      'FROM Pessoas a WITH(NOLOCK) ' +
                      'INNER JOIN RelacoesConceitos_Cotacoes b WITH(NOLOCK) ON (a.PessoaID = ISNULL(b.UsuarioID, 999)) ' +
                      'WHERE b.RelacaoID = ' + nRegistroID +
                      ' ORDER BY a.Fantasia';
        }
    }       

}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    if ( nTipoRegistroID == 41 )
	{
        vPastaName = window.top.getPastaName(21040);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 21040); //Empresas
    }

    if ( nTipoRegistroID == 48 )
	{
        vPastaName = window.top.getPastaName(21041);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 21041); //Incidencia de Impostos
    }

    if ( nTipoRegistroID == 49 )
	{
        vPastaName = window.top.getPastaName(21042);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 21042); //Cota��es
    }
 	
    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 21040) // Empresas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID]);
        else if (folderID == 21041) // Incidencia de Impostos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID]);
        else if (folderID == 21042) // Cotacoes
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,2], ['RelacaoID', registroID]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 21040)
    {
        headerGrid(fg,['Empresa', 'RelacaoID', 'RelConEmpresaID'], [1,2]);
        fillGridMask(fg,currDSO,['EmpresaID', 'RelacaoID', 'RelConEmpresaID']
                                     ,['','','']);
    }    
    else if (folderID == 21041)
    {
        headerGrid(fg,['Imposto', 'RelacaoID', 'RelConImpostoID'], [1,2]);
        fillGridMask(fg,currDSO,['ImpostoID', 'RelacaoID', 'RelConImpostoID']
                                     ,['','','']);
    }    
    else if  (folderID == 21042)  
    {
        headerGrid(fg, ['Data           Hora     ',
                        'Compra',
                        'Venda',
                        'Observa��o',
                        'RelacaoID',
                        'Usu�rio',
                        'RelConCotacaoID'], [4,6]);

        fillGridMask(fg, currDSO, ['dtCotacao',
                                   'CotacaoCompra',
                                   'CotacaoVenda',
                                   'Observacao',
                                   'RelacaoID',
                                  // 'UsuarioID',
                                   '^UsuarioID^dso01GridLkp^UsuarioID^Usuario*',
                                   'RelConCotacaoID'],
                                   ['99/99/9999 99:99','99999999.9999999','99999999.9999999','','','',''],
                                   [dTFormat + ' hh:mm','##,###,###.#######','##,###,###.#######','','','','']);
    }    
}
