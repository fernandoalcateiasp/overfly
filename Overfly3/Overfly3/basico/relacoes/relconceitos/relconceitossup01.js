/********************************************************************
relconceitossup01.js

Library javascript para o relconceitossup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// controla o retorno do servidor dos dados de combos dinamicos
// (selSujeitoID e selObjetoID)
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFramWork;

// guarda id do combo de lupa a preencher
var glb_cmbLupaID;

// Guarda o tipo do registro selecionado para ser usado no select
// que traz o registro devolta na gravacao, pois este form tem
// Identity calculado por trigger
var glb_RelConceitosTipoRegID = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoSup01 = new CDatatransport('dso01JoinSup');
var dsoCmbDynamic01 = new CDatatransport('dso01JoinSup');
var dsoCmbDynamic02 = new CDatatransport('dso01JoinSup');
var dsoCmbsLupa = new CDatatransport('dso01JoinSup');
var dsoStateMachine = new CDatatransport('dso01JoinSup');
var dsoEstaticCmbs = new CDatatransport('dso01JoinSup');

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()


FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )

FUNCOES ESPECIFICAS DO FORM:

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1']]);

    
    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/basico/relacoes/relconceitos/relconceitosinf01.asp',
                              SYS_PAGESURLROOT + '/basico/relacoes/relconceitos/relconceitospesqlist.asp');                          
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01',  ['divSup02_01',
                                         'divSup02_02',
                                         'divSup02_03',
                                         'divSup02_04',
                                         'divSup02_05',
                                         'divSup02_06',
                                         'divSup02_07',
                                         'divSup02_08'],
                                        [41, 42, 43, 45, 47, 48, 53, 49]);

    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RelacaoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoRelacaoID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01'],
                      [2,'divSup02_01'],
                      [2,'divSup02_02'],
                      [2,'divSup02_03'],
                      [2,'divSup02_04'],
                      [2,'divSup02_05'],
                      [2,'divSup02_06'],
                      [2,'divSup02_07'],
                      [2,'divSup02_08']]);
                          
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
						  ['lblEstadoID','txtEstadoID',2,1],
                          ['lblTipoRegistroID','selTipoRegistroID',20,1],
                          ['lblSujeitoID','selSujeitoID',25,1],
                          ['btnFindSujeito','btn',24,1],
		                  ['lblObjetoID','selObjetoID',25,1],
		                  ['btnFindObjeto','btn',24,1]]);
                  
    //@@ *** divSup02_01 ***/
    adjustElementsInForm([['lblDefault','chkDefault',3,2]],['lblHrEstrutura','hrEstrutura']);

    //@@ *** divSup02_02 ***/
    adjustElementsInForm([['lblQuantidade','txtQuantidade',4,2],['lblEssencial','chkEssencial',3,2]],['lblHrComposicao','hrComposicao']);

    //@@ *** divSup02_03 ***/
    adjustElementsInForm([['lblOrdem','txtOrdem',4,2],
                          ['lblValorDefault','txtValorDefault',80,2],
                          ['lblFichaTecnica','chkFichaTecnica',3,2],
                          ['lblRecebimento','chkRecebimento',3,2,-5],
                          ['lblDescricao','chkDescricao',3,2,-5],
                          ['lblDescricaoComplementar', 'chkDescricaoComplementar',3,2,-5],
                          ['lblCaracteristicaObrigatoria', 'chkCaracteristicaObrigatoria', 3, 2, -5],
                          ['lblImagens', 'chkImagens',3,2,-5],
                          ['lblPesosMedidas', 'chkPesosMedidas',3,2,-5],
                          ['lblPesquisaWeb','chkPesquisaWeb',3,2,-5]],['lblHrCaracteristica','hrCaracteristica']);

    //@@ *** divSup02_04 ***/
    adjustElementsInForm([['lblQuantidade2','txtQuantidade2',4,2],
						  ['lblObrigatorio','chkObrigatorio',3,2],
						  ['lblReciproco','chkReciproco',3,2,-5]],['lblHrRequerido','hrRequerido']);

    //@@ *** divSup02_05 ***/
    adjustElementsInForm([['lblOrdem_2','txtOrdem_2',4,2]],['lblHrAlternativo','hrAlternativo']);

    //@@ *** divSup02_06 ***/
    adjustElementsInForm([['lblIncideCompra','chkIncideCompra',3,2],
                          ['lblIncideVenda','chkIncideVenda',3,2]],['lblHrImposto','hrImposto']);

    //@@ *** divSup02_07 ***/
    adjustElementsInForm([['lblReciproco2','chkReciproco2',3,2]],['lblHrCrossSeling','hrCrossSeling']);
    
    //@@ *** divSup02_08 ***/
    adjustElementsInForm([['lblAtualizacaoAutomatica','chkAtualizacaoAutomatica',3,2],
						  ['lblFrequenciaAtualizacao','txtFrequenciaAtualizacao',7,2],
						  ['lblVariacaoMinimaAtualizacao', 'txtVariacaoMinimaAtualizacao', 6, 2]], ['lblHrConfCotacoes', 'HrConfCotacoes']);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWRELCONCEITOS')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids
        window.top.focus();
        
        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') )
        startDynamicCmbs();
    else    
        // volta para a automacao    
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    clearAndLockCmbsDynamics(btnClicked);
    
    // Ajusta os labels de sujeito e objeto
    // Apenas nos forms de relacoes
    changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID', 'lblObjetoID', null);

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    if ( cmbID == 'selTipoRegistroID' )
    {
        clearComboEx(['selSujeitoID','selObjetoID']);
        selSujeitoID.disabled = true;
        selObjetoID.disabled = true;
        lockBtnLupa(btnFindSujeito, true);
        lockBtnLupa(btnFindObjeto, true);

        if ( cmbID.selectedIndex != -1 )
            lockBtnLupa(btnFindSujeito, false);
    }
    else if ( cmbID == 'selSujeitoID' )
    {
        clearComboEx(['selObjetoID']);
        selObjetoID.disabled = true;
        lockBtnLupa(btnFindObjeto, true);
        
        if ( cmb.selectedIndex != -1 )
            // destrava o botao de lupa do objeto
            lockBtnLupa(btnFindObjeto, false);
    }    

    // Ajusta os labels de sujeito e objeto
    // Apenas nos forms de relacoes
    if ( (cmbID == 'selTipoRegistroID') || (cmbID == 'selSujeitoID') || (cmbID == 'selObjetoID') )
        changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID',
                           'lblObjetoID', 'selTipoRegistroID');

    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    //@@
    var cmbID = '';
    var nTipoRelacaoID = '';
    var nRegExcluidoID = '';
    
    if ( btnClicked.id == 'btnFindSujeito' )
    {
        nTipoRelacaoID = selTipoRegistroID.value;
        nRegExcluidoID = '';
        showModalRelacoes(window.top.formID, 'S', 'selSujeitoID' , 'SUJ', getLabelNumStriped(lblSujeitoID.innerText) , nTipoRelacaoID, nRegExcluidoID);
        return null;
    }
    else if ( btnClicked.id == 'btnFindObjeto' )
    {
        if ( selSujeitoID.selectedIndex == -1 )
        {
            if ( window.top.overflyGen.Alert( 'Selecionar primeiro o combo ' + lblSujeitoID.innerText + '!') == 0 )
                return null;

            window.focus();
            selObjetoID.focus();
            return null;    
        }
        
        nTipoRelacaoID = selTipoRegistroID.value;    
        nRegExcluidoID = selSujeitoID.value;    

        showModalRelacoes(window.top.formID, 'S', 'selObjetoID' , 'OBJ', getLabelNumStriped(lblObjetoID.innerText) , nTipoRelacaoID, nRegExcluidoID);
        return null;
    }    
    // Nao mexer - Inicio de automacao ==============================
    // Invoca janela modal
    loadModalOfLupa(cmbID, null);
    // Final de Nao mexer - Inicio de automacao =====================

}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selSujeitoID e selObjetoID)
    glb_CounterCmbsDynamics = 2;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selSujeitoID)
    setConnection(dsoCmbDynamic01);
    
    dsoCmbDynamic01.SQL = 'SELECT ConceitoID,Conceito ' +
                          'FROM Conceitos WITH(NOLOCK) ' +
                          'WHERE ConceitoID = ' +  dsoSup01.recordset['SujeitoID'].value;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    // parametrizacao do dso dsoCmbDynamic02 (designado para selObjetoID)
    setConnection(dsoCmbDynamic02);
    
    dsoCmbDynamic02.SQL = 'SELECT ConceitoID,Conceito ' +
                          'FROM Conceitos WITH(NOLOCK) ' +
                          'WHERE ConceitoID = ' +  dsoSup01.recordset['ObjetoID'].value;
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();
}

/********************************************************************
Funcao disparada pelo programado.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selSujeitoID,selObjetoID];
    var aDSOsDunamics = [dsoCmbDynamic01,dsoCmbDynamic02];
	var idso;
	
    // Inicia o carregamento de combos dinamicos (selSujeitoID, selObjetoID )
    //
    clearComboEx(['selSujeitoID','selObjetoID']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (idso=0; idso<=1; idso++)
        {
            while (! aDSOsDunamics[idso].recordset.EOF )
            {
                optionStr = aDSOsDunamics[idso].recordset['Conceito'].value;
	        	optionValue = aDSOsDunamics[idso].recordset['ConceitoID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[idso].add(oOption);
                aDSOsDunamics[idso].recordset.moveNext();
            }
        }

        // volta para a automacao
	    finalOfSupCascade(glb_BtnFromFramWork);
	}
	return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    var empresa = getCurrEmpresaData();
    
    if ( controlBar == 'SUP' ) {
        // Documentos
        if (btnClicked == 1) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }
        else if ( btnClicked == 3 )
			window.top.openModalControleDocumento('S', '', 751, null, '241', 'T');
        // Detalhar sujeito
        else if ( btnClicked == 4 )
        {
            // Manda o id do conceito a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWCONCEITO', new Array(empresa[0], selSujeitoID.value));       
        }
        // Detalhar objeto
        else if ( btnClicked == 5 )
        {
            // Manda o id do conceito a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWCONCEITO', new Array(empresa[0], selObjetoID.value));       
        }
    }
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Identity Calculado
    if ( (controlBar == 'SUP') && (btnClicked == 'SUPOK') )
        glb_RelConceitosTipoRegID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;

    // Para prosseguir a automacao retornar null
    return null;
}
/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de relacoes
    if ( idElement.toUpperCase() == 'MODALRELACOESHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            fillFieldsByRelationModal(param2);
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }        
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 0);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,0]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Sujeito', 'Detalhar Objeto', '']);
}

/********************************************************************
Funcao criada pelo programador.
Limpa e trava os combos dinamicos quando o usuario clicar em inclusao

Parametro:
btnClicked  -> Ultimo botao clicado da barra superior

Retorno:
nenhum
********************************************************************/
function clearAndLockCmbsDynamics(btnClicked)
{
    if (btnClicked == 'SUPINCL')
    {
        clearComboEx(['selSujeitoID','selObjetoID']);
        selSujeitoID.disabled = true;
        selObjetoID.disabled = true;
        
        // trava o botao de lupa do sujeito
        lockBtnLupa(btnFindSujeito, true);
 
        // trava o botao de lupa do objeto
        lockBtnLupa(btnFindObjeto, true);
 
     }
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
            aData[0] - quem e na relacao ('SUJ' ou 'OBJ')
            aData[1] - ID do tipo da relacao
            aData[2] - ID do campo do subform corrente
            aData[3] - ID selecionado no grid da modal
            aData[4] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData)
{
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;
    
    elem = window.document.getElementById(aData[2]);
    
    if ( elem == null )
        return;
    
    clearComboEx([elem.id]);
        
    oldDataSrc = elem.dataSrc;
    oldDataFld = elem.dataFld;
    elem.dataSrc = '';
    elem.dataFld = '';

    // Preencher o combo selSujeitoID
    if ( aData[2] == 'selSujeitoID' )
        dsoSup01.recordset['SujeitoID'].value = aData[3];
    // Preencher o combo selObjetoID
    else if ( aData[2] == 'selObjetoID' )
        dsoSup01.recordset['ObjetoID'].value = aData[3];

    oOption = document.createElement("OPTION");
    oOption.text = aData[4];
    oOption.value = aData[3];
    elem.add(oOption);
    elem.selectedIndex = 0;
        
    elem.dataSrc = oldDataSrc;
    elem.dataFld = oldDataFld;       

    if ( aData.length > 5 )
        addSexOrServ = aData[5];
                           
    optChangedInCmbSujOrObj(elem, addSexOrServ);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
cmb     - referencia do combo
addSexOrServ  - ID do sexo ou descricao do servico

Retorno:
nenhum
********************************************************************/
function optChangedInCmbSujOrObj(cmb, addSexOrServ)
{
    var cmbID = cmb.id;
 
    if ( cmbID == 'selSujeitoID' )
    {
        clearComboEx(['selObjetoID']);
        selObjetoID.disabled = true;
        lockBtnLupa(btnFindObjeto, true);
        
        if ( cmb.selectedIndex != -1 )
        {
            // destrava o botao de lupa do objeto
            lockBtnLupa(btnFindObjeto, false);
        }    
    }

    if ( (cmbID == 'selSujeitoID') || (cmbID == 'selObjetoID') )
        changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID',
                           'lblObjetoID', 'selTipoRegistroID',
                           selSujeitoID.value, selObjetoID.value);
}
