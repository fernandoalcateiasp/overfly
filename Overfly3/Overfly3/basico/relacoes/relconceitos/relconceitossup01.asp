<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="relconceitossup01Html" name="relconceitossup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf    
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relconceitos/relconceitossup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relconceitos/especificsup.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="relconceitossup01Body" name="relconceitossup01Body" LANGUAGE="javascript" onload="return window_onload()">
    
    <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">        
        
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoRelacaoID"></select>    
	    <p id="lblSujeitoID" name="lblSujeitoID" class="lblGeneral">Sujeito</p>
	    <select id="selSujeitoID" name="selSujeitoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="SujeitoID"></select>
        <input type="image" id="btnFindSujeito" name="btnFindSujeito" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
        <input type="image" id="btnFindObjeto" name="btnFindObjeto" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
	    <p id="lblObjetoID" name="lblObjetoID" class="lblGeneral">Objeto</p>
	    <select id="selObjetoID" name="selObjetoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ObjetoID"></select>
	</div>

    <!-- Primeiro div secundario superior - Estrutura -->    
    <div id="divSup02_01" name="divSup02_02" class="divExtern">
		<p id="lblHrEstrutura" name="lblHrEstrutura" class="lblGeneral">Estrutura</p>
		<hr id="hrEstrutura" name="hrEstrutura" class="lblGeneral"></hr>
	    <p id="lblDefault" name="lblDefault" class="lblGeneral">Default</p>
        <input type="checkbox" id="chkDefault" name="chkDefault" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EhDefault" title="� a Rela��o default?"></input>
    </div>

    <!-- Segundo div secundario superior - Composi��o -->    
    <div id="divSup02_02" name="divSup02_02" class="divExtern">
		<p id="lblHrComposicao" name="lblHrComposicao" class="lblGeneral">Composi��o</p>
		<hr id="hrComposicao" name="hrComposicao" class="lblGeneral"></hr>
	    <p id="lblQuantidade" name="lblQuantidade" class="lblGeneral">Quantidade</p>
	    <input type="text" id="txtQuantidade" name="txtQuantidade" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Quantidade"></input>
        <p class="lblGeneral"  id="lblEssencial">Essencial</p>
        <input type="checkbox" id="chkEssencial" name="chkEssencial" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Essencial" title="� essencial? "></input>
    </div>

    <!-- Terceiro div secundario superior - Caracteristica -->    
    <div id="divSup02_03" name="divSup02_03" class="divExtern">
		<p id="lblHrCaracteristica" name="lblHrCaracteristica" class="lblGeneral">Caracter�stica</p>
		<hr id="hrCaracteristica" name="hrCaracteristica" class="lblGeneral"></hr>
	    <p id="lblOrdem" name="lblOrdem" class="lblGeneral">Ordem</p>
	    <input type="text" id="txtOrdem" name="txtOrdem" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem"></input>
	    <p id="lblValorDefault"  name="lblValorDefault" class="lblGeneral">Valor Default</p>
	    <input type="text" id="txtValorDefault"  name="txtValorDefault" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ValorDefault"></input>
        <p id="lblFichaTecnica" name="lblFichaTecnica" class="lblGeneral">FT</p>
        <input type="checkbox" id="chkFichaTecnica" name="chkFichaTecnica" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FichaTecnica" title="Esta Caracter�stica ser� usada na Ficha T�cnica do produto?"></input>
        <p  id="lblRecebimento" name="lblRecebimento" class="lblGeneral">Rec</p>
        <input type="checkbox" id="chkRecebimento" name="chkRecebimento" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Recebimento" title="Esta Caracter�stica ser� usada no Recebimento do produto?"></input>
        <p  id="lblDescricao" name="lblDescricao" class="lblGeneral">Desc</p>
        <input type="checkbox" id="chkDescricao" name="chkDescricao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Descricao" title="Esta Caracter�stica ser� usada para compor a Descri��o do produto?"></input>
        <p  id="lblDescricaoComplementar" name="lblDescricaoComplementar" class="lblGeneral">DC</p>
        <input type="checkbox" id="chkDescricaoComplementar" name="chkDescricaoComplementar" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescricaoComplementar" title="Esta Caracter�stica ser� usada para compor a Descri��o Complementar do produto?"></input>
        <p  id="lblCaracteristicaObrigatoria" name="lblCaracteristicaObrigatoria" class="lblGeneral">Obrig</p>
        <input type="checkbox" id="chkCaracteristicaObrigatoria" name="chkCaracteristicaObrigatoria" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Obrigatorio" title="Esta Caracter�stica � obrigatoria para essa fam�lia?"></input>
        <p  id="lblImagens" name="lblImagens" class="lblGeneral">Imagens</p>
        <input type="checkbox" id="chkImagens" name="chkImagens" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Imagens" title="Esta Caracter�stica ser� usada para replicar imagens para os produtos da mesma s�rie desta fam�lia?"></input>
        <p  id="lblPesosMedidas" name="lblPesosMedidas" class="lblGeneral">PM</p>
        <input type="checkbox" id="chkPesosMedidas" name="chkPesosMedidas" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PesosMedidas" title="Esta Caracter�stica ser� usada para replicar pesos e medidas para os produtos da mesma s�rie desta fam�lia?"></input>
        <p  id="lblPesquisaWeb" name="lblPesquisaWeb" class="lblGeneral">Web</p>
        <input type="checkbox" id="chkPesquisaWeb" name="chkPesquisaWeb" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PesquisaWeb" title="Esta Caracter�stica ser� usada na pesquisa da Web?"></input>
   </div>

    <!-- Quarto div secundario superior - Requerido -->    
    <div id="divSup02_04" name="divSup02_04" class="divExtern">    
 		<p id="lblHrRequerido" name="lblHrRequerido" class="lblGeneral">Requerido</p>
		<hr id="hrRequerido" name="hrRequerido" class="lblGeneral"></hr>
	    <p id="lblQuantidade2" name="lblQuantidade2" class="lblGeneral">Quantidade</p>
	    <input type="text" id="txtQuantidade2" name="txtQuantidade2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Quantidade"></input>
        <p id="lblObrigatorio" name="lblObrigatorio" class="lblGeneral">Obrigat�rio</p>
        <input type="checkbox" id="chkObrigatorio" name="chkObrigatorio" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Essencial" title="� obrigat�rio?"></input>
        <p id="lblReciproco" name="lblReciproco" class="lblGeneral">Rec�proco</p>
        <input type="checkbox" id="chkReciproco" name="chkReciproco" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Reciproco" title="O requerido requer o requerente?"></input>
    </div>

    <!-- Quinto div secundario superior - Alternativo -->    
    <div id="divSup02_05" name="divSup02_05" class="divExtern">    
 		<p id="lblHrAlternativo" name="lblHrAlternativo" class="lblGeneral">Alternativo</p>
		<hr id="hrAlternativo" name="hrAlternativo" class="lblGeneral"></hr>
	    <p id="lblOrdem_2" name="lblOrdem_2" class="lblGeneral">Ordem</p>
	    <input type="text" id="txtOrdem_2" name="txtOrdem_2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Ordem"></input>
    </div>

    <!-- Sexto div secundario superior - Imposto -->    
    <div id="divSup02_06" name="divSup02_06" class="divExtern">    
 		<p id="lblHrImposto" name="lblHrImposto" class="lblGeneral">Imposto</p>
		<hr id="hrImposto" name="hrImposto" class="lblGeneral"></hr>
        <p id="lblIncideCompra" name="lblIncideCompra" class="lblGeneral">Compra</p>
        <input type="checkbox" id="chkIncideCompra" name="chkIncideCompra" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="IncideCompra" title="Incide na compra?"></input>
        <p id="lblIncideVenda" name="lblIncideVenda" class="lblGeneral">Venda</p>
        <input type="checkbox" id="chkIncideVenda" name="chkIncideVenda" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="IncideVenda" title="Incide na venda?"></input>
    </div>
    
    <!-- Setimo div secundario superior - Cross seling -->    
    <div id="divSup02_07" name="divSup02_07" class="divExtern">    
 		<p id="lblHrCrossSeling" name="lblHrCrossSeling" class="lblGeneral">Cross seling</p>
		<hr id="hrCrossSeling" name="hrCrossSeling" class="lblGeneral"></hr>
        <p id="lblReciproco2" name="lblReciproco2" class="lblGeneral">Rec�proco</p>
        <input type="checkbox" id="chkReciproco2" name="chkReciproco2" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Reciproco" title="O requerido requer o requerente?"></input>
    </div>
    
    <!-- Oitavo div secundario superior - Moedas -->    
    <div id="divSup02_08" name="divSup02_08" class="divExtern">  
        <p id="lblHrConfCotacoes" name="lblHrConfCotacoes" class="lblGeneral">Configura��es de Cota��es</p>  
        <hr id="HrConfCotacoes" name="HrConfCotacoes" class="lblGeneral"></hr>
 		<p id="lblAtualizacaoAutomatica" name="lblAtualizacaoAutomatica" class="lblGeneral" title="Atualiza cota��o automaticamente?">Aut</p>
 		<input type="checkbox" id="chkAtualizacaoAutomatica" name="chkAtualizacaoAutomatica" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="AtualizacaoAutomatica" title="Atualiza cota��o automaticamente?"></input>
		<p id="lblFrequenciaAtualizacao" name="lblFrequenciaAtualizacao" class="lblGeneral" title="Frequ�ncia de atualiza��o (em minutos)">Frequ�ncia</p>
		<input type="text" id="txtFrequenciaAtualizacao" name="txtFrequenciaAtualizacao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FrequenciaAtualizacao" title="Frequ�ncia de atualiza��o (em minutos)"></input>
		<p id="lblVariacaoMinimaAtualizacao" name="lblVariacaoMinimaAtualizacao" class="lblGeneral" title="Percentual m�nimo para atualiza��o da cota��o (%)">Varia��o</p>
		<input type="text" id="txtVariacaoMinimaAtualizacao" name="txtVariacaoMinimaAtualizacao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="VariacaoMinimaAtualizacao" title="Percentual m�nimo para atualiza��o da cota��o (%)"></input>
    </div>
   
</body>

</html>
