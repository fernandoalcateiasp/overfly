/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de relpessoaserecursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();
    var nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'SujeitoID' + '\'' + '].value');

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) || (folderID == 20061) ) // Prop/Altern/Observacoes/Setup
    {
        dso.SQL = 'SELECT a.*, ' +
               '(SELECT CONVERT(VARCHAR, a.dtInicio , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, a.dtInicio, 108) ' +
					'FROM RelacoesPesRec b WITH(NOLOCK) ' +
					'WHERE a.RelacaoID=b.RelacaoID) AS V_dtInicio, ' +
               '(SELECT CONVERT(VARCHAR, a.dtFim , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, a.dtFim, 108) ' +
					'FROM RelacoesPesRec b WITH(NOLOCK) ' +
					'WHERE a.RelacaoID=b.RelacaoID) AS V_dtFim, ' +
				'CONVERT(VARCHAR, a.dtContabilidade, ' + DATE_SQL_PARAM + ') as V_dtContabilidade, ' +
                'CONVERT(VARCHAR, dtBalanco, ' + DATE_SQL_PARAM + ') as V_dtBalanco,' +
                'CONVERT(VARCHAR, dtConciliacaoInicio, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoInicio,' +
                'CONVERT(VARCHAR, dtConciliacaoFim, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoFim, ' +
                'CONVERT(VARCHAR, dtDocumentoInicio, ' + DATE_SQL_PARAM + ') as V_dtDocumentoInicio, ' +
                'CONVERT(VARCHAR, dtConciliacaoRetificacao, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoRetificacao ' +
			    'FROM RelacoesPesRec a WITH(NOLOCK) WHERE ' + sFiltro + 'a.RelacaoID = ' + idToFind;
        return 'dso01JoinSup' + '_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20053) // Perfis de Usuario
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPesRec_Perfis a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ' +
        'ORDER BY (SELECT TOP 1 Fantasia FROM Pessoas WITH(NOLOCK) WHERE a.EmpresaID = PessoaID), Ordem '; 
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20064) // Educacao
    {
        dso.SQL = 'SELECT a.PessoaID, a.EducacaoID, a.Duracao, a.Concluido, a.Cursando ' +
			   'FROM Pessoas a WITH(NOLOCK) WHERE ' + sFiltro + ' ' +
			   '(SELECT COUNT(*) FROM RelacoesPesRec WITH(NOLOCK) ' +
					'WHERE (RelacaoID = ' + idToFind + ' AND a.PessoaID = SujeitoID)) > 0 ';
        return 'dso01JoinSup' + '_DSC';
    }
    else if (folderID == 20065) // Experiencia
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPesRec_Experiencias a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ' + 
        'ORDER BY ISNULL(a.dtFim, GETDATE()) DESC, a.PerfilID '; 
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20066) // Habilidades
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPesRec_Habilidades a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ' +
        'ORDER BY (SELECT TOP 1 Competencia FROM Competencias WITH(NOLOCK) WHERE a.HabilidadeID = CompetenciaID) '; 
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20054) // Descontos
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPesRec_Descontos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ORDER BY a.Faixa';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20055) // Moedas
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPesRec_Moedas a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ORDER BY a.RelPesRecMoedaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28083) // Cr�dito
    {
        dso.SQL = 'SELECT a.* ' +
            'FROM RelacoesPesRec_Creditos a WITH(NOLOCK) ' +
            'INNER JOIN RelacoesPesRec b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
            'WHERE ((dbo.fn_Pessoa_Localidade(b.SujeitoID, 1, NULL, NULL)) = (dbo.fn_Pessoa_Localidade(' + nSujeitoID + ', 1, NULL, NULL))) ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20056) // Politicas Financeiras
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPesRec_Financ a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ORDER BY a.RelPesRecFinancID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20057) // Notas Fiscais
    {
        dso.SQL = 'SELECT a.* ' +
            'FROM RelacoesPesRec_DocumentosFiscais a WITH(NOLOCK) ' +
            'WHERE ' + sFiltro + ' a.RelacaoID = ' + idToFind;
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20058) // Cobranca Bancaria
    {
        dso.SQL = 'SELECT a.* FROM RelacoesPesRec_Cobranca a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY a.EstadoID, a.RelPesContaID, ContaCobranca';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20059) // Atributos
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_Atributos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind +
			'ORDER BY (SELECT Ordem FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ItemID=a.AtributoID)';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20060) // Classificacao Produto
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_ClassificacaoProduto a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + 
			'ORDER BY a.RelPesRecClassificacaoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20062) // Classificacao Fornecedor
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_ClassificacaoFornecedor a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + 
			'ORDER BY a.RelPesRecClassificacaoID';
        return 'dso01Grid_DSC';
    }    
    else if (folderID == 20074) // Classifica��es
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_Classificacoes a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + 
			'ORDER BY a.RelPesRecClassificacaoID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20063) // Criterios Avaliacao
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_CriteriosAvaliacao a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + 
			'ORDER BY a.RelPesRecCriterioID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20067) // Treinamentos
    {
		var nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'SujeitoID' + '\'' + '].value');

        dso.SQL = 'SELECT a.dtData, c.Competencia, a.RETID, d.RecursoAbreviado AS Estado, b.Nota, b.Eficaz, b.dtAvaliacao, ' +
				'e.Fantasia AS Responsavel, b.Observacao ' +
			'FROM RET a WITH(NOLOCK), RET_Participantes b WITH(NOLOCK), Competencias c WITH(NOLOCK), Recursos d WITH(NOLOCK), Pessoas e WITH(NOLOCK) ' +
			'WHERE ' + sFiltro + ' a.RETID=b.RETID AND b.PessoaID=' + nSujeitoID + ' AND a.TreinamentoID=c.CompetenciaID AND ' +
				'a.EstadoID=d.RecursoID AND b.ResponsavelID=e.PessoaID ' +
			'ORDER BY dtData DESC, c.Competencia';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 20068) // Forma de pagamento
    {
        dso.SQL = 'SELECT a.* ' +
			'FROM RelacoesPesRec_FormasPagamento a WITH(NOLOCK) ' +
			'WHERE ' + sFiltro + ' a.RelacaoID=' + idToFind + '  ' +
			'ORDER BY a.Ordem';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 20069) // Financiamentos
    {
        dso.SQL = 'SELECT a.* ' +
			'FROM RelacoesPesRec_FinanciamentosPadrao a WITH(NOLOCK) ' +
			'WHERE ' + sFiltro + ' a.RelacaoID=' + idToFind + '  ' +
			'ORDER BY (SELECT Ordem FROM FinanciamentosPadrao WITH(NOLOCK) WHERE (FinanciamentoID = a.FinanciamentoID))';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 20070) // Beneficios
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_Beneficios a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ' +
			'ORDER BY (SELECT Verba FROM VerbasFopag z WITH(NOLOCK) WHERE a.VerbaID = z.VerbaID), ' +
					'(SELECT Fantasia FROM Pessoas w WITH(NOLOCK) WHERE a.PrestadorID = w.PessoaID), ' +
					'a.Plano, a.dtVigencia DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20071) // Despesas
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_Despesas a WITH(NOLOCK) ' +
			    'WHERE ' + sFiltro + 'a.RelacaoID = '+ idToFind + ' ' +
			    'ORDER BY ' +
			        '(SELECT b.Ordem ' +
                        'FROM TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                        'WHERE (b.ItemID = a.TipoDespesaID)), ' +
                    '(SELECT b.ItemAbreviado ' +
                        'FROM TiposAuxiliares_Itens b WITH(NOLOCK) ' +
					    'WHERE (b.ItemID = a.OrigemPedidoID)), ' +
                    '(SELECT b.ItemAbreviado ' +
                        'FROM TiposAuxiliares_Itens b WITH(NOLOCK) ' +
					    'WHERE (b.ItemID = a.OrigemID)), ' +
                    'a.AliquotaEntrada, a.AliquotaImposto, ' +
					'(SELECT b.ItemMasculino ' + 
                        'FROM TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                        'WHERE (b.ItemID = a.AplicacaoID)) , ' +
                    '(SELECT b.Conceito AS fldName ' + 
                        'FROM Conceitos b WITH(NOLOCK) ' +
                        'WHERE (b.ConceitoID = a.FamiliaID)), ' +
                    '(SELECT b.Conceito AS fldName ' + 
                        'FROM Conceitos b WITH(NOLOCK) ' + 
                        'WHERE (b.ConceitoID = a.MarcaID)), ' +
                    'a.ProdutoID, ' +
                    '(SELECT b.ItemMasculino ' +
                        'FROM TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                        'WHERE (b.ItemID = a.ClassificacaoPessoaID)), ' +
                    '(SELECT b.ItemMasculino ' +
                        'FROM TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                        'WHERE (b.ItemID = a.ClassificacaoProdutoID)), ' +
                    '(SELECT b.ItemMasculino ' +
                        'FROM TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                        'WHERE (b.ItemID = a.FinalidadeID)), ' +
                    '(SELECT b.ItemMasculino ' +
                        'FROM TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                        'WHERE (b.ItemID = a.TipoBeneficioID))';

        return 'dso01Grid_DSC';
    }
    else if (folderID == 20072) // Incidencia Vendas
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_IncidenciasVendas a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ' +
			'ORDER BY RelPesRecIncidenciaID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20073) // Convenios
    {
        dso.SQL = 'SELECT * FROM RelacoesPesRec_Convenios a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.RelacaoID = '+ idToFind + ' ' +
			'ORDER BY RelPesRecConvenioID';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 21029) // Benef�cios Fiscais
    {
        dso.SQL = 'SELECT a.* ' +
			'FROM dbo.RelacoesPesRec_BeneficiosFiscais a WITH(NOLOCK) ' +
			'WHERE ' + sFiltro + ' a.RelacaoID=' + idToFind;
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    var nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'SujeitoID' + '\'' + '].value');

    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Perfis de Usu�rio
    else if ( pastaID == 20053 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.PessoaID AS PessoaID, a.Fantasia AS Fantasia ' +
                      'FROM Pessoas a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK) ' +
                      'WHERE b.EstadoID = 2 AND b.TipoRelacaoID = 12 ' +
                      'AND b.ObjetoID = 999 AND b.SujeitoID = a.PessoaID ' +
                      'ORDER BY Fantasia';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT RecursoID,RecursoFantasia ' +
                      'FROM Recursos WITH(NOLOCK) ' +
                      'WHERE EstadoID IN (1, 2, 3) AND TipoRecursoID = 6 ' +
                      'ORDER BY RecursoFantasia';
        }
        else if ( dso.id == 'dsoCmb03Grid_01' )
        {
            dso.SQL = 'SELECT 1 AS Nivel ' +
                      'UNION ALL SELECT 2 AS Nivel ' +
                      'UNION ALL SELECT 3 AS Nivel ' +
                      'UNION ALL SELECT 4 AS Nivel ' +
                      'UNION ALL SELECT 5 AS Nivel';
        }
    }
    // Experiencias
    else if ( pastaID == 20065 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT RecursoID, RecursoFantasia ' +
                      'FROM Recursos WITH(NOLOCK) ' +
                      'WHERE EstadoID IN (1, 2, 3) AND TipoRecursoID = 6 ' +
                      'ORDER BY RecursoFantasia';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT 0 AS PessoaID, SPACE(0) AS Fantasia ' +
					  'UNION ALL SELECT a.PessoaID AS PessoaID, a.Fantasia AS Fantasia ' +
                      'FROM Pessoas a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK) ' +
                      'WHERE b.EstadoID = 2 AND b.TipoRelacaoID = 12 ' +
                      'AND b.ObjetoID = 999 AND b.SujeitoID = a.PessoaID ' +
                      'ORDER BY Fantasia';
        }
    }
    // Habilidades
    else if ( pastaID == 20066 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.CompetenciaID, a.Competencia ' +
                      'FROM Competencias a WITH(NOLOCK) ' +
                      'WHERE a.TipoCompetenciaID = 1703 AND a.EstadoID = 2 ' +
                      'ORDER BY a.Competencia';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT a.RelPesRecHabilidadeID, b.Fantasia  ' +
                      'FROM RelacoesPesRec_Habilidades a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE a.RelacaoID = ' + nRegistroID + ' AND a.ResponsavelID = b.PessoaID';
        }
    }
    // Moedas
    else if ( pastaID == 20055 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ConceitoID,SimboloMoeda ' +
                      'FROM Conceitos WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoConceitoID = 308 AND SimboloMoeda IS NOT NULL ' +
                      'ORDER BY ConceitoAbreviado';
        }
    }        
    // Politicas Financeiras
    else if ( pastaID == 20056 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT a.ConceitoID,a.SimboloMoeda ' +
                      'FROM Conceitos a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK) ' +
                      'WHERE b.RelacaoID = ' + nRegistroID + ' AND b.MoedaID=a.ConceitoID AND ' +
                      'a.TipoConceitoID = 308 AND a.SimboloMoeda IS NOT NULL ' +
                      'ORDER BY a.ConceitoAbreviado';
        }
    }    
    // Notas Fiscais
    else if ( pastaID == 20057 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT  ItemMasculino, ItemID ' +
                        'FROM  dbo.TiposAuxiliares_Itens WITH(NOLOCK) ' +
                        'WHERE TipoID = 434 AND EstadoID = 2 AND Aplicar = 1 ' +
                        'ORDER BY Ordem ';
        }
        else if (dso.id == 'dsoCmb02Grid_01') {
        dso.SQL = 'SELECT  ItemMasculino, ItemID ' +
                        ' FROM  dbo.TiposAuxiliares_Itens WITH(NOLOCK) ' +
                        ' WHERE   TipoID = 421 AND EstadoID = 2 ' +
                        ' ORDER BY Ordem ';
        }
    }
    // Cobranca Bancaria
    else if ( pastaID == 20058 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT a.RelPesContaID, e.Codigo, d.Fantasia, (ISNULL(d.Agencia, SPACE(0)) + ISNULL((CHAR(45) + d.AgenciaDV), SPACE(0))) AS Agencia, e.BancoID, d.PessoaID AS AgenciaID, c.RelacaoID AS ContaID ' +
                      'FROM RelacoesPesRec_Cobranca a WITH(NOLOCK), RelacoesPessoas_Contas b WITH(NOLOCK), RelacoesPessoas c WITH(NOLOCK), Pessoas d, Bancos e WITH(NOLOCK) ' +
                      'WHERE a.RelacaoID = ' + nRegistroID + ' AND a.RelPesContaID = b.RelPesContaID AND ' +
                      'b.RelacaoID = c.RelacaoID AND c.ObjetoID = d.PessoaID AND d.BancoID = e.BancoID';
        }
        else if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT c.RelPesContaID, e.Codigo, d.Fantasia, (ISNULL(d.Agencia, SPACE(0)) + ISNULL((CHAR(45) + d.AgenciaDV), SPACE(0))) AS Agencia, ' +
					  '(ISNULL(c.Conta, SPACE(0)) + ISNULL((CHAR(45) + c.ContaDV), SPACE(0))) AS Conta ' +
                      'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), RelacoesPessoas_Contas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Bancos e WITH(NOLOCK) ' +
                      'WHERE a.RelacaoID = ' + nRegistroID + ' AND ' +
                      'a.SujeitoID = b.SujeitoID AND b.TipoRelacaoID = 24 AND b.EstadoID = 2 AND ' +
                      'b.RelacaoID = c.RelacaoID AND b.ObjetoID = d.PessoaID AND d.EstadoID = 2 AND d.BancoID = e.BancoID AND e.EstadoID = 2 ' +
                      'ORDER BY e.Codigo, ' +
						'(ISNULL(d.Agencia, SPACE(0)) + ISNULL((CHAR(45) + d.AgenciaDV), SPACE(0))), ' +
						'(ISNULL(c.Conta, SPACE(0)) + ISNULL((CHAR(45) + c.ContaDV), SPACE(0)))';
        }
        else if (dso.id == 'dsoCmb02Grid_01') 
        {
            dso.SQL = 'SELECT 1 AS TipoID, 1 AS Tipo';
        }
        else if ( dso.id == 'dsoStateMachineLkp' )
        {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado, b.RecursoID ' +
                      'FROM RelacoesPesRec_Cobranca a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.RelacaoID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID';
        }
    }
    // Atributos
    else if ( pastaID == 20059 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 8 AND Aplicar = 1 ' +
                      'ORDER BY Ordem';
        }
	}    
    // Class Produto
    else if ( pastaID == 20060 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT NivelQualidadeAceitavel ' +
                      'FROM Documentos_PlanoAmostragem WITH(NOLOCK) ' +
                      'WHERE DocumentoID=743 ' +
                      'ORDER BY NivelQualidadeAceitavel';
        }
	}
    // Criterios Avaliacao
    else if ( pastaID == 20063 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 609 ' +
                      'ORDER BY Ordem';
        }
	}
    // Forma Pagamento
    else if ( pastaID == 20068 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID, ItemAbreviado ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 804 ' +
                      'ORDER BY Ordem';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT 1 AS fldID, 1 AS fldName ' +
					  'UNION ALL ' +
					  'SELECT 2 AS fldID, 2 AS fldName ' +
					  'UNION ALL ' +
					  'SELECT 3 AS fldID, 3 AS fldName ' +
					  'UNION ALL ' +
					  'SELECT 4 AS fldID, 4 AS fldName ';

        }
        else if ( dso.id == 'dsoStateMachineLkp' )
        {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM RelacoesPesRec_FormasPagamento a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.RelacaoID = '+ nRegistroID + ' AND a.EstadoID = b.RecursoID';
        }
	}
    // Financiamentos
    else if ( pastaID == 20069 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT FinanciamentoID, Financiamento ' +
                      'FROM FinanciamentosPadrao WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND FinanciamentoID <> 1 ' +
                      'ORDER BY Ordem';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT 1 AS fldID, 1 AS fldName ' +
					  'UNION ALL ' +
					  'SELECT 2 AS fldID, 2 AS fldName ' +
					  'UNION ALL ' +
					  'SELECT 3 AS fldID, 3 AS fldName ' +
					  'UNION ALL ' +
					  'SELECT 4 AS fldID, 4 AS fldName ';
        }
        else if ( dso.id == 'dsoStateMachineLkp' )
        {
            dso.SQL = 'SELECT DISTINCT b.RecursoAbreviado as Estado,b.RecursoID ' +
                      'FROM RelacoesPesRec_FinanciamentosPadrao a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE a.RelacaoID = '+ nRegistroID + ' AND a.EstadoID = b.RecursoID';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RelPesRecFinanciamentoID, c.NumeroParcelas, ' +
					'c.Prazo1, c.Incremento, dbo.fn_Financiamento_PMP(c.FinanciamentoID, NULL, NULL, NULL) AS PrazoMedioPagamento, ' +
					'c.Observacao ' +
				'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_FinanciamentosPadrao b WITH(NOLOCK), FinanciamentosPadrao c WITH(NOLOCK) ' +
				'WHERE (a.RelacaoID = '+ nRegistroID + ' AND a.RelacaoID = b.RelacaoID AND b.FinanciamentoID = c.FinanciamentoID)';
        }
	}
    // Planos de saude
    else if ( pastaID == 20070 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT a.RelPesRecBeneficioID, b.Fantasia ' +
				'FROM RelacoesPesRec_Beneficios a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
				'WHERE (a.RelacaoID = '+ nRegistroID + ' AND a.PrestadorID = b.PessoaID)';
        }
        else if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT VerbaID AS fldID, Verba AS fldName ' +
                      'FROM VerbasFopag WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND EhBeneficio = 1 ' +
                      'ORDER BY Verba';
        }
	}
    
    // Despesas
    else if (pastaID == 20071)
    {
        // Despesa
        if (dso.id == 'dsoCmb01Grid_01')
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
                'SELECT a.ItemID, a.ItemMasculino ' +
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE ((a.EstadoID = 2) AND (a.TipoID = 109)) ' +
                    'ORDER BY fldName';
        }
        
        // Origem
        if (dso.id == 'dsoCmb02Grid_01') 
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
                'SELECT a.ItemID, a.ItemAbreviado ' +
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE ((a.EstadoID = 2) AND (a.TipoID = 114)) ' +
                    'ORDER BY fldName';
        }
        
        // Aplicacao
        if ( dso.id == 'dsoCmb03Grid_01' )
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem ' +
				'UNION ' +
                'SELECT a.ItemID, a.ItemMasculino, a.Ordem ' + 
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE ((EstadoID = 2) AND (TipoID = 408) AND (ItemID <> 663)) ' +
                    'ORDER BY Ordem';
        }
        
        // Familia
        if ( dso.id == 'dsoCmb04Grid_01' )
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
                'SELECT a.ConceitoID AS fldID, a.Conceito AS fldName ' +
		            'FROM Conceitos a WITH(NOLOCK) ' +
		            'WHERE ((a.TipoConceitoID = 302) AND (a.EstadoID NOT IN (1,5))) ' +
		            'ORDER BY fldName';
        }
        
        // Marca
        if ( dso.id == 'dsoCmb05Grid_01' )
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
                'SELECT a.ConceitoID AS fldID, a.Conceito AS fldName ' +
		            'FROM Conceitos a WITH(NOLOCK) ' +
		            'WHERE ((a.TipoConceitoID = 304) AND (a.EstadoID NOT IN (1,5))) ' +
		            'ORDER BY fldName';
        }
        
        // Classificacao Pessoa
        if ( dso.id == 'dsoCmb06Grid_01' )
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
                'SELECT a.ItemID, a.ItemMasculino ' + 
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE ((EstadoID = 2) AND (TipoID = 13)) ' +
                    'ORDER BY fldName';
        }

        // Classificacao Produto
        if (dso.id == 'dsoCmb07Grid_01') 
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
                'SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName ' +
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE ((a.EstadoID = 2) AND (a.TipoID = 102)) ' +
                    'ORDER BY fldName';
        }
        
        // Finalidade
        if (dso.id == 'dsoCmb08Grid_01') 
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
                'SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName ' +
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE ((a.EstadoID = 2) AND (a.TipoID = 418)) ' +
                    'ORDER BY fldName';
        }
       
        // Beneficio Fiscal
        if (dso.id == 'dsoCmb09Grid_01') 
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
                'SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName ' +
                    'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                    'WHERE ((a.EstadoID = 2) AND (a.TipoID = 113) AND (a.Filtro LIKE ' + '\'' + '%<1132>%' + '\'' + ')) ' +
                    'ORDER BY fldName';
        }

        // Excecao
        if ( dso.id == 'dsoCmb10Grid_01' )
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
			    'SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName ' +
			        'FROM TiposAuxiliares_itens a WITH(NOLOCK) ' +
			        'WHERE ((a.EstadoID = 2) AND (TipoID = 111)) ' +
				    'ORDER BY fldName';
        }

        // OrigemPedidoID
        if (dso.id == 'dsoCmb11Grid_01') {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
				'UNION ' +
			    'SELECT a.ItemID AS fldID, a.ItemMasculino AS fldName ' +
			        'FROM TiposAuxiliares_itens a WITH(NOLOCK) ' +
			        'WHERE ((a.EstadoID = 2) AND (TipoID = 402)) ' +
				    'ORDER BY fldName';
        }
    }
    // Incidencias Vendas
    else if ( pastaID == 20072 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
					  'UNION ALL ' +
					'SELECT RecursoID AS fldID, RecursoFantasia AS fldName ' +
                      'FROM Recursos WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND EhCargo = 1 ' +
                      'ORDER BY fldName';
        }
        if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
					  'UNION ALL ' +
					'SELECT  VerbaID AS fldID, Verba AS fldName ' +
						'FROM VerbasFopag WITH(NOLOCK) ' +
						'WHERE EstadoID = 2 '  +
						'ORDER BY fldName';
        }
	}
    // Convenio
    else if ( pastaID == 20073 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT LocalidadeID AS fldID, Localidade AS fldName ' +
                      'FROM Localidades WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoLocalidadeID=204 AND ' +
                      'LocalizacaoID = (SELECT TOP 1 PaisID FROM Pessoas_Enderecos WITH(NOLOCK) WHERE PessoaID=' +
                        nSujeitoID + ' ORDER BY Ordem) ' +
                      'ORDER BY fldName';
        }
        if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 17 ' +
                      'ORDER BY Ordem';
        }
	}
	// Classificacoes
    else if ( pastaID == 20074 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoID = 35 ' +
                      'ORDER BY Ordem';
        }
	}
    // Beneficios Fiscais
    else if (pastaID == 21029) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT  ItemID, ItemMasculino ' +
                        'FROM TiposAuxiliares_itens WITH(NOLOCK) ' +
                       'WHERE (EstadoID=2 AND TipoID=113 AND Filtro LIKE \'%<1132>%\') ' +
                       'ORDER BY Ordem';
        }
    }
    // Cr�dito
    else if (pastaID == 28083) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
                'UNION ALL SELECT a.PessoaID AS fldID, a.Fantasia AS fldName ' +
                'FROM Pessoas a WITH(NOLOCK) ' +
                'WHERE a.EstadoID = 2 AND a.ClassificacaoID = 70 AND a.Observacoes LIKE \'%<SeguradoraCredito%\'' +
                'ORDER BY fldName';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

	if (nTipoRegistroID == 11) // Uso(F�s)
	{
	    vPastaName = window.top.getPastaName(20053);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20053); //Perfis de Usu�rio

	    vPastaName = window.top.getPastaName(20064);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20064); //Educacao

	    vPastaName = window.top.getPastaName(20065);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20065); //Experiencias

	    vPastaName = window.top.getPastaName(20066);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20066); //Habilidades

	    vPastaName = window.top.getPastaName(20067);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20067); //Treinamentos
	}

	if (nTipoRegistroID == 12) // Uso(Jur)
	{
		vPastaName = window.top.getPastaName(20061);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20061); //Setup
			
		vPastaName = window.top.getPastaName(20054);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20054); //Descontos

		vPastaName = window.top.getPastaName(20055);
		if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 20055); //Moedas

        vPastaName = window.top.getPastaName(28083);
        if (vPastaName != '')
            addOptionToSelInControlBar('inf', 1, vPastaName, 28083); // Cr�dito

		vPastaName = window.top.getPastaName(20056);
		if (vPastaName != '')
		    addOptionToSelInControlBar('inf', 1, vPastaName, 20056); //Pol�ticas Financeiras

		vPastaName = window.top.getPastaName(21029);
		if (vPastaName != '')
		    addOptionToSelInControlBar('inf', 1, vPastaName, 21029); //Benef�cios Fiscais	
			
		vPastaName = window.top.getPastaName(20068);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20068); //Forma Pagamento

		vPastaName = window.top.getPastaName(20069);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20069); //Financiamentos

		vPastaName = window.top.getPastaName(20057);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20057); //Notas Fiscais

		vPastaName = window.top.getPastaName(20058);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20058); //Cobranca Bancaria

		vPastaName = window.top.getPastaName(20070);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20070); //Planos de Saude

        vPastaName = window.top.getPastaName(20072);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20072); //Incidencias Vendas	
			
        vPastaName = window.top.getPastaName(20073);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20073); //Convenios

		vPastaName = window.top.getPastaName(20071);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20071); //Despesas

		vPastaName = window.top.getPastaName(20059);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20059); //Atributos

		vPastaName = window.top.getPastaName(20060);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20060); //Classificacao Produto

		vPastaName = window.top.getPastaName(20062);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20062); //Classificacao Fornecedor

		vPastaName = window.top.getPastaName(20074);
		if (vPastaName != '')
			addOptionToSelInControlBar('inf', 1, vPastaName, 20074); //Classificacoes

		vPastaName = window.top.getPastaName(20063);
		if (vPastaName != '')
		    addOptionToSelInControlBar('inf', 1, vPastaName, 20063); //Criterios Avaliacao

	}

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 20053) // Perfis de Usuario
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[2], ['RelacaoID', registroID]);
        else if (folderID == 20065) // Experiencia
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,2], ['RelacaoID', registroID]);
        else if (folderID == 20066) // Habilidades
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID],[6]);
        else if (folderID == 20054) // Descontos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1], ['RelacaoID', registroID]);
        else if (folderID == 20055) // Moedas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[1], ['RelacaoID', registroID]);
        else if (folderID == 20056) // Politicas Financeiras
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID]);
        else if (folderID == 20057) // Notas Fiscais
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID]);
        else if (folderID == 20058) // Cobranca Bancaria
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[3,7,8,9,10,16,18,19,20,23,24,25,26], ['RelacaoID', registroID]);
		else if (folderID == 20059) // Atributos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1,2], ['RelacaoID', registroID]);
		else if (folderID == 20060) // Classificacao Produto
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1], ['RelacaoID', registroID]);
		else if (folderID == 20062) // Classificacao Fornecedor
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1], ['RelacaoID', registroID]);
        else if (folderID == 20074) // Classificacoes
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['RelacaoID', registroID]);            
		else if (folderID == 20063) // Criterios Avaliacao
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1], ['RelacaoID', registroID]);
		else if (folderID == 20068) // Forma Pagamento
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 3], ['RelacaoID', registroID]);
		else if (folderID == 20069) // Financiamentos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 3], ['RelacaoID', registroID]);
		else if (folderID == 20070) // Beneficios
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 1, 2, 3, 4, 5], ['RelacaoID', registroID], [7]);
        else if (folderID == 20071) // Despesas
        {
            if ((fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Percentual')) < -100) ||
				(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Percentual')) > 100))
			{
				if ( window.top.overflyGen.Alert('Valor fora da faixa.') == 0 )
					return null;
					
				currLine = -1;
			}	
			else if ((fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'AliquotaImposto')) < 0) ||
				(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'AliquotaImposto')) > 100) ||
				(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'AliquotaEntrada')) < 0) ||
				(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'AliquotaEntrada')) > 100))
			{
				if ( window.top.overflyGen.Alert('Valor fora da faixa.') == 0 )
					return null;
					
				currLine = -1;
			}
            else
                currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 14], ['RelacaoID', registroID]);            
            	
            
        }
        else if (folderID == 20072) // Incidencias Vendas
		{
			if ((fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'FaixaMinima')) > 9999) ||
				(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'FaixaMaxima')) > 9999) ||
				(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Percentual')) > 999))
			{
				if ( window.top.overflyGen.Alert('Valor fora da faixa.') == 0 )
					return null;
					
				currLine = -1;
			}	
            else
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[2, 3, 5], ['RelacaoID', registroID]);            
        }    		
		else if (folderID == 20073) // Convenios
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0, 1, 2], ['RelacaoID', registroID]);
        else if (folderID == 21029) // Beneficios Fiscais
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['RelacaoID', registroID]);
        else if (folderID == 28083) // Cr�dito
        {
            var sMensagem = '';


            if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SeguradoraID')) != 0)
            {
                if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'dtInicio')) == 0)
                {
                    if (sMensagem != '')
                        sMensagem += '\n';

                    sMensagem += 'Informe uma data de in�cio.';

                }

                if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'dtFim')) == 0)
                {
                    if (sMensagem != '')
                        sMensagem += '\n';
                    
                    sMensagem += 'Informe uma data de fim.';

                }
            }


            if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SeguradoraID')) == 0)
            {

                if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ValorCreditoAutomatico')) == 0)
                {
                    if (sMensagem != '')
                        sMensagem += '\n';

                    sMensagem += 'Informe um valor de Cr�dito Autom�tico.';
                }

                if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PrazoValidade')) == 0) {
                    if (sMensagem != '')
                        sMensagem += '\n';

                    sMensagem += 'Informe o Prazo de Validade.';
                }

                if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PrazoVencendo')) == 0) {
                    if (sMensagem != '')
                        sMensagem += '\n';

                    sMensagem += 'Informe o Prazo Vencendo.';
                }

                if (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PrazoRenovacao')) == 0) {
                    if (sMensagem != '')
                        sMensagem += '\n';

                    sMensagem += 'Informe o Prazo Renova��o.';
                }

            }

            if (sMensagem != '') {
                if (window.top.overflyGen.Alert(sMensagem) == 0)
                    return null;

                return false;
            }
        }

            try
            {
                currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [12], ['RelacaoID', registroID], [13]);
            }
            catch (e)
            {
                if (window.top.overflyGen.Alert('N�o foi poss�vel fazer a inser��o. Por favor, tente novamente.') == 0)
                    return null;
                __btn_REFR('inf');
            }

        if (currLine < 0)
            return false;
    }

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
//function fg_ValidateEdit() 
//{
//    refillCmbCarregaLayout(1);
//}

//function refillCmbCarregaLayout(RelPesContaID) 
//{
//    setConnection(dsoCmbLayout);
//    dsoCmbLayout.SQL = 'SELECT DISTINCT f.Tipo AS TipoID, f.Tipo AS Tipo ' +
//		                            'FROM RelacoesPesRec a ' +
//			                            'INNER JOIN RelacoesPessoas b ON (a.SujeitoID = b.SujeitoID) ' +
//			                            'INNER JOIN RelacoesPessoas_Contas c ON (b.RelacaoID = c.RelacaoID) ' +
//			                            'INNER JOIN Pessoas d ON (b.ObjetoID = d.PessoaID) ' +
//			                            'INNER JOIN Bancos e ON (d.BancoID = e.BancoID) ' +
//			                            'INNER JOIN Bancos_Layout f ON (e.BancoID = f.BancoID) ' +
//                                    'WHERE a.RelacaoID = 2 AND c.RelPesContaID = 1 AND b.TipoRelacaoID = 24 AND ' +
//			                            'b.EstadoID = 2 AND d.EstadoID = 2 AND e.EstadoID = 2';

//    dsoCmbLayout.ondatasetcomplete = refillCmbCarregaLayout_DSC;
//    dsoCmbLayout.refresh();
//}

//function refillCmbCarregaLayout_DSC() 
//{
//    // alert("aqui");
    //    insertcomboData(fg, getColIndexByColKey(fg, 'Layout'), dsoCmbLayout, 'TipoID', 'Tipo');
//    ;
//}


function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
        
    }
    else if (folderID == 20053) // Perfis de Usuario
    {
        headerGrid(fg,['Empresa','Ordem','Perfil','N�vel','OK','Observa��o','RelPesRecPerfilID'], [6]);

        glb_aCelHint = [[0,4,'� perfil definitivo?']];

        fillGridMask(fg,currDSO,['EmpresaID','Ordem','PerfilID','Nivel','OK','Observacao','RelPesRecPerfilID'],
                                     ['','999','','','','',''],
                                     ['','','','','','','']);
        alignColsInGrid(fg,[1,3]);
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }    
    else if (folderID == 20065) // Experiencia
    {
        headerGrid(fg,['In�cio','Fim','Cargo','Empresa','Observa��o','RelPesRecExperienciaID'], [5]);
        fillGridMask(fg,currDSO,['dtInicio','dtFim','PerfilID','EmpresaID','Observacao','RelPesRecExperienciaID'],
                                     ['99/99/9999','99/99/9999','','','',''],
                                     [dTFormat,dTFormat,'','','','']);
        alignColsInGrid(fg,[]);
    }    
    else if (folderID == 20066) // Habilidades
    {
        headerGrid(fg,['Habilidade','RET','OK','Data Avalia��o','Respons�vel','Observa��o','ResponsavelID','RelPesRecHabilidadeID'], [6,7]);
        fillGridMask(fg,currDSO,['HabilidadeID','RETID*','OK','dtAvaliacao*','^RelPesRecHabilidadeID^dso01GridLkp^RelPesRecHabilidadeID^Fantasia*','Observacao','ResponsavelID','RelPesRecHabilidadeID'],
                                     ['','','','99/99/9999','','','',''],
                                     ['','','',dTFormat + ' hh:mm:ss','','','','']);
        alignColsInGrid(fg,[1]);
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }    
    else if (folderID == 20054) // Descontos
    {
        headerGrid(fg,['Faixa','Desconto','RelPesRecDescontoID'],[2]);
        fillGridMask(fg,currDSO,['Faixa','Desconto','RelPesRecDescontoID'],
                                ['999','999.99',''],
                                ['','###.00','']);
    }
    else if (folderID == 20055) // Moedas
    {
        headerGrid(fg,['Ordem','Moeda','Fat','RelPesRecMoedaID'],[3]);
        fillGridMask(fg,currDSO,['Ordem','MoedaID','Faturamento','RelPesRecMoedaID']
                                ,['99','','','']);
    }
    //Creditos
    else if (folderID == 28083) {
        headerGrid(fg, ['Seguradora',
            'In�cio',
            'Fim',
            'Valor Ap�lice',
            'Pr�mio',
            'Valor Assegurado',
            'Prazo Car�ncia',
            'Franquia',
            'Cr�dito Autom�tico',
            'Prazo Renova��o',
            'Prazo Validade',
            'Prazo Vencendo',
            'Prazo Solicita��o',
            'Observa��o',
            'RelCreditoID'], [14]);

        glb_aCelHint = [[0, 9, 'Prazo de Renova��o (dias)'],
        [0, 10, 'Prazo de Validade (dias)'],
        [0, 11, 'Prazo Vencendo (dias)'],
        [0, 12, 'Prazo de Solcicita��o (horas)']];

        fillGridMask(fg, currDSO, ['SeguradoraID',
            'dtInicio',
            'dtFim',
            'ValorApolice',
            'Premio',
            'ValorAssegurado',
            'PrazoCarencia',
            'Franquia',
            'ValorCreditoAutomatico',
            'PrazoRenovacao',
            'PrazoValidade',
            'PrazoVencendo',
            'PrazoSolicitacao',
            'Observacao',
            'RelCreditoID'],
            ['', '99/99/9999', '99/99/9999', '999999999.99', '999999999.99', '999999999.99', '9999', '999999999.99', '999999999.99', '999', '999', '999', '999', '', ''],
            ['', dTFormat, dTFormat, '###,###,##0.00', '###,###,##0.00', '###,###,##0.00', '####', '###,###,##0.00', '###,###,##0.00', '###', '###', '###', '###', '', '']);

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
    }
    else if (folderID == 20056) // Politicas Financeiras
    {
        headerGrid(fg,['Moeda','Estoque','Venda','Atualiza��o','Multa Atraso',
                       'Observa��o','RelPesRecFinancID'],[6]);
        fillGridMask(fg,currDSO,['MoedaID','TaxaEstoque','TaxaVenda','TaxaAtualizacao',
                                 'TaxaMultaAtraso','Observacao',
                                 'RelPesRecFinancID'],
                                ['','999.99','999.99','999.99','999.99','',''],
                                ['','###.00','###.00','###.00','###.00','','']);
    }
    else if (folderID == 20057) // Notas Fiscais
    {
        headerGrid(fg, ['Documento Fiscal', 'S�rie', 'S�rie Conting�ncia', 'Tipo Emiss�o', 'Nota Fiscal', 'Caminho NFe', 'Caminho NFe Homologacao', 'Observa��o',
            'RelPesRecDocumentoFiscalID'], [8]);

        fillGridMask(fg, currDSO, ['DocumentoFiscalID', 'Serie', 'SerieContigencia', 'TipoEmissaoID', 'NotaFiscal', 'CaminhoNFe', 'CaminhoNFeHomologacao',
            'Observacao', 'RelPesRecDocumentoFiscalID'],
            ['','','','','','','', '', ''],
            ['','','','','','','', '', '']);
    }
    else if (folderID == 20058) // Cobranca Bancaria
    {
        headerGrid(fg, ['BancoID', 'Banco', 'Est', 'Ag�ncia', 'Conta', 'Conta Cobranca', 'Digito', 'Default', 'Saldo M�nimo', 'Saldo Atual',
					   'Conv�nio L�der', 'Conv�nio COB', 'Conv�nio COD', 'Cart COB', 'Cart COD', 'C�digo Transmiss�o', 'Varia��o Carteira', 'NN Inicial', 'NN Final',
					   'Peso In�cio', 'Peso Fim', 'D�gito X', 'Comando', 'Inst1', 'Instr2', 'Mensagem 1', 'Mensagem 2', 'Arq Remessa', 'Arq Remessa Ren',
					   'Arq Retorno','Arq Retorno Ren', 'Layout', 'RelPesRecCobrancaID'],[32]);

        glb_aCelHint = [[0,7,'� o banco default da cobran�a?'],
                        [0,13,'Carteira COB default'],
                        [0,14,'Carteira COD default'],
                        [0,19,'Peso inicial para o c�lculo do d�gito verificador do nosso n�mero'],
                        [0,20,'Peso final para o c�lculo do d�gito verificador do nosso n�mero'],
                        [0,21,'Calcula d�gito X?']];

        fillGridMask(fg, currDSO, ['^RelPesContaID^dso01GridLkp^RelPesContaID^Codigo*', '^RelPesContaID^dso01GridLkp^RelPesContaID^Fantasia*', '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*', '^RelPesContaID^dso01GridLkp^RelPesContaID^Agencia*',
                                   'RelPesContaID', 'ContaCobranca', 'DVContaCobranca', 'EhDefault', 'SaldoMinimo', 'SaldoAtual', 'ConvenioLider', 'ConvenioCOB', 'ConvenioCOD',
                                   'CarteiraCOB', 'CarteiraCOD', 'CodigoTransmissao', 'VariacaoCarteira', 'NossoNumeroInicial', 'NossoNumeroFinal', 'PesoInicialNossoNumero', 'PesoFinalNossoNumero', 'DigitoX',
                                   'ComandoID', 'Instrucao1', 'Instrucao2', 'Mensagem1', 'Mensagem2', 'ArquivoRemessa', 'ArquivoRemessaRenomeado', 'ArquivoRetorno', 'ArquivoRetornoRenomeado',
                                   'Layout', 'RelPesRecCobrancaID'],
                                 ['', '', '', '', '', '', '', '', '99999999.99', '99999999.99', '', '', '', '', '', '', '', '9999999999', '999999999999', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                                 ['', '', '', '', '', '', '', '', '###,###,##0.00', '###,###,##0.00', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);
        alignColsInGrid(fg,[8,9]);
        fg.FrozenCols = 4;
        // Permite ordena��o pelo header do grid
        fg.ExplorerBar = 5;
    }
    // Atributos de usuario
    else if (folderID == 20059)
    {
        headerGrid(fg,['Atributo',
                       'M�nimo',
                       'M�ximo',
                       'RelPesRecAtributoID'],[3]);

        fillGridMask(fg,currDSO,['AtributoID','ValorMinimo','ValorMaximo','RelPesRecAtributoID'],
                                ['','#999.9999999','#999.9999999',''],
                                ['','###.0000000','###.0000000','']);

        alignColsInGrid(fg,[1,2]);
    }
    // Classificacao Produto
    else if (folderID == 20060)
    {
        headerGrid(fg,['Class',
                       'NQA',
                       'RelPesRecClassificacaoID'],[2]);

        glb_aCelHint = [[0,0,'Classifica��o de Produto']];

        fillGridMask(fg,currDSO,['Classificacao','NivelQualidadeAceitavel','RelPesRecClassificacaoID'],
                                ['','',''],
                                ['','','']);

        alignColsInGrid(fg,[1]);
    }
    // Classificacao Fornecedor
    else if (folderID == 20062)
    {
        headerGrid(fg,['Class',
                       'Pontua��o',
                       'RelPesRecClassificacaoID'],[2]);

        glb_aCelHint = [[0,0,'Classifica��o de Fornecedor']];

        fillGridMask(fg,currDSO,['Classificacao',
								 'Pontuacao',
								 'RelPesRecClassificacaoID'],
                                 ['','999',''],
                                 ['','###','']);

        alignColsInGrid(fg,[1]);
    }
    // Classificacoes
    else if (folderID == 20074)
    {
        headerGrid(fg,['Tipo',
                        'Inv',
                        'Perc',
                        'Rec',
                        'In�cio',
                        'Prazo',
                        'Per�odo',
                        'RelPesRecClassificacaoID'],[7]);

        glb_aCelHint = [[0,1,'Valores invertidos?'],
                        [0,2,'Valores em percentual?'],
                        [0,3,'Tem recorr�ncia?'],
                        [0,5,'Prazo entre classifica��es'],
                        [0,6,'Per�odo considerado para classifica��o']];

        fillGridMask(fg,currDSO,['TipoClassificacaoID',
								 'Invertido',
								 'EhPercentual',
								 'TemRecorrencia',
								 'dtInicio',
								 'Prazo',
								 'Periodo',
								 'RelPesRecClassificacaoID'],
                                 ['','','','','99/99/9999','',''],
                                 ['','','','',dTFormat,'','']);

        alignColsInGrid(fg,[4,5,6]);
    }
    // Crit�rios de Avalia��o
    else if (folderID == 20063)
    {
        headerGrid(fg,['Crit�rio',
                       'Peso',
                       'RelPesRecCriterioID'],[2]);

        fillGridMask(fg,currDSO,['CriterioID',
								 'Peso',
								 'RelPesRecCriterioID'],
                                 ['','99',''],
                                 ['','##','']);

        alignColsInGrid(fg,[1]);
    }
    // Treinamentos
    else if (folderID == 20067)
    {
        headerGrid(fg,['Data',
                       'Treinamento',
                       'RET',
                       'Est',
                       'Nota',
                       'Eficaz',
                       'Data Avalia��o',
                       'Respons�vel',
                       'Observa��o'],[]);

        fillGridMask(fg,currDSO,['dtData',
								 'Competencia',
								 'RETID',
								 'Estado',
								 'Nota',
								 'Eficaz',
								 'dtAvaliacao',
								 'Responsavel',
								 'Observacao'],
                                 ['99/99/9999', '', '', '', '999.99', '', '99/99/9999', '', ''],
                                 ['##/##/####', '', '', '', '##0.00', '', '##/##/####', '', '']);

        alignColsInGrid(fg,[ 2, 4]);
    }
    // Formas Pagamento
    else if (folderID == 20068)
    {
        headerGrid(fg,['Forma',
                       'Est',
                       'Ordem',
                       'N�vel',
                       'Valor M�n',
                       'Valor M�x',
                       'Prazo M�n',
                       'Prazo M�x',
                       'D+n',
                       'RelPesRecFormaPagamentoID'],[9]);

        glb_aCelHint = [[0,4,'Valor m�nimo'],
                        [0,5,'Valor m�ximo'],
                        [0,6,'Prazo m�nimo'],
                        [0,7,'Prazo m�ximo'],
                        [0,8,'N�mero de dias para o valor estar dispon�vel']];

        fillGridMask(fg,currDSO,['FormaPagamentoID',
								 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
								 'Ordem',
								 'NivelPagamento',
								 'ValorMinimo',
								 'ValorMaximo',
								 'PrazoMinimo',
								 'PrazoMaximo',
								 'DiasCompensacao',
								 'RelPesRecFormaPagamentoID'],
                                 ['','','999','','999999999.99','999999999.99','999','999','99',''],
                                 ['','','','','###,###,##0.00','###,###,##0.00','###','###','##','']);

        alignColsInGrid(fg,[2, 3, 4, 5, 6, 7, 8]);
    }
    // Formas Pagamento
    else if (folderID == 20069)
    {
        headerGrid(fg,['Financiamento',
					   'Est',
					   'Default',
					   'N�vel',
					   'Parcelas',
					   'Prazo 1',
					   'Incr',
					   'PMP',
					   'Observa��o',
					   'RelPesRecFinanciamentoID'],[9]);

        glb_aCelHint = [[0,2,'� o financiamento padr�o default?'],
                        [0,4,'N�mero de parcelas'],
                        [0,5,'Prazo da primeira parcela'],
                        [0,6,'Incremento das pr�ximas parcelas']];

        fillGridMask(fg,currDSO,['FinanciamentoID',
								 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*',
								 'EhDefault',
								 'NivelPagamento',
								 '^RelPesRecFinanciamentoID^dso01GridLkp^RelPesRecFinanciamentoID^NumeroParcelas*',
								 '^RelPesRecFinanciamentoID^dso01GridLkp^RelPesRecFinanciamentoID^Prazo1*',
								 '^RelPesRecFinanciamentoID^dso01GridLkp^RelPesRecFinanciamentoID^Incremento*',
								 '^RelPesRecFinanciamentoID^dso01GridLkp^RelPesRecFinanciamentoID^PrazoMedioPagamento*',
								 '^RelPesRecFinanciamentoID^dso01GridLkp^RelPesRecFinanciamentoID^Observacao*',
								 'RelPesRecFinanciamentoID'],
								 ['','','','','99','999','999','999.99','',''],
								 ['','','','','##','###','###','##0.00','','']);

        alignColsInGrid(fg,[4, 5, 6, 7]);
    }
    // Beneficios
    else if (folderID == 20070)
    {
        headerGrid(fg, ['Verba',
                       'Prestador',
                       'Plano',
                       'Vig�ncia',
					   'Valor Titular',
					   'Valor Dependente',
					   'Observa��o',
					   'PrestadorID',
					   'RelPesRecBeneficioID'],[7, 8]);

        fillGridMask(fg, currDSO, ['VerbaID',
                                 '^RelPesRecBeneficioID^dso01GridLkp^RelPesRecBeneficioID^Fantasia*',
								 'Plano',
								 'dtVigencia',
								 'ValorTitular',
								 'ValorDependente',
								 'Observacao',
								 'PrestadorID',
								 'RelPesRecBeneficioID'],
								 ['', '', '', '99/99/9999', '999999999.99', '999999999.99', '', '', ''],
								 ['','','',dTFormat,'###,###,##0.00','###,###,##0.00','','','']);

        alignColsInGrid(fg, [4, 5]);
        
    }
    // Despesas
    else if (folderID == 20071) 
    {
        fg.FrozenCols = 1;
        headerGrid(fg,['Despesa',
                       'Orig Pedido',        
                       'Orig Produto',
                       'Al�q Entrada',
                       'Al�q Sa�da',
                       'Aplica��o',
					   'Fam�lia',
					   'Marca',
					   'ProdutoID',
					   'Class Pessoa',
					   'Class Produto',
				       'Finalidade',
					   'Beneficio Fiscal',
					   'Exce��o',
					   'Perc',
					   'Observa��o',
					   'RelPesRecDespesaID'],[16]);

        glb_aCelHint = [[0, 1, 'Origem do Pedido'], [0, 2, 'Origem do Produto'], [0, 9, 'Classifica��o da Pessoa']];					   

        fillGridMask(fg,currDSO,['TipoDespesaID',
                                 'OrigemPedidoID',
                                 'OrigemID',
                                 'AliquotaEntrada',
                                 'AliquotaImposto',
                                 'AplicacaoID',
								 'FamiliaID',
								 'MarcaID',
								 'ProdutoID',
								 'ClassificacaoPessoaID',
								 'ClassificacaoProdutoID',
								 'FinalidadeID',
								 'TipoBeneficioID',
								 'TipoExcecaoID',
								 'Percentual',
								 'Observacao',
								 'RelPesRecDespesaID'],
								 ['','','','999.99','999.99','','','','9999999','','','','','','#999.99','',''],
								 ['','','##0.00','##0.00','','','','','','','','','','(##0.00)','','']);

        alignColsInGrid(fg, [3, 4, 8, 12]);
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
    
        fg.MergeCells = 4;
        fg.MergeCol(0) = true;
        fg.MergeCol(1) = true;
        fg.MergeCol(2) = true;
        fg.MergeCol(3) = true;
        fg.MergeCol(4) = true;
        fg.MergeCol(5) = true;
        fg.MergeCol(6) = true;
        fg.MergeCol(7) = true;
        fg.MergeCol(8) = true;
        fg.MergeCol(9) = true;
        fg.MergeCol(10) = true;
        fg.MergeCol(11) = true;
        fg.MergeCol(12) = true;        

        fg.Redraw = 2;
        
    
        
    }
    // Incidencia de Vendas
    else if (folderID == 20072)
    {
        headerGrid(fg,['Cargo',
					   'Verba',
					   'Vig�ncia',
					   'Faixa Min',
					   'Faixa Max',
					   'Percentual',
					   'Observa��o',
					   'RelPesRecIncidenciaID'],[7]);

        fillGridMask(fg,currDSO,['PerfilID',
								 'VerbaID',
								 'dtVigencia',
								 'FaixaMinima',
								 'FaixaMaxima',
								 'Percentual',
								 'Observacao',
								 'RelPesRecIncidenciaID'],
								 ['','','99/99/9999','#9999.99','#9999.99','#999.99','',''],
								 ['','',dTFormat,'(#,##0.00)','(#,##0.00)','(##0.00)','','']);

        alignColsInGrid(fg,[3,4,5]);
    }	
    // Convenios
    else if (folderID == 20073)
    {
        headerGrid(fg,['UF',
					   'Tipo',
					   'N�mero',
					   'Observa��es',
					   'RelPesRecConvenioID'],[4]);

        fillGridMask(fg,currDSO,['UFID',
					             'TipoDocumentoID',
					             'Numero',
					             'Observacao',
					             'RelPesRecConvenioID'],
								 ['','','',''],
								 ['','','','']);
    }	
    // Benef�cios Fiscais
    else if (folderID == 21029) {
        headerGrid(fg, ['Benef�cio Fiscal',
                       'Legisla��o',
                       'Vig in�cio',
                       'Vig Fim',
                       'Observa��o',
                       'RelPesRecBeneficioFiscalID'], [5]);

        //glb_aCelHint = [[0, 2, 'Intervalo m�nimo(dias) para fazer uma nova consulta']];

        fillGridMask(fg, currDSO, ['TipoBeneficioFiscalID',
								   'LegislacaoAplicavel',
								   'dtVigenciaInicio',
								   'dtVigenciaFim',
								   'Observacao',
								   'RelPesRecBeneficioFiscalID'],
                                  ['', '', '', '', '', ''],
                                  ['', '', '', '', '', '']);
    }
}
