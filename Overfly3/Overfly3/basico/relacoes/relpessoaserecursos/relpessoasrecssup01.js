/********************************************************************
relpessoaserecursossup01.js

Library javascript para o relpessoaserecursossup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// controla o retorno do servidor dos dados de combos dinamicos
// (selSujeitoID e selObjetoID)
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFramWork;
// guarda id do combo de pula a preencher
var glb_cmbLupaID;

// Guarda o tipo do registro selecionado para ser usado no select
// que traz o registro devolta na gravacao, pois este form tem
// Identity calculado por trigger
var glb_RelPesRecTipoRegID = null;

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoCmbDynamic02 = new CDatatransport('dsoCmbDynamic02');
var dsoCmbsLupa = new CDatatransport('dsoCmbsLupa');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoChangePassword = new CDatatransport('dsoChangePassword');
var dsoValidateUser = new CDatatransport('dsoValidateUser');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selTipoEmpresaID', '2'],
                          ['selObjetoID', '3'],
                          ['selNivelInspecao', '4'],
                          ['selEmpresaAlternativaID', '5']]);
    
    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/basico/relacoes/relpessoaserecursos/relpessoasrecsinf01.asp',
                              SYS_PAGESURLROOT + '/basico/relacoes/relpessoaserecursos/relpessoasrecspesqlist.asp');                          
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01',  ['divSup02_01',
                                         'divSup02_02'],
                                        [11, 12] );

    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'RelacaoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoRelacaoID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01'],
                      [2,'divSup02_01'],
                      [2,'divSup02_02']]);
    
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblTipoRegistroID','selTipoRegistroID',20,1],
                          ['lblSujeitoID','selSujeitoID',20,1],
                          ['btnFindSujeito','btn',24,1],
                          ['lblObjetoID','selObjetoID',20,1]]);

    //@@ *** Uso Fisico - Div secundario superior divSup02_01 ***/
    adjustElementsInForm([['lblUserName','txtUserName',20,2],
						  ['lbldtUltimaSenha','txtdtUltimaSenha',17,2],
                          ['lblIniciais','txtIniciais',8,2],
                          ['lblIntervalo','txtIntervalo',5,2],
                          ['lblEnderecoIP','txtEnderecoIP',15,2],
                          ['lblEmpresaDefaultID','selEmpresaDefaultID',20,2],
                          ['lblImpCodBarras', 'selImpCodBarras', 8, 3],
                          ['lblRoteamento', 'txtRoteamento', 25, 3]],
                          ['lblHrUsoFis','hrUsoFis']);

    //@@ *** Uso Juridico - Div secundario superior divSup02_02 ***/
    adjustElementsInForm([['lblTipoEmpresaID','selTipoEmpresaID',14,2,0,-3],
						  ['lblFaturamentoMensal','txtFaturamentoMensal',12,2],
						  ['lblFatorMercado','txtFatorMercado',6,2,-6],
						  ['lblFatorEmpresa','txtFatorEmpresa',4,2,-7],
                          ['lblPrazoClienteFaturado','txtPrazoClienteFaturado',4,2,-5],
                          ['lblPrazoClienteVinculado','txtPrazoClienteVinculado',4,2,-2],
                          ['lblPercentualComissaoVendas','txtPercentualComissaoVendas',4,2,-2],
                          ['lblPercentualMaximoEmprestimos','txtPercentualMaximoEmprestimos',6,2,-1],
                          ['lblNivelInspecao','selNivelInspecao',4,2],
                          ['lblEmpresaAlternativaID', 'selEmpresaAlternativaID', 20, 2],
                          ['lblNivelPagamento','selNivelPagamento',4,2],
                          ['lblEstaEmOperacao','chkEstaEmOperacao',3,3,0,-3],
                          ['lblTemProducao','chkTemProducao',3,3,-5],
                          ['lblTemISO','chkTemISO',3,3,-5],
                          ['lblTemRIR','chkTemRIR',3,3,-5],
                          ['lblReaproveitaNotaFiscal','chkReaproveitaNotaFiscal',3,3,-5],
                          ['lblEntrega','chkEntrega',3,3,-5],
                          ['lblSabadoUtil','chkSabadoUtil',3,3,-5],
                          ['lblImportaCodigoBarra','chkImportaCodigoBarra',3,3,-5],
                          ['lblImprimeNomeEmpresaCB','chkImprimeNomeEmpresaCB',3,3,-5],
                          ['lblPercentualSUP','txtPercentualSUP',6,3,-5],
                          ['lblDiasBase','txtDiasBase',3,3],
                          ['lblNumeroItens','txtNumeroItens',3,3],
                          ['lblNumeroParcelas','txtNumeroParcelas',3,3, -5],
                          ['lblDescontoProduto','txtDescontoProduto',6,3],
                          ['lblNumeroFaixas','txtNumeroFaixas',3,3],
                          ['lblDescontoFaixa','txtDescontoFaixa',6,3, -10],
                          ['lblPrevisaoVendasMinima','txtPrevisaoVendasMinima',6,3, -10],
                          ['lblLimitePedidoReserva', 'txtLimitePedidoReserva', 6, 3],
                          ['lblLimitePedidoReservaTemporaria', 'txtLimitePedidoReservaTemporaria', 6, 3],
                          ['lblToleranciaCustoPedidoEstoque', 'txtToleranciaCustoPedidoEstoque', 4, 3],
                          ['lblToleranciaCustoContabilGerencial', 'txtToleranciaCustoContabilGerencial', 4, 3],
                          ['lblNumeroEmails','txtNumeroEmails',5,4,0,-8],
                          ['lblLimiteEmails','txtLimiteEmails',5,4],
						  ['lblMensagemNota','txtMensagemNota',60,4,-17],
						  ['lblDescontoWebHorarioNaoUtil','txtDescontoWebHorarioNaoUtil',11,5,0,-8],
						  ['lbldtDescontoWebInicio','txtdtDescontoWebInicio',14,5,-24],
						  ['lbldtDescontoWebFim','txtdtDescontoWebFim',14,5,-10]],['lblHrUsoJur','hrUsoJur']);
						  
	txtdtDescontoWebInicio.style.width = parseInt(txtdtDescontoWebInicio.currentStyle.width, 10) - 2;
	txtdtDescontoWebFim.style.width = parseInt(txtdtDescontoWebFim.currentStyle.width, 10) - 2;

    // Estes campos tem dd/mm/yyyy hh:mm:ss
    txtdtDescontoWebInicio.maxLength = 19;    

    // Estes campos tem dd/mm/yyyy hh:mm:ss
    txtdtDescontoWebFim.maxLength = 19;    
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWRELPESREC')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        window.top.focus();
        
        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    // Campos Percetuais tem minimo e maximo
    txtPercentualSUP.setAttribute('minMax', new Array(0, 100), 1);
    txtDescontoProduto.setAttribute('minMax', new Array(0, 100), 1);
    txtDescontoFaixa.setAttribute('minMax', new Array(0, 100), 1);
    txtPercentualComissaoVendas.setAttribute('minMax', new Array(0, 100), 1);
    txtdtUltimaSenha.disabled = true;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') )
        startDynamicCmbs();
    else    
        // volta para a automacao    
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    clearAndLockCmbsDynamics(btnClicked);
    
    // Ajusta os labels de sujeito e objeto
    // Apenas nos forms de relacoes
    changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID', 'lblObjetoID', null);

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
    
    if (btnClicked == 'SUPINCL')
		setMaxLengthDataFields(true);

	var sTitle = ''; 

	if (dsoSup01.recordset['TotalEmails'].value != null)
		sTitle += 'Limite de e-mails para aceitar um novo email marketing.\n' +
			 'N�mero de e-mails pendentes:' + dsoSup01.recordset['TotalEmails'].value;

	lblLimiteEmails.title = sTitle;
	txtLimiteEmails.title = sTitle;
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    if ( cmbID == 'selTipoRegistroID' )
    {
        clearComboEx(['selSujeitoID']);
        selSujeitoID.disabled = true;
        lockBtnLupa(btnFindSujeito, true);

        if ( cmbID.selectedIndex != -1 )
            lockBtnLupa(btnFindSujeito, false);
    }
    adjustLabelsCombos(false);

    // Ajusta os labels de sujeito e objeto
    // Apenas nos forms de relacoes
    if ( (cmbID == 'selTipoRegistroID') || (cmbID == 'selSujeitoID') || (cmbID == 'selObjetoID') )
        changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID',
                           'lblObjetoID', 'selTipoRegistroID');

    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    //@@ padrao do frame work
    return false;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
    
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    //@@
    var cmbID = '';
    var nTipoRelacaoID = '';
    var nRegExcluidoID = '';
    
    if ( btnClicked.id == 'btnFindSujeito' )
    {
        nTipoRelacaoID = selTipoRegistroID.value;
        nRegExcluidoID = '';
        showModalRelacoes(window.top.formID, 'S', 'selSujeitoID' , 'SUJ', getLabelNumStriped(lblSujeitoID.innerText) , nTipoRelacaoID, nRegExcluidoID);
        return null;
    }

    // Nao mexer - Inicio de automacao ==============================
    // Invoca janela modal
    loadModalOfLupa(cmbID, null);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selSujeitoID,selEmpresaDefaultID)
    glb_CounterCmbsDynamics = 2;

    // parametrizacao do dso dsoCmbDynamic01 (designado para selSujeitoID)
    setConnection(dsoCmbDynamic01);
    
    dsoCmbDynamic01.SQL = 'SELECT PessoaID,Fantasia ' +
                          'FROM Pessoas  WITH(NOLOCK) ' +
                          'WHERE PessoaID = ' + dsoSup01.recordset['SujeitoID'].value;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.refresh();

    // parametrizacao do dso dsoCmbDynamic02 (designado para selEmpresaDefaultID)
    setConnection(dsoCmbDynamic02);
    
    dsoCmbDynamic02.SQL = 'SELECT DISTINCT b.PessoaID,b.Fantasia ' +
                          'FROM RelacoesPesRec_Perfis a WITH(NOLOCK) , Pessoas b  WITH(NOLOCK) ' +
                          'WHERE a.RelacaoID = ' + dsoSup01.recordset['RelacaoID'].value + ' AND ' +
                          'a.EmpresaID = b.PessoaID AND b.EstadoID = 2 ' +
                          'ORDER BY b.Fantasia';
    
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.refresh();

}

/********************************************************************
Funcao disparada pelo programadoR.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC() {
    if (--glb_CounterCmbsDynamics > 0) return null;

	var i;
    var optionStr;
    var optionValue;
    var aCmbsDynamics = [selSujeitoID, selEmpresaDefaultID];
    var aDSOsDynamics = [dsoCmbDynamic01, dsoCmbDynamic02];

    // Inicia o carregamento de combos dinamicos (selSujeitoID,selEmpresaDefaultID)
    clearComboEx(['selSujeitoID','selEmpresaDefaultID']);
    
    for(i=0; i<=1; i++)
    {
        for(; !aDSOsDynamics[i].recordset.EOF; aDSOsDynamics[i].recordset.MoveNext())
        {
            optionStr = aDSOsDynamics[i].recordset['Fantasia'].value;
        	optionValue = aDSOsDynamics[i].recordset['PessoaID'].value;

            var oOption = document.createElement("OPTION");

            oOption.text = optionStr;
            oOption.value = optionValue;

            aCmbsDynamics[i].add(oOption);
        }
    }
    
    adjustLabelsCombos(true);
    
    // volta para a automacao
    finalOfSupCascade(glb_BtnFromFramWork);

	return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    if (controlBar == 'SUP') {
        var empresa = getCurrEmpresaData();

        // Documentos
        if ((controlBar == 'SUP') && (btnClicked == 1)) {
            __openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
        }

        // Detalhar sujeito
        else if (btnClicked == 4) {
            // Manda o id da pessoa a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], selSujeitoID.value));
        }
        // Detalhar objeto
        else if (btnClicked == 5) {
            // Manda o id do recurso a detalhar 
            sendJSCarrier(getHtmlId(), 'SHOWRECURSO', new Array(empresa[0], selObjetoID.value));
        }
        // Alterar Senha
        else if (btnClicked == 6) {
            lockInterface(true);

            validateUser();
        }
        // Modal LNC
        else if (btnClicked == 8)
            openModalLNC();
    }
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal do carrinho de compras

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalLNC()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 575;
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');
    strPars += '&nEmpresaID=' + escape(nEmpresaID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaserecursos/modalpages/modallnc.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    var txtHex1, txtHex2;

    // Identity Calculado
    if ( (controlBar == 'SUP') && (btnClicked == 'SUPOK') )
    {
        glb_RelPesRecTipoRegID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
    }
    else if (btnClicked == 'SUPINCL')
		setMaxLengthDataFields(true);

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de relacoes
    if ( idElement.toUpperCase() == 'MODALRELACOESHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            fillFieldsByRelationModal(param2);
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    // Modal LNC
    if ( idElement.toUpperCase() == 'MODALLNCHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            fillFieldsByRelationModal(param2);
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
	setMaxLengthDataFields(false);
	
    // Estes campos tem dd/mm/yyyy hh:mm:ss
    txtdtDescontoWebInicio.maxLength = 19;

    txtdtDescontoWebFim.maxLength = 19;
	
    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

function setMaxLengthDataFields(bInclusao)
{
	if (bInclusao)
	{
		clearComboEx(['selEmpresaDefaultID']);
		selEmpresaDefaultID.disabled = true;
	}
	else
		selEmpresaDefaultID.disabled = (selEmpresaDefaultID.options.length == 0);
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 0);

    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Sujeito', 'Detalhar Objeto', 'Enviar e-mail p/ usuario', 'Gerar LNC']);
}

/********************************************************************
Funcao criada pelo programador.
Limpa e trava os combos dinamicos quando o usuario clicar em inclusao

Parametro:
btnClicked  -> Ultimo botao clicado da barra superior

Retorno:
nenhum
********************************************************************/
function clearAndLockCmbsDynamics(btnClicked)
{
    if (btnClicked == 'SUPINCL')
    {
        clearComboEx(['selSujeitoID']);
        selSujeitoID.disabled = true;
        
        // trava o botao de lupa do sujeito
        lockBtnLupa(btnFindSujeito, true);
     }
}

/********************************************************************
Funcao criada pelo programador.
Preenche os campos do subform corrente, com os dados vindos da janela
modal de relacoes

Parametro:
aData   -   array de dados
            aData[0] - quem e na relacao ('SUJ' ou 'OBJ')
            aData[1] - ID do tipo da relacao
            aData[2] - ID do campo do subform corrente
            aData[3] - ID selecionado no grid da modal
            aData[4] - texto correspondente ao ID selecionado no grid da modal
            
            os demais elementos sao opcionais

Retorno:
nenhum
********************************************************************/
function fillFieldsByRelationModal(aData)
{
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var elem;
    var addSexOrServ = null;
    
    elem = window.document.getElementById(aData[2]);
    
    if ( elem == null )
        return;
    
    clearComboEx([elem.id]);
        
    oldDataSrc = elem.dataSrc;
    oldDataFld = elem.dataFld;
    elem.dataSrc = '';
    elem.dataFld = '';

    // Preencher o combo selSujeitoID
    if ( aData[2] == 'selSujeitoID' )
        dsoSup01.recordset['SujeitoID'].value = aData[3];
    // Preencher o combo selObjetoID
    else if ( aData[2] == 'selObjetoID' )
        dsoSup01.recordset['ObjetoID'].value = aData[3];

    oOption = document.createElement("OPTION");
    oOption.text = aData[4];
    oOption.value = aData[3];
    elem.add(oOption);
    elem.selectedIndex = 0;
        
    elem.dataSrc = oldDataSrc;
    elem.dataFld = oldDataFld;       

    if ( aData.length > 5 )
        addSexOrServ = aData[5];
                           
    optChangedInCmbSujOrObj(elem, addSexOrServ);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
cmb     - referencia do combo
addSexOrServ  - ID do sexo ou descricao do servico

Retorno:
nenhum
********************************************************************/
function optChangedInCmbSujOrObj(cmb, addSexOrServ)
{
    var cmbID = cmb.id;
 
    if ( cmbID == 'selSujeitoID' )
    {
        // apenas neste form comentadas as chamadas abaixo
        //clearComboEx(['selObjetoID']);
        //selObjetoID.disabled = true;
        //lockBtnLupa(btnFindObjeto, true);
        
        if ( cmb.selectedIndex != -1 )
        {
            //destrava o botao de lupa do objeto
            // lockBtnLupa(btnFindObjeto, false);
            
            // apenas neste form de relacoes,
            // destrava o combo de objeto
            selObjetoID.disabled = false;
        }    
    }

    if ( (cmbID == 'selSujeitoID') || (cmbID == 'selObjetoID') )
        changeSujObjLabels(dsoEstaticCmbs, 'lblSujeitoID',
                           'lblObjetoID', 'selTipoRegistroID',
                           selSujeitoID.value, selObjetoID.value);
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
        setLabelOfControl(lblTipoRegistroID, dsoSup01.recordset['TipoRelacaoID'].value);
        setLabelOfControl(lblEmpresaDefaultID, dsoSup01.recordset['EmpresaDefaultID'].value);
    }
    else
    {
        setLabelOfControl(lblTipoRegistroID, selTipoRegistroID.value);
        setLabelOfControl(lblEmpresaDefaultID, selEmpresaDefaultID.value);
    }
    
}
function validateUser()
{
    setConnection(dsoValidateUser);

    dsoValidateUser.SQL = 'SELECT TOP 1 a.SujeitoID ' +
                            'FROM RelacoesPesRec a WITH(NOLOCK) ' +
                                'INNER JOIN Pessoas_URLs b WITH(NOLOCK) ON b.PessoaID = a.SujeitoID ' +
                            'WHERE ((b.URL LIKE  ' + '\'' + escape(dsoSup01.recordset['UserName'].value) + '%' + '\'' + ') AND (a.ObjetoID = 999) AND (a.TipoRelacaoID = 11) AND (a.EstadoID = 2))';

    dsoValidateUser.ondatasetcomplete = dsoValidateUser_DSC;
    dsoValidateUser.refresh();
}

function dsoValidateUser_DSC()
{
    if (dsoValidateUser.recordset['SujeitoID'].value > 0) {
        changePassword();
    }
    else {
        if (window.top.overflyGen.Alert('E-mail n�o localizado.') == 0) {
            return null;
        }

        lockInterface(false);

        __btn_REFR('sup');
    }
}

function changePassword()
{
    var strPars = new String();
    var nRelacaoID = dsoSup01.recordset['RelacaoID'].value;

    strPars = '?userName=' + escape(dsoSup01.recordset['UserName'].value);

    dsoChangePassword.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaserecursos/serverside/changepassword.aspx' + strPars;
    dsoChangePassword.ondatasetcomplete = dsoChangePassword_DSC;
    dsoChangePassword.refresh();
}

/********************************************************************
Funcao do programador, retorno do servidor apos executar a procedure 
	sp_xxxx
           
Parametros: Nenhum

Retorno: Nenhum
       
********************************************************************/
function dsoChangePassword_DSC() {
    if (!((dsoChangePassword.recordset.BOF) && (dsoChangePassword.recordset.EOF))) {
        if (dsoChangePassword.recordset['Mensagem'].value != null) {
            if (window.top.overflyGen.Alert(dsoChangePassword.recordset['Mensagem'].value) == 0)
                return null;
        }
    }

    lockInterface(false);

    __btn_REFR('sup');
}