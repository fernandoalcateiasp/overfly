using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.relacoes.relpessoaserecursosEx.serverside
{
    public partial class changepassword : System.Web.UI.OverflyPage
    {
        private string username;
        private string response;
        private string mensagem;

        public string userName
        {
            set { username = (value == null ? "" : value); }
        }

        private void Senha_Insert()
        {
            ProcedureParameters[] parameters = new ProcedureParameters[] {
				new ProcedureParameters("@EmailLogin",System.Data.SqlDbType.VarChar,username,80),
				new ProcedureParameters("@Senha",System.Data.SqlDbType.VarChar,DBNull.Value,50),
				new ProcedureParameters("@GerarSenha",System.Data.SqlDbType.Bit,1),
				new ProcedureParameters("@ForcarTroca",System.Data.SqlDbType.Bit,1),
				new ProcedureParameters("@CaminhoFTP",System.Data.SqlDbType.VarChar,DBNull.Value,800),
                new ProcedureParameters("@Tipo",System.Data.SqlDbType.Bit,1),
                new ProcedureParameters("@PesUrlID",System.Data.SqlDbType.VarChar,DBNull.Value,800),
				new ProcedureParameters("@Resultado",System.Data.SqlDbType.VarChar,System.DBNull.Value,ParameterDirection.InputOutput,8000)
			};

            DataInterfaceObj.execNonQueryProcedure(
                "sp_Senha_Insert",
                parameters);

            response = parameters[5].Data.ToString();
            mensagem = (response.Length > 0) ? "A nova senha foi enviada para o e-mail do usu�rio." : "N�o foi poss�vel gerar a nova senha.";
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
			Senha_Insert();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "SELECT " + ((mensagem == null || mensagem.Length == 0) ? "NULL" :
					"'" + mensagem + "'") + " AS Mensagem"
                )
            );
        }
    }
}
