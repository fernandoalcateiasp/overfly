using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.relacoes.relpessoaserecursosEx.serverside
{
    public partial class gravalnc : System.Web.UI.OverflyPage
    {        
        private Integer tipoGravacao;
        private Integer userID;
        private Integer empresaID;
        private Integer dataLen;

        private static Integer Zero = new Integer(0);

        public Integer nTipoGravacao
        {
            set { tipoGravacao = (value != null ? value : Zero); }
        }

        public Integer nUserID
        {
            set { userID = (value != null ? value : Zero); }
        }

        public Integer nEmpresaID
        {
            set { empresaID = (value != null ? value : Zero); }
        }

        public Integer nDataLen
        {
            set { dataLen = (value != null ? value : Zero); }
        }        

        protected override void PageLoad(object sender, EventArgs e)
        {
            
        }
    }
}
