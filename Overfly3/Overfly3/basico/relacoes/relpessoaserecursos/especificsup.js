/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de relpessoaserecursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
    {
        sSQL = 'SELECT TOP 1 *, ' +
               '(SELECT CONVERT(VARCHAR, dtInicio , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, dtInicio, 108) ' +
					'FROM RelacoesPesRec b WITH(NOLOCK)  ' +
					'WHERE RelacoesPesRec.RelacaoID=b.RelacaoID) AS V_dtInicio, ' +
               '(SELECT CONVERT(VARCHAR, dtFim , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, dtFim, 108) ' +
					'FROM RelacoesPesRec b WITH(NOLOCK)  ' +
					'WHERE RelacoesPesRec.RelacaoID=b.RelacaoID) AS V_dtFim, ' +
               'CONVERT(VARCHAR, dtContabilidade, ' + DATE_SQL_PARAM + ') as V_dtContabilidade,' +
               'CONVERT(VARCHAR, dtBalanco, ' + DATE_SQL_PARAM + ') as V_dtBalanco,' +
               'CONVERT(VARCHAR, dtConciliacaoInicio, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoInicio,' +
               'CONVERT(VARCHAR, dtConciliacaoFim, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoFim,' +
               'CONVERT(VARCHAR, dtDocumentoInicio, ' + DATE_SQL_PARAM + ') as V_dtDocumentoInicio, ' +
               'CONVERT(VARCHAR, dtConciliacaoRetificacao, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoRetificacao,' +
               'CONVERT(VARCHAR, dtDescontoWebInicio, ' + DATE_SQL_PARAM + ') + SPACE(1) + CONVERT(VARCHAR, dtDescontoWebInicio,108) as V_dtDescontoWebInicio,' +
               'CONVERT(VARCHAR, dtDescontoWebFim, ' + DATE_SQL_PARAM + ') + SPACE(1) + CONVERT(VARCHAR, dtDescontoWebFim,108) as V_dtDescontoWebFim,' +
               '(SELECT TOP 1 CONVERT(VARCHAR, dtSenha, '+DATE_SQL_PARAM+') + SPACE(1) + ' +
					'CONVERT(VARCHAR, dtSenha, 108) ' +
					'FROM Pessoas_Senhas  WITH(NOLOCK)  ' +
					'WHERE PessoaID=SujeitoID ORDER BY dtSenha DESC, PesSenhaID DESC) AS dtUltimaSenha, ' +
               '(SELECT COUNT(*) FROM _Emails WITH(NOLOCK)  WHERE (dtEnvio IS NULL)) AS TotalEmails, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK)  ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=RelacoesPesRec.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM RelacoesPesRec  WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = ' + nID + ' ';

        // ID Calculado pela Trigger
        if (window.top.registroID_Type == 16)
            sSQL += 'AND ' + glb_sFldTipoRegistroName + ' = ' + glb_RelPesRecTipoRegID + ' ';

        sSQL += 'ORDER BY RelacaoID DESC';
    }           
    else
        sSQL = 'SELECT *, ' +
               '(SELECT CONVERT(VARCHAR, dtInicio , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, dtInicio, 108) ' +
					'FROM RelacoesPesRec b WITH(NOLOCK)  ' +
					'WHERE RelacoesPesRec.RelacaoID=b.RelacaoID) AS V_dtInicio, ' +
               '(SELECT CONVERT(VARCHAR, dtFim , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, dtFim, 108) ' +
					'FROM RelacoesPesRec b WITH(NOLOCK)  ' +
					'WHERE RelacoesPesRec.RelacaoID=b.RelacaoID) AS V_dtFim, ' +
               'CONVERT(VARCHAR, dtContabilidade, ' + DATE_SQL_PARAM + ') as V_dtContabilidade,' +
               'CONVERT(VARCHAR, dtBalanco, ' + DATE_SQL_PARAM + ') as V_dtBalanco,' +
               'CONVERT(VARCHAR, dtConciliacaoInicio, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoInicio,' +
               'CONVERT(VARCHAR, dtConciliacaoFim, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoFim,' +
               'CONVERT(VARCHAR, dtDocumentoInicio, ' + DATE_SQL_PARAM + ') as V_dtDocumentoInicio, ' +
               'CONVERT(VARCHAR, dtConciliacaoRetificacao, ' + DATE_SQL_PARAM + ') as V_dtConciliacaoRetificacao,' +
               'CONVERT(VARCHAR, dtDescontoWebInicio, '+DATE_SQL_PARAM+') + SPACE(1) + CONVERT(VARCHAR, dtDescontoWebInicio,108) as V_dtDescontoWebInicio,' +
               'CONVERT(VARCHAR, dtDescontoWebFim, '+DATE_SQL_PARAM+') + SPACE(1) + CONVERT(VARCHAR, dtDescontoWebFim,108) as V_dtDescontoWebFim,' +
               '(SELECT TOP 1 CONVERT(VARCHAR, dtSenha, '+DATE_SQL_PARAM+') + SPACE(1) + ' +
					'CONVERT(VARCHAR, dtSenha, 108) ' +
					'FROM Pessoas_Senhas  WITH(NOLOCK)  ' +
					'WHERE PessoaID=SujeitoID ORDER BY dtSenha DESC, PesSenhaID DESC) AS dtUltimaSenha, ' +
               '(SELECT COUNT(*) FROM _Emails WITH(NOLOCK)  WHERE (dtEnvio IS NULL)) AS TotalEmails, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
    	       'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK)  ' +
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=RelacoesPesRec.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM RelacoesPesRec  WITH(NOLOCK) ' +
               'WHERE RelacaoID = ' + nID + ' ORDER BY RelacaoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
          '(SELECT CONVERT(VARCHAR, dtInicio , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
		 	  'CONVERT(VARCHAR, dtInicio, 108) ' +
		 	  'FROM RelacoesPesRec b ' +
		 	  'WHERE RelacoesPesRec.RelacaoID=b.RelacaoID) AS V_dtInicio, ' +
          '(SELECT CONVERT(VARCHAR, dtFim , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
		 	  'CONVERT(VARCHAR, dtFim, 108) ' +
		 	  'FROM RelacoesPesRec b ' +
		 	  'WHERE RelacoesPesRec.RelacaoID=b.RelacaoID) AS V_dtFim, ' +
          'CONVERT(VARCHAR, dtContabilidade, '+DATE_SQL_PARAM+') as V_dtContabilidade,' +
          'CONVERT(VARCHAR, dtDescontoWebInicio, '+DATE_SQL_PARAM+') + SPACE(1) + CONVERT(VARCHAR, dtDescontoWebInicio,108) as V_dtDescontoWebInicio,' +
          'CONVERT(VARCHAR, dtDescontoWebFim, '+DATE_SQL_PARAM+') + SPACE(1) + CONVERT(VARCHAR, dtDescontoWebFim,108) as V_dtDescontoWebFim,' +
               '(SELECT TOP 1 CONVERT(VARCHAR, dtSenha, '+DATE_SQL_PARAM+') + SPACE(1) + ' +
					'CONVERT(VARCHAR, dtSenha, 108) ' +
					'FROM Pessoas_Senhas  ' +
					'WHERE PessoaID=SujeitoID ORDER BY dtSenha DESC, PesSenhaID DESC) AS dtUltimaSenha, ' +
          '(SELECT COUNT(*) FROM _Emails WHERE (dtEnvio IS NULL)) AS TotalEmails, ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM RelacoesPesRec WHERE RelacaoID = 0';
    
    dso.SQL = sql;          
}              
