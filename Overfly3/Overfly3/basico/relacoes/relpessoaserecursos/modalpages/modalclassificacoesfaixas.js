/********************************************************************
modalclassificacoesfaixas.js

Library javascript para o modalclassificacoesfaixas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_localTimerRefresh = null;
var glb_dataGridWasChanged = false;
var glb_nMaxStringSize = 1024*1;
// Controla se algum dado do grid foi alterado
var glb_FGTimerInt = null;
var glb_refreshGrid = null;
var glb_nDSO = 0;
var glb_nDSO2 = 0;
var glb_aDeleteds = new Array();
var glb_nI = 0;
var glb_nA1 = 0;
var glb_nA2 = 0;
var glb_nE1 = 0;
var glb_nE2 = 0;

var dsoClassificacao = new CDatatransport('dsoClassificacao');
var dsoGrid = new CDatatransport('dsoGrid');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
fillGridData()
fillGridData_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalclassificacoesfaixas.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalclassificacoesfaixas.ASP

js_fg_modalclassificacoesfaixasBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalclassificacoesfaixasDblClick( grid, Row, Col)
js_modalclassificacoesfaixasKeyPress(KeyAscii)
js_modalclassificacoesfaixas_AfterRowColChange
js_modalclassificacoesfaixas_ValidateEdit()
js_modalclassificacoesfaixas_AfterEdit(Row, Col)
js_fg_modalclassificacoesfaixas_BeforeEdit(grid, row, col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    dealWithObjects_Load();
    dealWithGrid_Load();
    
    window_onload_1stPart();
    
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    // ajusta o body do html
    with (modalclassificacoesfaixasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   

    // mostra a modal
    setConnection(dsoClassificacao);
	dsoClassificacao.SQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName ' +
				'FROM TiposAuxiliares_itens  WITH(NOLOCK) ' +
				'WHERE EstadoID=2 AND TipoID = 29 ' + 
				'ORDER BY Ordem';

	dsoClassificacao.ondatasetcomplete = window_onload_DSC;
	dsoClassificacao.Refresh();
}

function window_onload_DSC(resultServ)
{
	// mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('Valores das faixas', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var topFree, widthFree, heightFree; 
    var frameBorder = 5;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    // heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    // altura livre da janela modal (descontando apenas a barra azul)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - (2 * frameBorder);
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;

    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder);
}

/********************************************************************
Posiciona os campos na interface, define labels e hints,
controla digitacao em campos numéricos, define eventos associados
a controles, etc

Parametros:
	topFree	- inicio da altura da janela (posicao onde termina o div do
	          titulo da janela )
	heightFree - altura livre da janela (do top free ate o botao OK,
	          supondo o btnOK em baixo, que e o padrao de modal)
	widthFree - largura livre da janela
	frameBorder - largura da borda da janela
		
Retorno:
	nenhum
********************************************************************/
function adjustControlsInInterface(topFree, heightFree, widthFree, frameBorder)
{
	var elem;
	var x_leftWidth = 0;
	var y_topHeight = 0;
	var nEstado = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'EstadoID' + '\'' + '].value');
	
	var lbl_height = 16;
	var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
	
	// Posiciona os controles do divControles
	adjustElementsInForm([['btnIncluir','btn',btn_width-18,1,4,-5],
						  ['btnExcluir','btn',btn_width-18,1,5],						  
						  ['btnRefresh','btn',btn_width-18,1,5],
						  ['btnGravar','btn',btn_width-18,1,5]], null, null, true);
			
	btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;	

	// Dimensiona os divs em funcao dos parametros
				
	elem = divFG;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		top = topFree - 10;
		width = widthFree - (2 * ELEM_GAP) ;
		left = (ELEM_GAP / 2) + 1.5 ;
		height = heightFree - parseInt(btnIncluir.currentStyle.height, 10) - (3 * ELEM_GAP);
		
	}
	elem = fg;
	with (elem.style)
	{
		border = 'none';
		top = divFG.offsetTop;
		width = divFG.offsetWidth;
		left = divFG.offsetLeft;		
		height = divFG.offsetHeight;
	}	
	
	// o div de controles
	elem = divControles;
	with (elem.style)
	{
		border = 'none';
		backgroundColor = 'transparent';
		top = divFG.offsetHeight + divFG.offsetTop + ELEM_GAP;
		left = frameBorder;
		width = widthFree;
		height = parseInt(btnIncluir.currentStyle.height, 10) * 1.18;
	}
	
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    var iLinhas = 0;
    var nValor = 0;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;   
    }
    // codigo privado desta janela
    else if (controlID == 'btnCanc')
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }
    else if (controlID == 'btnRefresh')
    {
		fillGridData();
    }
    else if (controlID == 'btnGravar')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnIncluir')
    {
		incluirLinha();
    }    
    else if (controlID == 'btnExcluir')
    {
		excluirLinha();
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar interface aqui
    
    // mostra a janela modal
    fillGridData();    
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    if (glb_FGTimerInt != null)
    {
        window.clearInterval(glb_FGTimerInt);
        glb_FGTimerInt = null;
    }
    
    lockControlsInModalWin(true);
        
    // zera o grid
    fg.Rows = 1;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT * ' + 
                  'FROM RelacoesPesRec_Classificacoes_Faixas WITH(NOLOCK) ' +                  
                  'WHERE RelPesRecClassificacaoID = '+ glb_sRelPesRecClassificacaoID + ' ' +
                  'ORDER BY Valor';

    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    showExtFrame(window, true);

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    
    
    headerGrid(fg,['Valor',
                   'Classificação',
                   'RelPesRecClassFaixaID'],[2]);                
    
    fillGridMask(fg,dsoGrid,['Valor',
                             'ClassificacaoID',
                             'RelPesRecClassFaixaID'],
                             ['999999999.99','',''],
                             ['###,###,##0.00','','']);
                             
	insertcomboData(fg, getColIndexByColKey(fg, 'ClassificacaoID'), dsoClassificacao, 'fldName', 'fldID');
	
	alignColsInGrid(fg,[0]);
	
	fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.Redraw = 2;
        
    if (fg.Rows > 1)
        fg.Editable = true;
    
    lockControlsInModalWin(false);
    
    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( (fg.Row < 1) && (fg.Rows > 1) )
        fg.Row = 1;
        
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    
    
    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                    
}

function incluirLinha()
{
    var sValor = '';
    var sClassificacao = '';    
    
    fg.Editable = false;
    
    if (fg.Rows>1)
    {
        sValor = fg.TextMatrix(fg.Rows - 1, getColIndexByColKey(fg, 'Valor'));
        sClassificacao = fg.TextMatrix(fg.Rows - 1, getColIndexByColKey(fg, 'ClassificacaoID'));
        
        if ((sValor=='') && (sClassificacao == ''))
        {
            fg.Row = fg.Rows - 1;
            
            fg.Editable = true;
            
            return;
        }    
    }
        
    fg.Rows = fg.Rows + 1;
    fg.TopRow = fg.Rows - 1;
    fg.Row = fg.Rows - 1;
    glb_dataGridWasChanged = true;   
    
    fg.Editable = true;
}

function excluirLinha()
{	
    var oldRow = 0;
    var bOldState = fg.Editable;
    
    fg.Editable = false;
    
	if (fg.Row > 0)
	{
        _retMsg = window.top.overflyGen.Confirm("Deseja excluir esta linha?");
	        	
	    if ( (_retMsg == 0) || (_retMsg == 2) )
	    {
	        fg.Editable = bOldState;
	        return null;
        }
	        
        oldRow = fg.Row;	        
	
		if (fg.TextMatrix(fg.Row, fg.Cols-1) == '')
		{
			fg.RemoveItem(fg.Row);
			
			if (fg.Rows > oldRow)
				fg.Row = oldRow;
			else if (fg.Rows > 1)
				fg.Row = fg.Rows - 1;
				
		    fg.Editable = bOldState;		
		    
			return null;
		}
		
        // Altera o campo do dsoGrid com o novo valor
        if (!dsoGrid.recordset.EOF)
        {
			dsoGrid.recordset.MoveFirst();
			dsoGrid.recordset.Find(fg.ColKey(fg.Cols-1), fg.TextMatrix(fg.Row, fg.Cols-1));
        }

        if (!dsoGrid.recordset.EOF)
        {
			fg.RemoveItem(fg.Row);
			dsoGrid.recordset.Delete();
			
			if (fg.Rows > oldRow)
				fg.Row = oldRow;
			else if (fg.Rows > 1)
				fg.Row = fg.Rows - 1;
			
			glb_dataGridWasChanged = true;			
        }
	}
	
	fg.Editable = bOldState;
}


/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
    if (!writeNewRowsInDSO())
		return null;
	
	glb_dataGridWasChanged = false;		
	lockControlsInModalWin(true);		
    try
    {
        dsoGrid.SubmitChanges();
    }
    catch(e)
    {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887)
        {
            if ( window.top.overflyGen.Alert ('Este registro acaba de ser alterado ou removido.') == 0 )
                return null;
        }
        
        fg.Rows = 1;
        lockControlsInModalWin(false);
        
		glb_FGTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
        return null;
    }    

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    glb_FGTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
}

function writeNewRowsInDSO()
{
	var i, j;
	var sColuna = '';		
	var sMsgError = '';
	var nLineError = 0;
	var nLinkedFieldValueID = glb_sRelPesRecClassificacaoID;
	var sLinkedField = 'RelPesRecClassificacaoID';
	var bRetVal = true;
	var bOldState = fg.Editable;
	
	fg.Editable = false;
			
	for (i=1; i<fg.Rows; i++)
	{
		if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
			dsoGrid.recordset.MoveFirst();
		
		nFieldKeyID = fg.TextMatrix(i,getColIndexByColKey(fg, 'RelPesRecClassFaixaID'));

		// Linha Nova
		if (nFieldKeyID == '')
			dsoGrid.recordset.AddNew();
		else
			continue;

		dsoGrid.recordset[sLinkedField].value = parseInt(nLinkedFieldValueID,10);			
		
		for (j=0; j<fg.Cols; j++)
		{
			sColuna = fg.ColKey(j);
			
			if (sColuna.toUpperCase() == 'RELPESRECCLASSFAIXAID')
				continue;

		    if ((sColuna.toUpperCase() == 'VALOR') &&
			    (fg.TextMatrix(i, j) == ''))
		    {
			    sMsgError = 'Preencha um valor';
			    nLineError = i;
		    }
		    else if ((sColuna.toUpperCase() == 'CLASSIFICACAOID') &&
			    (fg.TextMatrix(i, j) == ''))
		    {
			    sMsgError = 'Preencha uma classificação';
			    nLineError = i;
		    }
			
			if (sMsgError != '')
				break;

			nType = dsoGrid.recordset[fg.ColKey(j)].type;
	        
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
			{
				if ( (sColuna.substr(sColuna.length - 2).toUpperCase() == 'ID') &&
					 (fg.ValueMatrix(i, j) == 0) )
					dsoGrid.recordset[fg.ColKey(j)].value = '';
				else
					dsoGrid.recordset[fg.ColKey(j)].value = fg.ValueMatrix(i, j);
			}
			else    
				dsoGrid.recordset[fg.ColKey(j)].value = fg.TextMatrix(i, j);
		}
	}
	
	if (sMsgError != '')
	{
		dsoGrid.recordset.Delete();
		bRetVal = false;
        if ( window.top.overflyGen.Alert (sMsgError) == 0 )
            return null;
            
		fg.Row = nLineError;
	}
	
	fg.Editable = bOldState;
	
	return bRetVal;
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalclassificacoesfaixasBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalclassificacoesfaixas_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));
    
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalclassificacoesfaixasDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalclassificacoesfaixasKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalclassificacoesfaixas_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalclassificacoesfaixas_AfterEdit(Row, Col)
{
    var nType = 0;
	
    if (fg.Editable)
    {
		if (dsoGrid.recordset.Fields.Count > 0)
		{
			nType = dsoGrid.recordset[fg.ColKey(Col)].type;
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
				fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
		}

		if (fg.TextMatrix(Row, fg.Cols-1) != '')
		{
			// Altera o campo do dso com o novo valor

			dsoGrid.recordset.MoveFirst();
			dsoGrid.recordset.Find(fg.ColKey(fg.Cols-1), fg.TextMatrix(Row, fg.Cols-1));

			if ( !(dsoGrid.recordset.EOF) )
			{
				nType = dsoGrid.recordset[fg.ColKey(Col)].type;
	            
				// Se decimal , numerico , int, bigint ou boolean
				if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
					dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
				else    
					dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);
	                
				glb_dataGridWasChanged = true;				
			}
        }
        else
			glb_dataGridWasChanged = true;
    }
}

// FINAL DE EVENTOS DE GRID *****************************************

function refreshParamsAndDataAndShowModalWin(fromServer) {
    if (glb_refreshGrid != null) {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }

    /*
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    */

    // mostra a janela modal
    fillGridData();
    showExtFrame(window, true);
}

function window_onunload()
{
    dealWithObjects_Unload();
}