/********************************************************************
modalbalver.js

Library javascript para o modalbalver.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;

var dsoGrid = new CDatatransport('dsoGrid');
var dsoDetalhamento = new CDatatransport('dsoDetalhamento');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
getCurrEmpresaData()
fillGridData()
fillGridData_DSC()

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalbalver.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

js_fg_DblClick()

FINAL DE DEFINIDAS NO ARQUIVO modalbalver.ASP
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    
    // ajusta o body do html
    with (modalbalverBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
    with (btnCanc.style)
    {
		left = (parseInt(divFG.style.width,10) / 2) - (parseInt(btnCanc.style.width,10) / 2);
		visibility = 'hidden';
    }

    // Define TOP da modal
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 25;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText('EHO - Estrutura Hierarquica Organizacional', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

	// ajusta os controles do divControles
	lblOK.style.visibility = 'inherit';
	chkOK.style.visibility = 'inherit';
	
	if (chkOK.style.visibility == 'inherit')
	{
		adjustElementsInForm([['lblOK','chkOK',3,1,-10,-10],
							['lblQualidade','chkQualidade',3,1],
							['lblColaboradores','chkColaboradores',3,1],
							['btnFillGrid','btn',btnOK.offsetWidth,1, 10]],null,null,true);
	}
	else
	{
		adjustElementsInForm([['lblQualidade','chkQualidade',3,1,-10,-10],
							['lblColaboradores','chkColaboradores',3,1],
							['btnFillGrid','btn',btnOK.offsetWidth,1, 10]],null,null,true);
	}						  

	btnFillGrid.style.height = btnOK.offsetHeight;
	btnFillGrid.style.left = modWidth - ELEM_GAP - parseInt(btnFillGrid.currentStyle.width) - 14;

	chkOK.checked = true;
	chkOK.onclick = chkQualidade_onclick;
	chkQualidade.checked = false;
	chkQualidade.onclick = chkQualidade_onclick;
	chkColaboradores.checked = true;
	chkColaboradores.onclick = chkQualidade_onclick;

	// ajusta o divControles
	with (divControles.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnFillGrid.currentStyle.top, 10) + parseInt(btnFillGrid.currentStyle.height, 10);
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControles.currentStyle.top, 10) +
			  parseInt(divControles.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) -
				 parseInt(top, 10);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = 
		(getFrameInHtmlTop( 'frameSup01')).offsetTop;
		
	//redimAndReposicionModalWin(modWidth, modHeight, false);
    
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 3;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    
    try {
		with (fg)
		{
			FontName = 'Tahoma, Arial, MS SansSerif';
			FontSize = '10';
   			Editable = false;
			AllowUserResizing = 1;
			VirtualData = true;
			Ellipsis = 1;
			SelectionMode = 1;
			AllowSelection = false;
			Rows = 1;
			Cols = 3;
			FixedRows = 1;
			FixedCols = 2;
			ScrollBars = 3;
			OutLineBar = 5;
			GridLines = 0;
			FormatString = 'Hierarquia' + '\t' + 'N�vel' + '\t' + 'NivelHierarquico';
			OutLineBar = 1;
			GridLinesFixed = 13;
			GridLines = 1;
			GridColor = 0X000000;
			ColHidden(2) = true;
			ColKey(0) = 'Fantasia';
			ColKey(1) = 'Nivel';
			ColKey(2) = 'NivelHierarquico';
		}
	} catch(e) {
		alert(e);
		alert(e.message);
	}
	
    fg.Redraw = 2;

	showExtFrame(window, true);

	window.focus();
	chkQualidade.focus();
}

function chkQualidade_onclick()
{
	fg.Rows = 1;
}

/********************************************************************
Enter no campo data
********************************************************************/
function txtData_onKeyPress()
{
    if ( event.keyCode == 13 )
        btn_onclick(btnFillGrid);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
		;
    }
    else if (controlID == 'btnCanc')
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
    }    
    else if (controlID == 'btnFillGrid')
		btnFillGrid_onclick();
}

function btnFillGrid_onclick()
{
	fg.Rows = 1;
    fillGridData();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    lockControlsInModalWin(true);
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    var strSQL = '';

	strSQL = 'EXEC sp_RelacoesPesRec_EHO ' + glb_aEmpresaData[0] + ',' +
		(chkOK.checked ? '1' : '0') + ', ' + 
		(chkQualidade.checked ? '1' : '0') + ', ' + 
		(chkColaboradores.checked ? '1' : '0');

    dsoGrid.SQL = strSQL;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    var dTFormat = '';
    var i, j;
    var bgColorGreen, bgColorYellow, bgColorRed;
    var nTipoDetalhamentoID = 0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    
	if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
	{
		glb_GridIsBuilding = true;
		while (!dsoGrid.recordset.EOF)
		{
			fg.Row = fg.Rows - 1;

			fg.AddItem('', fg.Row+1);
			
			if (fg.Row < (fg.Rows-1))
				fg.Row++;
			
			fg.ColDataType(getColIndexByColKey(fg, 'Fantasia')) = 12;
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Fantasia')) = dsoGrid.recordset['Fantasia'].value;	
			
			fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) = (dsoGrid.recordset['Nivel'].value == null ? dsoGrid.recordset['NivelHierarquico'].value : dsoGrid.recordset['Nivel'].value);

			fg.ColDataType(getColIndexByColKey(fg, 'NivelHierarquico')) = 12;
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'NivelHierarquico')) = dsoGrid.recordset['NivelHierarquico'].value;	
			
			fg.IsSubTotal(fg.Row) = true;
			fg.RowOutlineLevel(fg.Row) = dsoGrid.recordset['NivelHierarquico'].value - 1;
			
			dsoGrid.recordset.MoveNext();
		}
	}

	glb_GridIsBuilding = false;
            
    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ( fg.Rows > 1 )
        fg.Row = 1;
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 2;
    
    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }
    else
    {
        ;
    }            
}

function js_fg_DblClick()
{
	var node = fg.GetNode();

	try
	{
		node.Expanded = !node.Expanded;
	}	
	catch(e)
	{
		;	
	}	
}

function fg_modalbalver_AfterRowColChange()
{
    if ( glb_GridIsBuilding )
        return true;
}
