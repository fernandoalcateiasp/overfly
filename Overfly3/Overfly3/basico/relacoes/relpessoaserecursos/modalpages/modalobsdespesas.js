﻿var glb_selIdiomaIndexOld = -1;

var dsoQueries = new CDatatransport('dsoQueries');

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalqueriesBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    if (! txtQuery.readOnly )
        txtQuery.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Observações', 1);

    // reajusta dimensoes e reposiciona a janela
    (getFrameInHtmlTop( getExtFrameID(window))).style.top = (getFrameInHtmlTop( 'frameSup01')).offsetTop;

    // reajusta dimensoes e reposiciona a janela
	//redimAndReposicionModalWin(740, 490, false);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    var nMemoHeight = 375;
    
    // ajusta o divCampos
    /*
    elem = window.document.getElementById('divCampos');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 7);
        temp = parseInt(width);
        height = 50;
    }
    */
    
    // ajusta o divObservacoes
    elem = window.document.getElementById('divObservacoes');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 7);
        temp = parseInt(width);
        //height = (nMemoHeight * 1.5);
        height = 320;
    }

    elem = document.getElementById('txtQuery');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = MAX_FRAMEWIDTH - (ELEM_GAP * 7);
        height = divObservacoes.offsetHeight - 3;
    }

	setConnection(dsoQueries);
    dsoQueries.SQL = 'SELECT Observacoes FROM RelacoesPesRec_Despesas WHERE RelPesRecDespesaID = ' + glb_nRelPesRecDespesaID;
    dsoQueries.ondatasetcomplete = dsoQueries_DSC;
    dsoQueries.Refresh();
}

function dsoQueries_DSC()
{
    dsoQueries.recordset.moveFirst();
    
    if ((!dsoQueries.recordset.BOF) && (!dsoQueries.recordset.EOF))
    {
        txtQuery.maxLength = dsoQueries.recordset['Observacoes'].definedSize;
        if (dsoQueries.recordset['Observacoes'].value != null)
            txtQuery.value = dsoQueries.recordset['Observacoes'].value;
        else
            txtQuery.value = '';
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
	var tempStr = '';
	var match = 0;
	
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        saveQueries();
		// 1. O usuario clicou o botao OK
        //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , txtQuery.value);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    
}

function saveQueries()
{
    var i=0;
    
    lockControlsInModalWin(true);
    
    if (!((dsoQueries.recordset.BOF) && (dsoQueries.recordset.EOF)))   
    {
        dsoQueries.recordset.moveFirst();
        dsoQueries.recordset['Observacoes'].value = txtQuery.value;
        dsoQueries.recordset.moveNext();
    }

	try
	{			
	    dsoQueries.SubmitChanges();   
		dsoQueries.ondatasetcomplete = gravacao_DSC;
		dsoQueries.Refresh();
	}
	catch(e)
	{
	    // Numero de erro qdo o registro foi alterado ou removido por outro usuario
	    // ou operacao de gravacao abortada no banco
	    if (e.number == -2147217887)
	    {
	        if ( window.top.overflyGen.Alert('Erro ao gravar.') == 0 )
	            return null;
	            
	        lockControlsInModalWin(false);
	    }
	}
}

function gravacao_DSC()
{
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null);
}

