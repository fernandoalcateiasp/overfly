/********************************************************************
modallnc.js

Library javascript para o modallnc.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Controla se algum dado do grid foi alterado
var glb_enableBtnsGravacao = false;
var glb_nDSOs = 0;
var glb_LVATimerInt = null;

var dsoGrid = new CDatatransport('dsoGrid');
var dsoCombo = new CDatatransport('dsoCombo');
var dsoGrava = new CDatatransport('dsoGrava');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chks_onclick()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
geraRET()
saveDataInGrid_DSC()
saveRetInGrid_DSC()
selectFirstCellGridModalLNC()
paintCellsSpecialyReadOnly()

js_fg_modallncBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_fg_modallncDblClick(grid, Row, Col)
js_modallncKeyPress(KeyAscii)
js_modallnc_ValidateEdit()
js_modallnc_BeforeEdit(grid, row, col)
js_modallnc_AfterEdit(Row, Col)
js_fg_AfterRowColModalLNC (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modallncBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    showExtFrame(window, true);
   
    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);

    // Define TOP da modal
    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 25;
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	if (glb_sCaller == 'PL')
		secText('LNC - Levantamento de Necessidades de Competências', 1);
	else
	{
		var sColaborador = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'selSujeitoID.options(selSujeitoID.selectedIndex).innerText');
		secText('LNC - ' + sColaborador, 1);
	}	

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;
    
    // desabilita o botao OK
    btnOK.disabled = true;
	btnRET.disabled = true;
	
    // troca label botao Cancelar   
    btnCanc.value = 'Cancelar';
    
    adjustElementsInForm([['lblQualidade','chkQualidade',3,1,-10,-10],
						  ['lblOK','chkOK',3,1],
						  ['lblSuperiorID','selSuperiorID',20,1],
						  ['lblPendentes','chkPendentes',3,1],
						  ['lblEducacao','chkEducacao',3,1],
						  ['lblExperiencia','chkExperiencia',3,1],
						  ['lblHabilidade','chkHabilidade',3,1],
						  ['lblTreinamento','chkTreinamento',3,1]], null, null, true);

    chkQualidade.onclick = chks_onclick;
    chkOK.onclick = chks_onclick;
    selSuperiorID.onchange = selSuperiorID_onchange;
    chkPendentes.onclick = chks_onclick;
	chkEducacao.onclick = chks_onclick;
	chkExperiencia.onclick = chks_onclick;
	chkHabilidade.onclick = chks_onclick;
	chkTreinamento.onclick = chks_onclick;
	
    chkQualidade.checked = true;
    chkOK.checked = true;
    chkPendentes.checked = false;
	chkEducacao.checked = true;
	chkExperiencia.checked = true;
	chkHabilidade.checked = true;
	chkTreinamento.checked = true;

    // ajusta o divFields
    with (divFields.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = chkPendentes.offsetTop + chkPendentes.offsetHeight + 2;
    }

    // ajusta o divFG
    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFields.currentStyle.top, 10) + parseInt(divFields.currentStyle.height, 10) + 1;
        width = modWidth - 2 * ELEM_GAP - 6;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - (6 * ELEM_GAP);
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
	with (txtObservacoes)
	{
        style.left = 10;
        style.top = divFG.offsetTop + divFG.offsetHeight + 12;
        style.width = parseInt(divFG.style.width, 10);
        style.height = 19 * 4;
        readOnly = true;
	}

    // Reposiciona botao Listar
    with (btnListar)
    {         
		style.top = parseInt(divFields.currentStyle.top, 10) + (ELEM_GAP / 2);
		style.left = parseInt(chkTreinamento.currentStyle.left) + parseInt(chkTreinamento.currentStyle.width) + 2*ELEM_GAP;
		style.visibility = 'inherit';
    }

    // Reposiciona botao OK
    with (btnOK)
    {         
		style.top = parseInt(divFields.currentStyle.top, 10) + (ELEM_GAP / 2);
		style.left = modWidth - ELEM_GAP - parseInt(btnOK.currentStyle.width) - 4;
		value = 'Avaliar';
    }
    
    with (btnRET)
    {         
		style.width = btnOK.currentStyle.width;
		style.height = btnOK.currentStyle.height;
		style.top = btnOK.currentStyle.top;
		style.left = parseInt(btnOK.currentStyle.left, 10) - parseInt(btnRET.currentStyle.width, 10) - 4;
		value = 'Gerar RET';
    }
    
    // Esconde botao cancel
    with (btnCanc)
    {
		style.visibility = 'hidden';
    }

	adjustLabelsCombos();
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    fg.Redraw = 2;
}

function chks_onclick()
{
	if (this == chkQualidade)
		fillCmbs();
	else if (this == chkOK)		
		fillCmbs();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        saveDataInGrid();
    }
    else if (controlID == 'btnListar')
    {
		txtObservacoes.value = '';
		fillGridData();
    }
    else if (controlID == 'btnRET')
    {
		geraRET();
    }
    // codigo privado desta janela
    else
    {
        var _retMsg;
        var sMsg = '';
            
        if ( sMsg != '' )
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
	        	    
	        if ( (_retMsg == 0) || (_retMsg == 2) )
	            return null;
        }    
        
        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false ); 
    }    
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;
    
    lockControlsInModalWin(true);
        
    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
	fillCmbs();
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 1;
    
    btnOK.disabled = !(bHasRowsInGrid && glb_enableBtnsGravacao);
    btnRET.disabled = !(bHasRowsInGrid && glb_enableBtnsGravacao);
}

function fillCmbs()
{
	if (glb_LVATimerInt != null)
    {
        window.clearInterval(glb_LVATimerInt);
        glb_LVATimerInt = null;
    }

	var sFiltroQualidade = '';
	var sFiltroOK = '';

	if (chkQualidade.checked)
		sFiltroQualidade = ' AND e.Qualidade = 1 ';
	
	if (chkOK.checked)
		sFiltroOK = ' AND d.OK = 1 ';

	fg.Rows = 1;
	
	lockControlsInModalWin(true);
	
    setConnection(dsoCombo);
    dsoCombo.SQL = 'SELECT 0 AS fldID, SPACE(0) AS fldName ' +
		'UNION ALL SELECT DISTINCT f.PessoaID AS fldID, f.Fantasia AS fldName ' +
			'FROM RelacoesPesRec a WITH(NOLOCK) , RelacoesPesRec_Perfis b WITH(NOLOCK) , RelacoesPesRec c WITH(NOLOCK) , RelacoesPesRec_Perfis d WITH(NOLOCK) , Recursos e WITH(NOLOCK) , Pessoas f WITH(NOLOCK)  ' +
			'WHERE (a.TipoRelacaoID = 11 AND a.EstadoID = 2 AND a.ObjetoID = 999 AND ' +
				'a.RelacaoID = b.RelacaoID AND b.EmpresaID = ' + glb_nEmpresaID + ' AND ' +
				'a.ProprietarioID = c.SujeitoID AND c.TipoRelacaoID = 11 AND c.ObjetoID = 999 AND ' +
				'c.RelacaoID = d.RelacaoID AND d.EmpresaID = ' + glb_nEmpresaID + ' AND d.PerfilID = e.RecursoID AND ' +
				'c.SujeitoID = f.PessoaID ' +
				sFiltroQualidade + sFiltroOK + ') ' +
		'ORDER BY fldName ';

    dsoCombo.ondatasetcomplete = fillCmbs_DSC;
    dsoCombo.Refresh();

}

function fillCmbs_DSC() {
    var i;
    var optionStr, optionValue;
    
	clearComboEx([selSuperiorID.id]);

    while (! dsoCombo.recordset.EOF )
    {
        optionStr = dsoCombo.recordset['fldName'].value;
	    optionValue = dsoCombo.recordset['fldID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selSuperiorID.add(oOption);
        
        dsoCombo.recordset.MoveNext();
    }

	selSuperiorID.selectedIndex = -1;

	lockControlsInModalWin(false);
	
	selSuperiorID.disabled = (selSuperiorID.options.length == 0);

	adjustLabelsCombos();
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if (glb_LVATimerInt != null)
    {
        window.clearInterval(glb_LVATimerInt);
        glb_LVATimerInt = null;
    }
    
	var aGrid = null;
	var i = 0;
	var strSQL = '';
	var nSujeitoID = 0;

	lockControlsInModalWin(true);
	
	glb_nDSOs = 1;
    
	if (glb_sCaller == 'PL')
		nSujeitoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,'getCellValueByColKey(fg, ' + '\'' + 'SujeitoID' + '\'' + ',fg.Row)');
	else
		nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'SujeitoID' + '\'' + '].value');

    // zera o grid
    fg.Rows = 1;
    
    glb_enableBtnsGravacao = false;
    
    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

	if ( glb_sCaller == 'PL' )
	{
		aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');
		for (i=0; i<aGrid.length; i++)
		{
		    strSQL = strSQL + aGrid[i];

		    if (i != (aGrid.length-1))
		        strSQL = strSQL + ',';
		}
		
		dsoGrid.SQL = 'EXEC sp_RelacoesPesRec_LNC NULL, ' + glb_aEmpresaData[0] + ', ' + 
			((selSuperiorID.value > 0) ? selSuperiorID.value : 'NULL') + ', ' + 
			(chkQualidade.checked ? 1 : 0) + ', ' + 
			(chkOK.checked ? 1 : 0) + ', ' + 
			(chkPendentes.checked ? 1 : 0) + ', ' + 
			(chkEducacao.checked ? 1 : 0) + ', ' + 
			(chkExperiencia.checked ? 1 : 0) + ', ' + 
			(chkHabilidade.checked ? 1 : 0) + ', ' + 
			(chkTreinamento.checked ? 1 : 0) + ', ' + 
			'\'' + strSQL + '\'';
	}
	else
	{
		strSQL = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'RelacaoID' + '\'' + '].value');
		
		dsoGrid.SQL = 'EXEC sp_RelacoesPesRec_LNC ' + nSujeitoID + ', ' + glb_aEmpresaData[0] + ', ' + 
			((selSuperiorID.value > 0) ? selSuperiorID.value : 'NULL') + ', ' + 
			(chkQualidade.checked ? 1 : 0) + ', ' + 
			(chkOK.checked ? 1 : 0) + ', ' + 
			(chkPendentes.checked ? 1 : 0) + ', ' + 
			(chkEducacao.checked ? 1 : 0) + ', ' + 
			(chkExperiencia.checked ? 1 : 0) + ', ' + 
			(chkHabilidade.checked ? 1 : 0) + ', ' + 
			(chkTreinamento.checked ? 1 : 0) + ', ' + 
			'\'' + strSQL + '\'';
	}			
			
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    glb_nDSOs--;
	
	if (glb_nDSOs > 0)
		return null;

    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    var i;
    var aLinesState = new Array();
    var bTempValue;
    var aHideCols;
        
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    
    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;    

    if ( glb_sCaller == 'PL' )
		aHideCols = [1, 3, 5, 13, 14, 15, 16];
    else
		aHideCols = [1, 2, 3, 5, 13, 14, 15, 16];

    headerGrid(fg,['Superior',
				   'PessoaID',
                   'Colaborador',
                   'TipoID',
				   'Tipo',
				   'CompetenciaID',
				   'Requerido',
				   'Existente',
				   'Lacuna',
				   'X',
				   'OK',
				   'Avaliação',
				   'Responsável',
				   'Observacoes',
				   'RelacaoID',
				   'RETID',
				   'RegistroID'], aHideCols);

    fillGridMask(fg,dsoGrid,['Superior*',
							 'PessoaID',
                             'Pessoa*',
                             'TipoID',
							 'Tipo*',
							 'CompetenciaID',
							 'Requerido*',
							 'Existente*',
							 'Lacuna*',
							 'AV',
							 'OK',
							 'dtAvaliacao*',
							 'Responsavel*',
							 'Observacoes',
							 'RelacaoID',
							 'RETID',
							 'RegistroID'],
							 ['', '', '', '', '', '', '', '', '', '', '', '99/99/9999', '', '', '', '', '' ],
                             ['', '', '', '', '', '', '', '', '', '', '', dTFormat, '', '', '', '', '']);

    alignColsInGrid(fg,[]);                           

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    if ( glb_sCaller == 'PL' )
		fg.FrozenCols = 3;
    else
		fg.FrozenCols = 5;

	paintCellsSpecialyReadOnly();
	    
    fg.Redraw = 2;
    
    if ( fg.Rows > 1 )
		selectFirstCellGridModalLNC();

    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a partir da primeira coluna readonly
    if ( fg.Rows > 1 )
        fg.Col = getColIndexByColKey(fg, 'OK');

    if (fg.Rows > 1)
        fg.Editable = true;
        
	fg.ExplorerBar = 5;
	
	lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;    

    fg.Redraw = 2;

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }                
    else
    {
        ;
    }            

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	
    glb_enableBtnsGravacao = false;
    lockControlsInModalWin(true);
    
    strPars = '?nTipoGravacao=' + escape(1);
    strPars += '&nUserID=' + escape(glb_nUserID);
    
    for (i=1; i<fg.Rows; i++)
    {
		if ( (getCellValueByColKey(fg, 'AV', i) != 0) && 
			 ((getCellValueByColKey(fg, 'TipoID', i) != 1704) || (getCellValueByColKey(fg, 'RETID', i) != '')) )
		{
			nDataLen++;
			strPars += '&nPessoaID=' + escape(getCellValueByColKey(fg, 'PessoaID', i));
			strPars += '&nTipoID=' + escape(getCellValueByColKey(fg, 'TipoID', i));
			strPars += '&nCompetenciaID=' + escape(getCellValueByColKey(fg, 'CompetenciaID', i));
			strPars += '&nRETID=' + escape(getCellValueByColKey(fg, 'RETID', i));
			strPars += '&nRelacaoID=' + escape(getCellValueByColKey(fg, 'RelacaoID', i));
			
			if ( getCellValueByColKey(fg, 'OK', i) != 0 )
				strPars += '&bOK=' + escape(1);
			else
				strPars += '&bOK=' + escape(0);
		}	
	}
	
	strPars += '&nDataLen=' + escape(nDataLen);

    if (nDataLen > 0)
    {
		dsoGrava.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaserecursos/serverside/gravalnc.asp' + strPars;
		dsoGrava.ondatasetcomplete = saveDataInGrid_DSC;
		dsoGrava.refresh();
    }
    else
		saveDataInGrid_DSC();
}

function geraRET()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nEmpresaID = glb_aEmpresaData[0];
	
    glb_enableBtnsGravacao = false;
    lockControlsInModalWin(true);
    
    strPars = '?nTipoGravacao=' + escape(2);
    strPars += '&nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nUserID=' + escape(glb_nUserID);
    
    for (i=1; i<fg.Rows; i++)
    {
		if ( (getCellValueByColKey(fg, 'AV', i) != 0) &&  (getCellValueByColKey(fg, 'TipoID', i) == 1704) && 
			 (getCellValueByColKey(fg, 'RETID', i) == '') )
		{
			nDataLen++;
			strPars += '&nTreinamentoID=' + escape(getCellValueByColKey(fg, 'CompetenciaID', i));
			strPars += '&nPessoaID=' + escape(getCellValueByColKey(fg, 'PessoaID', i));
		}	
	}
	
	strPars += '&nDataLen=' + escape(nDataLen);

    if (nDataLen > 0)
    {
		dsoGrava.URL = SYS_ASPURLROOT + '/basico/relacoes/relpessoaserecursos/serverside/gravalnc.asp' + strPars;
		dsoGrava.ondatasetcomplete = saveRetInGrid_DSC;
		dsoGrava.refresh();
    }
    else
		saveRetInGrid_DSC();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

	glb_LVATimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveRetInGrid_DSC() {
    lockControlsInModalWin(false);

	if ( !(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF))
	{
		dsoGrava.recordset.MoveFirst();
        if ( window.top.overflyGen.Alert(dsoGrava.recordset['fldResponse'].value) == 0 )
			return null;
	}

	glb_LVATimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');  
}

/********************************************************************
Selecao inicial de celula
********************************************************************/
function selectFirstCellGridModalLNC()
{
	var  bCondition, nColIsReadOnly;
	var i;
	
	for ( i=1; i<fg.Rows; i++ )
	{
		bCondition = ((getCellValueByColKey(fg, 'TipoID', i) == 1704) && (getCellValueByColKey(fg, 'CompetenciaID', i) == 101) && (getCellValueByColKey(fg, 'RETID', i) == ''));
		
		if ( !((getCellValueByColKey(fg, 'TipoID', i) <= 1702) || (bCondition)) )
		{
			// Column OK
			nColIsReadOnly = getColIndexByColKey(fg, 'OK');

			if ( nColIsReadOnly > -1 )
			{
				// Cell in OK column is not read only
				fg.Col = nColIsReadOnly;
				fg.Row = i;
				break;
			}
			
			// Column AV
			nColIsReadOnly = getColIndexByColKey(fg, 'AV');
			
			if ( !((nColIsReadOnly > -1)  && (!bCondition)) )
			{
				// Cell in AV column is not read only
				fg.Col = nColIsReadOnly;
				fg.Row = i;
				break;
			}
		}       
	}	
}

/********************************************************************
Pinta celulas especialmente read only
********************************************************************/
function paintCellsSpecialyReadOnly()
{
	var  bCondition, nColIsReadOnly;
	var i;
	
	for ( i=1; i<fg.Rows; i++ )
	{
		bCondition = ((getCellValueByColKey(fg, 'TipoID', i) == 1704) && (getCellValueByColKey(fg, 'CompetenciaID', i) == 101) && (getCellValueByColKey(fg, 'RETID', i) == ''));
		
		if ( (getCellValueByColKey(fg, 'TipoID', i) <= 1702) || (bCondition) )
		{
			// Column OK
			nColIsReadOnly = getColIndexByColKey(fg, 'OK');

			if ( nColIsReadOnly > -1 )
			{
				// Cell in OK column is read only
				paintBackgroundOfACell(fg, i, nColIsReadOnly, 0XDCDCDC);
			}
			
			// Column AV
			nColIsReadOnly = getColIndexByColKey(fg, 'AV');
			
			if ( (nColIsReadOnly > -1)  && (!bCondition) )
			{
				// Cell in AV column is read only
				paintBackgroundOfACell(fg, i, nColIsReadOnly, 0XDCDCDC);
			}
		}       
	}	
}

function selSuperiorID_onchange()
{
	fg.Rows = 1;
	adjustLabelsCombos();
	// ajusta estado dos botoes
	setupBtnsFromGridState();
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblSuperiorID, selSuperiorID.value);
}

// EVENTOS DE GRID **************************************************

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modallncBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modallncDblClick(grid, Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallncKeyPress(KeyAscii)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallnc_ValidateEdit()
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallnc_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modallnc_AfterEdit(Row, Col)
{
	var  bCondition = ((getCellValueByColKey(fg, 'TipoID', Row) == 1704) && (getCellValueByColKey(fg, 'CompetenciaID', Row) == 101) && (getCellValueByColKey(fg, 'RETID', Row) == ''));

	if ( (getCellValueByColKey(fg, 'TipoID', Row) <= 1702) || (bCondition) )
	{
		if (Col == getColIndexByColKey(fg, 'OK'))
		{
			if (getCellValueByColKey(fg, 'OK', Row) != 0)
				fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 0;
			else
				fg.TextMatrix(Row, getColIndexByColKey(fg, 'OK')) = 1;	
		}
		else if ( (Col == getColIndexByColKey(fg, 'AV')) && (!bCondition) )
		{
			if (getCellValueByColKey(fg, 'AV', Row) != 0)
				fg.TextMatrix(Row, getColIndexByColKey(fg, 'AV')) = 0;
			else
				fg.TextMatrix(Row, getColIndexByColKey(fg, 'AV')) = 1;	
		}
	}       

	if (Col == getColIndexByColKey(fg, 'AV'))
		glb_enableBtnsGravacao = true;
	
	setupBtnsFromGridState();
}

/********************************************************************
Atualiza linha de mensagens abaixo do grid.

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
********************************************************************/
function js_fg_AfterRowColModalLNC (grid, OldRow, OldCol, NewRow, NewCol)
{
	txtObservacoes.value = getCellValueByColKey(fg, 'Observacoes', NewRow);
}

// FINAL DE EVENTOS DE GRID *****************************************
