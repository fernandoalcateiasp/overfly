/********************************************************************
modalseguradora.js

Library javascript para o modalseguradora.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoPesq = new CDatatransport('dsoPesq');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES
/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalseguradoraBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtSujObj').disabled == false )
        txtSujObj.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Seguradora' , 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divCidade
    elem = window.document.getElementById('divCidade');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width);
        height = 40;
        
    }
    
    // txtSujObj
    elem = window.document.getElementById('txtSujObj');
    elem.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (elem.style)
    {
        left = 0;
        top = 16;
        width = (elem.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtSujObj').style.top);
        left = parseInt(document.getElementById('txtSujObj').style.left) + parseInt(document.getElementById('txtSujObj').style.width) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divCidade').style.top) + parseInt(document.getElementById('divCidade').style.height) + ELEM_GAP;
        width = temp + 140;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

           headerGrid(fg,['Fantasia',
						  'Nome'], []);
    fg.Redraw = 2;
}

function txtSujObj_ondigit(ctl)
{
    changeBtnState(ctl.value);
    
    if ( event.keyCode == 13 )
        btnFindPesquisa_onclick(btnFindPesquisa);
}

function btnFindPesquisa_onclick(ctl)
{
    txtSujObj.value = trimStr(txtSujObj.value);
    
    changeBtnState(txtSujObj.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtSujObj.value);
}

function fg_DblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', new Array(
                      fg.TextMatrix(fg.Row, 0), fg.TextMatrix(fg.Row, 2) ));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    
	var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 
							'dsoSup01.recordset[' + '\'' + 'SujeitoID' + '\'' + '].value');
    
    setConnection(dsoPesq);
/*
    dsoPesq.SQL = 'SELECT b.PessoaID, b.Fantasia, b.Nome ' +
		'FROM RelacoesPessoas a, Pessoas b ' +
		'WHERE a.SujeitoID=' + nEmpresaID + ' AND a.TipoRelacaoID=21 AND a.EstadoID=2 AND ' +
		'a.ObjetoID=b.PessoaID AND b.Fantasia >= ' + '\'' + txtSujObj.value + '\'' + ' ' +
		'ORDER BY b.Fantasia';
*/

    dsoPesq.SQL = 'SELECT b.PessoaID, b.Fantasia, b.Nome ' +
		'FROM Pessoas b  WITH(NOLOCK) ' +
		'WHERE b.Fantasia >= ' + '\'' + txtSujObj.value + '\'' + ' AND b.ClassificacaoID=70 ' +
		'ORDER BY b.Fantasia';

    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() {
    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg,['Fantasia',
                   'Nome',
                   'PessoaID'], [2]);

    fillGridMask(fg,dsoPesq,['Fantasia',
							 'Nome',
							 'PessoaID'],
							 ['','','']);
    lockControlsInModalWin(false);
    
    window.focus();
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        btnOK.disabled = false;
        fg.focus();
    }    
    else
    {
        btnOK.disabled = true;
        txtSujObj.focus();
    }    

    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
}
