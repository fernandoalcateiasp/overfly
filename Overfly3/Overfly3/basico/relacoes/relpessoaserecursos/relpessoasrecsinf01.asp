<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- ID do arquivo -->
<!-- //@@ -->
<html id="relpessoasrecsinf01Html" name="relpessoasrecsinf01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailinf.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaserecursos/relpessoasrecsinf01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaserecursos/especificinf.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">

</SCRIPT>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>

 fg_KeyPress(arguments[0]);

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPressEdit>

js_fg_KeyPressEdit(arguments[0], arguments[1], arguments[2]);

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>

 js_fg_EnterCell(fg);

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>

 js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseUp>

 fg_MouseUp();

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseDown>

 fg_MouseDown();

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>

 fg_BeforeEdit();

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>

 fg_DblClick();

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ChangeEdit>

 fg_ChangeEdit();

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>

 fg_ValidateEdit();

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>

 js_fg_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>

 js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);

</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>

 js_PesRec_AfterEdit(fg.Row, fg.Col);

</SCRIPT>

</head>

<!-- //@@ -->
<body id="relpessoasrecsinf01Body" name="relpessoasrecsinf01Body" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- LANGUAGE="javascript" onchange="return optChangedInCmb(this)", que esta no inf01.js" -->
    <!-- //@@ Todos os controles cujo onclick e invocado convergem para -->
    <!-- a funcao onClickControl(cmb), que esta no inf01.js" -->
    
    <!-- Primeiro div inferior - Observacoes -->
    <!-- O div com o controle abaixo e comum a todos os forms -->
    <div id="divInf01_01" name="divInf01_01" class="divExtern">
        <textarea id="txtObservacoes" name="txtObservacoes" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Observacoes"></textarea>
    </div>
    
    <!-- Segundo div inferior - Prop Alter -->
    <!-- O div com os controles abaixo e comum a todos os forms -->
    <div id="divInf01_02" name="divInf01_02" class="divExtern">
        <p  id="lblProprietario" name="lblProprietario" class="lblGeneral">Propriet�rio</p>
        <select id="selProprietario" name="selProprietario" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Proprietarioid"></select>
	    <!-- //@@ ajustar path abaixo-->
        <!-- //XX N�O TEM path PARA AJUSTAR -->
	    <input type="image" id="btnFindProprietario" name="btnFindProprietario" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23"></input>
        <p id="lblAlternativo" name="lblAlternativo" class="lblGeneral">Alternativo</p>
        <select id="selAlternativo" name="selAlternativo" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="AlternativoID"></select>
	    <!-- //@@ ajustar path abaixo-->
        <!-- //XX N�O TEM path PARA AJUSTAR -->
	    <input type="image" id="btnFindAlternativo" name="btnFindAlternativo" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23"></input>
    </div>
    
    <!-- Terceiro div inferior -->
    <div id="divInf01_03" name="divInf01_03" class="divExtern">
        <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtdtInicio" name="txtdtInicio" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="V_dtInicio" title="Data de in�cio do sistema"></input>
        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtdtFim" name="txtdtFim" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="V_dtFim" title="Data de fim do sistema"></input>
        <p id="lbldtContabilidade" name="lbldtContabilidade" class="lblGeneral">Contabilidade</p>
        <input type="text" id="txtdtContabilidade" name="txtdtContabilidade" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="V_dtContabilidade" title="Data de in�cio da contabilidade"></input>
        
        <p id="lbldtBalanco" name="lbldtBalanco" class="lblGeneral">�ltimo Balan�o</p>
        <input type="text" id="txtdtBalanco" name="txtdtBalanco" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="V_dtBalanco" title="Data do �ltimo balan�o"></input>
        <p id="lblDiasBalanco" name="lblDiasBalanco" class="lblGeneral">Dias</p>
        <input type="text" id="txtDiasBalanco" name="txtDiasBalanco" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="DiasBalanco" title="N�mero de dias que a data do �ltimo balan�o pode se aproximar do dia de hoje"></input>
        <p id="lbldtConciliacaoInicio" name="lbldtConciliacaoInicio" class="lblGeneral">Concilia��o In�cio</p>
        <input type="text" id="txtdtConciliacaoInicio" name="txtdtConciliacaoInicio" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="V_dtConciliacaoInicio" title="Data in�cio do per�odo de concilia��o cont�bil"></input>
        <p id="lbldtConciliacaoFim" name="lbldtConciliacaoFim" class="lblGeneral">Concilia��o Fim</p>
        <input type="text" id="txtdtConciliacaoFim" name="txtdtConciliacaoFim" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="V_dtConciliacaoFim" title="Data fim do per�odo de concilia��o cont�bil"></input>
        <p id="lblDiasConciliacao" name="lblDiasConciliacao" class="lblGeneral">Dias</p>
        <input type="text" id="txtDiasConciliacao" name="txtDiasConciliacao" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="DiasConciliacao" title="N�mero m�ximo de dias do per�odo de concilia��o"></input>
        
        <p id="lbldtDocumentoInicio" name="lbldtDocumentoInicio" class="lblGeneral">Documento In�cio</p>
        <input type="text" id="txtdtDocumentoInicio" name="txtdtDocumentoInicio" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="V_dtDocumentoInicio" title="Data m�nima que o sistema permite aprovar um documento"></input>
        
        <p id="lbldtConciliacaoRetificacao" name="lbldtConciliacaoRetificacao" class="lblGeneral">Conc Retifica��o</p>
        <input type="text" id="txtdtConciliacaoRetificacao" name="txtdtConciliacaoRetificacao" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="V_dtConciliacaoRetificacao" title="Sistema suspende retifica��o das datas de concilia��o at� esta data"></input>

        <p id="lblCorFrente" name="lblCorFrente" class="lblGeneral">Frente</p>
        <input type="text" id="txtCorFrente" name="txtCorFrente" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="CorFrente"></input>
        <p id="lblCorFundo" name="lblCorFundo" class="lblGeneral">Fundo</p>
        <input type="text" id="txtCorFundo" name="txtCorFundo" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="CorFundo"></input>
        <p id="lblCaminhoGuiaRecolhimento" name="lblCaminhoGuiaRecolhimento" class="lblGeneral">Caminho Guia Recolhimento</p>
        <input type="text" id="txtCaminhoGuiaRecolhimento" name="txtCaminhoGuiaRecolhimento" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="CaminhoGuiaRecolhimento"></input>
    </div>
    
    <!-- Quarto div inferior -->
    <div id="divInf01_04" name="divInf01_04" class="divExtern">
		<p id="lblEducacaoID" name="lblEducacaoID" class="lblGeneral">Educa��o</p>
		<select id="selEducacaoID" name="selEducacaoID" DATASRC="#dso01JoinSup" DATAFLD="EducacaoID" class="fldGeneral"></select>
		<p id="lblDuracao" name="lblDuracao" class="lblGeneral">Dur</p>
		<input type="text" id="txtDuracao" name="txtDuracao" DATASRC="#dso01JoinSup" DATAFLD="Duracao" class="fldGeneral" title="Dura��o do curso (anos)"></input>
		<p id="lblConcluido" name="lblConcluido" class="lblGeneral">Con</p>
		<input type="text" id="txtConcluido" name="txtConcluido" DATASRC="#dso01JoinSup" DATAFLD="Concluido" class="fldGeneral" title="Anos concluidos"></input>
	    <p id="lblCursando" name="lblCursando" class="lblGeneral">Curs</p>
	    <input type="checkbox" id="chkCursando" name="chkCursando" DATASRC="#dso01JoinSup" DATAFLD="Cursando" class="fldGeneral" Title="Est� cursando?"></input>
    </div>

    <!-- Quinto div inferior - Grid simples: Recursos Filhos, Wizard -->
    <!-- Transicao de Estados, Relacoes entre Recursos, Relacoes com Pessoas -->
    <div id="divInf01_05" name="divInf01_05" class="divExtern">
        <OBJECT CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </OBJECT>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>
</body>

</html>
