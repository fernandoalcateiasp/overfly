<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="relpessoasrecssup01Html" name="relpessoasrecssup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaserecursos/relpessoasrecssup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/relacoes/relpessoaserecursos/especificsup.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="relpessoasrecssup01Body" name="relpessoasrecssup01Body" LANGUAGE="javascript" onload="return window_onload()">

    <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">        
        
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Rela��o</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoRelacaoID"></select>    
        <p id="lblSujeitoID" name="lblSujeitoID" class="lblGeneral">Sujeito</p>
        <select id="selSujeitoID" name="selSujeitoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="SujeitoID"></select>    
	    <input type="image" id="btnFindSujeito" name="btnFindObjeto" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
        <p id="lblObjetoID" name="lblObjetoID" class="lblGeneral">Objeto</p>
        <select id="selObjetoID" name="selObjetoID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ObjetoID"></select>    
    </div>

    <!-- Primeiro div secundario superior - Uso Fis -->    
    <div id="divSup02_01" name="divSup02_01" class="divExtern">
        <p id="lblHrUsoFis" name="lblHrUsoFis" class="lblGeneral">Uso (F�s)</p>
        <hr id="hrUsoFis" name="hrUsoFis" class="lblGeneral">
		<p id="lblUserName" name="lblUserName" class="lblGeneral">User Name</p>
		<input type="text" id="txtUserName" DATASRC="#dsoSup01" DATAFLD="UserName" name="txtUserName" class="fldGeneral"></input>
		<p id="lbldtUltimaSenha" name="lbldtUltimaSenha" class="lblGeneral">Data �ltima Senha</p>
		<input type="text" id="txtdtUltimaSenha" DATASRC="#dsoSup01" DATAFLD="dtUltimaSenha" name="txtdtUltimaSenha" class="fldGeneral"></input>
		<p id="lblIniciais" name="lblIniciais" class="lblGeneral">Iniciais</p>
		<input type="text" id="txtIniciais" name="txtIniciais" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Iniciais">
		<p id="lblIntervalo" name="lblIntervalo" class="lblGeneral">Intervalo</p>
		<input type="text" id="txtIntervalo" name="txtIntervalo" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Intervalo" title="Intervalo para refei��es">
		<p id="lblEnderecoIP" name="lblEnderecoIP" class="lblGeneral">Endere�o IP</p>
		<input type="text" id="txtEnderecoIP" name="txtEnderecoIP" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EnderecoIP">
        <p id="lblEmpresaDefaultID" name="lblEmpresaDefaultID" class="lblGeneral">Empresa Default</p>
        <select id="selEmpresaDefaultID" name="selEmpresaDefaultID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EmpresaDefaultID">
        </select>
        <p id="lblImpCodBarras" name="lblImpCodBarras" class="lblGeneral">Imp C�d Barras</p>
        <select id="selImpCodBarras" name="selImpCodBarras" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ImpressoraCodigoBarra">
			<option value="1">1</option>
			<option value="2">2</option>
        </select>
		<p id="lblRoteamento" name="lblRoteamento" class="lblGeneral">Roteamento</p>
		<input type="text" id="txtRoteamento" name="txtRoteamento" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Roteamento">
    </div>

    <!-- Segundo div secundario superior - Uso Jur -->    
    <div id="divSup02_02" name="divSup02_02" class="divExtern">    
        <p id="lblHrUsoJur" name="lblHrUsoJur" class="lblGeneral">Uso (Jur)</p>
        <hr id="hrUsoJur" name="hrUsoJur" class="lblGeneral">
        <p id="lblTipoEmpresaID" name="lblTipoEmpresaID" class="lblGeneral">Tipo de Empresa</p>
        <select id="selTipoEmpresaID" name="selTipoEmpresaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoEmpresaID"></select>
        <p id="lblFaturamentoMensal" name="lblFaturamentoMensal" class="lblGeneral">Faturamento</p>
        <input type="text" id="txtFaturamentoMensal" name="txtFaturamentoMensal" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FaturamentoMensal" title="Faturamento Mensal (US$)">
        <p id="lblFatorMercado" name="lblFatorMercado" class="lblGeneral">Fator Merc</p>
        <input type="text" id="txtFatorMercado" name="txtFatorMercado" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FatorMercado" title="Fator de Mercado">
        <p id="lblFatorEmpresa" name="lblFatorEmpresa" class="lblGeneral">Fator Emp</p>
        <input type="text" id="txtFatorEmpresa" name="txtFatorEmpresa" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="FatorEmpresa" title="Fator entre empresa">
        <p id="lblPrazoClienteFaturado" name="lblPrazoClienteFaturado" class="lblGeneral">Fat</p>
        <input type="text" id="txtPrazoClienteFaturado" name="txtPrazoClienteFaturado" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PrazoClienteFaturado" title="Prazo (em dias corridos) de reten��o de cliente faturado">
        <p id="lblPrazoClienteVinculado" name="lblPrazoClienteVinculado" class="lblGeneral">Vinc</p>
        <input type="text" id="txtPrazoClienteVinculado" name="txtPrazoClienteVinculado" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PrazoClienteVinculado" title="Prazo (em dias corridos) de reten��o de cliente vinculado">
		<p id="lblPercentualComissaoVendas" name="lblPercentualComissaoVendas" class="lblGeneral">CV</p>
		<input type="text" id="txtPercentualComissaoVendas" name="txtPercentualComissaoVendas" DATASRC="#dsoSup01" DATAFLD="PercentualComissaoVendas" class="fldGeneral" title="Percentual de Comiss�o de Vendas Geral"></input>

		<p id="lblPercentualMaximoEmprestimos" name="lblPercentualMaximoEmprestimos" class="lblGeneral">PME</p>
		<input type="text" id="txtPercentualMaximoEmprestimos" name="txtPercentualMaximoEmprestimos" DATASRC="#dsoSup01" DATAFLD="PercentualMaximoEmprestimos" class="fldGeneral" title="Percentual M�ximo de Empr�stimos"></input>


        <p id="lblNivelInspecao" name="lblNivelInspecao" class="lblGeneral" title="N�vel de Inspe��o de Recebimento">NI</p>
        <select id="selNivelInspecao" name="selNivelInspecao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NivelInspecao"></select>
        <p id="lblEmpresaAlternativaID" name="lblEmpresaAlternativaID" class="lblGeneral" title="N�vel de Inspe��o de Recebimento">Empresa Alternativa</p>
        <select id="selEmpresaAlternativaID" name="selEmpresaAlternativaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EmpresaAlternativaID"></select>

        <p id="lblNivelPagamento" name="lblNivelPagamento" class="lblGeneral" title="N�vel de pagamento default">N�vel</p>
        <select id="selNivelPagamento" name="selNivelPagamento" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NivelPagamento">
			<option value="1">1</option>
			<option value="2">2</option>
			<option value="3">3</option>
			<option value="4">4</option>
		</select>
        <p id="lblEstaEmOperacao" name="lblEstaEmOperacao" class="lblGeneral">Oper</p>
        <input type="checkbox" id="chkEstaEmOperacao" name="chkEstaEmOperacao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="EstaEmOperacao" title="Empresa est� em opera��o?">
        <p id="lblTemProducao" name="lblTemProducao" class="lblGeneral">Prod</p>
        <input type="checkbox" id="chkTemProducao" name="chkTemProducao" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TemProducao" title="Tem Produ��o?">
        <p id="lblTemISO" name="lblTemISO" class="lblGeneral">ISO</p>
        <input type="checkbox" id="chkTemISO" name="chkTemISO" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TemISO" title="Tem ISO9000?">
        <p id="lblTemRIR" name="lblTemRIR" class="lblGeneral">RIR</p>
        <input type="checkbox" id="chkTemRIR" name="chkTemRIR" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TemRIR" title="Tem Inspe��o de Recebimento?">
        <p id="lblReaproveitaNotaFiscal" name="lblReaproveitaNotaFiscal" class="lblGeneral">Nota</p>
        <input type="checkbox" id="chkReaproveitaNotaFiscal" name="chkReaproveitaNotaFiscal" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ReaproveitaNotaFiscal" title="Reaproveita n�mero da Nota Fiscal se cancelar um formul�rio?">
        <p id="lblEntrega" name="lblEntrega" class="lblGeneral">Entr</p>
        <input type="checkbox" id="chkEntrega" name="chkEntrega" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="Entrega" title="Fazemos entrega?">
        <p id="lblSabadoUtil" name="lblSabadoUtil" class="lblGeneral">S�b</p>
        <input type="checkbox" id="chkSabadoUtil" name="chkSabadoUtil" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="SabadoUtil" title="S�bado � dia �til?">
        <p id="lblImportaCodigoBarra" name="lblImportaCodigoBarra" class="lblGeneral">Imp CB</p>
        <input type="checkbox" id="chkImportaCodigoBarra" name="chkImportaCodigoBarra" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ImportaCodigoBarra" title="Importa C�digo de Barra?">
        <p id="lblImprimeNomeEmpresaCB" name="lblImprimeNomeEmpresaCB" class="lblGeneral">IN</p>
        <input type="checkbox" id="chkImprimeNomeEmpresaCB" name="chkImprimeNomeEmpresaCB" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ImprimeNomeEmpresaCB" title="Imprime nome da empresa no C�digo de Barra?">
        <p id="lblPercentualSUP" name="lblPercentualSUP" class="lblGeneral">Perc SUP</p>
        <input type="text" id="txtPercentualSUP" name="txtPercentualSUP" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PercentualSUP" title="Percentual de acr�scimo de impostos p/ SUP">
        <p id="lblDiasBase" name="lblDiasBase" class="lblGeneral">Base</p>
        <input type="text" id="txtDiasBase" name="txtDiasBase" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DiasBase" title="N�mero de dias base para c�lculo de juros">
        <p id="lblNumeroItens" name="lblNumeroItens" class="lblGeneral">�tens</p>
        <input type="text" id="txtNumeroItens" name="txtNumeroItens" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NumeroItens" title="N�mero de �tens no pedido">
        <p id="lblNumeroParcelas" name="lblNumeroParcelas" class="lblGeneral">Parc</p>
        <input type="text" id="txtNumeroParcelas" name="txtNumeroParcelas" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NumeroParcelas" title="N�mero de parcelas no pedido">
        <p id="lblDescontoProduto" name="lblDescontoProduto" class="lblGeneral">Desc Prod</p>
        <input type="text" id="txtDescontoProduto" name="txtDescontoProduto" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescontoProduto" title="Desconto p/ produtos com Margem M�nima nula">
        <p id="lblNumeroFaixas" name="lblNumeroFaixas" class="lblGeneral">Faixas</p>
        <input type="text" id="txtNumeroFaixas" name="txtNumeroFaixas" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="NumeroFaixas" title="N�mero de faixas de desconto">
        <p id="lblDescontoFaixa" name="lblDescontoFaixa" class="lblGeneral">Desc Faixa</p>
        <input type="text" id="txtDescontoFaixa" name="txtDescontoFaixa" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescontoFaixa" title="Desconto por faixa">
        <p id="lblPrevisaoVendasMinima" name="lblPrevisaoVendasMinima" class="lblGeneral">PVM</p>
        <input type="text" id="txtPrevisaoVendasMinima" name="txtPrevisaoVendasMinima" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="PrevisaoVendasMinima" title="Previs�o de Vendas M�nima">
        <p id="lblLimitePedidoReserva" name="lblLimitePedidoReserva" class="lblGeneral">LPR</p>
        <input type="text" id="txtLimitePedidoReserva" name="txtLimitePedidoReserva" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="LimitePedidoReserva" title="Limite em horas que um pedido pode permanecer em R">
        <p id="lblLimitePedidoReservaTemporaria" name="lblLimitePedidoReservaTemporaria" class="lblGeneral">LPRT</p>
        <input type="text" id="txtLimitePedidoReservaTemporaria" name="txtLimitePedidoReservaTemporaria" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="LimitePedidoReservaTemporaria" title="Limite em minutos que um pedido ao sair da reserva garante a reserva de pe�as.">

        <p id="lblToleranciaCustoPedidoEstoque" name="lblToleranciaCustoPedidoEstoque" class="lblGeneral">TCPE</p>
        <input type="text" id="txtToleranciaCustoPedidoEstoque" name="txtToleranciaCustoPedidoEstoque" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ToleranciaCustoPedidoEstoque" title="Limite (%) de varia��o de custo que um pedido pode causar no estoque de um produto.">
        
        <p id="lblToleranciaCustoContabilGerencial" name="lblToleranciaCustoContabilGerencial" class="lblGeneral">TCCG</p>
        <input type="text" id="txtToleranciaCustoContabilGerencial" name="txtToleranciaCustoContabilGerencial" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ToleranciaCustoContabilGerencial" title="Limite (%) de varia��o entre o Custo Cont�bil e o Custo Gerencial de um produto.">

		<p id="lblNumeroEmails" name="lblNumeroEmails" class="lblGeneral">E-mails</p>
		<input type="text" id="txtNumeroEmails" name="txtNumeroEmails" DATASRC="#dsoSup01" DATAFLD="NumeroEmails" class="fldGeneral" title="N�mero de e-mails marketing para envio."></input>
		<p id="lblLimiteEmails" name="lblLimiteEmails" class="lblGeneral">Lim e-mails</p>
		<input type="text" id="txtLimiteEmails" name="txtLimiteEmails" DATASRC="#dsoSup01" DATAFLD="LimiteEmails" class="fldGeneral" title=""></input>
        <p id="lblMensagemNota" name="lblMensagemNota" class="lblGeneral">Mensagem Nota</p>
        <input type="text" id="txtMensagemNota" name="txtMensagemNota" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MensagemNota">
        <p id="lblDescontoWebHorarioNaoUtil" name="lblDescontoWebHorarioNaoUtil" class="lblGeneral">Desc Web Hor � �til</p>
        <input type="text" id="txtDescontoWebHorarioNaoUtil" name="txtDescontoWebHorarioNaoUtil" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="DescontoWebHorarioNaoUtil">
        <p id="lbldtDescontoWebInicio" name="lbldtDescontoWebInicio" class="lblGeneral">In�cio Desc Web</p>
        <input type="text" id="txtdtDescontoWebInicio" name="txtdtDescontoWebInicio" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtDescontoWebInicio">
        <p id="lbldtDescontoWebFim" name="lbldtDescontoWebFim" class="lblGeneral">Fim Desc Web</p>
        <input type="text" id="txtdtDescontoWebFim" name="txtdtDescontoWebFim" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="V_dtDescontoWebFim">
    </div>

</body>

</html>
