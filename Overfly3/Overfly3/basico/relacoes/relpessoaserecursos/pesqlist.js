/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form relpessoasrecursos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoListData01 = new CDatatransport('dsoListData01');
var dsoCmbsContFilt = new CDatatransport('dsoCmbsContFilt');
var dsoPropsPL = new CDatatransport('dsoPropsPL');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

retArrayGrid()

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Rela��o', 'SujeitoID', 'Sujeito', 'ObjetoID', 'Objeto');
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Sujeito', 'Detalhar Objeto', 'Enviar e-mail p/ usuario', 'EHO', 'LNC']);

    if (fg.Rows > 1)
        setupEspecBtnsControlBar('sup', 'HDDDDDHH');
    else
        setupEspecBtnsControlBar('sup', 'DDDDDDHD');
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // usuario clicou botao documentos
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }

    else if (btnClicked == 7)
        openModalEHO();
    else if (btnClicked == 8)
        openModalLNC();
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal do carrinho de compras

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalLNC()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 575;
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nEmpresaID=' + escape(nEmpresaID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaserecursos/modalpages/modallnc.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

function openModalEHO()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 400;
    var nHeight = 575;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('PL');
    strPars += '&sCurrDateFormat=' + escape(DATE_FORMAT);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaserecursos/modalpages/modaleho.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal LNC
    if ( idElement.toUpperCase() == 'MODALLNCHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            fillFieldsByRelationModal(param2);
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    if ( idElement.toUpperCase() == 'MODALBALVERHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            fillFieldsByRelationModal(param2);
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}

/********************************************************************
Funcao criada pelo programador.
retorna um array com os id's que estao no grid no momento

Parametro:
nenhum

Retorno:
array com os id's que estao no grid no momento
********************************************************************/
function retArrayGrid()
{
    if (fg.Rows <= 1)
        return null;
        
    var aGrid = new Array(fg.Rows-1);
    var i;

    for (i=1; i<fg.Rows; i++)
        aGrid[i-1] = fg.TextMatrix(i, getColIndexByColKey(fg, 'RelacaoID'));

    return aGrid;
}
