/********************************************************************
relpessoaserecursosinf01.js

Library javascript para o relpessoaserecursosinf01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dso01JoinSup = new CDatatransport('dso01JoinSup');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
var dsoDynamicCmbs = new CDatatransport('dsoDynamicCmbs');
var dso01Grid = new CDatatransport('dso01Grid');
var dsoCmb01Grid_01 = new CDatatransport('dsoCmb01Grid_01');
var dsoCmb02Grid_01 = new CDatatransport('dsoCmb02Grid_01');
var dsoCmb03Grid_01 = new CDatatransport('dsoCmb03Grid_01');   
var dsoCmb04Grid_01 = new CDatatransport('dsoCmb04Grid_01');    
var dsoCmb05Grid_01 = new CDatatransport('dsoCmb05Grid_01');
var dsoCmb06Grid_01 = new CDatatransport('dsoCmb06Grid_01');
var dsoCmb07Grid_01 = new CDatatransport('dsoCmb07Grid_01');
var dsoCmb08Grid_01 = new CDatatransport('dsoCmb08Grid_01');
var dsoCmb09Grid_01 = new CDatatransport('dsoCmb09Grid_01');
var dsoCmb10Grid_01 = new CDatatransport('dsoCmb10Grid_01');
var dsoCmb11Grid_01 = new CDatatransport('dsoCmb11Grid_01');    
var dso01GridLkp = new CDatatransport('dso01GridLkp');
var dso02GridLkp = new CDatatransport('dso02GridLkp');
var dso03GridLkp = new CDatatransport('dso03GridLkp');
var dso04GridLkp = new CDatatransport('dso04GridLkp');
var dsoStateMachineLkp = new CDatatransport('dsoStateMachineLkp');
var dsoFiltroInf = new CDatatransport('dsoFiltroInf');
var dsoCmbsPropAlt = new CDatatransport('dsoCmbsPropAlt');
var dsoCmbLayout = new CDatatransport('dsoCmbLayout');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    adjustElementsInDivsNonSTD()
    putSpecialAttributesInControls()
    optChangedInCmb(cmb)
    prgServerInf(btnClicked)
    prgInterfaceInf(btnClicked, pastaID, dso)
    finalOfInfCascade(btnClicked)
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, currDSO)
    btnLupaClicked(btnClicked)
    addedLineInGrid(folderID, grid, nLineInGrid, dso);
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    btnAltPressedInGrid( folderID )
    btnAltPressedInNotGrid( folderID )
    fillCmbFiltsInfAutomatic(folderID)
    selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
    frameIsAboutToApplyRights(folderID)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID )

EVENTOS DO GRID FG
    fg_AfterRowColChange_Prg()
    fg_DblClick_Prg()
    fg_ChangeEdit_Prg()
    fg_ValidateEdit_Prg()
    fg_BeforeRowColChange_Prg()
    fg_EnterCell_Prg()
    fg_MouseUp_Prg()
    fg_MouseDown_Prg()
    fg_BeforeEdit_Prg()

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();
    
    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    
    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selEducacaoID', '1']]);
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 
    glb_aGridStaticCombos = [ [20010, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dso02GridLkp'   , ''             , ''],
                                       [0, 'dso03GridLkp'   , ''             , ''],
                                       [0, 'dso04GridLkp'   , ''             , '']]],
                              [20053, [[0, 'dsoCmb01Grid_01', 'Fantasia', 'PessoaID'],
                                       [2, 'dsoCmb02Grid_01', 'RecursoFantasia', 'RecursoID'],  
                                       [3, 'dsoCmb03Grid_01', 'Nivel', 'Nivel']]],  
                              [20055, [[1, 'dsoCmb01Grid_01', 'SimboloMoeda', 'ConceitoID']]],
                              [20056, [[0, 'dsoCmb01Grid_01', 'SimboloMoeda', 'ConceitoID']]],
                              [20057, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
                                       [3, 'dsoCmb02Grid_01', 'ItemMasculino', 'ItemID']]],
                              [20058, [[3, 'dsoCmb01Grid_01', '*Conta|Agencia|Codigo', 'RelPesContaID'],
                                       [29, 'dsoCmb02Grid_01', 'Tipo', 'TipoID'],
									   [0, 'dso01GridLkp'   , ''             , ''],
									   [0, 'dsoStateMachineLkp', ''          , '']]],
                              [20059, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
                              [20060, [[1, 'dsoCmb01Grid_01', 'NivelQualidadeAceitavel', 'NivelQualidadeAceitavel']]],
                              [20063, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
                              [20065, [[2, 'dsoCmb01Grid_01', 'RecursoFantasia', 'RecursoID'],
									   [3, 'dsoCmb02Grid_01', 'Fantasia', 'PessoaID']]],
                              [20066, [[0, 'dsoCmb01Grid_01', 'Competencia', 'CompetenciaID'],
									   [0, 'dso01GridLkp'   , ''             , '']]],
						      [20068, [[0, 'dsoStateMachineLkp', ''             , ''      ],
						               [0, 'dsoCmb01Grid_01'   , 'ItemAbreviado', 'ItemID'],
						               [3, 'dsoCmb02Grid_01'   , 'fldName', 'fldID']]],
							  [20069, [[0, 'dsoStateMachineLkp', ''             , ''      ],
									   [0, 'dso01GridLkp'   , ''             , ''],
						               [0, 'dsoCmb01Grid_01'   , 'Financiamento', 'FinanciamentoID'],
						               [3, 'dsoCmb02Grid_01'   , 'fldName', 'fldID']]],
							  [20070, [[0, 'dso01GridLkp'   , ''             , ''],
							           [0, 'dsoCmb01Grid_01'   , 'fldName', 'fldID']]],
							           
							  [20071, [[0, 'dsoCmb01Grid_01', 'fldName', 'fldID'],
							           [1, 'dsoCmb11Grid_01', 'fldName', 'fldID'],
							           [2, 'dsoCmb02Grid_01', 'fldName', 'fldID'], 
                                       [5, 'dsoCmb03Grid_01', 'fldName', 'fldID'],
									   [6, 'dsoCmb04Grid_01', 'fldName', 'fldID'],
									   [7, 'dsoCmb05Grid_01', 'fldName', 'fldID'],
									   [9, 'dsoCmb06Grid_01', 'fldName', 'fldID'],
									   [10, 'dsoCmb07Grid_01', 'fldName', 'fldID'],
									   [11, 'dsoCmb08Grid_01', 'fldName', 'fldID'],
									   [12, 'dsoCmb09Grid_01', 'fldName', 'fldID'],
									   [13, 'dsoCmb10Grid_01', 'fldName', 'fldID']]],
									   
							  [20072, [[0, 'dsoCmb01Grid_01'   , 'fldName', 'fldID'],
									   [1, 'dsoCmb02Grid_01'   , 'fldName', 'fldID']]],
                              [20073, [ [0, 'dsoCmb01Grid_01'   , 'fldName', 'fldID'],
									   [1, 'dsoCmb02Grid_01'   , 'fldName', 'fldID'] ]],
                              [20074, [[0, 'dsoCmb01Grid_01'   , 'fldName', 'fldID']]],
							  [21029, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]],
                              [28083, [[0, 'dsoCmb01Grid_01', 'fldName', 'fldID']]]];

    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03',
                               'divInf01_04',
                               'divInf01_05'],
                               [20008, 20009, 20061, 20064,
                               [20010, 20053, 20054, 20055, 20056, 20057, 20058, 20059, 20060, 20062, 20063,
                               20065, 20066, 20067, 20068, 20069, 20070, 20071, 20072, 20073, 20074, 21029, 28083]]);
        
    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

function setupPage()
{
    adjustElementsInForm([['lbldtInicio','txtdtInicio',14,1],
                          ['lbldtFim','txtdtFim',14,1],
                          ['lbldtContabilidade','txtdtContabilidade',10,1],
                          ['lbldtBalanco','txtdtBalanco',10,1],
                          ['lblDiasBalanco','txtDiasBalanco',4,1],
                          ['lbldtConciliacaoInicio','txtdtConciliacaoInicio',10,1],
                          ['lbldtConciliacaoFim','txtdtConciliacaoFim',10,1,-26],
                          ['lblDiasConciliacao', 'txtDiasConciliacao', 4, 1, -9],
                          ['lbldtDocumentoInicio', 'txtdtDocumentoInicio', 10, 1],
                          ['lbldtConciliacaoRetificacao', 'txtdtConciliacaoRetificacao', 10, 1, -13],
                          ['lblCorFrente','txtCorFrente',7, 2],
                          ['lblCorFundo', 'txtCorFundo', 7, 2],
                          ['lblCaminhoGuiaRecolhimento', 'txtCaminhoGuiaRecolhimento', 30, 2]], null, null, true);

    adjustElementsInForm([['lblEducacaoID','selEducacaoID',18,1],
                          ['lblDuracao','txtDuracao',3,1],
                          ['lblConcluido','txtConcluido',3,1],
                          ['lblCursando','chkCursando',3,1]],null,null,true);

	txtdtInicio.style.width = parseInt(txtdtInicio.currentStyle.width, 10) - 2;
	txtdtFim.style.width = parseInt(txtdtFim.currentStyle.width, 10) - 2;

    // Estes campos tem dd/mm/yyyy hh:mm:ss
    txtdtInicio.maxLength = 19;

    // Estes campos tem dd/mm/yyyy hh:mm:ss
    txtdtFim.maxLength = 19;
}

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{
    ;
}

function fg_DblClick_Prg()
{
	var empresa = '';
	var RETID = '';

    // Detalhar Banco
    if ( (keepCurrFolder() == 20058) && (fg.Rows > 1) ) 
		detalharConta(1);
	else if ( (keepCurrFolder() == 20067) && (fg.Rows > 1) )
	{
		empresa = getCurrEmpresaData();
		nRETID = getCellValueByColKey(fg, 'RETID', fg.Row);
		sendJSCarrier(getHtmlId(), 'SHOWRET', new Array(empresa[0], nRETID));       
		return null;
	}		
}

function fg_ChangeEdit_Prg()
{

}
function fg_ValidateEdit_Prg() 
{
    
}

function js_PesRec_AfterEdit(Row, Col) 
{
    if (fg.Editable) 
    {
        if (keepCurrFolder() == 20058)
        {
            if (Col == getColIndexByColKey(fg, 'RelPesContaID')) 
            {
                refillCmbCarregaLayout(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesContaID')));
            }
        }
        else if (keepCurrFolder() == 28083) {
            var dtInicio = trimStr(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtInicio')));
            var dtFim = trimStr(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtFim')));

            if ((Col == getColIndexByColKey(fg, 'ValorApolice')) ||
                (Col == getColIndexByColKey(fg, 'Premio')) ||
                (Col == getColIndexByColKey(fg, 'ValorAssegurado')) ||
                (Col == getColIndexByColKey(fg, 'Franquia')) ||
                (Col == getColIndexByColKey(fg, 'ValorCreditoAutomatico'))){
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
            }
            else if (((Col == getColIndexByColKey(fg, 'dtInicio')) || (Col == getColIndexByColKey(fg, 'dtFim'))) &&
                (chkDataEx(dtInicio) && chkDataEx(dtFim))) {

                if (putDateInYYYYMMDD(dtInicio) > putDateInYYYYMMDD(dtFim)) {
                    if (window.top.overflyGen.Alert('A data in�cio n�o pode ser maior que a data fim.') == 0)
                        return null;

                    fg.TextMatrix(Row, Col) = '';
                }
            }
            else if (Col == getColIndexByColKey(fg, 'SeguradoraID')) {
                if ((fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'SeguradoraID')) == 0)) {
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SeguradoraID')) = '';
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtInicio')) = '';
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtFim')) = '';
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorApolice')) = '';
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Premio')) = '';
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorAssegurado')) = '';
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PrazoCarencia')) = '';
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Franquia')) = '';
                }
            }
        }
    }
}

// Carrega combo na inclus�o
function refillCmbCarregaLayout(RelPesContaID) 
{
    var nSujeitoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'SujeitoID' + '\'' + '].value');
    setConnection(dsoCmbLayout);
    dsoCmbLayout.SQL = 'SELECT DISTINCT f.Layout AS TipoID, f.Layout AS Tipo ' +
		                            'FROM RelacoesPesRec a WITH(NOLOCK) ' +
			                            'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (a.SujeitoID = b.SujeitoID) ' +
			                            'INNER JOIN RelacoesPessoas_Contas c WITH(NOLOCK) ON (b.RelacaoID = c.RelacaoID) ' +
			                            'INNER JOIN Pessoas d WITH(NOLOCK) ON (b.ObjetoID = d.PessoaID) ' +
			                            'INNER JOIN Bancos e WITH(NOLOCK) ON (d.BancoID = e.BancoID) ' +
			                            'INNER JOIN Bancos_Layout f WITH(NOLOCK) ON (e.BancoID = f.BancoID) ' +
                                    'WHERE a.RelacaoID = ' + nSujeitoID + ' AND c.RelPesContaID = ' + RelPesContaID + ' ' + 
                                        'AND b.TipoRelacaoID = 24 AND b.EstadoID = 2 AND ' + 
                                        'd.EstadoID = 2 AND e.EstadoID = 2';

    dsoCmbLayout.ondatasetcomplete = refillCmbCarregaLayout_DSC;
    dsoCmbLayout.refresh();
}

function refillCmbCarregaLayout_DSC() 
{
    insertcomboData(fg, getColIndexByColKey(fg, 'Layout'), dsoCmbLayout, 'TipoID', 'Tipo');
}

function fg_BeforeRowColChange_Prg()
{

}

function fg_EnterCell_Prg()
{

}

function fg_MouseUp_Prg()
{

}

function fg_MouseDown_Prg()
{

}

function fg_BeforeEdit_Prg()
{

}

// FINAL DE EVENTOS DO GRID FG **************************************

/********************************************************************
Funcao disparada pelo frame work.
Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
    // Este form nao tem este caso
}

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
 
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario

    if ( cmbID == 'selEducacaoID' )
		adjustLabelsCombos(false);
     
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // antes de trocar o div

	// Mover esta funcao para os finais retornos de operacoes no
	// banco de dados
	finalOfInfCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    tipsBtnsEspecControlBar('inf', ['','','','']);

    // Bancos
    if (pastaID == 20058)
    {
        showBtnsEspecControlBar('inf', true, [1,1,1,0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar Banco','Detalhar Ag�ncia','Detalhar Conta','']);
    }
    // Perfis de usuario
    else if (pastaID == 20053)
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Valores dos Atributos','','','']);
    }
    // Treinamentos
    else if (pastaID == 20067)
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Detalhar RET','Avalia��o Evento','','']);
    }
    // Planos Saude
    else if (pastaID == 20070)
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Preencher seguradora','','','']);
    }
    // Despesas
    else if (pastaID == 20071)
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Observa��es','','','']);
    }
    // Classificacoes
    else if (pastaID == 20074)
    {
        showBtnsEspecControlBar('inf', true, [1,0,0,0]);
        tipsBtnsEspecControlBar('inf', ['Valores das faixas','','','']);
    }

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
	    
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{
    //@@
    //@@
    
    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC')) {
        // O contexto
        var aContextoID = getCmbCurrDataInControlBar('sup', 1);
        var nContextoID = aContextoID[1];

        // Uso Fisico
        if (nContextoID == 1131)
            setupEspecBtnsControlBar('sup', 'HDDHHHDH');
        else
            setupEspecBtnsControlBar('sup', 'HDDHHDDD');
    }
    else
        setupEspecBtnsControlBar('sup', 'DDDDDDDD');

    // Bancos
    if (folderID == 20058)
    {
        if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HHHD');
    }
    // Perfis de Usuario
    else if (folderID == 20053)
	{
		if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
	}
	// Educacao
    else if (folderID == 20064)
	{
        adjustLabelsCombos(true);
	}
	// Treinamento
	else if (folderID == 20067)
	{
		if (fg.Rows > 1)
            setupEspecBtnsControlBar('inf', 'HDDD');
	}
    // Incidencia de Vendas
	else if (folderID == 20072)
		setupEspecBtnsControlBar('inf', 'DDDD');
	// Plano Saude
	else if (folderID == 20070)
		setupEspecBtnsControlBar('inf', 'DDDD');
	// Despesas
	else if (folderID == 20071)
	{
        if (fg.Rows > 1)
		    setupEspecBtnsControlBar('inf', 'HDDD');
    }
    // Classificacoes
	else if (folderID == 20074)
	{
	    if (fg.Rows > 1)
		    setupEspecBtnsControlBar('inf', 'HDDD');		
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{
    // Pasta de documentos
    if ( folderID == 20053 )
    {
        // Merge de Colunas
        fg.MergeCells = 0;
    }
    // Habilidades
    else if (folderID == 20066)
    {
        // Grava o id do colaborador
        fg.TextMatrix(keepCurrLineInGrid(), 6) = getCurrUserID();
    }
	// Plano saude
	else if (folderID == 20070)
	{
		setupEspecBtnsControlBar('inf', 'HDDD');
	}
	// Despesas
	else if (folderID == 20071)
	{
		setupEspecBtnsControlBar('inf', 'HDDD');
	}
	// Classificacoes
	else if (folderID == 20074)
	{
		setupEspecBtnsControlBar('inf', 'DDDD');
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
    
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    var nRETID = 0;
	var empresa = '';

	
	
    // Detalhar Conta
    if ( (controlBar == 'INF') && (keepCurrFolder() == 20058) )
    {
		detalharConta(btnClicked);
    }    
    // Valores de atributos
    else if ( (controlBar == 'INF') && (keepCurrFolder() == 20053) )
    {
		valoresDeAtributos();
    }    
	// Trienamentos
    else if ( (controlBar == 'INF') && (keepCurrFolder() == 20067) )
    {
		empresa = getCurrEmpresaData();
		nRETID = getCellValueByColKey(fg, 'RETID', fg.Row);
		sendJSCarrier(getHtmlId(), 'SHOWRET', new Array(empresa[0], nRETID));       
		return null;
    }
    // Planos de saude
    else if ( (controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20070) )
    {
		var strPars = '';

        // carregar modal - nao faz operacao de banco no carregamento
        htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaserecursos/modalpages/modalseguradora.asp' + strPars;
        showModalWin(htmlPath, new Array(490,284));
    }
    // Despesas
    else if ( (controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20071) )
    {
		var nRelPesRecDespesaID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesRecDespesaID'));
		var strPars = '?nRelPesRecDespesaID=' + nRelPesRecDespesaID;

        // carregar modal - Observa��es
        htmlPath = SYS_PAGESURLROOT + '/basico/relacoes/relpessoaserecursos/modalpages/modalobsdespesas.asp' + strPars;
        showModalWin(htmlPath, new Array(740,463));
    }
    // Classificacoes
    else if ( (controlBar == 'INF') && (btnClicked == 1) && (keepCurrFolder() == 20074) )
    {
		openModalClassificacoesFaixas();
    }    
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
	var nDuracao = 0;
	var nConcluido = 0;
	var sErrorMsg = '';
	var elemToFocus = null;

	if (controlBar == 'INFALT') 
	{
	    refillCmbCarregaLayout(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesContaID')));
	}
	
	// Educacao
	if ((keepCurrFolder() == 20064) && (btnClicked == 'INFOK'))
	{
		txtDuracao.value = trimStr(txtDuracao.value);
		if (txtDuracao.value != '')
		{
			nDuracao = parseFloat(txtDuracao.value);
			
			if (nDuracao < 1.5)
			{
				sErrorMsg = 'Dura��o do curso � inv�lida.';
				elemToFocus = txtDuracao;
			}	
			else
			{
				if (txtConcluido.value != '')
				{
					nConcluido = parseFloat(txtConcluido.value);
					
					if ((nConcluido < 0 ) || (nConcluido > nDuracao ))
					{
						sErrorMsg = 'Anos concluidos do curso � inv�lido.';
						elemToFocus = txtConcluido;
					}	
				}	
				else
				{
					sErrorMsg = 'Anos concluidos do curso � inv�lido.';
					elemToFocus = txtConcluido;
				}
			}

			if ((nDuracao == nConcluido) && (sErrorMsg == ''))
				chkCursando.checked = false;
				
			if (sErrorMsg != '')
			{
				if ( window.top.overflyGen.Alert(sErrorMsg) == 0 )
				    return null;
				    
				window.focus();    
				elemToFocus.focus();
				
				return false;
			}
		}
		else
		{
			txtConcluido.value = '';
			chkCursando.checked = false;
		}
    }
    else if (keepCurrFolder() == 28083) {
        if (btnClicked == 'INFEXCL') {
            if (fg.Rows == 2) {
                if (window.top.overflyGen.Alert('N�o � permitido deletar todas as linhas de cr�dito') == 0)
                    return null;
                
                window.focus();
                return false;
            }
        }
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
	if (idElement.toUpperCase() == 'MODALVALORESATRIBUTOSHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
	else if (idElement.toUpperCase() == 'MODALSEGURADORAHTML')
    {
        if ( param1 == 'OK' )                
        {
            // Escreve ID, Fantasia no grid de contatos
            writeSeguradoraInGrid(param2);
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALCLASSIFICACOESFAIXASHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOBSDESPESASHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

function writeSeguradoraInGrid(aSeguradora)
{
    // o array aContato contem: PessoaID, Fantasia
    
    fg.Redraw = 0;
    // Escreve nos campos
    // Fantasia
    fg.TextMatrix(keepCurrLineInGrid(),getColIndexByColKey(fg, 'PrestadorID')) = aSeguradora[1];
    // ID
    fg.TextMatrix(keepCurrLineInGrid(),getColIndexByColKey(fg, '^RelPesRecBeneficioID^dso01GridLkp^RelPesRecBeneficioID^Fantasia*')) = aSeguradora[0];
    
    fg.Redraw = 2;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{
    if (folderID == 20053)
    {
        // Merge de Colunas
        fg.MergeCells = 0;
	}        
    // Habilidades
    else if ( (folderID == 20066)  && (fg.Rows > 1) )
    {
        fg.TextMatrix(keepCurrLineInGrid(), 6) = getCurrUserID();
    }
	// Plano saude
	else if (folderID == 20070)
	{
		setupEspecBtnsControlBar('inf', 'HDDD');
	}
	// Despesas
	else if (folderID == 20071)
	{
		setupEspecBtnsControlBar('inf', 'HDDD');
	}
	else if (folderID == 20074)
	{
	    if (fg.Rows > 0)
		    setupEspecBtnsControlBar('inf', 'HDDD');
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados
particulares do form ou deixa o frame work preencher com o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{

    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
O frame work esta prestes a setar os botoes da barra infeior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
    // Pasta de LOG ou Treinamentos - Read Only
    if ((folderID == 20010) || (folderID == 20067))
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Criado pelo programador
Detalha: 1-Banco, 2-Agencia, 3-Conta

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function detalharConta(btnClicked)
{
    var empresa = '';
	var nRegistroID = null;

    if (fg.Rows > 1)
    {
        if (!((dso01GridLkp.recordset.BOF)&&(dso01GridLkp.recordset.EOF)))
            dso01GridLkp.recordset.MoveFirst();

		while ( !dso01GridLkp.recordset.EOF )
		{
			if (dso01GridLkp.recordset['RelPesContaID'].value == fg.ValueMatrix(fg.Row, 2))						
			{
				empresa = getCurrEmpresaData();
				if (btnClicked == 1)
				{
					nRegistroID = dso01GridLkp.recordset['BancoID'].value;
					sendJSCarrier(getHtmlId(), 'SHOWBANCO', new Array(empresa[0], nRegistroID));       
					return null;
				}
				else if (btnClicked == 2)					
				{
					nRegistroID = dso01GridLkp.recordset['AgenciaID'].value;
					sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(empresa[0], nRegistroID));       
					return null;
				}
				else if (btnClicked == 3)					
				{
					nRegistroID = dso01GridLkp.recordset['ContaID'].value;
					sendJSCarrier(getHtmlId(), 'SHOWRELPESSOAS', new Array(empresa[0], nRegistroID));       
					return null;
				}
			}
		    dso01GridLkp.recordset.moveNext();
		}
    }
}

function valoresDeAtributos()
{
	var strPars = new String();
	
	var nRelacaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset[' + '\'' + 'RelacaoID' + '\'' + '].value');
	
    strPars = '?nRelacaoID=' + escape(nRelacaoID);
    strPars += '&nFormID=' + escape(window.top.formID);

    // carregar modal - nao faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modalgen/modalvaloresatributos.asp'+strPars;
    
    showModalWin(htmlPath, new Array(680,468));
}

function openModalClassificacoesFaixas()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 244;
    var nHeight = 200;		    
    var sRelPesRecClassificacaoID = fg.TextMatrix(fg.Row,getColIndexByColKey(fg, 'RelPesRecClassificacaoID'));
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)    
    strPars = '?sCaller=' + escape('I');
    strPars += '&sRelPesRecClassificacaoID=' + escape(sRelPesRecClassificacaoID);
    
    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/relacoes/relpessoaserecursos/modalpages/modalclassificacoesfaixas.asp' + strPars;
    
    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

function adjustLabelsCombos(bPaging)
{
    if (bPaging)
        setLabelOfControl(lblEducacaoID, dso01JoinSup.recordset['EducacaoID'].value);
    else
        setLabelOfControl(lblEducacaoID, selEducacaoID.value);
}
