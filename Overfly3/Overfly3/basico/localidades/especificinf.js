/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de localidades
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
        
    //@@
    if ( (folderID == 20008) || (folderID == 20009)  ) // Prop/Altern
    {
        dso.SQL = 'SELECT a.* FROM Localidades a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.LocalidadeID = '+idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20246) // Localidades Filhas
    {
        //@@ As variaveis a seguir sao da automacao 
        //e devem ser definidas pelo programador
        glb_currGridTable = 'Localidades';
        glb_aCmbKeyPesq = [['ID','NaLocalidadeID'],['Localidade','VaLocalidade']];
        // Final da automacao =========================================================
        
        // paginacao de grid
        return execPaging(dso, 'LocalizacaoID', idToFind, folderID);
    }
    else if (folderID == 20247) // Idiomas
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Localidades_Idiomas WITH(NOLOCK)  ' +
                  'WHERE LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY Ordem';
        return 'dso01Grid_DSC';           
    }
    else if (folderID == 20248) // Moedas
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Localidades_Moedas WITH(NOLOCK) ' +
                  'WHERE LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY Ordem';
        return 'dso01Grid_DSC';           
    }
    else if (folderID == 20249) // CEP
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Localidades_CEPs WITH(NOLOCK) ' +
                  'WHERE LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY Ordem';
        return 'dso01Grid_DSC';           
    }
    else if (folderID == 20250) // DDD/DDI
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Localidades_DDDs WITH(NOLOCK) ' +
                  'WHERE LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY Ordem';
        return 'dso01Grid_DSC';           
    }
    else if (folderID == 20251) // Incidencias
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Localidades_Incidencias a WITH(NOLOCK) ' +
                  'WHERE a.LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY (SELECT Verba FROM VerbasFopag WITH(NOLOCK) WHERE VerbaID=a.VerbaID), ValorMinimo';
        return 'dso01Grid_DSC';           
    }
    else if (folderID == 20252) // Documentos
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Localidades_Documentos a WITH(NOLOCK) ' +
                  'WHERE a.LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY a.TipoDocumentoID, a.NomeDocumento';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 20253) // Taxas
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Localidades_Taxas a WITH(NOLOCK) ' +
                  'WHERE a.LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY a.dtInicio, a.dtFim';
        return 'dso01Grid_DSC';
    }
    
    else if (folderID == 21029) // Beneficios Fiscais
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Localidades_Beneficios a WITH(NOLOCK) ' +
                  'WHERE a.LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY a.TipoID';
        return 'dso01Grid_DSC';
    }

    else if (folderID == 20254) // Simples Nacional
    {
        dso.SQL = 'SELECT a.* ' +
                  'FROM Localidades_SimplesNacional a WITH(NOLOCK) ' +
                  'WHERE a.LocalidadeID = ' + idToFind + ' ' +
                  'ORDER BY ImpostoID, ValorMinimo, ValorMaximo';
        return 'dso01Grid_DSC';
    }
    
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    var nEmpresaData = getCurrEmpresaData();
    var nMoedaEmpresaSistema = nEmpresaData[9];
    
    var aContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nContextoID = aContextoID[1];
    
    // LOG
    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Idiomas/IdiomaID
    else if (pastaID == 20247) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID=48 ' +
                      'ORDER BY Ordem';
        }
    }
    // Moedas/MoedaID
    else if (pastaID == 20248) {
        if (dso.id == 'dsoCmb01Grid_01') {
            if (nContextoID = 1333) //Estados
            {
                dso.SQL = 'SELECT ConceitoID,ConceitoAbreviado,Conceito ' +
                          'FROM Conceitos WITH(NOLOCK) ' +
                          'WHERE EstadoID = 2 AND TipoConceitoID = 308 AND Filtro like ' + '\'' + '%(' + nContextoID + ')%' + '\'' + ' ' +
                          'ORDER BY ConceitoAbreviado';
            }
            else //Paises
            {
                dso.SQL = 'SELECT ConceitoID,ConceitoAbreviado,Conceito ' +
                          'FROM Conceitos WITH(NOLOCK) ' +
                          'WHERE EstadoID = 2 AND TipoConceitoID = 308 ' +
                          'ORDER BY ConceitoAbreviado';
            }
        }
    }
    // Incidencias
    else if (pastaID == 20251) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT VerbaID, Verba FROM VerbasFopag WITH(NOLOCK) ' +
					  'WHERE EstadoID=2 AND EhAutomatica=1 ' +
					  'ORDER BY Verba';
        }
    }
    // Documentos
    else if (pastaID == 20252) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ItemID, ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
					  'WHERE EstadoID=2 AND TipoID=17 ' +
					  'ORDER BY ItemID';
        }
    }

    //Taxas
    else if (pastaID == 20253) {
        if (dso.id == 'dsoCmb01Grid_01') 
        {
            dso.SQL = 'SELECT ConceitoID, SimboloMoeda ' +
                      'FROM Conceitos WITH(NOLOCK) ' +
                      'WHERE EstadoID = 2 AND TipoConceitoID = 308 AND ConceitoID = ' + nMoedaEmpresaSistema +
                      'UNION ' +
                      'SELECT a.ConceitoID, a.SimboloMoeda ' +
                      'FROM Conceitos A WITH(NOLOCK) ' +
                      'INNER JOIN Localidades_Moedas B WITH(NOLOCK) ON A.ConceitoID = B.MoedaID ' +
                      'WHERE EstadoID = 2 AND TipoConceitoID = 308 AND B.LocalidadeID = ' + nRegistroID + 
                      ' ORDER BY SimboloMoeda';
        }
    }


    //Beneficios Fiscais
    else if (pastaID == 21029) {
        if (dso.id == 'dsoCmb01Grid_01') {
            if (nContextoID == 1334)
                dso.SQL = 'SELECT 1 AS TipoID, ' + '\'' + 'Integrante' + '\'' + ' AS Tipo ';
            else if (nContextoID == 1332 || nContextoID == 1333)
                dso.SQL = 'SELECT 2 AS TipoID, ' + '\'' + 'Reconhece' + '\'' + ' AS Tipo ';

        }

        if (dso.id == 'dsoCmb01Grid_02') {
            dso.SQL = 'SELECT ItemID, ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
					  'WHERE EstadoID=2 AND TipoID=113 AND Filtro like ' + '\'' + '%<' + nContextoID + '>%' + '\'' + ' ' +
					  'ORDER BY ItemID';

        }
    }

    // Simples Nacional
    else if (pastaID == 20254) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT ConceitoID, Imposto FROM Conceitos WITH(NOLOCK) ' +
					  'WHERE EstadoID=2 AND TipoConceitoID=306 ' +
					  'ORDER BY Ordem';
        }
    }
         
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
    var aFolders = [[20008,'true'],  /* Observacoes   */
                    [20009,'true'],  /* Proprietarios */
                    [20010,'true'],  /* LOG */
                    [20246,'nTipoRegistroID <= 204'],  /* Localidades Filhas */
                    [20247,'nTipoRegistroID == 203'],  /* Idiomas */
                    [20248, '(nTipoRegistroID == 203) || (nTipoRegistroID == 204)'],  /* Moedas  */
                    [20252,'(nTipoRegistroID == 203)'], /* Documentos */
                    [20249,'nTipoRegistroID == 205'],  /* CEP     */
                    [20250,'(nTipoRegistroID == 203) || (nTipoRegistroID == 205)'], /* DDD/DDI     */
                    [20251, '(nTipoRegistroID == 203)'],
                    [21029, '(nTipoRegistroID == 203) || (nTipoRegistroID == 204) || (nTipoRegistroID == 205)'],
                    [20253, '(nTipoRegistroID == 203) || (nTipoRegistroID == 204) || (nTipoRegistroID == 205)'], /* Taxas */
                    [20254, '(nTipoRegistroID == 203)']]/*Simples Nacional*/; 

	cleanupSelInControlBar('inf',1);

    for (i=0; i<aFolders.length; i++)
    {
        if (eval(aFolders[i][1]))
        {
            vPastaName = window.top.getPastaName(aFolders[i][0]);
            if (vPastaName != '')
            	addOptionToSelInControlBar('inf', 1, vPastaName, aFolders[i][0]);
        }
    }

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;

    var currLine = 0;
    var thisTime = DateAndTimeNow();

    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 20247) // Idiomas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['LocalidadeID', registroID]);
        else if (folderID == 20248) // Moedas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['LocalidadeID', registroID]);
        else if (folderID == 20249) // CEP
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1, 2], ['LocalidadeID', registroID]);
        else if (folderID == 20250) // DDD/DDI
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [1], ['LocalidadeID', registroID]);
        else if (folderID == 20251) // Incidencias
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2], ['LocalidadeID', registroID]);
        else if (folderID == 20252) // Documentos
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2, 4, 5], ['LocalidadeID', registroID]);
        else if (folderID == 20253) // Taxas
        {
            var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
            var readonly = "*";

            if (nA2 == 1)
                readonly = "";

            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['LocalidadeID', registroID]);
        }
        else if (folderID == 21029) // Beneficios Fiscais
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1], ['LocalidadeID', registroID]);

        else if (folderID == 20254) // Simples Nacional
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['LocalidadeID', registroID]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

		// alinha todas as colunas a esquerda
		alignColsInGrid(fg);
    }
    else if (folderID == 20247) // Idiomas
    {
        headerGrid(fg,['Ordem','Idioma','Oficial','LocIdiomaID'], [3]);
        fillGridMask(fg,currDSO,['Ordem','IdiomaID','Oficial','LocIdiomaID']
                               ,['','99','','']);

		// alinha todas as colunas a esquerda
		alignColsInGrid(fg);
    }    
    else if (folderID == 20246) // Localidades Filhas
    {
        headerGrid(fg,['ID','Est','Localidade','Tipo'],[]);
        fillGridMask(fg,currDSO,['LocalidadeID','RecursoAbreviado','Localidade','ItemMasculino'],
                                    ['','','','']);
		// alinha todas as colunas a esquerda
		alignColsInGrid(fg);
    }
    else if (folderID == 20248)  // Moedas
    {
        headerGrid(fg,['Ordem','Moeda','Oficial','LocMoedaID'], [3]);
        fillGridMask(fg,currDSO,['Ordem','MoedaID','Oficial','LocMoedaID']
                                     ,['','99','','']);

		// alinha todas as colunas a esquerda
		alignColsInGrid(fg);
    }    
    else if  (folderID == 20249)  // CEP
    {
        headerGrid(fg,['Ordem',
                       'CEP Inicial',
                       'CEP Final','LocCEPID'],[3]);
        fillGridMask(fg,currDSO,['Ordem','CEPInicial','CEPFinal','LocCEPID']
                                     ,['','','','']);
		// alinha todas as colunas a esquerda
		alignColsInGrid(fg);
    }
    else if  (folderID == 20250)  // DDD/DDI
    {
        headerGrid(fg,['Ordem',
                       'DDD/DDI',
                       'Observa��o','LocDDDID'],[3]);
        fillGridMask(fg,currDSO,['Ordem','DDDI','Observacao','LocDDDID']
                                     ,['','','','']);

		// alinha todas as colunas a esquerda
		alignColsInGrid(fg);
    }
    else if  (folderID == 20251)  // Incidencias
    {
        headerGrid(fg,['Verba',
                       'Valor M�nimo',
                       'Valor M�ximo',
                       'Al�quota',
                       'Dedu��o',
                       'Dependente',
                       'Observa��o',
                       'LocIncidenciaID'],[7]);

        fillGridMask(fg,currDSO,['VerbaID',
								 'ValorMinimo',
								 'ValorMaximo',
								 'Percentual',
								 'ValorDeducao',
								 'ValorDependente',
								 'Observacao',
								 'LocIncidenciaID'],
                                 ['','999999999.99','999999999.99','999.99','999999999.99','999999999.99','',''],
                                 ['','###,###,##0.00','###,###,##0.00','##0.00','###,###,##0.00','###,###,##0.00','','']);
		
		alignColsInGrid(fg,[1,2,3,4,5]);
    }
    else if  (folderID == 20252)  // Documentos
    {
        headerGrid(fg,['Tipo de documento',
                       'Sigla',
                       'Nome do documento',
                       'M�scara',
                       'M�n',
                       'M�x',
                       'SN',
                       'LocDocumentoID'],[7]);

        glb_aCelHint = [[0,4,'N�mero m�nimo de caracteres'],
                        [0,5,'N�mero m�ximo de caracteres'],
                        [0,6,'S� n�meros?']];

        fillGridMask(fg,currDSO,['TipoDocumentoID',
								 'SiglaDocumento',
								 'NomeDocumento',
								 'Mascara',
								 'DigitosMinimo',
								 'DigitosMaximo',
								 'SoNumeros',
								 'LocDocumentoID'],
                                 ['','','','','99','99','',''],
                                 ['','','','','##','##','','']);
		
		alignColsInGrid(fg,[4,5]);
    }

    else if (folderID == 21029)  // Beneficios Fiscais
    {
        headerGrid(fg, ['Tipo',
                           'Benef�cio',
                           'Observa��o',
                           'LocBeneficioID'], [3]);

        fillGridMask(fg, currDSO, ['TipoID',
                                       'BeneficioID',
                                       'Observacao',
                                       'LocBeneficioID'],
                                         ['', '', '', ''],
                                         ['', '', '', '']);
    }

    else if (folderID == 20253) // Taxas
    {
        //caso o usuario tenha acesso de permiss�o a2 ele pode informar a data
        var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
        var readonly = "*";
        
        if (nA2 == 1)
            readonly = "";

        headerGrid(fg, ['C�digo', 'Taxa', 'In�cio', 'Fim', 'Moeda', 'Valor', 'Observa��o', 'LocalidadeID', 'LocTaxaID'], [7, 8]);
        fillGridMask(fg, currDSO, ['Codigo', 'Taxa', 'dtInicio' + readonly, 'dtFim' + readonly, 'MoedaID', 'Valor', 'Observacao', 'LocalidadeID', 'LocTaxaID'],
                                  ['', '', '99/99/9999', '99/99/9999', '', '999999999.99', '', '',''],
                                  ['', '', dTFormat, dTFormat, '', '###,###,##0.00', '', '','']);

        // alinha todas as colunas a esquerda
        alignColsInGrid(fg, [5]);
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
    }

    else if (folderID == 20254) // Simples Nacional
    {
        headerGrid(fg, ['Imposto',
                        'Vig�ncia',
                        'Valor M�nimo',
                        'Valor M�ximo',
                        'Al�quota',
                        'Observa��o',
                        'LocSimplesNacionalID'], [6]);

        fillGridMask(fg, currDSO, ['ImpostoID',
                                   'dtVigencia',
                                   'ValorMinimo',
                                   'ValorMaximo',
                                   'Aliquota',
                                   'Observacao',
                                   'LocSimplesNacionalID'],
                                 ['', '99/99/9999', '999999999.99', '999999999.99', '999.99', '', ''],
                                 ['', dTFormat, '###,###,##0.00', '###,###,##0.00', '##0.00', '', '']);


        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;

        alignColsInGrid(fg, [2, 3, 4]);
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
    }    
    
}
