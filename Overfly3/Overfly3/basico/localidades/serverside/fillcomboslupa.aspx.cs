using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.basico.localidades.serverside
{
    public partial class fillcomboslupa : System.Web.UI.OverflyPage
    {
        private string comboID;
        private Integer param1;
        private Integer param2;
        private string text;

        private static string emptyStr = "";
        private static Integer Zero = new Integer(0);

        protected string sComboID
        {
            set { comboID = (value != null) ? value : emptyStr; }   
        }

        protected Integer sParam1
        {
            set { param1 = (value != null) ? value : Zero; }
        }

        protected Integer sParam2
        {
            set { param2 = (value != null) ? value : Zero; }
        }        

        protected string sText
        {
            set
            { text = (value != null) ? value : emptyStr; }            
        }

        protected override void PageLoad(object sender, EventArgs e)            
        {
            string strSQL = "";

            if (comboID == "selLocalidadeMaeID")
            {                
                strSQL = "SELECT 0 AS fldID,' ' AS fldName " +
                         "UNION ALL SELECT DISTINCT TOP 100 a.LocalidadeID AS fldID," +
                         "a.Localidade + ' ' + b.CodigoLocalidade2 AS fldName " +
                         "FROM Localidades a WITH(NOLOCK),Localidades b WITH(NOLOCK) " +
                         "WHERE a.EstadoID=2 AND a.LocalizacaoID=b.LocalidadeID AND " +
                         "a.TipoLocalidadeID= " + param1 + " " +
                         "AND a.Localidade >= " + text +
                         "ORDER BY fldName";
            }
            // Executa o pacote
            int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(strSQL);

            // Gera o resultado para o usuario.
            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "SELECT " + rowsAffected + " as Resultado"
                )
            );
        }
    }
}
