﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;


namespace Overfly3.basico.localidades.serverside
{
    public partial class verificacaolocalidade : System.Web.UI.OverflyPage
    {
        private static Integer zero = new Integer(0);
        private Integer localidadeID = zero;
        private Integer currEstadoID = zero;
        private Integer newEstadoID = zero;

        // Variaveis de saida.
        private string response;
        private string mensagem;

        protected Integer nLocalidadeID
        {
            set { localidadeID = value != null ? value : zero; }
        }

        protected Integer nCurrEstadoID
        {
            set { currEstadoID = value != null ? value : zero; }
        }

        protected Integer nNewEstadoID
        {
            set { newEstadoID = value != null ? value : zero; }
        }


        protected void LocalidadeVerifica()
        {
            ProcedureParameters[] procparam = new ProcedureParameters[5];

            procparam[0] = new ProcedureParameters(
                "@LocalidadeID",
                SqlDbType.Int,
                localidadeID.ToString());

            procparam[1] = new ProcedureParameters(
                "@EstadoDeID",
                SqlDbType.Int,
                currEstadoID.ToString());

            procparam[2] = new ProcedureParameters(
               "@EstadoParaID",
               SqlDbType.Int,
               newEstadoID.ToString());

            procparam[3] = new ProcedureParameters(
                "@Resultado",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procparam[3].Length = 1;

            procparam[4] = new ProcedureParameters(
                "@Mensagem",
                SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procparam[4].Length = 8000;

            // Executa a procedure.
            DataInterfaceObj.execNonQueryProcedure(
                "sp_Localidade_Verifica", procparam
            );

            // Obtém o resultado da execução.
            response = procparam[3].Data.ToString();
            mensagem = procparam[4].Data.ToString();
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            LocalidadeVerifica();
            //updatePedidos();

            WriteResultXML(
                DataInterfaceObj.getRemoteData(
                    "select '" + response + "' as Resultado, '" + mensagem + "' as Mensagem"
                )
            );
        }
    }

}
