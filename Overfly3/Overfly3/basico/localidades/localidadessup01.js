/********************************************************************
localidadessup01.js

Library javascript para o localidadessup01.asp

ultima revisao:  06/03/2001 - 12:00
********************************************************************/

// As variaveis globais abaixo sao de uso exclusivo desta library
// VARIAVEIS GLOBAIS ************************************************

// controla o retorno do servidor dos dados de combos dinamicos
// (selSujeitoID e selObjetoID)
var glb_CounterCmbsDynamics = 0;
// guarda o botao clicado enviado pelo frameword
var glb_BtnFromFramWork;
// guarda id do combo de lupa a preencher
var glb_cmbLupaID;

// Guarda o tipo do registro selecionado para ser usado no select
// que traz o registro devolta na gravacao, pois este form tem
// Identity calculado por trigger
var glb_LocalidadesTipoRegID = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

var dsoSup01 = new CDatatransport('dsoSup01');
var dsoStateMachine = new CDatatransport('dsoStateMachine');
var dsoCmbDynamic01 = new CDatatransport('dsoCmbDynamic01');
var dsoCmbDynamic02 = new CDatatransport('dsoCmbDynamic02');
var dsoCmbsLupa = new CDatatransport('dsoCmbsLupa');
var dsoEstaticCmbs = new CDatatransport('dsoEstaticCmbs');
//  Verificacao do Localidade .URL 
var dsoVerificacao = new CDatatransport("dsoVerificacao");
/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()    
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)    

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selTipoRegistroID', '1'],
                          ['selTipoRegiaoID', '2'],
                          ['selTipoReligiaoID', '3'],
                          ['selSistemaGovernoID', '5'],
                          ['selFormaGovernoID', '6']]);
    
    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/basico/localidades/localidadesinf01.asp',
                              SYS_PAGESURLROOT + '/basico/localidades/localidadespesqlist.asp');                          
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01',  ['divSup02_01'],
                                        [[203,204,205]] );

    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'LocalidadeID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoLocalidadeID';
    
}

function setupPage()
{
    
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01'],
                      [2,'divSup02_01']]);
    
    //@@ *** Div principal superior divSup021_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblLocalidade','txtLocalidade',30,1],
                          ['lblTipoRegistroID','selTipoRegistroID',11,1],
                          ['lblCodigoLocalidade2','txtCodigoLocalidade2',4,1],
                          ['lblCodigoLocalidade','txtCodigoLocalidade',8,1],
                          ['lblCapital','chkCapital',3,1],
                          ['lblMetropole','chkMetropole',3,1],
                          ['lblFluvial','chkFluvial',3,1]]);

     //@@ *** Sistema - Div secundario superior divSup02_01 ***/
     adjustElementsInForm([['lblLocalizacaoID','selLocalizacaoID',30,2],
                           ['lblTipoRegiaoID','selTipoRegiaoID',20,2,-3],
                           ['lblLocalidadeMaeID','selLocalidadeMaeID',30,2,-3],
                           ['btnFindLocMae','btn',24,2,-3],
                           ['lbldtFundacao','txtdtFundacao',10,2,-3],
                           ['lblNaturalidade','txtNaturalidade',20,3],
                           ['lblArea','txtArea',11,3,-2],
                           ['lblPopulacao','txtPopulacao',11,3,-2],
                           ['lblPercentualMasculino','txtPercentualMasculino',6,3,-2],
                           ['lblPIB','txtPIB',14,3,-5],
                           ['lblTipoReligiaoID','selTipoReligiaoID',17,3,-2],
                           ['lblPercentualReligiao','txtPercentualReligiao',6,3,-2],
                           ['lblFuso', 'txtFuso', 6, 4],
                           ['lblRetemImposto', 'chkRetemImposto', 3, 4],
                           ['lblNomeOficial','txtNomeOficial',40,4],
                           ['lblNomeInternacional','txtNomeInternacional',25,4],
                           ['lblCodigoLocalidade3','txtCodigoLocalidade3',4,4],
                           ['lblCriticaDocumento','txtCriticaDocumento',3,4],
                           ['lblIDH','txtIDH',5,4],
                           ['lblEnderecoInvertido','chkEnderecoInvertido',3,4],
                           ['lblFantasiaInvertido','chkFantasiaInvertido',3,4,-5],
                           ['lblSistemaGovernoID','selSistemaGovernoID',14,5,0,-5],
                           ['lblFormaGovernoID','selFormaGovernoID',15,5],
                           ['lblExcecao','chkExcecao',3,5],
                           //['lblAnoConstituicao','txtAnoConstituicao',5,5],
                           ['lblSuspensa','chkSuspensa',3,5],
                           ['lblDivisaoAdministrativa','txtDivisaoAdministrativa',40,5]]);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    // Da automacao, deve constar de todos os ifs do carrierArrived
    if ( __currFormState() != 0 )
            return null;
            
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    txtFuso.setAttribute('thePrecision', 4, 1);
    txtFuso.setAttribute('theScale', 2, 1);
    txtFuso.setAttribute('minMax', new Array(-99, 99), 1);

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    

}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selLocalizacaoID e selLocalidadeMaeID)
    glb_BtnFromFramWork = btnClicked;

    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') )
        startDynamicCmbs();
    else    
        // volta para a automacao    
	    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    clearAndLockCmbsDynamics(btnClicked);

    setComboLabel(lblTipoRegistroID, 'Tipo ', dsoSup01.recordset['TipoLocalidadeID'].value);
    setComboLabel(lblLocalizacaoID, 'Localiza��o ', dsoSup01.recordset['LocalizacaoID'].value);
    setComboLabel(lblTipoRegiaoID, 'Regi�o ', dsoSup01.recordset['TipoRegiaoID'].value);
    setComboLabel(lblLocalidadeMaeID, 'Localidade M�e ', dsoSup01.recordset['LocalidadeMaeID'].value);
    setComboLabel(lblTipoReligiaoID, 'Religi�o Predominante ', dsoSup01.recordset['TipoReligiaoID'].value);
    setComboLabel(lblSistemaGovernoID, 'Sistema Governo ', dsoSup01.recordset['SistemaGovernoID'].value);
    setComboLabel(lblFormaGovernoID, 'Forma Governo ', dsoSup01.recordset['FormaGovernoID'].value);
    
    showHideByTipoRegistro();
    
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // as operacoes foram executadas manualmente pelo usuario
    
    if (cmbID == 'selTipoRegistroID')
        setComboLabel(lblTipoRegistroID, 'Tipo ', selTipoRegistroID.value);
    else if (cmbID == 'selLocalizacaoID')
        setComboLabel(lblLocalizacaoID, 'Localiza��o ', selLocalizacaoID.value);
    else if (cmbID == 'selTipoRegiaoID')
        setComboLabel(lblTipoRegiaoID, 'Regi�o ', selTipoRegiaoID.value);
    else if (cmbID == 'selLocalidadeMaeID')
        setComboLabel(lblLocalidadeMaeID, 'Localidade M�e ', selLocalidadeMaeID.value);
    else if (cmbID == 'selTipoReligiaoID')
        setComboLabel(lblTipoReligiaoID, 'Religi�o Predominante ', selTipoReligiaoID.value);
    else if (cmbID == 'selSistemaGovernoID')
        setComboLabel(lblSistemaGovernoID, 'Sistema Governo ', selSistemaGovernoID.value);
    else if (cmbID == 'selFormaGovernoID')
        setComboLabel(lblFormaGovernoID, 'Forma Governo ', selFormaGovernoID.value);

    if ((cmbID == 'selTipoRegistroID') && ((cmb.value >= 203) && (cmb.value <= 205)) )
        fillCmbLocalizacaoID(cmb);
    else
    {
        // Troca a interface do sup em funcao de item selecionado
        // no combo de tipo.
        // Se for necessario ao programador, repetir o if abaixo
        // definido, antes deste comentario.
        // Nao mexer - Inicio de automacao ==============================
        if ( cmbID == 'selTipoRegistroID' )
            if (adjustSupInterface() == null)
                showHideByTipoRegistro(true);
        // Final de Nao mexer - Inicio de automacao =====================
    }
}


/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{
    ;
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    verifyLocalidadeInServer(currEstadoID, newEstadoID);
    //alert(currEstadoID);
    //alert(newEstadoID);
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{
    //@@
    var cmbID = '';
    
    if ( btnClicked.id == 'btnFindLocMae' )
        cmbID = 'selLocalidadeMaeID';

    // Nao mexer - Inicio de automacao ==============================
    // Invoca janela modal
    loadModalOfLupa(cmbID, null);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
        
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Incluir, alterar, estado, excluir, OK, cancelar
                  refresh, anterior e proximo. (Testar a string que vem).

Retorno:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Identity Calculado
    if ( (controlBar == 'SUP') && (btnClicked == 'SUPOK') )
        glb_LocalidadesTipoRegID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;

    if ( btnClicked == 'SUPINCL' )
    {
        setComboLabel(lblTipoRegistroID, 'Tipo ');
        setComboLabel(lblLocalizacaoID, 'Localiza��o ');
        setComboLabel(lblTipoRegiaoID, 'Regi�o ');
        setComboLabel(lblLocalidadeMaeID, 'Localidade M�e ');
        setComboLabel(lblTipoReligiaoID, 'Religi�o Predominante ');
        setComboLabel(lblSistemaGovernoID, 'Sistema Governo ');
        setComboLabel(lblFormaGovernoID, 'Forma Governo ');
    }

    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if ( idElement.toUpperCase() == 'MODALOFLUPAHTML' )
    {
        if ( param1 == 'OK' )                
        {
            // param2 = array
            // param2[0] = texto digitado
            // param2[1] = id do combo a preencher
            // param2[2] = id do form
            
            getDataAndLoadCmbsLupa(param2[0], param2[1], param2[2]);
            
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');

            // nao mexer
            return 0;
        }
    }    
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    if( getCurrTipoRegID() == 204 ) // Estado
    {
        clearComboEx(['selLocalidadeMaeID']);
        selLocalidadeMaeID.disabled = true;
        chkST.disabled = false;
        // trava o botao de lupa do Localidade Mae
        lockBtnLupa(btnFindLocMae, true);
    }
    else if( (getCurrTipoRegID() == 203)||(getCurrTipoRegID() == 205) ) //Cidade/Pais
    {
        if ( selLocalidadeMaeID.options.length != 0 )
            selLocalidadeMaeID.disabled = false;                
        else
            selLocalidadeMaeID.disabled = true;
        // destrava o botao de lupa do Localidade Mae
        
        lockBtnLupa(btnFindLocMae, false);
    }

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    especBtnIsPrintBtn('sup', 0);
}

/********************************************************************
Funcao do programador
Disparada pela funcao optChangedInCmb() quando o combo de tipo de 
registro e alterado, ela preenche o combo de Localizacao de acordo com 
o tipo do registro.
           
Parametros: 
cmb     -> Objeto (combo)

Retorno:
nenhum
********************************************************************/
function fillCmbLocalizacaoID(cmb)
{
    var nTipoLocalidadeID = cmb.value;
    var sCondition = '';
    
    if ( nTipoLocalidadeID == 204)
    {
        clearComboEx(['selLocalidadeMaeID']);
        selLocalidadeMaeID.disabled = true;
        // trava o botao de lupa do Localidade Mae
        lockBtnLupa(btnFindLocMae, true);
    }
    else if( (nTipoLocalidadeID == 203)||(nTipoLocalidadeID == 205) )
    {
        selLocalidadeMaeID.disabled = true;
        // trava o botao de lupa do Localidade Mae
        lockBtnLupa(btnFindLocMae, false);
    }

    lockInterface(true);
    // parametrizacao do dso dsoCmbDynamic01 (designado para selSujeitoID)
    setConnection(dsoCmbDynamic01);                              
            
    if (nTipoLocalidadeID == 203) 
        sCondition = 'AND (a.TipoLocalidadeID<=202) ';
    else if (nTipoLocalidadeID == 204) 
        sCondition = 'AND (a.TipoLocalidadeID=203) ';
    else if (nTipoLocalidadeID == 205) 
        sCondition = 'AND (a.TipoLocalidadeID=204) ';

    // Pais ou Cidade
    if( (nTipoLocalidadeID == 203) || (nTipoLocalidadeID == 205) )  
    {
        dsoCmbDynamic01.SQL = 'SELECT a.LocalidadeID, a.Localidade + SPACE(1) + a.CodigoLocalidade2 AS Localidade ' +
                              'FROM Localidades a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                              'WHERE a.EstadoID=2 AND a.TipoLocalidadeID=b.ItemID ' +
                              sCondition + 
                              'ORDER BY Localidade';
    }
    // Estado
    else if(nTipoLocalidadeID == 204)  
    {
        dsoCmbDynamic01.SQL = 'SELECT a.LocalidadeID, a.Localidade + SPACE(1) + a.CodigoLocalidade2 AS Localidade ' +
                              'FROM Localidades a WITH(NOLOCK) ' +
                              'WHERE a.EstadoID=2 ' +
                              sCondition + 
                              'ORDER BY Localidade';
    }

    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic01_DSC;
    dsoCmbDynamic01.refresh();
}

/********************************************************************
Funcao do programador
Funcao de datasetcomplete disparada pela funcao fillCmbLocalizacaoID
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic01_DSC()
{
    var optionStr, optionValue;

    // Inicia o carregamento de combos dinamicos (selSujeitoID, selObjetoID )
    //
    clearComboEx(['selLocalizacaoID']);
    
    while (! dsoCmbDynamic01.recordset.EOF )
    {
        optionStr = dsoCmbDynamic01.recordset['Localidade'].value;
		optionValue = dsoCmbDynamic01.recordset['LocalidadeID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        selLocalizacaoID.add(oOption);
        dsoCmbDynamic01.recordset.MoveNext();
    }

    lockInterface(false);
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.

    // Nao mexer - Inicio de automacao ==============================
    
    if (adjustSupInterface() == null)
        showHideByTipoRegistro(true);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao criada pelo programador.
Recolhe dados no servidor e preenche combos de lupa.

Parametro:
textoDig        - texto digitado pelo usuario na modal 
idCombo         - id do combo a preencher
idForm          - id do form

Retorno:
nenhum
********************************************************************/
function getDataAndLoadCmbsLupa(textoDig, idCombo, idForm)
{
    glb_cmbLupaID = idCombo;
    var sParam1 = '';
    var sParam2 = '';
    
    if ( idCombo == 'selLocalidadeMaeID' )
    {
         sParam1 = selTipoRegistroID.value;
         sParam2 = selLocalizacaoID.value;
    }            
  	if ( sParam1 == null )
	{
	    clearCombo([idCombo]);
	    document.getElementById(idCombo).disabled = true;
	    return null;
	}

    var strPars = new String();
    strPars = '?';
    strPars += 'sComboID=';
    strPars += escape(idCombo.toString());
    strPars += '&sParam1=';
    strPars += escape(sParam1.toString());
    strPars += '&sParam2=';
    strPars += escape(sParam2.toString());
    strPars += '&sText=';
    strPars += escape(textoDig.toString());
            
    dsoCmbsLupa.URL = SYS_ASPURLROOT + '/basico/localidades/serverside/fillcomboslupa.aspx'+strPars;
    dsoCmbsLupa.ondatasetcomplete = getDataAndLoadCmbsLupa_DSC;
    dsoCmbsLupa.refresh();
}

/********************************************************************
Funcao criada pelo programador.
Recebe dados no servidor e preenche combos de lupa.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function getDataAndLoadCmbsLupa_DSC()
{
    var cmbRef;

    cmbRef = document.getElementById(glb_cmbLupaID);
        
    clearComboEx([glb_cmbLupaID]);

    var optionStr,optionValue;
    while (!dsoCmbsLupa.recordset.EOF)
    {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoCmbsLupa.recordset['fldName'].value;
        oOption.value = dsoCmbsLupa.recordset['fldID'].value;
        cmbRef.add(oOption);
        dsoCmbsLupa.recordset.MoveNext();
    }
            	    
    cmbRef.selectedIndex = -1;
                
    // destrava a interface e fecha a janela modal
    if (restoreInterfaceFromModal())
    {
        cmbRef.disabled = true;
        
        if ( cmbRef.options.length != 0 )
        {
            // destrava e coloca foco no combo se tem options
            cmbRef.disabled = false;
            cmbRef.focus();
        }
    }

    // escreve na barra de status
    writeInStatusBar('child', 'cellMode', 'Altera��o');
}

/********************************************************************
Funcao criada pelo programador.
Limpa e trava os combos dinamicos quando o usuario clicar em inclusao

Parametro:
btnClicked  -> Ultimo botao clicado da barra superior

Retorno:
nenhum
********************************************************************/
function clearAndLockCmbsDynamics(btnClicked)
{
    if (btnClicked == 'SUPINCL')
    {
        clearComboEx(['selLocalizacaoID','selLocalidadeMaeID']);
        selLocalidadeMaeID.disabled = true;
    }
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
    // controla o retorno do servidor dos dados de combos dinamicos
    // (selLocalizacaoID e selLocalidadeMaeID)
    glb_CounterCmbsDynamics = 1;

    var TipoLocalidadeID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
    if ( (TipoLocalidadeID >= 203) && (TipoLocalidadeID <= 205) )
    {
        glb_CounterCmbsDynamics = 2;
        
        setConnection(dsoCmbDynamic01);
                    
        if (TipoLocalidadeID == 203) 
            sCondition = 'AND (a.TipoLocalidadeID<=202) ';
        else if (TipoLocalidadeID == 204) 
            sCondition = 'AND (a.TipoLocalidadeID=203) ';
        else if (TipoLocalidadeID == 205) 
            sCondition = 'AND (a.TipoLocalidadeID=204) ';

        if( (TipoLocalidadeID == 203) || (TipoLocalidadeID == 205) )  
        {
            dsoCmbDynamic01.SQL = 'SELECT a.LocalidadeID AS fldID, a.Localidade + SPACE(1) + a.CodigoLocalidade2 AS fldName ' +
                                  'FROM Localidades a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                                  'WHERE a.EstadoID=2 AND a.TipoLocalidadeID=b.ItemID ' +
                                  sCondition + 
                                  'ORDER BY fldName';
        }
        else if(TipoLocalidadeID == 204)  
        {
            dsoCmbDynamic01.SQL = 'SELECT a.LocalidadeID AS fldID, a.Localidade + SPACE(1) + a.CodigoLocalidade2 AS fldName ' +
                                  'FROM Localidades a WITH(NOLOCK) ' +
                                  'WHERE a.EstadoID=2 ' +
                                  sCondition + 
                                  'ORDER BY fldName';
        }
  
        dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic02_DSC;
        dsoCmbDynamic01.Refresh();
    }

    // parametrizacao do dso dsoCmbDynamic02 
    setConnection(dsoCmbDynamic02);
    
    dsoCmbDynamic02.SQL = 'SELECT 0 AS fldID, ' + '\'' + ' ' + '\'' + ' ' + 'AS fldName ' +
                          'UNION ALL SELECT a.LocalidadeID AS fldID,' +
                          'a.Localidade + ' + '\'' + ' ' + '\'' + ' + b.CodigoLocalidade2 ' +
                          'AS fldName ' +
                          'FROM Localidades a WITH(NOLOCK), Localidades b WITH(NOLOCK) ' +
                          'WHERE a.LocalizacaoID = b.LocalidadeID ' +
                          'AND a.LocalidadeID = ' + dsoSup01.recordset['LocalidadeMaeID'].value;
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic02_DSC;
    dsoCmbDynamic02.Refresh();
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic02_DSC()
{
    var optionStr, optionValue;
    var aCmbsDynamics = [selLocalidadeMaeID,selLocalizacaoID];
    var aDSOsDunamics = [dsoCmbDynamic02,dsoCmbDynamic01];
    var oOption;
    var TipoLocalidadeID = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
    var lExecSecondCmb = (TipoLocalidadeID >= 203) && (TipoLocalidadeID <= 205);
    var i;
    
    // Inicia o carregamento de combo dinamico
    clearComboEx(['selLocalidadeMaeID','selLocalizacaoID']);
    
    glb_CounterCmbsDynamics --;
    
    if (glb_CounterCmbsDynamics == 0)
    {
        for (i=0; i <= (lExecSecondCmb ? 1 : 0); i++)
        {
            while (! aDSOsDunamics[i].recordset.EOF )
            {
                optionStr = aDSOsDunamics[i].recordset['fldName'].value;
	        	optionValue = aDSOsDunamics[i].recordset['fldID'].value;
                oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;
                aCmbsDynamics[i].add(oOption);
                aDSOsDunamics[i].recordset.MoveNext();
            }
        }
        
        setComboLabel(lblTipoRegistroID, 'Tipo ', dsoSup01.recordset['TipoLocalidadeID'].value);
        setComboLabel(lblLocalizacaoID, 'Localiza��o ', dsoSup01.recordset['LocalizacaoID'].value);
        setComboLabel(lblTipoRegiaoID, 'Regi�o ', dsoSup01.recordset['TipoRegiaoID'].value);
        setComboLabel(lblLocalidadeMaeID, 'Localidade M�e ', dsoSup01.recordset['LocalidadeMaeID'].value);
        setComboLabel(lblTipoReligiaoID, 'Religi�o Predominante ', dsoSup01.recordset['TipoReligiaoID'].value);
        setComboLabel(lblSistemaGovernoID, 'Sistema Governo ', dsoSup01.recordset['SistemaGovernoID'].value);
        setComboLabel(lblFormaGovernoID, 'Forma Governo ', dsoSup01.recordset['FormaGovernoID'].value);

        // volta para a automacao
	    finalOfSupCascade(glb_BtnFromFramWork);
	}
	return null;
}

/********************************************************************
Criada pelo programador
           
Parametros: 
nenhum:

Retorno:
nenhum
********************************************************************/
function showHideByTipoRegistro(bEditing)
{
    var nTipoRegistroID;
    if (bEditing == true)
        nTipoRegistroID = selTipoRegistroID.value;
    else
        nTipoRegistroID = dsoSup01.recordset['TipoLocalidadeID'].value;

    if (nTipoRegistroID != 204) 
    {

        adjustElementsInForm([['lblST', 'chkST', 3, 4, 55]]);
        lblST.style.visibility = 'hidden';
        chkST.style.visibility = 'hidden';
        
    }
    else 
    {
        adjustElementsInForm([['lblST', 'chkST', 3, 4, 55]]);
        lblST.style.visibility = 'inherit';
        chkST.style.visibility = 'inherit';
    }
    
    lblArea.style.visibility = 'hidden';
    txtArea.style.visibility = 'hidden';
    lblPopulacao.style.visibility = 'hidden';
    txtPopulacao.style.visibility = 'hidden';
    lblPercentualMasculino.style.visibility = 'hidden';
    txtPercentualMasculino.style.visibility = 'hidden';
    lblPIB.style.visibility = 'hidden';
    txtPIB.style.visibility = 'hidden';
    lblTipoReligiaoID.style.visibility = 'hidden';
    selTipoReligiaoID.style.visibility = 'hidden';
    lblPercentualReligiao.style.visibility = 'hidden';
    txtPercentualReligiao.style.visibility = 'hidden';
    lblFuso.style.visibility = 'hidden';
    txtFuso.style.visibility = 'hidden';
    lblRetemImposto.style.visibility = 'hidden';
    chkRetemImposto.style.visibility = 'hidden';
    lblNomeOficial.style.visibility = 'hidden';
    txtNomeOficial.style.visibility = 'hidden';
	lblNomeInternacional.style.visibility = 'hidden';
    txtNomeInternacional.style.visibility = 'hidden';
	lblCodigoLocalidade3.style.visibility = 'hidden';
    txtCodigoLocalidade3.style.visibility = 'hidden';
	lblCriticaDocumento.style.visibility = 'hidden';
	txtCriticaDocumento.style.visibility = 'hidden';
	
	lblIDH.style.visibility = 'hidden';    
	txtIDH.style.visibility = 'hidden';	
    
	lblEnderecoInvertido.style.visibility = 'hidden';
	chkEnderecoInvertido.style.visibility = 'hidden';
	lblFantasiaInvertido.style.visibility = 'hidden';
	chkFantasiaInvertido.style.visibility = 'hidden';
    lblEnderecoInvertido.style.visibility = 'hidden';
    chkEnderecoInvertido.style.visibility = 'hidden';
    lblSistemaGovernoID.style.visibility = 'hidden';
    selSistemaGovernoID.style.visibility = 'hidden';
	lblFormaGovernoID.style.visibility = 'hidden';
    selFormaGovernoID.style.visibility = 'hidden';
	lblExcecao.style.visibility = 'hidden';
    chkExcecao.style.visibility = 'hidden';
	//lblAnoConstituicao.style.visibility = 'hidden';    
	//txtAnoConstituicao.style.visibility = 'hidden';	
    lblSuspensa.style.visibility = 'hidden';
    chkSuspensa.style.visibility = 'hidden';
	lblDivisaoAdministrativa.style.visibility = 'hidden';
    txtDivisaoAdministrativa.style.visibility = 'hidden';

    if (nTipoRegistroID >= 203)
    {
        lblArea.style.visibility = 'inherit';
        txtArea.style.visibility = 'inherit';
        lblPopulacao.style.visibility = 'inherit';
        txtPopulacao.style.visibility = 'inherit';
        lblPercentualMasculino.style.visibility = 'inherit';
        txtPercentualMasculino.style.visibility = 'inherit';
        lblPIB.style.visibility = 'inherit';
        txtPIB.style.visibility = 'inherit';
        lblTipoReligiaoID.style.visibility = 'inherit';
        selTipoReligiaoID.style.visibility = 'inherit';
        lblPercentualReligiao.style.visibility = 'inherit';
        txtPercentualReligiao.style.visibility = 'inherit';
        lblFuso.style.visibility = 'inherit';
        txtFuso.style.visibility = 'inherit';
    }
    
    if (nTipoRegistroID == 203)
    {
        lblNomeOficial.style.visibility = 'inherit';
        txtNomeOficial.style.visibility = 'inherit';
	    lblNomeInternacional.style.visibility = 'inherit';
        txtNomeInternacional.style.visibility = 'inherit';
	    lblCodigoLocalidade3.style.visibility = 'inherit';
        txtCodigoLocalidade3.style.visibility = 'inherit';
		lblCriticaDocumento.style.visibility = 'inherit';
		txtCriticaDocumento.style.visibility = 'inherit';
		lblIDH.style.visibility = 'inherit';
		txtIDH.style.visibility = 'inherit';
		lblEnderecoInvertido.style.visibility = 'inherit';
		chkEnderecoInvertido.style.visibility = 'inherit';
		lblFantasiaInvertido.style.visibility = 'inherit';
		chkFantasiaInvertido.style.visibility = 'inherit';		
        lblEnderecoInvertido.style.visibility = 'inherit';
        chkEnderecoInvertido.style.visibility = 'inherit';
        lblSistemaGovernoID.style.visibility = 'inherit';
        selSistemaGovernoID.style.visibility = 'inherit';
	    lblFormaGovernoID.style.visibility = 'inherit';
        selFormaGovernoID.style.visibility = 'inherit';
	    lblExcecao.style.visibility = 'inherit';
        chkExcecao.style.visibility = 'inherit';
	    //lblAnoConstituicao.style.visibility = 'inherit';
		
//        if (bEditing)
//        {                       
//            txtAnoConstituicao.style.visibility = 'inherit';            
//        }
//        else
//        {            
//            txtAnoConstituicao.style.visibility = 'hidden';
//        }
        
        lblSuspensa.style.visibility = 'inherit';
        chkSuspensa.style.visibility = 'inherit';
	    lblDivisaoAdministrativa.style.visibility = 'inherit';
	    txtDivisaoAdministrativa.style.visibility = 'inherit';
    }

	if (nTipoRegistroID < 204)
	{
		lblCapital.style.visibility = 'hidden';
		chkCapital.style.visibility = 'hidden';
	}
	else
	{
		lblCapital.style.visibility = 'inherit';
		chkCapital.style.visibility = 'inherit';
	}
	
	if (nTipoRegistroID != 205)
	{
		lblMetropole.style.visibility = 'hidden';
		chkMetropole.style.visibility = 'hidden';
		lblFluvial.style.visibility = 'hidden';
		chkFluvial.style.visibility = 'hidden';
		lblRetemImposto.style.visibility = 'hidden';
		chkRetemImposto.style.visibility = 'hidden';
	}
	else
	{
		lblMetropole.style.visibility = 'inherit';
		chkMetropole.style.visibility = 'inherit';
		lblFluvial.style.visibility = 'inherit';
		chkFluvial.style.visibility = 'inherit';
		lblRetemImposto.style.visibility = 'inherit';
		chkRetemImposto.style.visibility = 'inherit';
	}
}

/********************************************************************
Funcao criada pelo programador.
Ajusta o label de um combo de acordo com o ID do mesmo

Parametros:
lblRef     Referencia ao combo
sLabel     Texto do label
nLabelID   ID do elemento selecionado

Retorno:
nenhum
********************************************************************/
function setComboLabel(lblRef, sLabel, nLabelID)
{
    if (nLabelID == null)
        nLabelID = '';
        
    var sFullLabel = sLabel + nLabelID;
    lblRef.style.width = sFullLabel.length * FONT_WIDTH;
    lblRef.innerText = sFullLabel;
}

/********************************************************************
Funcao criada pelo programador.
Verificacoes da Localidade

Parametros:
nCurrEstadoID ID do estado atual
nNewEstadoID  ID do estado novo

Retorno:
nenhum
********************************************************************/
function verifyLocalidadeInServer(nCurrEstadoID, nNewEstadoID) {
    var nLocalidadeID = dsoSup01.recordset['LocalidadeID'].value;
    var strPars = new String();

    strPars  = '?nLocalidadeID=' + escape(nLocalidadeID);
    strPars += '&nCurrEstadoID=' + escape(nCurrEstadoID);
    strPars += '&nNewEstadoID=' + escape(nNewEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/basico/localidades/serverside/verificacaolocalidade.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyLocalidadeInServer_DSC;
    dsoVerificacao.refresh();
}

function verifyLocalidadeInServer_DSC() {

    var sMensagem = (dsoVerificacao.recordset['Mensagem'].value == null ? '' : dsoVerificacao.recordset['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset['Resultado'].value == null ? 0 : dsoVerificacao.recordset['Resultado'].value);

    if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            stateMachSupExec('OK');
        else
            stateMachSupExec('CANC');
    }
    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('OK');
    }
    else {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('CANC');
    }
}
