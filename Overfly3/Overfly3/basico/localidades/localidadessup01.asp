<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="localidadessup01Html" name="localidadessup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf    
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/localidades/localidadessup01.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/basico/localidades/especificsup.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="localidadessup01Body" name="localidadessup01Body" LANGUAGE="javascript" onload="return window_onload()">    

    <!-- //@@ Os dsos sao definidos de acordo com o form -->
        
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">        
        <p id="lblLocalidade" name="lblLocalidade" class="lblGeneral">Localidade</p>
        <input type="text" id="txtLocalidade" DATASRC="#dsoSup01" DATAFLD="Localidade" name="txtLocalidade" class="fldGeneral">
        <p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
        <select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoLocalidadeID"></select>    
        <p id="lblCodigoLocalidade2" name="lblCodigoLocalidade2" class="lblGeneral">C�d2</p>
        <input type="text" id="txtCodigoLocalidade2" name="txtCodigoLocalidade2" DATASRC="#dsoSup01" DATAFLD="CodigoLocalidade2" class="fldGeneral" title="C�digo ISO de 2 caracteres">
        <p id="lblCodigoLocalidade" name="lblCodigoLocalidade" class="lblGeneral">C�digo</p>
        <input type="text" id="txtCodigoLocalidade" name="txtCodigoLocalidade" DATASRC="#dsoSup01" DATAFLD="CodigoLocalidade" class="fldGeneral" title="C�digo da localidade">
        <p id="lblCapital" name="lblCapital" class="lblGeneral">Cap</p>
        <input type="checkbox" id="chkCapital" name="chkCapital" DATASRC="#dsoSup01" DATAFLD="Capital" class="fldGeneral" title="� capital?">
        <p id="lblMetropole" name="lblMetropole" class="lblGeneral">Met</p>
        <input type="checkbox" id="chkMetropole" name="chkMetropole" DATASRC="#dsoSup01" DATAFLD="Metropole" class="fldGeneral" title="Est� na regi�o metropolitana do estado?">
        <p id="lblFluvial" name="lblFluvial" class="lblGeneral">Flu</p>
        <input type="checkbox" id="chkFluvial" name="chkFluvial" DATASRC="#dsoSup01" DATAFLD="Fluvial" class="fldGeneral" title="Est� em regi�o fluvial?">
        
    </div>
    
    <!-- Primeiro div secundario superior - Dados Complementares -->    
    <div id="divSup02_01" name="divSup02_01" class="divExtern">
        <p id="lblLocalizacaoID" name="lblLocalizacaoID" class="lblGeneral">Localiza��o</p>
        <select id="selLocalizacaoID" name="selLocalizacaoID" DATASRC="#dsoSup01" DATAFLD="LocalizacaoID" class="fldGeneral"></select>
        <p id="lblTipoRegiaoID" name="lblTipoRegiaoID" class="lblGeneral">Regi�o</p>
        <select id="selTipoRegiaoID" name="selTipoRegiaoID" DATASRC="#dsoSup01" DATAFLD="TipoRegiaoID" class="fldGeneral"></select>
        <p id="lblLocalidadeMaeID" name="lblLocalidadeMaeID" class="lblGeneral">Localidade M�e</p>
        <select id="selLocalidadeMaeID" name="selLocalidadeMaeID" DATASRC="#dsoSup01" DATAFLD="LocalidadeMaeID" class="fldGeneral"></select>
        <input type="image" id="btnFindLocMae" name="btnFindLocMae" class="fldGeneral" title="Preencher combo" WIDTH="24" HEIGHT="23">
        <p id="lbldtFundacao" name="lbldtFundacao" class="lblGeneral">Funda��o</p>
        <input type="text" id="txtdtFundacao" name="txtdtFundacao" DATASRC="#dsoSup01" DATAFLD="V_dtFundacao" class="fldGeneral">
        <p id="lblNaturalidade" name="lblNaturalidade" class="lblGeneral">Naturalidade</p>
        <input type="text" id="txtNaturalidade" name="txtNaturalidade" DATASRC="#dsoSup01" DATAFLD="Naturalidade" class="fldGeneral">
        <p id="lblArea" name="lblArea" class="lblGeneral">�rea (Km&#178)</p>
        <input type="text" id="txtArea" name="txtArea" DATASRC="#dsoSup01" DATAFLD="Area" class="fldGeneral"></input>
        <p id="lblPopulacao" name="lblPopulacao" class="lblGeneral">Popula��o</p>
        <input type="text" name="txtPopulacao" id="txtPopulacao" DATASRC="#dsoSup01" DATAFLD="Populacao" class="fldGeneral"></input>
        <p id="lblPercentualMasculino" name="lblPercentualMasculino" class="lblGeneral">Perc Masc</p>
        <input type="text" id="txtPercentualMasculino" name="txtPercentualMasculino" DATASRC="#dsoSup01" DATAFLD="PercentualMasculino" class="fldGeneral" title="Percentual masculino"></input> 
        <p id="lblPIB" name="lblPIB" class="lblGeneral">PIB (US$ Milh�es)</p>
        <input type="text" id="txtPIB" name="txtPIB" DATASRC="#dsoSup01" DATAFLD="PIB" class="fldGeneral"></input>
        <p id="lblTipoReligiaoID" name="lblTipoReligiaoID" class="lblGeneral">Religi�o Predominante</p>
        <select id="selTipoReligiaoID" DATASRC="#dsoSup01" DATAFLD="TipoReligiaoID" name="selTipoReligiaoID" class="fldGeneral"></select>
    	<p id="lblPercentualReligiao" name="lblPercentualReligiao" class="lblGeneral">Perc</p>
        <input type="text" id="txtPercentualReligiao" name="txtPercentualReligiao" DATASRC="#dsoSup01" DATAFLD="PercentualReligiao" class="fldGeneral" title="Percentual desta religi�o sobre a popula��o"></input>
    	<p id="lblFuso" name="lblFuso" class="lblGeneral">Fuso</p>
        <input type="text" id="txtFuso" name="txtFuso" DATASRC="#dsoSup01" DATAFLD="Fuso" class="fldGeneral" ></input>

        <p id="lblRetemImposto" name="lblRetemImposto" class="lblGeneral">RI</p>
        <input type="checkbox" id="chkRetemImposto" name="chkRetemImposto" DATASRC="#dsoSup01" DATAFLD="RetemImposto" class="fldGeneral" title="A prefeitura deste munic�pio ret�m impostos?"></input>

        <p id="lblST" name="lblST" class="lblGeneral">ST</p>
        <input type="checkbox" id="chkST" name="chkST" DATASRC="#dsoSup01" DATAFLD="ST" class="fldGeneral" title="Estado exige que separe produtos com ST na Nota Fiscal?"></input>
        <p id="lblNomeOficial" name="lblNomeOficial" class="lblGeneral">Nome Oficial</p>
        <input type="text" id="txtNomeOficial" name="txtNomeOficial" DATASRC="#dsoSup01" DATAFLD="NomeOficial" class="fldGeneral"></input>
		<p id="lblNomeInternacional" name="lblNomeInternacional" class="lblGeneral">Nome Internacional</p>
        <input type="text" id="txtNomeInternacional" name="txtNomeInternacional" DATASRC="#dsoSup01" DATAFLD="NomeInternacional" class="fldGeneral"></input>
		<p id="lblCodigoLocalidade3" name="lblCodigoLocalidade3" class="lblGeneral" title="C�digo ISO de 3 caracteres">C�d3</p>
        <input type="text" id="txtCodigoLocalidade3" name="txtCodigoLocalidade3" DATASRC="#dsoSup01" DATAFLD="CodigoLocalidade3" class="fldGeneral"></input>
		<p id="lblCriticaDocumento" name="lblCriticaDocumento" class="lblGeneral" title="Cr�tica de Documento Federal">CD</p>
        <input type="text" id="txtCriticaDocumento" name="txtCriticaDocumento" DATASRC="#dsoSup01" DATAFLD="CriticaDocumento" class="fldGeneral" title="Cr�tica de Documento Federal"></input>
		<p id="lblIDH" name="lblIDH" class="lblGeneral" title="�ndice de Desenvolvimento Humano">IDH</p>
        <input type="text" id="txtIDH" name="txtIDH" DATASRC="#dsoSup01" DATAFLD="IDH" class="fldGeneral" title="�ndice de Desenvolvimento Humano"></input>
        <p id="lblEnderecoInvertido" name="lblEnderecoInvertido" class="lblGeneral">EI</p>
        <input type="checkbox" id="chkEnderecoInvertido" name="chkEnderecoInvertido" DATASRC="#dsoSup01" DATAFLD="EnderecoInvertido" class="fldGeneral" title="Endere�o invertido?"></input>
		<p id="lblFantasiaInvertido" name="lblFantasiaInvertido" class="lblGeneral" title="Fantasia da pessoa invertido?">FI</p>
        <input type="checkbox" id="chkFantasiaInvertido" name="chkFantasiaInvertido" DATASRC="#dsoSup01" DATAFLD="FantasiaInvertido" class="fldGeneral" title="Fantasia da pessoa invertido?"></input>
        <p id="lblSistemaGovernoID" name="lblSistemaGovernoID" class="lblGeneral">Sistema Governo</p>
        <select id="selSistemaGovernoID" name="selSistemaGovernoID" DATASRC="#dsoSup01" DATAFLD="SistemaGovernoID" class="fldGeneral"></select>
		<p id="lblFormaGovernoID" name="lblFormaGovernoID" class="lblGeneral">Forma Governo</p>
        <select id="selFormaGovernoID" name="selFormaGovernoID" DATASRC="#dsoSup01" DATAFLD="FormaGovernoID" class="fldGeneral"></select>
		<p id="lblExcecao" name="lblExcecao" class="lblGeneral">Exc</p>
        <input type="checkbox" id="chkExcecao" name="chkExcecao" DATASRC="#dsoSup01" DATAFLD="Excessao" class="fldGeneral" title="Est� em regime de exce��o?"></input>
        <!-- Comentado por DMC, n�o esta sendo usado em nenhum contexto - 05/05/2011
		<p id="lblAnoConstituicao" name="lblAnoConstituicao" class="lblGeneral">Const</p>
        <input type="text" id="txtAnoConstituicao" name="txtAnoConstituicao" DATASRC="#dsoSup01" DATAFLD="AnoConstituicao" class="fldGeneral" title="Ano da Constitui��o"></input>-->
    	<p id="lblSuspensa" name="lblSuspensa" class="lblGeneral">Susp</p>
        <input type="checkbox" id="chkSuspensa" name="chkSuspensa" DATASRC="#dsoSup01" DATAFLD="Suspensa" class="fldGeneral" title="Constitui��o est� suspensa?"></input>
		<p id="lblDivisaoAdministrativa" name="lblDivisaoAdministrativa" class="lblGeneral">Divis�o Administrativa</p>
        <input type="text" id="txtDivisaoAdministrativa" name="txtDivisaoAdministrativa" DATASRC="#dsoSup01" DATAFLD="DivisaoAdministrativa" class="fldGeneral"></input>
    </div>
    
</body>

</html>
