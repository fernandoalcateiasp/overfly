/********************************************************************
modalvernumserie.js

Library javascript para o modalvernumserie.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_timerInterval = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalvernumserie_AfterEdit(Row, Col)
{
    ;
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modalvernumserieBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    onchange_selProdutoID(selProdutoID);

    window.focus();
    selProdutoID.focus();
    
    startPesq();
}

/********************************************************************
Usuario otrcou o option do combo de Produtos
********************************************************************/
function onchange_selProdutoID(ctl)
{
	fg.Rows = 1;
	
	if (parseInt(selProdutoID.value, 10) == 0)
		setLabelOfControl(lblProdutoID, '');
	else
		setLabelOfControl(lblProdutoID, selProdutoID.value);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('N�meros de S�rie', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

	// ajusta elementos no divFields
    adjustElementsInForm([['lblProdutoID','selProdutoID',25,1,-10,-10],
                          ['lblNumeroSerie','txtNumeroSerie',20,1]],null,null,true);

	elem = txtNumeroSerie;
	useBtnListar(true, divFields, elem);
    
    with (btnListar)
    {
        style.left = parseInt(elem.currentStyle.left, 10) +
               parseInt(elem.currentStyle.width, 10) + ELEM_GAP;
        style.top = parseInt(elem.currentStyle.top, 10) - 1;
    }

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);
        // altura e fixa para tres linhas de campos
        height = parseInt(selProdutoID.currentStyle.top, 10) + 
				 parseInt(selProdutoID.currentStyle.height, 10) + ELEM_GAP;
        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Editable = false;
    
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    
    btnCanc.style.left = ( parseInt(divFG.currentStyle.width, 10) -
                           parseInt(btnCanc.currentStyle.width, 10) ) / 2;
    
    // restringe digitacao em campos numericos
    // e da outras providencias
    setMaskAndLengthToInputs();
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs()
{
    // Seleciona conteudo de campo texto quando em foco
    txtNumeroSerie.onfocus = selFieldContent;
    txtNumeroSerie.onkeypress = execPesq_OnKey;
}

/********************************************************************
Pesquisa se Enter em campos com esta key
********************************************************************/
function execPesq_OnKey()
{
    if ( event.keyCode == 13 )
        btnListar_onclick();        
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;

    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();

    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    // 1. O usuario clicou o botao OK
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null );
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalvernumserieBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    if (txtNumeroSerie.currentStyle.visibility != 'hidden')
        if (txtNumeroSerie.disabled == false)
            txtNumeroSerie.focus();
}

/********************************************************************
Usuario clicou o botao listar
********************************************************************/
function btnListar_onclick()
{
    var stopPesq = false;
    
    // trima todos os campos
    selProdutoID.value = trimStr(selProdutoID.value);

    stopPesq = ( (trimStr(selProdutoID.value) == '') &&
                 (trimStr(txtNumeroSerie.value) == '') );

    fg.Rows = 1;

    if ( stopPesq )
    {
        if ( window.top.overflyGen.Alert('Preencha ao menos um campo, para listar.') == 0 )
            return null;
                    
        window.focus();
                    
        return true;
    }    
    
    lockControlsInModalWin(true);
    
    // conforme conteudo de selProdutoID
    // considera ou nao os valores digitados nos campos
    // txtNumeroSerie

    glb_timerInterval = window.setInterval ( 'startPesq()', 10 , 'JavaScript');
}

function startPesq()
{
    if ( glb_timerInterval != null )
    {
        window.clearInterval(glb_timerInterval);
        glb_timerInterval = null;
    }

	var sFiltroNumeSerie, sOperator, sFiltroProduto;
	sFiltroNumeSerie = '';
	sFiltroProduto = '';

    lockControlsInModalWin(true);
    
    txtNumeroSerie.value = trimStr(txtNumeroSerie.value);
    
    if (txtNumeroSerie.value != '')
    {
		if ( ((txtNumeroSerie.value).substr(0,1) == '%') || ((txtNumeroSerie.value).substr((txtNumeroSerie.value).length-1,1) == '%') )
			sOperator = 'LIKE';
		else
			sOperator = '=';
			
		sFiltroNumeSerie = ' AND b.NumeroSerie ' + sOperator + ' ' + '\'' + txtNumeroSerie.value + '\'' + ' ';
    }
    
    if (selProdutoID.value != 0)
		sFiltroProduto = ' AND c.ConceitoID = ' + selProdutoID.value + ' ';
    
    setConnection(dsoGen01);

    dsoGen01.SQL = 'SELECT dbo.fn_OrdemProducaoItem_Ordem(d.OrdItemID) AS Ordem, c.Conceito, c.ConceitoID, ' +
		'd.Quantidade, b.NumeroSerie ' +
		'FROM NumerosSerie_Movimento a WITH(NOLOCK), NumerosSerie b WITH(NOLOCK), Conceitos c WITH(NOLOCK), OrdensProducao_Itens d WITH(NOLOCK) ' +
		'WHERE (a.OrdemProducaoID = ' + glb_nOPID + ' AND ' +
			'a.NumeroSerieID = b.NumeroSerieID AND b.ProdutoID = c.ConceitoID AND ' +
			'd.OrdemProducaoID=a.OrdemProducaoID AND d.ProdutoID=b.ProdutoID ' +
			sFiltroNumeSerie + sFiltroProduto + ') ' +
		'ORDER BY dbo.fn_OrdemProducaoItem_Ordem(d.OrdItemID), c.ProdutoID, c.Conceito, b.NumeroSerie';
	
    dsoGen01.ondatasetcomplete = startPesq_DSC;

    try
    {
        dsoGen01.Refresh();
    }
    catch(e)
    {
        if ( window.top.overflyGen.Alert('Filtro Inv�lido.') == 0 )
            return null;
            
        lockControlsInModalWin(false);
        
        fg.Rows = 1;
        
        if (txtProdutoID.disabled == false)
        {
            window.focus();
            txtProdutoID.focus();
        }    
        return null;
    }
}

function startPesq_DSC()
{
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    headerGrid(fg,['Ordem',
				   'Produto', 
				   'ID',
				   'Quant',
				   'N�mero de S�rie'], []);

    fillGridMask(fg,dsoGen01,['Ordem',
							  'Conceito', 
							  'ConceitoID',
							  'Quantidade',
							  'NumeroSerie'],
                              ['','','','',''],
                              ['##########','','##########','##########','']);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    alignColsInGrid(fg,[1]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.Redraw = 2;
    
    alignColsInGrid(fg,[ 0, 2, 3]);
    
    lockControlsInModalWin(false);
    
    // se tem linhas no grid, coloca foco no grid, caso contrario
    // coloca foco no txtNumeroSerie
    if (fg.Rows > 1)
            fg.focus();
    else
    {
        if ( !txtNumeroSerie.disabled )
                txtNumeroSerie.focus();
    }            
}
