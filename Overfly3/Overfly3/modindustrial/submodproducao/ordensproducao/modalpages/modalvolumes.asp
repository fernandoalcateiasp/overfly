
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalvolumesHtml" name="modalvolumesHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modindustrial/submodproducao/ordensproducao/modalpages/modalvolumes.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim rsData, strSQL
Dim i, nOPID, nEstadoDeID, nEstadoParaID

nOPID = 0
nEstadoDeID = 0
nEstadoParaID = 0

For i = 1 To Request.QueryString("nOPID").Count    
    nOPID = Request.QueryString("nOPID")(i)
Next

Response.Write "var glb_nOPID = " & CStr(nOPID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nEstadoDeID").Count    
    nEstadoDeID = Request.QueryString("nEstadoDeID")(i)
Next

Response.Write "var glb_nEstadoDeID = " & CStr(nEstadoDeID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("nEstadoParaID").Count    
    nEstadoParaID = Request.QueryString("nEstadoParaID")(i)
Next

Response.Write "var glb_nEstadoParaID = " & CStr(nEstadoParaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nNumeroVolumes = '';"
Response.Write vbcrlf

strSQL = "SELECT NumeroVolumes FROM OrdensProducao WITH(NOLOCK) WHERE OrdemProducaoID= " & CStr(nOPID)

Set rsData = Server.CreateObject("ADODB.Recordset")         

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

If ( Not((rsData.BOF) AND (rsData.EOF)) ) Then
    If ( Not IsNull(rsData.Fields("NumeroVolumes").Value) ) Then
		Response.Write "glb_nNumeroVolumes = " & CStr(rsData.Fields("NumeroVolumes").Value) & ";"
		Response.Write vbcrlf
    End If
End If

rsData.Close
Set rsData = Nothing


Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<body id="modalvolumesBody" name="modalvolumesBody" LANGUAGE="javascript" onload="return window_onload()">
        
    <div id="divVolumes" name="divVolumes" class="divGeneral">
        <p id="lblNumeroVolumes" name="lblNumeroVolumes" class="lblGeneral">Volumes</p>
        <input type="text" id="txtNumeroVolumes" name="txtNumeroVolumes" class="fldGeneral"></input>
    </div>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
