/********************************************************************
modalincluiitens.js

Library javascript para o modalincluiitens.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_timerInterval = null;

// controla se os itens do pedido corrente no inf foram alterados
var glb_pedidoAlterado = false;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalIncluiItens_AfterEdit(Row, Col)
{
    if (fg.Editable)
    {
        if (fg.col == 5)
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    }
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modalincluiitensBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    txtPedidoReferencia.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', glb_pedidoAlterado );    
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Inclus�o de Itens', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    btnOK.value = 'Incluir';
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

	// ajusta elementos no divFields
    adjustElementsInForm([['lblPedidoReferencia','txtPedidoReferencia',10,1,-10,-10],
                          ['lblPalavraChave','txtPalavraChave',31,1]],null,null,true);

	elem = txtPalavraChave;
	useBtnListar(true, divFields, elem);
    
    with (btnListar)
    {
        style.left = parseInt(elem.currentStyle.left, 10) +
               parseInt(elem.currentStyle.width, 10) + ELEM_GAP;
        style.top = parseInt(elem.currentStyle.top, 10) - 1;
    }

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);
        // altura e fixa para tres linhas de campos
        height = parseInt(txtPedidoReferencia.currentStyle.top, 10) + 
				 parseInt(txtPedidoReferencia.currentStyle.height, 10) + ELEM_GAP;
        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    
    // restringe digitacao em campos numericos
    // e da outras providencias
    setMaskAndLengthToInputs();
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs()
{
    // Seleciona conteudo de campo texto quando em foco
    txtPedidoReferencia.onfocus = selFieldContent;
    txtPalavraChave.onfocus = selFieldContent;
    
    txtPedidoReferencia.onkeydown = execPesq_OnKey;
    txtPedidoReferencia.onkeypress = verifyNumericEnterNotLinked;
    txtPedidoReferencia.onkeyup = showHideFlds;
    txtPedidoReferencia.setAttribute('thePrecision', 10, 1);
    txtPedidoReferencia.setAttribute('theScale', 0, 1);
    txtPedidoReferencia.setAttribute('verifyNumPaste', 1);
    txtPedidoReferencia.setAttribute('minMax', new Array(1, 9999999999), 1);
    txtPedidoReferencia.value = '';
    
    txtPalavraChave.onkeypress = execPesq_OnKey;
}

/********************************************************************
Pesquisa se Enter em campos com esta key
********************************************************************/
function execPesq_OnKey()
{
    txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);

    if ( ((this.id == 'txtPedidoReferencia') ) &&
         (event.keyCode != 13) )
    {
        fg.Rows = 1;
    }

    if ( event.keyCode == 13 )
        btnListar_onclick();        
}

/********************************************************************
Mostra esconde campos
********************************************************************/
function showHideFlds()
{
    txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);

    if ( (txtPedidoReferencia.value == '') ||
         (parseInt(txtPedidoReferencia.value, 10) == 0) )
    {
        lblPalavraChave.style.visibility = 'inherit';
        txtPalavraChave.style.visibility = 'inherit';
    }
    else
    {
        lblPalavraChave.style.visibility = 'hidden';
        txtPalavraChave.style.visibility = 'hidden';
    }
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;

    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();

    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    // 1. O usuario clicou o botao OK
    //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null );

    var i, j, nNumeroItens;
    var aDataToSave = new Array();
    
    nNumeroItens = 0;
    j = 0;
    var nVariacaoItem = 0;
    var nValorUnitario = 0;
    var nTipoMaterialIDtoWrite = 0;

    // Verifica coluna de quantidades do grid
    for ( i=1; i<fg.Rows; i++)
    {
        fg.TextMatrix(i, getColIndexByColKey(fg, 'Quantidade')) = trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Quantidade')));
        if ( (isNaN(parseFloat(fg.TextMatrix(i, getColIndexByColKey(fg, 'Quantidade'))))) ||
             (parseFloat(fg.TextMatrix(i, getColIndexByColKey(fg, 'Quantidade'))) == 0) )
        {
            fg.TextMatrix(i, getColIndexByColKey(fg, 'Quantidade')) = '';
            continue;
        }
        else
        {
            nNumeroItens++;
			nVariacaoItem = 0;
			nValorUnitario = 0;

            nTipoMaterialIDtoWrite = (txtPalavraChave.currentStyle.visibility == 'inherit' ? 762 : 763);
            
            aDataToSave[j] = new Array(getCellValueByColKey(fg, 'ProdutoID*', i),
                                       getCellValueByColKey(fg, 'EstadoID', i),
                                       getCellValueByColKey(fg, 'Quantidade', i),
                                       nTipoMaterialIDtoWrite,
                                       parseInt(getCellValueByColKey(fg, 'Quantidade', i), 10) * parseFloat(getCellValueByColKey(fg, 'TempoProducao', i)));
            j++;
        }
    }

    if ( nNumeroItens == 0 )
    {
        if ( window.top.overflyGen.Alert('Nenhum �tem a ser gravado!') == 0 )
            return null;
        lockControlsInModalWin(false);
        window.focus();
        fg.focus();
        return null;
    }
    
    saveItens(aDataToSave, aDataToSave.length, aDataToSave[0].length);
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalincluiitensBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    if (txtPalavraChave.currentStyle.visibility != 'hidden')
        if (txtPalavraChave.disabled == false)
            txtPalavraChave.focus();
}

/********************************************************************
Usuario clicou o botao listar
********************************************************************/
function btnListar_onclick()
{
    var stopPesq = false;
    
    // trima todos os campos
    txtPedidoReferencia.value = trimStr(txtPedidoReferencia.value);

    stopPesq = ( (trimStr(txtPedidoReferencia.value) == '') &&
                 (trimStr(txtPalavraChave.value) == '') );

    fg.Rows = 1;

    if ( stopPesq )
    {
        if ( window.top.overflyGen.Alert('Preencha ao menos um campo, para listar.') == 0 )
            return null;
                    
        window.focus();
                    
        return true;
    }    
    
    lockControlsInModalWin(true);
    
    // conforme conteudo de txtPedidoReferencia
    // considera ou nao os valores digitados nos campos
    // txtPalavraChave

    glb_timerInterval = window.setInterval ( 'startPesq()', 10 , 'JavaScript');
}

function startPesq()
{
    if ( glb_timerInterval != null )
    {
        window.clearInterval(glb_timerInterval);
        glb_timerInterval = null;
    }

    lockControlsInModalWin(true);
    
	var nEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');

	var nIdiomaDeID = nEmpresaData[7];
	var nIdiomaParaID = nEmpresaData[8];
        
    var strSQL = '';
    var strSQLSelect = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    
    var nRegsToList = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    
    // N�o tem pedido de refer�ncia
    if (txtPedidoReferencia.value == '')
    {
        var sPalavraChave = txtPalavraChave.value;

        var sFiltroFROM = ', Conceitos Marcas WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), ' +
			'RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos1 WITH(NOLOCK) ';

        var sFiltro = ' AND Produtos.MarcaID = Marcas.ConceitoID AND Produtos.ProdutoID = Familias.ConceitoID AND ' +
                   'Familias.EstadoID = 2 AND Familias.ConceitoID = RelConceitos2.SujeitoID AND ' +
				   'RelConceitos2.TipoRelacaoID = 41 AND RelConceitos2.EstadoID = 2 AND ' +
				   'RelConceitos2.ObjetoID = Conceitos2.ConceitoID AND Conceitos2.EstadoID = 2 AND ' +
				   'Conceitos2.ConceitoID = RelConceitos1.SujeitoID AND RelConceitos1.TipoRelacaoID = 41 AND ' +
				   'RelConceitos1.EstadoID = 2 AND RelConceitos1.ObjetoID = Conceitos1.ConceitoID AND ' + 
				   'Conceitos1.EstadoID = 2 AND ' +
                   '((Produtos.ConceitoID LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (Produtos.Conceito LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (dbo.fn_Tradutor(Familias.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (dbo.fn_Tradutor(Conceitos1.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (dbo.fn_Tradutor(Conceitos2.Conceito, ' + nIdiomaDeID + ', ' + nIdiomaParaID + ', NULL) LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' +
                   ' (Marcas.Conceito LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.Modelo LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.Descricao LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' ) OR ' + 
                   ' (Produtos.PartNumber LIKE ' + '\'' + '%'  + sPalavraChave + '%' + '\'' + ' )) '; 

        strSQLSelect = 'SELECT DISTINCT TOP ' + nRegsToList + ' ' +
			'Produtos.Conceito AS Produto, Produtos.ConceitoID AS ProdutoID, ' +
    	    'Estados.RecursoID AS EstadoID, Estados.RecursoAbreviado AS Estado, ' +
	    	'dbo.fn_Produto_Estoque(ProdutosEmpresa.SujeitoID, ProdutosEmpresa.ObjetoID, 356, NULL, NULL, NULL, 375, NULL) AS Estoque, ' +
	    	'SPACE(0) AS Quantidade, dbo.fn_Produto_TempoProducao(Produtos.ConceitoID, NULL) AS TempoProducao ';

		strSQLFrom = 'FROM ' +
		    'RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Recursos Estados WITH(NOLOCK) ' + sFiltroFROM + ' ';

	    strSQLWhere = 'WHERE ' +
		    '(ProdutosEmpresa.SujeitoID= ' + glb_nEmpresaID + ' AND ProdutosEmpresa.TipoRelacaoID=61 AND ' +
		    'ProdutosEmpresa.EstadoID IN (2, 11, 12, 13, 14, 15) AND ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND ' +
		    'Produtos.EstadoID=2 AND ProdutosEmpresa.EstadoID=Estados.RecursoID AND ' +
		    '((SELECT COUNT(*) FROM OrdensProducao_Itens WITH(NOLOCK) WHERE (OrdemProducaoID = ' + glb_nOPID + ' AND ProdutoID = Produtos.ConceitoID)) = 0) ' + sFiltro + ' ) ' + 
		    'ORDER BY Produtos.Conceito ';
    
		strSQL = strSQLSelect + strSQLFrom + strSQLWhere;
    }
    // Pedido de Referencia
    else
    {
        var nPedidoReferenciaID = parseInt((txtPedidoReferencia.value) ,10); 

        strSQLSelect = 'SELECT TOP ' + nRegsToList + ' Produtos.Conceito AS Produto, Produtos.ConceitoID AS ProdutoID, Estados.RecursoAbreviado AS Estado, ' +
				'Estados.RecursoID AS EstadoID, Itens.Quantidade AS Estoque, 0 AS Quantidade, dbo.fn_Produto_TempoProducao(Produtos.ConceitoID, NULL) AS TempoProducao ';

        strSQLFrom = 'FROM Pedidos Pedidos WITH(NOLOCK), Pedidos_Itens Itens WITH(NOLOCK), RelacoesPesCon ProdutosEmpresa WITH(NOLOCK), Conceitos Produtos WITH(NOLOCK), Recursos Estados WITH(NOLOCK) ';
		
		strSQLWhere = 'WHERE Pedidos.PedidoID = ' + nPedidoReferenciaID + ' AND Pedidos.PedidoID = Itens.PedidoID AND ' +
				'Itens.ProdutoID=ProdutosEmpresa.ObjetoID AND ProdutosEmpresa.SujeitoID = ' + glb_nEmpresaID + ' AND ' +
				'ProdutosEmpresa.TipoRelacaoID=61 AND Itens.ProdutoID=Produtos.ConceitoID ' +
				'AND ProdutosEmpresa.EstadoID=Estados.RecursoID ' +
				'AND Itens.PedidoID<>' + glb_nPedidoID + ' ' +
			'ORDER BY ProdutoID, Produto';

    }
    setConnection(dsoGen01);

    dsoGen01.SQL = strSQLSelect + strSQLFrom + strSQLWhere;
    dsoGen01.ondatasetcomplete = startPesq_DSC;
    try
    {
        dsoGen01.Refresh();
    }
    catch(e)
    {
        if ( window.top.overflyGen.Alert('Filtro Inv�lido.') == 0 )
            return null;
            
        lockControlsInModalWin(false);
        
        fg.Rows = 1;
        btnOK.disabled = true;
        
        if (txtPedidoReferencia.disabled == false)
        {
            window.focus();
            txtPedidoReferencia.focus();
        }    
        return null;
    }
}

function startPesq_DSC()
{
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    headerGrid(fg,['Produto',
                   'ID',
                   'EstadoID',
                   'Est',
                   'Estoque',
                   'Quant',
				   'TempoProducao'], [2, 6]);

    fillGridMask(fg,dsoGen01,['Produto*',
                              'ProdutoID*',
                              'EstadoID',
                              'Estado*',
                              'Estoque*',
                              'Quantidade',
                              'TempoProducao'],
                              ['','','','','','999999',''],
                              ['','','','','','######','']);

    alignColsInGrid(fg,[1,4,5]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.FrozenCols = 2;

    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = 5;
    
    if (fg.Rows > 1)
        fg.Editable = true;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    
    
    // se tem linhas no grid, coloca foco no grid, caso contrario
    // coloca foco no txtPalavraChave
    if (fg.Rows > 1)
            fg.focus();
    else
    {
        if (txtPalavraChave.currentStyle.visibility != 'hidden')
            if ( !txtPalavraChave.disabled )
                txtPalavraChave.focus();
        else
        {
            if ( !txtPedidoReferencia.disabled )
                txtPedidoReferencia.focus();
        }        
    }            
}

function saveItens(aData, aDataLen, aDataElemLen)
{
    var strPars = new String();
    var i, j;

    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'EstadoID' + '\'' + ').value');

    strPars = '?nOPID=' + escape(glb_nOPID);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&nDataLen=' + escape(aDataLen);
    strPars += '&nDataElemLen=' + escape(aDataElemLen);

    for (i=0; i<aDataLen; i++)
    {
        for (j=0; j<aDataElemLen; j++)
            strPars += '&aData' + j.toString() + '=' + escape(aData[i][j]);
    }

    // pagina asp no servidor saveitemspedido.asp
    dsoGen03.URL = SYS_ASPURLROOT + '/modindustrial/submodproducao/ordensproducao/serverside/saveitems.asp' + strPars;
    dsoGen03.ondatasetcomplete = saveItens_DSC;
    dsoGen03.refresh();
}

function saveItens_DSC()
{
    var i;
    
    if ( !(dsoGen03.recordset.BOF && dsoGen03.recordset.EOF) )
    {
        // Outro usuario mudou o estadoID do pedido para diferente de cotacao
        if (dsoGen03.recordset('fldErrorNumber').Value > 50000)
        {
            if ( window.top.overflyGen.Alert (dsoGen03.recordset('fldErrorText').Value) == 0 )
                return null;
                
            lockControlsInModalWin(false);       
            window.focus();
            fg.focus();    
            return null;
        }
    }
    
    // pedido foi alterado
    glb_pedidoAlterado = true;
    
    for ( i=1;i<fg.Rows; i++ )
        fg.TextMatrix(i, getColIndexByColKey(fg, 'Quantidade')) = '';

    lockControlsInModalWin(false);       
    window.focus();
    fg.focus();
}
