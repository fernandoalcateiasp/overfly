/********************************************************************
modalchecklist.js

Library javascript para o modalchecklist.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se as colunas OK e Observacoes devem ser readOnly e funcao
// do Estado da OP

var glb_sLockFields = '';

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalchecklist_AfterEdit(Row, Col)
{
    if (fg.Editable)
    {
        if (fg.col == 5)
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalchecklist_BeforeEdit(grid, row, col)
{
    var fldDet = get_FieldDetails(dsoGen01, grid.ColKey(col));
    
    // limita qtd de caracteres digitados em uma celula do grid
    if ( fldDet == null )   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ( (fldDet[0] == 200) || (fldDet[0] == 129) )  // varchar ou char
        grid.EditMaxLength = fldDet[1];
    else    
        grid.EditMaxLength = 0;
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

	if ( glb_nEstadoID != 72 )
		glb_sLockFields = '*';

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modalchecklistBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
	else if (ctl.id == btnReset.id )
	{
		if (fg.Row > 0)
			setObsColumn(fg.Row);
		else
			lockControlsInModalWin(false);
	}	
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Checklist', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        // backgroundColor = 'transparent';
        backgroundColor = 'red';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        // top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    btnOK.value = 'Gravar';
    btnReset.style.height = parseInt(btnOK.style.height);
    btnReset.style.width = parseInt(btnOK.style.width);

	btnOK.style.left = (parseInt(document.getElementById('divFG').currentStyle.width,10) /2) -
					   (parseInt(btnOK.style.width, 10) / 2);
					   
	btnReset.style.top = parseInt(btnOK.style.top,10);
	btnReset.style.left = parseInt(btnOK.style.left,10) - ELEM_GAP - parseInt(btnReset.style.width, 10);
	
	btnCanc.style.left = parseInt(btnOK.style.left,10) + ELEM_GAP + parseInt(btnOK.style.width, 10);
					   
					   
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;

    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();

    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    // 1. O usuario clicou o botao OK
    //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , null );

    var i, j, nNumeroItens;
    var aDataToSave = new Array();
    var sObs = '';
    nNumeroItens = 0;
    j = 0;

    // Verifica coluna de quantidades do grid
    for ( i=1; i<fg.Rows; i++)
    {
        nNumeroItens++;
		nVariacaoItem = 0;
		sObs = trimStr(getCellValueByColKey(fg, 'Observacao' + glb_sLockFields, i));
		sObs = (sObs == '' ? 'NULL' : '\'' + sObs + '\'');

        aDataToSave[j] = new Array(getCellValueByColKey(fg, 'OrdItemID', i),
                                   (getCellValueByColKey(fg, 'OK' + glb_sLockFields, i) == '0' ? '0' : '1'),
                                   sObs);
        j++;
    }

    saveItens(aDataToSave, aDataToSave.length, aDataToSave[0].length);
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalchecklistBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    fg.focus();
}

function startPesq()
{
    lockControlsInModalWin(true);
    
    strSQL = 'SELECT a.OrdItemID, dbo.fn_OrdemProducaoItem_Ordem(a.OrdItemID) AS Ordem, ' +
			'b.ConceitoID, b.Conceito, a.OK, a.Observacao AS Observacao, c.Observacao AS ObservacaoDefault ' +
		'FROM OrdensProducao_Itens a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
		'WHERE (a.OrdemProducaoID = ' + glb_nOPID + ' AND a.TipoMaterialID IN(761, 763) AND ' +
			'a.ProdutoID = b.ConceitoID AND b.ProdutoID = c.ConceitoID) ' +
		'ORDER BY dbo.fn_OrdemProducaoItem_Ordem(a.OrdItemID) ';

    setConnection(dsoGen01);

    dsoGen01.SQL = strSQL;
    dsoGen01.ondatasetcomplete = startPesq_DSC;
    dsoGen01.Refresh();
}

function startPesq_DSC()
{
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    headerGrid(fg,['Ordem',
                   'ID',
                   'Produto',
                   'OK',
                   'Observação' ,
                   'ObservacaoDefault',
                   'OrdItemID'], [5,6]);

    fillGridMask(fg,dsoGen01,['Ordem*',
							  'ConceitoID*',
							  'Conceito*',
							  'OK' + glb_sLockFields,
							  'Observacao' + glb_sLockFields,
							  'ObservacaoDefault',
							  'OrdItemID'],
                              ['999','9999999999','','','','',''],
                              ['###','##########','','','','','']);

    alignColsInGrid(fg,[0,1]);
    
    if ( glb_nEstadoID == 72 )
		setObsColumn();

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.Redraw = 2;
    
    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if ( fg.Rows > 1 )
        fg.Col = 3;
    
    if ( (fg.Rows > 1) && (glb_sLockFields == '') )
        fg.Editable = true;
    else    
		fg.Editable = false;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
        btnOK.disabled = false;
    else
        btnOK.disabled = true;    

    if ( glb_nEstadoID != 72 )
    {
		btnOK.disabled = true;
		btnReset.disabled = true;
    }
    
	fg.focus();
}

function saveItens(aData, aDataLen, aDataElemLen)
{
    var strPars = new String();
    var i, j;

    strPars = '?nOPID=' + escape(glb_nOPID);
    strPars += '&nDataLen=' + escape(aDataLen);
    strPars += '&nDataElemLen=' + escape(aDataElemLen);

    for (i=0; i<aDataLen; i++)
    {
        for (j=0; j<aDataElemLen; j++)
            strPars += '&aData' + j.toString() + '=' + escape(aData[i][j]);
    }

    // pagina asp no servidor saveitemspedido.asp
    dsoGen03.URL = SYS_ASPURLROOT + '/modindustrial/submodproducao/ordensproducao/serverside/savechecklist.asp' + strPars;
    dsoGen03.ondatasetcomplete = saveItens_DSC;
    dsoGen03.refresh();
}

function saveItens_DSC()
{
    var i;
    
    if ( !(dsoGen03.recordset.BOF && dsoGen03.recordset.EOF) )
    {
        // Outro usuario mudou o estadoID do pedido para diferente de cotacao
        if (dsoGen03.recordset('fldErrorNumber').Value > 50000)
        {
            if ( window.top.overflyGen.Alert (dsoGen03.recordset('fldErrorText').Value) == 0 )
                return null;
                
            lockControlsInModalWin(false);       
            window.focus();
            fg.focus();    
            return null;
        }
    }
    
    lockControlsInModalWin(false);       
    window.focus();
    fg.focus();
}

function setObsColumn(nRow)
{
	if (fg.Rows <= 1)
		return null;

	if (nRow != null)
		if (nRow >= fg.Rows)
			return null;

	var i;

	for (i=(nRow == null ? 1 : nRow); i<fg.Rows; i++)
	{
		dsoGen01.recordset.filter = 'OrdItemID = ' + getCellValueByColKey(fg, 'OrdItemID', i);

		if (! ((dsoGen01.recordset.BOF)&&(dsoGen01.recordset.EOF)) )
		{
			dsoGen01.recordset.MoveFirst();

			if ( (nRow != null) || ((dsoGen01.recordset('Observacao').value == null) ||
				 (dsoGen01.recordset('Observacao').value == '')) )
			{
				if (dsoGen01.recordset('ObservacaoDefault').value != null)
					fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao' + glb_sLockFields)) = dsoGen01.recordset('ObservacaoDefault').value;
				else
					fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao' + glb_sLockFields)) = '';
			}	
			else
			{
				if (dsoGen01.recordset('Observacao').value != null)
					fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao' + glb_sLockFields)) = dsoGen01.recordset('Observacao').value;
				else
					fg.TextMatrix(i, getColIndexByColKey(fg, 'Observacao' + glb_sLockFields)) = '';
			}	
		}
		
		dsoGen01.recordset.filter = '';
		
		if (nRow != null)
			break;
	}
	
	lockControlsInModalWin(false);
}

