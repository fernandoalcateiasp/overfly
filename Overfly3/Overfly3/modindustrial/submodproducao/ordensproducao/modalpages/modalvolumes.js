/********************************************************************
modalvolumes.js

Library javascript para o modalvolumes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // dados dos campos da interface
	showModalAfterDataLoad();

    // O estado do botao btnOK
    btnOK_Status();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null );
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Volumes', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // Ajusta os divs internos do divVolumes
    setupDivVolumes();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao vertical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
            
    // ajusta o divVolumes
    with (divVolumes.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = widthFree - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP ;
    }
    
    // Por default o botao OK vem travado
    btnOK.disabled = false;
            
    // restringe digitacao em campos numericos
    // e da outras providencias
    setMaskAndLengthToInputs();
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

No divVolumes - habilita se tiver volume
No divTranspData - habilita sempre
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    
    if ( (txtNumeroVolumes.value > 0) )
        btnOKStatus = false;
    
    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs()
{
    var currValue;
    
    with (txtNumeroVolumes)
    {
        maxLength = 4;
        onfocus = selFieldContent;
        onkeypress = verifyNumericEnterNotLinked;
        onkeyup = keyUpTxtNumVolumes;
        onkeydown = keyDownTxtNumVolumes;
        setAttribute('thePrecision', 4, 1);
        setAttribute('theScale', 0, 1);
        setAttribute('minMax', new Array(1, 9999));
        setAttribute('verifyNumPaste', 1);
        value = glb_nNumeroVolumes;
    }
}

/********************************************************************
Usuario alterou o texto do txtNumVolumes
********************************************************************/
function keyUpTxtNumVolumes()
{
    btnOK_Status();
}

/********************************************************************
Usuario apertou a tecla Enter no campo txtNumVolumes
********************************************************************/
function keyDownTxtNumVolumes()
{
    if ( event.keyCode == 13 )
    {
        if (!btnOK.disabled)
            btnOK_Clicked();
    }    
}

/********************************************************************
Usuario alterou o texto do txtFormulario ou txtNotaFiscal
********************************************************************/
function onKeyUp_FormAndNF()
{
    btnOK_Status();
}

/********************************************************************
Usuario tirou foco do txtFormulario ou txtNotaFiscal
********************************************************************/
function onBlur_FormAndNF()
{
    btnOK_Status();
}

/********************************************************************
Configuracao do divVolumes
********************************************************************/
function setupDivVolumes()
{
    adjustElementsInForm([['lblNumeroVolumes','txtNumeroVolumes',5,1]],
                          null,null,true);
                          
    // centraliza horizontal
    var divW = parseInt (divVolumes.currentStyle.width, 10);
    var fldW = parseInt (txtNumeroVolumes.currentStyle.width, 10);
    
    lblNumeroVolumes.style.left = (divW - fldW) / 2;
    txtNumeroVolumes.style.left = (divW - fldW) / 2;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', 
		new Array(glb_nEstadoDeID, glb_nEstadoParaID, txtNumeroVolumes.value) );
	return true;
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalvolumesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
       
	txtNumeroVolumes.focus();
    
    // O estado do botao btnOK
    btnOK_Status();
}
