/********************************************************************
modalpedido.js

Library javascript para o modalpedido.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES:

window_onload()
setupPage()
btn_onclick(ctl)
********************************************************************/

/********************************************************************
// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares desta modal
********************************************************************/

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
	
    setConnection(dsoPesq);                              
            
    dsoPesq.SQL = 'SELECT a.PedidoID, ISNULL(a.Observacoes, SPACE(0)) AS Observacoes, b.NotaFiscal, ' +
					'CONVERT(VARCHAR, b.dtNotaFiscal, ' + DATE_SQL_PARAM + ') AS dtNotaFiscal ' +
                          'FROM Pedidos a WITH(NOLOCK) ' +
                          'LEFT OUTER JOIN NotasFiscais b WITH(NOLOCK) ON (a.NotaFiscalID = b.NotaFiscalID AND b.EstadoID=67) ' +
                          'WHERE (a.PedidoID= ' + glb_nPedidoID + ')';

    dsoPesq.ondatasetcomplete = dsoPesq_DSC;
    dsoPesq.refresh();
}

function dsoPesq_DSC()
{
    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalpedidoBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    if (! txtObservacoesFromGrid.readOnly )
        txtObservacoesFromGrid.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Pedido', 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divObservacoes
    elem = window.document.getElementById('divObservacoes');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width);
        height = 40;
    }

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblNotaFiscal','txtNotaFiscal',10,1,-10,-10],
  							['lbldtNotaFiscal','txtdtNotaFiscal',10,1]], null, null, true);

    elem = document.getElementById('txtObservacoesFromGrid');
    with (elem.style)
    {
        left = 0;
        top = parseInt(txtdtNotaFiscal.currentStyle.top, 10) + 
			parseInt(txtdtNotaFiscal.currentStyle.height, 10) + ELEM_GAP + 6;
        width = MAX_FRAMEWIDTH_OLD - (ELEM_GAP * 7);
        height = 200;
    }

	// ajusta o divObservacoes
    elem = window.document.getElementById('divObservacoes');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = parseInt(txtObservacoesFromGrid.currentStyle.width, 10);    
        temp = parseInt(width);
        height = 40;
    }

    btnOK.disabled = (glb_btnOKState == 'D');
    btnOK.style.visibility = 'hidden';
    
    btnCanc.style.left = (parseInt(divObservacoes.currentStyle.left, 10) + 
                          parseInt(divObservacoes.currentStyle.width, 10) -
                          parseInt(btnCanc.currentStyle.width, 10) ) /2;
	
	txtNotaFiscal.readOnly = true;
	txtdtNotaFiscal.readOnly = true;
    
    txtObservacoesFromGrid.maxLength = dsoPesq.recordset('Observacoes').definedSize;
    txtObservacoesFromGrid.readOnly = btnOK.disabled;
    txtObservacoesFromGrid.value = dsoPesq.recordset('Observacoes').value;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_SUPINF , txtObservacoesFromGrid.value);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_SUPINF, null );

}

// FINAL DE FUNCOES ESPECIFICAS DO FORM *****************************