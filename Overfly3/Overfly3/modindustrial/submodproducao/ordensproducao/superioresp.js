/********************************************************************
superioresp.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=OrdensProducao.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'NULL AS PessoaID, ' +
               'NULL AS PedidoEstadoHint, ' +
               'NULL AS NumeroMicros, ' +
               'NULL AS Sincronizado, ' +
               'NULL AS Ordem ' +
               'FROM OrdensProducao WITH(NOLOCK)  ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY OrdemProducaoID DESC';
    else
        sSQL = 'SELECT *, ' + 
               'CONVERT(VARCHAR, dtOP, '+DATE_SQL_PARAM+') AS V_dtOP, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=OrdensProducao.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               '(SELECT b.RecursoAbreviado ' +
					'FROM Pedidos a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
					'WHERE a.PedidoID=OrdensProducao.PedidoID AND a.EstadoID=b.RecursoID) AS EstadoPedidoID, ' +
               '(SELECT a.PessoaID ' +
					'FROM Pedidos a WITH(NOLOCK) ' +
					'WHERE a.PedidoID=OrdensProducao.PedidoID) AS PessoaID, ' +
               '(SELECT b.Fantasia ' +
					'FROM Pedidos a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
					'WHERE a.PedidoID=OrdensProducao.PedidoID AND a.PessoaID=b.PessoaID) AS Pessoa, ' +
               '(SELECT CONVERT(VARCHAR, a.dtPrevisaoEntrega , ' + DATE_SQL_PARAM + ') + SPACE(1) + ' +
					'CONVERT(VARCHAR, a.dtPrevisaoEntrega, 108) ' +
					'FROM Pedidos a WITH(NOLOCK) ' +
					'WHERE a.PedidoID=OrdensProducao.PedidoID) AS DT_PrevisaoEntrega, ' +
               '(SELECT b.NotaFiscal ' +
					'FROM Pedidos a WITH(NOLOCK), NotasFiscais b WITH(NOLOCK) ' +
					'WHERE a.PedidoID=OrdensProducao.PedidoID AND a.NotaFiscalID=b.NotaFiscalID AND b.EstadoID = 67) AS NotaFiscal, ' +
               '(SELECT CONVERT(VARCHAR, b.dtNotaFiscal , ' + DATE_SQL_PARAM + ') ' +
					'FROM Pedidos a WITH(NOLOCK), NotasFiscais b WITH(NOLOCK) ' +
					'WHERE a.PedidoID=OrdensProducao.PedidoID AND a.NotaFiscalID=b.NotaFiscalID AND b.EstadoID = 67) AS dtNotaFiscal, ' +
               '(SELECT CONVERT(VARCHAR,DATEADD(mm, OrdensProducao.PrazoGarantia ,b.dtNotaFiscal), ' + DATE_SQL_PARAM + ') ' +
					'FROM Pedidos a WITH(NOLOCK), NotasFiscais b WITH(NOLOCK) ' +
					'WHERE a.PedidoID=OrdensProducao.PedidoID AND a.NotaFiscalID=b.NotaFiscalID AND b.EstadoID = 67) AS DT_Garantia, ' +
			   '(SELECT LTRIM(STR(a.RecursoID)) + ' + '\'' + '-' + '\'' + ' + a.RecursoFantasia ' +
					'FROM Recursos a WITH(NOLOCK), Pedidos b WITH(NOLOCK) ' +
					'WHERE b.PedidoID = OrdensProducao.PedidoID AND b.EstadoID = a.RecursoID) AS PedidoEstadoHint, ' +
			   'CONVERT(NUMERIC (9),dbo.fn_Pedido_Totais(OrdensProducao.PedidoID, 7)) AS NumeroMicros, ' +
			   '(SELECT dbo.fn_OrdemProducao_Sincronizada(OrdensProducao.OrdemProducaoID)) AS Sincronizado, ' +
			   '(SELECT COUNT(*) ' +
					'FROM OrdensProducao a WITH(NOLOCK) ' +
					'WHERE (a.PedidoID = OrdensProducao.PedidoID AND a.EstadoID <> 5 AND ' +
						'a.OrdemProducaoID <= OrdensProducao.OrdemProducaoID)) AS Ordem, ' +
			   '(SELECT TOP 1 LEFT(b.Conceito, 6) + SPACE(1) + ISNULL(dbo.fn_Pedido_ProdutoModelo(OrdensProducao.PedidoID, b.ConceitoID), SPACE(0)) ' +
					'FROM OrdensProducao_Itens a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
					'WHERE a.OrdemProducaoID = OrdensProducao.OrdemProducaoID AND a.ProdutoID = b.ConceitoID AND ' +
						'b.ProdutoID = c.ConceitoID AND c.EhMicro = 1) AS Micro ' +
               'FROM OrdensProducao WITH(NOLOCK) WHERE OrdemProducaoID = ' + nID + ' ORDER BY OrdemProducaoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
          '0 AS Prop1, 0 AS Prop2, ' +
          'NULL AS PessoaID, ' +
          'NULL AS PedidoEstadoHint, ' +
          'NULL AS NumeroMicros, ' +
          'NULL AS Sincronizado, ' +
          'NULL AS Ordem ' +
          'FROM OrdensProducao WITH(NOLOCK) WHERE OrdemProducaoID = 0';

    dso.SQL = sql;          
}              
