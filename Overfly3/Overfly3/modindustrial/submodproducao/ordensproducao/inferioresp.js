/********************************************************************
inferioresp.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:	
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
	setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
	fillCmbPastasInCtlBarInf(nTipoRegistroID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao filtroComplete_DSC(emptyFilter) do js_detailinf.js

Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e no retorno chama a funcao de
ondatasetcomplete de acordo com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.ProprietarioID,a.AlternativoID,a.Observacoes FROM OrdensProducao a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.OrdemProducaoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Itens
    else if (folderID == 25001)
    {
        dso.SQL = 'SELECT * ' +
                  'FROM OrdensProducao_Itens a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.OrdemProducaoID = '+ idToFind + ' ' +
                  'ORDER BY dbo.fn_OrdemProducaoItem_Ordem(a.OrdItemID) ';
        return 'dso01Grid_DSC';
    }
    // RNC
    else if (folderID == 25002)
    {
        dso.SQL = 'SELECT * ' +
                  'FROM OrdensProducao_RNC a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.OrdemProducaoID = '+ idToFind + ' ' +
                  'ORDER BY a.dtData ';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao dso01Grid_DSC() do js_detailinf.js

Monta string de select para combo de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Itens
    else if (pastaID == 25001)
    {
		if ( dso.id == 'dsoCmb01Grid_01' )
		{
            dso.SQL = 'SELECT ItemID, ItemAbreviado, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID = 2 AND TipoID = 503) ' +
                      'ORDER BY Ordem';
		}
		else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT DISTINCT b.ConceitoID, b.Conceito ' +
                      'FROM OrdensProducao_Itens a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                      'WHERE (a.OrdemProducaoID = ' + nRegistroID + ' AND a.ProdutoID=b.ConceitoID)';
        }
		else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT DISTINCT b.RecursoID AS EstadoID, b.RecursoAbreviado AS Est ' +
                      'FROM OrdensProducao_Itens a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.OrdemProducaoID = ' + nRegistroID + ' AND a.EstadoID=b.RecursoID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
			if (glb_bNumeroSerie)
			{
				dso.SQL = 'SELECT a.OrdItemID, ' +
						'dbo.fn_OrdemProducaoItem_NumeroSerie(a.OrdItemID) AS NumerosSeries ' +
						'FROM OrdensProducao_Itens a WITH(NOLOCK) ' + 
						'WHERE a.OrdemProducaoID = '+ nRegistroID;

				glb_bNumeroSerie = false;
			}
			else
			{
				dso.SQL = 'SELECT a.OrdItemID, NULL AS NumerosSeries ' +
						'FROM OrdensProducao_Itens a WITH(NOLOCK) ' + 
						'WHERE a.OrdemProducaoID = '+ nRegistroID;
			}
		}			          
        else if ( dso.id == 'dso04GridLkp' )
        {
			dso.SQL = 'SELECT a.OrdItemID, dbo.fn_OrdemProducaoItem_Ordem(a.OrdItemID) AS Ordem ' +
			          'FROM OrdensProducao_Itens a WITH(NOLOCK) ' + 
			          'WHERE a.OrdemProducaoID = '+ nRegistroID;
		}			          
    }
    // RNC
    else if ( pastaID == 25002 )
    {
		if ( dso.id == 'dsoCmb01Grid_01' )
		{
            dso.SQL = 'SELECT 0 AS Ordem, 0 AS ItemID, SPACE(1) AS ItemMasculino UNION ALL ' + 
                      'SELECT Ordem, ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID = 2 AND TipoID = 511) ' +
                      'ORDER BY Ordem';
		}
		else if ( dso.id == 'dsoCmb02Grid_01' )
		{
            dso.SQL = 'SELECT 0 AS Ordem, 0 AS ItemID, SPACE(1) AS ItemMasculino UNION ALL ' + 
					  'SELECT Ordem, ItemID, ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID = 2 AND TipoID = 512) ' +
                      'ORDER BY Ordem';
		}
		else if ( dso.id == 'dsoCmb03Grid_01' )
		{
            dso.SQL = 'SELECT b.ConceitoID, b.Conceito ' +
                      'FROM OrdensProducao_Itens a WITH(NOLOCK), Conceitos b WITH(NOLOCK) ' +
                      'WHERE (a.OrdemProducaoID = ' + nRegistroID + ' AND a.ProdutoID = b.ConceitoID) ' +
                      'ORDER BY b.Conceito';
		}
		else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT a.ProdutoID ' +
                      'FROM OrdensProducao_Itens a WITH(NOLOCK) ' +
                      'WHERE (a.OrdemProducaoID = ' + nRegistroID + ')';
        }
		else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM OrdensProducao_RNC a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.OrdemProducaoID = ' + nRegistroID + ' AND a.ColaboradorID=b.PessoaID)';
        }

    }        
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(25001);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 25001); //Itens

    vPastaName = window.top.getPastaName(25002);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 25002); //RNC

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 25001) // Itens
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[1,4,7], ['OrdemProducaoID', registroID]);
        }    
        else if (folderID == 25002) // RNC
        {
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[1, 2, 3], ['OrdemProducaoID', registroID], [11, 12]);
        }    

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao buildGridFromFolder(folderID) do js_detailinf.js

Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'PI',
                       'Motivo',
                       'LOGID'], [7]);
                       
        // array de celulas com hint
        glb_aCelHint = [[0,5,'Processo j� foi iniciado?'], [0,7,'Tempo de produ��o']];

        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'ProcessoIniciado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Itens
    else if (folderID == 25001)
    {
        headerGrid(fg,['Ordem',
					   'ID',
                       'Est',
                       'Produto',
                       'Quant',
                       'N�mero de S�rie',
                       'Tipo',
					   'TP',
					   'EstadoID',
					   'OK',
					   'Observa��o',
                       'OrdItemID'], [8,11]);

        // array de celulas com hint
        glb_aCelHint = [[0,6,'Tipo de Material'], [0,7,'Tempo de produ��o']];

        fillGridMask(fg,currDSO,['^OrdItemID^dso04GridLkp^OrdItemID^Ordem*',
								 'ProdutoID*',
								 '^EstadoID^dso02GridLkp^EstadoID^Est*',
								 '^ProdutoID^dso01GridLkp^ConceitoID^Conceito*',
								 'Quantidade',
								 '^OrdItemID^dso03GridLkp^OrdItemID^NumerosSeries*',
								 'TipoMaterialID*',
								 'TempoProducao*',
								 'EstadoID',
								 'OK*',
								 'Observacao*',
								 'OrdItemID'],
								 ['','','','','','','','','','','',''],
                                 ['','','','','','','','##0.00','','','','']);

		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[4,'(######)','S'],[7,'(###0.00)','S']]);

        alignColsInGrid(fg,[ 0, 1, 4, 7]);
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // RNC
    else if (folderID == 25002)
    {
        headerGrid(fg,['RNC',
                       'Data',
                       'Tipo de NC',
                       'N�o-Conformidade',
                       'Tipo de Corre��o',
                       'Corre��o',
					   'ID',
					   'Produto',
					   'N�mero de S�rie',
					   'N�mero de S�rie Subst',
					   'Colaborador',
					   'Observa��es',
					   'ColaboradorID',
                       'RNCID'], [11, 12, 13]);

        fillGridMask(fg,currDSO,['OrdRNCID*',
								 'dtData',
								 'TipoNCID',
								 'NaoConformidade',
								 'TipoCorrecaoID',
								 'Correcao',
								 '^ProdutoID^dso01GridLkp^ProdutoID^ProdutoID*',
								 'ProdutoID',
								 'NumeroSerie',
								 'NumeroSerieSubstituto',
								 '^ColaboradorID^dso02GridLkp^PessoaID^Fantasia*',
								 'Observacoes',
								 'ColaboradorID',
								 'OrdRNCID'],
								 ['','99/99/9999 99:99','','','','','','','','','','','',''],
                                 ['',dTFormat + ' hh:mm','','','','','','','','','','','','']);

        alignColsInGrid(fg,[0, 6]);
		fg.FrozenCols = 3;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************
