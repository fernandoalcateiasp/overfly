/********************************************************************
superior.js

Library javascript para o superior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_BtnFromFramWork = null;
var glb_nNewEst = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
	setupPage()
    putSpecialAttributesInControls()
    formFinishLoad()
    prgServerSup(btnClicked)
    finalOfSupCascade(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    
	controlReadOnlyFields(bAlteracao)
	adjustLabelsCombos(bPaging)
	adjustPedidoEstadoHint()
	startDynamicCmbs()
	dsoCmbDynamic_DSC()
	verifyOPInServer(currEstadoID, newEstadoID)
	verifyOPInServer_DSC
********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos

    glb_aStaticCombos = null;

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modindustrial/submodproducao/ordensproducao/inferior.asp',
                              SYS_PAGESURLROOT + '/modindustrial/submodproducao/ordensproducao/pesquisa.asp');
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'OrdemProducaoID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = '';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = '';
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailsup.js

Usar para ajustes graficos da interface.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupPage()
{
    var elem, previousDiv, elemBottom;
    var divGap = ELEM_GAP / 2;
    
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);

    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
  							['lblEstadoID','txtEstadoID',2,1],
							['lblSuspenso','chkSuspenso',3,1],
							['lblProcessoIniciado','chkProcessoIniciado',3,1],
							['lblSincronizado','chkSincronizado',3,1],
							['lbldtOP','txtdtOP',10,1],
							['lblPedidoID','txtPedidoID',10,1],
							['lblEstadoPedidoID','txtEstadoPedidoID',2,1],
							['lblPessoa','txtPessoa',20,1],
							['lblNotaFiscal','txtNotaFiscal',7,1],
							['lbldtNotaFiscal','txtdtNotaFiscal',10,1]], null, null, true);

	adjustElementsInForm([['lbldtPrevisaoEntrega','txtdtPrevisaoEntrega',14,2],
							['lblTempoUtilProducao','txtTempoUtilProducao',6,2],
							['lbldtPrevisaoSistema','txtdtPrevisaoSistema',17,2,-5]], null, null, true);
							
	txtdtPrevisaoEntrega.style.width = parseInt(txtdtPrevisaoEntrega.currentStyle.width, 10) - 2;
							
	adjustElementsInForm([['lblOrdem','txtOrdem',4,3],
							['lblNumeroMicros','txtNumeroMicros',4,3],
							['lblNumeroVolumes','txtNumeroVolumes',5,3],
							['lblPrazoGarantia','txtPrazoGarantia',4,3,-5],
							['lbldtGarantia','txtdtGarantia',10,3],
							['lblObservacao','txtObservacao',30,3]], null, null, true);
							
	divSup01_01.style.height = parseInt(txtObservacao.currentStyle.top, 10) + 
	                           parseInt(txtObservacao.currentStyle.height, 10) - 
	                           parseInt(lblRegistroID.currentStyle.top, 10) + ELEM_GAP;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamado pela funcao windowOnLoad_2ndPart() do js_superior.js

Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    
    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Documentos','Relat�rios', 'Procedimento', 'Iniciar Processo', 'Inf Pedido', 'Detalhar Pedido']);
}

/********************************************************************
Funcao disparada pelo frame work.
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    // chama funcao do programador que inicia o carregamento
    // de dados dos combos dinamicos (selSujeitoID e selObjetoID)
    glb_BtnFromFramWork = btnClicked;
    
    finalOfSupCascade(glb_BtnFromFramWork);
}

/********************************************************************
Funcao disparada ao final da funcao prgServerInf(btnClicked) que e
disparada pelo frame work,

Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    
    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div
    
    // mostra dois botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,1,1]);
    tipsBtnsEspecControlBar('sup', ['Relat�rios', 'Procedimento', 'Iniciar Processo', 'Inf Pedido', 'Detalhar Pedido']);

    adjustLabelsCombos(true);
    adjustPedidoEstadoHint();
    
    if ( btnClicked == 'SUPINCL' )
    {
        // funcao que controla campos read-only
        controlReadOnlyFields(false);
    }
    
    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao especifica do form.
Ajusta o hint do campo txtEstadoPedidoID
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustPedidoEstadoHint()
{
	txtEstadoPedidoID.title = '';

	if (dsoSup01.recordset('PedidoEstadoHint').value != null)
		txtEstadoPedidoID.title = dsoSup01.recordset('PedidoEstadoHint').value;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao selChanged() do js_interfaceex.js

Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
	else
	{
		adjustLabelsCombos(false);
	}    
    
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo evento onClickBtnLupa() do js_detailsup.js

Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var aEmpresa = getCurrEmpresaData();
	var nPedidoID = 0;
	var nEstadoID = 0;
	var sAnchor = '';

	if (controlBar=='SUP')
	{
	    // Documentos
	    if ((controlBar == 'SUP') && (btnClicked == 1)) {
	        __openModalDocumentos(controlBar, dsoSup01.recordset.Fields(0).value);
	    }
	    // usuario clicou botao imprimir
        else if ( btnClicked == 2 )
            openModalPrint();                
		else if (btnClicked == 3)
		{
			nEstadoID = dsoSup01.recordset('EstadoID').value;

			if ( (nEstadoID == 121) || (nEstadoID == 124) || (nEstadoID == 5) )
				sAnchor = '21';		
			else if (nEstadoID == 122)
				sAnchor = '211';		
			else if (nEstadoID == 123)
				sAnchor = '212';		
			else if (nEstadoID == 72)
				sAnchor = '213';		
			else if (nEstadoID == 25)
				sAnchor = '214';		

			window.top.openModalControleDocumento('S', '', 751, null, sAnchor, 'T');
		}
		else if (btnClicked==4)
		{
			lockInterface(true);
    
			var strPars = new String();
			var nOPID = dsoSup01.recordset('OrdemProducaoID').value;
			var nEstadoID = dsoSup01.recordset('EstadoID').value;

			strPars = '?nOPID=' + escape(nOPID);
			strPars += '&nEstadoID=' + escape(nEstadoID);
			strPars += '&nUserID=' + escape(getCurrUserID());

			dsoIniciarProcesso.URL = SYS_ASPURLROOT + '/modindustrial/submodproducao/ordensproducao/serverside/iniciarprocesso.asp' + strPars ;
			dsoIniciarProcesso.ondatasetcomplete = dsoIniciarProcesso_DSC;
			dsoIniciarProcesso.refresh();
		}
		// Detalhar pedido
		else if (btnClicked==5)
		{
			nPedidoID = dsoSup01.recordset('PedidoID').value;

			if ((nPedidoID != null) && (nPedidoID != 0))
			{
				var strPars = new String();
				strPars = '?';

				strPars += ('btnOKState=' + escape('D'));
				strPars += ('&pedidoID=' + escape(nPedidoID));
				strPars += ('&SUPINF=' + escape('S'));

				htmlPath = SYS_PAGESURLROOT + '/modindustrial/submodproducao/ordensproducao/modalpages/modalpedido.asp' + strPars;
				showModalWin(htmlPath, new Array(740,345));
			}
		}
		else if (btnClicked==6)
		{
			nPedidoID = dsoSup01.recordset('PedidoID').value;
			
			if ( (nPedidoID != null) && (nPedidoID != 0) )
				sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(aEmpresa[0], nPedidoID));
		}
	}	
}

/********************************************************************
Funcao do programador, retorno do servidor apos executar a procedure 
	sp_xxxx
           
Parametros: Nenhum

Retorno: Nenhum
       
********************************************************************/
function dsoIniciarProcesso_DSC()
{
	if ( !((dsoIniciarProcesso.recordset.BOF) && (dsoIniciarProcesso.recordset.EOF)) )
	{
		if (dsoIniciarProcesso.recordset('Mensagem').value != null)
		{
            if ( window.top.overflyGen.Alert(dsoIniciarProcesso.recordset('Mensagem').value) ==0 )
                return null;
		}
	}

	lockInterface(false);

	__btn_REFR('sup');
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailsup.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    // Modal de impressao
    if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    else if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPEDIDOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // Preenche o combo de Pessoa
            fillComboPessoa(param2);
            // esta funcao que fecha a janela modal e destrava a interface
            // restoreInterfaceFromModal() foi movida para final das funcoes
            // chamadas pela funcao fillComboPessoa;    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALVOLUMESHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

			verifyOPInServer(param2[0], param2[1], param2[2]);
			
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            stateMachSupExec('CANC');
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editRegister() do js_superior.js

Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    // funcao que controla campos read-only
    controlReadOnlyFields(true);
    
    // Estes campos tem dd/mm/yyyy hh:mm:ss
    txtdtPrevisaoEntrega.maxLength = 19;
    if (getCloneFromOriginal('txtdtPrevisaoEntrega') != null)
    {
		getCloneFromOriginal('txtdtPrevisaoEntrega').maxLength = 19;
	}	

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************

// FUNCOES DO CARRIER ***********************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWORDEMPRODUCAO')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
        
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
            
        window.top.focus();
                
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE FUNCOES DO CARRIER **************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
	if ((currEstadoID == 25) && (newEstadoID == 124))
		getNumeroVolumes(currEstadoID, newEstadoID);
	else
		verifyOPInServer(currEstadoID, newEstadoID);
		
	return true;
}

function getNumeroVolumes(currEstadoID, newEstadoID)
{
	var empresaData = getCurrEmpresaData();

	// esconde a modal de maquina de estado
	showFrameInHtmlTop('frameModal', false);

    var htmlPath;
    var strPars = new String();
    strPars = '?nOPID=' + escape(dsoSup01.recordset('OrdemProducaoID').value);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);
    
    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modindustrial/submodproducao/ordensproducao/modalpages/modalvolumes.asp'+strPars;

    showModalWin(htmlPath, new Array(302,182));
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
}

/********************************************************************
Verificacoes do Lancamento
********************************************************************/
function verifyOPInServer(currEstadoID, newEstadoID, nVolumes)
{
    var nOrdemProducaoID = dsoSup01.recordset('OrdemProducaoID').value;
    var strPars = new String();

    strPars = '?nOrdemProducaoID='+escape(nOrdemProducaoID);
    strPars += '&nEstadoDeID='+escape(currEstadoID);
    strPars += '&nEstadoParaID='+escape(newEstadoID);
    strPars += '&nUsuarioID='+escape(getCurrUserID());
    
    if (nVolumes != null)
    {
		glb_nNewEst = newEstadoID;
		strPars += '&nVolumes=' + escape(nVolumes);
    }

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modindustrial/submodproducao/ordensproducao/serverside/verificacaoordenproducao.asp' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyOPInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Lancamento
********************************************************************/
function verifyOPInServer_DSC()
{
    if (dsoVerificacao.recordset.Fields('Verificacao').Value == null)
    {
		if (glb_nNewEst == 0)
			stateMachSupExec('OK');
        else
        {
			lockInterface(true);
			glb_btnCtlSup = 'SUPEST';
    
			dsoSup01.recordset.Fields('EstadoID').Value = glb_nNewEst;
			glb_nNewEst = 0;
			saveRegister_1stPart();
        }
    }
    else
    {
        if ( window.top.overflyGen.Alert(dsoVerificacao.recordset.Fields('Verificacao').Value) == 0 )
            return null;
        stateMachSupExec('CANC');
    }
}


// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************

// FUNCOES ESPECIFICAS DO FORM **************************************

/********************************************************************
Funcao para uso do programador.
Controla os campos Read Only. 
NAO DELETAR ESTA FUNCAO NEM AS SUAS CHAMADAS!!!, SO REMOVER O CONTEUDO

Parametro:
bAlteracao

Retorno:
nenhum
********************************************************************/
function controlReadOnlyFields(bAlteracao)
{
	if ( bAlteracao == null )
		return null;
		
    chkProcessoIniciado.disabled = true;
    if (getCloneFromOriginal('chkProcessoIniciado') != null)
        getCloneFromOriginal('chkProcessoIniciado').disabled = true;

    chkSincronizado.disabled = true;
    if (getCloneFromOriginal('chkSincronizado') != null)
        getCloneFromOriginal('chkSincronizado').disabled = true;

    txtPedidoID.readOnly = true;
    if (getCloneFromOriginal('txtPedidoID') != null)
        getCloneFromOriginal('txtPedidoID').readOnly = true;

    txtEstadoPedidoID.readOnly = true;
    if (getCloneFromOriginal('txtEstadoPedidoID') != null)
        getCloneFromOriginal('txtEstadoPedidoID').readOnly = true;

    txtPessoa.readOnly = true;
    if (getCloneFromOriginal('txtPessoa') != null)
        getCloneFromOriginal('txtPessoa').readOnly = true;

    txtNotaFiscal.readOnly = true;
    if (getCloneFromOriginal('txtNotaFiscal') != null)
        getCloneFromOriginal('txtNotaFiscal').readOnly = true;
        
	txtdtNotaFiscal.readOnly = true;
    if (getCloneFromOriginal('txtdtNotaFiscal') != null)
        getCloneFromOriginal('txtdtNotaFiscal').readOnly = true;	

    txtdtPrevisaoEntrega.readOnly = true;
    if (getCloneFromOriginal('txtdtPrevisaoEntrega') != null)
        getCloneFromOriginal('txtdtPrevisaoEntrega').readOnly = true;

    txtTempoUtilProducao.readOnly = true;
    if (getCloneFromOriginal('txtTempoUtilProducao') != null)
        getCloneFromOriginal('txtTempoUtilProducao').readOnly = true;

    txtdtPrevisaoSistema.readOnly = true;
    if (getCloneFromOriginal('txtdtPrevisaoSistema') != null)
        getCloneFromOriginal('txtdtPrevisaoSistema').readOnly = true;

    txtNumeroMicros.readOnly = true;
    if (getCloneFromOriginal('txtNumeroMicros') != null)
        getCloneFromOriginal('txtNumeroMicros').readOnly = true;

    txtdtGarantia.readOnly = true;
    if (getCloneFromOriginal('txtdtGarantia') != null)
        getCloneFromOriginal('txtdtGarantia').readOnly = true;

    txtOrdem.readOnly = true;
    if (getCloneFromOriginal('txtOrdem') != null)
        getCloneFromOriginal('txtOrdem').readOnly = true;

	if (bAlteracao)
	{
		;
	}
	else
	{
		;
	}
}

/********************************************************************
Funcao do programador
Altera labels de combos()
           
Parametros: 
	bPaging : True -> Usuario esta paginando, incluindo ou alterando registro
	bPaging : False -> Usuario alterou o option do combo

Retorno:
nenhum
********************************************************************/
function adjustLabelsCombos(bPaging)
{
    if (bPaging)
    {
		chkSincronizado.checked = (dsoSup01.recordset('Sincronizado').value == true);
		setLabelOfControl(lblPessoa, dsoSup01.recordset('PessoaID').value);
	}
    else
	{
        // setLabelOfControl(lblTipoLancamentoID, selTipoLancamentoID.value);
        ;
	}
}

/********************************************************************
Funcao do programador
Disparada pela funcao prgServerSup()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDynamicCmbs()
{
	;
}

/********************************************************************
Funcao disparada pelo programador.
Funcao do ondatasetcomplete disparada pela funcao prgServerSup()
           
Parametros:     
Nenhum  

Retorno:
nenhum
********************************************************************/
function dsoCmbDynamic_DSC()
{
	;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&currDateFormat=' + escape(DATE_FORMAT);

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modindustrial/submodproducao/ordensproducao/modalpages/modalprint.asp'+strPars;
    
    showModalWin(htmlPath, new Array(346,200));
}
// FINAL DE FUNCOES ESPECIFICAS DO FORM *****************************