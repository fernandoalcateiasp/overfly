/********************************************************************
inferior.js

Library javascript para o inferior.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var __winTimerHandler = null;
var glb_bFocus = null;
var glb_bForceRefrInf = false;
var glb_btnClicked = null;
var glb_bNumeroSerie = false;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
	window_onload()
	setupPage()
	adjustElementsInDivsNonSTD()
	putSpecialAttributesInControls()
	optChangedInCmb(cmb)
	frameIsAboutToApplyRights(folderID)
	prgServerInf(btnClicked)
	finalOfInfCascade(btnClicked)
	prgInterfaceInf(btnClicked, pastaID, dso)
	treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
	btnLupaClicked(btnClicked)
	btnBarClicked(controlBar, btnClicked)
	btnBarNotEspecClicked(controlBar, btnClicked)
	btnAltPressedInGrid( folderID )
	btnAltPressedInNotGrid( folderID )
	addedLineInGrid(folderID, grid, nLineInGrid, dso)
	selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
	fillCmbFiltsInfAutomatic(folderID)
	modalInformForm(idElement, param1, param2)
    
FUNCOES DA MAQUINA DE ESTADO:
	stateMachOpened( currEstadoID )
	stateMachClosed( oldEstadoID, currEstadoID )
    
EVENTOS DO GRID FG
	fg_AfterRowColChange_Prg()
	fg_DblClick_Prg()
	fg_ChangeEdit_Prg()
	fg_ValidateEdit_Prg()
	fg_BeforeRowColChange_Prg()
	fg_EnterCell_Prg()
	fg_MouseUp_Prg()
	fg_MouseDown_Prg()
	fg_BeforeEdit_Prg()
	
FUNCOES DO PROGRAMADOR (PARTICULARES DO FORM):	
	    
********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Evento disparado ao final do carregamento da pagina.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function window_onload()
{
    // Ze em 17/03/08
    dealWithGrid_Load();
    
    // Funcoes complementares da automacao
    windowOnLoad_1stPart();

    //@@ Combos estaticos. Array dos ids dos dsos que tenham campos linkados
    // a controles HTMLs.
    // Estes controles HTMls estao em divs e sao linkados a campos do dso
    // que sejam: obrigatorios, numericos ou de datas, nos dsos.
    // Se nao aplicavel ao form este array deve ser null
    glb_aDsosToCloneFieldsEtc = ['dso01JoinSup'];
    
    //@@ Combos estaticos. Array bidimensional de combos ids e campo indice
    // na tabela, identificador do combo.
    // O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = null;
    
    //@@ Combos estaticos em grids.
    // Array bidimensional da pasta id e array bidimensional
    // do indice das colunas
    // (a contar de zero), id do dso dos dados do combo,
    // nome do campo da tabela visivel no combo e
    // nome do campo do grid que linka o grid com o combo.
    // O array deve ser null se nenhum grid tem combos estaticos
    // Notacao para combo multicoluna:
    // [ [20012, [[1, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino','ItemID']]] ] 

    glb_aGridStaticCombos = [ [20010, [[0, 'dso01GridLkp'   , ''             , ''],
                                       [0, 'dso02GridLkp'   , ''             , ''],
                                       [0, 'dso03GridLkp'   , ''             , ''],
                                       [0, 'dso04GridLkp'   , ''             , '']]],
							  [25001, [[6, 'dsoCmb01Grid_01', '*ItemAbreviado|ItemMasculino', 'ItemID'],
									   [0, 'dso01GridLkp'   , ''             , ''],
									   [0, 'dso02GridLkp'   , ''             , ''],
									   [0, 'dso03GridLkp'   , ''             , ''],
									   [0, 'dso04GridLkp'   , ''             , '']]],
							  [25002, [[2, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID'],
									   [4, 'dsoCmb02Grid_01', 'ItemMasculino', 'ItemID'],
									   [7, 'dsoCmb03Grid_01', '*Conceito|ConceitoID', 'ConceitoID'],
									   [0, 'dso01GridLkp'   , ''             , ''],
									   [0, 'dso02GridLkp'   , ''             , '']]]	    ];


    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
        
    linkDivsAndSubForms(null, ['divInf01_01',
                               'divInf01_02',
                               'divInf01_03'],
                               [20008, 20009, [20010, 25001, 25002]] );

    // Funcoes complementares da automacao
    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailinf.js

Usar para ajustes graficos da interface.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupPage()
{
    //@@ Ajustar os divs
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao after_SetupPage() do js_detailinf.js

Ajusta graficamente elementos de divs que nao estao no padrao do
framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustElementsInDivsNonSTD()
{
    //@@
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao windowOnLoad_2ndPart() do js_detailinf.js

Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Usar para definir a pasta default pela variavel da automacao
glb_pastaDefault

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    glb_pastaDefault = 25001;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao selChanged() do js_interfaceex.js

Esta funcao recebe todos os onchange de combos do inf e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
cmb - referencia ao combo que disparou o evento

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@
    
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao finalDSOsCascade() do js_detailinf.js

O frame work esta prestes a setar os botoes da barra inferior, em funcao
dos direitos do usuario logado

Parametros: 
folderID    - pasta corrente

Retorno:
indiferente para a automacao
********************************************************************/
function frameIsAboutToApplyRights(folderID)
{
	var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset(' + '\'' + 'EstadoID' + '\'' + ').value');

    // Pasta de LOG Read Only
    if ( folderID == 20010 )
    {
        // trava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    // Itens
    else if ( folderID == 25001 )
    {
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    else
    {
        // destrava as barras do inf para a pasta acima definida
        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailinf.js

Esta funcao faz a chamada de todas as operacoes particulares de servidor
e preenchimentos de combos do inf. Esta funcao e chamada quando
o usuario clica um botao da barra do sup ou inf.
           
Parametros: 
btnClicked    - ultimo botao clicado na barra (superior ou inferior)

Retorno:
nenhum
********************************************************************/
function prgServerInf(btnClicked)
{
    //@@
    // Chamar aqui funcoes particulares do form que forem necessarias
    // antes de trocar o div

	glb_btnClicked = btnClicked;
	
	dsoCurrData.URL = SYS_ASPURLROOT + '/serversidegen/currdate.asp?currDateFormat=' + escape(DATE_FORMAT);
	dsoCurrData.ondatasetcomplete = dsoCurrDataComplete_DSC;
	dsoCurrData.refresh();
}

function dsoCurrDataComplete_DSC()
{
	// Se houver operacoes de banco de dados particulares do form,
	// mover esta funcao para os finais retornos destas operacoes no
	// banco de dados
	finalOfInfCascade(glb_btnClicked);
}

/********************************************************************
Funcao disparada ao final da funcao prgServerInf(btnClicked) que e
disparada pelo frame work,

Esta funcao tem que ser chamada por todas as funcoes de retorno de
dados do servidor.
Se o botao clicado e da barra superior volta mensagem para o superior
efetuar troca de interface.
Se o botao clicado e da barra inferior volta mensagem para o inferior
efetuar troca de interface.           
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfInfCascade(btnClicked)
{
    //@@

    // Nao mexer - Inicio de automacao ==============================
    if ( (btnClicked.toUpperCase()).indexOf('SUP') >= 0 )
        // usario clicou botao no control bar sup
	    sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
	else
	    // usario clicou botao no control bar inf
	    sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
	    
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailinf.js

Esta funcao faz a chamada de todas as operacoes particulares
do inf, necessarias apos um changeDiv.
           
Parametros: 
btnClicked  - ultimo botao clicado na barra de botoes inferior
pastaID     - id da pasta corrente
dso         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    
    // Pasta de Itens
	if ( pastaID == 25001 )
    {
        showBtnsEspecControlBar('inf', true, [1,1,1,1,1]);
        tipsBtnsEspecControlBar('inf', ['Incluir �tens','Incluir NS','Visualizar NS', 'Check List', 'Sincronizar OP com o pedido']);
    }
    // Pasta de RNC
	else if ( pastaID == 25002 )
    {
        showBtnsEspecControlBar('inf', true, [1,1,0,0]);
        tipsBtnsEspecControlBar('inf', ['Observa��es','Procedimento','','']);
    }

    // Ao final das operacoes particulares da interface do inf
    // manda mensagem para a biblioteca do inf tratar os controls
    // bars superior e inferior
    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Tratar os controls bar sup e inf aqui, apos o final de uma operacao
de interface e dados do form.
           
Parametros: 
btnClicked      - ultimo control bar (sup/inf) e botao clicado
folderID        - id da pasta corrente
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
dsoID           - id do dso corrente

Retorno:
nenhum
********************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID)
{
    if ( (btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') 
          || (btnClicked == 'SUPOK') || (btnClicked == 'INFREFR')
          || (btnClicked == 'INFOK') || (btnClicked == 'INFCANC')
          || (btnClicked == 'SUPEST') )
    {      
        setupEspecBtnsControlBar('sup', 'HHHHHH');
    }    
    else    
        setupEspecBtnsControlBar('sup', 'DDDDDD');
        
    if ( glb_bForceRefrInf == true )    
    {
        glb_bForceRefrInf = false;
        __btn_REFR('inf');
    }

    // nao altera Estado se o Pedido esta Suspenso
	var bSuspenso = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset(' + '\'' + 'Suspenso' + '\'' + ').value');
	var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'dsoSup01.recordset(' + '\'' + 'EstadoID' + '\'' + ').value');

	if (bSuspenso)
	{
	    var strBtns = currentBtnsCtrlBarString('sup');
        var aStrBtns = strBtns.split('');
        aStrBtns[4] = 'D';
        strBtns = aStrBtns.toString();
        re = /,/g;
        strBtns = strBtns.replace(re, '');
        adjustControlBarEx(window,'sup', strBtns);
    }

	// Itens
	if (folderID == 25001)
	{
		if (fg.Rows > 1)
		{
			if ((nEstadoID == 121) || (nEstadoID == 122))
				setupEspecBtnsControlBar('inf', 'HHDDH');
			else if ((nEstadoID == 123) || (nEstadoID == 72) || (nEstadoID == 25))
				setupEspecBtnsControlBar('inf', 'DHHHD');
			else if (nEstadoID == 124)
				setupEspecBtnsControlBar('inf', 'DDHHD');	
		}	
		else
			setupEspecBtnsControlBar('inf', 'DDDDD');
	}
	// RNC
	else if (folderID == 25002)
	{
		if (fg.Rows > 1)
			setupEspecBtnsControlBar('inf', 'HHDDD');
		else
			setupEspecBtnsControlBar('inf', 'DHDDD');
	}
	else
		setupEspecBtnsControlBar('inf', 'DDDDD');

    // Itens
    if ( folderID == 25001 )
    {
        if (glb_bFocus != null)
        {
            glb_bFocus = null;
            window.focus();
            fg.focus();
        }
    }
		
}

/********************************************************************
Funcao disparada pelo frame work.

Chamada pelo evento onClickBtnLupa() do js_detailinf.js

Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var empresa = getCurrEmpresaData();
	
	if (controlBar == 'INF')
	{
		// Itens
		if (keepCurrFolder() == 25001)
		{
			if (btnClicked==1)
			{
				openModalIncluiItens();
			}	
			else if (btnClicked==2)
			{
				openVerModalNumSerie();
			}
			else if (btnClicked==3)
			{
				openModalVisualizaNumSerie();
			}
			else if (btnClicked==4)
			{
				openModalCheckList();
			}
			else if (btnClicked==5)
			{
				sincronizaOP();
			}
		}
		// RNC
		else if (keepCurrFolder() == 25002)
		{
			if (btnClicked==1)
			{
				openModalObs();
			}	
			else if (btnClicked==2)
			{
				window.top.openModalControleDocumento('I', '', 830, null, null, 'T');
			}	
		}
	}
}

/********************************************************************
Funcao criada pelo programador.
Abre modal para pesquisa inclusao de itens no grid de itens

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalIncluiItens()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'PedidoID' + '\'' + ').value');

    var nOPID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'OrdemProducaoID' + '\'' + ').value');

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPedidoID=' + escape(nPedidoID);
    strPars += '&nOPID=' + escape(nOPID);
    
    // carregar modal - nao faz operacao de banco no carregamento           
    htmlPath = SYS_PAGESURLROOT + '/modindustrial/submodproducao/ordensproducao/modalpages/modalincluiitens.asp' + strPars;
    //showModalWin(htmlPath, new Array(680 - 15,389 + 3 * 17));
    showModalWin(htmlPath, new Array(450,400));
}

/********************************************************************
Funcao criada pelo programador.
Abre modal para pesquisa inclusao de itens no grid de itens

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCheckList()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'EstadoID' + '\'' + ').value');

    var nOPID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'OrdemProducaoID' + '\'' + ').value');

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&nOPID=' + escape(nOPID);
    
    // carregar modal - nao faz operacao de banco no carregamento           
    htmlPath = SYS_PAGESURLROOT + '/modindustrial/submodproducao/ordensproducao/modalpages/modalchecklist.asp' + strPars;
    showModalWin(htmlPath, new Array(620,400));
}

/********************************************************************
Funcao criada pelo programador.
Abre modal para pesquisa inclusao de itens no grid de itens

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalVisualizaNumSerie()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var nEmpresaID = empresaData[0];

    var nProdutoID = getCellValueByColKey(fg, 'ProdutoID*', fg.Row);

    var nOPID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'OrdemProducaoID' + '\'' + ').value');

    // mandar os parametros para o servidor
    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nProdutoID=' + escape(nProdutoID);
    strPars += '&nOPID=' + escape(nOPID);
    
    // carregar modal - nao faz operacao de banco no carregamento           
    htmlPath = SYS_PAGESURLROOT + '/modindustrial/submodproducao/ordensproducao/modalpages/modalvernumserie.asp' + strPars;
    showModalWin(htmlPath, new Array(555,450));
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diversos pontos do js_detailinf.js

Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
	// Itens
	if (keepCurrFolder() == 25001)
	{
		if (btnClicked == 'INFREFR')
		{
			glb_bNumeroSerie = true;
		}
		if ((btnClicked == 'INFEXCL') || (btnClicked == 'INFALT'))
		{
		    var TipoMaterialID = getCellValueByColKey(fg, 'TipoMaterialID*', fg.Row);

			if (TipoMaterialID == 761)
			{
				var sMsg = 'Este registro n�o pode ser ' + (btnClicked == 'INFALT' ? 'alterado.' : 'excluido.');

				if ( window.top.overflyGen.Alert(sMsg) == 0 )
				    return null;

				return true;
			}
		}
	}
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editCurrFolder() do js_detailinf.js

Usuario clicou o botao de alteracao em uma pasta de grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInGrid( folderID )
{
	if (folderID == 25001)
		setupEspecBtnsControlBar('inf', 'DDDD');
	else if (folderID == 25002)
		setupEspecBtnsControlBar('inf', 'HDDD');
	else		
		setupEspecBtnsControlBar('inf', 'DDDD');
		
    // Pasta de Itens
    if ( folderID == 25001 )
    {
        // Merge de Colunas
        fg.MergeCells = 0;
    }
		
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao editCurrFolder() do js_detailinf.js

Usuario clicou o botao de alteracao em uma pasta que nao e grid. 
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function btnAltPressedInNotGrid( folderID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pela funcao addLineInGrid() do js_detailinf.js

Usuario inseriu uma linha nova no grid.
           
Parametros: 
folderID        - id da pasta corrente
grid            - referencia ao grid
nLineInGrid     - numero da linha inserida no grid
dso             - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso)
{
	if (folderID == 25001)
		setupEspecBtnsControlBar('inf', 'DDDD');
	else if (folderID == 25002)
		setupEspecBtnsControlBar('inf', 'HDDD');
	else		
		setupEspecBtnsControlBar('inf', 'DDDD');

    // Pasta de Itens
    if ( folderID == 25001 )
    {
        // Merge de Colunas
        fg.MergeCells = 0;
    }

	// Pasta RNC
	if ( folderID == 25002 )
	{
		var nUsuarioID = getCurrUserID();

		fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'ColaboradorID')) = nUsuarioID;
		fg.Col = 0;
		fg.Col = getColIndexByColKey(fg, 'dtData');

		if ( !(dsoCurrData.recordset.BOF && dsoCurrData.recordset.EOF) )
			fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'dtData')) = dsoCurrData.recordset('currDate').value + ' 00:00';
		
		fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'TipoCorrecaoID')) = 775;
		
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo js_detailinf.js

Usuario selecionou uma pasta no control bar inferior.
Esta funcao executa antes de qualquer operacao do frame work           

Parametros: 
optText             - texto do option do combo
optValue            - value do option do combo
optIndex            - indice do option do combo
nQtyGridsInFolder   - 0 nao tem grid, 1 ou 2

Retorno:
qualquer coisa diferente de null para a automacao
********************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder)
{
    //@@ retornar null para prosseguir a automacao
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em dois pontos do js_detailinf.js

Usuario selecionou uma pasta no control bar inferior.
Nesta funcao o programador preenche o combo de filtros inferiores com 
dados particulares do form ou deixa o frame work preencher o combo de
filtros inferiores com os dados dos filtros inferiores.
           
Parametros: 
folderID    - id da pasta corrente

Retorno:
true    - o frame work opera automaticamente
false   - o programador preenche o combo manualmente
********************************************************************/
function fillCmbFiltsInfAutomatic(folderID)
{
    //@@ Retornar true se nao ha preenchimento manual.
    // Retornar false se ha.
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada em diveros pontos do js_detailinf.js

Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALNUMSERIEHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        // Botao nao ativo, funcao aqui apenas para compatibilidade
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            
            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALVERNUMSERIEHTML')
    {
        // Botao nao ativo, funcao aqui apenas para compatibilidade
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();

            window.focus();
            fg.focus();

            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALINCLUIITENSHTML')
    {
        // Botao nao ativo, funcao aqui apenas para compatibilidade
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
                        
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        // Botao ativo
        // se param2 == true, refrescar o grid de itens
        // pois o pedido foi alterado
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            
            window.focus();
            fg.focus();
            
            if ( param2 )
                __winTimerHandler = window.setInterval('refreshSupAfterModalClose(true)', 10, 'JavaScript');
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    
    // RNC - observacoes
    if ( keepCurrFolder() == 25002 && (idElement.toUpperCase() == 'MODALOBSERVACAOHTML') )
    {
        if ( param1 == 'OK' )
        {
            // escreve observacoes no grid
            fg.Redraw = 0;
            // Escreve no campo
            fg.TextMatrix(keepCurrLineInGrid(), getColIndexByColKey(fg, 'Observacoes')) = param2;
            fg.Redraw = 2;
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

    // Itens - Checklist
    if ( idElement.toUpperCase() == 'MODALCHECKLISTHTML' )
    {
        if ( param1 == 'OK' )
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            
            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            
            __winTimerHandler = window.setInterval('refreshAfterModalClose()', 10, 'JavaScript');
            
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }

}

/********************************************************************
Funcao criada pelo programador.
Executa um Refresh no SUP e/ou INF
           
Parametros:     
bRefreshInf - false- executa Refresh no SUP
            - true - executa refresh no SUP e no INF  

Retorno:
nenhum
********************************************************************/
function refreshSupAfterModalClose(bRefreshInf)
{
    if ( __winTimerHandler != null )
    {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }
        
    glb_bFocus = true;

    if (bRefreshInf)    
    {
        glb_bForceRefrInf = true;
    }
    
    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,
                                          '__btn_REFR(\'sup\')');
}                                      

// FINAL DE FUNCOES DA AUTOMACAO ************************************

// EVENTOS DO GRID FG ***********************************************
// Nota - sao disparados pelo frame work apos suas operacoes

function fg_AfterRowColChange_Prg()
{
}

function fg_DblClick_Prg()
{
}

function fg_ChangeEdit_Prg()
{
}

function fg_ValidateEdit_Prg()
{
}

function fg_BeforeRowColChange_Prg()
{
}

function fg_EnterCell_Prg()
{
}

function fg_MouseUp_Prg()
{
}

function fg_MouseDown_Prg()
{
}

function fg_BeforeEdit_Prg()
{
}

// FINAL DE EVENTOS DO GRID FG **************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelo loop de mensagens wndJSProc(idElement, msg, param1, param2)
do js_detailinf.js

Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{
}

/********************************************************************
Funcao disparada pelo frame work.
Chamada pelas funcoes submitChanges_DSC() e submitChangesParallel_DSC()
do js_detailinf.js

Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID )
{
}

// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de entrada de numero de serie

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openVerModalNumSerie()
{
    var htmlPath;
    var strPars = new String();

    var nPedidoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'PedidoID' + '\'' + ').value');
        
    var nOrdemProducaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'OrdemProducaoID' + '\'' + ').value');

    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  'dsoSup01.recordset(' + '\'' + 'EstadoID' + '\'' + ').value');

    // mandar os parametros para o servidor
    strPars = '?nPedidoID=' + escape(nPedidoID);
    strPars += '&nOrdemProducaoID=' + escape(nOrdemProducaoID);
    strPars += '&nEstadoID=' + escape(nEstadoID);
    strPars += '&sDATE_SQL_PARAM=' + escape(DATE_SQL_PARAM);
    
    strPars += '&nCurrUserID=' + escape(getCurrUserID());
    
    // carregar modal - nao faz operacao de banco no carregamento              
    htmlPath = SYS_PAGESURLROOT + '/modcomercial/submodpedidos/frmpedidos/modalpages/modalnumserie.asp'+strPars;
    showModalWin(htmlPath, new Array(780,460));
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de observacoes do grid RNC

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalObs()
{
	startObsWindow_inf( fg, dso01Grid, 'Observacoes', getColIndexByColKey(fg, 'Observacoes') );
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de entrada de numero de serie

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function sincronizaOP()
{
	lockInterface(true);
    
	var strPars = new String();
	var nOrdemProducaoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              'dsoSup01.recordset(' + '\'' + 'OrdemProducaoID' + '\'' + ').value');

	strPars = '?nOrdemProducaoID=' + escape(nOrdemProducaoID);

	dsoSincronizaOP.URL = SYS_ASPURLROOT + '/modindustrial/submodproducao/ordensproducao/serverside/sincronizaop.asp' + strPars ;
	dsoSincronizaOP.ondatasetcomplete = dsoSincronizaOP_DSC;
	dsoSincronizaOP.refresh();
}

/********************************************************************
Funcao do programador, retorno do servidor apos sincronizar OP
           
Parametros: Nenhum

Retorno: Nenhum
       
********************************************************************/
function dsoSincronizaOP_DSC()
{
	if ( !((dsoSincronizaOP.recordset.BOF) && (dsoSincronizaOP.recordset.EOF)) )
	{
		if (dsoSincronizaOP.recordset('Mensagem').value != null)
		{
            if ( window.top.overflyGen.Alert(dsoSincronizaOP.recordset('Mensagem').value) ==0 )
                return null;
		}
	}

	lockInterface(false);

	__btn_REFR('inf');
}

function refreshAfterModalClose()
{
    if ( __winTimerHandler != null )
    {
        window.clearInterval(__winTimerHandler);
        __winTimerHandler = null;
    }
    
    __btn_REFR('inf');
}

