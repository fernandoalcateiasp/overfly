<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="ordensproducaosup01Html" name="ordensproducaosup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modindustrial/submodproducao/ordensproducao/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modindustrial/submodproducao/ordensproducao/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="ordensproducaosup01Body" name="ordensproducaosup01Body" LANGUAGE="javascript" onload="return window_onload()">
            
    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
		
		<!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
		<p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">OP</p>
		<input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
		<p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
		<input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
		<p  id="lblSuspenso" name="lblSuspenso" class="lblGeneral">S</p>
		<input type="checkbox" id="chkSuspenso" name="chkSuspenso" DATASRC="#dsoSup01" DATAFLD="Suspenso" class="fldGeneral" title="OP Suspensa?"></input>
		<p  id="lblProcessoIniciado" name="lblProcessoIniciado" class="lblGeneral">PI</p>
		<input type="checkbox" id="chkProcessoIniciado" name="chkProcessoIniciado" DATASRC="#dsoSup01" DATAFLD="ProcessoIniciado" class="fldGeneral" title="Processo j� foi iniciado?"></input>
		<p  id="lblSincronizado" name="lblSincronizado" class="lblGeneral">Sinc</p>
		<input type="checkbox" id="chkSincronizado" name="chkSincronizado" class="fldGeneral" title="OP est� sincronizada com o pedido?"></input>
		<p id="lbldtOP" name="lbldtOP" class="lblGeneral">Data</p>
		<input type="text" id="txtdtOP" name="txtdtOP" DATASRC="#dsoSup01" DATAFLD="V_dtOP" class="fldGeneral"></input>
		<p id="lblPedidoID" name="lblPedidoID" class="lblGeneral">Pedido</p>
		<input type="text" id="txtPedidoID" name="txtPedidoID" DATASRC="#dsoSup01" DATAFLD="PedidoID" class="fldGeneral"></input>
		<p id="lblEstadoPedidoID" name="lblEstadoPedidoID" class="lblGeneral">Est</p>
		<input type="text" id="txtEstadoPedidoID" name="txtEstadoPedidoID" DATASRC="#dsoSup01" DATAFLD="EstadoPedidoID" class="fldGeneral"></input>
		<p id="lblPessoa" name="lblPessoa" class="lblGeneral">Pessoa</p>
		<input type="text" id="txtPessoa" name="txtPessoa" DATASRC="#dsoSup01" DATAFLD="Pessoa" class="fldGeneral"></input>
		<p id="lblNotaFiscal" name="lblNotaFiscal" class="lblGeneral">Nota Fiscal</p>
		<input type="text" id="txtNotaFiscal" name="txtNotaFiscal" DATASRC="#dsoSup01" DATAFLD="NotaFiscal" class="fldGeneral"></input>
		<p id="lbldtNotaFiscal" name="lbldtNotaFiscal" class="lblGeneral">Data</p>
		<input type="text" id="txtdtNotaFiscal" name="txtdtNotaFiscal" DATASRC="#dsoSup01" DATAFLD="dtNotaFiscal" class="fldGeneral"></input>
		
		<p id="lbldtPrevisaoEntrega" name="lbldtPrevisaoEntrega" class="lblGeneral">Previs�o Entrega</p>
		<input type="text" id="txtdtPrevisaoEntrega" name="txtdtPrevisaoEntrega" DATASRC="#dsoSup01" DATAFLD="DT_PrevisaoEntrega" class="fldGeneral"></input>
		<p id="lblTempoUtilProducao" name="lblTempoUtilProducao" class="lblGeneral">Tempo Restante</p>
		<input type="text" id="txtTempoUtilProducao" name="txtTempoUtilProducao" DATASRC="#dsoSup01" DATAFLD="" class="fldGeneral"></input>
		<p id="lbldtPrevisaoSistema" name="lbldtPrevisaoSistema" class="lblGeneral">Previs�o Sistema</p>
		<input type="text" id="txtdtPrevisaoSistema" name="txtdtPrevisaoSistema" DATASRC="#dsoSup01" DATAFLD="" class="fldGeneral"></input>
		<p id="lblOrdem" name="lblOrdem" class="lblGeneral">Ordem</p>
		<input type="text" id="txtOrdem" name="txtOrdem" DATASRC="#dsoSup01" DATAFLD="Ordem" class="fldGeneral"></input>
		<p id="lblNumeroMicros" name="lblNumeroMicros" class="lblGeneral">Quant</p>
		<input type="text" id="txtNumeroMicros" name="txtNumeroMicros" DATASRC="#dsoSup01" DATAFLD="NumeroMicros" class="fldGeneral"></input>
		<p id="lblNumeroVolumes" name="lblNumeroVolumes" class="lblGeneral">Volumes</p>
		<input type="text" id="txtNumeroVolumes" name="txtNumeroVolumes" DATASRC="#dsoSup01" DATAFLD="NumeroVolumes" class="fldGeneral"></input>
		<p id="lblPrazoGarantia" name="lblPrazoGarantia" class="lblGeneral">Gar</p>
		<input type="text" id="txtPrazoGarantia" name="txtPrazoGarantia" DATASRC="#dsoSup01" DATAFLD="PrazoGarantia" class="fldGeneral" title="Prazo de Garantia (em meses)"></input>
		<p id="lbldtGarantia" name="lbldtGarantia" class="lblGeneral">Data</p>
		<input type="text" id="txtdtGarantia" name="txtdtGarantia" DATASRC="#dsoSup01" DATAFLD="DT_Garantia" class="fldGeneral"></input>
		<p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
		<input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
    </div>
    
</body>

</html>
