
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Response.ContentType = "text/xml"

Dim rsData
Dim rsNew
Dim nOrdemProducaoID
Dim nEstadoDeID
Dim nEstadoParaID
Dim nUsuarioID
Dim strSQL
Dim nRecsAffected
Dim rsCommand
Dim sResponse
Dim i
Dim nVolumes

  
strSQL = ""
nEstadoDeID = 0
nEstadoParaID = 0
sResponse = ""
nUsuarioID = 0
nVolumes = 0

For i = 1 To Request.QueryString("nOrdemProducaoID").Count    
    nOrdemProducaoID = Request.QueryString("nOrdemProducaoID")(i)
Next

For i = 1 To Request.QueryString("nEstadoDeID").Count    
    nEstadoDeID = Request.QueryString("nEstadoDeID")(i)
Next

For i = 1 To Request.QueryString("nEstadoParaID").Count    
    nEstadoParaID = Request.QueryString("nEstadoParaID")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

For i = 1 To Request.QueryString("nVolumes").Count    
    nVolumes = Request.QueryString("nVolumes")(i)
Next

strSQL = "SELECT dbo.fn_OrdemProducao_Verifica( " & CStr(nOrdemProducaoID) & ", " & _
												CStr(nEstadoDeID) & ", " & _
												CStr(nEstadoParaID) & ", " & _
												CStr(nUsuarioID) & ", " & _
												"GETDATE() ) AS Verificacao "

Set rsData = Server.CreateObject("ADODB.Recordset")
    
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

sResponse = rsData.Fields("Verificacao").Value
    
rsData.Close
Set rsData = Nothing  

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "Verificacao", adVarChar, 8000, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew

If ( Not IsNull(sResponse) ) Then

	rsNew.Fields("Verificacao").Value = CStr(sResponse)

ElseIf (CInt(nVolumes) > 0) Then	
		'Gravacao do itens no pedido
		Set rsCommand = Server.CreateObject("ADODB.Command")

		rsCommand.CommandTimeout = 60 * 10

		strSQL = "UPDATE OrdensProducao SET NumeroVolumes = " & CStr(nVolumes) & _
			"WHERE OrdemProducaoID = " & CStr(nOrdemProducaoID)

		rsCommand.ActiveConnection = strConn
		rsCommand.CommandText = strSQL
		rsCommand.CommandType = adCmdText
		rsCommand.Execute , , adCmdText + adExecuteNoRecords
		Set rsCommand = Nothing
End If

rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing

%>

