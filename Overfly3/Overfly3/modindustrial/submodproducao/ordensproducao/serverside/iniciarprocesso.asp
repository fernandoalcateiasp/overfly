
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    'Captura os parametros de pesquisa
	Dim i
    Dim nOPID, nEstadoID, nUserID, sResult
    
    i = 0
    nOPID = 0
    nEstadoID = 0 
    nUserID = 0
    sResult = ""
    
	For i = 1 To Request.QueryString("nOPID").Count    
	  nOPID = Request.QueryString("nOPID")(i)
	Next

	For i = 1 To Request.QueryString("nEstadoID").Count    
	  nEstadoID = Request.QueryString("nEstadoID")(i)
	Next

	For i = 1 To Request.QueryString("nUserID").Count    
	  nUserID = Request.QueryString("nUserID")(i)
	Next

	Dim rsSPCommand
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
	  
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_OrdemProducao_IniciaProcesso"
	    .CommandType = adCmdStoredProc

	    .Parameters.Append( .CreateParameter("@OrdemProducaoID", adInteger, adParamInput) )
	    .Parameters("@OrdemProducaoID").Value = CLng(nOPID)

	    .Parameters.Append( .CreateParameter("@EstadoID", adInteger, adParamInput) )
	    .Parameters("@EstadoID").Value = CLng(nEstadoID)

	    .Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
	    .Parameters("@UsuarioID").Value = CLng(nUserID)

	    .Parameters.Append( .CreateParameter("@Resultado", adVarChar, adParamOutput, 8000) )

	    .Execute
	    
	End With

	sResult = rsSPCommand.Parameters("@Resultado").Value

	Dim rsNew
	Set rsNew = Server.CreateObject("ADODB.Recordset")
	rsNew.CursorLocation = adUseServer

	rsNew.Fields.Append "Mensagem", adVarchar, 8000, adFldMayBeNull OR adFldUpdatable
	  
	rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

	rsNew.AddNew
	
	If ( NOT IsNull(sResult) ) Then
	    rsNew.Fields("Mensagem").Value = sResult
	End If

	rsNew.Update

	rsNew.Save Response, adPersistXML

	Set rsSPCommand = Nothing

	rsNew.Close
	Set rsNew = Nothing

%>