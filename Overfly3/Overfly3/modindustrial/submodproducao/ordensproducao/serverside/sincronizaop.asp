
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    'Captura os parametros de pesquisa
	Dim i
    Dim nOrdemProducaoID, sResult, nResult
    
    i = 0
    nOrdemProducaoID = 0
    sResult = ""
    nResult = 0
    
	For i = 1 To Request.QueryString("nOrdemProducaoID").Count    
	  nOrdemProducaoID = Request.QueryString("nOrdemProducaoID")(i)
	Next

	Dim rsSPCommand
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
	  
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_OrdemProducao_Sincroniza"
	    .CommandType = adCmdStoredProc

	    .Parameters.Append( .CreateParameter("@OrdemProducaoID", adInteger, adParamInput) )
	    .Parameters("@OrdemProducaoID").Value = CLng(nOrdemProducaoID)

	    .Parameters.Append( .CreateParameter("@Ocorrencia", adInteger, adParamInputOutput ) )
	    .Parameters("@Ocorrencia").Value = 0

	    .Parameters.Append( .CreateParameter("@Resultado", adVarChar, adParamOutput, 8000) )

	    .Execute

	End With

	nResult = rsSPCommand.Parameters("@Ocorrencia").Value
	
	sResult = rsSPCommand.Parameters("@Resultado").Value

	Dim rsNew
	Set rsNew = Server.CreateObject("ADODB.Recordset")
	rsNew.CursorLocation = adUseServer

	rsNew.Fields.Append "Mensagem", adVarchar, 8000, adFldMayBeNull OR adFldUpdatable
	  
	rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

	rsNew.AddNew
	
	If ( NOT IsNull(sResult) ) Then
	    rsNew.Fields("Mensagem").Value = sResult
	End If

	rsNew.Update

	rsNew.Save Response, adPersistXML

	Set rsSPCommand = Nothing

	rsNew.Close
	Set rsNew = Nothing

%>