
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Response.ContentType = "text/xml"

Dim rsData, rsNew
Dim nOPID, nEstadoID, nDataLen, nDataElemLen
Dim i, j, rsCommand, strSQL, nPedidoReferencia
Dim retVal
Dim rsERROR
  
nOPID = 0
nEstadoID = 0
nDataLen = 0
nDataElemLen = 0
strSQL = ""
nPedidoReferencia = 0
retVal = 0

For i = 1 To Request.QueryString("nOPID").Count    
    nOPID = Request.QueryString("nOPID")(i)
Next

For i = 1 To Request.QueryString("nEstadoID").Count    
    nEstadoID = Request.QueryString("nEstadoID")(i)
Next

For i = 1 To Request.QueryString("nDataLen").Count    
    nDataLen = Request.QueryString("nDataLen")(i)
Next

For i = 1 To Request.QueryString("nDataElemLen").Count
    nDataElemLen = Request.QueryString("nDataElemLen")(i)
Next

'*********************************************************************
'Grava itens na tabela
ReDim aData(nDataLen - 1, nDataElemLen - 1)

For j = 0 To (nDataLen - 1)
    For i = 0 To (nDataElemLen - 1)
        aData(j, i) = Request.QueryString("aData" & CStr(i))(j + 1)
    Next
Next

'Gravacao do itens no pedido
Set rsCommand = Server.CreateObject("ADODB.Command")

rsCommand.CommandTimeout = 60 * 10

'Insere na tabela item por item para rodar a trigger de impostos
strSQL = ""
For i = 0 To (nDataLen - 1)

    'nPedidoReferencia = aData(i, 7)
    'If (CLng(nPedidoReferencia) = 0) Then
    '    nPedidoReferencia = "NULL"
    'Else
    '    nPedidoReferencia = CStr(aData(i, 7))
    'End If        
    
    strSQL = strSQL & "INSERT INTO OrdensProducao_Itens (OrdemProducaoID, ProdutoID, EstadoID, Quantidade, " & _
                      "TipoMaterialID, TempoProducao) " & _
                      "SELECT " & CStr(nOPID) & ", " & CStr(aData(i, 0)) & ", " & CStr(aData(i, 1)) & ", " & _
                                  CStr(aData(i, 2)) & ", " & CStr(aData(i, 3)) & ", " & CStr(aData(i, 4)) & " "
Next

If (strSQL <> "") Then
    rsCommand.ActiveConnection = strConn
    rsCommand.CommandText = strSQL
    rsCommand.CommandType = adCmdText
    rsCommand.Execute , , adCmdText + adExecuteNoRecords
End If

'Devolucao da resposta
Set rsNew = Server.CreateObject("ADODB.Recordset")

rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "fldErrorNumber", adDecimal, 10, adFldMayBeNull OR adFldUpdatable
rsNew.Fields.Append "fldErrorText", adVarchar, 100, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

'Tratamento do erro valor unitario <0
For Each rsERROR In rsCommand.ActiveConnection.Errors
    If rsERROR.NativeError > 50000 Then
        rsNew.AddNew
        rsNew.Fields("fldErrorNumber").Value = rsERROR.NativeError
        rsNew.Fields("fldErrorText").Value =  rsERROR.Description
        rsNew.Update
    End If
Next

rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
Set rsCommand = Nothing

%>

