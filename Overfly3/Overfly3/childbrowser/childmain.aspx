﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="childmain.aspx.cs" Inherits="Overfly3.childbrowser.childmain" %>

<%         
    string pagesURLRoot;
    string strConn;
    string ApplicationName;
    bool DevMode;


    OVERFLYSVRCFGLib.OverflyMTS objSvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();

    WSData.dataInterface oDataInterface = new WSData.dataInterface(
           System.Configuration.ConfigurationManager.AppSettings["application"]
       );
    pagesURLRoot = objSvrCfg.PagesURLRoot(
           System.Configuration.ConfigurationManager.AppSettings["application"]);

    DevMode = objSvrCfg.DevMode(System.Configuration.ConfigurationManager.AppSettings["application"]);

    ApplicationName = oDataInterface.ApplicationName;


    Response.Write("<script ID=" + (char)34 + "serverCfgVars" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
    Response.Write("\r\n");

    if (DevMode)
        Response.Write("var glb_devMode = true;");
    else
        Response.Write("var glb_devMode = false;");

    Response.Write("\r\n");

    Response.Write("var __APP_NAME__ = " + (char)39 + ApplicationName + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __PAGES_DOMINIUM__ = " + (char)39 + objSvrCfg.PagesDominium(ApplicationName) + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __PAGES_URLROOT__ = " + (char)39 + objSvrCfg.PagesURLRoot(ApplicationName) + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __DATABASE_DOMINIUM__ = " + (char)39 + objSvrCfg.DatabaseDominium(ApplicationName) + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __DATABASE_ASPURLROOT__ = " + (char)39 + objSvrCfg.DatabaseASPPagesURLRoot(ApplicationName) + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __DATABASE_WSURLROOT__ = " + (char)39 + objSvrCfg.PagesDominium(ApplicationName) + "/WSOverflyRDS" + (char)39 + ";");
    Response.Write("\r\n");

    Response.Write("</script>");
    Response.Write("\r\n");

%>
<html id="childmainHtml" name="childmainHtml">

<head>

    <title>Overfly</title>
    <%
        //Links de estilo, bibliotecas da automacao e especificas
        Response.Write("<LINK REL=" + (char)34 + "stylesheet" + (char)34 + " HREF=" + (char)34 + pagesURLRoot + "/childbrowser/childmain.css" + (char)34 + "type=" + (char)34 + "text/css" + (char)34 + ">"); Response.Write("\r\n");

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/Defines.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CRecordsetParser.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CDatatransportSystem.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CReturnParam.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CFieldStructure.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CField.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CFields.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CRecordset.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CDatatransport.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CReturnParam.js" + (char)34 + "></script>"); Response.Write("\r\n");

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_sysbase.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_browsers.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_constants.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_htmlbase.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_interface.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_statusbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_fastbuttonsbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_controlsbar.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_modalwin.js" + (char)34 + "></script>"); Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_strings.js" + (char)34 + "></script>"); Response.Write("\r\n");      

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/childbrowser/childmain.js" + (char)34 + "></script>"); Response.Write("\r\n");
    %>


    <%
        Response.Write("<script ID=" + (char)34 + "formVars" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
        Response.Write("\r\n");
        Response.Write("\r\n");

        // IP do usuário logado.
        Response.Write("var glb_UserIP = '" + Request.UserHostAddress.ToString() + "';");

        //Escreve userID na sessao
        Session["userID"] = userid;
        //Escreve formName na sessao
        Session["formName"] = formName;


        FormNAmes = DataInterfaceObj.getRemoteData("SELECT RecursoID, RecursoFantasia AS Recurso, ISNULL(TipoID, 15) AS TipoID, " +
                 "ISNULL((SELECT TOP 1 c.RecursoID FROM Recursos a, RelacoesRecursos b, Recursos c " +
                    "WHERE (a.RecursoMaeID = Recursos.RecursoID AND a.TipoRecursoID = 3 AND " +
                        "a.EstadoID = 2 AND a.RecursoID = b.ObjetoID AND b.TipoRelacaoID = 1 AND " +
                        "b.EstadoID = 2 AND b.SujeitoID = c.RecursoID AND c.EstadoID = 2 AND c.TipoRecursoID = 4 AND " +
                        "c.Principal = 1)), 0) AS SubFormID " +
                 "FROM Recursos WITH(NOLOCK) " +
                 "WHERE FormName='" + formName + "' AND TipoRecursoID = 2 AND ClassificacaoID = 11");

        formID = FormNAmes.Tables[1].Rows[0]["RecursoID"].ToString();
        formTitle = FormNAmes.Tables[1].Rows[0]["Recurso"].ToString();
        subFormID = FormNAmes.Tables[1].Rows[0]["SubFormID"].ToString();

        registroID_Type = FormNAmes.Tables[1].Rows[0]["TipoID"].ToString();



        //'Escreve as variaveis no arquivo
        Response.Write("var formName = null;");
        Response.Write("\r\n");
        Response.Write("var arqStart = null;");
        Response.Write("\r\n");
        Response.Write("var frameStart = null;");
        Response.Write("\r\n");
        Response.Write("var formID = null;");
        Response.Write("\r\n");
        Response.Write("var formTitle = null;");
        Response.Write("\r\n");
        Response.Write("var subFormID = null;");
        Response.Write("\r\n");
        Response.Write("var registroID_Type = null;");

        if (formName != null)
            Response.Write("formName = '" + formName + "';");

        Response.Write("\r\n");

        if (arqStart != null)
            Response.Write("arqStart = '" + arqStart + "';");

        Response.Write("\r\n");

        if (frameStart != null)
            Response.Write("frameStart = '" + frameStart + "';");

        Response.Write("\r\n");
        if (formID != null)
            Response.Write("formID = '" + formID + "';");

        if (formTitle != null)
            Response.Write("formTitle = '" + formTitle + "';");

        Response.Write("\r\n");
        if (subFormID != null)
            Response.Write("subFormID = '" + subFormID + "';");

        Response.Write("\r\n");


        //'Os tipos de geracao do id do registro possiveis (so para inclusao,
        //'alteracao o usuario nao pode alterar o id do registro):
        //'15 - Banco gera o id do registro (identity  )
        //'16 - A trigger gera o id do registro (identity)
        //'17 - O usuario define o id do registro

        Response.Write("registroID_Type = " + registroID_Type + ";");
        Response.Write("\r\n");
        //'----------------------------------------------------------------
        Response.Write("</script>");
        Response.Write("\r\n");

    %>
    <script id="wndJSProc" language="javascript">
<!--

    /********************************************************************
    Descritivo da interface nao carregando o fast buttons child
    1. Nome da interface = 'USERCHILDBROWSER'
    2. Componentes:
    html/asp                bin  hex    frame               observacao
    --------                ---  ---    -----               ----------
    statusbarchild.asp      001  0X1    frameStatusBar
    btnsbarsup.asp          010  0X2    frameBtnsBarSup
    btnsbarinf.asp          100  0X4    frameBtnsBarInf
    
    toda interface          111  0X7
    ********************************************************************/

    //-->
    </script>

    <script id="vbFunctions" language="vbscript">
<!--

Function DateIsInInterval( dateBase, dateToCheck, intInDays )
    DateIsInInterval = False
    
    Dim actualIntInDays
    
    actualIntInDays = daysBetween(dateBase, dateToCheck)
    
    If (intInDays >= 0) Then
        If ( actualIntInDays >= 0 ) Then    
            If (intInDays >= actualIntInDays) Then
                DateIsInInterval = True
            End If
        End If
    Else
        If ( actualIntInDays <= 0 ) Then
            If ( intInDays <= actualIntInDays ) Then
                DateIsInInterval = True
            End If
        End If    
    End If    
    
End Function

Function DateOfToday()
    DateOfToday = Date
End Function

Function chkTypeData( sType, vValue )
    If sType = "D" Then ' Date Format
       chkTypeData = IsDate(vValue)
    ElseIf sType = "N" Or sType = "M" Then ' Number Format
       chkTypeData = IsNumeric(vValue)
    Else
       chkTypeData = TRUE
    End If
End Function

Function formatCurrValue(nValue)
    formatCurrValue = FormatNumber(nValue,2)
End Function

Function daysBetween(date1, date2)
    daysBetween = DateDiff("d",date1,date2)
End Function

Function daysSUM(dDate, nDias)
    daysSUM = DateAdd("d",nDias, Ddate)
End Function

Function DateToStr(dDate, sDateFormat)
    Dim sDay,sMonth,sYear
    sDay = Day(dDate)
    sMonth = Month(dDate)
    sYear = Year(dDate)
    If (sDateFormat = "DD/MM/YYYY") Then
        DateToStr = sDay & "/" & sMonth & "/" & sYear
    Else
        DateToStr = sMonth & "/" & sDay & "/" & sYear
    End If    
End Function

Function dataExtenso( dData, sIdioma)
    Dim sDataExtenso
    Dim aMonths
    Dim nDay, nMonth, nYear

    If not IsDate(dData) Then
        dataExtenso = Null
        Exit Function
    End If
    
    sIdioma = UCase(sIdioma)
    nDay = Day(dData)
    nMonth = Month(dData) - 1
    nYear = Year(dData)
    
    If sIdioma = "I" Then
        aMonths = Array("January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December")
        sDataExtenso = aMonths(nMonth) & " " & CStr(nDay) & ", " & CStr(nYear)
    Else
        aMonths = Array("Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho", "Agosto", "Setembro", "Outubro", "Novembro", "Dezembro")
        sDataExtenso = CStr(nDay) & " de " & aMonths(nMonth) & " de " & CStr(nYear)
    End If    
        
    dataExtenso = sDataExtenso
End Function

//-->
    </script>

</head>

<body id="childMainBody" name="childMainBody" language="javascript" onload="return window_onload()" onbeforeunload="return window_onbeforeunload()" onunload="return window_onunload()">

    <!-- Objeto OverflyGen -->
    <object classid="clsid:CBD8996E-E238-4A48-AB72-7A7250F700C1" id="overflyGen" height="0" width="0"></object>

    <iframe id="frameStatusBar" name="frameStatusBar" class="theFrames" frameborder="no"></iframe>
    <iframe id="frameFastButtons" name="frameFastButtons" class="theFrames" frameborder="no"></iframe>
    <iframe id="frameBarSup" name="frameBarSup" class="theFrames" frameborder="no"></iframe>
    <iframe id="frameBarInf" name="frameBarInf" class="theFrames" frameborder="no"></iframe>
    <iframe id="frameSup01" name="frameSup01" class="theFrames" frameborder="no"></iframe>
    <iframe id="frameSup02" name="frameSup02" class="theFrames" frameborder="no"></iframe>
    <iframe id="frameInf01" name="frameInf01" class="theFrames" frameborder="no"></iframe>

    <iframe id="frameModal" name="frameModal" class="theFrames" frameborder="no"></iframe>

</body>

</html>
