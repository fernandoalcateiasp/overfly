/********************************************************************
childmain.js

Library javascript para o childmain.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoGen01 = new CDatatransport('dsoGen01');
var dsoPrintLog = new CDatatransport('dsoPrintLog');
var __intID;
var __btnClicked;
var __formName;

// Id do usuario logado
var userID = 0;

var __intIDCHILDBROWSER = null;
var __intUNLOCKFBTNSBAR = null;
var glb_dateTime = null;
var glb_pParam2 = null;

// Variavel para exibir ou nao msg no beforeunload
var beforeUnloadMsg = true;

// Variaveis que contem os ids da tag html dos arquivos pesqlist,
// sup01 e inf01
var pesqListID = null;
var sup01ID = null;
var inf01ID = null;

// Controla estado de travamento do browser filho
var __bInterfaceLocked = false;

var __glb_childMainTimer = null;

// Indica se o form esta em lista ou detalhe
var __glb_ListOrDet = '';

var __ArrayTeste = new Array();

function showAlert() {
    var sString = '';
    var sTempo = 0;
    /*
    if (userID == 1176) 
        {
        for (var i = (__ArrayTeste.length - 1); i > 0; i--) {
                sTempo = __ArrayTeste[i][1] - __ArrayTeste[i - 1][1];
                sString += __ArrayTeste[i][0] + ' ' + sTempo.toString() + 'ms.\n';
            }    
            alert(sString);
        }*/
}
function showAlert_AddDate(sMsg) {
    var data = new Date();
    var milis = Number(data);

    __ArrayTeste[__ArrayTeste.length] = Array(sMsg, milis);
}

// Variaveis para selecionar o ultimo relatorio impresso pela modalprint
var __glb_LastReportSelectedInSup = null;
var __glb_LastReportSelectedInPesqList = null;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RESUMO DAS FUNCOES
    getPastaName(pastaID)
    getCurrDateTimeInServer(pParam2)
    openModalHTML(nRegistroID, sTitulo)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    // obtem o id do usuario logado e guarda e var global do browser filho
    userID = sendJSMessage('childmainHtml', JS_WIDEMSG, '__CURRUSERID', null);

    initInterfaceLoad('USERCHILDBROWSER', 0X7);

    var i, elem, coll;
    var dimArray = new Array();

    // todos os iframes
    coll = document.all.tags('IFRAME');

    // posi��o e dimens�es  dos frames
    dimArray[0] = 0;
    dimArray[1] = 0;
    dimArray[2] = 0;
    dimArray[3] = 0;

    for (i = 0; i < coll.length; i++) {
        coll.item(i).style.visibility = 'hidden';
        coll.item(i).scrolling = 'no';
        coll.item(i).tabIndex = -1;
        moveFrameInHtmlTop(coll.item(i).id, dimArray);
        // originally no backgroundColor
        coll.item(i).style.backgroundColor = 'white';
    }

    // ajusta o body do html
    var elem = document.getElementById('childmainBody');
    with (elem) {
        // originally transparent before 11/10/2001
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    // carrega o statusbar
    sendJSMessage('childmainHtml', JS_PAGELOAD, new Array(window.top.name.toString(), 'frameStatusBar', SYS_PAGESURLROOT + '/statusbar/statusbarchild.aspx'), null);

    // carrega o controlbarsup
    sendJSMessage('childmainHtml', JS_PAGELOAD, new Array(window.top.name.toString(), 'frameBarSup', SYS_PAGESURLROOT + '/controlbars/controlbarsup.asp'), null);

    // carrega o controlbarinf
    sendJSMessage('childmainHtml', JS_PAGELOAD, new Array(window.top.name.toString(), 'frameBarInf', SYS_PAGESURLROOT + '/controlbars/controlbarinf.asp'), null);
}

function getCurrEmpresaData() {
    return (sendJSMessage(getHtmlId(), JS_WIDEMSG, '__EMPRESADATA', null));
}

/********************************************************************
Confirma se deve ou nao fechar o browser
********************************************************************/
function window_onbeforeunload() {
    if (beforeUnloadMsg) {
        event.returnValue = SYS_NAME;
    }
}

/********************************************************************
Diversos cleanup do sistema
********************************************************************/
function window_onunload() {
    // o if abaixo deve estar aqui obrigatoriamente
    try {
        if (window.opener && !window.opener.closed) {
            removeChildBrowserFromControl(window.name);

            // no caso de interrupcao do carregamento do browser filho
            // destrava a interface do browser mae
            // implantacao em 07/08/2001 - remover se comportamento
            // estranho do sistema
            sendJSMessage('childmainHtml', JS_CHILDBROWSERLOADED, null, null);
        }

        if (window.frames.length >= 8)
            window.frames[7].pb_KillTimer();
    }
    catch (e) {
        ;
    }
}

/********************************************************************
Coloca o titulo do browser filho com o nome do form
********************************************************************/
function setTitleWithFormName() {
    window.document.title = formTitle;
    writeInStatusBar('child', 'cellForm', formTitle);
}

/********************************************************************
Funcao de comunicacao do carrier:
Chamada do arquivo js_sysbase.js.
Veio da funcao sendJSCarrier definida no arquivo js_sysbase.js e chamada
em qualquer arquivo de form.

Localizacao:
childmain.asp

Chama:
Funcao sendJsCarrierFromChildBrowser(window.top.name,
                                     idElement, param1, param2)

do arquivo overfly.asp do browser mae.

Parametros:
idElement             - Obrigatorio. Id do html que manda o carrier.
param1                - De uso do programador. Qualquer objeto ou null.
param2                - De uso do programador. Qualquer objeto ou null.
especChildBrowserName - De uso do programador. Nome de um browser filho
                        para receber o carrier. Se null o carrier sera
                        propagado para todos os browsers.

Retorno     - null se nao tem browser filho.
********************************************************************/
function sendJsCarrierFromForm(idElement, param1, param2, especChildBrowserName) {
    var wndTop = window.opener;

    if (wndTop == null)
        return null;

    // Chamada do carrier. Chama funcao do browser mae no overfly.asp
    wndTop.sendJsCarrierFromChildBrowser(window.top.name, idElement,
                                         param1, param2,
                                         especChildBrowserName);
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2) {
    switch (msg) {
        // Msg do carrier
        case JS_CARRIERCAMING:
            {
                return null;
            }
            break;

        case JS_CARRIERRET:
            {
                return null;
            }
            break;

        case JS_PAGELOAD:
            {
                if (param1[0].toUpperCase() == window.top.name.toUpperCase())
                    loadPage(idElement, param1, param2);
            }
            break;

        case JS_PAGELOADED:
            {
                if (param1[0].toUpperCase() == window.top.name.toUpperCase()) {
                    if (param1[1].toUpperCase() == getInterfaceName()) {
                        if (componentOfInterfaceLoaded(param2) == true) {
                            // a interface terminou de carregar
                            // a funcao abaixo faz os ajustes finais da
                            // interface e exibe a mesma para o usuario
                            childBrowserUserLoaded();
                        }
                    }
                }
            }
            break;

        case JS_FORMOPENED:
            {
                // informa quem de direito o final de
                // carregamento do browser filho com o form correspondente
                if (param1[0].toUpperCase() == window.top.name.toUpperCase()) {
                    finishFormLoaded();
                    return 0;
                }
            }
            break;

        case JS_MAINBROWSERCLOSING:
            {
                overflyGen.ForceDlgModalClose();
                beforeUnloadMsg = false;
                window.close();
            }
            break;

            // retorna ou altera e retorna o status da flag de interface
            // interface travada do browser filho
            // param1 == false -> so retorna
        case JS_CHILDBROWSERLOCKED:
            {
                if (param1 == false)
                    return __bInterfaceLocked;
                else if (param1 == true) {
                    __bInterfaceLocked = !__bInterfaceLocked;
                    return __bInterfaceLocked;
                }
            }
            break;

            // botao clicado na barra de botoes de acesso rapido
            // param1 e o id do botao
            // param2 e o nome do form
        case JS_FASTBUTTONCLICKED:
            {
                // confirma se realmente vai recarregar o form
                if (formName.toUpperCase() == param2) {
                    var _retMsg = window.top.overflyGen.Confirm('Este � o form que est� carregado.\nRecarrega o form?');

                    if (_retMsg == 0)
                        return null;
                    else if (_retMsg == 1)
                        return true;
                }
                // carrega outro form no browser filho    
                loadOtherFormInBrowser(param2);

                return true;
            }
            break;

        case JS_MSGRESERVED:
            {
                if (param1 == 'ST_UP')
                    childMainBody.scrollIntoView(true);
            }
            break;

        default:
            return null;
    }
}

/********************************************************************
Esta funcao recebe o indicacao de final de carregamento da interface.
Nela � feita a configuracao final e mostra-se a interface.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function childBrowserUserLoaded() {
    var frameID;
    var frameDim = new Array();

    // parametros das barras de controle
    var stBarHeight = 27;
    var stBarDif = 0;
    var stBarInfTop = 280;

    // Configura statusBar
    writeInStatusBar('child', 'cellPrgName', SYS_NAME);
    writeInStatusBar('child', 'cellEmpresa', getDataInStatusBar('main', 'cellEmpresa')[2]);
    writeInStatusBar('child', 'cellUserName', getDataInStatusBar('main', 'cellUserName'));
    writeInStatusBar('child', 'cellForm', 'Form');
    writeInStatusBar('child', 'cellMode', 'Pesquisa');

    // ajusta a posicao e dimensoes do statusbar
    frameID = getFrameIdByHtmlId('statusbarchildHtml');
    moveFrameInHtmlTop(frameID, new Array(0, 0, MAX_FRAMEWIDTH, 24));
    // mostra o statusbar
    showFrameInHtmlTop(frameID, true);

    // trava o status bar para destravar apos o carregamento do form
    sendJSMessage('AnyHtml', JS_STBARGEN, 'CMBSTATUS', true);

    // pega posicao e dimensoes do statusbar
    frameID = getFrameIdByHtmlId('statusbarchildHtml');
    frameDim = getRectFrameInHtmlTop(frameID);

    // ajusta a posicao e dimensoes do frame que continha o fastbuttons
    frameID = 'frameFastButtons';
    moveFrameInHtmlTop(frameID, new Array(0, (frameDim[1] + frameDim[3] - 1), MAX_FRAMEWIDTH, 0));
    // previne nao mostrar o frame que continha o fastbuttons
    showFrameInHtmlTop(frameID, false);

    // Configura o frame que contem a barra de botoes superior
    // pega posicao e dimensoes do fastbuttons
    frameID = 'frameFastButtons';
    frameDim = getRectFrameInHtmlTop(frameID);

    // ajusta a posicao e dimensoes da barra de botoes superior
    frameID = getFrameIdByHtmlId('controlbarsupHtml');
    moveFrameInHtmlTop(frameID, new Array(0, (frameDim[1] + frameDim[3] + 1), MAX_FRAMEWIDTH, stBarHeight));
    // mostra o frame do fastbuttons
    showFrameInHtmlTop(frameID, true);

    // Configura a barra de botoes inferior
    // pega posicao e dimensoes do controlbarsup
    frameID = getFrameIdByHtmlId('controlbarsupHtml');
    frameDim = getRectFrameInHtmlTop(frameID);
    // ajusta a posicao e dimensoes da barra de botoes inferior
    frameID = getFrameIdByHtmlId('controlbarinfHtml');
    moveFrameInHtmlTop(frameID, new Array(0, stBarInfTop, MAX_FRAMEWIDTH, stBarHeight + stBarDif));
    // nao mostra o frame do controlbarinf
    showFrameInHtmlTop(frameID, false);

    // inicia o carregamento do form
    initFormLoad();
}

/********************************************************************
Esta funcao inicia o carregamento de outro form no browser filho.
Alem disto ela obtem, entre outros parametros o formID do novo
form. 

Parametros:
nome do form a carregar

Retorno:
nenhum
********************************************************************/
function loadOtherFormInBrowser(theForm) {
    // trava o status bar
    sendJSMessage('AnyHtml', JS_STBARGEN, 'CMBSTATUS', true);

    // trava a barra de botoes de acesso rapido
    lockFastButtons();

    // trava a botoeira superior
    setupControlBar('sup', 'disable', 'disable', 'DDDDDDDDDDDDDDD');
    // trava a botoeira inferior
    setupControlBar('inf', 'disable', 'disable', 'DDDDDDDDDDDDDDD');

    // remete dados basicos do form para o lado do servidor
    var strPars = new Array();
    strPars = '?';
    strPars += 'formName=';
    strPars += escape(theForm);

    dsoGen01.URL = SYS_PAGESURLROOT + '/serversidegen/formdata01.asp' + strPars;
    dsoGen01.ondatasetcomplete = dsoGen01_loadOtherFormInBrowser_DSC;

    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();

    dsoGen01.refresh();
}

/********************************************************************
Retorno do servidor da funcao loadOtherFormInBrowser

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoGen01_loadOtherFormInBrowser_DSC() {
    formName = dsoGen01.recordset['FormName'].value;
    arqStart = overflyRDSForms.form(
		SYS_PAGESURLROOT + dsoGen01.recordset['ArqStart'].value,
		glb_devMode);

    var userID = sendJSMessage('childmainHtml', JS_WIDEMSG, '__CURRUSERID', null);

    frameStart = dsoGen01.recordset['FrameStart'].value;
    formID = dsoGen01.recordset['RecursoID'].value;
    formTitle = dsoGen01.recordset['Recurso'].value;
    subFormID = dsoGen01.recordset['SubFormID'].value;

    // inicia a carga do novo form escolhido pelo usuario
    // para este browser filho
    __intIDCHILDBROWSER = window.setInterval('startNewFormInBrowser()', 50, 'JavaScript');
}

/********************************************************************
Esta funcao starta o carregamento do form no browser filho.
A funcao initFormLoad() nao foi chamada direta, para retornar
o ondatasetcomplete do dsoGen01 na funcao dsoGen01_resetPastasArray_DSC()

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function startNewFormInBrowser() {
    if (__intIDCHILDBROWSER != null) {
        window.clearInterval(__intIDCHILDBROWSER);
        __intIDCHILDBROWSER = null;
    }

    // descarrega o modal frame se estiver a modal
    // (na verdade so troca o id do html)
    // do carrinho de compras carregado e trocar a empresa selecionada
    // no combo de empresas
    if ((htmlLoadedInModal()).toUpperCase() == 'MODALTROLLEYHTML') {
        var coll = frameModal.document.getElementsByTagName('HTML');

        coll[0].id = '';
        coll[0].name = '';
    }

    initFormLoad();
}

/********************************************************************
Esta funcao inicia o carregamento do form no browser filho.
Assincronamente chama a funcao __initFormLoad()

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function initFormLoad() {
    __glb_childMainTimer = window.setInterval('__initFormLoad()', 10, 'JavaScript');
}

function __initFormLoad() {
    if (__glb_childMainTimer != null) {
        window.clearInterval(__glb_childMainTimer);
        __glb_childMainTimer = null;
    }

    // escreve no statusbar inicio do carregamento
    writeInStatusBar('child', 'cellMode', 'Carregando', true);

    // passa o nome do form por parametro
    var strPars = new Array();
    strPars = '?';
    strPars += 'formName=';
    strPars += escape(formName);

    // starta carregamento do html/asp que controla o carregamento do form
    sendJSMessage('childmainHtml', JS_PAGELOAD, new Array(window.top.name, frameStart, arqStart + strPars), null);
}

/********************************************************************
Esta funcao recebe o indicacao de final de carregamento de form
no browser filho.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function finishFormLoaded() {
    // destrava o status bar
    sendJSMessage('AnyHtml', JS_STBARGEN, 'CMBSTATUS', false);

    // destrava a barra de botoes de acesso rapido
    __intUNLOCKFBTNSBAR = window.setInterval('unlockFBtnBar()', 10, 'JavaScript');

    // informa o final do carregamento do browser filho com sua interface
    // e seu form
    sendJSMessage('childmainHtml', JS_CHILDBROWSERLOADED, new Array(window.top.name, formName), null);
}

function unlockFBtnBar() {
    if (__intUNLOCKFBTNSBAR != null) {
        window.clearInterval(__intUNLOCKFBTNSBAR);
        __intUNLOCKFBTNSBAR = null;
    }
    unlockFastButtons();
}

/********************************************************************
Esta funcao retorna o nome de fantasia de uma pasta do form

Parametros:
pastaID         - id da pasta a retornar o nome

Retorno:
O nome da pasta se sucesso ou uma string vazia se a pasta nao e
encontrada
********************************************************************/
function getPastaName(pastaID) {
    return sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getFolderNameByDso(' + pastaID + ')'));

}

/********************************************************************
Esta funcao obtem a data e hora corrente no servidor onde estao 
as paginas do Overfly.

A data e hora e retornada no wndJSProc conforme fragmento abaixo

case JS_DATETIMEINSERVER :
{
    // retorna data e hora do servidor de paginas no param1
    // e no param2, qualquer parametro que tenha sido
    // usado pelo programador na chamada da funcao
    return 0;
}

Parametros:
a vontade do programador

Retorno:
Se sucesso: hora e data no param1. Se falha: false no param1.
O parametro que for passado pelo programador retorna sempre como param2.
********************************************************************/
function getCurrDateTimeInServer(pParam2) {
    lockInterface(true);

    glb_pParam2 = null;

    if (typeof (pParam2) != 'undefined')
        glb_pParam2 = pParam2;

    dsoGen01.URL = SYS_PAGESURLROOT + '/serversidegenEx/datetime.aspx';
    dsoGen01.ondatasetcomplete = __dsoGen01_dateTime_DSC;
    dsoGen01.refresh();

    return null;
}

/********************************************************************
Retorno do servidor da funcao getCurrDateTimeInServer(pParam2)

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function __dsoGen01_dateTime_DSC() {
    if (!dsoGen01.recordset.BOF && !dsoGen01.recordset.EOF)
        glb_dateTime = dsoGen01.recordset.Fields['DateTime'].value;
    else
        glb_dateTime = false;

    lockInterface(false);

    sendJSMessage('childmainHtml', JS_DATETIMEINSERVER, glb_dateTime, glb_pParam2);
}

/********************************************************************
Esta funcao recebe evento de botao clicado na barra de botoes de
acesso rapido

Parametros:
btnClicked  - referencia ao botao clicado
formName    - nome do form conectado ao botao

Retorno:
nenhum
********************************************************************/
function emulEvent(btnClicked, formName) {
    __btnClicked = btnClicked;
    __formName = formName;
    __intID = window.setInterval('fastButtonClicked()', 50, 'JavaScript');
}

/********************************************************************
Funcao complementar da funcao emulEvent(btnClicked, formName)

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fastButtonClicked() {
    if (__intID != null) {
        window.clearInterval(__intID);
        __intID = null;
    }

    sendJSMessage('childmainHtml', JS_FASTBUTTONCLICKED, __btnClicked, __formName);

}

/********************************************************************
Retorna se o form corrente esta em modo pesquisa ou detalhe

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function __interfaceMode() {
    var i, coll, elem;

    coll = document.all.tags('IFRAME');

    for (i = 0; i < coll.length; i++) {
        if (coll.item(i).id == 'frameInf01') {
            elem = coll.item(i);
            break;
        }
    }

    if (elem.currentStyle.visibility == 'hidden')
        return 'PL';
    else
        return 'D';
}

/********************************************************************
Funcao para uso do programador, abre a modal de documentos

Parametros:

Retorno:
********************************************************************/

/* 
Bot�o 3
window.top.openModalControleDocumento('PL', '', 423, null, null, 'T', 'btn3');
Bot�o 4
window.top.openModalControleDocumento('S', '', txtRegistroID.value, null, null, 'T', 'btn4');
Bot�o 1 - Inferior
window.top.openModalControleDocumento('I', '', dso01JoinSup.recordset['DocumentoID'].value, -1, null, 'E');
Bot�o 1 - Desativos
window.top.openModalControleDocumento('I', '', nDocumentoID, nVersao, null, 'D');
*/

function openModalControleDocumento(sCaller, sTitulo, nDocumentoID, nTipoDocToLoad, sAnchorID, sCallOrigin, sCallOrigin2) {
    var strPars = new String();
    var htmlPath;

    if ((sTitulo == null) || (sTitulo == ''))
        sTitulo = 'Documento';

    strPars = '?sCaller=' + escape(sCaller);
    strPars += '&sTitulo=' + escape(sTitulo);
    strPars += '&nDocumentoID=' + escape((nDocumentoID == null ? '' : nDocumentoID));
    strPars += '&nTipoDocToLoad=' + escape((nTipoDocToLoad == null ? 'NULL' : nTipoDocToLoad));
    strPars += '&nFormatoData=' + escape(DATE_SQL_PARAM);

    if (sAnchorID != null)
        strPars += '&sAnchorID=' + escape(sAnchorID);

    strPars += '&sCallOrigin=' + escape(sCallOrigin);
    strPars += '&sCallOrigin2=' + escape(sCallOrigin2);

    // carregar modal de procedimentos
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalcontroledocumento.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 537));
}

/********************************************************************
Funcao para uso do programador, abre a modal Termo de Acordo

Parametros:

Retorno:
Criado por DMC: 29/04/2011
********************************************************************/
function openModalTermoAcordo(sCaller, sTitulo, nTermoAcordoID, nIdiomaID, sCallOrigin, nTipoTermoAcordoID, nRevisao) {
    // var aEmpresa = getCurrEmpresaData();

    var empresaData = getCurrEmpresaData();
    var aEmpresa = empresaData[0];

    var strPars = new String();
    var htmlPath;

    // Exibe o titulo da Modal
    if ((sTitulo == null) || (sTitulo == ''))
        sTitulo = 'Termo de Acordo';

    // Se for do Inf � hist�rico

    // Parametros passados para a p�gina ASP
    strPars = '?sCaller=' + escape(sCaller);
    strPars += '&sTitulo=' + escape(sTitulo);
    strPars += '&nTermoAcordoID=' + escape(nTermoAcordoID);
    strPars += '&nIdiomaID=' + escape(nIdiomaID);
    strPars += '&nFormatoData=' + escape(DATE_SQL_PARAM);
    strPars += '&nTipoTermoAcordoID=' + escape(nTipoTermoAcordoID);
    strPars += '&sCallOrigin=' + escape(sCallOrigin);
    strPars += '&aEmpresa=' + escape(aEmpresa);

    if ((sTitulo == null) || (sTitulo == '')) {
        strPars += '&nRevisao=NULL';
    }
    else {
        strPars += '&nRevisao=' + escape(nRevisao);
    }

    // Carrega o ASP da modal Termo de Acordo
    htmlPath = SYS_PAGESURLROOT + '/modbasico/submodauxiliar/TermoAcordo/modalpages/modaltermoacordo.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 537));
}

/********************************************************************
Funcao para uso do programador, abre a modal de documentos

Parametro:

Retorno:
********************************************************************/
function openModalHTML(nRegistroID, sTitulo, sCaller, nTipoID, nTemFoto) {
    var strPars = new String();
    var htmlPath;
    var nWidth = 771;
    var nHeight = 462;

    if ((sTitulo == null) || (sTitulo == ''))
        sTitulo = 'Documento';

    if ((nTemFoto == null) || (nTemFoto == ''))
        nTemFoto = 0;

    strPars = '?nRegistroID=' + escape(nRegistroID);
    strPars += '&sTitulo=' + escape(sTitulo);
    strPars += '&sCaller=' + escape(sCaller);
    strPars += '&nTipoID=' + escape(nTipoID);
    strPars += '&nTemFoto=' + escape(nTemFoto);

    if ((nTipoID == 7) || (nTipoID == 11)) {
        nWidth = 1000;
        nHeight = 539;
    }

    if (nTipoID == 12)
    {
        nWidth = 1000;
        nHeight = 600;
    }

    // carregar modal de documentos
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalopenhtml.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}
