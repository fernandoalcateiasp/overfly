﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;
using WSData;
using OVERFLYSVRCFGLib;


namespace Overfly3.childbrowser
{
    public partial class childmain : System.Web.UI.OverflyPage
    {

        protected DataSet FormNAmes;
        protected string formID, subFormID, registroID_Type;
        protected string formTitle;

        protected override void PageLoad(object sender, EventArgs e)
        {

        }

        private string FormName;

        protected string formName
        {
            get { return FormName; }
            set { FormName = value; }
        }
        private string ArqStart;

        protected string arqStart
        {
            get { return ArqStart; }
            set { ArqStart = value; }
        }

        private string FrameStart;

        protected string frameStart
        {
            get { return FrameStart; }
            set { FrameStart = value; }
        }
        private Integer UserID;

        protected Integer userID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private Integer EmpresaID;

        protected Integer empresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }

        protected int userid
        {
            get { return userID.intValue(); }
        }
        
    }
}