
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew
Dim nDataLen, nValidacaoDadoID, nNivel, sNodeMae, sNode, sAtributo, sValor, nPaisID, nFonteDadoID, sIdadeValue, sNode2
Dim strSQL
Dim nRecsAffected
Dim rsCommand
Dim i
  
strSQL = ""
nDataLen = 0
nValidacaoDadoID = 0
nNivel = 0
sNodeMae = ""
sNode = ""
sAtributo = ""
sValor = ""
nFonteDadoID = 0
nPaisID = 0
sIdadeValue = ""
sNode2 = ""

nDataLen = Request.QueryString("nNivel").Count

For i = 1 To Request.QueryString("nValidacaoDadoID").Count    
    nValidacaoDadoID = Request.QueryString("nValidacaoDadoID")(i)
Next

ReDim nNivel(nDataLen - 1)
For i = 1 To (nDataLen)
	nNivel(i-1) = Request.QueryString("nNivel")(i)
	
	If ((nNivel(i-1)) = "") Then
	    nNivel(i-1) = "NULL"
    End If	        
Next

ReDim sNodeMae(nDataLen - 1)
For i = 1 To (nDataLen)
	sNodeMae(i-1) = Request.QueryString("sNodeMae")(i)
    
    If (((sNodeMae(i-1)) = Null) OR ((sNodeMae(i-1)) = "")) Then
	    sNodeMae(i-1) = "NULL"
    Else	    
        sNodeMae(i-1) = "'" & sNodeMae(i-1) & "'"
    End If	        
Next

ReDim sNode(nDataLen - 1)
For i = 1 To (nDataLen)
	sNode(i-1) = Request.QueryString("sNode")(i)
    
    If (((sNode(i-1)) = Null) OR ((sNode(i-1)) = ""))Then
	    sNode(i-1) = "NULL"
    Else
        sNode(i-1) = "'" & sNode(i-1) & "'"	    
    End If	        
Next

ReDim sAtributo(nDataLen - 1)
For i = 1 To (nDataLen)
	sAtributo(i-1) = Request.QueryString("sAtributo")(i)
    
    If (((sAtributo(i-1)) = Null) OR ((sAtributo(i-1)) = ""))Then
	    sAtributo(i-1) = "NULL"
    Else
        sAtributo(i-1) = "'" & sAtributo(i-1) & "'"	    
    End If	        
Next

ReDim sValor(nDataLen - 1)
For i = 1 To (nDataLen)
	sValor(i-1) = Request.QueryString("sValor")(i)
    
    If (((sValor(i-1)) = Null) OR ((sValor(i-1)) = ""))Then
	    sValor(i-1) = "NULL"
    Else
        sValor(i-1) = "'" & sValor(i-1) & "'"	    
    End If	        
Next

For i = 1 To Request.QueryString("nFonteDadoID").Count    
    nFonteDadoID = Request.QueryString("nFonteDadoID")(i)
Next

For i = 1 To Request.QueryString("nPaisID").Count    
    nPaisID = Request.QueryString("nPaisID")(i)
Next

Set rsCommand = Server.CreateObject("ADODB.Command")

For i = 1 To (nDataLen)            
        sNode2 = sNode(i-1)
        sIdadeValue = sValor(i-1)
        
        strSQL = strSQL & " INSERT INTO ValidacoesDados_Detalhes (ValidacaoDadoID, Nivel, NodeMae, Node, Atributo, ValorID, Valor) " 
        
        strSQL = strSQL & "SELECT " & CStr(nValidacaoDadoID) & ", " & CStr(nNivel(i-1)) & ", " & CStr(sNodeMae(i-1)) & ", " & _
                    CStr(sNode(i-1))  & ", " & CStr(sAtributo(i-1))  & ", NULL, " & CStr(sValor(i-1))  	 	

        If ((sNode(i-1)="'DDD'") AND (nFonteDadoID = 287))Then
            strSQL = strSQL & " UNION ALL SELECT " & CStr(nValidacaoDadoID) & ", " & CStr(nNivel(i-1)) & ", " & CStr(sNodeMae(i-1)) & ", " & _
                    "'DDI', " & CStr(sAtributo(i-1))  & ", NULL, (SELECT DDDi FROM Localidades_DDDs WHERE LocalidadeID=" & CStr(nPaisID) & ") "
        End If                    

        If ((sNode(i-1)="'UF'") AND (nFonteDadoID = 287))Then
            strSQL = strSQL & " UNION ALL SELECT " & CStr(nValidacaoDadoID) & ", " & CStr(nNivel(i-1)) & ", " & CStr(sNodeMae(i-1)) & ", " & _
                    "'Pais', " & CStr(sAtributo(i-1))  & ", " & CStr(nPaisID) & ", (SELECT Localidade FROM Localidades WHERE LocalidadeID=" & CStr(nPaisID) & ") "  	 	                        
        End If	                                                

Next

If (strSQL <> "") Then    
	rsCommand.CommandTimeout = 60 * 10
    rsCommand.ActiveConnection = strConn
    rsCommand.CommandText = strSQL
    rsCommand.CommandType = adCmdText
    rsCommand.Execute nRecsAffected
End If

Set rsCommand = Nothing

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "fldresp", adVarChar, 50, adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew
rsNew.Fields("fldresp").Value = CStr(nRecsAffected)
rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
%>

