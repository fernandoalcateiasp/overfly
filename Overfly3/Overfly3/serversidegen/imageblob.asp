
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
		
	Response.Expires = 0
	
	Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
	
	Response.Buffer = TRUE
	Response.Clear
	' Change the HTTP header to reflect that an image is being passed.
	Response.ContentType = "image/JPEG"

    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	Dim i, nFormID, nSubFormID, nRegistroID, nTamanho, nOrdem, sVersao
	Dim rsData, strSQLGet, srtSQLInsert, bytChunk
			   
	nFormID = 0
	nSubFormID = 0
	nRegistroID = 0
	nTamanho = 0
	nOrdem = 1
	sVersao = ""
		
	For i = 1 To Request.QueryString("nFormID").Count    
	  nFormID = Request.QueryString("nFormID")(i)
	Next
	
	For i = 1 To Request.QueryString("nSubFormID").Count    
	  nSubFormID = Request.QueryString("nSubFormID")(i)
	Next
			    
	For i = 1 To Request.QueryString("nRegistroID").Count    
	  nRegistroID = Request.QueryString("nRegistroID")(i)
	Next
	
    For i = 1 To Request.QueryString("sVersao").Count    
	  sVersao = Request.QueryString("sVersao")(i)
	Next
	
	For i = 1 To Request.QueryString("nTamanho").Count    
	  nTamanho = Request.QueryString("nTamanho")(i)
	Next
	
	For i = 1 To Request.QueryString("nOrdem").Count    
	  nOrdem = Request.QueryString("nOrdem")(i)
	Next
	    
	If (nTamanho = 0) Then
		'Documentos, QAF, RET
		If ((nFormID = 7110) OR (nFormID = 7180) OR (nFormID = 12220)) Then
			nTamanho = 4
		Else			
			nTamanho = 2
		End If
	End If
	
	Dim strVersao, strOrder

    strVersao = ""
    strOrder = ""
		
	If (sVersao = null OR sVersao = "") Then
	    'strVersao = "a.Versao IS NULL AND "    
        
        If (CStr(nFormID) = 7110) Then
            strOrder = "(CASE WHEN a.Versao IS NULL THEN 0 ELSE 1 END), "
        End If
	Else
	    'strVersao = "a.Versao = '" & CStr(sVersao) & "' AND "    
        strOrder = "(CASE a.Versao WHEN '" & CStr(sVersao) & "' THEN 0 ELSE 1 END), "
	End If

	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQLGet = "SELECT TOP 1 * FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] a WITH(NOLOCK) " & _
	            "WHERE a.FormID = " & CStr(nFormID) & " AND " & _
	            "a.SubFormID = " & CStr(nSubFormID) & " AND " & _
	            "a.RegistroID = " & CStr(nRegistroID) & " AND " & _
                "a.TipoArquivoID = 1451 AND " & _
				strVersao & " a.TamanhoImagem = " & CStr(nTamanho) & " AND " & _
				"a.OrdemImagem = " & CStr(nOrdem) & " " & _
				"ORDER BY " & strOrder & "a.TamanhoImagem, a.OrdemImagem, a.DocumentoID"
	
	rsData.Open strSQLGet, strConn, adOpenStatic, adLockOptimistic, adCmdText
	
	'If (rsData.RecordCount = 0) Then
    '    rsData.AddNew
    '    
    '    rsData.Fields("FormID") = nFormID
    '    rsData.Fields("SubFormID") = nSubFormID
    '    rsData.Fields("RegistroID") = nRegistroID
    '    rsData.Fields("TamanhoImagem") = nTamanho
    '    rsData.Fields("OrdemImagem") = nOrdem
    '    
    '    if(sVersao <> null AND sVersao <> "") Then
    '        rsData.Fields("Versao") = sVersao        
    '    End If
    '    
    '    rsData.Update
    '    rsData.Close
    '            
    '    rsData.Open strSQLGet, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
	'End If
		
	If (rsData.EOF) Then
		Response.BinaryWrite ""
	Else
		Response.BinaryWrite rsData.Fields("Arquivo").Value
	End If
		
	rsData.Close

	Set rsData = Nothing

	Response.Flush
	
	Response.End
%>
