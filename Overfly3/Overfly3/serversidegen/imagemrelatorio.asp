
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	Dim i, nFormID, nSubFormID, nConceitoID
	
	nFormID = 0
	nSubFormID = 0
	nConceitoID = 0
		
	For i = 1 To Request.QueryString("nFormID").Count    
		nFormID = Request.QueryString("nFormID")(i)
	Next    
	    
	For i = 1 To Request.QueryString("nSubFormID").Count    
		nSubFormID = Request.QueryString("nSubFormID")(i)
	Next
  
	For i = 1 To Request.QueryString("nConceitoID").Count    
		nConceitoID = Request.QueryString("nConceitoID")(i)
	Next    

    Dim rsData, strSQL

    Set rsData = Server.CreateObject("ADODB.Recordset")
      
    strSQL = "SELECT dbo.fn_Imagem_Resumo(" & CStr(nFormID) & ", " & _
		CStr(nSubFormID) & ", " & CStr(nConceitoID) & ") AS ImagemResumo"

	rsData.Open  strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	rsData.Save Response,adPersistXML
    rsData.Close
    Set rsData = Nothing
%>
