
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    
Dim i, nFormID, nEmpresaID, nEmpresaPaisID, nUserID, nIdiomaSistemaID, nIdiomaEmpresaID

nFormID = 0
nEmpresaID = 0
nEmpresaPaisID = 0
nUserID = 0
nIdiomaSistemaID = 0
nIdiomaEmpresaID = 0
      
For i = 1 To Request.QueryString("nFormID").Count    
    nFormID = Request.QueryString("nFormID")(i)
Next      

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaPaisID").Count    
    nEmpresaPaisID = Request.QueryString("nEmpresaPaisID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next      
      
For i = 1 To Request.QueryString("nIdiomaSistemaID").Count    
    nIdiomaSistemaID = Request.QueryString("nIdiomaSistemaID")(i)
Next      

For i = 1 To Request.QueryString("nIdiomaEmpresaID").Count    
    nIdiomaEmpresaID = Request.QueryString("nIdiomaEmpresaID")(i)
Next      
      
Dim rsData, strSQL, rsSPCommand
   
'--------------------------- As strings de pesquisa -- INICIO ------------------------

'//@@
Select Case CLng(nFormID)

    Case 1110   'Form de Recursos

		strSQL =    "SELECT '1' AS Indice,STR(CompetenciaID) AS Ordem2,Competencia AS fldName,CompetenciaID AS fldId " & _
                    " FROM Competencias WITH(NOLOCK) WHERE (TipoCompetenciaID = 1701 AND EstadoID=2) " & _
		            "ORDER BY Indice, Ordem2 "

    Case 1130   'Form de RelacoesPesRec

		strSQL =    "SELECT '1' AS Indice,STR(CompetenciaID) AS Ordem2,Competencia AS fldName,CompetenciaID AS fldId " & _
                    " FROM Competencias WITH(NOLOCK) WHERE (TipoCompetenciaID = 1701 AND EstadoID=2) " & _
		            "ORDER BY Indice, Ordem2 "

    Case 1210   'Form de Pessoas

        strSQL =    "SELECT '1' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId " & _
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID = 16 AND EstadoID=2 " & _
                    "ORDER BY Indice, Ordem2 "

    Case 1330   'Form de Localidades
    
    Case 2130:   'Form de Rela��es entre Pessoas e Conceitos
        strSQL =  "SELECT '1' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "FROM Conceitos WITH(NOLOCK) " & _
                   "WHERE EstadoID = 2 AND TipoConceitoID = 307 " & _
                   "AND PaisID = " & nEmpresaPaisID & " " & _
                   "UNION ALL SELECT '2' as Indice, c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, c.SimboloMoeda AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "FROM RelacoesPesRec a WITH(NOLOCK) " & _
						"INNER JOIN RelacoesPesRec_Moedas b WITH(NOLOCK) ON (a.RelacaoID = b.RelacaoID) " & _
						"INNER JOIN Conceitos c WITH(NOLOCK) ON (b.MoedaID = c.ConceitoID) " & _
                   "WHERE a.SujeitoID = " & nEmpresaID & " " & _
                   "AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 " & _
                   "UNION ALL SELECT '3' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "UNION ALL SELECT '3' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ISNULL(CONVERT(VARCHAR(8000),Observacoes), SPACE(0)) AS Descricao  " & _
                   "FROM Conceitos WITH(NOLOCK) " & _
                   "WHERE EstadoID = 2 AND TipoConceitoID = 309 " & _
                   "AND PaisID = " & nEmpresaPaisID & " " & _
                   "UNION ALL " & _
                   "SELECT '4' as Indice, ItemID AS fldId, ItemAbreviado AS fldName, STR(Ordem) AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ItemMAsculino AS Descricao " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE TipoID = 114 AND EstadoID = 2 " & _
                   "UNION ALL " & _
                   "SELECT '5' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "UNION ALL SELECT '5' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ISNULL(CONVERT(VARCHAR(8000),Observacoes), SPACE(0)) AS Descricao  " & _
                   "FROM Conceitos WITH(NOLOCK) " & _
                   "WHERE EstadoID = 2 AND TipoConceitoID = 319 " & _
                   "AND PaisID = " & nEmpresaPaisID & " " & _
                   "ORDER BY Indice,Ordem2"


    Case 5110   'Form de Pedidos

    Case Else
        'Nao tem c�digo
    
End Select
  
'--------------------------- As strings de pesquisa -- FIM ---------------------------

Set rsData = Server.CreateObject("ADODB.Recordset")
                          
'rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Set rsSPCommand = Server.CreateObject("ADODB.Command")

With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandText = "sp_RelatorioEstoque"
    .CommandType = adCmdStoredProc
    .Parameters.Append( .CreateParameter("@sSQL", adVarChar, adParamInput,Len(strSQL) , strSQL) )
    Set rsData = .Execute
End With

rsData.Save Response, adPersistXML

rsData.Close
Set rsData = Nothing

Set rsSPCommand = Nothing
%>