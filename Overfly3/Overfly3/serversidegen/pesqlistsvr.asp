<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%


'Funcao de uso geral, retorna o tipo de dado de um campo de tabela
Function returnFldType(ByVal tableName, ByVal fieldName)
        
    Dim retVal, rsTable, fldToExam
        
    retVal = 0

    'Transformar o fieldName de x.YYYY para YYYY
    'se for generalizar a funcao, retirar a linha
    'abaixo e coloca-la na chamada deste arquivo
    fieldName = Right(fieldName, (Len(fieldName)-2))
        
    Set rsTable = Server.CreateObject("ADODB.Recordset")

    With rsTable
        .Source = "SELECT TOP 1 * FROM " & tableName & " WITH(NOLOCK) "
        .ActiveConnection = strConn
        .CursorType = adOpenForwardOnly
        .LockType = adLockReadOnly
        .Open
    End With    
        
    For Each fldToExam In rsTable.Fields
        If (fldToExam.Name = fieldName) Then
            retVal = fldToExam.Type
            Exit For
        End If
    Next
        
    rsTable.Close
    Set rsTable = Nothing
        
    returnFldType = retVal

End Function
%>

<%
'Arquivo pesqlistsvr.asp de todos os forms
'Para cada form e necessario acrescentar um novo case onde assinalado
'por //@@

Dim rsData
Dim rsNew
Dim fldF
Dim iRec
Dim sSql
Dim strSQL
Dim strSQL2  
Dim strSQLExecAfterCreateTable
Dim nFormID
Dim sFldKey
Dim nOrder
Dim sMetPesq
Dim sTextToSearch
Dim sCondition
Dim i
Dim sOperator
Dim rsStoredProc
Dim rsSPCommand
Dim nPageNumber
Dim nPageSize
Dim startID
Dim rsERROR
Dim errorOcurr

Dim nFiltroID
Dim sFiltro

Dim nFiltroContextoID
Dim rsFiltroContexto
Dim sFiltroContexto
Dim nEmpresaID
Dim nIdiomaSistemaID
Dim nIdiomaEmpresaID
Dim nIdiomaID
Dim nArgumentoIsEmpty
Dim sSpcClause
Dim sAdditionalFrom
Dim sAdditionalWhere

Dim C1
Dim C2
Dim A1
Dim A2
Dim userID


'Selecionar apenas os registros do usuario corrente
Dim nProprietarioID
nProprietarioID = 0

'Parameto do SELECT para aplicar o direito
'Parametro do WHERE para aplicar o direito
Dim sAccessAllowed

Dim strAdditionalFrom1
Dim strAdditionalFrom2
Dim strAdditionalCondition1
Dim strAdditionalCondition2
Dim sNovaPesq

strAdditionalFrom1 = ""
strAdditionalFrom2 = ""
strAdditionalCondition1 = ""
strAdditionalCondition2 = ""
sNovaPesq = false

nFormID = ""  
sFldKey = ""
nOrder = ""
sMetPesq = ""
sTextToSearch = ""
sCondition = ""
C1 = 0
C2 = 0
sAccessAllowed = ""
nArgumentoIsEmpty = -1
sSpcClause = ""

For i = 1 To Request.QueryString("nFormID").Count    
    nFormID = Request.QueryString("nFormID")(i)
Next

For i = 1 To Request.QueryString("sFldKey").Count    
    sFldKey = Request.QueryString("sFldKey")(i)
Next

'Transformar o fldKey de xYYYY para x.YYYY
sFldKey = Left(sFldKey, 1) & "." & Right(sFldKey, (Len(sFldKey)-1))

For i = 1 To Request.QueryString("nOrder").Count    
    nOrder = Request.QueryString("nOrder")(i)
Next

'Metodo de pesquisa
'default - DEF
'comeca com - COM
'termina com TERM
'contem - CONT
For i = 1 To Request.QueryString("sMetPesq").Count    
    sMetPesq = Request.QueryString("sMetPesq")(i)
Next

'Clausula especial de pesquisa de peslist nao padroes
For i = 1 To Request.QueryString("sSpcClause").Count    
    sSpcClause = Request.QueryString("sSpcClause")(i)
Next

For i = 1 To Request.QueryString("sTextToSearch").Count    
    sTextToSearch = Request.QueryString("sTextToSearch")(i)
Next

If ( (UCase(sMetPesq) = "DEF") AND (Trim(sTextToSearch) = "") ) Then
    sTextToSearch = "0"
End If
    
For i = 1 To Request.QueryString("sCondition").Count    
    sCondition = Request.QueryString("sCondition")(i)
Next

For i = 1 To Request.QueryString("nPageNumber").Count    
    nPageNumber = Request.QueryString("nPageNumber")(i)
Next

For i = 1 To Request.QueryString("nPageSize").Count    
    nPageSize = Request.QueryString("nPageSize")(i)
Next

For i = 1 To Request.QueryString("nFiltroContextoID").Count    
    nFiltroContextoID = Request.QueryString("nFiltroContextoID")(i)
Next

For i = 1 To Request.QueryString("nFiltroID").Count    
    nFiltroID = Request.QueryString("nFiltroID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nIdiomaSistemaID").Count    
    nIdiomaSistemaID = Request.QueryString("nIdiomaSistemaID")(i)
Next

For i = 1 To Request.QueryString("nIdiomaEmpresaID").Count    
    nIdiomaEmpresaID = Request.QueryString("nIdiomaEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nIdiomaID").Count
    nIdiomaID = Request.QueryString("nIdiomaID")(i)
Next

For i = 1 To Request.QueryString("nArgumentoIsEmpty").Count    
    nArgumentoIsEmpty = Request.QueryString("nArgumentoIsEmpty")(i)
Next

'Parametros de Direito do Usu�rio

For i = 1 To Request.QueryString("userID").Count    
    userID = Request.QueryString("userID")(i)
Next

For i = 1 To Request.QueryString("nProprietarioID").Count    
    nProprietarioID = Request.QueryString("nProprietarioID")(i)
Next

'Apenas registros do usuario logado
If ( (CLng(nProprietarioID) <> 0) AND (CLng(nProprietarioID) = CLng(userID)) ) Then
	sAccessAllowed = " " & " a.ProprietarioID=" & nProprietarioID & " AND "
ElseIf ( (CLng(nProprietarioID) <> 0) AND (CLng(nProprietarioID) <> CLng(userID)) ) Then
    sAccessAllowed =  " " & " a.ProprietarioID IN (SELECT " & nProprietarioID & " UNION ALL " & _
                                                   "SELECT a.SujeitoID " & _
                                                      "FROM RelacoesPessoas a WITH(NOLOCK) " & _
                                                      "WHERE (a.TipoRelacaoID = 34) AND (a.ObjetoID = " & nProprietarioID & ") AND " & _
                                                            "(a.EstadoID = 2)) AND "
End If

For i = 1 To Request.QueryString("C1").Count    
    C1 = Request.QueryString("C1")(i)
Next

For i = 1 To Request.QueryString("C2").Count    
    C2 = Request.QueryString("C2")(i)
Next

For i = 1 To Request.QueryString("A1").Count    
    A1 = Request.QueryString("A1")(i)
Next

For i = 1 To Request.QueryString("A2").Count    
    A2 = Request.QueryString("A2")(i)
Next



C1 = CLng(C1)
C2 = CLng(C2)
A1 = CLng(A1)
A2 = CLng(A2)

If ((C2=0)AND(C1=1)) Then
    sAccessAllowed = sAccessAllowed & " a.ProprietarioID= " & userID & " AND "
ElseIf ((C2=1)AND(C1=0)) Then
    sAccessAllowed = sAccessAllowed & " " & userID & " =(CASE WHEN a.ProprietarioID=" & userID & " THEN a.ProprietarioID " & _
                                            "WHEN a.AlternativoID=" & userID & " THEN a.AlternativoID " & _
                                            "WHEN a.AlternativoID=(SELECT TOP 1 grupo.ObjetoID FROM RelacoesPessoas grupo WITH(NOLOCK) " & _
                                                                  "WHERE grupo.TipoRelacaoID=34 AND grupo.EstadoID=2 " & _
                                                                  "AND grupo.SujeitoID=" & userID & " " & _ 
                                                                  "AND grupo.ObjetoID=a.AlternativoID) THEN " & userID & " " & _
                                            "ELSE 0 " & _
                                       "END) AND "
 
ElseIf ((C2=0)AND(C1=0)) Then
    nPageSize = 0
End If    
    
If sCondition <> "" Then
    sCondition = "AND (" & sCondition & ")"
End If

startID = (nPageNumber * nPageSize) - nPageSize

Set rsData = Server.CreateObject("ADODB.Recordset")

If (nOrder = "DESC" And sTextToSearch <> "0") Then
    sOperator = " <= "
Else
    sOperator = " >= "
End If

'default - DEF
'comeca com - COM
'termina com TERM
'contem - CONT

If ( (Len(sTextToSearch) > 1) AND ((Left(sTextToSearch, 1) = "%") OR (Right(sTextToSearch, 1) = "%")) ) Then
    sOperator = " LIKE "
ElseIf (UCase(sMetPesq) = "COM") Then
    sOperator = " LIKE "
    sTextToSearch = sTextToSearch & "%"
ElseIf (UCase(sMetPesq) = "CONT") Then
    sOperator = " LIKE "
    sTextToSearch = "%" & sTextToSearch & "%"
ElseIf (UCase(sMetPesq) = "TERM") Then
    sOperator = " LIKE "
    sTextToSearch = "%" & sTextToSearch
End If

If ((nFormID = 5110) OR (nFormID = 9110)) Then
    sNovaPesq = true
End If 

If (sNovaPesq = false) Then
    sTextToSearch = "'" & sTextToSearch & "'"
End If

'--------------------------- Filtro de Contexto e Filtros  -- INICIO ----------------------------
Set rsFiltroContexto = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT 1 AS Ordem, Filtro From Recursos WITH(NOLOCK) WHERE RecursoID = " & nFiltroContextoID & " " & _
            "UNION ALL " & _
            "SELECT 2 AS Ordem, Filtro From Recursos WITH(NOLOCK) WHERE RecursoID = " & nFiltroID
            
rsFiltroContexto.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

sFiltroContexto = " "    
sFiltro = " "

If ((NOT rsFiltroContexto.BOF) AND (NOT rsFiltroContexto.EOF)) Then
    rsFiltroContexto.MoveFirst

    While Not rsFiltroContexto.EOF
        
        If ((NOT IsNull(rsFiltroContexto.Fields("Filtro").Value)) AND (rsFiltroContexto.Fields("Filtro").Value <> "") AND (rsFiltroContexto.Fields("Ordem").Value = "1")) Then
            sFiltroContexto = " AND (" & rsFiltroContexto.Fields("Filtro").Value & ") "        
        End If                    
        
        If ((NOT IsNull(rsFiltroContexto.Fields("Filtro").Value)) AND (rsFiltroContexto.Fields("Filtro").Value <> "") AND (rsFiltroContexto.Fields("Ordem").Value = "2")) Then
            sFiltro = " AND (" & rsFiltroContexto.Fields("Filtro").Value & ") "            
        End If     
        
        rsFiltroContexto.MoveNext
    Wend
End If
'--------------------------- Filtro de Contexto e Filtros -- FIM --------------------------------

Set rsSPCommand = Server.CreateObject("ADODB.Command")
  
With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandType = adCmdStoredProc
End With

'--------------------------- As strings de pesquisa -- INICIO ------------------------

strSQLExecAfterCreateTable = ""

'//@@
Select Case CLng(nFormID)

Case 1110   'formID /basico/Recursos formID

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RecursoID " & _
             "FROM Recursos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK) WHERE " & sAccessAllowed & _
             "a.EstadoID = c.RecursoID AND a.TipoRecursoID = b.ItemID AND "  & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro
             
    If ( sFldKey = "a.RecursoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RecursoID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & " a.RecursoID,c.RecursoAbreviado as Estado,a.Recurso,a.RecursoFantasia,b.ItemMasculino " & _
             "FROM Recursos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK), @TempTable z WHERE "  & _
             "a.EstadoID = c.RecursoID AND a.TipoRecursoID = b.ItemID AND "  & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RecursoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

'--------------------------- As strings de pesquisa -- INICIO ------------------------

Case 1120   'formID  /basico/relacoes/relentrerecs

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
             "FROM RelacoesRecursos a WITH(NOLOCK), TiposRelacoes b WITH(NOLOCK), Recursos c WITH(NOLOCK), Recursos d WITH(NOLOCK),Recursos e WITH(NOLOCK) WHERE " & sAccessAllowed & _
             "a.TipoRelacaoID=b.TipoRelacaoID AND a.SujeitoID=c.RecursoID AND " & _
             "a.ObjetoID=d.RecursoID AND a.EstadoID=e.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro
                 
    If ( sFldKey = "a.RelacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.RelacaoID, e.RecursoAbreviado AS Estado, b.TipoRelacao AS Relacao, a.SujeitoID, c.RecursoFantasia AS Sujeito, a.ObjetoID, d.RecursoFantasia AS Objeto " & _
             "FROM RelacoesRecursos a WITH(NOLOCK), TiposRelacoes b WITH(NOLOCK), Recursos c WITH(NOLOCK),Recursos d WITH(NOLOCK), Recursos e WITH(NOLOCK), @TempTable z " & _
             "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.SujeitoID=c.RecursoID AND a.ObjetoID=d.RecursoID AND a.EstadoID=e.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
             "AND z.IDTMP >= " & startID & _
             " ORDER BY z.IDTMP"

Case 1130   'formID  /basico/relacoes/relpessoaserecursos

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
             "FROM RelacoesPesRec a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Recursos e WITH(NOLOCK) " & _
             "WHERE " & sAccessAllowed & _
             "a.TipoRelacaoID = c.TipoRelacaoID AND " & _
             "a.EstadoID = b.RecursoID AND " & _
             "a.SujeitoID = d.PessoaID AND " & _
             "a.ObjetoID = e.RecursoID AND "
    
    'Contexto de Uso Fisico
    'Comentado em 20/03/08, a Silvia nao conseguia visualizar funcionarios desligados
    'If (nFiltroContextoID = "1131") Then
	'	strSQL = strSQL & " ( (dbo.fn_Pessoa_Empresa(a.SujeitoID, " & CStr(nEmpresaID) & ", 1, 0) = 1) OR " & _
	'		"( (SELECT COUNT(*) FROM RelacoesPesRec_Perfis WITH(NOLOCK) WHERE (a.RelacaoID=RelacaoID AND OK=1))=0)) AND "
    'End If
    
    'Contexto de Uso Juridico
    If ( (nFiltroContextoID = "1132") AND NOT ((C2=1) AND (C1=1)) ) Then
		strSQL = strSQL & "a.SujeitoID=" & CStr(nEmpresaID) & " AND "
    End If
             
    strSQL = strSQL & sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.RelacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.RelacaoID, " & _
             "b.RecursoAbreviado as Estado, " & _
             "c.TipoRelacao, a.SujeitoID, d.Fantasia as Sujeito, a.ObjetoID, e.RecursoFantasia as Objeto " & _
             "FROM RelacoesPesRec a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Recursos e WITH(NOLOCK), @TempTable z " & _
             "WHERE a.TipoRelacaoID = c.TipoRelacaoID AND a.EstadoID = b.RecursoID AND a.SujeitoID = d.PessoaID AND a.ObjetoID = e.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 1210   'formID  /basico/Pessoas

	sAdditionalFrom = ""
	sAdditionalWhere = ""
	
	If ( sFldKey = "d.Numero" ) Then
		sAdditionalFrom = ", Pessoas_Documentos d WITH(NOLOCK) "
		sAdditionalWhere = " AND a.PessoaID=d.PessoaID AND d.TipoDocumentoID IN(101,111) "
	End If
	
	If (nFiltroContextoID = 1211) Then
        sAdditionalWhere = sAdditionalWhere & " AND ((a.ClassificacaoID IS NULL) OR (dbo.fn_Direitos_TiposAuxiliares(a.ClassificacaoID, " & nEmpresaID & ", " & CStr(userID) & ", GETDATE()) > 0)) "
    End If
    
    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.PessoaID " & _
             "FROM Pessoas a WITH(NOLOCK) " & _
                "LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.ClassificacaoID = e.ItemID) " & sAdditionalFrom & _
             "WHERE " & sAccessAllowed & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & sAdditionalWhere

    If (CLng(nFiltroID) = 41006) Then
        strSQL = strSQL & " ORDER BY dtCadastro"
    ElseIf ( sFldKey = "a.PessoaID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.PessoaID"
    End If    
                 
    strSQL2 = "SELECT TOP " & nPageSize & " " & _
              "a.PessoaID,b.RecursoAbreviado as Estado,a.Nome,a.Fantasia,c.ItemMasculino AS TipoPessoa, e.ItemMasculino AS Classificacao, dbo.fn_Pessoa_Observacao(a.PessoaID) AS Observacao, dbo.fn_Pessoa_Localizacao(a.PessoaID) AS Localizacao, a.TipoPessoaID, " & _
              "(CASE WHEN " & CStr(userID) & " = a.ProprietarioID THEN 1 WHEN " & CStr(userID) & " = a.AlternativoID THEN 1 WHEN dbo.fn_Pessoa_Grupo(" & CStr(userID) & ") = a.AlternativoID THEN 1 ELSE 0 END) AS Responsavel " & _              
              "FROM Pessoas a WITH(NOLOCK) " & _ 
                "LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.ClassificacaoID = e.ItemID), " & _
              "Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
              sAdditionalFrom & _
              "WHERE a.EstadoID=b.RecursoID AND a.TipoPessoaID = c.ItemID AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro & sAdditionalWhere & " AND z.IDFromPesq=a.PessoaID " & _
              "AND z.IDTMP > " & startID & _
              " ORDER BY z.IDTMP"

Case 1220   'formID  /basico/relacoes/relpessoas

	sAdditionalFrom = ""
	sAdditionalWhere = ""
	
	If ( sFldKey = "h.Numero" ) Then
		sAdditionalFrom = "INNER JOIN Pessoas_Documentos h WITH(NOLOCK) ON (a.SujeitoID = h.PessoaID) AND (h.TipoDocumentoID IN(101,111)) "
	ElseIf ( sFldKey = "i.Numero" ) Then
		sAdditionalFrom = "INNER JOIN Pessoas_Documentos i WITH(NOLOCK) ON (a.ObjetoID = i.PessoaID) AND (i.TipoDocumentoID IN(101,111)) "
	End If

    If ( sFldKey = "a.Faturamento" ) Then
       sFldKey = " CONVERT(NUMERIC(11,2), dbo.fn_Pessoa_Totaliza(a.SujeitoID, a.ObjetoID, 1, GETDATE()-10000, GETDATE(), 1)) "
    End If    

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
             "FROM RelacoesPessoas a WITH(NOLOCK) " & _ 
                    "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                    "INNER JOIN TiposRelacoes c WITH(NOLOCK) ON (a.TipoRelacaoID = c.TipoRelacaoID) " & _ 
                    "INNER JOIN Pessoas d WITH(NOLOCK) ON (a.SujeitoID = d.PessoaID) " & _
				    "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ObjetoID = e.PessoaID) " & _
				    sAdditionalFrom & _
             "WHERE " & sAccessAllowed & " " & _         
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro 
                 
    If ( sFldKey = "a.RelacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & _
                    "a.RelacaoID, b.RecursoAbreviado as Estado, c.TipoRelacao, " & _ 
                    "a.SujeitoID, d.Fantasia as Sujeito, a.ObjetoID, e.Fantasia as Objeto, " & _
                    "dbo.fn_RelPessoa_Classificacao(a.ObjetoID, a.SujeitoID, 2, GETDATE(), 1) AS C, " & _
                    "dbo.fn_RelPessoa_Classificacao(a.ObjetoID, a.SujeitoID, 0, GETDATE(), 1) AS I, " & _
                    "dbo.fn_RelPessoa_Classificacao(a.ObjetoID, a.SujeitoID, 1, GETDATE(), 1) AS E, " & _
                    "dbo.fn_Pessoa_Totaliza(a.SujeitoID, a.ObjetoID, 18, GETDATE()-10000, GETDATE(), 0) AS diasUltimaCompra, " & _
                    "dbo.fn_Pessoa_Credito(a.SujeitoID, 3) AS LimiteCredito, " & _
                    "f.Fantasia AS Proprietario, g.Fantasia AS Alternativo, " & _
                    "dbo.fn_Empresa_Sistema(a.SujeitoID) AS nSujeitoEmpresaSistema, dbo.fn_Empresa_Sistema(a.ObjetoID) AS nObjetoEmpresaSistema " & _
                "FROM RelacoesPessoas a WITH(NOLOCK) " & _ 
                    "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _ 
                    "INNER JOIN TiposRelacoes c WITH(NOLOCK) ON (a.TipoRelacaoID = c.TipoRelacaoID) " & _ 
                    "INNER JOIN Pessoas d WITH(NOLOCK) ON (a.SujeitoID = d.PessoaID) " & _ 
                    "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ObjetoID = e.PessoaID) " & _
                    "INNER JOIN Pessoas f WITH(NOLOCK) ON (a.ProprietarioID = f.PessoaID) " & _
                    "INNER JOIN Pessoas g WITH(NOLOCK) ON (a.AlternativoID = g.PessoaID) " & _
                    "INNER JOIN @TempTable z ON (z.IDFromPesq = a.RelacaoID) " & _
                    sAdditionalFrom & _
                "WHERE " & _
                    sFldKey & sOperator & sTextToSearch & " " & _
                    sCondition & sFiltroContexto & sFiltro & " " & _
                    "AND z.IDTMP > " & startID & " " & _
                "ORDER BY z.IDTMP"

Case 5240   'formID  /basico/visitas

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.VisitaID " & _
             "FROM Visitas a WITH(NOLOCK) LEFT JOIN Pessoas c WITH(NOLOCK) ON a.PessoaID=c.PessoaID " & _
             "LEFT JOIN Prospect g WITH(NOLOCK) ON a.ProspectID=g.ProspectID " & _
             "WHERE a.EmpresaID = " & nEmpresaID & " AND " & sAccessAllowed & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro
                 
    If ( sFldKey = "a.VisitaID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.VisitaID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " a.VisitaID, b.RecursoAbreviado as Estado, d.ItemMasculino AS TipoVisita, e.ItemMasculino AS TipoVisitado, " & _
		"c.Fantasia AS Pessoa, g.Fantasia AS Prospect, a.dtVisita, a.dtVencimento, a.Cadastro, a.Pesquisa, f.Fantasia AS Vendedor,a.Observacao " & _
			"FROM Visitas a WITH(NOLOCK) INNER JOIN Recursos b ON a.EstadoID=b.RecursoID " & _
			    "LEFT JOIN Pessoas c WITH(NOLOCK) ON a.PessoaID=c.PessoaID " & _
			    "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.TipoVisitaID=d.ItemID " & _
			    "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON a.TipoVisitadoID=e.ItemID " & _
			    "INNER JOIN Pessoas f WITH(NOLOCK) ON a.ProprietarioID=f.PessoaID " & _
			    "LEFT JOIN Prospect g WITH(NOLOCK) ON a.ProspectID=g.ProspectID " & _
			    "INNER JOIN @TempTable z ON z.IDFromPesq=a.VisitaID " & _
			"WHERE " & sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltroContexto & sFiltro & _
			" AND z.IDTMP > " & startID & _
			" ORDER BY z.IDTMP"

Case 1310   'formID  /basico/tabelasaux/tiposaux

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.TipoID " & _
             "FROM TiposAuxiliares a WITH(NOLOCK) WHERE " & sAccessAllowed & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.TipoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.TipoID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & " a.TipoID,b.RecursoAbreviado as Estado,a.Tipo,a.Feminino,a.Abreviacao, " & _
		"a.Numero,a.Filtro,a.Observacao " & _
			"FROM TiposAuxiliares a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z WHERE a.EstadoID=b.RecursoID AND " & _
			sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltroContexto & sFiltro & _
			" AND z.IDFromPesq=a.TipoID AND z.IDTMP > " & startID & _
			" ORDER BY z.IDTMP"

Case 1320   'formID  /basico/tabelasaux/tiposrel

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.TipoRelacaoID " & _
             "FROM TiposRelacoes a WITH(NOLOCK) WHERE " & sAccessAllowed & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro
                 
    If ( sFldKey = "a.TipoRelacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.TipoRelacaoID"
    End If    

    strSQL2 = "SELECT TOP " & nPageSize & " " & _
              "a.TipoRelacaoID,b.RecursoAbreviado as Estado,a.TipoRelacao,c.ItemMasculino AS RelacaoEntre,d.ItemMasculino AS TipoInferencia, a.Sujeito, a.Objeto " & _
              "FROM TiposRelacoes a WITH(NOLOCK),Recursos b WITH(NOLOCK),TiposAuxiliares_Itens c WITH(NOLOCK),TiposAuxiliares_Itens d WITH(NOLOCK), @TempTable z " & _
              "WHERE a.EstadoID=b.RecursoID AND a.RelacaoEntreID = c.ItemID AND a.TipoInferenciaID = d.ItemID AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.TipoRelacaoID " & _
              "AND z.IDTMP > " & startID & _
              " ORDER BY z.IDTMP"

Case 1330   'formID  /basico/Localidades

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.LocalidadeID " & _
             "FROM Localidades a WITH(NOLOCK) WHERE " & sAccessAllowed & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro
                 
    If ( sFldKey = "a.LocalidadeID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.LocalidadeID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " a.LocalidadeID,b.RecursoAbreviado as Estado, " & _
             "a.Localidade,c.ItemMasculino as TipoLocalidade, a.CodigoLocalidade2 " & _
             "FROM Localidades a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _ 
             "WHERE a.EstadoID=b.RecursoID AND " & _
             "a.TipoLocalidadeID = c.ItemID AND " & _ 
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.LocalidadeID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 1360   'formID /modbasico/submodauxiliar/dicionario

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.TermoID " & _
             "FROM Dicionario a WITH(NOLOCK) WHERE " & sAccessAllowed & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro
                 
    If ( sFldKey = "a.TermoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.TermoID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & " a.TermoID, b.RecursoAbreviado as Estado, a.Termo, c.ItemMasculino AS Idioma " & _
             "FROM Dicionario a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z WHERE a.EstadoID=b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltroContexto & sFiltro & " " & _
             "AND a.EstadoID=b.RecursoID AND a.IdiomaID=c.ItemID AND z.IDFromPesq=a.TermoID AND z.IDTMP > " & startID & " " & _
             "ORDER BY z.IDTMP"

Case 2110   'formID  /pcm/conceitos

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.ConceitoID " & _
             "FROM Conceitos a WITH(NOLOCK) " & _
             "WHERE " & sAccessAllowed & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.ConceitoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.ConceitoID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             "a.ConceitoID,b.RecursoAbreviado AS Estado,a.Conceito,c.ItemMasculino as TipoConceito " & _
             "FROM Conceitos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.TipoConceitoID = c.ItemID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.ConceitoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 2120   'formID  /basico/relacoes/relconceitos

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
             "FROM RelacoesConceitos a WITH(NOLOCK),TiposRelacoes b WITH(NOLOCK),Conceitos c WITH(NOLOCK),Conceitos d WITH(NOLOCK),Recursos e WITH(NOLOCK) WHERE " & sAccessAllowed & _
             "a.TipoRelacaoID=b.TipoRelacaoID AND a.SujeitoID=c.ConceitoID AND " & _
             "a.ObjetoID=d.ConceitoID AND a.EstadoID=e.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.RelacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.RelacaoID, e.RecursoAbreviado AS Estado, b.TipoRelacao AS Relacao, a.SujeitoID, c.Conceito AS Sujeito, a.ObjetoID, d.Conceito AS Objeto " & _
             "FROM RelacoesConceitos a WITH(NOLOCK),TiposRelacoes b WITH(NOLOCK),Conceitos c WITH(NOLOCK),Conceitos d WITH(NOLOCK),Recursos e WITH(NOLOCK), @TempTable z " & _
             "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.SujeitoID=c.ConceitoID AND a.ObjetoID=d.ConceitoID AND a.EstadoID=e.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
             "AND z.IDTMP >= " & startID & _
             " ORDER BY z.IDTMP"

Case 2130   'formID  /basico/relacoes/relpessoaseconceitos

    'Contexto de Compra e Venda tem filtro permanente
    If (nFiltroContextoID = 2131) Then
		strAdditionalFrom1 = ", Conceitos Marcas, Conceitos Familias, " & _
				"RelacoesConceitos RelConceitos2, Conceitos Conceitos2, " & _
				"RelacoesConceitos RelConceitos1, Conceitos Conceitos1 "
       
		strAdditionalCondition1 = " AND e.MarcaID = Marcas.ConceitoID AND e.ProdutoID = Familias.ConceitoID AND " & _
			"Familias.EstadoID = 2 AND Familias.ConceitoID = RelConceitos2.SujeitoID AND " & _
			"RelConceitos2.TipoRelacaoID = 41 AND RelConceitos2.EstadoID = 2 AND " & _
			"RelConceitos2.ObjetoID = Conceitos2.ConceitoID AND Conceitos2.EstadoID = 2 AND " & _
			"Conceitos2.ConceitoID = RelConceitos1.SujeitoID AND RelConceitos1.TipoRelacaoID = 41 AND " & _
			"RelConceitos1.EstadoID = 2 AND RelConceitos1.ObjetoID = Conceitos1.ConceitoID AND " & _
			"Conceitos1.EstadoID = 2 AND " & _
			"((e.Conceito LIKE " & sTextToSearch & " ) OR " & _
			"(dbo.fn_Tradutor(Familias.Conceito, " & nIdiomaSistemaID & ", " & nIdiomaEmpresaID & ", NULL) LIKE " & sTextToSearch & " ) OR " & _
			"(dbo.fn_Tradutor(Conceitos1.Conceito, " & nIdiomaSistemaID & ", " & nIdiomaEmpresaID & ", NULL) LIKE " & sTextToSearch & " ) OR " & _
			"(dbo.fn_Tradutor(conceitos2.Conceito, " & nIdiomaSistemaID & ", " & nIdiomaEmpresaID & ", NULL) LIKE " & sTextToSearch & " ) OR " & _
			"(Marcas.Conceito LIKE " & sTextToSearch & " ) OR " & _
			"(e.Modelo LIKE " & sTextToSearch & " ) OR " & _
			"(e.Descricao LIKE " & sTextToSearch & " ) OR " & _
			"(e.PartNumber LIKE " & sTextToSearch & " ) OR " & _
			"(e.ConceitoID LIKE " & sTextToSearch & " ) ) "

        strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                 "FROM RelacoesPesCon a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK) "
                 
		If ( (Trim(sOperator) = "LIKE") AND (sFldKey = "e.Conceito") ) Then
		    strSQL = strSQL & strAdditionalFrom1
		End If

		strSQL = strSQL & " WHERE " & sAccessAllowed & "a.EstadoID = b.RecursoID AND a.TipoRelacaoID = c.TipoRelacaoID AND "  & _
                 "a.SujeitoID = d.PessoaID AND a.ObjetoID = e.ConceitoID AND " & _
                 "a.SujeitoID = " & nEmpresaID & " "

		If ( (Trim(sOperator) = "LIKE") AND (sFldKey = "e.Conceito") ) Then
		    strSQL = strSQL & strAdditionalCondition1
		Else
		    strSQL = strSQL & " AND " & sFldKey & sOperator & sTextToSearch & " "
		End If
    
		strSQL = strSQL & sCondition & sFiltroContexto & sFiltro

    Else                 
        strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                 "FROM RelacoesPesCon a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK) " & _
                 "WHERE " & sAccessAllowed & "a.EstadoID = b.RecursoID AND a.TipoRelacaoID = c.TipoRelacaoID AND "  & _
                 "a.SujeitoID = d.PessoaID AND a.ObjetoID = e.ConceitoID AND " & _
                 sFldKey & sOperator & sTextToSearch & " " & _
                 sCondition & sFiltroContexto & sFiltro
    End If
    
    If ( sFldKey = "a.RelacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
    End If    

    'Contexto de Compra e Venda tem filtro permanente
    If (nFiltroContextoID = 2131) Then
        strSQL2 ="SELECT TOP " & nPageSize & _
                 "a.RelacaoID,b.RecursoAbreviado AS Estado, c.TipoRelacao, a.SujeitoID, d.Fantasia AS Sujeito, a.ObjetoID, e.Conceito AS Objeto " & _
                 "FROM RelacoesPesCon a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK), @TempTable z "

		If ( (Trim(sOperator) = "LIKE") AND (sFldKey = "e.Conceito") ) Then
		    strSQL2 = strSQL2 & strAdditionalFrom1
		End If

        strSQL2 = strSQL2 & " WHERE a.EstadoID = b.RecursoID AND a.TipoRelacaoID = c.TipoRelacaoID AND " & _
                 "a.SujeitoID = d.PessoaID AND a.ObjetoID = e.ConceitoID AND " & _
                 "a.SujeitoID = " & nEmpresaID

		If ( (Trim(sOperator) = "LIKE") AND (sFldKey = "e.Conceito") ) Then
		    strSQL2 = strSQL2 & strAdditionalCondition1
		Else
		    strSQL2 = strSQL2 & " AND " & sFldKey & sOperator & sTextToSearch & " "
		End If
             
		strSQL2 = strSQL2 & sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
                 "AND z.IDTMP > " & startID & _
                 " ORDER BY z.IDTMP"
    Else
        strSQL2 ="SELECT TOP " & nPageSize & _
                 "a.RelacaoID,b.RecursoAbreviado AS Estado, c.TipoRelacao, a.SujeitoID, d.Fantasia AS Sujeito, a.ObjetoID, e.Conceito AS Objeto " & _
                 "FROM RelacoesPesCon a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposRelacoes c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK), @TempTable z " & _
                 "WHERE a.EstadoID = b.RecursoID AND a.TipoRelacaoID = c.TipoRelacaoID AND " & _
                 "a.SujeitoID = d.PessoaID AND a.ObjetoID = e.ConceitoID AND " & _
                 sFldKey & sOperator & sTextToSearch & " " & _
                 sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
                 "AND z.IDTMP > " & startID & _
                 " ORDER BY z.IDTMP"

    End If
    
Case 2210 'formID  /modmateriais/submodestoques/localizacoes

    If ( sFldKey = "a.LocalizacaoCodificada" ) Then
       sFldKey = " dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) "
    ElseIf ( sFldKey = "a.LocalizacaoMae" ) Then
       sFldKey = " dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoMaeID, 1) "
    End If    

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.LocalizacaoID " & _
             "FROM LocalizacoesEstoque a WITH(NOLOCK),Recursos b WITH(NOLOCK) WHERE " & sAccessAllowed & _
             "a.EmpresaID = " & nEmpresaID & " AND " & _ 
             "a.EstadoID=b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.LocalizacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.LocalizacaoID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.LocalizacaoID AS [ID], b.RecursoAbreviado AS Est, a.Localizacao AS [Localiza��o], a.Codigo, dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoID, 1) AS [Loc Codificada], " & _
             "dbo.fn_LocalizacaoEstoque_Localizacao(a.LocalizacaoMaeID, 1) AS [Localiza��o M�e], " & _
             "dbo.fn_LocalizacaoEstoque_Nivel(a.LocalizacaoID) AS [N�vel], " & _
             "dbo.fn_LocalizacaoEstoque_Util(a.LocalizacaoID) AS LU, " & _
             "a.TamanhoMinimo, a.TamanhoMaximo, a.EstoqueSeguro, a.Palete AS Palete, a.ReposicaoDiaria AS RD, " & _
             "a.ProdutosDistintos AS ProdutosDistintos, a.PaletesAdicionais AS PaletesAdicionais, " & _
             "a.Observacao AS [Observa��o] " & _
             "FROM LocalizacoesEstoque a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EmpresaID = " & nEmpresaID & " AND a.EstadoID=b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.LocalizacaoID " & _
             "AND z.IDTMP >= " & startID & _
             " ORDER BY z.IDTMP"

Case 3140 'formID  /modestrategico/subplanejamentoestrategico/orcamentos

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.OrcamentoID " & _
             "FROM Orcamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK) WHERE " & sAccessAllowed & _
             "a.EmpresaID = " & nEmpresaID & " AND " & _ 
             "a.EstadoID=b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.OrcamentoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & ", a.OrcamentoID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.OrcamentoID AS [ID], b.RecursoAbreviado AS Est, a.Ano, " & _
             "a.Observacao AS [Observa��o] " & _
             "FROM Orcamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EmpresaID = " & nEmpresaID & " AND a.EstadoID=b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.OrcamentoID " & _
             "AND z.IDTMP >= " & startID & _
             " ORDER BY z.IDTMP"
             
'---------------------------------------------------------------------------------------------------------
    'Sidnei
Case 5410: '// formID  /modcomercial/submodtransporte/Conhecimentostransporte

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.ConhecimentoID " & _
             "FROM ConhecimentosTransporte a WITH(NOLOCK), Recursos b WITH(NOLOCK) WHERE " & sAccessAllowed & _
             "a.EmpresaID = " & nEmpresaID & " AND " & _ 
             "a.EstadoID=b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.ConhecimentoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & ", a.ConhecimentoID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.ConhecimentoID AS [ID], " & _
             "b.RecursoAbreviado AS [Est], " & _
             "a.Numero, " & _
             "a.dtEmissao AS [Emissao], " & _
             "c.Fantasia AS [Transportadora] " & _
             "FROM ConhecimentosTransporte a WITH (NOLOCK) " & _
             "INNER JOIN Recursos b WITH (NOLOCK) ON (b.RecursoID = a.EstadoID) " & _
             "INNER JOIN Pessoas c WITH (NOLOCK) ON (c.PessoaID = a.TransportadoraID) " & _
             "INNER JOIN @TempTable z ON (z.IDFromPesq = a.ConhecimentoID) " & _
             "WHERE a.EmpresaID = " & nEmpresaID & " AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.OrcamentoID " & _
             "AND z.IDTMP >= " & startID & _
             " ORDER BY z.IDTMP"
             
             
          
             
             
'---------------------------------------------------------------------------------------------------------

Case 4210   'formID  /modmarketing/subinformacoesmercado/vitrines

    If( (returnFldType("Vitrines", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

	strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.VitrineID " & _
	         "FROM Vitrines a WITH(NOLOCK) " & _
	            "LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.LojaID=d.ItemID) " & _
	         "WHERE " & sAccessAllowed & _
             "a.EmpresaID = " & nEmpresaID & " AND " & _ 
	         sFldKey & sOperator & sTextToSearch & " " & _
	         sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.VitrineID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.VitrineID"
    End If    
                 
	strSQL2 = "SELECT TOP " & nPageSize & " " & _
            "a.VitrineID, b.RecursoAbreviado AS Est, a.Vitrine, c.ItemMasculino AS Tipo, d.ItemFeminino AS LojaEspecial, e.ItemFeminino AS Loja, " & _
            "f.Conceito AS Marca, a.EhRandomica AS Rand, a.dtInicio, a.dtFim, a.Observacao " & _
        "FROM Vitrines a WITH(NOLOCK) " & _
            "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID=b.RecursoID) " & _
            "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.TipoVitrineID=c.ItemID) " & _
            "LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.LojaEspecialID=d.ItemID) " & _
            "LEFT OUTER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.LojaID=e.ItemID) " & _
            "LEFT OUTER JOIN Conceitos f WITH(NOLOCK) ON (a.MarcaID=f.ConceitoID) " & _
            "INNER JOIN @TempTable z ON (z.IDFromPesq=a.VitrineID) " & _
        "WHERE a.EmpresaID = " & nEmpresaID & " AND " & _
            sFldKey & sOperator & sTextToSearch & " " & _
            sCondition & sFiltroContexto & sFiltro & " " & _
            "AND z.IDTMP > " & startID & _
        " ORDER BY z.IDTMP"

Case 4220   'Acoesmarketing

    If ( sFldKey = "a.dtVencimento" ) Then
		sFldKey = "dbo.fn_AcaoMarketing_Datas(a.AcaoID,1)"

		If ( sTextToSearch = "'0'") Then
            sTextToSearch = "0"
        End If
    
    ElseIf( (returnFldType("AcoesMarketing", sFldKey) = 135) AND ( sTextToSearch = "'0'") ) Then
        sTextToSearch = "0"
    End If

	strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.AcaoID " & _
	         "FROM AcoesMarketing a WITH(NOLOCK) INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON a.TipoAcaoID=c.ItemID WHERE " & sAccessAllowed & _
             "a.EmpresaID = " & nEmpresaID & " AND " & _ 
	         sFldKey & sOperator & sTextToSearch & " " & _
	         sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.AcaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.AcaoID"
    End If    
                 
	strSQL2 = "SELECT TOP " & nPageSize & " " & _
	          "a.AcaoID, b.RecursoAbreviado AS Est, c.Observacao AS [Grupo de Acao], c.ItemMasculino AS [Tipo de Acao], " & _
	          "a.Acao, a.Detalhe, a.Periodo, a.dtInicio, a.dtFim, dbo.fn_Acaomarketing_Datas(a.AcaoID,1) AS dtVencimento, dbo.fn_AcaoMarketing_Totais(AcaoID, 11, GETDATE()) AS Atraso, " & _
	          "a.TotalPlanejado, " & _
	          "dbo.fn_AcaoMarketing_Totais(AcaoID, 1, GETDATE()) AS TotalPatrocinio, " & _
			  "dbo.fn_AcaoMarketing_Totais(AcaoID, 2, GETDATE()) AS Diferenca, " & _
			  "dbo.fn_AcaoMarketing_Totais(AcaoID, 3, GETDATE()) AS TotalPago, " & _
			  "dbo.fn_AcaoMarketing_Totais(AcaoID, 4, GETDATE()) AS TotalRecebido, " & _
			  "dbo.fn_AcaoMarketing_Totais(AcaoID, 5, GETDATE()) AS SaldoPlanejado, " & _
			  "dbo.fn_AcaoMarketing_Totais(AcaoID, 6, GETDATE()) AS SaldoPatrocinio, " & _			  
			  "dbo.fn_AcaoMarketing_Totais(AcaoID, 7, GETDATE()) AS SaldoEfetivo, a.Observacao AS Observacao " & _
			  "FROM AcoesMarketing a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
	          "WHERE a.EmpresaID = " & nEmpresaID & " AND a.EstadoID=b.RecursoID AND a.TipoAcaoID=c.ItemID AND " & _
	          sFldKey & sOperator & sTextToSearch & " " & _
	          sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.AcaoID " & _
	          "AND z.IDTMP > " & startID & _
	          " ORDER BY z.IDTMP"	          

Case 4230   'EmailMarketing

    If( (returnFldType("EmailMarketing", sFldKey) = 135) AND ( sTextToSearch = "'0'") ) Then
        sTextToSearch = "0"
    End If

	strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.EmailID " & _
	         "FROM EmailMarketing a WITH(NOLOCK) INNER JOIN Vitrines b WITH(NOLOCK) ON (b.VitrineID = a.VitrineID) WHERE " & sAccessAllowed & _
             "a.EmpresaID = " & nEmpresaID & " AND " & _ 
	         sFldKey & sOperator & sTextToSearch & " " & _
	         sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.EmailID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & ", a.EmailID"
    End If    
                 
	strSQL2 = "SELECT TOP " & nPageSize & " " & _
	          "a.EmailID, b.RecursoAbreviado AS Est, a.ProcessoIniciado, a.Assunto, a.dtEnvio, a.dtValidade, c.Vitrine AS [Vitrine], " & _
              "d.ItemMasculino AS [Template], a.PercentualProdutosCompativeis, a.TamanhoArgumentoVenda, " & _
              "a.QuantidadeProdutos AS Produtos, " & _
              "dbo.fn_EmailMarketing_Totais(a.EmailID, NULL, 2) AS Clientes, " & _
              "dbo.fn_EmailMarketing_Totais(a.EmailID, NULL, 3) AS Contatos, " & _
              "a.QuantidadeEmail AS Emails, " & _
              "dbo.fn_EmailMarketing_Totais(a.EmailID, NULL, 6) AS PercentualProcessamento, " & _
	          "a.Observacao AS Observacao " & _
			  "FROM EmailMarketing a WITH(NOLOCK) " & _
			        "INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.EstadoID) " & _
			        "INNER JOIN Vitrines c WITH(NOLOCK) ON (c.VitrineID = a.VitrineID) " & _
			        "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (d.ItemID = a.TemplateID) " & _
			        "INNER JOIN @TempTable z ON (z.IDFromPesq = a.EmailID) " & _
	          "WHERE a.EmpresaID = " & nEmpresaID & " AND " & _
	          sFldKey & sOperator & sTextToSearch & " " & _
	          sCondition & sFiltroContexto & sFiltro & " AND z.IDTMP > " & startID & _
	          " ORDER BY z.IDTMP"	          

Case 4410   'formID  /modmarketing/subinformacoesmercado/novidades

    If( (returnFldType("Novidades", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    'Contexto de Press-Release tem filtro permanente
    If (nFiltroContextoID = 4411) Then
		strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.NovidadeID " & _
		         "FROM Novidades a WITH(NOLOCK) WHERE " & sAccessAllowed & _
                 "a.EmpresaID = " & nEmpresaID & " AND " & _ 
		         sFldKey & sOperator & sTextToSearch & " " & _
		         sCondition & sFiltroContexto & sFiltro
	Else                          
		strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.NovidadeID " & _
		         "FROM Novidades a WITH(NOLOCK) WHERE " & sAccessAllowed & _
		         sFldKey & sOperator & sTextToSearch & " " & _
		         sCondition & sFiltroContexto & sFiltro

	End If

    If ( sFldKey = "a.NovidadeID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.NovidadeID"
    End If    
                 
    'Contexto de Press-Release tem filtro permanente
    If (nFiltroContextoID = 4411) Then
		strSQL2 = "SELECT TOP " & nPageSize & " " & _
		          "a.NovidadeID,b.RecursoAbreviado as Estado,a.Data,a.Titulo, a.Fonte " & _
		          "FROM Novidades a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z " & _
		          "WHERE a.EmpresaID = " & nEmpresaID & " AND " & _ 
				  "a.EstadoID=b.RecursoID AND " & _
		          sFldKey & sOperator & sTextToSearch & " " & _
		          sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.NovidadeID " & _
		          "AND z.IDTMP > " & startID & _
		          " ORDER BY z.IDTMP"
	Else		          
		strSQL2 = "SELECT TOP " & nPageSize & " " & _
		          "a.NovidadeID,b.RecursoAbreviado as Estado,a.Data,a.Titulo, a.Fonte " & _
		          "FROM Novidades a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z " & _
		          "WHERE a.EstadoID=b.RecursoID AND " & _
		          sFldKey & sOperator & sTextToSearch & " " & _
		          sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.NovidadeID " & _
		          "AND z.IDTMP > " & startID & _
		          " ORDER BY z.IDTMP"
	End If

Case 4420   'formID  /modmarketing/subinformacoesmercado/eventos

    If( (returnFldType("Eventos", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

	strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.EventoID " & _
	         "FROM Eventos a WITH(NOLOCK) WHERE " & sAccessAllowed & _
             "a.EmpresaID = " & nEmpresaID & " AND " & _ 
	         sFldKey & sOperator & sTextToSearch & " " & _
	         sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.EventoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.EventoID"
    End If    
                 
	strSQL2 = "SELECT TOP " & nPageSize & " " & _
	          "a.EventoID,b.RecursoAbreviado as Estado, c.ItemMasculino AS Tipo, a.Evento, a.Data, a.Horario, a.Local " & _
	          "FROM Eventos a WITH(NOLOCK),Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
	          "WHERE a.EmpresaID = " & nEmpresaID & " AND " & _ 
			  "a.EstadoID=b.RecursoID AND a.TipoEventoID=c.ItemID AND " & _
	          sFldKey & sOperator & sTextToSearch & " " & _
	          sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.EventoID " & _
	          "AND z.IDTMP > " & startID & _
	          " ORDER BY z.IDTMP"

Case 4430   'formID  /modmarketing/subinformacoesmercado/pesquisas

    If( (returnFldType("Pesquisas", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

	strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.PesquisaID " & _
	         "FROM Pesquisas a WITH(NOLOCK) WHERE " & sAccessAllowed & " " & _ 
	         sFldKey & sOperator & sTextToSearch & " " & _
	         sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.PesquisaID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.PesquisaID"
    End If    
                 
	strSQL2 = "SELECT TOP " & nPageSize & " " & _
	          "a.PesquisaID,b.RecursoAbreviado as Estado, a.Pesquisa, a.Data " & _
	          "FROM Pesquisas a WITH(NOLOCK),Recursos b WITH(NOLOCK), @TempTable z " & _
	          "WHERE a.EstadoID=b.RecursoID AND " & _
	          sFldKey & sOperator & sTextToSearch & " " & _
	          sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.PesquisaID " & _
	          "AND z.IDTMP > " & startID & _
	          " ORDER BY z.IDTMP"

Case 5110   'formID  /modcomercial/subpedidos/frmpedidos

    If ( (nArgumentoIsEmpty = 1) and (sSpcClause <> "aValorTotalPedido")) Then
        sTextToSearch = ""
        sOperator = " >= "
    End If
    
    Dim sOperatorNF
    Dim sOMesmoCondition
    Dim sJoinTransportadoraOperator
    Dim sParametros
    Dim sTableNS
    Dim sTablePes_d
    Dim sTablePes_e
    Dim sConditionMain
    
    sOMesmoCondition = ""
    sTableNS = ""
    sTablePes_d = ""
    sTablePes_e = ""
    sParametros = "@TTS NVARCHAR(128), @EmpID INT, @UserID INT"
    
    'Chave de pesquisa e Nota Fiscal
    If (sSpcClause = "cNotaFiscal") Then
        sOperatorNF = " INNER JOIN "
    Else
        sOperatorNF = " LEFT OUTER JOIN "
    End If

    sConditionMain = " AND " & sFldKey & sOperator & " @TTS "

    If ( sFldKey = "a.dtChegadaPortador" ) Then
		sOMesmoCondition = " AND a.TransportadoraID=a.ParceiroID "
    End If

    If ( sFldKey = "a.NumeroSerie" ) Then
		sTableNS = "INNER JOIN dbo.fn_Pedido_NumeroSerie_tbl(@TTS) g ON (a.PedidoID = g.PedidoID) "
        sConditionMain = ""
    End If

    If ( sFldKey = "d.Fantasia" ) Then
		sTablePes_d = "INNER JOIN Pessoas d WITH(NOLOCK) ON (a.PessoaID = d.PessoaID) "
    End If

    If ( sFldKey = "e.Fantasia" ) Then
		sTablePes_e = "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID=e.PessoaID) "
    End If
    
    If ( sFldKey = "l.Data" ) Then
        sConditionMain = " AND dbo.Fn_pedido_datas(a.pedidoid, 5) " & sOperator & " @TTS "
    End If

    If ((nFiltroID <> 41000) AND (nFiltroID <> 41037)) Then
        sFiltro = sFiltro & " AND ((a.EstadoID < 25) OR (a.EstadoID >= 25 AND a.EmpresaID = ISNULL(a.FilialID, a.EmpresaID))) "
    End If

    strSQL = "SET NOCOUNT ON DECLARE @DataZero DATETIME = dbo.fn_Data_Zero(GETDATE()) DECLARE @TempTable TABLE (IDTMP int IDENTITY, IDFromPesq INT NOT NULL) "

    'Chave de pesquisa Transportadora
    If (sSpcClause = "fFantasia") Then
        sJoinTransportadoraOperator = " INNER JOIN "

            strSQL = strSQL + "DECLARE @TempTable_Prov TABLE (PedidoID INT, Fantasia VARCHAR (20)) " & _
                    "INSERT INTO @TempTable_prov (PedidoID, Fantasia) " & _
                        "SELECT a.PedidoID, f.Fantasia " & _
                            "FROM Pedidos a WITH(NOLOCK) " & _
				                "LEFT OUTER JOIN NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID)  " & _
				                sJoinTransportadoraOperator & " Pessoas f WITH(NOLOCK) ON (a.TransportadoraID = f.PessoaID)  " & _
    	                    "WHERE a.EmpresaID = @EmpID AND (a.TransportadoraID <> a.ParceiroID AND a.TransportadoraID <> a.EmpresaID) " & _
				                    sConditionMain & sCondition & sFiltroContexto & sFiltro & sOMesmoCondition

            strSQL = strSQL + "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " PedidoID FROM @TempTable_Prov f "

    Else
        sJoinTransportadoraOperator = " LEFT OUTER JOIN "

        strSQL = strSQL + "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.PedidoID " & _
                 "FROM Pedidos a WITH(NOLOCK) " & _
                    sOperatorNF & " NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) " & _
                    sJoinTransportadoraOperator & " Pessoas f WITH(NOLOCK) ON (a.TransportadoraID = f.PessoaID) " & _
                    sTablePes_d & sTablePes_e & sTableNS & _
                 "WHERE " & sAccessAllowed & " " & _
                    "a.EmpresaID = @EmpID " & _
                 sConditionMain & sCondition & sFiltroContexto & sFiltro & sOMesmoCondition
    End If

    If ( sFldKey = "a.PedidoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder

	ElseIf ( sFldKey = "a.dtPrevisaoEntrega" ) Then
       strSQL = strSQL & " ORDER BY (CONVERT(DATETIME, CONVERT(VARCHAR, a.dtPrevisaoEntrega, 103), 103)), ISNULL(a.dtChegadaPortador, @DataZero), a.dtPrevisaoEntrega " & nOrder

    'Chave alterada a pedido do helio em 13/01/2006
    ElseIf ( sFldKey = "f.Fantasia" ) Then
		strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder

    'Chave criada a pedido do helio em 13/01/2006
    ElseIf ( sFldKey = "a.dtChegadaPortador" ) Then
		strSQL = strSQL & " ORDER BY ISNULL(a.dtChegadaPortador, dbo.fn_Data_Zero(GETDATE()+1)) " & nOrder

    ElseIf ( sFldKey = "a.NumeroSerie" ) Then
		strSQL = strSQL & " ORDER BY a.PedidoID " & nOrder
    
    ElseIf ( sFldKey = "l.Data" ) Then
		strSQL = strSQL & " ORDER BY a.PedidoID " & nOrder
    
    Else
        strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.PedidoID DESC"
    End If    

    strSQL2 = "SELECT TOP " & nPageSize &  " "
    
    If (( nFiltroID = 41025 ) OR ( nFiltroID = 41037 )) AND (nEmpresaID <> 7) Then
        strSQL2 = strSQL2 & "a.PedidoID AS PedidoID2, i.VinculadoID, "
    End If
    
    strSQL2 = strSQL2 & _
			"a.PedidoID, b.RecursoAbreviado AS Estado, a.dtPedido, d.Fantasia, g.OperacaoID, c.NotaFiscal, c.dtNotaFiscal, " & _
            "a.ValorTotalPedido, " & _
            "(CASE a.TransportadoraID WHEN a.ParceiroID THEN 'O mesmo' " & _
            "WHEN a.EmpresaID THEN 'Nosso carro' " & _
            "ELSE (SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=a.TransportadoraID) END) AS Transportadora, " & _
            "a.TotalPesoBruto, a.NumeroVolumes, a.Observacao, h.Fantasia AS Deposito, " & _
            "e.Fantasia AS Colaborador, a.dtPrevisaoEntrega, dbo.fn_Pedido_Datas(a.PedidoID, 5) AS LiberacaoPedido, " & _
            "a.dtChegadaPortador, dbo.fn_Pessoa_Localizacao(a.PessoaID) AS Localizacao, " & _
            "dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 1) AS CorPedido, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 2) AS CorEstado, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 3) AS CorPessoa, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 4) AS CorValorTotal, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 5) AS CorTransportadora, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 6) AS CorPesoBruto, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 7) AS CorVolumes, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 8) AS CorColaborador, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 9) AS CorPrevisaoEntrega, " & _
			"dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 10) AS CorDeposito "

    If (( nFiltroID = 41025 ) OR ( nFiltroID = 41037 )) AND (nEmpresaID <> 7) Then
        strSQL2 = strSQL2 & ", dbo.fn_Pedido_Cor(a.PedidoID,@UserID, GETDATE(), 11) AS CorVinculado "
    End If

    strSQL2 = strSQL2 & _
        "FROM @TempTable z " & _
            "INNER JOIN Pedidos a WITH(NOLOCK) ON (z.IDFromPesq=a.PedidoID) " & _
            "LEFT OUTER JOIN Pessoas h WITH(NOLOCK) ON (a.DepositoID=h.PessoaID) " & _
            sOperatorNF & " NotasFiscais c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) " & _
            sJoinTransportadoraOperator & " Pessoas f WITH(NOLOCK) ON (a.TransportadoraID=f.PessoaID) " & _
            "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
            "INNER JOIN Pessoas d WITH(NOLOCK) ON (a.PessoaID=d.PessoaID) " & _
            "INNER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID=e.PessoaID) " & _
            "INNER JOIN Operacoes g WITH(NOLOCK) ON (a.TransacaoID = g.OperacaoID) "

    If (( nFiltroID = 41025 ) OR ( nFiltroID = 41037 )) AND (nEmpresaID <> 7) Then
        strSQL2 = strSQL2 & "LEFT JOIN Pedidos_Vinculados i WITH(NOLOCK) ON (i.PedidoID = a.PedidoID) "
    End If

    strSQL2 = strSQL2 & _
        "WHERE z.IDTMP > " & startID & " " & _
        "ORDER BY z.IDTMP"
        
        sSql = strSQL + " " + strSQL2 + " SET NOCOUNT OFF"
        
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@SQl", adVarWChar, adParamInput, 4000, sSql))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@Parametros", adVarWChar, adParamInput, 4000, sParametros))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@TTS", adVarWChar, adParamInput,128 , sTextToSearch))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@EmpID", adInteger, adParamInput,8 , nEmpresaID))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@UserID", adInteger, adParamInput,8 , userID))

Case 5120   'formID  /modcomercial/subpedidos/notasfiscais

    If( (returnFldType("NotasFiscais", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.NotaFiscalID " & _
             "FROM NotasFiscais a WITH(NOLOCK) " & _
                "INNER JOIN NotasFiscais_Pessoas c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) " & _
                                        "AND ((a.Emissor = 1 AND c.TipoID = 791) OR (a.Emissor = 0 AND c.TipoID = 790)) " & _
             "WHERE " & sAccessAllowed & " a.EmpresaID = " & nEmpresaID & " AND " & _ 
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro
                 
    If ( sFldKey = "a.NotaFiscalID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.NotaFiscalID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.NotaFiscalID, b.RecursoAbreviado AS Estado, dbo.fn_TipoAuxiliar_Item(a.DocumentoFiscalID, 2) AS DF, a.NotaFiscal, a.dtNotaFiscal, a.CFOPs, " & _
             "a.PedidoID, a.Vendedor, a.ValorTotalNota, c.Nome, a.NumeroRPS AS RPS " & _
             "FROM NotasFiscais a WITH(NOLOCK) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN NotasFiscais_Pessoas c WITH(NOLOCK) ON (c.NotaFiscalID = a.NotaFiscalID) " & _ 
                                        "AND ((a.Emissor = 1 AND c.TipoID = 791) OR (a.Emissor = 0 AND c.TipoID = 790)) " & _
                "INNER JOIN @TempTable z ON (z.IDFromPesq=a.NotaFiscalID) " & _
             "WHERE a.EmpresaID = " & nEmpresaID & " AND " & _ 
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 5130    'formID  /modcomercial/submodpedidos/cartascorrecao

    If( (returnFldType("CartasCorrecao", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.CartaCorrecaoID " & _
              "FROM CartasCorrecao a WITH(NOLOCK) " & _
                "INNER JOIN NotasFiscais b WITH(NOLOCK) ON a.NotaFiscalID = b.NotaFiscalID AND b.EstadoID=67 " & _
                "INNER JOIN Pedidos c WITH(NOLOCK) ON b.NotaFiscalID = c.NotaFiscalID " & _
                "INNER JOIN Pessoas d WITH(NOLOCK) ON c.PessoaID = d.PessoaID " & _
              "WHERE " & sAccessAllowed & " a.EmpresaID= " & nEmpresaID & " AND " & _              
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.CartaCorrecaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.CartaCorrecaoID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.CartaCorrecaoID, e.RecursoAbreviado as Estado, a.dtCartaCorrecao, a.AjusteCartaPendente, a.SequencialEvento, b.NotaFiscal, b.dtNotaFiscal, " & _
             "c.PedidoID, d.Fantasia, a.Observacao " & _
             "FROM CartasCorrecao a WITH(NOLOCK) " & _ 
                "INNER JOIN NotasFiscais b WITH(NOLOCK) ON a.NotaFiscalID = b.NotaFiscalID AND b.EstadoID=67 " & _ 
                "INNER JOIN Pedidos c WITH(NOLOCK) ON b.NotaFiscalID = c.NotaFiscalID " & _ 
                "INNER JOIN Pessoas d WITH(NOLOCK) ON c.PessoaID = d.PessoaID " & _ 
                "INNER JOIN Recursos e WITH(NOLOCK) ON a.EstadoID = e.RecursoID " & _ 
                "INNER JOIN @TempTable z ON z.IDFromPesq=a.CartaCorrecaoID " & _
             "WHERE a.EmpresaID= " & nEmpresaID & " AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 5140   'formID  /modcomercial/subpedidos/operacoes
   
    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.OperacaoID " & _
              "FROM Operacoes a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.OperacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.OperacaoID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             "a.OperacaoID,b.RecursoAbreviado AS Estado,a.Operacao,c.ItemMasculino " & _
             "FROM Operacoes a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.TipoOperacaoID = c.ItemID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.OperacaoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 5150   'formID  /modcomercial/subpedidos/DPA

    If( (returnFldType("DPA", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

	Dim sClienteOperator
	
	If ( sFldKey = "d.Fantasia" ) Then
		sClienteOperator = " INNER JOIN "
	Else
		sClienteOperator = " LEFT OUTER JOIN "
	End If
	
    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.DPAID " & _
             "FROM DPA a WITH(NOLOCK) " & _
                sClienteOperator & " Pessoas d WITH(NOLOCK) ON (a.ClienteID = d.PessoaID) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN Pessoas c WITH(NOLOCK) ON (a.FornecedorID = c.PessoaID) " & _
                "INNER JOIN Conceitos e WITH(NOLOCK) ON (a.MoedaID = e.ConceitoID) " & _
                "INNER JOIN Pessoas f WITH(NOLOCK) ON (a.ContatoID = f.PessoaID) " & _
             "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.DPAID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.DPAID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & _
             "a.DPAID, b.RecursoAbreviado AS Estado, a.DPA, c.Fantasia AS Fornecedor, d.Fantasia AS Cliente, " & _
             "dbo.fn_ProgramaMarketing_Identificador(a.FornecedorID, a.ClienteID) AS Identificador, a.dtEmissao, a.dtVencimento, " & _
             "e.SimboloMoeda AS Moeda, dbo.fn_DPA_Totais(a.DPAID, NULL, 1) AS Total, dbo.fn_DPA_Totais(a.DPAID, NULL, 2) AS Saldo, " & _
             "f.Fantasia AS Contato, a.Custo, a.SolicitaAprovacao, a.Observacao " & _
             "FROM DPA a WITH(NOLOCK), Recursos b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Conceitos e WITH(NOLOCK), Pessoas f WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.FornecedorID = c.PessoaID AND a.ClienteID " & sClienteOperator & _
             " d.PessoaID AND " & _
				"a.MoedaID = e.ConceitoID AND a.ContatoID = f.PessoaID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.DPAID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 5160   'formID  /modcomercial/subpedidos/Campanhas

	Dim sClienteOperator2, sFornecedorOperador
	
    If( (returnFldType("Campanhas", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

	sClienteOperator2 = ""
	sFornecedorOperador = ""
	
	If ( sFldKey = "c.Fantasia" ) Then
		sFornecedorOperador = " INNER JOIN "
	Else
		sFornecedorOperador = " LEFT OUTER JOIN "
	End If

	If ( sFldKey = "d.Fantasia" ) Then
		sClienteOperator2 = " INNER JOIN "
	Else
		sClienteOperator2 = " LEFT OUTER JOIN "
	End If
	
    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.CampanhaID " & _
             "FROM Campanhas a WITH(NOLOCK) " & _
                "LEFT OUTER JOIN Pessoas f WITH(NOLOCK) ON (a.ContatoID = f.PessoaID) " & _
                sFornecedorOperador & " Pessoas c WITH(NOLOCK) ON (a.FornecedorID = c.PessoaID) " & _
                sClienteOperator2 & " Pessoas d WITH(NOLOCK) ON (a.ClienteID = d.PessoaID) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN Conceitos e WITH(NOLOCK) ON (a.MoedaID = e.ConceitoID) " & _
             "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.CampanhaID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.CampanhaID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
				"a.CampanhaID, b.RecursoAbreviado AS Estado, a.dtEmissao, a.Campanha, a.Codigo, " & _
				"c.Fantasia AS Fornecedor, f.Fantasia AS Contato, d.Fantasia AS Cliente, " & _
				"dbo.fn_ProgramaMarketing_Identificador(a.FornecedorID, a.ClienteID) AS Identificador, " & _
				"a.dtInicio, a.dtFim, e.SimboloMoeda AS Moeda, a.ValorTotal, dbo.fn_Campanha_Totais(a.CampanhaID, NULL, 2) AS Saldo, a.PedidosPorCliente, " & _
				"a.CampanhaInterna AS [Int], a.SolicitaAprovacao, a.Custo, a.RebatePermiteDPA, a.Observacao " & _
             "FROM Campanhas a WITH(NOLOCK) " & _
                "LEFT OUTER JOIN Pessoas f WITH(NOLOCK) ON (a.ContatoID = f.PessoaID) " & _
                sFornecedorOperador & " Pessoas c WITH(NOLOCK) ON (a.FornecedorID = c.PessoaID) " & _
                sClienteOperator2 & " Pessoas d WITH(NOLOCK) ON (a.ClienteID = d.PessoaID) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN Conceitos e WITH(NOLOCK) ON (a.MoedaID = e.ConceitoID) " & _
                "INNER JOIN @TempTable z  ON (z.IDFromPesq=a.CampanhaID) " & _
             "WHERE " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 5220    'formID  /modcomercial/subatendimento/listaprecos
    'Nota para Listas de Precos
    'sFldKey -> usado para Ordem
    'sTextToSearch -> usado como palavra chave para LIKE

	Dim lstPre_ParceiroID, lstPre_ListaPreco, lstPre_PessoaID, lstPre_UFID, lstPre_ImpostosIncidencia, lstPre_Frete, lstPre_TipoPessoa
	Dim lstPre_FinanciamentoID, lstPre_Quantidade, lstPre_MoedaConversaoID, lstPre_Estoque, lstPre_TransacaoID, lstPre_FinalidadeID
	Dim lstPre_ProdutoReferenciaID, lstPre_TipoRelacaoID,nLotPedItemID
	Dim sSQLPrecoZero, sSQLMC, sSQLEstoqueDisp1, sSQLEstoqueDisp2, sSQLEstoqueReserva, sSQLEstoqueEquipe, sSQLDisponibilidade
	Dim sStrInsertEstoque, sStrTop, sStrDeleteEstoque, sStrSelectEstoque, sSQLPMP, sStrCFOP

	lstPre_ParceiroID = "NULL"
	For i = 1 To Request.QueryString("lstPre_ParceiroID").Count    
		lstPre_ParceiroID = Request.QueryString("lstPre_ParceiroID")(i)
	Next
	
	lstPre_ListaPreco = "1"
	For i = 1 To Request.QueryString("lstPre_ListaPreco").Count    
		lstPre_ListaPreco = Request.QueryString("lstPre_ListaPreco")(i)
	Next

	lstPre_PessoaID = "NULL"
	For i = 1 To Request.QueryString("lstPre_PessoaID").Count    
		lstPre_PessoaID = Request.QueryString("lstPre_PessoaID")(i)
	Next

	lstPre_UFID = "NULL"
	For i = 1 To Request.QueryString("lstPre_UFID").Count    
		lstPre_UFID = Request.QueryString("lstPre_UFID")(i)
	Next

	lstPre_ImpostosIncidencia = "1"
	For i = 1 To Request.QueryString("lstPre_ImpostosIncidencia").Count    
		lstPre_ImpostosIncidencia = Request.QueryString("lstPre_ImpostosIncidencia")(i)
	Next

	lstPre_Frete = "1"
	For i = 1 To Request.QueryString("lstPre_Frete").Count    
		lstPre_Frete = Request.QueryString("lstPre_Frete")(i)
	Next

	lstPre_FinanciamentoID = "2"
	For i = 1 To Request.QueryString("lstPre_FinanciamentoID").Count    
		lstPre_FinanciamentoID = Request.QueryString("lstPre_FinanciamentoID")(i)
	Next

	lstPre_Quantidade = "1"
	For i = 1 To Request.QueryString("lstPre_Quantidade").Count    
		lstPre_Quantidade = Request.QueryString("lstPre_Quantidade")(i)
	Next

	lstPre_MoedaConversaoID = "NULL"
	For i = 1 To Request.QueryString("lstPre_MoedaConversaoID").Count    
		lstPre_MoedaConversaoID = Request.QueryString("lstPre_MoedaConversaoID")(i)
	Next
	
	lstPre_Estoque = "0"
	For i = 1 To Request.QueryString("lstPre_Estoque").Count    
		lstPre_Estoque = Request.QueryString("lstPre_Estoque")(i)
	Next
	
	lstPre_TransacaoID = "0"
	For i = 1 To Request.QueryString("lstPre_TransacaoID").Count    
		lstPre_TransacaoID = Request.QueryString("lstPre_TransacaoID")(i)
	Next
	
    If (lstPre_TransacaoID = "0") Then
        lstPre_TransacaoID = "NULL"
	End If
	
	lstPre_FinalidadeID = "0"
	For i = 1 To Request.QueryString("lstPre_FinalidadeID").Count    
		lstPre_FinalidadeID = Request.QueryString("lstPre_FinalidadeID")(i)
	Next
	
	lstPre_TipoPessoa = "NULL"
	For i = 1 To Request.QueryString("lstPre_TipoPessoa").Count    
		lstPre_TipoPessoa = Request.QueryString("lstPre_TipoPessoa")(i)
	Next
	
    If (lstPre_FinalidadeID = "0") Then
        lstPre_FinalidadeID = "NULL"
	End If

	lstPre_ProdutoReferenciaID = "0"
	For i = 1 To Request.QueryString("lstPre_ProdutoReferenciaID").Count    
		lstPre_ProdutoReferenciaID = Request.QueryString("lstPre_ProdutoReferenciaID")(i)
	Next
	
	lstPre_TipoRelacaoID = "0"
	For i = 1 To Request.QueryString("lstPre_TipoRelacaoID").Count    
		lstPre_TipoRelacaoID = Request.QueryString("lstPre_TipoRelacaoID")(i)
	Next

'Inicio Insert table
    strAdditionalFrom1 = ", Conceitos Marcas WITH(NOLOCK), Conceitos Familias WITH(NOLOCK), " & _
		"RelacoesConceitos RelConceitos2 WITH(NOLOCK), Conceitos Conceitos2 WITH(NOLOCK), " & _
		"RelacoesConceitos RelConceitos1 WITH(NOLOCK), Conceitos Conceitos1 WITH(NOLOCK) "
       
    strAdditionalCondition1 = " AND b.MarcaID = Marcas.ConceitoID AND b.ProdutoID = Familias.ConceitoID AND " & _
					 "Familias.EstadoID = 2 AND Familias.ConceitoID = RelConceitos2.SujeitoID AND " & _
					 "RelConceitos2.TipoRelacaoID = 41 AND RelConceitos2.EstadoID = 2 AND " & _
					 "RelConceitos2.ObjetoID = Conceitos2.ConceitoID AND Conceitos2.EstadoID = 2 AND " & _
					 "Conceitos2.ConceitoID = RelConceitos1.SujeitoID AND RelConceitos1.TipoRelacaoID = 41 AND " & _
					 "RelConceitos1.EstadoID = 2 AND RelConceitos1.ObjetoID = Conceitos1.ConceitoID AND " & _
					 "Conceitos1.EstadoID = 2 "

    If (lstPre_ProdutoReferenciaID = "0") Then
        strAdditionalFrom2 = ""

        strAdditionalCondition2 = " AND ((b.Conceito LIKE " & sTextToSearch & " ) OR " & _
                         "(dbo.fn_Tradutor(Familias.Conceito, " & nIdiomaSistemaID & ", " & nIdiomaEmpresaID & ", NULL) LIKE " & sTextToSearch & " ) OR " & _
                         "(dbo.fn_Tradutor(Conceitos1.Conceito, " & nIdiomaSistemaID & ", " & nIdiomaEmpresaID & ", NULL) LIKE " & sTextToSearch & " ) OR " & _
                         "(dbo.fn_Tradutor(conceitos2.Conceito, " & nIdiomaSistemaID & ", " & nIdiomaEmpresaID & ", NULL) LIKE " & sTextToSearch & " ) OR " & _
                         "(Marcas.Conceito LIKE " & sTextToSearch & " ) OR " & _
                         "(b.Modelo LIKE " & sTextToSearch & " ) OR " & _
                         "(b.Descricao LIKE " & sTextToSearch & " ) OR " & _
                         "(b.PartNumber LIKE " & sTextToSearch & " ) OR " & _
                         "(a.ObjetoID LIKE " & sTextToSearch & " ) ) "
    Else
        strAdditionalFrom2 = ", RelacoesConceitos Referencias WITH(NOLOCK) "

        strAdditionalCondition2 = " AND (Referencias.EstadoID = 2 AND (Referencias.ObjetoID = " & CStr(lstPre_ProdutoReferenciaID) & " AND Referencias.SujeitoID = b.ConceitoID) OR " & _
            "(Referencias.SujeitoID = " & CStr(lstPre_ProdutoReferenciaID) & " AND Referencias.ObjetoID = b.ConceitoID)) "

        If (lstPre_TipoRelacaoID <> "0") Then
            strAdditionalCondition2 = strAdditionalCondition2 & " AND (Referencias.TipoRelacaoID = " & CStr(lstPre_TipoRelacaoID) & ") "
        End If
    End If
    
    sStrInsertEstoque = " 0 "
    sStrTop = " TOP " & CStr((nPageSize * nPageNumber)) & " "
    sStrDeleteEstoque = " "
    sStrSelectEstoque =  " dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, NULL, NULL, NULL, 375, NULL) "
    
    If (CInt(lstPre_Estoque) = 1) Then            
		sStrInsertEstoque = " dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, NULL, NULL, NULL, 375, NULL) "		
		sStrTop = " "		
		sStrDeleteEstoque = " DELETE FROM @TempTable WHERE Estoque <= 0 "		
		sStrSelectEstoque = "Estoque"		
	End If

    strSQLExecAfterCreateTable = ", EmpresaID INT, Conceito VARCHAR(25), Estoque INT"

    strSQL = "INSERT INTO @TempTable (IDFromPesq, EmpresaID, Conceito, Estoque) SELECT DISTINCT " & sStrTop & " a.ObjetoID, a.SujeitoID, b.Conceito, " & sStrInsertEstoque & " " & _
             "FROM RelacoesPesCon a WITH(NOLOCK), Conceitos b WITH(NOLOCK), Recursos c WITH(NOLOCK), Conceitos d WITH(NOLOCK) "

    If ( Trim(sOperator) = "LIKE" ) Then
        strSQL = strSQL & strAdditionalFrom1
    End If

    strSQL = strSQL & strAdditionalFrom2

    strSQL = strSQL &  " WHERE " & sAccessAllowed & "(a.SujeitoID IN " & _
        "(SELECT " & CStr(nEmpresaID) & " " & _
        " UNION ALL SELECT EmpresaAlternativaID FROM FopagEmpresas WITH(NOLOCK) WHERE (EmpresaID = " & CStr(nEmpresaID) & " AND " & _
        "FuncionarioID IS NULL) AND (EmpresaAlternativaID = dbo.fn_Empresa_Matriz(EmpresaAlternativaID, 1, 1))) AND " & _
        "a.ObjetoID=b.ConceitoID AND " & _
        "a.EstadoID=c.RecursoID AND a.MoedaSaidaID=d.ConceitoID) "
        
    If (sSpcClause <> "") Then
        strSQL = strSQL & sSpcClause & " "
    End If    
             
    If (Trim(sOperator) = "LIKE") Then
        strSQL = strSQL & strAdditionalCondition1 & strAdditionalCondition2
    Else
        strSQL = strSQL & strAdditionalCondition2 & " AND " & sFldKey & sOperator & sTextToSearch & " "
    End If
    
    strSQL = strSQL & sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.ObjetoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.ObjetoID"
    End If
    
    strSQL = strSQL & sStrDeleteEstoque
'Fim Insert Table

'Inicio Select table
    Dim sPessoa, sPessoa2, sParceiroID, sUltimoPreco, nIncluiFrete, sMeioTransporteID
    
    sPessoa2 = "NULL"
    sParceiroID = "NULL"
    
    If (CInt(lstPre_PessoaID) > 0) Then
		sPessoa = lstPre_PessoaID
		sPessoa2 = lstPre_PessoaID
		sParceiroID = lstPre_ParceiroID
	ElseIf (CInt(lstPre_UFID) > 0) Then		
		sPessoa = CStr(CInt(lstPre_UFID) * -1)
	Else
		sPessoa = "NULL"
	End If	
	             
    If (CInt(lstPre_Frete) = 0) Then
		nIncluiFrete = 0
		sMeioTransporteID = "NULL"
	ElseIf (CInt(lstPre_Frete) = 1) Then		
		nIncluiFrete = 1
		sMeioTransporteID = "NULL"
	Else
		nIncluiFrete = 1
		sMeioTransporteID = lstPre_Frete
	End If	

    If ((CInt(nPageSize) <= 10) AND (CInt(lstPre_PessoaID) > 0) AND (CInt(lstPre_ParceiroID) > 0)) Then
		sUltimoPreco = "dbo.fn_Produto_PessoaUltimoPreco(a.SujeitoID, a.ObjetoID, " & lstPre_PessoaID & ", " & lstPre_ParceiroID & ", " & nIdiomaID & ")"
	Else
		sUltimoPreco = "NULL"
	End If
	
	sStrCFOP = "dbo.fn_EmpresaPessoaTransacao_CFOP(a.SujeitoID, " & sPessoa2 & ",NULL,NULL,NULL,NULL,NULL," & lstPre_TransacaoID & ", " & lstPre_FinalidadeID & ", a.ObjetoID, NULL)"

    If (CLng(nEmpresaID) = 7) Then
        sSQLPrecoZero = "ISNULL(dbo.fn_Preco_Preco(a.SujeitoID,a.ObjetoID,NULL,NULL,0,NULL,NULL,GetDate(),NULL,NULL,1,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, NULL,NULL, NULL),0) AS PrecoZero, "
        sSQLEstoqueEquipe = "STR(dbo.fn_Produto_EstoqueEquipe(a.SujeitoID, a.ObjetoID, -" & CStr(userID) & ", 4),6,0) AS EstoqueEquipe, "
    Else        
        sSQLPrecoZero = "NULL AS PrecoZero, "
        sSQLEstoqueEquipe = "NULL AS EstoqueEquipe, "
    End If         

    If (CInt(nPageSize) <= 1) Then         
        sSQLMC = "dbo.fn_Preco_MargemLista(a.SujeitoID, a.ObjetoID, " & lstPre_Quantidade & ", " & _
            "dbo.fn_Preco_PrecoLista(a.SujeitoID,a.ObjetoID,NULL,dbo.fn_Produto_Aliquota(a.SujeitoID, a.ObjetoID," & sPessoa & ",NULL,NULL,NULL,NULL, " & CStr(sStrCFOP) & "), " & _
            lstPre_MoedaConversaoID & ", GETDATE(), " & lstPre_ListaPreco & ", " & lstPre_ImpostosIncidencia & ", " & lstPre_Quantidade & ", " & _
            "0," & sPessoa2 & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL," & sParceiroID & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, 1,NULL,NULL, NULL, NULL," & lstPre_FinanciamentoID & ", " & nIncluiFrete & ", " & sMeioTransporteID & ", NULL, " + lstPre_FinalidadeID + ",NULL), " & _
            "NULL,NULL,dbo.fn_Produto_Aliquota(a.SujeitoID, a.ObjetoID," & sPessoa & ",NULL,NULL,NULL,NULL, " & CStr(sStrCFOP) & "), " & _
            lstPre_MoedaConversaoID & ", GETDATE(), " & lstPre_ImpostosIncidencia & ", " & sPessoa2 & "," & sParceiroID & ", " & lstPre_FinanciamentoID & ", " & _
            nIncluiFrete & ", " & sMeioTransporteID & ", NULL, " & CStr(lstPre_FinalidadeID) & "," & CStr(sStrCFOP) & ", 1 ," & CStr(nLotPedItemID) & ", NULL) AS MC, "

        sSQLEstoqueDisp1 = "STR(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, a.SujeitoID, NULL, NULL, 375, NULL),6,0) AS Disponivel1, "
        sSQLEstoqueDisp2 = "STR(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, -356, -1, NULL, NULL, 375, NULL),6,0) AS Disponivel2, "
        sSQLEstoqueReserva = "STR(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 353, NULL, NULL, NULL, 375, NULL),6,0) AS Reserva, "
        sSQLDisponibilidade = "dbo.fn_Produto_Disponibilidade(a.SujeitoID,a.ObjetoID,NULL,GETDATE(),NULL, " & CStr(nIdiomaEmpresaID) & ",1) AS dtPrevisaoDisponibilidade, "
        sSQLPMP = "ISNULL(dbo.fn_Empresa_Financiamento(a.SujeitoID, " & lstPre_FinanciamentoID & ", " & lstPre_MoedaConversaoID & ", 1), 0) AS PMP, "
    Else
        sSQLMC = "NULL AS MC, "
        sSQLEstoqueDisp1 = "NULL AS Disponivel1, "
        sSQLEstoqueDisp2 = "NULL AS Disponivel2, "
        sSQLEstoqueReserva = "NULL AS Reserva, "
        sSQLDisponibilidade = "NULL AS dtPrevisaoDisponibilidade, "
        sSQLPMP = "NULL AS PMP, "
    End If

	strSQL2 ="SELECT TOP " & nPageSize & _
             "b.ConceitoID AS ConceitoID, b.Conceito AS Conceito, f.Fantasia AS Empresa, c.RecursoAbreviado AS Estado, " & _
             sSQLPrecoZero              
             
    strSQL2 = strSQL2 & "ISNULL(dbo.fn_Preco_PrecoLista(a.SujeitoID,a.ObjetoID,NULL,dbo.fn_Produto_Aliquota(a.SujeitoID, a.ObjetoID," & sPessoa & ",NULL,NULL,NULL,NULL," & CStr(sStrCFOP) & "), " & _
					lstPre_MoedaConversaoID & ", GETDATE(), " & lstPre_ImpostosIncidencia & ", " & lstPre_ListaPreco & ", " & lstPre_Quantidade & ", " & _
					"0," & sPessoa2 & "," & sParceiroID & ",NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL, 1, NULL,NULL,NULL, NULL,NULL, " & lstPre_FinanciamentoID & ", " & nIncluiFrete & ", " & sMeioTransporteID & ", NULL," + lstPre_FinalidadeID + "," + (sStrCFOP) + "," +CStr(nLotPedItemID)+ " ),0) AS Preco, " & _
             "dbo.fn_Produto_Aliquota(a.SujeitoID, a.ObjetoID," & sPessoa & ", NULL," & CStr(sStrCFOP) & ") AS Imp, " & _
             sSQLMC
             
    strSQL2 = strSQL2 & "CONVERT(INT, 0) AS QuantidadeComprar, dbo.fn_Produto_PesosMedidas(b.ConceitoID, NULL, 10) AS QCx, " & _
             "STR("& sStrSelectEstoque &",6,0) AS Disponivel, " & _
             sSQLEstoqueDisp1 & sSQLEstoqueDisp2 & sSQLEstoqueReserva & sSQLEstoqueEquipe & sSQLDisponibilidade
    
    strSQL2 = strSQL2 & "a.Observacao AS Observacao, h.Conceito AS [LinhaProduto1], b.Descricao AS [Descricao1], " & _
		 sUltimoPreco & " AS [�ltima compra], " & sStrCFOP & " AS CFOP, g.ClassificacaoFiscal AS NCM, " & _
		 "d.Conceito AS ConceitoConcreto, e.Conceito AS Marca, b.Modelo, h.Conceito AS [LinhaProduto2], b.Descricao AS [Descricao2], " & sSQLPMP & "f.PessoaID AS EmpresaID, " & _
		 "a.SujeitoID, a.ObjetoID " & _
         "FROM RelacoesPesCon a WITH(NOLOCK) " & _
            "INNER JOIN Conceitos b WITH(NOLOCK) ON (a.ObjetoID = b.ConceitoID) " & _
            "INNER JOIN Recursos c WITH(NOLOCK) ON (a.EstadoID=c.RecursoID) " & _ 
            "INNER JOIN Conceitos d WITH(NOLOCK) ON (b.ProdutoID=d.ConceitoID) " & _ 
            "INNER JOIN Conceitos e WITH(NOLOCK) ON (b.MarcaID=e.ConceitoID) " & _ 
            "INNER JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID=a.SujeitoID) " & _
            "LEFT OUTER JOIN Conceitos g WITH(NOLOCK) ON (g.ConceitoID=a.ClassificacaoFiscalID) " & _
            "LEFT OUTER JOIN Conceitos h WITH(NOLOCK) ON (h.ConceitoID = b.LinhaProdutoID)" & _
            "INNER JOIN @TempTable z ON (z.IDFromPesq=a.ObjetoID AND z.EmpresaID=a.SujeitoID) "

    'strSQL2 = strSQL2 & " WHERE (a.SujeitoID IN " & _
    '    "(SELECT " & CStr(nEmpresaID) & " " & _
    '    " UNION ALL SELECT EmpresaAlternativaID FROM FopagEmpresas WHERE (EmpresaID = " & CStr(nEmpresaID) & " AND FuncionarioID IS NULL))) "

    strSQL2 = strSQL2 & " WHERE (1=1) "

    If (sSpcClause <> "") Then
        strSQL2 = strSQL2 & sSpcClause & " "
    End If

    strSQL2 = strSQL2 & sCondition & sFiltroContexto & sFiltro & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 6110    ' modindustrial/submodproducao/ordensproducao

    Dim sOperatorOP_NF
    sOperatorOP_NF = " LEFT OUTER JOIN "
    
    If( (returnFldType("OrdensProducao", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    ElseIf( (returnFldType("Pedidos", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If
    
    If ( sFldKey = "a.NumeroSerie" ) Then
		sFldKey = "dbo.fn_Pedido_NumeroSeriePesquisa(-1 * a.OrdemProducaoID, " & sTextToSearch & ")"
		sOperator = "="
		sTextToSearch="1"
	ElseIf ( sFldKey = "d.NotaFiscal" ) Then
		sOperatorOP_NF = " INNER JOIN "
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.OrdemProducaoID " & _
              "FROM OrdensProducao a WITH(NOLOCK) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN Pedidos c WITH(NOLOCK) ON (a.PedidoID = c.PedidoID) " & _
                sOperatorOP_NF & " NotasFiscais d WITH(NOLOCK) ON (c.NotaFiscalID = d.NotaFiscalID) " & _
                "INNER JOIN Pessoas f WITH(NOLOCK) ON (c.PessoaID=f.PessoaID) " & _
              "WHERE " & sAccessAllowed & " " & _
              "c.EmpresaID = " & CStr(nEmpresaID) & " AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.OrdemProducaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.OrdemProducaoID"
    End If    
                 
     strSQL2 ="SELECT TOP " & nPageSize & _
             "a.OrdemProducaoID, b.RecursoAbreviado as Estado, a.Suspenso, a.ProcessoIniciado, " & _
			 "dbo.fn_OrdemProducao_Sincronizada(a.OrdemProducaoID) AS Sincronizada, a.dtOP, a.PedidoID, e.RecursoAbreviado AS EstadoPedido, f.Fantasia AS Parceiro, d.NotaFiscal AS Nota, " & _
			 "c.dtPrevisaoEntrega, NULL as TempoUtilProducao, NULL AS dtPrevisaoSistema, " & _
			 "g.Fantasia AS Colaborador, " & _
			 "(CASE " & _
				"WHEN (a.EstadoID <> 124 AND c.dtPrevisaoEntrega < GETDATE()) THEN 1 " & _
				"WHEN (a.EstadoID <> 124 AND dbo.fn_Data_Zero(c.dtPrevisaoEntrega) = dbo.fn_Data_Zero(GETDATE())) THEN 2 " & _
				"ELSE 0 END) AS Vencida " & _
             "FROM OrdensProducao a WITH(NOLOCK) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN Pedidos c WITH(NOLOCK) ON (a.PedidoID = c.PedidoID) " & _
                sOperatorOP_NF & " NotasFiscais d WITH(NOLOCK) ON (c.NotaFiscalID = d.NotaFiscalID) " & _
                "INNER JOIN Recursos e WITH(NOLOCK) ON (c.EstadoID = e.RecursoID) " & _
                "INNER JOIN Pessoas f WITH(NOLOCK) ON (c.PessoaID = f.PessoaID) " & _
                "INNER JOIN Pessoas g WITH(NOLOCK) ON (a.ProprietarioID=g.PessoaID) " & _
                "INNER JOIN @TempTable z ON (z.IDFromPesq=a.OrdemProducaoID) " & _
             "WHERE " & sAccessAllowed & _
			 "c.EmpresaID = " & CStr(nEmpresaID) & " AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 7110    ' modqualidade/submodiso/documentos      

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.DocumentoID " & _
              "FROM Documentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK) " & _
              "WHERE a.EstadoID = b.RecursoID AND a.ItemNormaID = c.ItemID AND a.TipoDocumentoID = d.ItemID AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.DocumentoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.DocumentoID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.DocumentoID, b.RecursoAbreviado as Estado, a.Documento, a.DocumentoAbreviado, d.ItemAbreviado AS TipoDocumento, c.ItemAbreviado AS Norma, " & _
             "a.Versao, a.dtEmissao, a.Observacao, e.Fantasia " & _
             "FROM Documentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK), Pessoas e WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.ItemNormaID = c.ItemID AND a.TipoDocumentoID = d.ItemID AND a.ProprietarioID = e.PessoaID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.DocumentoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 7120    ' modqualidade/submodiso/riq      

    If( (returnFldType("RIQ", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RIQID " & _
              "FROM RIQ a WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK) " & _
              "WHERE a.EmpresaID = " & CStr(nEmpresaID) & " AND " & _
              "a.IndicadorID=c.ItemID AND a.ItemNormaID=d.ItemID AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.RIQID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RIQID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.RIQID, b.RecursoAbreviado as Estado, c.ItemMasculino AS Indicador, c.ItemAbreviado AS IndicadorAbrev, " & _
             "d.ItemAbreviado AS Norma, a.Unidade AS Unidade, " & _
             "(CASE a.Frequencia WHEN 1 THEN 'Mensal' " & _
							    "WHEN 2 THEN 'Bimestral' " & _
							    "WHEN 3 THEN 'Trimestral' " & _
							    "WHEN 6 THEN 'Semestral' " & _
							    "WHEN 12 THEN 'Anual' " & _
							    "ELSE SPACE(0) END) AS Frequencia, a.dtInicioCalculo, a.dtMeta, a.Meta, a.ResultadoMinimo, a.ResultadoMaximo, " & _
							    "e.Fantasia AS Proprietario " & _
             "FROM RIQ a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK), Pessoas e WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.IndicadorID=c.ItemID AND a.ItemNormaID=d.ItemID AND " & _
             "a.ProprietarioID=e.PessoaID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RIQID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 7130    ' modqualidade/submodiso/rad      

    If( (returnFldType("RAD", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RADID " & _
              "FROM RAD a WITH(NOLOCK) " & _
              "WHERE a.EmpresaID = " & CStr(nEmpresaID) & " AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.RADID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RADID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.RADID, b.RecursoAbreviado as Estado, a.dtAnalise, a.Local, a.dtVencimento, " & _
             "a.Observacao " & _
             "FROM RAD a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RADID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 7140    ' modqualidade/submodiso/rrc

    If( (returnFldType("RRC", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RRCID " & _
              "FROM RRC a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " & _
              "WHERE a.TipoRealimentacaoID=b.ItemID AND a.ClienteID=c.PessoaID AND a.EmpresaID = " & CStr(nEmpresaID) & " AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.RRCID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RRCID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.RRCID, e.RecursoAbreviado as Estado, a.Realimentacao, b.ItemMasculino, a.dtEmissao, a.dtVencimento, " & _
             "a.Critico, a.Observacao, c.Fantasia, d.Fantasia " & _
             "FROM RRC a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Recursos e WITH(NOLOCK), @TempTable z " & _
             "WHERE a.TipoRealimentacaoID=b.ItemID AND a.ClienteID=c.PessoaID AND a.ProprietarioID=d.PessoaID AND " & _
             "a.EstadoID=e.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RRCID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 7150    ' modqualidade/submodiso/psc

    If( (returnFldType("PSC", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.PSCID " & _
              "FROM PSC a WITH(NOLOCK), Pessoas c WITH(NOLOCK) " & _
              "WHERE a.ClienteID=c.PessoaID AND a.EmpresaID = " & CStr(nEmpresaID) & " AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.PSCID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.PSCID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.PSCID, b.RecursoAbreviado as Estado, a.SituacaoCliente, a.dtEmissao, " & _
             "a.Critico, a.Observacao, c.Fantasia AS Cliente, dbo.fn_PSC_GrauSatisfacao(a.PSCID) AS Satisfacao, d.Fantasia AS Proprietario " & _
             "FROM PSC a WITH(NOLOCK), Recursos b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID=b.RecursoID AND a.ClienteID=c.PessoaID AND a.ProprietarioID=d.PessoaID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.PSCID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 7160    ' modqualidade/submodiso/rai      

    If( (returnFldType("RAI", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RAIID " & _
              "FROM RAI a WITH(NOLOCK), Recursos c WITH(NOLOCK) " & _
              "WHERE a.EstadoID=c.RecursoID AND a.EmpresaID = " & CStr(nEmpresaID) & " AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.RAIID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RAIID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.RAIID, c.RecursoAbreviado as Estado, a.dtAuditoria, a.dtVencimento, " & _
             "dbo.fn_RAI_GrauConformidade(a.RAIID) AS GrauConformidade, a.SistemaOK, a.Observacao, d.Fantasia " & _
             "FROM RAI a WITH(NOLOCK), Recursos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = c.RecursoID AND a.ProprietarioID = d.PessoaID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RAIID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 7170    ' modqualidade/submodiso/racp      

    If( (returnFldType("RACP", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RACPID " & _
              "FROM RACP a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK) " & _
              "WHERE a.TipoAcaoID = b.ItemID AND a.EstadoID=c.RecursoID AND a.EmpresaID = " & CStr(nEmpresaID) & " AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.RACPID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RACPID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.RACPID, c.RecursoAbreviado as Estado, a.NaoConformidade, b.ItemMasculino, a.dtEmissao, a.dtVencimento, " & _
             "a.Critico, a.Observacao, d.Fantasia " & _
             "FROM RACP a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = c.RecursoID AND a.TipoAcaoID = b.ItemID AND a.ProprietarioID = d.PessoaID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.RACPID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 7180    ' modqualidade/submodiso/qaf

    If( (returnFldType("QAF", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.QAFID " & _
              "FROM QAF a WITH(NOLOCK), Pessoas c WITH(NOLOCK) " & _
              "WHERE a.FornecedorID=c.PessoaID AND a.EmpresaID = " & CStr(nEmpresaID) & " AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.QAFID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.QAFID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.QAFID, b.RecursoAbreviado as Estado, a.dtEmissao, a.dtVencimento, " & _
             "c.Fantasia AS Fornecedor, a.Observacao, d.Fantasia AS Proprietario " & _
             "FROM QAF a WITH(NOLOCK), Recursos b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Pessoas d WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID=b.RecursoID AND a.FornecedorID=c.PessoaID AND a.ProprietarioID=d.PessoaID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.QAFID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 8110    ' modservicos/subsuporte/assistenciatecnica      

    If( (returnFldType("Asstec", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    'NOTA:
    ' e.Fantasia -> Cliente
    ' m.Fantasia -> Fornecedor

    'Tabela temporaria para pesquisa por cliente
	If ( sFldKey = "e.Fantasia" ) Then
		'strAdditionalCondition1 = " AND ((SELECT COUNT(*) FROM Asstec_Pessoas AsstecClientes, Pessoas Clientes " & _
		'		"WHERE (a.AsstecID = AsstecClientes.AsstecID AND AsstecClientes.EhCliente = 1 AND " & _
		'			"AsstecClientes.PessoaID = Clientes.PessoaID AND Clientes.Fantasia " & sOperator & sTextToSearch & " )) >0))"
                
        strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.AsstecID " & _
              "FROM Asstec a WITH(NOLOCK), Recursos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Asstec_Pessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & " a.EmpresaID=" & nEmpresaID & " AND " & _
              "a.EstadoID = b.RecursoID AND a.ProdutoID=c.ConceitoID AND " & _
              "d.AsstecID = a.AsstecID AND " & _
              "d.EhCliente = 1 AND " & _
              "d.PessoaID = e.PessoaID AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    'Tabela temporaria para pesquisa por fornecedor
	ElseIf ( sFldKey = "m.Fantasia" ) Then       
		'strAdditionalCondition1 = " AND ((SELECT COUNT(*) FROM Asstec_Pessoas AsstecFornecedores, Pessoas Fornecedores " & _
		'		"WHERE (a.AsstecID = AsstecFornecedores.AsstecID AND AsstecFornecedores.EhCliente = 0 AND " & _
		'			"AsstecFornecedores.PessoaID = Fornecedores.PessoaID AND Fornecedores.Fantasia " & sOperator & sTextToSearch & " )) >0))" 

        strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.AsstecID " & _
              "FROM Asstec a WITH(NOLOCK), Recursos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Asstec_Pessoas d WITH(NOLOCK), Pessoas m WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & " a.EmpresaID=" & nEmpresaID & " AND " & _
              "a.EstadoID = b.RecursoID AND a.ProdutoID=c.ConceitoID AND " & _
              "d.AsstecID = a.AsstecID AND " & _
              "d.EhCliente = 0 AND " & _
              "d.PessoaID = m.PessoaID AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

	'Tabela temporaria para demais pesquisas
    Else
        strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.AsstecID " & _
                  "FROM Asstec a WITH(NOLOCK), Recursos b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                  "WHERE " & sAccessAllowed & " a.EmpresaID=" & nEmpresaID & " AND " & _
                  "a.EstadoID = b.RecursoID AND a.ProdutoID=c.ConceitoID AND " & _
                  sFldKey & sOperator & sTextToSearch & " " & _
                  sCondition & sFiltroContexto & sFiltro
    End If

    If ( sFldKey = "a.AsstecID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.AsstecID"
    End If    
    
    'Dados para pesquisa por cliente             
    If ( sFldKey = "e.Fantasia" ) Then
        strSQL2 ="SELECT TOP " & nPageSize & _
                 " a.AsstecID, b.RecursoAbreviado as Estado, " & _
                 "(SELECT Itens.ItemAbreviado FROM TiposAuxiliares_Itens Itens WITH(NOLOCK) " & _
                 "WHERE (Itens.ItemID = dbo.fn_Asstec_Estoque(a.AsstecID))) as Estoque, " & _
                 " a.Defeito, " & _
                 "a.dtAsstec, c.Conceito, c.ConceitoID, a.NumeroSerie, " & _
	    		 "(SELECT TOP 1 e.Fantasia " & _
                 "WHERE (d.AsstecID = a.AsstecID AND d.EhCliente = 1 AND d.PessoaID = e.PessoaID) " & _
                 "ORDER BY d.AstPessoaID DESC) AS Cliente, " & _
                 "(SELECT TOP 1 Pess.Fantasia " & _
                 "FROM Asstec_Pessoas Fornec WITH(NOLOCK), Pessoas Pess WITH(NOLOCK) " & _
                 "WHERE (Fornec.AsstecID = a.AsstecID AND Fornec.PessoaID = Pess.PessoaID AND Fornec.EhCliente = 0) " & _
                 "ORDER BY Fornec.AstPessoaID DESC) AS Fornecedor " & _
                 "FROM Asstec a WITH(NOLOCK), Recursos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Asstec_Pessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK), @TempTable z " & _
                 "WHERE a.EmpresaID=" & nEmpresaID & " AND a.EstadoID = b.RecursoID " & _
                 "AND a.ProdutoID=c.ConceitoID AND " & _
                 "d.AsstecID = a.AsstecID AND " & _
                 "d.EhCliente = 1 AND " & _
                 "d.PessoaID = e.PessoaID AND " & _
                 sFldKey & sOperator & sTextToSearch & " " & _
                 sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=d.AsstecID " & _
                 "AND z.IDTMP > " & startID & _
                 " ORDER BY z.IDTMP"
    
    'Dados para pesquisa por fornecedor
    ElseIf ( sFldKey = "m.Fantasia" ) Then       
        strSQL2 ="SELECT TOP " & nPageSize & _
                 " a.AsstecID, b.RecursoAbreviado as Estado, " & _
                 "(SELECT Itens.ItemAbreviado FROM TiposAuxiliares_Itens Itens WITH(NOLOCK) " & _
                 "WHERE (Itens.ItemID = dbo.fn_Asstec_Estoque(a.AsstecID))) as Estoque, " & _
                 " a.Defeito, " & _
                 "a.dtAsstec, c.Conceito, c.ConceitoID, a.NumeroSerie, " & _
                 "(SELECT TOP 1 Pess.Fantasia " & _
                 "FROM Asstec_Pessoas Cli WITH(NOLOCK), Pessoas Pess WITH(NOLOCK) " & _
                 "WHERE (Cli.AsstecID = a.AsstecID AND Cli.PessoaID = Pess.PessoaID AND Cli.EhCliente = 1) " & _
                 "ORDER BY Cli.AstPessoaID DESC) AS Cliente ," & _
	    		 "(SELECT TOP 1 m.Fantasia " & _
                 "WHERE (d.AsstecID = a.AsstecID AND d.EhCliente = 0 AND d.PessoaID = m.PessoaID) " & _
                 "ORDER BY d.AstPessoaID DESC) AS Fornecedor " & _
                 "FROM Asstec a WITH(NOLOCK), Recursos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Asstec_Pessoas d WITH(NOLOCK), Pessoas m WITH(NOLOCK), @TempTable z " & _
                 "WHERE a.EmpresaID=" & nEmpresaID & " AND a.EstadoID = b.RecursoID " & _
                 "AND a.ProdutoID=c.ConceitoID AND " & _
                 "d.AsstecID = a.AsstecID AND " & _
                 "d.EhCliente = 0 AND " & _
                 "d.PessoaID = m.PessoaID AND " & _
                 sFldKey & sOperator & sTextToSearch & " " & _
                 sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=d.AsstecID " & _
                 "AND z.IDTMP > " & startID & _
                 " ORDER BY z.IDTMP"

    'Dados para demais pesquisas
    Else
        strSQL2 ="SELECT TOP " & nPageSize & _
					" a.AsstecID, b.RecursoAbreviado as Estado, " & _
					"(SELECT Itens.ItemAbreviado FROM TiposAuxiliares_Itens Itens WITH(NOLOCK) " & _
					"WHERE (Itens.ItemID = dbo.fn_Asstec_Estoque(a.AsstecID))) as Estoque, " & _
					" a.Defeito, " & _
					"a.dtAsstec, c.Conceito, c.ConceitoID, a.NumeroSerie, " & _
	    			"(SELECT TOP 1 Clientes.Fantasia " & _
	    			"FROM Asstec_Pessoas AsstecClientes WITH(NOLOCK), Pessoas Clientes WITH(NOLOCK) " & _
	    			"WHERE (a.AsstecID = AsstecClientes.AsstecID AND AsstecClientes.EhCliente = 1 AND AsstecClientes.PessoaID = Clientes.PessoaID) " & _
	    			"ORDER BY AsstecClientes.AstPessoaID DESC) AS Cliente, " & _
	    			"(SELECT TOP 1 Fornecedores.Fantasia " & _
	    			"FROM Asstec_Pessoas AsstecFornecedores WITH(NOLOCK), Pessoas Fornecedores WITH(NOLOCK) " & _
	    			"WHERE (a.AsstecID = AsstecFornecedores.AsstecID AND AsstecFornecedores.EhCliente = 0 AND AsstecFornecedores.PessoaID = Fornecedores.PessoaID) " & _
	    			"ORDER BY AsstecFornecedores.AstPessoaID DESC) AS Fornecedor, " & _
	    			"a.OrdemProducaoID, a.CaseNumber, a.RMA " & _
                 "FROM Asstec a WITH(NOLOCK), Recursos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), @TempTable z " & _
                 "WHERE a.EmpresaID=" & nEmpresaID & " AND a.EstadoID = b.RecursoID " & _
					"AND a.ProdutoID=c.ConceitoID AND " & _
					sFldKey & sOperator & sTextToSearch & " " & _
					sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.AsstecID " & _
					"AND z.IDTMP > " & startID & _
                 " ORDER BY z.IDTMP"
    End If

Case 8120    ' modservicos/subsuporte/glossario      

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.TermoID " & _
              "FROM Glossario a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.TermoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.TermoID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.TermoID, b.RecursoAbreviado as Estado, a.Termo, a.Significado " & _
             "FROM Glossario a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.TermoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 9110    ' modfinanceiro/submodcontrolefinanceiro/financeiro

	Dim sOperatorSit
    strSQL = " SET NOCOUNT ON DECLARE @TempTable TABLE (IDTMP int IDENTITY, IDFromPesq INT NOT NULL) "
	sParametros = " @TTS NVARCHAR(128), @EmpID INT, @UserID INT, @PS INT "
	sOperatorSit = " LEFT OUTER JOIN "

    If ((A1=0)OR(A2=0)) Then
		sFiltro = sFiltro & " AND a.PedidoID IS NOT NULL "
	End If		

    If ( (nArgumentoIsEmpty = 1) and (sFldKey = "a.dtVencimento")) Then
        sTextToSearch = ""
    End If

    If( (returnFldType("Financeiro", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    'If ( sFldKey = "a.SaldoAtualizado" ) Then
    '   sFldKey = "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) "
    'End If
    
    If ( sFldKey = "g.ItemAbreviado" ) Then
		sOperatorSit = " INNER JOIN "
    End If    

    'Chave de pesquisa Transportadora
    strSQL = strSQL + "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.FinanceiroID " & _
              "FROM Financeiro a WITH(NOLOCK) " & _
                sOperatorSit & " TiposAuxiliares_Itens g WITH(NOLOCK) ON (a.SituacaoCobrancaID = g.ItemID) " & _
                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) " 
                
     if (nFiltroContextoID = 9111)  Then
        strSQL = strSQL + "INNER JOIN dbo.fn_Direitos_TiposAuxiliares_tbl (@UserID, @EmpID, GETDATE()) c ON ( b.ClassificacaoID = c.ClassificacaoID ) " 
     End if         
              
     strSQL = strSQL + "WHERE " & sAccessAllowed & " a.EmpresaID = @EmpID AND " & _
                  sFldKey & sOperator & "@TTS " & _
                  sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.FinanceiroID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.FinanceiroID"
    End If    

    strSQL2 ="SELECT TOP (@PS) " & _
             " a.FinanceiroID, c.RecursoAbreviado as Estado, g.ItemAbreviado AS Sit, a.PedidoID, a.Duplicata, b.Fantasia, " & _
             "d.ItemAbreviado, a.PrazoPagamento, a.dtVencimento, e.SimboloMoeda as Moeda, a.Valor, " & _
			 "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, " & _
	         "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 8, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, " & _
			 "dbo.fn_Financeiro_ContaTomacao(a.FinanceiroID, 2) AS HistoricoPadrao, a.Observacao, " & _
             "dbo.fn_Financeiro_Cor(a.FinanceiroID, @UserID, GETDATE(),1) AS CorObservacao " & _
             "FROM Financeiro a WITH(NOLOCK) " & _
                sOperatorSit & " TiposAuxiliares_Itens g WITH(NOLOCK) ON (a.SituacaoCobrancaID = g.ItemID) " & _
                "LEFT OUTER JOIN HistoricosPadrao f WITH(NOLOCK) ON (a.HistoricoPadraoID = f.HistoricoPadraoID) " & _
                "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.PessoaID = b.PessoaID) " & _
                "INNER JOIN Recursos c WITH(NOLOCK) ON (a.EstadoID = c.RecursoID) " & _
                "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.FormaPagamentoID = d.ItemID) " & _
                "INNER JOIN Conceitos e WITH(NOLOCK) ON (a.MoedaID = e.ConceitoID) " & _
                "INNER JOIN @TempTable z ON (z.IDFromPesq = a.FinanceiroID) " & _
             "WHERE a.EmpresaID = @EmpID AND " & _
             sFldKey & sOperator & "@TTS " & _
             sCondition & sFiltroContexto & sFiltro & " " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"
    
        sSql = strSQL + " " + strSQL2 + " SET NOCOUNT OFF"

        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@SQl", adVarWChar, adParamInput, 4000, sSql))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@Parametros", adVarWChar, adParamInput, 4000, sParametros))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@TTS", adVarWChar, adParamInput,128 , sTextToSearch))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@EmpID", adInteger, adParamInput,8 , nEmpresaID))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@UserID", adInteger, adParamInput,8 , userID))
        rsSPCommand.Parameters.Append( rsSPCommand.CreateParameter("@PS", adInteger, adParamInput,8 , nPageSize))

Case 9130    ' modfinanceiro/submodcontrolefinanceiro/valoresalocalizar

    If( (returnFldType("ValoresLocalizar", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If
    
    If ( sFldKey = "a.SaldoFinanceiro" ) Then
       sFldKey = "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) "
    ElseIf ( sFldKey = "a.SaldoOcorrencia" ) Then
       sFldKey = "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 2) "
    End If    

    Dim sOperatorPessoa
    
    'Chave de pesquisa e Nota Fiscal
    If (sFldKey = "e.Fantasia") Then
        sOperatorPessoa = " INNER JOIN "
    Else
        sOperatorPessoa = " LEFT OUTER JOIN "
    End If
    
    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.ValorID " & _
              "FROM ValoresLocalizar a WITH(NOLOCK) " & _
              sOperatorPessoa & " Pessoas e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID) " & _
              "WHERE " & sAccessAllowed & " a.EmpresaID=" & nEmpresaID & " AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.ValorID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.ValorID"
    End If    
                 
     strSQL2 ="SELECT TOP " & nPageSize & _
             " a.ValorID, b.RecursoAbreviado as Estado, f.ItemMasculino, c.ItemAbreviado as FormaPagamento, a.MotivoDevolucao, e.Fantasia, a.Emitente, a.dtEmissao, a.dtApropriacao, a.BancoAgencia, a.Conta, a.NumeroDocumento, d.SimboloMoeda as Moeda, a.Valor, " & _
             "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 4) AS SaldoFinanceiro, " & _
             "dbo.fn_ValorLocalizar_Posicao(a.ValorID, 2) AS SaldoOcorrencia, a.Identificador, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS ContaBancaria, a.Observacao, a.Historico " & _
             "FROM ValoresLocalizar a WITH(NOLOCK) " & _
                sOperatorPessoa & " Pessoas e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.FormaPagamentoID = c.ItemID) " & _
                "INNER JOIN Conceitos d WITH(NOLOCK) ON (a.MoedaID = d.ConceitoID) " & _
                "INNER JOIN @TempTable z ON (z.IDFromPesq = a.ValorID) " & _
                "INNER JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (a.ProcessoID = f.ItemID) " & _
             "WHERE a.EmpresaID=" & nEmpresaID & " AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 9140    ' modfinanceiro/submodcontrolefinanceiro/financiamentospadrao      

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.FinanciamentoID " & _
              "FROM FinanciamentosPadrao a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.FinanciamentoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.FinanciamentoID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.FinanciamentoID, b.RecursoAbreviado as Estado, a.Financiamento, a.Ordem, a.NumeroParcelas, " & _
             "a.Prazo1, a.Incremento as Incr, dbo.fn_Financiamento_PMP(a.FinanciamentoID,NULL,NULL,NULL) as PMP, a.Observacao " & _
             "FROM FinanciamentosPadrao a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.FinanciamentoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 9150    ' modfinanceiro/subcontrolefinanceiro/depositosbancarios

    If( (returnFldType("DepositosBancarios", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.DepositoID " & _
              "FROM DepositosBancarios a WITH(NOLOCK) " & _
              "WHERE a.EmpresaID = " & nEmpresaID & " AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.DepositoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.DepositoID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.DepositoID, b.RecursoAbreviado as Estado, a.dtData, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS Conta, " & _
             "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 3) AS Quantidade, " & _
             "c.SimboloMoeda AS Moeda, " & _
             "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 6) AS ValorDinheiro, " & _
             "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 7) AS ValorCheques, " & _
             "dbo.fn_DepositoBancario_Posicao(a.DepositoID, 8) AS Total, a.Observacao " & _
             "FROM DepositosBancarios a WITH(NOLOCK), Recursos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EmpresaID = " & nEmpresaID & " AND a.EstadoID=b.RecursoID AND a.MoedaID=c.ConceitoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.DepositoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 9210    ' modfinanceiro/subcobranca/cobranca      

    If( (returnFldType("Cobranca", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.CobrancaID " & _
              "FROM Cobranca a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & " a.EmpresaID=" & nEmpresaID & " AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.CobrancaID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.CobrancaID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.CobrancaID, b.RecursoAbreviado as Estado, c.Codigo, d.ItemMasculino, " & _
             "a.dtGravacao, a.Sequencial, a.Arquivo " & _
             "FROM Cobranca a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesPessoas_Contas e WITH(NOLOCK), RelacoesPessoas f WITH(NOLOCK), Pessoas g WITH(NOLOCK), Bancos c WITH(NOLOCK), TiposAuxiliares_Itens d WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EmpresaID=" & nEmpresaID & " AND a.EstadoID = b.RecursoID " & _
             "AND a.RelPesContaID = e.RelPesContaID " & _
             "AND e.RelacaoID = f.RelacaoID AND f.ObjetoID = g.PessoaID " & _
             "AND g.BancoID = c.BancoID AND a.TipoCobrancaID=d.ItemID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.CobrancaID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"
             
Case 9220    ' modfinanceiro/subcobranca/bancos      

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.BancoID " & _
              "FROM Bancos a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.BancoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.BancoID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.BancoID, b.RecursoAbreviado as Estado, a.Banco, a.Codigo " & _
             "FROM Bancos a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.BancoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 10110    ' modcontabil/submodcontabilidade/planocontas      

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.ContaID " & _
              "FROM PlanoContas a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.ContaID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.ContaID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.ContaID, b.RecursoAbreviado as Estado, dbo.fn_Conta_Identada(a.ContaID, " & CStr(nIdiomaID) & ", NULL, NULL) AS Conta, " & _
				"c.ItemMasculino AS Tipo, dbo.fn_Conta_Nivel(a.ContaID) AS Nivel, a.ContaMaeID, d.ItemMasculino AS UsoEspecial, " & _
				"e.ItemMasculino AS Detalhamento, a.Detalha, a.DetalhamentoObrigatorio, " & _
				"dbo.fn_Conta_AceitaLancamento(a.ContaID) AS AceitaLancamento, a.LancamentoManual, " & _
				"a.SaldoDevedor, a.SaldoCredor, a.UsoRestrito, a.USA, a.TemOrcamento, a.Observacao " & _
             "FROM PlanoContas a WITH(NOLOCK) " & _
                "LEFT OUTER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (a.TipoContaID = c.ItemID) " & _
                "LEFT OUTER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (a.UsoEspecialID = d.ItemID) " & _
                "INNER JOIN Recursos b WITH(NOLOCK) ON (a.EstadoID = b.RecursoID) " & _
                "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.TipoDetalhamentoID = e.ItemID) " & _
                "INNER JOIN @TempTable z ON (z.IDFromPesq=a.ContaID) " & _
             "WHERE " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 10120    ' modcontabil/submodcontabilidade/historicospadrao      

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.HistoricoPadraoID " & _
              "FROM HistoricosPadrao a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.HistoricoPadraoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.HistoricoPadraoID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.HistoricoPadraoID, b.RecursoAbreviado as Estado, a.HistoricoPadrao, " & _
				"a.HistoricoComplementar, a.HistoricoAbreviado, c.ItemAbreviado AS TipoLancamento, a.UsoCotidiano, a.UsoSistema, a.EhRetorno, a.AnteciparLancamento, a.MultiplasEmpresas, " & _
				"(CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.Pedido = 1)))) AS Pedido, " & _
				"(CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.Financeiro = 1)))) AS Financeiro, " & _
				"dbo.fn_HistoricoPadrao_ConciliacaoBancaria(a.HistoricoPadraoID, NULL, 0) AS ConciliacaoBancaria, " & _
				"dbo.fn_HistoricoPadrao_ConciliacaoBancaria(a.HistoricoPadraoID, NULL, 1) AS GeraLancamento, a.Marketing, " & _
				"a.PesoContabilizacao, a.Grupo, a.RequerDocumentoFiscal, a.Observacao " & _
             "FROM HistoricosPadrao a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.TipoLancamentoID = c.ItemID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.HistoricoPadraoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 10130    ' modcontabil/submodcontabilidade/lancamentos      

    If( (returnFldType("Lancamentos", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.LancamentoID " & _
              "FROM Lancamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), HistoricosPadrao d WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & " a.EmpresaID=" & nEmpresaID & " AND " & _
			  "a.EstadoID = b.RecursoID AND a.TipoLancamentoID = c.ItemID AND a.HistoricoPadraoID = d.HistoricoPadraoID AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.LancamentoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.LancamentoID"
    End If

     strSQL2 ="SELECT TOP " & nPageSize & _
             " a.LancamentoID, b.RecursoAbreviado as Estado, a.dtLancamento, a.dtBalancete, " & _
             "dbo.fn_Lancamento_Conciliado(-a.LancamentoID) AS Conciliado, " & _
				"a.EhEstorno, c.ItemAbreviado, CONVERT(NUMERIC(11), dbo.fn_Lancamento_Totais(a.LancamentoID, 7)) AS Quantidade, d.HistoricoPadrao, a.HistoricoComplementar, " & _
				"dbo.fn_Lancamento_Detalhe(a.LancamentoID) AS Detalhe, " & _
				"a.ValorTotalLancamento AS ValorTotalLancamento, " & _
				"dbo.fn_Lancamento_Totais(a.LancamentoID, 6) AS ValorTotalConvertido, a.Observacao " & _
             "FROM Lancamentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), HistoricosPadrao d WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EmpresaID=" & nEmpresaID & " AND a.EstadoID = b.RecursoID AND " & _
			 "a.TipoLancamentoID = c.ItemID AND a.HistoricoPadraoID = d.HistoricoPadraoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.LancamentoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 10140    ' modcontabil/submodcontabilidade/extratosbancarios      

    If( (returnFldType("ExtratosBancarios", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    If ( sFldKey = "a.Conta" ) Then
       sFldKey = "dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) "
    End If    

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.ExtratoID " & _
              "FROM ExtratosBancarios a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & " a.EmpresaID=" & nEmpresaID & " AND " & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.ExtratoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.ExtratoID"
    End If    
                 
     strSQL2 ="SELECT TOP " & nPageSize & _
             " a.ExtratoID, b.RecursoAbreviado as Estado, a.Concilia, a.dtEmissao, a.dtInicial, a.dtFinal, " & _
				"dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS Conta, " & _
				"dbo.fn_ExtratoBancario_Totais(a.ExtratoID, NULL, 1) AS NumeroLancamentos, " & _
				"dbo.fn_ExtratoBancario_Totais(a.ExtratoID, NULL, 2) AS TotalDebitos, " & _
				"dbo.fn_ExtratoBancario_Totais(a.ExtratoID, NULL, 3) AS TotalCreditos, " & _
				"a.SaldoFinal, a.Observacao " & _
             "FROM ExtratosBancarios a WITH(NOLOCK) " & _
             "INNER JOIN Recursos b WITH(NOLOCK) ON a.EstadoID = b.RecursoID " & _
             "INNER JOIN @TempTable z ON z.IDFromPesq=a.ExtratoID " & _
             "WHERE a.EmpresaID= " & nEmpresaID & " AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.ExtratoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 12210    'Competencias       

    If( (returnFldType("Competencias", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.CompetenciaID " & _
              "FROM Competencias a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK) " & _
              "WHERE a.EstadoID=b.RecursoID AND a.TipoCompetenciaID=c.ItemID AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.CompetenciaID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.CompetenciaID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.CompetenciaID, b.RecursoAbreviado as Estado, a.Competencia, c.ItemMasculino AS Tipo " & _
             "FROM Competencias a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.TipoCompetenciaID=c.ItemID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.CompetenciaID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 12220    ' modrecursoshumanos/submodtreinamento/ret

    If( (returnFldType("RET", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
    End If

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RETID " & _
              "FROM RET a WITH(NOLOCK), Competencias b WITH(NOLOCK) " & _
              "WHERE a.TreinamentoID=b.CompetenciaID AND a.EmpresaID = " & CStr(nEmpresaID) & " AND " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.RETID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RETID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.RETID, c.RecursoAbreviado as Estado, b.Competencia AS Treinamento, a.dtData, a.dtVencimento, a.Observacao, " & _
			 "a.AvaliacaoTreinandos, a.AvaliacaoTreinamento, a.AvaliacaoEficacia, d.Fantasia AS Fornecedor, " & _
			 "f.SimboloMoeda AS Moeda, a.Valor, CONVERT(NUMERIC(5,1), dbo.fn_RET_CargaHoraria(a.RETID)) AS CH, " & _
			 "a.FrequenciaMinima, a.NotaMinima, a.Aulas, a.Turmas," & _
			 "(SELECT COUNT(*) FROM RET_Participantes WITH(NOLOCK) WHERE RETID=a.RETID) AS Participantes, " & _
			 "e.Fantasia AS Proprietario " & _
             "FROM RET a WITH(NOLOCK) " & _
                "LEFT OUTER JOIN Pessoas d WITH(NOLOCK) ON (a.FornecedorID=d.PessoaID) " & _
                "LEFT OUTER JOIN Pessoas e WITH(NOLOCK) ON (a.ProprietarioID=e.PessoaID) " & _
                "LEFT OUTER JOIN Conceitos f WITH(NOLOCK) ON (a.MoedaID=f.ConceitoID) " & _
                "INNER JOIN Competencias b WITH(NOLOCK) ON (a.TreinamentoID=b.CompetenciaID) " & _
                "INNER JOIN Recursos c WITH(NOLOCK) ON (a.EstadoID=c.RecursoID) " & _
                "INNER JOIN @TempTable z ON (z.IDFromPesq=a.RETID) " & _
             "WHERE " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 1230    ' modbasico/submodreno/prospect

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.prospectID " & _
              "FROM prospect a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.prospectID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.prospectID"
    End If    
                 
    strSQL2 ="SELECT TOP " & nPageSize & _
             " a.prospectID, b.RecursoAbreviado as Estado, a.Nome, a.Fantasia, c.ItemMasculino " & _
             "FROM prospect a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.TipoPessoaID = c.ItemID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & _
             sCondition & sFiltroContexto & sFiltro & " AND z.IDFromPesq=a.prospectID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 12320 ' modrecursoshumanos/submodavaliacao/avaliacao

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.AvaliacaoID " & _
              "FROM AvaliacoesDesempenho a WITH(NOLOCK), Pessoas d WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro & " AND a.FuncionarioID=d.PessoaID AND " & _
              "dbo.fn_Avaliacao_Participante(a.AvaliacaoID, " & CStr(userID) & ") = 1 AND " & _
              "a.EmpresaID=" & CStr(nEmpresaID)

    If ( sFldKey = "a.AvaliacaoID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.AvaliacaoID"
    End If

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.AvaliacaoID, b.RecursoAbreviado as Estado, c.ItemMasculino AS TipoAvaliacao, d.Fantasia AS Funcionario, " & _
             "e.RecursoFantasia AS Cargo, a.dtInicio, a.dtFim, a.Observacao, f.Fantasia AS GestorImediato " & _
             "FROM AvaliacoesDesempenho a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), Pessoas d WITH(NOLOCK), Recursos e WITH(NOLOCK), Pessoas f WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.TipoAvaliacaoID = c.ItemID AND " & _
             "a.FuncionarioID=d.PessoaID AND a.CargoID=e.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltroContexto & sFiltro & " AND " & _
             "dbo.fn_Avaliacao_Participante(a.AvaliacaoID, " & CStr(userID) & ") = 1 AND " & _
             "a.EmpresaID=" & CStr(nEmpresaID) & " AND a.ProprietarioID=f.PessoaID AND z.IDFromPesq=a.AvaliacaoID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 12510 ' modrecursoshumanos/submodfopag/fopag

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.FopagID " & _
              "FROM Fopag a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro & " AND a.EmpresaID=" & CStr(nEmpresaID)

    If ( sFldKey = "a.FopagID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.FopagID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.FopagID, b.RecursoAbreviado as Estado, c.ItemMasculino AS TipoFopag, a.ProcessamentoLiberado, a.IncluiVerbasNormais, " & _
             "a.IncluiVerbasVendas, a.PlanoSaudeFuncionario, a.SubtraiSalarioFixo, a.ArredondaSalarios, a.GeraFinanceirosDistintos, " & _
             "a.DiaAntecipacaoAviso, a.DiaAntecipacaoPagamento, a.DiasUteisPagamento, a.DiasDesconsiderarVendas, " & _
             "a.DiasRecuarVendas, a.dtFopag, a.dtFopagInicio, a.dtFopagFim, dbo.fn_Fopag_Data(a.FopagID, 3) AS dtFopagPagamento, " & _
             "dbo.fn_Fopag_Totais(a.FopagID, NULL, NULL, NULL, NULL, NULL, 1, 1) AS TotalFopag, a.Observacao, " & _
             "dbo.fn_Fopag_Data(FopagID, 4) AS dtApuracaoInicio, dbo.fn_Fopag_Data(FopagID, 5) AS dtApuracaoFim, a.EstadoID, a.dtUltimoFechamento, a.TipoFopagID " & _
             "FROM Fopag a WITH(NOLOCK), Recursos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND a.TipoFopagID = c.ItemID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltroContexto & sFiltro & _
             " AND a.EmpresaID=" & CStr(nEmpresaID) & " AND z.IDFromPesq=a.FopagID " & _
             "AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"

Case 12530 ' modrecursoshumanos/submodfopag/verbas

    strSQL = "INSERT INTO @TempTable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.VerbaID " & _
              "FROM VerbasFopag a WITH(NOLOCK) " & _
              "WHERE " & sAccessAllowed & _
              sFldKey & sOperator & sTextToSearch & " " & _
              sCondition & sFiltroContexto & sFiltro

    If ( sFldKey = "a.VerbaID" ) Then
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
    Else
       strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.VerbaID"
    End If    

    strSQL2 ="SELECT TOP " & nPageSize & " " & _
             "a.VerbaID, b.RecursoAbreviado as Estado, a.Verba, a.EhProvento, a.EhAutomatica, a.PermiteValorExterno, a.MetodoDataReferenciaID," & _
             "a.MetodoReferenciaID, a.MetodoValorID, a.TrataDataFopagInicio, a.TrataDataFopagFim, a.TrataDataReferencia, " & _
             "a.TrataReferencia, a.TrataValor, a.Observacao " & _
             "FROM VerbasFopag a WITH(NOLOCK), Recursos b WITH(NOLOCK), @TempTable z " & _
             "WHERE a.EstadoID = b.RecursoID AND " & _
             sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltroContexto & sFiltro & _
             " AND z.IDFromPesq=a.VerbaID AND z.IDTMP > " & startID & _
             " ORDER BY z.IDTMP"
Case Else
    'Nao tem c�digo

End Select
  
'--------------------------- As strings de pesquisa -- FIM ---------------------------

On Error Resume Next  

If (sNovaPesq=True) Then
    With rsSPCommand	
        .CommandText = "dbo.sp_executesql"
        Set rsData = .Execute   
    End With  
Else  
    With rsSPCommand	

        .CommandText = "dbo.sp_PesqList"    

        .Parameters.Append( .CreateParameter("@iPageSize", adInteger, adParamInput,8 , nPageSize) )
        .Parameters.Append( .CreateParameter("@sSQL", adVarChar, adParamInput,Len(strSQL) , strSQL) )
        .Parameters.Append( .CreateParameter("@sSQL2", adVarChar, adParamInput,Len(strSQL2) , strSQL2) )
        'O parametro length abaixo eh o length da string + 1, porque se chegar 0, o sql gera uma excessao dizendo
        'que o parametro nao foi fornecido
        .Parameters.Append( .CreateParameter("@sExecAfterCreateTable", adVarChar, adParamInput,Len(strSQLExecAfterCreateTable) + 1, strSQLExecAfterCreateTable) )
        Set rsData = .Execute
    End With
End If    


errorOcurr = False
For Each rsERROR In rsSPCommand.ActiveConnection.Errors
    errorOcurr = True
Next

Set rsSPCommand = Nothing
  
Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
'Ze, acrescimo abaixo em 07/04/05  
rsNew.CursorType = adOpenKeySet
rsNew.LockType = adLockOptimistic
'Ze, fim de acrescimo em 07/04/05  
Dim nCounter
nCounter = -1
For Each fldF In rsData.Fields
    nCounter = nCounter + 1
    'If ( (fldF.Attributes =  adFldFixed) AND (fldF.Type = adNumeric) ) Then
    If ( fldF.Type = adNumeric ) Then
        rsNew.Fields.Append fldF.Name, adDecimal, fldF.DefinedSize, adFldMayBeNull OR adFldUpdatable
        rsNew.Fields(nCounter).Precision = fldF.Precision
        rsNew.Fields(nCounter).NumericScale = fldF.NumericScale
    Else
        rsNew.Fields.Append fldF.Name, fldF.Type, fldF.DefinedSize, adFldMayBeNull OR adFldUpdatable
    End If
Next
rsNew.Fields.Append "fldError", adVarChar, 50, adFldUpdatable

'Ze, alteracao abaixo em 07/04/05  
rsNew.Open' , , adOpenKeyset, adLockOptimistic, adCmdText

If rsData.EOF AND errorOcurr Then
    rsNew.AddNew
    rsNew.Fields("fldError").Value = "Filtro inv�lido."
    rsNew.Update
    rsNew.Save Response, adPersistXML
    rsNew.Close
    rsFiltroContexto.Close
    Set rsNew = Nothing
    rsData.Close
    Set rsData = Nothing
    Set rsFiltroContexto = Nothing
    Response.End
End If

rsData.CacheSize = CInt(nPageSize)

While Not rsData.EOF
    rsNew.AddNew
    For Each fldF In rsData.Fields
		If ( NOT (IsNull(fldF.Value)) ) Then
			rsNew.Fields(fldF.Name).Value = fldF.Value
		End If	
    Next
    rsNew.Update
    rsData.MoveNext
Wend
  
' send the new data back to the client
rsNew.Save Response, adPersistXML

If rsNew.State = adStateOpen Then
    rsNew.Close
End If
  
'rsNew.Close
rsFiltroContexto.Close
rsData.Close
Set rsNew = Nothing
Set rsData = Nothing
Set rsFiltroContexto = Nothing
%>