
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    'Captura os parametros de pesquisa
	Dim i
    Dim nEmpresaID, nUsuarioID
    Dim nResultado
    
    i = 0
	nEmpresaID = 0
	
	For i = 1 To Request.QueryString("nEmpresaID").Count    
	  nEmpresaID = Request.QueryString("nEmpresaID")(i)
	Next
	
	For i = 1 To Request.QueryString("nUsuarioID").Count    
	  nUsuarioID = Request.QueryString("nUsuarioID")(i)
	Next
	
	Dim rsSPCommand
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
	
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_RelatorioMicrosoft_BatchOpen"
	    .CommandType = adCmdStoredProc
	      
	    .Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
	    .Parameters("@EmpresaID").Value = CLng(nEmpresaID)

	    .Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
	    .Parameters("@UsuarioID").Value = CLng(nUsuarioID)
	    
	    .Parameters.Append( .CreateParameter("@Resultado", adInteger, adParamOutput) )
	    
		.Execute

	End With
    
    nResultado = CLng(rsSPCommand.Parameters("@Resultado").Value)    
    
	Dim rsNew
	Set rsNew = Server.CreateObject("ADODB.Recordset")
	rsNew.CursorLocation = adUseServer

	rsNew.Fields.Append "Resultado", adDecimal, 10, adFldMayBeNull OR adFldUpdatable
	  
	rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

	rsNew.AddNew
	If ( NOT IsNull(nResultado) ) Then
	    rsNew.Fields("Resultado").Value = nResultado
	End If

	rsNew.Update

	rsNew.Save Response, adPersistXML

	Set rsSPCommand = Nothing

	rsNew.Close
	Set rsNew = Nothing

%>
