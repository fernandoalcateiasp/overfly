
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    'A procedure sp_RelatorioEstoque usada neste arquivo
    'e geral para execucao de selects que retornam recordsets
    
    Dim i, userID, empresaID, formID, contextID
    Dim recursoID, folderID
    Dim dirC1, dirC2, dirI, dirA1, dirA2, dirE1, dirE2
    Dim dirC1C2True, dirA1A2True, dirE1E2True
    Dim sBtnNumber
    
    userID = 0
    empresaID = 0
    formID = 0
    contextID = 0

    For i = 1 To Request.QueryString("userID").Count    
        userID = Request.QueryString("userID")(i)
    Next
    
    For i = 1 To Request.QueryString("empresaID").Count    
        empresaID = Request.QueryString("empresaID")(i)
    Next

    For i = 1 To Request.QueryString("formID").Count    
        formID = Request.QueryString("formID")(i)
    Next
    
    For i = 1 To Request.QueryString("contextID").Count    
        contextID = Request.QueryString("contextID")(i)
    Next
  
    Dim rsData, strSQL, rsSPCommand

    Set rsData = Server.CreateObject("ADODB.Recordset")

    Set rsSPCommand = Server.CreateObject("ADODB.Command")
    
    'Os contextos
    strSQL = "SELECT DISTINCT 'sup' AS ControlBar, '1' AS Combo,a.RecursoFantasia AS Recurso,a.RecursoID,-1 AS SubFormID, " & _
                "a.Ordem AS Ordem, a.EhDefault AS EhDefault, g.Consultar1 AS C1,g.Consultar2 AS C2 ,g.Incluir AS I, " & _
                "g.Alterar1 AS A1,g.Alterar2 AS A2,g.Excluir1 AS E1,g.Excluir2 AS E2, ISNULL(h.SujeitoID,0) AS EstadoInicialID " & _
                "FROM Recursos a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK), RelacoesPesRec_Perfis c WITH(NOLOCK), Recursos_Direitos d WITH(NOLOCK), " & _
                "RelacoesRecursos e WITH(NOLOCK) " & _
                "LEFT OUTER JOIN RelacoesRecursos h WITH(NOLOCK) ON (e.MaquinaEstadoID=h.ObjetoID AND h.Ordem=1 AND h.TipoRelacaoID=3), " & _
                "Recursos f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos i WITH(NOLOCK) " & _ 
                "WHERE a.TipoRecursoID = 3 AND a.EstadoID=2 AND a.RecursoMaeID=" & CStr(formID) & " " & _  
                "AND b.ObjetoID=999 AND b.TipoRelacaoID=11 " & _
				"AND b.SujeitoID IN ((SELECT " & CStr(userID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(userID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
                "AND b.RelacaoID=c.RelacaoID " & _
                "AND c.EmpresaID=" & CStr(empresaID) & " AND d.RecursoID=a.RecursoID AND c.PerfilID=d.PerfilID " & _ 
                "AND (d.Consultar1=1 OR d.Consultar2=1) AND a.RecursoID=e.ObjetoID AND e.TipoRelacaoID=1 AND e.EstadoID=2 " & _ 
                "AND e.SujeitoID=f.RecursoID AND f.Principal=1 AND f.RecursoID=g.RecursoID AND a.RecursoID=g.RecursoMaeID AND g.PerfilID=c.PerfilID " & _
                "AND c.PerfilID = i.RecursoID AND i.EstadoID = 2 " & _
                "ORDER BY a.Ordem"
      
    With rsSPCommand
		.CommandTimeout = 60 * 10
        .ActiveConnection = strConn
        .CommandText = "sp_RelatorioEstoque"
        .CommandType = adCmdStoredProc
        .Parameters.Append( .CreateParameter("@sSQL", adVarChar, adParamInput,Len(strSQL) , strSQL) )
        Set rsData = .Execute
    End With
    rsSPCommand.Parameters.Delete 0
            
    If ( rsData.BOF And rsData.EOF ) Then
        rsData.Save Response, adPersistXML
        rsData.Close
        Set rsData = Nothing
        Set rsSPCommand = Nothing
        Response.End
    End If    
        
    rsData.CacheSize = 20
            
    'Os filtros do subform superior associados ao presente contexto
    
    Dim rsData2

    Set rsData2 = Server.CreateObject("ADODB.Recordset")
    
    strSQL = "SELECT DISTINCT 'sup' AS ControlBar, '2' AS Combo, d.RecursoID AS RecursoID, " & _
             "d.RecursoFantasia AS Recurso, c.Ordem AS Ordem, a.SujeitoID AS SubFormID, " & _
             "c.EhDefault, 0 AS C1, 0 AS C2, 0 AS I, 0 AS A1, 0 AS A2, 0 AS E1, 0 AS E2, 0 AS EstadoInicialID " & _
             "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec e WITH(NOLOCK), " & _
             "RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " & _
             "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID=" & contextID & " " & _
             "AND a.SujeitoID=b.RecursoID AND b.Principal=1 AND a.SujeitoID=c.ObjetoID AND c.EstadoID=2 " & _ 
             "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.ClassificacaoID=12 " & _
             "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) " & _
             "WHERE a.RecursoID=" & contextID & ")) " & _
             "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
             "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
			 "AND e.SujeitoID IN ((SELECT " & CStr(userID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(userID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
             "AND e.RelacaoID=f.RelacaoID " & _
             "AND f.EmpresaID=" & CStr(empresaID) & " AND a.ObjetoID=g.ContextoID AND c.ObjetoID=g.RecursoMaeID AND c.SujeitoID=g.RecursoID " & _
             "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
             "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " & _
             "ORDER BY c.Ordem"
                  
    With rsSPCommand
		.CommandTimeout = 60 * 10
        .ActiveConnection = strConn
        .CommandText = "sp_RelatorioEstoque"
        .CommandType = adCmdStoredProc
        .Parameters.Append( .CreateParameter("@sSQL", adVarChar, adParamInput,Len(strSQL) , strSQL) )
        Set rsData2 = .Execute
    End With
    rsSPCommand.Parameters.Delete 0
    
    rsData2.CacheSize = 20
    
    Dim rsData3

    Set rsData3 = Server.CreateObject("ADODB.Recordset")
    
    'Os filtros do subform inferior associados ao presente contexto
    
    strSQL = "SELECT DISTINCT 'inf' AS ControlBar, '2' AS Combo, d.RecursoID AS RecursoID, " & _
             "d.RecursoFantasia AS Recurso, c.Ordem AS Ordem, a.SujeitoID AS SubFormID, " & _
             "c.EhDefault, 0 AS C1, 0 AS C2, 0 AS I, 0 AS A1, 0 AS A2, 0 AS E1, 0 AS E2, 0 AS EstadoInicialID " & _
             "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec e WITH(NOLOCK), " & _
             "RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " & _
             "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID=" & contextID & " " & _
             "AND a.SujeitoID=b.RecursoID AND b.Principal=0 AND a.SujeitoID=c.ObjetoID AND c.EstadoID=2 " & _ 
             "AND c.TipoRelacaoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.ClassificacaoID=12 " & _
             "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) " & _
             "WHERE a.RecursoID=" & contextID & ")) " & _
             "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
             "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
			 "AND e.SujeitoID IN ((SELECT " & CStr(userID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(userID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
             "AND e.RelacaoID=f.RelacaoID " & _
             "AND f.EmpresaID=" & CStr(empresaID) & " AND a.ObjetoID=g.ContextoID AND c.ObjetoID=g.RecursoMaeID AND c.SujeitoID=g.RecursoID " & _
             "AND (g.Consultar1=1 OR g.Consultar2=1) " & _
             "AND f.PerfilID=g.PerfilID AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " & _
             "ORDER BY c.Ordem"

    With rsSPCommand
		.CommandTimeout = 60 * 10
        .ActiveConnection = strConn
        .CommandText = "sp_RelatorioEstoque"
        .CommandType = adCmdStoredProc
        .Parameters.Append( .CreateParameter("@sSQL", adVarChar, adParamInput,Len(strSQL) , strSQL) )
        Set rsData3 = .Execute
    End With
    rsSPCommand.Parameters.Delete 0
    
    rsData3.CacheSize = 20
    
    Dim rsData4

    Set rsData4 = Server.CreateObject("ADODB.Recordset")
    
    'As pastas do subform inferior associadas ao presente contexto
    strSQL = "SELECT DISTINCT 'inf' AS ControlBar, '1' AS Combo, b.RecursoID AS RecursoID, " & _
             "b.RecursoFantasia AS Recurso, b.RecursoID AS Ordem, 0 AS SubFormID, " & _
             "0 AS EhDefault, e.Consultar1 AS C1, e.Consultar2 AS C2, e.Incluir AS I, " & _
             "e.Alterar1 AS A1, e.Alterar2 AS A2, e.Excluir1 AS E1, e.Excluir2 AS E2, ISNULL(f.SujeitoID,0) AS EstadoInicialID " & _
             "FROM RelacoesRecursos a WITH(NOLOCK) " & _
             "LEFT OUTER JOIN RelacoesRecursos f WITH(NOLOCK) ON (a.MaquinaEstadoID=f.ObjetoID AND f.Ordem=1 AND f.TipoRelacaoID=3), " & _ 
             "Recursos b WITH(NOLOCK), RelacoesPesRec c WITH(NOLOCK), " & _
             "RelacoesPesRec_Perfis d WITH(NOLOCK), Recursos_Direitos e WITH(NOLOCK), Recursos g WITH(NOLOCK) " & _
             "WHERE a.EstadoID=2 AND a.TipoRelacaoID=1 AND a.ObjetoID= " & contextID & " " & _
             "AND a.SujeitoID=b.RecursoID AND b.Principal=0 AND b.EstadoID=2 " & _
             "AND c.ObjetoID=999 AND c.TipoRelacaoID=11 " & _
			 "AND c.SujeitoID IN ((SELECT " & CStr(userID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(userID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
             "AND c.RelacaoID=d.RelacaoID " & _
             "AND d.EmpresaID= " & CStr(empresaID) & _
             " AND a.ObjetoID=e.RecursoMaeID AND a.SujeitoID=e.RecursoID " & _
             "AND (e.Consultar1=1 OR e.Consultar2=1) AND d.PerfilID=e.PerfilID " & _
             "AND d.PerfilID = g.RecursoID AND g.EstadoID = 2 " & _
             "ORDER BY Ordem"
    
    With rsSPCommand
		.CommandTimeout = 60 * 10
        .ActiveConnection = strConn
        .CommandText = "sp_RelatorioEstoque"
        .CommandType = adCmdStoredProc
        .Parameters.Append( .CreateParameter("@sSQL", adVarChar, adParamInput,Len(strSQL) , strSQL) )
        Set rsData4 = .Execute
    End With
    rsSPCommand.Parameters.Delete 0
    
    rsData4.CacheSize = 20
        
    '----------------------------------------------------------------
        
    'O objeto que retorna o XML
    Dim rsNew
    Set rsNew = Server.CreateObject("ADODB.Recordset")
    rsNew.CursorLocation = adUseServer

    rsNew.Fields.Append "ControlBar", rsData.Fields("ControlBar").Type, rsData.Fields("ControlBar").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "Combo", rsData.Fields("Combo").Type, rsData.Fields("Combo").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "Recurso", rsData.Fields("Recurso").Type, rsData.Fields("Recurso").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "RecursoID", adInteger, rsData.Fields("RecursoID").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "SubFormID", adInteger, rsData.Fields("SubFormID").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "EhDefault", adBoolean, rsData.Fields("EhDefault").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "C1", adBoolean, rsData.Fields("C1").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "C2", adBoolean, rsData.Fields("C2").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "I", adBoolean, rsData.Fields("I").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "A1", adBoolean, rsData.Fields("A1").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "A2", adBoolean, rsData.Fields("A2").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "E1", adBoolean, rsData.Fields("E1").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "E2", adBoolean, rsData.Fields("E2").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "EstadoInicialID", adInteger, rsData.Fields("EstadoInicialID").DefinedSize, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B1A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B1A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B1C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B1C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B1I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B1E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B1E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B2A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B2A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B2C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B2C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B2I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B2E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B2E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    

    
    rsNew.Fields.Append "B3A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B3A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B3C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B3C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B3I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B3E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B3E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    

    rsNew.Fields.Append "B4A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B4A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B4C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B4C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B4I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B4E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B4E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B5A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B5A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B5C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B5C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B5I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B5E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B5E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B6A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B6A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B6C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B6C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B6I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B6E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B6E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B7A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B7A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B7C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B7C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B7I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B7E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B7E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B8A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B8A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B8C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B8C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B8I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B8E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B8E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B9A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B9A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B9C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B9C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B9I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B9E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B9E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B10A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B10A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B10C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B10C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B10I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B10E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B10E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B11A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B11A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B11C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B11C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B11I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B11E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B11E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B12A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B12A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B12C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B12C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B12I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B12E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B12E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B13A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B13A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B13C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B13C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B13I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B13E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B13E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B14A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B14A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B14C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B14C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B14I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B14E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B14E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B15A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B15A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B15C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B15C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B15I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B15E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B15E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    
    rsNew.Fields.Append "B16A1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B16A2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B16C1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B16C2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable
    rsNew.Fields.Append "B16I", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B16E1", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    
    rsNew.Fields.Append "B16E2", adBoolean, 2, adFldMayBeNull OR adFldUpdatable    

    rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
    
    'Copia o conteudo dos rds para o rsNew
    'Copia os contextos    
    Do While (Not rsData.EOF)
        rsNew.AddNew
        
        recursoID = rsData.Fields("RecursoID").Value
        dirC1 = rsData.Fields("C1").Value
        dirC2 = rsData.Fields("C2").Value
        dirI = rsData.Fields("I").Value
        dirA1 = rsData.Fields("A1").Value
        dirA2 = rsData.Fields("A2").Value
        dirE1 = rsData.Fields("E1").Value
        dirE2 = rsData.Fields("E2").Value
        
        rsNew.Fields("ControlBar").Value = rsData.Fields("ControlBar").Value
        rsNew.Fields("Combo").Value = rsData.Fields("Combo").Value
        rsNew.Fields("Recurso").Value = rsData.Fields("Recurso").Value
        rsNew.Fields("RecursoID").Value = rsData.Fields("RecursoID").Value
        rsNew.Fields("SubFormID").Value = rsData.Fields("SubFormID").Value
        rsNew.Fields("EhDefault").Value = rsData.Fields("EhDefault").Value
        rsNew.Fields("EstadoInicialID").Value = rsData.Fields("EstadoInicialID").Value
  
        If ( dirC1 AND dirC2 ) Then
            dirC1C2True = TRUE        
        Else
            dirC1C2True = FALSE    
        End If
        
        If ( dirA1 AND dirA2 ) Then
            dirA1A2True = TRUE        
        Else
            dirA1A2True = FALSE    
        End If
        
        If ( dirE1 AND dirE2 ) Then
            dirE1E2True = TRUE        
        Else
            dirE1E2True = FALSE    
        End If

        rsData.MoveNext
        
        Do While (Not rsData.EOF)
            If ( CLng(recursoID) = CLng(rsData.Fields("RecursoID").Value) ) Then
                'dirX1 e dirX2 e o anterior sempre
                If ( (rsData.Fields("C1").Value) AND (rsData.Fields("C2").Value) ) Then
                    dirC1C2True = TRUE        
                End If
                
                If ( (rsData.Fields("A1").Value) AND (rsData.Fields("A2").Value) ) Then
                    dirA1A2True = TRUE        
                End If

                If ( (rsData.Fields("E1").Value) AND (rsData.Fields("E2").Value) ) Then
                    dirE1E2True = TRUE        
                End If

                dirC1 = dirC1 OR rsData.Fields("C1").Value
                dirC2 = dirC2 OR rsData.Fields("C2").Value
                dirI = dirI OR rsData.Fields("I").Value
                dirA1 = dirA1 OR rsData.Fields("A1").Value
                dirA2 = dirA2 OR rsData.Fields("A2").Value
                dirE1 = dirE1 OR rsData.Fields("E1").Value
                dirE2 = dirE2 OR rsData.Fields("E2").Value
                rsData.MoveNext
            Else
                Exit Do
            End If    
        Loop    
        
        If ( (dirC1) AND (dirC2) ) Then
            If ( NOT (dirC1C2True)  ) Then
                dirC1 = FALSE
                dirC2 = TRUE
            End If
        End If    
        
        If ( (dirA1) AND (dirA2) ) Then
            If ( NOT (dirA1A2True)  ) Then
                dirA1 = FALSE
                dirA2 = TRUE
            End If
        End If    

        If ( (dirE1) AND (dirE2) ) Then
            If ( NOT (dirE1E2True)  ) Then
                dirE1 = FALSE
                dirE2 = TRUE
            End If
        End If    
                
        rsNew.Fields("C1").Value = dirC1
        rsNew.Fields("C2").Value = dirC2
        rsNew.Fields("I").Value = dirI
        rsNew.Fields("A1").Value = dirA1
        rsNew.Fields("A2").Value = dirA2
        rsNew.Fields("E1").Value = dirE1
        rsNew.Fields("E2").Value = dirE2
        
        rsNew.Update
    Loop
    
    'Copia os filtros superiores
    While Not rsData2.EOF
        rsNew.AddNew

        rsNew.Fields("ControlBar").Value = rsData2.Fields("ControlBar").Value
        rsNew.Fields("Combo").Value = rsData2.Fields("Combo").Value
        rsNew.Fields("Recurso").Value = rsData2.Fields("Recurso").Value
        rsNew.Fields("RecursoID").Value = rsData2.Fields("RecursoID").Value
        rsNew.Fields("SubFormID").Value = rsData2.Fields("SubFormID").Value
        rsNew.Fields("EhDefault").Value = rsData2.Fields("EhDefault").Value
        rsNew.Fields("EstadoInicialID").Value = rsData2.Fields("EstadoInicialID").Value

        rsNew.Update
        rsData2.MoveNext
    Wend
    
    'Copia os filtros inferiores
    While Not rsData3.EOF
        rsNew.AddNew

        rsNew.Fields("ControlBar").Value = rsData3.Fields("ControlBar").Value
        rsNew.Fields("Combo").Value = rsData3.Fields("Combo").Value
        rsNew.Fields("Recurso").Value = rsData3.Fields("Recurso").Value
        rsNew.Fields("RecursoID").Value = rsData3.Fields("RecursoID").Value
        rsNew.Fields("SubFormID").Value = rsData3.Fields("SubFormID").Value
        rsNew.Fields("EhDefault").Value = rsData3.Fields("EhDefault").Value
        rsNew.Fields("EstadoInicialID").Value = rsData3.Fields("EstadoInicialID").Value

        rsNew.Update
        rsData3.MoveNext
    Wend
    
    'Copia as pastas
    Do While (Not rsData4.EOF)
        rsNew.AddNew
        
        recursoID = rsData4.Fields("RecursoID").Value
        dirC1 = rsData4.Fields("C1").Value
        dirC2 = rsData4.Fields("C2").Value
        dirI = rsData4.Fields("I").Value
        dirA1 = rsData4.Fields("A1").Value
        dirA2 = rsData4.Fields("A2").Value
        dirE1 = rsData4.Fields("E1").Value
        dirE2 = rsData4.Fields("E2").Value
        
        rsNew.Fields("ControlBar").Value = rsData4.Fields("ControlBar").Value
        rsNew.Fields("Combo").Value = rsData4.Fields("Combo").Value
        rsNew.Fields("Recurso").Value = rsData4.Fields("Recurso").Value
        rsNew.Fields("RecursoID").Value = rsData4.Fields("RecursoID").Value
        rsNew.Fields("SubFormID").Value = rsData4.Fields("SubFormID").Value
        rsNew.Fields("EhDefault").Value = rsData4.Fields("EhDefault").Value
        rsNew.Fields("EstadoInicialID").Value = rsData4.Fields("EstadoInicialID").Value
      
        If ( dirC1 AND dirC2 ) Then
            dirC1C2True = TRUE        
        Else
            dirC1C2True = FALSE    
        End If
        
        If ( dirA1 AND dirA2 ) Then
            dirA1A2True = TRUE        
        Else
            dirA1A2True = FALSE    
        End If
        
        If ( dirE1 AND dirE2 ) Then
            dirE1E2True = TRUE        
        Else
            dirE1E2True = FALSE    
        End If

        rsData4.MoveNext
  
        Do While (Not rsData4.EOF)
            If ( CLng(recursoID) = CLng(rsData4.Fields("RecursoID").Value) ) Then
                'dirX1 e dirX2 e o anterior sempre
                If ( (rsData4.Fields("C1").Value) AND (rsData4.Fields("C2").Value) ) Then
                    dirC1C2True = TRUE        
                End If
                
                If ( (rsData4.Fields("A1").Value) AND (rsData4.Fields("A2").Value) ) Then
                    dirA1A2True = TRUE        
                End If

                If ( (rsData4.Fields("E1").Value) AND (rsData4.Fields("E2").Value) ) Then
                    dirE1E2True = TRUE        
                End If

                dirC1 = dirC1 OR rsData4.Fields("C1").Value
                dirC2 = dirC2 OR rsData4.Fields("C2").Value
                dirI = dirI OR rsData4.Fields("I").Value
                dirA1 = dirA1 OR rsData4.Fields("A1").Value
                dirA2 = dirA2 OR rsData4.Fields("A2").Value
                dirE1 = dirE1 OR rsData4.Fields("E1").Value
                dirE2 = dirE2 OR rsData4.Fields("E2").Value
                rsData4.MoveNext
            Else
                Exit Do
            End If    
        Loop    
        
        If ( (dirC1) AND (dirC2) ) Then
            If ( NOT (dirC1C2True)  ) Then
                dirC1 = FALSE
                dirC2 = TRUE
            End If
        End If    
        
        If ( (dirA1) AND (dirA2) ) Then
            If ( NOT (dirA1A2True)  ) Then
                dirA1 = FALSE
                dirA2 = TRUE
            End If
        End If    

        If ( (dirE1) AND (dirE2) ) Then
            If ( NOT (dirE1E2True)  ) Then
                dirE1 = FALSE
                dirE2 = TRUE
            End If
        End If    
                
        rsNew.Fields("C1").Value = dirC1
        rsNew.Fields("C2").Value = dirC2
        rsNew.Fields("I").Value = dirI
        rsNew.Fields("A1").Value = dirA1
        rsNew.Fields("A2").Value = dirA2
        rsNew.Fields("E1").Value = dirE1
        rsNew.Fields("E2").Value = dirE2
        
        rsNew.Update
    Loop
    
    'False em todos os campos de bits dos botoes especificos
    rsNew.MoveFirst
    
    Do While (Not rsNew.EOF)
        For i = 1 To 16
            rsNew.Fields("B" & CStr(i) & "A1") = FALSE
            rsNew.Fields("B" & CStr(i) & "A2") = FALSE
            rsNew.Fields("B" & CStr(i) & "C1") = FALSE
            rsNew.Fields("B" & CStr(i) & "C2") = FALSE
            rsNew.Fields("B" & CStr(i) & "I") = FALSE
            rsNew.Fields("B" & CStr(i) & "E1") = FALSE
            rsNew.Fields("B" & CStr(i) & "E2") = FALSE
        Next

        rsNew.Update
        rsNew.MoveNext
    Loop    
  
    Dim rsData5

    Set rsData5 = Server.CreateObject("ADODB.Recordset")
    
    'Os Direitos a botoes especificos dos SubForms

    strSQL = "SELECT DISTINCT b.RecursoID AS SubFormID, b.Principal AS Principal, " & _
             "c.SujeitoID AS BotaoID, g.Alterar1,g.Alterar2, g.Consultar1, g.Consultar2, g.Incluir, g.Excluir1, g.Excluir2 " & _
             "FROM RelacoesRecursos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _ 
             "RelacoesPesRec e WITH(NOLOCK), RelacoesPesRec_Perfis f WITH(NOLOCK), Recursos_Direitos g WITH(NOLOCK), Recursos h WITH(NOLOCK) " & _
             "WHERE a.ObjetoID= " & contextID & " AND a.TipoRelacaoID=1 AND a.EstadoID=2 AND a.SujeitoID=b.RecursoID " & _ 
             "AND (b.Principal=1 OR b.Principal=0) AND b.EstadoID=2 AND b.TipoRecursoID=4 AND b.RecursoID=c.ObjetoID " & _ 
             "AND c.TipoRelacaoID=2 AND c.EstadoID=2 AND c.SujeitoID=d.RecursoID AND d.EstadoID=2 AND d.TipoRecursoID=5 " & _
             "AND d.ClassificacaoID=13 AND (d.RecursoID BETWEEN 40001 AND 40016) " & _ 
             "AND ((c.RelacaoID IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK) WHERE a.RecursoID= " & contextID & " )) " & _
             "OR (c.RelacaoID NOT IN (SELECT a.RelacaoID FROM RelacoesRecursos_Contextos a WITH(NOLOCK)))) " & _
             "AND e.ObjetoID=999 AND e.TipoRelacaoID=11 " & _
			 "AND e.SujeitoID IN ((SELECT " & CStr(userID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(userID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
             "AND e.RelacaoID=f.RelacaoID " & _
             "AND f.EmpresaID= " & CStr(empresaID) & " AND g.RecursoID=d.RecursoID " & _ 
             "AND g.RecursoMaeID=b.RecursoID " & _
             "AND g.ContextoID= " & contextID & " AND g.PerfilID=f.PerfilID " & _
             "AND f.PerfilID = h.RecursoID AND h.EstadoID = 2 " & _
             "ORDER BY SubFormID"
             
    rsData5.Open strSQL, strConn,adOpenStatic, adLockReadOnly, adCmdText
    
    If (rsData5.RecordCount > 0) Then
        rsData5.CacheSize = rsData5.RecordCount
    End if    
    
    If ( NOT(rsData5.BOF AND rsData5.EOF) ) Then
    
        'Primeiro os direitos dos botoes especificos do control bar superior.
        'So interessa o contexto default
        rsNew.MoveFirst
        rsNew.Find("RecursoID = " & CStr(contextID))

        rsData5.Find("Principal = 1")

        If ( (NOT (rsNew.EOF)) AND (NOT (rsData5.EOF))  ) Then

            For i = 1 To 16
                sBtnNumber = CStr(40000 + i)
                
                rsData5.MoveFirst
                rsData5.Filter = "Principal = 1 AND BotaoID=" & sBtnNumber

                Do While (NOT rsData5.EOF)
                    dirA1 = rsData5.Fields("Alterar1").Value
                    dirA2 = rsData5.Fields("Alterar2").Value
                    dirC1 = rsData5.Fields("Consultar1").Value
                    dirC2 = rsData5.Fields("Consultar2").Value
                    dirI = rsData5.Fields("Incluir").Value
                    dirE1 = rsData5.Fields("Excluir1").Value
                    dirE2 = rsData5.Fields("Excluir2").Value
                    
                    If ( (dirA1) AND (dirA2) ) Then
                        dirA1A2True = TRUE        
                    Else
                        dirA1A2True = FALSE    
                    End If
                    
                    If ( (dirC1) AND (dirC2) ) Then
                        dirC1C2True = TRUE        
                    Else
                        dirC1C2True = FALSE    
                    End If
                    
                    If ((dirE1) AND (dirE2)) Then
                        dirE1E2True = TRUE
                    Else
                        dirE1E2True = FALSE
                    End If
                    
                    rsData5.MoveNext

                    Do While (NOT rsData5.EOF)
                    
                        If ( (rsData5.Fields("Alterar1").Value) AND (rsData5.Fields("Alterar2").Value) ) Then
                            dirA1A2True = TRUE        
                        End If
                        dirA1 = dirA1 OR rsData5.Fields("Alterar1").Value
                        dirA2 = dirA2 OR rsData5.Fields("Alterar2").Value
                        
                        If ( (rsData5.Fields("Consultar1").Value) AND (rsData5.Fields("Consultar2").Value) ) Then
                            dirC1C2True = TRUE        
                        End If
                        dirC1 = dirC1 OR rsData5.Fields("Consultar1").Value
                        dirC2 = dirC2 OR rsData5.Fields("Consultar2").Value
                        
                        If ( (rsData5.Fields("Excluir1").Value) AND (rsData5.Fields("Excluir2").Value) ) Then
                            dirE1E2True = TRUE
                        End If
                        dirE1 = dirE1 OR rsData5.Fields("Excluir1").Value
                        dirE2 = dirE2 OR rsData5.Fields("Excluir2").Value

                        dirI = dirI OR rsData5.Fields("Incluir").Value

                        rsData5.MoveNext
                    Loop

                    If ( (dirA1) AND (dirA2) ) Then
                        If ( NOT (dirA1A2True)  ) Then
                            dirA1 = FALSE
                            dirA2 = TRUE
                        End If
                    End If    
                    
                    If ( (dirC1) AND (dirC2) ) Then
                        If ( NOT (dirC1C2True)  ) Then
                            dirC1 = FALSE
                            dirC2 = TRUE
                        End If
                    End If    
        
                    If ( (dirE1) AND (dirE2) ) Then
                        If ( NOT (dirE1E2True)  ) Then
                            dirE1 = FALSE
                            dirE2 = TRUE
                        End If
                    End If    

                    rsNew.Fields(("B" & CStr(i) & "A1")).Value = dirA1
                    rsNew.Fields(("B" & CStr(i) & "A2")).Value = dirA2
                    rsNew.Fields(("B" & CStr(i) & "C1")).Value = dirC1
                    rsNew.Fields(("B" & CStr(i) & "C2")).Value = dirC2
                    rsNew.Fields(("B" & CStr(i) & "I")).Value = dirI
                    rsNew.Fields(("B" & CStr(i) & "E1")).Value = dirE1
                    rsNew.Fields(("B" & CStr(i) & "E2")).Value = dirE2
                    rsNew.Update        
                Loop
                
                rsData5.Filter = ""

            Next

        End If

        'Agora os direitos dos botoes especificos do control bar inferior.
        'Para todas as pastas
        rsNew.MoveFirst
        
        Do While ( Not (rsNew.EOF) )
        
            folderID = 0
            If ( ((rsNew.Fields("ControlBar").Value) = "inf") AND ((rsNew.Fields("Combo").Value) = 1) ) Then
                folderID = rsNew.Fields("RecursoID").Value
            End If

            rsData5.MoveFirst
            rsData5.Find("SubFormID = " & CStr(folderID))

            If ( (CLng(folderID) <>  0) AND ( NOT rsData5.EOF) ) Then

                For i = 1 To 16
                    
                    sBtnNumber = CStr(40000 + i)
                    
                    rsData5.MoveFirst
                    rsData5.Filter = "SubFormID=" & CStr(folderID) &  " AND Principal = 0 AND BotaoID=" & sBtnNumber

                    Do While (NOT rsData5.EOF)
                        dirA1 = rsData5.Fields("Alterar1").Value
                        dirA2 = rsData5.Fields("Alterar2").Value
                        dirC1 = rsData5.Fields("Consultar1").Value
                        dirC2 = rsData5.Fields("Consultar2").Value
                        dirI = rsData5.Fields("Incluir").Value
                        dirE1 = rsData5.Fields("Excluir1").Value
                        dirE2 = rsData5.Fields("Excluir2").Value
                        
                        If ( (dirA1) AND (dirA2) ) Then
                            dirA1A2True = TRUE        
                        Else
                            dirA1A2True = FALSE    
                        End If
                        
                        If ( (dirC1) AND (dirC2) ) Then
                            dirC1C2True = TRUE        
                        Else
                            dirC1C2True = FALSE    
                        End If
                        
                        If ((dirE1) AND (dirE2)) Then
                            dirE1E2True = TRUE
                        Else
                            dirE1E2True = FALSE
                        End If
                        
                        rsData5.MoveNext

                        Do While (NOT rsData5.EOF)
                        
                            If ( (rsData5.Fields("Alterar1").Value) AND (rsData5.Fields("Alterar2").Value) ) Then
                                dirA1A2True = TRUE        
                            End If
                            dirA1 = dirA1 OR rsData5.Fields("Alterar1").Value
                            dirA2 = dirA2 OR rsData5.Fields("Alterar2").Value
                            
                            If ( (rsData5.Fields("Consultar1").Value) AND (rsData5.Fields("Consultar2").Value) ) Then
                                dirC1C2True = TRUE        
                            End If
                            dirC1 = dirC1 OR rsData5.Fields("Consultar1").Value
                            dirC2 = dirC2 OR rsData5.Fields("Consultar2").Value
                            
                            If ( (rsData5.Fields("Excluir1").Value) AND (rsData5.Fields("Excluir2").Value) ) Then
                                dirE1E2True = TRUE
                            End If
                            dirE1 = dirE1 OR rsData5.Fields("Excluir1").Value
                            dirE2 = dirE2 OR rsData5.Fields("Excluir2").Value

                            dirI = dirI OR rsData5.Fields("Incluir").Value

                            rsData5.MoveNext
                        Loop

                        If ( (dirA1) AND (dirA2) ) Then
                            If ( NOT (dirA1A2True)  ) Then
                                dirA1 = FALSE
                                dirA2 = TRUE
                            End If
                        End If    
                        
                        If ( (dirC1) AND (dirC2) ) Then
                            If ( NOT (dirC1C2True)  ) Then
                                dirC1 = FALSE
                                dirC2 = TRUE
                            End If
                        End If    
            
                        If ( (dirE1) AND (dirE2) ) Then
                            If ( NOT (dirE1E2True)  ) Then
                                dirE1 = FALSE
                                dirE2 = TRUE
                            End If
                        End If    
        
                        rsNew.Fields(("B" & CStr(i) & "A1")).Value = dirA1
                        rsNew.Fields(("B" & CStr(i) & "A2")).Value = dirA2
                        rsNew.Fields(("B" & CStr(i) & "C1")).Value = dirC1
                        rsNew.Fields(("B" & CStr(i) & "C2")).Value = dirC2
                        rsNew.Fields(("B" & CStr(i) & "I")).Value = dirI
                        rsNew.Fields(("B" & CStr(i) & "E1")).Value = dirE1
                        rsNew.Fields(("B" & CStr(i) & "E2")).Value = dirE2
                        rsNew.Update        
                    Loop
                    
                    rsData5.Filter = ""

                Next

            End If
            
            rsNew.MoveNext
            
        Loop    

    End If
        
    '----------------------------------------------------------------
    'Fecha objetos ap�s mandar devolver o XML
   
    rsNew.Save Response, adPersistXML
    
    rsNew.Close
    Set rsNew = Nothing
    
    rsData5.Close
    Set rsData5 = Nothing
    
    rsData4.Close
    Set rsData4 = Nothing
    
    rsData3.Close
    Set rsData3 = Nothing
    
    rsData2.Close
    Set rsData2 = Nothing
    
    rsData.Close
    Set rsData = Nothing
    
    Set rsSPCommand = Nothing
%>