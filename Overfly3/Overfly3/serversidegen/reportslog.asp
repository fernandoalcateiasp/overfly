
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew
Dim strSQL
Dim nRecsAffected
Dim rsCommand
Dim i
Dim ReportID
Dim UserID
Dim EmpresaID
  
strSQL = ""
ReportID = 0
UserID = 0
EmpresaID = 0

Set rsCommand = Server.CreateObject("ADODB.Command")

For i = 1 To Request.QueryString("ReportID").Count    
    ReportID = Request.QueryString("ReportID")(i)
Next

For i = 1 To Request.QueryString("UserID").Count
    UserID = Request.QueryString("UserID")(i)
Next

For i = 1 To Request.QueryString("EmpresaID").Count
    EmpresaID = Request.QueryString("EmpresaID")(i)
Next

strSQL = "INSERT INTO _reportLog (EmpresaID, ReportID, UserID, DateStart) " & _
    "SELECT " & CStr(EmpresaID) & "," & CStr(ReportID) & "," & CStr(UserID) & ", GETDATE()"

If (strSQL <> "") Then
	rsCommand.CommandTimeout = 60 * 10
    rsCommand.ActiveConnection = strConn
    rsCommand.CommandText = strSQL
    rsCommand.CommandType = adCmdText
    rsCommand.Execute nRecsAffected
End If

Set rsCommand = Nothing

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "fldresp", adVarChar, 50, adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew
rsNew.Fields("fldresp").Value = ""
rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
%>
