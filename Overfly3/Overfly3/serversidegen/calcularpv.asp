
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    Dim i
    Dim nEmpresaID, nProdutos, nPrevisaVendas

    For i = 1 To Request.QueryString("nEmpresaID").Count
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next

    Redim nProdutoID(Request.QueryString("nProdutoID").Count - 1)

    For i = 1 To Request.QueryString("nProdutoID").Count
        nProdutoID(i-1) = Request.QueryString("nProdutoID")(i)
    Next

    Dim rsSPCommand
    
    For i = 0 To (Request.QueryString("nProdutoID").Count - 1)
	    Set rsSPCommand = Server.CreateObject("ADODB.Command")

	    With rsSPCommand
		    .CommandTimeout = 60 * 10
	        .ActiveConnection = strConn
	        .CommandText = "sp_Produto_PrevisaoVendas"
	        .CommandType = adCmdStoredProc

	        .Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
	        .Parameters("@EmpresaID").Value = CLng(nEmpresaID)

	        .Parameters.Append( .CreateParameter("@ProdutoID", adInteger, adParamInput) )
		    .Parameters("@ProdutoID").Value = CLng(nProdutoID(i))
		    
		    .Execute    		
		    
	    End With	
	    
	    Set rsSPCommand = Nothing
	Next
	
	Dim rsNew
	Set rsNew = Server.CreateObject("ADODB.Recordset")
	rsNew.CursorLocation = adUseServer
	rsNew.Fields.Append "Resultado", adVarchar, 30, adFldMayBeNull OR adFldUpdatable
	rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
	rsNew.AddNew
    rsNew.Fields("Resultado").Value = "Previs�o de vendas OK"
	rsNew.Update
	rsNew.Save Response, adPersistXML
	rsNew.Close
	Set rsNew = Nothing
%>
