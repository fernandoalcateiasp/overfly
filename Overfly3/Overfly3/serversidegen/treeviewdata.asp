
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Response.Expires=0

Dim rsData, strSQL, i, userID, empresaID

userID = 0
empresaID = 0

'Recupera a variavel userID
For i = 1 To Request.QueryString("userID").Count    
    userID = Request.QueryString("userID")(i)
Next

'Recupera a variavel empresaID
For i = 1 To Request.QueryString("empresaID").Count    
    empresaID = Request.QueryString("empresaID")(i)
Next

'----------------------------------------------------------------

strSQL = "SELECT DISTINCT d.Recurso AS Modulo,e.Recurso AS SubModulo, " & _ 
         "f.Recurso AS FormTitle,f.FormName AS FormName,d.RecursoID,e.RecursoID,f.RecursoID AS btnLeftRightOrdem, f.NomeBotao AS BtnValue, ISNULL(f.Ordem, 99) AS BtnOrdem " & _
         "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Recursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), Recursos e WITH(NOLOCK), Recursos f WITH(NOLOCK), " & _
         "Recursos_Direitos g WITH(NOLOCK), Recursos_Direitos h WITH(NOLOCK), Recursos_Direitos i WITH(NOLOCK), Recursos_Direitos j WITH(NOLOCK), Recursos l WITH(NOLOCK) " & _
         "WHERE a.SujeitoID IN ((SELECT " & CStr(userID) & " UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = " & CStr(userID) & " AND GETDATE() BETWEEN dtInicio AND dtFim))) " & _
         "AND a.ObjetoID=999 AND a.TipoRelacaoID=11 AND a.RelacaoID=b.RelacaoID " & _
         "AND b.EmpresaID= " & CStr(EmpresaID) & " AND c.EstadoID=2 AND c.TipoRecursoID=1 " & _
         "AND c.RecursoID=d.RecursoMaeID AND d.EstadoID=2 AND d.TipoRecursoID=2 AND d.ClassificacaoID=9 " & _
         "AND d.RecursoID=e.RecursoMaeID AND e.EstadoID=2 AND e.TipoRecursoID=2 AND e.ClassificacaoID=10 " & _
         "AND e.RecursoID=f.RecursoMaeID AND f.EstadoID=2 AND f.TipoRecursoID=2 AND f.ClassificacaoID=11 " & _
         "AND c.RecursoID=g.RecursoID AND b.PerfilID=g.PerfilID AND (g.Consultar1=1 OR g.Consultar2=1) " & _
         "AND d.RecursoID=h.RecursoID AND b.PerfilID=h.PerfilID AND (h.Consultar1=1 OR h.Consultar2=1) " & _
         "AND e.RecursoID=i.RecursoID AND b.PerfilID=i.PerfilID AND (i.Consultar1=1 OR i.Consultar2=1) " & _
         "AND f.RecursoID=j.RecursoID AND b.PerfilID=j.PerfilID AND (j.Consultar1=1 OR j.Consultar2=1) " & _
         "AND b.PerfilID = l.RecursoID AND l.EstadoID = 2 " & _
         "ORDER BY d.RecursoID,e.RecursoID,f.RecursoID "

Set rsData = Server.CreateObject("ADODB.Recordset")

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

'Fecha objetos ap�s mandar devolver o XML
rsData.Save Response, adPersistXML

rsData.Close
Set rsData = Nothing
'----------------------------------------------------------------

%>