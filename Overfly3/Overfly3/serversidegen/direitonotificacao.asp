
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Dim i, nEmpresaID, nPessoaID, nOverflyID

nEmpresaID = 0
nPessoaID = 0
nOverflyID = 0

For i = 1 To Request.QueryString("nEmpresaID").Count    
  nEmpresaID = CInt(Request.QueryString("nEmpresaID")(i))
Next

For i = 1 To Request.QueryString("nPessoaID").Count    
  nPessoaID = CInt(Request.QueryString("nPessoaID")(i))
Next

For i = 1 To Request.QueryString("nOverflyID").Count    
  nOverflyID = CInt(Request.QueryString("nOverflyID")(i))
Next

Dim rsData, strSQL
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT dbo.fn_Direitos(" & nEmpresaID  & ", " & nOverflyID  & ", NULL, NULL, NULL, NULL, NULL, 3, " & nPessoaID  & ", 1, 1) AS Direito"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

rsData.Save Response,adPersistXML
rsData.Close
Set rsData = Nothing
%>
