
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    'Captura os parametros de pesquisa
	Dim i
    Dim nUserID, nDepositoID, nEmpresaID, nPessoaID, nTransacaoID, nFinanciamentoID, nEncomenda, nLoteID, nConsolidar, nNovoLoteID, nCriaProdutoLote, nLotPedItemID
    Dim nProdutoID, nQuantidade, nCusto, nPedidoID, nPedItemCompraID, nPedItemVendaID, strSQL, sDescricao, sProjetoID
    Dim sResultado
    
    i = 0
    nUserID = 0
    nDepositoID = 0
    nEmpresaID = 0
    nPessoaID = 0
    nTransacaoID = 0
    nFinanciamentoID = 0
    nEncomenda = 0
    nPedItemCompraID = 0
    nPedItemVendaID = 0
    nLoteID = null
    nNovoLoteID = 0
    nConsolidar = 0
    strSQL = ""
    sDescricao = ""
    nCriaProdutoLote = 0
    sResultado = ""
    nLotPedItemID = 0
    
	For i = 1 To Request.QueryString("nUserID").Count    
	  nUserID = Request.QueryString("nUserID")(i)
	Next
	
	For i = 1 To Request.QueryString("nDepositoID").Count    
	  nDepositoID = Request.QueryString("nDepositoID")(i)
	Next

	For i = 1 To Request.QueryString("nEmpresaID").Count    
	  nEmpresaID = Request.QueryString("nEmpresaID")(i)
	Next

	For i = 1 To Request.QueryString("nPessoaID").Count    
	  nPessoaID = Request.QueryString("nPessoaID")(i)
	Next

	For i = 1 To Request.QueryString("nTransacaoID").Count    
	  nTransacaoID = Request.QueryString("nTransacaoID")(i)
	Next

    For i = 1 To Request.QueryString("nFinanciamentoID").Count    
	  nFinanciamentoID = Request.QueryString("nFinanciamentoID")(i)
	Next
	
	For i = 1 To Request.QueryString("nEncomenda").Count    
	  nEncomenda = Request.QueryString("nEncomenda")(i)
	Next
	
	For i = 1 To Request.QueryString("nConsolidar").Count    
	  nConsolidar = Request.QueryString("nConsolidar")(i)
	Next

    Redim nProdutoID(Request.QueryString("nProdutoID").Count - 1)
    Redim nQuantidade(Request.QueryString("nQuantidade").Count - 1)
    Redim nCusto(Request.QueryString("nCusto").Count - 1)
    Redim nPedItemVendaID(Request.QueryString("nPedItemVendaID").Count - 1)
    Redim nLoteID(Request.QueryString("nLoteID").Count - 1)
    Redim sDescricao(Request.QueryString("sDescricao").Count - 1)
    Redim sProjetoID(Request.QueryString("sProjetoID").Count - 1)
    Redim nLotPedItemID(Request.QueryString("nLoteID").Count - 1, 2)
    Redim sResultado(Request.QueryString("nLoteID").Count - 1, 2)
    
    For i = 1 To Request.QueryString("nProdutoID").Count    
	  nProdutoID(i-1) = Request.QueryString("nProdutoID")(i)
	Next

    For i = 1 To Request.QueryString("nQuantidade").Count    
	  nQuantidade(i-1) = Request.QueryString("nQuantidade")(i)
	Next
	
    For i = 1 To Request.QueryString("nCusto").Count    
	  nCusto(i-1) = Request.QueryString("nCusto")(i)
	Next
	
	For i = 1 To Request.QueryString("nPedItemVendaID").Count    
	  nPedItemVendaID(i-1) = Request.QueryString("nPedItemVendaID")(i)
	Next

    For i = 1 To Request.QueryString("nLoteID").Count    
	  nLoteID(i-1) = Request.QueryString("nLoteID")(i)
	Next

    For i = 1 To Request.QueryString("sDescricao").Count
	  sDescricao(i-1) = Request.QueryString("sDescricao")(i)
	Next
	
	For i = 1 To Request.QueryString("sProjetoID").Count
	  sProjetoID(i-1) = Request.QueryString("sProjetoID")(i)
	Next
	
	Dim rsSPCommand
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
	
	Dim rsCommand, nRecsAffected
	  
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_Pedido_Gerador"
	    .CommandType = adCmdStoredProc

	    .Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
	    .Parameters("@EmpresaID").Value = CLng(nEmpresaID)
	    
		.Parameters.Append( .CreateParameter("@PessoaID", adInteger, adParamInput) )
		.Parameters("@PessoaID").Value = CLng(nPessoaID)
	    
	    .Parameters.Append( .CreateParameter("@ParceiroID", adInteger, adParamInput) )
	    .Parameters("@ParceiroID").Value = CLng(nPessoaID)

	    .Parameters.Append( .CreateParameter("@EhCliente", adInteger, adParamInput) )
	    .Parameters("@EhCliente").Value = 0
	    
	    .Parameters.Append( .CreateParameter("@TransacaoID", adInteger, adParamInput) )
	    .Parameters("@TransacaoID").Value = CLng(nTransacaoID)

	    .Parameters.Append( .CreateParameter("@Observacao", adVarChar, adParamInput, 40) )
	    .Parameters("@Observacao").Value = Null

	    .Parameters.Append( .CreateParameter("@SeuPedido", adVarChar, adParamInput, 30) )
	    .Parameters("@SeuPedido").Value = null

	    .Parameters.Append( .CreateParameter("@OrigemPedidoID", adInteger, adParamInput) )
	    .Parameters("@OrigemPedidoID").Value = 605

	    .Parameters.Append( .CreateParameter("@FinanciamentoID", adInteger, adParamInput) )
	    .Parameters("@FinanciamentoID").Value = CLng(nFinanciamentoID)

	    .Parameters.Append( .CreateParameter("@dtPrevisaoEntrega", adDate, adParamInput) )
	    .Parameters("@dtPrevisaoEntrega").Value = Null

	    .Parameters.Append( .CreateParameter("@TransportadoraID", adInteger, adParamInput) )
	    .Parameters("@TransportadoraID").Value = null

	    .Parameters.Append( .CreateParameter("@MeioTransporteID", adInteger, adParamInput) )
	    .Parameters("@MeioTransporteID").Value = null

	    .Parameters.Append( .CreateParameter("@ModalidadeTransporteID", adInteger, adParamInput) )
	    .Parameters("@ModalidadeTransporteID").Value = null

	    .Parameters.Append( .CreateParameter("@Frete", adBoolean, adParamInput) )
	    .Parameters("@Frete").Value = 0
	    
		.Parameters.Append( .CreateParameter("@DepositoID", adInteger, adParamInput) )
		.Parameters("@DepositoID").Value = CLng(nDepositoID)

	    .Parameters.Append( .CreateParameter("@ProprietarioID", adInteger, adParamInput) )
	    .Parameters("@ProprietarioID").Value = CLng(nUserID)

	    .Parameters.Append( .CreateParameter("@AlternativoID", adInteger, adParamInput) )
	    .Parameters("@AlternativoID").Value = CLng(nUserID)

	    .Parameters.Append( .CreateParameter("@Observacoes", adVarChar, adParamInput, 8000) )
	    .Parameters("@Observacoes").Value = null
	    
	    .Parameters.Append( .CreateParameter("@NumProjeto", adVarChar, adParamInput, 20) )
	    .Parameters("@NumProjeto").Value = sProjetoID(0)

        .Parameters.Append( .CreateParameter("@ProgramaMarketingID", adInteger, adParamInput) )
	    .Parameters("@ProgramaMarketingID").Value = null

	    .Parameters.Append( .CreateParameter("@URLEtiqueta", adVarChar, adParamInput, 256) )
	    .Parameters("@URLEtiqueta").Value = null

		.Parameters.Append( .CreateParameter("@PedidoID", adInteger, adParamOutput) )
	    
	    .Parameters.Append( .CreateParameter("@Resultado", adVarchar, adParamOutput, 100) )

	    .Execute
	    
	End With

	nPedidoID = CLng(rsSPCommand.Parameters("@PedidoID").Value)
	
	Dim rsSPCommand1
	Set rsSPCommand1 = Server.CreateObject("ADODB.Command")

    ' Gera item do pedido de compra
	With rsSPCommand1
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_PedidoItem_Gerador"
	    .CommandType = adCmdStoredProc
	      
	    .Parameters.Append( .CreateParameter("@PedidoID", adInteger, adParamInput) )
	    .Parameters("@PedidoID").Value = CLng(nPedidoID)

	    .Parameters.Append( .CreateParameter("@ProdutoID", adInteger, adParamInput) )
	    
	    .Parameters.Append( .CreateParameter("@ServicoID", adInteger, adParamInput) )
        .Parameters("@ServicoID").Value = null

	    .Parameters.Append( .CreateParameter("@Quantidade", adInteger, adParamInput) )
	    
	    .Parameters.Append( .CreateParameter("@ValorInterno", adCurrency, adParamInput) )
	    .Parameters("@ValorInterno").Value = null
	    
	    .Parameters.Append( .CreateParameter("@ValorICMSSTInterno", adCurrency, adParamInput) )
	    .Parameters("@ValorICMSSTInterno").Value = null
	    
	    .Parameters.Append( .CreateParameter("@ValorRevenda", adCurrency, adParamInput) )
	    .Parameters("@ValorRevenda").Value = null
	    
	    .Parameters.Append( .CreateParameter("@ValorICMSSTRevenda", adCurrency, adParamInput) )
	    .Parameters("@ValorICMSSTRevenda").Value = null

	    .Parameters.Append( .CreateParameter("@ValorUnitario", adCurrency, adParamInput) )

	    .Parameters.Append( .CreateParameter("@FinalidadeID", adInteger, adParamInput) )
	    .Parameters("@FinalidadeID").Value = null

	    .Parameters.Append( .CreateParameter("@CFOPID", adInteger, adParamInput) )
	    .Parameters("@CFOPID").Value = null

	    .Parameters.Append( .CreateParameter("@PedidoReferencia", adInteger, adParamInput) )
	    
        .Parameters.Append( .CreateParameter("@PedItemEncomendaID", adInteger, adParamInput) )
	    .Parameters("@PedItemEncomendaID").Value = null 'Novo parametro, tratar aqui.

	    .Parameters.Append( .CreateParameter("@TipoGerador", adInteger, adParamInput) )
	    .Parameters("@TipoGerador").Value = 3
	    
	    .Parameters.Append( .CreateParameter("@NovoPedItemID", adInteger, adParamOutput) )
	    
	    For i = 0 To (Request.QueryString("nProdutoID").Count - 1)
			.Parameters("@ProdutoID").Value = CLng(nProdutoID(i))
			.Parameters("@Quantidade").Value = CLng(nQuantidade(i))
			.Parameters("@ValorUnitario").Value = CDbl(nCusto(i))
			.Parameters("@PedidoReferencia").Value = Null
			.Execute
			
			nPedItemCompraID = .Parameters("@NovoPedItemID").Value
			
			' Se eh compra por encomenda, grava o ID do pedItemID do pedido de compra no 
			' PedItemEncomendaID do pedido de venda correspondente a esta compra
			
            Dim rsSPCommand3, rsSPCommand4
	        Set rsSPCommand3 = Server.CreateObject("ADODB.Command")
	        Set rsSPCommand4 = Server.CreateObject("ADODB.Command")
			
			If ((NOT IsNull(nLoteID(i))) AND (nLoteID(i) <> "")) Then
			    If (nLoteID(i) = 0) Then
			        If ((nConsolidar = 0) OR ((nConsolidar = 1) AND (nNovoLoteID = 0))) Then
                        With rsSPCommand3
	                        .CommandTimeout = 60 * 10
                            .ActiveConnection = strConn
                            .CommandText = "sp_Lote_Gerador"
                            .CommandType = adCmdStoredProc

                            .Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
                            .Parameters("@EmpresaID").Value = nEmpresaID

                            .Parameters.Append( .CreateParameter("@Descricao", adVarChar, adParamInput, 40) )
                            .Parameters("@Descricao").Value = sDescricao(i)

                            .Parameters.Append( .CreateParameter("@dtEmissao", adDate , adParamInput, 8) )
                            .Parameters("@dtEmissao").Value = null

                            .Parameters.Append( .CreateParameter("@dtInicio", adDate , adParamInput, 8) )
                            .Parameters("@dtInicio").Value = null

                            .Parameters.Append( .CreateParameter("@dtFim", adDate , adParamInput, 8) )
                            .Parameters("@dtFim").Value = null
                            
                            .Parameters.Append( .CreateParameter("@Observacao", adVarChar, adParamInput, 40) )
                            .Parameters("@Observacao").Value = "Modal Gerente Produto"
                            
                            .Parameters.Append( .CreateParameter("@ProprietarioID", adInteger, adParamInput) )
                            .Parameters("@ProprietarioID").Value = CLng(nUserID)
                            
                            .Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
                            .Parameters("@UsuarioID").Value = CLng(nUserID)
                            
                            .Parameters.Append( .CreateParameter("@LoteID", adInteger, adParamOutput) )

                            .Execute

	                        If (NOT IsNull(rsSPCommand3.Parameters("@LoteID").Value)) Then
		                        nNovoLoteID = rsSPCommand3.Parameters("@LoteID").Value
	                        End If
                        End With
			        End If
			        
			        nLoteID(i) = nNovoLoteID
			        nCriaProdutoLote = 1
			    End If
			    'Associa Item ao Lote
                With rsSPCommand4
	                .CommandTimeout = 60 * 10
                    .ActiveConnection = strConn
                    .CommandText = "sp_Lote_ItemAssociar"
                    .CommandType = adCmdStoredProc

                    .Parameters.Append( .CreateParameter("@LoteID", adInteger, adParamInput) )
                    .Parameters("@LoteID").Value = nLoteID(i)
                    
                    .Parameters.Append( .CreateParameter("@PedItemID", adInteger, adParamInput) )
                    .Parameters("@PedItemID").Value = nPedItemCompraID
                    
                    .Parameters.Append( .CreateParameter("@Quantidade", adInteger, adParamInput) )
                    .Parameters("@Quantidade").Value = nQuantidade(i)

                    .Parameters.Append( .CreateParameter("@CriarProduto", adBoolean, adParamInput) )
                    .Parameters("@CriarProduto").Value = CBool(nCriaProdutoLote)

                    .Parameters.Append( .CreateParameter("@LotPedItemID", adInteger, adParamOutput) )
                    
		            .Parameters.Append( .CreateParameter("@Resultado", adVarChar, adParamOutput, 8000) )

                    .Execute

	                If (NOT IsNull(rsSPCommand4.Parameters("@LotPedItemID").Value)) Then
		                nLotPedItemID(i, 0) = rsSPCommand4.Parameters("@LotPedItemID").Value
		            Else
		                nLotPedItemID(i, 0) = null
	                End If
            	    
	                If (NOT IsNull(rsSPCommand4.Parameters("@Resultado").Value)) Then
		                sResultado(i, 0) = rsSPCommand4.Parameters("@Resultado").Value
		            Else
		                sResultado(i, 0) = null
	                End If

                    .Parameters("@PedItemID").Value = nPedItemVendaID(i)
                    
                    .Parameters("@CriarProduto").Value = CBool(0)
                    
                    .Execute

	                If (NOT IsNull(rsSPCommand4.Parameters("@LotPedItemID").Value)) Then
		                nLotPedItemID(i, 1) = rsSPCommand4.Parameters("@LotPedItemID").Value
		            Else
		                nLotPedItemID(i, 1) = null
	                End If
            	    
	                If (NOT IsNull(rsSPCommand4.Parameters("@Resultado").Value)) Then
		                sResultado(i, 1) = rsSPCommand4.Parameters("@Resultado").Value
		            Else
		                sResultado(i, 1) = null
	                End If

                End With
                
			    nCriaProdutoLote = 0
			End If

			'If (NOT IsNull(nPedItemCompraID)) Then
			'    If ((nPedItemVendaID(i) <> 0) And (nEncomenda <> 0)) Then
            '        Set rsCommand = Server.CreateObject("ADODB.Command")
            '
			'        strSQL = "UPDATE Pedidos_Itens SET PedItemEncomendaID = " & CLng(nPedItemCompraID) & _
			'        "WHERE PedItemID = " & CLng(nPedItemVendaID(i))
            '
            '        rsCommand.CommandTimeout = 60 * 10
            '        rsCommand.ActiveConnection = strConn
            '        rsCommand.CommandText = strSQL
            '        rsCommand.CommandType = adCmdText
            '        rsCommand.Execute nRecsAffected
            '        
            '        Set rsCommand = Nothing
            '    End If
			'End If
			
		Next
	End With
	
	Dim rsSPCommand2
	Set rsSPCommand2 = Server.CreateObject("ADODB.Command")
	
	With rsSPCommand2
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_Pedido_Parcelas"
	    .CommandType = adCmdStoredProc
	      
	    .Parameters.Append( .CreateParameter("@PedidoID", adInteger, adParamInput) )
	    .Parameters("@PedidoID").Value = CLng(nPedidoID)

	    .Parameters.Append( .CreateParameter("@FormaPagamentoID", adInteger, adParamInput) )
	    .Parameters("@FormaPagamentoID").Value = null

		.Execute

	End With

	Dim rsNew
	Set rsNew = Server.CreateObject("ADODB.Recordset")
	rsNew.CursorLocation = adUseServer

	rsNew.Fields.Append "PedidoID", adDecimal, 10, adFldMayBeNull OR adFldUpdatable
	  
	rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

	rsNew.AddNew
	If ( NOT IsNull(nPedidoID) ) Then
	    rsNew.Fields("PedidoID").Value = nPedidoID
	End If

	rsNew.Update

	rsNew.Save Response, adPersistXML

	Set rsSPCommand = Nothing
	Set rsSPCommand1 = Nothing
	Set rsSPCommand2 = Nothing

	rsNew.Close
	Set rsNew = Nothing

%>