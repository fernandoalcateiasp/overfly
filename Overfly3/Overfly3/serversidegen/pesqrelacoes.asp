
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	'Captura os parametros de pesquisa
	Dim i, nFormID, sComboID, nEmpresaID, sQuemENaRel, nTipoRelacaoID, nRegExcluidoID ,strToFind, sOperator, nKey
	Dim sKey
	
	sKey = ""
    i = 0
    nFormID = 0
    sComboID = ""
    nEmpresaID = 0
    sQuemENaRel = "" 
    nTipoRelacaoID = 0
    nRegExcluidoID = ""
    strToFind = ""   
    sOperator = " >= "
    nKey = 0

	For i = 1 To Request.QueryString("nFormID").Count    
	  nFormID = Request.QueryString("nFormID")(i)
	Next
	
	For i = 1 To Request.QueryString("sComboID").Count    
	  sComboID = Request.QueryString("sComboID")(i)
	Next
	
	For i = 1 To Request.QueryString("nEmpresaID").Count    
	  nEmpresaID = Request.QueryString("nEmpresaID")(i)
	Next
	
	If (CStr(nEmpresaID) = "") Then
        nEmpresaID = 0
    End If        
	
	For i = 1 To Request.QueryString("sQuemENaRel").Count    
	  sQuemENaRel = Request.QueryString("sQuemENaRel")(i)
	Next

	For i = 1 To Request.QueryString("nTipoRelacaoID").Count    
	  nTipoRelacaoID = Request.QueryString("nTipoRelacaoID")(i)
	Next

    If (CStr(nTipoRelacaoID) = "") Then
        nTipoRelacaoID = 0
    End If        
         
	For i = 1 To Request.QueryString("nRegExcluidoID").Count    
	  nRegExcluidoID = Request.QueryString("nRegExcluidoID")(i)
	Next
	
	If (CStr(nRegExcluidoID) = "") Then
        nRegExcluidoID = 0
    End If        

	For i = 1 To Request.QueryString("strToFind").Count    
	    strToFind = "'" & Request.QueryString("strToFind")(i) & "'"
	Next
	
    If ( (Len(strToFind) > 1) AND ((Left(strToFind, 2) = "'%") OR (Right(strToFind, 2) = "%'")) ) Then
        sOperator = " LIKE "
    End If
	
	For i = 1 To Request.QueryString("nKey").Count    
	    nKey = Request.QueryString("nKey")(i)
	Next
	
    Dim rsData, strSQL

    Set rsData = Server.CreateObject("ADODB.Recordset")
    
    'Relacao Entre Pessoas 
    If CInt(nFormID) = 1220 Then
        'Sujeito
        If ( sQuemENaRel = "SUJ" ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " & _
                     "a.Fantasia AS fldName, a.Sexo " & _
                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
        'Clientelia Asstec
        ElseIf ( (sQuemENaRel = "OBJ") AND (nTipoRelacaoID = 22) ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " & _
                     "a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK) " & _ 
                            "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoObjetoID " & _ 
                            "INNER JOIN Pessoas c WITH(NOLOCK) ON c.TipoPessoaID = b.TipoSujeitoID " & _                            
                            "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.ClassificacaoID = d.ItemID " & _                            
                     "WHERE a.EstadoID = 2 " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.PessoaID = " & CStr(nRegExcluidoID) & " " & _
                     "AND d.Filtro LIKE '%<ASS>%' " & _
                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " & _
                     "WHERE a.SujeitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND a.TipoRelacaoID =" & CStr(nTipoRelacaoID) & ") " & _                 
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
    
        'Clientelia Transporte
        ElseIf ( (sQuemENaRel = "OBJ") AND (nTipoRelacaoID = 23) ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, " & _ 
                     "a.PessoaID AS fldID, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK) " & _ 
                            "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoObjetoID " & _
                            "INNER JOIN Pessoas c WITH(NOLOCK) ON c.TipoPessoaID = b.TipoSujeitoID " & _                     
                            "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.ClassificacaoID = d.ItemID " & _                                                        
                     "WHERE a.EstadoID = 2 " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.PessoaID = " & CStr(nRegExcluidoID) & " " & _
                     "AND d.Filtro LIKE '%<TRA>%' " & _
                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " & _
                     "WHERE a.SujeitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND a.TipoRelacaoID =" & CStr(nTipoRelacaoID) & ") " & _                 
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
    
        'Clientelia Banco
        ElseIf ( (sQuemENaRel = "OBJ") AND (nTipoRelacaoID = 24) ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, " & _ 
                     "a.PessoaID AS fldID, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK) " & _ 
                            "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoObjetoID " & _ 
                            "INNER JOIN Pessoas c WITH(NOLOCK) ON c.TipoPessoaID = b.TipoSujeitoID " & _
                            "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.ClassificacaoID = d.ItemID " & _                                                                             
                     "WHERE a.EstadoID = 2 " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.PessoaID = " & CStr(nRegExcluidoID) & " " & _
                     "AND d.Filtro LIKE '%<BAN>%' " & _
                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " & _
                     "WHERE a.SujeitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND a.TipoRelacaoID =" & CStr(nTipoRelacaoID) & ") " & _                 
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
    
        'Clientelia Servi�os
        ElseIf ( (sQuemENaRel = "OBJ") AND (nTipoRelacaoID = 25) ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, " & _ 
                     "a.PessoaID AS fldID, a.Fantasia AS fldName, a.Observacao AS Servico " & _
                     "FROM Pessoas a WITH(NOLOCK) " & _ 
                        "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoObjetoID " & _ 
                        "INNER JOIN Pessoas c WITH(NOLOCK) ON c.TipoPessoaID = b.TipoSujeitoID " & _
                        "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.ClassificacaoID = d.ItemID " & _                                                                             
                     "WHERE a.EstadoID = 2 " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.PessoaID = " & CStr(nRegExcluidoID) & " " & _
                     "AND d.Filtro LIKE '%<PS>%' " & _
                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " & _
                     "WHERE a.SujeitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND a.TipoRelacaoID =" & CStr(nTipoRelacaoID) & ") " & _                 
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"

        'Outras Relacoes
        ElseIf ( (sQuemENaRel = "OBJ") AND (nTipoRelacaoID <> 22) AND (nTipoRelacaoID <> 23) _
                  AND (nTipoRelacaoID <> 24) AND (nTipoRelacaoID <> 25) ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " & _
                     "WHERE a.EstadoID = 2 AND a.TipoPessoaID = b.TipoObjetoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.PessoaID = " & CStr(nRegExcluidoID) & " " & _
                     "AND c.TipoPessoaID = b.TipoSujeitoID " & _
                     "AND a.PessoaID NOT IN (SELECT a.ObjetoID FROM RelacoesPessoas a WITH(NOLOCK) " & _
                     "WHERE a.SujeitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND a.TipoRelacaoID =" & CStr(nTipoRelacaoID) & ") " & _                 
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
        
        'Filiador de Programa de Marketing 
        ElseIf ( sComboID = "selFiliadorID_07" ) Then
         
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = 52 " & _
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName" 
        End If

    'Visitas
    ElseIf CInt(nFormID) = 5240 Then
    
        If ( (sQuemENaRel = "PESSOA") AND (sComboID = "selPessoaID") ) Then
        
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " & _
                     "a.Fantasia AS fldName, a.Sexo " & _
                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
        
        ElseIf ( (sQuemENaRel = "PROSPECT") AND (sComboID = "selProspectID") )Then
        
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.ProspectID AS fldID, " & _
                     "a.Fantasia AS fldName, '' AS Sexo " & _
                     "FROM Prospect a WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 " & _                     
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
                     
        End If

    'Relacoes entre Conceitos
    ElseIf CInt(nFormID) = 2120 Then

        'Sujeito
        If ( sQuemENaRel = "SUJ" ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.ConceitoID AS fldID, a.Conceito AS fldName " & _
                     "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND a.TipoConceitoID = b.TipoSujeitoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND a.Conceito " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldName"

        'Objeto
        ElseIf (sQuemENaRel = "OBJ") Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.ConceitoID AS fldID, a.Conceito AS fldName " & _
                     "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                     "WHERE a.EstadoID = 2 AND a.TipoConceitoID = b.TipoObjetoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.ConceitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND c.TipoConceitoID = b.TipoSujeitoID " & _
                     "AND a.ConceitoID NOT IN (SELECT a.ObjetoID FROM RelacoesConceitos a WITH(NOLOCK) " & _
                     "WHERE a.SujeitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND a.TipoRelacaoID =" & CStr(nTipoRelacaoID) & ") " & _                 
                     "AND a.Conceito " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldName"

        End If 
  
    'Relacoes entre Recursos
    ElseIf CInt(nFormID) = 1120 Then
    
        'Sujeito
        If ( sQuemENaRel = "SUJ" ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.RecursoID AS fldID, a.RecursoFantasia AS fldName " & _
                     "FROM Recursos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND a.TipoRecursoID = b.TipoSujeitoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND a.RecursoFantasia " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldName"

        'Objeto
        ElseIf (sQuemENaRel = "OBJ") Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.RecursoID AS fldID, a.RecursoFantasia AS fldName " & _
                     "FROM Recursos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK) " & _
                     "WHERE a.EstadoID = 2 AND a.TipoRecursoID = b.TipoObjetoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.RecursoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND c.TipoRecursoID = b.TipoSujeitoID " & _
                     "AND a.RecursoID NOT IN (SELECT a.ObjetoID FROM RelacoesRecursos a WITH(NOLOCK) " & _
                     "WHERE a.SujeitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND a.TipoRelacaoID =" & CStr(nTipoRelacaoID) & ") " & _                 
                     "AND a.RecursoFantasia " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldName"

        End If 
        
    'Relacoes entre Pessoas e Conceitos
    ElseIf CInt(nFormID) = 2130 Then

        'Produto
        If ( (sQuemENaRel = "SUJ") AND (nTipoRelacaoID = 61) ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.PessoaID AS fldID, a.Nome AS fldCompleteName, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "AND a.PessoaID = " & CStr(nEmpresaID) & " " & _
                     "ORDER BY fldCompleteName"
                     
        'Fabricante
        ElseIf ( (sQuemENaRel = "SUJ") AND (nTipoRelacaoID = 62) ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.PessoaID AS fldID, a.Nome AS fldCompleteName, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK) " & _
                        "INNER JOIN TiposRelacoes_Itens b WITH(NOLOCK) ON a.TipoPessoaID = b.TipoSujeitoID " & _                     
                        "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON a.ClassificacaoID = c.ItemID " & _                     
                     "WHERE a.EstadoID=2 " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.Filtro LIKE '%<FAB>%' AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
        
        'Marca
        ElseIf ( (sQuemENaRel = "SUJ") AND (nTipoRelacaoID = 63) ) Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.PessoaID AS fldID, a.Nome AS fldCompleteName, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"

        'Objeto
        ElseIf (sQuemENaRel = "OBJ") Then
             
            strSQL = "SELECT DISTINCT TOP 100 a.ConceitoID AS fldID, a.Conceito AS fldName " & _
                     "FROM Conceitos a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK), Pessoas c WITH(NOLOCK) " & _
                     "WHERE a.EstadoID = 2 AND a.TipoConceitoID = b.TipoObjetoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND c.PessoaID = " & CStr(nRegExcluidoID) & " " & _
                     "AND c.TipoPessoaID = b.TipoSujeitoID " & _
                     "AND a.ConceitoID NOT IN (SELECT a.ObjetoID FROM RelacoesPesCon a WITH(NOLOCK) " & _
                     "WHERE a.SujeitoID = " & CStr(nRegExcluidoID) & " " & _
                     "AND a.TipoRelacaoID =" & CStr(nTipoRelacaoID) & ") " & _                 
                     "AND a.Conceito " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldName"
        End If
        
    'Relacoes entre Pessoas e Recursos
    ElseIf CInt(nFormID) = 1130 Then

        'Sujeito
        If ( sQuemENaRel = "SUJ" ) Then

            strSQL = "SELECT DISTINCT TOP 100 a.PessoaID AS fldID, a.Nome AS fldCompleteName, a.Fantasia AS fldName " & _
                     "FROM Pessoas a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND a.TipoPessoaID = b.TipoSujeitoID " & _
                     "AND b.TipoRelacaoID=" & CStr(nTipoRelacaoID) & " " & _
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
        End If
        
	' Pedidos
    ElseIf CInt(nFormID) = 5110 Then

		If (nKey = 1) Then
			sKey = "a.PessoaID"

			If (strToFind = "''") Then
				strToFind = "0"
			End If

		ElseIf (nKey = 2) Then
			sKey = "a.Nome"
		ElseIf (nKey = 3) Then
			sKey = "a.Fantasia"
		End If

		strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " & _ 
		         "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 101, 0) AS Documento, f.Localidade " & _ 
		         "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " & _ 
		         "FROM Pessoas a WITH(NOLOCK) " & _
		         "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " & _
		         "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " & _
		         "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " & _
		         "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " & _
		         "WHERE a.PessoaID IN (319040) " & _
                 "ORDER BY " & sKey
                 
                 'Ship To
                 'Where acima esta duro, voltar o where abaixo quando decidir
                 'como vai ser o select p/ trazer importadoras
                 '"WHERE a.EstadoID = 2 AND " & sKey & " " & sOperator & " " & strToFind & " " & _

    'DPA
    ElseIf CInt(nFormID) = 5150 Then

		Dim sProgramaMarketing
		'selFornecedorID
		If ( sQuemENaRel = "OBJ" ) Then
			strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " & _ 
			         "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 101, 0) AS Documento, f.Localidade " & _ 
			         "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " & _ 
			         "FROM RelacoesPessoas b WITH(NOLOCK), Pessoas a WITH(NOLOCK) " & _
			         "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " & _
			         "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " & _
			         "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " & _
			         "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " & _
			         "WHERE a.EstadoID = 2 AND a.PessoaID=b.ObjetoID AND b.EstadoID=2 AND dbo.fn_Empresa_Sistema(b.SujeitoID) = 1 AND " & _
                     "a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
		'selClienteID
		Else
			If (nRegExcluidoID > 0) Then
				sProgramaMarketing = ", dbo.fn_ProgramaMarketing_Identificador(" & CStr(nRegExcluidoID) & ", a.PessoaID) AS Identificador "
			Else
				sProgramaMarketing = ", NULL AS Identificador " 
			End If

			strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " & _ 
			         "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 101, 0) AS Documento, f.Localidade " & _ 
			         "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " & sProgramaMarketing & " " & _ 
			         "FROM RelacoesPessoas b WITH(NOLOCK), Pessoas a WITH(NOLOCK) " & _
			         "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " & _
			         "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " & _
			         "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " & _
			         "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " & _
			         "WHERE a.EstadoID = 2 AND a.PessoaID=b.SujeitoID AND b.EstadoID=2 AND dbo.fn_Empresa_Sistema(b.ObjetoID) = 1 AND " & _
                     "a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
		End If

    'Campanhas
    ElseIf CInt(nFormID) = 5160 Then

		sProgramaMarketing = ""
		
		'selFornecedorID
		If ( sQuemENaRel = "OBJ" ) Then


            strSQL = "SELECT fldCompleteName, fldID, fldName, dbo.fn_Pessoa_Documento(fldID, NULL, NULL, 111, 101, 0) AS Documento, " & _ 
		                    "Cidade, UF, Pais " & _ 
	                    "FROM (SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " & _ 
				                    "f.Localidade AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " & _ 
			                    "FROM Pessoas a WITH(NOLOCK) " & _ 
				                    "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON a.PessoaID = b.ObjetoID " & _ 
				                    "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " & _ 
				                    "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " & _ 
				                    "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " & _ 
				                    "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " & _ 
				                    "INNER JOIN (SELECT SujeitoID " & _ 
								                    "FROM RelacoesPesRec WITH(NOLOCK) " & _ 
								                    "WHERE TipoRelacaoID = 12 AND EstadoID = 2) EmpSis ON EmpSis.SujeitoID = b.SujeitoID " & _ 
			                    "WHERE b.TipoRelacaoID = 21 " & _ 
					                    "AND a.EstadoID = 2 " & _ 
					                    "AND b.EstadoID = 2 " & _ 
					                    "AND a.Nome " & sOperator & " " & strToFind & " " & _
			                    "ORDER BY fldCompleteName) Pessoas "


		'selClienteID
		Else
			If (nRegExcluidoID > 0) Then
				sProgramaMarketing = ", dbo.fn_ProgramaMarketing_Identificador(" & CStr(nRegExcluidoID) & ", a.PessoaID) AS Identificador "
			Else
				sProgramaMarketing = ", NULL AS Identificador " 
			End If


            strSQL = "SELECT fldCompleteName, fldID, fldName, dbo.fn_Pessoa_Documento(fldID, NULL, NULL, 111, 101, 0) AS Documento, " & _ 
		                    "Cidade, UF, Pais, Identificador " & _ 
	                    "FROM (SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " & _ 
				                    "f.Localidade AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais  " & sProgramaMarketing & " " & _ 
			                    "FROM Pessoas a WITH(NOLOCK) " & _ 
				                    "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON a.PessoaID = b.SujeitoID " & _ 
				                    "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " & _ 
				                    "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " & _ 
				                    "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " & _ 
				                    "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " & _ 
				                    "INNER JOIN (SELECT SujeitoID " & _ 
								                    "FROM RelacoesPesRec WITH(NOLOCK) " & _ 
								                    "WHERE TipoRelacaoID = 12 AND EstadoID = 2) EmpSis ON EmpSis.SujeitoID = b.ObjetoID " & _ 
			                    "WHERE b.TipoRelacaoID = 21 " & _ 
					                    "AND a.EstadoID = 2 " & _ 
					                    "AND b.EstadoID = 2 " & _ 
					                    "AND a.Nome " & sOperator & " " & strToFind & " " & _
			                    "ORDER BY fldCompleteName) Pessoas "

		End If

    'RRC ou PSC
    ElseIf (CInt(nFormID) = 7140) OR (CInt(nFormID) = 7150) Then

		'selClienteID
		If (nRegExcluidoID = "0") Then
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " & _
                     "a.Fantasia AS fldName, " & _
                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " & _
                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " & _
                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " & _
                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " & _
                     "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND b.EstadoID=2 AND a.PessoaID=b.SujeitoID AND b.ObjetoID = " & Cstr(nEmpresaID) & " AND " & _
                     "a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
		'selContatoID
		Else
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " & _
                     "a.Fantasia AS fldName, " & _
                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " & _
                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " & _
                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " & _
                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " & _
                     "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), RelacoesPessoas_Contatos c WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND b.EstadoID=2 AND a.PessoaID=c.ContatoID AND b.SujeitoID = " & CStr(nRegExcluidoID) & " AND " & _
                     "b.ObjetoID = " & Cstr(nEmpresaID) & " AND c.RelacaoID=b.RelacaoID " & _
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
		End If

    'QAF ou Competencias
    ElseIf ((CInt(nFormID) = 7180) OR (CInt(nFormID) = 12210))Then

		'selFornecedorID
		If (nRegExcluidoID = "0") Then
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " & _
                     "a.Fantasia AS fldName, " & _
                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " & _
                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " & _
                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " & _
                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " & _
                     "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND b.EstadoID=2 AND a.PessoaID=b.ObjetoID AND b.SujeitoID = " & Cstr(nEmpresaID) & " AND " & _
                     "a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
		'Prest de Servico
		ElseIf (nRegExcluidoID = "-1") Then
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " & _
                     "a.Fantasia AS fldName, " & _
                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " & _
                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " & _
                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " & _
                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " & _
                     "FROM Pessoas a WITH(NOLOCK) " & _
                        "INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON a.ClassificacaoID = b.ItemID " & _
                     "WHERE a.EstadoID=2 AND a.TipoPessoaID IN (51,52) AND b.Filtro LIKE '%<PS>%' AND " & _
                     "a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
		'selContatoID
		Else
            strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, " & _
                     "a.Fantasia AS fldName, " & _
                     "(SELECT TOP 1 DDI FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDI, " & _
                     "(SELECT TOP 1 DDD FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS DDD, " & _
                     "(SELECT TOP 1 Numero FROM Pessoas_Telefones WITH(NOLOCK) WHERE (a.PessoaID = PessoaID AND TipoTelefoneID IN (119,121)) ORDER BY TipoTelefoneID, Ordem) AS Numero, " & _
                     "dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS EMail " & _
                     "FROM Pessoas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), RelacoesPessoas_Contatos c WITH(NOLOCK) " & _
                     "WHERE a.EstadoID=2 AND b.EstadoID=2 AND a.PessoaID=c.ContatoID AND b.ObjetoID = " & CStr(nRegExcluidoID) & " AND " & _
                     "b.SujeitoID = " & Cstr(nEmpresaID) & " AND c.RelacaoID=b.RelacaoID " & _
                     "AND a.Nome " & sOperator & " " & strToFind & " " & _
                     "ORDER BY fldCompleteName"
		End If

	' Valores a localizar
    ElseIf CInt(nFormID) = 9130 Then

		If (nKey = 1) Then
			sKey = "a.PessoaID"

			If (strToFind = "''") Then
				strToFind = "0"
			End If

		ElseIf (nKey = 2) Then
			sKey = "a.Nome"
		ElseIf (nKey = 3) Then
			sKey = "a.Fantasia"
		End If

		strSQL = "SELECT DISTINCT TOP 100 a.Nome AS fldCompleteName, a.PessoaID AS fldID, a.Fantasia AS fldName, " & _ 
		         "dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 111, 101, 0) AS Documento, f.Localidade " & _ 
		         "AS Cidade, g.CodigoLocalidade2 AS UF, h.CodigoLocalidade2 AS Pais " & _ 
		         "FROM Pessoas a WITH(NOLOCK) " & _
		         "LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (a.PessoaID = e.PessoaID AND (e.Ordem=1)) " & _
		         "LEFT OUTER JOIN Localidades f WITH(NOLOCK) ON e.CidadeID = f.LocalidadeID " & _
		         "LEFT OUTER JOIN Localidades g WITH(NOLOCK) ON e.UFID = g.LocalidadeID " & _
		         "LEFT OUTER JOIN Localidades h WITH(NOLOCK) ON e.PaisID = h.LocalidadeID " & _
		         "WHERE a.EstadoID = 2 AND " & sKey & " " & sOperator & " " & strToFind & " " & _
                 "ORDER BY " & sKey

    End If 

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    rsData.Save Response, adPersistXML

    rsData.Close
    Set rsData = Nothing
%>