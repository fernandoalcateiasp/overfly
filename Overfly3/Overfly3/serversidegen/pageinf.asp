
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'Funcao de uso geral, retorna o tipo de dado de um campo de tabela
Function returnFldType(ByVal tableName, ByVal fieldName)
        
    Dim retVal, rsTable, fldToExam
        
    retVal = 0

    'Transformar o fieldName de x.YYYY para YYYY
    'se for generalizar a funcao, retirar a linha
    'abaixo e coloca-la na chamada deste arquivo
    fieldName = Right(fieldName, (Len(fieldName)-2))
        
    Set rsTable = Server.CreateObject("ADODB.Recordset")

    With rsTable
        .Source = "SELECT TOP 1 * FROM " & tableName & " WITH(NOLOCK) "
        .ActiveConnection = strConn
        .CursorType = adOpenForwardOnly
        .LockType = adLockReadOnly
        .Open
    End With    
        
    For Each fldToExam In rsTable.Fields
        If (fldToExam.Name = fieldName) Then
            retVal = fldToExam.Type
            Exit For
        End If
    Next
        
    rsTable.Close
    Set rsTable = Nothing
        
    returnFldType = retVal

End Function
%>

<%
'Arquivo pageinf.asp de todos os forms
'Para cada form e necessario acrescentar um novo case onde assinalado
'por //@@

On Error Resume Next
Response.ContentType = "text/xml"

Dim rsData
Dim rsNew
Dim fldF
Dim strSQL
Dim strSQL2  
Dim strSQLExecAfterCreateTable
Dim nFormID
Dim sFldKey
Dim sLinkFieldName
Dim sLinkFieldValue
Dim nFolderID
Dim nOrder
Dim sTextToSearch
Dim sCondition
Dim i
Dim sOperator
Dim rsSPCommand
Dim nPageNumber
Dim nPageSize
Dim startID
Dim rsERROR
Dim errorOcurr
Dim sExpLink

Dim nFiltroID
Dim rsFiltro
Dim sFiltro
Dim vOptParam1
Dim vOptParam2
Dim aLinkField
Dim nContextoID
Dim nEmpresaID
Dim nEmpresaCidadeID
Dim A1
Dim A2

nFormID = ""  
sFldKey = ""
nOrder = ""
sTextToSearch = ""
sCondition = ""

vOptParam1 = ""
vOptParam2 = ""
nContextoID = 0
nEmpresaID = 0
nEmpresaCidadeID = 0
sFiltro = " "

For i = 1 To Request.QueryString("nFormID").Count    
    nFormID = Request.QueryString("nFormID")(i)
Next

For i = 1 To Request.QueryString("sFldKey").Count
    sFldKey = Request.QueryString("sFldKey")(i)
Next

'Transformar o fldKey de xYYYY para x.YYYY
sFldKey = Left(sFldKey, 1) & "." & Right(sFldKey, (Len(sFldKey)-1))

For i = 1 To Request.QueryString("sLinkFieldName").Count
    sLinkFieldName = Request.QueryString("sLinkFieldName")(i)
Next

For i = 1 To Request.QueryString("sLinkFieldValue").Count
    sLinkFieldValue = Request.QueryString("sLinkFieldValue")(i)
Next

For i = 1 To Request.QueryString("nContextoID").Count
    nContextoID = Request.QueryString("nContextoID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaCidadeID").Count
    nEmpresaCidadeID = Request.QueryString("nEmpresaCidadeID")(i)
Next

For i = 1 To Request.QueryString("nFolderID").Count    
    nFolderID = Request.QueryString("nFolderID")(i)
Next

For i = 1 To Request.QueryString("A1").Count    
    A1 = Request.QueryString("A1")(i)
Next

For i = 1 To Request.QueryString("A2").Count    
    A2 = Request.QueryString("A2")(i)
Next

A1 = CLng(A1)
A2 = CLng(A2)

' Caso o grid tenha mais de um campo de link com o sup
aLinkField = split(sLinkFieldName,",")
If (UBound(aLinkField, 1) > 0) Then
    sExpLink = " ("
    For i=0 To UBound(aLinkField, 1)
        sExpLink = sExpLink & " " & "a." & aLinkField(i) & " = " & sLinkFieldValue
        If (i=UBound(aLinkField, 1)) Then
            Exit For
        End If    
        
        sExpLink = sExpLink & " OR "

    Next
    sExpLink = sExpLink & " ) AND "
Else
    sExpLink = " " & "a." & sLinkFieldName & " = " & sLinkFieldValue & " AND "
End IF

For i = 1 To Request.QueryString("nOrder").Count    
    nOrder = Request.QueryString("nOrder")(i)
Next

For i = 1 To Request.QueryString("sTextToSearch").Count    
    sTextToSearch = "'" & Request.QueryString("sTextToSearch")(i) & "'"
Next

If (sTextToSearch = "''") Then
    sTextToSearch = "'0'"
End If

For i = 1 To Request.QueryString("sCondition").Count    
    sCondition = Request.QueryString("sCondition")(i)
Next

For i = 1 To Request.QueryString("nPageNumber").Count    
    nPageNumber = Request.QueryString("nPageNumber")(i)
Next

For i = 1 To Request.QueryString("nPageSize").Count    
    nPageSize = Request.QueryString("nPageSize")(i)
Next

For i = 1 To Request.QueryString("nFiltroID").Count    
    nFiltroID = Request.QueryString("nFiltroID")(i)
Next

For i = 1 To Request.QueryString("sFiltro").Count
    sFiltro = Request.QueryString("sFiltro")(i)
Next

If sCondition <> "" Then
    sCondition = "AND (" & sCondition & ")"
End If

startID = (nPageNumber * nPageSize) - nPageSize

Set rsData = Server.CreateObject("ADODB.Recordset")
    
If nOrder = "DESC" And sTextToSearch <> "'0'" Then
    sOperator = " <= "
Else
    sOperator = " >= "
End If

For i = 1 To Request.QueryString("vOptParam1").Count
    vOptParam1 = Request.QueryString("vOptParam1")(i)
Next

For i = 1 To Request.QueryString("vOptParam2").Count
    vOptParam2 = Request.QueryString("vOptParam2")(i)
Next

'--------------------------- Filtro (COMBO 2 INFERIOR) -- INICIO ----------------------
Set rsFiltro = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT Filtro From Recursos WHERE RecursoID = " & nFiltroID

rsFiltro.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

if (NOT rsFiltro.EOF) Then
    If ((NOT IsNull(rsFiltro.Fields("Filtro").Value)) AND (rsFiltro.Fields("Filtro").Value <> "")) Then
        sFiltro = sFiltro & " AND (" & rsFiltro.Fields("Filtro").Value & ") "
    End If    
End If
'--------------------------- Filtro (COMBO 2 INFERIOR) -- FIM ------------------------

'--------------------------- As strings de pesquisa -- INICIO ------------------------

strSQLExecAfterCreateTable = ""

'//@@
Select Case CLng(nFormID)

    Case 1110   'formID /basico/Recursos formID
        'a.RelacaoID-->sFldKey
        If (nFolderID = 20011) Then ' Recursos Filhos

            'No caso de Recursos Filhos, dependendo do tipo de registro inverte as tabelas
            If ( (vOptParam1 = 1) OR (vOptParam1 = 2) ) Then
                If (sFldKey = "b.RecursoID") Then
                    sFldKey = "a.RecursoID"
                ElseIf (sFldKey = "b.RecursoFantasia") Then
                    sFldKey = "a.RecursoFantasia"
                End If    
                'SELECT a.RecursoID,b.RecursoAbreviado,a.RecursoFantasia,c.ItemMasculino " & _
                'FROM Recursos a, Recursos b, TiposAuxiliares_Itens c ' +
                'WHERE ' + sFiltro + 'a.EstadoID = b.RecursoID AND a.TipoRecursoID = c.ItemID AND ' +
                'a.RecursoMaeID = '+idToFind+ ' ' +
                'ORDER BY a.RecursoFantasia';
                
                strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RecursoID " & _
                         "FROM Recursos a WHERE "  & _
                         sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                         sCondition & sFiltro

                If ( sFldKey = "a.RecursoID" ) Then
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
                Else
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RecursoID"
                End If

                strSQL2 ="SELECT TOP " & nPageSize & " a.RecursoID,b.RecursoAbreviado,a.RecursoFantasia,c.ItemMasculino " & _
                         "FROM Recursos a WITH (NOLOCK), Recursos b WITH (NOLOCK), TiposAuxiliares_Itens c WITH (NOLOCK), @temptable z " & _
                         "WHERE a.EstadoID = b.RecursoID AND a.TipoRecursoID = c.ItemID AND "  & _
                         sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                         sCondition & sFiltro & " AND z.IDFromPesq=a.RecursoID " & _
                         "AND z.IDTMP > " & startID & _
                         " ORDER BY z.IDTMP"
            ElseIf (vOptParam1 = 3) Then
                'SELECT b.RecursoID,c.RecursoAbreviado,b.RecursoFantasia,d.ItemMasculino ' +
                'FROM RelacoesRecursos a, Recursos b, Recursos c, TiposAuxiliares_Itens d ' +
                'WHERE ' + sFiltro + 'b.EstadoID = c.RecursoID AND b.TipoRecursoID = d.ItemID AND ' +
                'a.TipoRelacaoID = 1 AND a.SujeitoID=b.RecursoID AND ' +
                'a.ObjetoID = '+idToFind + ' ' +
                'ORDER BY b.RecursoFantasia';
                If (sFldKey = "a.RecursoID") Then
                    sFldKey = "b.RecursoID"
                ElseIf (sFldKey = "a.RecursoFantasia") Then
                    sFldKey = "b.RecursoFantasia"
                End If    

                strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " b.RecursoID " & _
                         "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK) " & _
                         "WHERE a.SujeitoID=b.RecursoID AND "  & _
                         sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                         sCondition & sFiltro

                If ( sFldKey = "b.RecursoID" ) Then
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
                Else
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,b.RecursoID"
                End If

                strSQL2 ="SELECT TOP " & nPageSize & " b.RecursoID,c.RecursoAbreviado,b.RecursoFantasia,d.ItemMasculino " & _
                         "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK), Recursos c WITH (NOLOCK), TiposAuxiliares_Itens d WITH (NOLOCK), @temptable z " & _
                         "WHERE b.EstadoID = c.RecursoID AND b.TipoRecursoID = d.ItemID AND a.TipoRelacaoID = 1 AND " & _
                         "a.SujeitoID=b.RecursoID AND "  & _
                         sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                         sCondition & sFiltro & " AND z.IDFromPesq=b.RecursoID " & _
                         "AND z.IDTMP > " & startID & _
                         " ORDER BY z.IDTMP"

            Elseif (vOptParam1 = 4) Then
                If (sFldKey = "a.RecursoID") Then
                    sFldKey = "b.RecursoID"
                ElseIf (sFldKey = "a.RecursoFantasia") Then
                    sFldKey = "b.RecursoFantasia"
                End If    
                'SELECT b.Recurso,c.RecursoAbreviado,b.RecursoFantasia,d.ItemMasculino ' +
                'FROM RelacoesRecursos a, Recursos b, Recursos c, TiposAuxiliares_Itens d '+
                'WHERE b.EstadoID = c.RecursoID AND b.TipoRecursoID = d.ItemID AND ' +
                'a.TipoRelacaoID = 2 AND a.SujeitoID=b.RecursoID AND ' +
                'a.ObjetoID = '+idToFind + ' ' +
                'ORDER BY b.RecursoFantasia';

                strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " b.RecursoID " & _
                         "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK) " & _
                         "WHERE a.SujeitoID=b.RecursoID AND "  & _
                         sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                         sCondition & sFiltro

                If ( sFldKey = "b.RecursoID" ) Then
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
                Else
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,b.RecursoID"
                End If

                strSQL2 ="SELECT TOP " & nPageSize & " b.RecursoID,c.RecursoAbreviado,b.RecursoFantasia,d.ItemMasculino " & _
                         "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK), Recursos c WITH (NOLOCK), TiposAuxiliares_Itens d WITH (NOLOCK), @temptable z " & _
                         "WHERE b.EstadoID = c.RecursoID AND b.TipoRecursoID = d.ItemID AND a.TipoRelacaoID = 2 AND " & _
                         "a.SujeitoID=b.RecursoID AND "  & _
                         sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                         sCondition & sFiltro & " AND z.IDFromPesq=b.RecursoID " & _
                         "AND z.IDTMP > " & startID & _
                         " ORDER BY z.IDTMP"
            End If
            
        ElseIf (nFolderID = 20016) Then ' Rel entre Recursos
            
            'SELECT b.RecursoFantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID ' +
            'FROM RelacoesRecursos a, Recursos b ' +
            'WHERE a.SujeitoID = b.RecursoID AND a.ObjetoID = ' + idToFind + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
            'AND b.RecursoFantasia >= ' + '\'' + glb_strPesquisa + '\'' + ' ' + 
            'ORDER BY b.RecursoFantasia'

            'SELECT b.RecursoFantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID ' +
            'FROM RelacoesRecursos a, Recursos b ' +
            'WHERE a.ObjetoID = b.RecursoID AND a.SujeitoID = ' + idToFind + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
            'AND b.RecursoFantasia >= ' + '\'' + glb_strPesquisa + '\'' + ' ' + 
            'ORDER BY b.RecursoFantasia'

            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                     "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK) "
            If (vOptParam1 = "SUJEITO") Then
                strSQL = strSQL & "WHERE a.SujeitoID = b.RecursoID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            ElseIf (vOptParam1 = "OBJETO") Then
                strSQL = strSQL & "WHERE a.ObjetoID = b.RecursoID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            End If

            strSQL = strSQL & sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.RelacaoID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
            End If    

            strSQL2 ="SELECT TOP " & nPageSize & " b.RecursoFantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " & _
                     "FROM RelacoesRecursos a WITH (NOLOCK), Recursos b WITH (NOLOCK), @temptable z "

            If (vOptParam1 = "SUJEITO") Then
                strSQL2 = strSQL2 & "WHERE a.SujeitoID = b.RecursoID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            ElseIf (vOptParam1 = "OBJETO") Then
                strSQL2 = strSQL2 & "WHERE a.ObjetoID = b.RecursoID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            End If

            strSQL2 = strSQL2 & sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"
        ElseIf (nFolderID = 20017) Then ' Rel com Pessoas
                  'SELECT b.Fantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID ' +
                  'FROM RelacoesPesRec a, Pessoas b ' +
                  'WHERE a.SujeitoID = b.PessoaID AND a.ObjetoID = ' + idToFind + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
                  'AND b.Fantasia >= ' + '\'' + glb_strPesquisa + '\'' + ' ' + 
                  'ORDER BY b.Fantasia'
        
                strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                         "FROM RelacoesPesRec a WITH (NOLOCK), Pessoas b WITH (NOLOCK) " & _
                         "WHERE a.SujeitoID = b.PessoaID AND a.TipoRelacaoID = " & vOptParam1 & " AND " & _
                         sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                         sCondition & sFiltro

                If ( sFldKey = "a.RelacaoID" ) Then
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
                Else
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
                End If    

                strSQL2 ="SELECT TOP " & nPageSize & " b.Fantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " & _
                         "FROM RelacoesPesRec a WITH (NOLOCK), Pessoas b WITH (NOLOCK), @temptable z " & _
                         "WHERE a.SujeitoID = b.PessoaID AND a.TipoRelacaoID = " & vOptParam1 & " AND " & _
                         sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                         sCondition & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
                         "AND z.IDTMP > " & startID & _
                         " ORDER BY z.IDTMP"
                         
        End If

    Case 1120   'formID  /basico/relacoes/relentrerecs


    Case 1330   'formID  /basico/Localidades

        If (nFolderID = 20246) Then ' Localidades Filhas
            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.LocalidadeID " & _
                     "FROM Localidades a WITH (NOLOCK) WHERE "  & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.LocalidadeID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.LocalidadeID"
            End If    
                         
            strSQL2 ="SELECT TOP " & nPageSize & " a.LocalidadeID,b.RecursoAbreviado,a.Localidade,c.ItemMasculino " & _
                     "FROM Localidades a WITH (NOLOCK), Recursos b WITH (NOLOCK), TiposAuxiliares_Itens c WITH (NOLOCK), @temptable z " & _
                     "WHERE a.EstadoID = b.RecursoID AND a.TipoLocalidadeID = c.ItemID AND "  & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDFromPesq=a.LocalidadeID " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"

        End If

    Case 1210   'formID  /basico/Pessoas

        If (nFolderID = 20120) Then ' Pedidos
            ' se o tipo do campo de pesquisa for data
            If( (returnFldType("Pedidos_Itens", sFldKey) = 135) AND _
                        ( sTextToSearch = "'0'") ) Then
                        sTextToSearch = "0"
            End If            
        
			If (sFldKey = "a.dtMovimento") Then
				sFldKey = " ISNULL((SELECT TOP 1 dtMovimento FROM Pedidos_Itens WITH (NOLOCK) WHERE (PedidoID = a.PedidoID)), GETDATE()) "
			End If
			
            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.PedidoID " & _
                     "FROM Pedidos a WITH (NOLOCK), Recursos b WITH (NOLOCK), Conceitos d WITH (NOLOCK), Pessoas e WITH (NOLOCK), Pessoas f WITH (NOLOCK), Operacoes g WITH (NOLOCK) " & _
                     "WHERE a.EstadoID=b.RecursoID AND a.MoedaID=d.ConceitoID AND " & _
                     "a.EmpresaID=e.PessoaID AND a.ProprietarioID=f.PessoaID AND a.TransacaoID = g.OperacaoID AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltro

            If (adtMovimento = "a.dtMovimento") Then
				strSQL = strSQL & " ORDER BY ISNULL((SELECT TOP 1 dtMovimento FROM Pedidos_Itens WITH (NOLOCK) WHERE (PedidoID = a.PedidoID)), GETDATE()) DESC,a.PedidoID"
            ElseIf ( sFldKey = "a.PedidoID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.PedidoID"
            End If

            strSQL2 ="SELECT TOP " & nPageSize & " e.Fantasia, a.PedidoID, b.RecursoAbreviado, " & _
                  "(SELECT TOP 1 dtMovimento FROM Pedidos_Itens WITH(NOLOCK) WHERE (PedidoID = a.PedidoID)) AS dtPedido, d.SimboloMoeda, " & _
                  "a.ValorTotalPedido AS TotalPedido, " & _
                  "i.Operacao AS Codigo, f.Fantasia AS Vendedor, g.Fantasia AS Pessoa, h.NotaFiscal, h.dtNotaFiscal, a.Observacao " & _
                  "FROM Pedidos a WITH (NOLOCK) " & _
                    "LEFT OUTER JOIN NotasFiscais h WITH (NOLOCK) ON (a.NotaFiscalID=h.NotaFiscalID AND h.EstadoID=67) " & _
                    "INNER JOIN Recursos b WITH (NOLOCK) ON (a.EstadoID=b.RecursoID) " & _
                    "INNER JOIN Conceitos d WITH (NOLOCK) ON (a.MoedaID=d.ConceitoID) " & _
                    "INNER JOIN Pessoas e WITH (NOLOCK) ON (a.EmpresaID=e.PessoaID) " & _
                    "INNER JOIN Pessoas f WITH (NOLOCK) ON (a.ProprietarioID=f.PessoaID) " & _
                    "INNER JOIN Pessoas g WITH (NOLOCK) ON (a.PessoaID=g.PessoaID) " & _
                    "INNER JOIN Operacoes i WITH (NOLOCK) ON (a.TransacaoID = i.OperacaoID) " & _
                    "INNER JOIN @temptable z ON (z.IDFromPesq = a.PedidoID) " & _
                  "WHERE " & _
                  sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                  sCondition & sFiltro & "  " & _
                  "AND z.IDTMP > " & startID & _
                  " ORDER BY z.IDTMP"
        
        ElseIf (nFolderID = 20121) Then ' Produtos
            ' se o tipo do campo de pesquisa for data
            If( (returnFldType("Pedidos_Itens", sFldKey) = 135) AND _
                        ( sTextToSearch = "'0'") ) Then
                        sTextToSearch = "0"
            End If            
        
            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " c.PedItemID " & _
                     "FROM Pedidos a WITH (NOLOCK), Pessoas b WITH (NOLOCK), Pedidos_Itens c WITH (NOLOCK), Conceitos d WITH (NOLOCK), Pessoas e WITH (NOLOCK), Conceitos g WITH (NOLOCK), Operacoes h WITH (NOLOCK) " & _
                     "WHERE a.EmpresaID=b.PessoaID AND a.PedidoID=c.PedidoID AND c.ProdutoID=d.ConceitoID " & _
                     "AND a.ProprietarioID=e.PessoaID AND a.MoedaID=g.ConceitoID AND a.TransacaoID = h.OperacaoID AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltro

            If ( sFldKey = "c.PedItemID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,c.PedItemID"
            End If

            strSQL2 ="SELECT TOP " & nPageSize & " c.dtMovimento, a.PedidoID, h.RecursoAbreviado AS Estado, " & _
                "i.Fantasia AS Pessoa, j.Operacao AS Codigo, d.ConceitoID, d.Conceito, c.Quantidade, g.SimboloMoeda, " & _
                "c.ValorUnitario AS Valor, (c.ValorUnitario*c.Quantidade) AS ValorTotal, e.Fantasia AS Vendedor, " & _ 
                "c.PedidoReferencia, b.Fantasia AS Empresa, a.Observacao " & _
                "FROM Pedidos a WITH (NOLOCK), Pessoas b WITH (NOLOCK), Pedidos_Itens c WITH (NOLOCK), Conceitos d WITH (NOLOCK), Pessoas e WITH (NOLOCK), Conceitos g WITH (NOLOCK), Recursos h WITH (NOLOCK), " & _
                "Pessoas i WITH (NOLOCK), Operacoes j WITH (NOLOCK), @temptable z " & _
                "WHERE a.EmpresaID=b.PessoaID AND a.PedidoID=c.PedidoID AND c.ProdutoID=d.ConceitoID " & _
                "AND a.ProprietarioID=e.PessoaID AND a.MoedaID=g.ConceitoID AND a.EstadoID=h.RecursoID AND " & _
                "a.PessoaID=i.PessoaID AND a.TransacaoID = j.OperacaoID AND " & _
                sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                sCondition & sFiltro & " AND z.IDFromPesq=c.PedItemID " & _
                "AND z.IDTMP > " & startID & _
                " ORDER BY z.IDTMP"
        
        ElseIf (nFolderID = 20122) Then ' Financeiros
            ' se o tipo do campo de pesquisa for data
            If( (returnFldType("Financeiro", sFldKey) = 135) AND _
                        ( sTextToSearch = "'0'") ) Then
                        sTextToSearch = "0"
            End If

			If ((A1=0)OR(A2=0)) Then
				sFiltro = sFiltro & " AND a.PedidoID IS NOT NULL "
			End If		

            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.FinanceiroID " & _
                     "FROM Financeiro a WITH (NOLOCK), Recursos c WITH (NOLOCK), TiposAuxiliares_Itens d WITH (NOLOCK), Pessoas e WITH (NOLOCK), " & _
                     "TiposAuxiliares_Itens g WITH (NOLOCK), Conceitos h WITH (NOLOCK), Pessoas i WITH (NOLOCK) " & _
                     "WHERE a.EstadoID=c.RecursoID AND a.TipoFinanceiroID=d.ItemID AND a.PessoaID=e.PessoaID AND " & _
                     "a.FormaPagamentoID=g.ItemID AND a.MoedaID=h.ConceitoID AND " & _
                     "a.EmpresaID=i.PessoaID AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltro

            If ( sFldKey = "a.FinanceiroID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.FinanceiroID"
            End If

            strSQL2 ="SELECT TOP " & nPageSize & " a.dtVencimento, dbo.fn_Financeiro_Totais(a.FinanceiroID, GETDATE(), 40) AS Atraso, " & _
				"a.FinanceiroID,c.RecursoAbreviado AS Estado,d.ItemMasculino AS TipoFinanceiro, " & _
                "a.PedidoID AS Pedido, j.Operacao AS Codigo, e.Fantasia AS Pessoa, a.Duplicata, a.dtEmissao, " & _
                "g.ItemAbreviado AS FormaPagamento, a.PrazoPagamento, h.SimboloMoeda AS Moeda, a.Valor, " & _
                "dbo.fn_Financeiro_Posicao(a.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL) AS SaldoDevedor, " & _
                "dbo.fn_Financeiro_Posicao(a.FinanceiroID, NULL, NULL, GETDATE(), NULL, NULL) AS SaldoAtualizado, i.Fantasia AS Empresa, a.Observacao " & _
                "FROM Financeiro a WITH (NOLOCK) LEFT OUTER JOIN Pedidos b WITH (NOLOCK) ON (a.PedidoID = b.PedidoID) " & _
                "LEFT OUTER JOIN Operacoes j WITH (NOLOCK) ON (b.TransacaoID = j.OperacaoID), Recursos c WITH (NOLOCK), TiposAuxiliares_Itens d WITH (NOLOCK), Pessoas e WITH (NOLOCK), " & _
                "TiposAuxiliares_Itens g WITH (NOLOCK), Conceitos h WITH (NOLOCK), Pessoas i WITH (NOLOCK), @temptable z " & _
                "WHERE a.EstadoID=c.RecursoID AND a.TipoFinanceiroID=d.ItemID AND a.PessoaID=e.PessoaID AND " & _
                "a.FormaPagamentoID=g.ItemID AND a.MoedaID=h.ConceitoID AND " & _
                "a.EmpresaID=i.PessoaID AND " & _
                sExpLink & sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltro & _
                " AND z.IDFromPesq=a.FinanceiroID " & "AND z.IDTMP > " & startID & _
                " ORDER BY z.IDTMP"
        
        ElseIf (nFolderID = 20123) Then ' Rel entre pessoas

            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                     "FROM RelacoesPessoas a WITH (NOLOCK), Pessoas b WITH (NOLOCK) "
            If (vOptParam1 = "SUJEITO") Then
                strSQL = strSQL & "WHERE a.SujeitoID = b.PessoaID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            ElseIf (vOptParam1 = "OBJETO") Then
                strSQL = strSQL & "WHERE a.ObjetoID = b.PessoaID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            End If

            strSQL = strSQL & sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.RelacaoID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
            End If    

            strSQL2 ="SELECT TOP " & nPageSize & " b.Fantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID, " & _
                     "a.Observacao, a.ProprietarioID, a.AlternativoID " & _
                     "FROM RelacoesPessoas a WITH (NOLOCK) "

            If (vOptParam1 = "SUJEITO") Then
                strSQL2 = strSQL2 & "INNER JOIN Pessoas b WITH(NOLOCK) ON a.SujeitoID = b.PessoaID "
            ElseIf (vOptParam1 = "OBJETO") Then
                strSQL2 = strSQL2 & "INNER JOIN Pessoas b WITH(NOLOCK) ON a.ObjetoID = b.PessoaID "
            End If            
            
            strSQL2 = strSQL2 & "INNER JOIN @temptable z ON z.IDFromPesq=a.RelacaoID "
            
            strSQL2 = strSQL2 & "WHERE a.TipoRelacaoID = " & vOptParam2 & " AND "

            strSQL2 = strSQL2 & sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"
                     
        ElseIf (nFolderID = 20124) Then ' Rel com recursos
        
            'SELECT b.RecursoFantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID ' +
            'FROM RelacoesPesRec a, Recursos b ' +
            'WHERE a.ObjetoID = b.RecursoID AND a.SujeitoID = ' + idToFind + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
            'AND b.RecursoFantasia >= ' + '\'' + glb_strPesquisa + '\'' + ' ' + 
            'ORDER BY b.RecursoFantasia'

            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                     "FROM RelacoesPesRec a WITH (NOLOCK), Recursos b WITH (NOLOCK) " & _
                     "WHERE a.ObjetoID = b.RecursoID AND a.TipoRelacaoID = " & vOptParam1 & " AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.RelacaoID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
            End If    

            strSQL2 ="SELECT TOP " & nPageSize & " b.RecursoFantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " & _
                     "FROM RelacoesPesRec a WITH (NOLOCK), Recursos b WITH (NOLOCK), @temptable z " & _
                     "WHERE a.ObjetoID = b.RecursoID AND a.TipoRelacaoID = " & vOptParam1 & " AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"
                     
        ElseIf (nFolderID = 20125) Then ' Rel com Conceitos
        
            'SELECT b.Conceito, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID ' +
            'FROM RelacoesPesCon a, Conceitos b ' +
            'WHERE a.ObjetoID=b.ConceitoID AND a.SujeitoID = ' + idToFind + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
            'AND b.Conceito >= ' + '\'' + glb_strPesquisa + '\'' + ' ' + 
            'ORDER BY b.Conceito'

            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                     "FROM RelacoesPesCon a WITH (NOLOCK), Conceitos b WITH (NOLOCK) " & _
                     "WHERE a.ObjetoID=b.ConceitoID AND a.TipoRelacaoID = " & vOptParam1 & " AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.RelacaoID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
            End If    

            strSQL2 ="SELECT TOP " & nPageSize & " b.Conceito, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " & _
                     "FROM RelacoesPesCon a WITH (NOLOCK), Conceitos b WITH (NOLOCK), @temptable z " & _
                     "WHERE a.ObjetoID=b.ConceitoID AND a.TipoRelacaoID = " & vOptParam1 & " AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"
        End If
                  
    Case 1320   'formID  /basico/tabelasaux/tiposrel


    Case 2120   'formID  /basico/relacoes/relconceitos

        If (nFolderID = 21042) Then ' Cotacoes
        
            'SELECT a.* FROM RelacoesConceitos_Cotacoes a WHERE '+
            'sFiltro + 'a.RelacaoID = ' + idToFind + ' ORDER BY a.dtCotacao DESC';
            
            If( (returnFldType("RelacoesConceitos_Cotacoes", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
            End If
            
            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelConCotacaoID " & _
                     "FROM RelacoesConceitos_Cotacoes a WITH (NOLOCK) " & _
                     "WHERE " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.RelConCotacaoID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelConCotacaoID"
            End If    

            strSQL2 ="SELECT TOP " & nPageSize & " * " & _
                     "FROM RelacoesConceitos_Cotacoes a WITH (NOLOCK), @temptable z " & _
                     "WHERE " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDFromPesq=a.RelConCotacaoID " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"
        End If


    Case 1120   'formID  /basico/relacoes/relentrerecs


    Case 1220   'formID  /basico/relacoes/relpessoas


    Case 1130   'formID  /basico/relacoes/relpessoaserecursos

                     
    Case 1310   'formID  /basico/tabelasaux/tiposaux

        If (nFolderID = 20201) Then ' Itens
            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.ItemID " & _
                     "FROM TiposAuxiliares_Itens a WITH (NOLOCK) WHERE "  & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.ItemID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.ItemID"
            End If    
                         
            strSQL2 ="SELECT TOP " & nPageSize & " a.* " & _
                     "FROM TiposAuxiliares_Itens a WITH (NOLOCK), @temptable z WHERE "  & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDFromPesq=a.ItemID " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"

        End If

'    Case 1320   'formID  /basico/tabelasaux/tiposrel


    Case 2110   'formID  /pcm/conceitos
    
        If (nFolderID = 21015) Then ' Rel entre Conceitos
        
            'SELECT b.Conceito, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID ' +
            'FROM RelacoesConceitos a, Conceitos b ' +
            'WHERE a.SujeitoID = b.ConceitoID AND a.ObjetoID = ' + idToFind + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
            'ORDER BY b.Conceito'

            'SELECT b.Conceito, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID ' +
            'FROM RelacoesConceitos a, Conceitos b ' +
            'WHERE a.ObjetoID = b.ConceitoID AND a.SujeitoID = ' + idToFind + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
            'ORDER BY b.Conceito'
        
            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                     "FROM RelacoesConceitos a WITH (NOLOCK), Conceitos b WITH (NOLOCK) "
            If (vOptParam1 = "SUJEITO") Then
                strSQL = strSQL & "WHERE a.SujeitoID = b.ConceitoID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            ElseIf (vOptParam1 = "OBJETO") Then
                strSQL = strSQL & "WHERE a.ObjetoID = b.ConceitoID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            End If

            strSQL = strSQL & sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.RelacaoID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
            End If    

            strSQL2 ="SELECT TOP " & nPageSize & " b.Conceito, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " & _
                     "FROM RelacoesConceitos a WITH (NOLOCK), Conceitos b WITH (NOLOCK), @temptable z "

            If (vOptParam1 = "SUJEITO") Then
                strSQL2 = strSQL2 & "WHERE a.SujeitoID = b.ConceitoID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            ElseIf (vOptParam1 = "OBJETO") Then
                strSQL2 = strSQL2 & "WHERE a.ObjetoID = b.ConceitoID AND a.TipoRelacaoID = " & vOptParam2 & " AND "
            End If

            strSQL2 = strSQL2 & sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"

        ElseIf (nFolderID = 21016) Then ' Rel com Pessoas
        
            'SELECT b.Fantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID ' +
            'FROM RelacoesPesCon a, Pessoas b ' +
            'WHERE a.SujeitoID = b.PessoaID AND a.ObjetoID = ' + idToFind + ' AND a.TipoRelacaoID = ' + aValue[0] + ' ' +
            'ORDER BY b.Fantasia'

            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelacaoID " & _
                     "FROM RelacoesPesCon a WITH (NOLOCK), Pessoas b WITH (NOLOCK) " & _
                     "WHERE a.SujeitoID = b.PessoaID AND a.TipoRelacaoID = " & vOptParam1 & " AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.RelacaoID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelacaoID"
            End If    

            strSQL2 ="SELECT TOP " & nPageSize & " b.Fantasia, a.RelacaoID, a.EstadoID, a.SujeitoID, a.ObjetoID, a.TipoRelacaoID " & _
                     "FROM RelacoesPesCon a WITH (NOLOCK), Pessoas b WITH (NOLOCK), @temptable z " & _
                     "WHERE a.SujeitoID = b.PessoaID AND a.TipoRelacaoID = " & vOptParam1 & " AND " & _
                     sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " AND z.IDFromPesq=a.RelacaoID " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"

        End If

    Case 2130   'formID  /basico/relacoes/relpessoaseconceitos

            If (nFolderID = 21066) Then ' CustoMedio
            
                ' se o tipo do campo de pesquisa for data
                If( (returnFldType("RelacoesPesCon_Custos_Antiga", sFldKey) = 135) AND _
                            ( sTextToSearch = "'0'") ) Then
                            sTextToSearch = "0"
                End If
            
               strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.RelPesConCustoID AS IDFromPesq " & _
                    "FROM RelacoesPesCon_Custos_Antiga a WITH(NOLOCK) " & _
                        "INNER JOIN RelacoesPesCon h WITH(NOLOCK) ON (a.RelacaoID = h.RelacaoID) " & _
                    "WHERE " & sExpLink & sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltro

                If ( sFldKey = "a.RelPesConCustoID" ) Then
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
                Else
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.RelPesConCustoID " & nOrder
                End If    
            
                strSQL2 = "SELECT TOP " & nPageSize & " " & _
                        "dbo.fn_Data_Fuso(a.dtCusto, NULL, 15231) AS Data, e.ItemAbreviado AS Metodo, dbo.fn_RelPesConCusto_Encomenda(a.RelPesConCustoID) AS Encomenda, " & _
                        "a.QuantidadeEstoque AS QuantidadeEstoque, a.QuantidadePedido AS QuantidadePedido, A.QuantidadeTotal AS QuantidadeTotal, b.SimboloMoeda AS Moeda, a.CustoEstoque AS CustoEstoque, " & _
                        "a.CustoPedido AS CustoPedido, a.CustoMedio AS CustoMedio, a.DespesaEstoque AS DespesaEstoque, a.DespesaPedido AS DespesaPedido, a.DespesaMedia AS DespesaMedia, a.dtPagamentoEstoque AS dtPagamentoEstoque, " & _
						"a.dtPagamentoPedido AS dtPagamentoPedido, a.dtMediaPagamento AS dtMediaPagamento, c.SimboloMoeda AS Moeda1, a.CustoContabilEstoque AS CustoContabilEstoque, a.CustoContabilPedido AS CustoContabilPedido, " & _
						"a.CustoContabilMedio AS CustoContabilMedio, d.PedidoID AS PedidoID, d.CFOPID AS CFOPID, " & _
						"g.Fantasia AS Pessoa, a.ValorUnitario AS Unitario, a.TaxaMoeda AS Taxa, a.Observacao AS Observacao, a.RelPesConCustoID AS RelPesConCustoID " & _
                    "FROM RelacoesPesCon_Custos_Antiga a WITH(NOLOCK) " & _
                        "LEFT OUTER JOIN Conceitos b WITH(NOLOCK) ON (a.MoedaCustoID = b.ConceitoID) " & _
                        "LEFT OUTER JOIN Conceitos c WITH(NOLOCK) ON (a.MoedaPedidoID = c.ConceitoID) " & _
                        "INNER JOIN Pedidos_Itens d WITH(NOLOCK) ON (a.PedItemID = d.PedItemID) " & _
                        "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (a.TipoCustoID = e.ItemID) " & _
						"INNER JOIN Pedidos f WITH(NOLOCK) ON (d.PedidoID = f.PedidoID) " & _
						"INNER JOIN Pessoas g WITH(NOLOCK) ON (f.PessoaID = g.PessoaID) " & _
						"INNER JOIN @temptable z ON (z.IDFromPesq = a.RelPesConCustoID) " & _
                    "WHERE " & _
                        sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                        sCondition & sFiltro & " " & _
                        "AND z.IDTMP > " & startID & " " & _
                    "ORDER BY z.IDTMP"
            
            ElseIf (nFolderID = 21067) Then ' Pedidos
                
                Dim sDataIsNull, sAuxChar
                
                sDataIsNull = ""
                sAuxChar = ""
                
                ' se o tipo do campo de pesquisa for data
                If( (returnFldType("Pedidos_Itens", sFldKey) = 135) AND _
                            ( sTextToSearch = "'0'") ) Then
                            sTextToSearch = "0"
                            
                            sAuxChar = "("
                            sDataIsNull = " OR " & sFldKey & " IS NULL) "
                End If

                strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " b.PedItemID " & _
                    "FROM RelacoesPesCon a WITH(NOLOCK) " & _
                        "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.ProdutoID = a.ObjetoID) " & _
                            "INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " & _
                                "INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID) " & _
                                "INNER JOIN Conceitos e WITH(NOLOCK) ON (e.ConceitoID = c.MoedaID) " & _
                                "INNER JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID = c.PessoaID) " & _
                                "INNER JOIN Pessoas g WITH(NOLOCK) ON (g.PessoaID = c.ProprietarioID) " & _
                    "WHERE (c.EmpresaID = a.SujeitoID) AND " & _
                         sExpLink & sAuxChar & sFldKey & sOperator & sTextToSearch & sDataIsNull & _
                         sCondition & sFiltro

                If ( sFldKey = "b.PedItemID" ) Then
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
                Else
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,b.PedItemID"
                End If    
            
                strSQL2 = "SELECT TOP " & nPageSize & " " & _
                      "b.dtMovimento AS dtMovimento, c.PedidoID AS PedidoID, " & _
                      "d.RecursoAbreviado AS Estado, f.Fantasia AS Pessoa, b.CFOPID AS CFOPID, " & _
                      "b.Quantidade AS Quantidade, e.SimboloMoeda AS Moeda, b.ValorUnitario AS ValorUnitario, " & _
                      "CONVERT(NUMERIC(11,2), b.ValorContribuicaoAjustada * c.TaxaMoeda) AS Contribuicao, " & _
                      "ROUND((dbo.fn_DivideZero(b.ValorContribuicaoAjustada, b.ValorFaturamentoBase)*100), 2) AS Margem, " & _
                      "g.Fantasia AS Vendedor, " & _
                      "b.PedidoReferencia AS PedidoReferencia, c.Observacao AS Observacao " & _
                  "FROM RelacoesPesCon a WITH(NOLOCK) " & _
                      "INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.ProdutoID = a.ObjetoID) " & _
                          "INNER JOIN Pedidos c WITH(NOLOCK) ON (c.PedidoID = b.PedidoID) " & _
                              "INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID) " & _
                              "INNER JOIN Conceitos e WITH(NOLOCK) ON (e.ConceitoID = c.MoedaID) " & _
                              "INNER JOIN Pessoas f WITH(NOLOCK) ON (f.PessoaID = c.PessoaID) " & _
                              "INNER JOIN Pessoas g WITH(NOLOCK) ON (g.PessoaID = c.ProprietarioID) " & _
                          "INNER JOIN @temptable z ON (z.IDFromPesq = b.PedItemID) " & _
                  "WHERE (c.EmpresaID = a.SujeitoID) AND " & _
                      sExpLink & sAuxChar & sFldKey & sOperator & sTextToSearch & sDataIsNull & _
                      sCondition & sFiltro & " AND (z.IDTMP > " & startID & ") " & _
                  "ORDER BY z.IDTMP" 

            ElseIf (nFolderID = 21068) Then ' Movimentos
                
                ' se o tipo do campo de pesquisa for data
                If( (returnFldType("RelacoesPesCon_Movimentos", sFldKey) = 135) AND _
                            ( sTextToSearch = "'0'") ) Then
                            sTextToSearch = "0"
                End If
            
                strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " b.RelPesConMovimentoID " & _
                    "FROM RelacoesPesCon_Movimentos b WITH(NOLOCK) " & _
                        "INNER JOIN RelacoesPesCon a WITH(NOLOCK) ON ((a.ObjetoID = b.ProdutoID) AND (a.SujeitoID = b.EmpresaID)) " & _
                    "WHERE " & sExpLink & sFldKey & sOperator & sTextToSearch & " " & sCondition & sFiltro

                If ( sFldKey = "b.RelPesConMovimentoID" ) Then
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
                Else 
                   strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,b.RelPesConMovimentoID"
                End If    
            
                strSQL2 = "SELECT TOP " & nPageSize & " " & _
                        "dbo.fn_Data_Fuso(b.dtMovimento, NULL, " & CStr(nEmpresaCidadeID) & ") AS dtMovimento, b.PedidoID AS PedidoID, " & _
                        "b.AsstecID AS AsstecID, c.Operacao AS Transacao, e.ItemAbreviado AS Estoque, d.Fantasia AS Deposito, " & _
                        "b.Quantidade AS Quantidade " & _
                    "FROM RelacoesPesCon_Movimentos b WITH (NOLOCK) " & _
                        "INNER JOIN RelacoesPesCon a WITH (NOLOCK) ON ((a.ObjetoID = b.ProdutoID) AND (a.SujeitoID = b.EmpresaID)) " & _
                        "LEFT OUTER JOIN Operacoes c WITH (NOLOCK) ON (c.OperacaoID = b.TransacaoID) " & _
                        "LEFT OUTER JOIN Pessoas d WITH (NOLOCK) ON (d.PessoaID = b.DepositoID) " & _
                        "INNER JOIN TiposAuxiliares_Itens e WITH (NOLOCK) ON (e.ItemID = b.EstoqueID) " & _
                        "INNER JOIN @temptable z ON (z.IDFromPesq = b.RelPesConMovimentoID) " & _
                    "WHERE " & _
                        sExpLink & sFldKey & sOperator & sTextToSearch & " " & _
                        sCondition & sFiltro & " " & _
                        "AND z.IDTMP > " & startID & " " & _
                    "ORDER BY z.IDTMP"

            End If
            
    Case 5110   'formID  /modcomercial/subpedidos/frmpedidos

    Case 5220    'formID  /modcomercial/subatendimento/listaprecos

    Case 10110   'formID /modcontabil/submodcontabilidade/planocontas

        If (nFolderID = 29001) Then ' Lancamentos
        
            If( (returnFldType("Lancamentos", sFldKey) = 135) AND _
                ( sTextToSearch = "'0'") ) Then
                sTextToSearch = "0"
            End If
            
			Dim PessoasJoinOperator, ImpostosJoinOperator, sDetalhe

			sDetalhe = "NULL"

			'Pessoas
			If ( CLng(vOptParam1) >= 1532 AND CLng(vOptParam1) <= 1541 ) Then
				PessoasJoinOperator = " INNER JOIN "
				ImpostosJoinOperator = " LEFT OUTER JOIN "

				If (sFiltro <> "") Then
					sDetalhe = "e.PessoaID"
				End If
			'RelPesConta
			ElseIf (CLng(vOptParam1) = 1542) Then
				PessoasJoinOperator = " LEFT OUTER JOIN "
				ImpostosJoinOperator = " LEFT OUTER JOIN "

				If (sFiltro <> "") Then
					sDetalhe = "a.RelPesContaID"
				End If
			'Impostos
			ElseIf (CLng(vOptParam1) = 1543) Then
				PessoasJoinOperator = " LEFT OUTER JOIN "
				ImpostosJoinOperator = " INNER JOIN "

				If (sFiltro <> "") Then
					sDetalhe = "f.ConceitoID"
				End If
			Else
				PessoasJoinOperator = " LEFT OUTER JOIN "
				ImpostosJoinOperator = " LEFT OUTER JOIN "
			End If
            
            Dim bSohConciliacao, sSohConciliacao
            
            bSohConciliacao = 0
            sSohConciliacao = ""
            
            For i = 1 To Request.QueryString("bSohConciliacao").Count
				bSohConciliacao = Request.QueryString("bSohConciliacao")(i)
			Next
			
			If (CLng(bSohConciliacao) = 1) Then
				sSohConciliacao = " dbo.fn_Lancamento_Conciliado(a.LanContaID) = 1 AND "
			End If	

            strSQL = "INSERT INTO @temptable (IDFromPesq) SELECT TOP " & CStr((nPageSize * nPageNumber)) & " a.LanContaID " & _
                     "FROM Lancamentos_Contas a WITH (NOLOCK) " & _
                        "INNER JOIN Lancamentos b WITH (NOLOCK) ON (a.LancamentoID = b.LancamentoID) " & _
                        "INNER JOIN Recursos c WITH (NOLOCK) ON (b.EstadoID = c.RecursoID) " & _
                        "INNER JOIN Conceitos d WITH (NOLOCK) ON (b.MoedaID = d.ConceitoID) " & _
                        PessoasJoinOperator & " Pessoas e WITH (NOLOCK) ON (a.PessoaID = e.PessoaID) " & _
                        ImpostosJoinOperator & " Conceitos f WITH (NOLOCK) ON (a.ImpostoID = f.ConceitoID) " & _
                     "WHERE " & _
					 "dbo.fn_Conta_EhFilha(a.ContaID, " & CStr(sLinkFieldValue) & ", 1) = 1 AND " & _
					 "b.EmpresaID = " & CStr(nEmpresaID) & " AND "& _
                     "b.EstadoID IN (2, 81, 82) AND " & _
					 sSohConciliacao & _
                     sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro

            If ( sFldKey = "a.LanContaID" ) Then
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder
            Else
               strSQL = strSQL & " ORDER BY " & sFldKey & " " & nOrder & " ,a.LanContaID " & nOrder
            End If    

            ' "dbo.fn_Conta_Saldo(a.ContaID, b.EmpresaID, " & sDetalhe & ", NULL, NULL, a.LanContaID, NULL, NULL, NULL, NULL) AS Saldo," & _
            strSQL2 ="SELECT TOP " & nPageSize & " " & _
						 "a.LanContaID, b.dtLancamento, b.dtBalancete, b.LancamentoID, c.RecursoAbreviado AS Estado," & _ 
						 "dbo.fn_Lancamento_Contrapartida(a.LanContaID) AS Contrapartida, d.SimboloMoeda, a.Valor," & _ 
						 "0 AS Saldo," & _
						 "a.Documento, e.Fantasia, dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4) AS ContaBancaria," & _ 
						 "f.Imposto AS Imposto, dbo.fn_Lancamento_Historico(b.LancamentoID, 1) AS Historico " & _
                     "FROM Lancamentos_Contas a WITH (NOLOCK) " & _
                        "LEFT OUTER JOIN Pessoas e WITH (NOLOCK) ON (a.PessoaID = e.PessoaID) " & _
                        "LEFT OUTER JOIN Conceitos f WITH (NOLOCK) ON (a.ImpostoID = f.ConceitoID) " & _
                        "INNER JOIN Lancamentos b WITH (NOLOCK) ON (a.LancamentoID = b.LancamentoID) " & _
                        "INNER JOIN Recursos c WITH (NOLOCK) ON (b.EstadoID = c.RecursoID) " & _
                        "INNER JOIN Conceitos d WITH (NOLOCK) ON (b.MoedaID = d.ConceitoID) " & _
                        "INNER JOIN @temptable z ON (z.IDFromPesq=a.LanContaID) " & _
                     "WHERE b.EstadoID IN (2, 81, 82) AND " & _
                     sFldKey & sOperator & sTextToSearch & " " & _
                     sCondition & sFiltro & " " & _
                     "AND z.IDTMP > " & startID & _
                     " ORDER BY z.IDTMP"
        End If

    Case Else
        'Nao tem c�digo

End Select
  
'--------------------------- As strings de pesquisa -- FIM ---------------------------

Set rsSPCommand = Server.CreateObject("ADODB.Command")
  
With rsSPCommand
  .CommandTimeout = 60 * 10
  .ActiveConnection = strConn
  .CommandText = "sp_PesqList"
  .CommandType = adCmdStoredProc
  .Parameters.Append .CreateParameter("@iPageSize", adInteger, adParamInput)
  .Parameters("@iPageSize").Value = nPageSize
  .Parameters.Append .CreateParameter("@sSQL", adVarChar, adParamInput, Len(strSQL) , strSQL)
  .Parameters.Append .CreateParameter("@sSQL2", adVarChar, adParamInput,Len(strSQL2) , strSQL2)   
  'O parametro length abaixo eh o length da string + 1, porque se chegar 0, o sql gera uma excessao dizendo
  'que o parametro nao foi fornecido
  .Parameters.Append( .CreateParameter("@sExecAfterCreateTable", adVarChar, adParamInput,Len(strSQLExecAfterCreateTable) + 1, strSQLExecAfterCreateTable) )
  Set rsData = .Execute
End With

errorOcurr = False
For Each rsERROR In rsSPCommand.ActiveConnection.Errors
    errorOcurr = True
Next
  
Set rsSPCommand = Nothing  
  
Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer

Dim nCounter
nCounter = -1

For Each fldF In rsData.Fields
      nCounter = nCounter + 1
      If ( fldF.Type = adNumeric ) Then
        rsNew.Fields.Append fldF.Name, adDecimal, fldF.DefinedSize, adFldMayBeNull OR adFldUpdatable
        rsNew.Fields(nCounter).Precision = fldF.Precision
        rsNew.Fields(nCounter).NumericScale = fldF.NumericScale
      Else
        rsNew.Fields.Append fldF.Name, fldF.Type, fldF.DefinedSize, adFldMayBeNull OR adFldUpdatable
      End If
Next

rsNew.Fields.Append "fldError", adVarChar, 50, adFldUpdatable
  
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

If rsData.EOF AND errorOcurr Then
   rsNew.AddNew
   rsNew.Fields("fldError").Value = "Filtro inv�lido."
   rsNew.Update
   rsNew.Save Response, adPersistXML
   rsNew.Close
   rsData.Close
   rsFiltro.Close
   Set rsNew = Nothing
   Set rsData = Nothing
   Set rsFiltro = Nothing
   Response.End
End If
  
While Not rsData.EOF
  rsNew.AddNew
  For Each fldF In rsData.Fields

	If ( NOT (IsNull(fldF.Value)) ) Then
		rsNew.Fields(fldF.Name).Value = fldF.Value
	End If	
    
  Next
  rsNew.Update
  rsData.MoveNext
Wend
  
' send the new data back to the client
rsNew.Save Response, adPersistXML
  
rsNew.Close
rsData.Close
rsFiltro.Close
Set rsNew = Nothing
Set rsData = Nothing
Set rsFiltro = Nothing

%>


