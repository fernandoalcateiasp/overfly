
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%

    Dim rsNew,currDateFormat,i

    For i = 1 To Request.QueryString("currDateFormat").Count
        currDateFormat = Request.QueryString("currDateFormat")(i)
    Next

    Set rsNew = Server.CreateObject("ADODB.Recordset")
    rsNew.CursorLocation = adUseServer
    rsNew.Fields.Append "currDate", adVarchar, 10, adFldUpdatable

    rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

    rsNew.AddNew
    If (currDateFormat = "DD/MM/YYYY") Then
        rsNew.Fields("currDate").Value = Day(Date) & "/" & Month(Date)& "/" & Year(Date)
    ElseIf (currDateFormat = "MM/DD/YYYY") Then
        rsNew.Fields("currDate").Value = Month(Date) & "/" & Day(Date)& "/" & Year(Date)
    ElseIf (currDateFormat = "YYYY/MM/DD/") Then
        rsNew.Fields("currDate").Value = Year(Date) & "/" & Month(Date) & "/" & Day(Date)
    End If
    rsNew.Update
    rsNew.Save Response, adPersistXML
    rsNew.Close
    Set rsNew = Nothing
    Response.End
%>
