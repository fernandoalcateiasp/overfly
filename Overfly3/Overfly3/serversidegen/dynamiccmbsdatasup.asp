
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'Parametros fixos que vem do cliente    
Dim i, nFormID, nUserID, nEmpresaID

nFormID = 0
nUserID = 0
nEmpresaID = 0

'Parametros variaveis que vem do cliente
'podem conter strings ou numericos
Dim param1, param2, param3, param4, param5, param6, param7, param8, param9, param10

param1 = 0
param2 = 0
param3 = 0
param4 = 0
param5 = 0
param6 = 0
param7 = 0
param8 = 0
param9 = 0
param10 = 0

Dim rsData, strSQL, rsSPCommand

strSQL = ""
      
For i = 1 To Request.QueryString("nFormID").Count    
    nFormID = Request.QueryString("nFormID")(i)
Next      

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next      

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("param1").Count    
    param1 = Request.QueryString("param1")(i)
Next

For i = 1 To Request.QueryString("param2").Count    
    param2 = Request.QueryString("param2")(i)
Next

For i = 1 To Request.QueryString("param3").Count    
    param3 = Request.QueryString("param3")(i)
Next

For i = 1 To Request.QueryString("param4").Count    
    param4 = Request.QueryString("param4")(i)
Next

For i = 1 To Request.QueryString("param5").Count    
    param5 = Request.QueryString("param5")(i)
Next

For i = 1 To Request.QueryString("param6").Count    
    param6 = Request.QueryString("param6")(i)
Next

For i = 1 To Request.QueryString("param7").Count    
    param7 = Request.QueryString("param7")(i)
Next

For i = 1 To Request.QueryString("param8").Count    
    param8 = Request.QueryString("param8")(i)
Next

For i = 1 To Request.QueryString("param9").Count    
    param9 = Request.QueryString("param9")(i)
Next

For i = 1 To Request.QueryString("param10").Count    
    param10 = Request.QueryString("param10")(i)
Next

If (Not IsNumeric(param7)) Then
    param7 = 0
End If

'--------------------------- As strings de pesquisa -- INICIO ------------------------

'//@@
Select Case CLng(nFormID)

    Case 1110   'Form de Recursos
              
    Case 1120   'Form de Rela��es entre Recursos

    Case 1210   'Form de Pessoas
    
    Case 1130   'Form de Rela��es Pessoas e Recursos

    Case 1220   'Form de Relacoes Entre Pessoas
    
    Case 1320   'Form de Tipos de Rela��es
    
    Case 1330   'Form de Localidades

    Case 2110   'Form de Conceitos

    Case 2120   'Form de Rela��es entre Conceitos

    Case 2130   'Form de Rela��es entre Pessoas e Conceitos
    
    Case 5110   'Form de Pedidos

        ' parametrizacao do dso dsoCmbDynamic01 (designado para selPessoaID)
        strSQL = "SELECT 1 AS Indice, PessoaID AS fldID, Fantasia AS fldName, 0 AS Ordem, dbo.fn_Pessoa_Telefone(PessoaID, 119, 122,1,0,NULL) AS Telefone " & _
                 "FROM Pessoas WITH(NOLOCK) " & _
                 "WHERE PessoaID = " & CStr(param1)
    
        ' parametrizacao do dso dsoCmbDynamic02 (designado para selParceiroID)
        strSQL = strSQL & " " & _
                 "UNION ALL " & _
                 "SELECT 2 AS Indice, PessoaID as fldID,Fantasia as fldName, 0 AS Ordem, dbo.fn_Pessoa_Telefone(PessoaID, 119, 122,1,0,NULL) AS Telefone " & _
                 "FROM Pessoas WITH(NOLOCK) " & _
                 "WHERE PessoaID = " & CStr(param2)
    
        ' parametrizacao do dso dsoCmbDynamic03 (designado para selTransacaoID)
        strSQL = strSQL & " " & _
                 "UNION ALL " & _
                 "SELECT 3 AS Indice, Transacoes.OperacaoID AS fldID, CONVERT(VARCHAR(10), Transacoes.OperacaoID) AS fldName, 0 AS Ordem, SPACE(0) AS Telefone " & _
                 "FROM Operacoes Transacoes WITH(NOLOCK) " & _
                 "WHERE Transacoes.OperacaoID = " & CStr(param3)

        ' parametrizacao do dso dsoCmbDynamic04 (designado para selNotaFiscalID)
        strSQL = strSQL & " " & _
                 "UNION ALL " & _
                 "SELECT 4 AS Indice, NotaFiscalID AS fldID, LTRIM(RTRIM(STR(NotaFiscal))) AS fldName, 0 AS Ordem, SPACE(0) AS Telefone " & _
                 "FROM NotasFiscais WITH(NOLOCK) " & _
                 "WHERE NotaFiscalID = " & CStr(param4)

        ' parametrizacao do dso dsoCmbDynamic05 (designado para selTransportadoraID)
        strSQL = strSQL & " " & _
                 "UNION ALL " & _
                 "SELECT 5 AS Indice, a.PessoaID as fldID, a.Fantasia as fldName, 0 AS Ordem, dbo.fn_Pessoa_Telefone(a.PessoaID, 119, 122,1,0,NULL) AS Telefone " & _
                 "FROM Pessoas a WITH(NOLOCK) " & _
                 "WHERE a.PessoaID=" & CStr(param7) & " AND a.PessoaID<>" & CStr(param5) & " " & _
                 "UNION ALL " & _
                 "SELECT 5 AS Indice, " & CStr(param5) & " AS fldID, 'O mesmo' AS fldName, 1000 AS Ordem, dbo.fn_Pessoa_Telefone(" & CStr(param5) & ", 119, 122,1,0,NULL) AS Telefone "

        ' parametrizacao do dso dsoCmbDynamic07 (designado para selMeioTransporteID)
        strSQL = strSQL & " " & _
                "UNION ALL SELECT 6 AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem, SPACE(0) AS Telefone " & _
                "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                "WHERE ItemID =" & CStr(param8) & " "

        ' parametrizacao do dso dsoCmbDynamic08 (designado para selModalidadeTransporteID)
        strSQL = strSQL & " " & _
                "UNION ALL SELECT 7 AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem AS Ordem, SPACE(0) AS Telefone " & _
                "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                "WHERE ItemID =" & CStr(param9) & " "
                
        ' parametrizacao do dso dsoCmbDynamic09 (designado para selShipTo)
        strSQL = strSQL & " " & _
				"UNION ALL SELECT 8 AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem, SPACE(0) AS Telefone " & _
                "UNION ALL SELECT 8 AS Indice, PessoaID AS fldID, Fantasia AS fldName, 1 AS Ordem, SPACE(0) AS Telefone " & _
                "FROM Pessoas WITH(NOLOCK) " & _
                "WHERE PessoaID =" & CStr(param10) & " " & _
        "ORDER BY Indice, Ordem"
        
    Case 5140   'Form de Opera��es
    
    Case 5220   'Form de Lista de Preco
    
    Case 9110   'Form Financeiro
    
    Case 9130   'Form Valores a Localizar

    Case 9210   'Form Cobranca

    Case 9220   'Form Bancos

    Case Else
        'Nao tem c�digo

End Select
  
'--------------------------- As strings de pesquisa -- FIM ---------------------------

Set rsData = Server.CreateObject("ADODB.Recordset")
                          
'rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Set rsSPCommand = Server.CreateObject("ADODB.Command")

With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandText = "sp_RelatorioEstoque"
    .CommandType = adCmdStoredProc
    .Parameters.Append( .CreateParameter("@sSQL", adVarChar, adParamInput,Len(strSQL) , strSQL) )
    Set rsData = .Execute
End With

rsData.Save Response, adPersistXML

rsData.Close
Set rsData = Nothing

Set rsSPCommand = Nothing
%>