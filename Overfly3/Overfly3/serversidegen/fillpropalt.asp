
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	Dim i, sCaller, strToFind, nNumeroRegistros
	sCaller = ""
	strToFind = ""
	nNumeroRegistros = 0
	
	'"PROP" - proprietario
	'"ALT" - alternativo
	For i = 1 To Request.QueryString("sCaller").Count    
	  sCaller = Request.QueryString("sCaller")(i)
	Next    
	    
	For i = 1 To Request.QueryString("strToFind").Count    
	  strToFind = Request.QueryString("strToFind")(i)
	Next
  
	For i = 1 To Request.QueryString("nNumeroRegistros").Count    
	  nNumeroRegistros = Request.QueryString("nNumeroRegistros")(i)
	Next

    Dim rsData, strSQL

    Set rsData = Server.CreateObject("ADODB.Recordset")
      
    If (sCaller = "PROP") Then
	    strSQL = "SELECT TOP " & CStr(nNumeroRegistros) & " b.Nome, b.PessoaID, b.Fantasia FROM RelacoesPesRec a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " & _
	             "WHERE (a.EstadoID=2 AND a.TipoRelacaoID=11 AND a.ObjetoID=999 AND b.Nome >='" & strToFind & "' " & _
	             "AND a.SujeitoID=b.PessoaID AND b.TipoPessoaID=51 AND b.EstadoID=2) " & _
	             "ORDER BY b.Nome"
    Else
	    strSQL = "SELECT TOP " & CStr(nNumeroRegistros) & " Nome, PessoaID, Fantasia FROM Pessoas WITH(NOLOCK) " & _
	             "WHERE (EstadoID=2 AND TipoPessoaID IN (51,53) AND Nome >='" & strToFind & "') " & _
	             "ORDER BY Nome"
    End If

	rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	rsData.Save Response,adPersistXML
    rsData.Close
    Set rsData = Nothing
%>
