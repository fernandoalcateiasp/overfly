
<%@ LANGUAGE=VBSCRIPT @EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew
Dim sUsuario, sSenha, sPagina, sValidade, sHashCode, nParceiroID, nPessoaID
Dim rsCommand
Dim i
Dim nRecsAffected
Dim rsERROR
Dim sError
Dim rsData
Dim strSQL

sUsuario = ""
sSenha = ""
sPagina = ""
sValidade = ""
sHashCode = ""
nRecsAffected = 0
strSQL = ""
nParceiroID = 0
nPessoaID = 0

For i = 1 To Request.QueryString("sUsuario").Count    
    sUsuario = Request.QueryString("sUsuario")(i)
Next

For i = 1 To Request.QueryString("sSenha").Count    
    sSenha = Request.QueryString("sSenha")(i)
Next

For i = 1 To Request.QueryString("sPagina").Count    
    sPagina = Request.QueryString("sPagina")(i)
Next

For i = 1 To Request.QueryString("sValidade").Count    
    sValidade = Request.QueryString("sValidade")(i)
Next

For i = 1 To Request.QueryString("nParceiroID").Count    
    nParceiroID = Request.QueryString("nParceiroID")(i)
Next

For i = 1 To Request.QueryString("nPessoaID").Count    
    nPessoaID = Request.QueryString("nPessoaID")(i)
Next


If((IsNull(sSenha)) OR (sSenha = ""))Then

    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT TOP 1 Senha " & _
            "FROM Pessoas_Senhas WITH(NOLOCK) " & _
            "WHERE Tipo = 1 AND PessoaID = " & CStr(sUsuario) & " " & _
            "ORDER BY dtSenha DESC, PesSenhaID DESC" 

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    If (Not (rsData.BOF AND rsData.EOF) ) Then
        sSenha = rsData.Fields("Senha").Value
    End If

End If

rsData.Close
Set rsData = Nothing

Set rsCommand = Server.CreateObject("ADODB.Command")

With rsCommand
    .CommandTimeout = 60 * 10
	.ActiveConnection = strConn
	.CommandText = "sp_WebIkeda_ServerLink"
	.CommandType = adCmdStoredProc
	
    .Parameters.Append( .CreateParameter("@bLinka", adBoolean, adParamInput) )
	.Parameters("@bLinka").Value = True
	
	.Execute	

End With

For Each rsERROR In rsCommand.ActiveConnection.Errors
    sError = rsERROR.Description
    Exit For
Next

If ((sValidade = "") OR (IsNull(sValidade))) Then
	    sValidade = DateAdd("h",1,Now) 
End If	    

Set rsCommand = Nothing

If (sError = "") Then
    Set rsCommand = Server.CreateObject("ADODB.Command")

    With rsCommand
        .CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "IkedaDatabase.Ecommerce.dbo.sp_Target_Hash_Retorno"
	    .CommandType = adCmdStoredProc
    	
        .Parameters.Append( .CreateParameter("@Usuario", adVarChar, adParamInput, 20) )
	    If (sUsuario = "") Then
	        .Parameters("@Usuario").Value = Null
	    Else
	        .Parameters("@Usuario").Value = sUsuario
	    End If
    	
	    .Parameters.Append( .CreateParameter("@Senha", adVarChar, adParamInput, 50) )
	    If (sSenha = "") Then
	        .Parameters("@Senha").Value = Null
	    Else
	        .Parameters("@Senha").Value = sSenha
	    End If
	    
	    .Parameters.Append( .CreateParameter("@RevendaCodigo", adInteger, adParamInput) )
	    If (nParceiroID = "") Then
	        .Parameters("@RevendaCodigo").Value = Null
	    Else
	        .Parameters("@RevendaCodigo").Value = CLng(nParceiroID)
	    End If
	    
	    .Parameters.Append( .CreateParameter("@ClienteCodigo", adInteger, adParamInput) )
	    If (nPessoaID = "") Then
	        .Parameters("@ClienteCodigo").Value = Null
	    Else
	        .Parameters("@ClienteCodigo").Value = CLng(nPessoaID)
	    End If
    	
	    .Parameters.Append( .CreateParameter("@Pagina", adVarChar, adParamInput, 200) )
	    If (sPagina = "") Then
	        .Parameters("@Pagina").Value = Null
	    Else
	        .Parameters("@Pagina").Value = sPagina
	    End If
	        	
	    .Parameters.Append( .CreateParameter("@Validade", adDate, adParamInput, 30) )
	    If ((sValidade = "") OR (IsNull(sValidade))) Then
	        .Parameters("@Validade").Value = Null
	    Else
	        .Parameters("@Validade").Value = CDate(sValidade)
	    End If
    		
	    .Parameters.Append( .CreateParameter("@PKID", adVarChar, adParamInputOutput, 4000) )
	    .Parameters("@PKID").Value = Null
    	
	    .Parameters.Append( .CreateParameter("@HashCode", adVarChar, adParamInputOutput, 4000) )
	    .Parameters("@HashCode").Value = Null

	    .Execute
    	
	    sHashCode = rsCommand.Parameters("@HashCode").Value

    End With

    For Each rsERROR In rsCommand.ActiveConnection.Errors
        sError = rsERROR.Description
        Exit For
    Next    

    Set rsCommand = Nothing
End If

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "sHashCode", adVarChar, 8000, adFldMayBeNull OR adFldUpdatable
rsNew.Fields.Append "Error", adVarChar, 8000, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew

If ( NOT IsNull(sHashCode) ) Then
	rsNew.Fields("sHashCode").Value = CStr(sHashCode)
End If

If (sError <> "") Then
	rsNew.Fields("Error").Value = sError
End If

rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
%>
