
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew
Dim nFonteDadoID, nTipoDadoID, nTipoRegistroID, nFormID, nSubFormID, nRegistroID, nDtConsulta, nUsuarioID, nValidacaoDadoID 
Dim rsCommand
Dim i

nFonteDadoID = 0
nFormID = 0
nSubFormID = 0
nRegistroID = 0
nDtConsulta = ""
nUsuarioID = 0
nValidacaoDadoID = 0

For i = 1 To Request.QueryString("nFonteDadoID").Count    
    nFonteDadoID = Request.QueryString("nFonteDadoID")(i)
Next


For i = 1 To Request.QueryString("nTipoDadoID").Count    
    nTipoDadoID = Request.QueryString("nTipoDadoID")(i)
Next


For i = 1 To Request.QueryString("nTipoRegistroID").Count    
    nTipoRegistroID = Request.QueryString("nTipoRegistroID")(i)
Next


For i = 1 To Request.QueryString("nFormID").Count    
    nFormID = Request.QueryString("nFormID")(i)
Next

For i = 1 To Request.QueryString("nSubFormID").Count    
    nSubFormID = Request.QueryString("nSubFormID")(i)
Next

For i = 1 To Request.QueryString("nRegistroID").Count    
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

For i = 1 To Request.QueryString("nDtConsulta").Count    
    nDtConsulta = Request.QueryString("nDtConsulta")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

Set rsCommand = Server.CreateObject("ADODB.Command")

With rsCommand
    .CommandTimeout = 60 * 10
	.ActiveConnection = strConn
	.CommandText = "sp_ValidacoesDados_Insert"
	.CommandType = adCmdStoredProc
	    
    .Parameters.Append( .CreateParameter("@FonteDadosID", adInteger, adParamInput) )    
    If ((nFonteDadoID = "") OR (nFonteDadoID = Null)) Then
		.Parameters("@FonteDadosID").Value = Null
	Else
		.Parameters("@FonteDadosID").Value = CLng(nFonteDadoID)
	End If	
	
	.Parameters.Append( .CreateParameter("@TipoDadoID", adInteger, adParamInput) )    
    If ((nTipoDadoID = "") OR (nTipoDadoID = Null)) Then
		.Parameters("@TipoDadoID").Value = Null
	Else
		.Parameters("@TipoDadoID").Value = CLng(nTipoDadoID)
	End If	
	
	.Parameters.Append( .CreateParameter("@TipoRegistroID", adInteger, adParamInput) )    
    If ((nTipoRegistroID = "") OR (nTipoRegistroID = Null)) Then
		.Parameters("@TipoRegistroID").Value = Null
	Else
		.Parameters("@TipoRegistroID").Value = CLng(nTipoRegistroID)
	End If	
	
    .Parameters.Append( .CreateParameter("@FormID", adInteger, adParamInput) )    
    If ((nFormID = "") OR (nFormID = Null)) Then
		.Parameters("@FormID").Value = Null
	Else
		.Parameters("@FormID").Value = CLng(nFormID)
	End If	

    .Parameters.Append( .CreateParameter("@SubFormID", adInteger, adParamInput) )    
    If ((nSubFormID = "") OR (nSubFormID = Null)) Then
		.Parameters("@SubFormID").Value = Null
	Else
		.Parameters("@SubFormID").Value = CLng(nSubFormID)
	End If	

    .Parameters.Append( .CreateParameter("@RegistroID", adInteger, adParamInput) )    
    If ((nRegistroID = "") OR (nRegistroID = Null)) Then
		.Parameters("@RegistroID").Value = Null
	Else
		.Parameters("@RegistroID").Value = CLng(nRegistroID)
	End If	

    .Parameters.Append( .CreateParameter("@DtConsulta", adDate, adParamInput, 30) )    
    If ((nDtConsulta = "") OR (nDtConsulta = Null)) Then
		.Parameters("@DtConsulta").Value = Null
	Else
		.Parameters("@DtConsulta").Value = CLng(nDtConsulta)
	End If	

    .Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )    
    If ((nUsuarioID = "") OR (nUsuarioID = Null)) Then
		.Parameters("@UsuarioID").Value = Null
	Else
		.Parameters("@UsuarioID").Value = CLng(nUsuarioID)
	End If	

	.Parameters.Append( .CreateParameter("@ValidacaoDadoID", adInteger, adParamOutput) )
	.Execute
	
	nValidacaoDadoID = rsCommand.Parameters("@ValidacaoDadoID").Value

End With

Set rsCommand = Nothing
    
Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "ValidacaoDadoID", adVarChar, 8000, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew

If ( NOT IsNull(nValidacaoDadoID) ) Then
	rsNew.Fields("ValidacaoDadoID").Value = CStr(nValidacaoDadoID)
End If

rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
%>

