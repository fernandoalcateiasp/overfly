
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Dim i, nUserID, nEmpresaID, sNomePessoa, nSoEmpLog, strOperator
Dim sEmpresa
nUserID = 0
nEmpresaID = 0
sNomePessoa = ""
nSoEmpLog = 0
strOperator = " >= "
sEmpresa = ""	
	    
For i = 1 To Request.QueryString("nUserID").Count    
  nUserID = Request.QueryString("nUserID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
  nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("sNomePessoa").Count
  sNomePessoa = Chr(39) & Request.QueryString("sNomePessoa")(i) & Chr(39)
Next

For i = 1 To Request.QueryString("nSoEmpLog").Count    
  nSoEmpLog = Request.QueryString("nSoEmpLog")(i)
Next

sNomePessoa = Trim(sNomePessoa)

If ( (Left(sNomePessoa, 2) = "'%") OR (Right(sNomePessoa, 2) = "%'") ) Then
    strOperator = " LIKE "
End If

Dim rsData, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")

If ( CLng(nSoEmpLog) = 0 ) Then
    sEmpresa = ""
Else
    sEmpresa = "(b.ObjetoID IN (" & Cstr(nEmpresaID) & ")) AND "
End If

strSQL = "SELECT DISTINCT TOP 1000 a.Fantasia, " & _
                "dbo.fn_Pessoa_Telefone(a.PessoaID, 119, 119, 1, 1, NULL) AS Telefone, " & _
                "dbo.fn_Pessoa_Telefone(a.PessoaID, 121, 121, 1, 0, NULL) AS Celular, dbo.fn_Pessoa_URL(a.PessoaID, 124, NULL) AS Email, " & _
		        "0 AS [To], 0 AS [Cc], 0 AS [Bcc] " & _
	        "FROM Pessoas a WITH(NOLOCK) " & _
		        "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.SujeitoID = a.PessoaID) " & _
	        "WHERE ((a.EstadoID = 2) AND " & _
			        "(b.EstadoID = 2) AND " & _
			        "(b.TipoRelacaoID = 31) AND " & _
			        sEmpresa & _
			        "(a.Fantasia " & strOperator & sNomePessoa & ")) " & _
	        "ORDER BY a.Fantasia "

'@PessoaID NUMERIC(10),
'@EmpresaID NUMERIC(10),
'@EstadoID NUMERIC(10),
'@TipoDetalhamentoID NUMERIC(10)   
             
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

rsData.Save Response,adPersistXML
rsData.Close
Set rsData = Nothing
%>
