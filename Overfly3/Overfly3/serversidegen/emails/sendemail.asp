
<!-- METADATA TYPE="typelib" NAME="Microsoft CDO for NTS 1.2 library" UUID="{0E064ADD-9D99-11D0-ABE5-00AA0064D470}" VERSION="1.2"-->
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0

    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Function ReplaceTest(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceTest = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function
%>

<%
	Dim i, nMailtoSendID, mailFrom, mailTo, mailCc, mailBcc, mailSubject, sMailBody, bHTML
	Dim objSendMail, rsData, strSQL
	
	bHTML = 0

	For i = 1 To Request.QueryString("nMailtoSendID").Count
        nMailtoSendID = Request.QueryString("nMailtoSendID")(i)
    Next

	For i = 1 To Request.QueryString("bHTML").Count
        bHTML = Request.QueryString("bHTML")(i)
    Next
    
	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = "SELECT * FROM EMailsTemp WITH(NOLOCK) WHERE EMailID = " & CStr(nMailtoSendID)

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    mailTo = ""
    mailFrom = ""
    mailCc = ""
    mailBcc = ""
    mailSubject = ""
    sMailBody = ""

    If (Not rsData.EOF) Then

        mailTo = rsData.Fields("MailTo").Value
        mailFrom = rsData.Fields("MailFrom").Value
        mailCc = rsData.Fields("MailCc").Value
        mailBcc = rsData.Fields("MailBcc").Value
        mailSubject = rsData.Fields("Subject").Value
        sMailBody = rsData.Fields("MailBody").Value

		if (CInt(bHTML) = 0) Then
			sMailBody = ReplaceTest( sMailBody, Chr(13) & Chr(10), "<BR>")
			sMailBody = ReplaceTest( sMailBody, Chr(32), "&nbsp")

			sMailBody = "<HTML><BODY><P><FONT FACE='Courier New' SIZE='2'>" & sMailBody & "</FONT></P></BODY></HTML>"
		End If
		
        'Manda o e mail
        Set objSendMail = CreateObject("CDONTS.NewMail")
                    
        With objSendMail
            .From = mailFrom
            .To = mailTo
            .Cc = mailCc
            .Bcc = mailBcc
            .Subject = mailSubject
            .Body = sMailBody
            
            .BodyFormat = 0
            .MailFormat = 0
            .Importance = CdoNormal
            .Send
        End With

        Set objSendMail = Nothing
        
        rsData.Close
        strSQL = "SELECT 'E-Mail enviado com sucesso.' AS msg "
    Else
        rsData.Close
        strSQL = "SELECT 'N�o foi poss�vel enviar o e-mail.' AS msg "
    End If
    
    
    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    rsData.Save Response, adPersistXML
    
    rsData.Close
    Set rsData = Nothing
%>
