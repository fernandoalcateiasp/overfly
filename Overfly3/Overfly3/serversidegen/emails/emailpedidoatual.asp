
<!-- METADATA TYPE="typelib" NAME="Microsoft CDO for NTS 1.2 library" UUID="{0E064ADD-9D99-11D0-ABE5-00AA0064D470}" VERSION="1.2"-->
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0

    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Function ReplaceTest(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceTest = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function
%>

<%
	Dim i, nVendedorID, nContatoID, nEmpresaID, nClienteID, nUsuarioFinalID, mailFrom, mailTo, mailCc, mailBcc
	Dim mailSubject, sMailBody, bHTML, bMandEmail
	Dim objSendMail, rsData, strSQL
	
	bHTML = 0
	bMandEmail = 0
	nVendedorID = 0
	nContatoID = 0
	nEmpresaID = 0
	nClienteID = 0
	nUsuarioFinalID = 0
    mailTo = ""
    mailFrom = ""
    mailCc = ""
    mailBcc = ""
    mailSubject = ""
    sMailBody = ""
	
	For i = 1 To Request.QueryString("bHTML").Count
        bHTML = Request.QueryString("bHTML")(i)
    Next

	For i = 1 To Request.QueryString("bMandEmail").Count
        bMandEmail = Request.QueryString("bMandEmail")(i)
    Next

	For i = 1 To Request.QueryString("nVendedorID").Count
        nVendedorID = Request.QueryString("nVendedorID")(i)
    Next

	For i = 1 To Request.QueryString("nEmpresaID").Count
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next

	For i = 1 To Request.QueryString("nClienteID").Count
        nClienteID = Request.QueryString("nClienteID")(i)
    Next

	For i = 1 To Request.QueryString("nUsuarioFinalID").Count
        nUsuarioFinalID = Request.QueryString("nUsuarioFinalID")(i)
    Next

	For i = 1 To Request.QueryString("nContatoID").Count
        nContatoID = Request.QueryString("nContatoID")(i)
    Next

	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQL = "SELECT (SELECT TOP 1 Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=" & CStr(nVendedorID) & ") AS Vendedor, " & _
				"dbo.fn_Pessoa_Telefone(" & CStr(nVendedorID) & ", 119, 120,1,0,NULL) AS VendedorTelefone, " & _
				"(SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID = " & CStr(nContatoID) & ") AS Contato," & _
				CStr(nContatoID) & " AS ContatoID," & _
				"dbo.fn_Pessoa_URL(" & CStr(nVendedorID) & ", 124, NULL) AS VendedorEmail, " & _
				"dbo.fn_Pessoa_URL(" & CStr(nContatoID) & ", 124, NULL) AS ContatoEmail, " & _
				"(SELECT TOP 1 Fantasia FROM Pessoas WITH(NOLOCK) WHERE PessoaID=" & CStr(nEmpresaID) & ") AS Empresa, " & _
				"dbo.fn_Pessoa_URL(" & CStr(nEmpresaID) & ", 125, NULL) AS EmpresaURL"

    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    Dim sWebPath
    
	sWebPath = rsData.Fields("EmpresaURL").Value & "/commonpages/serverside/imageblob.asp"
    If (CBool(bMandEmail) AND (Not rsData.EOF)) Then
		If ((rsData.Fields("ContatoEmail").Value = "") OR (isNull(rsData.Fields("ContatoEmail").Value))) Then
			rsData.Close
			strSQL = "SELECT 'O contato n�o tem e-mail cadastrado.' AS msg, " & CStr(nContatoID) & " AS ContatoID"
		Else
			nContatoID = rsData.Fields("ContatoID").Value
			mailTo = rsData.Fields("ContatoEmail").Value
			mailFrom = rsData.Fields("VendedorEmail").Value
			'mailCc = rsData.Fields("MailCc").Value
			'mailBcc = rsData.Fields("MailBcc").Value
			mailSubject = rsData.Fields("Empresa").Value & " - Pedido atual"
			sMailBody = "<B>" & rsData.Fields("Contato").Value & ",</B><BR><BR>" & _
				"<A HREF=" & Chr(34) & rsData.Fields("EmpresaURL").Value & "/default.aspx?nUserID=" & CStr(nContatoID) & _
				"&nEmpresaID=" & CStr(nEmpresaID) & _
				"&nClienteID=" & CStr(nClienteID) & _
				"&nUsuarioFinalID=" & CStr(nUsuarioFinalID) & _
				"&nPageID=13" & Chr(34) & ">Clique aqui</A> para finalizar o seu pedido.<BR><BR><BR>" & _
				"<B>" & rsData.Fields("Vendedor").Value & "</B><BR>" & _
				rsData.Fields("VendedorTelefone").Value & "<BR>"
				
			sMailBody = sMailBody & "<A href='" & rsData.Fields("EmpresaURL").Value & "'>" & _
				"<IMG SRC='" & sWebPath & "?nFormID=1210&" & _
						"nSubFormID=20100&nRegistroID=" & CStr(nEmpresaID) & "' BORDER='0'></IMG>" & _
				"</A><BR><BR>"
			
			sMailBody = "<HTML><BODY><P><FONT FACE='Tahoma' SIZE='2'>" & sMailBody & "</FONT></P></BODY></HTML>"
			
			'Manda o e mail
			Set objSendMail = CreateObject("CDONTS.NewMail")
			
			mailFrom = rsData.Fields("VendedorEmail").Value
			mailTo = rsData.Fields("ContatoEmail").Value
			mailCc = ""
			mailBcc = "silvia.goncalves@alcateia.com.br"
			
			With objSendMail
				.From = mailFrom
				.To = mailTo
				.Cc = mailCc
				.Bcc = mailBcc
				.Subject = mailSubject
				.Body = sMailBody
				.BodyFormat = 0
				.MailFormat = 0
				.Importance = CdoNormal
				.Send
			End With

			Set objSendMail = Nothing
	        
			rsData.Close
			strSQL = "SELECT 'E-Mail enviado com sucesso.' AS msg, " & CStr(nContatoID) & " AS ContatoID"
        End If
	ElseIf ((Not CBool(bMandEmail)) AND (Not rsData.EOF)) Then        
		nContatoID = rsData.Fields("ContatoID").Value
        rsData.Close
        strSQL = "SELECT 'E-Mail enviado com sucesso.' AS msg, " & CStr(nContatoID) & " AS ContatoID"
    Else
        rsData.Close
        strSQL = "SELECT 'N�o foi poss�vel enviar o e-mail.' AS msg, " & CStr(nContatoID) & " AS ContatoID"
    End If
    
    
    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    rsData.Save Response, adPersistXML
    
    rsData.Close
    Set rsData = Nothing
%>
