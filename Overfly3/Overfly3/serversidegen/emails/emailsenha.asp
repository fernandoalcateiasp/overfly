<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    Dim nFromID, nRelPesContatoID, nContatoID, bChangeSenha
    Dim i, sResultado, sResultado1
        
    nFromID = 0
    nContatoID = 0
    nRelPesContatoID = 0
    bChangeSenha = 0
    sResultado1 = ""
    sResultado = ""
    

    For i = 1 To Request.QueryString("nFromID").Count    
        nFromID = Request.QueryString("nFromID")(i)
    Next

    For i = 1 To Request.QueryString("nRelPesContatoID").Count    
        nRelPesContatoID = Request.QueryString("nRelPesContatoID")(i)
    Next
    
    For i = 1 To Request.QueryString("nContatoID").Count    
        nContatoID = Request.QueryString("nContatoID")(i)
    Next
        
    For i = 1 To Request.QueryString("bChangeSenha").Count    
        bChangeSenha = Request.QueryString("bChangeSenha")(i)
    Next
    
    For i = 1 To Request.QueryString("nPesUrlID").Count    
        nPesUrlID = Request.QueryString("nPesUrlID")(i)
    Next
    
	Dim rsSPCommand, rsCommand, strSQL, bHasError, nRecsAffected, rsERROR
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
	bHasError = False
	
	if (CInt(bChangeSenha)=1) Then				
	    Set rsSPCommand = Server.CreateObject("ADODB.Command")
		With rsSPCommand
			.CommandTimeout = 60 * 10
			.ActiveConnection = strConn
			.CommandText = "sp_Senha_Insert"
			.CommandType = adCmdStoredProc

			.Parameters.Append( .CreateParameter("@EmailLogin", adVarchar, adParamInput, 80) )
			.Parameters("@EmailLogin").Value = CStr(nContatoID)
			
			.Parameters.Append( .CreateParameter("@Senha", adVarchar, adParamInput, 20) )
			.Parameters("@Senha").Value = Null
			
			.Parameters.Append( .CreateParameter("@GerarSenha", adBoolean , adParamInput) )
			.Parameters("@GerarSenha").Value = True
			
			.Parameters.Append( .CreateParameter("@ForcarTroca", adBoolean, adParamInput) )
			.Parameters("@ForcarTroca").Value = True
			
			.Parameters.Append( .CreateParameter("@CaminhoFTP", adVarchar, adParamInput, 800) )
			.Parameters("@CaminhoFTP").Value = Null
			
			.Parameters.Append( .CreateParameter("@PesUrlID", adVarchar, adParamInput, 20) )
			.Parameters("@PesUrlID").Value = CStr(nPesUrlID)
			
			.Parameters.Append( .CreateParameter("@Tipo", adInteger, adParamInput) )
			.Parameters("@Tipo").Value = 2 'tipo web
						
	        .Parameters.Append( .CreateParameter("@Resultado", adVarchar, adParamOutput, 8000) )

			.Execute
        End With			
		
		sResultado1 = rsSPCommand.Parameters("@Resultado").Value
			
	End If	
	
	If ( ((CInt(bChangeSenha)=1) AND (sResultado1 <> "")) OR (CInt(bChangeSenha)=0)) Then
	
	    Set rsSPCommand = Nothing
	    Set rsSPCommand = Server.CreateObject("ADODB.Command")
	
		With rsSPCommand
			.CommandTimeout = 60 * 10
			.ActiveConnection = strConn
			.CommandText = "sp_Email_ConfirmacaoSenha"
			.CommandType = adCmdStoredProc

			.Parameters.Append( .CreateParameter("@RelPesContatoID", adInteger, adParamInput) )
			.Parameters("@RelPesContatoID").Value = CLng(nRelPesContatoID)

	        .Parameters.Append( .CreateParameter("@Resultado", adVarchar, adParamOutput, 100) )

			.Execute

		End With

		sResultado = rsSPCommand.Parameters("@Resultado").Value
	End If

    Dim rsNew
    
    Set rsNew = Server.CreateObject("ADODB.Recordset")
    rsNew.CursorLocation = adUseServer
    rsNew.Fields.Append "fldResponse", adVarChar, 50, adFldUpdatable
    rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
    
    rsNew.AddNew
    If ((sResultado = "") OR IsNull(sResultado))Then
		if (CInt(bChangeSenha)=1) Then
			rsNew.Fields("fldResponse").Value = "Senha alterada e enviada para o e-mail do contato."
		Else
			rsNew.Fields("fldResponse").Value = "Senha enviada para o e-mail do contato."
		End If
    Else
        rsNew.Fields("fldResponse").Value = sResultado
    End If
    
    rsNew.Update
    
    rsNew.Save Response, adPersistXML
    
    Set rsSPCommand = Nothing
    
    rsNew.Close
    Set rsNew = Nothing
%>
