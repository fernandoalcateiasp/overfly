
<!-- METADATA TYPE="typelib" NAME="Microsoft CDO for NTS 1.2 library" UUID="{0E064ADD-9D99-11D0-ABE5-00AA0064D470}" VERSION="1.2"-->
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    Dim nEmpresaID, nParceiroID, nContatoID, nTipo, sEMail
    Dim i, sProcedure, sResultado, sAlert
        
    nEmpresaID = 0
    nParceiroID = 0
    nContatoID = 0
    nTipo = 0
    sEMail = Null

    For i = 1 To Request.QueryString("nEmpresaID").Count    
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next

    For i = 1 To Request.QueryString("nParceiroID").Count    
        nParceiroID = Request.QueryString("nParceiroID")(i)
    Next

    For i = 1 To Request.QueryString("nContatoID").Count
        nContatoID = Request.QueryString("nContatoID")(i)
    Next

    For i = 1 To Request.QueryString("nTipo").Count
        nTipo = Request.QueryString("nTipo")(i)
    Next

    For i = 1 To Request.QueryString("sEMail").Count
        sEMail = Request.QueryString("sEMail")(i)
    Next

	If (nTipo = 0) Then
		sProcedure = "sp_Email_PSC"
		sAlert = "PSC enviada para o e-mail do contato."
	ElseIf (nTipo = 1) Then
		sProcedure = "sp_Email_QAF"
		sAlert = "QAF enviado para o e-mail do contato."
	ElseIf (nTipo = 2) Then
		sProcedure = "sp_Email_RRC"
		sAlert = "RRC enviado para o e-mail do contato."		
	End If
	
	Dim rsSPCommand
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
	  
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = sProcedure
	    .CommandType = adCmdStoredProc

		.Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
		.Parameters("@EmpresaID").Value = CLng(nEmpresaID)

		.Parameters.Append( .CreateParameter("@ParceiroID", adInteger, adParamInput) )
		.Parameters("@ParceiroID").Value = CLng(nParceiroID)

		.Parameters.Append( .CreateParameter("@ContatoID", adInteger, adParamInput) )
		.Parameters("@ContatoID").Value = CLng(nContatoID)
		
		If (nTipo = 2) Then
			.Parameters.Append( .CreateParameter("@EMail", adVarchar, adParamInput, 80) )
			.Parameters("@EMail").Value = sEMail
		End If
					    
	    .Parameters.Append( .CreateParameter("@Resultado", adVarchar, adParamOutput, 100) )
	    
	    .Execute
	    
	End With

	sResultado = rsSPCommand.Parameters("@Resultado").Value

    Dim rsNew
    
    Set rsNew = Server.CreateObject("ADODB.Recordset")
    rsNew.CursorLocation = adUseServer
    rsNew.Fields.Append "fldResponse", adVarChar, 100, adFldUpdatable
    rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
    
    rsNew.AddNew
    If IsNull(sResultado) Then
        rsNew.Fields("fldResponse").Value = sAlert
    Else
        rsNew.Fields("fldResponse").Value = sResultado
    End If
    
    rsNew.Update
    
    rsNew.Save Response, adPersistXML
    
    Set rsSPCommand = Nothing
    
    rsNew.Close
    Set rsNew = Nothing
%>
