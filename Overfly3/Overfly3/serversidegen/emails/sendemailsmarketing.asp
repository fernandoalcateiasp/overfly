
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    Dim nEmpresaID, sEstados, sCanais, sClassificacoes, sPaises, sUFs, sCidades, sFamilias, sMarcas
    Dim sProdutos, nEstadoProdutoID, sAssunto, nTipoEmailID, dtValidade
    Dim nUsuarioID, nVitrineID, nTipoResultadoID, nOK, bEspecifico
    Dim i 
        
    nEmpresaID = Null
    sEstados = Null
    sCanais = Null
    sClassificacoes = Null
    sPaises = Null
    sUFs = Null
    sCidades = Null
    sFamilias = Null
    sMarcas = Null
    sProdutos = Null
    nEstadoProdutoID = Null
    sAssunto = Null
    nTipoEmailID = 1
    nUsuarioID = 0
    nVitrineID = 0
    dtValidade = Null
    bEspecifico = 0

    For i = 1 To Request.QueryString("nEmpresaID").Count
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next
    
    For i = 1 To Request.QueryString("sEstados").Count
        sEstados = Request.QueryString("sEstados")(i)
    Next

    For i = 1 To Request.QueryString("sCanais").Count
        sCanais = Request.QueryString("sCanais")(i)
    Next

    For i = 1 To Request.QueryString("sClassificacoes").Count
        sClassificacoes = Request.QueryString("sClassificacoes")(i)
    Next

    For i = 1 To Request.QueryString("sPaises").Count
        sPaises = Request.QueryString("sPaises")(i)
    Next

    For i = 1 To Request.QueryString("sUFs").Count
        sUFs = Request.QueryString("sUFs")(i)
    Next

    For i = 1 To Request.QueryString("sCidades").Count
        sCidades = Request.QueryString("sCidades")(i)
    Next

    For i = 1 To Request.QueryString("sFamilias").Count
        sFamilias = Request.QueryString("sFamilias")(i)
    Next

    For i = 1 To Request.QueryString("sMarcas").Count
        sMarcas = Request.QueryString("sMarcas")(i)
    Next

    For i = 1 To Request.QueryString("sProdutos").Count
        sProdutos = Request.QueryString("sProdutos")(i)
    Next

    For i = 1 To Request.QueryString("nEstadoProdutoID").Count
        nEstadoProdutoID = Request.QueryString("nEstadoProdutoID")(i)
    Next

    For i = 1 To Request.QueryString("sAssunto").Count
        sAssunto = Request.QueryString("sAssunto")(i)
    Next

    For i = 1 To Request.QueryString("nTipoEmailID").Count
        nTipoEmailID = Request.QueryString("nTipoEmailID")(i)
    Next

    For i = 1 To Request.QueryString("nUsuarioID").Count
        nUsuarioID = Request.QueryString("nUsuarioID")(i)
    Next
    
    For i = 1 To Request.QueryString("nVitrineID").Count
        nVitrineID = Request.QueryString("nVitrineID")(i)
    Next

    For i = 1 To Request.QueryString("nTipoResultadoID").Count
        nTipoResultadoID = Request.QueryString("nTipoResultadoID")(i)
    Next

    For i = 1 To Request.QueryString("dtValidade").Count
        dtValidade = Request.QueryString("dtValidade")(i)
    Next

    For i = 1 To Request.QueryString("nOK").Count
        nOK = Request.QueryString("nOK")(i)
    Next

    For i = 1 To Request.QueryString("bEspecifico").Count
        bEspecifico = Request.QueryString("bEspecifico")(i)
    Next

	Dim rsData, rsSPCommand
	
	Set rsData = Server.CreateObject("ADODB.Recordset")
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
	  
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_Email_Marketing"
	    .CommandType = adCmdStoredProc

		.Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
		.Parameters("@EmpresaID").Value = nEmpresaID

		.Parameters.Append( .CreateParameter("@Estados", adVarchar, adParamInput, 8000) )
		.Parameters("@Estados").Value = sEstados

		.Parameters.Append( .CreateParameter("@Atuacoes", adVarchar, adParamInput, 8000) )
		.Parameters("@Atuacoes").Value = sCanais

		.Parameters.Append( .CreateParameter("@Classificacoes", adVarchar, adParamInput, 8000) )
		.Parameters("@Classificacoes").Value = sClassificacoes

		.Parameters.Append( .CreateParameter("@Paises", adVarchar, adParamInput, 8000) )
		.Parameters("@Paises").Value = sPaises

		.Parameters.Append( .CreateParameter("@UFs", adVarchar, adParamInput, 8000) )
		.Parameters("@UFs").Value = sUFs

		.Parameters.Append( .CreateParameter("@Cidades", adVarchar, adParamInput, 8000) )
		.Parameters("@Cidades").Value = sCidades

		.Parameters.Append( .CreateParameter("@Familias", adVarchar, adParamInput, 8000) )
		.Parameters("@Familias").Value = sFamilias

		.Parameters.Append( .CreateParameter("@Marcas", adVarchar, adParamInput, 8000) )
		.Parameters("@Marcas").Value = sMarcas

		.Parameters.Append( .CreateParameter("@Produtos", adVarchar, adParamInput, 1000) )
		.Parameters("@Produtos").Value = sProdutos

		.Parameters.Append( .CreateParameter("@EstadoProdutoID", adInteger, adParamInput) )
		.Parameters("@EstadoProdutoID").Value = nEstadoProdutoID

		.Parameters.Append( .CreateParameter("@TipoEmailID", adInteger, adParamInput) )
		.Parameters("@TipoEmailID").Value = CInt(nTipoEmailID)

		.Parameters.Append( .CreateParameter("@dtValidade", adDate, adParamInput) )
		.Parameters("@dtValidade").Value = CDate(dtValidade)

		.Parameters.Append( .CreateParameter("@Assunto", adVarchar, adParamInput, 8000) )
		.Parameters("@Assunto").Value = sAssunto

		.Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
		.Parameters("@UsuarioID").Value = nUsuarioID
		
		.Parameters.Append( .CreateParameter("@VitrineID", adInteger, adParamInput) )
		.Parameters("@VitrineID").Value = nVitrineID

		.Parameters.Append( .CreateParameter("@TipoResultadoID", adInteger, adParamInput) )
		.Parameters("@TipoResultadoID").Value = nTipoResultadoID
		
		.Parameters.Append( .CreateParameter("@EmailEspecifico", adBoolean, adParamInput) )
		.Parameters("@EmailEspecifico").Value = CBool(bEspecifico)

		.Parameters.Append( .CreateParameter("@OK", adBoolean, adParamInputOutput) )

		If (nOK = 1) Then
			.Parameters("@OK").Value = True
		Else			
			.Parameters("@OK").Value = False
		End If

		.Parameters.Append( .CreateParameter("@Resultado", adVarchar, adParamOutput, 8000 ) )

	    if (CInt(nTipoResultadoID) <> 2) Then
			.Execute
		Else
			Set rsData = .Execute
		End If	

	End With

    Dim rsNew
    
	if (CInt(nTipoResultadoID) <> 2) Then

		Set rsNew = Server.CreateObject("ADODB.Recordset")
		rsNew.CursorLocation = adUseServer
		rsNew.Fields.Append "nOK", adBoolean, , adFldUpdatable
		rsNew.Fields.Append "fldResponse", adVarChar, 8000, adFldUpdatable
		rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
    
		rsNew.AddNew
		If (isNull(rsSPCommand.Parameters("@Resultado").Value)) Then
			rsNew.Fields("fldResponse").Value = "E-mails enviados com sucesso"
		Else

			if (CInt(nTipoResultadoID) = 4) Then
				rsNew.Fields("nOK").Value = rsSPCommand.Parameters("@OK").Value
			End If

			rsNew.Fields("fldResponse").Value = rsSPCommand.Parameters("@Resultado").Value
		End If	
		rsNew.Update
    
		rsNew.Save Response, adPersistXML
		rsNew.Close
		Set rsNew = Nothing

	Else

		rsData.Save Response, adPersistXML
		rsData.Close
		Set rsData = Nothing		

	End If
	
    Set rsSPCommand = Nothing
    
%>
