
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew
Dim nValidacaoDadoID, nPaisID
Dim rsCommand, rsCommand2
Dim i
Dim strSQL
Dim rsData
Dim sResultado

nValidacaoDadoID = 0
nPaisID = 0

For i = 1 To Request.QueryString("nValidacaoDadoID").Count    
    nValidacaoDadoID = Request.QueryString("nValidacaoDadoID")(i)
Next

For i = 1 To Request.QueryString("nPaisID").Count    
    nPaisID = Request.QueryString("nPaisID")(i)
Next

Set rsCommand = Server.CreateObject("ADODB.Command")

With rsCommand
    .CommandTimeout = 60 * 10
	.ActiveConnection = strConn
	.CommandText = "sp_ValidacoesDados_GravaValorID"
	.CommandType = adCmdStoredProc
	    
    .Parameters.Append( .CreateParameter("@ValidacaoDadoID", adInteger, adParamInput) )    
    If ((nValidacaoDadoID = "") OR (nValidacaoDadoID = Null)) Then
		.Parameters("@ValidacaoDadoID").Value = Null
	Else
		.Parameters("@ValidacaoDadoID").Value = CLng(nValidacaoDadoID)
	End If	
	
    .Parameters.Append( .CreateParameter("@PaisID", adInteger, adParamInput) )    
    If ((nPaisID = "") OR (nPaisID = Null)) Then
		.Parameters("@PaisID").Value = Null
	Else
		.Parameters("@PaisID").Value = CLng(nPaisID)
	End If	
	
	.Execute
End With

Set rsCommand = Nothing

Set rsCommand2 = Server.CreateObject("ADODB.Command")

With rsCommand2
    .CommandTimeout = 60 * 10
	.ActiveConnection = strConn
	.CommandText = "sp_ValidacoesDados_AtualizaPessoa"
	.CommandType = adCmdStoredProc
	    
    .Parameters.Append( .CreateParameter("@ValidacaoDadoID", adInteger, adParamInput) )    
    If ((nValidacaoDadoID = "") OR (nValidacaoDadoID = Null)) Then
		.Parameters("@ValidacaoDadoID").Value = Null
	Else
		.Parameters("@ValidacaoDadoID").Value = CLng(nValidacaoDadoID)
	End If	
	
	.Execute
End With


Set rsData = Server.CreateObject("ADODB.Recordset")
strSQL = "SELECT dbo.fn_ValidacoesDados_EhValida(" & CStr(nValidacaoDadoID) & ") AS ResultadoConsulta"
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
sResultado = ""

If (Not (rsData.BOF AND rsData.EOF) ) Then
    sResultado = rsData.Fields("ResultadoConsulta").Value
End If

Set rsCommand2 = Nothing
rsData.Close
Set rsData = Nothing
    
Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "Resultado", adVarChar, 8000, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew
If ( NOT IsNull(sResultado) ) Then
	rsNew.Fields("Resultado").Value = sResultado
End If
rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing

%>

