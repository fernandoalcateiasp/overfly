
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "DateTime", adDBDate, 8, adFldMayBeNull OR adFldUpdatable
rsNew.Fields.Append "Hora", adInteger, adFldMayBeNull OR adFldUpdatable
rsNew.Fields.Append "Minuto", adInteger, adFldMayBeNull OR adFldUpdatable

rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

rsNew.AddNew
rsNew.Fields("DateTime").Value = Date
rsNew.Fields("Hora").Value = CInt(Hour(Now()))
rsNew.Fields("Minuto").Value = CInt(Minute(Now()))
rsNew.Update
   
' send the new data back to the client
rsNew.Save Response, adPersistXML
  
rsNew.Close
Set rsNew = Nothing
%>
