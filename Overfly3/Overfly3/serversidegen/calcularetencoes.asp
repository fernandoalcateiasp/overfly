
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew

'Dim nPedidoID, nFinanceiroID, nImpostoID, nValorRetencao, nValorID
Dim nFinanceiroID, nImpostoID
Dim nValor
Dim nValorID
Dim sDtFaturamento
Dim nRecsAffected
Dim i
Dim sResultado
Dim sFinanceiros

nFinanceiroID = 0
nImpostoID = 0
nValorID = 0
nValor = 0
nImpostoID = 0
sDtFaturamento = ""
sFinanceiros = ""

For i = 1 To Request.QueryString("nFinanceiroID").Count
    nFinanceiroID = Request.QueryString("nFinanceiroID")(i)
Next

For i = 1 To Request.QueryString("nValor").Count
    nValor = Request.QueryString("nValor")(i)
Next

If (nValor = 0) Then
    nValor = null
End If


For i = 1 To Request.QueryString("nValorID").Count
    nValorID = Request.QueryString("nValorID")(i)
Next

If (nValorID = 0) Then
    nValorID = null
End If

For i = 1 To Request.QueryString("nImpostoID").Count
    nImpostoID = Request.QueryString("nImpostoID")(i)
Next

If (nImpostoID = 0) Then
    nImpostoID = null
End If

For i = 1 To Request.QueryString("sDtFaturamento").Count
    sDtFaturamento = Request.QueryString("sDtFaturamento")(i)
Next

If ((sDtFaturamento = "") OR (sDtFaturamento = 0)) Then
    sDtFaturamento = null
End If

For i = 1 To Request.QueryString("sFinanceiros").Count
    sFinanceiros = Request.QueryString("sFinanceiros")(i)
Next

If (sFinanceiros = "") Then
    sFinanceiros = null
End If

Dim rsSPCommand
Set rsSPCommand = Server.CreateObject("ADODB.Command")
  
With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandText = "sp_Financeiro_Retencao"
    .CommandType = adCmdStoredProc

    .Parameters.Append( .CreateParameter("@FinanceiroID", adInteger, adParamInput) )
    .Parameters("@FinanceiroID").Value = nFinanceiroID

    .Parameters.Append( .CreateParameter("@Valor", adCurrency, adParamInput) )
    If ((CStr(nValor) = "") OR (CDbl(nValor) = 0)) Then
	    .Parameters("@Valor").Value = null
    Else
	    .Parameters("@Valor").Value = CDbl(nValor)
    End If
    
    .Parameters.Append( .CreateParameter("@ValorID", adCurrency, adParamInput) )
    If ((CStr(nValorID) = "") OR (CDbl(nValorID) = 0)) Then
	    .Parameters("@ValorID").Value = null
    Else
	    .Parameters("@ValorID").Value = nValorID
    End If
    
    .Parameters.Append( .CreateParameter("@ImpostoID", adCurrency, adParamInput) )
    If ((CStr(nImpostoID) = "") OR (CDbl(nImpostoID) = 0)) Then
	    .Parameters("@ImpostoID").Value = null
    Else
	    .Parameters("@ImpostoID").Value = nImpostoID
    End If

    .Parameters.Append( .CreateParameter("@DtData", adDate, adParamInput) )
    If ((CStr(sDtFaturamento) = "") OR (CDbl(sDtFaturamento) = 0)) Then
	    .Parameters("@DtData").Value = null
    Else
	    .Parameters("@DtData").Value = CDate(sDtFaturamento)
    End If
    
    .Parameters.Append( .CreateParameter("@Financeiros", adVarChar, adParamInput, 8000) )
    If (CStr(sFinanceiros) = "") Then
	    .Parameters("@Financeiros").Value = null
    Else
	    .Parameters("@Financeiros").Value = sFinanceiros
    End If

    .Execute

End With

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "fldresp", adVarChar, 50, adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew
rsNew.Fields("fldresp").Value = CStr(nRecsAffected)
rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing

%>