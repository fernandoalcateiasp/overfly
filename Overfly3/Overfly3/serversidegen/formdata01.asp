
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0

    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%

Dim i, formName

For i = 1 To Request.QueryString("formName").Count    
    formName = Request.QueryString("formName")(i)
Next
    
Dim strSQL, rsData

Set rsData = Server.CreateObject("ADODB.Recordset")
        
strSQL = "SELECT RecursoID, RecursoFantasia AS Recurso, FormName, FrameStart, ArqStart " & _
         "FROM Recursos WITH(NOLOCK) " & _
         "WHERE FormName='" & formName & "' AND TipoRecursoID = 2 AND ClassificacaoID = 11"
                  
rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

'----------------------------------------------------------------
'Fecha objetos ap�s mandar devolver o XML
   
rsData.Save Response, adPersistXML
    
rsData.Close
Set rsData = Nothing

%>