
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    Dim i, nEmpresaID, nEmpresaPaisID, nPessoaID, nParceiroID, nUserID, nTipo, rsData 
    
    nEmpresaID = 0
    nEmpresaPaisID = 0
    nPessoaID = 0
    nParceiroID = 0
    nUserID = 0
    nTipo = 0
    
    For i = 1 To Request.QueryString("nEmpresaID").Count    
        nEmpresaID = Request.QueryString("nEmpresaID")(i)
    Next

    For i = 1 To Request.QueryString("nEmpresaPaisID").Count    
        nEmpresaPaisID = Request.QueryString("nEmpresaPaisID")(i)
    Next

    For i = 1 To Request.QueryString("nPessoaID").Count    
        nPessoaID = Request.QueryString("nPessoaID")(i)
    Next

    For i = 1 To Request.QueryString("nParceiroID").Count    
        nParceiroID = Request.QueryString("nParceiroID")(i)
    Next

    For i = 1 To Request.QueryString("nUserID").Count    
        nUserID = Request.QueryString("nUserID")(i)
    Next
    
    For i = 1 To Request.QueryString("nTipo").Count    
        nTipo = Request.QueryString("nTipo")(i)
    Next

	Set rsData = Server.CreateObject("ADODB.Recordset")

	Dim rsSPCommand
	Set rsSPCommand = Server.CreateObject("ADODB.Command")

	Set rsSPCommand = Server.CreateObject("ADODB.Command")
		
	With rsSPCommand
		.CommandTimeout = 60 * 10
		.ActiveConnection = strConn
		.CommandText = "sp_CarrinhosCompras_Combos"
		.CommandType = adCmdStoredProc
		    
		.Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
		.Parameters("@EmpresaID").Value = CLng(nEmpresaID)

		.Parameters.Append( .CreateParameter("@VendedorID", adInteger, adParamInput) )
		.Parameters("@VendedorID").Value = CLng(nUserID)
	
		.Parameters.Append( .CreateParameter("@PessoaID", adInteger, adParamInput) )

		If (nPessoaID = 0) Then
			.Parameters("@PessoaID").Value = Null
		Else
			.Parameters("@PessoaID").Value = CLng(nPessoaID)
		End If

		.Parameters.Append( .CreateParameter("@ParceiroID", adInteger, adParamInput) )

		If (nParceiroID = 0) Then
			.Parameters("@ParceiroID").Value = Null
		Else
			.Parameters("@ParceiroID").Value = CLng(nParceiroID)
		End If

		.Parameters.Append( .CreateParameter("@IdiomaID", adInteger, adParamInput) )
		.Parameters("@IdiomaID").Value = Null

		.Parameters.Append( .CreateParameter("@Tipo", adInteger, adParamInput) )
		.Parameters("@Tipo").Value = CLng(nTipo)

		Set rsData = .Execute
		
	End With
		
	Set rsSPCommand = Nothing
    
    rsData.Save Response, adPersistXML

    rsData.Close
    Set rsData = Nothing
%>
