
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0

    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%

'-- FormID, ContextoID, EmpresaID, UsuarioID
'EXEC sp_Form_Proprietarios 1110, 1111, 2, 1004
'@FormID NUMERIC(10), 
'@ContextoID NUMERIC(10), 
'@EmpresaID NUMERIC(10), 
'@UsuarioID NUMERIC(10)

Dim i
Dim nFormID, nContextoID, nEmpresaID, nUsuarioID, bAllList
Dim rsData, rsSPCommand
    
i = 0

nFormID = 0
nContextoID = 0
nEmpresaID = 0
nUsuarioID = 0
bAllList = 0
    
For i = 1 To Request.QueryString("nFormID").Count    
  nFormID = Request.QueryString("nFormID")(i)
Next

For i = 1 To Request.QueryString("nContextoID").Count    
  nContextoID = Request.QueryString("nContextoID")(i)
Next
    
For i = 1 To Request.QueryString("nEmpresaID").Count    
  nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
  nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next
    
For i = 1 To Request.QueryString("bAllList").Count    
  bAllList = Request.QueryString("bAllList")(i)
Next    
    
Set rsData = Server.CreateObject("ADODB.Recordset")

Set rsSPCommand = Server.CreateObject("ADODB.Command")

With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandText = "sp_Form_Proprietarios"
    .CommandType = adCmdStoredProc

    .Parameters.Append( .CreateParameter("@FormID", adInteger, adParamInput) )
	.Parameters("@FormID").Value = CLng(nFormID)
	.Parameters.Append( .CreateParameter("@ContextoID", adInteger, adParamInput) )
	.Parameters("@ContextoID").Value = CLng(nContextoID)
	.Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
	.Parameters("@EmpresaID").Value = CLng(nEmpresaID)
	.Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
	.Parameters("@UsuarioID").Value = CLng(nUsuarioID)
	.Parameters.Append( .CreateParameter("@Proprietarios", adBoolean, adParamInput) )
	.Parameters("@Proprietarios").Value = CBool(bAllList)
    Set rsData = .Execute
End With

rsData.Save Response, adPersistXML

rsData.Close
Set rsData = Nothing

Set rsSPCommand = Nothing

%>