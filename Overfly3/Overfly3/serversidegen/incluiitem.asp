
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew
Dim nDocumentoID, nRegistroID, nDocumentoOrigemID, nRegistroOrigemID
Dim strSQL
Dim nRecsAffected
Dim rsCommand
Dim i
  
strSQL = ""
nDocumentoID = 0
nRegistroID = 0
nDocumentoOrigemID = 0
nRegistroOrigemID = 0

Set rsCommand = Server.CreateObject("ADODB.Command")

For i = 1 To Request.QueryString("nDocumentoID").Count    
    nDocumentoID = Request.QueryString("nDocumentoID")(i)
Next

For i = 1 To Request.QueryString("nRegistroID").Count
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

For i = 1 To Request.QueryString("nDocumentoOrigemID").Count
    nDocumentoOrigemID = Request.QueryString("nDocumentoOrigemID")(i)
Next

For i = 1 To Request.QueryString("nRegistroOrigemID").Count
    nRegistroOrigemID = Request.QueryString("nRegistroOrigemID")(i)
Next

strSQL = "INSERT INTO documentosRelacionados (DocumentoID, RegistroID, DocumentoOrigemID, RegistroOrigemID) " & _
         "VALUES (" & CStr(nDocumentoID) & ", " & CStr(nRegistroID) & ", " & CStr(nDocumentoOrigemID) & ", " & _
         CStr(nRegistroOrigemID) & ")"

If (strSQL <> "") Then
	rsCommand.CommandTimeout = 60 * 10
    rsCommand.ActiveConnection = strConn
    rsCommand.CommandText = strSQL
    rsCommand.CommandType = adCmdText
    rsCommand.Execute nRecsAffected
End If

Set rsCommand = Nothing

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "fldresp", adVarChar, 50, adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew
rsNew.Fields("fldresp").Value = CStr(nRecsAffected)
rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
%>

