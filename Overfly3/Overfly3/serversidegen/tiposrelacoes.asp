
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
   
	'Captura o ID do form passado por par�metro
	Dim i, formID
	
	formID = ""
	For i = 1 To Request.QueryString("formID").Count    
	  formID = Request.QueryString("formID")(i)
	Next

    Dim rsData, strSQL

    Set rsData = Server.CreateObject("ADODB.Recordset")
    
    'Form de Recursos
    If ( formID = 1110 ) Then
    
        strSQL = "SELECT 'SUJEITO' AS TIPO,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " & _
                 "a.Ordem,a.EhDefault,a.ObjetoOutros AS NOMEPASTA,a.Sujeito AS HEADER,b.TipoObjetoID AS TIPOPOSSIVEL " & _ 
                 "FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                 "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " & _
                 "AND (a.RelacaoEntreID =  131 OR a.RelacaoEntreID =  132) " & _
                 "UNION ALL (SELECT 'OBJETO' AS TIPO,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " & _
                 "a.Ordem,a.EhDefault,a.SujeitoOutros AS NOMEPASTA,a.Objeto AS HEADER,b.TipoSujeitoID AS TIPOPOSSIVEL " & _
                 "FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                 "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " & _
                 "AND (a.RelacaoEntreID =  131 OR a.RelacaoEntreID =  132)) " & _
                 "ORDER BY NomePasta"

    'Form de Pessoas
    ElseIf ( formID = 1210 ) Then
    
        strSQL = "SELECT 'SUJEITO' AS TIPO,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " & _
                 "a.Ordem,a.EhDefault,a.ObjetoOutros AS NOMEPASTA,a.Sujeito AS HEADER,b.TipoObjetoID AS TIPOPOSSIVEL " & _ 
                 "FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                 "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " & _
                 "AND (a.RelacaoEntreID =  132 OR a.RelacaoEntreID =  133 " & _
                 "OR a.RelacaoEntreID =  135) " & _
                 "UNION ALL (SELECT 'OBJETO' AS TIPO,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " & _
                 "a.Ordem,a.EhDefault,a.SujeitoOutros AS NOMEPASTA,a.Objeto AS HEADER,b.TipoSujeitoID AS TIPOPOSSIVEL " & _
                 "FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                 "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " & _
                 "AND (a.RelacaoEntreID =  132 OR a.RelacaoEntreID =  133 " & _
                 "OR a.RelacaoEntreID =  135)) " & _
                 "ORDER BY NomePasta"

    'Form de Conceitos
    ElseIf ( formID = 2110 ) Then
    
        strSQL = "SELECT 'SUJEITO' AS TIPO,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " & _
                 "a.Ordem,a.EhDefault,a.ObjetoOutros AS NOMEPASTA,a.Sujeito AS HEADER,b.TipoObjetoID AS TIPOPOSSIVEL " & _ 
                 "FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                 "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " & _
                 "AND (a.RelacaoEntreID =  134 OR a.RelacaoEntreID =  135) " & _
                 "UNION ALL (SELECT 'OBJETO' AS TIPO,a.TipoRelacaoID,a.TipoRelacao,a.RelacaoEntreID, " & _
                 "a.Ordem,a.EhDefault,a.SujeitoOutros AS NOMEPASTA,a.Objeto AS HEADER,b.TipoSujeitoID AS TIPOPOSSIVEL " & _
                 "FROM TiposRelacoes a WITH(NOLOCK), TiposRelacoes_Itens b WITH(NOLOCK) " & _
                 "WHERE a.TipoRelacaoID=b.TipoRelacaoID AND a.EstadoID = 2 " & _
                 "AND (a.RelacaoEntreID =  134 OR a.RelacaoEntreID =  135)) " & _
                 "ORDER BY NomePasta"

    End If
                      
    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    rsData.Save Response, adPersistXML

    rsData.Close
    Set rsData = Nothing
%>
