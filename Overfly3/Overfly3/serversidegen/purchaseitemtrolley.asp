
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%

	On Error Resume Next

    Dim i, nUserID, nEmpresaID, nPessoaID, nParceiroID, nMoedaID, nFinanciamentoID, nFrete, sMeioTransporteID
    Dim nDataLen, sResultado
	Dim nProdutoID, nQuantidade, nPrecoUnitario, nPrecoFaturamento, sCFOPID, sFinalidade

    i = 0
    nUserID = 0
    nEmpresaID = 0
    nPessoaID = 0
    nParceiroID = 0
    nMoedaID = 0
    nFinanciamentoID = 0
    nFrete = 0
    sMeioTransporteID = 0
    nDataLen = 0
    sResultado = ""
    sCFOPID =""
    sFinalidade = ""
    
	For i = 1 To Request.QueryString("nUserID").Count    
	  nUserID = Request.QueryString("nUserID")(i)
	Next

	For i = 1 To Request.QueryString("nPessoaID").Count
	  nPessoaID = Request.QueryString("nPessoaID")(i)
	Next

	For i = 1 To Request.QueryString("nParceiroID").Count
	  nParceiroID = Request.QueryString("nParceiroID")(i)
	Next

	For i = 1 To Request.QueryString("nMoedaID").Count
	  nMoedaID = Request.QueryString("nMoedaID")(i)
	Next

	For i = 1 To Request.QueryString("nFinanciamentoID").Count
	  nFinanciamentoID = Request.QueryString("nFinanciamentoID")(i)
	Next

	For i = 1 To Request.QueryString("nFrete").Count
	  nFrete = Request.QueryString("nFrete")(i)
	Next

	For i = 1 To Request.QueryString("sMeioTransporteID").Count
	  sMeioTransporteID = Request.QueryString("sMeioTransporteID")(i)
	Next

	For i = 1 To Request.QueryString("nDataLen").Count
		nDataLen = Request.QueryString("nDataLen")(i)
	Next

	ReDim nEmpresaID(CInt(nDataLen) - 1)
	For i = 0 To (CInt(nDataLen) - 1)
	  nEmpresaID(i) = Request.QueryString("nEmpresaID" & CStr(i+1))
	Next

	ReDim nProdutoID(CInt(nDataLen) - 1)
    For i = 0 To (CInt(nDataLen) - 1)
        nProdutoID(i) = Request.QueryString("nProdutoID" & CStr(i+1))
    Next

	ReDim nQuantidade(CInt(nDataLen) - 1)
    For i = 0 To (CInt(nDataLen) - 1)
        nQuantidade(i) = Request.QueryString("nQuantidade" & CStr(i+1))
    Next
	
	ReDim nPrecoUnitario(CInt(nDataLen) - 1)
    For i = 0 To (CInt(nDataLen) - 1)
        nPrecoUnitario(i) = Request.QueryString("nPrecoUnitario" & CStr(i+1))
    Next

	ReDim nPrecoFaturamento(CInt(nDataLen) - 1)
    For i = 0 To (CInt(nDataLen) - 1)
        nPrecoFaturamento(i) = Request.QueryString("nPrecoFaturamento" & CStr(i+1))
    Next
    
	ReDim sCFOPID(CInt(nDataLen) - 1)
    For i = 0 To (CInt(nDataLen) - 1)
        sCFOPID(i) = Request.QueryString("sCFOPID" & CStr(i+1))
        
        If (CStr(sCFOPID(i)) = "0") Then
            sCFOPID(i) = "NULL"
        End If
    Next
    
	ReDim sFinalidade(CInt(nDataLen) - 1)
    For i = 0 To (CInt(nDataLen) - 1)
        sFinalidade(i) = Request.QueryString("sFinalidade" & CStr(i+1))
        
        If (CStr(sFinalidade(i)) = "0") Then
            sFinalidade(i) = "NULL"
        End If
    Next

	Dim rsSPCommand
	'Set rsSPCommand = Server.CreateObject("ADODB.Command")

	For i = 0 To (CInt(nDataLen) - 1)
		Set rsSPCommand = Server.CreateObject("ADODB.Command")
		
		With rsSPCommand
			.CommandTimeout = 60 * 10
		    .ActiveConnection = strConn
		    .CommandText = "sp_ListaPrecos_Comprar"
		    .CommandType = adCmdStoredProc
		      
		    .Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
		    .Parameters("@EmpresaID").Value = CLng(nEmpresaID(i))

		    .Parameters.Append( .CreateParameter("@PessoaID", adInteger, adParamInput) )
		    .Parameters("@PessoaID").Value = CLng(nPessoaID)

		    .Parameters.Append( .CreateParameter("@ParceiroID", adInteger, adParamInput) )
		    .Parameters("@ParceiroID").Value = CLng(nParceiroID)

		    .Parameters.Append( .CreateParameter("@ProdutoID", adInteger, adParamInput) )
		    .Parameters("@ProdutoID").Value = CLng(nProdutoID(i))

		    .Parameters.Append( .CreateParameter("@Quantidade", adInteger, adParamInput) )
		    .Parameters("@Quantidade").Value = CLng(nQuantidade(i))
		    
		    .Parameters.Append( .CreateParameter("@MoedaID", adInteger, adParamInput) )
		    .Parameters("@MoedaID").Value = CLng(nMoedaID)

		    .Parameters.Append( .CreateParameter("@PrecoUnitario", adCurrency, adParamInput) )

			If (CCur(nPrecoUnitario(i)) = 0) Then
				.Parameters("@PrecoUnitario").Value = Null
			Else
				.Parameters("@PrecoUnitario").Value = CCur(nPrecoUnitario(i))
			End If

		    .Parameters.Append( .CreateParameter("@PrecoFaturamento", adCurrency, adParamInput) )

			If (CCur(nPrecoFaturamento(i)) = 0) Then
				.Parameters("@PrecoFaturamento").Value = Null
			Else
				.Parameters("@PrecoFaturamento").Value = CCur(nPrecoFaturamento(i))
			End If

		    .Parameters.Append( .CreateParameter("@FinalidadeID", adInteger, adParamInput) )
			If (CStr(sFinalidade(i)) = "NULL") Then
				.Parameters("@FinalidadeID").Value = Null
			Else
				.Parameters("@FinalidadeID").Value = CLng(sFinalidade(i))
			End If

		    .Parameters.Append( .CreateParameter("@CFOPID", adInteger, adParamInput) )
			If (CStr(sCFOPID(i)) = "NULL") Then
				.Parameters("@CFOPID").Value = Null
			Else
				.Parameters("@CFOPID").Value = CLng(sCFOPID(i))
			End If

		    .Parameters.Append( .CreateParameter("@FinanciamentoID", adInteger, adParamInput) )
		    .Parameters("@FinanciamentoID").Value = CLng(nFinanciamentoID)

		    .Parameters.Append( .CreateParameter("@IncluiFrete", adInteger, adParamInput) )
		    .Parameters("@IncluiFrete").Value = CLng(nFrete)

		    .Parameters.Append( .CreateParameter("@MeioTransporteID", adInteger, adParamInput) )

			If (CLng(sMeioTransporteID) = 0) Then
				.Parameters("@MeioTransporteID").Value = Null
			Else
				.Parameters("@PrecoFaturamento").Value = CLng(sMeioTransporteID)
			End If

		    .Parameters.Append( .CreateParameter("@ColaboradorID", adInteger, adParamInput) )
		    .Parameters("@ColaboradorID").Value = CLng(nUserID)

		    .Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
		    .Parameters("@UsuarioID").Value = CLng(nUserID)

		    .Parameters.Append( .CreateParameter("@Web", adBoolean, adParamInput) )
		    .Parameters("@Web").Value = FALSE

		    .Parameters.Append( .CreateParameter("@Resultado", adVarChar, adParamOutput, 100) )
		    .Execute
			
			If ( NOT IsNull(rsSPCommand.Parameters("@Resultado").Value) ) Then
				sResultado = sResultado & rsSPCommand.Parameters("@Resultado").Value
			
				If (rsSPCommand.Parameters("@Resultado").Value <> "") Then
					sResultado = sResultado & Chr(13) & Chr(10)
				End If
			End If
			
		End With
		
		Set rsSPCommand = Nothing
	Next
	
	Dim rsNew
	Set rsNew = Server.CreateObject("ADODB.Recordset")
	rsNew.CursorLocation = adUseServer

	rsNew.Fields.Append "Mensagem", adVarchar, 8000, adFldMayBeNull OR adFldUpdatable
	  
	rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

	rsNew.AddNew
    rsNew.Fields("Mensagem").Value = sResultado

	rsNew.Update

	rsNew.Save Response, adPersistXML

	rsNew.Close
	Set rsNew = Nothing
%>
