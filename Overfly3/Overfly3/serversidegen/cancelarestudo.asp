
<%@ LANGUAGE=VBSCRIPT %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'********************************************************************
'Retorna -> a definir
'Provisoriamente retorna true ou false
'********************************************************************
Dim i, nDocumentoID, nUsuarioID, sResultado
Dim rsNew, rsSPCommand
    
nDocumentoID = 0
nUsuarioID = 0
sResultado = Null

For i = 1 To Request.QueryString("nDocumentoID").Count    
    nDocumentoID = Request.QueryString("nDocumentoID")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

Set rsSPCommand = Server.CreateObject("ADODB.Command")

With rsSPCommand
	.CommandTimeout = 60 * 10
	.ActiveConnection = strConn

	.CommandText = "sp_Documentos_CancelarEstudo"
	.CommandType = adCmdStoredProc

	.Parameters.Append( .CreateParameter("@DocumentoID", adInteger, adParamInput) )
	.Parameters("@DocumentoID").Value = nDocumentoID

	.Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
	.Parameters("@UsuarioID").Value = nUsuarioID

	.Parameters.Append( .CreateParameter("@Resultado", adVarChar, adParamOutput, 8000) )

	.Execute
End With

sResultado = rsSPCommand.Parameters("@Resultado").Value

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer

'Cria tabela que vai voltar para o usuario em forma de xml
rsNew.Fields.Append "Resultado", adVarchar, 200, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew

If (NOT IsNull(sResultado)) Then
	rsNew.Fields("Resultado").Value = sResultado
Else
    rsNew.Fields("Resultado").Value = ""
End If

rsNew.Update
rsNew.Save Response, adPersistXML    
rsNew.Close
Set rsNew = Nothing

%>

