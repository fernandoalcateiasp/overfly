
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Dim i, nClienteID, nEmpresaID
nClienteID = 0
nEmpresaID = 0
	    
For i = 1 To Request.QueryString("nClienteID").Count    
  nClienteID = Request.QueryString("nClienteID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count    
  nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next
  
Dim rsData, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")
      
    strSQL = "SELECT TOP 1 Pessoas.PessoaID, Pessoas.Fantasia, Enderecos.UFID, 1 AS Parceiro, " & _
	    "dbo.fn_Empresa_Sistema(Pessoas.PessoaID) AS EmpresaSistema, " & _
		"RelPessoas.ListaPreco AS ListaPreco, dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL) AS PMP, " & _
		"c.NumeroParcelas AS NumeroParcelas, " & _
		"dbo.fn_RelacoesPessoas_FinanciamentoPadrao(Pessoas.PessoaID, RelPessoas.ObjetoID, 1) AS FinanciamentoPadrao, " & _
		"ISNULL(dbo.fn_RelPessoa_Classificacao(" & CStr(nEmpresaID) & ", " & CStr(nClienteID) & ", 0, GETDATE(),1), SPACE(0)) AS ClassificacaoInterna," & _
		"ISNULL(dbo.fn_RelPessoa_Classificacao(" & CStr(nEmpresaID) & ", " & CStr(nClienteID) & ", 1, GETDATE(),1), SPACE(0)) AS ClassificacaoExterna " & _
	    "FROM Pessoas Empresas WITH(NOLOCK) " & _
	    "INNER JOIN RelacoesPesRec EmpresasSistema WITH(NOLOCK) ON (Empresas.PessoaID = EmpresasSistema.SujeitoID) " & _
	    "INNER JOIN RelacoesPessoas RelPessoas WITH(NOLOCK) ON (RelPessoas.ObjetoID = Empresas.PessoaID) " & _
	    "INNER JOIN Pessoas WITH(NOLOCK) ON (Pessoas.PessoaID = RelPessoas.SujeitoID) " & _
        "INNER JOIN Pessoas_Creditos PesCredito WITH(NOLOCK) ON (PesCredito.PessoaID = Pessoas.SujeitoID) " & _
	    "LEFT OUTER JOIN Pessoas_Enderecos Enderecos WITH(NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID AND Enderecos.EndFaturamento = 1 AND Enderecos.Ordem = 1) " & _
	    "INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON (ISNULL(PesCredito.FinanciamentoLimiteID, 2) = c.FinanciamentoID) " & _
	    "WHERE (Empresas.PessoaID = " & CStr(nEmpresaID) & " AND " & _
            "PesCredito.PaisID = Enderecos.PaisID AND " & _
    		"EmpresasSistema.TipoRelacaoID = 12 AND EmpresasSistema.ObjetoID = 999 AND " & _
			"Pessoas.PessoaID = " & CStr(nClienteID) & " AND Pessoas.EstadoID = 2 AND " & _
			"RelPessoas.EstadoID = 2 AND RelPessoas.TipoRelacaoID = 21)"

rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

rsData.Save Response,adPersistXML
rsData.Close
Set rsData = Nothing
%>
