
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	Dim i
    Dim nCarrinhoID
    Dim nPedidoID, sResultado
    
    i = 0
    nCarrinhoID = 0
	nPedidoID = 0
    sResultado = ""
    
	For i = 1 To Request.QueryString("nCarrinhoID").Count    
	  nCarrinhoID = Request.QueryString("nCarrinhoID")(i)
	Next

	Dim rsSPCommand
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
	  
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_Pedido_Gerador"
	    .CommandType = adCmdStoredProc

	    .Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput) )
	    .Parameters("@EmpresaID").Value = CLng(-nCarrinhoID)
	    
		.Parameters.Append( .CreateParameter("@PessoaID", adInteger, adParamInput) )
		.Parameters("@PessoaID").Value = Null
	    
	    .Parameters.Append( .CreateParameter("@ParceiroID", adInteger, adParamInput) )
	    .Parameters("@ParceiroID").Value = Null

	    .Parameters.Append( .CreateParameter("@EhCliente", adInteger, adParamInput) )
	    .Parameters("@EhCliente").Value = Null
	    
	    .Parameters.Append( .CreateParameter("@TransacaoID", adInteger, adParamInput) )
	    .Parameters("@TransacaoID").Value = Null

	    .Parameters.Append( .CreateParameter("@Observacao", adVarChar, adParamInput, 40) )
	    .Parameters("@Observacao").Value = Null

	    .Parameters.Append( .CreateParameter("@SeuPedido", adVarChar, adParamInput, 30) )
	    .Parameters("@SeuPedido").Value = Null

	    .Parameters.Append( .CreateParameter("@OrigemPedidoID", adInteger, adParamInput) )
	    .Parameters("@OrigemPedidoID").Value = 605

	    .Parameters.Append( .CreateParameter("@FinanciamentoID", adInteger, adParamInput) )
	    .Parameters("@FinanciamentoID").Value = Null

	    .Parameters.Append( .CreateParameter("@dtPrevisaoEntrega", adDate, adParamInput) )
	    .Parameters("@dtPrevisaoEntrega").Value = Null

	    .Parameters.Append( .CreateParameter("@TransportadoraID", adInteger, adParamInput) )
	    .Parameters("@TransportadoraID").Value = Null

	    .Parameters.Append( .CreateParameter("@MeioTransporteID", adInteger, adParamInput) )
	    .Parameters("@MeioTransporteID").Value = Null

	    .Parameters.Append( .CreateParameter("@ModalidadeTransporteID", adInteger, adParamInput) )
	    .Parameters("@ModalidadeTransporteID").Value = Null

	    .Parameters.Append( .CreateParameter("@Frete", adBoolean, adParamInput) )
	    .Parameters("@Frete").Value = Null
	    
		.Parameters.Append( .CreateParameter("@DepositoID", adInteger, adParamInput) )
		.Parameters("@DepositoID").Value = Null

	    .Parameters.Append( .CreateParameter("@ProprietarioID", adInteger, adParamInput) )
	    .Parameters("@ProprietarioID").Value = Null

	    .Parameters.Append( .CreateParameter("@AlternativoID", adInteger, adParamInput) )
	    .Parameters("@AlternativoID").Value = Null

	    .Parameters.Append( .CreateParameter("@Observacoes", adVarChar, adParamInput, 8000) )
	    .Parameters("@Observacoes").Value = null

        .Parameters.Append( .CreateParameter("@NumProjeto", adVarChar, adParamInput, 20) )
	    .Parameters("@NumProjeto").Value = null
        
        .Parameters.Append( .CreateParameter("@ProgramaMarketingID", adInteger, adParamInput) )
	    .Parameters("@ProgramaMarketingID").Value = null

	    .Parameters.Append( .CreateParameter("@URLEtiqueta", adVarChar, adParamInput, 256) )
	    .Parameters("@URLEtiqueta").Value = null    

	    .Parameters.Append( .CreateParameter("@PedidoID", adInteger, adParamOutput) )
	    
	    .Parameters.Append( .CreateParameter("@Resultado", adVarchar, adParamOutput, 8000) )

	    .Execute
	    
	End With

    nPedidoID = 0
    
	If ( Not IsNull(rsSPCommand.Parameters("@PedidoID").Value) ) Then
	    nPedidoID = CLng(rsSPCommand.Parameters("@PedidoID").Value)
	End If
	
	sResultado = rsSPCommand.Parameters("@Resultado").Value
	
    Dim rsSPCommand1

	If ( IsNull(sResultado) ) Then

	    Set rsSPCommand1 = Server.CreateObject("ADODB.Command")

	    With rsSPCommand1
			.CommandTimeout = 60 * 10
	        .ActiveConnection = strConn
	        .CommandText = "sp_PedidoItemCarrinho_Gerador"
	        .CommandType = adCmdStoredProc
	          
	        .Parameters.Append( .CreateParameter("@PedidoID", adInteger, adParamInput) )
	        .Parameters("@PedidoID").Value = CLng(nPedidoID)

            .Execute
	    End With

	End If

	Dim rsNew
	Set rsNew = Server.CreateObject("ADODB.Recordset")
	rsNew.CursorLocation = adUseServer

	rsNew.Fields.Append "PedidoID", adDecimal, 10, adFldMayBeNull OR adFldUpdatable
	rsNew.Fields.Append "Resultado", adVarchar, 8000, adFldMayBeNull OR adFldUpdatable
	  
	rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

	rsNew.AddNew
    rsNew.Fields("PedidoID").Value = nPedidoID
    rsNew.Fields("Resultado").Value = sResultado

	rsNew.Update

	rsNew.Save Response, adPersistXML

	Set rsSPCommand = Nothing
	Set rsSPCommand1 = Nothing

	rsNew.Close
	Set rsNew = Nothing

%>