
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    
Dim i, nFormID, nEmpresaID, nEmpresaPaisID, nUserID, nIdiomaSistemaID, nIdiomaEmpresaID

nFormID = 0
nEmpresaID = 0
nEmpresaPaisID = 0
nUserID = 0
nIdiomaSistemaID = 0
nIdiomaEmpresaID = 0
      
For i = 1 To Request.QueryString("nFormID").Count    
    nFormID = Request.QueryString("nFormID")(i)
Next      

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nEmpresaPaisID").Count    
    nEmpresaPaisID = Request.QueryString("nEmpresaPaisID")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next      
      
For i = 1 To Request.QueryString("nIdiomaSistemaID").Count    
    nIdiomaSistemaID = Request.QueryString("nIdiomaSistemaID")(i)
Next      

For i = 1 To Request.QueryString("nIdiomaEmpresaID").Count    
    nIdiomaEmpresaID = Request.QueryString("nIdiomaEmpresaID")(i)
Next      

Dim rsData, strSQL, rsSPCommand
   
'--------------------------- As strings de pesquisa -- INICIO ------------------------

'//@@
Select Case CLng(nFormID)

    Case 1110   'Form de Recursos
              
        strSQL =    "SELECT '1' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName, ItemID AS fldId, Filtro AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=1 " & _
                    "UNION ALL (SELECT '2' AS Indice, SimboloMoeda AS Ordem2, SimboloMoeda AS fldName,ConceitoID AS fldId, ' ' AS Filtro " & _
                    " FROM Conceitos WITH(NOLOCK) WHERE EstadoID=2 AND TipoConceitoID=308 AND SimboloMoeda is not null) " & _
                    "UNION ALL (SELECT '3' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID,' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=48) " & _
                    "UNION ALL (SELECT '4' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=2) " & _
                    "UNION ALL (SELECT '5' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=3) " & _
                    "UNION ALL (SELECT '6' AS Indice,STR(Ordem) AS Ordem2,ItemAbreviado AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=4) " & _
                    "UNION ALL (SELECT '7' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=5) " & _
                    "UNION ALL (SELECT '8' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=11) " & _
                    "UNION ALL (SELECT '9' AS Indice,SPACE(0) AS Ordem2,SPACE(0) AS fldName,0 AS fldId, ' ' AS Filtro) " & _
                    "UNION ALL (SELECT '9' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=9) " & _
                    "ORDER BY Indice,Ordem2"

    Case 1120   'Form de Rela��es entre Recursos

        strSQL =   "SELECT '1' as Indice, TipoRelacaoID as fldID, TipoRelacao as fldName, TipoRelacao AS Ordem2,Sujeito AS NameSuj,Objeto AS NameObj, Filtro AS Filtro " & _
                   "FROM TiposRelacoes WITH(NOLOCK) WHERE EstadoID=2 AND RelacaoEntreID=131 " & _
                   "UNION ALL SELECT '2' AS Indice, 0 AS fldID, SPACE(0) AS fldName, SPACE(0) AS Ordem2,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " & _
                   "UNION ALL SELECT '2' AS Indice,RecursoID AS fldID, RecursoFantasia AS fldName, RecursoFantasia AS Ordem2,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " & _
                   " FROM Recursos WITH(NOLOCK) WHERE EstadoID=2 AND TipoRecursoID = 8 " & _
                   "ORDER BY Indice,Ordem2"

    Case 1210   'Form de Pessoas
    
        strSQL =    "SELECT '1' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, Filtro AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID = 12 AND EstadoID=2 " & _
                    "UNION ALL (SELECT '21' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 13 AND Filtro LIKE '%(51)%' " & _
                    "AND (dbo.fn_Direitos_TiposAuxiliares(ItemID, " & nEmpresaID & ", " & nUserID & ", GETDATE()) > 0) ) " & _
                    "UNION ALL (SELECT '22' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 13 AND Filtro LIKE '%(52)%' " & _
                    "AND (dbo.fn_Direitos_TiposAuxiliares(ItemID, " & nEmpresaID & ", " & nUserID & ", GETDATE()) > 0) ) " & _
                    "UNION ALL (SELECT '3' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 14) " & _
                    "UNION ALL (SELECT '4' AS Indice,STR(CompetenciaID) AS Ordem2,Competencia AS fldName,CompetenciaID AS fldId, ' ' AS Filtro " & _
                    " FROM Competencias WITH(NOLOCK) WHERE TipoCompetenciaID = 1701 AND EstadoID=2) " & _
                    "UNION ALL (SELECT '5' AS Indice, Localidade AS Ordem2,Localidade AS fldName,LocalidadeID AS fldId, ' ' AS Filtro " & _ 
                    " FROM Localidades WITH(NOLOCK) WHERE TipoLocalidadeID = 203 AND (EstadoID = 2 OR EstadoID = 3)) " & _
                    "UNION ALL (SELECT '6' AS Indice, Codigo AS Ordem2,Codigo AS fldName,BancoID AS fldId, ' ' AS Filtro " & _ 
                    " FROM Bancos WITH(NOLOCK) WHERE EstadoID = 2) " & _
                    "UNION ALL (SELECT '7' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID = 10) " & _
                    "ORDER BY Indice, Ordem2 "

    Case 1130   'Form de Rela��es Pessoas e Recursos

        strSQL =   "SELECT '1' AS Indice, TipoRelacao AS Ordem2, 0 AS Ordem, TipoRelacao AS fldName, TipoRelacaoID AS fldID,Sujeito AS NameSuj,Objeto AS NameObj, Filtro AS Filtro " & _
                   " FROM TiposRelacoes WITH(NOLOCK) WHERE RelacaoEntreID=132 AND EstadoID = 2 " & _
                   "UNION ALL (SELECT '2' AS Indice, STR(Ordem) AS Ordem2, 0 AS Ordem, ItemMasculino AS fldName,ItemID as fldID,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " & _
                    " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID=34) " & _
                   "UNION ALL (SELECT '3' as Indice, RecursoFantasia AS Ordem2, 0 AS Ordem, RecursoFantasia AS fldName,RecursoID as fldID,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " & _
                    " FROM Recursos WITH(NOLOCK) WHERE TipoRecursoID=1) " & _
                   "UNION ALL " & _
                   "SELECT '4' AS Indice, SPACE(0) AS Ordem2, 0 AS Ordem, SPACE(0) AS FldName, 0 AS fldID, '-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " & _
                   "UNION ALL (SELECT DISTINCT '4' as Indice, SPACE(0) AS Ordem2, NivelInspecao AS Ordem, CONVERT(VARCHAR(1),NivelInspecao) AS fldName, NivelInspecao as fldID,'-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " & _
                   "FROM Documentos_CodigosAmostra WITH(NOLOCK) WHERE DocumentoID=743) " & _
                   "UNION ALL " & _
                   "SELECT '5' AS Indice, SPACE(0) AS Ordem2, a.PessoaID AS Ordem, a.Fantasia AS FldName, a.PessoaID AS fldID, '-1' as NameSuj,'-2' as NameObj, ' ' AS Filtro " & _
                   "FROM Pessoas a WITH(NOLOCK), RelacoesPesRec b WITH(NOLOCK) " & _
                   "WHERE a.PessoaID = b.SujeitoID AND b.TipoRelacaoID=12 AND b.ObjetoID=999 AND b.EstadoID=2 " & _
                   "ORDER BY Indice, Ordem2, Ordem"
                   
    Case 1220   'Form de Relacoes Entre Pessoas

        strSQL =   "SELECT '1' AS Indice, TipoRelacao AS Ordem2, TipoRelacao AS fldName, '-1' AS fldNameAlt, TipoRelacaoID AS fldID,Sujeito AS NameSuj,Objeto AS NameObj, Filtro AS Filtro " & _
                   " FROM TiposRelacoes WITH(NOLOCK) WHERE RelacaoEntreID=133 AND EstadoID = 2 " & _
                   "UNION ALL (SELECT '2' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=13 and Filtro Like '%{1221}%')) " & _
                   "UNION ALL (SELECT '3' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=28)) " & _
                   "UNION ALL (SELECT '4' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=29)) " & _
                   "UNION ALL (SELECT '5' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE(EstadoID=2 and TipoID=30)) " & _
                   "UNION ALL (SELECT '7' AS Indice, STR(0) AS Ordem2, SPACE(0) AS fldName, '-1' AS fldNameAlt, 0 AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro) " & _
                   "UNION ALL (SELECT '7' AS Indice, STR(Ordem) AS Ordem2, ItemAbreviado AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE (EstadoID=2 AND TipoID=804 AND Filtro LIKE '%(1221)%')) " & _
                   "UNION ALL (SELECT '9' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemFeminino AS fldNameAlt , ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=31) " & _
                   "UNION ALL (SELECT '10' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, '-1' AS fldNameAlt, ItemID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID=2 AND TipoID=33) " & _
                   "UNION ALL (SELECT '11' AS Indice, RecursoFantasia AS Ordem2, RecursoFantasia AS fldName, '-1' AS fldNameAlt, RecursoID AS fldID,'-1' AS NameSuj,'-2' AS NameObj, ' ' AS Filtro " & _
                   " FROM Recursos WITH(NOLOCK) WHERE (EstadoID BETWEEN 1 AND 2) AND TipoRecursoID=6 AND EhCargo=1) " & _
                   "ORDER BY Indice, Ordem2"

    Case 1230   'Form prospect
    
        strSQL =   "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND ItemID IN(51,52)  " & _
                   "UNION ALL " & _
                   "SELECT '2' AS Indice, Localidade AS Ordem2, Localidade AS fldName, LocalidadeID AS fldID " & _
                   "FROM Localidades WITH(NOLOCK) WHERE EstadoID = 2 AND TipoLocalidadeID=203 " & _
                   "ORDER BY Indice, Ordem2"

    Case 5240   'Form prospect
    
        strSQL =   "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 37 " & _
                   "UNION ALL " & _
                   "SELECT '2' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 38 " & _
                   "ORDER BY Indice, Ordem2"

    Case 1320   'Form de Tipos de Rela��es
    
        strSQL =   "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 25 " & _
                   "UNION ALL (SELECT '2' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 26) " & _
                   "ORDER BY Indice, Ordem2"
   
    Case 1330   'Form de Localidades

        strSQL =   "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 42 " & _
                   "UNION ALL (SELECT '2' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 43) " & _
                   "UNION ALL (SELECT '3' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 47) " & _
                   "UNION ALL (SELECT '5' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 44) " & _
                   "UNION ALL (SELECT '6' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 45) " & _
                   "ORDER BY Indice, Ordem2"

    Case 1360   'Form de Dicionario

        strSQL =   "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, ' ' AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 48 " & _
                   "ORDER BY Indice, Ordem2"

    Case 2110   'Form de Conceitos

        strSQL =   "SELECT '1' as Indice, ItemID as fldID,ItemMasculino as fldName, STR(Ordem) AS Ordem2, Filtro AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   " WHERE EstadoID=2 AND TipoID=101 " & _
                   "UNION ALL " & _
                   "SELECT '2' as Indice, ItemID as fldID,ItemMasculino as fldName, STR(Ordem) AS Ordem2, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   " WHERE EstadoID=2 AND TipoID=102 " & _
                   "UNION ALL " & _
                   "SELECT '3' as Indice, ConceitoID as fldID, " & _
						"dbo.fn_Tradutor(Conceito, " & nIdiomaSistemaID & ", " & nIdiomaEmpresaID & ", NULL) as fldName, " & _
						"dbo.fn_Tradutor(Conceito, " & nIdiomaSistemaID & ", " & nIdiomaEmpresaID & ", NULL) AS Ordem2, ' ' AS Filtro " & _
                   " FROM Conceitos WITH(NOLOCK) " & _
                   " WHERE EstadoID=2 AND TipoConceitoID=302 " & _
                   "UNION ALL " & _
                   "SELECT '4' as Indice, ItemID as fldID,(ItemAbreviado + CHAR(45) + ItemMasculino) as fldName, STR(Ordem) AS Ordem2, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   " WHERE EstadoID=2 AND TipoID=103 " & _
                   "UNION ALL " & _
                   "SELECT '5' as Indice, LocalidadeID as fldID, Localidade as fldName, Localidade as Ordem2, ' ' AS Filtro " & _
                   " FROM Localidades WITH(NOLOCK) " & _
                   " WHERE EstadoID=2 AND TipoLocalidadeID = 203 " & _
                   "UNION ALL " & _
                   "SELECT '6' AS Indice, ItemID as fldID, ItemMasculino as fldName, STR(Ordem) as Ordem2, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   " WHERE EstadoID=2 AND TipoID=46 " & _
                   "UNION ALL " & _
                   "SELECT '7' AS Indice, ItemID as fldID, ItemMasculino as fldName, STR(Ordem) as Ordem2, ' ' AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   " WHERE EstadoID=2 AND TipoID=104 " & _
                   "UNION ALL " & _
                   "SELECT '8' as Indice, LocalidadeID as fldID, Localidade as fldName, Localidade as Ordem2, ' ' AS Filtro " & _
                   " FROM Localidades WITH(NOLOCK) " & _
                   " WHERE TipoLocalidadeID = 203 " & _
                   "ORDER BY Indice,Ordem2"

    Case 2120   'Form de Rela��es entre Conceitos

        strSQL =   "SELECT '1' as Indice, TipoRelacaoID as fldID, TipoRelacao as fldName, TipoRelacao AS Ordem2,Sujeito AS NameSuj,Objeto AS NameObj, Filtro AS Filtro " & _
                   "FROM TiposRelacoes WITH(NOLOCK) WHERE EstadoID=2 AND RelacaoEntreID=134 " & _
                   "ORDER BY Indice,Ordem2"

    Case 2130   'Form de Rela��es entre Pessoas e Conceitos
    
        strSQL =   "SELECT '1' as Indice, TipoRelacaoID as fldID,TipoRelacao as fldName, TipoRelacao AS Ordem2, SPACE(1) AS Ordem3 ,Sujeito AS NameSuj, Objeto AS NameObj, Filtro AS Filtro, SPACE(0) AS Descricao " & _
                   "FROM TiposRelacoes WITH(NOLOCK) " & _
                   "WHERE EstadoID=2 AND RelacaoEntreID=135 " & _
                   "UNION ALL SELECT '2' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "FROM Conceitos WITH(NOLOCK) " & _
                   "WHERE EstadoID = 2 AND TipoConceitoID = 307 " & _
                   "AND PaisID = " & nEmpresaPaisID & " " & _
                   "UNION ALL SELECT '3' as Indice, c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, c.SimboloMoeda AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                   "WHERE a.SujeitoID = " & nEmpresaID & " " & _
                   "AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 " & _
                   "AND a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID " & _
                   "UNION ALL SELECT '4' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "UNION ALL SELECT DISTINCT '4' as Indice, b.CodigoTaxaID AS fldID, LTRIM(RTRIM(STR(b.CodigoTaxaID))) AS fldName, LTRIM(RTRIM(STR(b.CodigoTaxaID))) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Moedas b WITH(NOLOCK) " & _
                   "WHERE a.ObjetoID= " & nEmpresaID & " AND a.TipoRelacaoID=21 AND a.EstadoID=2 " & _
                   "AND a.RelacaoID=b.RelacaoID " & _
                   "UNION ALL SELECT '5' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "UNION ALL SELECT '5' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ISNULL(CONVERT(VARCHAR(8000),Observacoes), SPACE(0)) AS Descricao  " & _
                   "FROM Conceitos WITH(NOLOCK) " & _
                   "WHERE EstadoID = 2 AND TipoConceitoID = 309 AND Observacao LIKE '%<PR>%' " & _
                   "AND PaisID = " & nEmpresaPaisID & " " & _
                   "UNION ALL SELECT '6' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "UNION ALL SELECT '6' as Indice, b.ConServicoID AS fldID, LEFT(ISNULL(dbo.fn_Produto_Descricao2(b.ConServicoID, " & nEmpresaPaisID & " , 13),SPACE(0)),20) AS fldName, a.Conceito AS Ordem2, LEFT(ISNULL(dbo.fn_Produto_Descricao2(b.ConServicoID, " & nEmpresaPaisID & ", 13),SPACE(0)),20) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ISNULL(dbo.fn_Produto_Descricao2(b.ConServicoID, " & nEmpresaPaisID & ", 13),SPACE(0)) AS Descricao  " & _
                   "FROM Conceitos a WITH(NOLOCK) " & _
                   "INNER JOIN Conceitos_Servicos b WITH(NOLOCK) ON (a.ConceitoID = b.ConceitoID) " & _
                   "WHERE (a.EstadoID = 2) AND (a.TipoConceitoID = 320) " & _
                   "AND (a.PaisID = " & nEmpresaPaisID & ") " & _
                   "AND (b.LocalidadeID = dbo.fn_Pessoa_Localidade(" & nEmpresaID & " , 3, NULL, NULL)) " & _
                   "AND (a.Observacao LIKE '%|PS|%') " & _
                   "UNION ALL " & _
                   "SELECT '7' as Indice, ItemID AS fldId, ItemAbreviado AS fldName, STR(Ordem) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ItemMAsculino AS Descricao " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE TipoID = 114 AND EstadoID = 2 " & _
                   "UNION ALL SELECT '8' as Indice, 0 AS fldID, SPACE(1) AS fldName, SPACE(1) AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, SPACE(0) AS Descricao " & _
                   "UNION ALL SELECT '8' as Indice, ConceitoID AS fldID, Conceito AS fldName, Conceito AS Ordem2, SPACE(1) AS Ordem3, ' ' AS NameSuj, ' ' AS NameObj, ' ' AS Filtro, ISNULL(CONVERT(VARCHAR(8000),Observacoes), SPACE(0)) AS Descricao  " & _
                   "FROM Conceitos WITH(NOLOCK) " & _
                   "WHERE EstadoID = 2 AND TipoConceitoID = 319 " & _
                   "ORDER BY Indice, Ordem2, Ordem3"
   
    Case 2210   'Form de Localizacoes de Estoque
    
        strSQL =   "SELECT '1' as Indice, LocalizacaoID as fldID, dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1) as fldName, " & _
						"dbo.fn_LocalizacaoEstoque_Localizacao(LocalizacaoID, 1) AS Ordem " & _
                   "FROM LocalizacoesEstoque WITH(NOLOCK) " & _
                   "WHERE EstadoID=2 AND EmpresaID=" & nEmpresaID & " " & _
                   "ORDER BY Indice, Ordem "

    Case 3140  'Form de Or�amentos
    
        strSQL =    "SELECT '1' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, ' ' as Filtro " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & nEmpresaID & " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " & _
                    "AND b.MoedaID = c.ConceitoID AND d.RecursoID=999) " & _
                    "ORDER BY Indice, Ordem2"

    Case 4210   'Form de Vitrines

        strSQL =   "SELECT '1' as Indice, STR(Ordem) AS Ordem2,ItemMasculino AS fldName, ItemID AS fldId, Filtro AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE TipoID = 303 AND EstadoID=2 " & _
				   "UNION ALL " & _
                   "SELECT '2' as Indice, STR(Ordem) AS Ordem2, ItemFeminino AS fldName, ItemID AS fldId, Filtro AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE TipoID = 110 AND EstadoID=2 " & _
				   "UNION ALL " & _                   
                   "SELECT '3' as Indice, STR(Ordem) AS Ordem2,ItemFeminino AS fldName, ItemID AS fldId, Filtro AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE TipoID = 108 AND EstadoID=2 " & _
				   "UNION ALL " & _
				   "SELECT DISTINCT '4' as Indice, Marcas.Conceito AS Ordem2, Marcas.Conceito AS fldName, Marcas.ConceitoID AS fldId, SPACE(0) AS Filtro " & _
				   "FROM RelacoesPesCon ProdutosEmpresa, Conceitos Produtos, Conceitos Marcas " & _
				   "WHERE ProdutosEmpresa.SujeitoID=" & nEmpresaID & " AND ProdutosEmpresa.EstadoID NOT IN (1, 5) AND " & _
				   "ProdutosEmpresa.ObjetoID=Produtos.ConceitoID AND  " & _
				   "Produtos.MarcaID=Marcas.ConceitoID  " & _
	               "ORDER BY Indice,Ordem2"
	               
	Case 4220   'Form A��es de Marketing

        strSQL =   "SELECT '1' as Indice, ItemMasculino AS fldName, ItemID AS fldId, Observacao " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE TipoID = 304 AND EstadoID=2 ORDER BY Ordem"             
                   
	Case 4230   'Form Email Marketing

        strSQL =   "SELECT '1' as Indice, a.Vitrine AS fldName, a.VitrineID AS fldId, " & _
                        "(CASE WHEN (a.LojaID IS NULL AND a.MarcaID IS NULL) THEN 1 WHEN (a.LojaID IS NOT NULL) THEN b.Ordem ELSE 999 END) AS Ordem1, " & _
                        "c.Conceito AS Ordem2 " & _
                   "FROM Vitrines a WITH(NOLOCK) " & _
                   "LEFT OUTER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON (b.ItemID = a.LojaID) " & _
                   "LEFT OUTER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.MarcaID) " & _
                   "WHERE (a.EstadoID=2 AND a.EmpresaID=" & nEmpresaID & ") " & _
                   "UNION ALL " & _
                   "SELECT '2' as Indice, ItemMasculino AS fldName, ItemID AS fldId, Ordem AS Ordem1, SPACE(0) AS Ordem2 " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE (EstadoID=2 AND TipoID = 306) " & _
                   "ORDER BY Indice, Ordem1, Ordem2 "              

	Case 4410   'Form de Novidades

        strSQL =   "SELECT '1' as Indice, STR(Ordem) AS Ordem2,ItemMasculino AS fldName, ItemID AS fldId, Filtro AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE TipoID = 301 AND EstadoID=2 " & _
                   "ORDER BY Indice,Ordem2"

    Case 4420   'Form de Eventos

        strSQL =   "SELECT '1' as Indice, STR(Ordem) AS Ordem2,ItemMasculino AS fldName, ItemID AS fldId, Filtro AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE TipoID = 302 AND EstadoID=2 " & _
                   "ORDER BY Indice,Ordem2"

    Case 5110   'Form de Pedidos
    
        strSQL =   "SELECT '1' as Indice, ItemID as fldID, ItemMasculino as fldName, Ordem as Ordem2, 0 AS Cotacao, 0 AS TaxaVenda, Filtro AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE EstadoID=2 AND TipoID=402 " & _
                   "UNION ALL " & _
                   "SELECT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, b.Ordem as Ordem2, " & _
                   "dbo.fn_Preco_Cotacao(d.MoedaID,c.ConceitoID,NULL,GETDATE()) AS Cotacao, e.TaxaVenda AS TaxaVenda, ' ' AS Filtro " & _
                   "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec_Financ e WITH(NOLOCK) " & _
                   "WHERE (a.SujeitoID = " & nEmpresaID & " AND a.ObjetoID=999 AND a.TipoRelacaoID=12 AND a.RelacaoID = b.RelacaoID AND b.Faturamento=1 " & _
                   "AND b.MoedaID = c.ConceitoID " & _
                   "AND d.RecursoID=999 AND a.RelacaoID=e.RelacaoID AND b.MoedaID=e.MoedaID) " & _
                   "UNION ALL " & _
                   "SELECT '6' as Indice, NumeroItens as fldID, 'NumeroItens' as fldName, 0 as Ordem2, 0 AS Cotacao, 0 AS TaxaVenda, ' ' AS Filtro " & _
                   "FROM RelacoesPesRec WITH(NOLOCK) " & _
                   "WHERE (SujeitoID = " & nEmpresaID & " AND ObjetoID=999 AND TipoRelacaoID=12) " & _
                   "UNION ALL " & _
                   "SELECT '7' as Indice, NumeroParcelas as fldID, 'NumeroParcelas' as fldName, 0 as Ordem2, 0 AS Cotacao, 0 AS TaxaVenda, ' ' AS Filtro " & _
                   "FROM RelacoesPesRec WITH(NOLOCK) " & _
                   "WHERE (SujeitoID = " & nEmpresaID & " AND ObjetoID=999 AND TipoRelacaoID=12) " & _
                   "UNION ALL " & _
                   "SELECT '8' as Indice, DiasBase as fldID, 'DiasBase' as fldName, 0 as Ordem2, 0 AS Cotacao, 0 AS TaxaVenda, ' ' AS Filtro " & _
                   "FROM RelacoesPesRec WITH(NOLOCK) " & _
                   "WHERE (SujeitoID = " & nEmpresaID & " AND ObjetoID=999 AND TipoRelacaoID=12) " & _
                   "ORDER BY Indice, Ordem2 "
    
    Case 5120   'Form Notas Fiscais
    
        strSQL =   "SELECT '1' as Indice, STR(Ordem) AS Ordem2, ItemAbreviado as fldName, ItemID as fldID, ' ' AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                   "WHERE EstadoID=2 AND TipoID=434 " & _
                   "ORDER BY Ordem " 
    
    Case 5140   'Form de Opera��es
    
        strSQL =    "SELECT '1' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, Filtro AS Filtro " & _
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                    "WHERE TipoID = 407 AND EstadoID=2 " & _
                    "UNION ALL " & _
                    "SELECT '2' AS Indice,SPACE(0) AS Ordem2,SPACE(0) AS fldName,0 AS fldId, ' ' AS Filtro " & _
                    "UNION ALL SELECT '2' AS Indice, Localidade AS Ordem2, Localidade AS fldName, LocalidadeID as fldID, ' ' AS Filtro " & _
                    "FROM Localidades WITH(NOLOCK) " & _
                    "WHERE EstadoID=2 AND TipoLocalidadeID=203 " & _
                    "UNION ALL " & _
                    "SELECT '3' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                    "WHERE TipoID = 408 AND EstadoID=2 " & _
                    "UNION ALL " & _
                    "SELECT '4' AS Indice,STR(Ordem) AS Ordem2,ItemAbreviado AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                    "WHERE TipoID = 107 AND EstadoID=2 " & _
                    "UNION ALL " & _
                    "SELECT '5' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                    "WHERE TipoID = 115 AND EstadoID=2 AND Filtro LIKE '%<Custo>%' " & _
                    "UNION ALL " & _
                    "SELECT '6' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                    "WHERE TipoID = 433 AND EstadoID=2 " & _
                    "UNION ALL " & _
                    "SELECT '7' AS Indice,STR(Ordem) AS Ordem2,ItemMasculino AS fldName,ItemID AS fldId, ' ' AS Filtro " & _
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                    "WHERE EstadoID=2 AND TipoID=115 AND Filtro LIKE '%<ValorUnitario>%' " & _
                    "UNION ALL " & _
                    "SELECT '8' AS Indice,SPACE(0) AS Ordem2,SPACE(0) AS fldName,0 AS fldId, ' ' AS Filtro " & _
                    "UNION ALL SELECT '8' AS Indice,HistoricoPadrao AS Ordem2,HistoricoPadrao AS fldName,HistoricoPadraoID AS fldId, ' ' AS Filtro " & _
                    "FROM HistoricosPadrao WITH(NOLOCK) " & _
                    "WHERE EstadoID=2 " & _
                    "ORDER BY Indice, Ordem2"

    Case 5160   'Form de Campanhas
    
        strSQL =   "SELECT DISTINCT '1' as Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem, SPACE(0) as Ordem2, Filtro " & _
				   "FROM tiposauxiliares_itens WITH(NOLOCK) " & _
				   "WHERE Tipoid=812 and Aplicar=1 and Estadoid=2 " & _
				   "UNION ALL " & _
				   "SELECT DISTINCT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, 0 AS Ordem, c.SimboloMoeda as Ordem2, ' ' AS Filtro " & _
                   "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec_Financ e WITH(NOLOCK) " & _
                   "WHERE (a.ObjetoID=999 AND a.TipoRelacaoID=12 AND a.RelacaoID = b.RelacaoID " & _
                   "AND b.MoedaID = c.ConceitoID AND d.RecursoID=999 AND a.RelacaoID=e.RelacaoID AND b.MoedaID=e.MoedaID) " & _
				   "UNION ALL " & _
				   "SELECT DISTINCT '3' as Indice, 1 as fldID, 'Dia' as fldName, 1 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " & _
				   "UNION ALL " & _
				   "SELECT DISTINCT '3' as Indice, 2 as fldID, 'Semana' as fldName, 2 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " & _
				   "UNION ALL " & _
				   "SELECT DISTINCT '3' as Indice, 3 as fldID, 'M�s' as fldName, 3 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " & _
				   "UNION ALL " & _
				   "SELECT DISTINCT '3' as Indice, 4 as fldID, 'Quarter' as fldName, 4 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " & _
				   "UNION ALL " & _
				   "SELECT DISTINCT '3' as Indice, 5 as fldID, 'Ano' as fldName, 5 AS Ordem, ' ' as Ordem2, ' ' AS Filtro " & _
                   "ORDER BY Indice, Ordem, Ordem2"

    Case 5220   'Form de Lista de Preco
    
        strSQL =    "SELECT '1' as Indice, 0 AS fldID, ' ' AS fldName, ' ' AS Ordem2, 0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " & _
                    "UNION ALL SELECT DISNTICT '1' as Indice, UFs.LocalidadeID AS fldID, UFs.CodigoLocalidade2 AS fldName, UFs.CodigoLocalidade2 as Ordem2, " & _
                    "0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, Aliquotas.Aliquota AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " & _ 
                    "FROM Conceitos Impostos WITH(NOLOCK), Conceitos_Aliquotas Aliquotas WITH(NOLOCK), Pessoas_Enderecos EmpresaEndereco WITH(NOLOCK), Localidades UFs WITH(NOLOCK) " & _
                    "WHERE Impostos.TipoConceitoID=306 AND Impostos.IncidePreco=1 AND Impostos.InclusoPreco=1 AND " & _
                    "Impostos.AlteraAliquota=1 AND Impostos.DestacaNota=1 AND " & _
                    "Impostos.ConceitoID=Aliquotas.ImpostoID AND EmpresaEndereco.PessoaID= " & nEmpresaID & " " & _
                    "AND EmpresaEndereco.UFID=Aliquotas.LocalidadeOrigemID " & _
                    "AND LocalidadeDestinoID=UFs.LocalidadeID " & _
                    "UNION ALL " & _
                    "SELECT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, " & _
                    "dbo.fn_Preco_Cotacao(d.MoedaID,c.ConceitoID,-a.SujeitoID,GETDATE()) AS Cotacao, " & _
                    "a.DiasBase as DiasBase, e.TaxaVenda as TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK), RelacoesPesRec_Financ e WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & CStr(nEmpresaID) & " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " & _
                    "AND b.MoedaID = c.ConceitoID AND d.RecursoID=999) " & _
                    "AND a.RelacaoID = e.RelacaoID AND e.MoedaID = c.ConceitoID " & _
                    "UNION ALL " & _
					"SELECT '3' as Indice, c.FinanciamentoID as fldID, " & _
					"(CONVERT(VARCHAR(50), CONVERT(INT, ROUND(dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL),2))) + SPACE(1) + CHAR(40) + c.Financiamento + CHAR(41)) AS fldName, " & _
					"STR(c.Ordem) as Ordem2, " & _ 
					"NULL AS Cotacao, NULL AS DiasBase, NULL AS TaxaVenda, 0 AS Aliquota, " & _
					"dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL) AS PMP, c.NumeroParcelas AS NumeroParcelas " & _
					"FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_FinanciamentosPadrao b WITH(NOLOCK), FinanciamentosPadrao c WITH(NOLOCK) " & _
					"WHERE (a.SujeitoID = " & CStr(nEmpresaID) & " AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 AND a.RelacaoID = b.RelacaoID " & _ 
					"AND b.EstadoID = 2 AND b.FinanciamentoID = c.FinanciamentoID AND c.FinanciamentoID <> 1) " & _
					"UNION ALL " & _
                    "SELECT DISTINCT '4' as Indice, 0 AS fldID, SPACE(0) AS fldName, '-1' AS Ordem2, 0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " & _
                    "UNION ALL " & _
                    "SELECT DISTINCT '4' as Indice, 1 AS fldID, 'Pago' AS fldName, '-2' AS Ordem2, 0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " & _
                    "UNION ALL " & _
					"SELECT DISTINCT '4' as Indice, ItemID AS fldID, ItemMasculino AS fldName, LTRIM(RTRIM(STR(Ordem))) AS Ordem2, 0 AS Cotacao, 0 AS DiasBase, 0 AS TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " & _
					"FROM tiposauxiliares_itens WITH(NOLOCK) " & _
					"WHERE Tipoid=405 and Aplicar=1 and Estadoid=2  AND Filtro= \'\%<N>% \'\ " & _
                    "UNION ALL " & _
                    "SELECT '5' as Indice, EmpresaAlternativaID as fldID, SPACE(0) as fldName, STR(0) as Ordem2, " & _
                    "0 AS Cotacao, 0 as DiasBase, 0 as TaxaVenda, 0 AS Aliquota, NULL AS PMP, NULL AS NumeroParcelas " & _
                    "FROM RelacoesPesRec WITH(NOLOCK) " & _
                    "WHERE (SujeitoID = " & CStr(nEmpresaID) & " AND ObjetoID=999) " & _
                    "ORDER BY Indice, Ordem2"

    Case 7110  'Form Documentos

        strSQL =   "SELECT '1' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem AS Ordem2, SPACE(0) AS Ordem3, ' ' AS Filtro, SPACE(0) AS Hint " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=601) " & _
                   "UNION ALL " & _
				   "SELECT '2' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, ItemID AS Ordem2, SPACE(0) AS Ordem3, ' ' AS Filtro, ItemMasculino AS Hint " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=602) " & _
                   "UNION ALL " & _
                   "SELECT '3' AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem2, SPACE(0) AS Ordem3, ' ' AS Filtro, SPACE(0) AS Hint " & _
                   "UNION ALL " & _
				   "SELECT '3' AS Indice, b.RecursoID AS fldID, (a.RecursoFantasia + CHAR(47) + b.RecursoFantasia) AS fldName, 0 AS Ordem2, (a.RecursoFantasia + CHAR(47) + b.RecursoFantasia) AS Ordem3, ' ' AS Filtro, SPACE(0) AS Hint " & _
				   "FROM Recursos a WITH(NOLOCK), Recursos b WITH(NOLOCK) " & _
				   "WHERE (a.EstadoID=2 AND a.TipoRecursoID=2 AND a.ClassificacaoID=11 AND a.RecursoID=b.RecursoMaeID AND " & _
					"b.EstadoID=2 AND b.TipoRecursoID=3) " & _
                   "ORDER BY Indice, Ordem2, Ordem3"

    Case 7120  'Form RIQ

        strSQL =   "SELECT '1' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, SPACE(0) AS Ordem3, Filtro AS Filtro, SPACE(0) AS Hint " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=606) " & _
				   "UNION ALL " & _
				   "SELECT '2' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, ItemID AS Ordem2, SPACE(0) AS Ordem3, Filtro AS Filtro, ItemMasculino AS Hint " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=602) " & _
				   "UNION ALL " & _
				   "SELECT '3' AS Indice, 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem2, '0' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " & _
				   "UNION ALL " & _
				   "SELECT '3' AS Indice, 1 AS fldID, 'Mensal' AS fldName, 1 AS Ordem2, '1' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " & _
				   "UNION ALL " & _
				   "SELECT '3' AS Indice, 2 AS fldID, 'Bimestral' AS fldName, 2 AS Ordem2, '2' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " & _
				   "UNION ALL " & _
				   "SELECT '3' AS Indice, 3 AS fldID, 'Trimestral' AS fldName, 3 AS Ordem2, '3' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " & _
				   "UNION ALL " & _
				   "SELECT '3' AS Indice, 6 AS fldID, 'Semestral' AS fldName, 6 AS Ordem2, '6' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " & _
				   "UNION ALL " & _
				   "SELECT '3' AS Indice, 12 AS fldID, 'Anual' AS fldName, 12 AS Ordem2, '12' AS Ordem3, SPACE(0) AS Filtro, SPACE(0) AS Hint " & _
                   "ORDER BY Indice, Ordem2, Ordem3"

    Case 7140  'Form RRC

        strSQL =   "SELECT '1' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, SPACE(0) AS Ordem3, Filtro AS Filtro, SPACE(0) AS Hint " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=605) " & _
                   "ORDER BY Indice, Ordem2, Ordem3"

    Case 7170  'Form RACP

        strSQL =   "SELECT '1' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, SPACE(0) AS Ordem3, Filtro AS Filtro, SPACE(0) AS Hint " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=603) " & _
                   "ORDER BY Indice, Ordem2, Ordem3"

    Case 9110   'Form Financeiro

        strSQL =    "SELECT '1' as Indice, a.ItemID as fldID, a.ItemAbreviado as fldName, STR(a.Ordem) as Ordem2, ' ' as Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " & _
                    "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
                    "WHERE (a.EstadoID = 2 AND TipoID=804) " & _
                    "UNION ALL " & _
                    "SELECT DISTINCT '2' as Indice, a.HistoricoPadraoID as fldID, a.HistoricoPadrao as fldName, a.HistoricoPadrao as Ordem2, ' ' as Filtro, " & _
					"a.TipoLancamentoID, " & _
					"CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.TipoLancamentoID = 1571 AND (aa.Pedido = 1 OR aa.Financeiro = 1)))) AS Pagamento, " & _
					"(CASE WHEN (a.HistoricoPadraoID = 4) THEN 1 ELSE CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.TipoLancamentoID = 1572 AND (aa.Pedido = 1 OR aa.Financeiro = 1)))) END) AS Recebimento, " & _
					"(CASE WHEN (a.HistoricoPadraoID = 4) THEN 1 ELSE CONVERT(BIT, (SELECT COUNT(*) FROM HistoricosPadrao_Contas aa WITH(NOLOCK) WHERE (a.HistoricoPadraoID = aa.HistoricoPadraoID AND aa.TipoLancamentoID = 1573 AND (aa.Pedido = 1 OR aa.Financeiro = 1)))) END) AS Importe, " & _
					"dbo.fn_HistoricoPadrao_TipoDetalhamento(a.HistoricoPadraoID, 1571, 1543) AS ImpostoPagamento, " & _
					"dbo.fn_HistoricoPadrao_TipoDetalhamento(a.HistoricoPadraoID, 1572, 1543) AS ImpostoRecebimento, " & _
					"dbo.fn_HistoricoPadrao_TipoDetalhamento(a.HistoricoPadraoID, 1573, 1543) AS ImpostoImporte, " & _
					"CONVERT(BIT, ISNULL(a.Marketing, 0)) AS Marketing " & _
                    "FROM HistoricosPadrao a WITH(NOLOCK), HistoricosPadrao_Contas b WITH(NOLOCK) " & _
                    "WHERE (a.EstadoID = 2 AND ((a.HistoricoPadraoID = b.HistoricoPadraoID AND (b.Pedido = 1 OR b.Financeiro = 1)) OR (a.HistoricoPadraoID = 4))) " & _
                    "UNION ALL " & _
                    "SELECT '3' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, ' ' as Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & nEmpresaID & " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " & _
                    "AND b.MoedaID = c.ConceitoID AND b.Faturamento = 1 " & _
                    "AND d.RecursoID=999) " & _
                    "UNION ALL " & _
                    "SELECT '4' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, ' ' as Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & nEmpresaID & " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " & _
                    "AND b.MoedaID = c.ConceitoID " & _
                    "AND d.RecursoID=999) " & _
                    "UNION ALL " & _
                    "SELECT '5' as Indice, c.RelPesContaID as fldID, f.Codigo as fldName, f.Banco as Ordem2, ' ' AS Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Cobranca b WITH(NOLOCK), RelacoesPessoas_Contas c WITH(NOLOCK), RelacoesPessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK), Bancos f WITH(NOLOCK) " & _
                    "WHERE a.TipoRelacaoID = 12 AND a.ObjetoID=999 AND a.SujeitoID= " & nEmpresaID & " " & _
                    "AND a.RelacaoID=b.RelacaoID AND b.EstadoID=2 AND b.RelPesContaID = c.RelPesContaID " & _
                    "AND c.RelacaoID = d.RelacaoID AND d.EstadoID = 2 AND d.ObjetoID = e.PessoaID " & _
                    "AND e.EstadoID = 2 AND e.BancoID = f.BancoID AND f.EstadoID = 2 " & _
                    "UNION ALL " & _
                    "SELECT '6' as Indice, 0 as fldID, LTRIM(STR(a.DiasBase)) as fldName, ' ' as Ordem2, ' ' as Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK) " & _
                    "WHERE (a.TipoRelacaoID = 12 AND a.ObjetoID = 999 AND a.SujeitoID = " & nEmpresaID & ") " & _
					"UNION ALL " & _
					"SELECT '7' as Indice, 0 as fldID, SPACE(0) as fldName, SPACE(0) as Ordem2, ' ' AS Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " & _
					"UNION ALL " & _
					"SELECT '7' as Indice, ConceitoID as fldID, Imposto as fldName, Imposto as Ordem2, ' ' AS Filtro, NULL AS TipoLancamentoID, NULL AS Pagamento, NULL AS Recebimento, NULL AS Importe, NULL AS ImpostoPagamento, NULL AS ImpostoRecebimento, NULL AS ImpostoImporte, CONVERT(BIT, 0) AS Marketing " & _
					"FROM Conceitos WITH(NOLOCK) " & _
					"WHERE (TipoConceitoID = 306 AND EstadoID = 2 AND PaisID = " & CStr(nEmpresaPaisID) & ") " & _					
                    "ORDER BY Indice, Ordem2"

    Case 9130   'Form Valores a Localizar

        strSQL =    "SELECT '1' as Indice, a.ItemID as fldID, a.ItemAbreviado as fldName, SPACE(0) AS Ordem, a.Ordem as Ordem2, Filtro AS Filtro " & _
                    "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
                    "WHERE (a.EstadoID = 2 AND TipoID=804 AND Filtro LIKE '%(9130)%') " & _
                    "UNION ALL " & _
                    "SELECT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, SPACE(0) AS Ordem, b.Ordem as Ordem2, ' ' as Filtro " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & nEmpresaID & " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " & _
                    "AND b.MoedaID = c.ConceitoID AND b.Faturamento = 1 " & _
                    "AND d.RecursoID=999) " & _
					"UNION ALL " & _
                    "SELECT '3' as Indice, c.RelPesContaID as fldID, dbo.fn_ContaBancaria_Nome(c.RelPesContaID, 4) as fldName, " & _
                    "dbo.fn_ContaBancaria_Nome(c.RelPesContaID, 4) as Ordem, 0 AS Ordem2,  ' ' AS Filtro " & _
                    "FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK), RelacoesPessoas_Contas c WITH(NOLOCK), Bancos d WITH(NOLOCK) " & _
                    "WHERE a.SujeitoID= " & nEmpresaID & " AND a.TipoRelacaoID = 24 AND a.ObjetoID=b.PessoaID AND " & _
                    "b.EstadoID = 2 AND a.RelacaoID=c.RelacaoID AND c.EstadoID = 2 AND b.BancoID = d.BancoID AND d.EstadoID = 2 " & _
					"UNION ALL " & _
                    "SELECT '4' as Indice, a.ItemID as fldID, a.ItemMasculino as fldName, SPACE(0) AS Ordem, a.Ordem as Ordem2, Filtro AS Filtro " & _
                    "FROM TiposAuxiliares_Itens a WITH(NOLOCK) " & _
                    "WHERE (TipoID=821) " & _
                    "ORDER BY Indice, Ordem, Ordem2"

    Case 9150   'Form Depositos Bancarios
    
        strSQL =    "SELECT '1' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, STR(b.Ordem) as Ordem2, ' ' as Filtro " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Recursos d WITH(NOLOCK) " & _
                    "WHERE (a.SujeitoID = " & nEmpresaID & " AND a.ObjetoID=999 AND a.RelacaoID = b.RelacaoID " & _
                    "AND b.MoedaID = c.ConceitoID AND b.Faturamento = 1 " & _
                    "AND d.RecursoID=999) " & _
                    "UNION ALL " & _
                    "SELECT '2' as Indice, b.RelPesContaID as fldID, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as fldName, dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as Ordem2, ' ' AS Filtro " & _
                    "FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Contas b WITH(NOLOCK) " & _
                    "WHERE (a.TipoRelacaoID = 24 AND a.SujeitoID = " & nEmpresaID & " AND a.EstadoID = 2 AND " & _
						"a.RelacaoID = b.RelacaoID AND b.TipoContaID = 1506 AND b.EstadoID=2) " & _
                    "ORDER BY Indice, Ordem2"

    Case 9210   'Form Cobranca

        strSQL =    "SELECT '1' as Indice, b.RelPesContaID as fldID, f.Codigo as fldName, f.Banco as Ordem2, ' ' AS Filtro " & _
                    "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Cobranca b WITH(NOLOCK), RelacoesPessoas_Contas c WITH(NOLOCK), RelacoesPessoas d WITH(NOLOCK), Pessoas e WITH(NOLOCK), Bancos f WITH(NOLOCK) " & _
                    "WHERE a.TipoRelacaoID = 12 AND a.ObjetoID=999 AND a.SujeitoID= " & nEmpresaID & " " & _
                    "AND a.RelacaoID=b.RelacaoID AND b.RelPesContaID = c.RelPesContaID AND c.RelacaoID = d.RelacaoID " & _
                    "AND d.ObjetoID = e.PessoaID AND e.BancoID = f.BancoID " & _
                    "UNION ALL SELECT '2' as Indice, ItemID AS fldID, ItemMasculino as fldName, STR(Ordem) as Ordem2, Filtro AS Filtro " & _
                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE TipoID=809 AND EstadoID=2 " & _
                    "ORDER BY Indice, Ordem2"

    Case 9220   'Form Bancos

        strSQL =    "SELECT '1' as Indice, a.LocalidadeID as fldID, a.Localidade as fldName, a.Localidade as Ordem2, ' ' AS Filtro " & _
                    "FROM Localidades a WITH(NOLOCK) " & _
                    "WHERE a.TipoLocalidadeID = 203 AND a.EstadoID=2 " & _
                    "ORDER BY Indice, Ordem2"

    Case 10110  'Form PlanoContas

        strSQL =   "SELECT '1' as Indice, 0 as fldID, SPACE(0) as fldName, 0 as Ordem2, ' ' AS Filtro " & _
					"UNION ALL " & _
                    "SELECT '1' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " & _
					"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
					"WHERE (EstadoID=2 AND TipoID=901) " & _
                    "UNION ALL " & _ 
					"SELECT '2' as Indice, 0 as fldID, SPACE(0) as fldName, 0 as Ordem2, ' ' AS Filtro " & _
					"UNION ALL " & _
                    "SELECT '2' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " & _
					"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
					"WHERE (EstadoID=2 AND TipoID=903) " & _
                    "UNION ALL " & _ 
        			"SELECT '3' as Indice, 0 as fldID, SPACE(0) as fldName, 0 as Ordem2, ' ' AS Filtro " & _
					"UNION ALL " & _
					"SELECT '3' as Indice, a.ContaID as fldID, CONVERT(VARCHAR(10), a.ContaID) as fldName, a.ContaID as Ordem2, ' ' AS Filtro " & _
                    "FROM PlanoContas a WITH(NOLOCK) " & _
                    "WHERE a.EstadoID = 2 " & _
                    "UNION ALL " & _
                    "SELECT '4' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " & _
					"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
					"WHERE (EstadoID=2 AND TipoID=904) " & _
                    "UNION ALL " & _
                    "SELECT '5' as Indice, 0 as fldID, SPACE(0) as fldName, 0 as Ordem2, ' ' AS Filtro " & _
					"UNION ALL " & _
                    "SELECT '5' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " & _
					"FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
					"WHERE (EstadoID=2 AND TipoID=905) " & _
                    "ORDER BY Indice, Ordem2"

    Case 10120  'Form Hist�ricos Padr�o

        strSQL =   "SELECT '1' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem AS Ordem2, ' ' AS Filtro " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=907) " & _
                   "UNION ALL " & _
                   "SELECT '2' AS Indice, ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem2, ' ' AS Filtro " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=909) " 
				   
    Case 10130  'Form Lancamentos

        strSQL =   "SELECT '1' AS Indice, ItemID AS fldID, ItemAbreviado AS fldName, Ordem AS Ordem2, ' ' AS Filtro " & _
				   "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
				   "WHERE (EstadoID=2 AND TipoID=907) " & _
                   "UNION ALL " & _
				   "SELECT '2' as Indice, c.ConceitoID as fldID, c.SimboloMoeda as fldName, b.Ordem as Ordem2, ' ' AS Filtro " & _
                   "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                   "WHERE (a.SujeitoID = " & nEmpresaID & " AND a.ObjetoID=999 AND a.TipoRelacaoID=12 AND a.RelacaoID = b.RelacaoID AND b.Faturamento=1 " & _
                   "AND b.MoedaID = c.ConceitoID) " & _
                   "ORDER BY Indice, Ordem2"

    Case 10140   'Form ExtratosBancarios
    
        strSQL =   "SELECT '1' as Indice, b.RelPesContaID as fldID, " & _
						"dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as fldName, " & _
						"dbo.fn_ContaBancaria_Nome(b.RelPesContaID, 4) as Ordem2, ' ' AS Filtro " & _
                    "FROM RelacoesPessoas a WITH(NOLOCK), RelacoesPessoas_Contas b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Bancos d WITH(NOLOCK) " & _
                    "WHERE a.TipoRelacaoID = 24 AND a.EstadoID = 2 AND a.SujeitoID= " & CStr(nEmpresaID) & " " & _
                    "AND a.RelacaoID=b.RelacaoID AND b.EstadoID = 2 AND a.ObjetoID = c.PessoaID AND c.EstadoID = 2 " & _
                    "AND c.BancoID = d.BancoID AND d.EstadoID = 2 " & _
                    "ORDER BY Indice, Ordem2"

    Case 12210   'Form Competencias
    
        strSQL =   "SELECT '1' AS Indice, STR(Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, Filtro AS Filtro " & _
                   " FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1101 AND Aplicar = 1 " & _
                   "ORDER BY Indice, Ordem2"

    Case 12220   'Form RET
    
        strSQL =   "SELECT '1' AS Indice, Competencia AS Ordem2, Competencia AS fldName, CompetenciaID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM Competencias WITH(NOLOCK) WHERE EstadoID = 2 AND TipoCompetenciaID = 1704 " & _
                   "UNION ALL SELECT '2' as Indice, SPACE(0) AS Ordem2, SPACE(0) AS fldName, 0 AS fldID,  SPACE(0) AS Filtro  " & _
                   "UNION ALL SELECT '2' as Indice, c.SimboloMoeda AS Ordem2, c.SimboloMoeda AS fldName, c.ConceitoID AS fldID,  SPACE(0) AS Filtro  " & _
                   "FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) " & _
                   "WHERE a.SujeitoID = " & nEmpresaID & " " & _
                   "AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 " & _
                   "AND a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID " & _
                   "ORDER BY Indice, Ordem2"

    Case 12320   'Form Avaliacoes
    
        strSQL =   "SELECT '1' AS Indice, CONVERT(VARCHAR,Ordem) AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1201 " & _
                   "UNION ALL " & _
                   "SELECT '2' AS Indice, RecursoFantasia AS Ordem2, RecursoFantasia AS fldName, RecursoID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM Recursos WITH(NOLOCK) WHERE (EstadoID BETWEEN 1 AND 2) AND TipoRecursoID=6 AND EhCargo=1 " & _
                   "UNION ALL " & _
				   "SELECT '3' AS Indice, b.Fantasia AS Ordem2, b.Fantasia AS fldName, b.PessoaID AS fldID, SPACE(0) AS Filtro " & _
				   "FROM RelacoesPessoas a WITH(NOLOCK), Pessoas b WITH(NOLOCK) " & _
				   "WHERE a.ObjetoID=" & nEmpresaID & " AND a.TipoRelacaoID=31 AND a.EstadoID=2 AND a.SujeitoID=b.PessoaID " & _
                   "ORDER BY Indice, Ordem2"

    Case 12510   'Form Fopag
    
        strSQL =   "SELECT '1' AS Indice, Ordem AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, NULL AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1001 " & _
                   "ORDER BY Indice, Ordem2"

    Case 12530   'Form Verbas
    
        strSQL =   "SELECT '1' AS Indice, ItemMasculino AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1002 " & _
				   "UNION ALL SELECT '2' AS Indice, ItemMasculino AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1003 " & _
				   "UNION ALL SELECT '3' AS Indice, ItemMasculino AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE EstadoID = 2 AND TipoID = 1004 " & _
				   "UNION ALL SELECT '4' AS Indice, CONVERT(VARCHAR(10),ContaID) AS Ordem2, Conta AS fldName, ContaID AS fldID, SPACE(0) AS Filtro " & _
				   "FROM  PlanoContas WITH(NOLOCK) WHERE EstadoID=2 and dbo.fn_Conta_AceitaLancamento(Contaid) = 1 " & _
				   "UNION ALL SELECT '5' AS Indice, SPACE(0) AS Ordem2, SPACE(0) AS fldName, 0 AS fldID, SPACE(0) AS Filtro " & _
				   "UNION ALL SELECT '5' AS Indice, Imposto AS Ordem2, Imposto AS fldName, ConceitoID AS fldID, SPACE(0) AS Filtro " & _
				   "FROM Conceitos WITH(NOLOCK) " & _
				   "WHERE EstadoID=2 AND TipoConceitoID=306 AND PaisID=" & CStr(nEmpresaPaisID) & " " & _
                   "ORDER BY Indice, Ordem2"
                   
    Case 5410 'Form Conhecimento de Transporte
    
        strSQL =   "SELECT '1' AS Indice, ItemMasculino AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH (NOLOCK) WHERE EstadoID = 2 AND TipoID = 422 " & _   
                   "UNION ALL SELECT '2' AS Indice, ItemAbreviado AS Ordem2, ItemAbreviado AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH (NOLOCK) WHERE EstadoID=2 AND TipoID=403 " & _
	               "UNION ALL SELECT '3' AS Indice, ItemMasculino AS Ordem2, ItemMasculino AS fldName, ItemID AS fldID, SPACE(0) AS Filtro " & _
                   "FROM TiposAuxiliares_Itens WITH (NOLOCK) WHERE EstadoID=2 AND TipoID=405  AND Filtro like \'\%<N>%\'\ " & _
	               "ORDER BY Indice, Ordem2"
    
    
    Case Else
        'Nao tem c�digo

End Select
  
'--------------------------- As strings de pesquisa -- FIM ---------------------------

Set rsData = Server.CreateObject("ADODB.Recordset")
                          
'rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

Set rsSPCommand = Server.CreateObject("ADODB.Command")

With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandText = "sp_RelatorioEstoque"
    .CommandType = adCmdStoredProc
    .Parameters.Append( .CreateParameter("@sSQL", adVarChar, adParamInput,Len(strSQL) , strSQL) )
    Set rsData = .Execute
End With

rsData.Save Response, adPersistXML

rsData.Close
Set rsData = Nothing

Set rsSPCommand = Nothing
%>