/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de orcamentos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Orcamentos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 1), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoPrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 15), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBasePrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 2), 2, 1, ' + DATE_SQL_PARAM + ') AS ContribuicaoPrevista, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 3), 2, 1, ' + DATE_SQL_PARAM + ') AS DespesasPrevista, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 4), 2, 1, ' + DATE_SQL_PARAM + ') AS ResultadoPrevisto, ' +
               'dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 5) AS FuncionariosPrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 6), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 16), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBaseProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 7), 2, 1, ' + DATE_SQL_PARAM + ') AS ContribuicaoProjetada, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 8), 2, 1, ' + DATE_SQL_PARAM + ') AS DespesasProjetada, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 9), 2, 1, ' + DATE_SQL_PARAM + ') AS ResultadoProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 10), 2, 1, ' + DATE_SQL_PARAM + ') AS Faturamento, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 17), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBase, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 11), 2, 1, ' + DATE_SQL_PARAM + ') AS Contribuicao, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 12), 2, 1, ' + DATE_SQL_PARAM + ') AS Despesas, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 13), 2, 1, ' + DATE_SQL_PARAM + ') AS Resultado, ' +
               'dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 14) AS Funcionarios ' +
               'FROM Orcamentos WITH(NOLOCK) ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY OrcamentoID DESC';
    else
        sSQL = 'SELECT *, ' + 
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Orcamentos.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 1), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoPrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 15), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBasePrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 2), 2, 1, ' + DATE_SQL_PARAM + ') AS ContribuicaoPrevista, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 3), 2, 1, ' + DATE_SQL_PARAM + ') AS DespesasPrevista, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 4), 2, 1, ' + DATE_SQL_PARAM + ') AS ResultadoPrevisto, ' +
               'dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 5) AS FuncionariosPrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 6), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 16), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBaseProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 7), 2, 1, ' + DATE_SQL_PARAM + ') AS ContribuicaoProjetada, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 8), 2, 1, ' + DATE_SQL_PARAM + ') AS DespesasProjetada, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 9), 2, 1, ' + DATE_SQL_PARAM + ') AS ResultadoProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 10), 2, 1, ' + DATE_SQL_PARAM + ') AS Faturamento, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 17), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBase, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 11), 2, 1, ' + DATE_SQL_PARAM + ') AS Contribuicao, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 12), 2, 1, ' + DATE_SQL_PARAM + ') AS Despesas, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 13), 2, 1, ' + DATE_SQL_PARAM + ') AS Resultado, ' +
               'dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 14) AS Funcionarios ' +
               'FROM Orcamentos WITH(NOLOCK) WHERE OrcamentoID = ' + nID + ' ORDER BY OrcamentoID DESC';

    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
          '0 AS Prop1, 0 AS Prop2, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 1), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoPrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 15), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBasePrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 2), 2, 1, ' + DATE_SQL_PARAM + ') AS ContribuicaoPrevista, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 3), 2, 1, ' + DATE_SQL_PARAM + ') AS DespesasPrevista, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 4), 2, 1, ' + DATE_SQL_PARAM + ') AS ResultadoPrevisto, ' +
               'dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 5) AS FuncionariosPrevisto, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 6), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 16), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBaseProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 7), 2, 1, ' + DATE_SQL_PARAM + ') AS ContribuicaoProjetada, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 8), 2, 1, ' + DATE_SQL_PARAM + ') AS DespesasProjetada, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 9), 2, 1, ' + DATE_SQL_PARAM + ') AS ResultadoProjetado, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 10), 2, 1, ' + DATE_SQL_PARAM + ') AS Faturamento, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 17), 2, 1, ' + DATE_SQL_PARAM + ') AS FaturamentoBase, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 11), 2, 1, ' + DATE_SQL_PARAM + ') AS Contribuicao, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 12), 2, 1, ' + DATE_SQL_PARAM + ') AS Despesas, ' +
               'dbo.fn_Numero_Formata(dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 13), 2, 1, ' + DATE_SQL_PARAM + ') AS Resultado, ' +
               'dbo.fn_Orcamento_Totais(OrcamentoID, NULL, 0, 14) AS Funcionarios ' +
          'FROM Orcamentos WITH(NOLOCK) WHERE OrcamentoID = 0';
    
    dso.SQL = sql;          
}              
