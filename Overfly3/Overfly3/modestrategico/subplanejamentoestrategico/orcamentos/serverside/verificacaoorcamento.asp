
<%@ LANGUAGE=VBSCRIPT @EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'On Error Resume Next
Response.ContentType = "text/xml"

Dim rsData
Dim rsNew
Dim nOrcamentoID
Dim nEstadoDeID
Dim nEstadoParaID
Dim strSQL
Dim nRecsAffected
Dim rsCommand
Dim sResultado
Dim i
  
strSQL = ""
nOrcamentoID = 0
nEstadoDeID = 0
nEstadoParaID = 0
sResultado = ""

For i = 1 To Request.QueryString("nOrcamentoID").Count    
    nOrcamentoID = Request.QueryString("nOrcamentoID")(i)
Next

For i = 1 To Request.QueryString("nEstadoDeID").Count    
    nEstadoDeID = Request.QueryString("nEstadoDeID")(i)
Next

For i = 1 To Request.QueryString("nEstadoParaID").Count    
    nEstadoParaID = Request.QueryString("nEstadoParaID")(i)
Next

strSQL = "SELECT dbo.fn_Orcamento_Verifica( " & CStr(nOrcamentoID) & ", " & _
												     CStr(nEstadoDeID) & ", " & _
												     CStr(nEstadoParaID) & ") AS Resultado"
         
Set rsData = Server.CreateObject("ADODB.Recordset")
    
rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
    
sResultado = rsData.Fields("Resultado").Value
    
rsData.Close
Set rsData = Nothing  


Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "Resultado", adVarChar, 8000, adFldMayBeNull OR adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew

If ( NOT IsNull(sResultado) ) Then
	rsNew.Fields("Resultado").Value = CStr(sResultado)
End If

rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
%>

