
<%@ LANGUAGE=VBSCRIPT @EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    Dim sEmpresas, sOrigem, sGerentesProduto, sMarcas, nPeriodoEstoque, sDataInicio, sDataFim
    Dim nCompleto, nEstoque, nAlerta, nTipoResultado
    Dim i 
        
    sEmpresas = ""
    sOrigem = ""
    sGerentesProduto = ""
    sMarcas = ""
    sDataInicio = ""
    sDataFim = ""
    nPeriodoEstoque = 0
    nCompleto = 0
    nEstoque = 0
    nAlerta = 0
    nTipoResultado = 0

    For i = 1 To Request.QueryString("sEmpresas").Count
        sEmpresas = Request.QueryString("sEmpresas")(i)
    Next

    For i = 1 To Request.QueryString("sOrigem").Count
        sOrigem = Request.QueryString("sOrigem")(i)
    Next

    For i = 1 To Request.QueryString("sGerentesProduto").Count
        sGerentesProduto = Request.QueryString("sGerentesProduto")(i)
    Next
    
    For i = 1 To Request.QueryString("sMarcas").Count
        sMarcas = Request.QueryString("sMarcas")(i)
    Next

    For i = 1 To Request.QueryString("sDataInicio").Count    
        sDataInicio = Request.QueryString("sDataInicio")(i)
    Next

    For i = 1 To Request.QueryString("sDataFim").Count    
        sDataFim = Request.QueryString("sDataFim")(i)
    Next

    For i = 1 To Request.QueryString("nPeriodoEstoque").Count
        nPeriodoEstoque = Request.QueryString("nPeriodoEstoque")(i)
    Next

    For i = 1 To Request.QueryString("nCompleto").Count
        nCompleto = Request.QueryString("nCompleto")(i)
    Next

    For i = 1 To Request.QueryString("nEstoque").Count
        nEstoque = Request.QueryString("nEstoque")(i)
    Next

    For i = 1 To Request.QueryString("nAlerta").Count
        nAlerta = Request.QueryString("nAlerta")(i)
    Next

    For i = 1 To Request.QueryString("nTipoResultado").Count
        nTipoResultado = Request.QueryString("nTipoResultado")(i)
    Next

	Dim rsSPCommand
	Dim rsData
	
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
    Set rsData = Server.CreateObject("ADODB.Recordset")
	  
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_Orcamento_CapitalGiro"
	    .CommandType = adCmdStoredProc

		.Parameters.Append( .CreateParameter("@Empresas",adVarChar, adParamInput, 8000) )
		.Parameters("@Empresas").Value = sEmpresas

		.Parameters.Append( .CreateParameter("@Origem",adVarChar, adParamInput, 8000) )
        If (CStr(sOrigem) = "") Then
		    .Parameters("@Origem").Value = Null
        Else		        
		    .Parameters("@Origem").Value = sOrigem
        End If

		.Parameters.Append( .CreateParameter("@GerentesProduto",adVarChar, adParamInput, 8000) )
        If (CStr(sGerentesProduto) = "") Then
		    .Parameters("@GerentesProduto").Value = Null
        Else		        
		    .Parameters("@GerentesProduto").Value = sGerentesProduto
        End If

		.Parameters.Append( .CreateParameter("@Marcas",adVarChar, adParamInput, 8000) )
        If (CStr(sMarcas) = "") Then
		    .Parameters("@Marcas").Value = Null
        Else		        
		    .Parameters("@Marcas").Value = sMarcas
        End If

	    .Parameters.Append( .CreateParameter("@dtInicio", adDate , adParamInput) )
        If (CStr(sDataInicio) = "") Then
		    .Parameters("@dtInicio").Value = Null
        Else		        
		    .Parameters("@dtInicio").Value = CDate(sDataInicio)
        End If

	    .Parameters.Append( .CreateParameter("@dtFim", adDate, adParamInput) )
        If (CStr(sDataFim) = "") Then
		    .Parameters("@dtFim").Value = Null
        Else		        
		    .Parameters("@dtFim").Value = CDate(sDataFim)
        End If

		.Parameters.Append( .CreateParameter("@PeriodoEstoque", adInteger, adParamInput) )
        If (CInt(nPeriodoEstoque) = 0) Then
		    .Parameters("@PeriodoEstoque").Value = Null
        Else		        
		    .Parameters("@PeriodoEstoque").Value = nPeriodoEstoque
        End If

		.Parameters.Append( .CreateParameter("@Completo", adBoolean, adParamInput) )
        If (CStr(nCompleto) = "") Then
		    .Parameters("@Completo").Value = Null
        Else		        
		    .Parameters("@Completo").Value = CBool(nCompleto)
        End If

		.Parameters.Append( .CreateParameter("@Estoque", adBoolean, adParamInput) )
        If (CStr(nEstoque) = "") Then
		    .Parameters("@Estoque").Value = Null
        Else		        
		    .Parameters("@Estoque").Value = CBool(nEstoque)
        End If

		.Parameters.Append( .CreateParameter("@TipoResultado", adInteger, adParamInput) )
        If (CStr(nTipoResultado) = "") Then
		    .Parameters("@TipoResultado").Value = Null
        Else		        
		    .Parameters("@TipoResultado").Value = CInt(nTipoResultado)
        End If

	    Set rsData = .Execute

	End With

    Set rsSPCommand = Nothing
	rsData.Save Response, adPersistXML
	rsData.Close
    Set rsData = Nothing
%>
