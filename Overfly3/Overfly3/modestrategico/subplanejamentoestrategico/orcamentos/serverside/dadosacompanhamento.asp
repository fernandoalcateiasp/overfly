
<%@ LANGUAGE=VBSCRIPT @EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
    Dim sEmpresas, nAno, sMeses, sContas, nResponsavelID
    Dim nPrevisto, nProjeta, nContabilidade, nAlerta, nTipoResultado
    Dim i 
        
    sEmpresas = ""
    nAno = 0
    sMeses = ""
    sContas = ""
    nResponsavelID = 0
    nPrevisto = 0
    nProjeta = 0
    nContabilidade = 0
    nAlerta = 0
    nTipoResultado = 0

    For i = 1 To Request.QueryString("sEmpresas").Count
        sEmpresas = Request.QueryString("sEmpresas")(i)
    Next

    For i = 1 To Request.QueryString("nAno").Count
        nAno = Request.QueryString("nAno")(i)
    Next
    
    For i = 1 To Request.QueryString("sMeses").Count
        sMeses = Request.QueryString("sMeses")(i)
    Next

    For i = 1 To Request.QueryString("sContas").Count
        sContas = Request.QueryString("sContas")(i)
    Next

    For i = 1 To Request.QueryString("nResponsavelID").Count
        nResponsavelID = Request.QueryString("nResponsavelID")(i)
    Next

    For i = 1 To Request.QueryString("nPrevisto").Count
        nPrevisto = Request.QueryString("nPrevisto")(i)
    Next

    For i = 1 To Request.QueryString("nProjeta").Count
        nProjeta = Request.QueryString("nProjeta")(i)
    Next

    For i = 1 To Request.QueryString("nContabilidade").Count
        nContabilidade = Request.QueryString("nContabilidade")(i)
    Next

    For i = 1 To Request.QueryString("nAlerta").Count
        nAlerta = Request.QueryString("nAlerta")(i)
    Next

    For i = 1 To Request.QueryString("nTipoResultado").Count
        nTipoResultado = Request.QueryString("nTipoResultado")(i)
    Next

	Dim rsSPCommand
	Dim rsData
	
	Set rsSPCommand = Server.CreateObject("ADODB.Command")
    Set rsData = Server.CreateObject("ADODB.Recordset")
	  
	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_Orcamento_Acompanhamento"
	    .CommandType = adCmdStoredProc

		.Parameters.Append( .CreateParameter("@Empresas",adVarChar, adParamInput, 8000) )
		.Parameters("@Empresas").Value = sEmpresas

		.Parameters.Append( .CreateParameter("@Ano", adInteger, adParamInput) )
	    .Parameters("@Ano").Value = nAno

		.Parameters.Append( .CreateParameter("@Meses",adVarChar, adParamInput, 8000) )
        If (CStr(sMeses) = "") Then
		    .Parameters("@Meses").Value = Null
        Else		        
		    .Parameters("@Meses").Value = sMeses
        End If

		.Parameters.Append( .CreateParameter("@Contas",adVarChar, adParamInput, 8000) )
        If (CStr(sContas) = "") Then
		    .Parameters("@Contas").Value = Null
        Else		        
		    .Parameters("@Contas").Value = sContas
        End If

		.Parameters.Append( .CreateParameter("@ResponsavelID", adInteger, adParamInput) )
        If (CLng(nResponsavelID) = 0) Then
		    .Parameters("@ResponsavelID").Value = Null
        Else		        
		    .Parameters("@ResponsavelID").Value = nResponsavelID
        End If

		.Parameters.Append( .CreateParameter("@Previsto", adBoolean, adParamInput) )
	    .Parameters("@Previsto").Value = CBool(nPrevisto)

		.Parameters.Append( .CreateParameter("@Projeta", adBoolean, adParamInput) )
	    .Parameters("@Projeta").Value = CBool(nProjeta)

		.Parameters.Append( .CreateParameter("@Contabilidade", adBoolean, adParamInput) )
	    .Parameters("@Contabilidade").Value = CBool(nContabilidade)

		.Parameters.Append( .CreateParameter("@Alerta", adInteger, adParamInput) )
        If (CLng(nAlerta) = -1) Then
		    .Parameters("@Alerta").Value = Null
        Else		        
		    .Parameters("@Alerta").Value = nAlerta
        End If

		.Parameters.Append( .CreateParameter("@MoedaSistema", adBoolean, adParamInput) )
	    .Parameters("@MoedaSistema").Value = False

		.Parameters.Append( .CreateParameter("@TipoResultado", adInteger, adParamInput) )
		.Parameters("@TipoResultado").Value = nTipoResultado

	    Set rsData = .Execute

	End With

    Set rsSPCommand = Nothing
	rsData.Save Response, adPersistXML
	rsData.Close
    Set rsData = Nothing
%>
