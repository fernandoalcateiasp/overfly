
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalacompanhamentoHtml" name="modalacompanhamentoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modestrategico/subplanejamentoestrategico/orcamentos/modalpages/modalacompanhamento.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf

Dim i, nEmpresaID, nUsuarioID, nDireito

nEmpresaID = 0
nUsuarioID = 0
nDireito = 0

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

For i = 1 To Request.QueryString("nDireito").Count    
    nDireito = Request.QueryString("nDireito")(i)
Next

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nUsuarioID = " & CStr(nUsuarioID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nDireito = " & CStr(nDireito) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>
    
<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//js_modalacompanhamento_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
// js_modalacompanhamento_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_modalacompanhamento_DblClick();
//-->
</SCRIPT>

</head>

<body id="modalacompanhamentoBody" name="modalacompanhamentoBody" LANGUAGE="javascript" onload="return window_onload()">

    <div id="divFields" name="divFields" class="divGeneral">
        <p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
        <select id="selEmpresas" name="selEmpresas" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT b.PessoaID AS fldID, b.Fantasia AS fldName " & _
		 "FROM RelacoesPesRec a WITH(NOLOCK) " & _
		    "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) " & _
		 "WHERE(a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND a.EstaEmOperacao = 1 AND b.EstadoID = 2) " & _
		 "ORDER BY b.PessoaID "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	
	If ( CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaID) ) Then
		Response.Write " SELECTED "
	End If

	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblMeses" name="lblMeses" class="lblGeneral">Meses</p>
        <select id="selMeses" name="selMeses" class="fldGeneral" MULTIPLE>
            <option value=1>1</option>
            <option value=2>2</option>
            <option value=3>3</option>
            <option value=4>4</option>
            <option value=5>5</option>
            <option value=6>6</option>
            <option value=7>7</option>
            <option value=8>8</option>
            <option value=9>9</option>
            <option value=10>10</option>
            <option value=11>11</option>
            <option value=12>12</option>
        </select>
        <p id="lblContas" name="lblContas" class="lblGeneral">Contas</p>
        <select id="selContas" name="selContas" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT a.ContaID AS fldID, a.Conta AS fldName " & _
		 "FROM PlanoContas a WITH(NOLOCK) " & _
		 "WHERE (a.TemOrcamento = 1 AND (a.TipoContaID IS NULL OR a.TipoContaID IN (1503,1504))) " & _
		 "ORDER BY a.ContaID "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblResponsavel" name="lblResponsavel" class="lblGeneral">Responsável</p>
        <select id="selResponsavel" name="selResponsavel" class="fldGeneral">
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName " & _
         "UNION ALL " & _
         "SELECT DISTINCT c.PessoaID AS fldID, c.Fantasia AS fldName " & _
		 "FROM Orcamentos a WITH(NOLOCK) " & _
            "INNER JOIN Orcamentos_Contas b WITH(NOLOCK) ON (b.OrcamentoID = a.OrcamentoID) " & _
            "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = b.ResponsavelID) " & _
		 "ORDER BY fldName "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)

	If ( CLng(rsData.Fields("fldID").Value) = CLng(nUsuarioID) ) Then
		Response.Write " SELECTED "
	End If

	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblDicas" name="lblDicas" class="lblGeneral">Dic</p>
        <input type="checkbox" id="chkDicas" name="chkDicas" class="fldGeneral" title="Dicas das contas?"></input>
        <p id="lblPrevisto" name="lblPrevisto" class="lblGeneral">Pre</p>
        <input type="checkbox" id="chkPrevisto" name="chkPrevisto" class="fldGeneral" title="Valor previsto ou realizado?"></input>
        <p id="lblProjeta" name="lblProjeta" class="lblGeneral">Pro</p>
        <input type="checkbox" id="chkProjeta" name="chkProjeta" class="fldGeneral" title="Projeta os meses futuros?"></input>
        <p id="lblContabilidade" name="lblContabilidade" class="lblGeneral">Cont</p>
        <input type="checkbox" id="chkContabilidade" name="chkContabilidade" class="fldGeneral" title="Mostra a contabilidade?"></input>
        <p id="lblAlerta" name="lblAlerta" class="lblGeneral">Alerta</p>
        <select id="selAlerta" name="selAlerta" class="fldGeneral">
            <option value=-1></option>
            <option value=0>Normal</option>
            <option value=1>Alerta 1</option>
            <option value=2>Alerta 2</option>
        </select>
        <p id="lblAno" name="lblAno" class="lblGeneral">Ano</p>
        <select id="selAno" name="selAno" class="fldGeneral">
<%
Dim rsData, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT a.Ano AS fldID, CONVERT(VARCHAR(4), a.Ano) AS fldName " & _
		 "FROM Orcamentos a "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
		<input type="button" id="btnOK" name="btnOK" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnExcel" name="btnExcel" value="Excel" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnImprimir" name="btnImprimir" value="Imprimir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    </div>

    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" class="fldGeneral" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
