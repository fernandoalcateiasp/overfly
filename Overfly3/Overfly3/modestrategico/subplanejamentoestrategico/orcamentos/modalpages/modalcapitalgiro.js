/********************************************************************
modalcapitalgiro.js

Library javascript para o modalcapitalgiro.asp
********************************************************************/

/* INDICE DE FUNCOES ************************************************

window_onload()
btn_onclick(ctl)
setupPage()
loadDataAndTreatInterface()
cmbs_ondblclick()
buildStrParscapitalgiro(nTipo)
sendMails_DSC()

*********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_modcapitalgiroProm_Timer = null;
var glb_nOK = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

function sendMail_DSC()
{
    ;
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalcapitalgiroBody)
    {
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    configuraGrid();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    if (ctl.id == btnOK.id )
    {
        fillGridData();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null );
    /*else if (ctl.id == btnExcel.id)
        sendToExcel();*/
    else if (ctl.id == btnImprimir.id)
        imprimirGrid();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Capital de Giro', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;
    var nCmbsHeight = 75;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

	// ajusta elementos no divFields
    adjustElementsInForm([['lblEmpresas','selEmpresas',16,1,-10,-10],
						  ['lblOrigem','selOrigem',5,1],
						  ['lblGerentesProduto','selGerentesProduto',20,1],
						  ['lblMarcas','selMarcas',16,1],
						  ['lblDataInicio','txtDataInicio',10,1,-5],
						  ['lblDataFim','txtDataFim',10,1,-5],
						  ['lblPeriodoEstoque','selPeriodoEstoque',5,1,-6],
						  ['lblCompleto','chkCompleto',3,1,-5],
						  ['lblEstoque','chkEstoque',3,1,-5],
						  ['lblAlerta','selAlerta',10,2]],null,null,true);

    selEmpresas.style.height = nCmbsHeight;
    selOrigem.style.height = nCmbsHeight;
    selGerentesProduto.style.height = nCmbsHeight;
    selMarcas.style.height = nCmbsHeight;
    selAlerta.style.height = nCmbsHeight;
    selPeriodoEstoque.style.height = nCmbsHeight;

    lblAlerta.style.left = parseInt(lblDataInicio.currentStyle.left, 10);
    selAlerta.style.left = parseInt(txtDataInicio.currentStyle.left, 10);
	
    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);
        // altura e fixa para tres linhas de campos
        height = parseInt(selMarcas.currentStyle.top, 10) + 
				 parseInt(selMarcas.currentStyle.height, 10) + ELEM_GAP;
        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = modHeight - parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Editable = false;
    
    if (glb_nDireito == 0)
       selGerentesProduto.disabled = true;
    else       
       selGerentesProduto.disabled = false;

    if ((selGerentesProduto.value == 0) && (glb_nDireito == 0))
    {
        btnOK.disabled = true;
        btnExcel.disabled = true;
        btnImprimir.disabled = true;
    }
    else    
    {
        btnOK.disabled = false;
        btnExcel.disabled = false;
        btnImprimir.disabled = false;
    }

    btnOK.style.visibility = 'visible';

	btnOK.style.top = selAlerta.offsetTop;
    btnOK.style.left = parseInt(selAlerta.currentStyle.left, 10) +
		selAlerta.offsetWidth + ELEM_GAP - 4;
	btnOK.style.width = btnOK.offsetWidth - (3 * ELEM_GAP);

	btnExcel.style.top = selAlerta.offsetTop;
    btnExcel.style.left = parseInt(btnOK.currentStyle.left, 10) +
		btnOK.offsetWidth + ELEM_GAP - 4;
	btnExcel.style.width = btnOK.offsetWidth;

	btnImprimir.style.top = selAlerta.offsetTop;
    btnImprimir.style.left = parseInt(btnExcel.currentStyle.left, 10) +
		btnExcel.offsetWidth + ELEM_GAP - 4;
	btnImprimir.style.width = btnOK.offsetWidth;

	btnCanc.style.top = 0;
    btnCanc.style.left = 0;
    btnCanc.style.width = 0;
    btnCanc.style.height = 0;
    btnCanc.style.visibility = 'hidden';
}

/********************************************************************
Evento de dblclick dos combos que atuam como listbox.
	Dblclick no combo de paises preenche o combo de UFs.
	Dblclick no combo de UFS preenche o combo de Cidades.
********************************************************************/
function cmbs_ondblclick()
{

}

/********************************************************************
Constroi a string de parametros que eh enviada para a pagina 
/serversidegen/emails/sendcapitalgiro.asp
********************************************************************/
function buildStrParscapitalgiro(nTipo)
{
	var aCmbs;
	var i, j;
	var nItensSelected=0;
	var sStrPars='';
	var sSelecteds='';
	var nUserID = getCurrUserID();
	var sError;
	var sDataInicio = trimStr(txtDataInicio.value);
	var sDataFim = trimStr(txtDataFim.value);

    // Listar
    if (nTipo == 1)
	    aCmbs = [[selEmpresas, 'sEmpresas'],
		         [selOrigem,'sOrigem'],
		         [selGerentesProduto,'sGerentesProduto'],
		         [selMarcas,'sMarcas']];
    // Detalhes
    else
	    aCmbs = [[selEmpresas, 'sEmpresas']];
	
    // Percorre o array de combos
    for (i=0; i<aCmbs.length; i++)
    {
		for (j=0; j<(aCmbs[i][0]).length; j++)
		{
		    // Verifica se o item esta selecionado
		    if ( (aCmbs[i][0]).options[j].selected == true )
		    {
	            sSelecteds += (sSelecteds == '' ? '/' : '') + (aCmbs[i][0]).options[j].value + '/';
		        nItensSelected++;
		    }
		}
		
		if (sSelecteds != '')
		{
			sStrPars += (sStrPars == '' ? '?' : '&');
			sStrPars += aCmbs[i][1] + '=' + escape(sSelecteds);
		}

		sSelecteds = '';
    }

	sStrPars += (sStrPars == '' ? '?' : '&');

	if (sDataInicio != '')
		sDataInicio = normalizeDate_DateTime(sDataInicio, 1);

	if (sDataFim != '')
		sDataFim = normalizeDate_DateTime(sDataFim, 1);
	
	sStrPars += 'sDataInicio=' + escape(sDataInicio);
	sStrPars += '&sDataFim=' + escape(sDataFim);
	sStrPars += '&nPeriodoEstoque=' + escape(selPeriodoEstoque.value);
	sStrPars += '&nCompleto=' + escape(chkCompleto.checked ? '1' : '0');
	sStrPars += '&nEstoque=' + escape(chkEstoque.checked ? '1' : '0');
	sStrPars += '&nAlerta=' + escape(selAlerta.value);
	sStrPars += '&nTipoResultado=' + escape(nTipo);
	
	return sStrPars;
}

function configuraGrid()
{
    startGridInterface(fg);
    
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    
    with (fg)
    {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '8';
   		Editable = false;
		AllowUserResizing = 1;
		VirtualData = true;
		Ellipsis = 1;
		SelectionMode = 1;
		AllowSelection = false;
		Rows = 1;
		Cols = 9;
		FixedRows = 1;
		FixedCols = 1;
		ScrollBars = 3;
		OutLineBar = 5;
		GridLines = 0;
		FormatString = 'Campo' + '\t' + 
		               'Un' + '\t' + 
		               'Previsto' + '\t' + 
		               'Realizado' + '\t' + 
		               'Diferen�a' + '\t' + 
		               'AH' + '\t' + 
		               'PME P' + '\t' + 
		               'PME R' + '\t' + 
		               'N�vel';
		OutLineBar = 1;
        GridLinesFixed = 13;
		GridLines = 1;
		GridColor = 0X000000;		
		ColKey(0) = 'Campo';
		ColKey(1) = 'Unidade';
		ColKey(2) = 'ValorPrevisto';
		ColKey(3) = 'ValorRealizado';
		ColKey(4) = 'Diferenca';
		ColKey(5) = 'AnaliseHorizontal';
		ColKey(6) = 'PMEPrevisto';
		ColKey(7) = 'PMERealizado';
		ColKey(8) = 'Nivel';
		ColHidden(0) = false;
		ColHidden(1) = false;		
		ColHidden(2) = false;		
		ColHidden(3) = false;		
		ColHidden(4) = false;		
		ColHidden(5) = false;
		ColHidden(6) = true;
		ColHidden(7) = true;
		ColHidden(8) = true;
		ColFormat(2) = '(###,###,###,##0.00';		
		ColFormat(3) = '(###,###,###,##0.00';		
		ColFormat(4) = '(###,###,###,##0.00';		
		ColFormat(5) = '(###,##0.00';		
		ColFormat(6) = '(###,###,###,##0.00';		
		ColFormat(7) = '(###,###,###,##0.00';		
    }
    
    fg.Redraw = 2;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if ( glb_modcapitalgiroProm_Timer != null )    
	{
		window.clearInterval(glb_modcapitalgiroProm_Timer);
		
		glb_modcapitalgiroProm_Timer = null;
	}

	lockControlsInModalWin(true);
	
    var strPars = buildStrParscapitalgiro(1);

	dsoGrid.URL = SYS_ASPURLROOT + '/modestrategico/subplanejamentoestrategico/orcamentos/serverside/dadoscapitalgiro.asp' + strPars;
	dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    var dTFormat = '';
    var i, j;
    var nNivel = 0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Rows = 1;
    
    if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
        dsoGrid.recordset.MoveFirst();
    
	if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
	{
		glb_GridIsBuilding = true;
		while (!dsoGrid.recordset.EOF)
		{
			fg.Row = fg.Rows - 1;
			
		    fg.AddItem('', fg.Row+1);
			
		    if (fg.Row < (fg.Rows-1))
			    fg.Row++;
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Campo')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Campo')) = (dsoGrid.recordset('Campo').value == null ? '' : dsoGrid.recordset('Campo').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Unidade')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Unidade')) = (dsoGrid.recordset('Unidade').value == null ? '' : dsoGrid.recordset('Unidade').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'ValorPrevisto')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorPrevisto')) = (dsoGrid.recordset('ValorPrevisto').value == null ? '' : dsoGrid.recordset('ValorPrevisto').value);	
			
		    fg.ColDataType(getColIndexByColKey(fg, 'ValorRealizado')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorRealizado')) = (dsoGrid.recordset('ValorRealizado').value == null ? '' : dsoGrid.recordset('ValorRealizado').value);	
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Diferenca')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Diferenca')) = (dsoGrid.recordset('Diferenca').value == null ? '' : dsoGrid.recordset('Diferenca').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'AnaliseHorizontal')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AnaliseHorizontal')) = (dsoGrid.recordset('AnaliseHorizontal').value == null ? '' : dsoGrid.recordset('AnaliseHorizontal').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'PMEPrevisto')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PMEPrevisto')) = (dsoGrid.recordset('PMEPrevisto').value == null ? '' : dsoGrid.recordset('PMEPrevisto').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'PMERealizado')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PMERealizado')) = (dsoGrid.recordset('PMERealizado').value == null ? '' : dsoGrid.recordset('PMERealizado').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) = (dsoGrid.recordset('Nivel').value == null ? '' : dsoGrid.recordset('Nivel').value);	

		    fg.IsSubTotal(fg.Row) = true;
		    fg.RowOutlineLevel(fg.Row) = dsoGrid.recordset('Nivel').value;
            
            dsoGrid.recordset.MoveNext();
		}
	}

    paintGrid();
    
	glb_GridIsBuilding = false;
    
    alignColsInGrid(fg,[2,3,4,5,6,7]);    
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    for (i=0; i<fg.Rows; i++ )
    {
        nNivel = getCellValueByColKey(fg, 'Nivel', i);

        if ( (fg.IsSubTotal(i) == true) && (nNivel >= 0) )
            fg.IsCollapsed(i) = 2;
    }

    if (chkEstoque.checked)
    {
        fg.ColHidden(6) = false;
        fg.ColHidden(7) = false;
    }
    else            
    {
        fg.ColHidden(6) = true;
        fg.ColHidden(7) = true;
    }

    fg.Redraw = 2;
    
    lockControlsInModalWin(false);
	    
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }            
}

function js_modalcapitalgiro_DblClick()
{
    if (( ( (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID')) == 4) || 
            (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID')) == 6)) && 
          (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')) > 0) && 
          (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) == 3)))
    {
        fillGridDetalhe();
    }
}

function paintGrid()
{
    var i;

    for ( i=1; i<fg.Rows; i++ )
    {

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorPrevisto')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'ValorPrevisto'), i, getColIndexByColKey(fg, 'ValorPrevisto'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorRealizado')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'ValorRealizado'), i, getColIndexByColKey(fg, 'ValorRealizado'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Diferenca')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Diferenca'), i, getColIndexByColKey(fg, 'Diferenca'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'AnaliseHorizontal')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'AnaliseHorizontal'), i, getColIndexByColKey(fg, 'AnaliseHorizontal'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }
    }
}
function imprimirGrid()
{
	fg.ColWidth(getColIndexByColKey(fg, 'ValorPrevisto')) = 10 * 145;
	fg.ColWidth(getColIndexByColKey(fg, 'ValorRealizado')) = 10 * 145;
	fg.ColWidth(getColIndexByColKey(fg, 'Diferenca')) = 10 * 145;

	var gridLine = fg.gridLines;
	fg.gridLines = 2;

	fg.PrintGrid('Capital de Giro', false, 1, 0, 450);
	
	fg.gridLines = gridLine;
	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}

/********************************************************************
Constroi a string dos �tens selecionados em um combo
********************************************************************/
function buildStrCmbs(Cmb)
{
	var i;
	var nItensSelected=0;
	var sSelecteds = '';

	for (i=0; i<Cmb.length; i++)
	{
	    // Verifica se o item esta selecionado
	    if ( Cmb.options[i].selected == true )
            sSelecteds += (sSelecteds == '' ? '/' : '') + Cmb.options[i].value + '/';
	}
		
    return sSelecteds;
}
