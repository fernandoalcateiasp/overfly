
<%@ LANGUAGE=VBSCRIPT @EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalcapitalgiroHtml" name="modalcapitalgiroHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modestrategico/subplanejamentoestrategico/orcamentos/modalpages/modalcapitalgiro.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf

Dim i, nEmpresaID, nUsuarioID, nDireito, rsData, strSQL

nEmpresaID = 0
nUsuarioID = 0
nDireito = 0

For i = 1 To Request.QueryString("nEmpresaID").Count    
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count    
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

For i = 1 To Request.QueryString("nDireito").Count    
    nDireito = Request.QueryString("nDireito")(i)
Next

Response.Write "var glb_nEmpresaID = " & CStr(nEmpresaID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nUsuarioID = " & CStr(nUsuarioID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nDireito = " & CStr(nDireito) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>


<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
js_fg_EnterCell (fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//js_modalcapitalgiro_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
// js_modalcapitalgiro_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
js_modalcapitalgiro_DblClick();
//-->
</SCRIPT>

</head>

<body id="modalcapitalgiroBody" name="modalcapitalgiroBody" LANGUAGE="javascript" onload="return window_onload()">

    <div id="divFields" name="divFields" class="divGeneral">
        <p id="lblEmpresas" name="lblEmpresas" class="lblGeneral">Empresas</p>
        <select id="selEmpresas" name="selEmpresas" class="fldGeneral" MULTIPLE>
<%

Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT b.PessoaID AS fldID, b.Fantasia AS fldName " & _
		 "FROM RelacoesPesRec a WITH(NOLOCK) " & _
		    "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) " & _
		 "WHERE(a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND a.EstaEmOperacao = 1 AND b.EstadoID = 2) " & _
		 "ORDER BY b.PessoaID "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	
	If ( CLng(rsData.Fields("fldID").Value) = CLng(nEmpresaID) ) Then
		Response.Write " SELECTED "
	End If

	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblOrigem" name="lblOrigem" class="lblGeneral">Origem</p>
        <select id="selOrigem" name="selOrigem" class="fldGeneral" MULTIPLE>
			<option value="0">0-Nac</option>
			<option value="1">1-ID</option>
			<option value="2">2-IT</option>
        </select>
        <p id="lblGerentesProduto" name="lblGerentesProduto" class="lblGeneral">Gerentes Produto</p>
        <select id="selGerentesProduto" name="selGerentesProduto" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT b.PessoaID AS fldID, b.Fantasia AS fldName " & _
		 "FROM RelacoesPesCon a WITH(NOLOCK) " & _
		    "INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ProprietarioID) " & _
		 "WHERE (a.TipoRelacaoID = 61 AND a.EstadoID NOT IN (4,5)) " & _
		 "ORDER BY b.Fantasia "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)

	If ( CLng(rsData.Fields("fldID").Value) = CLng(nUsuarioID) ) Then
		Response.Write " SELECTED "
	End If

	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblMarcas" name="lblMarcas" class="lblGeneral">Marcas</p>
        <select id="selMarcas" name="selMarcas" class="fldGeneral" MULTIPLE>
<%
Set rsData = Server.CreateObject("ADODB.Recordset")

strSQL = "SELECT DISTINCT c.ConceitoID AS fldID, c.Conceito AS fldName " & _
		 "FROM RelacoesPesCon a WITH(NOLOCK) " & _
		    "INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) " & _
		    "INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.MarcaID) " & _
		 "WHERE (a.TipoRelacaoID = 61 AND a.EstadoID NOT IN (4,5)) " & _
		 "ORDER BY c.Conceito "

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

While Not rsData.EOF
    Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
	Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
    rsData.MoveNext
Wend

rsData.Close
Set rsData = Nothing
%>
        </select>
        <p id="lblDataInicio" name="lblDataInicio" class="lblGeneral">In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" class="fldGeneral"></input>
        <p id="lblDataFim" name="lblDataFim" class="lblGeneral">Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" class="fldGeneral"></input>
        <p id="lblPeriodoEstoque" name="lblPeriodoEstoque" class="lblGeneral" title="Per�odo para an�lise do estoque (dias)">PE</p>
        <select id="selPeriodoEstoque" name="selPeriodoEstoque" class="fldGeneral">
			<option value="0"></option>
			<option value="15">15</option>
			<option value="30">30</option>
        </select>
        <p id="lblCompleto" name="lblCompleto" class="lblGeneral">Com</p>
        <input type="checkbox" id="chkCompleto" name="chkCompleto" class="fldGeneral" title="Completo?"></input>
        <p id="lblEstoque" name="lblEstoque" class="lblGeneral">Estq</p>
        <input type="checkbox" id="chkEstoque" name="chkEstoque" class="fldGeneral" title="An�lise de Estoque?"></input>
        <p id="lblAlerta" name="lblAlerta" class="lblGeneral">Alerta</p>
        <select id="selAlerta" name="selAlerta" class="fldGeneral">
            <option value=-1></option>
            <option value=0>Normal</option>
            <option value=1>Alerta 1</option>
            <option value=2>Alerta 2</option>
        </select>
		<input type="button" id="btnOK" name="btnOK" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnExcel" name="btnExcel" value="Excel" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnImprimir" name="btnImprimir" value="Imprimir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    </div>

    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" class="fldGeneral" VIEWASTEXT>
        </object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
