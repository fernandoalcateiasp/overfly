/********************************************************************
modalacompanhamento.js

Library javascript para o modalacompanhamento.asp
********************************************************************/

/* INDICE DE FUNCOES ************************************************

window_onload()
btn_onclick(ctl)
setupPage()
loadDataAndTreatInterface()
cmbs_ondblclick()
buildStrParsAcompanhamento(nTipo)
sendMails_DSC()

*********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_modAcompanhamentoProm_Timer = null;
var glb_nOK = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   

    // ajusta o body do html
    with (modalacompanhamentoBody)
    {
        style.backgroundColor = 'white';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    configuraGrid();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    if (ctl.id == btnOK.id )
    {
        fillGridData();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null );
    /*else if (ctl.id == btnExcel.id)
        sendToExcel();*/
    else if (ctl.id == btnImprimir.id)
        imprimirGrid();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Acompanhamento', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;
    var nCmbsHeight = 75;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

	// ajusta elementos no divFields
    adjustElementsInForm([['lblEmpresas','selEmpresas',16,1,-10,-10],
						  ['lblMeses','selMeses',5,1],
						  ['lblContas','selContas',30,1],
						  ['lblResponsavel','selResponsavel',20,1],
						  ['lblDicas','chkDicas',3,1],
						  ['lblPrevisto','chkPrevisto',3,1,-5],
						  ['lblProjeta','chkProjeta',3,1,-5],
						  ['lblContabilidade','chkContabilidade',3,1,-5],
						  ['lblAlerta','selAlerta',10,2],
						  ['lblAno','selAno',7,2]],null,null,true);

    selEmpresas.style.height = nCmbsHeight;
    selMeses.style.height = nCmbsHeight;
    selContas.style.height = nCmbsHeight;
    selResponsavel.style.height = nCmbsHeight;
    selAlerta.style.height = nCmbsHeight;
    selAno.style.height = nCmbsHeight;

    lblAlerta.style.left = parseInt(lblResponsavel.currentStyle.left, 10);
    selAlerta.style.left = parseInt(selResponsavel.currentStyle.left, 10);
	
    selAno.style.left = parseInt(selAlerta.currentStyle.left, 10) + parseInt(selAlerta.offsetWidth, 10) + ELEM_GAP;
    lblAno.style.left = parseInt(selAno.currentStyle.left, 10);

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);
        // altura e fixa para tres linhas de campos
        height = parseInt(selResponsavel.currentStyle.top, 10) + 
				 parseInt(selResponsavel.currentStyle.height, 10) + ELEM_GAP;
        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = modHeight - parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Editable = false;

    if (glb_nDireito == 0)
       selResponsavel.disabled = true;
    else       
       selResponsavel.disabled = false;

    if ((selResponsavel.value == 0) && (glb_nDireito == 0))
    {
        btnOK.disabled = true;
        btnExcel.disabled = true;
        btnImprimir.disabled = true;
    }
    else
    {
       btnOK.disabled = false;
       btnExcel.disabled = false;
       btnImprimir.disabled = false;
    }

    btnOK.style.visibility = 'visible';

	btnOK.style.top = selAno.offsetTop;
    btnOK.style.left = parseInt(selAno.currentStyle.left, 10) +
		selAno.offsetWidth + ELEM_GAP - 4;
	btnOK.style.width = btnOK.offsetWidth - (3 * ELEM_GAP);
    
	btnExcel.style.top = selAno.offsetTop;
    btnExcel.style.left = parseInt(btnOK.currentStyle.left, 10) +
		btnOK.offsetWidth + ELEM_GAP - 4;
	btnExcel.style.width = btnOK.offsetWidth;

	btnImprimir.style.top = selAno.offsetTop;
    btnImprimir.style.left = parseInt(btnExcel.currentStyle.left, 10) +
		btnExcel.offsetWidth + ELEM_GAP - 4;
	btnImprimir.style.width = btnOK.offsetWidth;

	btnCanc.style.top = 0;
    btnCanc.style.left = 0;
    btnCanc.style.width = 0;
    btnCanc.style.height = 0;
    btnCanc.style.visibility = 'hidden';
}

/********************************************************************
Evento de dblclick dos combos que atuam como listbox.
	Dblclick no combo de paises preenche o combo de UFs.
	Dblclick no combo de UFS preenche o combo de Cidades.
********************************************************************/
function cmbs_ondblclick()
{

}

/********************************************************************
Constroi a string de parametros que eh enviada para a pagina 
/serversidegen/emails/sendacompanhamento.asp
********************************************************************/
function buildStrParsAcompanhamento(nTipo)
{

	var aCmbs;
	var i, j;
	var nItensSelected=0;
	var sStrPars='';
	var sSelecteds='';
	var nUserID = getCurrUserID();
	var sError;

    // Listar
    if (nTipo == 1)
	    aCmbs = [[selEmpresas, 'sEmpresas'],
		         [selMeses,'sMeses'],
		         [selContas,'sContas']];
    // Detalhes
    else
	    aCmbs = [[selEmpresas, 'sEmpresas']];
	
    // Percorre o array de combos
    for (i=0; i<aCmbs.length; i++)
    {
		for (j=0; j<(aCmbs[i][0]).length; j++)
		{
		    // Verifica se o item esta selecionado
		    if ( (aCmbs[i][0]).options[j].selected == true )
		    {
                if (aCmbs[i][0] == selMeses)
		            sSelecteds += (sSelecteds == '' ? '/' : '') + (aCmbs[i][0]).options[j].innerText + '/';
                else
		            sSelecteds += (sSelecteds == '' ? '/' : '') + (aCmbs[i][0]).options[j].value + '/';
                
		        nItensSelected++;
		    }
		}
		
		if (sSelecteds != '')
		{
			sStrPars += (sStrPars == '' ? '?' : '&');
			sStrPars += aCmbs[i][1] + '=' + escape(sSelecteds);
		}

		sSelecteds = '';
    }

	sStrPars += (sStrPars == '' ? '?' : '&');
	
	sStrPars += 'nAno=' + escape(selAno.value);
	sStrPars += '&nResponsavelID=' + escape(selResponsavel.value);
	sStrPars += '&nPrevisto=' + escape(chkPrevisto.checked ? '1' : '0');
	sStrPars += '&nProjeta=' + escape(chkProjeta.checked ? '1' : '0');
	sStrPars += '&nContabilidade=' + escape(chkContabilidade.checked ? '1' : '0');
	sStrPars += '&nAlerta=' + escape(selAlerta.value);
	sStrPars += '&nTipoResultado=' + escape(nTipo);
	
	if (nTipo == 2)
    {
	    sStrPars += '&sMeses=' + escape(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Campo')));
	    sStrPars += '&sContas=' + escape(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')));
    }

	return sStrPars;
}

function configuraGrid()
{
    startGridInterface(fg);
    
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;    
    
    with (fg)
    {
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '8';
   		Editable = false;
		AllowUserResizing = 1;
		VirtualData = true;
		Ellipsis = 1;
		SelectionMode = 1;
		AllowSelection = false;
		Rows = 1;
		Cols = 14;
		FixedRows = 1;
		FixedCols = 1;
		ScrollBars = 3;
		OutLineBar = 5;
		GridLines = 0;
		FormatString = 'Campo' + '\t' + 
		               'Previsto' + '\t' + 
		               'Realizado' + '\t' + 
		               'Diferen�a' + '\t' + 
		               'AH' + '\t' + 
		               'AV1' + '\t' + 
		               'AV2' + '\t' + 
		               'Respons�veis' + '\t' + 
		               'N�vel' + '\t' + 
		               'Alerta1' + '\t' + 
		               'Alerta2' + '\t' + 
		               'Cor' + '\t' + 
		               'TipoID' + '\t' + 
		               'ContaID';
		OutLineBar = 1;
        GridLinesFixed = 13;
		GridLines = 1;
		GridColor = 0X000000;		
		ColKey(0) = 'Campo';
		ColKey(1) = 'ValorPrevisto';
		ColKey(2) = 'ValorRealizado';
		ColKey(3) = 'Diferenca';
		ColKey(4) = 'AnaliseHorizontal';
		ColKey(5) = 'AnaliseVertical1';
		ColKey(6) = 'AnaliseVertical2';
		ColKey(7) = 'Responsaveis';
		ColKey(8) = 'Nivel';
		ColKey(9) = 'Alerta1';
		ColKey(10) = 'Alerta2';
		ColKey(11) = 'Cor';
		ColKey(12) = 'TipoID';
		ColKey(13) = 'ContaID';
		ColHidden(0) = false;
		ColHidden(1) = false;		
		ColHidden(2) = false;		
		ColHidden(3) = false;		
		ColHidden(4) = false;		
		ColHidden(5) = false;
		ColHidden(6) = false;
		ColHidden(7) = false;
		ColHidden(8) = true;
		ColHidden(9) = true;
		ColHidden(10) = true;
		ColHidden(11) = true;
		ColHidden(12) = true;
		ColHidden(13) = true;
		ColFormat(1) = '(###,###,###,##0.00';		
		ColFormat(2) = '(###,###,###,##0.00';		
		ColFormat(3) = '(###,###,###,##0.00';		
		ColFormat(4) = '(###,##0.00';		
		ColFormat(5) = '(###,##0.00';		
		ColFormat(6) = '(###,##0.00';		
    }
    
    fg.Redraw = 2;
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
	if ( glb_modAcompanhamentoProm_Timer != null )    
	{
		window.clearInterval(glb_modAcompanhamentoProm_Timer);
		
		glb_modAcompanhamentoProm_Timer = null;
	}

	lockControlsInModalWin(true);
	
    var strPars = buildStrParsAcompanhamento(1);

	dsoGrid.URL = SYS_ASPURLROOT + '/modestrategico/subplanejamentoestrategico/orcamentos/serverside/dadosacompanhamento.asp' + strPars;
	dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    var dTFormat = '';
    var i, j;
    var nNivel = 0;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    fg.Rows = 1;
    
    if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
        dsoGrid.recordset.MoveFirst();
    
	if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
	{
		glb_GridIsBuilding = true;
		while (!dsoGrid.recordset.EOF)
		{
			fg.Row = fg.Rows - 1;
			
		    fg.AddItem('', fg.Row+1);
			
		    if (fg.Row < (fg.Rows-1))
			    fg.Row++;
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Campo')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Campo')) = (dsoGrid.recordset('Campo').value == null ? '' : dsoGrid.recordset('Campo').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'ValorPrevisto')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorPrevisto')) = (dsoGrid.recordset('ValorPrevisto').value == null ? '' : dsoGrid.recordset('ValorPrevisto').value);	
			
		    fg.ColDataType(getColIndexByColKey(fg, 'ValorRealizado')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorRealizado')) = (dsoGrid.recordset('ValorRealizado').value == null ? '' : dsoGrid.recordset('ValorRealizado').value);	
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Diferenca')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Diferenca')) = (dsoGrid.recordset('Diferenca').value == null ? '' : dsoGrid.recordset('Diferenca').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'AnaliseHorizontal')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AnaliseHorizontal')) = (dsoGrid.recordset('AnaliseHorizontal').value == null ? '' : dsoGrid.recordset('AnaliseHorizontal').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'AnaliseVertical1')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AnaliseVertical1')) = (dsoGrid.recordset('AnaliseVertical1').value == null ? '' : dsoGrid.recordset('AnaliseVertical1').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'AnaliseVertical2')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AnaliseVertical2')) = (dsoGrid.recordset('AnaliseVertical2').value == null ? '' : dsoGrid.recordset('AnaliseVertical2').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Responsaveis')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Responsaveis')) = (dsoGrid.recordset('Responsaveis').value == null ? '' : dsoGrid.recordset('Responsaveis').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) = (dsoGrid.recordset('Nivel').value == null ? '' : dsoGrid.recordset('Nivel').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Alerta1')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Alerta1')) = (dsoGrid.recordset('Alerta1').value == null ? 0 : dsoGrid.recordset('Alerta1').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Alerta2')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Alerta2')) = (dsoGrid.recordset('Alerta2').value == null ? 0 : dsoGrid.recordset('Alerta2').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Cor')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Cor')) = (dsoGrid.recordset('Cor').value == null ? '' : dsoGrid.recordset('Cor').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'TipoID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID')) = (dsoGrid.recordset('TipoID').value == null ? '' : dsoGrid.recordset('TipoID').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'ContaID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')) = (dsoGrid.recordset('ContaID').value == null ? '' : dsoGrid.recordset('ContaID').value);	

		    fg.IsSubTotal(fg.Row) = true;
		    fg.RowOutlineLevel(fg.Row) = dsoGrid.recordset('Nivel').value;
            
            dsoGrid.recordset.MoveNext();
		}
	}

    paintGrid();
    
	glb_GridIsBuilding = false;
    
    alignColsInGrid(fg,[1,2,3,4,5,6]);    
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    for (i=0; i<fg.Rows; i++ )
    {
        nNivel = getCellValueByColKey(fg, 'Nivel', i);

        if ( (fg.IsSubTotal(i) == true) && (nNivel >= 0) )
            fg.IsCollapsed(i) = 2;
    }

    fg.Redraw = 2;
    
    lockControlsInModalWin(false);
	    
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }            
}

function js_modalacompanhamento_DblClick()
{
    if (( ( (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID')) == 4) || 
            (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID')) == 6)) && 
          (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')) > 0) && 
          (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) == 3)))
    {
        fillGridDetalhe();
    }
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridDetalhe()
{
	if ( glb_modAcompanhamentoProm_Timer != null )    
	{
		window.clearInterval(glb_modAcompanhamentoProm_Timer);
		
		glb_modAcompanhamentoProm_Timer = null;
	}

	lockControlsInModalWin(true);
	
    var strPars = buildStrParsAcompanhamento(2);

	dsoGrid.URL = SYS_ASPURLROOT + '/modestrategico/subplanejamentoestrategico/orcamentos/serverside/dadosacompanhamento.asp' + strPars;
	dsoGrid.ondatasetcomplete = fillGridDetalhe_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridDetalhe_DSC()
{
    var dTFormat = '';
    var i, j;
    var nNivel = 0;
    var nRow = fg.Row;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
        
    fg.Redraw = 0;
    
    if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
        dsoGrid.recordset.MoveFirst();
    
	if ( !(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) ) 
	{
		glb_GridIsBuilding = true;

		while (!dsoGrid.recordset.EOF)
		{
			fg.Row = nRow;
			
		    fg.AddItem('', fg.Row+1);
			
		    if (fg.Row < (fg.Rows-1))
			    fg.Row++;
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Campo')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Campo')) = (dsoGrid.recordset('Campo').value == null ? '' : dsoGrid.recordset('Campo').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'ValorPrevisto')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorPrevisto')) = (dsoGrid.recordset('ValorPrevisto').value == null ? '' : dsoGrid.recordset('ValorPrevisto').value);	
			
		    fg.ColDataType(getColIndexByColKey(fg, 'ValorRealizado')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ValorRealizado')) = (dsoGrid.recordset('ValorRealizado').value == null ? '' : dsoGrid.recordset('ValorRealizado').value);	
			
		    fg.ColDataType(getColIndexByColKey(fg, 'Diferenca')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Diferenca')) = (dsoGrid.recordset('Diferenca').value == null ? '' : dsoGrid.recordset('Diferenca').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'AnaliseHorizontal')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AnaliseHorizontal')) = (dsoGrid.recordset('AnaliseHorizontal').value == null ? '' : dsoGrid.recordset('AnaliseHorizontal').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'AnaliseVertical1')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AnaliseVertical1')) = (dsoGrid.recordset('AnaliseVertical1').value == null ? '' : dsoGrid.recordset('AnaliseVertical1').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'AnaliseVertical2')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'AnaliseVertical2')) = (dsoGrid.recordset('AnaliseVertical2').value == null ? '' : dsoGrid.recordset('AnaliseVertical2').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Responsaveis')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Responsaveis')) = (dsoGrid.recordset('Responsaveis').value == null ? '' : dsoGrid.recordset('Responsaveis').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Nivel')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Nivel')) = (dsoGrid.recordset('Nivel').value == null ? '' : dsoGrid.recordset('Nivel').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Alerta1')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Alerta1')) = (dsoGrid.recordset('Alerta1').value == null ? 0 : dsoGrid.recordset('Alerta1').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Alerta2')) = 11;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Alerta2')) = (dsoGrid.recordset('Alerta2').value == null ? 0 : dsoGrid.recordset('Alerta2').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'Cor')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Cor')) = (dsoGrid.recordset('Cor').value == null ? '' : dsoGrid.recordset('Cor').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'TipoID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID')) = (dsoGrid.recordset('TipoID').value == null ? '' : dsoGrid.recordset('TipoID').value);	

		    fg.ColDataType(getColIndexByColKey(fg, 'ContaID')) = 12;
		    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ContaID')) = (dsoGrid.recordset('ContaID').value == null ? '' : dsoGrid.recordset('ContaID').value);	

		    fg.IsSubTotal(fg.Row) = true;
		    fg.RowOutlineLevel(fg.Row) = dsoGrid.recordset('Nivel').value;
            
			nRow++;
            dsoGrid.recordset.MoveNext();
		}
	}

	glb_GridIsBuilding = false;
    
    alignColsInGrid(fg,[1,2,3,4,5,6]);    
    
    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

    fg.Redraw = 2;
    
    lockControlsInModalWin(false);
	    
    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    }            
}

function paintGrid()
{
    var i;

    for ( i=1; i<fg.Rows; i++ )
    {

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorPrevisto')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'ValorPrevisto'), i, getColIndexByColKey(fg, 'ValorPrevisto'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorRealizado')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'ValorRealizado'), i, getColIndexByColKey(fg, 'ValorRealizado'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'Diferenca')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Diferenca'), i, getColIndexByColKey(fg, 'Diferenca'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'AnaliseHorizontal')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'AnaliseHorizontal'), i, getColIndexByColKey(fg, 'AnaliseHorizontal'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'AnaliseVertical1')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'AnaliseVertical1'), i, getColIndexByColKey(fg, 'AnaliseVertical1'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.ValueMatrix(i, getColIndexByColKey(fg, 'AnaliseVertical2')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'AnaliseVertical2'), i, getColIndexByColKey(fg, 'AnaliseVertical2'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
			fg.FillStyle = 0;
        }

		if ( fg.TextMatrix(i, getColIndexByColKey(fg, 'Cor')) != '' )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Campo'), i, getColIndexByColKey(fg, 'Campo'));
			fg.FillStyle = 1;
            fg.CellBackColor = eval(fg.TextMatrix(i, getColIndexByColKey(fg, 'Cor')));
			fg.FillStyle = 0;
        }
    }
}
function imprimirGrid()
{
	fg.ColWidth(getColIndexByColKey(fg, 'ValorPrevisto')) = 10 * 145;
	fg.ColWidth(getColIndexByColKey(fg, 'ValorRealizado')) = 10 * 145;
	fg.ColWidth(getColIndexByColKey(fg, 'Diferenca')) = 10 * 145;

	var gridLine = fg.gridLines;
	fg.gridLines = 2;

	fg.PrintGrid('Acompanhamento', false, 1, 0, 450);
	
	fg.gridLines = gridLine;
	fg.AutoSizeMode = 0;
	fg.AutoSize(0,fg.Cols-1);
    fg.Redraw = 1;
}

/********************************************************************
Constroi a string dos �tens selecionados em um combo
********************************************************************/
function buildStrCmbs(Cmb)
{
	var i;
	var nItensSelected=0;
	var sSelecteds = '';

	for (i=0; i<Cmb.length; i++)
	{
	    // Verifica se o item esta selecionado
	    if ( Cmb.options[i].selected == true )
            sSelecteds += (sSelecteds == '' ? '/' : '') + Cmb.options[i].value + '/';
	}
		
    return sSelecteds;
}
