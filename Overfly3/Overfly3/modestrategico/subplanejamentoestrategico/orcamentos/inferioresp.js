/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de orcamentos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.ProprietarioID, a.AlternativoID, a.Observacoes FROM Orcamentos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.OrcamentoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Previs�es Mensais
    else if (folderID == 22201)
    {
        dso.SQL = 'SELECT a.* ' + 
                  'FROM Orcamentos_PrevisoesMensais a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.OrcamentoID = '+ idToFind + ' ' +
                  'ORDER BY a.Mes';
        return 'dso01Grid_DSC';
    }
    // Contas
    else if (folderID == 22202)
    {
        dso.SQL = 'SELECT a.* ' + 
                  'FROM Orcamentos_Contas a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.OrcamentoID = '+ idToFind + ' ' +
                  'ORDER BY a.ContaID';
        return 'dso01Grid_DSC';
    }
    // Previs�es Vendas
    else if (folderID == 22203)
    {
        dso.SQL = 'SELECT a.* ' + 
                  'FROM Orcamentos_PrevisoesVendas a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.OrcamentoID = '+ idToFind + ' ' +
                  'ORDER BY (SELECT aa.Conceito FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.MarcaID)), ' +
                        '(SELECT aa.Conceito FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.LinhaProdutoID)), ' +
                        'a.Mes';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
	var nEmpresaData = getCurrEmpresaData();
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Contas
    else if (pastaID == 22202)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
				'FROM RelacoesPesRec a WITH(NOLOCK) ' +
					'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.SujeitoID) ' +
				'WHERE (a.TipoRelacaoID = 11 AND a.EstadoID = 2 AND b.EstadoID = 2) ' +
				'ORDER BY fldName';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT DISTINCT a.OrcContaID, b.Conta AS Conta ' +
				'FROM Orcamentos_Contas a WITH(NOLOCK) ' +
                    'INNER JOIN PlanoContas b WITH(NOLOCK) ON (b.ContaID = a.ContaID) ' +
				'WHERE (a.OrcamentoID=' + nRegistroID + ') ' +
				'ORDER BY a.OrcContaID';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT DISTINCT a.OrcContaID, c.ItemAbreviado AS TipoVariacao ' +
				'FROM Orcamentos_Contas a WITH(NOLOCK) ' +
                    'INNER JOIN PlanoContas b WITH(NOLOCK) ON (b.ContaID = a.ContaID) ' +
                    'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = b.TipoVariacaoID) ' +
				'WHERE (a.OrcamentoID=' + nRegistroID + ') ' +
				'ORDER BY a.OrcContaID';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT DISTINCT a.OrcContaID, dbo.fn_OrcConta_Totais(a.OrcContaID, NULL, 1, 0, 1, 0) AS ValorEfetivo ' +
				'FROM Orcamentos_Contas a WITH(NOLOCK) ' +
				'WHERE (a.OrcamentoID=' + nRegistroID + ') ' +
				'ORDER BY a.OrcContaID';
        }
    }
    // Previs�es Vendas
    else if (pastaID == 22203)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT a.ConceitoID AS fldID, a.Conceito AS fldName ' +
				'FROM Conceitos a WITH(NOLOCK) ' +
				'WHERE (a.EstadoID = 2 AND a.TipoConceitoID = 304) ' +
				'ORDER BY fldName';
        }
        else if ( dso.id == 'dsoCmb02Grid_01' )
        {
            dso.SQL = 'SELECT DISTINCT a.ConceitoID AS fldID, a.Conceito AS fldName ' +
				'FROM Conceitos a WITH(NOLOCK) ' +
				'WHERE (a.EstadoID = 2 AND a.TipoConceitoID = 310) ' +
				'ORDER BY fldName';
        }
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(22201);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 22201); //Previs�es Mensais

    vPastaName = window.top.getPastaName(22202);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 22202); //Contas

    vPastaName = window.top.getPastaName(22203);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 22203); //Previs�es de Vendas

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        // Previs�es Mensais
        if (folderID == 22201)
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1,2], ['OrcamentoID', registroID]);
        // Contas
        else if (folderID == 22202)
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,3], ['OrcamentoID', registroID]);
        // Previs�es Vendas
        else if (folderID == 22203)
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[2,4,5,6,7,8,9,10,11], ['OrcamentoID', registroID]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

    // Previs�es Mensais
    else if (folderID == 22201)
    {
        headerGrid(fg,['M�s',
					   'Faturamento',
					   'Contribui��o',
					   'Funcion�rios',
                       'OrcPrevisaoID'], [4]);

        fillGridMask(fg,currDSO,['Mes',
								 'Faturamento',
								 'Contribuicao',
								 'Funcionarios',
								 'OrcPrevisaoID'],
								 ['99','999999999999.99','999999999999.99','9999',''],
								 ['##','###,###,###,##0.00','###,###,###,##0.00','####','']);


		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[0,'##','C'], [1,'###,###,###,##0.00','S'], [2,'###,###,###,##0.00','S']]);

	    alignColsInGrid(fg,[0,1,2,3,4]);
		
		fg.FrozenCols = 1 ;
    }
    // Contas
    else if (folderID == 22202)
    {
        headerGrid(fg,['ContaID',
					   'Conta',
					   'Varia��o',
					   'Valor',
					   'Valor Efetivo',
					   'Perc Alerta 1',
					   'Perc Alerta 2',
					   'TL',
					   'TG',
					   'Respons�vel',
                       'OrcContaID'], [10]);
        
        glb_aCelHint = [[0, 5, 'Percentual para emitir o primeiro alerta.'],
						[0, 6, 'Percentual para emitir o segundo alerta.'],
                        [0, 7, 'Trava financeiro considerando o saldo na empresa?'],
                        [0, 8, 'Trava financeiro considerando o saldo em todas as empresas?']];

        fillGridMask(fg,currDSO,['ContaID*',
								 '^OrcContaID^dso01GridLkp^OrcContaID^Conta*',
								 '^OrcContaID^dso02GridLkp^OrcContaID^TipoVariacao*',
								 'Valor',
								 '^OrcContaID^dso03GridLkp^OrcContaID^ValorEfetivo*',
								 'PercentualAlerta1',
								 'PercentualAlerta2',
								 'TravaLocal',
								 'TravaGlobal',
								 'ResponsavelID',
								 'OrcContaID'],
								 ['','','','#9999999999999.999999','','9999.99','9999.99','','','',''],
								 ['','','','##,###,###,###,##0.000000','###,###,###,##0.00','###,0.00','###,0.00','','','','']);

		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[0,'###','C'], [4,'###,###,###,##0.00','S']]);

	    alignColsInGrid(fg,[0,3,4,5,6]);
		
		fg.FrozenCols = 1 ;
    }
    // Previs�es Vendas
    else if (folderID == 22203)
    {
        headerGrid(fg,['Marca',
                       'Linha de Produto',
                       'M�s',
                       'Unidades',
					   'Faturamento',
					   'Contribui��o',
					   'Imp E',
					   'Imp S',
					   'PMRV',
					   'PME',
					   'PMPC',
					   'PMEC',
					   'Observa��o',
                       'OrcPrevisaoID'], [13]);

        glb_aCelHint = [[0, 6, 'Impostos Entrada'],
						[0, 7, 'Impostos Sa�das'],
                        [0, 8, 'Prazo M�dio Recebimento Vendas'],
                        [0, 9, 'Prazo M�dio Estoque'],
                        [0, 10, 'Prazo M�dio Pagamento Compras'],
						[0, 11, 'Prazo M�dio Entrega Compras']];
        
        fillGridMask(fg,currDSO,['MarcaID',
                                 'LinhaProdutoID',
                                 'Mes',
                                 'Unidades',
								 'Faturamento',
								 'Contribuicao',
								 'ImpostosEntrada',
								 'ImpostosSaida',
								 'PrazoMedioRecebimentoVendas',
								 'PrazoMedioEstoque',
								 'PrazoMedioPagamentoCompras',
								 'PrazoMedioEntregaCompras',
								 'Observacao',
								 'OrcPrevisaoID'],
								 ['','','99','9999999','999999999999.99','999999999999.99','999.99','999.99','999','999','999','999','',''],
								 ['','','##','#######','###,###,###,##0.00','###,###,###,##0.00','####.00','####.00','###','###','###','###','','']);

		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[2,'###,###,###,###','S'], [3,'###,###,###,##0.00','S'], [4,'###,###,###,##0.00','S']]);

	    alignColsInGrid(fg,[2,3,4,5,6,7,8,9,10,11]);
		
		fg.FrozenCols = 1;

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
}
