<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="orcamentossup01Html" name="orcamentossup01Html">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modestrategico/subplanejamentoestrategico/orcamentos/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modestrategico/subplanejamentoestrategico/orcamentos/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

</head>

<!-- //@@ -->
<body id="orcamentossup01Body" name="orcamentossup01Body" LANGUAGE="javascript" onload="return window_onload()">

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
    
    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->

        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">
        <p id="lblAno" name="lblAno" class="lblGeneral">Ano</p>
        <input type="text" id="txtAno" name="txtAno" DATASRC="#dsoSup01" DATAFLD="Ano" title="Ano Projeção" class="fldGeneral">
        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="MoedaID"></select>
        <p id="lblFaturamentoPrevisto" name="lblFaturamentoPrevisto" class="lblGeneral">Faturamento Previsto</p>
		<input type="text" id="txtFaturamentoPrevisto" name="txtFaturamentoPrevisto" title="Faturamento Previsto no Ano." class="fldGeneral"></input>
        <p id="lblFaturamentoBasePrevisto" name="lblFaturamentoBasePrevisto" class="lblGeneral">Faturamento Base Prev</p>
		<input type="text" id="txtFaturamentoBasePrevisto" name="txtFaturamentoBasePrevisto" title="Faturamento Base Previsto no Ano." class="fldGeneral"></input>
        <p id="lblContribuicaoPrevista" name="lblContribuicaoPrevista" class="lblGeneral">Contribuição Prevista</p>
		<input type="text" id="txtContribuicaoPrevista" name="txtContribuicaoPrevista" class="fldGeneral"></input>
        <p id="lblDespesasPrevista" name="lblDespesasPrevista" class="lblGeneral">Despesas Previstas</p>
		<input type="text" id="txtDespesasPrevista" name="txtDespesasPrevista" class="fldGeneral"></input>
        <p id="lblResultadoPrevisto" name="lblResultadoPrevisto" class="lblGeneral">Resultado Previsto</p>
		<input type="text" id="txtResultadoPrevisto" name="txtResultadoPrevisto" class="fldGeneral"></input>
        <p id="lblFuncionariosPrevisto" name="lblFuncionariosPrevisto" class="lblGeneral">Func Prev</p>
		<input type="text" id="txtFuncionariosPrevisto" name="txtFuncionariosPrevisto" title="Funcionários Previsto no Ano." class="fldGeneral"></input>
        <p id="lblTaxaImpostos" name="lblTaxaImpostos" class="lblGeneral">Imp %</p>
        <input type="text" id="txtTaxaImpostos" name="txtTaxaImpostos" DATASRC="#dsoSup01" DATAFLD="TaxaImpostos" class="fldGeneral">
        <p id="lblFaturamentoProjetado" name="lblFaturamentoProjetado" class="lblGeneral">Faturamento Projetado</p>
		<input type="text" id="txtFaturamentoProjetado" name="txtFaturamentoProjetado" class="fldGeneral"></input>
        <p id="lblFaturamentoBaseProjetado" name="lblFaturamentoBaseProjetado" class="lblGeneral">Fat Base Projetado</p>
		<input type="text" id="txtFaturamentoBaseProjetado" name="txtFaturamentoBaseProjetado" title="Faturamento Base Projetado." class="fldGeneral"></input>
        <p id="lblContribuicaoProjetada" name="lblContribuicaoProjetada" class="lblGeneral">Contribuição Projetada</p>
		<input type="text" id="txtContribuicaoProjetada" name="txtContribuicaoProjetada" class="fldGeneral"></input>
        <p id="lblDespesasProjetada" name="lblDespesasProjetada" class="lblGeneral">Despesas Projetadas</p>
		<input type="text" id="txtDespesasProjetada" name="txtDespesasProjetada" class="fldGeneral"></input>
        <p id="lblResultadoProjetado" name="lblResultadoProjetado" class="lblGeneral">Resultado Projetado</p>
		<input type="text" id="txtResultadoProjetado" name="ResultadoProjetado" class="fldGeneral"></input>
        <p id="lblFaturamento" name="lblFaturamento" class="lblGeneral">Faturamento</p>
		<input type="text" id="txtFaturamento" name="txtFaturamento" class="fldGeneral"></input>
        <p id="lblFaturamentoBase" name="lblFaturamentoBase" class="lblGeneral">Faturamento Base</p>
		<input type="text" id="txtFaturamentoBase" name="txtFaturamentoBase" class="fldGeneral"></input>
        <p id="lblContribuicao" name="lblContribuicao" class="lblGeneral">Contribuição</p>
		<input type="text" id="txtContribuicao" name="txtContribuicao" class="fldGeneral"></input>
        <p id="lblDespesas" name="lblDespesas" class="lblGeneral">Despesas</p>
		<input type="text" id="txtDespesas" name="txtDespesas" class="fldGeneral"></input>
        <p id="lblResultado" name="lblResultado" class="lblGeneral">Resultado</p>
		<input type="text" id="txtResultado" name="txtResultado" class="fldGeneral"></input>
        <p id="lblFuncionarios" name="lblFuncionarios" class="lblGeneral">Funcionários</p>
		<input type="text" id="txtFuncionarios" name="txtFuncionarios" class="fldGeneral"></input>
        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observação</p>
		<input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral"></input>
    </div>
    
</body>

</html>
