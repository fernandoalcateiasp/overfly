/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form orcamentos
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Ano', 'Observa��o');

    //@@ Formatacao das colunas do grid de pesquisa, definicao opcional
    // padrao do framework = null se variavel nao for definida aqui
    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    glb_aCOLPESQFORMAT = new Array('', '', '', '');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked()
{
	showBtnsEspecControlBar('sup', true, [1,1,1,1,1,0]);
	tipsBtnsEspecControlBar('sup', ['Documentos','Relat�rios','Procedimento','Acompanhamento','Capital de Giro','']);
 
	setupEspecBtnsControlBar('sup', 'HDHHHD');

    alignColsInGrid(fg,[0,2]);

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    // Usuario clicou botao documentos
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
    else if (btnClicked == 3)
	{
		window.top.openModalControleDocumento('PL', '', 910, null, '2113', 'T');
	}
	else if (btnClicked == 4)
	{
		openModalAcompanhamento();
	}
	else if (btnClicked == 5)
	{
		openModalCapitalGiro();
	}
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    // Modal de acompanhamento
    else if ( idElement.toUpperCase() == 'MODALACOMPANHAMENTOHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    // Modal de Capital de Giro
    else if ( idElement.toUpperCase() == 'MODALCAPITALGIROHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}
/********************************************************************
openModalAcompanhamento()
Abre o modal de e-mail markting.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalAcompanhamento()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 775;
    var nHeight = 460;
    var aEmpresa = getCurrEmpresaData();

    var nUsuarioID = getCurrUserID();

    var nA1 = getCurrRightValue('SUP','B4A1');

    var nA2 = getCurrRightValue('SUP','B4A2');

    var nDireito = 0;
    
    //DireitoEspecifico
    //Lista de Precos->Lista de Precos->Modal E-mail Marketing
    //40005 B5-> !A1&&A2 -> Filtra produtos pelo proprietario.
    //40005 B5-> A1||A2 -> Libera bot�o enviar.
	if ((nA1 == 1) || (nA2 == 1))
        nDireito = 1;
		
    // mandar os parametros para o servidor
    // Esta modal fica sempre carregada e o que identifica
    // isto e o terceiro parametro da funcao showModalWin abaixo.
    // No strPars, o primeiro parametro e' obrigatorio e e' o UNICO!!!
    // que pode ser mandado
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
	strPars += '&nDireito=' + escape(nDireito);
	
    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modestrategico/subplanejamentoestrategico/orcamentos/modalpages/modalacompanhamento.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth,nHeight));
}

/********************************************************************
openModalCapitalGiro()
Abre o modal de e-mail markting.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalCapitalGiro()
{
    var htmlPath;
    var strPars = new String();
    var nWidth = 775;
    var nHeight = 460;
    var aEmpresa = getCurrEmpresaData();

    var nUsuarioID = getCurrUserID();

    var nA1 = getCurrRightValue('SUP', 'B5A1');

    var nA2 = getCurrRightValue('SUP', 'B5A2');

    var nDireito = 0;
    
    //DireitoEspecifico
    //Lista de Precos->Lista de Precos->Modal E-mail Marketing
    //40005 B5-> !A1&&A2 -> Filtra produtos pelo proprietario.
    //40005 B5-> A1||A2 -> Libera bot�o enviar.
	if ((nA1 == 1) || (nA2 == 1))
        nDireito = 1;
		
    // mandar os parametros para o servidor
    // Esta modal fica sempre carregada e o que identifica
    // isto e o terceiro parametro da funcao showModalWin abaixo.
    // No strPars, o primeiro parametro e' obrigatorio e e' o UNICO!!!
    // que pode ser mandado
    strPars = '?sCaller=' + escape('PL');
    strPars += '&nEmpresaID=' + escape(aEmpresa[0]);
    strPars += '&nUsuarioID=' + escape(nUsuarioID);
	strPars += '&nDireito=' + escape(nDireito);
	
    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modestrategico/subplanejamentoestrategico/orcamentos/modalpages/modalcapitalgiro.asp' + strPars;

    showModalWin(htmlPath, new Array(nWidth,nHeight));
}
