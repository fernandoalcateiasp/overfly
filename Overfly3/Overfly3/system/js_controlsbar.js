/********************************************************************
js_controlsbar.js

Library javascript para os controlbars sup e inf

Dependencias:
js_sysbase.js
js_htmlbase.js
********************************************************************/

// CONSTANTES *******************************************************
/*
var NUMBTNS_SUP = 19;
var NUMBTNS_INF = 19;
var NUMBTNS_SUP_INF = 19;
var NUMBTNSESPEC_BOTH = 8;
*/
var NUMBTNS_SUP = 27;
var NUMBTNS_INF = 27;
var NUMBTNS_SUP_INF = 27;
var NUMBTNSESPEC_BOTH = 16;

// FINAL DE CONSTANTES **********************************************

/********************************************************************
INDICE DAS FUNCOES:
regHtmlInControlBar(theBar, htmlID)
getHtmlRegInControlBar(theBar)
setupControlBar(theBar, combo1, combo2, buttonsArray)
lockControlBar(theBar)
unlockControlBar(theBar)
controlBarIsLocked(theBar)
showControlBar(theBar, cmdShow)
cleanupSelInControlBar(theBar, comboNumber)
addOptionToSelInControlBar(theBar, comboNumber, optionText, optionValue)
removeOptionSelInControlBar(theBar, comboNumber, optionValue)
selOptByValueOfSelInControlBar(theBar, comboNumber, optionValue)
getCmbCurrDataInControlBar(theBar, comboNumber)
getCmbDataByOptionIndex(theBar, comboNumber, optionIndex)
showBtnsEspecControlBar(theBar, cmdShow, arrayBtnsAffected)
tipsBtnsEspecControlBar(theBar, hintArray)
setupEspecBtnsControlBar(theBar, buttonsArray)
especBtnIsPrintBtn(theBar, btnNumber)
numOptionsInCmbControlBar(theBar, comboNumber)
setFocusInCmbOfControlBar(theBar, comboNumber)
btnIsEnableInCtrlBar(theBar, btnNumber)
lastCmbSelectInCtrlBar(theBar)
lastBtnUsedInCtrlBar(theBar)
currentBtnsCtrlBarString(theBar)
lockBtnInCtrlBar(theBar, btnNumber, cmdLock)
lockEspecBtnInCtrlBar(theBar, btnNumber, cmdLock)
unlockBtnsInCtrlBar(theBar) (DO FRAMEWORK)
refreshCtrlBar(theBar)
strArrayValuesInCmbInCtrlBar(theBar, comboNumber)
enableCmbsControlBars(bar, cmbNumber, enable)
stateBtnInCtrlBar(theBar, btnNumber)

INDICE DAS FUNCOES INTERNAS DA LIB, NAO USAR DIRETAMENTE:
__getBarScript(theBar)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Registra um html/asp para falar com um controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
htmlID          id da tag html do arquivo que vai falar com o controlbar

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function regHtmlInControlBar(theBar, htmlID) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript != null)
        return htmlScript.regHtml(htmlID);
    else
        return false;
}

/********************************************************************
Retorna ID do html/asp registrado para falar com um controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
o ID do html/asp ou null
********************************************************************/
function getHtmlRegInControlBar(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript != null)
        return htmlScript.getIDOfFrameAttached();
    else
        return null;
}

/********************************************************************
Configura um controlbar.

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
combo1          'enable' - desbloqueia o combo
e combo2        'disable' - bloqueia o combo
buttonsArray    array dos botoes da esquerda para a direita, onde
cada elemento pode ser:
'h' - chapado, colorido, habilitado
'p' - pressionado, colorido, desabilitado
'd' - chapado, cinza, desabilitado

Retorno:
true se sucesso, caso contrario false

IMPORTANTE: SE O CONTROL BAR ESTA TRAVADO A FUNCAO NAO EXECUTA
E RETORNA FALSE
********************************************************************/
function setupControlBar(theBar, combo1, combo2, buttonsArray) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    // manter descomentado -> testa se esta travado
    if (htmlScript.fBarIsLocked)
        return false;

    var i, tempChar = null;
    var tempStr = new String();

    // passa tudo para maiusculas
    theBar = theBar.toUpperCase();
    combo1 = combo1.toUpperCase();
    combo2 = combo2.toUpperCase();
    buttonsArray = buttonsArray.toUpperCase();

    // Nao tem o numero de botoes, completa com desabilitados
    if ((buttonsArray.length) > 0 && (buttonsArray.length < NUMBTNS_SUP_INF)) {
        while (buttonsArray.length < NUMBTNS_SUP_INF)
            buttonsArray = buttonsArray + 'D';
    }

    // salva a string de botoes
    htmlScript.__currentBtnsCtrlBarStr = buttonsArray;

    // converte o estado dos botoes
    // h == 1 - chapado, colorido, habilitado
    // p == 3 - pressionado, colorido, desabilitado
    // d == 4 - chapado, cinza, desabilitado

    for (i = 0; i < buttonsArray.length; i++) {
        tempChar = buttonsArray.charAt(i) == 'H' ? '1' : buttonsArray.charAt(i);
        if (tempChar == '1') {
            tempStr += tempChar;
            continue;
        }
        tempChar = buttonsArray.charAt(i) == 'P' ? '3' : buttonsArray.charAt(i);
        if (tempChar == '3') {
            tempStr += tempChar;
            continue;
        }
        tempChar = buttonsArray.charAt(i) == 'D' ? '4' : buttonsArray.charAt(i);
        if (tempChar == '4')
            tempStr += tempChar;
    }

    buttonsArray = tempStr;

    // critica os parametros passados:
    // se theBar != 'SUP' || thebar != 'INF' --> false
    // e conforme:
    // se buttonsArray.length != NUMBTNS_SUP; --> false
    // se buttonsArray.length != NUMBTNS_INF; --> false

    if (!(((theBar == 'SUP') && (buttonsArray.length == NUMBTNS_SUP)) ||
         ((theBar == 'INF') && (buttonsArray.length == NUMBTNS_INF))))
        return false;

    // se combo1 != 'ENABLE' || combo1 != 'DISABLE' --> false
    if (!((combo1 == 'ENABLE') || (combo1 == 'DISABLE')))
        return false;

    // se combo2 != 'ENABLE' || combo2 != 'DISABLE' --> false
    if (!((combo2 == 'ENABLE') || (combo2 == 'DISABLE')))
        return false;

    // converte os parametros passados para os valores
    // internos do controlbarxxx.asp
    // combo1
    (combo1 == 'ENABLE') ? combo1 = false : combo1 = true;
    // combo2
    (combo2 == 'ENABLE') ? combo2 = false : combo2 = true;

    // configura o control bar
    htmlScript.setupFBar(combo1, combo2, buttonsArray);

    return true;
}

/********************************************************************
Configura os botoes especificos de um controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

buttonsArray    array dos quatro botoes da esquerda para a direita, onde
cada elemento pode ser:
'h' - chapado, colorido, habilitado
'p' - pressionado, colorido, desabilitado
'd' - chapado, cinza, desabilitado

Retorno:
true se sucesso, caso contrario false

IMPORTANTE: SE O CONTROL BAR ESTA TRAVADO A FUNCAO NAO EXECUTA
E RETORNA FALSE
********************************************************************/
function setupEspecBtnsControlBar(theBar, buttonsArray) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    var i, tempChar = null;
    var tempStr = new String();

    // passa tudo para maiusculas
    theBar = theBar.toUpperCase();
    buttonsArray = buttonsArray.toUpperCase();

    // Nao tem o numero de botoes, completa com desabilitados
    if ((buttonsArray.length) > 0 && (buttonsArray.length < NUMBTNSESPEC_BOTH)) {
        while (buttonsArray.length < NUMBTNSESPEC_BOTH)
            buttonsArray = buttonsArray + 'D';
    }

    // salva a string de botoes
    htmlScript.__currentBtnsCtrlBarStr = (htmlScript.__currentBtnsCtrlBarStr).substr(0, 11) + buttonsArray;

    // converte o estado dos botoes
    // h == 1 - chapado, colorido, habilitado
    // p == 3 - pressionado, colorido, desabilitado
    // d == 4 - chapado, cinza, desabilitado

    for (i = 0; i < buttonsArray.length; i++) {
        tempChar = buttonsArray.charAt(i) == 'H' ? '1' : buttonsArray.charAt(i);
        if (tempChar == '1') {
            tempStr += tempChar;
            continue;
        }
        tempChar = buttonsArray.charAt(i) == 'P' ? '3' : buttonsArray.charAt(i);
        if (tempChar == '3') {
            tempStr += tempChar;
            continue;
        }
        tempChar = buttonsArray.charAt(i) == 'D' ? '4' : buttonsArray.charAt(i);
        if (tempChar == '4')
            tempStr += tempChar;
    }

    buttonsArray = tempStr;

    // critica os parametros passados:
    // se theBar != 'SUP' || thebar != 'INF' --> false
    // e conforme:
    // se buttonsArray.length != NUMBTNSESPEC_BOTH; --> false

    if (!(((theBar == 'SUP') && (buttonsArray.length == NUMBTNSESPEC_BOTH)) ||
         ((theBar == 'INF') && (buttonsArray.length == NUMBTNSESPEC_BOTH))))
        return false;

    // testa se esta travado
    if (htmlScript.fBarIsLocked)
        return false;

    // configura o control bar
    htmlScript.setupEspecFBar(buttonsArray);

    return true;
}

/********************************************************************
Configura um botao especifico de um controlbar para imagem de print

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

btnNumber       0 ou null   - nenhum botao com imagem de print
1, 2, 3 ... - o botao com imagem de print

Retorno:
true se sucesso ou false se falha
********************************************************************/
function especBtnIsPrintBtn(theBar, btnNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    if (btnNumber == null)
        btnNumber = 0;

    // o parametro btn number esta errado
    // supondo que as duas barras tenham sempre o mesmo numero de botoes
    // especificos
    if ((btnNumber < 0) || (btnNumber > NUMBTNSESPEC_BOTH))
        return false;

    return htmlScript.printBtn(btnNumber);
}

/********************************************************************
Trava um controlbar e salva sua configuracao

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
1 - sucesso
0 - falhou
-1 - o control bar ja estava travado
********************************************************************/
function lockControlBar(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return 0;

    // verifica se ja estava travada
    if (htmlScript.fBarIsLocked)
        return -1;

    // trava a barra e salva seu estado
    htmlScript.saveStateBarAndLockIt();

    return 1;
}

/********************************************************************
Destrava um controlbar e refaz sua configuracao

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
1 - sucesso
0 - falhou
-1 - o control bar ja estava destravado
********************************************************************/
function unlockControlBar(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return 0;

    // verifica se ja estava destravada
    if (!htmlScript.fBarIsLocked)
        return -1;

    // destrava a barra e refaz sua configuracao
    htmlScript.restoreStateBarAndUnlockIt();

    return 1;
}

/********************************************************************
Informa se um controlbar esta travado

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
true - esta travado
false - esta destravado
null - falhou
********************************************************************/
function controlBarIsLocked(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    return htmlScript.fBarIsLocked;
}

/********************************************************************
Mostra ou esconde um controlBar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
cmdShow         true - mostra
false - esconde

Retorno:
Nenhum
********************************************************************/
function showControlBar(theBar, cmdShow) {
    // pega o html do controlbar
    var frameID = null;
    var frameObj = null;
    var htmlScript = null;

    // passa tudo para maiusculas
    theBar = theBar.toUpperCase();

    if (theBar == 'SUP') {
        frameID = getFrameIdByHtmlId('controlbarsupHtml');
        if (frameID)
            frameObj = getFrameInHtmlTop(frameID);
    }

    if (theBar == 'INF') {
        frameID = getFrameIdByHtmlId('controlbarinfHtml');
        if (frameID)
            frameObj = getFrameInHtmlTop(frameID);
    }

    if (frameObj) {
        if (cmdShow)
            frameObj.style.visibility = 'visible';
        else
            frameObj.style.visibility = 'hidden';
    }
}

/********************************************************************
Remove todos os options de um combo do controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function cleanupSelInControlBar(theBar, comboNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    return htmlScript.cleanupCombo(comboNumber);
}

/********************************************************************
Acrescenta options em um combo do controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2
optionText      o texto do option
optionValue     o value do option

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function addOptionToSelInControlBar(theBar, comboNumber, optionText, optionValue) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    return htmlScript.addOptionInCombo(comboNumber, optionText, optionValue);
}

/********************************************************************
Remove um option de um combo do controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2
optionValue     o value do option

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function removeOptionSelInControlBar(theBar, comboNumber, optionValue) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    return htmlScript.removeOptionInCombo(comboNumber, optionValue);
}

/********************************************************************
Deseleciona os options em um combo do controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function unselAllOptsOfSelInBar(theBar, comboNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    return htmlScript.unselOptInSel(comboNumber);
}

/********************************************************************
Seleciona um option em um combo do controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2
optionValue     o value do option a ser selecionado
sCaller         quem chamou, se diferente de null foi a automacao,
se igual a null foi o programador em algum form

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function selOptByValueOfSelInControlBar(theBar, comboNumber, optionValue, sCaller) {
    var retVal = false;

    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return retVal;

    retVal = htmlScript.selByOptValueInSel(comboNumber, optionValue);

    // Especifico para o combo de pastas do control bar inferior
    // Se a corrente funcao nao foi chamada pela automacao,
    // a chamada abaixo __combo_1_Adj_Variables(theBar, comboNumber, optionValue)
    // zera variaveis do detail inf, que sao zeradas
    // na funcao __combo_1(controlBar, optText, optValue, optIndex)
    // definida no js_detailInf.js.
    if ((sCaller == null) && ((theBar.toUpperCase()) == 'INF') && (comboNumber == 1))
        try {
        __combo_1_Adj_Variables(theBar, comboNumber, optionValue);
    }
    catch (e) {
        ;
    }
    return retVal;
}

/********************************************************************
Retorna os dados do option atualmente selecionado no combo

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2


Retorno:
se sucesso      array: indice do option selecionado,
value do option selecionado,
texto do option selecionado
se falha        null;
********************************************************************/
function getCmbCurrDataInControlBar(theBar, comboNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    return htmlScript.getComboData(comboNumber);
}

/********************************************************************
Retorna os dados do option no combo, dado seu indice

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2
optionIndex     indice do option do combo

Retorno:
se sucesso      array: indice do option selecionado,
value do option selecionado,
texto do option selecionado
se falha        null;
********************************************************************/
function getCmbDataByOptionIndex(theBar, comboNumber, optionIndex) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    return htmlScript.getComboDataByOptionIndex(comboNumber, optionIndex);
}

/********************************************************************
Retorna os dados do option no combo, dado seu indice

Parametros:
bar             'sup' - controlbar superior
'inf' - controlbar inferior
cmbNumber       da esquerda para a direita: 1 ou 2
enable          habilita ou nao o combo

Retorno:
Nenhum
********************************************************************/
function enableCmbsControlBars(bar, cmbNumber, enable) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(bar);

    if (htmlScript == null)
        return null;

    return htmlScript.enableCmbsControlBars(cmbNumber, enable);
}

/********************************************************************
Esta funcao mostra ou esconde os botoes especificos do controlbar

Parametros:
theBar              'sup' - controlbar superior
'inf' - controlbar inferior
cmdShow             true (mostra), false (esconde)
arrayBtnsAffected   array dos botoes afetados por cdmShow
0 - nao afeta
1 - afeta
exemplos: 1,0,0,0, ... afeta o primeiro
Retorno:
Nao e significativo
********************************************************************/
function showBtnsEspecControlBar(theBar, cmdShow, arrayBtnsAffected) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    while (arrayBtnsAffected.length < NUMBTNSESPEC_BOTH) {
        if (cmdShow)
            arrayBtnsAffected[arrayBtnsAffected.length] = 0;
        else
            arrayBtnsAffected[arrayBtnsAffected.length] = 1;
    }

    htmlScript.showBtnsEspec(cmdShow, arrayBtnsAffected);
}

/********************************************************************
Retorna se um botao especifico esta visivel

Parametros:
theBar              'sup' - controlbar superior
'inf' - controlbar inferior
btnNumber           12, 13, 14, ou 15

Retorno:
true se visivel, false se invisivel e null se houve erro
********************************************************************/
function btnEspecificIsVisibleInCtrlBar(theBar, btnNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    return htmlScript.btnSpcIsVisible(btnNumber);
}

/********************************************************************
Esta funcao troca o hint dos botoes especificos do controlbar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
hintArray       array dos tres hints


Retorno:
Nao e significativo
********************************************************************/
function tipsBtnsEspecControlBar(theBar, hintArray) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    while (hintArray.length < NUMBTNSESPEC_BOTH) {
        hintArray[hintArray.length] = '';
    }

    return htmlScript.hintsBtnsEspec(hintArray);
}

/********************************************************************
Retorna numero de options em um combo

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2

Retorno:
Se encontra o combo retorna o numero de options, caso contrario
retorna -1
********************************************************************/
function numOptionsInCmbControlBar(theBar, comboNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return -1;

    return htmlScript.numOptionsInCmb(comboNumber);
}

/********************************************************************
Coloca foco em um combo de um control bar 

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     da esquerda para a direita: 1 ou 2

Retorno:
Se coloca o foco no combo retorna true, caso contrario retorna
false
********************************************************************/
function setFocusInCmbOfControlBar(theBar, comboNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    return htmlScript.setFocusInCmb(comboNumber);
}

/********************************************************************
Retorna o estado de um botao de um control bar 

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
btnNumber       da esquerda para a direita: 1, 2, etc

Retorno:
Se habilitado retorna true, se desabilitado retorna false
se o botao nao existe retorna null
********************************************************************/
function btnIsEnableInCtrlBar(theBar, btnNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    return htmlScript.btnIsEnable(btnNumber);
}

/********************************************************************
Retorna o ultimo combo usado pelo usuario

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
-1 se falha
0 se nenhum combo foi usado, ou 1, ou 2
********************************************************************/
function lastCmbSelectInCtrlBar(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return -1;

    return htmlScript.lastCmbSel;
}

/********************************************************************
Retorna o ultimo botao usado pelo usuario

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
null se nenhum botao foi usado, ou
do control bar superior
Pesquisar     PESQ
Listar        LIST
Detalhar      DET
Incluir       INCL
Alterar       ALT
Estado        EST
Excluir       EXCL
Ok            OK
Cancelar      CANC
Refresh       REFR
Anterior      ANT
Seguinte      SEG
Um            UM
Dois          DOIS
Tres          TRES    
do control bar inferior
Retroceder    RETRO
Avancar       AVANC
********************************************************************/
function lastBtnUsedInCtrlBar(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    return htmlScript.btnID;
}

/********************************************************************
Retorna a string controladora do estado dos botoes

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
a string se sucesso ou null se falha
********************************************************************/
function currentBtnsCtrlBarString(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    return htmlScript.__currentBtnsCtrlBarStr;
}

/********************************************************************
Desabilita ou habilita um botao da barra de botoes.
ATENCAO: O botao desabilitado assim permanecera para todo o form, ate
trocar o form ou habilitar o botao por esta funcao.

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
btnNumber       os 11 botoes normais. btnNumber varia de 1 a 11
cmdLock         true - desabilita o botao, false habilita

Retorno:
true se sucesso ou false se falha
********************************************************************/
function lockBtnInCtrlBar(theBar, btnNumber, cmdLock) {
    // verifica se btnNumber esta na faixa
    if ((btnNumber < 1) || (btnNumber > 11))
        return false;

    // verifica se cmdLock e true ou false
    if (!((cmdLock == true) || (cmdLock == false)))
        return false;

    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    // atua sobre o botao
    htmlScript.__btnLocked[(btnNumber - 1)] = cmdLock;
}

/********************************************************************
Desabilita ou habilita um botao da barra de botoes.
ATENCAO: O botao desabilitado assim permanecera para todo o form, ate
trocar o form ou habilitar o botao por esta funcao.

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
btnNumber       os 4 botoes especificos, btnNumber varia de 1 a 4
cmdLock         true - desabilita o botao, false habilita

Retorno:
true se sucesso ou false se falha
********************************************************************/
function lockEspecBtnInCtrlBar(theBar, btnNumber, cmdLock) {
    var theBtnGap;

    // verifica se btnNumber esta na faixa
    // supondo o mesmo numero de botoes especificos
    // no control bar superior e no inferior
    if ((btnNumber < 1) || (btnNumber > NUMBTNSESPEC_BOTH))
        return false;

    // verifica se cmdLock e true ou false
    if (!((cmdLock == true) || (cmdLock == false)))
        return false;

    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    if (theBar.toUpperCase() == 'SUP')
        theBtnGap = (NUMBTNS_SUP - NUMBTNSESPEC_BOTH);
    else
        theBtnGap = (NUMBTNS_INF - NUMBTNSESPEC_BOTH);

    // atua sobre o botao
    htmlScript.__btnLocked[(theBtnGap + btnNumber - 1)] = cmdLock;
}

/********************************************************************
Habilita todos os botoes da barra de botoes.
ATENCAO: O botao habilitado assim permanecera para todo o form, ate
ser desabilitado pela funcao lockBtnInCtrlBar(theBar, btnNumber, cmdLock).
Esta funcao e de uso exclusivo do framework.

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
true se sucesso ou false se falha
********************************************************************/
function unlockBtnsInCtrlBar(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return false;

    var i;
    var nCeil;

    if (theBar.toUpperCase() == 'SUP')
        nCeil = NUMBTNS_SUP;
    else
        nCeil = NUMBTNS_INF;

    for (i = 0; i < nCeil; i++)
        htmlScript.__btnLocked[i] = false;
}

/********************************************************************
Redesenha os botoes da barra, caso a mesma nao esteja travada.

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
nao considerado, e sempre null;
********************************************************************/
function refreshCtrlBar(theBar) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    if (htmlScript.fBarIsLocked == false)
        htmlScript.allBtnsGrayOrColorOrPressed();
}

/********************************************************************
Retorna uma string array (elementos separados por virgula) dos values
de um combo do control bar.

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
comboNumber     1 ou 2

Retorno:
string array ou null se falha
********************************************************************/
function strArrayValuesInCmbInCtrlBar(theBar, comboNumber) {
    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return null;

    if (htmlScript != null)
        return htmlScript.strArrayValuesInCmb(comboNumber);
}

/********************************************************************
Le o estado de um botao das barras de botoes, ap�s aplicacao dos direitos
sobre os botoes das barras.

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior
--btnNumber       os 11 botoes normais. btnNumber varia de 1 a 11
btnNumber       os 19 botoes. btnNumber varia de 1 a 19

Retorno:
'' se falha, 'H' se habilitado e 'D' se desabilitado
********************************************************************/
function stateBtnInCtrlBar(theBar, btnNumber) {
    // verifica se btnNumber esta na faixa
    if ((btnNumber < 1) || (btnNumber > 11))
        return '';

    // obtem referencia ao script do controlbar
    var htmlScript = __getBarScript(theBar);

    if (htmlScript == null)
        return '';

    return (htmlScript.__btnLocked[(btnNumber - 1)] == false ? 'H' : 'D');
}

// FINAL DE IMPLEMENTACAO DAS FUNCOES

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****

/********************************************************************
Obtem referencia ao script do control bar

Parametros:
theBar          'sup' - controlbar superior
'inf' - controlbar inferior

Retorno:
a referencia ou null se falha
********************************************************************/
function __getBarScript(theBar) {
    // pega o html do controlbar
    var frameID = null;
    var htmlScript = null;

    // passa tudo para maiusculas
    theBar = theBar.toUpperCase();

    if (theBar == 'SUP') {
        frameID = getFrameIdByHtmlId('controlbarsupHtml');
        if (frameID)
            htmlScript = getPageFrameInHtmlTop(frameID);
    }

    if (theBar == 'INF') {
        frameID = getFrameIdByHtmlId('controlbarinfHtml');
        if (frameID)
            htmlScript = getPageFrameInHtmlTop(frameID);
    }

    // obteve o script?
    if (htmlScript)
        return htmlScript;

    return null;
}
// FINAL DE FUNCOES INTERNAS DA LIBRARY *****************************
