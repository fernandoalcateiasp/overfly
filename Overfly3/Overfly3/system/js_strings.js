/********************************************************************
js_strings.js

Library javascript de funcoes basicas para strings
********************************************************************/

/********************************************************************
INDICE DAS FUNCOES:
    trimStr(strToTrim)
    trimAllSpacesInStr(strToTrim)
    trimCarriageReturnAtEnd(strToTrim)
    trimCarriageReturnAtAll(strToTrim)
    getLabelNumStriped(strComp)
    getNumAlphaStriped(strComp)
    getNumAlphaStripedEx(strComp, sDecSep, bPreserveSignal)
    dupCharInString(strToDupChar, charToDup)
    ascan(aArray,vToFind,lpartial)
    chkDataEx(sData)
    anoBisexto(nYear)
    roundNumber(theNumber, decimals)
    padNumReturningStr(theNumber, decimals, sep, charToUse)
    padL( s,n,c )
    padR( s,n,c )
    DTOC(sDate)    
    verifyNumericEnterNotLinked()
    selFieldContent()
    valueOfFieldInArray(arrayRef, fldOrigIndex, fldOrigValue, fldTargetIndex)
    putDateInMMDDYYYY2(sDate)	- uso geral no sistema
    putDateInYYYYMMDD(sDate)
    strToDate(strDate)
    useOutLook(arrayTo, arrayCc, arrayBcc, theSubject, theBody)
    openOutLookAndSetClipB(arrayTo, arrayCc, arrayBcc)
    __implementsPasteInTxtFld()
    __beforepasteTxtFld()
    __pasteTxtFld()
    __msgNotPaste(ctrl)
    check_Flds_ForHex(vlr)
    setLabelOfControl(lblRef, sID)
    stripNonNumericChars(strToStrip)
    stripNonAlphaNumericChars(strToStrip)
    transformStringInNumeric(strToTransform)
    parseStrEscaped(strToParse)
    asort(arrayToSort, nArrayDimension)
    aseek(aArray, strToSeek, nArrayDimension)
    translateTherm(sTherm, aTranslate)
	lookUpValueInCmb(cmbRef, theValue)
	urlEmailTerminator(nIdiomaID, nTipoURLID)
	removeCharFromString(sString, cChar)
	normalizeDateFormat(sString)
	formatCurrencyValue(sStrValue, sMilharSep, sCentSep)
	removeDiatricsOnJava(strToRemove)
    tempoDecorrido(inicio, fim)	
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Duplica caracter em uma string
Parametros:
strToDupChar        - string a duplicar o caracter
charToDup           - caracter a duplicar

Retornos:
a string com o caracter duplicado ou null se erro
********************************************************************/
function dupCharInString(strToDupChar, charToDup)
{
    // Nao aceita parametros nulos
    if ( strToDupChar == null || charToDup == null )
        return null;
    
    // So duplica caracter isolado
    if ( charToDup.length > 1 )
        return null;
        
    var i, retStr;
    
    retStr = '';
    for ( i=0; i<strToDupChar.length; i++ )
    {    
        if ( strToDupChar.charAt(i) == charToDup )
        {
            retStr += (charToDup + charToDup);
            continue;
        }
        else
            retStr += strToDupChar.charAt(i);
    
    }
    
    return retStr;
}

/********************************************************************
Trima espacos em branco do inicio e do fim de uma string
Parametros:
strToTrim               string a remover os espacos

Retornos:
a string sem os espacos
********************************************************************/
function trimStr(strToTrim)
{
    if ( strToTrim == null )
        return '';
        
    if ( strToTrim == '' )
        return '';
    
    var i, len;
    var charac, strTrimmed = new String();
    
    // trima lado esquerdo
    len = strToTrim.length;
        
    for (i=0; i<(len); i++)
    {
        charac = strToTrim.charAt(i);
        if ( charac != " " )
            break;
    }
    
    strTrimmed = strToTrim.substr(i);
        
    // trima lado direito
    len = strTrimmed.length;
        
    for (i=(len-1); i>=0; i--)
    {
        charac = strTrimmed.charAt(i);
        if ( charac != " " )
            break;
    }
    
    i++;
    
    strToTrim = strTrimmed.substr(0, i);
    
    return (strToTrim);
}

/********************************************************************
Trima espacos em branco do inicio, no fim e no interior de uma string
Parametros:
strToTrim               string a remover os espacos

Retornos:
a string sem os espacos
********************************************************************/
function trimAllSpacesInStr(strToTrim)
{
    var i, len;
    var charac, strTrimmed = new String();
    
    // trima lado esquerdo
    len = strToTrim.length;
        
    for (i=0; i<(len); i++)
    {
        charac = strToTrim.charAt(i);
        if ( charac != " " )
            break;
    }
    
    strTrimmed = strToTrim.substr(i);
        
    // trima lado direito
    len = strTrimmed.length;
        
    for (i=(len-1); i>=0; i--)
    {
        charac = strTrimmed.charAt(i);
        if ( charac != " " )
            break;
    }
    
    i++;
    
    strToTrim = strTrimmed.substr(0, i);
    
    // trima interior da string
    strTrimmed = '';
    
    for (i=0; i<strToTrim.length; i++)
        if ( strToTrim.charCodeAt(i) != 32 )
            strTrimmed += strToTrim.charAt(i);
    
    return (strTrimmed);
}

/********************************************************************
Recebe uma string e remove 0D0A do final
Parametros:
    strToTrim

Retorno:
A string sem o 0D0A
********************************************************************/
function trimCarriageReturnAtEnd(strToTrim)
{
	if ( strToTrim == null )
		return strToTrim;
		
    var len = strToTrim.length;
    var charac;
    var strTrimmed = strToTrim;

    if ( len == 0 )
		return strToTrim;
        
	if ( (len > 1) && (strTrimmed.charCodeAt(len - 1) == 0XA) )
	{
		strTrimmed = strTrimmed.substr(0, len - 1);
		len--;	
	}
	
	if ( (len > 1) && (strTrimmed.charCodeAt(len - 1) == 0XD) )
		strTrimmed = strTrimmed.substr(0, len - 1);	
    
    return strTrimmed;
}

/********************************************************************
Recebe uma string e remove todos os 0D0A
Parametros:
    strToTrim

Retorno:
A string sem o 0D0A
********************************************************************/
function trimCarriageReturnAtAll(strToTrim)
{
	if ( strToTrim == null )
		return strToTrim;
		
    var len = strToTrim.length;
    var i;
    var strTrimmed = '';

    if ( len == 0 )
		return strToTrim;
        
    for ( i=0; i<len; i++ )    
    {
		if ( strToTrim.charCodeAt(i) == 0XD)
			continue;
			
		if ( strToTrim.charCodeAt(i) == 0XA)
			continue;
		
		strTrimmed += strToTrim.charAt(i);		
    }

    return strTrimmed;
}

/********************************************************************
Recebe uma string e remove todos os caracteres indesejados.
Parametros:
str

Retorno:
A string sem os caracteres indesejados.
********************************************************************/
function filtraString(str) {

	if (str == null || str.length == 0)
		return str;

	var result = "";
	var i;

	for (i = 0; i < str.length; i++) {
		if (((str.charCodeAt(i) == 13) || (str.charCodeAt(i) == 10) || (str.charCodeAt(i) >= 32 && str.charCodeAt(i) <= 126) || (str.charCodeAt(i) >= 128 && str.charCodeAt(i) <= 255) ) && (str.charCodeAt(i) != 0X22) && (str.charCodeAt(i) != 0X27))
			result += str.charAt(i);
	}

	return result;
}

/********************************************************************
Recebe uma string e remove todos os caracteres indesejados,
mas aceita BULLET e TAB.
Parametros:
str

Retorno:
A string sem os caracteres indesejados.
********************************************************************/
function filtraString_Exception(str) {
    if (str == null || str.length == 0)
        return str;

    var result = "";
    var i;

    for (i = 0; i < str.length; i++) {
        if (((str.charCodeAt(i) == 13) || (str.charCodeAt(i) == 10) || (str.charCodeAt(i) >= 32 && str.charCodeAt(i) <= 126) || (str.charCodeAt(i) >= 128 && str.charCodeAt(i) <= 255)) && (str.charCodeAt(i) != 0X22) && (str.charCodeAt(i) != 0X27))
            result += str.charAt(i);

        // 05/05/2011 - Para aceitar bullet e tab. BJBN
        if ((str.charCodeAt(i) == 8226) || (str.charCodeAt(i) == 9))
            result += str.charAt(i);

    }

    return result;
}

/********************************************************************
Recebe uma string composta (texto + numerico) e devolve apenas
o texto existente antes do numerico
Parametros:
    strComp

Retorno:
A string sem a parte numerica
********************************************************************/
function getLabelNumStriped(strComp)
{
    var strRes = '';
    var i;
    
    if ( (strComp == null) || (strComp == '') )
        return strRes;

    for ( i=0; i<strComp.length; i++ )
    {
        if ( (strComp.charCodeAt(i) >= 48) && (strComp.charCodeAt(i) <= 57) )
            break;
        strRes += strComp.charAt(i);
    }

    return trimStr(strRes);
}

/********************************************************************
Recebe uma string composta (texto + numerico) e devolve apenas
os numericos, mantendo o sinal menos
Parametros:
    strComp

Retorno:
A parte numerica da string
********************************************************************/
function getNumAlphaStriped(strComp)
{
    var strRes = '';
    var i;
    
    if ( (strComp == null) || (strComp == '') )
        return strRes;
        
    strComp = strComp.toString();    

    for ( i=0; i<strComp.length; i++ )
    {
		if ( (i == 0) && (strComp.charCodeAt(i) == 45) )
			strRes += strComp.charAt(i);
		
        if ( (strComp.charCodeAt(i) >= 48) && (strComp.charCodeAt(i) <= 57) )
            strRes += strComp.charAt(i);
            
    }

    return trimStr(strRes);
}

/********************************************************************
Recebe uma string composta (texto + numerico) e devolve apenas
os numericos, mantendo o sinal menos
Parametros:
    strComp

Retorno:
A parte numerica da string
********************************************************************/
function getNumAlphaStripedEx(strComp, sDecSep, bPreserveSignal)
{
    var strRes = '';
    var i;
    
    if (sDecSep == ',')
		charCodeDecSep = 44;
	else
		charCodeDecSep = 46;	
	
	if ( bPreserveSignal == null )	
		bPreserveSignal == true;
    
    if ( (strComp == null) || (strComp == '') )
        return strRes;
        
    strComp = strComp.toString();    

    for ( i=0; i<strComp.length; i++ )
    {
		if ( (i == 0) && (strComp.charCodeAt(i) == 45) && bPreserveSignal )
			strRes += strComp.charAt(i);
		
        if ( (strComp.charCodeAt(i) >= 48) && (strComp.charCodeAt(i) <= 57) )
            strRes += strComp.charAt(i);
            
        if ( strComp.charCodeAt(i) == charCodeDecSep )
            strRes += strComp.charAt(i);    
    }

    return trimStr(strRes);
}

/********************************************************************
Retorna a posicao do elemento encontrado no array

Parametros:
aArray          um array a ser pesquisado
vToFind         valor a ser procurado dentro do array
lpartial        true - verifica se vToFind existe em um elemento do array
                false - verifica se vToFind e extamente igual a um elemento
                        do array

A pesquisa nao e case sensitive                        

Retornos:
integer   -1 Nao achou
        >= 0 Retorna o numero do elemento encontrado no array
********************************************************************/
function ascan(aArray,vToFind,lpartial)
{
    var i = 0;
    var retorno = -1;
    var sElemem;
    
    if (typeof(vToFind) == 'string')
        vToFind = vToFind.toUpperCase();
        
    for (i=0;i<aArray.length;i++)
    {
        sElemem = aArray[i];

        if (typeof(sElemem) == 'string')
            sElemem = sElemem.toUpperCase();
                
        if (lpartial)
        {
            if (sElemem.lastIndexOf(vToFind) >= 0)
            {
                retorno = i;
                break;
            }
        }
        else
        {
            if (sElemem == vToFind)
            {
                retorno = i;
                break;
            }
        }
    }
    return retorno;
}

/********************************************************************
Verifica se uma string data e valida.

Parametros:
sData          - data a verificar
notSQLMode     - se != null, nao forca a parte de hora ter hora
                 e minuto

Retornos:
true - a data e valida
false - a data nao e valida
null - a data e vazia (o campo nao contem data)
********************************************************************/
function chkDataEx(sData, notSQLMode)
{
    var nSecond = null;
    var nMinute = null;
    var nHour = null;
    var nDay = null;
    var nMonth = null;
    var nYear = null;
    var sString;
    var aStringPrev;
    var aString;
    var bDateIsValid = true;
    var bHourIsValid = true;
    var nMonths = new Array();
    var rExp;
    var bhasTime = false;
    
    if (sData == null || sData == '')
        return null;
    
    sString=sData;
    rExp = /\W/g;
    aStringPrev = sString.split(rExp);
    
    // string nao tem o minimo de componentes (dia, mes, ano)
	if ( aStringPrev.length < 3 )
        return false;
	
	// string tem formato de data mas nao e data pois tem caracteres outros alem de
	// numeros e separadores
	if ( aStringPrev[0].length > 2 )
        return false;
    if ( aStringPrev[1].length > 2 )
        return false;    
    if ( aStringPrev[2].length > 4 )
        return false;
    
    sString=sData;
        
    rExp = /\D/g;
    aString = sString.split(rExp);
    
    if ( (typeof(aString)).toUpperCase() != 'OBJECT' )
        return false;
        
    if ( aString.length == 3 )
        bHasTime = false;
    else if ( (notSQLMode != null) && (aString.length == 4)  )
        bHasTime = true;    
    else if ( (aString.length == 5) || (aString.length == 6) )    
        bHasTime = true;
    else
        return false;
        
    if ( DATE_FORMAT == "DD/MM/YYYY" )
    {
        nDay = parseInt(aString[0], 10);
        nMonth = parseInt(aString[1], 10);
        nYear = parseInt(aString[2], 10);
    }
    else
    {
        nDay = parseInt(aString[1], 10);
        nMonth = parseInt(aString[0], 10);
        nYear = parseInt(aString[2], 10);
    }
    
    if ( bHasTime )
    {
        nHour = parseInt(aString[3], 10);
        nMinute = parseInt(aString[4], 10);
        // tem segundo
        if ( aString.length == 6 )
            nSecond = parseInt(aString[5], 10);
    }
    
    // Valida a data
    // a data esta incompleta
    if ( nDay == null || nMonth == null || nYear == null )
        return false;
    
    // sistema configurado com ano de dois digitos
    if ( (nYear > 22) && (nYear <= 99) )
        nYear = (parseInt(nYear, 10) + 1900).toString();
    if ( (nYear >= 0) && (nYear <= 22) )
        nYear = (parseInt(nYear, 10) + 2000).toString();
    
    // trapeia se a data esta no range 1753 - 9999
    if ( (nYear < 1753) || (nYear > 9999) )
        return false;
    
    if (sData == '')
        return null;
    
    nMonths[01]=31;
    
    if (anoBisexto(nYear))
    {
        nMonths[02]=29;
    }
    else
    {
        nMonths[02]=28;
    }
    
    nMonths[03]=31;
    nMonths[04]=30;
    nMonths[05]=31;
    nMonths[06]=30;
    nMonths[07]=31;
    nMonths[08]=31;
    nMonths[09]=30;
    nMonths[10]=31;
    nMonths[11]=30;
    nMonths[12]=31;
    
    if ((nDay < 1) || (nDay > nMonths[nMonth]))
    {
        bDateIsValid = false;
    }
    else if ((nMonth < 1) || (nMonth > 12))
    {
        bDateIsValid = false;
    }
    
    if ( bHasTime == false ) 
        return bDateIsValid;
        
    // Valida hora e minuto se tem
    if ( (notSQLMode == null) && ((nMinute == null) || (nHour == null)) )
        bHourIsValid = false;
        
    if ( (nMinute != null) && ((nMinute < 0) || (nMinute > 59)) )
        bHourIsValid = false;        
    
    if ( (nHour != null ) && ((nHour < 0) || (nHour > 24)) )
        bHourIsValid = false; 
        
    // Valida o segundo se tem
    if ( (nSecond != null) && ((nSecond < 0) || (nSecond > 59)) )
        bHourIsValid = false;        
        
    return (bDateIsValid && bHourIsValid);
}

/****************************************************************************************************************
A valida��o do ano bisexto se baseia nas seguintes regras.
	1� S�o bissextos todos os anos m�ltiplos de 400, p.ex: 1600, 2000, 2400, 2800
	2� N�o s�o bissextos todos os m�ltiplos de 100 e n�o de 400, p.ex: 1700, 1800, 1900, 2100, 2200, 2300, 2500�
	3� S�o bissextos todos os m�ltiplos de 4 e n�o m�ltiplos de 100, p.ex: 1996, 2004, 2008, 2012, 2016�
	4� N�o s�o bissextos todos os demais anos.
****************************************************************************************************************/
function anoBisexto(nYear) {
	var ano = nYear;
	
	// Se ano m�dulo 100 � 0 e se Y m�dulo 4 � 0 ent�o bissexto
	if(ano % 100 == 0)
		ano = nYear % 100;
	
	ano = ano % 4;

	return ano == 0;
}

/********************************************************************
Trunca um float em decimals apos a virgula
Parametros:
theNumber   - o numero a truncar
decimals    - quantidade da casas apos a virgula

Retornos:
o numero truncado ou zero se erro
********************************************************************/
function roundNumber(theNumber, decimals)
{
    // erro de parametro
    if ( (theNumber == null) || (decimals == null) )
        return 0;
    
    if( isNaN(theNumber) || isNaN(decimals) )
        return 0;
 
    var i;
    var rExp;       
    var aNumber;                // array: parte inteira e parte decimal
    var sFracPart;
    var fFracPart;
    var nIsNegNumber = 1;    
    var sPartOut = '0.';
    var sRndPartOut = '0.';
    var fPartAband = 0;
    var sDigOut = '0';
    var retVal = 0;
        
    if ( theNumber < 0 )    
        nIsNegNumber = -1;
        
    rExp = /\D/g;

    // splita o numero em duas strings sem sinal:
    // [0] parte inteira e [1] parte decimal
    aNumber = (theNumber.toString()).split(rExp);
    
    // nao e numero, devolve zero
    if ( ((typeof(aNumber)).toUpperCase() != 'OBJECT') || (aNumber.length > 2) )
        return 0;

    // numero e inteiro, devolve
    if ( aNumber.length == 1 )
        return theNumber;
        
    // o primeiro digito a ser abandonado e o primeiro digito
    // apos decimals digitos
    sDigOut = (aNumber[1].toString()).substr(decimals, 1);
    
    // a parte fracionaria e truncada com decimals casas
    sFracPart = '0.' + (aNumber[1].toString()).substr(0, decimals);
    
    // a parte do numero a ser abandonada e
    // o arredondador da parte a ser abandonada    
    for (i=0; i<decimals; i++)
        sPartOut += '0';
    
    sRndPartOut = sPartOut + '5';    
    sPartOut += sDigOut;
    
    fPartAband = parseFloat(sRndPartOut) + parseFloat(sPartOut);
    
    // transforma a parte string decimal em numero do tipo 0.xxxx
    fFracPart = parseFloat(sFracPart) + fPartAband;
    
    // arredondando para inteiro (sem decimais)
    if ( decimals == 0 )
        fFracPart = Math.floor(fFracPart);
    // arredondando com decimais
    else
        fFracPart = parseFloat((fFracPart.toString()).substr(0, decimals + 2));
    
    if ( fFracPart == 0 )
        retVal = nIsNegNumber * parseFloat(aNumber[0].toString());
    // else if parece desnecessario
    else if (fFracPart < 1)    
    {
        retVal = nIsNegNumber * parseFloat(aNumber[0].toString() + '.' +
                 (fFracPart.toString()).substr(2));
    }
    else if (fFracPart == 1)    
    {
        retVal = nIsNegNumber * (parseFloat(aNumber[0].toString()) + fFracPart);
    }
    else
        retVal = 0;
    
    // o retorno errado com 1 + .84
    //return (nIsNegNumber * (parseFloat(aNumber[0].toString()) + fFracPart));
 
    return retVal;
}

/********************************************************************
Transforma um numero em string com decimals casas apos a virgula
Parametros:
theNumber   - o numero a truncar
decimals    - quantidade da casas apos a virgula
sep         - separador de decimal a usar. Opcional, se null
              default para ponto
charToUse   - caracter a usar para o padding. Opcional, se null
              default para zero

Retornos:
a string ou '' se erro
********************************************************************/
function padNumReturningStr(theNumber, decimals, sep, charToUse)
{
    if ( (isNaN(theNumber)) || (isNaN(decimals)) )
        return '';
    
    if (sep == null)
        sep = '.';
        
    if (charToUse == null)    
        charToUse = '0';
        
    // se nao tem decimais, retorna
    if ( decimals == 0 )    
        return theNumber.toString();
    
    var i;    
    var numStr = theNumber.toString();
    var intPart, decPart;
    // posicao do ponto, conta do zero
    var pointPos = numStr.indexOf('.');
    
    // a string da parte inteira e a da parte decimal
    if ( pointPos < 0)
    {
        intPart = numStr;
        decPart = '';
    }
    else
    {
        intPart = numStr.substr(0, pointPos);
        decPart = numStr.substr(pointPos + 1);
    }
    
    for ( i=0; i<decimals; i++ )
    {
        if ( decPart.length < decimals )
            decPart +=charToUse;
    }
    
    return intPart + sep + decPart;
}

/********************************************************************
Preenche uma string a esquerda com um caracter especificado
Parametros:
s   - String a ser
n   - numero de caracteres da string de retorno
c   - caracter usado no preenchimento


Retornos:
a string ou '' se erro
********************************************************************/
function padL( s,n,c )
{
    var x = '';
    var nI = 0;
    for (nI=1;nI <= n; nI++)
        x=x+c;
    x=x+trimStr(s);
    return x.substr(x.length-n,n);
}

/********************************************************************
Preenche uma string a direita com um caracter especificado
Parametros:
s   - String a ser
n   - numero de caracteres da string de retorno
c   - caracter usado no preenchimento


Retornos:
a string ou '' se erro
********************************************************************/
function padR( s,n,c )
{
    var x = '';
    var nI = 0;
    for (nI=1;nI <= n; nI++)
        x=x+c;
    x=trimStr(s) + x;
    return x.substr(0, n);
}

/********************************************************************
Retorna uma string no formato YYYYMMDD, para ser usado em indexacao de
data

Parametros :    sData: Data a ser convertida

Retorno:        String no formato YYYYMMDD ou null se nao for possivel a conversao
********************************************************************/
function DTOC(sDate)
{
    if (sDate == null || sDate == '')
        return null;
    
    sString=sDate;
        
    rExp = /\D/g;
    var aString = sString.split(rExp);
    
    if ( (typeof(aString)).toUpperCase() != 'OBJECT' )
        return null;
        
    if ( aString.length != 3 )
        return null;
        
    var sDay, sMonth, sYear;
    if (DATE_FORMAT == "DD/MM/YYYY")
    {
        sDay   = padL(aString[0],2,' ');
        sMonth = padL(aString[1],2,' ');
        sYear  = padL(aString[2],4,' ');
    }
    else
    {
        sMonth = padL(aString[0],2,' ');
        sDay   = padL(aString[1],2,' ');
        sYear  = padL(aString[2],4,' ');
    }
    
    return sYear + sMonth + sDay;
}

/********************************************************************
Verifica digitacao em campo texto (input type text) para date
ou datetime , nao linkado a campo de tabela
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function verifyDateTimeNotLinked()
{
    // nao aceita digitacao diferente de numero ou / ou space :
    if ( !(((event.keyCode >= 48) && (event.keyCode <= 57)) ||
          (event.keyCode == 47) || (event.keyCode == 58) || (event.keyCode == 32)) )
        return false;                    
}

/********************************************************************
Verifica digitacao em campo texto (input type text) numerico, nao
linkado a campo de tabela
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function verifyNumericEnterNotLinked()
{
    /* Tabela de teclas
    08 --> backspace
    09 --> Tab
    35 --> end
    36 --> Home
    37 --> seta p/ esquerda
    39 --> seta p/ esquerda
    44 --> virgula
    46 --> ponto
    */
    
    var nextLenInt;
    var nextLenDec;
    var nextVal = 0;
    var minMax;
    var nPrecision;
    var nScale;
    var signalHouse = 0;

    ctrl = this;
        
    nPrecision = ctrl.getAttribute('thePrecision', 1);
    nScale = ctrl.getAttribute('theScale', 1);

    //===============================================================
    // O maxLength para numeros positivos ou negativos
    if ( (ctrl.getAttribute('posMaxLen', 1) == null) ||
         (ctrl.getAttribute('posMaxLen', 1) == '') )
        ctrl.setAttribute('posMaxLen', ctrl.maxLength , 1);
    
    var ctrlValue = trimStr(ctrl.value);
        
    // ajusta o maxLength conforme o valor do controle seja positivo ou negativo
    if ( isNaN(ctrlValue) )
    {
        ctrl.maxLength = ctrl.getAttribute('posMaxLen', 1);
    }
    else if ( ctrlValue == '' )
    {
        ctrl.maxLength = ctrl.getAttribute('posMaxLen', 1);
    }
    else if ( ctrlValue.substr(0,1) == '-' )
    {
        ctrl.maxLength = ctrl.getAttribute('posMaxLen', 1) + 1;
    }
    else
    {
        ctrl.maxLength = ctrl.getAttribute('posMaxLen', 1);
    }
    //===============================================================
    
    // resolve se aceita digitacao de sinal negativo.
    // apenas se o conteudo do controle esta vazio
    if ( (event.keyCode == 45) && (ctrl.value == ''))
    {
        // teste de limites
        minMax = ctrl.getAttribute('minMax', 1);
        if ( minMax != null )
        {
            if ( minMax[0] < 0 )
                return true;
            else
                return false;    
        }
        // por default nao aceita digitar negativo
        return false;
    }
    
    // resolve se aceita digitacao de ponto ou virgula
    if ( (event.keyCode == 44) || (event.keyCode == 46) )
        if ( nScale <=0 )
            return false;
            
    // nao aceita digitacao diferente de numero ou ponto ou virgula
    if ( !(((event.keyCode >= 48) && (event.keyCode <= 57)) ||
          (event.keyCode == 44) || (event.keyCode == 46)) )
        return false;                    
    
    // o novo valor que tera o numero se aceitar o caracter digitado
    if ( (event.keyCode >= 48) && (event.keyCode <= 57) )
    {
        nextVal = parseFloat(ctrl.value + (event.keyCode - 48).toString());        
        
        // teste de limites
        minMax = ctrl.getAttribute('minMax', 1);
        if ( minMax != null )
        {
            if ( (nextVal < minMax[0]) ||
                 (nextVal > minMax[1]) )        
                return false;
        }
    }
    else
        nextVal = parseFloat(ctrl.value);
        
    // sai fora se nao e numero - resolve a digitacao de deadkey
    // no primeiro caracter
    if ( isNaN (nextVal) )    
        return false;    

    // tamanho da parte inteira se aceitar o caracter
    if ( (nPrecision - nScale) > 0 )
    {
        if ( (nextVal.toString()).indexOf('.') < 0 )
            nextLenInt = (nextVal.toString()).length;
        else
            nextLenInt = (nextVal.toString()).indexOf('.');
    }
    else
        nextLenInt = (nextVal.toString()).length;

    //===============================================================
    // O maxLength para numeros positivos ou negativos
    if ( ((nextVal.toString()).indexOf('-') == 0 ) && (nextLenInt > 0) )   
        nextLenInt--;
    //===============================================================
            
    // tamanho da parte decimal se aceitar o caracter
    if ( nScale > 0 )
    {
        if ( (nextVal.toString()).indexOf('.') < 0 )
            nextLenDec = 0;
        else
            nextLenDec = (nextVal.toString()).length -
                         (nextVal.toString()).indexOf('.') - 1;
    }
    else
        nextLenDec = 0;
        
    // prossegue se a parte inteira e a parte decimal estao dentro da faixa
    // leva em conta numero negativo
    if ( nextVal < 0 )
        signalHouse = 1;
        
    if ( !((nextLenInt <= (nPrecision + signalHouse - nScale)) && (nextLenDec <= nScale)) )
        return false;
        
    // Digitacao ***
    
    // 1. aceita digitar setas
    if ((event.keyCode == 39) || (event.keyCode == 37) || (event.keyCode == 9) ||
        (event.keyCode == 8)  || (event.keyCode == 35) || (event.keyCode == 36))
    {
        return true;
    }
    // 2. digitacao de virgula ou ponto
    else if ( (event.keyCode == 44) || (event.keyCode == 46) )
    {
        // so aceita um ponto
        // se for pressionado virgula aceita trocando por ponto
        if ( ((ctrl.value).indexOf('.') < 0 ) && (nScale > 0) )
        {
            event.keyCode = 46;
            return true;
        }    
        else    
            return false;
    }
    // 3. digitacao de digitos
    else if ( (event.keyCode >= 48) && (event.keyCode <= 57) )
        return true;
    else
        return false;
}

/********************************************************************
Seleciona conteudo de um controle quando o mesmo recebe foco
********************************************************************/
function selFieldContent()
{
    this.select();
}

/********************************************************************
Retorna o valor de um campo de um array multidimensional,
dado o valor de um outro campo.

Parametros:
arrayRef        - referencia ao array
fldOrigIndex    - indice do campo cujo valor e dado
fldOrigValue    - valor dado
fldTargetIndex  - indice do campo a obter o valor

Retorno:
o valor obtido ou null se erro
********************************************************************/
function valueOfFieldInArray(arrayRef, fldOrigIndex, fldOrigValue, fldTargetIndex)
{
    var i;
    var retVal = null;
    
    // trap arrayRef nao e array
    if ( (typeof(arrayRef)).toUpperCase() != 'OBJECT' )
        return null;
    
    // trap array e nulo
    if (arrayRef.length == 0)    
        return null;
        
    // trap fldOrigIndex e fldTargetIndex nao sao numeros inteiros
    // e nao sao >= zero
    if ( isNaN(fldOrigIndex) || isNaN(fldTargetIndex) )
        return null;
    
    if ( (fldOrigIndex < 0) || (fldTargetIndex < 0) )
        return null;    
        
    // trap fldOrigIndex e fldTargetIndex nao estao na faixa
    if ( (fldOrigIndex > arrayRef[0].length) || (fldTargetIndex > arrayRef[0].length) )
        return null;    
        
    // obtem o valor e retorna-o    
    for ( i=0; i<arrayRef.length; i++ )
    {
        if ( arrayRef[i][fldOrigIndex] == fldOrigValue )
        {
            retVal = arrayRef[i][fldTargetIndex];
            break;
        }
    }
    
    return retVal;
}

/********************************************************************
Coloca data no formato MMDDYYYY. Uso geral no sistema.

Parametros:
string data ou qualquer outra string

Retorno:
string data convertida ou sDate se sDate nao e data
********************************************************************/
function putDateInMMDDYYYY2(sDate)
{
    // parece que e data, se nao for devolve como esta
    if ( chkDataEx(sDate) != true )    
        return sDate;
        
    // e data, se o sistema esta no formato MMDDYYYY devolve
    if ( DATE_FORMAT == "MM/DD/YYYY" )
        return sDate;
        
    // e data e o sistema esta no formato DDMMYYYY, inverte e devolve
    var rExp;
    var aString;
    var nDay, nMonth, nYear;
    
    rExp = /\D/g;
    aString = sDate.split(rExp);
    
    if ( (typeof(aString)).toUpperCase() != 'OBJECT' )
        return sDate;
        
    if ( aString.length != 3 )
        return sDate;
        
    nDay = parseInt(aString[0], 10);
    nMonth = parseInt(aString[1], 10);
    nYear = parseInt(aString[2], 10);    
    
    return (nMonth.toString() + '/' + nDay.toString() + '/' + nYear.toString());
}

/********************************************************************
Coloca data no formato YYYYMMDD. Uso geral no sistema.

Parametros:
string data ou qualquer outra string

Retorno:
string data convertida ou sDate se sDate nao e data
********************************************************************/
function putDateInYYYYMMDD(sDate)
{
    // parece que e data, se nao for devolve como esta
    if ( chkDataEx(sDate) != true )    
        return sDate;
        
    // e data e o sistema esta no formato DDMMYYYY, inverte e devolve
    var rExp;
    var aString;
    var nDay, nMonth, nYear;
    
    rExp = /\D/g;
    aString = sDate.split(rExp);
    
    if ( (typeof(aString)).toUpperCase() != 'OBJECT' )
        return sDate;
        
    if ( aString.length != 3 )
        return sDate;

    // e data, se o sistema esta no formato MMDDYYYY devolve
    if ( DATE_FORMAT == "MM/DD/YYYY" )
    {
		nMonth = parseInt(aString[0], 10);
		nDay = parseInt(aString[1], 10);
		nYear = parseInt(aString[2], 10);
    }
    else
    {
		nDay = parseInt(aString[0], 10);
		nMonth = parseInt(aString[1], 10);
		nYear = parseInt(aString[2], 10);
    }
    
    return (nYear.toString() + padL(nMonth.toString(), 2, '0') + padL(nDay.toString(), 2, '0'));
}

/********************************************************************
Esta fun��o converte uma string para data
Par�metros:
	strDate :  String de Data a ser convertida
	
Retorno: Data convertida
********************************************************************/
function strToDate(strDate)
{
    var rExp;
    var aString;
    var nDay, nMonth, nYear;
    
    rExp = /\D/g;
    aString = strDate.split(rExp);
    
    if ( (typeof(aString)).toUpperCase() != 'OBJECT' )
        return null;
    else if ( aString.length != 3 )
        return null;
        
    if ( DATE_FORMAT == "DD/MM/YYYY" )
    {
        nDay = parseInt(aString[0], 10);
        nMonth = parseInt(aString[1], 10);
        nYear = parseInt(aString[2], 10);
    }
    else
    {
        nDay = parseInt(aString[1], 10);
        nMonth = parseInt(aString[0], 10);
        nYear = parseInt(aString[2], 10);
    }
    
    var dReturnDate = new Date(nYear, nMonth - 1, nDay);
    return dReturnDate; 
}

/********************************************************************
A fun��o ValorExtenso calcula o numero por extenso de um valor.
Par�metros:
	nNumero (Obrigat�rio): � o n�mero digitado que ter� o seu valor por extenso calculado;
	sMoedaSingular (Opcional): � a moeda no singular;
	sMoedaPlural (Opcional): � a moeda no plural;
	nIdioma: 0-Portugues, 1-Ingles, 2-Espanhol
Eduardo e Camilo Rodrigues  21/07/2001 10:15, concluida 23/07/2001 11:00.
Eduardo e Marcio Rodrigues  17/09/2004 14:38 - multi-lingua.
********************************************************************/
function valorExtenso(nNumero, sMoedaSingular, sMoedaPlural, nIdioma)
{
	var sRetorno='';

	// Declara��o de variaveis
	{
		var sNumero;
		var nQuantidadeMilhar=5;
		var sNumeroMilhar;
		var sCentena;
		var sDezena;
		var sUnidade;
		var sConectorCentena;
		var sExtensoCentena;
		var sConectorDezena;
		var sExtensoDezena;
		var nTipo;  // {0-nenhum, 1-singular, 2-plural}
		var sMilhar='';
		var nContadorMilhar;
		var nFreio;
		var sConector='';
		var sValorExtenso='';
		var i;
		var j;
		var k=0;

		var aUnidade;
		var aDezenaUnidade;
		var aDezena;
		var aCentena;
		var aMilharSingular;
		var aMilharPlural;
		var aConector;

		// Matriz das unidades
		{
			aUnidade = new Array();
			aUnidade[0]=['Zero','Zero','Zero'];
			aUnidade[1]=['Um','One','Um'];
			aUnidade[2]=['Dois','Two','Dois'];
			aUnidade[3]=['Tres','Three','Tres'];
			aUnidade[4]=['Quatro','Four','Quatro'];
			aUnidade[5]=['Cinco','Five','Cinco'];
			aUnidade[6]=['Seis','Six','Seis'];
			aUnidade[7]=['Sete','Seven','Sete'];
			aUnidade[8]=['Oito','Eight','Oito'];
			aUnidade[9]=['Nove','Nine','Nove'];
		}

		// Matriz das unidades de 11 a 19
		{
			aDezenaUnidade = new Array();
			aDezenaUnidade[1]=['Onze','Eleven','Onze'];
			aDezenaUnidade[2]=['Doze','Twelve','Doze'];
			aDezenaUnidade[3]=['Treze','Thirteen','Treze'];
			aDezenaUnidade[4]=['Quatorze','Fourteen','Quatorze'];
			aDezenaUnidade[5]=['Quinze','Fifteen','Quinze'];
			aDezenaUnidade[6]=['Dezesseis','Sixteen','Dezesseis'];
			aDezenaUnidade[7]=['Dezessete','Seventeen','Dezessete'];
			aDezenaUnidade[8]=['Dezoito','Eighteen','Dezoito'];
			aDezenaUnidade[9]=['Dezenove','Nineteen','Dezenove'];
		}

		// Matriz das dezenas
		{
			aDezena = new Array();
			aDezena[1]=['Dez','Ten','Dez'];
			aDezena[2]=['Vinte','Twenty','Vinte'];
			aDezena[3]=['Trinta','Thirty','Trinta'];
			aDezena[4]=['Quarenta','Forty','Quarenta'];
			aDezena[5]=['Cinquenta','Fifty','Cinquenta'];
			aDezena[6]=['Sessenta','Sixty','Sessenta'];
			aDezena[7]=['Setenta','Seventy','Setenta'];
			aDezena[8]=['Oitenta','Eighty','Oitenta'];
			aDezena[9]=['Noventa','Ninety','Noventa'];
		}
					    
		// Matriz das centenas
		{
			aCentena = new Array();
			aCentena[0]=['Cem','One Hundred','Cem'];
			aCentena[1]=['Cento','One Hundred','Cento'];
			aCentena[2]=['Duzentos','Two Hundred','Duzentos'];
			aCentena[3]=['Trezentos','Three Hundred','Trezentos'];
			aCentena[4]=['Quatrocentos','Four Hundred','Quatrocentos'];
			aCentena[5]=['Quinhentos','Five Hundred','Quinhentos'];
			aCentena[6]=['Seiscentos','Six Hundred','Seiscentos'];
			aCentena[7]=['Setecentos','Seven Hundred','Setecentos'];
			aCentena[8]=['Oitocentos','Eight Hundred','Oitocentos'];
			aCentena[9]=['Novecentos','Nine Hundred','Novecentos'];
		}

		// Matriz dos milhares singular
		{
			aMilharSingular = new Array();
			// aMilharSingular[1]=['Trilhao','Trillion','Trilhao'];
			aMilharSingular[1]=['Bilhao','Billion','Bilhao'];
			aMilharSingular[2]=['Milhao','Million','Milhao'];
			aMilharSingular[3]=['Mil','Thousand','Mil'];
			aMilharSingular[4]=[sMoedaSingular,sMoedaSingular,sMoedaSingular];
			aMilharSingular[5]=['Centavo','Cent','Centavo'];
  		}

		// Matriz dos milhares plural
		{
			aMilharPlural = new Array();
			// aMilharPlural[1]=['Trilhoes','Trillions','Trilhoes'];
			aMilharPlural[1]=['Bilhoes','Billions','Bilhoes'];
			aMilharPlural[2]=['Milhoes','Millions','Milhoes'];
			aMilharPlural[3]=['Mil','Thousand','Mil'];
			aMilharPlural[4]=[sMoedaPlural,sMoedaPlural,sMoedaPlural];
			aMilharPlural[5]=['Centavos','Cents','Centavos'];
		}

		// Matriz dos conectores
		{
			aConector = new Array();
			aConector[0]=[' ',' ',' '];
			aConector[1]=[',',',',','];
			aConector[2]=['e','','e'];
			aConector[3]=['de','','de'];
		}
	}

	// Prepara o numero
	{
		nNumero=roundNumber(nNumero*1000,2);
		sNumero=padL(nNumero.toString(),nQuantidadeMilhar*3,'0');
		sNumero=sNumero.substr(0,sNumero.length-1);
		sNumero=sNumero.substr(0,sNumero.length-2)+'0'+sNumero.substr(sNumero.length-2);
	}
	
	// Looping de Milhares
	for (i=1; i<=nQuantidadeMilhar; i++)
	{
		sNumeroMilhar=sNumero.substr((i-1)*3,3);
		sCentena=sNumeroMilhar.substr(0,1);
		sDezena=sNumeroMilhar.substr(1,1);
		sUnidade=sNumeroMilhar.substr(2,1);
        if ((sNumeroMilhar=='000') && ((i!=nQuantidadeMilhar-1) || (k==0)))
			continue;

		k++;
		// Resolve a Centena
		sConectorCentena='';
		if (sCentena>'0')
		{
			if (sNumeroMilhar=='100')
				sExtensoCentena=aCentena[0][nIdioma];
			else
				sExtensoCentena=aCentena[sCentena][nIdioma];
				
			sValorExtenso+=aConector[0][nIdioma]+sExtensoCentena;

			if (aConector[2][nIdioma] != "")
				sConectorCentena=aConector[2][nIdioma]+aConector[0][nIdioma];
		}

		// Resolve a Dezena
		sConectorDezena=sConectorCentena;
		if (sDezena>'0')
		{
			if ((sNumeroMilhar.substr(1,2)>='11') && (sNumeroMilhar.substr(1,2)<='19'))
				sExtensoDezena=aDezenaUnidade[sUnidade][nIdioma];
			else
				sExtensoDezena=aDezena[sDezena][nIdioma];

			sValorExtenso+=aConector[0][nIdioma]+sConectorCentena+sExtensoDezena;

			if (aConector[2][nIdioma] != "")
				sConectorDezena=aConector[2][nIdioma]+aConector[0][nIdioma];
		}
					
		// Resolve a Unidade
		if ((sUnidade>'0') && (sDezena!='1'))
		{
			sExtensoUnidade=aUnidade[sUnidade][nIdioma];
			sValorExtenso+=aConector[0][nIdioma]+sConectorDezena+sExtensoUnidade;
		}				
		// Resolve o Milhar
		if (i==nQuantidadeMilhar-1)
			nTipo=(sNumero.substr(0,i*3)>padL('1',i*3,'0')?2:1);
		else
			nTipo=(sNumeroMilhar=='000'?0:(sNumeroMilhar>'001'?2:1));
					
		if (nTipo==0)
			sMilhar='';
		else if (nTipo==1)
			sMilhar=aConector[0][nIdioma]+aMilharSingular[i][nIdioma];
		else if (nTipo=2)
			sMilhar=aConector[0][nIdioma]+aMilharPlural[i][nIdioma];
					
		sValorExtenso+=sMilhar;	
										
		// Resolve o Conector
		nContadorMilhar=0;
		nFreio=(i<nQuantidadeMilhar-1?2:1);
		for (j=i; j<=nQuantidadeMilhar-nFreio; j++)
		{
					
			if (sNumero.substr(j*3,3)!='000')
				nContadorMilhar++;
		}

		sConector='';
		if (nContadorMilhar==0)
		{
			if (i<nQuantidadeMilhar-2)
				sConector=aConector[0][nIdioma]+aConector[3][nIdioma];
		}		

		else if (nContadorMilhar==1)
			sConector=aConector[0][nIdioma]+aConector[2][nIdioma];
		else
			sConector=aConector[1][nIdioma];

		sValorExtenso+=sConector;
	}
															
	sRetorno=trimStr(sValorExtenso);

	// Retorna o resultado
	return sRetorno;
}

/********************************************************************
A Fun��o Digito efetua a valida��o de CNPJ, CPF, D�gito X ou 0.
Par�metros:
	sNumero (Obrigat�rio): � o n�mero digitado que ter� o seu d�gito calculado/verificado;
	nTipo (Opcional): � o tipo de calculo que ser� feito, sendo:
		(1-CNPJ, 2-CPF, 8-D�gito X, 9-D�gito 0 (op��o default));
	nVerifica (Opcional): � a forma de verifica��o, sendo:
		1-Verifica se o d�gito est� correto, retornando true ou false (op��o default);
		2-Retorna o d�gito correto e emite um alerta;
		3-Apenas retorna o d�gito correto.
Eduardo Rodrigues  25/02/2001 12:30
********************************************************************/
function verificaDigito(sNumero, nTipo, nVerifica)
{
	// Verifica se algum par�metro � nulo
	if (nTipo == null)
		nTipo = 9;

	if (nVerifica == null)
		nVerifica = 1;

	// Verifica se os tipos dos par�mentros s�o v�lidos
	if (typeof(sNumero) != 'string' || typeof(nTipo) != 'number' || typeof(nVerifica) != 'number')
		nRetorno = 11;
	else
	{
		// Declara e inicializa vari�veis
		var nTamanhoRequerido;
		var nLimite;
		var nQtyDigitos;
		var sX;
		var nModulo;

		var nRetorno = 10;
		var nTamanho = sNumero.length;

		// Inicializa vari�veis, dependendo de nTipo
		if (nTipo == 1)
		{
			nTamanhoRequerido = 14;
			nQtyDigitos = 2;
			nLimite = 9;
			sX = '0';
			nModulo = 11;
		}
		else if (nTipo == 2)
		{
			nTamanhoRequerido = 11;
			nQtyDigitos = 2;
			nLimite = 11;
			sX = '0';
			nModulo = 11;
		}
		else if (nTipo == 8)
		{
			nTamanhoRequerido = 0;
			nQtyDigitos = 1;
			nLimite = 9;
			sX = 'X';
			nModulo = 11;
		}
		else if (nTipo == 9)
		{
			nTamanhoRequerido = 0;
			nQtyDigitos = 1;
			nLimite = 9;
			sX = '0';
			nModulo = 11;
		}

		// Verifica se a quantidade de digitos � maior que o tamanho digitado
		if (nQtyDigitos >= nTamanho)
			nRetorno = 11;

		// Verifica se o tamanho digitado est� de acordo com o tamanho requerido
		else if (nTamanhoRequerido > 0 && nTamanho != nTamanhoRequerido)
			nRetorno = 12;

		else
			// Chama a fun��o calculaDigito 
			nRetorno = calculaDigito(sNumero, nTipo, nVerifica, nQtyDigitos, nLimite, sX, nModulo);
	}

	// Trata o retorno do resultado
	if (nRetorno <= 10)
	{
		if (nVerifica == 1)
			nRetorno = true;
		//else if (nVerifica == 2)
		//	alert('O d�gito � '+nRetorno);
	}
	else
	{
		// Trata Erros
		aMensagemErro = new Array(5);
		aMensagemErro[0] = 'OK';
		aMensagemErro[1] = 'Paramentos inv�lidos';
		aMensagemErro[2] = 'Tamanho incorreto';  // S� CNPJ/CPF
		aMensagemErro[3] = 'CNPJ inv�lido';  // S� CNPJ
		aMensagemErro[4] = 'Digito inv�lido';

		//alert(aMensagemErro[nRetorno-10]);
				
		nRetorno = false;
	}

	// Retorna o resultado
	return nRetorno;
}

/********************************************************************
A Fun��o CalculaDigito calcula efetivamente o DV (D�gito Verificador).
Par�metros:
	sNumero (Obrigat�rio): � o n�mero digitado que ter� o seu d�gito calculado/verificado;
	nTipo (Obrigat�rio): � o tipo de calculo que ser� feito, sendo:
		(1-CNPJ, 2-CPF, 8-D�gito X, 9-D�gito 0 (op��o default));
	nVerifica (Obrigat�rio): � a forma de verifica��o, sendo:
		1-Verifica se o d�gito est� correto, retornando true ou false (op��o default);
		2-Retorna o d�gito correto e emite um alerta;
		3-Apenas retorna o d�gito correto.
    nQtyDigitos (Obrigat�rio): � a quantidade de d�gitos do DV;
    nLimite (Obrigat�rio): � o limite a partir do qual o nMultiplicador recomeca em 2;
    sX (Obrigat�rio): � o d�gito que a fun��o deve retorna se o DV for maior que 9;
    nModulo (Obrigat�rio): � a base do M�dulo para efeito de c�lculo do DV.
Eduardo Rodrigues  25/02/2001 12:30
********************************************************************/
function calculaDigito(sNumero, nTipo, nVerifica, nQtyDigitos, nLimite, sX, nModulo)
{
    // Inicializa vari�veis
    var nRetorno = 10;
    var nTamanho = sNumero.length;

    // Verifica se CNPJ � v�lido
    if (nTipo == 1 && sNumero.substr(0,1) != '0')
    {
    	var sNumero0 = '';
    	var sNumero1 = '';
    	var sNumero2 = '';
    	for (i=0; i<8; i++)
    	{
    		if (i%2 == 0)
    			sNumero1 += (sNumero.toString()).substr(i,1);
    		else
    			sNumero2 += (sNumero.toString()).substr(i,1);
    	}
    	sNumero1*=2;
    	sNumero0 = sNumero1.toString() + sNumero2.toString();
    				
    	var nSum = 0;
    	var nTamNum = eval(sNumero0.length);
    	for (i=0; i<nTamNum; i++)
    		nSum += eval(sNumero0.substr(i,1));
    	if (nSum%10 != 0)
    	{
    		nRetorno = 13;
    		return nRetorno;
    	}
    }

    // Declara e inicializa vari�veis
    var nMultiplicador;

    var nDigito = 0;
    var sDigito = '';

    // Calcula o D�gito Verificador
    for (i=0; i<nQtyDigitos; i++)
    {
        nMultiplicador = 1;
        nSum = 0;
    			    
        for (j=0; j<nTamanho-nQtyDigitos+i; j++)
        {
            nMultiplicador = (nMultiplicador>=nLimite ? 2 : ++nMultiplicador);
            nSum += eval(sNumero.substr(nTamanho-1-nQtyDigitos+i-j,1)) * nMultiplicador;
        }

        nDigito = nModulo - (nSum%nModulo);
        
        if (nDigito==nModulo)
            nDigito='0';
        else if (nDigito==(nModulo-1))
            nDigito=sX;
            
        // Verifica se o DV � v�lido
        if (nVerifica == 1)
        {
            if (nDigito != sNumero.substr(nTamanho-nQtyDigitos+i,1))
            {
               nRetorno = 14;
               return nRetorno;
            }
        }
        sDigito += nDigito;
    }
    			
    // Ajusta o resultado
    if (nVerifica >= 2)
        nRetorno = sDigito;
    			
    // Retorna o resultado
    return nRetorno;
}

/********************************************************************
Substitui parte de string por outra
********************************************************************/
function replaceStr(sString, sFromStr, sToStr)
{
    while (sString.indexOf(sFromStr) >= 0)
        sString = sString.replace(sFromStr,sToStr);

    return sString;
}

/********************************************************************
Abre Outlook eventualmente parametrizado co Tos, Ccs, Bccs etc
Parametros:
arrayTo     - array dos enderecos To
arrayCc     - array dos enderecos Cc
arrayBcc    - array dos enderecos Bcc
theSubject  - subject da mensagem
theBody     - corpo da mensagem

Caso a linha de comando para abrir o Outlook ultrapasse 1000 chars
retorna chamando funcao openOutLookAndSetClipB()

Retorno:
irrelevante
********************************************************************/
function useOutLook(arrayTo, arrayCc, arrayBcc, theSubject, theBody)
{
    // Exemplos:
    //window.location = "mailto:jose@terra.com.br?cc=marcio@terra.com.br&bcc=eduardo@terra.com.br&subject=teste";
    //window.location = "mailto:?cc=marcio@terra.com.br&bcc=eduardo@terra.com.br&subject=teste";
    //window.location = "mailto:jose@terra.com.br?cc=marcio@terra.com.br&bcc=eduardo@terra.com.br&subject=teste&body=Este � um texto de exemplo!!!";
    
    var i;
    var strPars = new String();
    var nextBlkUnion;
    var addBlkTo = false;
    var addBlkCc = false;
    
    strPars = 'mailto:';
    
    nextBlkUnion = '';
    // To
    for (i=0;i<arrayTo.length; i++)
    {
        if (arrayTo[i] == null)
            continue;
        
        strPars += arrayTo[i];
        
        // se strPars.length >= 1000
        // abre Outlook em branco e coloca lista de e-mails no clipboard
        if ( strPars.length >= 1000 )
        {
            openOutLookAndSetClipB(arrayTo, arrayCc, arrayBcc);
            return null;
        }    
            
        if ( i<arrayTo.length - 1 )
            strPars += ';';        
        
        addBlkTo = true;    
    }
    
    if ( addBlkTo )
        nextBlkUnion = '?';
    else
        nextBlkUnion = '?';
        
    // Cc
    for (i=0;i<arrayCc.length; i++)
    {
        if (arrayCc[i] == null)
            continue;
    
        if ( nextBlkUnion != '' )
        {
            strPars += trimStr(nextBlkUnion) + 'cc='+ arrayCc[i];
            nextBlkUnion = '';
        }    
        else
            strPars += arrayCc[i];

        // se strPars.length >= 1000
        // abre Outlook em branco e coloca lista de e-mails no clipboard
        if ( strPars.length >= 1000 )
        {
            openOutLookAndSetClipB(arrayTo, arrayCc, arrayBcc);
            return null;
        }    
            
        if ( i<arrayCc.length - 1 )
            strPars += ';';        

        addBlkCc = true;
    }
    
    if ( (addBlkTo == true) && (addBlkCc == true) )
        nextBlkUnion = '&';        
    else if ( (addBlkTo == true) && (addBlkCc == false) )
        nextBlkUnion = '?';        
    else if ( (addBlkTo == false) && (addBlkCc == true) )
        nextBlkUnion = '&';
    else if ( (addBlkTo == false) && (addBlkCc == false) )
        nextBlkUnion = '?';                    
    
    // Bcc
    for (i=0;i<arrayBcc.length; i++)
    {
        if (arrayBcc[i] == null)
            continue;
    
        if ( nextBlkUnion != '' )
        {
            strPars += trimStr(nextBlkUnion) + 'bcc='+ arrayBcc[i];
            nextBlkUnion = '';            
        }    
        else
            strPars += arrayBcc[i];

        // se strPars.length >= 1000
        // abre Outlook em branco e coloca lista de e-mails no clipboard
        if ( strPars.length >= 1000 )
        {
            openOutLookAndSetClipB(arrayTo, arrayCc, arrayBcc);
            return null;
        }    
            
        if ( i<arrayBcc.length - 1 )
            strPars += ';';        
    }
    
    // Subject
    if ( theSubject != null )
    {
        strPars += nextBlkUnion + 'subject='+ escape(theSubject);
        nextBlkUnion = '&';
    }    
            
    // Corpo
    if ( theBody != null )
    {
        strPars += nextBlkUnion + 'body='+ escape(theBody);
    }    
    
    // abre Outlook eventualmente com To, Cc e Bcc preenchidos
    // se strPars.length <=1000;
    // se strPars.length >= 1000
    // abre Outlook em branco e coloca lista de e-mails no clipboard
    if ( strPars.length >= 1000 )
    {
        openOutLookAndSetClipB(arrayTo, arrayCc, arrayBcc);
        return null;
    }    
    
    window.location = strPars;
}

/********************************************************************
Abre Outlook em branco e coloca lista de emails no clipboard

Parametros:
arrayTo     - array dos enderecos To
arrayCc     - array dos enderecos Cc
arrayBcc    - array dos enderecos Bcc

Retorno:
irrelevante
********************************************************************/
function openOutLookAndSetClipB(arrayTo, arrayCc, arrayBcc)
{
    var strPars = new String();
    var nextBlkUnion;
    var i;
    
    window.location = 'mailto:?body=' + escape('Use Ctrl-V para colar emails selecionados!');
    
    strPars = '';

    // To
    for (i=0;i<arrayTo.length; i++)
    {
        if (arrayTo[i] == null)
            continue;
        
        strPars += arrayTo[i];
            
        if ( i<arrayTo.length - 1 )
            strPars += ';';        
    }
    
    if ( (strPars != '') && (strPars.charAt(strPars.length - 1) != ';') && (arrayCc.length >=0) )
        strPars += ';';
    
    // Cc
    for (i=0;i<arrayCc.length; i++)
    {
        if (arrayCc[i] == null)
            continue;
    
        strPars += arrayCc[i];
            
        if ( i<arrayCc.length - 1 )
            strPars += ';';        
    }
        
    if ( (strPars != '') && (strPars.charAt(strPars.length - 1) != ';') && (arrayBcc.length >=0) )
        strPars += ';';
    
    // Bcc
    for (i=0;i<arrayBcc.length; i++)
    {
        if (arrayBcc[i] == null)
            continue;
    
        strPars += arrayBcc[i];
            
        if ( i<arrayBcc.length - 1 )
            strPars += ';';        
    }
    
    if ( (strPars != '') && (strPars.charAt(strPars.length - 1) == ';') )
        strPars += strPars.substr(0, strPars.length - 1);
    
    if ( strPars != '' )
        window.clipboardData.setData('Text', strPars);
    
    return null;
}

/********************************************************************
Campos numericos originais linkados a tabelas.
Implementa eventos onbeforepaste e onpaste em campos textos numericos.
Na funcao maxLenOnKeyPressSetArrays, os campos numericos
receberam o atributo 'verifyNumPaste', 1, 1

Campos numericos originais nao linkados
Junto a implementacao do onkeypress = verifyNumericEnterNotLinked()
os campos numericos ano linkados precisam ter os seguintes atributos:
'verifyNumPaste', 1, 1
'thePrecision'
'theScale'
********************************************************************/
function __implementsPasteInTxtFld()
{
    var i, coll;
    
    coll = window.document.getElementsByTagName('INPUT');
    
    for ( i=0; i<coll.length; i++ )
    {
        if ( (coll.item(i).type.toUpperCase() == 'TEXT') ||
             (coll.item(i).type.toUpperCase() == 'PASSWORD') )
        {
            // Campos originais linkados a tabelas
            if ( coll.item(i).getAttribute('verifyNumPaste', 1) != null )
            {
                coll.item(i).onbeforepaste = __beforepasteTxtFld;
                coll.item(i).onpaste = __pasteTxtFld;
            }
        }
    }
}

/********************************************************************
Evento on before paste em campos textos.
Sendo o campo numerico, verifica se tem limites de valor e se
o valor a ser passado esta dentro da faixa.
Se o conteudo do clipboard atender, por principio o conteudo atual
do campo sera sempre substituido pelo do clipboard, ou seja,
insercao nao sera permitido
********************************************************************/
function __beforepasteTxtFld()
{
    // teste de limites
    // getAttribute('minMax', 1);
    // minMax[0] -> vlr minimo
    // minMax[0] -> vlr maximo
    
    var ctrl = event.srcElement;
    
    if ( ctrl.readOnly )
        return false;
    
    var clipContent = window.clipboardData.getData('Text');
    var nClipData;
    var minMax = null;
    var nPrecision;
    var nScale;
    var algQty;
    
    // Trata o dado do clipboard
    // se nao for numerico, sai fora
    if ( isNaN(clipContent) )
    {
        __msgNotPaste(ctrl);
        return false;
    }
    
    // Trima conteudo do clipboard    
    clipContent = trimStr(clipContent);
    
    if ( clipContent.length == 0 )
        return false;
    
    nClipData = parseFloat(clipContent);
    
    // Obtem a precisao e o scale do controle
    nPrecision = ctrl.getAttribute('thePrecision', 1);
    nScale = ctrl.getAttribute('theScale', 1);
    
    // Ajusta as casas decimais do dado se for o caso
    nPrecision = ctrl.getAttribute('thePrecision', 1);
    if ( nScale != null )    
        nClipData = roundNumber(nClipData, nScale);
    
    // Se o controle pede verificacao do paste
    if ( ctrl.getAttribute('verifyNumPaste', 1) != null )
    {
        minMax = ctrl.getAttribute('minMax', 1);

        // Se necessario, verifica se o dado do clipboard esta no limite
        if ( minMax != null )
        {
            if ( !((nClipData >= minMax[0]) &&
                   (nClipData <= minMax[1])) )    
            {
                __msgNotPaste(ctrl);   
                return false;
            }    
        }
        else
        {
            // por default nao aceita digitar negativo
            nClipData = Math.abs(nClipData);
        }
        
        // Verifica se o dado do clipboard esta no limite
        // do precision e do scale
        // A quantidade de algarismos do dado do clipboard
        // tem que ser menor ou igual o precision
        if ( nPrecision != null )
        {
            algQty = (nClipData.toString()).length;
            // abate o sinal menos se dado do clipboard e negativo
            if ( (algQty.toString()).indexOf('-') >=0 )
                algQty--;
            // abate o ponto
            if ( (algQty.toString()).indexOf('.') >=0 )
                algQty--;
            if (algQty > parseInt(nPrecision, 10))
            {
                __msgNotPaste(ctrl);
                return false;
            }    
        }    
    }    

    ctrl.value = nClipData;
        
    event.returnValue = false;
}

/********************************************************************
Evento on paste em campos textos.
Se o campo tem o atributo 'verifyNumPaste' o paste ja foi tratado
na funcao __beforepasteTxtFld() e sempre retorna false (nunca executa
o paste).
********************************************************************/
function __pasteTxtFld()
{
    var ctrl = event.srcElement;
    
    if ( ctrl.getAttribute('verifyNumPaste', 1) != null )
        event.returnValue = false;
    else
        event.returnValue = true;
}

/********************************************************************
Mensagem se nao for feito o paste
********************************************************************/
function __msgNotPaste(ctrl)
{
    if ( window.top.overflyGen.Alert('Valor a colar n�o compat�vel!') == 0 )
        return null;
        
    window.focus();    
    ctrl.focus();    
}

/********************************************************************
Remove caracteres nao numericos e retorna so os numericos de uma
string
Parametros      - a string a estripar
Retorno         - a string estripada ou uma string vazia
                - se o parametro nao e compativel
********************************************************************/
function stripNonNumericChars(strToStrip)
{
    if ( strToStrip == null )
        return '';
            
    if ( (typeof(strToStrip)).toUpperCase() == 'UNDEFINED' )    
        return '';
    
    if ( !(((typeof(strToStrip)).toUpperCase() == 'NUMBER') ||
           ((typeof(strToStrip)).toUpperCase() == 'STRING')) )
        return '';
        
    if ( strToStrip == '' )
        return '';
        
    var tempStr = strToStrip.toString();
    var retStr = '';
   
    for ( i=0; i<tempStr.length; i++  )
    {
        if ( !isNaN(parseInt(tempStr.charAt(i), 10)) )
            retStr += tempStr.charAt(i);
    }
    
    return retStr;
}

/********************************************************************
Remove caracteres nao alphanumericos de uma string

Parametros      - a string a estripar
Retorno         - a string estripada ou uma string vazia
                - se o parametro nao e compativel
********************************************************************/
function stripNonAlphaNumericChars(strToStrip)
{
    if ( strToStrip == null )
        return '';
            
    if ( (typeof(strToStrip)).toUpperCase() == 'UNDEFINED' )    
        return '';
    
    if ( !(((typeof(strToStrip)).toUpperCase() == 'NUMBER') ||
           ((typeof(strToStrip)).toUpperCase() == 'STRING')) )
        return '';
        
    if ( strToStrip == '' )
        return '';
        
    var tempStr = strToStrip.toString();
    var retStr = '';
    var currChar = '';
    
    for ( i=0; i<tempStr.length; i++  )
    {
        currChar = tempStr.charAt(i);

        if ( ((currChar >= '0') && (currChar <= '9')) || ((currChar >= 'a') && (currChar <= 'z')) || 
             ((currChar >= 'A') && (currChar <= 'Z')))
            retStr += tempStr.charAt(i);
    }

    return retStr;
}

/********************************************************************
Transorma uma string em string formato numerico coforme configurado
na maquina do usuario, estripando os
separadores de milhar e garantindo o separador de decimal como
ponto.

Parametros      - a string transformar
Retorno         - a string transformada ou uma string vazia
                  se o parametro for null ou ''
********************************************************************/
function transformStringInNumeric(strToTransform)
{
	if ( (strToTransform == null) || (strToTransform == '') )
		return '';
		
	var strRet = '';
	var i, currChar;
	
	strToTransform = removeCharFromString(strToTransform, THOUSAND_SEP);
	strToTransform = removeCharFromString(strToTransform, ' ');
	
	for ( i=0; i<strToTransform.length; i++  )
    {
        currChar = strToTransform.charAt(i);
        
        if ( !((currChar >= '0') && (currChar <= '9') || (currChar == ',') || (currChar == '.')) )
			continue;

		if ( currChar == ',' )
			currChar = '.';
        
        strRet += currChar;
    }
    
    return strRet;	
}

/********************************************************************
Troca operadores matematicos dentro de uma string por: & + seu valor 
hexa, somente para manter compatibilidade com o metodo 
Request.QueryString nas paginas ASP

Parametros      - strToParse, string a ser analizada

Retorno         - string com operadores trocados
********************************************************************/
function parseStrEscaped(strToParse)
{
    if (strToParse == null)
        return '';

    var i;
    var sRet = '';
    
    for (i=0; i<strToParse.length; i++)
    {
        if ( strToParse.charAt(i) == '+' )
            sRet += '%2B';
        else if ( strToParse.charAt(i) == '-' )
            sRet += '%2D';
        else if ( strToParse.charAt(i) == '*' )
            sRet += '%2A';
        else if ( strToParse.charAt(i) == '/' )
            sRet += '%2F';
        else    
            sRet += strToParse.charAt(i);
    }
    
    return sRet;
}

/********************************************************************
Ordena um array de elementos string, em ordem crescente

Parametros:
arrayToSort      - Referencia do array a ser ordenado
nArrayDimension  - (opcional), dimensao a ser usada na ordenacao caso
                   o array seja multidimensional

Retorno:      null
********************************************************************/
function asort(arrayToSort, nArrayDimension)
{
    var i, j, k, vTemp;
    var nFim = arrayToSort.length;
    
	if (nArrayDimension != null)
	{
	    if (arrayToSort[0].lenght > nArrayDimension)
	        return null;
	}
	
	nArrayDimension = (nArrayDimension == null ? 0 : nArrayDimension);
    
    for (j=nFim; j>=1; j--)
    {
        for (i=0; i<=j-2; i++)
        {
            if (arrayToSort[i][nArrayDimension] > arrayToSort[i+1][nArrayDimension])
            {
                // Array unidimensional
                if (nArrayDimension == null)
                {
                    vTemp = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i+1];
                    arrayToSort[i+1] = vTemp;
                }
                // Array multidimensional
                else
                {
                    for (k=0; k<arrayToSort[i].length; k++)
                    {
                        vTemp = arrayToSort[i][k];
                        arrayToSort[i][k] = arrayToSort[i+1][k];
                        arrayToSort[i+1][k] = vTemp;
                    }
                }
            }
        }
    }
    
    return null;
}

/********************************************************************
Traduz um termo escrito no idioma do sistema(Portugues) para outro
idioma (atualmente somente para o Ingles)

Parametro:
sTherm      - Termo a ser traduzido

Retorno:
            String - Termo traduzido.
********************************************************************/
function translateTherm(sTherm, aTranslate)
{
    var aEmpresa = getCurrEmpresaData();
	var nIdiomaDeID = parseInt(aEmpresa[7], 10);
	var nIdiomaParaID = parseInt(aEmpresa[8], 10);
	var retVal = '';
	var nSeek;

    if ( (sTherm == null) || (sTherm == '') || (nIdiomaDeID == nIdiomaParaID) || (nIdiomaParaID == 246) )
        return sTherm;

    nSeek = aseek(aTranslate, sTherm, 0);
    if ( nSeek >= 0 )
        retVal = aTranslate[nSeek][1];
    else
        retVal = '';

    return retVal;
}

/********************************************************************
Faz uma pesquisa binaria em um array.

Parametros:
aArray     Array a usado como fonte de pesquisa
strToSeek  String a ser pesquisada
nArrayDimension  - (opcional), dimensao a ser usada na pesquisa caso
                   o array seja multidimensional

Retorno:   Indice do elemento encontrado no array
           -1 caso o valor nao foi encontrado
********************************************************************/
function aseek(aArray, strToSeek, nArrayDimension)
{
	var nInicio = 0;
	var nFinal = aArray.length - 1;
	var nMetade = 0;
	var vMetadeValue;
	var retVal = -1;
	
	if (nArrayDimension != null)
	{
	    if (aArray[0].lenght > nArrayDimension)
	        return retVal;
	}
	
	if (trimStr(strToSeek) == '')
	    return retVal;

	while ( nInicio <= nFinal)
	{
		nMetade = Math.ceil((nInicio + nFinal)/2);

        // Ternario em funcao do Array ser unidimensional ou multidimensional
		vMetadeValue = (nArrayDimension == null ? aArray[nMetade] : aArray[nMetade][nArrayDimension]);

		if (strToSeek < vMetadeValue)
			nFinal = nMetade - 1;
		else
			if (strToSeek > vMetadeValue)
				nInicio = nMetade + 1;
			else
				nInicio = nFinal + 1;
	}

	if (strToSeek == (nArrayDimension == null ? aArray[nMetade] : aArray[nMetade][nArrayDimension]))
		retVal = nMetade;

    return retVal;
}

/********************************************************************
Transforma um date ou um datetime incompleto para um datetime completo.
Um datetime completo tem o seguinte formato
dd/mm/yyyy hh:mm:ss ou mm/yy/yyyy hh:mm:ss

Parametros      - sDateTime  e o date, datetime a normalizar
                - forceMMDDYYYY -> se != null forca MM/DD/YYY format

Retorno         - date, datetime normalizada ou null se nao consegue
********************************************************************/
function normalizeDate_DateTime(sDateTime, forceMMDDYYYY)
{
    var retVal = null;
    var rExp, aDateTime;
    
    rExp = /\D/g;
    aDateTime = (sDateTime).split(rExp);
    
    if ( (typeof(aDateTime)).toUpperCase() != 'OBJECT' )
        return retVal;
    
    if ( aDateTime.length == 3 )
    {
        retVal = aDateTime[0] + '/' + aDateTime[1] + '/' + aDateTime[2] +
                               ' 00:00:00';
           
        // sempre formato mes/dia/ano
        if ( forceMMDDYYYY != null )
        {
            if ( DATE_FORMAT == "DD/MM/YYYY" )
            {
                retVal  = aDateTime[1] + '/' + aDateTime[0] + '/' + aDateTime[2] +
                                         ' 00:00:00';
            }
        }    
    }
    else if ( aDateTime.length == 4 )
    {
        retVal = aDateTime[0] + '/' + aDateTime[1] + '/' + aDateTime[2] +
                               ' ' + aDateTime[3] + ':00:00';
           
        // sempre formato mes/dia/ano
        if ( forceMMDDYYYY != null )
        {
            if ( DATE_FORMAT == "DD/MM/YYYY" )
            {
                retVal = aDateTime[1] + '/' + aDateTime[0] + '/' + aDateTime[2] +
                                       ' ' + aDateTime[3] + ':00:00';
            }
        }
    }
    else if ( aDateTime.length == 5 )
    {
        retVal = aDateTime[0] + '/' + aDateTime[1] + '/' + aDateTime[2] +
                               ' ' + aDateTime[3] + ':' + aDateTime[4] + ':00';
           
        // sempre formato mes/dia/ano
        if ( forceMMDDYYYY != null )
        {
            if ( DATE_FORMAT == "DD/MM/YYYY" )
            {
                retVal = aDateTime[1] + '/' + aDateTime[0] + '/' + aDateTime[2] +
                                       ' ' + aDateTime[3] + ':' + aDateTime[4] + ':00';
            }
        }    
    }
    else if ((aDateTime.length == 6) || (aDateTime.length == 7))
    {
        retVal = aDateTime[0] + '/' + aDateTime[1] + '/' + aDateTime[2] +
                               ' ' + aDateTime[3] + ':' + aDateTime[4] + ':' + aDateTime[5];
           
        // sempre formato mes/dia/ano
        if ( forceMMDDYYYY != null )
        {
            if ( DATE_FORMAT == "DD/MM/YYYY" )
            {
                retVal = aDateTime[1] + '/' + aDateTime[0] + '/' + aDateTime[2] +
                                       ' ' + aDateTime[3] + ':' + aDateTime[4] + ':' + aDateTime[5];
            }
        }
    }
    
    return retVal;                
}

/********************************************************************
Verifica se um dado valor e um hexa correto

Parametro:
vlr     - valor a verificar

Retorno:
true ou false
********************************************************************/
function check_Flds_ForHex(vlr)
{
    var retVal =  false;
    var i, vlrChar;
    
    vlr = trimStr(vlr);
    
    if ( (vlr == null) || (vlr == '') )
        return retVal;
    
    vlr = vlr.toString();
    vlr = vlr.toUpperCase();
        
    if ( vlr == '0' )
    {    
        retVal = true;
        return retVal;
    }    
    
    retVal = true;
    
    for ( i=0; i< vlr.length; i++ )
    {
        vlrChar = vlr.charCodeAt(i);
        if ( !(((vlrChar >= 48) && (vlrChar <= 57)) || ((vlrChar >= 65) && (vlrChar <= 70)) || (vlrChar == 45)) )
        {
            retVal = false;
            break;
        }
    }
    
    return retVal;
}

/********************************************************************
Ajusta o label de controle combo incluindo ID no texto,
de acordo com o ID do item selecionado
no combo

Parametro:
lblRef      - referencia ao label
sID         - Id

Retorno:
nenhum
********************************************************************/
function setLabelOfControl(lblRef, sID)
{
    if ((sID == null) || (sID == 0))
        sID = '';
    else
        sID = ' ' + sID;
    
    var sLabelTranslated = translateTerm(getLabelNumStriped(lblRef.innerText), null);
    lblRef.style.width = FONT_WIDTH * (sLabelTranslated + sID).length;
    lblRef.innerText = sLabelTranslated + sID;
}

/********************************************************************
Ajusta o label de controle combo incluindo ID no texto,
de acordo com o ID do item selecionado
no combo. Segunda funcao extendida.

Parametro:
lblRef      - referencia ao label
cmbRef      - referencia ao combo

Retorno:
nenhum
********************************************************************/
function setLabelOfControlEx(lblRef, cmbRef)
{
    var sID = null;
    
    if ( cmbRef.selectedIndex > -1 )
        sID = (cmbRef.value).toString();
    
    if ( (sID == null) || (sID == '0') )
        sID = '';
    else
        sID = ' ' + sID;
    
    var sLabelTranslated = translateTerm(getLabelNumStriped(lblRef.innerText), null);
    lblRef.style.width = FONT_WIDTH * (sLabelTranslated + sID).length;
    lblRef.innerText = sLabelTranslated + sID;
}

/********************************************************************
Pesquisa options de um combo, por value
Parametros:
    cmbRef      - referencia ao combo
    theValue    - value a pesquisar
Retorno:
    null se erros em parametros
    false - o value nao existe no combo
    true  - - value existe no combo
********************************************************************/
function lookUpValueInCmbEx(cmbRef, theValue)
{
	if ( (cmbRef == null) || (theValue == null) )
		return null;
		
	var r, re;
	var s = cmbRef.innerHTML;
	
	re = new RegExp('value=' + theValue, 'i');
	r = s.match(re);
	
	if (r == null)
		return false;
	
	return true;	
}

/********************************************************************
Pesquisa options de um combo, por value
Parametros:
    cmbRef      - referencia ao combo
    theValue    - value a pesquisar
Retorno:
    null se erros em parametros
    -1 se nao encontrou ou a posicao do option na colecao de options
********************************************************************/
function lookUpValueInCmb(cmbRef, theValue)
{
    if ( (cmbRef == null) || (theValue == null) )
        return null;
        
    var i;
    var retVal = -1;
    
    for ( i=0; i<cmbRef.options.length; i++ )    
    {
        if ( cmbRef.options.item(i).value == theValue )
        {
            retVal = i;
            break;
        }
    }
    
    return retVal;
}

/********************************************************************
Retorna ULR em portugues ou ingles para e-mails ou ulrs
Parametros:
    nIdiomaID   - idioma da empresa logada
    nTipoURLID  - 124 == e-mail 125 == URL

Retorno:
    a string URL
********************************************************************/
function urlEmailTerminator(nIdiomaID, nTipoURLID)
{
	var retVal = '';

	if ((nIdiomaID == null) || (nIdiomaID == '') || (nIdiomaID == 0))
	    return retVal;
	
	if ((nTipoURLID == null) || (nTipoURLID == '') || (nTipoURLID == 0))
	    return retVal;
	
	// Se e-mail
	if (nTipoURLID == 124)
	    retVal = '_@_.com';
	// Se URL    
	else if (nTipoURLID == 125)
	    retVal = 'http://www._.com';
	else
	    return retVal;
	    
    // Se o Idioma e Portugues
    if (nIdiomaID == 246)
        retVal += '.br';
	
	return retVal;
}

/********************************************************************
Remove todas as ocorrencias de um determinado caracter de uma string

Parametros:

sString   - String a ser processada
cChar     - Caracter a ser removido
********************************************************************/
function removeCharFromString(sString, cChar)
{
    var i;
    var sRetorno;
    
    if ((sString == null) || (sString == '') || (cChar == null) || (cChar == ''))
        return sString;
    
    sRetorno = '';
    for (i=0; i<sString.length; i++)
    {
        if (sString.charAt(i) != cChar)
            sRetorno += sString.charAt(i);
    }
    
    return sRetorno;
}

/********************************************************************
Transforma ponto e traco e outros caracteres para barra em pretensas datas
A funcao nao e valida para data : hora

Parametros:

sString   - String a ser processada
********************************************************************/
function normalizeDateFormat(sString)
{
	var i;
    var sRetorno;
    var sepDateQty;
    var sepHourQty;
    
    if ( (sString == null) || (sString == '') )
        return sString;
    
    sRetorno = '';
    sepDateQty = 0;
    sepHourQty = 0;
    bFlagSpcDateHour = false;
    
    for (i=0; i<sString.length; i++)
    {
        if ( !((sString.charCodeAt(i) >= 48) &&
               (sString.charCodeAt(i) <= 57)) )
        {       
            if ( sepDateQty < 2 )
            {
				sRetorno += '/';
				sepDateQty++;	
			}
			else if ( sepDateQty == 2 )
            {
				if ( (sepHourQty == 0) && (bFlagSpcDateHour == false) )
				{
					sRetorno += ' ';
					bFlagSpcDateHour = true;
				}	
				else if ( (sepHourQty < 2) && (bFlagSpcDateHour == true) )
				{
					sRetorno += ':';
					sepHourQty++;	
				}
			}        
        }    
        else    
			sRetorno += sString.charAt(i);
    }
    
    return sRetorno;
}

/********************************************************************
Formata um valor monetario passado em formato de string e ja com as duas
casas de centavos

Parametros:
	sStrValue - string a formatar
	milharSep - separador de milhar
	centSep - separador de centavo

Retorno
	a string formatada

********************************************************************/
function formatCurrencyValue(sStrValue, sMilharSep, sCentSep)
{
	var retString = '';
	var sIntegerPart = '';
	var sDecimalPart = '';
	var sTemp = '';
	var i;
	var bIntComplete = false;
	
	if ( sStrValue == '' )
		return retString;

	// Obtem da string passada a parte inteira e a parte decimal
	for ( i=0; i<sStrValue.length; i++ )
	{
		sTemp = sStrValue.substr(i, 1);
		
		if ( sTemp == '.' )
			bIntComplete = true;
		
		if ( (sTemp  != '.') && (!bIntComplete) )
			sIntegerPart += sTemp;
		if ( (sTemp  != '.') && (bIntComplete) )
			sDecimalPart += sTemp;
	}
	
	// Normaliza as partes
	if ( sIntegerPart == '' )
		sIntegerPart = '0';
	if ( sDecimalPart == '' )
		sDecimalPart = '0';	
	if ( sDecimalPart.length == 1 )
		sDecimalPart += '0';
	
	//Inverte, formata e desinverte a parte inteira
	sTemp = sIntegerPart;
	sIntegerPart = '';
	
	for ( i =(sTemp.length - 1); i>=0; i-- )
			sIntegerPart += sTemp.substr(i, 1);
	
	sTemp = '';
	
	for ( i=0; i<sIntegerPart.length; i++ )
	{
		sTemp += sIntegerPart.substr(i, 1);
		
		if ( ((i+1) % 3) == 0 )
		{
			if ( (i > 0) && (i < (sIntegerPart.length - 1)) )
				sTemp += sMilharSep;
		}
	}		
	
	sIntegerPart = '';
	
	for ( i =(sTemp.length - 1); i>=0; i-- )
			sIntegerPart += sTemp.substr(i, 1);			

	retString = sIntegerPart + sCentSep + sDecimalPart;
	
	return retString;
}

function removeDiatricsOnJava(strToRemove)
{
	if ( strToRemove == null)
		return null;
	if ( strToRemove == '')
		return strToRemove;	
		
	var sDiatrics = '��������������������������������';
    var sNormal   = 'aaaaeeeiioooouucAAAAEEEIIOOOOUUC';
	var i, j, k, l;
	var charInOrig = '';
	var charInRet = '';
	var strToReturn = '';
	
	k = strToRemove.length;
	l = sDiatrics.length;
	
    for (i=0; i<k; i++)
    {
		charInOrig = strToRemove.charAt(i);
		charInDest = strToRemove.charAt(i);
		
        for (j = 0; j < l; j++)
        {
			if (charInOrig == sDiatrics.charAt(j)  )
			{
				charInDest = sNormal.charAt(j);
				break;
			}
        }
        
        strToReturn += charInDest;
    }
    
    return strToReturn;

}

function tempoDecorrido(inicio, fim)
{
   var resultado = '00:00';
   var delta = ((fim - inicio) / 1000);
   
   if(delta >= 1)
   {
      var horas = 0;
      var minutos = 0;
      var segundos = 0;
      
      horas = ((delta / 3600) | 0);
      delta = (delta - (horas*3600));
      minutos  = ((delta / 60) | 0);
      delta = (delta - (minutos*60));
      segundos  = (delta | 0);
      
      resultado = ((horas > 0 ? ('' + horas.toString() + ':') : '') +
                   (minutos < 10 ? '0' : '') + minutos.toString() + ':' +
                   (segundos < 10 ? '0' : '') + segundos.toString());
   }

   return resultado;
}
