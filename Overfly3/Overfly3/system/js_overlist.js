// Objeto OverList===================================================

function _overInitializeList()
{
	var i;
	
	// variaveis de uso do programador
	this.nDirection = 0;
	
	for (i=0; i<this.aList.length; i++)
		this.aList[i] = null;

	this.nBookMark = -1;	
	
	this.sAnchorInFirstPage = null;
}

function _overCurrentInList()
{
	if ( this.aList.length == 0 )
		return null;
	
	if ( (this.nBookMark < 0) || (this.nBookMark > (this.aList.length - 1)) )
		return null;
	
	return this.aList[this.nBookMark];
}

function _overMoveNextInList()
{
	if ( this.aList.length == 0 )
		return false;
	
	if ( this.nBookMark >= (this.aList.length - 1) )	
		return false;
	
	var i;
	var nStart = this.nBookMark + 1;
	
	for (i=nStart; i<this.aList.length; i++)
	{
		if (this.aList[i] == null)
			continue;
		else
		{
			this.nBookMark = i;
			return true;
		}	
	}
	
	return false;
}

function _overMovePreviousInList()
{
	if ( this.aList.length == 0 )
		return false;
	
	if ( this.nBookMark <= 0 )	
		return false;

	var i;
	var nStart = this.nBookMark - 1;

	for (i=nStart; i<=0; i--)
	{
		if (this.aList[i] == null)
			continue;
		else
		{
			this.nBookMark = i;
			return true;
		}	
	}
	
	return false;
}


function _overMoveFirstInList()
{
	if ( this.aList.length == 0 )
		return false;
	
	if ( this.nBookMark >= (this.aList.length - 1) )	
		return false;
	
	var i;
	var nStart = 0;
	
	for (i=nStart; i<this.aList.length; i++)
	{
		if (this.aList[i] == null)
			continue;
		else
		{
			this.nBookMark = i;
			return true;
		}	
	}
	
	return false;
}

function _overMoveLastInList()
{
	if ( this.aList.length == 0 )
		return false;
	
	if ( this.nBookMark >= (this.aList.length - 1) )	
		return false;
	
	var i;
	var nStart = this.aList.length - 1;
	
	for (i=nStart; i<=0; i--)
	{
		if (this.aList[i] == null)
			continue;
		else
		{
			this.nBookMark = i;
			return true;
		}	
	}
	
	return false;
}

function _overAddAndMakeCurrentInList(elemToAdd, fnToEval)
{
	var i;
		
	for (i=0; i<this.aList.length; i++)
	{
		if (this.aList[i] == null)	
		{
			this.aList[i] = elemToAdd;
			this.nBookMark = i;
			
			if ( fnToEval != null )
				eval(fnToEval);
			
			return true;
		}
	}
	
	this.aList[this.aList.length] = elemToAdd;
	this.nBookMark = this.aList.length - 1;
	
	if ( fnToEval != null )
		eval(fnToEval);
	
	return true;	
}

function _overRemoveCurrentInList(fnToEval)
{
	if ( this.aList.length == 0 )
		return false;
		
	if ( (this.nBookMark < 0) || (this.nBookMark > (this.aList.length - 1))	)
		return false;
		
	if (this.aList[this.nBookMark] == null)		
		return false;

	var i;		

	this.aList[this.nBookMark] = null;
	
	for (i=this.nBookMark; i<(this.aList.length-1); i++)
		this.aList[this.nBookMark] = this.aList[this.nBookMark+1];

	this.aList[this.aList.length-1] = null;
	
	if (this.aList[this.nBookMark] == null)
	{
		for (i=this.nBookMark; i >= 0; i--)
		{
			if (this.aList[i] != null)
			{
				this.nBookMark = i;
				
				if ( fnToEval != null )
					eval(fnToEval);
				
				return true;
			}
		}	
	}

	if (this.aList[this.nBookMark] == null)
		this.nBookMark = -1;

	if ( fnToEval != null )
		eval(fnToEval);

	return true;
}

function _overQtyElemsInList()
{
	var i;
	var k = 0;		
	
	for (i=0; i<this.aList.length; i++)
	{
		if (this.aList[i] != null)	
			k++;
	}
	
	return k;	
}

function _getBookMarkInList()
{
	if ( nBookMark < 0 )
		return null;
	else
		return this.nBookMark;
}

function _setBookMarkInList(nBookMark)
{
	if ( nBookMark < 0 )
		return false;
	
	if ( nBookMark > aList.length - 1 )
		return false;
	
	this.nBookMark = nBookMark;
	
	return true;
}

function _OverList()
{
	// metodos para uso do programador
	this.initializeList = _overInitializeList;
	this.currentInList = _overCurrentInList;
	this.moveNextInList = _overMoveNextInList;
	this.movePreviousInList = _overMovePreviousInList;
	this.moveFirstInList = _overMoveFirstInList;
	this.moveLastInList = _overMoveLastInList;
	this.addAndMakeCurrentInList = _overAddAndMakeCurrentInList;
	this.removeCurrentInList = _overRemoveCurrentInList;
	this.qtyElemsInList = _overQtyElemsInList;
	this.getBookMarkInList = _getBookMarkInList;
	this.setBookMarkInList = _setBookMarkInList;

	// variaveis de uso do programador
	this.nDirection = 0;
	this.sAnchorInFirstPage = null;
		
	// variaveis internas do objeto
	this.aList = new Array();
	this.nBookMark = -1;
	
	// funcoes internas do objeto
}

// Final do Objeto OverList==========================================
