/********************************************************************
js_facilitiesnonforms.js

Library javascript para asp's nao forms
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var __ELEMSTATE = new Array();  //array: controlId, state
var __ELEMCOUNT = 0;

var __ARELOCKED = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao habilita/desabilita todos os controles do html/asp corrente.
A funcao nao executa se o parametro pede o status atual

Parametros:
cmdLock         - true (desabilita) / false (habilita)

Retornos:
Nenhum
********************************************************************/
function lockControls(cmdLock)
{
    if ( cmdLock == __ARELOCKED )
        return null;
        
    __ARELOCKED = cmdLock;
    
    __ELEMCOUNT = 0;
    var i;
    var elem;
    
    for (i=0; i<window.document.all.length; i++)
    {
        elem = window.document.all.item(i);
        
        // controles ActiveX
        // o grid
        if ( elem.tagName.toUpperCase() == 'OBJECT' )
        {
            if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072')
            {
                // grid
                adjustActiveXControl(elem, cmdLock);                
            }
        }
        
        // demais elementos        
        if ( elem.tagName.toUpperCase() == 'INPUT' )
        {
            adjustElement(elem, cmdLock);
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            if(elem.type == 'image')
            {
                if (cmdLock == true)
                    elem.src = glb_LUPA_IMAGES[1].src;
                else    
                    elem.src = glb_LUPA_IMAGES[0].src;
            }
            // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        }    
        else if ( elem.tagName.toUpperCase() == 'SELECT' )
            adjustElement(elem, cmdLock);
        else if ( elem.tagName.toUpperCase() == 'TEXTAREA' )
            adjustElement(elem, cmdLock);
        else if ( elem.tagName.toUpperCase() == 'A' )
            adjustElement(elem, cmdLock);
    }
}

/********************************************************************
Esta funcao trava ou destrava um elemento de html/asp da interface
da pagina corrente.

Parametros:
elem            - elemento corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustElement(elem, cmdLock)
{
    // cmdLock == true => desabilitar elemento
        
    if (cmdLock == true)
    {
        // desabilita o elemento
        if ( (elem.tagName.toUpperCase() == 'TEXTAREA') ||
             ((elem.tagName.toUpperCase() == 'INPUT') &&
               (elem.type.toUpperCase() == 'TEXT')) )
        {
			// salva status corrente
			__ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.readOnly);                  
            elem.readOnly = true;
        }    
        else
        {
			// salva status corrente
			__ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.disabled);           
            elem.disabled = true;
        }    

        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        if ( (elem.tagName.toUpperCase() == 'TEXTAREA') ||
             ((elem.tagName.toUpperCase() == 'INPUT') &&
               (elem.type.toUpperCase() == 'TEXT')) )
            window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).readOnly = (__ELEMSTATE[__ELEMCOUNT])[1];
        else
            window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).disabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Esta funcao trava ou destrava um elemento ACTIVEX CONTROL
de html/asp da pagina corrente.

Parametros:
elem            - elemento ACTIVEX CONTROL corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustActiveXControl(elem, cmdLock)
{
    // cmdLock == true => desabilitar elemento
    
    if (cmdLock == true)
    {
        // salva status corrente
        __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.Enabled);           
        // desabilita o elemento
        elem.Enabled = false;
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).Enabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Esta funcao retorna um array com os dados da empresa corrente.
Estes dados estao no statusbarmain.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais e de la foi copiada.

Parametros:
nenhum

Retorno:
array   - array[0] - empresaID
        - array[1] - empresaPaisID
        - array[2] - empresaCidadeID
        - array[3] - nome de fantasia da empresa
        - array[4] - empresaUFID
        - array[5] - TipoEmpresaID
        - array[6] - nome completo da empresa
        - array[7] - idioma do sistema.
        - array[8] - idioma da empresa.

null    - nao tem empresa selecionada        
********************************************************************/
function getCurrEmpresaData()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__EMPRESADATA', null) );
}