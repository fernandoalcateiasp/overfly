/********************************************************************
js_progressbar.js
Library javascript de funcoes basicas para progress bar
********************************************************************/

// CONSTANTES *******************************************************
	var glb_pb_TimerPB = null;
	var glb_nNumSteps = 40;
	var glb_TimerToReset = 250;
// FINAL DE CONSTANTES **********************************************

/********************************************************************
INDICE DAS FUNCOES:
DO PROGRAMADOR:
	pb_CreateProgressBarInModal()
	pb_SetLookProgressBar(backColor, gaugeColor)
	pb_SetDimAndPosProgressBar(nLeft, nTop, nWidth, nHeight)
	pb_ShowProgressBar(bShow)
	pb_StartProgressBar(nTimeToFill)
	pb_StopProgressBar(bReset)
	pb_KillTimer()
INTERNAS DA LIBRARY:	
	__pb_InitGauge()
	__pb_AdjustGauge()
	__pb_ResetGauge()
	__pb_AdjustGauge()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES DO PROGRAMADOR

/********************************************************************
Cria o progress bar. Atualmente ocupa a parte inferior da janela
onde esta criado.
Desta forma se o progress bar esta em janela modal, a mesma e
redimensionada para suportar-lo.
********************************************************************/
function pb_CreateProgressBarInModal()
{
	var oP;
	var oP1;
	var elem;
	
	// The frame (external label)
    oP = document.createElement("DIV");
    oP.innerText = '';
    oP.id = '_lbl_pbExt';
    oP.name = '_lbl_pbExt';
    oP.className = '_pb_lblGeneral';
    window.document.appendChild(oP);
    
    elem = _lbl_pbExt;
    with (elem.style)
    {
		position = 'absolute';
		borderWidth = 1;
		borderStyle = 'inset';
		fontFamily = 'Tahoma,Arial,Sans-Serif';
		fontSize = '0px';
		left = 0;
		top = 0;
		width = 50;
		height = 24;
		backgroundColor = 0XDCDCDC;
		visibility = 'hidden';
    }
    
    // The gauge (internal label)
    oP1 = document.createElement("DIV");
    oP1.innerText = '';
    oP1.id = '_lbl_pbInt';
    oP1.name = '_lbl_pbInt';
    oP1.className = '_pb_lblGeneral';
    window.document.appendChild(oP1);
    _lbl_pbExt.appendChild(oP1);
    
    elem = _lbl_pbInt;
    with (elem.style)
    {
		position = 'absolute';
		borderWidth = 0;
		borderStyle = 'none';
		fontFamily = 'Tahoma,Arial,Sans-Serif';
		fontSize = '0px';
		left = 1;
		top = 1;
		width = 0;//parseInt(_lbl_pbExt.currentStyle.width) / 2;
		height = parseInt(_lbl_pbExt.currentStyle.height) - 4;
		backgroundColor = 'navy';
		visibility = 'inherit';
    }
}

/********************************************************************
Configura as cores do progress bar e do gauge.
********************************************************************/
function pb_SetLookProgressBar(backColor, gaugeColor)
{
	var elem;
	
	if ( backColor != null )
	{
		elem = _lbl_pbExt;
		with (elem.style)
		{
			backgroundColor = backColor;
		}
    }
    
    if ( gaugeColor != null )
	{
		elem = _lbl_pbInt;
		with (elem.style)
		{
			backgroundColor = gaugeColor;
		}
    }
}

/********************************************************************
Reposiciona e redimensiona o progress bar.
	nLeft	- coordenada esquerda
	nTop	- coordenada top
	nWidth	- largura
	nHeight	- altura
********************************************************************/
function pb_SetDimAndPosProgressBar(nLeft, nTop, nWidth, nHeight)
{
	if ( (nLeft == null) || (nTop == null) ||
	     (nWidth == null)|| (nHeight == null) )
		return;
		
	if ( (nWidth < 0)|| (nHeight < 0) )		     
		return;

	var elem;
	
	elem = _lbl_pbExt;
    with (elem.style)
    {
		left = nLeft;
		top = nTop;
		width = nWidth;
		height =nHeight;
    }
    
    elem = _lbl_pbInt;
    with (elem.style)
    {
		//width = parseInt(_lbl_pbExt.currentStyle.width) / 2;
		height = parseInt(_lbl_pbExt.currentStyle.height) - 4;
    }
}

/********************************************************************
Mostra ou esconde o progress bar.
	bShow - true mostra, false esconde
********************************************************************/
function pb_ShowProgressBar(bShow)
{
	if ( bShow == null )
		return;
		
	var elem;
	
	elem = _lbl_pbExt;
    with (elem.style)
    {
		if ( bShow == true )
			visibility = 'inherit';		
		else	
			visibility = 'hidden';		
    }
}

/********************************************************************
Starta o progress bar para ciclo com um determinado intervalo de
tempo.
	nTimeToFill	- tempo do ciclo em segundos, no minimo 1 segundo
********************************************************************/
function pb_StartProgressBar(nTimeToFill)
{
    
	if ( (nTimeToFill == null) || (nTimeToFill < 1) )
		return;
	
	__pb_InitGauge();
	
	glb_nBar_XStep = _lbl_pbExt.offsetWidth / glb_nNumSteps;
		
	var nTimeToJump = nTimeToFill * 1000 / glb_nNumSteps;
		
	if ( glb_pb_TimerPB == null )
	{
		glb_pb_TimerPB = window.setInterval('__pb_AdjustGauge()', nTimeToJump, 'JavaScript');
	}

	try
	{
	    var strPars = '?ReportID=' + escape(selReports.value) +
	        '&EmpresaID=' + escape(getCurrEmpresaData()[0]) +
	        '&UserID=' + escape(getCurrUserID());
    	
	    window.top.dsoPrintLog.URL = SYS_ASPURLROOT + '/serversidegen/reportslog.asp' + strPars;
	    window.top.dsoPrintLog.ondatasetcomplete = dsoPrintLog_DSC;
	    window.top.dsoPrintLog.Refresh();
	}
	catch(e)
	{
	    dsoPrintLog_DSC();
	}
}

function dsoPrintLog_DSC()
{
    ;
}

/********************************************************************
Para o progress bar.
	bReset = true - reseta o gauge
********************************************************************/
function pb_StopProgressBar(bReset)
{
	if ( glb_pb_TimerPB != null )
	{
		window.clearInterval(glb_pb_TimerPB);
		glb_pb_TimerPB = null;
	}
	
	if ( bReset == true )
	{
		_lbl_pbInt.style.width = Math.abs(_lbl_pbExt.offsetWidth - 4);
		glb_pb_TimerPB = window.setInterval('__pb_ResetGauge()', glb_TimerToReset, 'JavaScript');
	}	
}

/********************************************************************
Descarrega o timer do progress bar, devolvendo o recurso ao SO.
********************************************************************/
function pb_KillTimer()
{
	if ( glb_pb_TimerPB != null )
	{
		window.clearInterval(glb_pb_TimerPB);
		glb_pb_TimerPB = null;
	}
}

// IMPLEMENTACAO DAS FUNCOES INTERNAS DA LIBRARY

/********************************************************************
Inicializa o progress bar.
********************************************************************/
function __pb_InitGauge()
{
	if ( glb_pb_TimerPB != null )
	{
		window.clearInterval(glb_pb_TimerPB);
		glb_pb_TimerPB = null;
	}
	
	_lbl_pbInt.style.width = 0;
}

/********************************************************************
Reseta o progress bar.
	bReset = true - reseta o gauge
********************************************************************/
function __pb_ResetGauge()
{
	if ( glb_pb_TimerPB != null )
	{
		window.clearInterval(glb_pb_TimerPB);
		glb_pb_TimerPB = null;
	}
	
	_lbl_pbInt.style.width = 0;
}

/********************************************************************
Ajusta o gauge do progress bar.
********************************************************************/
function __pb_AdjustGauge()
{
	var nCurrGaugeWidth = _lbl_pbInt.offsetWidth;
	var nNextGaugeWidth = nCurrGaugeWidth + glb_nBar_XStep;
	
	if ( nNextGaugeWidth > Math.abs(_lbl_pbExt.offsetWidth) )
		_lbl_pbInt.style.width = 0;
	else	
		_lbl_pbInt.style.width = Math.abs(nCurrGaugeWidth + glb_nBar_XStep);
}
