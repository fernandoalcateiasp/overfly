/********************************************************************
js_interface.js

Library javascript de funcoes basicas para interface
********************************************************************/

// As constantes abaixo sao de uso exclusivo desta library
// CONSTANTES *******************************************************
var glb_PARTSLOADED = 0;
var glb_INTERFACELOADED = 0;
var glb_INTERFACENAME = null;

var glb_aDivs_SUP = null;
// FINAL DE CONSTANTES **********************************************

/********************************************************************

INDICE DAS FUNCOES:
Carregamento de interface:
    initInterfaceLoad(interfaceName, interfaceLoadedInHex)
    getInterfaceName()
    componentOfInterfaceLoaded(keyInHex)
    componentOfInterfaceUnloaded(partInHex)
    gridToInternationalColor(color)
StatusBar:
    writeInStatusBar(statusBarToWrite, cellName, strToWrite, ellFlag)
    getDataInStatusBar(statusBarToGet, cellName)
Partes (htmls ou asp) de forms:
    putFormPartInPos(htmlPartID, thePos)
Montagem de interface:
    adjustElementsInForm(aElements,aTitle)
    adjustDivs(sForm,aDivs)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Ajusta a variavel glb_INTERFACELOADED.
A fun��o deve ser disparado antes de iniciar o carregamento da interface.

Parametros:
interfaceName   nome da interface que est� sendo carregada.

Retornos:
nenhum
********************************************************************/
function initInterfaceLoad(interfaceName, interfaceLoadedInHex)
{
    glb_INTERFACENAME = interfaceName;
    glb_PARTSLOADED = 0;
    glb_INTERFACELOADED = interfaceLoadedInHex;
}

/********************************************************************
Retorna o nome da interface que esta carregando.

Parametros:
nenhum

Retornos:
o nome da interface
********************************************************************/
function getInterfaceName()
{
    return (glb_INTERFACENAME.toUpperCase());
}

/********************************************************************
Controla se uma interface foi totalmente carregada.
A fun��o deve ser disparado por cada componente da interface,
quando termina efetivamente de carregar.

Parametros:
keyInHex               chave em hex do arquivo (aps/html) carregado

Retornos:
true se a interface foi toda carregada
false se a interface nao foi toda carregada
********************************************************************/
function componentOfInterfaceLoaded(keyInHex)
{
    glb_PARTSLOADED = glb_PARTSLOADED | eval(keyInHex);
    return ( glb_PARTSLOADED == glb_INTERFACELOADED );
}

/********************************************************************
Controla se um componente de uma interface foi totalmente descarregado.
A fun��o deve ser disparado por cada componente da interface,
quando termina efetivamente de descarregar.

Parametros:
keyInHex               chave em hex do arquivo (aps/html) descarregado

Retornos:
true se a interface foi toda descarregada
false se a interface nao foi toda descarregada
********************************************************************/
function componentOfInterfaceUnloaded(partInHex)
{
    glb_PARTSLOADED = glb_PARTSLOADED & eval(~keyInHex);
    return ( glb_PARTSLOADED == 0X0 );
}

/********************************************************************
Move e redimensiona um frame e e indicada para frames
contendo arquivos htmls/asp de form

Parametros:
frameID             id do frame a mover
framePos            string constantes: 'sup', 'inf', 'both'

Retornos:
true se sucesso, caso contrario false

Nota:
A fun��o atua sobre todos os frames do html mais externo do sistema,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre o primeiro encontrado.
********************************************************************/
function putFormPartInPos(htmlPartID, thePos)
{
    var frameID, frameCtrlBarSup, frameCtrlBarInf;
    var gap = 1;
    
    // Obtem as coordenadas do dois controlbars
    frameID = getFrameIdByHtmlId('controlbarsupHtml');
    frameCtrlBarSup = getRectFrameInHtmlTop(frameID);
    
    frameID = getFrameIdByHtmlId('controlbarinfHtml');
    frameCtrlBarInf = getRectFrameInHtmlTop(frameID);
    
    frameID = getFrameIdByHtmlId(htmlPartID);

    // Nota: 0 17 acrescido abaixo vem de ocultar o fast buttons
    if ( thePos.toUpperCase() == 'BOTH' )
        moveFrameInHtmlTop(frameID,
            new Array(0,
                      frameCtrlBarSup[1] + frameCtrlBarSup[3] + gap,
                      MAX_FRAMEWIDTH,
                      MAX_FRAMEHEIGHT - (frameCtrlBarSup[1] + frameCtrlBarSup[3]) - 2 * gap));
    
    if ( thePos.toUpperCase() == 'SUP' )
        moveFrameInHtmlTop(frameID,
            new Array(0,
                      frameCtrlBarSup[1] + frameCtrlBarSup[3] + gap,
                      MAX_FRAMEWIDTH, frameCtrlBarInf[1] - (frameCtrlBarSup[1] + frameCtrlBarSup[3]) - 2 * gap));
        
    if ( thePos.toUpperCase() == 'INF' )    
        moveFrameInHtmlTop(frameID,
            new Array(0,
                      frameCtrlBarInf[1] + frameCtrlBarInf[3] + gap,
                      MAX_FRAMEWIDTH, MAX_FRAMEHEIGHT - (frameCtrlBarInf[1] + frameCtrlBarInf[3]) - 2 * gap));
}

/********************************************************************
Posiciona os elementos criados do BODY do HTML na tela do form:

Parametros
1-aElements        -> Array  bidimensional que contem sub-arrays dentro dela:
                      Este array tem a seguinte estrutura:
                           Primeira dimensao: -> Um array que contem os sub-arrays dentro dele
                           Segunda dimensao: ->  Um array com 3 elementos na seguinte ordem:
                           0-> ID do Label relacionado ao controle
                           1-> ID do controle
                           2-> Quantidade de caracteres que o controle suporta
                           3-> Numero da linha que aparecera na tela (para o form superior de 1 a 4)
                           
2-aTitle           -> (opcional) Contem 2 Elementos:
                           0-> ID do label do titulo
                           1-> ID da tag HR que contem a linha horizontal
                           
3-sSubForm         -> (opcional) String indica se os elementos estao localizados no
                      sup ('SUP') ou no inf ('INF')
                      
4-mdMultiLine      -> (opcional) Boolean, indica se os elementos do parametro aElements
                      estao contidos no div principal (maindiv) e se ele ocupam mais de
                      uma linha do div (2 linhas ou mais)

Exemplo:
adjustElementsInForm([['txtEstadoID','lblEstado',10,1],['txtNome','lblNome',20,1],
                      ['selTipoRecursoID','lblTipo',10,2]])
********************************************************************/
function adjustElementsInForm(aElements,aTitle,sSubForm,mdMultiLine)
{
    if (sSubForm == null)
        sSubForm = 'SUP';
    else    
        sSubForm = sSubForm.toUpperCase();

    var nTop  = 10;
	var nLeft = 10;
	var aTops;
    var i=0;

    // testa se os elementos estao em um div principal e se ele e multi linhas
    if (mdMultiLine == true)
    {
        // Ze trocou a linha abaixo em 24/05/2001
        // aTops = new Array(7,54,102,149,191 + ELEM_GAP -2);
        // Ze trocou a linha abaixo em 11/12/2001
        //aTops = new Array(7,52,98,141,181 + ELEM_GAP -2);
        aTops = new Array(7,52,98,141,181 + ELEM_GAP -4, 229, 271, 311, 353, 395);
    }    
    else
    {
        if (sSubForm == 'SUP')
	        aTops = new Array(7,7,49,92,134 + ELEM_GAP -2, 184, 226, 268, 310, 352);
	    else
	        aTops = new Array(7,49,92,134,176 + ELEM_GAP -2, 226, 268, 310, 352, 394);
	}
	
	var nAntLine = 1;
	var lNewLine = false;
	var lWidth = 0;
	var cWidth = 0;
	var nTitleWidth;
	
	if (aTitle != null)
	{
        elem = window.document.getElementById(aTitle[0]);
        with (elem.style)
        {
            backgroundColor = 'transparent';
            fontSize = '10pt';
            fontWeight = 'bold';
            nTitleWidth = document.getElementById(aTitle[0]).outerText.length * 8;
            width = nTitleWidth; 
            top = 7;
            left = ELEM_GAP;
        }

        elem = window.document.getElementById(aTitle[1]);
        with (elem.style)
        {
            color = 'black';
            left = nTitleWidth + (ELEM_GAP * 2) + (aTitle.length == 3 ? aTitle[2] : 0);
            top = 15;
	    	topLeft = parseInt(top, 10) -5;
            height = 2;
            width = MAX_FRAMEWIDTH - parseInt(left, 10) - 2*ELEM_GAP;
        }
        // testa se os elementos estao em um div principal e se ele e multi linhas
        if (mdMultiLine == true)
            aTops = [26,68,111,153,195 + ELEM_GAP -2, 245, 287, 329, 371, 413];
        else
        {
            if (sSubForm == 'SUP')
                aTops = [26,26,68,111,153 + ELEM_GAP -2, 203, 245, 287, 329, 371];
            else
                aTops = [26,68,111,153,195 + ELEM_GAP -2, 245, 287, 329, 371, 413];
        }    
	}
	 
	for (i=0;i<aElements.length;i++)
	{
	 	if (nAntLine != aElements[i][3]) 
	 	    nLeft = 10;
		lWidth = 0;
		cWidth = 0; 
        /*Ajusta o label html*/
        if ((aElements[i][0]!='') && (aElements[i][0] != null))
        {
            // posicionamento vertical da linha manualmente
            if (aElements[i].length >= 6)
            {
                aTops = incrementATops(aElements[i][3],aElements[i][5],aTops);
            }
	 	    with (document.getElementById(aElements[i][0]).style)
		    {
       	    	 nLeft = (aElements[i].length >= 5 ? nLeft + aElements[i][4] : nLeft);
       	    	 left = nLeft;
       	    	 if (((aElements[i][0]).substr(0,3)).toUpperCase() == 'BTN')
       	     	 {
		    	    lWidth=aElements[i][2];
		    	    width = aElements[i][2];
		    	    height = 21;
		    	    top = aTops[(aElements[i][3])-1] + 16;
		    	    nLeft -= 9;
		    	    left  =  nLeft;
		    	 }
       	    	 else
       	    	 {
       	    		backgroundColor = 'transparent';
       	    		// Se for uma linha separadora o elemento html tem que
       	    		// ter um outro comportamento
       	    		if (document.getElementById(aElements[i][1]).tagName == 'HR')
       	    		{
					    backgroundColor = 'transparent';
					    fontSize = '10pt';
					    fontWeight = 'bold';
					    nTitleWidth = document.getElementById(aElements[i][0]).outerText.length * 8;
					    width = nTitleWidth; 
					    top = aTops[(aElements[i][3])-1];
					    left = ELEM_GAP;
       	    		}
		    		else
		    		{
       	    		   width = FONT_WIDTH * document.getElementById(aElements[i][0]).outerText.length;
		    		   lWidth = document.getElementById(aElements[i][0]).outerText.length * 6;
		    		   top = aTops[(aElements[i][3])-1];
		    		}
				}
		    }
		}
	    /*Ajusta o controle html*/
	    if ((aElements[i][1]!='') && (aElements[i][1] != null))
	    {
	        if (((aElements[i][0]).substr(0,3)).toUpperCase() != 'BTN')
	        {
	 	        with (document.getElementById(aElements[i][1]).style)
		        {
		             if (document.getElementById(aElements[i][1]).tagName == 'HR')
		             {
		                 with (document.getElementById(aElements[i][1]).style)
		                 {
                             color = 'black';
                             left = parseInt(document.getElementById(aElements[i][0]).style.width, 10) +
                                   (ELEM_GAP * 2);
                             top = aTops[(aElements[i][3])-1]+8;
	    	                 topLeft = parseInt(top, 10) -5;
                             height = 2;
                             width = MAX_FRAMEWIDTH - parseInt(left, 10) - 2*ELEM_GAP;
                         }
                         aTops = decrementATops(aElements[i][3],aTops);
		             }
		             else
		             {
       	         	    backgroundColor = 'transparent';
       	        	    width = FONT_WIDTH * aElements[i][2];
       	        	 
		        	    cWidth = FONT_WIDTH * aElements[i][2];
		        	    if (((aElements[i][1]).substr(0,3)).toUpperCase() == 'CHK')
		        	    {
       	        	       top = (aTops[(aElements[i][3])-1]+16) - 4;
       	        	       left = nLeft - 4 ;
       	        	    }   
       	        	    else
       	        	    {
       	        	       top = aTops[(aElements[i][3])-1]+16;
       	        	       left = nLeft;
       	        	    }   
       	            }	 
		        }
		    }
		}
        nLeft += Math.max(lWidth,cWidth) + ELEM_GAP;
		nAntLine = aElements[i][3];
	}
}

/********************************************************************
Auxiliar da funcao adjustElementsInForm
********************************************************************/
function decrementATops(nLine,aTops)
{
    var i;
    for ( i=nLine; i<aTops.length; i++ )
        aTops[i] = aTops[i] - 27;
    return aTops;
}

/********************************************************************
Auxiliar da funcao adjustElementsInForm
********************************************************************/
function incrementATops(nLine,nPixels,aTops)
{
    var i;
    nLine--;
    for ( i=nLine; i<aTops.length; i++ )
        aTops[i] = aTops[i] + nPixels;
    return aTops;
}

/********************************************************************
Ajusta os divs que contem os campos do form.

Parametros:
1 -> sForm  : 'pesqlist', 'sup' ou 'inf' (indica se o form que contem
               os divs e o pesqlist, o superior ou inferior.
2 -> aDivs  : um array que contem o numero do div (1 para primario, 2 para secundario),
              o ID do div a ser ajustado e um terceiro elemento opcional que e o numero
              de linhas que o div ira suportar.

exemplo de chamada:  adjustDivs('sup',[[1,'divSup01_01',2],
                                       [2,'divSup02_01'],
                                       [2,'divSup02_02'],
                                       [2,'divSup02_03']]);
********************************************************************/
function adjustDivs(sForm,aDivs)
{
    var i=0;
    var elem;
    var frameID;
    var frameObj;
    
    glb_aDivs_SUP = aDivs;
    
    for (i=0;i<aDivs.length;i++)
    {
        sForm = sForm.toUpperCase();
        if (sForm == 'SUP')
        {
            if (aDivs[i][0] == 1)
            {
                // divSup01_01
                elem = window.document.getElementById(aDivs[i][1]);
                  
                with (elem.style)
                {
                    visibility = 'visible';
                    backgroundColor = 'transparent';
                    width = MAX_FRAMEWIDTH;
                    // verifica se o parametro opcional de numero de linhas foi passado
                    if ((aDivs[i]).length == 3)
                        height = 45 * aDivs[i][2];
                    else
                        height = 45;
                        
                    top = 0;
                    left = 0;
                }
            }
            else if (aDivs[i][0] == 2)
            {
                frameID = getFrameIdByHtmlId('controlbarinfHtml');
                            
                if (frameID)
                    frameObj = getFrameInHtmlTop(frameID);

                elem = window.document.getElementById(aDivs[i][1]);
                with (elem.style)
                {
                    visibility = 'hidden';
                    backgroundColor = 'transparent';
                    width = MAX_FRAMEWIDTH;
                    top = parseInt(window.document.getElementById('divSup01_01').style.height, 10) +
						  parseInt(window.document.getElementById('divSup01_01').style.top, 10);
                    left = 0;
                    height = (parseInt(frameObj.style.top, 10) - parseInt(elem.style.top, 10)) - 1;
                }
            }
        }    
        else if (sForm == 'INF')
        {
            elem = window.document.getElementById(aDivs[i][1]);
            with (elem.style)
            {
                visibility = 'hidden';
                backgroundColor = 'transparent';
                width = MAX_FRAMEWIDTH - ELEM_GAP;
                height = (MAX_FRAMEHEIGHT / 2) - (6 * ELEM_GAP);
                top = 0;
                left = 0;
            }
        }
        else if (sForm == 'PESQLIST')
        {
            if (aDivs[i][0] == 1)
            {
                // divSup01_01 - a pesquisa
                elem = window.document.getElementById(aDivs[i][1]);
                with (elem.style)
                {
                    visibility = 'hidden';
                    backgroundColor = 'transparent';
                    width = MAX_FRAMEWIDTH;
                    height = 45;
                    top = 0;
                    left = 0;
                }
            }
            else if (aDivs[i][0] == 2)
            {
                // divSup01_01 - a listagem
                // Nota: 0 (2 * 12) acrescido abaixo vem de ocultar o fast buttons
                elem = window.document.getElementById(aDivs[i][1]);
                with (elem.style)
                {
                    visibility = 'hidden';
                    backgroundColor = 'transparent';
                    left = ELEM_GAP;
                    width = MAX_FRAMEWIDTH - (2 * ELEM_GAP);
                    top = ELEM_GAP;
                    height = MAX_FRAMEHEIGHT + (2 * 12) - (ELEM_GAP * 10) + 5;
                }
            }
        }    
    }
}

/********************************************************************
Ajusta os divs que contem os campos do form.
Funcao interna da automacao que so value para o sup
********************************************************************/
function adjustDivsEx_Sup()
{
    var i=0;
    var elem;
    var frameID;
    var frameObj;
    var j = 0;
    var maxDivHeight = 0;
    var elemChildren;
    
    aDivs = glb_aDivs_SUP;
    
    for (i=0;i<aDivs.length;i++)
    {
        if (aDivs[i][0] == 1)
        {
            // divSup01_01
            elem = window.document.getElementById(aDivs[i][1]);
                
			// ajusta a altura do div em funcao do elemento filho
			// mais baixo 
			for ( j=0; j< elem.children.length; j++ )
			{
				elemChildren = elem.children.item(j);
					
				if ( (elemChildren.offsetTop + elemChildren.offsetHeight) > maxDivHeight )
					maxDivHeight = elemChildren.offsetTop + elemChildren.offsetHeight;
			}
                  
            with (elem.style)
            {
                visibility = 'visible';
                backgroundColor = 'transparent';

                width = MAX_FRAMEWIDTH;
                height = maxDivHeight;    
                        
                top = 0;
                left = 0;
            }
        }
        else if (aDivs[i][0] == 2)
        {
            frameID = getFrameIdByHtmlId('controlbarinfHtml');
                
            if (frameID)
                frameObj = getFrameInHtmlTop(frameID);
                
            maxDivHeight = frameObj.offsetTop;    
            
            frameID = getFrameIdByHtmlId('controlbarsupHtml');
                
            if (frameID)
                frameObj = getFrameInHtmlTop(frameID);
                
            maxDivHeight -= ( frameObj.offsetTop + frameObj.offsetHeight );    

            elem = window.document.getElementById(aDivs[i][1]);
            with (elem.style)
            {
                visibility = 'hidden';
                backgroundColor = 'transparent';
                width = MAX_FRAMEWIDTH;
                top = parseInt(window.document.getElementById('divSup01_01').style.height, 10) +
					  parseInt(window.document.getElementById('divSup01_01').style.top, 10);
                left = 0;
                         
				height = maxDivHeight -
                         parseInt(window.document.getElementById('divSup01_01').style.height, 10) -
                         2;
            }
        }
	}
}

/********************************************************************
Converte uma codifica��o de cores do grid para o padr�o HTML. No 
grid o padr�o de cores � BGR, mas o html segue o padr�o internacional
RGB.

Parametros:
C�digo da cor do gri no formato 0xBBGGRR.

Retorno:
Cor convertida para o padr�o internacional 0xRRGGBB.
********************************************************************/
function gridToInternationalColor(color)
{
	var result = '0x';
	
	result += color.substring(6,8); // RR
	result += color.substring(4,6); // GG
	result += color.substring(2,4); // BB
	
	return result;
}

// FINAL DE IMPLEMENTACAO DAS FUNCOES

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****


// FINAL DE FUNCOES INTERNAS DA LIBRARY *****************************


