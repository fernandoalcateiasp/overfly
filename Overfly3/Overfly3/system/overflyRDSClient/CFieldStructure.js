/***********************************************************************************************
CFieldStructure.js

Armezena as caracteristicas de um campo
***********************************************************************************************/

/*************************************************************************************
CFieldStructure:construtor
    sValor: Valor do campo

    nType:
	bigint   (nType == 3)
	int      (nType == 11)
	numerico (nType == 14)
	bool     (nType == 20)
	char     (nType == 129)
	decimal  (nType == 131)
	datetime (nType == 135)
	varchar  (nType == 200)
	
	nSize: Tamanho do campo
	nPrecision: Precision do campo
	nScale: Scale do campo

    Attributes:
    4         - campo updatable
    8         - campo idenity 
    64        - campo aceita nulo
    32768     - campo primary key

(*) - parametro opcional
*************************************************************************************/
function CFieldStructure(sTableName, sNome, nType, nSize, nPrecision, nScale, nAttributes)
{
    var nType = (nType == null ? 200 : nType);
    var nSize = (nSize == null ? 0 : nSize);
    var nPrecision = (nPrecision == null ? 0 : nPrecision);

	this.table = sTableName;
    this.name = sNome;
    this.Name = sNome;
    this.type = nType;
    this.Type = nType;
    this.attributes = nAttributes;
    this.Attributes = nAttributes;

    // size, para tipo varchar ou numeric
    this.size = nSize;
    this.Size = nSize;

    // Precision, para tipo numeric
    this.precision = nPrecision;
    this.Precision = nPrecision;
    
    // scale, para tipo decimal
    this.numericScale = nScale;
    this.NumericScale = nScale;
}
