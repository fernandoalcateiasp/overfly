/********************************************************************
Trima espacos em branco do inicio e do fim de uma string
Parametros:
strToTrim               string a remover os espacos

Retornos:
a string sem os espacos
********************************************************************/
function trimStr(strToTrim)
{
    if ( strToTrim == null )
        return '';
        
    if ( strToTrim == '' )
        return '';
    
    var i, len;
    var charac, strTrimmed = new String();
    
    // trima lado esquerdo
    len = strToTrim.length;
        
    for (i=0; i<(len); i++)
    {
        charac = strToTrim.charAt(i);
        if ( charac != " " )
            break;
    }

    strTrimmed = strToTrim.substr(i);

    // trima lado direito
    len = strTrimmed.length;
        
    for (i=(len-1); i>=0; i--)
    {
        charac = strTrimmed.charAt(i);
        if ( charac != " " )
            break;
    }
    
    i++;
    
    strToTrim = strTrimmed.substr(0, i);
    
    return (strToTrim);
}

/********************************************************************
Retorna a posicao do elemento encontrado no array

Parametros:
aArray          um array a ser pesquisado
vToFind         valor a ser procurado dentro do array
lpartial        true - verifica se vToFind existe em um elemento do array
                false - verifica se vToFind e extamente igual a um elemento
                        do array

A pesquisa nao e case sensitive                        

Retornos:
integer   -1 Nao achou
        >= 0 Retorna o numero do elemento encontrado no array
********************************************************************/
function ascan(aArray,vToFind,lpartial)
{
    var i = 0;
    var retorno = -1;
    var sElemem;
    
    if (typeof(vToFind) == 'string')
        vToFind = vToFind.toUpperCase();
        
    for (i=0;i<aArray.length;i++)
    {
        sElemem = aArray[i];

        if (typeof(sElemem) == 'string')
            sElemem = sElemem.toUpperCase();
                
        if (lpartial)
        {
            if (sElemem.lastIndexOf(vToFind) >= 0)
            {
                retorno = i;
                break;
            }
        }
        else
        {
            if (sElemem == vToFind)
            {
                retorno = i;
                break;
            }
        }
    }
    return retorno;
}

/********************************************************************
Ordena um array de elementos string, em ordem crescente

Parametros:
arrayToSort      - Referencia do array a ser ordenado
nArrayDimension  - (opcional), dimensao a ser usada na ordenacao caso
                   o array seja multidimensional

Retorno:      null
********************************************************************/
function asort(arrayToSort, nArrayDimension)
{
    var i, j, k, vTemp;
    var nFim = arrayToSort.length;
    
	if (nArrayDimension != null)
	{
	    if (arrayToSort[0].lenght > nArrayDimension)
	        return null;
	}
	
	nArrayDimension = (nArrayDimension == null ? 0 : nArrayDimension);
    
    for (j=nFim; j>=1; j--)
    {
        for (i=0; i<=j-2; i++)
        {
            if (arrayToSort[i][nArrayDimension] > arrayToSort[i+1][nArrayDimension])
            {
                // Array unidimensional
                if (nArrayDimension == null)
                {
                    vTemp = arrayToSort[i];
                    arrayToSort[i] = arrayToSort[i+1];
                    arrayToSort[i+1] = vTemp;
                }
                // Array multidimensional
                else
                {
                    for (k=0; k<arrayToSort[i].length; k++)
                    {
                        vTemp = arrayToSort[i][k];
                        arrayToSort[i][k] = arrayToSort[i+1][k];
                        arrayToSort[i+1][k] = vTemp;
                    }
                }
            }
        }
    }
    
    return null;
}

/********************************************************************
Faz uma pesquisa binaria em um array.

Parametros:
aArray     Array a usado como fonte de pesquisa
strToSeek  String a ser pesquisada
nArrayDimension  - (opcional), dimensao a ser usada na pesquisa caso
                   o array seja multidimensional

Retorno:   Indice do elemento encontrado no array
           -1 caso o valor nao foi encontrado
********************************************************************/
function aseek(aArray, strToSeek, nArrayDimension)
{
	var nInicio = 0;
	var nFinal = aArray.length - 1;
	var nMetade = 0;
	var vMetadeValue;
	var retVal = -1;
	
	if (nArrayDimension != null)
	{
	    if (aArray[0].lenght > nArrayDimension)
	        return retVal;
	}
	
	if (trimStr(strToSeek) == '')
	    return retVal;

	while ( nInicio <= nFinal)
	{
		nMetade = Math.ceil((nInicio + nFinal)/2);

        // Ternario em funcao do Array ser unidimensional ou multidimensional
		vMetadeValue = (nArrayDimension == null ? aArray[nMetade] : aArray[nMetade][nArrayDimension]);

		if (strToSeek < vMetadeValue)
			nFinal = nMetade - 1;
		else
			if (strToSeek > vMetadeValue)
				nInicio = nMetade + 1;
			else
				nInicio = nFinal + 1;
	}

	if (strToSeek == (nArrayDimension == null ? aArray[nMetade] : aArray[nMetade][nArrayDimension]))
		retVal = nMetade;

    return retVal;
}

/********************************************************************
Retorna o numero de elementos de um array desconsiderando os 
    elementos que estao null

Parametros:
aArray     Array

Retorno:   Numero de elementos validos (diferente de null) do array
********************************************************************/
function aLength(aArray)
{
    var retVal = -1;
    var i = 0;
    
    if (aArray != null)
    {
        if (aArray.length > 0)
        {
            for (i=0; i<aArray.length; i++)
            {
                if (aArray[i] == null)
                {
                    retVal = i;
                    break;
                }
            }

            if (retVal == -1)
                retVal = aArray.length;
        }
    }

    if (retVal == -1)
        retVal = 0;

    return retVal;
}
