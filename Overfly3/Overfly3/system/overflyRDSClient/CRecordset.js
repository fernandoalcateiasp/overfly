/***********************************************************************************************
CRecordset.js

Recordset que armazena os dados vindos do objeto datatransfer
***********************************************************************************************/

/****** Constantes ******/

// Indica que o regisro foi lido do banco e n�o sofreu nenhuma alteracao
var REGISTRO_SINCRONIZADO = 0;
// Indica que o regisro foi lido do banco e sofreu nenhuma alteracao
var REGISTRO_ALTERADO = 1;
// Indica que um novo registro foi incluido no array de registros e precisa ser salvo
var REGISTRO_NOVO = 2;
// Indica que o registro foi excluido.
var REGISTRO_EXCLUIDO = 3;

// Caso se deseja mover o ponteiro para o inicio
var PARA_INICIO = -1;
// Caso se deseja mover o ponteiro para o fim
var PARA_FIM = +1;
// Indicativo de posicao invalida.
var POSICAO_INVALIDA = -1;
/*************************************************************************************
CRecordset:construtor

(*) - parametro opcional
*************************************************************************************/
function CRecordset() {
    // Variaveis internas de controle
    this.aFieldsStructure = new Array();
    // Mantem em memoria os dados originais do banco para no momento
    // da atualizacao possa-se verificar se o registro foi alterado ou nao.
    this.dataBaseRecords = new Array();

    // Array que contem os registros do recordset
    this.aRecords = new Array();
    // Array que contem os registros filtrados recordset
    // Isto soh acontece quando a propriedade "filter" foi aplicada pelo programador
    this.aFilteredRecords = new Array();

    // Posi��o do ponteiro no buffer de registro    
    this.recordPointer = -1;
    this.lastRecordPointer = -1;
    this.BOF = true;
    this.EOF = true;

    // Corresponsde a pripriedade filter do RDS da microsoft. Lembrar na puclicacao.
    this.filter = '';
    this.setFilter = _setFilter;
    this.getFilter = _getFilter;

    // Flag que indica que o buffer foi filtrado.
    this.filtered = false;

    // Colecao de CFields com informacoes sobre os campos dos registros.
    this.Fields = new CFields();
    this.fields = this.Fields;

    // Metodos protected
    this.loadFieldStruct = _loadFieldStruct;
    this.updateFieldsHash = _updateFieldsHash;
    this.clear = _clearRecordset;
    this.loadCurrentRecord = _loadCurrentRecord;

    // Metodos publicos
    this.addNew = _AddNew;
    this.AddNew = _AddNew;
    this.Delete = _Delete;

    this.seek = _seek;
    this.moveFirst = _moveFirst;
    this.MoveFirst = _moveFirst;
    this.movePrevious = _movePrevious;
    this.MovePrevious = _movePrevious;
    this.moveNext = _moveNext;
    this.MoveNext = _moveNext;
    this.moveLast = _moveLast;
    this.MoveLast = _moveLast;

    this.posicionar = _posicionar;
    this.updateRecord = _updateRecord;

    /*
	 * Retorna a quantidade de registros no recordset.
     */
    this.RecordCount =
    function () {
        return this.aRecords.length;
    };

    /*
	 * Busca um registro cujo campo tenha o valor passado.
	 */
    this.Find =
	function (field, value) {
	    // Se o campo n�o existir, move para o fim e sai.
	    if (this[field] == null) {
	        this.moveLast();
	        this.moveNext();

	        return;
	    }

	    // Move para o primeiro registro para teste.
	    this.moveFirst();

	    // Procura o resgistro 
	    for (; !this.EOF && this[field].value != value;) {
	        this.moveNext();
	    }
	};

    this.find = this.Find;

    /**
	 * Retorna a posi��o do ponteiro de dados no buffer de registros.
	 */
    this.Bookmark =
	function () {
	    var result;

	    result = this.recordPointer;

	    return result;
	};

    /**
	 * Posiciona no bookmark indicado.
	 */
    this.gotoBookmark =
	function (bookmark) {
	    this.moveFirst();

	    this.seek(bookmark);
	};
}

/*************************************************************************************
_clearRecordset:funcao interna

    descricao: limpa o objeto

    parametros: 
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _clearRecordset() {
    // Libera o aFieldsStucture
    for (i = 0; i < this.aFieldsStructure.length; i++) {
        delete this.aFieldsStructure[i];
    }
    delete this.aFieldsStructure;
    this.aFieldsStructure = new Array();

    // Array que contem os registros do recordset
    for (i = 0; i < this.aRecords.length; i++) {
        for (j = 0; j < this.aRecords[i].length; j++) {
            delete this.aRecords[i][j];
        }

        delete this.aRecords[i];
    }
    delete this.aRecords;
    this.aRecords = new Array();

    // Recria o array para os dados originais do banco de dados.
    for (i = 0; i < this.dataBaseRecords.length; i++) {
        for (j = 0; j < this.dataBaseRecords[i].length; j++) {
            delete this.dataBaseRecords[i][j];
        }

        delete this.dataBaseRecords[i];
    }
    delete this.dataBaseRecords;
    this.dataBaseRecords = new Array();

    // Array que contem os registros filtrados recordset
    // Isto soh acontece quando a propriedade "filter" foi aplicada pelo programador
    for (i = 0; i < this.aFilteredRecords.length; i++) {
        for (j = 0; j < this.aFilteredRecords[i].length; j++) {
            delete this.aFilteredRecords[i][j];
        }

        delete this.aFilteredRecords[i];
    }
    delete this.aFilteredRecords;
    this.aFilteredRecords = new Array();

    this.recordPointer = -1;
    this.lastRecordPointer = -1;

    //Propriedades
    this.BOF = true;
    this.EOF = true;

    // Filtros
    this.filtered = false;
    this.filter = '';
}

/*************************************************************************************
_loadCurrentRecord:funcao interna

    descricao: carregar o objeto Fields do registro corrente do recordset

    parametros: bApplyFilter, aplica o filtro no recordset se o usuario tiver setado
                a propriedade filter.
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _loadCurrentRecord() {
    var i = 0;
    var j = 0;

    // Se o ponteiro nao esta no buffer, invalida os Fields. 
    if (this.BOF || this.EOF) {
        this.Fields.eraseValues();

        return;
    }

    // Obtrm o array de registro para verificar se alguns de seus registros
    // foi alterados.
    var aCurrentARecord = (this.filtered ? this.aFilteredRecords : this.aRecords);
    var record = aCurrentARecord[this.lastRecordPointer];

    // Atatualiza os dados do registro.
    this.updateRecord(record);

    // Se esta posicionado dentro do buffer, atualiaza os fields.
    if (!(this.BOF || this.EOF)) {
        // Pega o registro apontado no buffer
        var record = this.filtered ? this.aFilteredRecords[this.recordPointer] : this.aRecords[this.recordPointer];

        // Atualiza os valor de cada field com o valor do registro
        // recuperado do buffer.
        for (i = 0; i < this.fields.count; i++) {
            this.fields.item[i].value = record[i];
            this.fields.item[i].originalValue = record[i];
        }

        // Indica que o field esta valido.
        this.fields.isValid = true;
    }
}

/*************************************************************************************
_applyCurrentFilter:funcao interna

    descricao: Inicia o processo de filtragem dos registros no recordset, 
               obedecendo as regras atraves da expressao da propriedade "Filter"

    parametros: sCurrentFilter: expressao que representa o filtro (proprietade Filter)
                pRecordset:     ponteiro para o objeto CRecordset
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _applyCurrentFilter(sCurrentFilter, pRecordset) {
    var aTokens = new Array();

    try {
        aTokens = _filterLexical(sCurrentFilter);
        aTokens = _filterSyntactical(aTokens);
        _filterSemantic(aTokens, pRecordset);
        _filterRecords(aTokens, pRecordset);
        pRecordset.filtered = true;

        delete aTokens;
    }
    catch (e) {
        if (e.message != null) {
            alert(e.message);
        }
        else {
            alert(e);
        }
    }
}

/*************************************************************************************
_applyCurrentFilter:funcao interna

    descricao: Aplica o filtra dos registros no recordset, aplicando a regra de filtro
               utilizando o array de Tokens para isso 

    parametros: aTokens:    array que contem os tokens para aplicar o filtro
                pRecordset: ponteiro para o objeto CRecordset
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _filterRecords(aTokens, pRecordset) {
    var i = 0;
    var j = 0;
    var k = 0;
    var nCounter = 0;
    var sCurrToken = '';
    var strToEval = '';
    var strOperatorLogical = '&&';
    var bResult = true;
    var strToCompareA = '';
    var strToCompareB = '';
    var regExp = /'/g;

    for (k = 0; k < pRecordset.aRecords.length; k++) {
        for (i = 0; i < aTokens.length; i++) {
            sCurrToken = aTokens[i];

            if ((sCurrToken == '&&') || (sCurrToken == '||')) {
                strOperatorLogical = sCurrToken;
                continue;
            }

            // Multiplo de 3
            if (nCounter % 3 == 0) {
                // Procura o campo nos fields
                for (j = 0; j < pRecordset.aFieldsStructure.length; j++) {
                    if (sCurrToken == pRecordset.aFieldsStructure[j].name) {
                        strToCompareA = (pRecordset.aRecords[k][j] == null ? '' : pRecordset.aRecords[k][j].toString());
                        strToCompareB = aTokens[i + 2].toString();

                        strToCompareA = strToCompareA.replace(regExp, '');
                        strToCompareB = strToCompareB.replace(regExp, '');

                        if (strToCompareA == '' || isNaN(strToCompareA) || strToCompareB == '' || isNaN(strToCompareB)) {
                            strToCompareA = '\'' + strToCompareA + '\'';
                            strToCompareA = strToCompareA.toUpperCase();
                            strToCompareB = '\'' + strToCompareB + '\'';
                            strToCompareB = strToCompareB.toUpperCase();
                        }

                        strToEval = strToCompareA + aTokens[i + 1].toString() + strToCompareB + strOperatorLogical + bResult.toString();
                        bResult = eval(strToEval);
                        break;
                    }
                }
            }

            nCounter++;
        }

        if (bResult) {
            pRecordset.aFilteredRecords[pRecordset.aFilteredRecords.length] =
				pRecordset.aRecords[k];
        }

        strOperatorLogical = '&&';
        bResult = true;
    }
}

/*************************************************************************************
_filterSyntactical:funcao interna

    descricao: Analisador sintatico, faz a analise sintatica da propriedade "Filter"

    parametros: aTokens:    array que contem os tokens para aplicar o analisador sintatico
    
    retorno: void

(*) - parametro opcional

// Regras da maquina de estado para o parser da propriedade 'filter'
// Estados                     Regra
//   1-Inicial                 Nenhuma
//   2-FieldName               Deve comecar com caracter alpha, seguidor por alphanumerico
//   3-Operador de comparacao  <,>,<>,=
//   4-ComparedValue           Deve comecar com caracter alphanumerico, seguidor por alphanumerico
//   5-Operador Logico         AND,OR
//   6-Estado Final            Nenhuma

// Transitividade
// 1->2
// 2->3
// 3->4
// 4->5
// 4->6
// 5->2
*************************************************************************************/
function _filterSyntactical(aTokens) {
    var estadoID = 1;
    var currToken = '';
    var bError = false;

    for (i = 0; i < aTokens.length; i++) {
        currToken = aTokens[i];

        if (estadoID == 1)
            estadoID = 2;
        else if (estadoID == 2)
            estadoID = 3;
        else if (estadoID == 3)
            estadoID = 4;
        else if (estadoID == 4)
            estadoID = 5;
        else if (estadoID == 5)
            estadoID = 2;

        for (j = 0; j < currToken.length; j++) {
            sCurrChar = currToken.substr(j, 1);
            currCharCategory = charCategory(sCurrChar);

            // 2-FieldName, Deve comecar com caracter alpha, seguidor por alphanumerico
            if (estadoID == 2) {
                if ((j == 0) && (currCharCategory != 1)) {
                    bError = true;
                    break;
                }
                else if ((j > 0) && (currCharCategory > 2)) {
                    bError = true;
                    break;
                }
            }
                // 3-Operador de comparacao <,>,<>,=
            else if (estadoID == 3) {
                if ((currToken != '<') &&
                    (currToken != '>') &&
                    (currToken != '<=') &&
                    (currToken != '>=') &&
                    (currToken != '<>') &&
                    (currToken != '=')) {
                    bError = true;
                    break;
                }
                else {
                    currToken = (currToken == '=' ? '==' : currToken);
                    currToken = (currToken == '<>' ? '!=' : currToken);
                    aTokens[i] = currToken;
                    break;
                }
            }
                // 4-ComparedValue
            else if (estadoID == 4) {
                    ;
            }
                // 5-Operador Logico AND,OR
            else if (estadoID == 5) {
                if ((currToken != 'AND') &&
                    (currToken != 'OR')) {
                    bError = true;
                    break;
                }
                else {
                    currToken = (currToken.toUpperCase() == 'AND' ? '&&' : currToken);
                    currToken = (currToken.toUpperCase() == 'OR' ? '||' : currToken);
                    aTokens[i] = currToken;
                    break;
                }
            }
        }

        if (bError) {
            break;
        }
    }

    if (estadoID == 4)
        estadoID = 6;

    if (estadoID != 6)
        bError = true;

    if (bError)
        throw "OverflyRDS Erro: (Filter) - Erro sintatico";

    return aTokens;
}

/*************************************************************************************
_filterSemantic:funcao interna

    descricao: Analisador semantico, faz a analise semantica da propriedade "Filter"

    parametros: aTokens:  array que contem os tokens para aplicar o analisador semantico
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _filterSemantic(aTokens, pRecordset) {
    var sFieldName = '';
    var sCurrToken = '';
    var nCounter = 0;
    var sFieldError = '';

    for (i = 0; i < aTokens.length; i++) {
        sCurrToken = aTokens[i];

        if ((sCurrToken.toUpperCase() == '&&') || (sCurrToken.toUpperCase() == '||'))
            continue;

        // Multiplo de 3
        if (nCounter % 3 == 0) {
            sFieldError = aTokens[i];

            // Procura o campo nos fields
            for (j = 0; j < pRecordset.aFieldsStructure.length; j++) {
                if (aTokens[i] == pRecordset.aFieldsStructure[j].name) {
                    sFieldError = '';
                    break;
                }
            }

            if (sFieldError != '')
                break;
        }

        nCounter++;
    }

    if (sFieldError != '')
        throw "OverflyRDS Erro: (Filter) - Erro semantico - field (" + sFieldError + ") not found";
}

/*************************************************************************************
_filterLexical:funcao interna

    descricao: Analisador lexico, faz a analise lexica da propriedade "Filter"

    parametros: sCurrentFilter: Filtro aplicado pelo programador na propriedade "Filter"
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _filterLexical(sCurrentFilter) {
    var aTokens = new Array();
    var sToken = '';
    var sCurrChar = '';
    var nKindToken = 0; // 0->Indifinido, (1|2)->AlphaNumericos, 3->Operador de comparacao
    var retVal = '';
    var aspaState = false; //false->fechado, true->aberto e esperando fechamento

    for (i = 0; i < sCurrentFilter.length; i++) {
        sCurrChar = sCurrentFilter.substr(i, 1);
        currCharCategory = charCategory(sCurrChar);

        // Aspa simples
        if (currCharCategory == 5) {
            aspaState = !aspaState;

            if ((!aspaState) && (trimStr(sToken) != ''))
                aTokens[aTokens.length] = sToken;

            sToken = '';
            continue;
        }

        if (aspaState) {
            sToken += sCurrChar;
            continue;
        }

        // Caracter invalido
        if (currCharCategory == 0) {
            throw "OverflyRDS Erro: (Filter) - Erro lexico, caracter invalido";
            break;
        }
            // Token corrente vazio
        else if (sToken == '') {
            if (sCurrChar != ' ') {
                sToken += sCurrChar;
                nKindToken = currCharCategory;
            }
            continue;
        }
            // Espaco
        else if (currCharCategory == 4) {
            if (trimStr(sToken) != '')
                aTokens[aTokens.length] = sToken;

            sToken = '';
            continue;
        }
            // Alphanumericos, seguido de um caracter de operador de comparacao
        else if (((nKindToken == 1) || (nKindToken == 2)) && (currCharCategory == 3)) {
            if (trimStr(sToken) != '')
                aTokens[aTokens.length] = sToken;

            sToken = sCurrChar;
            nKindToken = currCharCategory;
            continue;
        }
            // Caracter de operador de comparacao, seguido de um alphanumerico
        else if ((nKindToken == 3) && (currCharCategory <= 2)) {
            if (trimStr(sToken) != '')
                aTokens[aTokens.length] = sToken;

            sToken = sCurrChar;
            nKindToken = currCharCategory;
            continue;
        }
        else
            sToken += sCurrChar;
    }

    if (aspaState)
        throw "OverflyRDS Erro: (Filter) - Erro lexico, fechamento de string esperado";

    if (sToken != '')
        aTokens[aTokens.length] = sToken;

    return aTokens;
}

/*************************************************************************************
charCategory:funcao interna

    descricao: Retorna a categoria de um char

    parametros: sChar: caracter a ser analisado
    
    retorno: categoria

(*) - parametro opcional
*************************************************************************************/
function charCategory(sChar) {
    var nASCII = sChar.charCodeAt(0);
    var retVal = 0;

    // Caracter Alpha
    if (((nASCII >= 65) && (nASCII <= 90)) ||
         ((nASCII >= 97) && (nASCII <= 122)))
        retVal = 1;
        // Caracter Numerico
    else if ((nASCII >= 48) && (nASCII <= 57))
        retVal = 2;
        // Igual, Menor, Maior, 
    else if ((nASCII == 61) || (nASCII == 60) || (nASCII == 62))
        retVal = 3;
        // Espaco
    else if (nASCII == 32)
        retVal = 4;
        // Apaspas simples
    else if (nASCII == 39)
        retVal = 5;

    return retVal;
}

// Esta funcao indica uma posicao valida para o ponteiro do registro a partir
// da posicao indicada. Movendo o ponteiro no sentido indicado caso necess�rio.
// O sentido sera indicado por 1, caso se deseja mover para o inicio ou
// -1, caso se deseja mover para o fim.
function _posicionar(posicao, sentido) {
    var max = (this.filtered ? this.aFilteredRecords.length : this.aRecords.length);
    var regs = (this.filtered ? this.aFilteredRecords : this.aRecords);
    var index;
    var record;

    // Corrige a posicao do ponteiro movendo na direcao indicada.
    for (index = posicao; index >= 0 && index < max; index += sentido) {
        record = regs[index];
        if (record[record.length - 1] != REGISTRO_EXCLUIDO) {
            return index;
        }
    }

    // Se nao foi possivel encontrar um registro valido, retorna a posicao
    // fora do buffer apontada por index que podera ser -1 ou max, conforme
    // a condicao do for acima (index >= 0 && index < max).
    return index;
}

/**
 * _seek
 *		Esta funcao varia a posicao do ponteiro do buffer conforme a variacao
 *	indicada.
 */
function _seek(delta) {
    var nCurrentARecordTop = (this.filtered ? this.aFilteredRecords.length : this.aRecords.length);
    var record;

    var novaPosicaoIni;
    var novaPosicaoFim;
    // Nao havendo registros no buffer, nao ha o que fazer.
    if (nCurrentARecordTop <= 0) {
        this.recordPointer = -1;
        this.lastRecordPointer = -1;

        this.BOF = true;
        this.EOF = true;

        return;
    }
    else {
        // Para uma variacao nula ou uma variacao alem do inici ou uma variacao alem do fim,
        // apenas ajusta os marcadores de posicao.
        if ((delta == 0) ||
		   (this.recordPointer == -1 && delta < 0) ||
		   (this.recordPointer == nCurrentARecordTop && delta > 0)) {
            // Antes de calcular a posicao atual para a movimentacao nula,
            // atauliza a posicao anterior a atual pois ela pode ter sido
            // alterada.
            this.loadCurrentRecord();

            // Localiza a primeira posicao valida no sentido da posicao atual
            // para o inicio do buffer.
            novaPosicaoIni = this.posicionar(this.recordPointer, PARA_INICIO);
            // Localiza a primeira posicao valida no sentido da posicao atual
            // para o fim do buffer.
            novaPosicaoFim = this.posicionar(this.recordPointer, PARA_FIM);

            this.BOF = novaPosicaoIni == -1;
            this.EOF = novaPosicaoFim == nCurrentARecordTop;

            if (this.BOF && this.EOF) {
                this.recordPointer = -1;
                this.lastRecordPointer = -1;
            }
            else {
                // Marca a posicao atual como a primeira posicao valida 
                // encontrada.
                this.recordPointer = !this.BOF ? novaPosicaoIni : novaPosicaoFim;

                // Marca a posicao anterior com a posi��o atual, pois o
                //registro atual pode ter sido alterado.
                this.lastRecordPointer = this.recordPointer;

                this.BOF = this.recordPointer == -1;
                this.EOF = this.recordPointer == nCurrentARecordTop;
            }
        }
        else {
            // Ajusta a posicao do ponteiro se ele estiver fora do buffer.
            if (this.recordPointer <= -1) {
                this.recordPointer = 0;
                delta--;
            }
            else if (this.recordPointer >= nCurrentARecordTop) {
                this.recordPointer = nCurrentARecordTop - 1;
                delta++;
            }

            // Atualiza o buffer se ele estiver posicionado em um registro novo.		
            record = this.filtered ? this.aFilteredRecords[this.recordPointer] :
				this.aRecords[this.recordPointer];

            // Atualiza os dados do registro corrente se ele for um novo registro.
            // tamb�m atualiza para registros alterados.
            if (record[record.length - 1] == REGISTRO_NOVO || record[record.length - 1] == REGISTRO_ALTERADO) {
                this.updateRecord(record);
            }

            // Para uma variacao negativa maior que a distancia da posicao atual para
            // o inicio, ajusta-se o valor do delta para a posicionar no inicio do buffer.
            if (delta < 0 && delta + this.recordPointer < -1) {
                delta = -1 * this.recordPointer - 1;
            }
                // Para uma variacao positiva maior que a distancia da posicao atual para
                // o fim, ajusta-se o valor do delta para a posicionar no fim do buffer.
            else if (delta > 0 && this.recordPointer + delta > nCurrentARecordTop) {
                delta = nCurrentARecordTop - this.recordPointer;
            }

            // Tenta posicionar o ponteiro anterior.
            this.lastRecordPointer = this.posicionar(this.recordPointer, PARA_INICIO);
            if (this.lastRecordPointer < 0 || this.lastRecordPointer >= nCurrentARecordTop) {
                this.lastRecordPointer = this.posicionar(this.recordPointer, PARA_FIM);
            }

            // Se nao foi encontrado uma posicao dentro do buffer para o ponteiro
            // anterior, e ja que ele foi movido para ambas as direcoes no buffer e
            // o ponteiro atual esta dentro do buffer, significa que nao ha registros
            // validos no buffer ou o buffer esta vazio.
            if ((this.lastRecordPointer < 0 || this.lastRecordPointer >= nCurrentARecordTop) &&
				(this.recordPointer >= 0 && this.recordPointer < nCurrentARecordTop)) {
                this.recordPointer = this.lastRecordPointer;
                this.BOF = this.recordPointer == -1;
                this.EOF = this.recordPointer == nCurrentARecordTop;
            }
                // Ja que o ponteiro da posicao anterior esta posicionado corretamente,
                // tenta-se posicionar o ponteiro do registro corrente.
            else {
                // Tenta encontrar uma posicao para o ponteiro do registro corrente
                // dentro da variacao indicada.
                posicao = this.posicionar(
					this.recordPointer + delta,
					delta >= 0 ? PARA_FIM : PARA_INICIO);

                // Se a posicao encontrada estiver fora do buffer,
                // significa que foi encontrado o inicio ou o fim do buffer.
                if (posicao < 0 || posicao >= nCurrentARecordTop) {
                    this.recordPointer = (delta > 0) ? nCurrentARecordTop : -1;
                    this.BOF = this.recordPointer == -1;
                    this.EOF = this.recordPointer == nCurrentARecordTop;
                }
                    // Se a posicao for valida, ajusta os marcadores de posicao.
                else {
                    // Ajusta os atributos de controle
                    this.BOF = false;
                    this.EOF = false;

                    // Coloca o ponteiro de registro, na posicao encontrada.
                    this.recordPointer = posicao;
                }
            }
        }
    }

    // Atualiza os dados do registro corrente.
    this.loadCurrentRecord();
}

/*************************************************************************************
_moveFirst:funcao interna

    descricao: move o ponteiro dos registros para o 1o registro

    parametros: 
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _moveFirst() {
    this.lastRecordPointer = this.recordPointer;
    this.recordPointer = 0;

    this.seek(0);
}

/*************************************************************************************
_movePrevious:funcao interna

    descricao: move o ponteiro dos registros para o registro anterior

    parametros: 
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _movePrevious() {
    this.seek(-1);
}

/*************************************************************************************
_moveNext:funcao interna

    descricao: move o ponteiro dos registros para o proximo registro

    parametros: 
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _moveNext() {
    this.seek(1);
}

/*************************************************************************************
_moveLast:funcao interna

    descricao: move o ponteiro dos registros para o ultimo registro

    parametros: 
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _moveLast() {
    var max = (this.filtered ? this.aFiteredRecords.length : this.aRecords.length);
    this.recordPointer = max - 1;
    this.seek(0);
}

/**
 * Filtra o buffer de registro colocando no buffer de registros filtrados os
 * registros que atendem as condi��es.
 */
function _setFilter(filtro) {
    // Para nao se perder uma alteracao no registro atual, atualiza o buffer...
    var buffer = (this.filtered ? this.aFilteredRecords : this.aRecords);
    var record = (this.recordPointer > -1 && this.recordPointer < buffer.length) ? buffer[this.recordPointer] : null;

    // ... se o ponteiro estiver em uma posicao valida.
    if (record != null) {
        this.updateRecord(record);
    }

    // Elimina registros do ultimo filtro, se houver.
    for (i = 0; this.aFilteredRecords != null && this.aFilteredRecords.length != null && i < this.aFilteredRecords.length; i++) {
        this.aFilteredRecords[i] = null;
    }
    delete this.aFilteredRecords;
    this.aFilteredRecords = new Array();

    // Filtra o buffer.
    this.filter = trimStr(filtro);

    if (this.filter != '') {
        _applyCurrentFilter(this.filter, this);
    }
    else if (this.filter == '') {
        this.filtered = false;
    }

    // Quando nao volta registros do buffer, e' necessario que os fields sejam nulos.
    this.fields.eraseValues();

    // Ajusta a posicao dos ponteiros se o filtro estiver vazio 
    // para que nao ocorra erro quando for movimentar para a primeira posicao.
    if (this.filtered && this.aFilteredRecords.length == 0) {
        this.recordPointer = -1;
        this.lastRecordPointer = -1;
        this.BOF = true;
        this.EOF = true;
    }
        // Mas se o ponteiro de registro estiver em uma posi��o al�m do �ltimo
        // registro filtrado, posiciona-o em EOF.
    else if (this.filtered && this.aFilteredRecords.length > 0 && this.recordPointer >= this.aFilteredRecords.length) {
        this.lastRecordPointer = this.aFilteredRecords.length;
        this.recordPointer = this.aFilteredRecords.length;
        this.BOF = false;
        this.EOF = true;
    }

    // Uma vez filtrado (ou nao) reposiciona o ponteiro do buffer no inicio.
    // Isto e' regra.
    this.moveFirst();
}

function _getFilter(filtro) {
    return this.filter;
}

/*************************************************************************************
_loadFieldStruct:funcao interna

    descricao: carrega um array que descreve a estrutura dos campos do recordset

    parametros: aRecord - Array com oa estrutura de todos os campos do recordset
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _loadFieldStruct(aFieldStructure) {
    var i;

    // Copia a estrutura para o objeto.
    this.aFieldsStructure = aFieldStructure;

    /**** Cria a estrutura de fields do objeto ****/
    // Limpa o conjunto de fields da memoria. Se houver.
    if (this.fields.item.length > 0) {
        // Se o fields n�o est� vazio, o hash do recordset pode n�o estar
        // e ser� preciso limpa-lo.
        for (i = 0; i < this.fields.count; i++) {
            if (this[this.fields.item[i].name] != null) {
                delete this[this.fields.item[i].name];
            }
        }

        this.fields.clear();
    }
    // Para cada estrutura de campo, cria um field.
    for (fs = 0; fs < this.aFieldsStructure.length; fs++) {
        // Cria o field para a estrutura.
        this.fields.add(
			new CField(
				this.aFieldsStructure[fs].table,
				this.aFieldsStructure[fs].name,
				null,
				this.aFieldsStructure[fs].type,
				this.aFieldsStructure[fs].size,
				this.aFieldsStructure[fs].precision,
				this.aFieldsStructure[fs].numericScale,
				this.aFieldsStructure[fs].attributes));

        // Atualiza o hashtable.
        this.updateFieldsHash(this.fields.item[this.fields.count - 1]);
    }
}

function _updateFieldsHash(oField) {
    this[oField.name] = oField;
}

/*************************************************************************************
_AddNew:funcao interna

    descricao: adiciona um novo registro

    parametros: 
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _AddNew() {
    // Desliga o filtro.
    this.setFilter('');

    // Insere um novo elemento no buffer.
    this.aRecords[this.aRecords.length] = new Array();

    // Inicia cada posicao do novo regitro com null.
    for (r = 0; r < this.fields.Count; r++) {
        this.aRecords[this.aRecords.length - 1][r] = null;
    }

    // Marca o endereco do novo registro.
    this.aRecords[this.aRecords.length - 1][r++] = this.aRecords.length - 1;
    // Indica que nenhum registro sofreu alteracao.
    this.aRecords[this.aRecords.length - 1][r++] = null;
    // Indica que e um novo registro.
    this.aRecords[this.aRecords.length - 1][r++] = REGISTRO_NOVO;

    // Move os ponteiros de controle para o novo registro
    this.recordPointer = this.aRecords.length - 1;
    this.lastRecordPointer = this.recordPointer;

    this.BOF = false;
    this.EOF = false;

    this.fields.eraseValues();
    this.fields.isValid = true;
}

/**
 * Atualiza o buffer de registros copiando os dados dos fields para o novo
 * registro que esta no buffer. Ou atauliza a marca de alteracao do registro
 * se ele tiver sido alterado.
 *
 * Nenhuma acao se faz se o registro estiver marcado para exclusao.
 */
function _updateRecord(record) {
    // Marcar que indicam se um campo foi alterado ou nao.
    var marcadorDeAlteracao = new Array();
    var registroAlterado = false;

    // Varre cada campo do registro
    for (i = 0; i < this.Fields.count; i++) {
        // Atualiza o valor do campo para um novo resgistro, no caso dos
        // fields serem v�lidos.
        if (record[record.length - 1] == REGISTRO_NOVO && this.Fields.isValid) {
            record[i] = this.Fields[i].value;
        }
            // Seta o flag como registro alterado se houve alteracao no registro
            // e o registro nao estiver marcado como novo ou excluido.
        else if (record[record.length - 1] != REGISTRO_NOVO && record[record.length - 1] != REGISTRO_EXCLUIDO && this.Fields[i].value != this.Fields[i].originalValue) {
            // Indica que o registro foi alterado.
            registroAlterado = true;
            // Indica que o campo naquela posicao foi alterado.
            record[record.length - 2][i] = 1;
            // Atualiza o valor do registro alterado.
            record[i] = this.Fields[i].value;
        }
            // Copia os dados do registro atual para o field que esta nulo.
        else if (this.Fields[i].value == null && this.Fields.isValid) {
            this.Fields[i].value = record[i];
        }
    }

    // Coloca as marcas de alteracao do registro e de cada campo alterado.
    if (registroAlterado) {
        record[record.length - 1] = REGISTRO_ALTERADO;
    }
}

/*************************************************************************************
_remove:funcao interna

    descricao: Marca o registro atual para ser excluido.

    parametros: 
    
    retorno: void
*************************************************************************************/
function _Delete() {
    // Se estiver no inicio ou fim do buffer, retorna sem nenhuma acao.
    if (this.EOF || this.BOF) {
        return;
    }

    var position = this.recordPointer;
    var buffer = (this.filtered ? this.aFilteredRecords : this.aRecords);
    var status = buffer[position][buffer[position].length - 1];

    // Se for um registro novo, retira o registro do buffer para que nao haja
    // uma tentativa de exclusao no banco de dados de um registro que nao 
    // existe.
    if (status == REGISTRO_NOVO) {
        // Novo buffer de dados.
        var newBuffer = new Array();

        // Armazena o endereco
        var positionInRecords = buffer[position][buffer[position].length - 3];

        // Ja' que o usuario pode ter incluido varios registros, 
        // trabalhado no buffer e resolvido excluir um dos registros
        // incluidos, copia-se todo o buffer para um novo e 
        for (i = 0; i < this.aRecords.length; i++) {
            // Se for o registro novo que esta sendo excluido, 
            // nao o coloca no novo buffer.
            if (i == positionInRecords) {
                for (j = 0; j < this.aRecords[i].length; j++) {
                    delete this.aRecords[i][j];
                    this.aRecords[i][j] = null;
                }

                continue;
            }

            // Pega o registro atual.
            var record = this.aRecords[i];
            // Marca o endereco no novo buffer.
            record[record.length - 3] = newBuffer.length;
            // Armazena o registro no novo buffer.
            newBuffer[newBuffer.length] = record;

            // Retira a refer�ncia do registro do buffer antigo.
            this.aRecords[i] = null;
        }

        // Guarda o novo buffer no recordset.
        delete this.aRecords;
        this.aRecords = newBuffer;

        // Se o recordset estiver filtrado, Executa novamente o filtro.
        if (this.filtered) {
            this.setFilter(this.filter);
        }

        // Invalida o conteudo dos fields para que nao haja atualizacoes indevidas.
        this.fields.eraseValues();
    }
        // Se o registro nao e' novo pode marcar para exclusao quando da 
        // submissao ao banco de dados.
    else {
        buffer[position][buffer[position].length - 1] = REGISTRO_EXCLUIDO;
    }

    // Apos acertar o buffer precisa mover o ponteiro para uma
    // posicao antes do registro excluido, conforme regra do 
    // componente.
    this.movePrevious();

    // Porem se ele estiver em BOF, pode ser que esteja em uma posicao invalida.
    // Ou seja, estar em BOF havendo registros nao excluidos na lista.
    if (this.BOF) {
        this.moveNext();
    }

}
