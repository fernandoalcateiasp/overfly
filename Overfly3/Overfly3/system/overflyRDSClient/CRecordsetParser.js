/******************************************************************************
CRecordsetParser

	Classe responsavel por processar o XML de retorno do banco de dados e crias
as colecoes de CFieldStruct e CField.
 
******************************************************************************/
function CRecordsetParser()
{
/***************************  Metodos Publicos  ******************************/
	/**************************************************************************
	 * getStructs: funcao publica
	 *
	 * descricao: Retorna a colecao de CFieldStructure.
	 *
	 * retorno: A estrutura retornada do banco de dados.
	 *************************************************************************/
	this.getStructs =
    function() {
    	return this.structs;
    };
    
	/**************************************************************************
	 * getData: funcao publica
	 *
	 * descricao: Retorna a colecao de dados.
	 *
	 * retorno: Buffer com os registro do banco de dados.
	 *************************************************************************/
    this.getData =
    function() {
    	return this.data;
    };
    
	/**************************************************************************
	 * freeRecordsetParser: funcao publica
	 *
	 * descricao: Libera os recursos utilizados para fazer o parser.
	 *
	 * retorno: Sem retorno.
	 *************************************************************************/
    this.freeRecordsetParser =
	function() {
		// Libera as estruturas.
		this.structs = null;

		// Libera de dados.
		this.data = null;
	};

	/**************************************************************************
	 * startParser: funcao publica
	 *
	 * descricao: Metodo para ler o XML.
	 *
	 * parametro: HttpRequest com o retorno do banco de dados.
	 *
	 * retorno: Sem retorno.
	 *************************************************************************/
	this.startParser =
	function(xmlHttp) {
		// Variavel auxiliar para o conteudo das tags.
		var chs = '';

		// Obtem o documento DOM.
		documentDOM = xmlHttp.responseXML;

		/***** Analise da estrutura retornada do banco de dados *****/
		// Obtem a lista de tags TABLE_STRUCTURE
		tableStructureNodeList = documentDOM.getElementsByTagName("TABLE_STRUCTURE");

		// Obtem os valores das tags internas de cada tag TABLE_STRUCTURE da lista
		for (tsInd = 0; tsInd < tableStructureNodeList.length; tsInd++) {
			var baseTableName = tableStructureNodeList[tsInd].getElementsByTagName("BaseTableName");
			for (btnInd = 0; btnInd < baseTableName.length; btnInd++) {
				this.sTableName = (baseTableName[btnInd].text != null ?
					baseTableName[btnInd].text :
					baseTableName[btnInd].textContent);
			}

			var columnName = tableStructureNodeList[tsInd].getElementsByTagName("ColumnName");
			for (cnInd = 0; cnInd < columnName.length; cnInd++) {
				this.sNome = (columnName[cnInd].text != null ?
					columnName[cnInd].text :
					columnName[cnInd].textContent);
			}

			var columnSize = tableStructureNodeList[tsInd].getElementsByTagName("ColumnSize");
			for (csInd = 0; csInd < columnSize.length; csInd++) {
				this.nSize = (columnSize[csInd].text != null ?
					parseInt(columnSize[csInd].text) :
					parseInt(columnSize[csInd].textContent));
			}

			var numericPrecision = tableStructureNodeList[tsInd].getElementsByTagName("NumericPrecision");
			for (npInd = 0; npInd < numericPrecision.length; npInd++) {
				this.nPrecision = (numericPrecision[npInd].text != null ?
					parseInt(numericPrecision[npInd].text) :
					parseInt(numericPrecision[npInd].textContent));
			}

			var numericScale = tableStructureNodeList[tsInd].getElementsByTagName("NumericScale");
			for (nsInd = 0; nsInd < numericScale.length; nsInd++) {
				this.nScale = (numericScale[nsInd].text != null ?
					parseInt(numericScale[nsInd].text) :
					parseInt(numericScale[nsInd].textContent));
			}

			var isKey = tableStructureNodeList[tsInd].getElementsByTagName("IsKey");
			for (ikInd = 0; ikInd < isKey.length; ikInd++) {
				chs = (isKey[ikInd].text != null ?
					isKey[ikInd].text :
					isKey[ikInd].textContent);

				if (chs != null && chs.toLowerCase() == 'true') {
					this.nAttributes += attributesValues[isKey[ikInd].nodeName];
				}
			}

			var allowDBNull = tableStructureNodeList[tsInd].getElementsByTagName("AllowDBNull");
			for (adbnInd = 0; adbnInd < allowDBNull.length; adbnInd++) {
				chs = (allowDBNull[adbnInd].text != null ?
					allowDBNull[adbnInd].text :
					allowDBNull[adbnInd].textContent);

				if (chs != null && chs.toLowerCase() == 'true') {
					this.nAttributes += attributesValues[allowDBNull[adbnInd].nodeName];
				}
			}

			var isReadOnly = tableStructureNodeList[tsInd].getElementsByTagName("IsReadOnly");
			for (iroInd = 0; iroInd < isReadOnly.length; iroInd++) {
				chs = (isReadOnly[iroInd].text != null ?
					isReadOnly[iroInd].text :
					isReadOnly[iroInd].textContent);

				if (chs != null && chs.toLowerCase() == 'true') {
					this.nAttributes += attributesValues[isReadOnly[iroInd].nodeName];
				}
			}

			var isIdentity = tableStructureNodeList[tsInd].getElementsByTagName("IsIdentity");
			for (iiInd = 0; iiInd < isIdentity.length; iiInd++) {
				chs = (isIdentity[iiInd].text != null ?
					isIdentity[iiInd].text :
					isIdentity[iiInd].textContent);

				if (chs != null && chs.toLowerCase() == 'true') {
					this.nAttributes += attributesValues[isIdentity[iiInd].nodeName];
				}
			}

			var isAutoIncrement = tableStructureNodeList[tsInd].getElementsByTagName("IsAutoIncrement");
			for (iaiInd = 0; iaiInd < isAutoIncrement.length; iaiInd++) {
				chs = (isAutoIncrement[iaiInd].text != null ?
					isAutoIncrement[iaiInd].text :
					isAutoIncrement[iaiInd].textContent);

				if (chs != null && chs.toLowerCase() == 'true') {
					this.nAttributes += attributesValues[isAutoIncrement[iaiInd].nodeName];
				}
			}

			var dataTypeName = tableStructureNodeList[tsInd].getElementsByTagName("DataTypeName");
			for (dtnInd = 0; dtnInd < dataTypeName.length; dtnInd++) {
				this.nType = (dataTypeName[dtnInd].text != null ?
					dataTypesCode[dataTypeName[dtnInd].text] :
					dataTypesCode[dataTypeName[dtnInd].textContent]);
			}

			// Apos obter o valor de cada tag ...
			// ... cria o novo objeto de strutura do campo, ...
			this.structs[this.sNome] =
				new CFieldStructure(
					this.sTableName,
					this.sNome,
					this.nType,
					this.nSize,
					this.nPrecision,
					this.nScale,
					this.nAttributes);

			this.structs[this.structs.length] =
				this.structs[this.sNome];

			// ... e "Zera" os atributos para a criacao do proximo objeto de estrutura.
			this.sTableName = '';
			this.sNome = '';
			this.nType = '';
			this.nSize = '';
			this.nPrecision = '';
			this.nScale = '';
			this.nAttributes = 0;
		}

		/***** Extracao dos dados retornados do banco de dados *****/
		// Obtem a lista de tags TABLE_DATA
		tableDataNodeList = documentDOM.getElementsByTagName("TABLE_DATA");

		// Obtem os valores dos armazenados como tags internas de cada tag
		// TABLE_DATA da lista
		for (tdInd = 0; tdInd < tableDataNodeList.length; tdInd++) {
			// Apaga o indice de campos com o mesmo nome se ele existir,
			// pois para cada TABLE_DATA a contagem deve recome�ar.
			if (this.unformatedFieldsName != null) {
				for (i = 0; i < this.fieldsNamesIndex.length; i++) {
					delete this.unformatedFieldsName[this.fieldsNamesIndex[i]];
					delete this.fieldsNamesIndex[i];

					this.fieldsNamesIndex[i] = null;
				}

				delete this.unformatedFieldsName;
				delete this.fieldsNamesIndex[i];
			}
			this.unformatedFieldsName = new Array();

			// Cria o array para um novo registro com a quantidade de campos
			// definidos na estrutura.
			this.record = new Array(this.structs.length);

			// Para cada campo definido na estrutura retornada do banco de dados...
			for (recordIndex = 0; recordIndex < this.structs.length; recordIndex++) {
				// ... tenta recuperar um no' do xml com o mesmo nome.
				var novoNome = this.formatNodeName(this.structs[recordIndex].name);
				fieldNode = tableDataNodeList[tdInd].getElementsByTagName(novoNome);

				// Caso o no' exista...
				if ((fieldNode != null) && (fieldNode.length > 0)) {
					// ... coloca o valor do campo no campo correspondente no buffer.
					for (nodeIndex = 0; nodeIndex < fieldNode.length; nodeIndex++) {
						var recordType = this.structs[recordIndex].type;


						if (recordType == dataTypesCode["bit"]) {
							var bitValue = (fieldNode[nodeIndex].text != null ? fieldNode[nodeIndex].text : fieldNode[nodeIndex].textContent);

							this.record[recordIndex] = (bitValue.toUpperCase() == 'TRUE' || bitValue == '1' ? 1 : 0);
						}
						else if (recordType == dataTypesCode["bigint"] ||
							recordType == dataTypesCode["int"] ||
							recordType == dataTypesCode["smallint"] ||
							recordType == dataTypesCode["tinyint"] ||
							recordType == dataTypesCode["uniqueidentifier"]) {
							this.record[recordIndex] = (fieldNode[nodeIndex].text != null ?
								parseInt(fieldNode[nodeIndex].text, 10) :
								parseInt(fieldNode[nodeIndex].textContent, 10));
						}
						else if (recordType == dataTypesCode["decimal"] ||
							recordType == dataTypesCode["float"] ||
							recordType == dataTypesCode["money"] ||
							recordType == dataTypesCode["numeric"] ||
							recordType == dataTypesCode["real"] ||
							recordType == dataTypesCode["smallmoney"]) {
							this.record[recordIndex] = (fieldNode[nodeIndex].text != null ?
								parseFloat(fieldNode[nodeIndex].text) :
								parseFloat(fieldNode[nodeIndex].textContent));
						}
						else if (recordType == dataTypesCode["datetime"] ||
							recordType == dataTypesCode["smalldatetime"] ||
							recordType == dataTypesCode["timestamp"]) {
							var dateValue = (fieldNode[nodeIndex].text != null ? fieldNode[nodeIndex].text : fieldNode[nodeIndex].textContent);

							// Grava a data como uma string, no formato configurado.
							if (DATE_FORMAT == "DD/MM/YYYY") {
								this.record[recordIndex] =
									dateValue.substr(8, 2) + // Dia
									"/" +
									dateValue.substr(5, 2) + // Mes
									"/" +
									dateValue.substr(0, 4) + // Ano
									" " +
									dateValue.substr(11, 2) + // Hora
									":" +
									dateValue.substr(14, 2) + // Minuto
									":" +
									dateValue.substr(17, 2);   // Segundo
							}
							else {
								this.record[recordIndex] =
									dateValue.substr(5, 2) + // Mes
									"/" +
									dateValue.substr(8, 2) + // Dia
									"/" +
									dateValue.substr(0, 4) + // Ano
									" " +
									dateValue.substr(11, 2) + // Hora
									":" +
									dateValue.substr(14, 2) + // Minuto
									":" +
									dateValue.substr(17, 2);   // Segundo
							}

							var milliFromString = "";
							// Acrescenta os milissegundos do datetime
							if (dateValue.substr(19, 1) != "-") {
								milliFromString += ".";

								milliFromString += parseInt(dateValue.substr(20, 1), 10) ? parseInt(dateValue.substr(20, 1), 10) : 0;
								milliFromString += parseInt(dateValue.substr(21, 1), 10) ? parseInt(dateValue.substr(21, 1), 10) : 0;
								milliFromString += parseInt(dateValue.substr(22, 1), 10) ? parseInt(dateValue.substr(22, 1), 10) : 0;
							}

							this.record[recordIndex] += milliFromString.length > 0 ? milliFromString : ".0";

							// Grava a data como objeto Date() para fins de calculo
							// quando necessario.
							/*
							this.record[recordIndex].dateValue = new Date(
							parseInt(dateValue.substr( 0, 4), 10), // Ano
							parseInt(dateValue.substr( 8, 2), 10), // Mes
							parseInt(dateValue.substr( 5, 2), 10), // Dia
							parseInt(dateValue.substr(11, 2), 10), // Hora
							parseInt(dateValue.substr(14, 2), 10), // Minuto
							parseInt(dateValue.substr(17, 2), 10)  // Segundo
							);
							*/
						}
						else {
							this.record[recordIndex] = (fieldNode[nodeIndex].text != null ?
								fieldNode[nodeIndex].text :
								fieldNode[nodeIndex].textContent);
						}
					}
				}
				// Caso contrario, ...
				else {
					// ... coloca null no campo correspondente no buffer.
					this.record[recordIndex] = null;
				}
			}

			// Termina de configurar o registro fazendo...
			// Seta o indice do registro no array de registros.
			this.record[this.record.length] = this.data.length;
			// Reserva uma posicao para o marcador de mudancas.
			var marcasAlteracao = new Array();
			for (i = 0; i < this.record.length; i++) {
				marcasAlteracao[marcasAlteracao.length] = 0;
			}

			this.record[this.record.length] = marcasAlteracao;

			// Seta o flag que indica mudanca no registro.
			// 0 - Sincronizado com o banco de dados.
			// 1 - Registro alterado.
			// 2 - Registro novo.
			// 3 - Registro excluido.
			this.record[this.record.length] = 0;
			// Insere o novo registro no buffer.
			this.data[this.data.length] = this.record;
		}
	};

/****************************  Metodos Privados  *****************************/
	/**************************************************************************
	 * formatNodeName: funcao interna
	 *
	 * descricao: Formata os nomes dos campos. Ja� que o nome de um compo no
	 *			  TABLE_STRUCTURE transforma-se em um nome de tag, torna-se
	 *			  necessario codificar os espacos e colocar um indice no caso
	 *			  de nome repetido.
	 *
	 * parametros: fieldName, e' o nome do campo a ser formatado para um nome 
	 *			   valido para uma tag xml.
	 *             Exemplo: Nome do primeiro campo-> "Faturamento Anual",
	 *                      tag->"Faturamento_x0020_Anual"
	 *
	 *                      Nome do segundo campo-> "Faturamento Anual",
	 *                      tag->"Faturamento_x0020_Anual1"
	 *
	 * retorno: Nome do campo formatado
	 *************************************************************************/
	this.formatNodeName =
	function(fieldName) {
		if (this.unformatedFieldsName[fieldName] == null) {
			this.unformatedFieldsName[fieldName] = -1;
		}

		this.unformatedFieldsName[fieldName]++;

		// Resultado das substitui��es.
		var result;

		// Matriz com os caracteres de substitui��o.
		var translate = [new Array(), new Array()];

		// Cria a tabel com caracteres que devem ser substituidos 
		// em qualquer parte do nome do campo.
		translate[0][translate[0].length] = [" ", "_x0020_"];
		translate[0][translate[0].length] = ["!", "_x0021_"];
		translate[0][translate[0].length] = ["\"", "_x0022_"];
		translate[0][translate[0].length] = ["#", "_x0023_"];
		translate[0][translate[0].length] = ["$", "_x0024_"];
		translate[0][translate[0].length] = ["%", "_x0025_"];
		translate[0][translate[0].length] = ["&", "_x0026_"];
		translate[0][translate[0].length] = ["'", "_x0027_"];
		translate[0][translate[0].length] = ["(", "_x0028_"];
		translate[0][translate[0].length] = [")", "_x0029_"];
		translate[0][translate[0].length] = ["*", "_x002A_"];
		translate[0][translate[0].length] = ["+", "_x002B_"];
		translate[0][translate[0].length] = [",", "_x002C_"];
		translate[0][translate[0].length] = ["/", "_x002F_"];
		translate[0][translate[0].length] = [":", "_x003A_"];
		translate[0][translate[0].length] = [";", "_x003B_"];
		translate[0][translate[0].length] = ["<", "_x003C_"];
		translate[0][translate[0].length] = ["=", "_x003D_"];
		translate[0][translate[0].length] = [">", "_x003E_"];
		translate[0][translate[0].length] = ["?", "_x003F_"];
		translate[0][translate[0].length] = ["@", "_x0040_"];
		translate[0][translate[0].length] = ["[", "_x005B_"];
		translate[0][translate[0].length] = ["\\", "_x005C_"];
		translate[0][translate[0].length] = ["^", "_x005E_"];
		translate[0][translate[0].length] = ["`", "_x0060_"];
		translate[0][translate[0].length] = ["{", "_x007B_"];
		translate[0][translate[0].length] = ["|", "_x007C_"];
		translate[0][translate[0].length] = ["}", "_x007D_"];
		translate[0][translate[0].length] = ["~", "_x007E_"];

		// Cria a tabela com caracteres que devem ser substituidos 
		// no caso de serem o primeiro caracter do nome do campo.
		translate[1][translate[1].length] = ["0", "_x0030_"];
		translate[1][translate[1].length] = ["1", "_x0031_"];
		translate[1][translate[1].length] = ["2", "_x0032_"];
		translate[1][translate[1].length] = ["3", "_x0033_"];
		translate[1][translate[1].length] = ["4", "_x0034_"];
		translate[1][translate[1].length] = ["5", "_x0035_"];
		translate[1][translate[1].length] = ["6", "_x0036_"];
		translate[1][translate[1].length] = ["7", "_x0037_"];
		translate[1][translate[1].length] = ["8", "_x0038_"];
		translate[1][translate[1].length] = ["9", "_x0039_"];

		var i;

		// Se o nome do campo for n�mero, coloca _ no final.
		//if(!IsNaN(fieldName)) {
		//	fieldName = "_" + fieldName;
		//}

		result = fieldName;

		// Substitui o primeiro caracter do nome.
		for (i = 0; i < translate[1].length; i++) {
			if (result.charAt(0) >= '0' && result.charAt(0) <= '9' && translate[1][i][0] == result.charAt(0)) {
				result = result.replace(translate[1][i][0], translate[1][i][1]);

				break;
			}
		}

		// Substitui os caracteres em todo o nome.
		for (i = 0; i < translate[0].length; i++) {
			while (result.indexOf(translate[0][i][0]) != -1) {
				result = result.replace(translate[0][i][0], translate[0][i][1]);
			}
		}

		result += ((this.unformatedFieldsName[fieldName] > 0) ? this.unformatedFieldsName[fieldName] : "");

		return result;
	};
		
	/**************************************************************************
	 * parseNodeName:funcao interna
	 *
	 * descricao: normatiza o nome do campo, pois este vem formatado de forma
	 *            diferente do que vem nas tags que informam a estrurura dos
	 *            campos.
	 *
	 * parametros: nodeName, e' o nome do noh que corresponde ao nome do campo.
	 *             Exemplo: Nome do campo-> "Faturamento Anual",
	 *                      tag->"Faturamento_x0020_Anual_x0020_1"
	 *
	 * retorno: Nome do campo normatizado
	 *************************************************************************/
	this.parseNodeName =
	function(nodeName) {
		var i = 0;
		var strNode = '';
		var strASCIIToReplace = '';

		for (i = 0; i < nodeName.length; i++) {
			strNode += nodeName.substr(i, 1);

			if (i >= 6) {
				//Faturamento_x0020_Anual_x0020_1
				if ((nodeName.substr(i - 6, 4) == '_X00' ||
						nodeName.substr(i - 6, 4) == '_x00') &&
					(nodeName.substr(i, 1) == '_')) {
					strASCIIToReplace = nodeName.substr(i - 2, 2);

					// Espaco em branco
					if (strASCIIToReplace == '20') {
						strNode = strNode.substr(0, strNode.length - 7) + ' ';
					}
					else {
						strNode = strNode.substr(0, strNode.length - 7) +
							String.fromCharCode(parseInt(
								strASCIIToReplace, 16));
					}
				}
			}
		}

		return strNode;
	};
	
/*******************************  Atributos  *********************************/
	// Unformated fields name.
	this.unformatedFieldsName = new Array();
	this.fieldsNamesIndex = new Array();

	// Array de struturas lidas do XML.
	this.structs = new Array();
	// Registro com dos dados recuperados do banco.
	this.record = new Array();
	// Array para o buffer de dados. Contem os registros recuperados do banco.
	this.data = new Array();

	// Atributos para a criacao de um CFieldStruct.
	this.sTableName = '';
	this.sNome = '';
	this.nType = '';
	this.nSize = '';
	this.nPrecision = '';
	this.nScale = '';
	this.nAttributes = 0;
}

