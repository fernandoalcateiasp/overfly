/***********************************************************************************************
CDatatransport.js

Objeto de transporte de dados, que utiliza http como protocolo de comunicacao entre um client 
    javascript e um servidor de web-services.

pre-requesitos:
    Segundo a nota da microsoft (http://support.microsoft.com/default.aspx?scid=kb;en-us;819267)
    os metodos POST e GET foram desabilitados a partir da versao 1.1 do .net framework.
    Estes metodos sao utilizados por este componente, e para habilita-los no servidor do
    web-service, temos que fazer a seguinte alteracao no web.config do projeto do werb-service:
    
    <configuration>
        <system.web>
        <webServices>
            <protocols>
                <add name="HttpGet"/>
                <add name="HttpPost"/>
            </protocols>
        </webServices>
        </system.web>
    </configuration>
***********************************************************************************************/
var glb_reloadSupEstadoID = null;

/*************************************************************************************
Cdatatransport:construtor

(*) - parametro opcional
*************************************************************************************/
function CDatatransport(sID)
{
    // Variaveis internas
    this.insertInterval = null;
    
    this.id = sID;
    this.name = sID;
    
    // objeto xmlHTTP
    this.xmlHttp = null;
    try
    {
		var thisPointer = this;
		
		// Tenta obter o parser para o Internet Explorer
		try {
			try {
				this.xmlHttp = new ActiveXObject("MSXML2.XMLHTTP.6.0");
			} catch(e) {
				this.xmlHttp = new ActiveXObject("Microsoft.XMLHTTP");
			}
		}
		// Se falhou ...
		catch(e) {
			// ... tenta obter o parser para o Firefox, Mozilla, Opera, etc.
			try {
				this.xmlHttp = new XMLHttpRequest();
			}
			// Permanecendo o erro, lanca excecao.
			catch(e) {
				throw "OverflyRDS Erro: Navegador n�o suporta XML";
			}
		}

		this.xmlHttp.onreadystatechange = 
        function() {
			__readyStateChange.call(this, thisPointer);
		};
    }
    catch(e)
    {
        if(e.message != null)
        {
			window.top.overflyGen.Alert(e.message);
		}
		else
		{
			window.top.overflyGen.Alert(e);
		}
		
        return null;
    }
    
    ////////////////// Propriedades //////////////////
    // Array de attributos do objeto.
    this.attributes = new Array();
    // Seta um attributo.
    this.setAttribute =
    function(name, value) {
    	this.attributes[name] = value;
    };
    // Obtem o valor
    this.getAttribute =
    function(name, flag) {
    	var internalName = ((flag != null && flag == 0) ? internalName = name.toUpperCase() : name);

    	return this.attributes[internalName];
    };
    
    // Metodo CancelUpdate() para manter compatibilidade.
    this.CancelUpdate = 
    function() { };
    
    // Tipo de operacao sendo realizado com o webservice;
    this.operationType = 'READ';
    this.setOperationType =
    function(type)
    {
    	if (type != 'READ' && type != 'CHANGE') {
    		throw "CDatatransport.operationType value must be READ or CHANGE.";
    	}

    	this.operationType = type;
    };
    
    // Flag de erro a ser consultada pelo usuario
    this.Error = 0;
    
    // URL pertinente aos web-services
    this.host = '';
    // webservice a ser executado
    this.webservice = '';
    //parametros do web-service, devem ter o seguinte formato: '?param1=valor1&param2=valor2'
    this.parameters = '';
    //query a ser executada
    this.SQL = '';
    //url da query a ser executada
    this.URL = '';
    //Objeto que armazena o recordset do objeto
    this.recordset = new CRecordset();
        
    // Eventos
    this.ondatasetcomplete = null;
    
    // Metodos publicos
    this.refresh = _refresh;
    this.Refresh = _refresh;

    this.SubmitChanges = _SubmitChanges;
    
    setConnection(this);
}

/*************************************************************************************
_refresh:funcao interna

    descricao: executa a requisicao a um web-service

    parametros: 
    
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _refresh(objPointer)
{
	if(objPointer == null) {
		objPointer = this;
	}
	
	if (objPointer.insertInterval != null)
	{
		window.clearInterval(objPointer.insertInterval);
		objPointer.insertInterval = null;
	}

    if (objPointer.xmlHttp != null)
    {
        objPointer.recordset.clear();
        var objHTTPPointer = objPointer.xmlHttp;
        var ondatasetcompletePointer = objPointer.ondatasetcomplete;
        var thisPointer = objPointer;
        var strSQL = objPointer.SQL;
        var strURL = objPointer.URL;
        
        // No caso de ir buscar os dados por SQL, forca
        // chamar o web-service WSgetXMLRemoteData
        if (strSQL != '')
        {
            objPointer.webservice = "/WSgetXMLRemoteData";
            objPointer.parameters = "strSQL=" + escape(strSQL);
			objPointer.parameters += ("&applicationName=" + window.top.__APP_NAME__);
            
            objPointer.parameters = replaceStr(objPointer.parameters, "+", "%2B");
        }
        // Se for por URL, este passa a ser o host tambem
        else if (strURL != '')
        {
            objPointer.host = strURL;
            objPointer.webservice = "";
        }

        try
        {
            // Define a operacao do CDatatransport como READ
			objPointer.setOperationType('READ');
			
            objPointer.xmlHttp.open("POST", objPointer.host + objPointer.webservice, true);
            objPointer.xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=UTF-8");

            if ((objPointer.parameters != null) && ((objPointer.parameters != ""))) {
                objPointer.xmlHttp.send(objPointer.parameters);
            } else {
                objPointer.xmlHttp.send();
            }
        }
        catch(e)
        {
			if(e.message != null)
			{
			    window.top.overflyGen.Alert(e.message);
			}
			else
			{
			    window.top.overflyGen.Alert(e);
			}
        }
    }
}

/*************************************************************************************
__readyStateChange:funcao interna

    descricao: controla o retorno da requisicao feita ao servidor do web-service.
               readyState = 4 informa que os dados ja voltaram

    parametros: param1 - ponteiro para o objeto xmlHttp do objeto datatransport
                         
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function __readyStateChange(objPointer)
{
    var msg;

    // Ate aqui nenhum erro
    objPointer.Error = 0;

    // Os dados retornaram
	if (objPointer.xmlHttp.readyState == 4) 
    {
		if(objPointer.operationType == "READ")
		{
			try
			{
				if(objPointer.xmlHttp.status != 200) {
					msg = objPointer.xmlHttp.statusText;

					// Aqui um erro
					objPointer.Error = 1;

					window.top.overflyGen.Alert(msg.substr(7));
					unlockInterface(document.body.id);
					
					return;
				}
				
				// Carrega o XML para dentro do recordser
				if (_loadXML(objPointer)) {
					// Verifica se o retorno � um erro ao inv�s dos dados.
				    if (objPointer.recordset['OverflyRDSError'] != null) {

				        // Aqui um erro
				        objPointer.Error = 1;
					
					    window.top.overflyGen.Alert(objPointer.recordset['OverflyRDSError'].value);
					    unlockInterface(document.body.id);
					}
					// Dispara a funcao para o programador
					else if (objPointer.ondatasetcomplete != null) {
						objPointer.ondatasetcomplete.call(this);
					}
				}
			}
			catch(e)
			{
			    msg = ((e.message != null) ? e.message : e);

			    // Aqui um erro
			    objPointer.Error = 1;

				window.top.overflyGen.Alert(msg);
				unlockInterface(document.body.id);
			}
		}
		else if(objPointer.operationType == "CHANGE")
		{
			msg = (objPointer.xmlHttp.responseXML.text != null) ?
				objPointer.xmlHttp.responseXML.text :
				objPointer.xmlHttp.responseXML.textContent;
				
			if((msg != null) && (msg != '')) {

			    // Aqui um erro
			    objPointer.Error = 1;
			    
			    // Bug Termo de Uso 18/04/11
			    window.top.overflyGen.Alert(msg);
			    unlockInterface(document.body.id);

                // If abaixo feito para esconder a modal e dar um refresh no sup quando for troca de estado e tiver dado erro na trigger
			    if (glb_btnCtlSup && glb_EstadoIDUpdated) {
			        getStateMachineAbrev("dsoStateMachineInsert_DSC");
			        glb_reloadSupEstadoID = window.setInterval('reloadSupEstadoID()', 10, 'JavaScript');
			    }
			}
			else
			{
				objPointer.insertInterval = window.setInterval(
					function()
					{
						_refresh.call(this, objPointer);
					},
					10,
					'JavaScript');
			}
		}
		else {

		    // Aqui um erro
		    this.Error = 'Err_5';
			window.top.overflyGen.Alert("Incorrect CDatatransport.operationType in __readyStateChange.\n" + "CDatatransport.operationType = " + objPointer.operationType);
			
			unlockInterface(document.body.id);
		}
    }
}

function reloadSupEstadoID() {

    if (glb_reloadSupEstadoID != null) {
        window.clearInterval(glb_reloadSupEstadoID);
        glb_reloadSupEstadoID = null;
    }

    __btn_REFR('sup');
}

/*************************************************************************************
unlockInterface(htmlID):funcao interna

    descricao: Destrava a interface atual.

    parametros: htmlID - ID do HTML em quest�o.
                         
    retorno: void
*************************************************************************************/
function unlockInterface(htmlID) {
	if(document.body.id.toUpperCase().indexOf("MODAL",0) == 0) {
		lockControlsInModalWin(false);
	} else {
		lockInterface(false);
	}
}

/*************************************************************************************
_loadXML:funcao interna

    descricao: controla o retorno da requisicao feita ao servidor do web-service.
               readyState = 4 informa que os dados ja voltaram

    parametros: param1 - ponteiro para o objeto xmlHttp do objeto datatransport
                param2 - ponteiro para a funcao setada pelo programador para ser disparada
                         quando os dados voltarem.
                         
    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _loadXML(objPointer) {
	var msgToThrow;
	var msgWithLines;
	var pos;
	
	msgToThrow = "OverflyRDS Erro: Server not available\n" +
		objPointer.id + '\nURL:' + objPointer.URL +
		'\nSQL:' + objPointer.SQL;
		
	msgWithLines = "";
	for(pos = 0; pos < msgToThrow.length - 50; pos = pos + 50)
	{
		msgWithLines += msgToThrow.substring(
			pos,
			((msgToThrow.length - pos > 50) ? pos + 50 : msgToThrow.length) 
		) +
		"\n";
	}
	
	if((msgToThrow.length % 50) > 0)
	{
		msgWithLines += msgToThrow.substring(pos);
	}
	
	// Libera os dados antigos do recordset
	objPointer.recordset.clear();

	// Varre o XML para ver onde comeca e onde termina as tags de etrurura de campos e a de dados
	// pois ambas estao no mesmo node
	// Servidor indisponivel
	if (objPointer.xmlHttp.responseXML.childNodes.length == 0)
	{
		throw msgWithLines;
	}
    
	// Le os dados do XML.
	try {
		var recorsetParser = new CRecordsetParser();
		
		recorsetParser.startParser(objPointer.xmlHttp);

		// Copia a estrutura.
		objPointer.recordset.loadFieldStruct(recorsetParser.getStructs());		

		// Copia os registros.
		objPointer.recordset.aRecords = recorsetParser.getData();
		
		// Libera o objeto de parser.
		recorsetParser.freeRecordsetParser();
		delete recorsetParser;
	} catch (e) {
		throw "OverflyRDS Error in Return XML: " + e.message;
	}
    
    // Posiciona o ponteiro no inicio do conjunto retornado para comecar
    // a copia dos dados originais.
    objPointer.recordset.moveFirst();

	// Copia os dados originais do banco de dados.
	for(r = 0; r < objPointer.recordset.aRecords.length; r++) {
		var record = objPointer.recordset.aRecords[r];
		var dataBaseRecord = new Array();
		
		for(db = 0;	db < record.length; db++) {
			dataBaseRecord[db] = record[db];
		}
		
		objPointer.recordset.dataBaseRecords[r] = dataBaseRecord;
	}
	
    // Posiciona o ponteiro no inicio do conjunto retornado para comecar
    // a trabalhar com o objeto.
    objPointer.recordset.moveFirst();

    return true;
}

// Indica que o regisro foi lido do banco e nao sofreu nenhuma alteracao
var REGISTRO_SINCRONIZADO = 0; 
// Indica que o regisro foi lido do banco e sofreu nenhuma alteracao
var REGISTRO_ALTERADO     = 1;
// Indica que um novo registro foi incluido no array de registros e precisa ser salvo
var REGISTRO_NOVO         = 2;
// Indica que o registro foi excluido.
var REGISTRO_EXCLUIDO     = 3;

/*
 * Funcao para sincronizar os dados do recordset com o banco de dados.
 */
function _SubmitChanges()
{
	// Se o record set foi obtido por URL, nao e permitido alteracao.
	if(this.URL != null && this.URL.length > 0)
	{
		throw "Read only recordset.";
	}
	
	var separedores = new Array();
	separedores["TABELA"]   = String.fromCharCode(23);//'^';
	separedores["REGISTRO"] = String.fromCharCode(24);//'~';
	separedores["CAMPO"]    = String.fromCharCode(25);//'`';

	if (this.xmlHttp != null) {
		// Define o webservice a ser chamado.
		this.webservice = "/WSupdateRecords";
		// Obtem o buffer dos registro
		var buffer = this.recordset.aRecords;
		// Obtem o buffer dos registros com os valores originais do banco.
		var dbBuffer = this.recordset.dataBaseRecords;
		// Para decidir se atualiza o registro atual pega a tamanho do buffer.
		var max = this.recordset.filtered ? this.recordset.aFilteredRecords.length : this.recordset.aRecords.length;
		
		// Se o ponteiro estiver no buffer, atualiza o registro atual.
		if(this.recordset.recordPointer > -1 && this.recordset.recordPointer < max) {
			this.recordset.updateRecord(this.recordset.filtered ? this.recordset.aFilteredRecords[this.recordset.recordPointer] : this.recordset.aRecords[this.recordset.recordPointer]);
		}
						
		var acao; // Indice do flag de acao.
		var marcas; // Indice das marcas no registro.
		var dbReg = ''; // Auxiliar para formatar o registro original do banco
		var liberado = false;
		var tipo = null;
		var size = null;
		var fieldAtts;

		// Comeca a formatar os registros para envio ao servidor.
		this.parameters = "strRecords=";
		
		for(reg = 0; reg < buffer.length; reg++) {
			// Guarda o indice do flag de acao sobre o campo (Alteracao, Exclusao, ...).
			acao = buffer[reg].length - 1;
			// Obtem o indice das marcas de alteracoes sobre os registros.
			marcas = buffer[reg].length - 2;

			// Se o registro nao possuir marca para atualizacao,
			// nao e necessario fazer nada.
			if(buffer[reg][acao] == REGISTRO_SINCRONIZADO) {
				continue;
			}
			// Se ha' alteracoes que precisam ser sincronizadas,
			// verifica se as chaves primarias contem valores sem as quais,
			// as atualizacoes nao sao possiveis.
			else {
				for(f = 0; f < this.recordset.fields.count; f++) {
					fieldAtts = this.recordset.fields.item[f].attributes;
					
					// Se o campo e' chave, mas nao e' autoincrementado, 
					// ele deve possuir valor.
					if((fieldAtts & attributesValues['IsKey']) && !(fieldAtts & attributesValues['IsAutoIncrement']) && buffer[reg][f] == null)
					{
						throw "Nao e possivel fazer a atualizacao sem chave primaria.";
					}
				}
			}
			
			this.parameters += buffer[reg][acao] + separedores["TABELA"] + this.recordset.aFieldsStructure[0].table + separedores["TABELA"];
			
			// Obtem os dados dos campos do registro corrente.
			for(f = 0; f < this.recordset.fields.count; f++) {
				// Se nao tem um table associado, despreza
				if (this.recordset.fields.item[f].table == '')
				{
					continue;
				}

				// Identidfica se o campo original sera ou nao enviado
				// para verificacao no servidor. Comeca com false e sera
				// alterado apenas se o campo do buffer de trabalho for incluido.
				liberado = false;
				
				// O conteudo do campo sera passado para o servidor se
				// ele atender a uma das condicoes:
				// 1. O registro e' um novo registro
				// 2. O registro foi excluido e o campo e' chave
				// 3. O registro foi alterado e o campo em questao foi o alvo da alteracao
				if(buffer[reg][acao] == REGISTRO_NOVO || (buffer[reg][acao] == REGISTRO_EXCLUIDO && (this.recordset.fields.item[f].attributes & attributesValues['IsKey'])) ||
				  (buffer[reg][acao] == REGISTRO_ALTERADO && buffer[reg][marcas] != null && buffer[reg][marcas][f] == 1) ||
				  (buffer[reg][acao] == REGISTRO_ALTERADO && (this.recordset.fields.item[f].attributes & attributesValues['IsKey'])) ||
				  (buffer[reg][acao] == REGISTRO_ALTERADO && (this.recordset.fields.item[f].attributes & attributesValues['IsAutoIncrement'])))
				{
					// String.fromCharCode(253) => CAMPO
					this.parameters += (buffer[reg][marcas] != null ?
							buffer[reg][marcas][f] + separedores["CAMPO"] : "0" + separedores["CAMPO"]) +
						this.recordset.fields.item[f].name + separedores["CAMPO"] + 
						dataTypeName[this.recordset.fields.item[f].type] + separedores["CAMPO"] + 
						buffer[reg][f] + separedores["CAMPO"] + 
						this.recordset.fields.item[f].attributes;
									
					if(f < this.recordset.fields.item.length - 1) {
						this.parameters += separedores["REGISTRO"];
					}
					
					// Ja' que o campo do buffer de trabalho entrou, o campo original
					// talvez entre. Dependera' do tipo do mesmo.
					liberado = true;
				}
				
				// Alem de saber se o campo do buffer de trabalho necessita de altualizacao,
				// o tipo do campo tambem e' verificado.
				// Os campos nchar, char, varchar e nvarchar entram na avaliacao
				// se o tamanho for <= 200. Por isso avalia-se o tamanho na primeira
				// condicao, favorecendo o short-circuit. Na sequencia e' verificado 
				// os campos que em hipotese alguma entram
				// na clausula where.
				tipo = this.recordset.fields.item[f].type;
				size = this.recordset.fields.item[f].size;
				liberado = liberado &&
					((size <= 200) &&
					(tipo != dataTypesCode["image"]) &&
					(tipo != dataTypesCode["text"]) &&
					(tipo != dataTypesCode["ntext"]) &&
					(tipo != dataTypesCode["binary"]) &&
					(tipo != dataTypesCode["varbinary"]) &&
					(tipo != dataTypesCode["sql_variant"]));

				// Formata o registro com valores originais do banco de dados
				// se nao for um registro novo e nao for um tipo de campo bloqueado.
				if(buffer[reg][acao] != REGISTRO_NOVO && liberado) {
					dbReg += "0" + separedores["CAMPO"] +
						this.recordset.fields.item[f].name + separedores["CAMPO"] + 
						dataTypeName[this.recordset.fields.item[f].type] + separedores["CAMPO"] + 
						(dbBuffer[reg][f] != null ? dbBuffer[reg][f] : "null") + separedores["CAMPO"] + 
						this.recordset.fields.item[f].attributes;

					if(f < this.recordset.fields.item.length - 1) {
						dbReg += separedores["REGISTRO"];
					}
				}
			}
			
			// Se nao houver registros para atualizar ou apagar,
			// caloca apenas a marca de registro novo no registro
			// original do banco.
			if(dbReg == '') {
				dbReg = REGISTRO_NOVO;
			}
			
			// Acrescenta ao parametro de envio o registro original.
			this.parameters += separedores["TABELA"];
			this.parameters += dbReg;
			this.parameters += separedores["TABELA"];
			
			// Limpa a formatacao do registro original para formatar o proximo
			dbReg = '';
		}
		
		delete separedores;
		
		// Substitui o caracter usado para separar par�metros na chamada do webservice.
		this.parameters = replaceStr(this.parameters, "&", "%26");
		
		// Trecho de codigo que chama o webservice para sincronizacao
		// dos dados com o banco de dados se houver registros a sincronizar.
		// O formato da string enviada e':
		// "<ACAO>|<TABELA>|<REGISTRO>|<REGISTRO_ORIGINAL>[/]*
		// Onde REGISTRO segue o formato:
		// <FLAG ALTERADO>;<CAMPO>;<TIPO>;<VALOR>;<ATTRIBUTOS>
		// Exemplo de uma alteracao de registro.
		//1|Pessoas
		//0;PessoaID;int;7;557056/1;Fantasia;varchar;Allplus;12|0;PessoaID;int;7;557056/0;Fantasia;varchar;Alcateia;12
		//0;PessoaID;int;2;557056/1;Fantasia;varchar;Sendas;12|0;PessoaID;int;2;557056/0;Fantasia;varchar;Grupo Sendas;12
		if(this.parameters != "strRecords=")
		{
			this.parameters += ("&culture=" + (DATE_FORMAT == 'MM/DD/YYYY' ? 'en-US' : 'pt-BR'));
			this.parameters += ("&applicationName=" + window.top.__APP_NAME__);
			
			this.parameters = replaceStr(this.parameters, "+", "%2B");
			
			try
			{
				var thisPointer = this;

				this.setOperationType('CHANGE');

				this.xmlHttp.open("post", this.host + this.webservice, false);
				this.xmlHttp.setRequestHeader("Content-Type","application/x-www-form-urlencoded");

				if ((this.parameters != null) && ((this.parameters != ""))) {
					this.xmlHttp.send(this.parameters);
				} else {
					this.xmlHttp.send();
				}
			} catch(e) {
				if(e.message != null) throw "Nao foi possivel atualizar os registros. Motivo: " + e.message;
				else throw "Nao foi possivel atualizar os registros. Motivo: " + e;
			}
		}
		else
		{
		    this.ondatasetcomplete.call(this);
		}
    }
}
