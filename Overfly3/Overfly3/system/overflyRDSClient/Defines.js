﻿var dataTypesCode = new Array();

dataTypesCode["bigint"] = 20;
dataTypesCode["binary"] = 128;
dataTypesCode["bit"] = 11;
dataTypesCode["char"] = 129;
dataTypesCode["datetime"] = 135;
dataTypesCode["decimal"] = 131;
dataTypesCode["float"] = 5;
dataTypesCode["image"] = 205;
dataTypesCode["int"] = 3;
dataTypesCode["money"] = 6;
dataTypesCode["nchar"] = 130;
dataTypesCode["ntext"] = 203;
dataTypesCode["numeric"] = 131;
dataTypesCode["nvarchar"] = 202;
dataTypesCode["real"] = 4;
dataTypesCode["smalldatetime"] = 135;
dataTypesCode["smallint"] = 2;
dataTypesCode["smallmoney"] = 6;
dataTypesCode["sql_variant"] = 12;
dataTypesCode["text"] = 201;
dataTypesCode["timestamp"] = 128;
dataTypesCode["tinyint"] = 17;
dataTypesCode["uniqueidentifier"] = 72;
dataTypesCode["varbinary"] = 204;
dataTypesCode["varchar"] = 200;



var dataTypeName = new Array();

dataTypeName[2] = "smallint";
dataTypeName[3] = "int";
dataTypeName[4] = "real";
dataTypeName[5] = "float";
dataTypeName[6] = "money";
dataTypeName[11] = "bit";
dataTypeName[12] = "sql_variant";
dataTypeName[17] = "tinyint";
dataTypeName[20] = "bigint";
dataTypeName[72] = "uniqueidentifier";
dataTypeName[128] = "binary";
dataTypeName[129] = "char";
dataTypeName[130] = "nchar";
dataTypeName[131] = "decimal";
dataTypeName[135] = "datetime";
dataTypeName[200] = "varchar";
dataTypeName[201] = "text";
dataTypeName[202] = "nvarchar";
dataTypeName[203] = "ntext";
dataTypeName[204] = "varbinary";
dataTypeName[205] = "image";

// DefiniÃ§Ã£o dos attributos de um Field.
var attributesValues = new Array();
attributesValues['IsKey'] = 32768;
attributesValues['AllowDBNull'] = 64;
attributesValues['IsReadOnly'] = 4 + 8;
attributesValues['IsIdentity'] = 524288;
attributesValues['IsAutoIncrement'] = 1048576;


/************************************************************************
* Objeto de controle de acesso aos forms do Overfly.
************************************************************************/
function OverflyRDSForms() {
    // Matriz de forms do Overfly.
    this.forms = [new Array(), new Array()];

    // Grupo de forms para a produção.
    this.forms[0][this.forms[0].length] = ["/recursos/", "/recursos/"];
    this.forms[0][this.forms[0].length] = ["/relentrerecs/", "/relentrerecs/"];
    this.forms[0][this.forms[0].length] = ["/relpessoaserecursos/", "/relpessoaserecursos/"];
    this.forms[0][this.forms[0].length] = ["/localizacoes/", "/localizacoes/"];
    this.forms[0][this.forms[0].length] = ["/acoesmarketing/", "/acoesmarketing/"];
    this.forms[0][this.forms[0].length] = ["/frmpedidos/", "/frmpedidos/"];
    this.forms[0][this.forms[0].length] = ["/relpessoaseconceitos/", "/relpessoaseconceitos/"];
    this.forms[0][this.forms[0].length] = ["/historicospadrao/", "/historicospadrao/"];
    this.forms[0][this.forms[0].length] = ["/operacoes/", "/operacoes/"];
    this.forms[0][this.forms[0].length] = ["/campanhas/", "/campanhas/"];
    this.forms[0][this.forms[0].length] = ["/planocontas/", "/planocontas/"];
    this.forms[0][this.forms[0].length] = ["/lancamentos/", "/lancamentos/"];
    this.forms[0][this.forms[0].length] = ["/extratosbancarios/", "/extratosbancarios/"];
    this.forms[0][this.forms[0].length] = ["/fopag/", "/fopag/"];
    this.forms[0][this.forms[0].length] = ["/valoresalocalizar/", "/valoresalocalizar/"];
    //this.forms[0][this.forms[0].length] = ["/valoresalocalizar/", "/valoresalocalizar/"];
    this.forms[0][this.forms[0].length] = ["/financeiro/", "/financeiro/"];

    // Grupo de forms para o desenvolvimento	
    this.forms[1][this.forms[1].length] = ["/recursos/", "/recursos/"];
    this.forms[1][this.forms[1].length] = ["/relentrerecs/", "/relentrerecs/"];
    this.forms[1][this.forms[1].length] = ["/relpessoaserecursos/", "/relpessoaserecursos/"];
    this.forms[1][this.forms[1].length] = ["/localizacoes/", "/localizacoes/"];
    this.forms[1][this.forms[1].length] = ["/acoesmarketing/", "/acoesmarketing/"];
    this.forms[1][this.forms[1].length] = ["/frmpedidos/", "/frmpedidos/"];
    this.forms[1][this.forms[1].length] = ["/relpessoaseconceitos/", "/relpessoaseconceitos/"];
    this.forms[1][this.forms[1].length] = ["/historicospadrao/", "/historicospadrao/"];
    this.forms[1][this.forms[1].length] = ["/operacoes/", "/operacoes/"];
    this.forms[1][this.forms[1].length] = ["/notasfiscais/", "/notasfiscais/"];
    this.forms[1][this.forms[1].length] = ["/avaliacao/", "/avaliacao"];
    this.forms[1][this.forms[1].length] = ["/cartascorrecao/", "/cartascorrecao/"];
    this.forms[1][this.forms[1].length] = ["/campanhas/", "/campanhas/"];
    this.forms[1][this.forms[1].length] = ["/planocontas/", "/planocontas/"];
    this.forms[1][this.forms[1].length] = ["/lancamentos/", "/lancamentos/"];
    this.forms[1][this.forms[1].length] = ["/extratosbancarios/", "/extratosbancarios/"];
    this.forms[1][this.forms[1].length] = ["/fopag/", "/fopag/"];    
    //this.forms[1][this.forms[1].length] = ["/cartascorrecao/", "/cartascorrecaoEx/"];
    //this.forms[1][this.forms[1].length] = ["/operacoes/", "/operacoesEx/"];
    //this.forms[1][this.forms[1].length] = ["/notasfiscais/", "/notasfiscaisEx/"]; // REFAZER
    //this.forms[1][this.forms[1].length] = ["/extratosbancarios/", "/extratosbancariosEx/"];
    //this.forms[1][this.forms[1].length] = ["/historicospadrao/", "/historicospadraoEx/"];
    //this.forms[1][this.forms[1].length] = ["/lancamentos/", "/lancamentosEx/"];
    //this.forms[1][this.forms[1].length] = ["/planocontas/", "/planocontasEx/"];
    this.forms[1][this.forms[1].length] = ["/valoresalocalizar/", "/valoresalocalizar/"];
    this.forms[1][this.forms[1].length] = ["/financeiro/", "/financeiro/"];

    /************************************************************************
	* Retorna a URL para utilizar o OverflyRDS para os IPs especificados.
	************************************************************************/
    this.form =
	function (url, ambiente) {
	    var ipValido;
	    var i;
	    var formsSet;

	    // Se for ambiente de desenvolvimento, pega o
	    // conjunto de form que usam o OverflyRDS.
	    formsSet = this.forms[(ambiente ? 1 : 0)];

	    // Percorre o conjunto de forms para saber se ...
	    for (i = 0; i < formsSet.length; i++) {
	        // ... troca o form antigo por um que use o OverflyRDS.
	        if (url.indexOf(formsSet[i][0]) != -1) {
	            url = url.replace(formsSet[i][0], formsSet[i][1]);

	            // Após a troca não é necessário verificar os
	            // outros e sai do loop.
	            break;
	        }
	    }

	    // Retorna o form a ser utilizado.
	    return url;
	}

}

// Objeto de seleção do form a ser usado.
var overflyRDSForms = new OverflyRDSForms();
