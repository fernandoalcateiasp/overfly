/*************************************************************************************
CFields.js

Armezena uma colecao de objetos CField
**************************************************************************************/

/*************************************************************************************
CFields:construtor

(*) - parametro opcional
*************************************************************************************/
function CFields() {
    // Variaveis internas
    this.item = new Array();
    this.Item = this.item;
    
    // Propriedades
    this.count = 0;
    this.Count = 0;
    this.nSize = 0;

	this.isValid = true;
	
    // Metodos
    this.clear = _fieldsClear;
    this.add = _add;
    
    this.eraseValues = _eraseValues;
}

function _eraseValues() {
	for(i = 0; i < this.count; i++) {
		this.item[i].value = null;
		this.item[i].originalValue = null;
	}
	
	this.isValid = false;
}

/*************************************************************************************
_fieldsClear:funcao interna

    descricao: limpa o objeto, zerando o array que contem os campos

    parametros: 

    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _fieldsClear()
{
    // Invalida todos os campos
    for(i = 0; i < this.count; i++)
    {
		delete this[i];
		delete this[this.item[i].name];
		
        delete this.item[i];
    }

    this.count = 0;
    this.Count = 0;
}

/*************************************************************************************
_add:funcao interna

    descricao: adiciona um campo na colecao de campos

    parametros: fieldName - Nome do campo
                fieldValue - Valor do campo
                fieldType - tipo do campo (inteiro, descrito com detalhes no objeto CField)
                *fieldSize - Tamanho do campo
                *fieldPrecision - Precision do campo (cadas decimais)

    retorno: void

(*) - parametro opcional
*************************************************************************************/
function _add(oField)
{
    var currArray = this.item;

    this.item[aLength(currArray)] = oField;
    this[oField.name] = oField;

    currArray = this.item;
    var nTop = aLength(currArray);
    this[nTop-1] = this.item[nTop-1];

    this.count = nTop;
    this.Count = nTop;
}
