/***********************************************************************************************
CField.js

Armezena as caracteristicas de um campo
***********************************************************************************************/

/*************************************************************************************
CField: Construtor da classe java script
    sNome: Nome do campo
    sValor: Valor do campo

    nType:
	bigint   (nType == 3)
	int      (nType == 11)
	numerico (nType == 14)
	bool     (nType == 20)
	char     (nType == 129)
	decimal  (nType == 131)
	datetime (nType == 135)
	varchar  (nType == 200)
	
	nSize: Tamanho do campo
	nPrecision: Precision do campo
	nScale: Scale do campo

    Attributes:
    4         - campo updatable
    8         - campo idenity 
    64        - campo aceita nulo
    32768     - campo primary key

(*) - parametro opcional
*************************************************************************************/
function CField(sTableName, sNome, sValor, nType, nSize, nPrecision, nScale, nAttributes)
{
	this.table = sTableName;
	this.Table = sTableName;
	this.name = sNome;
	this.Name = sNome;
	this.value = sValor;
	this.dateValue = null;
	this.originalValue = this.value;
	this.type = nType;
	this.Type = nType;
	this.attributes = nAttributes;
	this.Attributes = nAttributes;

	// size, para tipo varchar ou numeric
	this.size = nSize;
	this.Size = nSize;

	// Precision, para tipo numeric
	this.precision = nPrecision;
	this.Precision = nPrecision;

	// Scale para o tipo decimal
	this.numericScale = nScale;
	this.NumericScale = nScale;

	// Metodos publicos
	/**
	 * Recupera o valor do campo como um valor de ponto flutuante.
	 */

	this.valueAsStrFloat =
	function () {
	    if (this.Name == 'MargemContribuicao')
	    {
	        this.NumericScale = nScale;
	    }

	    if ((this.type != dataTypesCode["decimal"]) &&
			(this.type != dataTypesCode["float"]) &&
			(this.type != dataTypesCode["real"]) &&
			(this.type != dataTypesCode["money"]) &&
			(this.type != dataTypesCode["numeric"]) &&
			(this.type != dataTypesCode["smallmoney"])) {
	        return this.value;
	    }

	    // Define a precis�o conforme o tipo. Se for dinheiro, fixa em duas casas decimais.
	    var precision = ((this.type == dataTypesCode["money"]) || (this.type == dataTypesCode["smallmoney"])) ? 2 : this.numericScale;
	    // Define a constante para o n�mero de casas decimais.
	    var K = Math.pow(10, -precision);
	    // Define a constante para o n�mero de casas decimais com a margem de seguran�a.
	    // var margem = Math.min(precision + 3, 6);
	    var margem = precision + 3;
	    var bNegativo = (this.value < 0);

        // Transforma numero em positivo
	    if (bNegativo)
	        this.value = this.value * -1;

	    // Parte inteira do n�mero.
	    var intValue = Math.floor(this.value);
	    // Parte decimal do n�mero como n�mero inteiro.
	    var D = ((this.value - intValue) / K);

	    // Tamanho do n�mero resultante.
	    var size = ("" + intValue).length + precision + (precision > 0 ? 1 : 0);

	    // Transforma numero em negativo
	    if (bNegativo)
	    {
	        this.value = this.value * -1;
	        intValue = intValue * -1;
	        size += 1;
	    }

	    // Tamanho do n�mero com a margem de seguran�a.
	    // if (intValue < 0)
	    if (bNegativo)
	        var result = intValue - (D * K + Math.pow(10, -margem));
	    else
	        var result = intValue + (D * K + Math.pow(10, -margem));

	    result = result.toFixed(margem);

	    // Retorna o valor com o n�mero de casas decimais necess�rias.
	    return ("" + result).substr(0, size);
	};
	
	/*************************************************************************************
	 * Fieldclear():metodo - limpa o objeto
	 ************************************************************************************/
	this.clear =
    function() {
    	this.name = null;
    	this.Name = null;
    	this.value = null;
    	this.type = null;
    	this.Type = null;
    	this.attributes = null;
    	this.Attributes = null;

    	// size, para tipo varchar ou numeric
    	this.size = null;
    	this.Size = null;

    	// Precision, para tipo numeric
    	this.precision = null;
    	this.Precision = null;

    	// Scale para o tipo decimal
    	this.numericScale = null;
    	this.NumericScale = null;
    };
	
	/*************************************************************************************
	 * Retorna o o valor do field como uma data segundo a formatacao definida pelo
	 * usuario.
	 * Se o tipo do campo nao for uma campo data, o formato nulo ou o formato nao for um
	 * formato de data, � retornado um null.
	 ************************************************************************************/
    this.asDatetime =
	function(formato) {
		if (this.type != dataTypesCode["datetime"] ||
			this.value == null ||
			this.value.length == null ||
			this.value.length < 2 ||
			formato == null ||
			formato.length == null ||
			formato.length == 0) {
			return null;
		}

		return this.value.substr(0, formato.length);
	};
	
	/*************************************************************************************
	 * Seta o valor Retorna o o valor do field como uma data segundo a formatacao definida pelo
	 * usuario.
	 * Se o tipo do campo nao for uma campo data, � retornado um null.
	 ************************************************************************************/
	this.setDatetime =
	function(valor, formato) {
		var aux = new CField(null, "aux", valor, dataTypesCode["datetime"], null, null, null, null);

		value = aux.asDatetime(formato);

		delete aux;
	};
}


