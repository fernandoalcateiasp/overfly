/********************************************************************
js_fastbuttonsbar.js

Library javascript para o fastbuttons.asp
********************************************************************/

/********************************************************************
INDICE DAS FUNCOES:
    lockFastButtons()
    unlockFastButtons()
    fastButtonsIsLocked()
    setupFastButton(btnNumber, arrayInterface, formName)
********************************************************************/

// As variaveis globais abaixo sao de uso exclusivo desta library
// VARIAVEIS GLOBAIS ************************************************
var _MAXNUNBTNS = 9;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Trava uma barra de botoes de atalho do browser mae ou de um browser
filho.

Parametros:
nenhum

Retorno:
1 - sucesso
0 - falhou
-1 - a barra de botoes de atalho ja estava travada

A funcao setupFastButton prevalece sobre esta e sobre a unlockFastButtons.
A ordem deve ser a seguinte: setupFastButton, lockFastButtons e unlockFastButtons
********************************************************************/
function lockFastButtons()
{
    return lockUnlockControls(true);
}

/********************************************************************
Destrava uma barra de botoes de atalho do browser mae ou de um browser
filho.

Parametros:
nenhum

Retorno:
1 - sucesso
0 - falhou
-1 - a barra de botoes de atalho ja estava destravada

A funcao setupFastButton prevalece sobre esta e sobre a lockFastButtons.
A ordem deve ser a seguinte: setupFastButton, lockFastButtons e unlockFastButtons
********************************************************************/
function unlockFastButtons()
{
    return lockUnlockControls(false);
}

/********************************************************************
Informa se uma barra de botoes de atalho esta travada ou
destravada. Se chamada de um arquivo contido no browser mae, informa
o status da barra do browser mae, se chamada em um browser filho, informa
o status do browser filho.

Parametros:
nenhum

Retorno:
true se travada, caso contrario false
********************************************************************/
function fastButtonsIsLocked()
{
    return getCorrectHtml().__fBarIsLocked;
}

/********************************************************************
Configura um botao da barra de botoes de atalho

Parametros:
btnNumber           - botao a atuar: de 1 a 9
arrayInterface      - array: cmdShow (true/false),
                             cmdLocked (true/false),
                             caption,
                             title
formName            - nome do form a carregar

Retorno:
true se configurou, caso contrario false

Se a funcao e chamada em um arquivo contido no browser mae, atua sobre
botao da barra do browser mae, se chamada em um browser filho, atua
sobre botao da barra do browser filho.

NOTAS IMPORTANTES:
1. O elemento de cada array que for passado como null, permanecera
    inalterado.
2. Se o formName associado ao ultimo botao for null, dispara o
    out look com o Fale Conosco.
********************************************************************/
function setupFastButton(btnNumber, arrayInterface, formName)
{
    // obtem referencia ao html correto 
    var htmlScript = getCorrectHtml();
    
    if (htmlScript == null)
        return false;
        
    // verifica se o botao esta dentro do range
    if (btnNumber > htmlScript.__NUMMAXBTNS)
        return false;
    
    // chama funcao no html da barra correta
    return htmlScript.setupButton(btnNumber, arrayInterface, formName);    
}

/********************************************************************
Inicializa todos os botoes da barra de botoes de atalho

Parametros:
Nenhum

Retorno:
O numero de botoes inicializados

Se a funcao e chamada em um arquivo contido no browser mae, atua sobre
botao da barra do browser mae, se chamada em um browser filho, atua
sobre botao da barra do browser filho.
********************************************************************/
function initializeAllFastBtns()
{
    // obtem referencia ao html correto 
    var htmlScript = getCorrectHtml();
    
    if (htmlScript == null)
        return 0;
    
    // chama funcao no html da barra correta
    return htmlScript.initAllFastBtns();    
}

// FINAL DE IMPLEMENTACAO DAS FUNCOES

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****

/********************************************************************
Obtem uma referencia do html de uma barra de botoes de atalho

Parametros:
nenhum

Retorno:
a referencia ou null, se falha

Se a funcao e chamada em um arquivo contido no browser mae, retorna
referencia ao html da barra do browser mae, se chamada em um browser
filho, retorna referencia ao html da barra do browser filho
********************************************************************/
function getCorrectHtml()
{
    // pega o html do controlbar
    var frameID = null;
    var htmlScript = null;
    var re;
        
    // passa tudo para maiusculas
    var theBrowser = window.top.name.toUpperCase();
    
    // browser mae
    re = /MAIN/gi;            
    
    if (theBrowser.search(re) != -1)    
    {
        frameID = getFrameIdByHtmlId('fastbuttonsmainHtml');
        if (frameID)
            htmlScript = getPageFrameInHtmlTop(frameID);
    }
    
    // browser filho
    re = /CHILD/gi;            
    
    if (theBrowser.search(re) != -1)    
    {
        frameID = getFrameIdByHtmlId('fastbuttonschildHtml');
        if (frameID)
            htmlScript = getPageFrameInHtmlTop(frameID);
    }
    
    return htmlScript;
}

/********************************************************************
Trava ou destrava uma barra de botoes de atalho

Parametros:
nenhum

Retorno:
1 - sucesso
0 - falhou
-1 - a barra de botoes de atalho ja estava no estado solicitado

Se a funcao e chamada em um arquivo contido no browser mae, trava a
barra do browser mae, se chamada em um browser filho, trava a barra
do browser filho
********************************************************************/
function lockUnlockControls(cmdLock)
{
    var retVal = 0;
    var htmlScript;

    // obtem referencia ao html da barra
    htmlScript = getCorrectHtml();
    if (htmlScript == null)
        return retVal;
    
    // verifica se a barra ja esta no estado solicitado
    if ( cmdLock == htmlScript.__fBarIsLocked)
        return -1;
        
    // trava ou destrava a barra e salva seu estado
    if (htmlScript)
    {
        htmlScript.lockFastButtonsControls(cmdLock);
        retVal = 1;
    }    

    return retVal;            
}
// FINAL DE FUNCOES INTERNAS DA LIBRARY *****************************
