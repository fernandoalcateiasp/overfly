/********************************************************************
js_statusbar.js

Library javascript de funcoes basicas para os statusbars
********************************************************************/

// As constantes abaixo sao de uso exclusivo desta library
// CONSTANTES *******************************************************

// FINAL DE CONSTANTES **********************************************

/********************************************************************
INDICE DAS FUNCOES:
StatusBar:
    writeInStatusBar(statusBarToWrite, cellName, strToWrite, ellFlag)
    getDataInStatusBar(statusBarToGet, cellName)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Escreve em uma celula do statusbar.

Parametros:
statusBarToWrite    statusBarname: login, main, e child
cellName            nome da celula a escrever:
                    nome                paragraph ID
                    -----------         ------------
                    'cellPrgName'       paraPrgNome
                    'cellEmpresa'       paraEmpresa
                    'cellUserName'      paraUsuario
                    'cellForm'          paraForm
                    'cellMode'          paraModo

strToWrite          string a escrever
ellipsis            usar ... apos a palavra.
                    true (usa), false ou null (nao usa)

Retornos:
nenhum
********************************************************************/
function writeInStatusBar(statusBarToWrite, cellName, strToWrite, ellFlag)
{
    sendJSMessage('func_writeInStatusBar', JS_STBARWRITE, new Array(statusBarToWrite, cellName, strToWrite, ellFlag), null);
}

/********************************************************************
Retorna o dado escrito em uma celula do statusbar.

Parametros:
statusBarToGet      statusBarname: login, main, e child
cellName            nome da celula a recolher o dado:
                    nome                paragraph ID
                    -----------         ------------
                    'cellPrgName'       paraPrgNome
                    'cellEmpresa'       paraEmpresa ou selEmpresa
                    'cellUserName'      paraUsuario
                    'cellForm'          paraForm
                    'cellMode'          paraModo

Retornos:
O conteudo escrito na celula. Se o celula e um combo retorna o
seguinte array:
indice do option selecionado
value do option selecionado
texto do option selecionado
********************************************************************/
function getDataInStatusBar(statusBarToGet, cellName)
{
    return sendJSMessage('func_getDataInStatusBar', JS_STBARREAD, new Array(statusBarToGet, cellName), null);
}

// FINAL DE IMPLEMENTACAO DAS FUNCOES

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****


// FINAL DE FUNCOES INTERNAS DA LIBRARY *****************************