/********************************************************************
js_interfaceex.js

Library javascript de funcoes basicas para interface
********************************************************************/

// CONSTANTES *******************************************************
// variaveis de controle do lockInterface
var __ELEMSTATE = new Array();  //array: controlId, state
var __ELEMCOUNT = 0;
// variaveis de controle do lockControls
var __ELEMSTATEM = new Array();  //array: controlId, state
var __ELEMCOUNTM = 0;

var __HTMLISLOCKED = false;
var __MODALLOCKED = false;

var NUM_BTNS_ESPECIFICS = 16;

var glb_interfaceExTimer = null;

// FINAL DE CONSTANTES **********************************************

/********************************************************************

INDICE DAS FUNCOES:

Carregamento de interface:
loadFilesInfAndPesqList(formName, htmlID, wndTopName,
aFrame, aFilePath)
finishLoadForm(sup01HtmlID, inf01HtmlID, pesqlistHtmlID)                                 
Geral de interface:
setupEventOnFocusInTextFields()
setupEventOnClickInBtnsLupa()
setupEventOnChangeInSelects()
invertChkBox(ctl)
verifyNumericEnter()    
verifyNumericEnterNotLinked() - movida para a lib js_strings.js
setFocus1Field()
Travamento de interface:
lockControls(cmdLock, localAction)
Geral de html:
getHtmlId()
getBodyRef()
Carregamento combos contexto e filtro (sup e inf) e proprietario do pesqlist:
conjunto de funcoes usadas no sup01
e funcao que retorna o estado inicial para gravacao
de registro novo
Eventos disparados pelos controls bars nos arquivos:
conjunto de funcoes usadas no sup01, inf01 e pesqlist
Campos:
maxLenOnKeyPressSetArrays(dso)
Outros controles:
showEnableControls( aAttrib, cmdShow, cmdEnable )
evalShowHideCtlsInDivs(divRef)

Funcoes de direitos:
getCurrRightValue(sCtrlBar, sDirBit, nID)
Ajustagem dos controls bars em funcao dos direitos:
adjustControlBarSupByRights(prop1, prop2)
btnsSupAreTreatedManually(treatBtnRight)
adjustControlBarInfByRights(prop1, prop2)
btnsInfAreTreatedManually(treatBtnRight)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao carrega os arquivos recursosinf01.asp e
recursospesqlist.asp

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function loadFilesInfAndPesqList(formName, htmlID, wndTopName, aFrame, aFilePath) {
    // Monta variavel para passar o nome do form por parametro
    var strPars = new String();
    strPars = '?';
    strPars += 'formName=';
    strPars += escape(formName);

    // mensagem que inicia o carregamento do inf01.asp
    sendJSMessage(htmlID, JS_PAGELOAD,
                  new Array(wndTopName,
                  aFrame[0],
                  aFilePath[0] + strPars),
                  null);
    // mensagem que inicia o carregamento do pesqlist01.asp              
    sendJSMessage(htmlID, JS_PAGELOAD,
                  new Array(wndTopName,
                  aFrame[1],
                  aFilePath[1] + strPars),
                  null);

}

/********************************************************************
Esta funcao e chamada ao final do carregamento de todos os componentes
(arquivos e/ou DSOs) do form

Parametros:
nenhum

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function finishLoadForm(sup01HtmlID, inf01HtmlID, pesqlistHtmlID) {
    var frameID;

    // Atencao, a funcao putFormPartInPos e muito particular
    // e depende dos controls bars terem as tags html ==
    // controlbarsupHtml e controlbarinfHtml

    // Ajusta a posicao e dimensoes do sup01.asp
    putFormPartInPos(sup01HtmlID, 'SUP');

    // Ajusta a posicao e dimensoes do inf01.asp
    putFormPartInPos(inf01HtmlID, 'INF');

    // Ajusta a posicao e dimensoes do pesqlist.asp
    putFormPartInPos(pesqlistHtmlID, 'BOTH');

    // mostra o pesqlist.asp
    frameID = getFrameIdByHtmlId(pesqlistHtmlID);
    if (frameID) {
        // mostra o pesqlist.asp
        showFrameInHtmlTop(frameID, true);
        // escreve o modo no status bar
        writeInStatusBar('child', 'cellMode', 'Pesquisa');
        // registra o pesqlist.asp no control bar superior 
        regHtmlInControlBar('sup', pesqlistHtmlID);
    }
    else
        return false;

    // avisa o browser filho que o form acabou de carregar
    sendJSMessage(getHtmlId(), JS_FORMOPENED, new Array(window.top.name, glb_FORMNAME), null);

    return true;
}

/********************************************************************
Esta funcao prepara todos os controles input text de um arquivo
para selecionarem seu conteudo quando em foco
********************************************************************/
function setupEventOnFocusInTextFields() {
    var coll = window.document.getElementsByTagName('INPUT');
    var elem;
    var i;

    for (i = 0; i < coll.length; i++) {
        elem = coll.item(i);
        if (elem.type.toUpperCase() == 'TEXT') {
            elem.onfocus = selFieldContent;
        }
    }

    coll = window.document.getElementsByTagName('TEXTAREA');

    for (i = 0; i < coll.length; i++)
        coll.item(i).onfocus = selFieldContent;
}

/********************************************************************
Esta funcao prepara todos os controles input image de um arquivo
(botao de lupa) para receber onclick
********************************************************************/
function setupEventOnClickInBtnsLupa() {
    var coll = window.document.getElementsByTagName('INPUT');
    var elem, i;

    for (i = 0; i < coll.length; i++) {
        elem = coll.item(i);
        if (elem.type.toUpperCase() == 'IMAGE')
            elem.onclick = onClickBtnLupa;
    }
}

/********************************************************************
Esta funcao prepara todos os select de um arquivo
para dispararem a funcao optChangedInCmb(this)
********************************************************************/
function setupEventOnChangeInSelects() {
    var coll = window.document.getElementsByTagName('SELECT');
    var i;

    for (i = 0; i < coll.length; i++)
        coll.item(i).onchange = selChanged;
}

/********************************************************************
Dispara funcao optChangedInCmb(this)
********************************************************************/
function selChanged() {
    optChangedInCmb(this);
}

/********************************************************************
Esta funcao prepara todos os labels de checkbox de um arquivo
para dispararem a funcao invertChkBox(ctl)
********************************************************************/
function setupEventInvertChkBox() {
    var coll = window.document.getElementsByTagName('P');
    var i;
    var elem;

    for (i = 0; i < coll.length; i++) {
        elem = coll.item(i);
        if ((elem.nextSibling.tagName).toUpperCase() == 'INPUT')
            if ((elem.nextSibling.type).toUpperCase() == 'CHECKBOX')
            elem.onclick = invertChkBox;
    }
}

/********************************************************************
Inverte o check de um checkbox associado a um label
********************************************************************/
function invertChkBox() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;
}

// CARREGAMENTO COMBOS CONTEXTO E FILTRO (SUP E INF) ================

function fillCmbsSupContAndFilt() {
    var formID = window.top.formID;  // variavel global do browser filho

    // passa o id do form por parametro
    var strPars = new Array();
    strPars = '?';

    // formID
    strPars += 'formID=';
    strPars += escape(formID.toString());
    // empresaID
    strPars += '&empresaID=' + escape((sendJSMessage('AnyHtml', JS_COMBODATA, 'VALUE', null)).toString());
    // userID
    strPars += '&userID=' + escape((sendJSMessage('AnyHtml', JS_WIDEMSG, '__CURRUSERID', null)).toString());

    dsoCmbsContFilt.URL = SYS_ASPURLROOT + '/serversidegenEx/fillcmbscontfilters.aspx' + strPars;

    // Ultimo passo
    dsoCmbsContFilt.ondatasetcomplete = fillCmbsContAndFiltSup_DSC;
    dsoCmbsContFilt.Refresh();
}

function fillCmbsContAndFiltSup_DSC() {
    // Preenche os combos de contexto e filtro do controlbar superior

    // limpa os combos de contexto e filtros (sup e inf)
    cleanupSelInControlBar('sup', 1);
    cleanupSelInControlBar('sup', 2);
    cleanupSelInControlBar('inf', 2);

    var contDef = null;
    var firstCont = null;
    var filtContDef = null;
    var cmbContextData;

    while (!dsoCmbsContFilt.recordset.EOF) {
        if (dsoCmbsContFilt.recordset['ControlBar'].value == 'sup') {
            addOptionToSelInControlBar(dsoCmbsContFilt.recordset['ControlBar'].value,
                      dsoCmbsContFilt.recordset['Combo'].value,
                      dsoCmbsContFilt.recordset['Recurso'].value,
                      dsoCmbsContFilt.recordset['RecursoID'].value);
            if ((dsoCmbsContFilt.recordset['Combo'].value == 1) &&
                  (firstCont == null))
                firstCont = dsoCmbsContFilt.recordset['RecursoID'].value;
            if ((dsoCmbsContFilt.recordset['Combo'].value == 1) &&
                  (dsoCmbsContFilt.recordset['EhDefault'].value))
                contDef = dsoCmbsContFilt.recordset['RecursoID'].value;
            if ((dsoCmbsContFilt.recordset['Combo'].value == 2) &&
                  (dsoCmbsContFilt.recordset['EhDefault'].value))
                filtContDef = dsoCmbsContFilt.recordset['RecursoID'].value;
        }
        dsoCmbsContFilt.recordset.MoveNext();
    }

    // Seleciona o contexto default e correspondente filtro default
    // no controlBar superior 
    if (contDef != null)
        selOptByValueOfSelInControlBar('sup', 1, contDef);
    if (filtContDef != null)
        selOptByValueOfSelInControlBar('sup', 2, filtContDef);

    // Ajusta botao incluir pelo direito
    adjustControlBarSupByRights(1, 1);

    cmbContextData = getCmbCurrDataInControlBar('sup', 1);

    if (cmbContextData != null) {
        getCmbProprietariesList(true, cmbContextData[1]);
    }
    else {
        if (window.top.overflyGen.Alert('Imposs�vel terminar carregamento do form.\nInconsist�ncia nos direitos do usu�rio.') == 0)
            return null;

        glb_interfaceExTimer = window.setInterval('__closeCurrentTopWin()', 100, 'JavaScript');
    }
}

function __closeCurrentTopWin() {
    if (glb_interfaceExTimer != null) {
        window.clearInterval(glb_interfaceExTimer);
        glb_interfaceExTimer = null;
    }

    window.top.overflyGen.ForceDlgModalClose();
    window.top.beforeUnloadMsg = false;
    window.top.close();
}

function contextChanged(contextID) {
    // trava a interface
    lockInterface(true);

    // Parametros para o servidor
    var strPars = new String();
    strPars = '?';

    var formID = window.top.formID;  // variavel global do browser filho

    // formID
    strPars += 'formID=';
    strPars += escape(formID.toString());
    // contextoID
    strPars += '&contextID=';
    strPars += escape(contextID.toString());
    // empresaID
    strPars += '&empresaID=' + escape((sendJSMessage('AnyHtml', JS_COMBODATA, 'VALUE', null)).toString());
    // userID
    strPars += '&userID=' + escape((sendJSMessage('AnyHtml', JS_WIDEMSG, '__CURRUSERID', null)).toString());

    dsoCmbsContFilt.URL = SYS_ASPURLROOT + '/serversidegenEx/refillcmbsfilter.aspx' + strPars;

    // Ultimo passo
    dsoCmbsContFilt.ondatasetcomplete = refillCmbsFiltSup_DSC;
    dsoCmbsContFilt.Refresh();
}

function refillCmbsFiltSup_DSC() {
    // Preenche o combo de filtro do controlbar superior

    // limpa os combos de filtros (sup e inf)
    cleanupSelInControlBar('sup', 2);
    cleanupSelInControlBar('inf', 2);

    var filtContDef = null;
    var cmbContextData = getCmbCurrDataInControlBar('sup', 1);

    while (!dsoCmbsContFilt.recordset.EOF) {
        if ((dsoCmbsContFilt.recordset.Fields['ControlBar'].value == 'sup') &&
             (dsoCmbsContFilt.recordset.Fields['Combo'].value != 1)) {
            addOptionToSelInControlBar(dsoCmbsContFilt.recordset.Fields['ControlBar'].value,
                     dsoCmbsContFilt.recordset.Fields['Combo'].value,
                     dsoCmbsContFilt.recordset.Fields['Recurso'].value,
                     dsoCmbsContFilt.recordset.Fields['RecursoID'].value);
            if ((dsoCmbsContFilt.recordset.Fields['Combo'].value == 2) &&
                 (dsoCmbsContFilt.recordset.Fields['EhDefault'].value))
                filtContDef = dsoCmbsContFilt.recordset.Fields['RecursoID'].value;
        }
        dsoCmbsContFilt.recordset.MoveNext();
    }

    // Seleciona o filtro default no controlBar superior
    if (filtContDef != null)
        selOptByValueOfSelInControlBar('sup', 2, filtContDef);

    // Ajusta botao incluir pelo direito
    adjustControlBarSupByRights(1, 1);

    // no sup01, se tem combo de tipo de registro,
    // repreenche o combo de tipo de registro aqui
    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'fillTipRegStaticCombo()');

    // Dados do combo de proprietarios do pesqlist
    getCmbProprietariesList(false, cmbContextData[1]);

    // Movido em 10/10/2002
    // Este sendMessage informa o pesqlist que o segundo combo da barra superior
    // terminou de carregar
    //sendJSMessage(getHtmlId(), JS_DATAINFORM, 'EXECPESQ', null);
}

function fillCmbsFiltInf_DSC(pastaID) {
    if (dsoCmbsContFilt.recordset.fields.Count == 0)
        return false;

    // Preenche o combo de filtro inferior
    // limpa o combo de filtro inferior
    cleanupSelInControlBar('inf', 2);

    var filtPastaDef = null;

    if (!(dsoCmbsContFilt.recordset.BOF && dsoCmbsContFilt.recordset.EOF))
        dsoCmbsContFilt.recordset.MoveFirst();

    while (!dsoCmbsContFilt.recordset.EOF) {
        if ((dsoCmbsContFilt.recordset.Fields['ControlBar'].value == 'inf') &&
             (dsoCmbsContFilt.recordset.Fields['SubFormID'].value == pastaID)) {
            addOptionToSelInControlBar(dsoCmbsContFilt.recordset.Fields['ControlBar'].value,
                     dsoCmbsContFilt.recordset.Fields['Combo'].value,
                     dsoCmbsContFilt.recordset.Fields['Recurso'].value,
                     dsoCmbsContFilt.recordset.Fields['RecursoID'].value);
            if ((dsoCmbsContFilt.recordset.Fields['Combo'].value == 2) &&
                  (dsoCmbsContFilt.recordset.Fields['EhDefault'].value))
                filtPastaDef = dsoCmbsContFilt.recordset.Fields['RecursoID'].value;
        }

        dsoCmbsContFilt.recordset.MoveNext();
    }

    // Seleciona o filtro default
    // no controlBar inferior
    if (filtPastaDef != null)
        selOptByValueOfSelInControlBar('inf', 2, filtPastaDef);

    return true;
}

/********************************************************************
Retorna o estado inicial que deve ter um registro ao ser inserido

Parametros:
askedFrom           - 'sup' ou 'inf'.
String que identifica se a pergunta vem do sup ou do inf.

Retorno:
O estado inicial ou 1 se falha

********************************************************************/
function getInitialState(askedFrom) {
    var initialState = 1;
    var bmk = null;
    var dso = dsoCmbsContFilt;
    var contextOrFolderID = null;

    // traps iniciais
    if ((askedFrom == null) || (contextOrFolderID) ||
         (dso.recordset.BOF && dso.recordset.EOF))
        return initialState;

    askedFrom = askedFrom.toUpperCase();

    // parametro veio errado
    if (!((askedFrom == 'SUP') || (askedFrom == 'INF')))
        return initialState;

    if (!(dso.recordset.BOF || dso.recordset.EOF))
        bmk = dso.recordset.Bookmark();

    contextOrFolderID = getCmbCurrDataInControlBar(askedFrom, 1);

    // nao tem nada selecionado no combo do control bar
    if (contextOrFolderID == null)
        return initialState;

    dso.recordset.MoveFirst();

    dso.recordset.Find('RecursoID', contextOrFolderID[1]);

    if (!dso.recordset.EOF)
        if (dso.recordset.Fields['EstadoInicialID'].value != null)
        initialState = dso.recordset.Fields['EstadoInicialID'].value;

    if (bmk != null)
        dso.recordset.gotoBookmark(bmk);

    return initialState;
}

// FINAL DE CARREGAMENTO COMBOS CONTEXTO E FILTROS (SUP E INF) ======

// EVENTOS DISPARADOS PELOS CONTROLS BARS NOS ARQUIVOS ==============

var __dataArray;

function emulEvent(dataArray) {
    __dataArray = dataArray;

    if (__dataArray[0] && 'BTN') {
        switch (__dataArray[1]) {
            case ('PESQ'): eval(__btn_PESQ(__dataArray[2])); break;

            case ('LIST'): eval(__btn_LIST(__dataArray[2])); break;
            case ('DET'): eval(__btn_DET(__dataArray[2])); break;

            case ('RETRO'): eval(__btn_RETRO(__dataArray[2])); break;
            case ('AVANC'): eval(__btn_AVANC(__dataArray[2])); break;

            case ('INCL'): eval(__btn_INCL(__dataArray[2])); break;
            case ('ALT'): eval(__btn_ALT(__dataArray[2])); break;
            case ('EST'): eval(__btn_EST(__dataArray[2])); break;
            case ('EXCL'): eval(__btn_EXCL(__dataArray[2])); break;
            case ('OK'): eval(__btn_OK(__dataArray[2])); break;
            case ('CANC'): eval(__btn_CANC(__dataArray[2])); break;
            case ('REFR'): eval(__btn_REFR(__dataArray[2])); break;
            case ('ANT'): eval(__btn_ANT(__dataArray[2])); break;
            case ('SEG'): eval(__btn_SEG(__dataArray[2])); break;
            case ('UM'): eval(__btn_UM(__dataArray[2])); break;
            case ('DOIS'): eval(__btn_DOIS(__dataArray[2])); break;
            case ('TRES'): eval(__btn_TRES(__dataArray[2])); break;
            case ('QUATRO'): eval(__btn_QUATRO(__dataArray[2])); break;
            case ('CINCO'): eval(__btn_CINCO(__dataArray[2])); break;
            case ('SEIS'): eval(__btn_SEIS(__dataArray[2])); break;
            case ('SETE'): eval(__btn_SETE(__dataArray[2])); break;
            case ('OITO'): eval(__btn_OITO(__dataArray[2])); break;
            case ('NOVE'): eval(__btn_NOVE(__dataArray[2])); break;
            case ('DEZ'): eval(__btn_DEZ(__dataArray[2])); break;
            case ('ONZE'): eval(__btn_ONZE(__dataArray[2])); break;
            case ('DOZE'): eval(__btn_DOZE(__dataArray[2])); break;
            case ('TREZE'): eval(__btn_TREZE(__dataArray[2])); break;
            case ('QUATORZE'): eval(__btn_QUATORZE(__dataArray[2])); break;
            case ('QUINZE'): eval(__btn_QUINZE(__dataArray[2])); break;
            case ('DEZESSEIS'): eval(__btn_DEZESSEIS(__dataArray[2])); break;

            default:
                ;
        }
    }
    if (__dataArray[0] && 'CMB') {
        switch (__dataArray[1]) {
            case (1): eval(__combo_1(__dataArray[2], __dataArray[3], __dataArray[4], __dataArray[5])); break;
            case (2): eval(__combo_2(__dataArray[2], __dataArray[3], __dataArray[4], __dataArray[5])); break;

            default:
                ;
        }
    }

}
// FINAL DE EVENTOS DISPARADOS PELOS CONTROLS BARS NOS ARQUIVOS =====

/********************************************************************
Esta funcao habilita/desabilita todos os controles do html/asp corrente.

Parametros:
cmdLock         - true (desabilita) / false (habilita)
localAction     - qualquer parametro roda a funcao como local ao html
- e nao atua em grids
- se null roda como modal do sistema e atua em grids

Retornos:
Nenhum
********************************************************************/
function lockControls(cmdLock, localAction) {
    var ret = true;

    // a biblioteca js_modalwin.js tem prioridade na chamada
    // desta funcao via a funcao lockInterface()
    // sobre qualquer chamada local (a nivel de form)

    // Executa se:
    // 1. Local trava se esta destravado
    // 2. Modal trava se local travou ou se esta destravado
    // 3. Local destrava se foi travado pela local
    // 4. Modal destrava se foi travado pela modal

    // 1. Local trava se esta destravado
    if ((cmdLock == true) && (localAction != null) && (__MODALLOCKED == false) && (__HTMLISLOCKED == false))
        ret = false;

    // 2. Modal trava se local travou ou se esta destravado
    else if (((cmdLock == true) && (localAction == null)) && ((__HTMLISLOCKED == true) || ((__MODALLOCKED == false) && (__HTMLISLOCKED == false))))
        ret = false;

    // 3. Local destrava se foi travado pela local e modal esta destravada
    else if (((cmdLock == false) && (localAction != null) && (__MODALLOCKED == false) && (__HTMLISLOCKED == true)))
        ret = false;

    // 4. Modal destrava se foi travado pela modal
    else if (((cmdLock == false) && (localAction == null) && (__MODALLOCKED == true)))
        ret = false;

    if (ret)
        return;

    // ultimo passe da modal
    if (localAction == null)
        __MODALLOCKED = cmdLock;

    // estado da interface
    if (localAction != null)
        __HTMLISLOCKED = cmdLock;

    // trava ou destrava a barra de botoes de acesso rapido
    if (cmdLock)
        lockFastButtons();
    else
        unlockFastButtons();

    __ELEMCOUNT = 0;
    __ELEMCOUNTM = 0;
    var i, j;
    var elem;

    if (localAction == null) {
        for (i = 0; i < window.document.all.length; i++) {
            elem = window.document.all.item(i);

            // controles ActiveX
            // o grid
            if (elem.tagName.toUpperCase() == 'OBJECT') {
                if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072') {
                    adjustActiveXControlM(elem, cmdLock);
                }
            }

            // demais elementos        
            if (elem.tagName.toUpperCase() == 'INPUT') {
                adjustElementM(elem, cmdLock);
            }
            else if (elem.tagName.toUpperCase() == 'SELECT') {
                adjustElementM(elem, cmdLock);
            }
            else if (elem.tagName.toUpperCase() == 'TEXTAREA') {
                adjustElementM(elem, cmdLock);
            }
        }
    }
    else {
        for (i = 0; i < window.document.all.length; i++) {
            elem = window.document.all.item(i);

            // controles ActiveX
            // o grid
            /*
            // trecho de codigo removido quanto a funcao e invocada
            // em modo local
            if ( elem.tagName.toUpperCase() == 'OBJECT' )
            {
            if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072')
            {
            adjustActiveXControl(elem, cmdLock);                
            }
            }
            */
            // demais elementos        
            if (elem.tagName.toUpperCase() == 'INPUT') {
                adjustElement(elem, cmdLock);
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                if (elem.type == 'image') {
                    if (cmdLock == true)
                        elem.src = glb_LUPA_IMAGES[1].src;
                    else
                        elem.src = glb_LUPA_IMAGES[0].src;
                }
                // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            }
            else if (elem.tagName.toUpperCase() == 'SELECT') {
                adjustElement(elem, cmdLock);
            }
            else if (elem.tagName.toUpperCase() == 'TEXTAREA') {
                //if ( window.top.overflyGen.Alert(elem.id + ' -> cmdLock = ' + cmdLock + ' -> disabled = ' + elem.disabled ) == 0 )
                //  return null;
                adjustElement(elem, cmdLock);
                //if ( window.top.overflyGen.Alert(elem.id + ' -> disabled = ' + elem.disabled ) == 0 )
                //  return null;
            }
        }
    }
}

// FUNCOES DA LOCKCONTROLS, NAO USE DIRETAMENTE *********************

/********************************************************************
Esta funcao trava ou destrava um elemento de html/asp da interface
do browser filho corrente.

Parametros:
elem            - elemento corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustElement(elem, cmdLock) {
    // cmdLock == true => desabilitar elemento

    if (cmdLock == true) {
        // desabilita o elemento
        if ((elem.tagName.toUpperCase() == 'TEXTAREA') ||
             ((elem.tagName.toUpperCase() == 'INPUT') &&
               (elem.type.toUpperCase() == 'TEXT'))) {
            // salva status corrente
            __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.readOnly);
            elem.readOnly = true;
        }
        else {
            // salva status corrente
            __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.disabled);
            elem.disabled = true;
        }

        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        if ((elem.tagName.toUpperCase() == 'TEXTAREA') ||
             ((elem.tagName.toUpperCase() == 'INPUT') &&
               (elem.type.toUpperCase() == 'TEXT')))
            window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).readOnly = (__ELEMSTATE[__ELEMCOUNT])[1];
        else
            window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).disabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}


/********************************************************************
Esta funcao trava ou destrava um elemento ACTIVEX CONTROL
de html/asp da interface do browser filho corrente.

Parametros:
elem            - elemento ACTIVEX CONTROL corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustActiveXControl(elem, cmdLock) {
    // cmdLock == true => desabilitar elemento
    if (cmdLock == true) {
        // salva status corrente
        __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.Enabled);
        // desabilita o elemento
        elem.Enabled = false;
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).Enabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Esta funcao trava ou destrava um elemento de html/asp da interface
do browser filho corrente.

Parametros:
elem            - elemento corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustElementM(elem, cmdLock) {
    // cmdLock == true => desabilitar elemento

    if (cmdLock == true) {
        // salva status corrente
        __ELEMSTATEM[__ELEMCOUNTM] = new Array(elem.id, elem.disabled);
        // desabilita o elemento
        elem.disabled = true;
        // incrementa contador de elementos
        __ELEMCOUNTM++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        window.document.getElementById((__ELEMSTATEM[__ELEMCOUNTM])[0]).disabled = (__ELEMSTATEM[__ELEMCOUNTM])[1];
        // incrementa contador de elementos
        __ELEMCOUNTM++;
    }
}

/********************************************************************
Esta funcao trava ou destrava um elemento ACTIVEX CONTROL
de html/asp da interface do browser filho corrente.

Parametros:
elem            - elemento ACTIVEX CONTROL corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustActiveXControlM(elem, cmdLock) {
    // cmdLock == true => desabilitar elemento
    if (cmdLock == true) {
        // salva status corrente
        __ELEMSTATEM[__ELEMCOUNTM] = new Array(elem.id, elem.Enabled);
        // desabilita o elemento
        elem.Enabled = false;
        // incrementa contador de elementos
        __ELEMCOUNTM++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        window.document.getElementById((__ELEMSTATEM[__ELEMCOUNTM])[0]).Enabled = (__ELEMSTATEM[__ELEMCOUNTM])[1];
        // incrementa contador de elementos
        __ELEMCOUNTM++;
    }
}

// FINAL DE FUNCOES DA LOCKCONTROLS, NAO USE DIRETAMENTE ************

/********************************************************************
Esta funcao mostra ou esconde/habilita ou desabilita controles
em funcao de atributos especiais
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function showEnableControls(aAttrib, cmdShow, cmdEnable) {
    var i, j, elem;
    var coll = window.document.all;
    var applyConditions;

    for (i = 0; i < coll.length; i++) {
        applyConditions = true;

        elem = coll.item(i);

        for (j = 0; j < aAttrib.length; j++) {
            applyConditions = applyConditions &&
                             (aAttrib[j][1] == elem.getAttribute(aAttrib[j][0], 1));
        }

        if (applyConditions) {
            // habilita ou nao
            elem.disabled = !cmdEnable;

            // mostra ou nao
            elem.style.visibility = ((cmdShow) ? 'visible' : 'hidden');
        }

    }
}

/********************************************************************
Esta funcao mostra ou esconde/habilita ou desabilita controles
em funcao do atributo especial fnShowHide
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function evalShowHideCtlsInDivs(divRef) {
    // loop nos controles filhos do div
    var coll = divRef.children;
    var i;
    var ctrl;

    for (i = 0; i < coll.length; i++) {
        ctrl = coll.item(i);
        if (ctrl.getAttribute('fnShowHide', 1))
            eval(ctrl.getAttribute('fnShowHide', 1));
    }


}

/********************************************************************
Verifica digitacao em campo texto (input type text) numerico,
linkado a campo de tabela
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function verifyNumericEnter() {
    /* Tabela de teclas
    08 --> backspace
    09 --> Tab
    35 --> end
    36 --> Home
    37 --> seta p/ esquerda
    39 --> seta p/ esquerda
    44 --> virgula
    46 --> ponto
    */

    var nextLenInt;
    var nextLenDec;
    var nextVal = 0;
    var minMax;
    var nPrecision;
    var nScale;
    var signalHouse = 0;

    var ctrlValue;
    var ctrl;
    var idControl;

    ctrl = this;
    idControl = ctrl.id;

    var dsoName = ctrl.dataSrc.substr(1, ctrl.dataSrc.length - 1);
    var dso = eval(dsoName);
    var fld = ctrl.dataFld;

    // Se int ou bigint
    if ((dso.recordset[fld].type == 3) || (dso.recordset[fld].type == 20)) {
        nPrecision = ctrl.getAttribute('thePrecision', 1);
        nScale = ctrl.getAttribute('theScale', 1);
    }
    // Se numerico
    else {
        nPrecision = dso.recordset[fld].precision;
        nScale = dso.recordset[fld].numericScale;
    }

    //===============================================================
    // O maxLength para numeros positivos ou negativos
    if ((ctrl.getAttribute('posMaxLen', 1) == null) ||
         (ctrl.getAttribute('posMaxLen', 1) == ''))
        ctrl.setAttribute('posMaxLen', ctrl.maxLength, 1);

    ctrlValue = trimStr(ctrl.value);

    // ajusta o maxLength conforme o valor do controle seja positivo ou negativo
    if (isNaN(ctrlValue)) {
        ctrl.maxLength = ctrl.getAttribute('posMaxLen', 1);
    }
    else if (ctrlValue == '') {
        ctrl.maxLength = ctrl.getAttribute('posMaxLen', 1);
    }
    else if (ctrlValue.substr(0, 1) == '-') {
        ctrl.maxLength = ctrl.getAttribute('posMaxLen', 1) + 1;
    }
    else {
        ctrl.maxLength = ctrl.getAttribute('posMaxLen', 1);
    }


    //===============================================================

    // resolve se aceita digitacao de sinal negativo.
    // apenas se o conteudo do controle esta vazio
    if ((event.keyCode == 45) && (ctrl.value == '')) {
        // teste de limites
        minMax = ctrl.getAttribute('minMax', 1);
        if (minMax != null) {
            if (minMax[0] < 0)
                return true;
            else
                return false;
        }
        // por default nao aceita digitar negativo
        return false;
    }

    // resolve se aceita digitacao de ponto ou virgula
    if ((event.keyCode == 44) || (event.keyCode == 46))
        if (nScale <= 0)
        return false;

    // nao aceita digitacao diferente de numero ou ponto ou virgula
    if (!(((event.keyCode >= 48) && (event.keyCode <= 57)) ||
          (event.keyCode == 44) || (event.keyCode == 46)))
        return false;

    // o novo valor que tera o numero se aceitar o caracter digitado
    if ((event.keyCode >= 48) && (event.keyCode <= 57)) {
        nextVal = parseFloat(ctrl.value + (event.keyCode - 48).toString());

        // teste de limites
        minMax = ctrl.getAttribute('minMax', 1);
        if (minMax != null) {
            if ((nextVal < minMax[0]) ||
                 (nextVal > minMax[1]))
                return false;
        }
    }
    else
        nextVal = parseFloat(ctrl.value);

    // sai fora se nao e numero - resolve a digitacao de deadkey
    // no primeiro caracter
    if (isNaN(nextVal))
        return false;

    // tamanho da parte inteira se aceitar o caracter
    if ((nPrecision - nScale) > 0) {
        if ((nextVal.toString()).indexOf('.') < 0)
            nextLenInt = (nextVal.toString()).length;
        else
            nextLenInt = (nextVal.toString()).indexOf('.');
    }
    else
        nextLenInt = (nextVal.toString()).length;

    //===============================================================
    // O maxLength para numeros positivos ou negativos
    if (((nextVal.toString()).indexOf('-') == 0) && (nextLenInt > 0))
        nextLenInt--;
    //===============================================================

    // tamanho da parte decimal se aceitar o caracter
    if (nScale > 0) {
        if ((nextVal.toString()).indexOf('.') < 0)
            nextLenDec = 0;
        else
            nextLenDec = (nextVal.toString()).length -
                         (nextVal.toString()).indexOf('.') - 1;
    }
    else
        nextLenDec = 0;

    // prossegue se a parte inteira e a parte decimal estao dentro da faixa
    // leva em conta numero negativo
    if (nextVal < 0)
        signalHouse = 1;

    if (!((nextLenInt <= (nPrecision - nScale)) && (nextLenDec <= nScale)))
        return false;

    // Digitacao ***

    // 1. aceita digitar setas
    if ((event.keyCode == 39) || (event.keyCode == 37) || (event.keyCode == 9) ||
        (event.keyCode == 8) || (event.keyCode == 35) || (event.keyCode == 36)) {
        return true;
    }
    // 2. digitacao de virgula ou ponto
    else if ((event.keyCode == 44) || (event.keyCode == 46)) {
        // so aceita um ponto
        // se for pressionado virgula aceita trocando por ponto
        if (((ctrl.value).indexOf('.') < 0) && (nScale > 0)) {
            event.keyCode = 46;
            return true;
        }
        else
            return false;
    }
    // 3. digitacao de digitos
    else if ((event.keyCode >= 48) && (event.keyCode <= 57))
        return true;
    else
        return false;
}

/********************************************************************
Refaz os arrays de controles do lock control, para inserir os clones
no array

Parametros:
nenhum

Retornos:
nenhum
********************************************************************/
function redoLockControlsArrays() {
    var tempArray = new Array();
    var elem;
    var i, j;

    // Cria um array temporario dos contoles que interessam e seus estados
    j = 0;
    for (i = 0; i < window.document.all.length; i++) {
        elem = window.document.all.item(i);

        if (elem.tagName.toUpperCase() == 'OBJECT') {
            if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072') {
                tempArray[j] = new Array(null, null);
                tempArray[j][0] = elem.id;
                tempArray[j][1] = elem.Enabled;
                j++;
            }
        }
        else if (elem.tagName.toUpperCase() == 'INPUT') {
            tempArray[j] = new Array(null, null);
            tempArray[j][0] = elem.id;
            tempArray[j][1] = elem.disabled;
            j++;
        }
        else if (elem.tagName.toUpperCase() == 'SELECT') {
            tempArray[j] = new Array(null, null);
            tempArray[j][0] = elem.id;
            tempArray[j][1] = elem.disabled;
            j++;
        }
        else if (elem.tagName.toUpperCase() == 'TEXTAREA') {
            tempArray[j] = new Array(null, null);
            tempArray[j][0] = elem.id;
            tempArray[j][1] = elem.disabled;
            j++;
        }
    }

    // lockControls local
    // Altera no array temporario os estados dos controles
    // existentes no array __ELEMSTATEM
    j = 0;
    for (i = 0; i < tempArray.length; i++) {
        if (j == __ELEMSTATEM.length)
            break;

        if (tempArray[i][0] == __ELEMSTATEM[j][0]) {
            tempArray[i][1] = __ELEMSTATEM[j][1];
            j++;
        }
    }

    // Copia os elementos do array temporario para
    // o array __ELEMSTATEM
    for (i = 0; i < tempArray.length; i++) {
        if (i == __ELEMSTATEM.length)
            __ELEMSTATEM[i] = new Array(null, null);

        __ELEMSTATEM[i][0] = tempArray[i][0];
        __ELEMSTATEM[i][1] = tempArray[i][1];
    }

    __ELEMCOUNTM = tempArray.length;

    // lockControls do lockInterface
    // Altera no array temporario os estados dos controles
    // existentes no array __ELEMSTATE
    j = 0;
    for (i = 0; i < tempArray.length; i++) {
        if (j == __ELEMSTATE.length)
            break;

        if (tempArray[i][0] == __ELEMSTATE[j][0]) {
            tempArray[i][1] = __ELEMSTATE[j][1];
            j++;
        }
    }

    // Copia os elementos do array temporario para
    // o array __ELEMSTATE
    for (i = 0; i < tempArray.length; i++) {
        if (i == __ELEMSTATE.length)
            __ELEMSTATE[i] = new Array(null, null);

        __ELEMSTATE[i][0] = tempArray[i][0];
        __ELEMSTATE[i][1] = tempArray[i][1];
    }

    __ELEMCOUNT = tempArray.length;

}

/********************************************************************
Coloca foco no primeiro campo liberado do arquivo.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setFocus1Field() {
    var coll = window.document.all;
    var i;
    var tName;

    for (i = 0; i < coll.length; i++) {

        // Sempre pula txtNulo
        if (coll.item(i).id == 'txtNulo')
            continue;

        // Da foco em INPUT, SELECT, TEXTAREA
        tName = coll.item(i).tagName.toUpperCase();
        if ((tName == 'INPUT') || (tName == 'SELECT') || (tName == 'TEXTAREA'))
        {
            if ((coll.item(i).disabled == false) &&
                 (coll.item(i).currentStyle.visibility == 'visible'))
            {
                if (tName != 'SELECT') {
                    if (coll.item(i).readOnly == false) {
                        coll.item(i).focus();
                        break;
                    }
                }
                else
                {
                    coll.item(i).focus();
                        break;
                }
            }
            else if ((coll.item(i).disabled == false) &&
                 (coll.item(i).currentStyle.visibility == 'inherit')) 
            {
                if (coll.item(i).parentElement.currentStyle.visibility == 'visible') {

                    if (tName != 'SELECT') {
                        if (coll.item(i).readOnly == false) {
                            coll.item(i).focus();
                            break;
                        }
                    }
                    else {
                        coll.item(i).focus();
                        break;
                    }
                }
            }
        }
    }
}

/********************************************************************
Clona todos os controles textos necessarios.
Limita quantidade de caracteres digitados em todos os controles input
tipo text e textarea.
Seta evento de digitacao onkeypress se for controle com conteudo numerico.
Chamada a primeira vez que o dso corrente e invocado. Isto ocorre quando
vem do pesqlist para incluir ou mostrar registro.
Colocada a chamada na funcao finalDSOsCascade().

Limita quantidade de caracteres digitados para todos
controles textos e textarea.
Seta eventos onkeypress para controles textos de data.
Guarda id dos campos que nao sao input nem textarea
mas sao obrigatorios e estao linkados ao dso    
Ao final reconstroi os arrays do lockControls e do lockInterface

Parametros: 
dso     - o corrente dso

Retorno:
false se nao executa, caso sucesso retorna true
********************************************************************/
function maxLenOnKeyPressSetArrays(dso) {
    // Trap, nao tem dso ativo
    if (dso == null)
        return false;

    // Trap se o dso nao contem recordset
    if (dso.recordset.Fields.Count == 0)
        return false;

    // Trap se o dso esta com o mesmo select
    if (glb_lastSelect == dso.SQL)
        return false;
    else
        glb_lastSelect = dso.SQL;

    // Variaveis
    var coll;
    var aTextAreaToLimitInput = new Array();
    var i, j, k;
    var elem;
    var fGoAhead = false;
    // Array de campos requeridos e seus respectivos labels
    var aRequiredFieldsIDs = new Array();
    // Array dos campos de tipo data
    var aDateFieldsIDs = new Array();
    var tblName = '';
    var fldlen = 0;

    var allowDBNullAndReadOnly;

    if (window.document.getElementById('txtRegistroID') != null) {
        // por padrao e colocado desahabilitado
        window.document.getElementById('txtRegistroID').disabled = true;
    }

    if (window.document.getElementById('txtEstadoID') != null) {
        // por padrao e colocado desahabilitado
        window.document.getElementById('txtEstadoID').disabled = true;
    }

    coll = window.document.getElementsByTagName('INPUT');

    // Loop nos controles input. Atua apenas nos textos e textarea.
    // Se o controles esta linkado a um dso, clona se
    // e obrigatorio, numerico ou de data
    j = 0;
    for (i = 0; i < coll.length; i++) {
        elem = coll.item(i);
        // Prossegue se o controle nao esta linkado a este dso
        if (((typeof (elem.dataSrc)).toUpperCase() != 'STRING') ||
             ((elem.dataSrc).substr(1) != dso.id))
            continue;

        // Guarda referencia aos controles type textarea
        if (elem.type.toUpperCase() == 'TEXTAREA')
            aTextAreaToLimitInput[++j] = elem;

        // Prossegue porque nao e controle type text
        if (elem.type.toUpperCase() != 'TEXT')
            continue;

        // Prossegue se o campo ao qual o controle esta linkado
        // nao existe no dso.
        fGoAhead = false;
        for (k = 0; k < dso.recordset.Fields.Count; k++) {
            if (dso.recordset.Fields[k].Name == elem.dataFld) {
                fGoAhead = true;
                break;
            }
        }

        if (fGoAhead != true)
            continue;

        // Atua no controle se e campo obrigatorio,
        // numerico ou de data e esta linkado ao corrente dso

        // Marca os atributos AllowDBNull e IsReadOnly.
        allowDBNullAndReadOnly = attributesValues['AllowDBNull'] | attributesValues['IsReadOnly'];

        // 1. Campo obrigatorio
        if ((dso.recordset.Fields[elem.dataFld].Attributes & allowDBNullAndReadOnly) == 0) {
            // se o campo e numerico int e bigint
            if ((dso.recordset.Fields[elem.dataFld].Type == 131) ||
                 (dso.recordset.Fields[elem.dataFld].Type == 14) ||
                 (dso.recordset.Fields[elem.dataFld].Type == 3) ||
                 (dso.recordset.Fields[elem.dataFld].Type == 20))
            // linka evento onkeypress
                window.document.getElementById(elem.id).onkeypress =
                    verifyNumericEnter;

            // se o campo e de data
            if (dso.recordset.Fields[elem.dataFld].Name.substr(0, 2) == 'V_') {
                // Forca campo data para maximo de 10 caracteres
                window.document.getElementById(elem.id).maxLength = 10;

                // Guarda id no array aDateFieldsIDs
                aDateFieldsIDs[aDateFieldsIDs.length] = elem.id;
            }
            else {
                // seta atributo para eventos de paste
                //elem.setAttribute('verifyNumPaste', 1, 1);
                //window.document.getElementById(elem.id).setAttribute('verifyNumPaste', 1, 1);

                // int e big int
                if ((dso.recordset.Fields[elem.dataFld].Type == 3) ||
					 (dso.recordset.Fields[elem.dataFld].Type == 20)) {
                    // Nome da tabela
                    tblName = '';
                    fldlen = 0;

                    try {
                        tblName = dso.recordset.Fields[elem.dataFld].table;
                        fldlen = sendJSMessage('', JS_FIELDINTLEN, tblName, elem.dataFld);
                    }
                    catch (e) {
                        fldlen = 0;
                    }

                    elem.setAttribute('thePrecision', fldlen, 1);
                    elem.setAttribute('theScale', 0, 1);

                    window.document.getElementById(elem.id).setAttribute('thePrecision', fldlen, 1);
                    window.document.getElementById(elem.id).setAttribute('theScale', 0, 1);
                }
            }

            // Guarda id no array aRequiredFieldsIDs
            aRequiredFieldsIDs[aRequiredFieldsIDs.length] = elem.id;

            continue;
        }

        // 2. Campo numerico, int t big int
        if ((dso.recordset.Fields[elem.dataFld].Type == 131) ||
             (dso.recordset.Fields[elem.dataFld].Type == 14) ||
             (dso.recordset.Fields[elem.dataFld].Type == 3) ||
             (dso.recordset.Fields[elem.dataFld].Type == 20)) {
            // linka evento onkeypress
            window.document.getElementById(elem.id).onkeypress =
                verifyNumericEnter;

            // seta atributo para eventos de paste
            elem.setAttribute('verifyNumPaste', 1, 1);
            window.document.getElementById(elem.id).setAttribute('verifyNumPaste', 1, 1);

            // seta atributos de precision e scale
            // numeric
            if ((dso.recordset.Fields[elem.dataFld].Type == 131) ||
				 (dso.recordset.Fields[elem.dataFld].Type == 14)) {
                elem.setAttribute('thePrecision', dso.recordset.Fields[elem.dataFld].Precision, 1);
                elem.setAttribute('theScale', dso.recordset.Fields[elem.dataFld].NumericScale, 1);

                window.document.getElementById(elem.id).setAttribute('thePrecision', dso.recordset.Fields[elem.dataFld].Precision, 1);
                window.document.getElementById(elem.id).setAttribute('theScale', dso.recordset.Fields[elem.dataFld].NumericScale, 1);
            }
            // int e big int
            else if ((dso.recordset.Fields[elem.dataFld].Type == 3) ||
				      (dso.recordset.Fields[elem.dataFld].Type == 20)) {
                // Nome da tabela
                tblName = '';
                fldlen = 0;

                try {
                    //tblName = dso.recordset.Fields[elem.dataFld].properties(1).value;
                    tblName = dso.recordset.Fields[elem.dataFld].table;
                    fldlen = sendJSMessage('', JS_FIELDINTLEN, tblName, elem.dataFld);
                }
                catch (e) {
                    fldlen = 0;
                }

                elem.setAttribute('thePrecision', fldlen, 1);
                elem.setAttribute('theScale', 0, 1);

                window.document.getElementById(elem.id).setAttribute('thePrecision', fldlen, 1);
                window.document.getElementById(elem.id).setAttribute('theScale', 0, 1);
            }

            continue;
        }

        // 3. Campo de data
        if (dso.recordset.Fields[elem.dataFld].Name.substr(0, 2) == 'V_') {
            // Forca campo data para maximo de 10 caracteres
            window.document.getElementById(elem.id).maxLength = 10;

            // Guarda id no array aDateFieldsIDs
            aDateFieldsIDs[aDateFieldsIDs.length] = elem.id;

            continue;
        }
    }

    // Limitamos quantidade de caracteres digitados para todos
    // os controles textarea
    for (i = 0; i < aTextAreaToLimitInput; i++) {
        elem = aTextAreaToLimitInput[i];

        // Prossegue se o campo ao qual o controle esta linkado
        // nao existe no dso.
        fGoAhead = false;
        for (k = 0; k < dso.recordset.Fields.Count; k++) {
            if (dso.recordset.Fields[k].Name == elem.dataFld) {
                fGoAhead = true;
                break;
            }
        }

        if (fGoAhead != true)
            continue;

        // Textarea e sempre varchar    
        elem.maxLength = dso.recordset.Fields[elem.dataFld].size;
    }

    // Guarda id dos campos que nao sao input nem textarea
    // mas sao obrigatorios e estao linkados ao dso
    coll = window.document.all;

    for (i = 0; i < coll.length; i++) {
        elem = coll.item(i);

        // Prossegue se o controle nao esta linkado a este dso
        if (((typeof (elem.dataFld)).toUpperCase() != 'STRING') ||
             ((elem.dataSrc).substr(1) != dso.id))
            continue;

        // Trap, nao opera tags INPUT e TEXTAREA
        if ((elem.tagName.toUpperCase() == 'INPUT') ||
             (elem.tagName.toUpperCase() == 'TEXTAREA'))
            continue;

        // Trap nao opera tags que nao tem a propriedade dataFld
        if (((typeof (elem.dataFld)).toUpperCase() != 'STRING') ||
             (elem.dataFld == ''))
            continue;

        for (k = 0; k < dso.recordset.Fields.Count; k++) {
            if (dso.recordset.Fields[k].Name == elem.dataFld) {
                if (~dso.recordset.Fields[elem.dataFld].Attributes & 64)
                    aRequiredFieldsIDs[aRequiredFieldsIDs.length] = elem.id;

                break;
            }
        }

    }

    // Sorteia o array aRequiredFieldsIDs conforme a disposicao
    // dos controles na interface
    if (aRequiredFieldsIDs.length != 0)
        aRequiredFieldsIDs.sort(sortReqFlds__);

    // Guarda array dos controles requeridos e controles de data
    dso.setAttribute('aRequiredFieldsIDs', aRequiredFieldsIDs, 1);
    dso.setAttribute('aDateFieldsIDs', aDateFieldsIDs, 1);

    // Reconstroi os arrays do lockControls e do lockInterface
    redoLockControlsArrays();

    // Rearranja onfocus dos campos
    setupEventOnFocusInTextFields();

    return true;
}

/********************************************************************
Funcao que define a forma de sortear o array de campos requeridos

Parametros: 

Retorno:
A negative value if the first argument passed is less than the second argument. 
Zero if the two arguments are equivalent. 
A positive value if the first argument is greater than the second argument. 
********************************************************************/
function sortReqFlds__() {
    if (arguments.length != 2)
        return 0;

    var i, coll, elem, retVal = 0;

    // Loop na interface
    coll = window.document.all;
    for (i = 0; i < coll.length; i++) {
        elem = coll.item(i);
        if (elem.id == arguments[0]) {
            retVal = -1;
            break;
        }
        else if (elem.id == arguments[1]) {
            retVal = 1;
            break;
        }
    }

    return retVal;
}
// FUNCOES DE DIREITOS **********************************************

/********************************************************************
Esta funcao retorna o valor de um bit de direito do usuario logado.
Este dado esta no dsoCmbsContFilt de todos os pesqlist

Parametros:
sCtrlBar    - posicao do control bar: 'sup' or 'inf'
sDirBit     - o nome do bit a ser consultado
nID         - opcional. Forca retornar o direito de uma pasta/contexto
que nao necessariamente a pasta/contexto corrente
no combo de pastas/contextos

Retorno:
o valor do bit de direito como 1 ou 0
********************************************************************/
function getCurrRightValue(sCtrlBar, sDirBit, nID) {
    var dsoBmk = null;
    var dso = dsoCmbsContFilt;
    var retVal = 0;
    var aItemSelected;
    var nRecursoID;

    sCtrlBar = sCtrlBar.toUpperCase();
    if (!((sCtrlBar == 'SUP') || (sCtrlBar == 'INF')))
        return retVal;

    // O dso nao foi preenchido
    if (dso.recordset.Fields.Count == 0)
        return retVal;

    // nao tem registros, retorna nao ter direito
    if (dso.recordset.BOF && dso.recordset.EOF)
        return retVal;

    // tem registros, guarda o bookmark se o dso nao esta no BOF nem o EOF    
    if (!(dso.recordset.BOF || dso.recordset.EOF))
        dsoBmk = dso.recordset.Bookmark();

    // faremos aqui nossa pesquisa
    dso.recordset.MoveFirst();

    // pega o value do contexto ou da pasta atual, em funcao do CtrlBar
    if ((nID == null) || ((typeof (nID)).toUpperCase() == 'UNDEFINED')) {
        aItemSelected = getCmbCurrDataInControlBar(sCtrlBar, 1);

        // o combo de pastas ou de contextos esta vazio
        if (aItemSelected == null)
            return retVal;

        nRecursoID = aItemSelected[1];
    }
    else
        nRecursoID = nID;

    dso.recordset.setFilter('ControlBar = ' + '\'' + sCtrlBar + '\'' + ' AND Combo = ' +
                           '\'' + 1 + '\'' + ' AND RecursoID = ' +
                           '\'' + nRecursoID + '\'');

    // verifica se o objeto n�o est� no EOF
    if (!dso.recordset.EOF && (dso.recordset.Fields[sDirBit].value != null)) {
        // obtem o bit de direito no contexto ou na pasta atual
        if (dso.recordset.Fields[sDirBit].value == true)
            retVal = 1;
        else
            retVal = 0;
    }

    dso.recordset.setFilter('');

    // volta o ponteiro do dso no registro anterior a funcao, se temos
    // Bookmark
    if (dsoBmk != null)
        dso.recordset.gotoBookmark(dsoBmk);

    return retVal;
}

// FINAL DE FUNCOES DE DIREITOS *************************************

// AJUSTAGEM DOS CONTROLS BARS EM FUNCAO DOS DIREITOS ***************

/********************************************************************
Ajusta os botoes de: incluir, alterar, excluir
O estado dos botoes dependem de:
incluir     - depende so do contexto corrente
alterar     - depende do contexto corrente e dos campos de controle
- do registro
excluir     - depende do contexto corrente e dos campos de controle
- do registro

Parametros: 
prop1 e prop2   - Opcional, so usado no js_detailsup.js, quando vem da
lista para o detalhe ou quando esta paginando
ou quando incluiu um registro novo. Nos demais casos
passar prop1 = 1 e prop2 = 1

Retorno:
nenhum

Nota: Funcoes que controlam a liberacao dos botoes das barras
lockBtnInCtrlBar(theBar, btnNumber, cmdLock)
lockEspecBtnInCtrlBar(theBar, btnNumber, cmdLock)
unlockBtnsInCtrlBar(theBar) (DO FRAMEWORK)
refreshCtrlBar(theBar)
********************************************************************/
function adjustControlBarSupByRights(prop1, prop2) {
    // destrava todos os botoes por garantia
    unlockBtnsInCtrlBar('sup');

    var theRight1, theRigth2;
    var treatBtnRight;
    var i;

    treatBtnRight = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                      ('glb_BtnsIncAltEstExcl'));

    // trata os botoes em funcao do direito do usuario    
    // botao incluir                  
    if (treatBtnRight[0] == null) {
        theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'I' + '\'' + ')'));
        // trava o botao
        if (theRight1 == 0)
            lockBtnInCtrlBar('sup', 3, true);
        // destrava o botao
        else if (theRight1 == 1)
            lockBtnInCtrlBar('sup', 3, false);
        // trava o botao pois o direito do usuario depende dos direitos no registro
        else
            lockBtnInCtrlBar('sup', 3, true);
    }

    // botao alterar
    if (treatBtnRight[1] == null) {
        theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
        theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
        // trava o botao
        if (theRight1 == 0 && theRight2 == 0)
            lockBtnInCtrlBar('sup', 4, true);
        // destrava o botao
        else if (theRight1 == 1 && theRight2 == 1)
            lockBtnInCtrlBar('sup', 4, false);
        // trava o botao pois o direito do usuario depende dos direitos no registro
        else
            lockBtnInCtrlBar('sup', 4, true);
    }

    // botao excluir
    if (treatBtnRight[3] == null) {
        theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E1' + '\'' + ')'));
        theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E2' + '\'' + ')'));
        // trava o botao
        if (theRight1 == 0 && theRight2 == 0)
            lockBtnInCtrlBar('sup', 6, true);
        // destrava o botao
        else if (theRight1 == 1 && theRight2 == 1)
            lockBtnInCtrlBar('sup', 6, false);
        // trava o botao pois o direito do usuario depende dos direitos no registro
        else
            lockBtnInCtrlBar('sup', 6, true);
    }

    for (i = 1; i <= NUM_BTNS_ESPECIFICS; i++) {
        // botao especifico i
        theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		              ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B' + i.toString() + 'A1' + '\'' + ')'));
        theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		              ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B' + i.toString() + 'A2' + '\'' + ')'));
        // trava o botao
        if (theRight1 == 0 && theRight2 == 0)
            lockEspecBtnInCtrlBar('sup', i, true);
        // destrava o botao
        else if (theRight1 == 1 && theRight2 == 1)
            lockEspecBtnInCtrlBar('sup', i, false);
        // trava o botao pois o direito do usuario depende dos direitos no registro
        else
            lockEspecBtnInCtrlBar('sup', i, true);
    }

    // trata barra em funcao do registro
    if (!((prop1 == null) || (prop2 == null))) {
        // botao alterar
        if (treatBtnRight[1] == null) {
            theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
            theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
            if (!(((theRight1 == 1) && (theRight2 == 1)) ||
                 ((theRight1 == 0) && (theRight2 == 0)))) {
                if (((theRight1 * prop1) || (theRight2 * prop2)) && 1)
                // habilita
                    lockBtnInCtrlBar('sup', 4, false);
                else
                // nao habilita
                    lockBtnInCtrlBar('sup', 4, true);
            }
        }

        // botao excluir
        if (treatBtnRight[3] == null) {
            theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E1' + '\'' + ')'));
            theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'E2' + '\'' + ')'));
            if (!(((theRight1 == 1) && (theRight2 == 1)) ||
                 ((theRight1 == 0) && (theRight2 == 0)))) {
                if (((theRight1 * prop1) || (theRight2 * prop2)) && 1)
                // habilita
                    lockBtnInCtrlBar('sup', 6, false);
                else
                // nao habilita
                    lockBtnInCtrlBar('sup', 6, true);
            }
        }

        for (i = 1; i <= NUM_BTNS_ESPECIFICS; i++) {
            // botao especifico i
            theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			          ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B' + i.toString() + 'A1' + '\'' + ')'));
            theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			          ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B' + i.toString() + 'A2' + '\'' + ')'));
            if (!(((theRight1 == 1) && (theRight2 == 1)) ||
			     ((theRight1 == 0) && (theRight2 == 0)))) {
                if (((theRight1 * prop1) || (theRight2 * prop2)) && 1)
                // habilita
                    lockEspecBtnInCtrlBar('sup', i, false);
                else
                // nao habilita
                    lockEspecBtnInCtrlBar('sup', i, true);
            }
        }
    }

    btnsSupAreTreatedManually(treatBtnRight);
}

/********************************************************************
Ajusta os botoes de: incluir, alterar, estado e excluir
O estado dos botoes dependem de:
incluir     - depende so do contexto corrente
alterar     - depende do contexto corrente e dos campos de controle
- do registro
estado      - tratado sempre manualmente            
excluir     - depende do contexto corrente e dos campos de controle
- do registro

Parametros: 
treatBtnRight   - array dos botoes (incl, alt, est e excl)

Retorno:
nenhum

********************************************************************/
function btnsSupAreTreatedManually(treatBtnRight) {
    // botao incluir
    if (treatBtnRight[0] != null)
        lockBtnInCtrlBar('sup', 3, true);

    // botao alterar
    if (treatBtnRight[1] != null)
        lockBtnInCtrlBar('sup', 4, true);

    // botao estado !!! ATENCAO !!!
    if (treatBtnRight[2] != null)
        lockBtnInCtrlBar('sup', 5, true);

    // botao excluir
    if (treatBtnRight[3] != null)
        lockBtnInCtrlBar('sup', 6, true);
}

/********************************************************************
Ajusta os botoes de: incluir, alterar, excluir
O estado dos botoes dependem de:
incluir     - depende so do contexto corrente
alterar     - depende do contexto corrente e dos campos de controle
- do registro
excluir     - depende do contexto corrente e dos campos de controle
- do registro

Parametros: 
prop1 e prop2   - Opcional, so usado no js_detailsup.js, quando vem da
lista para o detalhe ou quando esta paginando

Retorno:
nenhum

Nota: Funcoes que controlam a liberacao dos botoes das barras
lockBtnInCtrlBar(theBar, btnNumber, cmdLock)
lockEspecBtnInCtrlBar(theBar, btnNumber, cmdLock)
unlockBtnsInCtrlBar(theBar) (DO FRAMEWORK)
refreshCtrlBar(theBar)
********************************************************************/
function adjustControlBarInfByRights(prop1, prop2) {
    // destrava todos os botoes por garantia
    unlockBtnsInCtrlBar('inf');

    var theRight1, theRigth2;
    var treatBtnRight;
    var i;

    treatBtnRight = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                      ('glb_BtnsIncAltEstExcl'));

    // botao incluir                  
    if (treatBtnRight[0] == null) {
        theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'I' + '\'' + ')'));
        // trava o botao
        if (theRight1 == 0)
            lockBtnInCtrlBar('inf', 3, true);
        // destrava o botao
        else if (theRight1 == 1)
            lockBtnInCtrlBar('inf', 3, false);
        // trava o botao pois o direito do usuario depende dos direitos no registro
        else
            lockBtnInCtrlBar('inf', 3, true);
    }

    // botao alterar
    if (treatBtnRight[1] == null) {
        theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
        theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
        // trava o botao
        if (theRight1 == 0 && theRight2 == 0)
            lockBtnInCtrlBar('inf', 4, true);
        // destrava o botao
        else if (theRight1 == 1 && theRight2 == 1)
            lockBtnInCtrlBar('inf', 4, false);
        // trava o botao pois o direito do usuario depende dos direitos no registro
        else
            lockBtnInCtrlBar('inf', 4, true);
    }

    // botao excluir
    if (treatBtnRight[3] == null) {
        theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'E1' + '\'' + ')'));
        theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'E2' + '\'' + ')'));
        // trava o botao
        if (theRight1 == 0 && theRight2 == 0)
            lockBtnInCtrlBar('inf', 6, true);
        // destrava o botao
        else if (theRight1 == 1 && theRight2 == 1)
            lockBtnInCtrlBar('inf', 6, false);
        // trava o botao pois o direito do usuario depende dos direitos no registro
        else
            lockBtnInCtrlBar('inf', 6, true);
    }

    for (i = 1; i <= NUM_BTNS_ESPECIFICS; i++) {
        // botao especifico i
        theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		              ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'B' + i.toString() + 'A1' + '\'' + ')'));
        theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		              ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'B' + i.toString() + 'A2' + '\'' + ')'));
        // trava o botao
        if (theRight1 == 0 && theRight2 == 0)
            lockEspecBtnInCtrlBar('inf', i, true);
        // destrava o botao
        else if (theRight1 == 1 && theRight2 == 1)
            lockEspecBtnInCtrlBar('inf', i, false);
        // trava o botao pois o direito do usuario depende dos direitos no registro
        else
            lockEspecBtnInCtrlBar('inf', i, true);
    }

    // trata barra em funcao do registro
    if (!((prop1 == null) || (prop2 == null))) {
        // botao alterar
        if (treatBtnRight[1] == null) {
            theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
            theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
            if (!(((theRight1 == 1) && (theRight2 == 1)) ||
                 ((theRight1 == 0) && (theRight2 == 0)))) {
                if (((theRight1 * prop1) || (theRight2 * prop2)) && 1)
                // habilita
                    lockBtnInCtrlBar('inf', 4, false);
                else
                // nao habilita
                    lockBtnInCtrlBar('inf', 4, true);
            }
        }

        // botao excluir
        if (treatBtnRight[3] == null) {
            theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'E1' + '\'' + ')'));
            theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                      ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'E2' + '\'' + ')'));
            if (!(((theRight1 == 1) && (theRight2 == 1)) ||
                 ((theRight1 == 0) && (theRight2 == 0)))) {
                if (((theRight1 * prop1) || (theRight2 * prop2)) && 1)
                // habilita
                    lockBtnInCtrlBar('inf', 6, false);
                else
                // nao habilita
                    lockBtnInCtrlBar('inf', 6, true);
            }
        }

        for (i = 1; i <= NUM_BTNS_ESPECIFICS; i++) {
            // botao especifico i
            theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			          ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'B' + i.toString() + 'A1' + '\'' + ')'));
            theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
			          ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'B' + i.toString() + 'A2' + '\'' + ')'));
            if (!(((theRight1 == 1) && (theRight2 == 1)) ||
			     ((theRight1 == 0) && (theRight2 == 0)))) {
                if (((theRight1 * prop1) || (theRight2 * prop2)) && 1)
                // habilita
                    lockEspecBtnInCtrlBar('inf', i, false);
                else
                // nao habilita
                    lockEspecBtnInCtrlBar('inf', i, true);
            }
        }
    }

    btnsInfAreTreatedManually(treatBtnRight);
}

/********************************************************************
Ajusta os botoes de: incluir, alterar, estado e excluir
O estado dos botoes dependem de:
incluir     - depende so do contexto corrente
alterar     - depende do contexto corrente e dos campos de controle
- do registro
estado      - tratado sempre manualmente            
excluir     - depende do contexto corrente e dos campos de controle
- do registro

Parametros: 
treatBtnRight   - array dos botoes (incl, alt, est e excl)

Retorno:
nenhum

********************************************************************/
function btnsInfAreTreatedManually(treatBtnRight) {
    // botao incluir
    if (treatBtnRight[0] != null)
        lockBtnInCtrlBar('inf', 3, true);

    // botao alterar
    if (treatBtnRight[1] != null)
        lockBtnInCtrlBar('inf', 4, true);

    // botao estado !!! ATENCAO !!!
    if (treatBtnRight[2] != null)
        lockBtnInCtrlBar('inf', 5, true);

    // botao excluir
    if (treatBtnRight[3] != null)
        lockBtnInCtrlBar('inf', 6, true);
}

/********************************************************************
Requisita dados para preencher o combo de proprietarios do pesqlist
********************************************************************/
function getCmbProprietariesList(bFirstTime, contextID) {
    // Parametros para o servidor
    var strPars = new String();
    strPars = '?';

    var formID = window.top.formID;  // variavel global do browser filho

    // formID
    strPars += 'nFormID=';
    strPars += escape(formID.toString());
    // contextoID
    strPars += '&nContextoID=';
    strPars += escape(contextID.toString());
    // empresaID
    strPars += '&nEmpresaID=' + escape((sendJSMessage('AnyHtml', JS_COMBODATA, 'VALUE', null)).toString());
    // userID
    strPars += '&nUsuarioID=' + escape((sendJSMessage('AnyHtml', JS_WIDEMSG, '__CURRUSERID', null)).toString());

    dsoPropsPL.URL = SYS_ASPURLROOT + '/serversidegenEx/cmbspropspl.aspx' + strPars;

    // Ultimo passo
    if (bFirstTime)
        dsoPropsPL.ondatasetcomplete = getCmbProprietariesList_FirstTime_DSC;
    else
        dsoPropsPL.ondatasetcomplete = getCmbProprietariesList_DSC;

    dsoPropsPL.Refresh();
}

/********************************************************************
Retorno da requisicao de dados para preencher o combo de proprietarios
do pesqlist.
Preenche o combo de proprietarios do pesqlist.
********************************************************************/
function getCmbProprietariesList_FirstTime_DSC() {
    fillcmbProprietariosPL(selProprietariosPL, dsoPropsPL);

    // informa arquivo controlador que os combos
    // de contexto e filtros e de proprietarios do pesqlist
    // foram preenchidos
    sendJSMessage(getHtmlId(), JS_CONTFILTFINISH,
                  null, null);
}

/********************************************************************
Retorno da requisicao de dados para preencher o combo de proprietarios
do pesqlist.
Preenche o combo de proprietarios do pesqlist.
********************************************************************/
function getCmbProprietariesList_DSC() {
    fillcmbProprietariosPL(selProprietariosPL, dsoPropsPL);

    // Este sendMessage informa o pesqlist que o segundo combo da barra superior
    // terminou de carregar
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'EXECPESQ', null);
}

/********************************************************************
Preenche o combo de proprietarios do pesqlist.
********************************************************************/
function fillcmbProprietariosPL(cmb, dso) {
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var oOption;

    clearComboEx([cmb.id]);

    oldDataSrc = cmb.dataSrc;
    oldDataFld = cmb.dataFld;
    cmb.dataSrc = '';
    cmb.dataFld = '';

    while (!dso.recordset.EOF) {

        optionStr = dso.recordset['Proprietario'].value;
        optionValue = dso.recordset['ProprietarioID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        cmb.add(oOption);
        dso.recordset.MoveNext();
    }

    if (cmb.options.length != 0)
        cmb.selectedIndex = 0;

    cmb.dataSrc = oldDataSrc;
    cmb.dataFld = oldDataFld;
}

function __openModalDocumentos(controlBar, nRegistroID) {
    var nFormID = window.top.formID;
    var nSubFormID = window.top.subFormID;
    var nUserID = getCurrUserID();

    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + controlBar + '\'' + ',' + '\'' + 'B1A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + controlBar + '\'' + ',' + '\'' + 'B1A2' + '\'' + ')'));

    var nC1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + controlBar + '\'' + ',' + '\'' + 'B1C1' + '\'' + ')'));

    var nC2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + controlBar + '\'' + ',' + '\'' + 'B1C2' + '\'' + ')'));

    var nI = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + controlBar + '\'' + ',' + '\'' + 'B1I' + '\'' + ')'));

    var nE1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + controlBar + '\'' + ',' + '\'' + 'B1E1' + '\'' + ')'));

    var nE2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + controlBar + '\'' + ',' + '\'' + 'B1E2' + '\'' + ')'));

    var nFormID = window.top.formID;

    var nSubFormID = window.top.subFormID;

    htmlPath = SYS_PAGESURLROOT + '/modalgen/modaldocumentos.asp?RegistroID=' + escape(nRegistroID) +
        '&FormID=' + escape(nFormID) +
        '&SubFormID=' + escape(nSubFormID) +
        '&UserID=' + escape(nUserID) +
        '&A1=' + escape(nA1) + '&A2=' + escape(nA2) +
        '&C1=' + escape(nC1) + '&C2=' + escape(nC2) +
        '&I=' + escape(nI) +
        '&E1=' + escape(nE1) + '&E2=' + escape(nE2);

    // htmlPath = SYS_PAGESURLROOT + '/modalgen/modaldocumentos.asp?RegistroID=' + dsoSup01.recordset.Fields[0].value
    showModalWin(htmlPath, new Array(1000, 420));
}
