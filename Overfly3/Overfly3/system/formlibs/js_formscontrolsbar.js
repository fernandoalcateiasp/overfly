/********************************************************************
js_formscontrolsbar.js

Library javascript de funcoes para o control bar superior e inferior
********************************************************************/

// CONSTANTES *******************************************************

//var NUMBTNS_BOTH = 19;
//var NUMBTNSESPEC_BOTH = 8;

var NUMBTNS_BOTH = 27;
var NUMBTNSESPEC_BOTH = 16;

// FINAL DE CONSTANTES **********************************************

/********************************************************************

INDICE DAS FUNCOES:

adjustSupInfControlsBar(sInterfMode, regOrListPos, sBtnsSpec, win, grid, currDSO)
adjustControlBarEx(win, theBar, btns) (nao usar direto)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao configura o control bar superior e o inferior em funcao
do modo da interface. Para efeito de raciocinio temos dois conjuntos
de interface: pesquisa/listagem e detalhe.

Parametros:
sInterfMode          - Obrigatorio. Parametro composto XXXX + YYYY
regOrListPos         - Opcional ou null. Na paginacao de lista ou registro esta no
primeiro ('P') ou ultimo ('U')
sBtnsSpec            - Opcional ou null. Os botoes especificos.
win                  - Opcional ou null. Se nao passa nada assume o window corrente.
grid                 - Opcional ou null. Referencia a grid.
currDso              - Opcional ou null. Referencia a dso (geralmente linkado ao grid).

XXXX    YYYY
DISALL
PESQ    MODE, DIS
LIST    MODE, DIS
INF
SUP     CONS, INCL, DIS

Retorno:
nenhum
********************************************************************/
function adjustSupInfControlsBar(sInterfMode, regOrListPos, sBtnsSpec, win, grid, currDSO) {
    // Nao tem o numero de botoes, completa com desabilitados
    if (sBtnsSpec != null) {
        if ((sBtnsSpec.length) > 0 && (sBtnsSpec.length < NUMBTNSESPEC_BOTH)) {
            while (sBtnsSpec.length < NUMBTNSESPEC_BOTH)
                sBtnsSpec = sBtnsSpec + 'D';
        }
    }

    if (sInterfMode.toUpperCase() == 'PESQMODE')
        adjustControlBarEx(window, 'sup', 'HDHDDDDDDDDDDDDDDD');
    else if (sInterfMode.toUpperCase() == 'PESQLISTONEVIEW') {
        adjustControlBarEx(window, 'sup', defaultGridListModeEx(grid, eval(glb_nPAGENUMBER == 1)));
    }
    else if (sInterfMode.toUpperCase() == 'PESQDIS')
        adjustControlBarEx(window, 'sup', 'DDDDDDDDDDDDDDDDDD');
    else if (sInterfMode.toUpperCase() == 'LISTMODE')
        adjustControlBarEx(window, 'sup', defaultGridListModeEx(grid, eval(glb_nPAGENUMBER == 1)));
    else if (sInterfMode.toUpperCase() == 'DISALL') {
        adjustControlBarEx(window, 'sup', 'DDDDDDDDDDDDDDDDDD');
        adjustControlBarEx(window, 'inf', 'DDDDDDDDDDDDDDDDDD');
    }
    else if (sInterfMode.toUpperCase() == 'SUPCONS')  // so pode usar no sup    
    {
        if (regOrListPos == null)
            adjustControlBarEx(window, 'sup', 'HDHHHHDDHHHDDDDDDD');
        else if (regOrListPos.toUpperCase() == 'P')
            adjustControlBarEx(window, 'sup', 'HDHHHHDDHDHDDDDDDD');
        else if (regOrListPos.toUpperCase() == 'U')
            adjustControlBarEx(window, 'sup', 'HDHHHHDDHHDDDDDDDD');
    }
    else if (sInterfMode.toUpperCase() == 'SUPINCL') {
        adjustControlBarEx(window, 'sup', 'DDDDDDHHDDDDDDDDDD');
        adjustControlBarEx(window, 'inf', 'DDDDDDDDDDDDDDDDDD');
    }
    else if (sInterfMode.toUpperCase() == 'SUPDIS')
        adjustControlBarEx(window, 'sup', 'DDDDDDDDDDDDDDDDDD');
    else if (sInterfMode.toUpperCase() == 'INFCONS') // so pode usar no inf
    {
        var x = currDSO.id;

        if (x.lastIndexOf('Grid') >= 0) // se � grid
        // As variaveis glb_nPAGENUMBER e glb_useParallelDSO
        // estao definidas na lib js_detailinf.js
            if (glb_useParallelDSO == true)
            adjustControlBarEx(window, 'inf', defaultGridModeEx(grid, eval(glb_nPAGENUMBER == 1)));
        else
            adjustControlBarEx(window, 'inf', defaultGridModeEx(grid));
        else
            adjustControlBarEx(window, 'inf', 'DDDHDDDDHDDDDDDDDD');
    }
}

/********************************************************************
Esta funcao complementa a funcao adjustSupInfControlsBar() que
configura o control bar superior e o inferior.

Parametros:
win - referencia ao wihndow corrente
theBar - 'SUP' ou 'INF' - a barra corrente
btns - array string dos botoes

Retorno:
true se consegue mostrar uma pasta, caso contrario false
********************************************************************/
function adjustControlBarEx(win, theBar, btns) {
    var fSupisVisible = ascan(htmlIdsVisibles(win), 'sup01Html', true) >= 0;
    var fPesqListVisible = ascan(htmlIdsVisibles(win), 'pesqlistHtml', true) >= 0;
    var sCmb1 = '';
    var sCmb2 = '';

    // Nao tem o numero de botoes, completa com desabilitados
    if ((btns.length) > 0 && (btns.length < NUMBTNS_BOTH)) {
        while (btns.length < NUMBTNS_BOTH)
            btns = btns + 'D';
    }

    if (fSupisVisible) {
        if (theBar.toUpperCase() == 'INF') {
            // se OK e Cancela estiverem habilitados na barra inferior ...
            // ou se todos os botoes da barra inferior estiverem desabilitados
            if ((btns.substr(6, 2) == 'HH') || (btns == 'DDDDDDDDDDDDDDDDDDD')) {
                sCmb1 = 'disable';
                sCmb2 = 'disable';
            }
            else {
                sCmb1 = 'enable';
                sCmb2 = 'enable';
            }
        }
        else {
            sCmb1 = 'disable';
            sCmb2 = 'disable';
        }

    }
    else if (fPesqListVisible) {
        sCmb1 = 'enable';
        sCmb2 = 'enable';
    }

    if (numOptionsInCmbControlBar(theBar, 1) < 1)
        sCmb1 = 'disable';

    if (numOptionsInCmbControlBar(theBar, 2) < 1)
        sCmb2 = 'disable';

    setupControlBar(theBar, sCmb1, sCmb2, btns);
}

/********************************************************************
Esta funcao complementa a funcao adjustSupInfControlsBar() que
configura o control bar superior e o inferior.

Parametros:
grid        - referencia ao grid corrente
firstPage   - primeira pagina da paginacao do grid

Nota: No modo anterior o botao pesquisa era habilitado em todas
as strings de botoes e o botao lista desabilitado.

Retorno:
true se consegue mostrar uma pasta, caso contrario false
********************************************************************/
function defaultGridListModeEx(grid, firstPage) {
    var sRetorno = '';

    // se for a primeira pagina e nao tiver nenhuma linha no grid
    if (firstPage && (grid.Rows == 1)) {
        sRetorno = 'HDHDDDDDHDDDDDDDDD'; // desabilita o Anterior e o Proximo
    }
    // se for a primeira pagina e tiver linha no grid
    else if (firstPage) {
        sRetorno = 'HHHDDDDDHDHDDDDDDD'; // desabilita o Anterior e habilita o Proximo
    }
    // se for a ultima pagina e nao importa se tem ou nao linha no grid
    else if (grid.Rows == 1) {
        sRetorno = 'HDHDDDDDHHDDDDDDDD'; // habilita o Anterior e desabilita o Proximo
    }
    // se for uma pagina intermediaria
    else {
        sRetorno = 'HHHDDDDDHHHDDDDDDD'; // habilita o Anterior e o Proximo
    }

    return sRetorno;
}

/********************************************************************
Esta funcao complementa a funcao adjustSupInfControlsBar() que
configura o control bar superior e o inferior.

Parametros:
grid        - referencia ao grid corrente

Retorno:
a string de botoes
********************************************************************/
function defaultGridModeEx(grid, firstPage) {
    var i = 0;
    var sEstado = 'D';
    for (i = 0; i < grid.Cols; i++) {
        if ((grid.ColKey(i)).lastIndexOf('Estado*') >= 0) {
            sEstado = 'H';
        }
    }

    var sRetorno = '';
    var sPaging = '';
    if (grid.Rows == 1) {
        // Retorno = habilita Novo e Refresh
        sRetorno = 'DDHDDDDDHDDDDDDDDD'; // habilita Novo e Refresh
    }
    else if (grid.Rows == 2) {
        // sRetorno = habilita Novo,Altera,Refresh
        sRetorno = 'DDHH' + sEstado + 'HDDHDDDDDDDDD'; // habilita Novo,Altera,Refresh
    }
    else if (grid.Rows >= 3) {
        // sRetorno =  habilita Novo,Altera,Refresh
        sRetorno = 'DDHH' + sEstado + 'HDDHHHDDDDDDD'; // habilita Novo,Altera,Refresh
    }

    if (firstPage != null) {
        // se for a primeira pagina e nao tiver nenhuma linha no grid
        if (firstPage && (grid.Rows == 1)) {
            sPaging = 'DD'; // desabilita o Anterior e o Proximo
        }
        // se for a primeira pagina e tiver linha no grid
        else if (firstPage) {
            sPaging = 'DH'; // desabilita o Anterior e habilita o Proximo
        }
        // se for a ultima pagina e nao importa se tem ou nao linha no grid
        else if (grid.Rows == 1) {
            sPaging = 'HD'; // habilita o Anterior e desabilita o Proximo
        }
        // se for uma pagina intermediaria
        else {
            sPaging = 'HH'; // habilita o Anterior e o Proximo
        }
        sRetorno = sRetorno.substr(0, 9) + sPaging + sRetorno.substr(11, 7);
    }

    return sRetorno;
}
