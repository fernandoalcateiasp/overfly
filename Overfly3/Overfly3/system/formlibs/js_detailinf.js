/********************************************************************
js_detailinf.js

Library javascript de funcoes basicas para detalhes infs de forms
********************************************************************/

// CONSTANTES *******************************************************

// Controla se implementou _implementsPasteInTxtFld()
var glb_implementsPasteInTxtFld = false;

// Qualquer valor da variavel abaixo nao monta o inf quando:
// 1. Detalha do pesqList
// 2. Refresca no sup
// 3. Pagina no sup
// 4. Cancela inclusao ou edicao no sup 
var glb_bInfNoData = true;

// Mantem compatibilidade com algumas bibliotecas
var glb_FORMNAME = window.top.formName;
var glb_USERID = window.top.userID;

// Array dos divs do arquivo
var glb_aDIVS = new Array();

// Combos estaticos em divs
// Array bidimensional: id dos combos estaticos e indice na tabela de dados
var glb_aStaticCombos = null;

// Combos estaticos em grids.
// Array bidimensional da pasta id e array bidimensional
// do indice das colunas
// (a contar de zero), id do dso dos dados do combo,
// nome do campo da tabela visivel no combo e
// nome do campo do grid que linka o grid com o combo.
// O array deve ser null se nenhum grid tem combos estaticos
var glb_aGridStaticCombos = null;

// Quantidade de combos no grid corrente
var glb_nCombosInCurrGrid = 0;

// Guarda ID da pasta default. E a pasta que e mostrada quando as outras
// nao atendem as operacoes.
var glb_pastaDefault = null;

// Mantem referencia ao dso corrente 
var glb_currDSO = null;

// Ultimo select executado pelo dso corrente
// e usado na funcao maxLenOnKeyPressSetArrays
var glb_lastSelect = '';

// Guarda linha corrente no grid
var glb_currLineInGrid = -1;

// Ultimo botao clicado no control bar superior ou inferior
var glb_btnCtlSupInf = null;

// ID do registro corrente no sup
var glb_registroID = 0;

// Modo anterior do grid: inclusao, alteracao
var glb_antGridMode = '';

// Tipo do registro corrente no sup (usado apenas em alguns forms)
// quando o form nao tem este tipo de campo no sup, as chamadas
// para preencher esta variavel colocam 0 nela 
var glb_nTipoRegistroID = 0;

// EstadoID do registro antes de abrir a maquina de estado
var glb_EstadoIDBeforeStateMach = 0;

// Guarda o numero da pagina atual na paginacao inferior
var glb_nPAGENUMBER = 1;

// Array das pastas de grid que tem paginacao
var glb_aFoldersPaging = [];

// Guarda se a pasta corrente usa dso paralelo ou nao
// null ou false nao usa, true usa
var glb_useParallelDSO = null;

// Guarda a tabela associada ao dso paralelo do grid corrente
var glb_currGridTable = null;

// Variavel de timer
var glb_DetInfTimerVar = null;

// Variaveis de pesquisa de grid em modo paging
var glb_aCmbKeyPesq = null;
var glb_bCheckInvert = false;
var glb_sArgument = '';
var glb_sFilterPesq = '';
var glb_scmbKeyPesqSel = '';
var glb_scmbLinePerPageSel = '100';
var glb_vOptParam1 = '';
var glb_vOptParam2 = '';

// Excepcionalidade em 05/02/2011 - gravar BULLET e TAB
var glb_AcceptBulletAndTab = null;

var glb_refrInf = null;

// Array que controla botoes incluir, alterar e excluir
// quando nao devam ser tratados pelos direitos na automacao
// Os elementos do array tem valores true se nao devam ser tratados
var glb_BtnsIncAltEstExcl = new Array(null, null, null, null);

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// FINAL DE CONSTANTES **********************************************

/* ATRIBUTOS ********************************************************

Aplicado        Atributo            Valores
---------------------------------------------------------------------

********************************************************************/

/********************************************************************

INDICE DAS FUNCOES:

LOOP DE MENSAGENS

EVENTOS DO CONTROL BAR INFERIOR

FUNCOES CHAMADAS PELOS EVENTOS DO GRID:
fg_EnterCell()
fg_AfterRowColChange()
fg_MouseUp()
fg_MouseDown()
fg_BeforeEdit()
fg_DblClick()
fg_ChangeEdit()
fg_ValidateEdit()
fg_BeforeRowColChange()

FUNCOES GERAIS:
windowOnLoad_1stPart()
windowOnLoad_2ndPart()
before_SetupPage()
after_SetupPage()    
onClickBtnLupa()

FUNCOES DA AUTOMACAO:
lockAndOrSvrInf_SYS(btnClicked)
doChangeDivInf_SYS(btnClicked)
finalOfLockAndOrSvr_SYS(btnClicked)        
supIsInclOrEdit()
showDetailInterface()
showCompatiblePasta()    
keepRegIDAndTypeID()
keepCurrFolder()
keepCurrDso()
editCurrFolder()
cancelEditCurrFolder()
setCurrDSOByCmb1(pastaID, doRefresh)
refillCmb2Inf(pastaID)
treats_FldRegID_Inf()
fillCmbPastasInCtlBarInf_Aut(glb_nTipoRegistroID)
treatsControlsBars_Aut(btnClicked, folderID, tipoCtlrsPasta, dsoID)

FUNCOES DE GRAVACAO:
saveRegister_1stPart()
saveRegister_2ndPart()
submitChanges_DSC()
setupParallelSaveDSO()
setupParallelSaveDSO_DSC()
setupParallelSaveDSO_2ndPart()
submitChangesParallel_DSC()

FUNCOES DE ACESSO A DADOS NO SERVIDOR:
getDataOfInfInServer()
filtroComplete_DSC(null, emptyFilter)    
dso01JoinSup_DSC()    
finalDSOsCascade()

FUNCOES DE INICIALIZACAO
inicializeDataInInfAndShowCompatiblePasta(btnClicked)
processDataAndPasta()
adjustInfBar__()
    
FUNCOES DE GRID:
buildGridFromFolder(folderID)
addLineInGrid()
nextLineInGrid()
previousLineInGrid()
deleteConfirmLineInGrid()
excludeRegisterOfLineInGrid()
deleteLineInGrid()
deleteLineInGridPaging(nRegistroID)
setupParallelDeleteDSO_DSC()
setupParallelDeleteDSO_DSC_2ndPart()
folderHasGrid(nFolderID)    parametro opcional
lockBtnsInCtrlBarInf_Grid()
getEstadoIDOfLineInGrid()
keepCurrLineInGrid()
startObsWindow_inf( grid, dso, fldObsName, colObsNumber )

FUNCOES DE PROPRIETARIO E ALTERNATIVO:
setPropAlt()    
loadComboProprietario(strFind)
cmbProprietarioComplete()
loadComboAlternativo(strToFind)
cmbAlternativoComplete()
getFantasiaPropAlt()
dsoCmbsPropAlt_DSC()

FUNCOES DA MAQUINA DE ESTADO:
showStateMachine()
saveEstadoID()

FUNCOES DA PAGINACAO INFERIOR:
execPaging(dso,  folderID)
changePageInList(nNumber)
execPesqInPaging(param1, param2)
    
FUNCOES DE DIREITOS:    
stripFoldersByRight(selBefore)
refreshSup__()
    
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// LOOP DE MENSAGENS ************************************************

function wndJSProc(idElement, msg, param1, param2) {
    switch (msg) {
        // Mensagens da maquina de estado. 
        // Sao enviadas ao terminar de carregar e ao fechar no botao cancela. 
        case JS_STMACHMODAL:
            {
                if (idElement == 'statemachinfmodal01Html') {
                    if (param1 == 'STATEMACH_VISIBLE') {
                        glb_EstadoIDBeforeStateMach = getEstadoIDOfLineInGrid();

                        // alerta programador que a maquina de estado abriu
                        stateMachOpened(glb_EstadoIDBeforeStateMach);

                        writeInStatusBar('child', 'cellMode', 'Estado');
                    }
                    else if (param1 == 'STATEMACH_HIDDEN')
                        writeInStatusBar('child', 'cellMode', 'Detalhe');

                    return 0;
                }
            }
            break;

        // Mensagens da troca de divs
        case JS_INFSYS:
            {
                // Informa o programador acabou de fazer as operacoes de
                // servidor particulares do sup. Repassa para a biblioteca de
                // inf fazer as operacoes de servidor de inf
                if ((param1 == 'ENDPROGSERVERSUP') && (idElement == window.top.sup01ID)) {
                    // param2 e o botao clicado em controlbar
                    lockAndOrSvrInf_SYS(param2);
                    return 0;
                }
                // Informa o programador para fazer as operacoes de
                // servidor particulares do inf
                else if ((param1 == 'PROGSERVER') &&
                    ((idElement == window.top.sup01ID) || (idElement == getHtmlId()))) {
                    // param2 e o botao clicado em controlbar
                    // salva aqui e propaga para o usuario
                    glb_btnCtlSupInf = param2;
                    prgServerInf(param2);
                    return 0;
                }
                // O sup informa que terminou as operacoes particulares de interface
                // e que deve ser executado a troca de divs pela biblioteca inf
                else if ((param1 == 'ENDCHANGEDIVSUP') && (idElement == window.top.sup01ID)) {
                    // param2 == ultimo botao clicado em control bar
                    doChangeDivInf_SYS(param2);
                    return 0;
                }
                // Informa que terminou as operacoes particulares do inf
                // e que deve ser executado o changeDiv
                else if ((param1 == 'CHANGEDIV') && (idElement == getHtmlId())) {
                    // param2 e o botao clicado
                    doChangeDivInf_SYS(param2);
                    return 0;
                }
                // Informa que foi feita a troca de divs
                // Fazer as operacoes particulares do form que devem ser
                // feitas apos trocar os divs
                // e no final destas operacoes mandar mensagem
                // sendJSMessage(getHtmlId(), JS_CHANGESUPDIV, 'FINISHCHANGEDIV', null)
                else if ((param1 == 'AFTERCHANGEDIV') && (idElement == getHtmlId())) {
                    // param2 e o botao clicado em controlbar
                    // salva aqui e propaga para o usuario
                    glb_btnCtlSupInf = param2;
                    prgInterfaceInf(param2, keepCurrFolder(), glb_currDSO);
                    return 0;
                }
                // Informa que terminaram as operacoes particulares
                // apos a troca de divs tratar aqui a interface
                else if ((param1 == 'FINISHCHANGEDIV') && (idElement == getHtmlId())) {
                    // param2 ==  botao clicado em control bar
                    finalOfLockAndOrSvr_SYS(param2);
                    return 0;
                }
                // Informa que terminaram as operacoes de interface
                // chama funcao para o programador tratar particularidades
                // dos control bar inferior r superior
                else if (param1 == 'TREATCTLBARS') {
                    if (idElement == getHtmlId()) {
                        // param2 e o botao clicado em controlbar
                        // salva aqui e propaga para o usuario
                        glb_btnCtlSupInf = param2;

                        var aItemSelected = getCmbCurrDataInControlBar('inf', 1);

                        // modificacao abaixo em 18/12/2001
                        // ate esta data retornava 0
                        // o combo de pastas nao tem item selecionado
                        // forca selecionar a pasta default
                        if (aItemSelected == null) {
                            //return 0;
                            fillCmbPastasInCtlBarInf_Aut(glb_nTipoRegistroID);
                        }

                        var folderID = aItemSelected[1];

                        if (glb_btnCtlSupInf == 'SUPOK')
                            adjustInfBar__();

                        // A pasta corrente nao e grid
                        // Alterado em 08/11/2001
                        //if ( (glb_currDSO.id).lastIndexOf('Grid') < 0 )
                        if (folderHasGrid(keepCurrFolder()) == 0) {
                            if (glb_btnCtlSupInf == 'SUPINCL')
                                treats_FldRegID_Inf();

                            treatsControlsBars_Aut(glb_btnCtlSupInf, folderID, 'CTRLS', glb_currDSO.id);
                        }
                        else {
                            adjColsWidthInGrid();

                            if (glb_btnCtlSupInf == 'SUPINCL')
                                treats_FldRegID_Inf();

                            treatsControlsBars_Aut(glb_btnCtlSupInf, folderID, 'GRID', glb_currDSO.id);
                            lockBtnsInCtrlBarInf_Grid();
                        }
                    }
                    // especifico do sup, entrou em edicao
                    else if (idElement == window.top.sup01ID) {
                        adjColsWidthInGrid();

                        if (param2 == 'SUPINCL')
                            treats_FldRegID_Inf();

                        treatsControlsBars_Aut(param2, null, null, null);

                        lockBtnsInCtrlBarInf_Grid();
                    }

                    return 0;
                }
                // O sup foi paginado e ja esta no primeiro ou no ultimo registro
                else if ((param1 == 'CFGINFCTLBAR') && (idElement == window.top.sup01ID)) {
                    adjustSupInfControlsBar('INFCONS', null, null, null, fg, glb_currDSO);
                    // Ze em 21/04/2011 passou a chamar o treats
                    // foi inserido todo o codigo abaixo
                    var folderID = keepCurrFolder();
                    var folderType = '';

                    if (folderHasGrid(keepCurrFolder()) == 0)
                        folderType = 'CTRLS';
                    else
                        folderType = 'GRID';

                    treatsControlsBars_Aut(param2, folderID, folderType, glb_currDSO.id);
                    return true;
                }
            }
            break;

        // Mensagens do programador 
        case JS_DATAINFORM:
            {
                // Os dados dos combos estaticos chegaram do servidor
                if ((param1 == 'FINISHSTCMBS') && (idElement == getHtmlId())) {
                    // carrega combos estaticos
                    // array bidimensional de combos ids e ou null se nao tem
                    // combos estaticos
                    if (fillStaticCombos(glb_aStaticCombos, 'INF')) {
                        // Este arquivo carregou
                        sendJSMessage(getHtmlId(), JS_PAGELOADED,
                                  new Array(window.top.name.toString(),
                                  window.top.formName), 0X2);
                        return 0;
                    }
                }
                // Interface do sup01 vai entrar em modo de inclusao.
                // Seleciona pasta default no combo1 do ctl bar inf
                // Oculta todos os divs
                else if ((param1 == 'SUPINEDITING') && (idElement == window.top.sup01ID)) {
                    supIsInclOrEdit();
                    return 0;
                }
                // Mensagem recebida do sup, para travar a interface do inf
                else if ((param1 == 'LOCKCONTROLS') && (idElement == window.top.sup01ID)) {
                    // param2 == cmdLock
                    lockControls(param2, false);
                    return 0;
                }
                // Mensagem recebida do sup, para mostrar pasta compativel
                // e ajustar o control bar inferior
                // apos uma gravacao no sup
                else if ((param1 == 'SHOWCOMPATIBLEPASTA') && (idElement == window.top.sup01ID)) {
                    inicializeDataInInfAndShowCompatiblePasta(param2);

                    return 0;
                }
                else if ((param1 == 'PROP') && (idElement == 'modalpropaltHtml')) {
                    loadComboProprietario(param2);
                    return 0;
                }
                else if ((param1 == 'ALT') && (idElement == 'modalpropaltHtml')) {
                    loadComboAlternativo(param2);
                    return 0;
                }
                // Msg vinda da janela da maquina de estado.
                // Troca o estado do registro.
                else if ((param1 == 'ESTIDCHANGED') && (idElement == 'statemachinfmodal01Html')) {
                    // Novo estadoID selecionado na maquina de estado
                    glb_currLineInGrid = fg.Row;
                    changeStateMachine('', '', param2, glb_currDSO, fg, 'inf');

                    return 0;
                }
                // Msg vinda de janela modal que deve ser propagada
                // para o form
                else if (param1.lastIndexOf('_CALLFORM_I') > 0) {
                    if (param1.indexOf('OK') >= 0) {
                        // Volta para a primeira pagina de pesquisa no inf
                        // quando a modal de pesquisa muda os parametros de pesquisa
                        if (idElement.toUpperCase() == 'MODALOFPAGINGINFHTML')
                            glb_nPAGENUMBER = 1;

                        modalInformForm(idElement, 'OK', param2);
                    }
                    if (param1.indexOf('CANCEL') >= 0)
                        modalInformForm(idElement, 'CANCEL', param2);

                    return true;
                }
                // Mensagens de retorno de dados para o programador
                else if ((idElement == 'INF_HTML') && (param1 == EXECEVAL)) {
                    return eval(param2);
                }
            }
            break;

        default:
            return null;
    }
}

// FINAL DE LOOP DE MENSAGENS ***************************************

// EVENTOS DO CONTROL BAR INFERIOR **********************************
// A ORDEM DOS COMBOS
// combo1               COMBO1
// combo2               COMBO2

// A ORDEM DOS BOTOES
// 0    - Pesquisar     PESQ
// 1    - Listar        RETRO
// 2    - Detalhar      AVANC
// 3    - Incluir       INCL
// 4    - Alterar       ALT
// 5    - Estado        EST
// 6    - Excluir       EXCL
// 7    - Ok            OK
// 8    - Cancelar      CANC
// 9    - Refresh       REFR
// 10   - Anterior      ANT
// 11   - Seguinte      SEG
// 12   - Um            UM
// 13   - Dois          DOIS
// 14   - Tres          TRES

function __combo_1(controlBar, optText, optValue, optIndex) {
    // Inicializacao das variaveis de pesquisa para paginacao de grid
    glb_bCheckInvert = false;
    glb_sArgument = '';
    glb_sFilterPesq = '';
    glb_scmbLinePerPageSel = '100';
    glb_vOptParam1 = '';
    glb_vOptParam2 = '';
    glb_useParallelDSO = null;

    glb_nPAGENUMBER = 1;
    glb_btnCtlSupInf = 'INFCOMBO1';

    var nQtyGridsInFolder = folderHasGrid();

    // Pasta corrente nao tem grid
    if (nQtyGridsInFolder == 0)
        glb_currDSO = dso01JoinSup;
    // Pasta corrente tem 1 grid
    else if (nQtyGridsInFolder == 1) {
        if (ascan(glb_aFoldersPaging, optValue, false) >= 0) {
            glb_useParallelDSO = true;
            glb_currDSO = dso01PgGrid;
        }
        else
            glb_currDSO = dso01Grid;
    }

    // chama funcao do lado do programador
    // prossegue se nada retorna
    if (selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder) == null)
        setCurrDSOByCmb1(optValue, true);
}

function __combo_2(controlBar, optText, optValue, optIndex) {
    glb_nPAGENUMBER = 1;
    glb_btnCtlSupInf = 'INFCOMBO2';

    // Funciona como um refresh no inf
    glb_btnCtlSupInf = 'INFREFR';

    lockAndOrSvrInf_SYS(glb_btnCtlSupInf);
}

function __btn_PESQ(controlBar) {
    glb_btnCtlSupInf = 'INFPESQ';
}

function __btn_RETRO(controlBar) {
    glb_btnCtlSupInf = 'INFRETRO';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;
}

function __btn_AVANC(controlBar) {
    glb_btnCtlSupInf = 'INFAVANC';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;
}

function __btn_INCL(controlBar) {
    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 11;

    glb_btnCtlSupInf = 'INFINCL';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    addLineInGrid();

    setColumnWidthToEdition(fg, glb_currDSO);
}

function __btn_ALT(controlBar) {
    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 21;

    glb_btnCtlSupInf = 'INFALT';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    editCurrFolder();
}

function __btn_EST(controlBar) {
    glb_btnCtlSupInf = 'INFEST';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    showStateMachine();
}

function __btn_EXCL(controlBar) {
    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 31;

    glb_btnCtlSupInf = 'INFEXCL';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    deleteConfirmLineInGrid();
}

function __btn_OK(controlBar) {
    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 0;

    glb_btnCtlSupInf = 'INFOK';

    // Excepcionalidade em 05/05/2011, aceitar gravar BULLET
    // e TAB, fun��o chamada no inf do form, para o programador
    try {
        glb_AcceptBulletAndTab = saveBulletAndTabInTextArea('INF', 'INFOK', keepCurrFolder());
    }
    catch (e) {
        glb_AcceptBulletAndTab = null;
    }
    
    try {

        // Trima os campos e verifica campos obrigatorios
        if (glb_AcceptBulletAndTab == null)
            trimAllImputFields(window);
        else {
            glb_AcceptBulletAndTab = null;
            trimAllImputFields_Exception(window);
        }

        updateRecordFromHTMLFields();
    }
    catch (e) {
        ;
    }

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    saveRegister_1stPart();
}

function __btn_CANC(controlBar) {
    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 0;

    glb_btnCtlSupInf = 'INFCANC';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    cancelEditCurrFolder();
}

function __btn_REFR(controlBar) {
    glb_btnCtlSupInf = 'INFREFR';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    lockAndOrSvrInf_SYS(glb_btnCtlSupInf);
}

function __btn_ANT(controlBar) {
    glb_btnCtlSupInf = 'INFANT';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    if (glb_useParallelDSO == true)
        changePageInList(-1);
    else
        nextLineInGrid();
}

function __btn_SEG(controlBar) {
    glb_btnCtlSupInf = 'INFSEG';

    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if (btnBarNotEspecClicked(controlBar, glb_btnCtlSupInf) != null)
        return true;

    if (glb_useParallelDSO == true)
        changePageInList(1);
    else
        previousLineInGrid();
}

function __btn_UM(controlBar) {
    glb_btnCtlSupInf = 'INFUM';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 1);
}

function __btn_DOIS(controlBar) {
    glb_btnCtlSupInf = 'INFDOIS';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 2);
}

function __btn_TRES(controlBar) {
    glb_btnCtlSupInf = 'INFTRES';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 3);
}

function __btn_QUATRO(controlBar) {
    glb_btnCtlSupInf = 'INFQUATRO';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 4);
}

function __btn_CINCO(controlBar) {
    glb_btnCtlSupInf = 'INFCINCO';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 5);
}

function __btn_SEIS(controlBar) {
    glb_btnCtlSupInf = 'INFSEIS';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 6);
}

function __btn_SETE(controlBar) {
    glb_btnCtlSupInf = 'INFSETE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 7);
}

function __btn_OITO(controlBar) {
    glb_btnCtlSupInf = 'INFOITO';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 8);
}


function __btn_NOVE(controlBar) {
    glb_btnCtlSupInf = 'INFNOVE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 9);
}

function __btn_DEZ(controlBar) {
    glb_btnCtlSupInf = 'INFDEZ';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 10);
}

function __btn_ONZE(controlBar) {
    glb_btnCtlSupInf = 'INFONZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 11);
}

function __btn_DOZE(controlBar) {
    glb_btnCtlSupInf = 'INFDOZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 12);
}

function __btn_TREZE(controlBar) {
    glb_btnCtlSupInf = 'INFTREZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 13);
}

function __btn_QUATORZE(controlBar) {
    glb_btnCtlSupInf = 'INFQUATORZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 14);
}

function __btn_QUINZE(controlBar) {
    glb_btnCtlSupInf = 'INFQUINZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 15);
}

function __btn_DEZESSEIS(controlBar) {
    glb_btnCtlSupInf = 'INFDEZESSEIS';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 16);
}

// FINAL DE EVENTOS DO CONTROL BAR INFERIOR *************************

// FUNCOES CHAMADAS PELOS EVENTOS DO GRID ***************************

/********************************************************************
Substituida por js_fg_EnterCell() do js_gridex.js
para Novas Features
********************************************************************/
function fg_EnterCell() {
    if (fg.Editable == true) {
        var sFldName = fg.ColKey(fg.Col);
        var lastPos = sFldName.length - 1;
        if (sFldName.substr(lastPos, 1) == '*') // ser for campo readOnly
            selectFirstCellNotReadOnly(fg, fg.Row, fg.Col, 0);
    }
    // invoca funcao no inf.js do form
    fg_EnterCell_Prg();
}

/********************************************************************
Substituida por js_fg_AfterRowColChange() do js_gridex.js
para Novas Features
********************************************************************/
function fg_AfterRowColChange() {
    if (glb_GridIsBuilding)
        return true;

    if (fg.Editable == true) {
        // impede de mudar de linha
        if (fg.Row != glb_currLineInGrid)
            fg.Row = glb_currLineInGrid;
    }

    // grid tem primeira linha de totalizacao
    if (glb_totalCols__) {
        if ((!fg.Editable) && (fg.Row == 1)) {
            if (fg.Rows > 2)
                fg.Row = 2;
        }
    }

    // invoca funcao no inf.js do form
    fg_AfterRowColChange_Prg();
}

function fg_MouseUp() {
    if (fg.Editable == true) {
        // impede de mudar de linha
        if (fg.Row != glb_currLineInGrid)
            fg.Row = glb_currLineInGrid;
    }

    // invoca funcao no inf.js do form
    fg_MouseUp_Prg();
}

function fg_MouseDown() {
    if (fg.Editable == true) {
        if ((fg.ColDataType(fg.Col) == 11) && (fg.Row != glb_currLineInGrid))
            fg.TextMatrix(fg.Row, fg.Col) = (fg.TextMatrix(fg.Row, fg.Col) == 0) ? 1 : 0;
    }

    // invoca funcao no inf.js do form
    fg_MouseDown_Prg();
}

function fg_BeforeEdit() {
    var fldDet = get_FieldDetails(glb_currDSO, fg.ColKey(fg.Col));

    // limita qtd de caracteres digitados em uma celula do grid
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        fg.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char
        fg.EditMaxLength = fldDet[1];
    else
        fg.EditMaxLength = 0;

    // invoca funcao no inf.js do form
    fg_BeforeEdit_Prg();
}

function fg_DblClick() {
    fg_DblClick_Prg();
}

function fg_ChangeEdit() {
    fg_ChangeEdit_Prg();
}

function fg_ValidateEdit() {
    fg_ValidateEdit_Prg();
}

/********************************************************************
Substituida por js_fg_BeforeRowColChange() do js_gridex.js
para Novas Features
********************************************************************/
function fg_BeforeRowColChange() {
    if (glb_GridIsBuilding)
        return true;

    fg_BeforeRowColChange_Prg();
}

// FINAL DE FUNCOES CHAMADAS PELOS EVENTOS DO GRID ******************

// FUNCOES GERAIS ***************************************************

/*
Ze em 17/03/08
*/
function window_OnUnload() {
    dealWithObjects_Unload();
}

/********************************************************************
Executa a primeira parte do window_onload

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function windowOnLoad_1stPart() {
    // Ze em 17/03/08
    document.body.onunload = window_OnUnload;
    dealWithObjects_Load();

    // A funcao abaixo sera envocada form a form
    // dealWithGrid_Load();
    // Fim de Ze em 17/03/08

    // Salva o id deste html (a mesma operacao e feita no pesqlist, no sup01
    // e no inf01)
    window.top.inf01ID = getHtmlId();

    // Seta o InternetTimeOut dos componentes RDS
    dsoFixedParams();

    // Define classe para grid
    // ver funcao after_SetupPage()
}

/********************************************************************
Executa a segunda parte do window_onload

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function windowOnLoad_2ndPart() {
    var i;

    // Inicia carregamento dos combos estaticos do arquivo se o form tem
    // combos desta natureza (se glb_aStaticCombos != null)
    getStaticCmbsData(glb_aStaticCombos, 'INF');

    // configura o body do html
    with (getBodyRef()) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // Operacoes da automacao que estavam no setupPage
    before_SetupPage();

    // Configura elementos do arquivo
    setupPage();

    // Operacoes da automacao que estavam no setupPage
    after_SetupPage();

    // Guarda ID da pasta default. E a pasta que e mostrada quando as outras
    // nao atendem as operacoes.
    glb_pastaDefault = 20008; // Observacoes

    // Coloca atributos especiais em controles
    // funcao particular do inf01.asp
    putSpecialAttributesInControls();

    // Garante selecao de texto nos campos input text and textarea
    setupEventOnFocusInTextFields();

    // Garante disparo funcao optChangedInCmb(this) no inf para combos
    setupEventOnChangeInSelects();

    // Garante mudanca de checkboxes ao clicar o label correspondente
    setupEventInvertChkBox();

    // Converge click em botao de lupa para a funcao onClickBtnLupa()
    setupEventOnClickInBtnsLupa();

    // Previne backspace em selects para impedir retorno
    // a pagina anteriormente carregada
    preventBkspcInSel(window);

    // Garante montagem do array de controles para travamento de interface
    lockControls(true, true);

    // Garante mandar quebra de linhas para o arquivo no campo observacao
    if (window.document.getElementById('txtObservacoes') != null)
        txtObservacoes.wrap = 'hard';
    // Garante mandar quebra de linhas para o arquivo no campo homologacao
    if (window.document.getElementById('txtHomologacao') != null)
        txtHomologacao.wrap = 'hard';

    // Garante entrada do grid inicializado em branco
    var elem = window.document.getElementById('fg');
    if (elem != null)
        resetGridInterface();

    // Translada interface pelo dicionario do sistema
    translateInterface(window, null);
}

/********************************************************************
Executa operacoes que estavam no setupPage do lado do programador

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function before_SetupPage() {
    var coll;
    var i;
    var aAdjDiv = new Array();

    // Ajusta os divs e seus elementos

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        aAdjDiv[i] = new Array(1, null);
        aAdjDiv[i][1] = coll.item(i).id;
    }

    adjustDivs('inf', aAdjDiv);

    /*** Prop Alt - Div secundario inferior divInf01_02 ***/
    if (window.document.getElementById('lblProprietario') != null) {
        if (((window.document.getElementById('lbldtValidade') == null) ||
             (window.document.getElementById('txtdtValidade') == null)) &&
             ((window.document.getElementById('lbldtProprietario') == null) ||
             (window.document.getElementById('txtdtProprietario') == null)))

            adjustElementsInForm([['lblProprietario', 'selProprietario', 20, 1],
                                  ['btnFindProprietario', '', 21, 1],
                                  ['lblAlternativo', 'selAlternativo', 20, 1],
                                  ['btnFindAlternativo', '', 21, 1]]);
        else
            adjustElementsInForm([['lblProprietario', 'selProprietario', 20, 1],
                                  ['btnFindProprietario', '', 21, 1],
                                  ['lblAlternativo', 'selAlternativo', 20, 1],
                                  ['btnFindAlternativo', '', 21, 1],
                                  ['lbldtProprietario', 'txtdtProprietario', 10, 1],
                                  ['lbldtValidade', 'txtdtValidade', 10, 1],
                                  ['lblDiasValidade', 'txtDiasValidade', 5, 1]]);
    }
}

/********************************************************************
Executa operacoes que estavam no setupPage do lado do programador

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function after_SetupPage() {
    // txtObservacoes do divInf01_01
    if (window.document.getElementById('txtObservacoes') != null) {
        txtObservacoes.disabled = false;
        with (txtObservacoes.style) {
            left = 10;
            top = 10;
            width = MAX_FRAMEWIDTH - (ELEM_GAP * 2);
            height = (MAX_FRAMEHEIGHT / 2) - (ELEM_GAP * 7) + 6;
        }
    }

    // Por padrao os combos de proprietario e alternativo sao travados
    if (window.document.getElementById('selProprietario') != null)
        selProprietario.disabled = true;
    if (window.document.getElementById('selAlternativo') != null)
        selAlternativo.disabled = true;

    // Inicializa variavel que mantem referencia o dso corrente 
    glb_currDSO = dso01JoinSup;

    // Ajusta campos comuns a todos os forms (txtNulo, txtRegistroID e txtEstadoID)
    adjustSupInfCommonFields();

    // SubForms com grids, ajusta posicao
    var coll, eDiv, elem;
    var i, j;

    coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        // O div
        eDiv = coll.item(i);

        for (j = 0; j < eDiv.children.length; j++) {
            // Elemento contido no div
            elem = eDiv.children.item(j);

            // O div tem grid
            if (elem.tagName.toUpperCase() == 'OBJECT') {
                if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072') {
                    with (eDiv.style) {
                        left = 10;
                        top = 10;
                        backgroundColor = 'transparent';
                    }

                    // FG - grid
                    // coloca classe no grid
                    elem.className = 'fldGeneral';

                    with (elem.style) {
                        left = 0;
                        top = 0;
                        width = MAX_FRAMEWIDTH - (ELEM_GAP * 2);
                        //height = (MAX_FRAMEHEIGHT / 2) - (ELEM_GAP * 7) + 6;
                        // alteracao para 1024 em 14/07/2010
                        height = (MAX_FRAMEHEIGHT / 2) - (ELEM_GAP * 7) + 6 + 35;
                    }

                    // alteracao para 1024 em 14/07/2010
                    eDiv.style.height = elem.offsetHeight + ELEM_GAP;

                    break;
                }
            }
        }
    }

    //Divs com formatacao fora do padrao do frame work
    adjustElementsInDivsNonSTD();
}

/********************************************************************
Recebe o click do usuario em todos os botoes de lupa
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function onClickBtnLupa() {
    var elem = this;

    if (elem != null) {
        if (elem.id.toUpperCase() == 'BTNFINDPROPRIETARIO') {
            elem.src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_Find.gif';
            onClickPropAlt(elem);
            return true;
        }
        else if (elem.id.toUpperCase() == 'BTNFINDALTERNATIVO') {
            elem.src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_Find.gif';
            onClickPropAlt(elem);
            return true;
        }
    }

    // Se nao e botao proprietario ou alternativo,
    // propaga para o inf do form
    btnLupaClicked(elem);
}

// FINAL DE FUNCOES GERAIS ******************************************

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Informa que devem ser executadas as operacoes de bancos
de dados da automacao, relativas ao inf
           
Parametros: 
btnClicked      - botao clicado no control bar (sup ou inf)

Retorno:
nenhum
********************************************************************/
function lockAndOrSvrInf_SYS(btnClicked) {
    if (glb_DetInfTimerVar != null) {
        window.clearInterval(glb_DetInfTimerVar);
        glb_DetInfTimerVar = null;
    }

    // Salva botao clicado no control bar
    glb_btnCtlSupInf = btnClicked;

    var bForwardChangeDiv = true;

    // Trava a interface
    lockInterface(true);

    // Salva regID e, se aplicavel, typeRegID
    keepRegIDAndTypeID();

    // Se nao tem pasta selecionada, preenche combo de pastas
    if ((!((glb_btnCtlSupInf == 'SUPDET') || (glb_btnCtlSupInf == 'SUPREFR'))) && (getCmbCurrDataInControlBar('inf', 1) == null))
        fillCmbPastasInCtlBarInf_Aut(glb_nTipoRegistroID);

    // Detalhe vindo do pesqlist ou refresh no sup (inclui paginar no sup)
    // Obter dados no servidor relativos ao registro corrente no sup    
    if ((glb_btnCtlSupInf == 'SUPDET') || (glb_btnCtlSupInf == 'SUPREFR')) {
        // zera variavel de paginacao 26/12/2001
        glb_nPAGENUMBER = 1;

        fillCmbPastasInCtlBarInf_Aut(glb_nTipoRegistroID);
        // garante o repreenchimento do combo de filtros inferior
        // se o programador nao faz manual
        if (fillCmbFiltsInfAutomatic(keepCurrFolder()) == true)
            refillCmb2Inf(keepCurrFolder());

        bForwardChangeDiv = false;

        // ***INF_NO_DATA***
        if (glb_bInfNoData != null) {
            // Zera os dados mostrados na pasta corrente, se for grid
            if (folderHasGrid(keepCurrFolder()) > 0) {
                with (fg) {
                    Redraw = 0;
                    Rows = 1;
                    Redraw = 2;
                }
                //sendJSMessage(getHtmlId(), JS_INFSYS, 'PROGSERVER', glb_btnCtlSupInf);                
                sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', glb_btnCtlSupInf);
                return true;
            }
        }
        // ***INF_NO_DATA***

        // Recolhe dados do inf no servidor
        getDataOfInfInServer();
    }

    if (glb_btnCtlSupInf == 'INFREFR') {
        bForwardChangeDiv = false;

        writeInStatusBar('child', 'cellMode', 'Refresh', true);

        // Recolhe dados do inf no servidor
        getDataOfInfInServer();
    }

    if (bForwardChangeDiv == true)
        sendJSMessage(getHtmlId(), JS_INFSYS, 'PROGSERVER', glb_btnCtlSupInf);
}

/********************************************************************
Esta funcao esconde todos os divs e mostra apenas os coerentes com o
modo atual do sistema
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function doChangeDivInf_SYS(btnClicked) {
    // @@123 Informa se o grid deve executar o auto size
    var bExecuteGridAutoSize = false;

    // Salva botao clicado no control bar
    glb_btnCtlSupInf = btnClicked;

    // Destrava a interface
    lockInterface(false);

    // Nao preenche o combo1 do control bar inf:
    // se veio do pesqlist para detalhe (25/03/2002)
    // se foi refresh no sup
    // se foi refresh no inf
    // se foi inclusao no inf
    // se foi edicao no inf
    // se foi exclusao no inf
    // se foi gravacao no inf
    // se foi cancelamento no inf
    if (!((btnClicked == 'SUPDET') ||
           (btnClicked == 'SUPREFR') ||
           (btnClicked == 'INFREFR') ||
           (btnClicked == 'INFINCL') ||
           (btnClicked == 'INFEXCL') ||
           (btnClicked == 'INFOK') ||
           (btnClicked == 'INFCANC')))
    // Preenche combo de pastas ajustando a pasta corrente
        fillCmbPastasInCtlBarInf_Aut(glb_nTipoRegistroID);

    // Vindo do sup inclusao, seleciona a pasta default no combo1
    // do control bar inferior, esconde as pastas inferiores
    // e segue para configurar os controls bars
    if (btnClicked == 'SUPINCL') {
        supIsInclOrEdit();

        // Mostra as interface do sup e do inf
        showDetailInterface();

        // Coloca foco no primeiro campo editavel do sup
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'FOCUS1STFIELD', null);

        // Informa que trocou os divs e prossegue
        // chamando a funcao prgInterfaceInf(btnClicked)
        // no xxx_inf.js do form
        sendJSMessage(getHtmlId(), JS_INFSYS, 'AFTERCHANGEDIV', btnClicked);

        return null;
    }

    // Se a pasta corrente e grid monta o grid
    if ((glb_currDSO.id.toUpperCase()).indexOf('GRID') >= 0) {
        // ***INF_NO_DATA***
        // Nao monta o grid se SUPDET, SUPREFR e glb_bInfNoData != null
        if (!((btnClicked == 'SUPDET' || btnClicked == 'SUPREFR') && (glb_bInfNoData != null))) {
            var nFolderSelected = getCmbCurrDataInControlBar('inf', 1);

            buildGridFromFolder(nFolderSelected[1]);
            if (glb_currLineInGrid > 0) {
                if (glb_currLineInGrid < fg.Rows) {
                    fg.Row = glb_currLineInGrid;
                    fg.TopRow = glb_currLineInGrid;
                    window.focus();
                    fg.focus();
                }
            }

        }

        // @@123
        bExecuteGridAutoSize = true;

        // ***INF_NO_DATA***    
    }

    // Detalhe vindo do pesqlist ou refresh no sup
    if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR') || (btnClicked == 'INFREFR')) {
        // Mostra as interface do sup e do inf
        showDetailInterface();
    }

    // if colocado abaixo em 02/10/2001
    // Nao chama showCompatiblePasta()
    // se foi exclusao no inf
    // se foi gravacao no inf
    // se foi cancelamento no inf
    if (!((btnClicked == 'INFINCL') ||
           (btnClicked == 'INFEXCL') ||
           (btnClicked == 'INFOK') ||
           (btnClicked == 'INFCANC')))
    // Mostra a pasta compativel   
        showCompatiblePasta();

    // @@123
    if (bExecuteGridAutoSize) {
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
    }

    // Informa que trocou os divs e prossegue
    // chamando a funcao prgInterfaceInf(btnClicked)
    // no xxx_inf.js do form
    sendJSMessage(getHtmlId(), JS_INFSYS, 'AFTERCHANGEDIV', btnClicked);
}

/********************************************************************
Esta funcao e invocada quando o sup entra em modo de inclusao ou
alteracao. Volta o combo de pastas para a pasta default e esconde todos
os divs.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function supIsInclOrEdit() {
    // Se a pasta corrente antes da troca de pastas pela pasta
    // default tem dois grids, esconde o segundo grid
    var nQtyGridsInFolder = folderHasGrid();

    selOptByValueOfSelInControlBar('inf', 1, glb_pastaDefault, true);

    nQtyGridsInFolder = folderHasGrid();

    // Pasta corrente nao tem grid
    if (nQtyGridsInFolder == 0)
        glb_currDSO = dso01JoinSup;
    // Pasta corrente tem 1 grid
    else if (nQtyGridsInFolder == 1) {
        if (ascan(glb_aFoldersPaging, glb_pastaDefault, false) >= 0)
            glb_currDSO = dso01PgGrid;
        else
            glb_currDSO = dso01Grid;
    }

    setCurrDSOByCmb1(glb_pastaDefault, false);
    showDivBySFS(glb_aDIVS, -1);
}

/********************************************************************
Mostra a interface sup/inf e registra o control bar superior para
sup e o inferior para o inf.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function showDetailInterface() {
    var frameID;

    // Mostra o sup01.asp
    frameID = getFrameIdByHtmlId(window.top.sup01ID);
    if (frameID) {
        showFrameInHtmlTop(frameID, true);
        // Registra o control bar superior
        regHtmlInControlBar('sup', window.top.sup01ID);
    }

    // Mostra o control bar inferior
    showControlBar('inf', true);

    // Mostra o inf01.asp
    frameID = getFrameIdByHtmlId(window.top.inf01ID);
    if (frameID) {
        showFrameInHtmlTop(frameID, true);
        // Registra o control bar inferior
        regHtmlInControlBar('inf', window.top.inf01ID);
    }

    // Esconde o tiposauxpesqlist.asp
    frameID = getFrameIdByHtmlId(window.top.pesqListID);
    if (frameID)
        showFrameInHtmlTop(frameID, false);
}

/********************************************************************
Esta funcao mostra a pasta compativel com o registro do sup.
Quando um registro novo e gravado no sup esta funcao executa ao final
da gravacao.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function showCompatiblePasta() {
    // Mostra a pasta coerente
    var currSelection;
    var divToShow;

    // Salva regID e, se aplicavel, typeRegID
    keepRegIDAndTypeID();

    // Recarrega o combo1 de pastas do control bar inferior
    // condicao if abaixo colocado em 25/03/2002
    if (!((glb_btnCtlSupInf == 'SUPDET') || (glb_btnCtlSupInf == 'SUPREFR')))
        fillCmbPastasInCtlBarInf_Aut(glb_nTipoRegistroID);

    // Pasta selecionada no combo e correspondente div    
    currSelection = getCmbCurrDataInControlBar('inf', 1);
    // o combo de pastas esta vazio
    if (currSelection == null) {
        divIdToShow = -1;
        showDivBySFS(glb_aDIVS, divIdToShow);
        return null;
    }
    else
        divIdToShow = currSelection[1];

    // Tenta mostrar o div da pasta selecionada no combo
    if (!showDivBySFS(glb_aDIVS, divIdToShow)) {
        // Mostra o div da pasta default
        divIdToShow = glb_pastaDefault;
        showDivBySFS(glb_aDIVS, divIdToShow);
    }

    //if ( glb_btnCtlSupInf == 'SUPOK' )
    //    adjustInfBar__();
}

/********************************************************************
Final de operacoes de servidor e interfaces.
Tratar a botoeira e escrever na barra de status aqui.
           
Parametros: 
btnClicked  - ultima barra (superior/inferior) e botao clicado

Retorno:
nenhum
********************************************************************/
function finalOfLockAndOrSvr_SYS(btnClicked) {
    // Salva botao clicado no control bar
    glb_btnCtlSupInf = btnClicked;

    // Sup entrou em modo de inclusao
    if (btnClicked == 'SUPINCL') {
        adjustSupInfControlsBar('SUPINCL');
        writeInStatusBar('child', 'cellMode', 'Inclus�o');
    }
    // Detalhe vindo do pesqlist ou refresh no sup
    else if ((btnClicked == 'SUPDET') || (btnClicked == 'SUPREFR')) {
        adjustSupInfControlsBar('SUPCONS');
        adjustSupInfControlsBar('INFCONS', null, null, null, fg, glb_currDSO);

        writeInStatusBar('child', 'cellMode', 'Detalhe');
    }
    // Inf terminou refresh, uma gravacao, uma gravacao de alteracao de estado
    // ou uma delecao
    else if ((btnClicked == 'INFREFR') || (btnClicked == 'INFOK') ||
              (btnClicked == 'INFEXCL') || (btnClicked == 'INFEST')) {
        adjustSupInfControlsBar('SUPCONS');
        adjustSupInfControlsBar('INFCONS', null, null, null, fg, glb_currDSO);

        writeInStatusBar('child', 'cellMode', 'Detalhe');
    }

    // Pasta selecionada no combo e correspondente div    
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    // o combo de pastas esta vazio
    if (currSelection == null) {
        // trava a barra inferior toda
        setupControlBar('inf', 'disable', 'disable', 'DDDDDDDDDDDDDDD');
    }

    // coloca foco no ultimo combo usado na barra inferior
    setFocusInCmbOfControlBar('inf', lastCmbSelectInCtrlBar('inf'));

    // Mensagem ao programador para tratar particularidades das
    // barras de botoes na funcao treatsControlsBars(btnClicked)
    // do xxx_inf.js

    sendJSMessage(getHtmlId(), JS_INFSYS, 'TREATCTLBARS', btnClicked);
}

/********************************************************************
Salva o registro ID e, se o form tem, o tipo de registro ID
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function keepRegIDAndTypeID() {
    // Guarda registro corrente no sup
    glb_registroID = sendJSMessage(getHtmlId(), JS_DATAINFORM,
                                 'REGISTROID', null);

    // Tipo do registro (so tem em alguns forms)
    // quando tem volta um numero != 0, caso contrario volta 0
    // tambem volta zero se em modo de inclusao
    glb_nTipoRegistroID = sendJSMessage(getHtmlId(), JS_DATAINFORM,
                                        'TIPOREGISTROID', null);
}

/********************************************************************
Retorna ID da pasta corrente
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function keepCurrFolder() {
    var ret = null;

    var pastaData = getCmbCurrDataInControlBar('inf', 1);

    if (pastaData != null)
        ret = pastaData[1];

    return ret;
}

/********************************************************************
Retorna referencia ao dso corrente
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function keepCurrDso() {
    return glb_currDSO;
}

/********************************************************************
Coloca pasta corrente em edicao
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function editCurrFolder() {
    glb_antGridMode = 'ALT';

    // Campos entram em modo de edi��o
    lockControls(false, true);

    // A pasta corrente e grid
    if ((glb_currDSO.id.toUpperCase()).indexOf('GRID') >= 0) {
        setColumnWidthToEdition(fg, glb_currDSO);

        with (fg) {
            glb_currLineInGrid = fg.Row;

            selectFirstCellNotReadOnly(fg, fg.Row, fg.Col, 0);

            Editable = gridIsEditable(fg);

            SelectionMode = 0;

            //fg.Select (fg.Row, fg.Col, fg.Row, fg.Col);

            if (fg.disabled == false)
                fg.focus();
        }
    }
    // A pasta corrente sao campos
    else {
        btnAltPressedInNotGrid(keepCurrFolder());

        // Pasta de Proprietario e Alternativo
        if (keepCurrFolder() == 20009) {
            try {
                if (selProprietario.options.length > 0)
                    selProprietario.disabled = false;

                if (selAlternativo.options.length > 0)
                    selAlternativo.disabled = false;
            }
            catch (e) {
                ;
            }
        }

        // Foca o primeiro campo habilitado
        setFocus1Field();
    }

    writeInStatusBar('child', 'cellMode', 'Altera��o');

    adjustControlBarEx(window, 'sup', 'DDDDDDDDDDDDDDD');
    adjustControlBarEx(window, 'inf', 'DDDDDDHHDDDDDDD');

    // Dispara funcao no form se grid
    if ((glb_currDSO.id.toUpperCase()).indexOf('GRID') >= 0)
        btnAltPressedInGrid(keepCurrFolder());
}

/********************************************************************
Cancela edicao da pasta corrente
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function cancelEditCurrFolder() {
    var x = glb_currDSO.id;
    var currLine = 0;
    var aItemSelected = getCmbCurrDataInControlBar('inf', 1);
    var folderID = aItemSelected[1];

    // Se estava atuando em campos
    if (x.lastIndexOf('Grid') < 0) // se nao e grid
    {
        // Efeito estetico do cursor de edicao
        document.getElementById('txtNulo').focus();

        // Trava controles
        lockControls(true, true);

        // Atualiza o buffer com os dados da p�gina.
        updateHTMLFields();

        // Refresh se pasta e prop/alt
        if (keepCurrFolder() == 20009) {
            __btn_REFR('inf');
            return null;
        }
    }

    // Se estava atuando em grid
    if (x.lastIndexOf('Grid') >= 0) {

        fg.Editable = false;
        fg.SelectionMode = 1;

        // Grid estava em modo de alteracao
        // guarda a linha editada, chama o fillgrid e posiciona na
        // linha editada
        if (glb_antGridMode == 'ALT') {
            currLine = keepCurrLineInGrid();
            var nFolderSelected = getCmbCurrDataInControlBar('inf', 1);
            buildGridFromFolder(nFolderSelected[1]);
            fg.Row = currLine;
        }
        // Grid estava em modo de inclusao
        // deleta linha do grid, e volta para linha anterior
        else if (glb_antGridMode == 'INCL') {
            currLine = keepCurrLineInGrid();
            fg.RemoveItem(currLine);
            if (currLine > 1)
                fg.Row = currLine - 1;
            else if (currLine == 1 && fg.Rows > 1)
                fg.Row = 1;
            else
                fg.Row = 0;

            glb_currLineInGrid = fg.Row;

        }

        // Destrava a interface
        lockInterface(false);
        // Destrava controles
        lockControls(true, true);

        // @@123            
        fg.Redraw = 0;

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);

        fg.Redraw = 2;
    }

    // Configura os controls bars    
    adjustSupInfControlsBar('SUPCONS');
    adjustSupInfControlsBar('INFCONS', null, null, null, fg, glb_currDSO);

    // Escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Detalhe');

    // Chama funcao de ajustagem de control bar inferior do programador
    treatsControlsBars_Aut(glb_btnCtlSupInf, folderID, 'CTRLS', glb_currDSO.id);
    lockBtnsInCtrlBarInf_Grid();

    // Se estava atuando em grid
    if (x.lastIndexOf('Grid') >= 0) {
        window.focus();
        fg.focus();
    }
}

/********************************************************************
Seta glb_currDSO para o dso correspondente a pasta selecionada no
combo1 do control bar inferior
           
Parametros:
pastaID     - ID da pasta corrente 
dorefresh   - True executa o refresh

Retorno:
nenhum
********************************************************************/
function setCurrDSOByCmb1(pastaID, doRefresh) {
    // Preenche o combo inferior de filtros apropriadamente
    // ou nao, em funcao do programador
    if (fillCmbFiltsInfAutomatic(pastaID))
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'REFILLCMBINF', pastaID);

    // Nao executa se o dso nao esta inplementado
    if (glb_currDSO == null) {
        if (window.top.overflyGen.Alert('N�o implementado!') == 0)
            return null;
        return null;
    }

    if (doRefresh)
        __btn_REFR('inf');
}

/********************************************************************
Esta funcao forca o repreenchimento do combo de filtros inferior
           
Parametros:
pastaID     - ID da pasta corrente 

Retorno:
nenhum
********************************************************************/
function refillCmb2Inf(pastaID) {
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'REFILLCMBINF', pastaID);
}

/********************************************************************
Trata o campo registroID:
Os tipos de geracao do id do registro possiveis (so para inclusao,
alteracao o usuario nao pode alterar o id do registro):
15 - Banco gera o id do registro (identity  )
16 - A trigger gera o id do registro (identity)
17 - O usuario define o id do registro

window.top.registroID_Type = valores acima

Esta funcao e invocada antes da automacao do inf chamar a funcao
treatsControlsBars()

Esta funcao invoca a funcao treats_FldRegID_Sup no sup
           
Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function treats_FldRegID_Inf() {
    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'treats_FldRegID_Sup()');
}

/********************************************************************
Chama funcao do lado cliente para repreencher o combo de pastas
do control bar inferior
********************************************************************/
function fillCmbPastasInCtlBarInf_Aut(nTipoRegistroID) {
    fillCmbPastasInCtlBarInf(nTipoRegistroID);
}

/********************************************************************
Chama funcao do lado cliente para tratar estado dos botoes
do control bar inferior
********************************************************************/
function treatsControlsBars_Aut(btnClicked, folderID, tipoCtlrsPasta, dsoID) {
    treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID);

    if ((btnClicked == 'SUPEST') || (btnClicked == 'SUPDET') || (btnClicked == 'SUPOK') || (btnClicked == 'SUPREFR')) {
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'updateHTMLFields()');
    }
}

// FINAL DE FUNCOES DA AUTOMACAO ************************************

// FUNCOES DE ACESSO A DADOS NO SERVIDOR ****************************

/********************************************************************
Descricao: Obtem os dados do inf no servidor. Antiga cfgPgRefreshMode

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function getDataOfInfInServer() {
    // Ajusta o dso corrente
    var nQtyGridsInFolder = folderHasGrid();

    // Pasta corrente nao tem grid
    if (nQtyGridsInFolder == 0)
        glb_currDSO = dso01JoinSup;
    // Pasta corrente tem 1 grid que pode ser comum ou de paging
    else if ((nQtyGridsInFolder == 1) || (ascan(glb_aFoldersPaging, keepCurrFolder(), false) >= 0)) {
        if (ascan(glb_aFoldersPaging, keepCurrFolder(), false) >= 0)
            glb_currDSO = dso01PgGrid;
        else
            glb_currDSO = dso01Grid;
    }

    var aFiltroInf = getCmbCurrDataInControlBar('inf', 2);
    // marco
    // nao considera o filtro se o value for um array
    // que e usado nos grids de relacoes
    if (aFiltroInf != null) {
        if ((typeof (aFiltroInf[1])).toUpperCase() == 'STRING')
            if (aFiltroInf[1].length != 0) {
            var aValue = aFiltroInf[1].split(',');
            if (aValue.length > 1)
                aFiltroInf = null;
        }
    }

    if (aFiltroInf == null)
        filtroComplete_DSC(true);
    else {
        get_FiltroInf(dsoFiltroInf, aFiltroInf[1]);
        dsoFiltroInf.ondatasetcomplete = filtroComplete_DSC;
        dsoFiltroInf.Refresh();
    }
}

/********************************************************************
Descricao: Retorna a expresao correspondente ao filtro em uso no
control bar inferior, para complementar a string SQL que vai
carregar os dados do inf correspondentes ao registro corrente no sup.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function filtroComplete_DSC(emptyFilter) {
    var aItemSelected = getCmbCurrDataInControlBar('inf', 1);

    // O combo de pastas esta vazio
    if (aItemSelected == null) {
        finalDSOsCascade();
        return null;
    }

    var sFiltro = '';

    if (!emptyFilter) {
        if (!((dsoFiltroInf.recordset.EOF) && (dsoFiltroInf.recordset.BOF))) {
            if ((dsoFiltroInf.recordset['Filtro'].value != null) &&
                (dsoFiltroInf.recordset['Filtro'].value != '')) {
                sFiltro = '(' + dsoFiltroInf.recordset['Filtro'].value + ') AND ';
            }
        }
    }

    // garante uso do dso por SQL ou URL
    if (glb_currDSO.URL != '')
        glb_currDSO.URL = '';

    // se a pasta corrente tem paginacao, ajusta variaveis
    initializeVarsToShowPagingGrid();

    var sComplete = setSqlToCurrDso(glb_currDSO,
                    aItemSelected[1], glb_registroID, sFiltro, glb_nTipoRegistroID);

    if (sComplete != null) {
        // Necessario porque vai carregar nova pasta no grid
        glb_currLineInGrid = -1;

        glb_currDSO.ondatasetcomplete = eval(sComplete);

        try {
            glb_currDSO.Refresh();
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Filtro inv�lido') == 0)
                return null;

            lockInterface(false);
        }
    }
    else
        finalDSOsCascade();
}

/********************************************************************
Retorno do servidor com os dados do registro corrente no sup no caso
de pastas grids.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function dso01Grid_DSC() {
    var i, j;
    var bHasCombo = false;

    // Tratamento de erro na paginacao de grid
    if (glb_useParallelDSO == true) {
        for (i = 0; i < glb_currDSO.recordset.fields.count; i++) {
            if (glb_currDSO.recordset.fields[i].name == 'fldError')
                if (!((glb_currDSO.recordset.BOF) && (glb_currDSO.recordset.EOF)))
                if ((glb_currDSO.recordset['fldError'].value != null) &&
                         (glb_currDSO.recordset['fldError'].value != '')) {
                if (window.top.overflyGen.Alert(glb_currDSO.recordset['fldError'].value) == 0)
                    return null;
                glb_sFilterPesq = '';
                glb_sArgument = '';
                glb_nPAGENUMBER = 1;
                glb_DetInfTimerVar = window.setInterval('lockAndOrSvrInf_SYS(' + '\'' + 'INFREFR' + '\'' + ')', 10, 'JavaScript');
                return null;
            }
        }
    }

    glb_nCombosInCurrGrid = 1;

    // Trap se nao tem combos estaticos em grids
    if (glb_aGridStaticCombos == null) {
        var testVar = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_btnCtlSup');
        //O trapeado abaixo funciona, o liberado e mais geral
        //if ( (testVar == 'SUPDET') || (testVar == 'SUPREFR') )
        if ((testVar.toUpperCase()).indexOf('SUP', 0) >= 0)
            finalDSOsCascade();
        else
            doChangeDivInf_SYS('INFREFR');

        return null;
    }

    // ID da pasta corrente
    var pastaData = getCmbCurrDataInControlBar('inf', 1);
    var pastaID = pastaData[1];

    var dso;

    // Tem combos estaticos no grid    
    // glb_aGridStaticCombos = [ [20010, [[1, 'dsoCmb01Grid_01']] ];

    for (i = 0; i < glb_aGridStaticCombos.length; i++) {
        if (pastaID == glb_aGridStaticCombos[i][0]) {
            bHasCombo = false;

            //glb_nCombosInCurrGrid = glb_aGridStaticCombos[i][1].length;
            glb_nCombosInCurrGrid = 0;

            for (j = 0; j < glb_aGridStaticCombos[i][1].length; j++) {
                if (glb_aGridStaticCombos[i][1][j].length == 5) {
                    if (glb_aGridStaticCombos[i][1][j][4] != glb_currDSO.id)
                        continue;
                }

                bHasCombo = true;
                glb_nCombosInCurrGrid++;
            }

            for (j = 0; j < glb_aGridStaticCombos[i][1].length; j++) {
                if (glb_aGridStaticCombos[i][1][j].length == 5) {
                    if (glb_aGridStaticCombos[i][1][j][4] != glb_currDSO.id)
                        continue;
                }

                dso = eval(glb_aGridStaticCombos[i][1][j][1]);
                setSqlToDsoCmbOrLkpGrid(dso, pastaID, glb_registroID, glb_nTipoRegistroID);
                dso.ondatasetcomplete = setSqlToDsoCmbOrLkpGrid_DSC;
                dso.Refresh();
            }
        }
    }

    // O grid nao tem combos estaticos
    if (!bHasCombo) {
        glb_nCombosInCurrGrid = 1;
        setSqlToDsoCmbOrLkpGrid_DSC();
    }

}

/********************************************************************
Retorno do servidor com os dados do combo do grid cujos dados voltaram
do servidor na funcao dso01Grid_DSC().

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid_DSC() {
    glb_nCombosInCurrGrid--;

    if (glb_nCombosInCurrGrid != 0)
        return null;

    // Excepcionalidade da gravacao e da delecao, volta para o framework
    if (glb_btnCtlSupInf == 'INFOK' || glb_btnCtlSupInf == 'INFEXCL')
        doChangeDivInf_SYS(glb_btnCtlSupInf);
    // Excepcionalidade da gravacao da maquina de estado,
    // reconstroi o grid e vai direto para finalOfLockAndOrSvr_SYS
    else if (glb_btnCtlSupInf == 'INFEST') {
        // Se a pasta corrente e grid monta o grid
        if ((glb_currDSO.id.toUpperCase()).indexOf('GRID') >= 0) {
            var nFolderSelected = getCmbCurrDataInControlBar('inf', 1);

            buildGridFromFolder(nFolderSelected[1]);
            if (glb_currLineInGrid > 0) {
                if (glb_currLineInGrid < fg.Rows)
                    fg.Row = glb_currLineInGrid;
            }
        }

        finalOfLockAndOrSvr_SYS(glb_btnCtlSupInf);
    }
    else
        finalDSOsCascade();
}

/********************************************************************
Retorno do servidor com os dados do registro corrente no sup no caso
de pastas nao grids.
No minimo: regID, observacoes, propID e alternativoID

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function dso01JoinSup_DSC() {
    var currPastaData = getCmbCurrDataInControlBar('inf', 1);
    var currPastaID;

    // Limita quantidade de caracteres que podem ser digitados
    // nos controles
    if ((glb_currDSO.id).lastIndexOf('Grid') < 0)
        fldMaxLen(glb_currDSO);

    if (currPastaData != null)
        currPastaID = currPastaData[1];

    // Se o combo1 do control bar inferior esta com a pasta
    // proprietario alternativo
    if (currPastaID == 20009)
        getFantasiaPropAlt();
    else
        finalDSOsCascade();
}

/********************************************************************
Funcao executada por todo final de cascatas de DSOs.
Informa o xxx_inf01 para executar operacoes de servidor do programador
chamando a funcao prgServerInf.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalDSOsCascade() {
    // Configura campos de digitacao
    // e arrays de critica da gravacao
    // no inf refaz os arrays de campos sempre
    // if abaixo colocado em 07/11/2001 e comentado em 12/11/2001
    //if ( glb_currDSO.getAttribute('setControls', 1) == false )
    // novo if colocado em 20/11/2001 faz a operacao se pasta corrente
    // nao e de grid
    if ((glb_currDSO.id.toUpperCase()).indexOf('GRID') < 0) {
        glb_currDSO.setAttribute('setControls',
                             maxLenOnKeyPressSetArrays(glb_currDSO), 1);
        // se nao foi implementado implementa
        if (glb_implementsPasteInTxtFld == false) {
            __implementsPasteInTxtFld();
            glb_implementsPasteInTxtFld = true;
        }
    }

    // chama funcao no inf do form, apos zerar o array
    glb_BtnsIncAltEstExcl[0] = null;
    glb_BtnsIncAltEstExcl[1] = null;
    glb_BtnsIncAltEstExcl[2] = null;
    glb_BtnsIncAltEstExcl[3] = null;
    frameIsAboutToApplyRights(keepCurrFolder());

    // Se vindo da lista para o detalhe ou paginando,
    // ajusta a barra superior
    var prop1 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              "dsoSup01.recordset['Prop1'].value");
    var prop2 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              "dsoSup01.recordset['Prop2'].value");

    adjustControlBarInfByRights(prop1, prop2);

    // A pasta corrente
    var aItemSelected = getCmbCurrDataInControlBar('inf', 1);
    // O combo de pastas esta vazio
    if (aItemSelected == null) {
        // inicia operacoes de interface
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', 'SUPREFR');
        return null;
    }

    updateHTMLFields();

    sendJSMessage(getHtmlId(), JS_INFSYS, 'PROGSERVER', glb_btnCtlSupInf);
}

// FINAL DE FUNCOES DE ACESSO A DADOS NO SERVIDOR *******************

// FUNCOES DE PROPRIETARIO E ALTERNATIVO ****************************

/********************************************************************
Usuario selecionou um proprietario ou alternativo no combo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setPropAlt() {
    // Nao faz nada. So para manter compatibilidade com o frame work
}

function onClickPropAlt(control) {
    var htmlPath;
    var strPars;

    strPars = '?sCaller=';

    // se o botao de Proprietario foi clicado
    if ((control.id).toUpperCase() == 'BTNFINDPROPRIETARIO')
        strPars += escape('PROP');
    // se o botao de Alternativo foi clicado
    else if ((control.id).toUpperCase() == 'BTNFINDALTERNATIVO')
        strPars += escape('ALT');

    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalpropalt.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
}

function loadComboProprietario(aData) {
    clearComboEx(['selProprietario']);
    var oldSrc = selProprietario.dataSrc;
    var oldDtf = selProprietario.dataFld;
    selProprietario.dataSrc = '';
    selProprietario.dataFld = '';

    var optionStr, optionValue;

    var oOption = document.createElement("OPTION");
    oOption.text = aData[1];
    oOption.value = aData[0];
    document.all.selProprietario.add(oOption);

    dso01JoinSup.recordset['ProprietarioID'].value = aData[0];

    document.all.selProprietario.selectedIndex = 0;

    selProprietario.dataSrc = oldSrc;
    selProprietario.dataFld = oldDtf;

    setLabelOfControlEx(lblProprietario, selProprietario);

    // Se eh para gravar o alternativo com o mesmo dado do proprietario
    if (aData[2]) {
        loadComboAlternativo(aData);
        return null;
    }

    // destrava a interface e fecha a janela modal
    if (restoreInterfaceFromModal()) {
        // destrava se tem proprietarios
        if (selProprietario.options.length != 0) {
            // coloca foco no combo selProprietario
            selProprietario.disabled = false;
            selProprietario.focus();
        }
    }
}

function loadComboAlternativo(aData) {
    clearComboEx(['selAlternativo']);
    var oldSrc = selAlternativo.dataSrc;
    var oldDtf = selAlternativo.dataFld;
    selAlternativo.dataSrc = '';
    selAlternativo.dataFld = '';

    var optionStr, optionValue;

    var oOption = document.createElement("OPTION");
    oOption.text = aData[1];
    oOption.value = aData[0];
    document.all.selAlternativo.add(oOption);

    dso01JoinSup.recordset['AlternativoID'].value = aData[0];
    document.all.selAlternativo.selectedIndex = 0;

    selAlternativo.dataSrc = oldSrc;
    selAlternativo.dataFld = oldDtf;

    setLabelOfControlEx(lblAlternativo, selAlternativo);

    // destrava a interface e fecha a janela modal
    if (restoreInterfaceFromModal()) {
        // destrava se tem alternativos
        if (selAlternativo.options.length != 0) {
            // coloca foco no combo selAlternativo
            selAlternativo.disabled = false;
            selAlternativo.focus();
        }
    }
}

/********************************************************************
Atualiza combos selProprietario e selAlternativo.
Entrando em detalhe, refrescando paginando e apos a gravacao
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function getFantasiaPropAlt() {
    // Limpa os combos
    clearComboEx(['selProprietario']);
    clearComboEx(['selAlternativo']);

    setConnection(dsoCmbsPropAlt);

    dsoCmbsPropAlt.SQL = 'SELECT PessoaID, Fantasia ' +
                         'FROM Pessoas ' +
                         'WHERE PessoaID = ' + dso01JoinSup.recordset['ProprietarioID'].value + ' OR ' +
                              'PessoaID = ' + dso01JoinSup.recordset['AlternativoID'].value;

    dsoCmbsPropAlt.ondatasetcomplete = dsoCmbsPropAlt_DSC;
    dsoCmbsPropAlt.Refresh();
}

function dsoCmbsPropAlt_DSC() {
    var optionStr, optionValue;
    var oldDataSrcProp, oldDataFldProp;
    var oldDataSrcAlt, oldDataFldAlt;
    var oOption;
    var bPropAltDiferente = true;

    // Combo sel proprietario 
    oldDataSrcProp = selProprietario.dataSrc;
    oldDataFldProp = selProprietario.dataFld;
    selProprietario.dataSrc = '';
    selProprietario.dataFld = '';

    // Combo sel alternativo 
    oldDataSrcAlt = selAlternativo.dataSrc;
    oldDataFldAlt = selAlternativo.dataFld;
    selAlternativo.dataSrc = '';
    selAlternativo.dataFld = '';

    while (!dsoCmbsPropAlt.recordset.EOF) {
        optionStr = dsoCmbsPropAlt.recordset['Fantasia'].value;
        optionValue = dsoCmbsPropAlt.recordset['PessoaID'].value;
        oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        if (dso01JoinSup.recordset['ProprietarioID'].value == dso01JoinSup.recordset['AlternativoID'].value) {
            selProprietario.add(oOption);
            oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            selAlternativo.add(oOption);
            bPropAltDiferente = false;
            break;
        }
        else if (optionValue == dso01JoinSup.recordset['ProprietarioID'].value)
            selProprietario.add(oOption);
        else if (optionValue == dso01JoinSup.recordset['AlternativoID'].value)
            selAlternativo.add(oOption);

        dsoCmbsPropAlt.recordset.MoveNext();
    }

    // Se o proprietario for diferente do alternativo
    // Acrescenta o option do combo de proprietario no combo de alternativo
    if (bPropAltDiferente) {
        if (selProprietario.options.length > 0) {
            oOption = document.createElement("OPTION");
            oOption.text = selProprietario.options(0).innerText;
            oOption.value = selProprietario.options(0).value;
            selAlternativo.add(oOption);
        }
    }

    selProprietario.dataSrc = oldDataSrcProp;
    selProprietario.dataFld = oldDataFldProp;

    selAlternativo.dataSrc = oldDataSrcAlt;
    selAlternativo.dataFld = oldDataFldAlt;

    setLabelOfControlEx(lblProprietario, selProprietario);
    setLabelOfControlEx(lblAlternativo, selAlternativo);

    finalDSOsCascade();
}

// FINAL DE FUNCOES DE PROPRIETARIO E ALTERNATIVO *******************

// FUNCOES DE GRAVACAO **********************************************

/********************************************************************
Critica os campos do registro e salva a inclusao ou alteracao
no servidor.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveRegister_1stPart() {
    // Campo nulo apenas para simular tab e resolver a gravacao
    if (!txtNulo.disabled) // qdo estiver trocando estado do registro o txtNulo
    {                       // estara desabilitado 
        document.getElementById('txtNulo').maxLength = 0;
        document.getElementById('txtNulo').focus();
    }
    // Trava o control bar inferior
    lockControlBar('inf');

    // Trava controles do inf os do sup ja estavam travados
    lockControls(true, true);

    var x = glb_currDSO.id;
    var retPrg = false;

    var aItemSelected = getCmbCurrDataInControlBar('inf', 1);
    var folderID = aItemSelected[1];

    // A pasta corrente nao e grid
    if (x.lastIndexOf('Grid') < 0) {
        if (glb_btnCtlSupInf != 'INFEST') {
            // Trima os campos e verifica campos obrigatorios
            try {
                glb_AcceptBulletAndTab = saveBulletAndTabInTextArea('INF', 'INFOK', keepCurrFolder());
            }
            catch (e) {
                glb_AcceptBulletAndTab = null;
            }
     
            if (glb_AcceptBulletAndTab == null)
                trimAllImputFields(window);
            else {
                glb_AcceptBulletAndTab = null;
                trimAllImputFields_Exception(window);
            }
            
            updateRecordFromHTMLFields();

            // Verifica checkboxes e coloca false nos campos do dso
            // se o checkbox nao estiver checado
            adjustFldInTableByChkBoxUnchecked(window, glb_currDSO);

            // Array de ids dos campos obrigatorios
            var aRequiredFieldsIDs = new Array();
            // Array de ids dos campos de datas
            var aDateFieldsIDs = new Array();

            aRequiredFieldsIDs = glb_currDSO.getAttribute('aRequiredFieldsIDs', 1);
            aDateFieldsIDs = glb_currDSO.getAttribute('aDateFieldsIDs', 1);

            // Percorre o array de campos obrigatorios e faz a critica
            // Isto nao e feito se gravando vindo da maquina de estado
            var i = 0;
            var oFieldRequired;
            var oFieldDate, oLabelDate;

            for (i = 0; i < aRequiredFieldsIDs.length; i++) {
                // valida campos obrigatorios
                oFieldRequired = document.getElementById(aRequiredFieldsIDs[i]);
                if (oFieldRequired.value.length == 0) {
                    if (window.top.overflyGen.Alert('O campo ' + labelAssociate(aRequiredFieldsIDs[i]) + ' n�o foi preenchido...') == 0)
                        return null;

                    // Destrava controles do inf os do sup ja estavam travados
                    // e assim continuam
                    lockControls(false, true);

                    // Destrava o control bar inferior
                    unlockControlBar('inf');

                    oFieldRequired.focus();
                    return null;
                }
            }

            // valida campo numerico
            // nao precisa, e tratado na digitacao

            // valida campo data
            for (i = 0; i < aDateFieldsIDs.length; i++) {
                oFieldDate = document.getElementById(aDateFieldsIDs[i]);

                // A funcao normalizeDateFormat transforma separadores
                // como ponto e traco para barra
                if (oFieldDate.value != '')
                    oFieldDate.value = normalizeDateFormat(oFieldDate.value);

                // A funcao chkData valida o campos se ele e uma data valida
                if (chkDataEx(oFieldDate.value) == false) {
                    if (window.top.overflyGen.Alert('O campo ' + labelAssociate(aDateFieldsIDs[i]) + ' n�o � v�lido...') == 0)
                        return null;

                    // Destrava controles do inf os do sup ja estavam travados
                    lockControls(false, true);

                    // Destrava o control bar inf
                    unlockControlBar('inf');

                    oFieldDate.focus();
                    return null;
                }

                // Nome do campo de data nao virtual
                if (oFieldDate.parentElement.currentStyle.visibility == 'visible') {
                    sFieldInTableName = oFieldDate.getAttribute('dataFld', 1).substr(2);

                    // verifica se o campo data que deu origem ao campo VARCHAR
                    // e obrigatorio
                    if (~glb_currDSO.recordset[sFieldInTableName].Attributes & 64) {
                        if (oFieldDate.value == null || oFieldDate.value == '') {
                            if (window.top.overflyGen.Alert('O campo ' + labelAssociate(aDateFieldsIDs[i]) + ' n�o foi preenchido...') == 0)
                                return null;

                            // Destrava controles do inf os do sup ja estavam travados
                            lockControls(false, true);

                            // Destrava o control bar inf
                            unlockControlBar('inf');

                            if (oFieldDate.disabled == false)
                                oFieldDate.focus();
                            return null;
                        }
                    }

                    // se a data for valida grava o valor do campo HTML no campo do DSO
                    if (oFieldDate.value == '')
                        glb_currDSO.recordset[sFieldInTableName].value = null;
                    else
                        glb_currDSO.recordset[sFieldInTableName].value = oFieldDate.value;
                }
            }

            // Programador critica dados inseridos
            // Chama funcao do xxx_inf01.js    
            retPrg = criticDataForSave(glb_btnCtlSupInf, folderID, 'CTRLS', glb_currDSO, glb_registroID);
            // Programador nao aceitou dados inseridos
            if (!retPrg) {
                // Destrava controles do inf os do sup ja estavam travados
                lockControls(false, true);

                // Destrava o control bar inf
                unlockControlBar('inf');

                return null;
            }
        }
    }
    // A pasta e grid
    else {
        if (glb_useParallelDSO == true) {
            setupParallelSaveDSO();
            return null;
        }

        // Remove carriage return nos campos do grid
        trimAllInputFieldsInGrid(fg);

        // Programador critica dados inseridos
        // Chama funcao do xxx_inf01.js    
        retPrg = criticDataForSave(glb_btnCtlSupInf, folderID, 'GRID', glb_currDSO, glb_registroID);
        // Programador nao aceitou dados inseridos
        if (!retPrg) {
            // Destrava controles do inf os do sup ja estavam travados
            lockControls(false, true);

            // Destrava o control bar inf
            unlockControlBar('inf');

            return null;
        }

    }

    // Trava a interface
    lockInterface(true);

    // Escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Gravando', true);

    // Tenta gravar o registro no banco
    saveRegister_2ndPart();
}

function trimAllInputFieldsInGrid(grid) {
    var i;

    /*
    for (i = 0; i < grid.Cols; i++) {
        grid.TextMatrix(grid.Row, grid.Col) = trimCarriageReturnAtAll(trimStr(grid.TextMatrix(grid.Row, grid.Col)));
        grid.TextMatrix(grid.Row, grid.Col) = filtraString(grid.TextMatrix(grid.Row, grid.Col));
    }
*/
    if (grid.Row <= 0)
        return;

    for (i = 0; i < grid.Cols; i++) {
        grid.TextMatrix(grid.Row, i) = trimCarriageReturnAtAll(trimStr(grid.TextMatrix(grid.Row, i)));
        grid.TextMatrix(grid.Row, i) = filtraString(grid.TextMatrix(grid.Row, i));
    }
}

/********************************************************************
Prepara dso paralelo para gravacao de linha do grid

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupParallelSaveDSO() {
    var nColID = fg.TextMatrix(keepCurrLineInGrid(), fg.Cols - 1);
    var sFldIDName = fg.ColKey(fg.Cols - 1);

    // Trava a interface
    lockInterface(true);

    // Escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Gravando', true);

    setConnection(dsoParallelGrid);

    // e um registro novo pois nao tem ID
    if ((nColID == '') || (nColID == null))
        dsoParallelGrid.SQL = 'SELECT * FROM ' + glb_currGridTable + ' WHERE ' +
                          sFldIDName + ' = 0';
    else
        dsoParallelGrid.SQL = 'SELECT * FROM ' + glb_currGridTable + ' WHERE ' +
                          sFldIDName + ' = ' + nColID;

    dsoParallelGrid.ondatasetcomplete = setupParallelSaveDSO_DSC;
    dsoParallelGrid.Refresh();
}

/********************************************************************
O dso paralelo recebeu dados do servidor para gravacao de linha do grid

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupParallelSaveDSO_DSC() {
    var nColID = fg.TextMatrix(keepCurrLineInGrid(), fg.Cols - 1);

    // Testa se houve erro na alteracao
    // e um registro ja existente pois tem ID
    if (!((nColID == '') || (nColID == null)) &&
          ((dsoParallelGrid.recordset.BOF) && (dsoParallelGrid.recordset.EOF))) {
        // Destrava a interface
        lockInterface(false);

        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Detalhe');

        if (window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0)
            return null;

        // Destrava controles do inf os do sup ja estavam travados
        lockControls(false, true);

        // Destrava o control bar inf
        unlockControlBar('inf');

        return null;
    }

    setupParallelSaveDSO_2ndPart();
}

/********************************************************************
O dso paralelo recebeu dados do servidor para gravacao de linha do grid.
Continua a operacao.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupParallelSaveDSO_2ndPart() {
    var i = 0;
    var nRegistroIDValue = fg.TextMatrix(keepCurrLineInGrid(), fg.Cols - 1);
    var sRegistroIDName = fg.ColKey(fg.Cols - 1);

    // coloca o dso paralelo como dso corrente
    glb_currDSO = dsoParallelGrid;

    // Programador critica dados inseridos
    // Chama funcao do xxx_inf01.js
    retPrg = criticDataForSave(glb_btnCtlSupInf, keepCurrFolder(), 'GRID', glb_currDSO, glb_registroID);

    // se nao e um registro novo
    if ((nRegistroIDValue != null) && (nRegistroIDValue != '')) {
        for (i = 0; i < glb_currDSO.recordset.Fields.Count; i++) {
            if ((glb_currDSO.recordset.Fields[i].Name).toUpperCase() == 'ESTADOID') {
                dso01PgGrid.recordset.MoveFirst();
                dso01PgGrid.recordset.Find(sRegistroIDName, nRegistroIDValue);
                glb_currDSO.recordset.Fields[i].value = dso01PgGrid.recordset['EstadoID'].value;

                if (dso01PgGrid.recordset['LOGMotivo'].value != null)
                    glb_currDSO.recordset.Fields[i].value = dso01PgGrid.recordset['LOGMotivo'].value;

                break;
            }
        }
    }

    // Programador nao aceitou dados inseridos
    if (!retPrg) {
        // Destrava a interface
        lockInterface(false);

        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Detalhe');

        // Destrava controles do inf os do sup ja estavam travados
        lockControls(false, true);

        // Destrava o control bar inf
        unlockControlBar('inf');

        return null;
    }

    glb_DetInfTimerVar = window.setInterval('saveRegister_2ndPart()', 10, 'JavaScript');
}

/********************************************************************
Salva a inclusao ou alteracao no servidor

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveRegister_2ndPart() {
    if (glb_DetInfTimerVar != null) {
        window.clearInterval(glb_DetInfTimerVar);
        glb_DetInfTimerVar = null;
    }

    // LOG - sempre altera o UsuarioID
    if (LOG_ACT_FLD != null)
        if (fieldExists(glb_currDSO, LOG_ACT_FLD))
        glb_currDSO.recordset[LOG_ACT_FLD].value = getCurrUserID();

    // NULL Campos IDS: substitui valor 0 por null
    changeZeroByNull(glb_currDSO);

    glb_currDSO.ondatasetcomplete = saveRegister_2ndPart_DSC;

    try {
        glb_currDSO.SubmitChanges();
    } catch (e) {
        window.top.overflyGen.Alert((e.message != null ? e.message : e));
        unlockInterface(document.body.id);

        return null;
    }
}

function saveRegister_2ndPart_DSC() {
    if (glb_useParallelDSO != true)
        submitChanges_DSC();
    else
        submitChangesParallel_DSC();
}

/********************************************************************
Retorno do salvamento de inclusao ou alteracao no servidor, usando
dso paralelo

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function submitChangesParallel_DSC() {
    // testa se o dso corrente e o paralelo
    if (glb_useParallelDSO == true) {
        glb_currDSO = dso01PgGrid;
    }

    // esta funcao so executa se a maquina de estado estiver visivel
    if (restoreInterfaceFromModal()) {
        if (glb_EstadoIDBeforeStateMach != getEstadoIDOfLineInGrid())
        // alerta programador que a maquina de estado fechou
        // apenas se o estado do registro foi alterado
            stateMachClosed(glb_EstadoIDBeforeStateMach, getEstadoIDOfLineInGrid());

        // Prosegue como um refresh
        lockAndOrSvrInf_SYS('INFREFR');

        return null;
    }

    getDataOfInfInServer();
}

/********************************************************************
Retorno do servidor do SubmitChanges da funcao saveRegister_2ndPart.
Para cada EstadoID do grid, tras a descricao (Fantasia) do estado.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function submitChanges_DSC() {
    // testa se o dso corrente e o paralelo
    if (glb_useParallelDSO == true) {
        glb_currDSO = dso01PgGrid;
    }

    // esta funcao so executa se a maquina de estado estiver visivel
    if (restoreInterfaceFromModal()) {
        if (glb_EstadoIDBeforeStateMach != getEstadoIDOfLineInGrid())
        // alerta programador que a maquina de estado fechou
        // apenas se o estado do registro foi alterado
            stateMachClosed(glb_EstadoIDBeforeStateMach, getEstadoIDOfLineInGrid());
    }

    // Se a pasta corrente for grid, recarrega o combo do grid
    // e de la prossegue no framework
    if ((glb_currDSO.id.toUpperCase()).indexOf('GRID') >= 0)
        dso01Grid_DSC();
    // Se a pasta corrente nao for grid
    else
    {
        // se a pasta corrente nao for proprietario/alternativo
        if (keepCurrFolder() != 20009)
            glb_refrInf = window.setInterval('forceRefreshInf()', 10, 'JavaScript');
            // doChangeDivInf_SYS(glb_btnCtlSupInf);
        else
            glb_DetInfTimerVar = window.setInterval('refreshSup__()', 10, 'JavaScript');
    }
}

function forceRefreshInf()
{
    if (glb_refrInf != null) {
        window.clearInterval(glb_refrInf);
        glb_refrInf = null;
    }

    __btn_REFR('inf');
}

// FINAL DE FUNCOES DE GRAVACAO *************************************

// FUNCOES DE GRID **************************************************

/********************************************************************
Constroi os combos do grid (se tiver), no frame work.
Constroi o corpo do grid , no js especifico do form.

Parametros: 
folderID        - id da pasta corrente

Retorno:
nenhum
********************************************************************/
function buildGridFromFolder(folderID) {
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    removeCombo(fg);
    startGridInterface(fg);

    fg.Redraw = 0;

    // Instala combos no grid da pasta corrente
    var i, j;

    // Tem combos estaticos em grids
    if (glb_aGridStaticCombos != null) {
        var dso, col, cmbCols, cmbGridCommonField;

        // Tem combos estaticos no grid    
        // glb_aGridStaticCombos = [ [20010, [[1, 'dsoCmb01Grid_01']] ];

        for (i = 0; i < glb_aGridStaticCombos.length; i++) {
            if (folderID == glb_aGridStaticCombos[i][0]) {
                glb_nCombosInCurrGrid = glb_aGridStaticCombos[i][1].length;

                for (j = 0; j < glb_aGridStaticCombos[i][1].length; j++) {
                    dso = eval(glb_aGridStaticCombos[i][1][j][1]);

                    // Verifica se o dso e de combo ou lookup
                    // se for de lookup nao prosegue, pois nao e um combo
                    if (((dso.id).toUpperCase()).indexOf('LKP') >= 0)
                        continue;

                    if (glb_aGridStaticCombos[i][1][j].length == 5) {
                        if (glb_aGridStaticCombos[i][1][j][4] != glb_currDSO.id)
                            continue;
                    }

                    col = glb_aGridStaticCombos[i][1][j][0];
                    cmbCols = glb_aGridStaticCombos[i][1][j][2];
                    cmbGridCommonField = glb_aGridStaticCombos[i][1][j][3];

                    insertcomboData(fg,
                                    col,
                                    dso,
                                    cmbCols,
                                    cmbGridCommonField);

                }
            }
        }
    }

    // funcao do lado do programador no inf01.asp
    // constroi o corpo do grid
    constructGridBody(folderID, glb_currDSO);

    // Volta para a ultima linha corrente ou para a primeira
    if ((glb_currLineInGrid < fg.Rows) && (glb_currLineInGrid > 0))
        fg.Row = glb_currLineInGrid;
    else {
        if (fg.Rows > 0) {
            while (glb_currLineInGrid >= fg.Rows)
                glb_currLineInGrid--;
            if (glb_currLineInGrid > 0)
                fg.Row = glb_currLineInGrid;
        }
    }

    // grid tem primeira linha de totalizacao
    if (glb_totalCols__) {
        if ((!fg.Editable) && (fg.Row == 1)) {
            if (fg.Rows > 2) {
                fg.Row = 2;
            }
        }
    }

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ((fg.Row < 1) && (fg.Rows > 1))
        fg.Row = 1;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fg.Redraw = 2;
}

/********************************************************************
Adiciona uma linha em grid.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function addLineInGrid() {
    glb_antGridMode = 'INCL';

    adjustControlBarEx(window, 'sup', 'DDDDDDDDDDDDDDD');
    adjustControlBarEx(window, 'inf', 'DDDDDDHHDDDDDDD');

    addNewLine(fg);

    // Guarda a linha nova do grid
    glb_currLineInGrid = fg.Rows - 1;

    fg.Row = glb_currLineInGrid;

    // Informa programador
    addedLineInGrid(keepCurrFolder(), fg, glb_currLineInGrid);

    SelectionMode = 0;

    if (fg.disabled == false)
        fg.focus();

    writeInStatusBar('child', 'cellMode', 'Inclus�o');
}

/********************************************************************
Avanca uma linha em grid.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function nextLineInGrid() {
    walkInGrid(fg, -1);
}

/********************************************************************
Reatroage uma linha em grid.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function previousLineInGrid() {
    walkInGrid(fg, 1);
}

/********************************************************************
Confirma se deleta ou nao uma linha em grid.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function deleteConfirmLineInGrid() {
    // Alteracao por problema de delecao ap�s inclus�o
    glb_currLineInGrid = fg.Row;

    var _retMsg = window.top.overflyGen.Confirm('Voc� tem certeza?');

    if (_retMsg == 0)
        return null;
    else if (_retMsg == 1) {
        // Trava interface
        lockInterface(true);

        writeInStatusBar('child', 'cellMode', 'Excluindo', true);

        excludeRegisterOfLineInGrid();

    }
    else {
        window.focus();
        fg.focus();
    }

    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 0;

    return null;
}


/********************************************************************
Funcao que salva ou nao o registro corrente com o campo de UsuarioID
(caso exista do recordset do dso, )alterado para o id do usuario logado.
Isto e necessariopara executar as operacoes de log no banco.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function excludeRegisterOfLineInGrid() {
    // LOG - sempre altera o UsuarioID
    if (LOG_ACT_FLD != null) {
        // grid de paging
        if (glb_useParallelDSO == true) {
            getDataAndSaveForLogBeforeDeleteInPaging();
            return null;
        }
        // grid nao de paging
        else {
            if (fieldExists(glb_currDSO, LOG_ACT_FLD))
                saveForLogBeforeDelete();
            else
                deleteLineInGrid();
        }
    }
    else
        deleteLineInGrid();
}

/********************************************************************
Salva o registro corrente com o usuario logado, para executar o log,
antes de deletar o registro.
Grid nao em modo paging.
No retorno inicia o deleta do registro corrente.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveForLogBeforeDelete() {
    // LOG - sempre altera o UsuarioID
    if (LOG_ACT_FLD != null) {
        if (fieldExists(glb_currDSO, LOG_ACT_FLD)) {
            // Posiciona o ponteiro de registro no registro
            // da linha corrente
            glb_currDSO.recordset.MoveFirst();

            glb_currDSO.recordset.Find(fg.ColKey(fg.Cols - 1),
                                        fg.TextMatrix(keepCurrLineInGrid(), fg.Cols - 1));

            if (!glb_currDSO.recordset.EOF) {
                glb_currDSO.recordset[LOG_ACT_FLD].value = getCurrUserID();

                glb_currDSO.ondatasetcomplete = saveForLogBeforeDelete_DSC;
                glb_currDSO.SubmitChanges();
            }
            else    // erro na operacao
            {
                // destrava interface
                lockInterface(false);

                writeInStatusBar('child', 'cellMode', 'Detalhe', true);

                if (window.top.overflyGen.Alert('Erro ao tentar deletar o registro!') == 0)
                    return null;
                window.focus();
                fg.focus();
            }
        }
    }
}

function saveForLogBeforeDelete_DSC() {
    saveForLogBeforeDelete_DSC();
}

/********************************************************************
Retorno da funcao que salva o registro corrente com o usuario logado,
para executar o log.
Inicia o deleta do registro da linha corrente do grid.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveForLogBeforeDelete_DSC() {
    glb_DetInfTimerVar = window.setInterval('deleteLineInGrid()', 10, 'JavaScript');
}

/********************************************************************
Obtem dados para salvar o registro corrente com o usuario logado,
para executar o log, antes de deletar o registro.
Grid em modo paging.
No retorno inicia o deleta do registro corrente.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function getDataAndSaveForLogBeforeDeleteInPaging() {
    // LOG - sempre altera o UsuarioID
    if (LOG_ACT_FLD != null) {
        // Recupera o registro no banco de dados,
        // para alterar e salvar o usuario ID

        // Guarda o ID deletado
        var nRegistroID = fg.TextMatrix(fg.Row, fg.Cols - 1);

        // nome do campo ID do registro corrente
        var sFldIDName = fg.ColKey(fg.Cols - 1);

        setConnection(dsoParallelGrid);

        dsoParallelGrid.SQL = 'SELECT * FROM ' + glb_currGridTable + ' WHERE ' +
                          sFldIDName + ' = ' + nRegistroID;

        dsoParallelGrid.ondatasetcomplete = getDataAndSaveForLogBeforeDeleteInPaging_DSC;
        dsoParallelGrid.Refresh();
    }
}

/********************************************************************
Retorno da funcao que obtem o registro no servidor, para
salvar o registro corrente com o usuario logado,
para executar o log.
Salva este registro alterado, para depois deleta-lo.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function getDataAndSaveForLogBeforeDeleteInPaging_DSC() {
    if (!(dsoParallelGrid.recordset.BOF && dsoParallelGrid.recordset.EOF)) {
        if (LOG_ACT_FLD != null) {
            if (fieldExists(dsoParallelGrid, LOG_ACT_FLD)) {
                dsoParallelGrid.recordset[LOG_ACT_FLD].value = (-1) * (getCurrUserID());
                glb_DetInfTimerVar = window.setInterval('saveForLogBeforeDeleteInPaging()', 10, 'JavaScript');
            }
            else
                glb_DetInfTimerVar = window.setInterval('deleteLineInGrid()', 10, 'JavaScript');
        }
    }
    else
        glb_DetInfTimerVar = window.setInterval('deleteLineInGrid()', 10, 'JavaScript');
}

/********************************************************************
Gravacao do registro corrente com o usuario logado,
para executar o log.
Inicia o deleta do registro da linha corrente do grid.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveForLogBeforeDeleteInPaging() {
    if (glb_DetInfTimerVar != null) {
        window.clearInterval(glb_DetInfTimerVar);
        glb_DetInfTimerVar = null;
    }

    dsoParallelGrid.ondatasetcomplete = saveForLogBeforeDeleteInPaging02_DSC;
    dsoParallelGrid.SubmitChanges();
}

function saveForLogBeforeDeleteInPaging02_DSC() {
    saveForLogBeforeDeleteInPaging_DSC();
}

/********************************************************************
Retorno da gravacao do registro corrente com o usuario logado,
para executar o log.
Inicia o deleta do registro da linha corrente do grid.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveForLogBeforeDeleteInPaging_DSC() {
    glb_DetInfTimerVar = window.setInterval('deleteLineInGrid()', 10, 'JavaScript');
}

/********************************************************************
Deleta realmente uma linha em grid.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function deleteLineInGrid() {
    if (glb_DetInfTimerVar != null) {
        window.clearInterval(glb_DetInfTimerVar);
        glb_DetInfTimerVar = null;
    }

    var currLine = keepCurrLineInGrid();

    // Guarda o ID deletado
    var currID = fg.TextMatrix(fg.Row, fg.Cols - 1);

    if (glb_useParallelDSO == true) {
        deleteLineInGridPaging(currID);
        return null;
    }

    // Deleta o registro no banco
    glb_currDSO.recordset.MoveFirst();

    glb_currDSO.recordset.Find(fg.ColKey(fg.Cols - 1),
                           fg.TextMatrix(keepCurrLineInGrid(), fg.Cols - 1));

    if (!glb_currDSO.recordset.EOF) {
        if (fg.Rows > 1) {
            if (currLine > 1)
                glb_currLineInGrid = currLine - 1;
            else
                glb_currLineInGrid = -1;
        }
        else
            glb_currLineInGrid = -1;

        fg.RemoveItem(keepCurrLineInGrid());

        glb_currDSO.recordset.Delete();

        glb_currDSO.ondatasetcomplete = deleteLineInGrid_DSC;
        glb_currDSO.SubmitChanges();
    }
}

function deleteLineInGrid_DSC() {
    dso01Grid_DSC();
}

/********************************************************************
Deleta uma linha em grid que esta usando paginacao.

Parametros: 
nRegistroID     :Id do registro corrente no grid

Retorno:
nenhum
********************************************************************/
function deleteLineInGridPaging(nRegistroID) {
    // nome do campo ID do registro corrente
    var sFldIDName = fg.ColKey(fg.Cols - 1);

    setConnection(dsoParallelGrid);

    dsoParallelGrid.SQL = 'SELECT * FROM ' + glb_currGridTable + ' WHERE ' +
                      sFldIDName + ' = ' + nRegistroID;

    dsoParallelGrid.ondatasetcomplete = setupParallelDeleteDSO_DSC;
    dsoParallelGrid.Refresh();
}

/********************************************************************
O dso paralelo recebeu dados do servidor para delecao da linha do grid

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupParallelDeleteDSO_DSC() {
    var nColID = fg.TextMatrix(keepCurrLineInGrid(), fg.Cols - 1);

    // Testa se houve erro na delecao
    if (!((nColID == '') || (nColID == null)) &&
          ((dsoParallelGrid.recordset.BOF) && (dsoParallelGrid.recordset.EOF))) {
        // Destrava interface
        lockInterface(false);

        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Detalhe');

        if (window.top.overflyGen.Alert('Este registro ja foi removido.') == 0)
            return null;

        return null;
    }

    glb_DetInfTimerVar = window.setInterval('setupParallelDeleteDSO_DSC_2ndPart()', 30, 'JavaScript');
}

/********************************************************************
O dso paralelo recebeu dados do servidor para delecao da linha do grid
Continua a operacao.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setupParallelDeleteDSO_DSC_2ndPart() {
    if (glb_DetInfTimerVar != null) {
        window.clearInterval(glb_DetInfTimerVar);
        glb_DetInfTimerVar = null;
    }

    var currLine = keepCurrLineInGrid();

    // A setagem de linha abaixo nao tem nenhum efeito pratico
    // o grid nao volta para esta linha
    if (fg.Rows > 1) {
        if (currLine > 1)
            glb_currLineInGrid = currLine - 1;
        else
            glb_currLineInGrid = -1;
    }
    else
        glb_currLineInGrid = -1;

    fg.RemoveItem(keepCurrLineInGrid());

    dsoParallelGrid.recordset.Delete();
    dsoParallelGrid.ondatasetcomplete = setupParallelDeleteDSO_DSC_2ndPart_DSC;
    dsoParallelGrid.SubmitChanges();
}

function setupParallelDeleteDSO_DSC_2ndPart_DSC() {
    getDataOfInfInServer();
}

/********************************************************************
Verifica se a pasta tem grid.

Parametros:
nFolderID   -  Opcional. Se != null, forca a pesquisa em um dado
folder. Se null pesquisa o folder corrente no combo
de pastas.

Retorno:
quantidade de grids na pasta: 0, 1, etc
se retorna null a pasta nao tem nada
********************************************************************/
function folderHasGrid(nFolderID) {
    // SubForms com grids
    var eDiv = null;
    var elem;
    var i, j;
    var nQtyGrids = 0;
    var aPastas;

    // A pasta corrente
    var aItemSelected = getCmbCurrDataInControlBar('inf', 1);
    // O combo de pastas esta vazio
    if (aItemSelected == null)
        return null;

    var folderID = nFolderID;

    if (nFolderID == null)
        folderID = aItemSelected[1];

    // O div corrente
    for (i = 0; i < glb_aDIVS.length; i++) {
        aPastas = (eval(glb_aDIVS[i])).getAttribute('sfsGrupo', 1);

        if ((typeof (aPastas)).toUpperCase() != 'NUMBER') {
            for (j = 0; j < aPastas.length; j++)
                if (aPastas[j] == folderID)
                eDiv = eval(glb_aDIVS[i]);
        }
        else
            if (aPastas == folderID)
            eDiv = eval(glb_aDIVS[i]);
    }

    if (eDiv == null)
        return null;

    for (j = 0; j < eDiv.children.length; j++) {
        // Elemento contido no div
        elem = eDiv.children.item(j);

        // O div tem grid
        if (elem.tagName.toUpperCase() == 'OBJECT') {
            if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072') {
                nQtyGrids++;
            }
        }
    }

    return nQtyGrids;
}

/********************************************************************
Trava botoes da barra inferior se a pasta corrente e grid e ocorreu
um SUPDET ou SUPREFR.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function lockBtnsInCtrlBarInf_Grid() {
    // trap do botao
    if (!((glb_btnCtlSupInf == 'SUPDET') ||
           (glb_btnCtlSupInf == 'SUPREFR')))
        return null;
    // trap do tipo de pasta    
    if (folderHasGrid(keepCurrFolder()) <= 0)
        return null;

    // trap se form nao esta configurado para montar grid
    if (glb_bInfNoData == null)
        return null;

    var strBtns = currentBtnsCtrlBarString('inf');
    var aStrBtns = strBtns.split('');

    // trava botao de inclusao da barra inferior    
    aStrBtns[2] = 'D';

    // trava botao de alteracao da barra inferior    
    aStrBtns[3] = 'D';

    // trava botoes especificos da barra inferior
    aStrBtns[11] = 'D';
    aStrBtns[12] = 'D';
    aStrBtns[13] = 'D';
    aStrBtns[14] = 'D';

    strBtns = aStrBtns.toString();
    re = /,/g;
    strBtns = strBtns.replace(re, '');
    adjustControlBarEx(window, 'inf', strBtns);
}

/********************************************************************
Aplicavel em grids. Retorna a linha corrente do grid.

Parametros: 
nenhum

Retorno:
A linha corrente do grid
********************************************************************/
function keepCurrLineInGrid() {
    if (glb_currLineInGrid != -1)
        return glb_currLineInGrid;
    else
        return fg.Row;
}

/********************************************************************
Starta janela de Observacoes para grids do inf

Parametros: 
grid            - referencia ao grid do inf
dso             - referencia ao dso do grid
fldObsName      - nome do campo de observacoes no dso
colObsNumber    - numero da coluna de observacoes no grid

Retornos:
irrelevante
********************************************************************/
function startObsWindow_inf(grid, dso, fldObsName, colObsNumber) {
    var strPars = new String();
    strPars = '?';

    // se esta em modo de edicao/inclusao
    if (grid.Editable)
        strPars += ('btnOKState=' + escape('H'));
    else
        strPars += ('btnOKState=' + escape('D'));

    // dso do grid
    strPars += ('&dso=' + escape(dso.id));

    // nome do campo de Obs no dso
    strPars += ('&fieldName=' + escape(fldObsName));

    // id do grid
    strPars += ('&gridID=' + escape(grid.id));

    // coluna do grid referente ao campo de Observacoes
    strPars += ('&colObs=' + escape(colObsNumber));

    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalobservacao.asp' + strPars;
    showModalWin(htmlPath, new Array(1000, 539));
}

/********************************************************************
Aplicavel em grids. Ajusta a largura das colunas em funcao dos dados.

Parametros: 
nenhum

Retorno:
********************************************************************/
function adjColsWidthInGrid() {
    // se a pasta corrente nao e grid nao executa
    if (!folderHasGrid())
        return null;

    fg.Redraw = 0;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;
}

// FINAL DE FUNCOES DE GRID *****************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Chamada da gravacao pela maquina de estado.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveEstadoID() {
    // usando paging
    if (glb_useParallelDSO == true) {
        setupParallelSaveDSO();
        return null;
    }

    // nao usando paging
    saveRegister_2ndPart();
}

/********************************************************************
Invoca a maquina de estado para o inf

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function showStateMachine() {
    // escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Estado', true);

    var htmlPath;

    // Para trazer a maquina de estado correta e
    // os possiveis estados transitivos para o
    // registro corrente, mandaremos para o servidor:
    // 1. EstadoID do registro corrente
    // 2. FormID
    // 3. PastaID corrente
    // 4. ContextoID corrente
    // 5. O ID do usuario logado
    // 6. O ID da empresa corrente

    var nCurrEstadoID = getEstadoIDOfLineInGrid();
    var pastaID = getCmbCurrDataInControlBar('inf', 1);
    var contextoID = getCmbCurrDataInControlBar('sup', 1);

    var strPars = new Array();
    strPars = '?';
    strPars += 'estadoID=';
    strPars += nCurrEstadoID;
    strPars += '&';
    strPars += 'formID=';
    strPars += escape(window.top.formID);
    strPars += '&';
    strPars += 'pastaID=';
    strPars += escape(pastaID[1]);
    strPars += '&';
    strPars += 'contextoID=';
    strPars += escape(contextoID[1]);

    strPars += '&';
    strPars += 'userID=';
    strPars += escape(getCurrUserID());

    var empresa = getCurrEmpresaData();

    strPars += '&';
    strPars += 'empresaID=';
    strPars += escape(empresa[0]);

    var Prop1 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                    "dsoSup01.recordset['Prop1'].value");

    strPars += '&';
    strPars += 'nProp1=';
    strPars += escape(Prop1);

    var Prop2 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                    "dsoSup01.recordset['Prop2'].value");

    strPars += '&';
    strPars += 'nProp2=';
    strPars += escape(Prop2);

    htmlPath = SYS_ASPURLROOT + '/statemachEx/statemachinfmodal.asp' + strPars;

    // Medidas minimas das modais 262,3333333 x 173
    showModalWin(htmlPath, new Array(436, 246));
}

/********************************************************************
Aplicavel em grids. Obtem o estado ID do registro da linha corrente

Parametros: 
nenhum

Retorno:
O EstadoID ou zero se nao obtem
********************************************************************/
function getEstadoIDOfLineInGrid() {
    var nCurrEstadoID = 0;
    var nCurrRecIDVal = 0;
    var nCurrRecIDName = '';

    var i = 0;

    if (glb_currDSO.recordset.BOF && glb_currDSO.recordset.EOF)
        return nCurrEstadoID;

    for (i = 0; i < fg.Cols; i++) {
        if ((fg.ColKey(i)).lastIndexOf('Estado*') >= 0) {
            nCurrRecIDVal = fg.TextMatrix(fg.Row, fg.Cols - 1);
            nCurrRecIDName = fg.ColKey(fg.Cols - 1);
            glb_currDSO.recordset.MoveFirst();
            glb_currDSO.recordset.Find(nCurrRecIDName, nCurrRecIDVal);
            if (!glb_currDSO.recordset.EOF) {
                nCurrEstadoID = glb_currDSO.recordset['EstadoID'].value;
            }
        }
    }

    return nCurrEstadoID;
}

// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************

// FUNCOES DA PAGINACAO INFERIOR ************************************

/********************************************************************
Funcao chamada pela funcao setSqlToCurrDso() do especific.js
Executa a paginacao dos dados nos grids inferiores.
           
Parametros: 
dso             - dso corrente
sLinkFieldName  - Nome do campo que linka os registros do grid com registro corrente
do sup
sLinkFieldValue - Valor do campo que linka os registros do grid com registro corrente
do sup
folderID        - id da pasta corrente
sFiltro         - (Opcional) Filtro definido no js, que sera concatenado a string de select
no servidor.
                  
strParsOptional - Complemento opcional do strPars que eh enviado a pagina de pesquisa.
Somente usado nos forms que tem a janela de pesquisa do paginf espeficica.
                  
Retorno:
funcao de datasetcomplete do dso corrente
********************************************************************/
function execPaging(dso, sLinkFieldName, sLinkFieldValue, folderID, sFiltro, strParsOptional) {
    var nEmpresaData = getCurrEmpresaData();

    var nFiltroID = getCmbCurrDataInControlBar('inf', 2);
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    // Habilita desabilita campo frete chkFrete
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A1' + '\'' + ',' + folderID + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
		('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'A2' + '\'' + ',' + folderID + ')'));

    // Move foco por questoes esteticas
    if (fg.Enabled)
        fg.focus();

    lockInterface(true);

    if (lastBtnUsedInCtrlBar('inf') == 'SEG')
    // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Seguinte', true);
    else if (lastBtnUsedInCtrlBar('inf') == 'ANT')
    // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Anterior', true);
    else if (lastBtnUsedInCtrlBar('inf') == 'REFR')
    // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Refresh', true);

    var strPas = new String();

    strPas = '?nFormID=' + escape(window.top.formID);
    strPas += '&sFldKey=' + escape(glb_scmbKeyPesqSel);
    strPas += '&sLinkFieldName=' + escape(sLinkFieldName);
    strPas += '&sLinkFieldValue=' + escape(sLinkFieldValue);
    strPas += '&nFolderID=' + escape(folderID);

    if (glb_bCheckInvert == true)
        strPas += '&nOrder=DESC';
    else
        strPas += '&nOrder=ASC';

    strPas += '&sTextToSearch=' + escape(putDateInMMDDYYYY2(glb_sArgument));

    strPas += '&sCondition=' + escape(glb_sFilterPesq);
    strPas += '&nPageNumber=' + escape(glb_nPAGENUMBER);
    strPas += '&nPageSize=' + escape(glb_scmbLinePerPageSel);

    if (nFiltroID == null)
        strPas += '&nFiltroID=0';
    else
        strPas += '&nFiltroID=' + escape(nFiltroID[1]);

    if (sFiltro != null)
        strPas += '&sFiltro=' + escape(sFiltro);

    strPas += '&vOptParam1=' + escape(glb_vOptParam1);
    strPas += '&vOptParam2=' + escape(glb_vOptParam2);
    strPas += '&nContextoID=' + escape(contexto[1]);

    strPas += '&nEmpresaID=' + escape(nEmpresaData[0]);
    strPas += '&nEmpresaCidadeID=' + escape(nEmpresaData[2]);
    strPas += '&A1=' + escape(nA1);
    strPas += '&A2=' + escape(nA2);

    if (strParsOptional != null)
        strPas += strParsOptional;

    // garante uso do dso por SQL ou URL
    glb_currDSO.SQL = '';

    dso.URL = SYS_ASPURLROOT + '/serversidegenEx/pageinf.aspx' + strPas;

    return 'dso01Grid_DSC';
}

/********************************************************************
Esta funcao pagina a listagem.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function changePageInList(nNumber) {
    glb_nPAGENUMBER = glb_nPAGENUMBER + nNumber;
    if (glb_nPAGENUMBER <= 1)
        glb_nPAGENUMBER = 1;

    __btn_REFR('inf');
}

/********************************************************************
Carrega janela modal para combo de lupa.
           
Parametros: 
por enquanto nenhum

Retorno:
nenhum
********************************************************************/
function loadModalOfPagingPesq() {
    var htmlPath;

    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalofpaginginf.asp';

    // Veio do sup ou do inf?
    var htmlId = (getHtmlId()).toUpperCase();
    var filePos = 'SUP';

    if (htmlId.indexOf('SUP') > 0)
        filePos = 'SUP';
    else if (htmlId.indexOf('INF') > 0)
        filePos = 'INF';

    // Passar parametro se veio do inf ou do sup
    var strPars = new String();
    strPars = '?';

    strPars += 'filePos=' + escape(filePos);

    showModalWin((htmlPath + strPars), new Array(416, 178));
}

/********************************************************************
Funcao chamada pelo programador na funcao
modalInformForm(idElement, param1, param2) do xxx_inf.js
Executa a pesquisa na paginacao dos dados nos grids inferiores.
           
Parametros: 
param1  - dados vindos da janela de pesquisa OK ou CANCEL
param2  - dados vinsod da janela de pesquisa (parametros da pesquisa)

Retorno:
nenhum
********************************************************************/
function execPesqInPaging(param1, param2) {
    if (param1 == 'OK') {
        // Ajusta variaveis da pesquisa
        glb_bCheckInvert = param2[1];
        glb_sArgument = param2[3];
        glb_sFilterPesq = param2[4];
        glb_scmbKeyPesqSel = param2[0];
        glb_scmbLinePerPageSel = param2[2];

        restoreInterfaceFromModal();
        // Fun��o que simula o Refresh
        __btn_REFR('inf');
        return 0;
    }
    else if (param1 == 'CANCEL') {
        // esta funcao fecha a janela modal e destrava a interface
        restoreInterfaceFromModal();
        // escreve na barra de status
        writeInStatusBar('child', 'cellMode', 'Detalhe');
        return 0;
    }
}

/********************************************************************
Inicializa variaveis do modo paging para grid
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function initializeVarsToShowPagingGrid() {
    // a pasta corrente e de paginacao
    if (ascan(glb_aFoldersPaging, keepCurrFolder(), false) >= 0)
        glb_useParallelDSO = true;
    else
        glb_useParallelDSO = null;
}

// FINAL DE FUNCOES DA PAGINACAO INFERIOR ***************************

// FUNCOES DE DIREITOS **********************************************

/********************************************************************
Remove pastas do combo de pastas que nao sao aplicaveis nos direitos
correntes
           
Parametros: 
selBefore       - option data (array) da pasta selecionada antes
do repreenchimento do combo de pastas na funcao
fillCmbPastasInCtlBarInf(nTipoRegistroID)
dos especific inf

Retorno:
nenhum
********************************************************************/
function stripFoldersByRight(selBefore) {
    var i;
    var strFolderID;
    var aFolderID;      // array dos ids das pastas do combo de pastas
    var nFolderID = 0;
    var prop1, prop2;
    var theRight1, theRight2;
    var bRemove = true;
    var folderBeforeSelID = 0;
    var bRefillCmbFilters = false;

    prop1 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              "dsoSup01.recordset['Prop1'].value");
    prop2 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                              "dsoSup01.recordset['Prop2'].value");

    // nao tira nenhuma pasta
    if ((prop1 == 1) && ((prop2 == 1)))
        bRemove = false;

    // nao tira nenhuma pasta
    if ((prop1 == null) && ((prop2 == null)))
        bRemove = false;

    strFolderID = strArrayValuesInCmbInCtrlBar('inf', 1);
    if (strFolderID == null)
        bRemove = false;

    // o folder que estava selecionado antes do repreenchimento
    // do combo de pastas
    if ((selBefore != null))
        folderBeforeSelID = selBefore[1];

    if (bRemove) {
        aFolderID = strFolderID.split(',');

        for (i = 0; i < aFolderID.length; i++) {
            nFolderID = parseFloat(aFolderID[i]);

            theRight1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                          ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'C1' + '\'' + ',' +
                                              nFolderID + ')'));
            theRight2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                          ('getCurrRightValue(' + '\'' + 'INF' + '\'' + ',' + '\'' + 'C2' + '\'' + ',' +
                                              nFolderID + ')'));

            // o usuario tem direito de acesso para esta pasta em todos
            // os seus registros
            if ((theRight1 == 1) && (theRight2 == 1))
                continue;

            if (((prop1 * theRight1) + (prop2 * theRight2)) == 0) {
                if (removeOptionSelInControlBar('inf', 1, nFolderID)) {
                    if (nFolderID == folderBeforeSelID)
                        bRefillCmbFilters = true;
                }
            }
        }
    }

    // tenta selecionar a pasta selecionada antes do repreenchimento
    // do combo de pastas
    if ((folderBeforeSelID != 0)) {
        if (selOptByValueOfSelInControlBar('inf', 1, folderBeforeSelID, true) == false)
            selOptByValueOfSelInControlBar('inf', 1, glb_pastaDefault, true);
    }
    else
        selOptByValueOfSelInControlBar('inf', 1, glb_pastaDefault, true);

    if (bRefillCmbFilters)
        refillCmb2Inf(keepCurrFolder());
}

/********************************************************************
Atualiza o sup para corrigir o control bar superior em funcao dos
direitos, se trocar o proprietario e/ou alternativo corrente.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function refreshSup__() {
    if (glb_DetInfTimerVar != null) {
        window.clearInterval(glb_DetInfTimerVar);
        glb_DetInfTimerVar = null;
    }

    // da refresh em cima se for gravacao na pasta
    // proprietario/alternativo
    if (keepCurrFolder() == 20009)
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,
                                  '__btn_REFR(\'sup\')');
}

// FINAL DE FUNCOES DE DIREITOS *************************************

// FUNCOES DE INICIALIZACAO *****************************************

/********************************************************************
Inicializa dados no inf e mostra pasta compativel, vindo de um SUPOK
ou de um SUPEST

Parametros: 
btnClicked

Retorno:
nenhum
********************************************************************/
function inicializeDataInInfAndShowCompatiblePasta(btnClicked) {
    adjustSupInfControlsBar('INFCONS', null, null, null, fg, glb_currDSO);

    if (btnClicked != null) {
        glb_btnCtlSupInf = btnClicked;

        /*************************************
        Fiz esta modificacao por causa da gravacao
        // da maquina de estado superior em 16/08/2001
        btnClicked      - ultimo control bar (sup/inf) e botao clicado
        folderID        - id da pasta corrente
        tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
        um grid
        dsoID           - id do dso corrente

		*************************************/

        if (btnClicked == 'SUPEST') {
            var aItemSelected = getCmbCurrDataInControlBar('inf', 1);
            var folderID;

            // o combo de pastas esta vazio
            if (aItemSelected == null)
                folderID = null;
            else
                folderID = aItemSelected[1];

            // A pasta corrente nao e grid
            if ((glb_currDSO.id).lastIndexOf('Grid') < 0) {
                treatsControlsBars_Aut(btnClicked, folderID, 'CTRLS', glb_currDSO.id);
            }
            else {
                adjColsWidthInGrid();
                treatsControlsBars_Aut(btnClicked, folderID, 'GRID', glb_currDSO.id);
                lockBtnsInCtrlBarInf_Grid();
            }
        }
        else if (btnClicked == 'SUPOK') {
            lockInterface(true);

            // assim ate 22/09/2001
            //glb_DetInfTimerVar = window.setInterval( 'processDataAndPasta()', 10, 'JavaScript');    
            // assim apos 22/09/2001
            processDataAndPasta();
        }
    }
}

/********************************************************************
Inicializa dados no inf e mostra pasta compativel, vindo de um SUPOK
Complementa a fancao:
inicializeDataInInfAndShowCompatiblePasta(btnClicked)

Parametros: 
btnClicked

Retorno:
nenhum
********************************************************************/
function processDataAndPasta() {
    if (glb_DetInfTimerVar != null) {
        window.clearInterval(glb_DetInfTimerVar);
        glb_DetInfTimerVar = null;
    }

    // Salva regID e, se aplicavel, typeRegID
    keepRegIDAndTypeID();

    getDataOfInfInServer();

    showCompatiblePasta();
}

/********************************************************************
Ajusta a barra inferior, colocando-a em modo consulta.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustInfBar__() {
    // Ajusta barra inferior
    adjustSupInfControlsBar('INFCONS', null, null, null, fg, glb_currDSO);
}

/********************************************************************
Zera variaveis do detail inf, que sao zeradas na funcao
__combo_1(controlBar, optText, optValue, optIndex) definida no
js_detailInf.js. Isto e necessario para o combo de pastas do inferior

Parametros: 
comboNumber     1 ou 2
optValue        value do option selecionado no combo

Retorno:
nenhum
********************************************************************/
function __combo_1_Adj_Variables(comboNumber, optValue) {
    if ((optValue == '') || (optValue == null))
        return;

    // Inicializacao das variaveis de pesquisa para paginacao de grid
    glb_bCheckInvert = false;
    glb_sArgument = '';
    glb_sFilterPesq = '';
    glb_scmbLinePerPageSel = '100';
    glb_vOptParam1 = '';
    glb_vOptParam2 = '';
    glb_useParallelDSO = null;

    glb_nPAGENUMBER = 1;
    glb_btnCtlSupInf = 'INFCOMBO1';

    var nQtyGridsInFolder = folderHasGrid();

    // Pasta corrente nao tem grid
    if (nQtyGridsInFolder == 0)
        glb_currDSO = dso01JoinSup;
    // Pasta corrente tem 1 grid
    else if (nQtyGridsInFolder == 1) {
        if (ascan(glb_aFoldersPaging, optValue, false) >= 0) {
            glb_useParallelDSO = true;
            glb_currDSO = dso01PgGrid;
        }
        else
            glb_currDSO = dso01Grid;
    }
}

// FINAL DE FUNCOES DE INICIALIZACAO ********************************
