/********************************************************************
js_gridsex.js

Library javascript de funcoes basicas para grids
********************************************************************/

// CONSTANTES *******************************************************

// esta variavel e setada nas paginas que nao sao pesqList,
// inf ou sup. Por exemplo, paginas modais seta no commommodal.js
var glb_HasGridButIsNotFormPage = null;

// Controla se o grid esta em construcao
var glb_GridIsBuilding = false;

// parametro para grid com linha de totalizacao
var glb_totalCols__ = false;

// Tecla especial pressionada em grid
var glb_keyBoardKey = null;

var glb_gridTimerVar = null;

// array de celulas com hint
// [[Row,Col,'Hint'], [Row,Col,'Hint'], ...]
var glb_aCelHint = null;
var glb_currCellRow = -1;
var glb_currCellCol = -1;
var glb_validRow    = -1;
var glb_validCol    = -1;
var glb_FreezeRolColChangeEvents = false;

// array de linhas read only
var __glb_aLinesState = null;

// zebra o grid
var glb_bLinesLikeContinuousForm = null;

// FINAL DE CONSTANTES **********************************************

/********************************************************************
INDICE DAS FUNCOES:

    startGridInterface(grid, app, bStyle)
    headerGrid(grid,sHeader,aColInvisible)
    fillGridMask(grid, dsoData, aFields, aMasks, aMasksV)
    alignColsInGrid(grid, aRightPos)
    insertcomboData(grid,nCol,dso,sField,sFieldID)
    removeCombo(grid,nCol)
    criticLineGridAndFillDsoEx(grid, dsoData, nLine, aFields, aID)
    walkInGrid(grid,direction)
    addNewLine(grid)
    deleteLine(grid)
    trimLine(grid,dsoData)
    trimStr(strToTrim)
    trimInternalNumStr(strToTrim)
    validDateInGrid(sData)
    fieldMaxLength(grid, nCol, dsoData)
    gridHasTotalLine(grid, lineLabel, lineBkColor, lineFColor, useBold, aTotLine)
    gridIsEditable(grid)
    treatNumericCell(vTemp)
    getCellValueByColKey(grid, columnName, lineNumber)
    getColIndexByColKey(grid, columnKey)
    setCellValueByColKey(grid, columnName, lineNumber, cellData)
	addAndFillineInGrid(grid, lineData, rowPosition, bSelectNewLine)
	deleteLineInGrid(grid, lineToRemove, bSelectPreviousLine)
	setLineAfterDelete(grid, lineToDelete)
	setLinesState(aLinesState)
	
	setLinesLikeContinuousForm(grid, zebColor)
	setLinesDoubleLikeContinuousForm(grid, zebColor)

    fg_KeyPress(KeyAscii)    
    js_fg_MouseMove (grid, Button, Shift, X, Y)
    js_fg_AfterRowColChange(grid, oldRow, oldCol, newRow, newCol)
    js_fg_AfterRowColChange2 (grid, OldRow, OldCol, NewRow, NewCol)
    js_fg_BeforeRowColChange(grid, oldRow, oldCol, newRow, newCol, cancel)
    js_fg_EnterCell(grid)
    js_fg_KeyPressEdit(Row, Col, KeyAscii)
    
    treatCheckBoxReadOnly(dso, grid, oldRow, oldCol, newRow, newCol)
    treatCheckBoxReadOnly2(dso, grid, oldRow, oldCol, newRow, newCol)
    selectFirstCellNotReadOnly(grid, row, col, direction)
    setColumnWidthToEdition(grid, dsoData)
    resetGridInterface()
    sortGridByCols(grid, arrayColsSort, nColUniqueID, hasTotalLine)
    unblockColsInPesqList(grid, aCols)
    
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Inicia configuracao padrao para um grid

Parametros:
grid
app         - appearance do grid segundo VB
bStyle      - borderStyle do grid segundo VB

Retorno:
Nenhum
********************************************************************/
function startGridInterface(grid, app, bStyle)
{
    with (grid)
    {
        if ( app == null )
            Appearance = 1;
        else
            Appearance = app;
        
        if ( bStyle == null )
            BorderStyle = 0;
        else
            BorderStyle = bStyle;
        
        FontName = 'Tahoma, Arial, MS SansSerif';
        FontSize = '10';
   		Editable = false;
		AllowUserResizing = 1;
		// DataMode = 1;
		VirtualData = true;
		Ellipsis = 1;
		SelectionMode = 1;
		AllowSelection = false;
		Rows = 1;
		Cols = 40;
		FixedRows = 1;
		ScrollBars = 3;
		AllowUserFreezing = 1;
		FrozenCols = 0;
        MergeCells = 0;
        
        // ajusta a variavel da automacao para grid com linha de total
		glb_totalCols__ = false;
    }

    // array de celulas com hint
    glb_aCelHint = null;
    
    if ( app == null )
        drawBordersAroundTheGrid(grid);
}

/********************************************************************
Desenha bordas verticais para grid simples.
Para duplos, desenha a borda esquerda do primeiro e a borda direita
do segundo.
********************************************************************/
function drawBordersAroundTheGrid(grid)
{
    var elemBorder = window.document.getElementById('hr_L_FGBorder');
    
    if ( elemBorder != null )
    {
        with (elemBorder.style)
        {
            left = parseInt(grid.currentStyle.left, 10) - 1;
            top = parseInt(grid.currentStyle.top, 10) + 1;
            width = 1;
            height = parseInt(grid.currentStyle.height, 10) - 1;
            backgroundColor = 'black';
        }
    }
    
    elemBorder = window.document.getElementById('hr_R_FGBorder');
    
    if ( elemBorder != null )
    {
        with (elemBorder.style)
        {
            left = parseInt(grid.currentStyle.left, 10) +
                   parseInt(grid.currentStyle.width, 10);
            top = parseInt(grid.currentStyle.top, 10);
            width = 1;
            height = parseInt(grid.currentStyle.height, 10);
            backgroundColor = 'black';
        }
    }

    elemBorder = window.document.getElementById('hr_B_FGBorder');
    
    if ( elemBorder != null )
    {
        with (elemBorder.style)
        {
            left = parseInt(grid.currentStyle.left, 10) - 1;
            top = parseInt(grid.currentStyle.top, 10) + 
                  parseInt(grid.currentStyle.height, 10);
            width = parseInt(grid.currentStyle.width, 10) + 2;
            height = 1;
            backgroundColor = 'black';
        }
    }
}

/********************************************************************
Insere o cabecalho do grid e informa quais colunas sao escondidas

Parametros:
grid            :Objeto grid que recebe o header
sHeader         :Array de strings que contem o header
aColInvisible   :Array que contem o(s) numero(s) da(s) coluna(s) escondida(s)

Retorno:
Nenhum
********************************************************************/
function headerGrid(grid,sHeader,aColInvisible)
{
    glb_aCelHint = null;
    glb_validRow    = -1;
    glb_validCol    = -1;
    
    grid.Redraw = 0;
    
    // o parametro aColInvisible determina a visibilidade da coluna correspondente
    // a ele
    var i=0, sFillHeader = '';
    var j = 0;

    // seta todas as colunas escondidas para visiveis
    // e esconde somente a aColInvisible na rotina abaixo
    for ( i = 0; i < grid.Cols ; i++ )
    {
        grid.ColHidden(i) = false;
        grid.ColDataType(i) = 12; // format variant
    }

    with (grid)
    {
        Redraw = false;
        Clear();
        FixedCols = 0;
        GridColor = 0X808080;
        Cols = sHeader.length;
        for (i=0; i<aColInvisible.length; i++)
        {
            grid.ColHidden(aColInvisible[i]) = true;
        }    
        Rows = 1;
        FixedRows = 1;
    }
    
    for ( i = 0; i < sHeader.length ; i++ )
    {
        sFillHeader += translateTerm(sHeader[i], null);
        
        if ( i < sHeader.length - 1 )
            sFillHeader += '\t';
    }

    grid.FormatString = sFillHeader;
}

/********************************************************************
Preenche um grid partindo de um 
datasource , e formata os campos no grid
a partir do array aMasks 
Quando um elemento do array aFields comecar com ^, significa que e um campo
com link para uma outra tabela
estrutura para um link:
Ex: ^NomeCampoid^NomeDataSourceAPesquisar^NomeCampoPesquisado^NomeCampoAMostrar
quando um elemento do array aFields comecar com _calc_ , significa que e um campo
calculado e que deve seguir a seguinte convencao:
Ex: _calc_NomeDoCampo_10 (10 e o tamanho do campo calculado)

Nome de campo que terminar com * sera considerado pelo grid como campo
somente leitura
Exemplo:  'EstadoID*'

Parametros:
grid        :Objeto grid que sera preenchido com dados
dsoData     :Objeto RDS que sera usado para preencher o grid com dados
aFields     :Array dos campos a serem inseridos no grid
aMasks      :Array de mascaras de campos para digitacao
aMasksV     :Array de mascaras de campos para visualizacao

Retorno:
Nenhum
********************************************************************/
function fillGridMask(grid, dsoData, aFields, aMasks, aMasksV)
{
    grid.Enabled = false;
    
    __glb_aLinesState = null;
    
    glb_totalCols__ = false;
    glb_currCellRow = -1;
    glb_currCellCol = -1;
    
    if (glb_aCelHint != null)
    {
        grid.parentElement.onmouseenter = __MouseOutGrid;
        grid.parentElement.onmouseleave = __MouseOutGrid;
    }
    
    grid.FrozenRows = 0;
    
    var i = 0, strFields = '';
    var j = 0;
    
    var aHeader = new Array();
    
    var sHeader = '';
    var nLenght = 0;
    var dso = dsoData;
    var sFldName = '';
    var lastPos = 0;
    var lIsCalcField = false;
    var aCalcFields;
    var currFieldName;
    var aTextsInCombo;
    var nLength;
    var nLookUpValue;
    var sDisplayValue;
    
    var sField;
    var sdso;
    var sFindField;
    var sDisplayField;
    var currParam;
    var tblName = '';
    
    aHeader = grid.FormatString.split('\t');
    
    for ( i = 0; i < aFields.length ; i++ ) // seta o colkey de cada coluna para o nome de
                                              // seu respectivo campo
    {
        nLength = 0;
        dso = dsoData;
        sFldName = aFields[i];
        lastPos = sFldName.length-1;
        
        if (sFldName.substr(lastPos,1) == '*')
        {
            sFldName = sFldName.substr(0,lastPos); // retira o asterisco do nome do campo
        }    
        
        if (sFldName.substr(0,1) == '^') // se for campo lookup
        {
            aLookUp = sFldName.split('^');
            sFldName = aLookUp[4];
            dso = eval(aLookUp[2]);
            
			// Se o campo lookup for do tipo boolean
			if ( !((dso.recordset.BOF) && (dso.recordset.EOF)) )
			{
				if (dso.recordset[sFldName].type == 11) // se for boolean adiciona checkbox no grid
				    grid.ColDataType(i) = 11; // format boolean (checkbox)
			}    
        }

        lIsCalcField = (sFldName.substr(0,6).toUpperCase() == '_CALC_');
        
        if (lIsCalcField)
        {
            // da parse na string que contem o nome do campo
            // retornando um array de 3 elementos sendo
            // 1-> calc 2->Nome do Campo 3->Tamanho do campo
            aCalcFields = sFldName.split('_');
            nLength = parseInt(aCalcFields[3], 10);
        }
        else if (grid.ColComboList(i) != '') // se tiver combo nesta coluna
        {
            aTextsInCombo = (grid.ColComboList(i)).split(';');
            j=0;
            nLength = 0;
            for (j=0;j<aTextsInCombo.length;j++)
            {
                sColSeparator = String.fromCharCode(9);
                if (aTextsInCombo[j].lastIndexOf(sColSeparator) > 0)
                    nLength = Math.max(nLength,aTextsInCombo[j].indexOf(sColSeparator));
                else
                {
                    if (j == (aTextsInCombo.length -1)) // se for ultimo item
                        nLength = Math.max(nLength,aTextsInCombo[j].length);
                    else
                        nLength = Math.max(nLength,aTextsInCombo[j].lastIndexOf('|'));
                }
            }
            nLength = ( nLength <= 2 ? 3 : nLength );
            nLength += 3;
        }
        else
        {
            if ( (dso.recordset[sFldName].type == 200) || (dso.recordset[sFldName].type == 129)) // se varchar ou char
                nLength = dso.recordset[sFldName].size;
            else if ((dso.recordset[sFldName].type == 131) ||
                     (dso.recordset[sFldName].type == 14)) // Se numerico ou decimal
                nLength = dso.recordset[sFldName].precision;
            // campo no dso e int ou big int
            else if ( (dso.recordset[sFldName].type == 3) ||
                 (dso.recordset[sFldName].type == 20) )  
            {
                // Nome da tabela
				tblName = '';
				try
				{
					tblName = dso.recordset.Fields[sFldName].table;
					nLength = sendJSMessage('', JS_FIELDINTLEN, tblName, sFldName);
				}
				catch(e)
				{
					nLength = 0;
				}
				
				if (nLength <= 0)
				{
					if ( ((sFldName.substr(sFldName.length - 2)).toUpperCase() == 'ID') ||
						 ((sFldName.substr(sFldName.length - 3)).toUpperCase() == 'ID*') )
						nLength = 10;
					else
						nLength = 5;
				}
            }
            else if (dso.recordset[sFldName].type == 135) // se data
                nLength = 10;

            nLength = ( nLength <= 2 ? 3 : nLength );
        }
        
        nLength = Math.min(nLength,50);
        
        if ((aHeader[i]).length < nLength)
            sHeader += aHeader[i] + replicate(' ',(nLength - aHeader[i].length) * 1.8);
        else    
            sHeader += aHeader[i];
            
        if ( i < aFields.length - 1 )
            sHeader += '\t';
    }
    
    grid.FormatString = sHeader;
    
    for ( i = 0; i < aFields.length ; i++ ) // seta o colkey de cada coluna para o nome de
                                            // seu respectivo campo
        grid.ColKey(i) = aFields[i];

    
    putMasksInGrid(grid, aMasks, aMasksV);

    for ( i = 0; i < aFields.length ; i++ )
    {
        sFldName = aFields[i];
        lastPos = sFldName.length-1;
        if (sFldName.substr(lastPos,1) == '*')
            sFldName = sFldName.substr(0,lastPos); // retira o asterisco do nome do campo

        if ( (sFldName.substr(0,1) == '^') ||
             (sFldName.substr(0,6).toUpperCase() == '_CALC_') ) // se for campo lookup ou calculado
            continue;

		if (dsoData.recordset[sFldName].type == 11) // se for boolean adiciona checkbox no grid
			grid.ColDataType(i) = 11; // format boolean (checkbox)
		else if (dsoData.recordset[sFldName].type == 135) // se for data
			grid.ColDataType(i) = 7; // format data
		else
			grid.ColDataType(i) = 12; // format variant
    }
    
    if (dsoData.recordset.RecordCount() < 1)
    {
        with (grid)
        {
            Refresh();
            Redraw = 2;
            Enabled = true;
        }
        return;
    }   
    
    dsoData.recordset.MoveFirst();
    
    while (!dsoData.recordset.EOF)
    {
        with (grid)
        {
            Rows++;
            for ( i = 0; i < aFields.length ; i++ )
            {
                currFieldName = aFields[i];

                if (currFieldName.substr(0,6).toUpperCase() == '_CALC_') // se for campo calculado
                    continue;
                    
                if (currFieldName.substr(0,1) == '^')
                {
                    sField = '';
                    sdso = '';
                    sFindField = '';
                    sDisplayField = '';
                    currParam = 0;
                    for (j=0; j<currFieldName.length; j++)
                    {
                        if (currFieldName.substr(j,1) == '^')
                            currParam++;

                        if ((currParam == 1) && (currFieldName.substr(j,1) != '^'))
                            sField+=currFieldName.substr(j,1);
                        else if ((currParam == 2) && (currFieldName.substr(j,1) != '^'))
                            sdso+=currFieldName.substr(j,1);
                        else if ((currParam == 3) && (currFieldName.substr(j,1) != '^'))
                            sFindField+=currFieldName.substr(j,1);
                        else if ((currParam == 4) && (currFieldName.substr(j,1) != '^') &&
                                 (currFieldName.substr(j,1) != '*'))
                            sDisplayField+=currFieldName.substr(j,1);
                    }
                    if ( (!(eval(sdso+'.recordset.BOF && '+sdso+'.recordset.EOF'))) && (dsoData.recordset[sField].value != null) )
                    {
                        eval(sdso+'.recordset.moveFirst()');
                        nLookUpValue = dsoData.recordset[sField].value;
                        eval(sdso + ".recordset.Find('" + sFindField + "', " + nLookUpValue+ ")");
                        if ( !eval(sdso+'.recordset.EOF') )
                        {
                            sDisplayValue = eval(sdso+'.recordset['+'\''+sDisplayField+'\''+'].value');
                            grid.TextMatrix(grid.Rows - 1, i) = (sDisplayValue == null ? '' : sDisplayValue);
                        }    
                    }
                }
                else
                {
                    sFldName = aFields[i];
                    lastPos = sFldName.length-1;
                    if (sFldName.substr(lastPos,1) == '*')
                        sFldName = sFldName.substr(0,lastPos); // retira o asterisco do nome do campo

                    if (dsoData.recordset[sFldName].value != null)
                    {
						switch(dsoData.recordset[sFldName].type)
						{
							case dataTypesCode["datetime"]:
							{
								if(aMasksV != null && aMasksV[i] != null && aMasksV[i] != "")
								{
									grid.TextMatrix(grid.Rows - 1, i) = 
										dsoData.recordset[sFldName].asDatetime(aMasksV[i]);
								}
								else
								{
									grid.TextMatrix(grid.Rows - 1, i) = 
										dsoData.recordset[sFldName].asDatetime("dd/mm/yyyy");
								}
								
								break;
							}
							case dataTypesCode["bit"]:
							{
								grid.TextMatrix(grid.Rows - 1, i) = 
									dsoData.recordset[sFldName].value ? 1 : 0;
								
								break;
							}
							default:
							{
								grid.TextMatrix(grid.Rows - 1, i) = 
									dsoData.recordset[sFldName].value;
							}
						}
                    }    
                }    
            }
        }

        dsoData.recordset.MoveNext();
    }
    
    for ( i=0; i<grid.Cols; i++ )
    {
        // todas colunas alinham pelo default do grid
        grid.ColAlignment(i) = 9;
    }    
    
    if ( glb_bLinesLikeContinuousForm != null )
		setLinesDoubleLikeContinuousForm(grid, glb_bLinesLikeContinuousForm);
		
    paintReadOnlyCols(grid);
        
    with (grid)
    {
        // Seleciona a primeira celula nao read only
        if ( Rows > 0 )
        {
            try
            {
				Row = 1;
				selectFirstCellNotReadOnly(grid, 1, 1, 1);
			}
			catch (e)
			{
				Row = 0;
				selectFirstCellNotReadOnly(grid, 0, 1, 1);
			}
        }    
        else
        {
            Row = 0;
            selectFirstCellNotReadOnly(grid, 0, 1, 1);
        }    
            
        // AutoSizeMode = 0;
        // AutoSize(0,aFields.length-1,false,0);
        Redraw = true;
        
        //Refresh();        
        Enabled = true;
    }
    
    if ( grid.Rows > 1 )
        grid.ShowCell(1, 0);
}

/********************************************************************
Alinha os textos das colunas do grid. Por default todas as colunas
alinham a esquerda

Parametros:
grid          - Referencia ao grid
aRigthPos     - array de colunas a alinhar a direita
aRigthPos     - array de colunas a centralizar

Retorno:
Nenhum
********************************************************************/
function alignColsInGrid(grid, aRightPos, aCenterPos)
{
    var i, j;
    
    // alinhamento das colunas
    for ( i=0; i<grid.Cols; i++ )
    {
        // por default, todas as colunas alinham a esquerda
        grid.ColAlignment(i) = 1;
        
        // as colunas que alinham a direita
        if ( aRightPos != null )
        {
            for (j=0; j<aRightPos.length; j++)
                if ( aRightPos[j] == i )
                    grid.ColAlignment(i) = 7;
        }
        // centraliza as colunas
        if (aCenterPos != null) {
            for (j = 0; j < aCenterPos.length; j++)
                if (aCenterPos[j] == i)
                    grid.ColAlignment(i) = 4;
        }
    }
    
}

/********************************************************************
Insere um combo em uma coluna no grid

Parametros:
grid        :Objeto grid que sera inserido um combo
nCol        :Numero da coluna no grid que recebera o combo
dso         :Objeto RDS que contem os dados que preencherao o combo
sField      :Nome do campo a ser mostrado no combo
sFieldID    :Nome do campo que comtem o valor a ser gravado no dso do grid

Retorno:
Nenhum
********************************************************************/
function insertcomboData(grid,nCol,dso,sField,sFieldID)
{
    if ( (dso.recordset.BOF) && (dso.recordset.EOF) )
    {
        grid.ColComboList(nCol) = '#;';
    }
    else
    {
        grid.ColComboList(nCol) = buildComboList(dso, sField, sFieldID);
    }
}

/********************************************************************
Formata a string recebida no DSO.

Parametros:
dso        : Objeto DSO com os dados a serem concatenados na string
sField     : String com o nome do campo a ser inserido no combo. Se
             houver mais de um, serao separados por |.
sFieldID   : Id do registro

Retorno:
Nenhum
********************************************************************/
function buildComboList(dso, sField, sFieldID)
{
    var result = "";
    var aFields = sField.split("|");
    var indicadorDeNegrito = "";

    // Marca qual campo deve ser exibido em negrito    
    for(i = 0; i < aFields.length; i++)
    {
        if(aFields[i].indexOf("*") != -1)
        {
            indicadorDeNegrito = "*" + i;
            
            aFields[i] = replaceStr(aFields[i], "*", "");
        }
    }
    
    // Para garantir que todos entrem
    dso.recordset.moveFirst();
    
    // Concatena o primeiro ID
    result += "#" + dso.recordset[sFieldID].value + indicadorDeNegrito + ";";
    
    // Monta o resultado
    while(!dso.recordset.EOF)
    {
        // Concatena os campos.
        for(i = 0; i < aFields.length; i++)
        {
            result += dso.recordset[aFields[i]].value;
            
            // Se ouver outro campo insere o separador de campos que nao sao ID.
            if(i+1 < aFields.length)
            {
                result += String.fromCharCode(9);
            }
        }
        
        // Move para o proximo registro.
        dso.recordset.moveNext();
        
        // Se nao for o fim dos registros, fecha a formatacao do registro com pipe
        if(!dso.recordset.EOF)
        {
            // Concatena o proximo ID
            result += "|#" + dso.recordset[sFieldID].value + ";";
        }
    }
    
    return result;
}

/********************************************************************
Remove Todos os combos do grid

Parametros:
grid        :Objeto grid a ter os combos removidos

Retorno:
Nenhum
********************************************************************/
function removeCombo(grid)
{
    var i = 0;
    for ( i = 0; i < grid.Cols; i++ )
        grid.ColComboList(i) = '';    

    grid.ComboList = '';
}

/********************************************************************
Critica os dados inseridos em uma linha de grid e estando
certo, coloca-os no dso que esta fornecendo dados para o grid.
Nome anterior: gravaInGrid

Parametros:
ver abaixo

Retorno:
Tudo OK retorna a linha corrente do grid (>0). Falha retorna -1.
********************************************************************/
function criticLineGridAndFillDsoEx(grid, dsoData, nLine, aFields, aID, aColsHiddenWriteable)
{
    // Parametros:
    // grid --> O grid a ser tratado
    // dsoData --> Datasource
    // nLine --> Linha do grid a ser gravada
    // aFields --> Array que contem as colunas obrigatorias
    // aID --> string 'PessoaID' mais vlr do PessoaID, se 0 nao grava
    // aColsHiddenWriteable --> Array das colunas escondidas que sao gravaveis
        
    // esta funcao retorna a linha corrente do grid 
    // -1 a gravacao nao sera feita
    // maior que 0 vai gravar o registro
    
    trimLine(grid,dsoData);
    
    var i = 0;
    var j = 0;
    var k = 0;
    var nRet = -1;
    var sIndentifier;
    var sFldName;
    var lastPos;
    var lReadOnly;
    var lIsCalcField;
    var dDateToWrite;
    var nCol;
    var sFldName;
    var lastPos;
  	var vTemp;
	var vTemp1;
	var chTemp;
	var signPos;
	var re;
	var tempAttr;
	var bFieldIsNull = false;
    var fldIDValue;
    var theMask;
    var dateMask;
    var hourMask;
    var strDate;
    var strHour;
    
    // Verifica preenchimento de campos obrigatorios
    for ( i = 0; i < aFields.length; i++ )
    {
        // NULL ***
        bFieldIsNull = false;
        
        nCol = aFields[i];
    
        // Se o campo e numerico e o conteudo da celula nao e numero
        // coloca null no conteudo da celula
        sFldName = grid.ColKey(nCol);
        
        if ( (sFldName.substr(0,1) == '^') ||
             (sFldName.substr(0,6).toUpperCase() == '_CALC_') ) // se for campo lookup ou calculado
            continue;
        
        if ( sFldName.substr(sFldName.length - 1) == '*' )
            sFldName = sFldName.substr(0, sFldName.length - 1);
            
        // Se o campo e numerico, int ou bigint e seu conteudo nao e um numerico valido
        if ( (dsoData.recordset.Fields[sFldName].type == 131) && (isNaN(grid.ValueMatrix(nLine,nCol))) )
            bFieldIsNull = true;
        else if ( (dsoData.recordset.Fields[sFldName].type == 3) && (isNaN(grid.ValueMatrix(nLine,nCol))) )
            bFieldIsNull = true;
        else if ( (dsoData.recordset.Fields[sFldName].type == 20) && (isNaN(grid.ValueMatrix(nLine,nCol))) )
            bFieldIsNull = true;    
            
        // Campos IDs obrigatorios nao podem ser gravados com 0
        if ( ( (((grid.ColKey(nCol)).substr((grid.ColKey(nCol)).length - 2)).toUpperCase() == 'ID') ||
             (((grid.ColKey(nCol)).substr((grid.ColKey(nCol)).length - 3)).toUpperCase() == 'ID*') ) && dsoData.recordset.Fields[sFldName].attributes == 0)
        {     
            if (grid.ValueMatrix(nLine,nCol) == 0)
                bFieldIsNull = true;
        }     
        
        if ( (grid.TextMatrix(nLine,nCol) == '') || (grid.TextMatrix(nLine,nCol) == null) || (bFieldIsNull) )
        {
            if ( window.top.overflyGen.Alert('O campo '+grid.TextMatrix(0,nCol)+' n�o foi preenchido...') == 0 )
                return null;
            
            grid.Row = nLine;
            grid.Col = nCol;
            
            return nRet;
        }
        // Final de NULL ****
    }
    
    // critica celulas de datas
    // 1. loop no grid verificando se a celula contem um valor tipo data
    for ( i=0; i<grid.Cols; i++ )
    {
        sIndentifier = (grid.ColKey(i)).substr(0,1);
        lIsCalcField = ((grid.ColKey(i)).substr(0,6)).toUpperCase() == '_CALC_';
        // se o primeiro caracter do ColKey(i) for = '^', nao grava. pois � um
        // campo lookup
        
        sFldName = grid.ColKey(i);
        lastPos = sFldName.length-1;
        lReadOnly = sFldName.substr(lastPos,1) == '*'; // campo read-only
        
        if ((sIndentifier == '^') || (lReadOnly) || (lIsCalcField))
            continue;

        // Tratamento de data    
        if ( dsoData.recordset.Fields[grid.ColKey(i)].type == 135 )
        {
            if( grid.TextMatrix(grid.Row, i) != '' )
            {
                if ( chkDataEx(grid.TextMatrix(grid.Row, i)) == null)
                {
                    // a data e nula, zera o campo
                    grid.TextMatrix(grid.Row, i) = '';
                    continue;
                }
                
                // verifica se a mascara de digitacao tem hora e se tiver
                // e o usuario nao digitou estripa a hora completa ajusta
                // a mascara: '99/99/9999 99:99'
                theMask = grid.ColEditMask(i);
                
                // a mascara tem hora?
                dateMask = '';
                hourMask = '';
                if ( theMask.lastIndexOf(':') >=0 )
                {
                    dateMask = trimStr(theMask.substr(0, theMask.lastIndexOf(' ')));
                    hourMask = trimStr(theMask.substr(theMask.lastIndexOf(' ')));
                }
                
                if (hourMask != '')
                {
                    strDate = '';
                    strHour = '';
               
                    strDate = trimStr((grid.TextMatrix(grid.Row, i)).substr(0, theMask.lastIndexOf(' ')));
                    strHour = trimStr((grid.TextMatrix(grid.Row, i)).substr(theMask.lastIndexOf(' ')));
                    
                    // O usuario nao digitou a parte de hora e minuto
                    if (strHour == ':')
                        grid.TextMatrix(grid.Row, i) = strDate + ' 00:00';
                     
                }
               
                if ( chkDataEx(grid.TextMatrix(grid.Row, i)) == false)
                {
                    if ( window.top.overflyGen.Alert('Data inv�lida!') == 0 )
                        return null;
                        
                    grid.TextMatrix(grid.Row, i) = '';
                        
                    return -1;
                }
               
            }
        }    
    }
    
    fldIDValue = grid.TextMatrix(grid.Row,grid.Cols - 1);
                                   
    if ((fldIDValue == '') || (fldIDValue == null)) // � um registro novo, pq nao tem ID
    {
		var field;

		// Move o ponteiro para o �ltimo registro e ...
		dsoData.recordset.MoveLast();
		
		// ... guarda o indice do registro e o buffer de registros para ...
		var reg = dsoData.recordset.recordPointer;
		var buffer = dsoData.recordset.aRecords;
		
		// ... saber se o se est� no fim do buffer ou o registro n�o � um registro novo, ...
		var addNewRecord = dsoData.recordset.EOF || (buffer[reg][buffer[reg].length - 1] != REGISTRO_NOVO);
			
		// ... pois se ele for um registro novo, faz-se uma varredura ...
		for(field = 0; !addNewRecord && field < dsoData.recordset.fields.count; field++) {
			// ... para saber se a(s) chave(s) prim�ria(s) cont�m valore(s), ...
			if(dsoData.recordset.fields[field].attributes & attributesValues['IsKey']) {
				addNewRecord &= (buffer[reg][field].value != null);
			}
		}
		
		// ... isso significa que � necess�rio incluir um novo registro.
		if(addNewRecord) {
			dsoData.recordset.AddNew();
		}

        if ((aID[0] != '') && (aID[0] != null) && (aID[1] > 0))
            dsoData.recordset[aID[0]].value = aID[1]; // grava ID no registro
        
        for (i=0;i<dsoData.recordset.fields.Count;i++)
        {
            sFldName = dsoData.recordset.fields[i].name;

            if (sFldName.toUpperCase() == 'ESTADOID')
            {
                var _initialState = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                          'getInitialState(' + '\'' + 'INF' + '\'' + ')');

                // Forca estado de cadastrado se o estado inicial nao foi encontrado                
                if ( (_initialState == null) || (_initialState ==0) )
                    _initialState = 1;
                
                dsoData.recordset[sFldName].value = _initialState;
            }
            else if (sFldName.toUpperCase() == 'PROPRIETARIOID')
                dsoData.recordset[sFldName].value = getCurrUserID();
            else if (sFldName.toUpperCase() == 'ALTERNATIVOID')
                dsoData.recordset[sFldName].value = getCurrUserID();
        }
        
        tempAttr = dsoData.recordset[grid.ColKey(grid.Cols-1)].attributes;
        
		// Campo deve ser parte de primary key e nao pode ser identity
		if ((tempAttr & attributesValues['IsKey']) && !(tempAttr & attributesValues['IsIdentity']) && !(tempAttr & attributesValues['IsAutoIncrement']))
		{
			dsoData.recordset[grid.ColKey(grid.Cols-1)].value = -1;
		}
        
        nRet = 0;
    }
    else // � um registro existente, pq tem ID
    {
        // posiciona o recordset
        dsoData.recordset.MoveFirst();

        dsoData.recordset.Find(grid.ColKey(grid.Cols-1), fldIDValue);
        nRet = grid.Row; // retornar linha corrente
    }
    
    for ( i = 0; i < grid.Cols ; i++ ) // -1 porque a ultima coluna � a escondida
                                         // e a funcao dela � somente guardar o ID
    {
        // verifica se a coluna e escondida e se ela e gravavel
        if (aColsHiddenWriteable != null)
        {
            if (grid.ColHidden(i) == true)
            {
                if (ascan(aColsHiddenWriteable,i,false) == -1)
                    continue;
            }    
        }
        else if (grid.ColHidden(i) == true)
            continue;

        sIndentifier = (grid.ColKey(i)).substr(0,1);
        // se o primeiro caracter do ColKey(i) for = '^', nao grava. pois � um
        // campo lookup
        
        sFldName = grid.ColKey(i);
        lastPos = sFldName.length-1;
        lReadOnly = sFldName.substr(lastPos,1) == '*'; // campo read-only
        lIsCalcField = ((grid.ColKey(i)).substr(0,6)).toUpperCase() == '_CALC_';
        
        // se nao for lookup e nao for campo calculado
        if ((sIndentifier != '^') && (! lIsCalcField))
        {
            if (sFldName.substr(lastPos,1) == '*')
                sFldName = sFldName.substr(0,lastPos); // retira o asterisco do nome do campo
            
            if ( (dsoData.recordset.Fields[sFldName].type == 131) || 
				 (dsoData.recordset.Fields[sFldName].type == 3) ||
				 (dsoData.recordset.Fields[sFldName].type == 20)) // numerico, int ou big int
            {
				vTemp = grid.TextMatrix(nLine,i);
				vTemp1 = null;
				chTemp = null;
				
				// 1. Troca ultima virgula por ponto
				signPos = vTemp.lastIndexOf(',');
				re = /,/g;
				if (signPos != -1)
					vTemp = vTemp.replace(re, '.');
					
				// 2. Elimina qualquer caracter que nao seja numerico ou ponto
				// e tambem elimina o ponto se ele e o ultimo caracter	
	            vTemp1 = vTemp;
	            j = vTemp.length;
	            vTemp = '';
	            
	            for ( k=0; k<j; k++ )
	            {
	                chTemp = vTemp1.charAt(k);
	                
	                if ( k == (j - 1) )
	                    if (chTemp == '.')
	                        break;
	                
	                if ( (chTemp == '-') ||
	                     (chTemp == '.') ||
	                     (chTemp == '0') ||
	                     (chTemp == '1') ||
	                     (chTemp == '2') ||
	                     (chTemp == '3') ||
	                     (chTemp == '4') ||
	                     (chTemp == '5') ||
	                     (chTemp == '6') ||
	                     (chTemp == '7') ||
	                     (chTemp == '8') ||
	                     (chTemp == '9') )
	                    vTemp += chTemp;
	            }
	            
	            // Se numero tem sinal, elimina tudo o que esta a esquerda do sinal
	            if ( (vTemp.indexOf('-') >= 0) || (vTemp.indexOf('+') >= 0)  )
	            {
	                vTemp1 = vTemp;
	                vTemp = '';
	                
	                for ( k=0; k<vTemp1.length; k++ )
	                {
	                    if ( vTemp1.charCodeAt(k) == 48 )
	                        continue;
	                    else
	                        break;    
	                }
	                vTemp = vTemp1.substr(k);
	            }
				
				// 4. Coloca o valor no dso
				// Campo nao pode ser parte de primary key
                if ( ~dsoData.recordset.Fields[sFldName].attributes & 32768 )
                {					
				    if (vTemp == '')
				        dsoData.recordset[sFldName].value = null;
				    else
				        dsoData.recordset[sFldName].value = vTemp;
				}        

			}	
            else if ( dsoData.recordset.Fields[sFldName].type == 135 ) // data alterar
            {
                if (grid.TextMatrix(nLine,i) == '')
                    dsoData.recordset[sFldName].value = null;
                else
                    dsoData.recordset[sFldName].value = grid.TextMatrix(nLine,i);
            }
            else if ( dsoData.recordset.Fields[sFldName].type == 11 ) // boolean alterar
            {
                if (grid.TextMatrix(nLine,i) == '')
                    dsoData.recordset[sFldName].value = 0;
                else    
                    dsoData.recordset[sFldName].value = grid.TextMatrix(nLine,i);
            }
            else if ( (dsoData.recordset[sFldName].type == 200) || (dsoData.recordset[sFldName].type == 129) ) // varchar ou char alterado
            {
                if (grid.TextMatrix(nLine,i) == '')
                    dsoData.recordset[sFldName].value = null;
                else
                    dsoData.recordset[sFldName].value = grid.TextMatrix(nLine,i);
            }
            else
				dsoData.recordset[sFldName].value = grid.TextMatrix(nLine,i);
        }    
    }
    
    return nRet;
}

/********************************************************************
Move a linha corrente do grid

Parametros:
grid        :Objeto grid a ter os combos removidos
direction   :Integer (1 --> anda para frente, -1 --> anda para tras )

Retorno:
Nenhum
********************************************************************/
function walkInGrid(grid,direction)
{
    // direction =  1 --> anda para frente
    // direction = -1 --> anda para tras
    if (direction == 1)
    {
        if (grid.Row < grid.Rows -1 )
        {
            grid.Row++;

            if (grid.BottomRow > 4)
                grid.TopRow = grid.Row;
        }
    }   
    else
    {
        if (grid.Row > 1)
        {
            grid.Row--;

            if (grid.Row < grid.TopRow)
                grid.TopRow = grid.Row;
        }
    }
}

/********************************************************************
Adiciona um linha em branco no grid

Parametros:
grid        :Objeto grid

Retorno:
Nenhum
********************************************************************/
function addNewLine(grid)
{
    with (grid)
    {
        Redraw = 0;
        SelectionMode = 0;
        Rows= Rows + 1;
        Row = Rows - 1;
        // Col = Cols -1;
        // Col = 0;
        // TopRow = Row;
    }    
        
    paintReadOnlyCols(grid, grid.Row);

    with (grid)
    {                
        Col = 0;
        Editable = gridIsEditable(grid);
        selectFirstCellNotReadOnly(grid, grid.Rows - 1, 0, 1);
        TopRow = grid.Rows - 1;
        Redraw = 2;
        TopRow = grid.Rows - 1;
    }
}

/********************************************************************
Deleta uma linha do grid

Parametros:
grid        :Objeto grid

Retorno:
Nenhum
********************************************************************/
function deleteLine(grid)
{
    var delLine = grid.Row;
    if (grid.Rows > 1)
        grid.RemoveItem(delLine);
    else
        return;

    if (delLine > 1)
       grid.Row = delLine - 1;
    else if (delLine == 1)
    {
        if (grid.Rows > 1)
            grid.Row = 1;
    }
}

/********************************************************************
Trima todos os campos de uma determinada linha

Parametros:
grid        :Objeto grid
dsData      :Objeto RDS correspondente

Retorno:
Nenhum
********************************************************************/
function trimLine(grid,dsoData)
{
    // funcao interna da biblioteca
    // que trima os campos de uma linha do grid
    var i = 0;
    var sFldName = '';
    
    for ( i = 0; i < grid.Cols; i++ )
    {
        grid.TextMatrix(grid.Row, i) = trimStr(grid.TextMatrix(grid.Row, i));
        
        sFldName = grid.ColKey(i);
        
        // trima espacos em branco no interior da celula se
        // for valor numerico (se for o caso)
        if ( (sFldName.substr(0,1) == '^') ||
             (sFldName.substr(0,6).toUpperCase() == '_CALC_') ) // se for campo lookup ou calculado
            continue;

        if (fg.ColHidden(i)) // coluna escondida
            continue;
            
        if ( sFldName.substr(sFldName.length - 1) == '*' )
            sFldName = sFldName.substr(0, sFldName.length - 1);

        if (dsoData.recordset.Fields[sFldName].type != 131)
            continue;

        grid.TextMatrix(grid.Row, i) = trimInternalNumStr(grid.TextMatrix(grid.Row, i));
    }
}

/********************************************************************
Valida uma celula do grid de tipo date

Parametros:
sData       :String da data digitada

Retorno:
True  -> Data Valida
False -> Data nao e valida
********************************************************************/
function validDateInGrid(sData)
{
    var nDay,nMonth,nYear, sString, bRet, i,sNewValue;
    var nMonths = new Array();
    sNewValue = '';

    for (i = 0; i< sData.length; i++)
    {
        if ((sData.substr(i,1) != '/') && (sData.substr(i,1) != ' '))
           sNewValue+=sData.substr(i,1);
    }
    
    sString=sNewValue;
    nDay=sString.substr(0,2);
    nMonth=new Number(sString.substr(2,2));
    nYear=new Number(sString.substr(4,4));
    bRet = true;

    if ((sString.length == 0))
        return true;

    if (sString.length != 8)
       bRet = false;

    nMonths[01]=31;
    if (anoBisexto(nYear))
        nMonths[02]=29;
    else
        nMonths[02]=28;

    nMonths[03]=31;
    nMonths[04]=30;
    nMonths[05]=31;
    nMonths[06]=30;
    nMonths[07]=31;
    nMonths[08]=31;
    nMonths[09]=30;
    nMonths[10]=31;
    nMonths[11]=30;
    nMonths[12]=31;

    if ((nDay < 1) | (nDay > nMonths[nMonth]))
        bRet = false;
    else if ((nMonth < 1) | (nMonth > 12))
        bRet = false;
    else if ((nYear < 1700) | (nYear == null))
        bRet = false;
    
    return bRet;
}

/********************************************************************
Funcao interna usada trimar espacos em branco no interior de campos
numericos

Parametros:
strToTrim       :String a ser trimada

Retorno:        :String trimada
********************************************************************/
function trimInternalNumStr(strToTrim)
{
    var tempStr = '';
    var i;
        
    for ( i=0; i<strToTrim.length; i++ )
    {
        if ( strToTrim.charAt(i) == ' ' )
            continue;
        tempStr += strToTrim.charAt(i);
    }    
    
    return tempStr;
}

/********************************************************************
Funcao interna usada pela funcao trimLine() para trimar os campos

Parametros:
strToTrim       :String a ser trimada

Retorno:        :String trimada
********************************************************************/
function trimStr(strToTrim)
{
    if ( strToTrim == null )
        return '';
        
    if ( strToTrim == '' )
        return '';
    
    var i, len;
    var charac, strTrimmed = new String();
    
    // trima lado esquerdo
    len = strToTrim.length;
        
    for (i=0; i<(len); i++)
    {
        charac = strToTrim.charAt(i);
        if ( charac != " " )
            break;
    }
    
    strTrimmed = strToTrim.substr(i);
        
    // trima lado direito
    len = strTrimmed.length;
        
    for (i=(len-1); i>=0; i--)
    {
        charac = strTrimmed.charAt(i);
        if ( charac != " " )
            break;
    }
    
    i++;
    
    strToTrim = strTrimmed.substr(0, i);
    
    return (strToTrim);
}

/********************************************************************
Determina o maxlength de uma determinada coluna

Parametros:
grid        :Objeto grid
nCol        :numero da coluna
dsoData     :Objeto RDS do grid

Retorno:        :String trimada
********************************************************************/
function fieldMaxLength(grid, nCol, dsoData)
{
    var nLength = 0;
    var tblName = 0;
    
    var sFldName = grid.ColKey(nCol);
    var lastPos = sFldName.length-1;
    
    if (sFldName.substr(lastPos,1) == '*')
        sFldName = sFldName.substr(0,lastPos); // retira o asterisco do nome do campo
        
    if ((sFldName).substr(0,1) == '^')
        return;
    
    if ( (dsoData.recordset[sFldName].type == 200) || (dsoData.recordset[sFldName].type == 129) ) // se varchar ou char
        nLength = dsoData.recordset[sFldName].size;
    else if (dsoData.recordset[sFldName].type == 131) // se numerico
        nLength = dsoData.recordset[sFldName].precision;
    // campo no dso e int ou big int
    else if ( (dso.recordset[sFldName].type == 3) ||
            (dso.recordset[sFldName].type == 20) )  
    {
        // Nome da tabela
		tblName = '';
		try
		{
			tblName = dso.recordset.Fields[sFldName].table;
			nLength = sendJSMessage('', JS_FIELDINTLEN, tblName, sFldName);
		}
		catch(e)
		{
			nLength = 0;
		}
    }
    else if (dsoData.recordset[sFldName].type == 135) // se data
        nLength = 10;

    grid.EditMaxLength = nLength;
}

/********************************************************************
Prepara grid para linha de totalizacao

Parametros:
grid            referencia ao grid
lineLabel       label da linha de titulos
lineBkColor     cor de fundo da linha de totais (usar hexa invertido 0X000000)
                ou null
lineFColor      cor dos caracteres da linha de totais  (usar hexa invertido 0X000000)
                ou null
useBold         caracteres em bold (usar false ou true)
a totLine       Array de parametros para linha de totalizacao
                        1 elemento do array - indice da linha
                        2 em diante - mascara e operacao a usar na coluna    
                        As operacoes possiveis sao 'S' soma 'C' count e 'M' media
bNotMoveToTopLine   se null, move para a linha top do grid

Retorno:
nenhum
********************************************************************/
function gridHasTotalLine(grid, lineLabel, lineBkColor, lineFColor, useBold, aTotLine, bNotMoveToTopLine)
{
    var i;
    
    // grid com primeira linha de totalizacao
    if ( grid.Rows > 1 )
    {
        glb_totalCols__ = true;
        
        grid.MultiTotals = true;
        grid.Subtotal(1);
        
        grid.SubtotalPosition = 1;
        
        for ( i=0; i <aTotLine.length; i++ )    
        {
            if ( aTotLine[i][2].toUpperCase() == 'S' )
            {
                grid.Subtotal( 2,
							   -1,
                               aTotLine[i][0],
                               aTotLine[i][1],
                               lineBkColor,
                               lineFColor,
                               useBold,
                               lineLabel );

			}
			else if ( aTotLine[i][2].toUpperCase() == 'C' )
			{
                grid.Subtotal( 4,
                               -1,
                               aTotLine[i][0],
                               aTotLine[i][1],
                               lineBkColor,
                               lineFColor,
                               useBold,
                               lineLabel );    
			}
			else if ( aTotLine[i][2].toUpperCase() == 'M' )
			{
                grid.Subtotal( 5,
                               -1,
                               aTotLine[i][0],
                               aTotLine[i][1],
                               lineBkColor,
                               lineFColor,
                               useBold,
                               lineLabel );    
			}
        }
        
        grid.FrozenRows = 1;
        
        if ( (grid.Rows > 2) && (lineLabel != null) )
			grid.TextMatrix(1, 0) = lineLabel;
        
        if ( bNotMoveToTopLine == null )
        {
			if ( grid.Rows > 2 )
				grid.Row = 2;
        }    
    }
}

/********************************************************************
Por principio o grid e editavel e pelo menos uma coluna visivel nao
e readonly

Parametros:
grid                - referencia ao grid

Retorno:
true ou false
********************************************************************/
function gridIsEditable(grid)
{
    var retVal = false;
    var i;
    var sFldName, lastPos;
    
    for ( i=0; i<grid.Cols; i++ )
    {
        // Primeiro verifica se a coluna nao e read only
        sFldName = grid.ColKey(i);
        lastPos = sFldName.length-1;
        
        if ( (!(sFldName.substr(lastPos,1) == '*')) && (!grid.ColHidden(i)) )
        {
            retVal = true;    
            break;
        }    
    }
    
    return retVal;
}

/********************************************************************
Coloca mascaras de vizualizacao e digitacao no grid

Parametros:
grid            referencia ao grid
aMasks          array das mascaras de digitacao
aMasksV         array das mascaras de vizualizacao

Retorno:
nenhum
********************************************************************/
function putMasksInGrid(grid, aMasks, aMasksV)
{
    var i;
    
    for ( i = 0 ; i < aMasks.length ; i++ )
    {
        with (grid)
        {
                // Mascara de digitacao
                ColEditMask(i) = aMasks[i];
                // Mascara de visualizacao
                if ( aMasksV != null )
                    ColFormat(i) = aMasksV[i];
                else
                    ColFormat(i) = '';
        }
    }
}

/********************************************************************
Pinta as colunas read only do grid

Parametros:
grid            referencia ao grid
theRow          opcional linha a pintar

Retorno:
nenhum
********************************************************************/
function paintReadOnlyCols(grid, theRow)
{
    var i, j, k;
    var aReadOnlyCol = new Array();
    var sFldName, lastPos;
    var origRow = 0;
    var origCol = 0;
    
    if ( (theRow == null) || (theRow == 0) )
        theRow = 1;

    k = 0;
    for (i=0; i<grid.Cols; i++)
    {
        sFldName = grid.ColKey(i);
        lastPos = (grid.ColKey(i)).length-1;
            
        if (sFldName.substr(lastPos,1) == '*')
        {
            // guarda col read only number
            aReadOnlyCol[k] = i;
            k++;
        }    
    }    
        
    // Colunas read only sao coloridas
    if ( (aReadOnlyCol != null) && (grid.Rows > 1) )
    {
		origRow = grid.Row;
		origCol = grid.Col;
		
        for (i=0; i<aReadOnlyCol.length; i++)
        {
            // Substituido pelo for abaixo em 27/04/2002
            // Nova versao do grid nao refresca pintura sob selecao
            // de multiplas celulas
            /*****************
            with (grid)
            {
                Select( theRow, aReadOnlyCol[i], grid.Rows - 1, aReadOnlyCol[i] );
                FillStyle = 1;
                CellBackColor = 0XDCDCDC;
                FillStyle = 0;
            }
            *****************/    
            for (j=1; j<grid.Rows; j++)
            {
                with (grid)
                {
                    Select(j, aReadOnlyCol[i], null, null);
                    FillStyle = 1;
                    CellBackColor = 0XDCDCDC;
                    FillStyle = 0;
                }    
            }
        }
        
        grid.Row = origRow;
		grid.Col = origCol;
    }
}

/********************************************************************
Pinta o background de uma celula

Parametros:
grid                - referencia ao grid
nRow				- linha da celula
nCol				- coluna da celula
nColor				- cor a ser usada

Retorno:
nenhum
********************************************************************/
function paintBackgroundOfACell(grid, nRow, nCol, nColor)
{
    grid.Cell(6, nRow, nCol, nRow, nCol) = nColor;
}

/********************************************************************
Coloca hints nas celulas header do grid
Tambem pode ser usada em grids de modais

Parametros:
grid                - referencia ao grid
Button, Shift, X, Y - conforme VB (X e Y em twips)

Retorno:
nenhum
********************************************************************/
function js_fg_MouseMove (grid, Button, Shift, X, Y)
{
    // array de celulas com hint
    // [Row,Col,'Hint'], [Row,Col,'Hint'], ...'

    if (glb_aCelHint == null)
        return true;
 
    var currRow = grid.MouseRow;
    var currCol = grid.MouseCol;
    var theMsg = '';
    var i;

    if ( (glb_currCellRow != currRow) || (glb_currCellCol != currCol) )
    {
        glb_currCellRow = currRow;
        glb_currCellCol = currCol;
        
        // troca o msg do statusBar do IE
        for (i=0; i<glb_aCelHint.length; i++)
        {
            if ( (glb_aCelHint[i][0] == currRow) && (glb_aCelHint[i][1] == currCol) )
            {
                theMsg = glb_aCelHint[i][2];
                break;
            }    
        }
            
        window.status = theMsg;    
    }
}

function __MouseOutGrid()
{
    glb_currCellRow = -1;
    glb_currCellCol = -1;
    
    window.status = '';
}

/********************************************************************
Impede usuario alterar celula checkbox readonly.
Usada pela funcao afterRowColChange.

Parametros:
dso                 - referencia ao dso que alimenta o grid
grid                - referencia ao grid
oldRow              - linha atual
oldCol              - coluna atual
newRow              - linha para onde pretende ir
newCol              - coluna para onde pretende ir

Retorno:
true, nao prossegue o afterRowColChange
false, prossegue
********************************************************************/
function treatCheckBoxReadOnly(dso, grid, oldRow, oldCol, newRow, newCol)
{
    if ( dso == null )
        return false;
        
    if ( (newRow < 0) || (newCol < 0) )
        return false;
        
    var retVal = false;
    var sRegIDFldName = grid.ColKey(grid.Cols - 1);
    var nRegID = grid.ValueMatrix(newRow, grid.Cols - 1);
    var chkBoxIsReadOnly = (grid.ColKey(newCol)).substr((grid.ColKey(newCol)).length-1, 1) == '*';
    var chkBoxFieldName = (grid.ColKey(newCol)).substr(0,(grid.ColKey(newCol)).length-1);
    var dsoBmk = null;

    if ( (grid.ColDataType(newCol) == 11) && (chkBoxIsReadOnly) )
    {
        if (dso.recordset.BOF || dso.recordset.EOF)
            dsoBmk = null;
        else
            dsoBmk = dso.recordset.Bookmark();
        
        // move o cursor do dso para o primeiro registro
        if (!(dso.recordset.BOF && dso.recordset.EOF))
            dso.recordset.MoveFirst();

        // filtra o dso
        dso.recordset.setFilter(sRegIDFldName  + '=' + nRegID);
        
        if (!(dso.recordset.BOF || dso.recordset.EOF))
        {
			// Se nao for campo lookup
			if (chkBoxFieldName.substr(0,1) != '^')
				fg.TextMatrix(newRow, newCol) = (dso.recordset[chkBoxFieldName].value == null ? 0 : dso.recordset[chkBoxFieldName].value);
			else
			{
				var aLookUp = chkBoxFieldName.split('^');
				var sFldKeySourceName = aLookUp[1];
				var sFldKeyDestName = aLookUp[3];
				var sFldName = aLookUp[4];
				var dsoLookUp = eval(aLookUp[2]);

				// filtra o dso de lookup
				if (dso.recordset[sFldKeySourceName].value != null)
				{
					dsoLookUp.recordset.setFilter(sFldKeyDestName + ' = ' + '\'' + dso.recordset[sFldKeySourceName].value + '\'');
				
					if (!(dsoLookUp.recordset.BOF || dsoLookUp.recordset.EOF))
						fg.TextMatrix(newRow, newCol) = dsoLookUp.recordset[sFldName].value;
				}
				
				dsoLookUp.recordset.setFilter('');
			}	
        }    

        // desfiltra o dso
        dso.recordset.setFilter('');

        if ( dsoBmk != null )
            dso.recordset.gotoBookmark(dsoBmk);

        retVal = true;
    }

    return retVal;
}

/********************************************************************
Impede usuario alterar celula checkbox readonly.
Usada pela funcao afterRowColChange2.

Parametros:
dso                 - referencia ao dso que alimenta o grid
grid                - referencia ao grid
oldRow              - linha atual
oldCol              - coluna atual
newRow              - linha para onde pretende ir
newCol              - coluna para onde pretende ir

Retorno:
true, nao prossegue o afterRowColChange2
false, prossegue
********************************************************************/
function treatCheckBoxReadOnly2(dso, grid, oldRow, oldCol, newRow, newCol)
{
	if ( grid.Editable == false )
		return true;

    if ( (newRow < 0) || (newCol < 0) )
        return false;
    
    if ( __glb_aLinesState == null )
		return false;
        
    var retVal = false;
    var sRegIDFldName = grid.ColKey(grid.Cols - 1);
    var nRegID = grid.ValueMatrix(newRow, grid.Cols - 1);
    var chkBoxIsReadOnly = false;
    var chkBoxFieldName = '';

	grid.Editable = false;
    
    if ( grid.ColKey(newCol).substr(grid.ColKey(newCol).length-1,1) == '*' )
		chkBoxFieldName = (grid.ColKey(newCol)).substr(0,(grid.ColKey(newCol)).length-1);
	else	
		chkBoxFieldName = (grid.ColKey(newCol)).substr(0,(grid.ColKey(newCol)).length);
    
    var dsoBmk = null;
    var i = 0;
    
    for (i=0; i<__glb_aLinesState.length; i++)
    {
		if ((i==newRow) && (__glb_aLinesState[i] == true))
		{
			chkBoxIsReadOnly = true;
			break;
		}	
    }

    if ( (grid.ColDataType(newCol) == 11) && (chkBoxIsReadOnly) )
    {
        if (dso.recordset.BOF || dso.recordset.EOF)
            dsoBmk = null;
        else
            dsoBmk = dso.recordset.Bookmark();
        
        // move o cursor do dso para o primeiro registro
        dso.recordset.MoveFirst();
                        
        // filtra o dso
        dso.recordset.setFilter(sRegIDFldName  + '=' + '\'' + nRegID + '\'');
        
        if (!(dso.recordset.BOF || dso.recordset.EOF))
        {
			// Se nao for campo lookup
			if (chkBoxFieldName.substr(0,1) != '^')
				fg.TextMatrix(newRow, newCol) = (dso.recordset[chkBoxFieldName].value == null ? 0 : dso.recordset[chkBoxFieldName].value);
			else
			{
				var aLookUp = chkBoxFieldName.split('^');
				var sFldKeySourceName = aLookUp[1];
				var sFldKeyDestName = aLookUp[3];
				var sFldName = aLookUp[4];
				var dsoLookUp = eval(aLookUp[2]);

				// filtra o dso de lookup
				dsoLookUp.recordset.setFilter(sFldKeyDestName  + '=' + '\'' + dso.recordset[sFldKeySourceName].value + '\'');
				
				if (!(dsoLookUp.recordset.BOF || dsoLookUp.recordset.EOF))
					fg.TextMatrix(newRow, newCol) = dsoLookUp.recordset[sFldName].value;

				dsoLookUp.recordset.setFilter('');
			}	
        }    

        // desfiltra o dso
        dso.recordset.setFilter('');

        if ( dsoBmk != null )
            dso.recordset.gotoBookmark(dsoBmk);

        retVal = true;
    }
    
	grid.Editable = true;
	
    return retVal;
}

/********************************************************************
Impede usuario alterar celula checkbox readonly.
Usada pela funcao treatCheckBoxReadOnly3.

Parametros:
dso                 - referencia ao dso que alimenta o grid
grid                - referencia ao grid
nRow                - linha que receber� o tratamento

Retorno:
true, nao prossegue o treatCheckBoxReadOnly3
false, prossegue
********************************************************************/
function treatCheckBoxReadOnly3(dso, grid, fldColKey, nRow)
{
	if ( grid.Editable == false )
		return true;

    if (nRow < 0)
        return false;

    var nRegID;
    var chkBoxFieldName;
    var bFirst;
    var strColName = fldColKey;

    for (nRow = 1; nRow < grid.Rows; nRow++)
    {
        nRegID = grid.ValueMatrix(nRow, getColIndexByColKey(grid, strColName));
        chkBoxFieldName = '';
        bFirst = true;
        
        if (fldColKey.substr(fldColKey.length-1,1) == '*' )
            fldColKey = fldColKey.substr(0, fldColKey.length-1);
        else	
            fldColKey = fldColKey.substr(0, fldColKey.length);
        
        
        for (nCol=0; nCol<grid.Cols; nCol++)
        {
            if ((grid.ColDataType(nCol) == 11) &&
                (grid.ColKey(nCol).substr(grid.ColKey(nCol).length-1,1) == '*'))
            {
                if ( grid.ColKey(nCol).substr(grid.ColKey(nCol).length-1,1) == '*' )
		            chkBoxFieldName = (grid.ColKey(nCol)).substr(0,(grid.ColKey(nCol)).length-1);
	            else	
		            chkBoxFieldName = (grid.ColKey(nCol)).substr(0,(grid.ColKey(nCol)).length);
            
                if (bFirst)
                {
                    dso.recordset.MoveFirst();
                    dso.recordset.setFilter(fldColKey  + '=' + nRegID);
                    bFirst = false;

                    if ((dso.recordset.BOF) || (dso.recordset.EOF))
                        break;
                }
                    
	            // Se nao for campo lookup
	            if (chkBoxFieldName.substr(0,1) != '^')
		            fg.TextMatrix(nRow, nCol) = (dso.recordset[chkBoxFieldName].value == null ? 0 : dso.recordset[chkBoxFieldName].value);
	            else
	            {
		            var aLookUp = chkBoxFieldName.split('^');
		            var sFldKeySourceName = aLookUp[1];
		            var sFldKeyDestName = aLookUp[3];
		            var sFldName = aLookUp[4];
		            var dsoLookUp = eval(aLookUp[2]);

		            // filtra o dso de lookup
		            dsoLookUp.recordset.setFilter(sFldKeyDestName  + '=' + '\'' + dso.recordset[sFldKeySourceName].value + '\'');
    				
		            if (!(dsoLookUp.recordset.BOF || dsoLookUp.recordset.EOF))
			            fg.TextMatrix(nRow, nCol) = dsoLookUp.recordset[sFldName].value;

		            dsoLookUp.recordset.setFilter('');
	            }	
            }   
        }
    }
    
    if (!bFirst)
        dso.recordset.setFilter('');
    
}


/********************************************************************
Substitui fg_AfterRowColChange() do detailinf.js para Novas Features.
Tambem pode ser usada em grids de modais.

Parametros:
grid                - referencia ao grid
oldRow              - linha atual
oldCol              - coluna atual
newRow              - linha para onde pretende ir
newCol              - coluna para onde pretende ir

Retorno:
nenhum
********************************************************************/
function js_fg_AfterRowColChange(grid, oldRow, oldCol, newRow, newCol)
{
    if ( glb_FreezeRolColChangeEvents )
        return true;

    if ( glb_GridIsBuilding )
        return true;
    
    var lastBtnCtrlBarInf = null;
    var sCurrDSOID = '';
    
    // grid tem primeira linha de totalizacao
    if ( glb_totalCols__ )
    {
        if ( (!grid.Editable) && (grid.Row == 1) )
        {
            if ( grid.Rows > 2 )
                grid.Row = 2;
        }
    }
    
    try
    {
        sCurrDSOID = glb_currDSO.id;
    }
    catch(e)
    {
        sCurrDSOID = '';
    }    
    
    if ( sCurrDSOID != '' )
    {
        if ( treatCheckBoxReadOnly(glb_currDSO, grid, oldRow, oldCol, newRow, newCol) )
            return true;
    }
    
    try
    {
        lastBtnCtrlBarInf = glb_btnCtlSupInf;
    }
    catch(e)
    {
        lastBtnCtrlBarInf = null;
    }    
    
    // bloqueia mudanca de linha e grids cujas colunas visiveis
    // sao todas read only
    if ( !gridIsEditable(grid)  && (lastBtnCtrlBarInf == 'INFINCL') )
    {
        glb_FreezeRolColChangeEvents = true;
        grid.Row = grid.Rows - 1;
        glb_FreezeRolColChangeEvents = false;
    }
    else if ( !gridIsEditable(grid)  && (lastBtnCtrlBarInf == 'INFALT') )
    {
        glb_FreezeRolColChangeEvents = true;
        grid.Row = oldRow;
        glb_FreezeRolColChangeEvents = false;
    }
    
    // invoca funcao no inf.js do form
    if ( glb_HasGridButIsNotFormPage == null )
    {
        try
		{
		    fg_AfterRowColChange_Prg();
		}
		catch(e)
		{
		    ;
		}
    }
}

/********************************************************************
Processa linha readonly no grid.
Nota, para usar esta funcao, antes e necessario
preencher o array __glb_aLinesState.
Para isto, usar a funcao setLinesState(grid, aLinesState)

Parametros:
	grid
	OldRow
	OldCol
	NewRow
	NewCol
	bMoveCursor	- true, move o cursor para a linha linha nao readonly,
				  respeitando o sentido OldRow -> NewRow
********************************************************************/
function js_fg_AfterRowColChange2 (grid, OldRow, OldCol, NewRow, NewCol, bMoveCursor)
{
	if ( grid.Editable == false )
		return true;
	
	if (__glb_aLinesState == null )
		return true;
		
	if ( NewRow >= __glb_aLinesState.length )	
		return true;
	
	var i;
	var nArrayLen = __glb_aLinesState.length;
	var oldGridState = grid.Editable;
	var lastLineNotReadOnly = null;
	var bNewNewRow = false;
	
	grid.Editable = false;
	
	// Se a nova linha e readonly, nao muda para ela
	// Tenta a nao readonly mais proxima
	if ( bMoveCursor == true )
	{
		if ( __glb_aLinesState[NewRow] == true )
		{
			// grid.Row = OldRow;

			// Tres possibilidades:
			// Na mesma linha (so mudanca de coluna)
			// Se vem de cima para baixo
			// Se vem de baixo para cima
	
			// Na mesma linha (so mudanca de coluna)
			if ( OldRow == NewRow )
			{
				// Nao faz nada (permanece na linha corrente)
				;
			}
			// Se vem de cima para baixo
			else if ( OldRow < NewRow )
			{
				// Tenta achar a linha nao readonly
				// mais proxima da linha readonly
				lastLineNotReadOnly = OldRow;
				
				for ( i=OldRow; i<=NewRow; i++ )
				{
					if ( __glb_aLinesState[i] == false )
						lastLineNotReadOnly = i;
				}
				
				grid.Row = lastLineNotReadOnly;
			}
			// Se vem de baixo para cima
			else if ( OldRow > NewRow )
			{
				// Tenta achar a linha nao readonly
				// mais proxima da linha readonly
				lastLineNotReadOnly = OldRow;
				
				for ( i=OldRow; i>=NewRow; i-- )
				{
					if ( __glb_aLinesState[i] == false )
						lastLineNotReadOnly = i;
				}
				
				grid.Row = lastLineNotReadOnly;
			}
			
		}
	}
	else
	{
		if ( __glb_aLinesState[NewRow] == true )
			grid.Row = OldRow;
	}
	
	glb_validRow    = grid.Row;
    glb_validCol    = grid.Col;
	
	grid.Editable = oldGridState;
}

/********************************************************************
Substitui fg_BeforeRowColChange() do detailinf.js para Novas Features.
Tambem pode ser usada em grids de modais.

Parametros:
grid                - referencia ao grid
oldRow              - linha atual
oldCol              - coluna atual
newRow              - linha para onde pretende ir
newCol              - coluna para onde pretende ir
cancel              - irrelevante para o IE
bForceNewLine       - forca mudanca de linha ao clicar em celula
                      read only. Deve ser passado true pelo
                      programador

Retorno:
true ou false mas nao retorna para o OCX do grid
********************************************************************/
function js_fg_BeforeRowColChange (grid, oldRow, oldCol, newRow, newCol, cancel, bForceNewLine)
{
    if ( glb_FreezeRolColChangeEvents )
        return true;

    var retVal = false;
    var sFldName;
    var lastPos;
    var nWalkDir;
    var i;
          
    if ( glb_GridIsBuilding )
        return retVal;
    
    glb_validRow    = -1;
    glb_validCol    = -1;    
    
    // em edicao se for pesqList, inf ou sup
    // o grid em edicao nao muda de linha
    if ( (grid.Editable == true) && (glb_HasGridButIsNotFormPage == null) )
    {
        if (oldRow != newRow)
        {
            glb_validRow    = oldRow;
            glb_validCol    = oldCol;
                
            glb_FreezeRolColChangeEvents = true;
                
            retVal = false;
            return retVal;
        }
    
        // Primeiro verifica se a nova coluna nao e read only
        sFldName = grid.ColKey(newCol);
        lastPos = sFldName.length-1;
        // se e readonly
        if (sFldName.substr(lastPos,1) == '*')
        {
            glb_validRow    = oldRow;
            glb_validCol    = oldCol;
            
            // Se a coluna e read only tendo a direcao (ir para uma celula
            // a direita ou para uma a esquerda)
            // procura proxima celula nao read only
            nWalkDir = (oldCol < newCol) ? 1 : -1;

            // Controla se todas as colunas do grid sao readonly
            var bColsGridReadOnly = true;
    
            // nWalkDir = 1 -> Nova celula esta a direita da corrente
            if ( nWalkDir == 1 )
            {
                for ( i=newCol; i<grid.Cols; i++)
                {
                    sFldName = grid.ColKey(i);
                    lastPos = sFldName.length-1;
                    // se e readonly
                    if (sFldName.substr(lastPos,1) == '*')
                        continue;
                        
                    // se a coluna esta escondida
                    if ( grid.ColHidden(i) )    
                        continue;
                        
                    glb_validRow    = newRow;
                    glb_validCol    = i;
                    
                    bColsGridReadOnly = false;
                    
                    break;
                }
            }
            // nWalkDir = -1 -> Nova celula esta a esquerda da corrente
            else if ( nWalkDir == -1 )
            {
                for ( i=newCol; i>=0; i--)
                {
                    sFldName = grid.ColKey(i);
                    lastPos = sFldName.length-1;
                    // se e readonly
                    if (sFldName.substr(lastPos,1) == '*')
                        continue;
                        
                    // se a coluna esta escondida
                    if ( grid.ColHidden(i) )    
                        continue;
                        
                    glb_validRow    = newRow;
                    glb_validCol    = i;
                    
                    bColsGridReadOnly = false;
                    
                    break;
                }
            }
                
        }
        // se nao e readonly
        else
        {
            glb_validRow    = newRow;
            glb_validCol    = newCol;
        }
        
        // Se todas as colunas do grid sao readonly neste sentido de andar
        if ( bColsGridReadOnly && (grid.Editable == false) )
        {
            glb_validRow    = newRow;
            glb_validCol    = newCol;
        }
    }
    
    // em edicao se nao for pesqList, inf ou sup (geralmente modal page,
    // mas tambem usado em e-mail)
    // o grid em edicao muda de linha
    if ( (grid.Editable == true) && (glb_HasGridButIsNotFormPage != null) )
    {
        glb_FreezeRolColChangeEvents = true;
        
        // Primeiro verifica se a nova coluna nao e read only
        sFldName = grid.ColKey(newCol);
        lastPos = sFldName.length-1;
        // se e readonly
        if (sFldName.substr(lastPos,1) == '*')
        {
			// Ze , inicio de comentar se der problema
            if ( bForceNewLine )
				glb_validRow    = newRow;
            else
            // Ze , fim de comentar se der problema
				glb_validRow    = oldRow;
            
            glb_validCol    = oldCol;
            
            // Se a coluna e read only tendo a direcao (ir para uma celula
            // a direita ou para uma a esquerda)
            // procura proxima celula nao read only
            nWalkDir = (oldCol < newCol) ? 1 : -1;
    
            // Controla se todas as colunas do grid sao readonly
            var bColsGridReadOnly = true;
    
            // nWalkDir = 1 -> Nova celula esta a direita da corrente
            if ( nWalkDir == 1 )
            {
                for ( i=newCol; i<grid.Cols; i++)
                {
                    sFldName = grid.ColKey(i);
                    lastPos = sFldName.length-1;
                    // se e readonly
                    if (sFldName.substr(lastPos,1) == '*')
                        continue;
                        
                    // se a coluna esta escondida
                    if ( grid.ColHidden(i) )    
                        continue;
                        
                    glb_validRow    = newRow;
                    glb_validCol    = i;
                    
                    bColsGridReadOnly = false;
                    
                    break;
                }
            }
            // nWalkDir = -1 -> Nova celula esta a esquerda da corrente
            else if ( nWalkDir == -1 )
            {
                for ( i=newCol; i>=0; i--)
                {
                    sFldName = grid.ColKey(i);
                    lastPos = sFldName.length-1;
                    // se e readonly
                    if (sFldName.substr(lastPos,1) == '*')
                        continue;
                        
                    // se a coluna esta escondida
                    if ( grid.ColHidden(i) )    
                        continue;
                        
                    glb_validRow    = newRow;
                    glb_validCol    = i;
                    
                    bColsGridReadOnly = false;
                    
                    break;
                }
            }
        }
        // se nao e readonly
        else
        {
            glb_validRow    = newRow;
            glb_validCol    = newCol;
        }
        
        // Se todas as colunas do grid sao readonly neste sentido de andar
        if ( bColsGridReadOnly && (grid.Editable == false) )
        {
            glb_validRow    = newRow;
            glb_validCol    = newCol;
        }
    }
    
    // evento do lado do programador
    if ( glb_HasGridButIsNotFormPage == null )
    {
        try
		{
		    fg_BeforeRowColChange_Prg();
		}
		catch(e)
		{
		    ;
		}    
    }
    
    retVal = true;
    return retVal;
}

/********************************************************************
Substitui fg_EnterCell() do detailinf.js para Novas Features.
Tambem pode ser usada em grids de modais

Parametros:
grid                - referencia ao grid

Retorno:
irrelevante
********************************************************************/
function js_fg_EnterCell(grid)
{
    var theRow, theCol;
    var sCurrDSOID;
    
    if ( !grid.Editable )
    {
        theRow = grid.Row;
        theCol = grid.Col;
        
        if ( theRow < 0 )
            theRow = 0;
        
        if ( theCol < 0 )
            theCol = 0;
        
        grid.ShowCell(theRow, theCol);

        return true;
    }    

    if ((grid.Editable == true) && (grid.Rows > 1))
    {
        glb_FreezeRolColChangeEvents = true;
        
        // Codigo acrescentado em 20/11/2003
        // para resolver checkbox em coluna read only de grid editavel
        try
		{
		    sCurrDSOID = glb_currDSO.id;
		}
		catch(e)
		{
		    sCurrDSOID = '';
		}    
    
		if ( sCurrDSOID != '' )
	        treatCheckBoxReadOnly(glb_currDSO, grid, 0, 0, grid.Row, grid.Col);
        // Final de Codigo acrescentado em 20/11/2003
        
		if ((glb_validRow > 0) && (glb_validCol > 0))
		{
			grid.Row = glb_validRow;
			grid.Col = glb_validCol;
	        
			grid.ShowCell(grid.Row, grid.Col);
        }
        
        if ( grid.Col == 0 )
            grid.LeftCol = 0;
            
        glb_FreezeRolColChangeEvents = false;    
    }

    // invoca funcao no inf.js do form
    if ( glb_HasGridButIsNotFormPage == null )
        fg_EnterCell_Prg();
}

/********************************************************************
Seleciona a primeira celula nao read only. Se a celula apontada nao
for read only seleciona.

Parametros:
grid         - referencia ao grid
row          - linha da celula
col          - coluna da celula
direction    - 1 -> para direita, -1 -> para esquerda
             - 0 -> caminha nos dois sentidos
              
Retorno:
irrelevante

Se nao e possivel selecionar a celula ou a proxima, seleciona 0,0
********************************************************************/
function selectFirstCellNotReadOnly(grid, newRow, newCol, direction)
{
    if ( grid.Rows == 0 )
        return null;
        
    if ( (newRow < 0) || (newCol < 0) )    
        return null;
        
    if ( !((direction == 1) || (direction == -1) || (direction == 0)) )    
        return null;    
    
    var sFldName;
    var lastPos;
    var nWalkDir;
    var bBothDir = false;
    
    if ( direction == 0 )
    {
        direction = 1;
        bBothDir = true;
    }    
    
    glb_validRow    = 0;
    glb_validCol    = 0;
    
    // Primeiro verifica se a nova coluna nao e read only
    sFldName = grid.ColKey(newCol);
    lastPos = sFldName.length-1;
    // se e readonly
    if (sFldName.substr(lastPos,1) == '*')
    {
        glb_validRow    = 0;
        glb_validCol    = 0;
            
        // Se a coluna e read only tendo a direcao (ir para uma celula
        // a direita ou para uma a esquerda)
        // procura proxima celula nao read only
        nWalkDir = direction;
    
        // nWalkDir = 1 -> tenta selecionar para a direita
        if ( nWalkDir == 1 )
        {
            for ( i=newCol; i<grid.Cols; i++)
            {
                sFldName = grid.ColKey(i);
                lastPos = sFldName.length-1;
                // se e readonly
                if (sFldName.substr(lastPos,1) == '*')
                    continue;
                        
                // se a coluna esta escondida
                if ( grid.ColHidden(i) )    
                    continue;
                        
                glb_validRow    = newRow;
                glb_validCol    = i;
                
                // achou celula valida, nao precisa reverter
                bBothDir = false;
                
                break;
            }
        }
        
        // reverte se nao achou celula valida
        if ( bBothDir == true )
            nWalkDir = -1;
                
        // nWalkDir = -1 -> tenta selecionar para a esquerda
        if ( nWalkDir == -1 )
        {
            for ( i=newCol; i>=0; i--)
            {
                sFldName = grid.ColKey(i);
                lastPos = sFldName.length-1;
                // se e readonly
                if (sFldName.substr(lastPos,1) == '*')
                    continue;
                        
                // se a coluna esta escondida
                if ( grid.ColHidden(i) )    
                    continue;
                        
                glb_validRow    = newRow;
                glb_validCol    = i;
                break;
            }
        }
                
    }
    // se nao e readonly
    else
    {
        glb_validRow    = newRow;
        glb_validCol    = newCol;
    }
    
    grid.Row = glb_validRow;
    grid.Col = glb_validCol;
}

/********************************************************************
Funciona apenas se o grid nao esta editable
Teclas: 
ESC         - cancela edicao do gris
+           - insere nova linha no grid
*           - edita linha corrente do grid
-           - deleta linha corrente do grid
1, 2, 3, 4  - chama funcao btnBarClicked do lado do programador

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_KeyPress(KeyAscii)
{
    if ( fg.Editable )
    {
        // Cancela edicao
        if ( KeyAscii == 27 )
        {
            glb_keyBoardKey = 27; 
            glb_gridTimerVar = window.setInterval('btnNotEspecificByKB()', 10, 'JavaScript');
        }    
           
        return true;    
    }

// Comentado ate usuario acostumar com o Overfly
/***********************************************    
    // +
    if ( KeyAscii == 43 )
    {
        // o botao esta visivel mas esta desabilitado    
        if ( !btnIsEnableInCtrlBar('inf', 3) )
        {
            fg.focus();
            return true;
        }
        
        glb_keyBoardKey = '+';
        glb_gridTimerVar = window.setInterval('btnNotEspecificByKB()', 10, 'JavaScript');
        
        return true;
    }
    
    // *
    if ( KeyAscii == 42 )
    {
        // o botao esta visivel mas esta desabilitado    
        if ( !btnIsEnableInCtrlBar('inf', 4) )
        {
            fg.focus();
            return true;
        }
        
        // o grid nao esta em linha de registro
        if ( fg.Row < 1)
        {
            fg.focus();
            return true;
        }
        
        glb_keyBoardKey = '*';
        glb_gridTimerVar = window.setInterval('btnNotEspecificByKB()', 10, 'JavaScript');
        
        return true;
    }
    
    // -
    if ( KeyAscii == 45 )
    {
        // o botao esta visivel mas esta desabilitado    
        if ( !btnIsEnableInCtrlBar('inf', 6) )
        {
            fg.focus();
            return true;
        }
        
        // o grid nao esta em linha de registro
        if ( fg.Row < 1)
        {
            fg.focus();
            return true;
        }
        
        glb_keyBoardKey = '-';
        glb_gridTimerVar = window.setInterval('btnNotEspecificByKB()', 10, 'JavaScript');
        
        return true;
    }
    
    // De 1 a 4    
    if ( (KeyAscii >= 49) && (KeyAscii <= 52) )   
    {
        // o botao nao esta visivel
        if ( !btnEspecificIsVisibleInCtrlBar('inf', KeyAscii - 37) )
        {
            fg.focus();
            return true;
        }    
        
        // o botao esta visivel mas esta desabilitado    
        if ( !btnIsEnableInCtrlBar('inf', KeyAscii - 37) )
        {
            fg.focus();
            return true;
        }    
        
        glb_keyBoardKey = (KeyAscii - 48);
        glb_gridTimerVar = window.setInterval('btnEspecificByKB()', 10, 'JavaScript');    
    } 
***********************************************/
    return true;
}

/********************************************************************
Funciona apenas se o grid esta editable
Teclas: 
Enter       - chama botao gravar da barra de botoes

Parametros:
Row         - linha corrente 
Col         - col coluna corrente
KeyAscii    - tecla pressionada

Retorno:
nenhum
********************************************************************/
function js_fg_KeyPressEdit(Row, Col, KeyAscii)
{
    // Confirma edicao    
     if ( KeyAscii == 13 )
     {
        glb_keyBoardKey = 13;
        glb_gridTimerVar = window.setInterval('btnNotEspecificByKB()', 10, 'JavaScript');      
        return true;
     }    
}

/********************************************************************
Funcao auxiliar para botoes nao especificos
********************************************************************/
function btnNotEspecificByKB()
{
    if ( glb_gridTimerVar != null )
    {
        window.clearInterval(glb_gridTimerVar);
        glb_gridTimerVar = null;
    }
    
    if ( glb_HasGridButIsNotFormPage != null )
        return true;
    
    if ( glb_keyBoardKey == '+' )
        __btn_INCL('inf') ;
    else if ( glb_keyBoardKey == '*' )
        __btn_ALT('inf') ;
    else if ( glb_keyBoardKey == '-' )
        __btn_EXCL('inf') ;        
    else if ( glb_keyBoardKey == 13 )
        __btn_OK('inf') ;                                                
    else if ( glb_keyBoardKey == 27 )
    {
        __btn_CANC('inf') ;
            
        selectFirstCellNotReadOnly(fg, fg.Row, fg.Col, 1);
    }    
}

/********************************************************************
Funcao auxiliar para botoes especificos
********************************************************************/
function btnEspecificByKB()
{
    if ( glb_gridTimerVar != null )
    {
        window.clearInterval(glb_gridTimerVar);
        glb_gridTimerVar = null;
    }

    if ( glb_HasGridButIsNotFormPage != null )
        return true;
    
    if ( glb_keyBoardKey == 1 )
        __btn_UM('inf') ;
    else if ( glb_keyBoardKey == 2 )
        __btn_DOIS('inf') ;
    else if ( glb_keyBoardKey == 3 )
        __btn_TRES('inf') ;
    else if ( glb_keyBoardKey == 4 )
        __btn_QUATRO('inf') ;
}

/********************************************************************
Ajusta largura da coluna se o grid entra em edicao
********************************************************************/
function setColumnWidthToEdition(grid, dsoData)
{
    var i;
    var j;
    var nLength;
    var tblName = '';
    var dso;
    var sFldName;
    var lastPos;
    var aLookUp;
    var lIsCalcField;
    var aCalcFields;
    var aTextsInCombo;
    var sHeader = '';
    
    fg.Redraw = 0;
    
    aHeader = grid.FormatString.split('\t');
	// seta o colkey de cada coluna para o nome de seu respectivo campo
    for ( i = 0; i < grid.Cols ; i++ )
    {
        nLength = 0;
        dso = dsoData;
        sFldName = grid.ColKey(i);
        lastPos = sFldName.length-1;
            
		// retira o asterisco do nome do campo
        if (sFldName.substr(lastPos,1) == '*')
        {
            sFldName = sFldName.substr(0,lastPos);
        }    
            
        // se for campo lookup
		if (sFldName.substr(0,1) == '^')
        {
            aLookUp = sFldName.split('^');
            sFldName = aLookUp[4];
            dso = eval(aLookUp[2]);
        }
            
        lIsCalcField = (sFldName.substr(0,6).toUpperCase() == '_CALC_');
            
        // da parse na string que contem o nome do campo
        // retornando um array de 3 elementos sendo
        // 1-> calc 2->Nome do Campo 3->Tamanho do campo
		if (lIsCalcField)
        {
            aCalcFields = sFldName.split('_');
            nLength = parseInt(aCalcFields[3], 10);
        }
		// se tiver combo nesta coluna
        else if (grid.ColComboList(i) != '')
        {
            aTextsInCombo = (grid.ColComboList(i)).split(';');
            j=0;
            nLength = 0;
            for (j=0;j<aTextsInCombo.length;j++)
            {
                sColSeparator = String.fromCharCode(9);
                if (aTextsInCombo[j].lastIndexOf(sColSeparator) > 0)
                    nLength = Math.max(nLength,aTextsInCombo[j].indexOf(sColSeparator));
                else
                {
                    if (j == (aTextsInCombo.length -1)) // se for ultimo item
                        nLength = Math.max(nLength,aTextsInCombo[j].length);
                    else
                        nLength = Math.max(nLength,aTextsInCombo[j].lastIndexOf('|'));
                }
            }
            nLength = ( nLength <= 2 ? 3 : nLength );
            nLength += 3;
        }
        else
        {
			// se varchar ou char
            if ( (dso.recordset[sFldName].type == 200) || (dso.recordset[sFldName].type == 129) )
                nLength = dso.recordset[sFldName].size;
			// se numerico ou decimal
            else if ((dso.recordset[sFldName].type == 131) ||
                     (dso.recordset[sFldName].type == 14))
                nLength = Math.floor(1.50 * dso.recordset[sFldName].precision);
            // campo no dso e int ou big int
			else if ( (dso.recordset[sFldName].type == 3) ||
					(dso.recordset[sFldName].type == 20) )  
			{
				// Nome da tabela
				tblName = '';
				try
				{
					tblName = dso.recordset[sFldName].table;
					nLength = sendJSMessage('', JS_FIELDINTLEN, tblName, sFldName);
					nLength = Math.floor(1.50 * nLength);
				}
				catch(e)
				{
					nLength = 0;
				}
			}    
			// se data
            else if (dso.recordset[sFldName].type == 135)
                nLength = 10;

            nLength = ( nLength <= 2 ? 3 : nLength );
        }
            
        nLength = Math.min(nLength,50);

        if ((aHeader[i]).length < nLength)
        {
            sHeader += (aHeader[i] + replicate(' ',(nLength - aHeader[i].length) * 1.8));
        }    
        else    
            sHeader += aHeader[i];
                
        if ( i < grid.Cols - 1 )
            sHeader += '\t';
    }
    
    grid.FormatString = sHeader;
    
    fg.Redraw = 2;
    
    // chama funcao para uso do programador
    // uso do try .. catch necessario pois a funcao
    // nao esta definida em todos os arquivos
    try
    {
        afterSetColumnWidthToEdition(keepCurrFolder(), grid, dsoData);
    }
    catch(e)
    {
        ;
    }
}

/********************************************************************
Reseta grid para nada na interface

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/            
function resetGridInterface()
{
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divInf01_02.currentStyle.width, 10) * 18;
}

/********************************************************************
Sorteia grid por colunas

Parametros: 
grid          - referencia ao grid
arrayColsSort - (colNumber_1, order_1) ... (colNumber_n, order_n)
                onde order = 'ASC' or 'DESC'
                
nColUniqueID  - colNumber que a funcao vai usar para achar a linha
				selecionada ap�s o sorteio
hasTotalLine  - true se tiver, false ou null se nao tiver

Retorno:
nenhum

Exemplo:
sortGridByCols(fg, new Array( [3, 'DESC'], [0, 'ASC'] ), 0, false);
********************************************************************/            
function sortGridByCols(grid, arrayColsSort, nColUniqueID, hasTotalLine)
{
	if ( grid == null )
		return null;
		
	if ( arrayColsSort == null )
		return null;
		
	if ( nColUniqueID == null )	
		return null;
	
	if ( hasTotalLine == null )	
		hasTotalLine = false;
		
	if ( grid.Rows == 1 )
		return null;
	
	if ( (hasTotalLine == true) && (grid.Rows == 2) )
		return null;	
	
	var i, n, nOrder;
	var sOrder;
	var nLinesToRespect = 0;
	var lastLineID = 0;
	
	if ( hasTotalLine == true )
		nLinesToRespect++;
		
	lastLineID = nLinesToRespect + 1;	
		
	try
	{
		n = arrayColsSort.length;

		if ( n == 0 )
			return null;
			
        lastLineID = grid.TextMatrix(grid.Row, nColUniqueID);
			
		for ( i = 0; i < n; i++ )
		{
			nOrder = 0;
			sOrder = arrayColsSort[i][1].toUpperCase();
			
			if ( sOrder == 'ASC' )
				nOrder = 1;
			else if ( sOrder == 'DESC' )
				nOrder = 2;
			
			grid.ColSort(arrayColsSort[i][0]) = nOrder;
		}
		
		grid.Select( (grid.FixedRows + nLinesToRespect), grid.FixedCols, grid.Rows - 1, grid.Cols - 1 );		grid.Sort = 10;	
		
		for ( i = nLinesToRespect; i < grid.Rows; i++ )
        {
            if ( grid.TextMatrix(i, 0) == lastLineID )
            {
                grid.TopRow = i;
                grid.Row = i;        
                break;
            }    
        }
	}
	catch (e)
	{
		;
	}
}

/********************************************************************
Trata conteudo de celula ou campo de edicao numerico em grid.
NAO PODE RECEBER VALUEMATRIX COMO PARAMETRO, TEM QUE SER TEXTMATRIX

Parametros: 
vTemp       - o conteudo a tratar

Retorno:
o conteudo tratado
********************************************************************/            
function treatNumericCell(vTemp)
{
    var signPos;
	var vTemp1 = null;
	var chTemp = null;
	var i, k, j;

    vTemp = vTemp.toString();
	vTemp = trimStr(vTemp);

    var thSep = window.top.overflyGen.ThousandSep;
    var decSep = window.top.overflyGen.DecimalSep;

    // Estripa todos os separadores de milhar
    vTemp1 = vTemp;
	vTemp = '';
	for ( i=0; i<vTemp1.length; i++ )
	{
	    if ( vTemp1.charAt(i) != thSep )
	        vTemp += vTemp1.charAt(i);
	}
	
	vTemp1 = '';

	// Troca separador decimal virgula por ponto
	// se o separador decimal em uso no sistema for virgula
	signPos = vTemp.lastIndexOf(',');
	re = /,/g;
	if (signPos != -1)
		vTemp = vTemp.replace(re, '.');

	// 2. Elimina qualquer caracter que nao seja numerico ponto
	// e tambem elimina o ponto se ele e o ultimo caracter	
	vTemp1 = vTemp;
	j = vTemp.length;
	vTemp = '';
	            
	for ( k=0; k<j; k++ )
	{
	    chTemp = vTemp1.charAt(k);
	                
	    if ( k == (j - 1) )
	        if (chTemp == '.')
	            break;
	                
	    if ( (chTemp == '-') ||
	         (chTemp == '.') ||
	         (chTemp == '0') ||
	         (chTemp == '1') ||
	         (chTemp == '2') ||
	         (chTemp == '3') ||
	         (chTemp == '4') ||
	         (chTemp == '5') ||
	         (chTemp == '6') ||
	         (chTemp == '7') ||
	         (chTemp == '8') ||
	         (chTemp == '9') )
	        vTemp += chTemp;
	}
	            
	// Se numero tem sinal, elimina tudo o que esta a esquerda do sinal
	if ( (vTemp.indexOf('-') >= 0) || (vTemp.indexOf('+') >= 0)  )
	{
	    vTemp1 = vTemp;
	    vTemp = '';
	                
	    for ( k=0; k<vTemp1.length; k++ )
	    {
	        if ( vTemp1.charCodeAt(k) == 48 )
	            continue;
	        else
	            break;    
	    }
	    vTemp = vTemp1.substr(k);
	}
	
	// Troca ponto pelo separador decimal em uso no sistema
	vTemp1 = vTemp;
	vTemp = '';
	for ( i=0; i<vTemp1.length; i++ )
	{
	    if ( vTemp1.charAt(i) != '.' )
	        vTemp += vTemp1.charAt(i);
	    else
	        vTemp += decSep;
	}

    return vTemp;
}

/***********************************************************************
Retorna o TextMatrix ou o ValueMatrix de uma celula do grid a partir
do colKey da coluna
Parametros: 
grid       - referencia ao grid
columnName - colKey da coluna
lineNumber - numero da linha a ser consultada

Retorno:
o conteudo da celula, null se a coluna ou a linha nao forem encontradas
***********************************************************************/            
function getCellValueByColKey(grid, columnName, lineNumber)
{
    var i;
    var retVal = null;
    
    for (i=0; i<grid.Cols; i++)
    {
        if (grid.ColKey(i).toUpperCase() == columnName.toUpperCase())
        {
            if (grid.Rows > lineNumber)
            {
				retVal = grid.TextMatrix(lineNumber, i);
                break;
            }
        }
    }

    return retVal;
}

/***********************************************************************
Retorna o indice da coluna.
Parametros: 
grid       - referencia ao grid
columnKey - colKey da coluna

Retorno:
indice da coluna ou -1 se nao encontrada
***********************************************************************/            
function getColIndexByColKey(grid, columnKey)
{
    var i;
    var retVal = -1;
    columnKey = columnKey.replace('*', '');
    
    for (j = 0; j <= 1; j++)
    {
        for (i = 0; i < grid.Cols; i++)
        {
            if (grid.ColKey(i).toUpperCase() == columnKey.toUpperCase())
            {
                retVal = i;
                break;
            }
        }

        if (retVal >= 0)
            break;

        columnKey += '*';
    }

    return retVal;
}

/***********************************************************************
Seta o TextMatrix ou o ValueMatrix de uma celula do grid a partir
do nome da coluna
Parametros: 
grid       - referencia ao grid
columnName - nome da coluna
lineNumber - numero da linha a ser consultada
retValueMatrix - opcional, se true retorna o valueMatrix

Retorno:
true se setou ou false se nao setou
***********************************************************************/            
function setCellValueByColKey(grid, columnName, lineNumber, cellData)
{
    var i;
    var retVal = false;
    
    for (i=0; i<grid.Cols; i++)
    {
        if (grid.ColKey(i).toUpperCase() == columnName.toUpperCase())
        {
            if (grid.Rows > lineNumber)
            {
				grid.TextMatrix(lineNumber, i) = cellData;
				retVal = true;
                break;
            }
        }
    }

    return retVal;
}

function addAndFillineInGrid(grid, lineData, rowPosition, bSelectNewLine)
{
	if ( (rowPosition == -1) )
		return null;
		
	if ( rowPosition > (fg.Rows - 1) )	
	{
		grid.AddItem(lineData);
		rowPosition = grid.Rows - 1;
	}	
	else	
		grid.AddItem(lineData, rowPosition);
		
	paintReadOnlyCols(grid, rowPosition);
	
	if ( bSelectNewLine == true )
		grid.Row = rowPosition;
}

function deleteLineInGrid(grid, nLineToRemove, bSelectPreviousLine)
{
	if ( (nLineToRemove < 1) || (nLineToRemove > grid.Rows - 1) )
		return null;
		
	grid.RemoveItem(nLineToRemove);	
	
	if ( bSelectPreviousLine == true )
	{
		if ( grid.Rows == 1 )
			return null;
		
		var nLineToSelect = 1;	
			
		if ( nLineToRemove == 1 )	
			nLineToSelect = 1;
		else
			nLineToSelect = nLineToRemove - 1;

		grid.Row = nLineToSelect;
	}
}

function setLineAfterDelete(grid, lineToDelete)
{
	// ATENCAO - A VARIAVEL ABAIXO E DA AUTOMACAO
	// NAO MEXER
	if (glb_validRow == (grid.Rows - 1))
		glb_validRow--;

	// Movimenta a linha corrente do grid
	if (grid.Rows >= 2)
	{
		// Deletou a primeira linha
		if ( lineToDelete == 1 )
		{
			grid.Row = 1;
		}
		// Deletou a ultima linha	
		else if ( lineToDelete == grid.Rows )	
		{
			grid.Row = grid.Rows - 1;
		}
		else
		{
			grid.Row = lineToDelete;
		}
	}
}

/***********************************************************************
Guarda estado das linhas (estado pode ser: readonly ou nao)
***********************************************************************/            
function setLinesState(grid, aLinesState)
{
	__glb_aLinesState = aLinesState;
	
	// Linhas read only sao coloridas
	var i;
	
    if ( (aLinesState != null) && (grid.Rows > 1) )
    {
        for (i=1; i<aLinesState.length; i++)
        {
			if ( aLinesState[i] == false )
				continue;
			
            grid.Cell(6, i, 0, i, grid.Cols - 1) = 0XDCDCDC;
        }
    }
}

/***********************************************************************
Guarda estado das linhas (estado pode ser: readonly ou nao)
***********************************************************************/            
function setLinesLikeContinuousForm(grid, zebColor)
{
	var i;
	
    if ( grid.Rows > 1 )
    {
        for (i=1; i<grid.Rows; i++)
        {
			if ( i % 2 != 0 )
				continue;

            grid.Cell(6, i, 0, i, grid.Cols - 1) = zebColor;
        }
    }
}

/***********************************************************************
Guarda estado das linhas (estado pode ser: readonly ou nao)
***********************************************************************/            
function setLinesDoubleLikeContinuousForm(grid, zebColor)
{
	var i;
	var bZeb = false;
	
	var origRow = 0;
	var origCol = 0;
	
    if ( grid.Rows > 1 )
    {
		origRow = grid.Row;
		origCol = grid.Col;
		
        for (i=1; i<grid.Rows; i+=2)
        {
			if ( bZeb == false )
			{
				bZeb = true;
				continue;
			}	
			
            with (grid)
            {
                Cell(6, i, 0, i, grid.Cols - 1) = zebColor;

				if ( (i + 1) < grid.Rows )                
					Cell(6, i + 1, 0, i + 1, grid.Cols - 1) = zebColor;
            }

            bZeb = false;    
        }
        
        grid.Row = origRow;
		grid.Col = origCol;
    }
}

/***********************************************************************
Desbloqueia colunas para edicao no pesqlist
***********************************************************************/            
function unblockColsInPesqList(grid, aCols, aMaskDisplay, aMaskEdition)
{
    var i=0;
    var sFldName = '';
    var lastPos = 0;
    
    asort(aCols, 0);
    
    for (i=0; i<grid.Cols; i++)
    {
        if (grid.ColHidden(i))
            continue;

        sFldName = grid.ColKey(i);
        lastPos = sFldName.length-1;
        
        // se ja eh readonly
        if (sFldName.substr(lastPos,1) == '*')
            continue;

        // Se nao eh uma das colunas editaveis, coloca read-only
        if (ascan(aCols, i, false) == -1)
            grid.ColKey(i) = grid.ColKey(i) + '*';
    }
    
    glb_HasGridButIsNotFormPage = true;
    grid.Editable = true;
    
    if ( grid.Rows >= 2 )
    {
        grid.Row = 1;
        selectFirstCellNotReadOnly(grid, 1, 1, 1);
    }    
    else
    {
        grid.Row = 0;
        selectFirstCellNotReadOnly(grid, 0, 1, 1);
    }
}

/***********************************************************************
Corrige alinhamento de celulas numericas
***********************************************************************/            
function js_PesqList_AfterEdit(dsoGrid, grid, nRow, nCol)
{
    var nType = 0;
	
    if (grid.Editable)
    {
		if (dsoGrid.recordset.Fields.Count > 0)
		{
			nType = dsoGrid.recordset[grid.ColKey(nCol)].type;
			// Se decimal , numerico , int, bigint ou boolean
			if ( (nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20) )
				grid.TextMatrix(nRow, nCol) = treatNumericCell(grid.TextMatrix(nRow, nCol));
		}
    }
}
