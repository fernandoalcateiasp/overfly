/********************************************************************
js_pesqlist.js

Library javascript de funcoes basicas para pesquisa e listagem de forms
********************************************************************/

// CONSTANTES *******************************************************

// Grid do pesqlist esta sendo preenchido
var glb_GridPesqListMounting = false;

// Mantem compatibilidade com algumas bibliotecas
var glb_FORMNAME = window.top.formName;
var glb_USERID = window.top.userID;

// Numero da pagina a apresentar quando em modo listagem
var glb_nPAGENUMBER = 1;

// Ordem e titulos das colunas do grid de pesquisa
var glb_COLPESQORDER;

// Ultima linha selecionada
var glb_LASTLINESELID = '';

// Formatacao das colunas do grid de pesquisa
var glb_aCOLPESQFORMAT = null;

// Variavel de controle, a pagina tem elementos nao padroes
// para glb_pesqListNotPadron = true;
var glb_pesqListNotPadron = null;

// Variavel de controle de timer
glb_pesqListTimer = null;

var glb_bHasTotalLine = false;

// Variavel que indica se grid fr pesqlist tem linha de totais
var glb_bPesqlistAutomaticTotalLines = true;

var glb_modoComboProprietario = true;

// FINAL DE CONSTANTES **********************************************

/********************************************************************

INDICE DAS FUNCOES:

LOOP DE MENSAGENS
EVENTOS DO CONTROL BAR SUPERIOR

FUNCOES GERAIS:
windowOnLoad_1stPart()
windowOnLoad_2ndPart()
createExtraElems()
setupPage()
fg_DblClick()
fg_BeforeSort()
fg_AfterSort()
js_fg_AfterRowColChangePesqList(fg, OldRow, OldCol, NewRow, NewCol)
txtArgumento_onkeydown()
txtFiltro_onkeydown()
includeNewRegister()
showPesqMode()
execPesqShowList(formID)
dsoListData01_DSC()
setButtonsInCtrlBar()
buildGrid()
applyRefresh(formID)
changePageInList(formID, nNumber)
showLastModeAndSetRow(registerID)
supFilterChanged()
btnGetProps_onClick()
btnGetProps_onClick_DSC()
showDetail()
putDateInMMDDYYYY(sDate)	- especifica do pesqlist
numRegsInList(showNumber, rowNumber)
	
getCurrUserID()
getCurrUserEmail()
getCurrPrinterBarCode()
	    
FUNCOES DE DIREITOS:    
getFolderNameByDso(folderID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// LOOP DE MENSAGENS ************************************************

function wndJSProc(idElement, msg, param1, param2) {
    switch (msg) {
        // Msg enviada pela funcao fillCmbsContAndFiltSup_DSC() 
        // informando que combos de contexto e filtros 
        // foram preenchidos 
        case JS_CONTFILTFINISH:
            {
                // se for necessario carregar outros dados
                // tais como combos estaticos, iniciar o carregamento
                // aqui e mover o sendJSMessage abaixo para o final
                // do carregamento
                sendJSMessage(getHtmlId(), JS_PAGELOADED,
                          new Array(window.top.name.toString(),
                          window.top.formName), 0X4);
                return 0;
            }
            break;

        // Mensagens diversas do programador 
        case JS_DATAINFORM:
            {
                // Msg enviada pelo sup01 para mostrar o ultimo modo
                // do pesqlist (pesquisa ou listagem)
                if ((param1 == 'SHOWPESQLISTFROMDET') && (idElement == window.top.sup01ID)) {
                    // param2 == ID do registro a selecionar na lista

                    // trava a interface
                    lockInterface(true);

                    // se a lista nao esta em modo automatico de refresh
                    if (!chkRefrInf.checked) {
                        if ((param2 != null) && (param2 > 0))
                            showLastModeAndSetRow(param2);
                        else
                            showLastModeAndSetRow(null);
                    }
                    else
                        __idOfRegToSelectInGrid = param2;

                    glb_pesqListTimer = window.setInterval('showPesqListFromDet_fn()', 50, 'JavaScript');

                    return 0;
                }
                // Msg enviada pelo sup01 para mostrar o modo de pesquisa
                else if ((param1 == 'SHOWPESQFROMDET') && (idElement == window.top.sup01ID)) {
                    // registra o control bar superior
                    regHtmlInControlBar('sup', window.top.pesqListID);

                    // configura a interface para modo de pesquisa
                    __btn_PESQ('sup');

                    setButtonsInCtrlBar();

                    // dispara evento no pesqlist.js do form
                    if (fg.Rows > 1) {
                        if (fg.FrozenCols == 0)
                            fg.FrozenCols = 1;
                    }
                    //window.top.__glb_ListOrDet = 'PL';
                    pesqlistIsVisibleAndUnlocked();

                    return 0;
                }
                // Msg enviada pela funcao refillCmbsFiltSup_DSC()
                // da biblioteca js_interfaceex.js
                else if ((param1 == 'EXECPESQ') && (idElement == getHtmlId())) {
                    // reinicia a paginacao da pesquisa apos alteracao
                    // do combo de contexto
                    glb_nPAGENUMBER = 1;
                    if (divSup01_02.style.visibility != 'hidden') // se estiver em modo lista
                    {
                        // alteracao em 27/09/2001
                        // o grid nao lista mais automatico
                        //__btn_LIST('sup');
                        // reseta lista de pesquisa
                        fg.ExplorerBar = 0;
                        fg.Rows = 1;

                        lockInterface(false);
                        // Escreve no status bar
                        writeInStatusBar('child', 'cellMode', numRegsInList(null));

                        divSup01_02.style.visibility = 'visible';
                        divSup01_01.style.visibility = 'visible';

                        setButtonsInCtrlBar();

                        // dispara evento no pesqlist.js do form
                        if (fg.Rows > 1) {
                            if (fg.FrozenCols == 0)
                                fg.FrozenCols = 1;
                        }
                        //window.top.__glb_ListOrDet = 'PL';
                        pesqlistIsVisibleAndUnlocked();
                        btnGetProps.disabled = false;
                    }
                    else   // destrava a interface
                    {
                        lockInterface(false);
                        // foca o combo da barra
                        setFocusInCmbOfControlBar('sup', lastCmbSelectInCtrlBar('sup'));
                    }

                    if (chkRefrInf.checked)
                        startTimerRefr();

                    return 0;
                }
                // Preenche o combo de filtros do control bar inferior quando
                // o usuario muda o option do combo1 do control bar inferior
                else if ((idElement == window.top.inf01ID) && (param1 == 'REFILLCMBINF')) {
                    // selecao da pasta no combo de pastas foi alterada
                    fillCmbsFiltInf_DSC(param2);
                    return 0;
                }
                // Mensagens de retorno de dados para o programador
                else if ((idElement == 'PESQLIST_HTML') && (param1 == EXECEVAL)) {
                    return eval(param2);
                }
                // Msg vinda de janela modal que deve ser propagada
                // para o form
                else if (param1.lastIndexOf('_CALLFORM_PL') > 0) {
                    if (param1.indexOf('OK') >= 0)
                        modalInformForm(idElement, 'OK', param2);
                    if (param1.indexOf('CANCEL') >= 0)
                        modalInformForm(idElement, 'CANCEL', param2);

                    return true;
                }
            }
            break;

        // Usuario mudou a empresa no combo de empresas 
        case JS_STBARGEN:
            {
                if (param1.toUpperCase() == 'EMPRESACHANGED') {
                    lockInterface(true);

                    // reseta lista de pesquisa
                    fg.ExplorerBar = 0;
                    fg.Rows = 1;
                    //fillCmbsSupContAndFilt();

                    // reseta o form
                    window.top.startNewFormInBrowser();
                    return true;
                }
            }
            break;

        default:
            return null;
    }
}

// FINAL DE LOOP DE MENSAGENS ***************************************

// EVENTOS DO CONTROL BAR SUPERIOR **********************************
// A ORDEM DOS BOTOES
// 0    - Pesquisar     PESQ
// 1    - Listar        LIST
// 2    - Detalhar      DET
// 3    - Incluir       INCL
// 4    - Alterar       ALT
// 5    - Estado        EST
// 6    - Excluir       EXCL
// 7    - Ok            OK
// 8    - Cancelar      CANC
// 9    - Refresh       REFR
// 10   - Anterior      ANT
// 11   - Seguinte      SEG
// 12   - Um            UM
// 13   - Dois          DOIS
// 14   - Tres          TRES
// 15   - Quatro        QUATRO

function __combo_1(controlBar, optText, optValue, optIndex) {
    // contexto foi alterado pelo usuario

    if (chkRefrInf.checked)
        stopTimerRefr();

    writeInStatusBar('child', 'cellMode', numRegsInList(null), true);

    contextChanged(optValue);
}

function __combo_2(controlBar, optText, optValue, optIndex) {
    // filtro superior foi alterado pelo usuario

    if (chkRefrInf.checked)
        stopTimerRefr();

    writeInStatusBar('child', 'cellMode', numRegsInList(null), true);

    supFilterChanged();

    if (chkRefrInf.checked)
        startTimerRefr();
}

function __btn_PESQ(controlBar) {
    try {
        // Acao do usuario, qualquer retorno a automacao nao prossegue
        if (btnBarNotEspecClicked(controlBar, 'PLPESQ') != null)
            return true;
    }
    catch (e) {
        ;
    }

    showPesqMode();
}

function __btn_LIST(controlBar) {
    try {
        // Acao do usuario, qualquer retorno a automacao nao prossegue
        if (btnBarNotEspecClicked(controlBar, 'PLLIST') != null)
            return true;
    }
    catch (e) {
        ;
    }

    stopTimerRefr();
    glb_nPAGENUMBER = 1;
    execPesqShowList(window.top.formID);
}

function __btn_DET(controlBar) {
    try {
        // Acao do usuario, qualquer retorno a automacao nao prossegue
        if (btnBarNotEspecClicked(controlBar, 'PLDET') != null)
            return true;
    }
    catch (e) {
        ;
    }

    stopTimerRefr();
    showDetail();
}

function __btn_INCL(controlBar) {
    try {
        // Acao do usuario, qualquer retorno a automacao nao prossegue
        if (btnBarNotEspecClicked(controlBar, 'PLINCL') != null)
            return true;
    }
    catch (e) {
        ;
    }

    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    stopTimerRefr();

    sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_LastActionInForm__ = 10');

    includeNewRegister();
}

function __btn_REFR(controlBar) {
    try {
        // Acao do usuario, qualquer retorno a automacao nao prossegue
        if (btnBarNotEspecClicked(controlBar, 'PLREFR') != null)
            return true;
    }
    catch (e) {
        ;
    }

    stopTimerRefr();
    applyRefresh(window.top.formID);
}

function __btn_ANT(controlBar) {
    try {
        // Acao do usuario, qualquer retorno a automacao nao prossegue
        if (btnBarNotEspecClicked(controlBar, 'PLANT') != null)
            return true;
    }
    catch (e) {
        ;
    }

    stopTimerRefr();
    changePageInList(window.top.formID, -1);
}

function __btn_SEG(controlBar) {
    try {
        // Acao do usuario, qualquer retorno a automacao nao prossegue
        if (btnBarNotEspecClicked(controlBar, 'PLSEG') != null)
            return true;
    }
    catch (e) {
        ;
    }

    stopTimerRefr();
    changePageInList(window.top.formID, 1);
}

function __btn_UM(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 1);
}

function __btn_DOIS(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 2);
}

function __btn_TRES(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 3);
}

function __btn_QUATRO(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 4);
}

function __btn_CINCO(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 5);
}

function __btn_SEIS(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 6);
}

function __btn_SETE(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 7);
}

function __btn_OITO(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 8);
}

function __btn_NOVE(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 9);
}

function __btn_DEZ(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 10);
}

function __btn_ONZE(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 11);
}

function __btn_DOZE(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 12);
}

function __btn_TREZE(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 13);
}

function __btn_QUATORZE(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 14);
}

function __btn_QUINZE(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 15);
}

function __btn_DEZESSEIS(controlBar) {
    // Funcao de uso do programador
    btnBarClicked(controlBar, 16);
}

// FINAL DE EVENTOS DO CONTROL BAR SUPERIOR *************************

// FUNCOES GERAIS ***************************************************

/*
Ze em 17/03/08
*/
function window_OnUnload() {
    dealWithObjects_Unload();
}

/********************************************************************
Executa a primeira parte do window_onload

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function windowOnLoad_1stPart() {
    // Ze em 17/03/08
    document.body.onunload = window_OnUnload;
    dealWithObjects_Load();
    dealWithGrid_Load();
    // Fim de Ze em 17/03/08

    sendJSMessage('AnyHtml', JS_MSGRESERVED, 'SETCMBFORMS', null);

    var elem;

    // Cores padroes
    // Lavender
    //glb_bLinesLikeContinuousForm = 0XFAE6E6;
    // Aliceblue
    //glb_bLinesLikeContinuousForm = 0XFFF8F0;

    // Salva o id deste html (a mesma operacao e feita no pesqlist, no sup01
    // e no inf01)
    window.top.pesqListID = getHtmlId();

    // Seta o InternetTimeOut dos componentes RDS
    dsoFixedParams();

    // Cria elementos nao definidos no projeto original
    createExtraElems();

    // chamada movida para este ponto em 18/12/2001     
    // Preenche combos de contexto e filtros
    fillCmbsSupContAndFilt();

    // FG - grid
    // coloca classe no grid
    elem = window.document.getElementById('fg');

    if (elem != null)
        elem.className = 'fldGeneral';

    // Configura o body do html
    with (getBodyRef()) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // Configura elementos do arquivo
    setupPage();

    // Garante montagem do array de controles para travamento de interface
    lockControls(true, true);
    lockControls(false, true);
}


/********************************************************************
Executa a segunda parte do window_onload

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function windowOnLoad_2ndPart() {
    var i;
    var coll;

    // Preenche combos de contexto e filtros
    // fillCmbsSupContAndFilt();

    // Garante selecao de texto nos campos input text
    setupEventOnFocusInTextFields();

    // Garante mudanca de checkboxes ao clicar o label correspondente
    setupEventInvertChkBox();

    // Evento do clique do chkRefrInf e seu label
    chkRefrInf.onclick = chkRefrInf_onclick;
    lblRefrInf.onclick = lblRefrInf_onclick;

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divSup01_02.style.width, 10) * 18;

    // Previne backspace em selects para impedir retorno
    // a pagina anteriormente carregada
    preventBkspcInSel(window);

    // Garante pesquisa no campo de filtro
    txtFiltro.onkeydown = txtFiltro_onkeydown;

    // Evento onchange em combos
    coll = window.document.getElementsByTagName('SELECT');

    for (i = 0; i < coll.length; i++) {
        coll.item(i).onchange = select_onchange;
    }

    // Evento onclick em checkbox != (chkRefrInf || chkOrdem)
    coll = window.document.getElementsByTagName('INPUT');

    for (i = 0; i < coll.length; i++) {
        if ((coll.item(i).type).toUpperCase() == 'CHECKBOX') {
            if (!((coll.item(i) == chkRefrInf) || (coll.item(i) == chkOrdem)))
                coll.item(i).onclick = chk_onclick;
        }
    }

    // Translada interface pelo dicionario do sistema
    translateInterface(window, null);
}

/********************************************************************
Cria elementos nao definidos no projeto original

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function createExtraElems() {
    // Define classe para grid
    var elem = window.document.getElementById('fg');
    if (elem != null)
        elem.className = 'fdlGeneral';

    // Adiciona option para 50 linhas ao combo selRegistros
    var oOptSelReg = document.createElement("OPTION");
    oOptSelReg.innerText = '50';
    oOptSelReg.value = 50;
    selRegistros.insertBefore(oOptSelReg, selRegistros.options.item(0));

    // Adiciona option para 20 linhas ao combo selRegistros
    var oOptSelReg1 = document.createElement("OPTION");
    oOptSelReg1.innerText = '25';
    oOptSelReg1.value = 25;
    selRegistros.insertBefore(oOptSelReg1, selRegistros.options.item(0));
    selRegistros.selectedIndex = 1;

    // Adiciona label, combo e opcoes para metodo de pesquisa
    // antes do lblArgumento, do campo txtArgumento
    // O combo
    var oSel = document.createElement("SELECT");
    oSel.id = 'selMetodoPesq';
    oSel.name = 'selMetodoPesq';
    oSel.className = 'fldGeneral';
    window.document.appendChild(oSel);
    divSup01_01.insertBefore(oSel, lblArgumento);

    // Os options do combo
    // Option Default
    var oOptSelMetPesq1 = document.createElement("OPTION");
    oOptSelMetPesq1.innerText = 'Default';
    oOptSelMetPesq1.value = 'DEF';
    oSel.appendChild(oOptSelMetPesq1);

    // Option ComecaCom
    var oOptSelMetPesq2 = document.createElement("OPTION");
    oOptSelMetPesq2.innerText = 'Come�a';
    oOptSelMetPesq2.value = 'COM';
    oSel.appendChild(oOptSelMetPesq2);

    // Option TerminaCom
    var oOptSelMetPesq3 = document.createElement("OPTION");
    oOptSelMetPesq3.innerText = 'Termina';
    oOptSelMetPesq3.value = 'TERM';
    oSel.appendChild(oOptSelMetPesq3);

    // Option Contem
    var oOptSelMetPesq4 = document.createElement("OPTION");
    oOptSelMetPesq4.innerText = 'Cont�m';
    oOptSelMetPesq4.value = 'CONT';
    oSel.appendChild(oOptSelMetPesq4);

    oSel.selectedIndex = 0;

    // O label do combo
    var oP = document.createElement("P");
    oP.innerText = 'M�todo';
    oP.id = 'lblMetodoPesq';
    oP.name = 'lblMetodoPesq';
    oP.className = 'lblGeneral';
    window.document.appendChild(oP);
    divSup01_01.insertBefore(oP, oSel);

    // Adiciona botao para preencher combo de proprietarios
    // antes do combo de Registros
    // O combo
    var oBtn = document.createElement("INPUT");
    oBtn.type = 'button';
    oBtn.id = 'btnGetProps';
    oBtn.name = 'btnGetProps';
    oBtn.className = 'btns';
    oBtn.value = 'P';
    oBtn.title = 'Preenche combo de propriet�rios';
    oBtn.onclick = btnGetProps_onClick;
    window.document.appendChild(oBtn);
    divSup01_01.insertBefore(oBtn, selProprietariosPL);
}

/********************************************************************
Configura interface do peslist

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function setupPage() {
    // Ajusta os divs
    adjustDivs('pesqlist', [[1, 'divSup01_01'],
                      [2, 'divSup01_02']]);

    /*********** DIV PESQUISA ******************************************/

    if (glb_pesqListNotPadron != null)
        readjustInterface();
    else {
        adjustElementsInForm([['lblRefrInf', 'chkRefrInf', 3, 1],
                              ['lblOrdem', 'chkOrdem', 3, 1, -9],
                              ['lblRegistrosVencidos', 'chkRegistrosVencidos', 3, 1, -9],
                              ['lblProprietariosPL', 'selProprietariosPL', 15, 1, -5],
                              ['btnGetProps', 'btn', 20, 1],
                              ['lblRegistros', 'selRegistros', 6, 1, -3],
                              ['lblMetodoPesq', 'selMetodoPesq', 9, 1, -9],
                              ['lblPesquisa', 'selPesquisa', 14, 1, -4],
                              ['lblArgumento', 'txtArgumento', 18, 1, -4],
                              ['lblFiltro', 'txtFiltro', 44, 1, -4]]);

        // Adicionado campo chkRegistroVencidos, solicitado por Eduardo Rodrigues - VRM 21/10/2015
        // Alteracao da resolucao de 800x600 para 1024x768 - Marco Fortunato - 14/07/2010
        //['lblArgumento','txtArgumento',12,1, -4],
        //['lblFiltro','txtFiltro',28,1, -4]]);

        with (lblRefrInf.style) {
            backgroundColor = 'transparent';
            width = (lblRefrInf.innerText).length * FONT_WIDTH - ELEM_GAP + 2;
        }

        with (lblOrdem.style) {
            backgroundColor = 'transparent';
            width = (lblOrdem.innerText).length * FONT_WIDTH - ELEM_GAP + 6;
        }
    }

    // Reajusta o divSup01_02
    with (divSup01_02.style) {
        left = ELEM_GAP;
        top = parseInt(divSup01_01.style.top, 10) +
                    parseInt(divSup01_01.style.height, 10) + ELEM_GAP;
        height = parseInt(divSup01_02.currentStyle.height, 10) - parseInt(top, 10) + ELEM_GAP;
    }

    // Ajusta o grid do divSup01_02
    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divSup01_02.currentStyle.width, 10);
        height = parseInt(divSup01_02.currentStyle.height, 10);
    }
}

/********************************************************************
Retorna a string Listagem ou Listagem-xx regs

Parametros:
showNumber      - se not null, acrescenta o numero de registros
rowNumber       - se not null, acrescenta a linha corrente

Retorno:
a string

********************************************************************/
function numRegsInList(showNumber, rowNumber) {
    var rowsQty = fg.Rows - 1;
    var currRow = fg.Row;

    if (showNumber != null)
        return 'Listagem';

    if (fg.Rows <= 1)
        return 'Listagem';

    if (glb_totalCols__) {
        rowsQty--;
        currRow--;
    }

    if (rowsQty < 0)
        rowsQty = 0;

    if (currRow < 0)
        currRow = 0;

    if (rowNumber != null)
        return 'Listagem:' + ' ' + (currRow).toString() + '/' + (rowsQty).toString();
    else
        return 'Listagem:' + ' ' + (rowsQty).toString();

}

/********************************************************************
Torna a selecionar conteudo do campo argumento quando em foco
********************************************************************/
function __focusSelect() {
    if (glb_pesqListTimer != null) {
        window.clearInterval(glb_pesqListTimer);
        glb_pesqListTimer = null;
    }

    txtArgumento.onfocus = selFieldContent;
}

/********************************************************************
Usuario mudou item selecionado em um combo
********************************************************************/
function select_onchange() {
    if (chkRefrInf.checked)
        startTimerRefr();

    try {
        setLabelOfControlEx(lblProprietariosPL, selProprietariosPL);
    }
    catch (e) {
        ;
    }
}

/********************************************************************
Usuario clicou um checkbox nao padrao da interface
********************************************************************/
function chk_onclick() {
    if (chkRefrInf.checked)
        startTimerRefr();
}

/********************************************************************
Usuario clicou o label do check box de refresh no inferior
********************************************************************/
function lblRefrInf_onclick() {
    var ctl = this.nextSibling;

    if (ctl.disabled == true)
        return;

    ctl.checked = !ctl.checked;

    chkRefrInf_onclick();
}

/********************************************************************
Usuario alterou o check box de refresh no inferior
********************************************************************/
function chkRefrInf_onclick() {
    // A variavel abaixo esta no inf
    // Qualquer valor da variavel abaixo nao monta o inf quando:
    // 1. Detalha do pesqList
    // 2. Refresca no sup
    // 3. Pagina no sup
    // 4. Cancela inclusao ou edicao no sup 
    // var glb_bInfNoData = true;

    if (chkRefrInf.checked) {
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bInfNoData = null');
        startTimerRefr();
    }
    else {
        stopTimerRefr();
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bInfNoData = true');
    }
}

/********************************************************************
Foca campo argumento, limpa conteudo e coloca caracter digitado em
outro campo.
********************************************************************/
function initNewArgument(initialChar) {
    txtArgumento.onfocus = null;
    txtArgumento.focus();
    txtArgumento.value = '';
    txtArgumento.value = initialChar;
    glb_pesqListTimer = window.setInterval('__focusSelect()', 10, 'JavaScript');
}

/********************************************************************
Abre detalhe quando o usuario pressiona Enter na lista,
insere registro novo quando o usuario pressiona + na lista
da foco no campo argumento se for digitado caracter alfa numerico

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_KeyPress(KeyAscii) {
    if (fg.Editable)
        return true;

    // Enter
    if ((KeyAscii == 13) && (fg.Row > 0)) {
        stopTimerRefr();
        fg_DblClick();
    }
    // +
    else if (KeyAscii == 43) {
        includeNewRegister();
    }
    // Caracter Alfa numerico
    else if ((KeyAscii >= 21) && (KeyAscii <= 146)) {
        initNewArgument(String.fromCharCode(KeyAscii));
    }
}

/********************************************************************
Retorno do detalhe para o pesqlist. Esta funcao evita manchada de tela
causada pelo scroll bar do grid do pesq list

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function showPesqListFromDet_fn() {
    if (glb_pesqListTimer != null) {
        window.clearInterval(glb_pesqListTimer);
        glb_pesqListTimer = null;
    }

    // Mostra o pesqlist
    var frameID = getFrameIdByHtmlId(window.top.pesqListID);
    if (frameID) {
        showFrameInHtmlTop(frameID, true);
        // registra o control bar superior
        regHtmlInControlBar('sup', window.top.pesqListID);
    }

    // destrava a interface
    lockInterface(false);

    // Coloca foco no campo argumento se modo pesq ativo
    if ((divSup01_01.currentStyle.visibility == 'visible') &&
			(txtArgumento.disabled == false))
        txtArgumento.focus();

    setButtonsInCtrlBar();

    // dispara evento no pesqlist.js do form
    if (fg.Rows > 1) {
        if (fg.FrozenCols == 0)
            fg.FrozenCols = 1;
    }

    // Alterado pela questao do Ulisses em 02/09/04
    try {
        adjustControlBarSupByRights(1, 1);
    }
    catch (e) {
        ;
    }
    // Fim de Alterado pela questao do Ulisses em 02/09/04
    //window.top.__glb_ListOrDet = 'PL';
    pesqlistIsVisibleAndUnlocked();

    // Escreve no status bar
    if (fg.Row > 0)
        writeInStatusBar('child', 'cellMode', numRegsInList(null, true));

    startTimerRefr(100);
}

/********************************************************************
Abre detalhe quando o usuario da um duplo clique na lista

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_DblClick() {
    // grid tem primeira linha de totalizacao
    if (glb_totalCols__) {
        if ((!fg.Editable) && (fg.Row == 1)) {
            if (fg.Rows > 2) {
                if (fg.Row == 2)
                    return true;
            }
        }
    }

    // Entra em modo detalhe, se o botao detalhe esta habilitado
    // no control bar superior
    if (!btnIsEnableInCtrlBar('sup', 2))
        return true;

    stopTimerRefr();

    __btn_DET('sup');
}

/********************************************************************
Grid vai mudar o sort order

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_BeforeSort() {
    if (chkRefrInf.checked && (glb_GridPesqListMounting == false)) {
        startTimerRefr();
    }

    if (fg.Row > 0)
        glb_LASTLINESELID = fg.TextMatrix(fg.Row, 0);
    else
        glb_LASTLINESELID = '';
}

/********************************************************************
Grid mudou o sort order

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_AfterSort() {
    var i;

    if ((glb_LASTLINESELID != '') && (fg.Rows > 1)) {
        for (i = 1; i < fg.Rows; i++) {
            if (fg.TextMatrix(i, 0) == glb_LASTLINESELID) {
                fg.TopRow = i;
                fg.Row = i;
                break;
            }
        }
    }

    if (fg.Row > 0)
        writeInStatusBar('child', 'cellMode', numRegsInList(null, true));
}

/********************************************************************
Usuario mudou a linha do grid

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function js_fg_AfterRowColChangePesqList(grid, OldRow, OldCol, NewRow, NewCol) {
    if (chkRefrInf.checked && (glb_GridPesqListMounting == false) && NewRow > 0) {
        startTimerRefr();
        __idOfRegToSelectInGrid = grid.ValueMatrix(NewRow, 0);
    }

    if (fg.Row > 0)
        writeInStatusBar('child', 'cellMode', numRegsInList(null, true));

    // grid tem primeira linha de totalizacao
    if (glb_totalCols__) {
        if ((!fg.Editable) && (fg.Row == 1)) {
            if (fg.Rows > 2)
                fg.Row = 2;
        }
    }
}

/********************************************************************
Executa pesquisa se o usuario da enter no campo de argumentos

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function txtArgumento_onkeydown() {
    if (event.keyCode == 13) {
        __btn_LIST('sup');
    }
    else {
        if (chkRefrInf.checked)
            startTimerRefr();
    }
}

/********************************************************************
Executa pesquisa se o usuario da enter no campo de filtros

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function txtFiltro_onkeydown() {
    if (event.keyCode == 13) {
        __btn_LIST('sup');
    }
    else {
        if (chkRefrInf.checked)
            startTimerRefr();
    }
}

/********************************************************************
Incluir um novo registro a partir do pesqlist.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function includeNewRegister() {
    // Entra em detalhe inclusao, se o botao inclusao esta habilitado
    // no control bar superior
    if (!btnIsEnableInCtrlBar('sup', 3))
        return true;

    adjustSupInfControlsBar('DISALL');

    // Escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Inclus�o', true);

    // Mensagem capturada no sup para entrar em modo detalhe inclusao
    // salva param2 no sup se veio da pesquisa ou da lista, para retornar
    if (divSup01_01.style.visibility == 'visible')    // se em modo pesquisa
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'INSERTNEWREG', 'FROMPESQ');
    else    // se em modo lista
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'INSERTNEWREG', 'FROMLIST');
}

/********************************************************************
Mostra a interface do pesqlist vindo do detalhe, por um clique no
botao de pesquisa

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function showPesqMode() {
    try {
        setLabelOfControlEx(lblProprietariosPL, selProprietariosPL);
    }
    catch (e) {
        ;
    }

    divSup01_01.style.visibility = 'visible';
    divSup01_02.style.visibility = 'visible';

    lockInterface(false);

    adjustSupInfControlsBar('PESQMODE');

    // mostra o xxx_pesqlist.asp
    frameID = getFrameIdByHtmlId(window.top.pesqListID);
    if (frameID) {
        showFrameInHtmlTop(frameID, true);
        // registra o control bar superior
        regHtmlInControlBar('sup', window.top.pesqListID);
    }

    txtArgumento.focus();

    // Escreve no status bar
    writeInStatusBar('child', 'cellMode', numRegsInList(null));
}

/********************************************************************
Chama o arquivo pesqlistsvr.asp que retorna um xml com os
dados para preenchimento da listagem da pesquisa

Parametros:
nenhum

Retorno:
o xml com os dados ou um registro de mensagem de erro
********************************************************************/
function execPesqShowList(formID) {
    if (lastBtnUsedInCtrlBar('sup') == 'SEG')
    // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Seguinte', true);
    else if (lastBtnUsedInCtrlBar('sup') == 'ANT')
    // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Anterior', true);
    else if (lastBtnUsedInCtrlBar('sup') == 'LIST')
    // Escreve no status bar
        writeInStatusBar('child', 'cellMode', numRegsInList(false), true);
    else if (lastBtnUsedInCtrlBar('sup') == 'REFR')
    // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Refresh', true);
    else
    // Escreve no status bar
        writeInStatusBar('child', 'cellMode', numRegsInList(null), true);

    glb_pesqListTimer = window.setInterval('goExecPesqShowList()', 10, 'Javascript');
}

/********************************************************************
Chama o arquivo pesqlistsvr.asp que retorna um xml com os
dados para preenchimento da listagem da pesquisa

Parametros:
nenhum

Retorno:
o xml com os dados ou um registro de mensagem de erro
********************************************************************/
function goExecPesqShowList(formID) {
    var nUserID = getCurrUserID();
    var nSubFormID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'window.top.subFormID');

    if (glb_pesqListTimer != null) {
        window.clearInterval(glb_pesqListTimer);
        glb_pesqListTimer = null;
    }

    if (false) {
        if (document.getElementById('selRegistros').value > 50) {
            if ((nUserID != 1004) && (nUserID != 1171) && (nUserID != 1121) && (nUserID != 1165)) {
                if (window.top.overflyGen.Alert('Utilizar 50 Registros por P�gina.') == 0)
                    return null;

                lockInterface(false);
                return null;
            }
        }
    }

    if (!formID)
        formID = window.top.formID;

    var sType = document.getElementById('selPesquisa').options[document.getElementById('selPesquisa').selectedIndex].id;
    sType = sType.substr(0, 1);

    // trima o conteudo do campo txtArgumento se o stype
    // nao for varchar
    if (!(sType.toUpperCase() == 'V')) {
        document.getElementById('txtArgumento').value =
            trimStr(document.getElementById('txtArgumento').value);
    }

    // parametros necessarios ao arquivo de pesquisa do servidor
    var sString = document.getElementById('txtArgumento').value;

    var nFiltroContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nFiltroID = getCmbCurrDataInControlBar('sup', 2);
    var empresaID = getCurrEmpresaData();

    if ((document.getElementById('txtArgumento').value != '') &&
        (!window.top.chkTypeData(sType, sString))) {
        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', numRegsInList(null));

        if (window.top.overflyGen.Alert('O campo Argumento cont�m tipo de dado incompativel.') == 0) {
            startTimerRefr();
            return null;
        }
        document.getElementById('txtArgumento').focus();
        __btn_PESQ('sup');
        return null;
    }

    adjustSupInfControlsBar('PESQDIS');

    // Move foco por questoes esteticas apenas
    // se nao em refresh automatico
    if (fg.Enabled) {
        if (!chkRefrInf.checked) {
            window.focus();
            fg.focus();
        }
    }

    lockInterface(true);

    var strPas = new String();

    strPas = '?nFormID=' + encodeURIComponent(formID);
    strPas += '&nSubFormID=' + encodeURIComponent(nSubFormID);  
    strPas += '&sFldKey=' + encodeURIComponent(document.getElementById('selPesquisa').value);

    if (document.getElementById('chkOrdem').checked)
        strPas += '&nOrder=DESC';
    else
        strPas += '&nOrder=ASC';

    if (document.getElementById('chkRegistrosVencidos').checked)
        strPas += '&bRegistrosVencidos=true';
    else 
        strPas += '&bRegistrosVencidos=false';

    // Chama funcao do lado cliente pedindo clausula nao padrao de pesquisa
    var sSpcClause = specialClauseOfResearch();

    if (sSpcClause != null)
        strPas += '&sSpcClause=' + encodeURIComponent(sSpcClause);

    try {
        var sSpcClause_Ex = specialClauseOfResearchEx();

        if (sSpcClause_Ex != null)
            strPas += sSpcClause_Ex;
    }
    catch (e) {
        ;
    }

    strPas += '&sMetPesq=' + encodeURIComponent(selMetodoPesq.value);

    var argToPass = document.getElementById('txtArgumento').value;

    // O campo argumento esta vazio
    if (document.getElementById('txtArgumento').value == '')
        strPas += '&nArgumentoIsEmpty=' + encodeURIComponent(0);
    else
        strPas += '&nArgumentoIsEmpty=' + encodeURIComponent(1);

    // Coloca data no sistema mm/dd/yyyy se for o caso
    argToPass = putDateInMMDDYYYY(document.getElementById('txtArgumento').value);

    // Ajuste temportario para resolver pesquisa da Allplus. BJBN 05/06/2017
    argToPass = argToPass.replace('&', encodeURIComponent('&'));

    // Ajuste temportario para resolver pesquisa do Lista de Pre�o (Cotador). MSO 03/10/2018
    argToPass = argToPass.replace('+', encodeURIComponent('+'));

    if (argToPass == '')
        strPas += '&sTextToSearch=';
    else
        strPas += '&sTextToSearch=' + argToPass;

    strPas += '&sCondition=' + encodeURIComponent(document.getElementById('txtFiltro').value);
    strPas += '&nPageNumber=' + encodeURIComponent(glb_nPAGENUMBER);
    strPas += '&nPageSize=' + encodeURIComponent(document.getElementById('selRegistros').value);
    strPas += '&nFiltroContextoID=' + encodeURIComponent(nFiltroContextoID[1]);
/*
    var re = /%3C/g;
    var strPas1 = strPas.replace(re, "%3E%3E");
    
    strPas = strPas1;*/
    
    if (!nFiltroID) 
        strPas += '&nFiltroID=0';
        
    else 
        strPas += '&nFiltroID=' + encodeURIComponent(nFiltroID[1]);
        
    strPas += '&nProprietarioID=' + encodeURIComponent(selProprietariosPL.value);
    strPas += '&bModoComboProprietario=' + encodeURIComponent(glb_modoComboProprietario);
    strPas += '&nEmpresaID=' + encodeURIComponent(empresaID[0]);
    strPas += '&nIdiomaSistemaID=' + encodeURIComponent(empresaID[7]);
    strPas += '&nIdiomaEmpresaID=' + encodeURIComponent(empresaID[8]);
    strPas += '&nIdiomaID=' + encodeURIComponent(getDicCurrLang());
    strPas += '&userID=' + encodeURIComponent(getCurrUserID());
    strPas += '&C1=' + encodeURIComponent(getCurrRightValue('SUP', 'C1'));
    strPas += '&C2=' + encodeURIComponent(getCurrRightValue('SUP', 'C2'));
    strPas += '&A1=' + encodeURIComponent(getCurrRightValue('SUP', 'A1'));
    strPas += '&A2=' + encodeURIComponent(getCurrRightValue('SUP', 'A2'));    

    dsoListData01.URL = SYS_ASPURLROOT + '/serversidegenEx/pesqlistsvr.aspx' + strPas;
    dsoListData01.ondatasetcomplete = dsoListData01_DSC;
    dsoListData01.Refresh();
}

/********************************************************************
Esta funcao preenche a listagem da pesquisa.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoListData01_DSC() {
    var i;

    if ((!dsoListData01.recordset.EOF) && (!dsoListData01.recordset.BOF)) {
        if ((dsoListData01.recordset['fldError'].value != '') &&
            (dsoListData01.recordset['fldError'].value != null)) {
            if (window.top.overflyGen.Alert(dsoListData01.recordset['fldError'].value) == 0)
                return null;

            lockInterface(false);

            __btn_PESQ('sup');

            setButtonsInCtrlBar();

            // dispara evento no pesqlist.js do form
            if (fg.Rows > 1) {
                if (fg.FrozenCols == 0)
                    fg.FrozenCols = 1;
            }
            //window.top.__glb_ListOrDet = 'PL';
            pesqlistIsVisibleAndUnlocked();

            // foca campo de filtro
            if (txtFiltro.disabled == true)
                txtFiltro.focus();
            // ou foca combo do control bar superior
            else
                setFocusInCmbOfControlBar('sup', lastCmbSelectInCtrlBar('sup'));

            return null;
        }
    }

    lockInterface(false);

    buildGrid();

    glb_LASTLINESELID = '';

    // Escreve no status bar
    writeInStatusBar('child', 'cellMode', numRegsInList(null));

    divSup01_02.style.visibility = 'visible';
    divSup01_01.style.visibility = 'visible';

    setButtonsInCtrlBar();

    // dispara evento no pesqlist.js do form
    if (fg.Rows > 1) {
        if (fg.FrozenCols == 0)
            fg.FrozenCols = 1;
    }
    //window.top.__glb_ListOrDet = 'PL';
    // pesqlistIsVisibleAndUnlocked();

    if (glb_aCOLPESQFORMAT != null) {
        if (glb_bPesqlistAutomaticTotalLines) {
            var aArraySum = new Array();

            for (i = 0; i < glb_aCOLPESQFORMAT.length; i++) {
                if (glb_aCOLPESQFORMAT[i].indexOf('#.00') >= 0)
                    aArraySum[aArraySum.length] = new Array(i, glb_aCOLPESQFORMAT[i], 'S');
            }

            if (aArraySum.length > 0) {
                glb_bHasTotalLine = true;
                gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, aArraySum);

                fg.Redraw = 0;
                fg.AutoSizeMode = 0;
                fg.AutoSize(0, fg.Cols - 1);
                fg.Redraw = 2;
            }
        }
    }

    pesqlistIsVisibleAndUnlocked();

    // foca o combo da barra
    setFocusInCmbOfControlBar('sup', lastCmbSelectInCtrlBar('sup'));

    // foca a lista apenas se nao em modo automatico
    if (!chkRefrInf.checked) {
        window.focus();
        fg.focus();
    }

    // tenta selecionar item na listagem 
    // se em refresh automatico
    if ((chkRefrInf.checked) && (__idOfRegToSelectInGrid != null)) {
        for (i = 1; i < fg.Rows; i++) {
            if (fg.TextMatrix(i, 0) == __idOfRegToSelectInGrid.toString()) {
                fg.Row = i;
                if (fg.Row > fg.BottomRow) {
                    fg.TopRow = fg.Row;
                }
                break;
            }
        }
    }

    if (fg.Row > 0)
        writeInStatusBar('child', 'cellMode', numRegsInList(null, true));

    // start o refresh automatico
    startTimerRefr();
}

/********************************************************************
Esta funcao anula linha de totais em grid do pesqlist colocada pela
automacao

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function disableTotalLineInGrid(bEnable) {
    glb_bPesqlistAutomaticTotalLines = !bEnable;
}

/********************************************************************
Esta funcao ajusta o control bar superior, conforme a interface
esteja em modo de pesquisa ou de lista.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function setButtonsInCtrlBar() {
    // Novo modo
    if ((fg.Rows - 1) < parseInt(selRegistros.value, 10))
        lockBtnInCtrlBar('sup', 11, true);
    else
        lockBtnInCtrlBar('sup', 11, false);

    adjustSupInfControlsBar('PESQLISTONEVIEW', null, null, null, fg);

    // destrava o combo de empresas
    sendJSMessage('AnyHtml', JS_STBARGEN, 'CMBSTATUS', false);

    if ((fg.Enabled == true) && (fg.Rows > 1)) {
        if (!chkRefrInf.checked)
            fg.focus();
    }
    else if (txtArgumento.disabled == true)
        txtArgumento.focus();

}

/********************************************************************
Esta funcao constroi a lista de registros conforme a pequisa.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function buildGrid() {
    glb_GridPesqListMounting = true;

    var i;
    var aFields = new Array();
    var aMask = new Array();

    removeCombo(fg);
    startGridInterface(fg);

    headerGrid(fg, glb_COLPESQORDER, []);

    for (i = 0; i < dsoListData01.recordset.Fields.Count; i++) {
        if (dsoListData01.recordset.Fields[i].Name.toUpperCase() == 'FLDERROR') {
            continue;
        }

        aFields[aFields.length] = dsoListData01.recordset.Fields[i].Name;
        aMask[aMask.length] = '';
    }

    fg.Redraw = 0;
    fillGridMask(fg, dsoListData01, aFields, aMask, glb_aCOLPESQFORMAT);

    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;
    
    glb_GridPesqListMounting = false;
}

/********************************************************************
Esta funcao refresca a listagem.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function applyRefresh(formID) {
    execPesqShowList(formID);
}

/********************************************************************
Esta funcao pagina a listagem.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function changePageInList(formID, nNumber) {
    glb_nPAGENUMBER = glb_nPAGENUMBER + nNumber;
    if (glb_nPAGENUMBER < 1)
        glb_nPAGENUMBER = 1;

    execPesqShowList(formID);
}

/********************************************************************
Esta funcao mostra a ultima interface ativa (pesquisa ou listagem) e
posiciona a listagem no registro atual do sup01.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function showLastModeAndSetRow(registerID) {
    var i = 0;
    var frameID;

    if (registerID != null) {
        for (i = 1; i < fg.Rows; i++) {
            if (fg.TextMatrix(i, 0) == registerID.toString()) {
                fg.Row = i;
                if (fg.Row > fg.BottomRow) {
                    fg.TopRow = fg.Row;
                }
                break;
            }
        }
    }

    // Esconde o sup e o inf
    frameID = getFrameIdByHtmlId(window.top.sup01ID);
    if (frameID)
        showFrameInHtmlTop(frameID, false);
    frameID = getFrameIdByHtmlId(window.top.inf01ID);
    if (frameID)
        showFrameInHtmlTop(frameID, false);

    // Mostra o recursospesqlist.asp
    frameID = getFrameIdByHtmlId(window.top.pesqListID);
    if (frameID) {
        showFrameInHtmlTop(frameID, true);
        // registra o control bar superior
        regHtmlInControlBar('sup', window.top.pesqListID);
    }
}

/********************************************************************
Usuario clicou o botao do combo de proprietarios

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function btnGetProps_onClick() {
    btnGetProps.disabled = true;
    lockInterface(true);
    var cmbContextData = getCmbCurrDataInControlBar('sup', 1);
    var contextID = cmbContextData[1];

    // Parametros para o servidor
    var strPars = new String();
    strPars = '?';

    var formID = window.top.formID;  // variavel global do browser filho

    // formID
    strPars += 'nFormID=';
    strPars += encodeURIComponent(formID.toString());
    // contextoID
    strPars += '&nContextoID=';
    strPars += encodeURIComponent(contextID.toString());
    // empresaID
    strPars += '&nEmpresaID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_COMBODATA, 'VALUE', null)).toString());
    // userID
    strPars += '&nUsuarioID=' + encodeURIComponent((sendJSMessage('AnyHtml', JS_WIDEMSG, '__CURRUSERID', null)).toString());
    strPars += '&bAllList=' + encodeURIComponent(1);

    dsoPropsPL.URL = SYS_ASPURLROOT + '/serversidegenEx/cmbspropspl.aspx' + strPars;
    dsoPropsPL.ondatasetcomplete = btnGetProps_onClick_DSC;
    dsoPropsPL.refresh();
}

/********************************************************************
Retorno de usuario clicou o botao do combo de proprietarios

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function btnGetProps_onClick_DSC() {
    var optionStr, optionValue;

    // Inicia o carregamento de combos dinamicos (selFabricanteID, selMarcaID)
    //
    clearComboEx(['selProprietariosPL']);

    while (!dsoPropsPL.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoPropsPL.recordset['Proprietario'].value;
        oOption.value = dsoPropsPL.recordset['ProprietarioID'].value;
        selProprietariosPL.add(oOption);
        dsoPropsPL.recordset.MoveNext();
    }

    lockInterface(false);
}

/********************************************************************
Filtro do control bar superior foi alterado pelo usuario.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function supFilterChanged() {
    glb_nPAGENUMBER = 1;

    if (divSup01_02.style.visibility != 'hidden') // se estiver em modo lista
    {
        // alteracao em 27/09/2001
        // o grid nao lista mais automatico
        //__btn_LIST('sup');
        // reseta lista de pesquisa
        fg.ExplorerBar = 0;
        fg.Rows = 1;
        lockInterface(false);

        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', numRegsInList(null));

        divSup01_02.style.visibility = 'visible';
        divSup01_01.style.visibility = 'visible';

        setButtonsInCtrlBar();

        // dispara evento no pesqlist.js do form
        if (fg.Rows > 1) {
            if (fg.FrozenCols == 0)
                fg.FrozenCols = 1;
        }
        //window.top.__glb_ListOrDet = 'PL';
        pesqlistIsVisibleAndUnlocked();
    }
}

/********************************************************************
Mostra detalhes de um item selecionado na lista.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function showDetail() {
    adjustSupInfControlsBar('DISALL');

    // Por questoes esteticas da foco na listagem
    if (fg.Enabled == true) {
        if (!chkRefrInf.checked)
            fg.focus();
    }

    lockInterface(true);

    // reseta grid do inf para estado nada
    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'resetGridInterface()');

    var aIDs = new Array();
    var i = 0;
    for (i = (glb_bHasTotalLine ? 2 : 1); i < fg.Rows; i++)
        aIDs[i - (glb_bHasTotalLine ? 2 : 1)] = fg.TextMatrix(i, 0);

    // Mensagem capturada no sup para mostrar detalhe de um registro
    // param2 = array composto de:
    // id do registro selecionado na lista,
    // linha selecionada na lista
    // array composto dos ids dos registros que compoem a lista
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWDETAIL',
                  [fg.TextMatrix(fg.Row, 0), fg.Row - (glb_bHasTotalLine ? 2 : 1), aIDs]);
}

/********************************************************************
Coloca data no formato MMDDYYYY. Especifica do pesqlist.

Parametros:
string data ou qualquer outra string

Retorno:
string data convertida ou sDate se sDate nao e data
********************************************************************/
function putDateInMMDDYYYY(sDate) {
    // o argumento nao e data, devolve como esta
    if (((selPesquisa.options(selPesquisa.selectedIndex).id).substr(0, 1)).toUpperCase() != 'D')
        return sDate;

    // parece que e data, se nao for devolve como esta
    if (chkDataEx(sDate) != true)
        return sDate;

    // e data, se o sistema esta no formato MMDDYYYY devolve
    if (DATE_FORMAT == "MM/DD/YYYY")
        return sDate;

    // e data e o sistema esta no formato DDMMYYYY, inverte e devolve
    var rExp;
    var aString;
    var nDay, nMonth, nYear;
    var sRet = '';

    rExp = /\D/g;
    aString = sDate.split(rExp);

    if ((typeof (aString)).toUpperCase() != 'OBJECT')
        return sDate;

    if (aString.length > 6)
        return sDate;

    nDay = parseInt(aString[0], 10);
    nMonth = parseInt(aString[1], 10);
    nYear = parseInt(aString[2], 10);

    sRet = (nMonth.toString() + '/' + nDay.toString() + '/' + nYear.toString());

    // Devolve mes, dia e ano, se nao tem parte de hora na string
    if (aString.length == 3)
        return sRet;

    if (aString.length == 4)
        sRet = sRet + ' ' + aString[3] + ':00:00';
    else if (aString.length == 5)
        sRet = sRet + ' ' + aString[3] + ':' + aString[4] + ':00';
    else if (aString.length == 6)
        sRet = sRet + ' ' + aString[3] + ':' + aString[4] + ':' + aString[5];

    // Devolve data com hora
    return sRet;
}

// FUNCOES DE DADOS DO BROWSER MAE **********************************

/********************************************************************
Esta funcao retorna um array com os dados da empresa corrente.
Estes dados estao no statusbarchild.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
array   - array[0] - empresaID
- array[1] - empresaPaisID
- array[2] - empresaCidadeID
- array[3] - nome de fantasia da empresa
- array[4] - empresaUFID
- array[5] - TipoEmpresaID
- array[6] - nome completo da empresa
- array[7] - idioma do sistema.
- array[8] - idioma da empresa.

null    - nao tem empresa selecionada        
********************************************************************/
function getCurrEmpresaData() {
    return (sendJSMessage(getHtmlId(), JS_WIDEMSG, '__EMPRESADATA', null));
}

/********************************************************************
Esta funcao retorna o id do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID do usuario ou 0 se nao tem usuario logado

********************************************************************/
function getCurrUserID() {
    return (sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null));
}

/********************************************************************
Esta funcao retorna o e-mail do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID do usuario ou 0 se nao tem usuario logado

********************************************************************/
function getCurrUserEmail() {
    return (sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSEREMAIL', null));
}

/********************************************************************
Esta funcao retorna o ID da impressora de codigo de barras do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID da impressora de codigo de barras do usuario logado.

********************************************************************/
function getCurrPrinterBarCode() {
    return (sendJSMessage(getHtmlId(), JS_WIDEMSG, '__PRINTERBARCODE', null));
}

// FINAL DE FUNCOES DE DADOS DO BROWSER MAE *************************

// FUNCOES DE DIREITOS **********************************************

/********************************************************************
Atualiza o label dos combos de sujeito e objeto
Esta funcao e usada nos forms de relacoes

Parametro:
folderID        - ID do folder a pesquisar

Retorno:
O nome da pasta se sucesso ou uma string vazia se a pasta nao e
encontrada
********************************************************************/
function getFolderNameByDso(folderID) {
    if (folderID == null)
        return '';

    var dsoBmk = null;
    var dso = dsoCmbsContFilt;
    var retVal = '';
    var aItemSelected;

    // O dso nao foi preenchido
    if (dso.recordset.Fields.Count == 0)
        return retVal;

    // nao tem registros, retorna nao ter direito
    if (dso.recordset.BOF && dso.recordset.EOF)
        return retVal;

    // tem registros, guarda o bookmark se o dso nao esta no BOF nem o EOF    
    if (!(dso.recordset.BOF || dso.recordset.EOF))
        dsoBmk = dso.recordset.Bookmark();

    // faremos aqui nossa pesquisa
    dso.recordset.MoveFirst();

    dso.recordset.setFilter('ControlBar = ' + '\'' + 'INF' + '\'' + ' AND Combo = ' +
                           '\'' + 1 + '\'' + ' AND RecursoID = ' +
                           '\'' + folderID + '\'');

    // verifica se o objeto n�o est� no EOF
    if (!dso.recordset.EOF)
        retVal = dso.recordset.Fields['Recurso'].value;

    dso.recordset.setFilter('');

    // volta o ponteiro do dso no registro anterior a funcao, se temos
    // Bookmark
    if (dsoBmk != null)
        dso.recordset.gotoBookmark(dsoBmk);

    return retVal;
}

// FINAL DE FUNCOES DE DIREITOS *************************************

// VARIAVEIS E FUNCOES DO CARRIER ***********************************
// Controla registro ID enviado pelo carrier
var glb_regID_Carrier = 0;

/********************************************************************
Verifica se registro solicitado pelo carrier vale no corrente contexto

Parametros:
nenhum

Retorno:
o xml com os dados ou um registro de mensagem de erro
********************************************************************/
function execPesqForCarrier(regID) {
    if (chkRefrInf.checked)
        stopTimerRefr();

    lockInterface(true);

    glb_regID_Carrier = regID;

    // parametros necessarios ao arquivo de pesquisa do servidor
    var formID = window.top.formID;
    var nSubFormID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'window.top.subFormID');
    var sType = document.getElementById('selPesquisa').options[0].id;
    var nFiltroContextoID = getCmbCurrDataInControlBar('sup', 1);
    var nFiltroID = getCmbCurrDataInControlBar('sup', 2);
    var empresaID = getCurrEmpresaData();

    sType = sType.substr(0, 1);

    var strPas = new String();

    strPas = '?nFormID=' + encodeURIComponent(formID);
    strPas += '&nSubFormID=' + encodeURIComponent(nSubFormID);
    strPas += '&sFldKey=' + encodeURIComponent(document.getElementById('selPesquisa').options[0].value);

    strPas += '&nOrder=ASC';

    strPas += '&bRegistrosVencidos=false';

    var argToPass = regID.toString();

    // O campo argumento esta vazio
    if (argToPass == '')
        strPas += '&nArgumentoIsEmpty=' + encodeURIComponent(0);
    else
        strPas += '&nArgumentoIsEmpty=' + encodeURIComponent(1);

    // Coloca data no sistema mm/dd/yyyy se for o caso
    argToPass = putDateInMMDDYYYY(argToPass);

    if (argToPass == '')
        strPas += '&sTextToSearch=';
    else
        strPas += '&sTextToSearch=' + encodeURIComponent(argToPass);

    strPas += '&sCondition=' + encodeURIComponent('');
    strPas += '&nPageNumber=' + encodeURIComponent(1);
    strPas += '&nPageSize=' + encodeURIComponent(1);
    strPas += '&nFiltroContextoID=' + encodeURIComponent(nFiltroContextoID[1]);

    strPas += '&nFiltroID=0';

    strPas += '&nProprietarioID=' + encodeURIComponent(selProprietariosPL.value);
    strPas += '&bModoComboProprietario=' + encodeURIComponent(glb_modoComboProprietario);
    strPas += '&nEmpresaID=' + encodeURIComponent(empresaID[0]);
    strPas += '&nIdiomaSistemaID=' + encodeURIComponent(empresaID[7]);
    strPas += '&nIdiomaEmpresaID=' + encodeURIComponent(empresaID[8]);
    strPas += '&nIdiomaID=' + encodeURIComponent(getDicCurrLang());
    strPas += '&userID=' + encodeURIComponent(getCurrUserID());
    strPas += '&C1=' + encodeURIComponent(getCurrRightValue('SUP', 'C1'));
    strPas += '&C2=' + encodeURIComponent(getCurrRightValue('SUP', 'C2'));
    strPas += '&A1=' + encodeURIComponent(getCurrRightValue('SUP', 'A1'));
    strPas += '&A2=' + encodeURIComponent(getCurrRightValue('SUP', 'A2'));

    dsoListData01.URL = SYS_ASPURLROOT + '/serversidegenEx/pesqlistsvr.aspx' + strPas;
    dsoListData01.ondatasetcomplete = dsoListData01Carrier_DSC;
    dsoListData01.Refresh();
}

/********************************************************************
Retorno do servidor da funcao execPesqForCarrier(regID)

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function dsoListData01Carrier_DSC() {
    var result = false;

    if (!(dsoListData01.recordset.BOF && dsoListData01.recordset.EOF)) {
        dsoListData01.recordset.MoveFirst();
        if (dsoListData01.recordset.Fields[0].value == parseInt(glb_regID_Carrier, 10))
            result = true;
    }

    lockInterface(false);

    if (result)
        sendJSMessage(window.top.pesqListID, JS_DATAINFORM, 'SHOWDETAIL',
                      [parseInt(glb_regID_Carrier, 10), null, null]);
    else {
        window.focus();
        if (window.top.overflyGen.Alert('Registro n�o localizado ou \nincompat�vel com o contexto/filtro!') == 0) {
            if (chkRefrInf.checked)
                startTimerRefr();

            return null;
        }
    }

}
// FINAL DE VARIAVEIS E FUNCOES DO CARRIER **************************

// VARIAVEIS E FUNCOES DE REFRESH AUTOMATICO ************************

/********************************************************************
Notas:
Quando em modo de listagem:
Botoes Listar e Refresh, Proximo e Anterior - 1 e 9, 10 e 11 -
param o refresh automatico ao serem acionados e startam o refresh
automatico apos repreencherem a listagem
Botoes Detalhar e Incluir - 2 e 3 - cancelam o refresh automatico

Vindo do detalhe sao duas mensagens
'SHOWPESQFROMDET' - mostra o pesqlist: 1 - no carregamento do form,
2 - o usuario clicou o botao Pesquisar no detalhe. A possibilidade e
desta mensagem esta desativada pois o botao de pesquisa nao existe mais
no modo detalhe

'SHOWPESQLISTFROMDET' - mostra o pesqlist: 1 - se nao voltou dados do
do servidor para preencher o detalhe, 2 - o usuario clicou o botao
Listar no detalhe.

********************************************************************/
var __timerRefrPesqList = null;

var __idOfRegToSelectInGrid = null;

function startTimerRefr(intInMilliSecs) {
    if (!chkRefrInf.checked) {
        stopTimerRefr();
        return null;
    }

    stopTimerRefr();

    // default 180 segundos
    if ((intInMilliSecs == null) || (intInMilliSecs == 0))
        intInMilliSecs = 3 * 60 * 1000;

    __timerRefrPesqList = window.setInterval('__doTimerRefr()', intInMilliSecs, 'JavaScript');
}

function stopTimerRefr() {
    if (__timerRefrPesqList != null) {
        window.clearInterval(__timerRefrPesqList);
        __timerRefrPesqList = null;
    }
}

function __doTimerRefr() {
    stopTimerRefr();

    __refreshItemsList();
}

function __refreshItemsList() {
    var frameTop = getFrameInHtmlTop('frameSup02');

    if (frameTop == null)
        return null;

    if (frameTop.currentStyle.visibility == 'visible') {
        // verificar se botoes listar e refresh estao habilitados
        if (btnIsEnableInCtrlBar('sup', 1) && btnIsEnableInCtrlBar('sup', 9))
            __btn_REFR('sup');
    }
}

// FINAL DE VARIAVEIS E FUNCOES DE REFRESH AUTOMATICO ***************
