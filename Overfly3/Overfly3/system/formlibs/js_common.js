/********************************************************************
js_common.js

Library javascript de funcoes basicas para funcoes comuns aos pesqlist,
sup e inf dos forms
********************************************************************/

// CONSTANTES *******************************************************

var glb_LASTMSGSTBAR = 'Processando';

var glb_LastActionInForm__ = 0;

// FINAL DE CONSTANTES **********************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES GERAIS:
    interfaceMode()
    showDivBySFS(aSecDivs, nSFSToShow)
    getStaticCmbsData(aStaticCmbs, fType)
    dsoEstaticCmbs_DSC()
    fillStaticCombos(aCmbAndIndex, formPos)
    linkDivsAndSubForms(mainDivID, aSecDivs, aSubFormID)
    tagMainDiv(mainDivID)
    fillArrayOfSecondaryDivIDs()
    adjustSupInfCommonFields()
    genericMsgProcessing()
    dateFormatToSearch(sDate)
    fieldExists(dso, fldName)
    __currFormState()

FUNCOES DE DADOS DO BROWSER MAE:
    getCurrEmpresaData()
    getCurrDataInControl(sPos, controlID, bInnerText)
    getCurrUserID()
    getCurrUserEmail()
    getCurrPrinterBarCode()

FUNCOES PARA JANELAS MODAIS DE LUPA:
    loadModalOfLupa(cmbID, labelOpt)
    lockBtnLupa(btnRef, cmdLock)

ESPECIFICAS PARA RELACOES
showModalRelacoes(nFormID, sComboID, sCaller, sLabel, nTipoRelacaoID, nRegExcluidoID)
    
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// FUNCOES GERAIS ***************************************************

/********************************************************************
Retorna se o form corrente esta em modo pesquisa ou detalhe

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function interfaceMode()
{
    return window.top.__interfaceMode();
}

/********************************************************************
Esta funcao mostra um div em funcao do seu atributo sfsGrupo

Parametros:
aSecDivs            - array de divs secundarios
nSFSToShow          - valor do atribtuto do div a mostrar

Retorno:
true se consegue mostrar uma pasta, caso contrario false
********************************************************************/
function showDivBySFS(aSecDivs, nSFSToShow)
{
    var i, j;
    var elem, aAttrib;
    
    var retVal = false;
    
    for ( i=0; i<aSecDivs.length; i++ )
    {
        elem = window.document.getElementById(aSecDivs[i]);
        
        // 2 possibilidades: atributo sfsGrupo do div e um array ou nao
        // Primeiro caso, nao e array
        if ( (typeof(elem.getAttribute('sfsGrupo', 1))).toUpperCase()== 'NUMBER' )
        {
            if ( parseInt(elem.getAttribute('sfsGrupo', 1)) == parseInt(nSFSToShow, 10) )
            {
                // Mostra/esconde controles que tenham funcao a evaluar
                // como atributo especial
                evalShowHideCtlsInDivs(elem);
                
                // Mostra o div
                elem.style.visibility = 'visible';
                retVal = true;
            }    
            else
                elem.style.visibility = 'hidden';
        }        
        // Segundo caso, e array
        if ( (typeof(elem.getAttribute('sfsGrupo', 1))).toUpperCase()== 'OBJECT' )
        {
            aAttrib = elem.getAttribute('sfsGrupo', 1);
            for ( j=0; j<aAttrib.length; j++ )
                if ( parseInt(aAttrib[j], 10) == parseInt(nSFSToShow, 10) )
                {
                    // Mostra/esconde controles que tenham funcao a evaluar
                    // como atributo especial
                    evalShowHideCtlsInDivs(elem);
                    
                    // Mostra o div
                    elem.style.visibility = 'visible';
                    retVal = true;
                    break;
                }    
                else
                    elem.style.visibility = 'hidden';
                    
        }
    }
    
    return retVal;
}

/********************************************************************
Esta funcao obtem os dados no servidor, dos combos estaticos do sup01
e do inf01
           
Parametros: 
aStaticCmbs     - array de combos estaticos (variavel glb_aStaticCombos)
                  do sup01.asp e do inf01.asp (== null se nao tem combos)
                  estaticos
fType           - tipo do arquivo no form: 'sup' ou 'inf'

Retorno:
nenhum
********************************************************************/
function getStaticCmbsData(aStaticCmbs, fType)
{
    if ( aStaticCmbs == null )
    {
        dsoEstaticCmbs_DSC();
        return null;
    }            

    if ( fType == null )
        return null;

    var strPas = new String();
    
    strPas = '?nFormID='+escape(window.top.formID);
    
    var empresaID = getCurrEmpresaData();
    strPas += '&nEmpresaID=' + escape(empresaID[0]);
    strPas += '&nEmpresaPaisID=' + escape(empresaID[1]);
    strPas += '&nUserID=' + escape(getCurrUserID());
    strPas += '&nIdiomaSistemaID=' + escape(empresaID[7]);
    strPas += '&nIdiomaEmpresaID=' + escape(empresaID[8]);
    
    setConnection(dsoEstaticCmbs);
    if ( fType.toUpperCase() == 'SUP')
        dsoEstaticCmbs.URL = SYS_ASPURLROOT + '/serversidegenEx/staticcmbsdatasup.aspx' + strPas;
    else if ( fType.toUpperCase() == 'INF')
        dsoEstaticCmbs.URL = SYS_ASPURLROOT + '/serversidegenEx/staticcmbsdatainf.aspx' + strPas;
    
    dsoEstaticCmbs.ondatasetcomplete = dsoEstaticCmbs_DSC;
    dsoEstaticCmbs.Refresh();
}

/********************************************************************
Esta funcao e disparada ao final do retorno do servidor, dos dados
que preenchem os combos estaticos do sup01 e do inf01
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoEstaticCmbs_DSC()
{
    // carregou os dados para os combos estaticos
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'FINISHSTCMBS', null);
}

/********************************************************************
Esta funcao preenche os combos estaticos do sup01 e do inf01
           
Parametros: 
aCmbAndIndex      - array bidimensional: combo a preencher e campo Indice
                    no dso ou null se nao tem combos
formPos           - 'SUP' se chamado pelo sup01 e 'INF'se chamado pelo
                    inf01

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function fillStaticCombos(aCmbAndIndex, formPos)
{
    // caso nao tenha combos estaticos
    if ( aCmbAndIndex == null )
        return true;

    var i;
    var oldDataSrc, oldDataFld;
    var cmbRef;
    
    if (dsoEstaticCmbs.recordset.BOF && dsoEstaticCmbs.recordset.EOF)
        return true;
    
    for (i=0; i<aCmbAndIndex.length; i++)
    {
        cmbRef = window.document.getElementById(aCmbAndIndex[i][0]);
        
        // no sup01, se tem combo de tipo de registro,
        // nao preenche o combo de tipo de registro aqui
        // este combo e preenchido no finishLoadForm do sup01
        // refill dos combos de contexto e filtro
        if ( formPos.toUpperCase() == 'SUP')
        {
            if ( cmbRef.id == glb_sCtlTipoRegistroID )
                continue;
        }
        
        oldDataSrc = cmbRef.dataSrc;
        oldDataFld = cmbRef.dataFld;
        cmbRef.dataSrc = '';
        cmbRef.dataFld = '';
        clearComboEx([cmbRef.id]);
        
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.Find('Indice', aCmbAndIndex[i][1]);
        
        if (!dsoEstaticCmbs.recordset.EOF) 
        {
            while ((!dsoEstaticCmbs.recordset.EOF) && (dsoEstaticCmbs.recordset['Indice'].value == aCmbAndIndex[i][1]))
            {
                var oOption = document.createElement("OPTION");
                oOption.text = dsoEstaticCmbs.recordset.Fields['fldName'].value;
                oOption.value = dsoEstaticCmbs.recordset.Fields['fldID'].value;
                cmbRef.add(oOption);
                dsoEstaticCmbs.recordset.MoveNext();
            }
        }
        cmbRef.dataSrc = oldDataSrc;
        cmbRef.dataFld = oldDataFld;
    }

    return true;
}

/********************************************************************
Esta funcao linka cada div do arquivo ao seu correspondente subform

Parametros:
aSecDivs        - array de divs secundarios
aSubFormID      - array dos ids dos subforms

Retorno:
nenhum
********************************************************************/
function linkDivsAndSubForms(mainDivID, aSecDivs, aSubFormID)
{
    // Tageia os divs
    if ( mainDivID == null )
        mainDivID = '';
        
   tagMainAndSecsDiv(mainDivID);
    
    // Divs Secundarios
    if ( aSecDivs == null )
        return null;
    
    var i;
    
    // interceptar apenas os forms que tenham divs secundarios    
    for ( i=0; i<aSecDivs.length; i++ )           
        window.document.getElementById(aSecDivs[i]).setAttribute('sfsGrupo', aSubFormID[i], 1);

    // preenche array de divs secundarios
    fillArrayOfSecondaryDivIDs();
    
    return null;

}

/********************************************************************
Esta funcao identifica o div principal do sup01, setando o atributo
mainDiv para true no div principal e false nos div secundarios 

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function tagMainAndSecsDiv(mainDivID)
{
    var i;
    var coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        if ( coll.item(i).id.toUpperCase() == mainDivID.toUpperCase() )
            coll.item(i).setAttribute('mainDiv', true, 1);
        else
            coll.item(i).setAttribute('mainDiv', false, 1);    
    }    
}

/********************************************************************
Esta funcao monta o array de ids dos divs de um arquivo

Parametros:
nenhum

Retorno:
Numero de divs colocados no array se sucesso.
-1 se falha
********************************************************************/
function fillArrayOfSecondaryDivIDs()
{
    var i = 0;
    var j = -1;
    
    var coll = window.document.getElementsByTagName('DIV');
    
    for ( j=0; j<coll.length; j++ )
    {
        if ( coll.item(j).getAttribute('mainDiv', 1) == false )
        {
            glb_aDIVS[i] = coll.item(j).id;
            i++;
        }    
    }    
        
    return i;    
}    

/********************************************************************
Esta funcao ajusta os campos txtNulo, txtRegistroID e txtEstadoID
que sao comuns a todos os forms.

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function adjustSupInfCommonFields()
{
    // campo nulo para simular tab
    txtNulo.maxLength = 0;
    with (txtNulo.style)
    {
        visibility = 'visible';
        backgroundColor = 'transparent';
        width = 0;
        height = 0;
        top = 0;
        left = 0;
    }
    
    // se chamada de sup
    if ( getHtmlId() == window.top.sup01ID )
    {
		// Marco 07/12/2004
        // desabilita o campo txtRegistroID
        txtRegistroID.readOnly = true;

        // desabilita o campo txtEstadoID
        txtEstadoID.disabled = true;
    }
}

/********************************************************************
Escreve mensagem generica na barra de status ou torna a escrever
a mensagem anterior.

Parametros:
nenhum
                      
Retorno:
nenhum
********************************************************************/
function genericMsgProcessing()
{
	// obtem a mensagem atual
	var currMsg = getDataInStatusBar('child', 'cellMode');
	
	// strip eventuais 3 pontinhos
	var pPos = currMsg.indexOf('...');
	if ( pPos >= 0 )
        currMsg = currMsg.substr(0, (currMsg.length - 3));
	
	// a mensagem atual e Processando, escreve o vlr arquivado
	if ( (currMsg.toUpperCase()) == 'PROCESSANDO' )
		writeInStatusBar('child', 'cellMode', glb_LASTMSGSTBAR);	
	// a mensagem atual nao e Processando, escreve processando
	else
		writeInStatusBar('child', 'cellMode', glb_LASTMSGSTBAR, true);	

	glb_LASTMSGSTBAR = currMsg;
}

/********************************************************************
Coloca data no formato MMDDYYYY

Parametros:
string data ou qualquer outra string

Retorno:
string data convertida ou sDate se sDate nao e data
********************************************************************/
function dateFormatToSearch(sDate)
{
    // parece que e data, se nao for devolve como esta
    if ( chkDataEx(sDate) != true )    
        return sDate;
        
    // e data, se o sistema esta no formato MMDDYYYY devolve
    if ( DATE_FORMAT == "MM/DD/YYYY" )
        return sDate;
        
    // e data e o sistema esta no formato DDMMYYYY, inverte e devolve
    var rExp;
    var aString;
    var nDay, nMonth, nYear;
    
    rExp = /\D/g;
    aString = sDate.split(rExp);
    
    if ( (typeof(aString)).toUpperCase() != 'OBJECT' )
        return sDate;
        
    if ( aString.length != 3 )
        return sDate;
        
    nDay = parseInt(aString[0], 10);
    nMonth = parseInt(aString[1], 10);
    nYear = parseInt(aString[2], 10);    
    
    return (nMonth.toString() + '/' + nDay.toString() + '/' + nYear.toString());
}

/********************************************************************
Verifica se um dado campo existe em um dado dso

Parametros:
dso         - referencia ao dso
fldName     - nome do campo a verificar a existencia
                      
Retorno:
true se o campo existe, caso contrario false
********************************************************************/
function fieldExists(dso, fldName)
{
    var retVal = false;
    var i;
    
    for ( i=0; i< dso.recordset.Fields.Count; i++ )
    {
        if ( dso.recordset.Fields[i].Name == fldName )
            retVal = true;
    }
    
    return retVal;
}

/********************************************************************
Retorna o estado corrente do form

Parametros:
nenhum
                      
Retorno:
0           - form esta em modo consulta
10          - form esta em modo inclusao sup
11          - form esta em modo inclusao inf
20          - form esta em modo alteracao sup
21          - form esta em modo alteracao inf
30          - form esta em modo exclusao sup
31          - form esta em modo exclusao inf
40          - form esta com modal aberta
50          - form esta com dialogo alert/confirm aberto
********************************************************************/
function __currFormState()
{
    var retVal = 0;
    
    // Predomina modal aberta
    if ( getFrameInHtmlTop('frameModal').currentStyle.visibility == 'visible' )
        retVal = 40;
    else if ( window.top.overflyGen.DlgModalIsOpened )
        retVal = 50;
    else
        retVal = glb_LastActionInForm__;
    
    return retVal;
}

// FINAL DE FUNCOES GERAIS ******************************************

// FUNCOES DE DADOS DO BROWSER MAE **********************************

/********************************************************************
Esta funcao retorna um array com os dados da empresa corrente.
Estes dados estao no statusbarmain.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
array   - array[0] - empresaID
        - array[1] - empresaPaisID
        - array[2] - empresaCidadeID
        - array[3] - nome de fantasia da empresa
        - array[4] - empresaUFID
        - array[5] - TipoEmpresaID
        - array[6] - nome completo da empresa
        - array[7] - idioma do sistema.
        - array[8] - idioma da empresa.

null    - nao tem empresa selecionada        
********************************************************************/
function getCurrEmpresaData()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__EMPRESADATA', null) );
}

/********************************************************************
Esta funcao retorna o id do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID do usuario ou 0 se nao tem usuario logado

********************************************************************/
function getCurrUserID()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null) );
}

/********************************************************************
Esta funcao retorna o e-mail do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID do usuario ou 0 se nao tem usuario logado

********************************************************************/
function getCurrUserEmail()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSEREMAIL', null) );
}

/********************************************************************
Esta funcao retorna o ID da impressora de codigo de barras do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID da impressora de codigo de barras do usuario logado.

********************************************************************/
function getCurrPrinterBarCode()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__PRINTERBARCODE', null) );
}

/********************************************************************
Esta funcao retorna o dado corrente de um controle html.

Parametros:
sPos        - 'sup' se o controle esta no sup, inf se o controle
              esta no inf
controlID   - id do controle
bInnerText  - opcional para combos, se true retorna o texto do option

Retorno:
o dado ou null se nao consegue obter o dado
********************************************************************/
function getCurrDataInControl(sPos, controlID, bInnerText)
{
    // Testa controlID
    if ( controlID == null )
        return null;
    
    // Testa sPos
    if ( sPos == null )
        return null;
        
    sPos = sPos.toUpperCase();
    
    if ( !((sPos == 'SUP') || (sPos == 'INF')) )
        return null;
        
    // Obtem referencia ao html sup ou inf
    var frameID = null;
    var htmlScript = null;
    var ctrl;

    if (sPos == 'SUP')    
    {
        frameID = getFrameIdByHtmlId(window.top.sup01ID);
        if (frameID != null)
            htmlScript = getPageFrameInHtmlTop(frameID);
    }
     
    if (sPos == 'INF')    
    {
        frameID = getFrameIdByHtmlId(window.top.inf01ID);
        if (frameID != null)
            htmlScript = getPageFrameInHtmlTop(frameID);
    }

    // Obteve o script?
    if (htmlScript != null)
    {
        // Obtem referencia ao controle
        ctrl =  htmlScript.document.getElementById(controlID);
        
        if ( ctrl != null )
        {
            // Controle e input type text
            if ( (ctrl.tagName.toUpperCase() == 'INPUT') && (ctrl.type.toUpperCase() == 'TEXT') )
                return ctrl.value;
            // Controle e combo
            else if ( ctrl.tagName.toUpperCase() == 'SELECT' )
            {
                if ( bInnerText )
                {
                    if ( ctrl.length !=0 )
                        if ( ctrl.selectedIndex != -1 )
                            return ctrl.options(ctrl.selectedIndex).innerText;
                    else
                        return null;        
                }    
                else
                    return ctrl.value;
            }    
            // Controle e checkbox
            else if ( (ctrl.tagName.toUpperCase() == 'INPUT') && (ctrl.type.toUpperCase() == 'CHECKBOX') )
                return ctrl.checked;
        }    

    }
    
    // Falhou
    return null;
}

// FINAL DE FUNCOES DE DADOS DO BROWSER MAE *************************

// FUNCOES PARA JANELAS MODAIS DE LUPA ******************************

/********************************************************************
Carrega janela modal para combo de lupa.
           
Parametros: 
cmbID     - id do combo cujo botao de lupa foi apertado
labelOpt  - se  null, usa o default da funcao, caso contrario
            usa labelOpt

Retorno:
nenhum
********************************************************************/
function loadModalOfLupa(cmbID, labelOpt)
{
    var htmlPath;
    
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modaloflupa.asp';
    
    // Veio do sup ou do inf?
    var htmlId = (getHtmlId()).toUpperCase();
    var filePos = 'SUP';
    
    if ( htmlId.indexOf('SUP') > 0 )
        filePos = 'SUP';
    else if ( htmlId.indexOf('INF') > 0 )
        filePos = 'INF';
    
    // Passar o id do label do combo que vai preencher
    // para configurar a janela
    // e o id do combo que vai preencher, que volta com a mensagem
    // para saber o combo a preencher
    var strPars = new String();
    strPars = '?';

    strPars += 'filePos=' + escape(filePos);
    
    if ( labelOpt == null )
        strPars += '&labelToUse=' + escape(labelAssociate(cmbID));
    else
        strPars += '&labelToUse=' + labelOpt;
    
    strPars += '&formID='+ escape(window.top.formID.toString());    
    strPars += '&comboID='+ escape(cmbID);    
    
    showModalWin( (htmlPath + strPars) , new Array(205,126) );
}

/********************************************************************
Trava/destrava botao de lupa.
           
Parametros: 
btnRef      - referencia do botao
cmdLock     - true (trava), false (destrava) 

Retorno:
nenhum
********************************************************************/
function lockBtnLupa(btnRef, cmdLock)
{
    if ( cmdLock == true )
        btnRef.src = glb_LUPA_IMAGES[1].src;
    else
        btnRef.src = glb_LUPA_IMAGES[0].src;
    
    // trava/destrava o botao de lupa do objeto
    btnRef.disabled = cmdLock;
}

// FINAL DE FUNCOES PARA JANELAS MODAIS DE LUPA *********************

// ESPECIFICAS PARA RELACOES ****************************************

/********************************************************************
Abre janela modal para estabelecer relacoes
Parametros:
    nFormID         - ID do form corrente
    sCaller         - 'S', 'I', 'PL' - subform que chamou esta funcao
    sComboID        - ID do combo (do sup do form) a preencher
    sQuemENaRel     - 'SUJ' -> sujeito ou 'OBJ' -> objeto
    sLabel          - complemento do caption da modal
    nTipoRelacaoID  - tipo da relacao
    nRegExcluidoID  - ID de registro a ser excluido dos registros
                      de retorno

Retorno:
Nenhum
********************************************************************/
function showModalRelacoes(nFormID, sCaller, sComboID, sQuemENaRel, sLabel, nTipoRelacaoID, nRegExcluidoID)
{
    var htmlPath;
    var strPars;
    
    var empresaData = getCurrEmpresaData();
        
    strPars = '?nFormID=';    
    strPars += escape(nFormID);
    strPars += '&sCaller=';    
    strPars += escape(sCaller);
    strPars += '&nEmpresaID=';    
    strPars += escape(empresaData[0]);
    strPars += '&sComboID=';    
    strPars += escape(sComboID);
    strPars += '&sQuemENaRel=';    
    strPars += escape(sQuemENaRel);
    strPars += '&sLabel=';    
    strPars += escape(sLabel);
    strPars += '&nTipoRelacaoID=';    
    strPars += escape(nTipoRelacaoID);
    strPars += '&nRegExcluidoID=';    
    strPars += escape(nRegExcluidoID);
    
    htmlPath = SYS_PAGESURLROOT + '/modalgen/modalrelacoes.asp' + strPars;
    showModalWin(htmlPath, new Array(571,284));
}

// FINAL DE ESPECIFICAS PARA RELACOES *******************************
