/********************************************************************
js_detailsup.js

Library javascript de funcoes basicas para detalhes de sups de forms
********************************************************************/

// CONSTANTES *******************************************************
var refresh = 
function()
{
	try { __btn_REFR("sup"); }
	catch(e) {}
};

// Controla se a quantidade de caracteres
// foi definida nos campos de digitacao
var glb_fldMaxLen__ = false;

// Controla se implementou _implementsPasteInTxtFld()
var glb_implementsPasteInTxtFld = false;

// Mantem compatibilidade com algumas bibliotecas
var glb_FORMNAME = window.top.formName;
var glb_USERID = window.top.userID;

// Path dos arquivos inf e pesqlist
var glb_aFilePath = null;

// Array dos divs do arquivo
var glb_aDIVS = new Array();
// Pesqlist em modo pesquisa ou listagem
var glb_PESQLISTMODE = null;
// Guarda o ID do Registro corrente do detalhe superior
var glb_nRegistroID = null;
// Array que contem todos os IDs correntes da lista de pesquisa
var glb_aListaIDs = new Array();
// Guarda um ponteiro que indica o elemento corrente no array glb_aListaIDs
var glb_nPointer = -1;
// Guarda o nome do campo que contem o ID do registro corrente
var glb_sFldIDName = null;
// Guarda o id do controle (combo) que contem os tipos de registros
var glb_sCtlTipoRegistroID = null;
// Guarda o nome do campo que contem o tipo do registro corrente
var glb_sFldTipoRegistroName = null;
// Array bidimensional: id dos combos estaticos e indice na tabela de dados
var glb_aStaticCombos = null;
// Ultimo select executado pelo dso corrente
// e usado na funcao maxLenOnKeyPressSetArrays
var glb_lastSelect = '';

// EstadoID do registro antes de abrir a maquina de estado
var glb_EstadoIDBeforeStateMach = 0;

// Controla se o EstadoID do registro foi gravado no banco
var glb_EstadoIDUpdated = true;

// Guarda motivo da troca de estado
var glb_sMotivoSup = '';

// Ultimo botao clicado no control bar superior
var glb_btnCtlSup = null;

// Array que controla botoes incluir, alterar e excluir
// quando nao devam ser tratados pelos direitos na automacao
// Os elementos do array tem valores true se nao devam ser tratados
var glb_BtnsIncAltEstExcl = new Array(null, null, null, null);

// Variavel que controla gravar valor especifico no registroID
// ID de registro calculado por procedure no banco de dados
var glb_regIDEspecificVal = null;

// Variavel de timer
var glb_DetSupTimerVar = null;

// Variavel global de timer
var glb_treats_FldRegID_Sup_Timer = null;

var __glb_applyCheckBoxDefaultValue = true;

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

// FINAL DE CONSTANTES **********************************************

/* ATRIBUTOS ********************************************************

Aplicado        Atributo            Valores
---------------------------------------------------------------------
dsoSup01        setControls         false, true. Ver nota 1 
                aRequiredFieldsIDs  array de controles requeridos
                aDateFieldsIDs      array de controles de data
INPUT
type text       minMax              array (vlrMax, vlrMin)                

Notas:
1. Quantidade de caracteres limitados, eventos onkeypress
        setados arrays do lockControls e do lockInterface reconstruidos

********************************************************************/

/********************************************************************

INDICE DAS FUNCOES:

LOOP DE MENSAGENS
EVENTOS DO CONTROL BAR SUPERIOR

FUNCOES DA AUTOMACAO:
    lockAndOrSvrSup_SYS()
        - Inicia automacao fazendo operacoes do frame work no servidor
    startInsertFromPesqList()
        - Inicia inserir um novo registro vindo do pesqlist
    dsoSup01FromPesqList_DSC()    
    finalDSOsCascade()
    doChangeDivSup_SYS(btnClicked)
        - Faz as trocas de div da automacao
    startDataFromList()
        - Inicia mostrar detalhes de um registro vindo do pesqlist
    dsoSup01_DSC()
    dsoStateMachine_DSC()
    previousRegister()
    nextRegister()
    movePointerRDS(nID)
    dsoSup01MovePointer_DSC()
    cancelIncludeEdit()
    adjustSupInterface()
    treats_FldRegID_Sup()
    treats_FldRegID_Sup_Timer()
    verify_FldRegID_Sup()
    doNotSaveNewStateSup()

FUNCOES GERAIS:
    windowOnLoad_1stPart()
    windowOnLoad_2ndPart()
    onClickBtnLupa()
    showCurrRegistroID()    
    getCurrRegID()
    getCurrEstadoID()
    getCurrTipoRegID()
    changeSujObjLabels(dso, lblSujID, lblObjID, cmbTipoRegID)
    fillTipRegStaticCombo()

FUNCOES DA ALTERACAO:
    editRegister()

FUNCOES DA GRAVACAO:
    saveRegister_1stPart()
    saveRegister_2ndPart()
    dsoSup01Insert_DSC()
    dsoStateMachineInsert_DSC()
    finalOfSave()

FUNCOES DA EXCLUSAO:
    excludeRegister_1stPart()
    excludeRegister_2stPart()
    excludeRegister_3stPart()
    saveForLogBeforeDelete()

FUNCOES DA MAQUINA DE ESTADO:
    saveEstadoID()
    showStateMachine()
    stateMachSupExec(action)
    getStateMachineAbrev(retFunc)
    setStateMachTxtHint(strHint)
    dsoStateMachLoadDoNothing()

FUNCOES DO CARRIER:
    showDetailByCarrier(regID)
    
********************************************************************/
    
// IMPLEMENTACAO DAS FUNCOES

// LOOP DE MENSAGENS ************************************************

function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        // Msg do carrier chegada vindo de um outro form
        // Se intercepta sempre devolver array:
        // array[0] = getHtmlId() ou null
        // array[1] = qualquer coisa ou null
        // array[2] = qualquer coisa ou null
        case JS_CARRIERCAMING :
        {
            return carrierArrived(idElement[0], idElement[1], param1, param2);
        
            /*
            //Exemplo do carrier
            if ( param1 != window.top.formName )
            {
                if ( window.top.overflyGen.Alert ( 'Carrier chegou no form: ' + window.top.formName ) == 0 )
                    return null;
                if ( window.top.overflyGen.Alert (idElement + '\n' + param1 + '\n' + param2 ) == 0 )
                    return null;
                var temp = new Array(getHtmlId(), 'param1', 'param2');
                return temp;
            }
            */
        }    
        break;
        
        // Msg do carrier retorno vindo de um form que interceptou
        // Veja nota em case JS_CARRIERCAMING. Sempre tras:
        // idElement[0] = null ou id do html do arquivo que interceptou
        // idElement[1] = nome do browser filho que contem o arquivo
        //                que interceptou
        // param1 e o array[1] passado = qualquer coisa ou null
        // param2 e o array[2] passado = qualquer coisa ou null
        case JS_CARRIERRET :
        {
            lockInterface(false);
            return carrierReturning(idElement[0], idElement[1], param1, param2);
        
            /*
            //Exemplo do carrier
            if ( window.top.overflyGen.Alert ('Voltou ao form de partida') == 0 )
                return  null;    
            if ( window.top.overflyGen.Alert(idElement + '\n' + param1 + '\n' + param2) == 0 )
                return null;
            */
        }
        break;    

        // Msg para iniciar carregamento dos arquivos inf01 e pesqlist
        case JS_PAGELOAD :
        {
            if ( param1[0].toUpperCase() == window.top.name.toUpperCase())
                loadPage(idElement, param1, param2);
            return 0;    
        }
        break;

        // Msg que informa o final de carregamento de um arquivo do form:
        // sup01, inf01 e pesqlist. Resolve a funcao que controla o carregamento
        // total do form e mostra sua interface ao usuario.
        case JS_PAGELOADED :
        {
            if ( param1[0].toUpperCase() == window.top.name.toUpperCase())
            {
                if ( param1[1].toUpperCase() == getInterfaceName() )
                {
                    if ( componentOfInterfaceLoaded(param2) == true )
                    {
                        if ( finishLoadForm(window.top.sup01ID, window.top.inf01ID, window.top.pesqListID) )
                        {
                            // no sup01, se tem combo de tipo de registro,
                            // preenche o combo de tipo de registro aqui
                            fillTipRegStaticCombo();    
                            
                            // dispara evento no sup01, mostrou a interface
                            // pela primeira vez
                            formFinishLoad();
                            
                            // Abre o form em modo de pesquisa
                            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWPESQFROMDET', null);
                        }
                    }
                }
            }
            return 0;    
        }
        break;
        
        // Mensagens da automacao
        case JS_SUPSYS :
        {
            // Msg enviada pelo arquivo js_detailsup.js.
            // As operacoes de servidor do sup foram feitas.
            // Chama funcao onde o programador faz as operacoes de
            // servidor particulares do sup
            if ( (param1 == 'PROGSERVER') && (idElement == getHtmlId()) )
            {
				//window.top.__glb_ListOrDet = 'DET';
                // param2 == ultimo botao clicado no control bar superior
                prgServerSup(param2);
                return 0;
            }
            // Msg enviada pelo arquivo xxx_inf.js
            // Terminaram as operacoes particulares de servidor do inf e deve
            // ser executado a troca de divs do sup
            else if ( (param1 == 'CHANGEDIV') && (idElement == window.top.inf01ID) )
            {
                // param2 e o botao clicado
                doChangeDivSup_SYS(param2);
                return 0;
            }
            // Msg enviada pelo arquivo js_detailsup.js
            // Os divs do sup foram trocados na automacao
            // Fazer as operacoes de interface particulares do form
            else if ( (param1 == 'AFTERCHANGEDIV') && (idElement == getHtmlId()) )
            {
                // param2 == ultimo botao clicado no control bar superior
                prgInterfaceSup(param2);
                
                // procurar o sendJS igual ao abaixo que foi trapeado para 
                // fazer esta alteracao
                if ( param2 == 'SUPDET' )
                {
                    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                                  'showDetailInterface()');
                }
                                
                return 0;
            }
        }
        break;
        
        // Mensagens da maquina de estado.
        // Sao enviadas ao terminar de carregar e ao fechar no botao cancela.
        case JS_STMACHMODAL :
        {
            if (idElement == 'statemachsupmodal01Html')
            {
                if (param1 == 'STATEMACH_VISIBLE')
                {
                    glb_EstadoIDBeforeStateMach = getCurrEstadoID();
                    
                    // alerta programador que a maquina de estado abriu
                    stateMachOpened( glb_EstadoIDBeforeStateMach );
                    
                    writeInStatusBar('child', 'cellMode', 'Estado');
                }    
                else if (param1 == 'STATEMACH_HIDDEN')
                {
                    writeInStatusBar('child', 'cellMode', 'Detalhe');
                }    
            
                return 0;
            }    
        }
        break;
        
        // Mensagens do programador
        case JS_DATAINFORM :
        {
            // Msg vinda do pesqlist (pesquisa ou listagem).
            // Inserir novo registro.
            if ( (param1 == 'INSERTNEWREG') && (idElement == window.top.pesqListID) )
            {
                // param2 == 'FROMPESQ' ou 'FROMLIST'
				glb_PESQLISTMODE = param2;
				__btn_INCL('pesqlist');             
                return 0;
            }
            // Msg vinda do js_detailsup.js.
            // Retorna valor correspondente ao id do tipo de registro
            // se o form tem tipo de registro.
            else if ( (param1 == 'SFS') && (idElement == getHtmlId()) )
            {
                if (glb_sFldTipoRegistroName != '' && param2 =='SELVALUE')
                    return document.getElementById(glb_sCtlTipoRegistroID).value;
                else if (glb_sFldTipoRegistroName != '' && param2 =='DSOVALUE')
                    return dsoSup01.recordset[glb_sFldTipoRegistroName].value;
                else
                    return -1;
            }
            // Msg vinda do js_detailsup.js.
            // Dados dos combos estaticos chegaram do servidor.
            else if ( (param1 == 'FINISHSTCMBS') && (idElement == getHtmlId()) )
            {
                // Preenche combos estaticos
                if ( fillStaticCombos(glb_aStaticCombos, 'SUP') )
                {
                    // Este arquivo carregou
                    sendJSMessage(getHtmlId(), JS_PAGELOADED,
                                  new Array(window.top.name.toString(),
                                  window.top.formName), 0X1);                            
                    return 0;
                }
            }
            // Msg vinda do pesqlist.
            // Esta mensagem e enviada pelo pesqlist em modo de listagem qdo
            // o usuario seleciona um registro da lista e clica o botao
            // detalhe do control bar superior.
            else if ( (param1 == 'SHOWDETAIL') && (idElement == window.top.pesqListID) )
            {
                // Guarda o id do registro
                glb_nRegistroID = param2[0];
                // Guarda linha corrente na lista
                glb_nPointer = param2[1];
                // Guarda os ids da lista 
                glb_aListaIDs = param2[2];
                // Inicia operacoes para mostrar os detalhes do registro
                glb_DetSupTimerVar = window.setInterval('startDataFromList()', 10, 'JavaScript');
                
                return 0;
            }
            // Msg vinda da janela da maquina de estado.
            // Troca o estado do registro.
            else if ( (param1 == 'ESTIDCHANGED') && (idElement == 'statemachsupmodal01Html') )
            {
                // Novo estadoID selecionado na maquina de estado
                changeStateMachine(glb_sFldIDName,dsoSup01.recordset[glb_sFldIDName].value,param2,
                                    dsoSup01,'','sup');
                return 0;
            }
            // Msg vinda do js_detailinf.js.
            // Retorna o registro ID corrente ou zero se o sup
            // esta em modo de inclusao
            else if ( (param1 == 'REGISTROID') && (idElement == window.top.inf01ID) )
            {
                var currRegistroID = null;
    
                if ((! dsoSup01.recordset.EOF) && (! dsoSup01.recordset.BOF))
                    currRegistroID = dsoSup01.recordset[glb_sFldIDName].value;

                return currRegistroID;
            }
            // Msg vinda do js_detailinf.js.
            // Retorna o tipo de registro ID corrente ou zero se o form
            // nao tem este tipo
            else if ( (param1 == 'TIPOREGISTROID') && (idElement == window.top.inf01ID) )
            {
                if ( glb_sFldTipoRegistroName == '' )
                    return 0;
                else
                {
                    if( !dsoSup01.recordset[glb_sFldTipoRegistroName].value )
                        return 0;
                    return dsoSup01.recordset[glb_sFldTipoRegistroName].value;
                }    
            }
            // Msg vinda da automacao do js_detailinf.js.
            // Coloca foco no primeiro campo editavel do sup
            else if ( (param1 == 'FOCUS1STFIELD') && (idElement == window.top.inf01ID) )
                setFocus1Field();
            // Mensagem recebida do inf, para travar a interface do inf
            else if ( (param1 == 'LOCKCONTROLS') && (idElement == window.top.inf01ID) )
            {
                // param2 == cmdLock
                lockControls(param2, false);
                return 0;
            }    
            // Msg vinda de janela modal que deve ser propagada
            // para o form
            else if ( param1.lastIndexOf('_CALLFORM_S') > 0 )
            {
                if ( param1.indexOf('OK') >= 0 )
                    modalInformForm(idElement, 'OK', param2);
                if ( param1.indexOf('CANCEL') >= 0 )
                    modalInformForm(idElement, 'CANCEL', param2);
                        
                return true;
            }
            // Mensagens de retorno de dados para o programador
            else if ((idElement == 'SUP_HTML') && (param1 == EXECEVAL))
            {
                return eval(param2);
            }
        }
        break;
        
        default:
            return null;
    }
}

// FINAL DE LOOP DE MENSAGENS ***************************************

// EVENTOS DO CONTROL BAR SUPERIOR **********************************
// A ORDEM DOS BOTOES
// 0    - Pesquisar     PESQ
// 1    - Listar        LIST
// 2    - Detalhar      DET
// 3    - Incluir       INCL
// 4    - Alterar       ALT
// 5    - Estado        EST
// 6    - Excluir       EXCL
// 7    - Ok            OK
// 8    - Cancelar      CANC
// 9    - Refresh       REFR
// 10   - Anterior      ANT
// 11   - Seguinte      SEG
// 12   - Um            UM
// 13   - Dois          DOIS
// 14   - Tres          TRES

function __btn_PESQ(controlBar) 
{
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, 'SUPPESQ') != null )
        return true;
        
    showPesqListInterface('SUPPESQ');
}

function __btn_LIST(controlBar)
{
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, 'SUPLIST') != null )
        return true;

    showPesqListInterface('SUPLIST');
}

function __btn_INCL(controlBar) 
{
    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 10;
    
    // usuario clicou INCL, nao veio direto do pesqlist
    if ( controlBar.toUpperCase() == 'SUP' )
        glb_PESQLISTMODE = null;        
        
    glb_btnCtlSup = 'SUPINCL';
    
    // se inclusao zera o hint
    setStateMachTxtHint('');
    
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
    
    lockAndOrSvrSup_SYS();

    updateHTMLFields();
}    

function __btn_ALT(controlBar) 
{
    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 20;
    
    glb_PESQLISTMODE = null;
    
    glb_btnCtlSup = 'SUPALT';
        
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
    
	try
	{
		editRegisterParticular();
	}
	catch (e)
	{
		editRegister();
	} 
}

function __btn_EST(controlBar) 
{
    glb_btnCtlSup = 'SUPEST';
    
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
    
    showStateMachine();
}

function __btn_EXCL(controlBar)
{
/*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
*******************/
    glb_LastActionInForm__ = 30;
    
    glb_btnCtlSup = 'SUPEXCL';
    
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
    
    excludeRegister_1stPart();
}    

function __btn_OK(controlBar) 
{
/*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
*******************/
    glb_LastActionInForm__ = 0;
    
    glb_btnCtlSup = 'SUPOK';
    
    try
    {
		trimAllImputFields(window);
		updateRecordFromHTMLFields();
	}
	catch(e)
	{
		;
	}
	
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
    
    saveRegister_1stPart();
}    

function __btn_CANC(controlBar) 
{
/*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
*******************/
    glb_LastActionInForm__ = 0;
    
    glb_btnCtlSup = 'SUPCANC';
    
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
        
    // Escreve no status bar
    // Foi colocado aqui para acelerar a escrita
    writeInStatusBar('child', 'cellMode', 'Cancelando', true);
    
    lockAndOrSvrSup_SYS();
}    

function __btn_REFR(controlBar) 
{
    glb_btnCtlSup = 'SUPREFR';
        
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
    
    lockAndOrSvrSup_SYS();
}    

function __btn_ANT(controlBar) 
{
    glb_btnCtlSup = 'SUPANT';
    
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
    
    // Nao deve paginar, foi aberto pelo carrier
    if ( glb_nPointer == null )
    {
        adjustSupInfControlsBar('SUPCONS', 'P');     
        return true;
    }        
    
    lockAndOrSvrSup_SYS();
}    

function __btn_SEG(controlBar) 
{
    glb_btnCtlSup = 'SUPSEG';
    
    // Acao do usuario, qualquer retorno a automacao nao prossegue
    if ( btnBarNotEspecClicked(controlBar, glb_btnCtlSup) != null )
        return true;
    
    if ( glb_nPointer == null )
    {
        adjustSupInfControlsBar('SUPCONS', 'U');
        return true;
    }    
    
    lockAndOrSvrSup_SYS();
}

function __btn_UM(controlBar) 
{
    glb_btnCtlSupInf = 'SUPUM';
        
    // Funcao de uso do programador
    btnBarClicked(controlBar, 1);
}

function __btn_DOIS(controlBar) 
{
    glb_btnCtlSupInf = 'SUPDOIS';
    
    // Funcao de uso do programador
    btnBarClicked(controlBar, 2);
}    

function __btn_TRES(controlBar) 
{
    glb_btnCtlSupInf = 'SUPTRES';
    
    // Funcao de uso do programador
    btnBarClicked(controlBar, 3);
}

function __btn_QUATRO(controlBar) 
{
    glb_btnCtlSupInf = 'SUPQUATRO';
    
    // Funcao de uso do programador
    btnBarClicked(controlBar, 4);
}

function __btn_CINCO(controlBar) 
{
	glb_btnCtlSupInf = 'SUPCINCO';

     // Funcao de uso do programador
    btnBarClicked(controlBar, 5);
}

function __btn_SEIS(controlBar) 
{
	glb_btnCtlSupInf = 'SUPSEIS';
	
     // Funcao de uso do programador
    btnBarClicked(controlBar, 6);
}

function __btn_SETE(controlBar) 
{
	glb_btnCtlSupInf = 'SUPSETE';
	
     // Funcao de uso do programador
    btnBarClicked(controlBar, 7);
}

function __btn_OITO(controlBar) 
{
	glb_btnCtlSupInf = 'SUPOITO';
	
     // Funcao de uso do programador
    btnBarClicked(controlBar, 8);
}


function __btn_NOVE(controlBar) {
    glb_btnCtlSupInf = 'SUPNOVE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 9);
}

function __btn_DEZ(controlBar) {
    glb_btnCtlSupInf = 'SUPDEZ';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 10);
}

function __btn_ONZE(controlBar) {
    glb_btnCtlSupInf = 'SUPONZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 11);
}

function __btn_DOZE(controlBar) {
    glb_btnCtlSupInf = 'SUPDOZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 12);
}

function __btn_TREZE(controlBar) {
    glb_btnCtlSupInf = 'SUPTREZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 13);
}

function __btn_QUATORZE(controlBar) {
    glb_btnCtlSupInf = 'SUPQUATORZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 14);
}

function __btn_QUINZE(controlBar) {
    glb_btnCtlSupInf = 'SUPQUINZE';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 15);
}

function __btn_DEZESSEIS(controlBar) {
    glb_btnCtlSupInf = 'SUPDEZESSEIS';

    // Funcao de uso do programador
    btnBarClicked(controlBar, 16);
}

// FINAL DE EVENTOS DO CONTROL BAR SUPERIOR *************************

// FUNCOES DA AUTOMACAO *********************************************

/********************************************************************
Esta funcao inicia o processamento de uma acao do usuario. Trava a
interface e faz as operacoes de servidor do frame work.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function lockAndOrSvrSup_SYS()
{
    // Salva string de botoes da barra
    // para ajustar os botoes especificos da barra se necessario
    var strBtnsBarSup = currentBtnsCtrlBarString('sup');

    // Trava os dois controls bars
    adjustSupInfControlsBar('DISALL');
    
    // reseta grid do inf para estado nada
    sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'resetGridInterface()');
    
    // Campo nulo apenas para simular tab aqui nao resolve a gravacao
    // mas resolve aspectos visuais por travar controles
    if (! txtNulo.disabled) // qdo estiver trocando estado do registro o txtNulo
    {                       // estara desabilitado 
        document.getElementById('txtNulo').maxLength = 0;
        if ( document.getElementById('txtNulo').runtimeStyle.visibility == 'visible' )
            document.getElementById('txtNulo').focus();
    }
    
    // Trava controles do sup e do inf
    lockControls(true, true);
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'LOCKCONTROLS', true);
    
    // Trava interface
    lockInterface(true);

    // Controla se prossegue a automacao nesta funcao
    // ou na funcao finalDSOsCascade()
    var bForwardChangeDiv = true;
    
    // Incluir um registro novo
    if ( glb_btnCtlSup == 'SUPINCL' )
    {
        bForwardChangeDiv = false;

        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Inclus�o', true);
        
        // O evento pode ter ocorrido no pesqlist ou no sup
        if ( glb_PESQLISTMODE != null )
        {
            startInsertFromPesqList();
        }    
        else
        {
            // Adiciona record e segue a automacao
            bForwardChangeDiv = true;
            dsoSup01.recordset.AddNew();
            updateHTMLFields();
        }    
    }    
    // Mostrar detalhes de um registro (vindo do pesqlist)
    // Refrescar detalhes de um registro
    // A sequencia e semelhante a mostrar detalhe vindo do pesqlist,
    else if ( (glb_btnCtlSup == 'SUPDET') || (glb_btnCtlSup == 'SUPREFR') )
    {
        bForwardChangeDiv = false;
 
        if ( glb_btnCtlSup == 'SUPDET' )
            // Escreve no status bar
            writeInStatusBar('child', 'cellMode', 'Detalhe', true);
        else if ( glb_btnCtlSup == 'SUPREFR' )
            // Escreve no status bar
            writeInStatusBar('child', 'cellMode', 'Refresh', true);
           
        // Inicia o carregamento do dsoSup01
        setSqlToDso(dsoSup01,glb_nRegistroID,'dsoSup01_DSC');
    }
    // Paginar significa trocar o registro corrente do sup (os dados) e
    // prosseguir como um refresh, no ponto do refresh apos a troca de dados
    // do sup.
    // Paginar mostrando o registro anterior ou o proximo registro
    else if ( (glb_btnCtlSup == 'SUPANT') || (glb_btnCtlSup == 'SUPSEG') )
    {
        bForwardChangeDiv = false;
        
        // garante voltar o grid inferior para a primeira pagina se em
        // modo de paginacao
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nPAGENUMBER = 1');
        
        // Se for o primeiro registro da listagem do pesqlist
        // ou se este registro acabou de ser incluido
        // vindo do pesqlist para a automacao
        if ( glb_btnCtlSup == 'SUPANT' )
        {
            if ( glb_nPointer <= 0 )
            {
                lockInterface(false);
                
                adjustSupInfControlsBar('SUPCONS', 'P');
                // Ajusta os botoes especificos
                setupEspecBtnsControlBar('sup', strBtnsBarSup.substr(strBtnsBarSup.length - 4));

                // Automacao prossegue direto para destravar a barra do inf
                // e informa que o usuario tenta paginar antes do primeiro registro
                sendJSMessage(getHtmlId(), JS_INFSYS, 'CFGINFCTLBAR', glb_btnCtlSup + '_FIRST');
                return false;
            }
            else
            {
                // Escreve no status bar
                writeInStatusBar('child', 'cellMode', 'Anterior', true);
                previousRegister();
            }    
        }
        
        // Se for o ultimo registro da listagem do pesqlist
        // ou se este registro acabou de ser incluido
        // vindo do pesqlist para a automacao
        if ( glb_btnCtlSup == 'SUPSEG' )
        {
            if (glb_nPointer == (glb_aListaIDs.length-1))
            {
                lockInterface(false);
                
                adjustSupInfControlsBar('SUPCONS', 'U');
                // Ajusta os botoes especificos
                setupEspecBtnsControlBar('sup', strBtnsBarSup.substr(strBtnsBarSup.length - 4));
                
                // Automacao prossegue direto para destravar a barra do inf
                // e informa que o usuario tenta paginar alem do ultimo registro
                sendJSMessage(getHtmlId(), JS_INFSYS, 'CFGINFCTLBAR', glb_btnCtlSup + '_LAST');
                return false;
            }
            else
            {
                // Escreve no status bar
                writeInStatusBar('child', 'cellMode', 'Seguinte', true);
                nextRegister();
            }    
        }
    }
    // Cancelamento de inclusao ou edicao
    else if ( glb_btnCtlSup == 'SUPCANC' )
    {
        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Cancelando', true);
    
        bForwardChangeDiv = false;
                
        // Para a automacao do frame work e
        // retorna para o pesqlist se foi uma inclusao iniciada la
        var retToPesqList = cancelIncludeEdit();
        
        if ( retToPesqList )
        {
            if ( glb_PESQLISTMODE == 'FROMPESQ' )
                __btn_PESQ('sup');
            else if ( glb_PESQLISTMODE == 'FROMLIST' )
                __btn_LIST('sup');

            return null;
        }

        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Cancelando', true);
        
        // Prossegue como um refresh
        glb_btnCtlSup = 'SUPREFR';   
        // Inicia o carregamento do dsoSup01
        setSqlToDso(dsoSup01, glb_nRegistroID, 'dsoSup01_DSC');        
    }

    // Avisa sup para comecar operacoes de servidor do programador    
    if ( bForwardChangeDiv )
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'PROGSERVER', glb_btnCtlSup);
}

/********************************************************************
Esta funcao inicia a colocacao do detalhe em modo de inclusao,
vindo do pesqlist.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startInsertFromPesqList()
{
    setup_dsoSup01FromPesq(dsoSup01);
    dsoSup01.ondatasetcomplete = dsoSup01FromPesqList_DSC;
    dsoSup01.Refresh();
}

/********************************************************************
Esta funcao e o retorno do servidor da chamada feita na funcao 
startInsertFromPesq().
Esconde o pesqlist e mostra o sup01, o controlbar inf e o inf01.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoSup01FromPesqList_DSC()
{
    // Limita a quantidade de caracteres que podem ser digitados
    // nos campos do sup
    fldMaxLen(dsoSup01);

    // Adiciona novo record no dsoSup01
    dsoSup01.recordset.AddNew();

    updateHTMLFields();
    
    finalDSOsCascade();
}

/********************************************************************
Funcao executada por todo final de cascatas de DSOs, ao final
avisa o sup para fazer as operacoes de servidor do programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalDSOsCascade()
{
    // Configura campos de digitacao
    // e arrays de critica da gravacao
    if ( dsoSup01.getAttribute('setControls', 1) == false )
    {
        dsoSup01.setAttribute('setControls',
                               maxLenOnKeyPressSetArrays(dsoSup01), 1);
        // if abaixo introduzido em 20/11/2001
        // se nao foi implementado implementa
        if ( glb_implementsPasteInTxtFld == false )
        {
            __implementsPasteInTxtFld();
            glb_implementsPasteInTxtFld = true;
        }
    }
        
    // Vindo do pesqlist para inclusao de um novo registro. Faz a troca
    // de divs do sup e nao prossegue na funcao
    if ( glb_PESQLISTMODE != null && (glb_btnCtlSup == 'SUPINCL') )
    {
        doChangeDivSup_SYS(glb_btnCtlSup);
        updateHTMLFields();
        sendJSMessage(getHtmlId(), JS_SUPSYS, 'PROGSERVER', glb_btnCtlSup);
        return null;
    }
    
    // Se vindo da lista para o detalhe ou paginando,
    // ajusta a barra superior
    if ( (glb_btnCtlSup == 'SUPDET') || (glb_btnCtlSup == 'SUPREFR') ||
         (glb_btnCtlSup == 'SUPANT') || (glb_btnCtlSup == 'SUPSEG') )
         adjustControlBarSupByRights(dsoSup01.recordset['Prop1'].value,
                                     dsoSup01.recordset['Prop2'].value);
    
    // Se paginando prossegue como um refresh
    if ( (glb_btnCtlSup == 'SUPANT') || (glb_btnCtlSup == 'SUPSEG') )
    {
        glb_btnCtlSup = 'SUPREFR';
    }
    
    updateHTMLFields();
    
    // Avisa sup para comecar as operacoes de servidor do programador
    sendJSMessage(getHtmlId(), JS_SUPSYS, 'PROGSERVER', glb_btnCtlSup);
}

/********************************************************************
Mensagem enviada do inf, informa que todas as operacoes de bancos
de dados acabaram. Fazer as trocas de divs da automacao aqui.
           
Parametros: 
btnClicked      - barra (superior e inferior) e botoes clicados

Retorno:
nenhum
********************************************************************/    
function doChangeDivSup_SYS(btnClicked)
{
    // ID do div superior a mostrar
    var nSFSGrupoToShow = -1;
    
    // Destrava interface
    // Alteracao 16/11/2001
    if ( !((glb_btnCtlSup == 'SUPANT') ||
           (glb_btnCtlSup == 'SUPSEG')) )
        lockInterface(false);

    // Retrava o combo de empresa se o pesqlist esta visivel
    var fPesqListVisible = ascan(htmlIdsVisibles(window),'pesqlistHtml',true) >= 0;
    if ( fPesqListVisible )
        sendJSMessage('AnyHtml',JS_STBARGEN, 'CMBSTATUS', true);
    // Destrava o combo de forms
    sendJSMessage('AnyHtml',JS_STBARGEN, 'CMBSTATUS_FORMS', false);    

    // Se veio do pesqlist para incluir um registro faz as operacoes
    // abaixo e a automacao segue direto para a troca de divs do inf
    if ( (glb_btnCtlSup == 'SUPINCL') )
    {
        // Campos entram em modo de edi��o
        lockControls(false, true);
        
        nSFSGrupoToShow = -1;
        
        // Ajusta o controle que mostra o registro corrente
        showCurrRegistroID();
        
        // Esconde todos os divs secundarios
        showDivBySFS(glb_aDIVS, nSFSGrupoToShow);
        
        // Se a inclusao veio do pesqlist
        if ( glb_PESQLISTMODE != null )
        {
            // Automacao prossegue direto para a troca de divs do inf
            // sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);    
            // A operacao acima substituida pela abaixo em 12/04/2001    
            sendJSMessage(getHtmlId(), JS_SUPSYS, 'AFTERCHANGEDIV', btnClicked);    
        
            return null;
        }
    }
    
    // Mostra o div secundario coerente
    // Modo refresh ou paginacao
    if ( (glb_btnCtlSup == 'SUPDET') ||
         (glb_btnCtlSup == 'SUPREFR') ||
         (glb_btnCtlSup == 'SUPANT') ||
         (glb_btnCtlSup == 'SUPSEG') )
    {
        nSFSGrupoToShow = sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SFS', 'DSOVALUE');
       
        // Esconde todos os divs secundarios e mostra o div secundario coerente
        showDivBySFS(glb_aDIVS, nSFSGrupoToShow);
    }
    
    // Mostra o ID do registro corrente    
    showCurrRegistroID();

    // mostra a interface se vem do pesqlist
    if ( glb_btnCtlSup == 'SUPDET' )
    {
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                      'showDetailInterface()');
                      
        if (glb_DetSupTimerVar == null)
            glb_DetSupTimerVar = window.setInterval('GoAheadComingFromPesq()', 10, 'JavaScript');
                      
        return null;              
    }

    // Prossegue a automacao
    // Informa o sup que trocou os divs para que o mesmo
    // faca as suas operacoes particulares de interface de sup
    sendJSMessage(getHtmlId(), JS_SUPSYS, 'AFTERCHANGEDIV', btnClicked);    
}

/********************************************************************
Mostra a interface do detalhe, vindo do pesqlist

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function GoAheadComingFromPesq()
{
    if ( glb_DetSupTimerVar != null )
    {
        window.clearInterval(glb_DetSupTimerVar);
        glb_DetSupTimerVar = null;
    }
    
    // Prossegue a automacao
    // Informa o sup que trocou os divs para que o mesmo
    // faca as suas operacoes particulares de interface de sup
    sendJSMessage(getHtmlId(), JS_SUPSYS, 'AFTERCHANGEDIV', glb_btnCtlSup);    
}


/********************************************************************
Vinda do pesqlist pela messagem JS_DATAINFORM param1 == 'SHOWDETAIL'
Chamada pelo wndproc do sup, esta funcao da a partida no carregamento
dos dados para mostrar os detalhes de um registro.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function startDataFromList()
{
    if ( glb_DetSupTimerVar != null )
    {
        window.clearInterval(glb_DetSupTimerVar);
        glb_DetSupTimerVar = null;
    }
    
    glb_btnCtlSup = 'SUPDET';
    lockAndOrSvrSup_SYS();    
}

/********************************************************************
Descricao: Funcao de retorno do servidor da funcao setSqlToDso

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoSup01_DSC() {
    // Se nenhum registro voltou do servidor, para a automacao
    if ((dsoSup01.recordset.BOF) && (dsoSup01.recordset.EOF))
    {
        if ( window.top.overflyGen.Alert('Registro n�o localizado.') == 0 )
            return null;
        
        // Destrava a interface
        lockInterface(false);
        
        // Volta para o pesqlist no ultimo modo ativo
        // param2 = 0 e o registroID do registro corrente
        if ( (glb_nPointer == null) || glb_aListaIDs == null )
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWPESQLISTFROMDET',
                          null);
        else
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWPESQLISTFROMDET',
                          glb_aListaIDs[glb_nPointer]);
        return null;
    }
    
    getStateMachineAbrev('dsoStateMachine_DSC');
}

/********************************************************************
Funcao de retorno do servidor da funcao dsoSup01_DSC.
Tras o RecursoAbreviado do registro corrente no dsoSup01
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoStateMachine_DSC()
{
	// Se nenhum registro voltou do servidor, para a automacao
    if ((dsoSup01.recordset.BOF) && (dsoSup01.recordset.EOF))
    {
        if ( window.top.overflyGen.Alert('Registro n�o localizado.') == 0 )
            return null;
        
        // Destrava a interface
        lockInterface(false);
        
        // Volta para o pesqlist no ultimo modo ativo
        // param2 = 0 e o registroID do registro corrente
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWPESQLISTFROMDET',
                      glb_aListaIDs[glb_nPointer]);
        return null;
    }

    // Limita quantidade de caracteres que podem ser digitados
    // nos controles
    if ( glb_fldMaxLen__ == false )
    {
        glb_fldMaxLen__ = true;
        fldMaxLen(dsoSup01);
    }
    
    finalDSOsCascade();
}

/********************************************************************
Obtem dados no servidor do registro anterior. Paginando em modo detalhe

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function previousRegister()
{
	if (!isNaN(glb_aListaIDs[glb_nPointer-1]))
		glb_nPointer--;
	
	movePointerRDS(glb_aListaIDs[glb_nPointer]);
    return true;
}

/********************************************************************
Obtem dados no servidor do proximo registro. Paginando em modo detalhe

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function nextRegister()
{
    glb_nPointer++;
    movePointerRDS(glb_aListaIDs[glb_nPointer]);
    return true;
}

/********************************************************************
Descricao: Move o ponteiro de registro do dsoSup01, e chamada pela
           previousRegister() e pela nextRegister()

Parametros: 
nID     - O ID do Proximo/Anterior Registro 

Retorno:
nenhum
********************************************************************/
function movePointerRDS(nID)
{
    setSqlToDso(dsoSup01,nID,'dsoSup01MovePointer_DSC');
}

/********************************************************************
Funcao de retorno do servidor da funcao movePointerRDS()
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoSup01MovePointer_DSC()
{
    // O registro nao foi localizado
    if ((dsoSup01.recordset.EOF) || (dsoSup01.recordset.BOF))
    {
        if ( window.top.overflyGen.Alert('Registro n�o localizado.') == 0 )
            return null;
        lockInterface(false);        
        __btn_LIST('sup');
        return null;
    }
    
    // Salva o id do registro corrente
    glb_nRegistroID = dsoSup01.recordset[glb_sFldIDName].value;
    
    getStateMachineAbrev('dsoStateMachine_DSC');
}

/********************************************************************
Funcao que que cancela a inclusao ou alteracao do registro

Parametros: 
nenhum

Retorno:
true se cancela operacao iniciada no pesq list, caso contrario
false
********************************************************************/
function cancelIncludeEdit()
{
    dsoSup01.CancelUpdate();
    
    if ( (glb_PESQLISTMODE == 'FROMLIST') || (glb_PESQLISTMODE == 'FROMPESQ') )
        return true;
    
    return false;
}

/********************************************************************
Esta funcao e disparada quando o sup esta em inclusao/alteracao
e o usuario esta manipulando combos. Efetua troca de divs do sup.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function adjustSupInterface()
{
    var nSFSGrupoToShow = -1;
    
    // Mostra o div secundario coerente
    // Verifica se o sup esta em modo de consulta ou de inclusao/edicao
    if ( (glb_btnCtlSup == 'SUPINCL') ||    // entrou em modo de inclusao
         ( glb_btnCtlSup == 'SUPALT') )     // entrou em modo de edicao
    {
        nSFSGrupoToShow = sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SFS', 'SELVALUE');
    }
       
    // Esconde todos os divs secundarios e mostra o div secundario coerente
    showDivBySFS(glb_aDIVS, nSFSGrupoToShow);
}    

/********************************************************************
Trata o campo registroID:
Os tipos de geracao do id do registro possiveis (so para inclusao,
alteracao o usuario nao pode alterar o id do registro):
15 - Banco gera o id do registro (identity  )
16 - A trigger gera o id do registro (identity)
17 - O usuario define o id do registro

window.top.registroID_Type = valores acima

Esta funcao e invocada pela funcao treats_FldRegID_Inf do Inf
           
Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function treats_FldRegID_Sup()
{
	var actionType = window.top.registroID_Type;
	
	if ( (actionType == 15) || (actionType == 16) )
		return null;
		
	if (txtRegistroID == null)
		return null;
		
	txtRegistroID.disabled = false;
	txtRegistroID.readOnly = false;
	
	// Nome da tabela
	var tblName = '';
	var fldlen = 0;
	
	try
	{
		//tblName = dsoSup01.recordset.Fields[glb_sFldIDName].properties(1).value;
		tblName = dsoSup01.recordset[glb_sFldIDName].table;
		fldlen = sendJSMessage('', JS_FIELDINTLEN, tblName, glb_sFldIDName);
		fldlen = fldlen - 1;
	}
	catch(e)
	{
		fldlen = 0;
	}
	
	txtRegistroID.maxLength = fldlen;
	
	txtRegistroID.setAttribute('thePrecision', fldlen, 1);
    txtRegistroID.setAttribute('theScale', 0, 1);
    txtRegistroID.setAttribute('verifyNumPaste', 1);
    txtRegistroID.setAttribute('minMax', new Array(0, replicate('9', fldlen)), 1);
    txtRegistroID.onfocus = selFieldContent;

	txtRegistroID.onkeypress = verifyNumericEnterNotLinked;	
	
	txtRegistroID.style.visibility = 'visible';
	
	if (glb_treats_FldRegID_Sup_Timer != null)
	    glb_treats_FldRegID_Sup_Timer = window.setInterval('treats_FldRegID_Sup_Timer()', 100, 'JavaScript');
}

function treats_FldRegID_Sup_Timer()
{
	if ( glb_treats_FldRegID_Sup_Timer != null )
	{
		window.clearInterval(glb_treats_FldRegID_Sup_Timer);
		glb_treats_FldRegID_Sup_Timer = null;
	}
	
	if (txtRegistroID == null)
		return null;
	
	window.focus();
	txtRegistroID.focus();
}

/********************************************************************
Verifica o campo registroID:
Os tipos de geracao do id do registro possiveis (so para inclusao,
alteracao o usuario nao pode alterar o id do registro):
15 - Banco gera o id do registro (identity  )
16 - A trigger gera o id do registro (identity)
17 - O usuario define o id do registro

window.top.registroID_Type = valores acima

Esta funcao e invocada pela funcao treats_FldRegID_Inf do Inf
           
Parametros:
nenhum

Retorno:
true ou false
********************************************************************/
function verify_FldRegID_Sup()
{
	var actionType = window.top.registroID_Type;

	//Altera��o para for�ar ID calculado pelo sistema, caso o ID informado pelo Usuario seja igual a 0. BJBN 22/11/2012
	if ((actionType == 17) && (txtRegistroID.value == 0)) 
	{
	    actionType = 16;
	    window.top.registroID_Type = 16;
	}	    
	
	glb_regIDEspecificVal = null;
	
	if ( (actionType == 15) || (actionType == 16) )
	{
		if(actionType == 16)
			glb_regIDEspecificVal = -1;
			
		return true;
	}	
		
	if (txtRegistroID == null)
		return true;
		
	if ( (txtRegistroID.value == null) || 
	     (txtRegistroID.value == '') )	
	{
		if ( window.top.overflyGen.Alert('O campo ' + labelAssociate(txtRegistroID.id) + ' n�o foi preenchido...') == 0 )
                    return null;

		// Destrava controles do sup os do inf ja estavam travados
        // e assim continuam
        lockControls(false, true);
                
        // Destrava os controls bars
        unlockControlBar('sup');
        unlockControlBar('inf');
        
        window.focus();
        txtRegistroID.focus();

		return false;
	}
	
	glb_regIDEspecificVal = parseInt(txtRegistroID.value, 10 );
	
	return true;     
}

/********************************************************************
O programador interceptou a gravacao da maquina de estado e nao vai
prossegui-la

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function doNotSaveNewStateSup()
{
	glb_sMotivoSup = '';
}

/********************************************************************
Funcao que atualiza o campo txtRegistroID na tela
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function showCurrRegistroID()
{
    if ((dsoSup01.recordset.EOF) || (dsoSup01.recordset.BOF))
        return;
    
    if(dsoSup01.recordset[glb_sFldIDName].value == null)
        txtRegistroID.value = ''; 
    else
        txtRegistroID.value = dsoSup01.recordset[glb_sFldIDName].value;
}
// FINAL DAS FUNCOES DA AUTOMACAO ***********************************

// FUNCOES GERAIS ***************************************************
function window_OnUnload()
{
	dealWithObjects_Unload();
}

/********************************************************************
Executa a primeira parte do window_onload

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function windowOnLoad_1stPart()
{
	document.body.onunload = window_OnUnload;
	dealWithObjects_Load();
	dealWithGrid_Load();
	
    // O titulo do form no browser filho
    window.top.setTitleWithFormName();

    // Salva o id deste html (a mesma operacao e feita no pesqlist, no sup01
    // e no inf01)
    window.top.sup01ID = getHtmlId();
    
    // Seta o InternetTimeOut dos componentes RDS
    dsoFixedParams();
   
    // Garante preenchimento do dso de maquina de estado
    dsoStateMachLoadDoNothing();
    
    // Garante nenhum botao travado nas barras
    unlockBtnsInCtrlBar('sup');
    unlockBtnsInCtrlBar('inf');
        
    // Previne controls bars mostrarem botoes especificos na carga
    showBtnsEspecControlBar('sup', false, [1,1,1,1]);
    showBtnsEspecControlBar('inf', false, [1,1,1,1]);
    
    // Previne controls bars com botao especifico com imagem de print
    especBtnIsPrintBtn('sup', 0);
    especBtnIsPrintBtn('inf', 0);
    
    // Inicia carregamento dos combos estaticos do arquivo se o form tem
    // combos desta natureza (se glb_aStaticCombos != null)
    getStaticCmbsData(glb_aStaticCombos, 'SUP');
    
    // Attributo que controla se o dso foi inicializado
    // com recordset e setou os controles de html
    dsoSup01.setAttribute('setControls', false, 1);
}

/********************************************************************
Executa a segunda parte do window_onload

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/ 
function windowOnLoad_2ndPart()
{
    // Chama para carregamento os arquivos inf01 e pesqlist
    var aFrame =  new Array('frameInf01', 'frameSup02');
    
    // Carrega o pesqlist e o inf
    loadFilesInfAndPesqList(window.top.formName, getHtmlId(),
                                window.top.name,
                                aFrame, glb_aFilePath);
        
    // Coloca atributos especiais em controles.
    // Funcao particular programador. Esta no xxx_sup01.js
    putSpecialAttributesInControls();
    
    // configura o body do html
    with (getBodyRef())
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // Configura elementos do arquivo.
    // Funcao particular programador. Esta no xxx_sup01.asp
    setupPage();
    
    // Ajusta campos comuns a todos os forms (txtNulo, txtRegistroID e txtEstadoID)
    adjustSupInfCommonFields();
    
    // Garante selecao de texto nos campos input text
    setupEventOnFocusInTextFields();
    
    // Garante disparo funcao optChangedInCmb(this) no sup para combos
    setupEventOnChangeInSelects();
    
    // Garante mudanca de checkboxes ao clicar o label correspondente
    setupEventInvertChkBox();
    
    // Converge click em botao de lupa para a funcao onClickBtnLupa()
    setupEventOnClickInBtnsLupa();
    
    // Previne backspace em selects para impedir retorno
    // a pagina anteriormente carregada
    preventBkspcInSel(window);
    
    // Reajusta divs da interface
    adjustDivsEx_Sup();
    
    // Translada interface pelo dicionario do sistema
    translateInterface(window, null);
    
    // Garante montagem do array de controles para travamento de interface
    lockControls(true,true);
}

/********************************************************************
Recebe o click do usuario em todos os botoes de lupa
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function onClickBtnLupa()
{
    var elem = this;
    
    if ( elem != null )
    {
        if ( elem.id.toUpperCase() == 'BTNFINDPROPRIETARIO' )    
        {
            elem.src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_Find.gif';
            onClickPropAlt(elem);
            return true;
        }
        else if ( elem.id.toUpperCase() == 'BTNFINDALTERNATIVO' )
        {
            elem.src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_Find.gif';
            onClickPropAlt(elem);
            return true;
        }
    }    

    // Se nao e botao proprietario ou alternativo,
    // propaga para o sup do form
    btnLupaClicked(elem);
}

/********************************************************************
Mostra esconde a interface de detalhe e manda mensagem ao pesqlist
para mostrar sua interface e registrar o control bar superior para o
pesqlist.

Parametros: 
btnOrig         - o botao clicado no control bar superior

Retorno:
nenhum
********************************************************************/
function showPesqListInterface(btnClicked)
{
    var frameID;
    
    // Esconde o recursossup01.asp
    frameID = getFrameIdByHtmlId(window.top.sup01ID);
    if (frameID)
        showFrameInHtmlTop(frameID, false);
    
    // Esconde o control bar inferior
    showControlBar('inf', false);
    
    // Esconde o recursosinf01.asp
    frameID = getFrameIdByHtmlId(window.top.inf01ID);
    if (frameID)
        showFrameInHtmlTop(frameID, false);
    
    // Destrava a interface
    lockInterface(false);
    
    if ( btnClicked == 'SUPPESQ' )
    {
        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Pesquisa');    

        // Volta para o pesqlist no ultimo modo ativo
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWPESQFROMDET', null);
    }
    else if ( btnClicked == 'SUPLIST' )
    {        
        // Escreve no status bar
        writeInStatusBar('child', 'cellMode', 'Listagem');

        var sSendID = 0;
    
		if ((dsoSup01.recordset.fields.count > 0) && (!dsoSup01.recordset.BOF) && (!dsoSup01.recordset.EOF))
			sSendID = dsoSup01.recordset[glb_sFldIDName].value;
        else if ( glb_aListaIDs != null )  
        {
            if ((glb_aListaIDs[glb_nPointer] != null) && (glb_aListaIDs[glb_nPointer] >= 0))
                sSendID = glb_aListaIDs[glb_nPointer];
        }         
    
        // Volta para o pesqlist no ultimo modo ativo    
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWPESQLISTFROMDET', sSendID);
    }
}

/********************************************************************
Retorna o registro corrente.
           
Parametros: 
nenhum

Retorno:
um valor ou null se o dso nao tem dados
********************************************************************/
function getCurrRegID()
{
    var ret = null;
    
    if ( dsoSup01.recordset.Fields.Count != 0 )
        ret = dsoSup01.recordset[glb_sFldIDName].value;
        
    return ret;
}

/********************************************************************
Retorna o estadoID do registro corrente.
           
Parametros: 
nenhum

Retorno:
um valor ou null se o dso nao tem dados
********************************************************************/
function getCurrEstadoID()
{
    var ret = null;
    
    if ( dsoSup01.recordset.Fields.Count != 0 )
        ret = dsoSup01.recordset['EstadoID'].value;

    return ret;
}

/********************************************************************
Retorna o tipo do registro do registro corrente.
           
Parametros: 
nenhum

Retorno:
um valor ou null se o dso nao tem dados
********************************************************************/
function getCurrTipoRegID()
{
    var ret = null;
    
    if ( dsoSup01.recordset.Fields.Count != 0 )
        if ( glb_sFldTipoRegistroName != '' )
            ret = dsoSup01.recordset[glb_sFldTipoRegistroName].value;
        
    return ret;
}

/********************************************************************
Atualiza o label dos combos de sujeito e objeto
Esta funcao e usada nos forms de relacoes

Parametro:
dso             - referencia ao dso que deve ser usado
lblSujID        - id do label sujeito
lblObjID        - id do label objeto
cmbTipoRegID    - se informado, obtem o tipoRegistroID do value do combo
                - se null obtem o tipoRegistroID do dso
sujID           - se informado, usa, caso contrario obtem o valor do dsoSup01
objID           - se informado, usa, caso contrario obtem o valor do dsoSup01

Retorno:
nenhum
********************************************************************/
function changeSujObjLabels(dso, lblSujID, lblObjID, cmbTipoRegID, sujID, objID)
{
    // Trap se nao tem tipo de registro e nao esta usando o combo
    // de tipo de registro
    if ( getCurrTipoRegID() == null && (cmbTipoRegID == null) )
    {
        window.document.getElementById(lblSujID).style.width = 7 * FONT_WIDTH;
		window.document.getElementById(lblSujID).innerText = translateTerm('Sujeito', null);    
		window.document.getElementById(lblObjID).style.width = 6 * FONT_WIDTH;
		window.document.getElementById(lblObjID).innerText = translateTerm('Objeto', null);    
		return;
	}
    
    // O dso nao foi preenchido
    if (dso.recordset.Fields.Count == 0)
        return null;
    
    // segundo ver se o dso tem registros
    if (dso.recordset.BOF && dso.recordset.EOF)
        return;
    
    var dsoBmk = null;    
    var tipoRegID = null;
    
    // tem registros, guarda o bookmark se o dso nao esta no BOF nem o EOF    
    if ( !(dso.recordset.BOF || dso.recordset.EOF) )
        dsoBmk = dso.recordset.Bookmark();

    // faremos aqui nossa pesquisa
    dso.recordset.MoveFirst();
    
    // onde obter o tipoRegID
    if ( cmbTipoRegID == null )
        tipoRegID = getCurrTipoRegID();
    else
        tipoRegID = window.document.getElementById(cmbTipoRegID).value;
    
    dso.recordset.setFilter('fldID = ' + tipoRegID + ' AND Indice = ' + '\'' + 1 + '\'');
    
    // verifica se o objeto n�o est� no EOF
    if (!dso.recordset.EOF);
    {  
		var suj;
		var obj;
	
		suj = translateTerm(dso.recordset['NameSuj'].value, null);
		obj = translateTerm(dso.recordset['NameObj'].value, null);

//	    if ( (typeof(dsoSup01.recordset.Fields['SujeitoID'].value)).toUpperCase() != 'UNDEFINED' )
        if ((dsoSup01.recordset['SujeitoID'] != null) && (dsoSup01.recordset['SujeitoID'].value != null))
        {	    
	        if ( sujID == null )
		    	suj += ' ' + dsoSup01.recordset['SujeitoID'].value;
		    else	
		        suj += ' ' + sujID;
        }
        else if ( sujID != null )
            suj += ' ' + sujID;

//	    if ( (typeof(dsoSup01.recordset.Fields['ObjetoID'].value)).toUpperCase() != 'UNDEFINED' )
        if ((dsoSup01.recordset['ObjetoID'] != null) && (dsoSup01.recordset['ObjetoID'].value != null))
	    {
	        if ( objID == null )
			    obj += ' ' + dsoSup01.recordset['ObjetoID'].value;
			else
			    obj += ' ' + objID;
		}
		else if (objID != null)
		    obj += ' ' + objID;

		if ((suj != '') && (obj != ''))
		{
		    window.document.getElementById(lblSujID).style.width = suj.length * FONT_WIDTH;
			window.document.getElementById(lblSujID).innerText = suj;    
			window.document.getElementById(lblObjID).style.width = obj.length * FONT_WIDTH;
			window.document.getElementById(lblObjID).innerText = obj;    
		}
		else
		{
		    window.document.getElementById(lblSujID).style.width = 7 * FONT_WIDTH;
			window.document.getElementById(lblSujID).innerText = 'Sujeito';    
			window.document.getElementById(lblObjID).style.width = 6 * FONT_WIDTH;
			window.document.getElementById(lblObjID).innerText = 'Objeto';    
		}
    
    }    
   
	dso.recordset.setFilter('');
   
    // volta o ponteiro do dso no registro anterior a funcao, se temos
    // Bookmark
    if ( dsoBmk != null )
        dso.recordset.gotoBookmark(dsoBmk);
}

/********************************************************************
Esta funcao preenche o combo estatico do sup01 quando o form
tem tipo de registro que depende do contexto selecionado. 
O combo selTipoRegistroID, tambem e preenchido na funcao
fillStaticCombos(aCmbAndIndex) do js_common.js
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function fillTipRegStaticCombo()
{
    // caso nao tenha combos estaticos
    if ( glb_aStaticCombos == null )
        return null;

    // caso nao tenha combo estatico de tipo de registro
    if ( (glb_sCtlTipoRegistroID == null) || (glb_sCtlTipoRegistroID == '') )
        return null;

    // dso de dados esta vazio
    if (dsoEstaticCmbs.recordset.BOF && dsoCmbsData.recordset.EOF)
        return null;

    var i;
    var aCmbAndIndex = glb_aStaticCombos;
    var oldDataSrc, oldDataFld;
    var cmbRef;
    var nContextoID = '';
    
    // pega o value do contexto atual
    var aItemSelected = getCmbCurrDataInControlBar('sup', 1);
    if ( aItemSelected != null )
    {
        nContextoID = aItemSelected[1];
        nContextoID = '(' + nContextoID + ')';
    }    
    
    for (i=0; i<aCmbAndIndex.length; i++)
    {
        cmbRef = window.document.getElementById(aCmbAndIndex[i][0]);
        
        if ( cmbRef.id != glb_sCtlTipoRegistroID )
            continue;
        
        oldDataSrc = cmbRef.dataSrc;
        oldDataFld = cmbRef.dataFld;
        cmbRef.dataSrc = '';
        cmbRef.dataFld = '';
        clearComboEx([cmbRef.id]);
        
        dsoEstaticCmbs.recordset.MoveFirst();
        dsoEstaticCmbs.recordset.Find('Indice', aCmbAndIndex[i][1]);
        
        if (!dsoEstaticCmbs.recordset.EOF) 
        {
            while ((!dsoEstaticCmbs.recordset.EOF) && (dsoEstaticCmbs.recordset['Indice'].value == aCmbAndIndex[i][1]))
            {
                if ( dsoEstaticCmbs.recordset['Filtro'].value == null )
                {
                    var oOption = document.createElement("OPTION");
                    oOption.text = dsoEstaticCmbs.recordset['fldName'].value;
                    oOption.value = dsoEstaticCmbs.recordset['fldID'].value;
                    cmbRef.add(oOption);
                }
                else if ( dsoEstaticCmbs.recordset['Filtro'].value == ' ' )
                {
                    var oOption = document.createElement("OPTION");
                    oOption.text = dsoEstaticCmbs.recordset['fldName'].value;
                    oOption.value = dsoEstaticCmbs.recordset['fldID'].value;
                    cmbRef.add(oOption);
                }
                else if ( (dsoEstaticCmbs.recordset['Filtro'].value).indexOf(nContextoID) >= 0 )
                {
                    var oOption = document.createElement("OPTION");
                    oOption.text = dsoEstaticCmbs.recordset['fldName'].value;
                    oOption.value = dsoEstaticCmbs.recordset['fldID'].value;
                    cmbRef.add(oOption);
                }
                
                dsoEstaticCmbs.recordset.MoveNext();
            }
        }
        cmbRef.dataSrc = oldDataSrc;
        cmbRef.dataFld = oldDataFld;
    }

    return null;
}

// FINAL DE FUNCOES GERAIS ******************************************

// FUNCOES DA ALTERACAO *********************************************

/********************************************************************
Esta funcao e disparada quando o sup entra em modo de alteracao
Manda mensagem ao inf para voltar o combo de pastas para a pasta default,
e esconde todas as pastas.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function editRegister()
{
    // campos entram em modo de edi��o
    lockControls(false, true);
    
    // dispara evento em form para tratamento grafico enable/disable
    // ou mostra/esconde controles
    supInitEditMode();
}    
 
/********************************************************************
Esta funcao e disparada do form, apos o mesmo terminar
eventuais operacoes de servidor, quando o sup entra em modo de alteracao.
Esta funcao foi extraida da editRegister() e e a sua parte final.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/ 
function supInitEditMode_Continue()
{
    // da direito ao usuario setar botoeira na funcao apropriada
    // que fica no inf    
    sendJSMessage(getHtmlId(), JS_INFSYS, 'TREATCTLBARS', glb_btnCtlSup);
        
    // setup o formbar, s� libera o Ok e o cancelar
    adjustSupInfControlsBar('SUPINCL');
    
    // escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Altera��o');
    
    // foca o primeiro campo habilitado
    setFocus1Field();
}

// FINAL DE FUNCOES DA ALTERACAO ************************************

// FUNCOES DA GRAVACAO **********************************************

/********************************************************************
Critica os campos do registro e salva a inclusao ou alteracao
no servidor.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveRegister_1stPart()
{
    var sFieldRequired = null;

    // Campo nulo apenas para simular tab e resolver a gravacao
    if (! txtNulo.disabled) // qdo estiver trocando estado do registro o txtNulo
    {                       // estara desabilitado 
        document.getElementById('txtNulo').maxLength = 0;
        document.getElementById('txtNulo').focus();
    }

    // Trava os controls bars
    lockControlBar('sup');
    lockControlBar('inf');
    
    // Trava controles do sup os do inf ja estavam travados
    lockControls(true, true);
    
    if ( (glb_btnCtlSup != 'SUPEST' ) )
    {
        // Trima os campos e verifica campos obrigatorios
        trimAllImputFields(window);
        updateRecordFromHTMLFields();
        
        // NULL Campos IDS: substitui valor 0 por null
        sFieldRequired = changeZeroByNull(dsoSup01);
        
        if (sFieldRequired != null)
        {
            if ( window.top.overflyGen.Alert('O campo ' + sFieldRequired + ' n�o foi preenchido...') == 0 )
                return null;
                
            // Destrava controles do sup os do inf ja estavam travados
            // e assim continuam
            lockControls(false, true);
                
            // Destrava os controls bars
            unlockControlBar('sup');
            unlockControlBar('inf');
                
            return null;
        }        
        
        // Verifica checkboxes e coloca false nos campos do dso
        // se o checkbox nao estiver checado
        if (__glb_applyCheckBoxDefaultValue)
			adjustFldInTableByChkBoxUnchecked(window, dsoSup01);

        // Array de ids dos campos obrigatorios
        var aRequiredFieldsIDs = new Array();
        // Array de ids dos campos de datas
        var aDateFieldsIDs = new Array();

        aRequiredFieldsIDs = dsoSup01.getAttribute('aRequiredFieldsIDs', 1);
        aDateFieldsIDs = dsoSup01.getAttribute('aDateFieldsIDs', 1);

        // Percorre o array de campos obrigatorios e faz a critica
        // Isto nao e feito se gravando vindo da maquina de estado
        var i=0;
        var oFieldRequired;
        var oFieldDate, oLabelDate;
        
        // Trata campo txtRegistroID editavel
        if ( !verify_FldRegID_Sup() )
			return null;
    
        for( i=0; i<aRequiredFieldsIDs.length; i++ )
        {
            // valida campos obrigatorios
            oFieldRequired = document.getElementById(aRequiredFieldsIDs[i]);
            if (oFieldRequired.value.length == 0)  
            {
                if ( window.top.overflyGen.Alert('O campo ' + labelAssociate(aRequiredFieldsIDs[i]) + ' n�o foi preenchido...') == 0 )
                    return null;
                
                // Destrava controles do sup os do inf ja estavam travados
                // e assim continuam
                lockControls(false, true);
                
                // Destrava os controls bars
                unlockControlBar('sup');
                unlockControlBar('inf');
                
                if ( !oFieldRequired.disabled )
                    oFieldRequired.focus();
                    
                return null;
            }
        }

        // valida campo data
        for( i=0; i<aDateFieldsIDs.length; i++ )
        {
            oFieldDate = document.getElementById(aDateFieldsIDs[i]);

            // A funcao normalizeDateFormat transforma separadores
            // como ponto e traco para barra
            if ( oFieldDate.value != '' )
				oFieldDate.value = normalizeDateFormat(oFieldDate.value);
            
            // A funcao chkDataEx valida o campo se ele e uma data valida
            if (chkDataEx(oFieldDate.value) == false)
            {
                if ( window.top.overflyGen.Alert('O campo ' + labelAssociate(aDateFieldsIDs[i]) + ' n�o � v�lido...') == 0 )
                    return null;

                // Destrava controles do sup os do inf ja estavam travados
                lockControls(false, true);
                
                // Destrava os controls bars
                unlockControlBar('sup');
                unlockControlBar('inf');
                
                if ( oFieldDate.disabled == false )      
                    oFieldDate.focus();
                    
                return null;
            }
            
            // Nome do campo de data nao virtual
            if ( oFieldDate.parentElement.currentStyle.visibility == 'visible' )   
            {    
                sFieldInTableName =  oFieldDate.dataFld.substr(2);
                
                // verifica se o campo data que deu origem ao campo VARCHAR
                // e obrigatorio
                if ( ~dsoSup01.recordset[sFieldInTableName].Attributes & 64 )
                {
                    if ( oFieldDate.value == null || oFieldDate.value == '' )
                    {
                        if ( window.top.overflyGen.Alert('O campo ' + labelAssociate(aDateFieldsIDs[i]) + ' n�o foi preenchido...') == 0 )
                            return null;

                        // Destrava controles do sup os do inf ja estavam travados
                        lockControls(false, true);
                
                        // Destrava os controls bars
                        unlockControlBar('sup');
                        unlockControlBar('inf');
                
                        if ( oFieldDate.disabled == false )      
                            oFieldDate.focus();
                        return null;
                    }
                }
                            
                // se a data for valida grava o valor do campo HTML no campo do DSO
                if (oFieldDate.value == '')
                    dsoSup01.recordset[sFieldInTableName].value = null;
                else
                    dsoSup01.recordset[sFieldInTableName].value = oFieldDate.value;
            }    
        }
    }
        
    glb_PESQLISTMODE = null; // zera a variavel que controla o modo
    
    // Trava a interface
    lockInterface(true);
            
    // Escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Gravando',true);
        
    // Tenta gravar o registro no banco
    saveRegister_2ndPart();
}

/********************************************************************
Salva a inclusao ou alteracao no servidor

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveRegister_2ndPart()
{
    var isNewRecord = false;
    var i;
    
    // LOG - sempre altera o UsuarioID
    if ( LOG_ACT_FLD != null )
        if ( fieldExists(dsoSup01, LOG_ACT_FLD) )
            dsoSup01.recordset[LOG_ACT_FLD].value = glb_USERID;
    
     // Um novo registro (nao e alteracao)
    if (dsoSup01.recordset[glb_sFldIDName].value == null)
    {
        // ID de registro calculado por procedure no banco de dados
        // o digitado pelo usuario:
        // window.top.registroID_Type != 15
        if ( glb_regIDEspecificVal != null )
            dsoSup01.recordset[glb_sFldIDName].value = glb_regIDEspecificVal;
        
        // Coloca o usuario logado como proprietario e alternativo
        // do registro    
        dsoSup01.recordset['ProprietarioID'].value = glb_USERID;
        dsoSup01.recordset['AlternativoID'].value = glb_USERID;
        dsoSup01.recordset['EstadoID'].value = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getInitialState(' + '\'' + 'SUP' + '\'' + ')');
        isNewRecord = true;
    }
    else
    {
        for ( i=0; i<dsoSup01.recordset.Fields.Count; i++ )
        {
			if ( dsoSup01.recordset.Fields[i].name == 'EstadoID' )
            {
                if ( glb_sMotivoSup != '' )
					dsoSup01.recordset['LOGMotivo'].value = glb_sMotivoSup;

                glb_sMotivoSup = '';

                if ( (glb_btnCtlSup != 'SUPEST' ) )
					dsoSup01.recordset['EstadoID'].value = dsoSup01.recordset['EstadoID'].value;

                break;
            }
        }
    }
    
    dsoSup01.setAttribute('isNewRecord', isNewRecord, 1);
    
    try
    {
        glb_EstadoIDUpdated = true;
        
        dsoSup01.ondatasetcomplete = dsoSup01WriteComplete_DSC;
        dsoSup01.SubmitChanges();   
    }
    catch(e)
    {
        glb_EstadoIDUpdated = false;
        
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        // ou operacao de gravacao abortada no banco
        if (e.number == -2147217887)
            if (window.top.overflyGen.Alert(e.name + ' - ' + e.message) == 0)
                return null;
            // if (window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0)
    }    
}

function dsoSup01WriteComplete_DSC() {
	var isNewRecord = dsoSup01.getAttribute('isNewRecord', 1);
	
	dsoSup01.setAttribute('isNewRecord', 0, 1);

	// ID do registro definido pelo usuario
	if ( (window.top.registroID_Type == 17) && (isNewRecord) )
		setSqlToDso(dsoSup01,glb_regIDEspecificVal,'dsoSup01_SetSqlToDso_DSC');
	else if ( (window.top.registroID_Type != 17) && (isNewRecord) ) // se � novo registro
		setSqlToDso(dsoSup01,glb_USERID,'dsoSup01_SetSqlToDso_DSC',true);
	else
		setSqlToDso(dsoSup01,glb_nRegistroID,'dsoSup01_SetSqlToDso_DSC');

	// Destrava a interface
	lockInterface(false);
}

/********************************************************************
Funcao de retorno do servidor da funcao saveRegister_2ndPart()

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoSup01Insert_DSC()
{
    // Salva o id do registro corrente
    glb_nRegistroID = dsoSup01.recordset[glb_sFldIDName].value;
    
    getStateMachineAbrev('dsoStateMachineInsert_DSC');
}

function dsoSup01_SetSqlToDso_DSC()
{
    // Salva o id do registro corrente
    glb_nRegistroID = dsoSup01.recordset[glb_sFldIDName].value;
    
    getStateMachineAbrev('dsoStateMachineInsert_DSC');
}

/********************************************************************
Funcao de retorno do servidor da funcao dsoSup01Insert_DSC(),
traz a descricao do EstadoID do registro corrente.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoStateMachineInsert_DSC() {
    // esta funcao so executa se a maquina de estado estiver visivel
    if ( restoreInterfaceFromModal() )
    {
        if ( glb_EstadoIDBeforeStateMach != getCurrEstadoID() )    
            // alerta programador que a maquina de estado fechou
            // apenas se o estado do registro foi alterado
            stateMachClosed( glb_EstadoIDBeforeStateMach, getCurrEstadoID(), glb_EstadoIDUpdated );
    }
    // Implementado em 08/04/07 para atender situacao especifica do pedido
    // executa sempre indenpendente da maquina de estado estar visivel
    // nem todos os forms tem esta chamada
    if ( (glb_EstadoIDBeforeStateMach!=0) && ( glb_EstadoIDBeforeStateMach != getCurrEstadoID() ))
    {
		try
		{
			stateMachClosedEx( glb_EstadoIDBeforeStateMach, getCurrEstadoID(), glb_EstadoIDUpdated );
		}
		catch(e)
		{
			;
		}
		
		glb_EstadoIDBeforeStateMach = 0;
    }
    
    updateHTMLFields();//marco

    if ((! dsoSup01.recordset.EOF) && (! dsoSup01.recordset.BOF))
    {
        // Trata interface do sup
        finalOfSave();
    }
    else // se o registro foi deletado por outro usuario, volta p/ lista
        dsoSup01Delete_DSC();
}

/********************************************************************
Funcao final de salvamento. Ajusta interface e controls bars.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSave()
{
    // Ajusta o controle que mostra o registro corrente
    showCurrRegistroID();
    
    // Destrava a interface
    lockInterface(false);

    // Destrava os controls bars
    unlockControlBar('sup');
    unlockControlBar('inf');
    
    // Ajusta a barra superior por direitos do usuario
    adjustControlBarSupByRights(dsoSup01.recordset['Prop1'].value,
                                     dsoSup01.recordset['Prop2'].value);
    
    // Ajusta o control bar superior
    adjustSupInfControlsBar('SUPCONS');
                                                  
    // Mostra a pasta default do registro no inf e configura
    // o control bar inferior
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'SHOWCOMPATIBLEPASTA', glb_btnCtlSup);
    
    writeInStatusBar('child', 'cellMode', 'Detalhe');
}

// FINAL DE FUNCOES DA GRAVACAO *************************************

// FUNCOES DA EXCLUSAO **********************************************

function excludeRegister_1stPart()
{
    // Escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Exclus�o');

    // Alert e confirm do overflyGen.dll
    // Icones possiveis
    // IDI_ERROR, IDI_QUESTION , IDI_EXCLAMATION ,IDI_INFORMATION
    // var _retMsg_ = window.top.overflyGen.Alert('Texto do alert');
    // var _ retMsg_ = window.top.overflyGen.Confirm('Texto do confirm');
    // Respostas: 0 - sistema fechando, 1 - Btn OK, 2 - Btn Cancel
    
    var _retMsg_ = window.top.overflyGen.Confirm('Voc� tem certeza?');
    
    // sistema fechando
    if ( _retMsg_ == 0 )
        return null;
    // btn OK    
    else if ( _retMsg_ == 1 )
    {
    
    }
    // btn Cancel
    else if ( _retMsg_ == 2 )    
    {
        /*******************
        0           - form esta em modo consulta
        10          - form esta em modo inclusao sup
        11          - form esta em modo inclusao inf
        20          - form esta em modo alteracao sup
        21          - form esta em modo alteracao inf
        30          - form esta em modo exclusao sup
        31          - form esta em modo exclusao inf
        40          - form esta com modal aberta
        *******************/
        glb_LastActionInForm__ = 0;

    
    	// Escreve no status bar
    	writeInStatusBar('child', 'cellMode', 'Detalhe');
        return null;
    }
    
    /*******************
    0           - form esta em modo consulta
    10          - form esta em modo inclusao sup
    11          - form esta em modo inclusao inf
    20          - form esta em modo alteracao sup
    21          - form esta em modo alteracao inf
    30          - form esta em modo exclusao sup
    31          - form esta em modo exclusao inf
    40          - form esta com modal aberta
    *******************/
    glb_LastActionInForm__ = 0;

            
    // Desabilita os control bars
    adjustSupInfControlsBar('DISALL');
    
    // Trava a interface
    lockInterface(true);
        
    excludeRegister_2stPart();
}

/********************************************************************
Funcao que salva ou nao o registro corrente com o campo de UsuarioID
(caso exista do recordset do dso, )alterado para o id do usuario logado.
Isto e necessariopara executar as operacoes de log no banco.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function excludeRegister_2stPart()
{
    // LOG - sempre altera o UsuarioID
    if ( LOG_ACT_FLD != null )
    {
        if ( fieldExists(dsoSup01, LOG_ACT_FLD) )
            saveForLogBeforeDelete();
        else
            excludeRegister_3stPart();
    }
    else
        excludeRegister_3stPart();    
}

/********************************************************************
Salva o registro corrente com o usuario logado, para executar o log,
antes de deletar o registro.
No retorno inicia o deleta do registro corrente.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveForLogBeforeDelete()
{
    // LOG - sempre altera o UsuarioID
    if ( LOG_ACT_FLD != null )
    {
        if ( fieldExists(dsoSup01, LOG_ACT_FLD) )
            dsoSup01.recordset[LOG_ACT_FLD].value = (-1) * (glb_USERID);
    }
            
    try
    {
        dsoSup01.SubmitChanges();   
    }
    catch(e)
    {
        // Nao faz nada
        ;    
    }    
    
    dsoSup01.ondatasetcomplete = saveForLogBeforeDelete_DSC;
    dsoSup01.Refresh();    
}

/********************************************************************
Retorno da funcao que salva o registro corrente com o usuario logado,
para executar o log.
Inicia o deleta do registro corrente.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveForLogBeforeDelete_DSC()
{
    glb_DetSupTimerVar = window.setInterval('excludeRegister_3stPart()', 10, 'JavaScript');
}

/********************************************************************
Deleta o registro corrente
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function excludeRegister_3stPart()
{
    if ( glb_DetSupTimerVar != null )
    {
        window.clearInterval(glb_DetSupTimerVar);
        glb_DetSupTimerVar = null;
    }    

    // Primeiro deleta o registro na tabela mae
    // (Falta fazer a delecao nas tabelas filhas)
    dsoSup01.recordset.Delete();
        
    try
    {
        dsoSup01.SubmitChanges();
    }
    catch(e)
    {
        if (e.number == -2147217887) // numero de erro qdo o registro ja foi deletado por outro usuario
        {
            if (window.top.overflyGen.Alert(e.name + ' - ' + e.message) == 0)
                return null;
            // if (window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0)
        }    
    }    
        
    dsoSup01.ondatasetcomplete = dsoSup01Delete_DSC;
    dsoSup01.Refresh();
}

/********************************************************************
Retorno do servidor da funcao excludeRegister_2stPart()

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoSup01Delete_DSC()
{
    __btn_LIST('sup');
}

// FINAL DE FUNCOES DA EXCLUSAO *************************************

// FUNCOES DA MAQUINA DE ESTADO *************************************

/********************************************************************
Chamada da gravacao pela maquina de estado.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function saveEstadoID()
{
    saveRegister_1stPart();
}

/********************************************************************
Invoca a maquina de estado para o sup

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function showStateMachine()
{
    // escreve no status bar
    writeInStatusBar('child', 'cellMode', 'Estado',true);

    var htmlPath;
    
    // Para trazer a maquina de estado correta e
    // os possiveis estados transitivos para o
    // registro corrente, mandaremos para o servidor:
    // 1. EstadoID do registro corrente
    // 2. FormID
    // 3. ContextoID corrente
    // 4. O ID do usuario logado
    // 5. O ID da empresa corrente

    var contextID = getCmbCurrDataInControlBar('sup', 1);

    var strPars = new String();
    strPars = '?';
    strPars += 'estadoID=';
    strPars += escape(dsoSup01.recordset['EstadoID'].value);
    strPars += '&';
    strPars += 'formID=';
    strPars += escape(window.top.formID);
    strPars += '&';
    strPars += 'contextoID=';
    strPars += escape(contextID[1]);
    
    strPars += '&';
    strPars += 'userID=';
    strPars += escape(getCurrUserID());
    
    var empresa = getCurrEmpresaData();
    
    strPars += '&';
    strPars += 'empresaID=';
    strPars += escape(empresa[0]);
    
    var Prop1 = dsoSup01.recordset['Prop1'].value;
    strPars += '&';
    strPars += 'nProp1=';
    strPars += escape(Prop1);

    var Prop2 = dsoSup01.recordset['Prop2'].value; 
    strPars += '&';
    strPars += 'nProp2=';
    strPars += escape(Prop2);
    
    htmlPath = SYS_ASPURLROOT + '/statemachEx/statemachsupmodal.asp' + strPars;
    
    // Medidas minimas das modais 262,3333333 x 173
    showModalWin(htmlPath, new Array(436, 246));
}

/********************************************************************
Chamada pelo programador, apos interceptar o botao OK
da maquina de estado.
Prossegue com a operacao da maquina.

Parametros: 
action = 'OK' - o novo estado sera gravado
         'CANC' - a gravacao sera cancelada

Retorno:
nenhum
********************************************************************/
function stateMachSupExec(action)
{
    action = action.toUpperCase();
    
    glb_DetSupTimerVar = window.setInterval('stateMachSupExec_2(' + '\'' + action + '\'' + ')', 10, 'JavaScript');
    
}

/********************************************************************
Chamada pela funcao stateMachSupExec
Prossegue com a operacao da maquina.

Parametros: 
action = 'OK' - o novo estado sera gravado
         'CANC' - a gravacao sera cancelada

Retorno:
nenhum
********************************************************************/
function stateMachSupExec_2(action)
{
    if (glb_DetSupTimerVar != null )
    {
        window.clearInterval(glb_DetSupTimerVar);
        glb_DetSupTimerVar = null;
    }
    
    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'STMACH_SUP_GO', action );
    
}

/********************************************************************
Obtem o estado abreviado do registro corrente

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function getStateMachineAbrev(retFunc)
{
    // se o dsoStateMachine nao foi aberto
    if ( dsoStateMachine.recordset.Fields.Count == 0 )
    {
        setStateMachTxtHint('');
        
        // Resolve o EstadoID para RecursoAbreviado
        get_stateMachine(dsoStateMachine,dsoSup01.recordset['EstadoID'].value);
        dsoStateMachine.ondatasetcomplete = eval(retFunc);
        dsoStateMachine.Refresh();
    }
    else
    {    
        var estAbrev = sendJSMessage(getHtmlId(), JS_WIDEMSG, '__ESTADODATA', dsoSup01.recordset['EstadoID'].value);
        
        dsoStateMachine.recordset.moveFirst();
        
        dsoStateMachine.recordset['Estado'].value = estAbrev[0];
        
        setStateMachTxtHint( (dsoSup01.recordset['EstadoID'].value).toString() + '-' + estAbrev[1]);

        eval(retFunc + '()');
    }
}

/********************************************************************
Coloca o Fantasia do estado do registro como hint do campo txtEstadoID

Parametros: 
strHint     - o hint

Retorno:
nenhum
********************************************************************/
function setStateMachTxtHint(strHint)
{
    if ( strHint == null )
        strHint = '';
        
    if ( window.document.getElementById('txtEstadoID') != null )
            txtEstadoID.title = strHint;
}

/********************************************************************
Carregamento do dsoStateMachine pela primeira vez.
Nao faz nada

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoStateMachLoadDoNothing()
{
    // se o dsoStateMachine nao foi aberto
    if ( dsoStateMachine.recordset.Fields.Count == 0 )
    {
        // Resolve o EstadoID para RecursoAbreviado
        get_stateMachine(dsoStateMachine,1);
        dsoStateMachine.ondatasetcomplete = dsoStateMachLoadDoNothing_DSC;
        dsoStateMachine.Refresh();
    }
    else
        return null;
}

/********************************************************************
Retorno de carregamento do dsoStateMachine pela primeira vez.
Nao faz nada

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoStateMachLoadDoNothing_DSC()
{

}

// FINAL DE FUNCOES DA MAQUINA DE ESTADO ****************************

// FUNCOES DO CARRIER ***********************************************

/********************************************************************
Exclusivo para exibir detalhe em form
Parametros:
              - regID - id do registro a mostrar
Retorno:
              - irrelevante
********************************************************************/
function showDetailByCarrier(regID)
{
    if ( (regID == null) || (regID == '') )
        regID = 0;

    sendJSMessage( 'PESQLIST_HTML', JS_DATAINFORM, EXECEVAL ,
                   'execPesqForCarrier(' + regID.toString() + ')' );

}
// FINAL DE FUNCOES DO CARRIER **************************************



// FUNCOES APENAS DE TESTES *****************************************
/********************************************************************
Mostra os atributos dos campos de um SELECT
Parametros:
              - recSet -> referencia ao dso.recordset
Retorno:
              - irrelevante
********************************************************************/
function test_showFldsAttrib(recSet)
{
    var i;
    var fldName;
    var fldAttr;
    
    for (i=0; i<recSet.Fields.Count; i++)
    {
        fldName = recSet.Fields[i].Name;
        fldAttr = recSet.Fields[i].Attributes;
        if (window.top.overflyGen.Alert ('field: ' + fldName + ', attributes in dec = ' + fldAttr))
            return null;
        
    }
}
// FINAL DE FUNCOES APENAS DE TESTES ********************************
