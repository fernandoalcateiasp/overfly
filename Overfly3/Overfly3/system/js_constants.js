/********************************************************************
js_constants.js
Library javascript de constantes para o sistema
********************************************************************/

// MENSAGEMS DO SISTEMA *********************************************

// === RESERVADAS, NAO LIBERADAS PARA O PROGRAMADOR =================
var JS_NOTIFICATIONCALL = -16;
var JS_NOTIFICATIONQTY = -15;
var JS_FIELDINTLEN = -14;
var JS_FOCUSFORM = -13;
var JS_LOGIN = -12;
var JS_STRCONN = -11;
var JS_WIDEMSG = -10;
var JS_CARRIERCAMING = -9;
var JS_CARRIERRET = -8;
var JS_SUPSYS = -7;
var JS_INFSYS = -6;
var JS_CONTFILTFINISH = -5;
var JS_DATETIMEINSERVER = -4;
var JS_FASTBUTTONCLICKED = -3;
var JS_CHILDBROWSERLOCKED = -2;
var JS_MAINBROWSERCLOSING = -1;
var JS_MSGRESERVED = 0;

var JS_CHILDBROWSERLOADED = 100;
var JS_FORMOPEN = 150;
var JS_NOMFORMOPEN = 200;
var JS_NOMFORMCLOSE = 210;
var JS_FORMOPENED = 250;
var JS_USERLOAD = 300;
var JS_NEWPASSWORD = 350;
var JS_PAGELOAD = 400;
var JS_PAGELOADED = 420;
var JS_STMACHMODAL = 450;
var JS_STBARWRITE = 500;
var JS_STBARREAD = 510;
var JS_STBARGEN = 520;
var JS_COMBOLOAD = 600;
var JS_COMBOSELCHANGE = 610;
var JS_COMBOSELCHANGEDATA = 620;
var JS_COMBODATA = 630;

// === LIBERADAS PARA O PROGRAMADOR =================================
var JS_DATAINFORM = 700;

// === PARAMETROS DE MENSAGENS ======================================
var EXECEVAL = 'E_';

// === CONSTANTES GERAIS USADAS PELO SISTEMA ========================
// Dominio das paginas
var SYS_PAGESDOMINIUM = window.top.__PAGES_DOMINIUM__;
// URL raiz das paginas no dominio
var SYS_PAGESURLROOT = window.top.__PAGES_URLROOT__;
// Dominio do banco de dados
var SYS_DATABASEDOMINIUM = window.top.__DATABASE_DOMINIUM__;
// URL raiz das paginas ASP no dominio
var SYS_ASPURLROOT = window.top.__DATABASE_ASPURLROOT__;
// URL raiz do web-service OverflyRDS
var SYS_WSURLROOT = window.top.__DATABASE_WSURLROOT__;
// === NOME DO SISTEMA ==============================================
var SYS_NAME = 'Overfly';
var SYS_DEBUGMODE = false;

// === CONSTANTES DIVERSAS ==========================================
var BROWSERCHILD_INTERFACE = 'toolbar = no, menubar = no, scrollbar = no, status = yes, resizable = yes';
var BROWSERCHILD_NOTIFICATION = 'toolbar = no, menubar = no, scrollbar = no, status = yes, resizable = no';


var _MARGIN_TOP = 5;
var _MARGIN_BOTTON = 15;
var _MARGIN_LEFT = 5;
var _MARGIN_LEFT_LANDSCAPE = 5;
var _MARGIN_RIGHT_PORTRAIT = 5;
var _MARGIN_RIGHT_LANDSCAPE = 16;

// Overfly teste usa red na variavel abaixo
var OVER_TESTE = false;
var MAX_USERNAME = 80;
var MAX_SENHA = 20;
var MAX_FRAMEWIDTH_OLD = 787;
var MAX_FRAMEWIDTH = 1000;
var MAX_FRAMEHEIGHT_OLD = 519;
var MAX_FRAMEHEIGHT = 600;
var FONT_WIDTH = 8;
var LABEL_HEIGHT = 15;
var ELEM_GAP = 10;

// Intervalo em milisegundos de gravacao em lote
var INTERVAL_BATCH_SAVE = 500;

// Variavel global do dicionario para translado de 
// termos dos dicionarios alternativos, em relatorios
var glb_sTermUse__ = null;

// Do Overfly Teste
try 
    {
        if ((window.top.__APP_NAME__).toUpperCase() == 'OVERFLY_REPORTS') {
            SYS_NAME = 'Overfly Relatórios';
            OVER_TESTE = true;
        }
        else if ((window.top.__APP_NAME__).toUpperCase() == 'OVERFLY_SIMULADOR') {
            SYS_NAME = 'Overfly Simulador';
            OVER_TESTE = true;
        } 
        else if ((window.top.__APP_NAME__).toUpperCase() == 'OVERFLY') {
            SYS_NAME = 'Overfly';
            OVER_TESTE = false;
        }
    }
catch (e) {
    ;
}

// MessageBox Icons
var IDI_ERROR = 32513;
var IDI_QUESTION = 32514;
var IDI_EXCLAMATION = 32515;
var IDI_INFORMATION = 32516;

// Sounds
var MB_ICONASTERISK = 64; //0X40
var MB_ICONEXCLAMATION = 48; //0X30
var MB_ICONHAND = 16; //0X10
var MB_ICONQUESTION = 32; //0X20
var MB_OK = 0; //0X0

// Espaco
var SYS_SPACE = String.fromCharCode(32);

// Formato de data
// dia/mes/ano - "DD/MM/YYYY"
// mes/dia/ano - "MM/DD/YYYY"
var DATE_FORMAT = "DD/MM/YYYY";
var DATE_SQL_PARAM = 103;

if (window.top.document.getElementById('overflyGen') != null) {
    DATE_FORMAT = window.top.overflyGen.ShortDateFormat();
    if (DATE_FORMAT == "DD/MM/YYYY")
        DATE_SQL_PARAM = 103;
    else if (DATE_FORMAT == "MM/DD/YYYY")
        DATE_SQL_PARAM = 101;
}

// Separadores de numero formatado
var DECIMAL_SEP = ",";
var THOUSAND_SEP = ".";

if (window.top.document.getElementById('overflyGen') != null) {
    DECIMAL_SEP = window.top.overflyGen.DecimalSep;
    THOUSAND_SEP = window.top.overflyGen.ThousandSep;
}

// Altura e Gap das etiquetas de codigo de barras
var LABELBARCODE_HEIGHT = 15;
var LABELBARCODE_GAP = 3;

// As operacoes de LOG estao ativadas se variavel abaixo != null
var LOG_ACT_FLD = 'UsuarioID';

// Sites das empresas do grupo
// 1o elemento ID da Empresa
// 2o elemento ID da Empresa Alternativa
// 3o URL do site
var glb_aSites =
    [[2, 4, 'http://www.alcateia.com.br'],
     [3, 3, 'http://www.alcabyt.com.br'],
     [5, 5, 'http://www.nordtec.com.br'],
     [7, 8, 'http://www.allplus.com'],
     [10, 10, 'http://www.alcateia.com.br']];

// Alert e confirm do overflyGen.dll
// Icones possiveis
// IDI_ERROR, IDI_QUESTION , IDI_EXCLAMATION ,IDI_INFORMATION
// window.top.overflyGen.Alert('Texto do alert');
// window.top.overflyGen.Confirm('Texto do confirm');
// Respostas: 0 - sistema fechando, 1 - Btn OK, 2 - Btn Cancel

// CONSTANTES DA TELEFONIA ==========================================
// As operacoes de telefonia estao ativadas ou nao
var TEL_ACTIVETED = true;

// Inicio dos parametros dos OCXs da Voice
var VOICE_OCX_CLSID = null;
// Porta do server do CTI que e a maquina MIDDLEWARE
var SERVER_CTI_PORT = null;
// IP do server do CTI que e a maquina MIDDLEWARE
var SERVER_MACH_ADDRESS = null;
// Final dos parametros dos OCXs da Voice

// ATENCAO !!!!!
// Alterar aqui para definir o OCX e server a usar
var __new_Voice_OCX = true;

// Antigo
if (!(__new_Voice_OCX == true)) {
    VOICE_OCX_CLSID = '{DE09A419-A252-11D6-B1B9-00E098033019}';
    SERVER_CTI_PORT = '2556';
    SERVER_MACH_ADDRESS = '172.16.0.25';
}
// Novo
else {
    VOICE_OCX_CLSID = '{61953245-DBC6-11D6-99F4-00E07D72158F}';
    SERVER_CTI_PORT = '2556';
    SERVER_MACH_ADDRESS = '172.16.0.27';
}

// FINAL DE CONSTANTES DA TELEFONIA =================================