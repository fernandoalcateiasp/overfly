/********************************************************************
js_rdsfacil.js

Library javascript de funcoes facilidades para objeto RDS
********************************************************************/

/********************************************************************

INDICE DAS FUNCOES:
    setValueInControlLinked(ctlRef, dso, fldValue)
    dsoFixedParams()
    changeZeroByNull(dso)
    
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Seleciona o primeiro option em um combo ou checa um checkbox.
Esta funcao atua no value do campo do dso ao qual o controle esta
linkado.

Parametros:
ctlRef      - referencia ao controle
dso         - objeto RDS ao qual o controle esta linkado
fldValue    - vlr a ser atribuido ao campo do dso ao qual o controle
              esta linkado. Opcional para checkbox.
              Se null para select, usa o option de index 0

Retornos:
nenhum
********************************************************************/
function setValueInControlLinked(ctlRef, dso, fldValue)
{
	// campo da tabela ao qual o controle esta linkado
	if ( ctlRef.dataFld == '' )    
		return null;
        
	// controle e select
	if ( ctlRef.tagName.toUpperCase() == 'SELECT' )
		if ( ctlRef.options.length != 0 )
			if ( fldValue != null ) {
				dso.recordset.Fields[ctlRef.dataFld].value = fldValue;
				ctlRef.selectedIndex = -1;
				
				for(i = 0; i < ctlRef.options.length; i++) {
					if(fldValue == ctlRef.options.item(i).value) {
						ctlRef.selectedIndex = i;
						break;
					}
				}
			} else {
				dso.recordset.Fields[ctlRef.dataFld].value = ctlRef.options.item(0).value;
			}

	// controle e checkbox        
	if ( (ctlRef.tagName.toUpperCase() == 'INPUT') &&
		(ctlRef.type.toUpperCase() == 'CHECKBOX') )
	{     
		dso.recordset.Fields[ctlRef.dataFld].value = 1;
		ctlRef.checked = true;
	}     
         
    return null;
}

/********************************************************************
Limita a quantidade de caracteres de um campo, em funcao
de um rds ao qual o campo esta linkado

Parametros:
Nenhum

Retornos:
Nenhum
********************************************************************/
function fldMaxLen(dso)
{
    var i;
    var aFieldName = new Array();
    var aFieldSize = new Array();
    var tblName = '';
    
    for (i=0;i<dso.recordset.Fields.Count;i++)
    {
        aFieldName[i] = dso.recordset.fields.item[i].name;
        if ( (dso.recordset.fields.item[i].type == 200) || (dso.recordset.fields.item[i].type == 129) ) // se varchar ou char
        {
			aFieldSize[i] = dso.recordset.fields.item[i].size;
        }
        else if (dso.recordset.fields.item[i].type == 131) // se numerico
        {
            if ( dso.recordset.fields.item[i].NumericScale != 0 )
                aFieldSize[i] = dso.recordset.fields.item[i].Precision + 1;
            else
                aFieldSize[i] = dso.recordset.fields.item[i].Precision;
        }
        else if ( (dso.recordset.fields.item[i].type == 3) || (dso.recordset.fields.item[i].type == 20) ) // se int ou big int
        {
			// Nome da tabela
			tblName = '';
			try
			{
				//tblName = dso.recordset.fields.item[i].properties(1).value;
				tblName = dso.recordset.fields.item[i].table;
				aFieldSize[i] = sendJSMessage('', JS_FIELDINTLEN, tblName, aFieldName[i]);
			}
			catch(e)
			{
				aFieldSize[i] = 0;
				continue;
			}
        }
        else if (dso.recordset.fields.item[i].type == 135) // se data
        {
            aFieldSize[i] = 10;
        }
    }
    
    // funcao que seta o tamanho dos campos
    getDataFields(aFieldName, aFieldSize);
}

/********************************************************************
Substitui por null o valor de todos os campos IDS que chegam para gravacao
com valor igual a 0 (se numerico) ou '' (se varchar).

Parametros:
dso - Referencia ao dso corrente

Retornos:
	null ou, se o campo requer valor, retorna o nome do campo e para
	o loop nos campos
********************************************************************/
function changeZeroByNull(dso)
{
    var i;
    var sFieldName;
    var bFieldIsID = false;
    var retVal = null;

    for (i=0;i<dso.recordset.Fields.Count;i++)
    {
        sFieldName = dso.recordset.fields.item[i].name;
        
        if ( (dso.recordset.fields.item[i].type == 131) ||
			 (dso.recordset.fields.item[i].type == 3) ||
			 (dso.recordset.fields.item[i].type == 20) ) // se numerico int ou bigint
        {
            bFieldIsID = false;
            
            if ( (sFieldName.substr(sFieldName.length - 2)).toUpperCase() == 'ID' )
                bFieldIsID = true;
            
            if ( (dso.recordset.fields.item[i].value == 0) && (bFieldIsID) )
            {
                // Se o campo for obrigatorio
                if ( ~dso.recordset.fields.item[i].Attributes & 64 )
                {
					if ( retVal == null )
						retVal = sFieldName.substr(0, sFieldName.length - 2);
                }    
                else    
                    dso.recordset.fields.item[i].value = null;
            }
        }
        else if ( (dso.recordset.fields.item[i].type == 200) || (dso.recordset.fields.item[i].type == 129)) // se varchar ou char
        {
            if ( (dso.recordset.fields.item[i].value == null) || (dso.recordset.fields.item[i].value == '') )
            {
                // Se o campo for obrigatorio
                if ( !(~dso.recordset.fields.item[i].Attributes & 64) )
                {
					try
					{
						dso.recordset.fields.item[i].value = null;
					}	
					catch(e)
					{
						;
					}	
                }    
            }
        }
    }
    
    return retVal;
}

// FINAL DE IMPLEMENTACAO DAS FUNCOES

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****

/********************************************************************
Esta funcao percorre os elementos do html e verifica se tem link
com um objeto RDS e seta o maxlength do campo de acordo com o bco. dados.

Parametros:
Nenhum

Retornos:
Nenhum
********************************************************************/
function getDataFields(aFieldName, aFieldSize)
{
    var i, nTamanho;
    
    for (i=0; i<document.all.length; i++)
    {
        if (document.all(i).id != '')
        {
            try // esta try existe pq os controles de botao (lupa)
                // nao tem a propriedade dataFld, e da pau
            {
                if ((document.all(i).dataFld != null) && 
                    (document.all(i).dataFld != ''))
                {
                    nTamanho=getFieldSize(document.all(i).dataFld, aFieldName, aFieldSize);
                    if (nTamanho > 0)
                    {
                        document.all(i).maxLength=nTamanho;
                    }
                }
            }
            catch(e)
            {
                ;
            }
        }    
    }
}

/********************************************************************
Esta funcao retorna o tamanho do campo para a funcao getDataFields()
Esta funcao e chamada pela funcao getDataFields().

Parametros:
string      - FldName (Nome do campo)

Retornos:
int         - Tamanho do campo
********************************************************************/
function getFieldSize(FldName, aFieldName, aFieldSize)
{
    var i=0;
    
    for (i=0; i<aFieldName.length; i++)
    {
        if (FldName == aFieldName[i])
        {
            return aFieldSize[i];
        }
    }
    return 0;
}

/********************************************************************
Funcao replicate

Parametros:
sString     String a ser replicada
nVezes      numero de vezes a replicar
********************************************************************/
function replicate(sString,nVezes)
{
    var i,sNewString;
    sNewString = "" ;
    for (i=0;i<nVezes;i++)
    {
        sNewString+=sString;
    }
    return sNewString;
}

function changeStateMachine(sFldIDName,nIDValue,aNewState,dso,grid,pos)
{
	var nNewState = aNewState[0];
	var sMotivo = aNewState[1];

	// escreve no status bar
	writeInStatusBar('child', 'cellMode', 'Gravando',true);

    if (!((dso.recordset.BOF) && (dso.recordset.EOF)))
    {
        dso.recordset.MoveFirst();
        if (pos == 'sup')
        {
            dso.recordset.Find(sFldIDName, nIDValue);
            if (!dso.recordset.EOF)
            {
                dso.recordset['EstadoID'].value = nNewState;
                
                if (sMotivo != '')
					dso.recordset['LOGMotivo'].value = sMotivo;
                
                saveEstadoID();
            }
        }
        else if(pos == 'inf')
        {
            var nCurrRecIDVal = 0;
            var nCurrRecIDName = '';
            var i=0;
            nCurrRecIDVal = grid.TextMatrix(fg.Row,fg.Cols-1);
            nCurrRecIDName = grid.ColKey(fg.Cols-1);
            dso.recordset.MoveFirst();
            dso.recordset.Find(nCurrRecIDName, nCurrRecIDVal);
            if (!((dso.recordset.BOF) && (dso.recordset.EOF)))
            {
                dso.recordset['EstadoID'].value = nNewState;

                if (sMotivo != '')
					dso.recordset['LOGMotivo'].value = sMotivo;
                
                saveEstadoID();
            }
        }
    }
}

function get_stateMachine(dso,nEstadoID)
{
    setConnection(dso);

    dso.SQL = 'SELECT RecursoID, RecursoAbreviado AS Estado  FROM Recursos WHERE RecursoID = ' + nEstadoID;
}

function get_FiltroInf(dso,nFiltroID)
{
    setConnection(dso);

    dso.SQL = 'SELECT Filtro FROM Recursos WHERE RecursoID = ' +  nFiltroID;
}

/********************************************************************
Retorna tipo e numero de caracteres de um campo de um recordset

Parametros:
dso         - objeto rds que contem o recordset
fldName   - nome do campo

Retorno:
array       - [0] tipo do campo
              [1] DefinedSize do campo
              [2] Precision do campo
              [3] NumericScale do campo
              null se falha
********************************************************************/
function get_FieldDetails(dso, fldName)
{
    var aRet = new Array();
   
    try
    {
        with (dso.recordset)
        {
            aRet[0] = Fields[fldName].Type;
            aRet[1] = Fields[fldName].size;
            aRet[2] = Fields[fldName].Precision;
            aRet[3] = Fields[fldName].NumericScale;
        }
    }    
    catch (e)
    {
        return null;
    }    
   
    return aRet;    
}

/********************************************************************
Remove um link de um controle html com um objeto rds, e retorna
o datasrc e o datafld.
Ela recebe 1 parametro:

1 -> Controle  :o controle que deseja remover o link

exemplo de chamada:  var aSaveDataLink = unlinkControl(selTipoConceitoID);
********************************************************************/
function unlinkControl(controle)
{
    var oldDataSrc = controle.dataSrc;
    var oldDataFld = controle.dataFld;
    controle.dataSrc = '';
    controle.dataFld = '';
    return new Array(oldDataSrc,oldDataFld);
}

/********************************************************************
Adiciona um link a um bco de dados ,salvo pela funcao unlinkControl.
 
Parametros:

controle    - controle que deseja adicionar o link
aData       - um array com 2 elementos:
                1-> DataSrc
                2-> DataFld  
                
exemplo de chamada:  
      var aSaveDataLink = unlinkControl(selTipoConceitoID);
      sua operacao ...
      linkControl(selTipoConceitoID,aSaveDataLink);
********************************************************************/
function linkControl(controle,aData)
{
    controle.dataSrc = aData[0];
    controle.dataFld = aData[1];
    return null;
}

/********************************************************************
Elimina os options de um array de combos
 
Parametros:
aCombos     - Array dos ids dos combos a limpar
********************************************************************/
function clearComboEx(aCombos)
{
    var comboID;

    var i, j, y, coll = document.all.tags('SELECT');
    
    var oldDataSrc, oldDataFld;
    
    for (y=0; y < aCombos.length; y++)
    {
        for (i=0; i<coll.length ; i++)
        {
             if (coll(i).id == aCombos[y])
             {
                oldDataSrc = coll(i).dataSrc;
                oldDataFld = coll(i).dataFld;
                coll(i).dataSrc='';
                coll(i).dataFld='';

                for (j=coll(i).length - 1; j>=0; j--)
                {
                    coll(i).remove(j);
                }
                    
                coll(i).dataSrc=oldDataSrc;
                coll(i).dataFld=oldDataFld;
             }
        }
    }    
}

/********************************************************************
Esta funcao seta o Internet Time Out e outros parametros
para os objetos rds da pagina
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function dsoFixedParams()
{
/*********************************
    '---- enum Values ----
    Const adcExecSync = 1
    Const adcExecAsync = 2  'Default

    '---- enum Values ----
    Const adcFetchUpFront = 1
    Const adcFetchBackground = 2
    Const adcFetchAsync = 3 'Default

    '---- enum Values ----
    Const adcReadyStateLoaded = 2
    Const adcReadyStateInteractive = 3
    Const adcReadyStateComplete = 4
*********************************/

    var i, elem;
        
    for (i=0; i<window.document.all.length; i++)
    {
        elem = window.document.all.item(i);
            
        // controles RDS
        if ( elem.tagName.toUpperCase() == 'OBJECT' )
        {
            if (elem.classid.toUpperCase() == 'CLSID:BD96C556-65A3-11D0-983A-00C04FC29E33')
            {
                // 10 minutos
                elem.InternetTimeout = 10 * 60 * 1000;
                elem.ExecuteOptions = 1;//2;
                elem.FetchOptions = 3;//1;                
                //elem.Prepared = false;
            }
        }
    }
            
}

/********************************************************************
Verifica checkboxes e coloca false nos campos do dso se o checkbox
estiver visivel mas nao estiver checado
 
Parametros:
win     - referencia ao window corrent
dso     - referencia ao dso corrente
********************************************************************/
function adjustFldInTableByChkBoxUnchecked(win, dso)
{
    var i, j , coll;
    var fldName;
    
    coll = window.document.getElementsByTagName('INPUT');
    
    for ( i=0; i<coll.length; i++ )
    {
        if ( (coll.item(i).type).toUpperCase() == 'CHECKBOX' )
        {
            // check box esta invisivel, nao faz nada
            // check box esta invisivel se o div que o contem esta invisivel
            if ( coll.item(i).parentElement.currentStyle.visibility == 'visible' )
            {
                if ( coll.item(i).checked == false )
                {
                    fldName = coll.item(i).dataFld.toUpperCase();
            
                    for ( j=0; j<dso.recordset.Fields.Count; j++ )   
                    {
                        if ( (dso.recordset.Fields[j].Name).toUpperCase() == fldName )
                        {
							// Somente campos updatables
							if ( ! ((dso.recordset.Fields[j].Attributes & 4 == 4) ||
								    (dso.recordset.Fields[j].Attributes & 8 == 8)) )
								dso.recordset.Fields[j].value = false;
                        }    
                    }
                }
            }
        }
    }
}
