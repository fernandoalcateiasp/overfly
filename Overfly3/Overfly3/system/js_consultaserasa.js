/********************************************************************
js_consultaserasa.js
Library javascript de funcoes basicas para consultaserasa
********************************************************************/

// CONSTANTES *******************************************************

var glb_sCodigoAutenticador = '';
var glb_ValidacaoDadoID = null;
var glb_bConsultaOK = true;
var glb_FonteDadoID = 0;
var glb_TipoDadoID = 0;
var glb_TipoRegistroID = 0;
var glb_RegistroID = 0;

/*
var glb_Usuario = 'testealcateia';
var glb_Pass = '1v7a6h1j';
var glb_Dominio = 'alcateia';
*/
var glb_Usuario = '69566838';
var glb_Pass = 'Aba@20';
var glb_Dominio = 'alcateia';

var glb_teste = '';

var glb_aXML = new Array();

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024*1;

// FINAL DE CONSTANTES **********************************************

/********************************************************************
INDICE DAS FUNCOES:
    consultaserasa(nTipoRegistroID, sDocumento)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Faz consulta no Serasa.

Parametros:

nRegistroID - RegistroID da pessoa

nTipoDadoID - Tipo do dado
                - 295  - Cadastro
                - 296 - Cr�dito    

nTipoRegistroID - Tipo de Registro
                    - 51 - Pessoa Fisica
                    - 52 - Pessoa Juridica
sDocumento - Documento a ser consultado no Serasa                

Retorno:
true - Consulta realizada com sucesso
false - Consulta n�o realizada
********************************************************************/
function consultaSerasa(nRegistroID, nTipoDadoID, nTipoRegistroID, sDocumento)
{
    var sUrl = '';
    var sUrlSoapAction = '';
    var sMethodName = '';
    var sMethodName2 = '';
    var sStringToSend = '';
    
    glb_teste = retornaXMLTeste(nTipoRegistroID);
    glb_RegistroID = nRegistroID;
    glb_TipoRegistroID = nTipoRegistroID;
    glb_TipoDadoID = nTipoDadoID;
    glb_bConsultaOK = true;
    
    if(nTipoRegistroID==51)
        sMethodName = 'retornaDadosPF';
    else if(nTipoRegistroID==52) 
        sMethodName = 'retornaDadosPJ';

    sUrl = 'https://www.experianmarketing.com.br/webservice/infobuscaws.asmx?WSDL';
    sUrlSoapAction = 'http://www.experianmarketing.com.br/';
    
    sStringToSend = getParameters(sUrlSoapAction, sMethodName, 1, glb_Usuario, glb_Pass, glb_Dominio, sDocumento);
    xmlhtmlRequest(sUrl, sUrlSoapAction, sMethodName, sStringToSend);        
}

/*************************************************************************************
getParameters:funcao interna

    descricao: monta a string de SOAP de acordo com o metodo passado,

    parametros: 
                sParam1 - Url SoapAction
                sParam2 - metodo
                sParam3 - versao SOAP
                sParam4 - usuario, 
                sParam5 - senha, codigoautendicador
                sParam6 - dominio, cpf, cnpj                
                sParam7 - CPF                
    
    retorno: XML no padrao SOAP

(*) - parametro opcional
*************************************************************************************/
function getParameters(sParam1, sParam2, sParam3, sParam4, sParam5, sParam6, sParam7)
{
    var sXMLReturn = '';   
    
    //versao soap
    if (sParam3==1)
    {
        sXMLReturn = '<?xml version="1.0" encoding="utf-8"?>' +
                        '<soap:Envelope xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:soap="http://schemas.xmlsoap.org/soap/envelope/">' +
                        '<soap:Body>' +
                            '<' + sParam2 + ' xmlns="' + sParam1 + '">';
        
        if (sParam2=='getLogin')                              
        {
                sXMLReturn += '<strLogin>' + sParam4 + '</strLogin> ' +
                              '<strSenha>' + sParam5 + '</strSenha>' +
                              '<strDominio>' + sParam6 + '</strDominio>';
        }                     
        else if (sParam2=='retornaDadosPJ')
        {
                sXMLReturn += '<strLogin>' + sParam4 + '</strLogin>' +
                              '<strSenha>' + sParam5 + '</strSenha>' +
                              '<strDominio>' + sParam6 + '</strDominio>' + 
                              '<strDocumento>' + sParam7 + '</strDocumento>';
        }                     
        else if (sParam2=='retornaDadosPF')
        {
                sXMLReturn += '<strLogin>' + sParam4 + '</strLogin>' +
                              '<strSenha>' + sParam5 + '</strSenha>' +
                              '<strDominio>' + sParam6 + '</strDominio>' + 
                              '<strDocumento>' + sParam7 + '</strDocumento>';
        }
        else if (sParam2=='getLogout')
        {
                sXMLReturn += '<strLogin>' + sParam4 + '</strLogin> ' +
                              '<strCodAutenticador>' + sParam5 + '</strCodAutenticador>';                              
        }
        
        
        sXMLReturn +=       '</' + sParam2 + '>' +
                        '</soap:Body>' +
                     '</soap:Envelope>';       
    }
    
    return sXMLReturn;
}
  
/********************************************************************
Faz consulta no Serasa.

Parametros:

sUrl
sUrlSoapActiopn - url soap action
sMethodName - metodo utilizado
sStringToSend - string xml para ser enviada
sDocumento - Documento a ser consultado no Serasa                

Retorno:
true - Consulta realizada com sucesso
false - Consulta n�o realizada
********************************************************************/
function xmlhtmlRequest(sUrl, sUrlSoapAction, methodName, sToSend)
{   
    if ((sUrl=='') || (sUrlSoapAction==''))
        return;                

    if ((methodName==null) || (methodName==''))
        return;
        
    if (sToSend==null)
        sToSend = '';            

    var objXmlDoc = new ActiveXObject("Msxml2.DOMDocument");        
    var objXMLHttpRequest;    
    
    try
    {
        objXMLHttpRequest = new ActiveXObject('Msxml2.XMLHTTP');        
    }
    catch(e)
    {
        try
        {
            objXMLHttpRequest = new ActiveXObject('Microsoft.XMLHTTP');                
        }
        catch(e)
        {
            glb_bConsultaOK = false;                
            return;                
        }
    }    
    
    //Fun��o chamada para tratar o resposta do xml
    objXMLHttpRequest.onreadystatechange = 
        function (){ 
        if(objXMLHttpRequest.readyState == 4)
        {
            // get the return envelope
            var szResponse = objXMLHttpRequest.responseText;
    		
		    //Marco			
            // load the return into an XML data island
            objXmlDoc.loadXML(szResponse);
            
            /*Teste - deletar*/
            //objXmlDoc.loadXML(glb_teste);

            if (objXmlDoc.parseError.errorCode != 0) 
            {
                var xmlErr = objXmlDoc.parseError;                                                                
                
                if ( window.top.overflyGen.Alert ("Erro: " + xmlErr.reason) == 0 )
                    return null;        
                
                glb_bConsultaOK = false;
                return;
            } 
            else
            {
                if(methodName=='getLogin')
                    glb_sCodigoAutenticador = objXmlDoc.text;            
                    
                else if( (methodName=='retornaDadosPJ') || (methodName=='retornaDadosPF'))
                {
                    insertArraydados(objXmlDoc,methodName);
                    saveConsultaSerasa();
                }
            }
        }
    }
    objXMLHttpRequest.open('POST', sUrl, false); 
    objXMLHttpRequest.setRequestHeader('Content-Type', 'text/xml; charset=utf-8');        
    objXMLHttpRequest.setRequestHeader('SOAPAction', '"' + sUrlSoapAction + methodName + '"');
    objXMLHttpRequest.send(sToSend);          
}

function insertArraydados(objXmlDoc, sMethod)
{
    var i,j,k,l,m,n;
    var jj,kk,ll,mm,nn;    
    
    var sXMLchildnode1;    
    var sXMLchildnode2;
    var sXMLchildnode3;
    var sXMLchildnode4;
    var sXMLchildnode5;
    
    var sAtributo1=null;
    var sAtributo2=null;
    var sAtributo3=null;
    var sAtributo4=null;
    var sAtributo5=null;
    var sAtributo6=null;
        
    var sXMLdados;
    
    glb_aXML = [];
    
    if (sMethod=='retornaDadosPF')
        sXMLdados = objXmlDoc.getElementsByTagName("DadosPF");
    else if (sMethod=='retornaDadosPJ')        
        sXMLdados = objXmlDoc.getElementsByTagName("DadosPJ");

    for(i=0; i<sXMLdados.length;i++)
    {  
        sXMLchildnode1 = sXMLdados[i]; 
        
        if(sXMLchildnode1.attributes.length > 0)
                sAtributo1 = sXMLchildnode1.attributes(0).nodeValue;             
        
        glb_aXML[glb_aXML.length] = new Array(0, null, sXMLchildnode1.nodeName, sAtributo1, formatStr(sXMLchildnode1.nodeValue));
        
        for (j=0; j<sXMLchildnode1.childNodes.length; j++)
        {
            sXMLchildnode2 = sXMLchildnode1.childNodes(j);
            
            childName2 = sXMLchildnode2.nodeName;
                        
            if(sXMLchildnode2.attributes.length > 0)
                sAtributo2 = sXMLchildnode2.attributes(0).nodeValue;             
        
            if (sXMLchildnode2.firstChild!=null)
            {       
                if (sXMLchildnode2.firstChild.nodeType==3)        
                {
                    glb_aXML[glb_aXML.length] = new Array(1, sXMLchildnode2.parentNode.nodeName, childName2, sAtributo1, formatStr(sXMLchildnode2.firstChild.nodeValue));
                   
                    continue;
                }
            }
            
            glb_aXML[glb_aXML.length] = new Array(1, sXMLchildnode2.parentNode.nodeName, childName2, sAtributo2, formatStr(sXMLchildnode2.nodeValue));
            
            for (k=0; k<sXMLchildnode2.childNodes.length; k++)
            {
                sXMLchildnode3 = sXMLchildnode2.childNodes(k);
                
                childName3 = sXMLchildnode3.nodeName;
                        
                if(sXMLchildnode3.attributes.length > 0)
                    sAtributo3 = sXMLchildnode3.attributes(0).nodeValue;
        
                if (sXMLchildnode3.firstChild!=null)
                {
                    if (sXMLchildnode3.firstChild.nodeType==3)        
                    {
                        glb_aXML[glb_aXML.length] = new Array(2, childName2, childName3, sAtributo2, formatStr(sXMLchildnode3.firstChild.nodeValue));
                        
                        continue;
                    }
                }
                
                glb_aXML[glb_aXML.length] = new Array(2, childName2, childName3, sAtributo3, formatStr(sXMLchildnode3.nodeValue));
                
                for (l=0; l<sXMLchildnode3.childNodes.length ; l++)
                {
                    sXMLchildnode4 = sXMLchildnode3.childNodes(l);
                    
                    childName4 = sXMLchildnode4.nodeName;
                    
                    if(sXMLchildnode4.attributes.length > 0)
                        sAtributo4 = sXMLchildnode4.attributes(0).nodeValue;
        
                    if (sXMLchildnode4.firstChild!=null)
                    {
                        if (sXMLchildnode4.firstChild.nodeType==3)        
                        {
                            glb_aXML[glb_aXML.length] = new Array(3, childName3, childName4, sAtributo3, formatStr(sXMLchildnode4.firstChild.nodeValue));
                            
                            continue;                            
                        }
                    }
                    
                    glb_aXML[glb_aXML.length] = new Array(3, childName3, childName4, sAtributo4, formatStr(sXMLchildnode4.nodeValue));      
                        
                    for (m=0; m<sXMLchildnode4.childNodes.length ; m++)
                    {
                        sXMLchildnode5 = sXMLchildnode4.childNodes(m);
                        
                        childName5 = sXMLchildnode5.nodeName;
                                    
                        if (sXMLchildnode5.attributes.length > 0)
                            sAtributo5 = sXMLchildnode5.attributes(0).nodeValue;
        
                        if (sXMLchildnode5.firstChild!=null)
                        {
                            if (sXMLchildnode5.firstChild.nodeType==3)        
                            {
                                glb_aXML[glb_aXML.length] = new Array(4, childName4, childName5, sAtributo4, formatStr(sXMLchildnode5.firstChild.nodeValue));
                                continue;
                            }
                        }
                        
                        glb_aXML[glb_aXML.length] = new Array(4, childName4, childName5, sAtributo5, formatStr(sXMLchildnode5.nodeValue));      
                            
                        for (n=0; n<sXMLchildnode5.childNodes.length ; n++)
                        {
                            sXMLchildnode6 = sXMLchildnode5.childNodes(n);
                            
                            childName6 = sXMLchildnode6.nodeName;
                                        
                            if (sXMLchildnode6.attributes.length > 0)
                                sAtributo6 = sXMLchildnode6.attributes(0).nodeValue;
        
                            if (sXMLchildnode6.firstChild!=null)
                            {
                                if (sXMLchildnode6.firstChild.nodeType==3)        
                                {
                                    glb_aXML[glb_aXML.length] = new Array(5, childName5, childName6, sAtributo5, formatStr(sXMLchildnode6.firstChild.nodeValue));
                                    
                                    continue;
                                }
                            }
                            
                            glb_aXML[glb_aXML.length] = new Array(5, childName5, childName6, sAtributo6, formatStr(sXMLchildnode6.nodeValue));      
                        }                                
                            
                    }                        
                }
            }
        }
    }
} 

/********************************************************************
Salva consulta no banco
********************************************************************/
function saveConsultaSerasa()
{
	if (glb_aXML.length < 1)
	{        
		window.top.overflyGen.Alert ('Consulta n�o retornou dados v�lidos');
		
		lockControlsInModalWin(false);
		glb_bConsultaOK = false;
				    
		return;
	}

    var strPars = new String();    
    
    var nFormID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'window.top.formID');
    var nSubFormID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'window.top.subFormID');    
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');
    
    glb_FonteDadoID = 292;
        
    strPars = '?nFonteDadoID=' + escape(glb_FonteDadoID);
    strPars += '&nTipoDadoID=' + escape(glb_TipoDadoID);
    strPars += '&nTipoRegistroID=' + escape(glb_TipoRegistroID);
    strPars += '&nFormID=' + escape(nFormID);
    strPars += '&nSubFormID=' + escape(nSubFormID);
    strPars += '&nRegistroID=' + escape(glb_RegistroID);
    strPars += '&nUsuarioID=' + escape(nUserID);
    
    try
	{		    
		dsoGravaConsultaSerasa.URL = SYS_ASPURLROOT + '/serversidegenEx/incluirconsultaserasa.aspx' + strPars;
		dsoGravaConsultaSerasa.ondatasetcomplete = saveConsultaSerasaDetalhes;
		dsoGravaConsultaSerasa.refresh();	
	}
	catch(e)
	{
		glb_bConsultaOK = false;
		
		if ( window.top.overflyGen.Alert ('Erro ao gravar consulta') == 0 ) {
			lockControlsInModalWin(false);
			
			return null;
		}
		
		consultaSerasaEnded(false);
    }
}

/********************************************************************
Salva consulta detalhes no banco
********************************************************************/
function saveConsultaSerasaDetalhes()
{
	var strPars = new String();
	var i=0;
	var nDataLen = 0;
	var nBytesAcum = 0;	
	var aEmpresaData = getCurrEmpresaData();	
    var sNivel = '';
    var sNodeMae = '';
    var sNode = '';
    var sAtributo = '';

	glb_aSendDataToServer = [];
	glb_nPointToaSendDataToServer = 0;
	glb_GravacaoPendente = false;

    if (!((dsoGravaConsultaSerasa.recordset.BOF) || (dsoGravaConsultaSerasa.recordset.EOF)))    
        glb_ValidacaoDadoID = dsoGravaConsultaSerasa.recordset['ValidacaoDadoID'].value;
    
    if ((glb_ValidacaoDadoID == null) || (glb_ValidacaoDadoID == 0))
    {
        glb_bConsultaOK = false;            
        return;            
    }
    
    for (i=0; i<glb_aXML.length; i++)
    {    
		nBytesAcum=strPars.length;
		
		if ((nBytesAcum >= glb_nMaxStringSize)||(strPars == ''))
		{
			nBytesAcum = 0;
			if (strPars != '')
			{
				glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
				strPars = '';
				nDataLen = 0;
			}
			
			strPars = '?nValidacaoDadoID=' + escape(glb_ValidacaoDadoID);
		}

        sNivel = (glb_aXML[i][0] == null ? '' : glb_aXML[i][0]);
        sNivel = replaceStr(sNivel.toString(), '\'', '');
        sNivel = replaceStr(sNivel.toString(), '"', '');
        sNivel = replaceStr(sNivel.toString(), '.', '');
        sNivel = replaceStr(sNivel.toString(), ',', '');
        sNivel = replaceStr(sNivel.toString(), '-', '');
        sNivel = replaceStr(sNivel.toString(), ';', '');

        sNodeMae = (glb_aXML[i][1] == null ? '' : glb_aXML[i][1]);
        sNodeMae = replaceStr(sNodeMae.toString(), '\'', '');
        sNodeMae = replaceStr(sNodeMae.toString(), '"', '');
        sNodeMae = replaceStr(sNodeMae.toString(), '.', '');
        sNodeMae = replaceStr(sNodeMae.toString(), ',', '');
        sNodeMae = replaceStr(sNodeMae.toString(), '-', '');
        sNodeMae = replaceStr(sNodeMae.toString(), ';', '');
        
        sNode = (glb_aXML[i][2] == null ? '' : glb_aXML[i][2]);
        sNode = replaceStr(sNode.toString(), '\'', '');
        sNode = replaceStr(sNode.toString(), '"', '');
        sNode = replaceStr(sNode.toString(), '.', '');
        sNode = replaceStr(sNode.toString(), ',', '');
        sNode = replaceStr(sNode.toString(), '-', '');
        sNode = replaceStr(sNode.toString(), ';', '');

        sAtributo = (glb_aXML[i][3] == null ? '' : glb_aXML[i][3]);
        sAtributo = replaceStr(sAtributo.toString(), '\'', '');
        sAtributo = replaceStr(sAtributo.toString(), '"', '');
        sAtributo = replaceStr(sAtributo.toString(), '.', '');
        sAtributo = replaceStr(sAtributo.toString(), ',', '');
        sAtributo = replaceStr(sAtributo.toString(), '-', '');
        sAtributo = replaceStr(sAtributo.toString(), ';', '');

        var sValor = glb_aXML[i][4];
        sValor = replaceStr(sValor.toString(), '\'', '').toString();
        sValor = replaceStr(sValor.toString(), '"', '').toString();
        sValor = replaceStr(sValor.toString(), '.', '').toString();
        sValor = replaceStr(sValor.toString(), ',', '').toString();
        sValor = replaceStr(sValor.toString(), '-', '').toString();
        sValor = replaceStr(sValor.toString(), ';', '').toString();
        
        // Verifica se o campo eh uma data
        if (chkDataEx(sValor))
        {
            // Se for, coloca no formato sql
            sValor = putDateInMMDDYYYY2(sValor);
        }
        
        if ((trimStr(sNode.toUpperCase()) == 'COD.ATIV.PRINCIPAL') && (sValor.length>9))
                sValor = sValor.toString().substring(0,10);
                
        if (trimStr(sNode.toUpperCase()) == 'CNPJ')
            sValor = replaceStr(sValor.toString(), '/', '').toString();

        glb_aXML[i][4] = sValor;
        
        strPars += '&nNivel=' + escape(sNivel);
        strPars += '&sNodeMae=' + escape(sNodeMae);
        strPars += '&sNode=' + escape(sNode);
        strPars += '&sAtributo=' + escape(sAtributo);
        strPars += '&sValor=' + escape((glb_aXML[i][4] == null ? '' : glb_aXML[i][4]));
        strPars += '&nFonteDadoID=' + escape(glb_FonteDadoID);
        strPars += '&nPaisID=' + escape(aEmpresaData[1]);
	    
		nDataLen++;
	}

	if (nDataLen > 0)
		glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars;
		
	sendDataToServer();
}

function sendDataToServer()
{
	if (glb_nTimerSendDataToServer != null)
	{
		window.clearInterval(glb_nTimerSendDataToServer);
		glb_nTimerSendDataToServer = null;
	}
	
	try
	{
		if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length)
		{		    
			dsoGravaConsultaSerasaDetalhes.URL = SYS_ASPURLROOT + '/serversidegenEx/incluirconsultaserasadetalhes.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
			dsoGravaConsultaSerasaDetalhes.ondatasetcomplete = sendDataToServer_DSC;
			dsoGravaConsultaSerasaDetalhes.refresh();
		}
		else
		{   
		    validacaoDados_AtualizaPessoa();
		}	
	}
	catch(e)
	{
	    if ( window.top.overflyGen.Alert ('Erro ao gravar consulta') == 0 )
            return null;        
                
	    glb_bConsultaOK = false;
	    
		if (!isNaN(glb_ValidacaoDadoID))
		{
		    setConnection(dsoDeletaConsultaSerasa);

            dsoDeletaConsultaSerasa.SQL = 'DELETE FROM ValidacoesDados_Detalhes WHERE ValidacaoDadoID = ' + glb_ValidacaoDadoID + ' ' +
                                            'DELETE FROM ValidacoesDados WHERE ValidacaoDadoID = ' + glb_ValidacaoDadoID;            
            dsoDeletaConsultaSerasa.Refresh();
		}
		
		consultaSerasaEnded(false);
	}
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
	glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');  
}

function retornaXMLTeste(nTipo)
{
    var sXML = '';
    
    if(nTipo==51)
    {
        sXML = '<?xml version=\"1.0\"?> ' +
                '<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"  ' +
	                    'xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  ' +
	                    'xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"> ' +
                    '<soap:Body> ' +
	                    '<retornaDadosPFResponse xmlns=\"http://www.experianmarketing.com.br/\"> ' +
		                    '<retornaDadosPFResult> ' +
			                    '<DadosPF xmlns=\"\"> ' +
				                    '<DadosCadastrais> ' +
					                    '<CPF>21339409879</CPF> ' +
					                    '<Nome>DENIS CERQUEIRA DA SILVA</Nome> ' +
					                    '<Idade>27 Anos - Data de Nascimento: 17/04/1981</Idade> ' +
					                    '<Estado_Civil></Estado_Civil> ' +
					                    '<Sexo>M</Sexo> ' +
					                    '<Cheques_Devolvidos__ha_menos_de_6_meses_></Cheques_Devolvidos__ha_menos_de_6_meses_> ' +
					                    '<Cheques_Devolvidos__ha_mais_de_6_meses_></Cheques_Devolvidos__ha_mais_de_6_meses_> ' +
					                    '<Email></Email> ' +
					                    '<Nome_da_Mae>MARINALVA CERQUEIRA DA SILVA</Nome_da_Mae> ' +
					                    '<Renda_Presumida></Renda_Presumida> ' +
					                    '<Enderecos> ' +
						                    '<Endereco ID=\"1\"> ' +
							                    '<Logradouro>R ALBERTO CAEIRO 38  CS 2</Logradouro> ' +
							                    '<Bairro>JARDIM SAO JUDAS TADEU</Bairro> ' +
							                    '<Cidade>SAO PAULO</Cidade> ' +
							                    '<UF>SP</UF> ' +
							                    '<CEP>04813150</CEP> ' +
						                    '</Endereco> ' +
							                    '<Endereco ID=\"2\"> ' +
							                    '<Logradouro>R CAP LORENA 285</Logradouro> ' +
							                    '<Bairro>VILA INVERNADA</Bairro> ' +
							                    '<Cidade>SAO PAULO</Cidade> ' +
							                    '<UF>SP</UF> ' +
							                    '<CEP>03350080</CEP> ' +
						                    '</Endereco> ' +
							                    '<Endereco ID=\"3\"> ' +
							                    '<Logradouro>R ALBERTO CAEIRO 38  CS</Logradouro> ' +
							                    '<Bairro>JARDIM SAO JUDAS TADEU</Bairro> ' +
							                    '<Cidade>SAO PAULO</Cidade> ' +
							                    '<UF>SP</UF> ' +
							                    '<CEP>04813150</CEP> ' +
						                    '</Endereco> ' +
					                    '</Enderecos> ' +
					                    '<Telefones> ' +
						                    '<Telefone EnderecoID=\"1\"> ' +
							                    '<DDD>11</DDD> ' +
							                    '<Numero>56611963</Numero> ' +
						                    '</Telefone> ' +
						                    '<Telefone EnderecoID=\"2\"> ' +
							                    '<DDD>11</DDD> ' +
							                    '<Numero>20225170</Numero> ' +
						                    '</Telefone><Telefone EnderecoID=\"3\"> ' +
							                    '<DDD>11</DDD> ' +
							                    '<Numero>96027812</Numero> ' +
							                    '<DDD>11</DDD> ' +
							                    '<Numero>96623055</Numero> ' +
							                    '<DDD>11</DDD> ' +
							                    '<Numero>56633476</Numero> ' +
						                    '</Telefone> ' +
					                    '</Telefones> ' +
				                    '</DadosCadastrais> ' +
				                    '<SituacaoCadastral> ' +
					                    '<Situacao>REGULAR</Situacao> ' +
					                    '<Codigo_de_Controle>76E4.EB03.EFD0.4A2E</Codigo_de_Controle> ' +
					                    '<Data>21/10/2008</Data> ' +
					                    '<Hora>10:50:33</Hora> ' +
					                    '<Fonte_Pesquisada>Receita Federal (Pessoa F�sica) - 002</Fonte_Pesquisada> ' +
				                    '</SituacaoCadastral> ' +
			                    '</DadosPF> ' +
		                    '</retornaDadosPFResult> ' +
	                    '</retornaDadosPFResponse> ' +
                    '</soap:Body> ' +
                '</soap:Envelope> ';
    }
    else if (nTipo==52)
    {
        sXML = '<?xml version=\"1.0\"?> ' +
                    '<soap:Envelope  ' +
	                    'xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\"  ' +
	                    'xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"  ' +
	                    'xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"> ' +
	                    '<soap:Body> ' +
		                    '<retornaDadosPJResponse xmlns=\"http://www.informarketing.com/UnionWebService\"> ' +
			                    '<retornaDadosPJResult>	 ' +
				                    '<DadosPJ xmlns=\"\"> ' +
					                    '<IBGE/> ' +
					                    '<SimplesNacional> ' +
						                '    <CNPJ>56.525.025/0001-63</CNPJ> ' +
						                '    <Raz�oSocial>ALCATEIA ENGENHARIA DE SISTEMAS LTDA</Raz�oSocial> ' +
						                '    <Situa��o>Não optante pelo Simples Nacional Caso tenha formalizado sua opção, você poderá verificar o resultado do processamento da sua solicitação de opção em Acompanhamento da formalização da opção.</Situa��o> ' +
						                '    <DatadaConsulta>17/10/2008</DatadaConsulta> ' +
					                    '</SimplesNacional> ' +
					                    '<Sintegra> ' +
					                    '    <CNPJ>56.525.025/0001-63</CNPJ> ' +						                
					                    '    <inscr_estadual>111626412114</inscr_estadual> ' +
						                '    <razao_social>ALCATEIA ENGENHARIA DE SISTEMAS LTDA</razao_social> ' +						                
						                '    <nome_fantasia>Alcateia</nome_fantasia> ' +						                
						                '    <Logradouro>R DOS ITALIANOS</Logradouro> ' +
						                '    <Numero>1.127</Numero> ' +
						                '    <Complemento>R.ANHAIA, 1142 E 1116</Complemento> ' +
						                '    <CEP>01.131-000</CEP> ' +
						                '    <Bairro>Bom retiro</Bairro> ' +
						                '    <Munic�pio>Sao Paulo</Munic�pio> ' +
						                '    <UF>SP</UF> ' +						                
					                    '</Sintegra> ' +
					                    '<ReceitaFederal> ' +
						                '    <CNPJ>56.525.025/0001-63</CNPJ> ' +
						                '    <DataAbertura>03/11/2005</DataAbertura> ' +
						                '    <Nome>ALCATEIA ENGENHARIA DE SISTEMAS LTDA</Nome> ' +
						                '    <NomeFantasia>********</NomeFantasia> ' +
						                '    <Cod.Ativ.Principal>46.51-6-01 - Com�rcio atacadista de equipamentos de inform�tica</Cod.Ativ.Principal> ' +
						                '    <Desc.Ativ.Principal>206-2 - SOCIEDADE EMPRESARIA LIMITADA</Desc.Ativ.Principal> ' +
						                '    <Cod.Ativ.Secund�ria>206-2</Cod.Ativ.Secund�ria> ' +
						                '    <Desc.Ativ.Secund�ria>Com�rcio atacadista de suprimentos para inform�tica</Desc.Ativ.Secund�ria> ' +
						                '    <Cod.NaturezaJur�dica>46.51-6-02</Cod.NaturezaJur�dica> ' +
						                '    <Desc.NaturezaJur�dica>Com�rcio atacadista de equipamentos de inform�tica</Desc.NaturezaJur�dica> ' +
						                '    <Logradouro>R DOS ITALIANOS</Logradouro> ' +
						                '    <Numero>1.127</Numero> ' +
						                '    <Complemento>R.ANHAIA, 1142 E 1116</Complemento> ' +
						                '    <CEP>01.131-000</CEP> ' +
						                '    <Bairro>SAO PAULO</Bairro> ' +
						                '    <Munic�pio>BOM RETIRO</Munic�pio> ' +
						                '    <UF>SP</UF> ' +
						                '    <Situa��o>ATIVA</Situa��o> ' +
						                '    <DataSitua��o>13/10/1986</DataSitua��o> ' +
						                '    <Situa��oEspecial>********</Situa��oEspecial> ' +
						                '    <DataEspecial>********</DataEspecial> ' +
						                '    <MotivoSitua��o></MotivoSitua��o> ' +
						                '    <Data>10/17/2008</Data> ' +
						                '    <Hora>8:43:05 AM</Hora> ' +
						                '    <Consulta>1</Consulta> ' +
					                    '</ReceitaFederal> ' +
				                    '</DadosPJ> ' +
			                    '</retornaDadosPJResult> ' +
		                    '</retornaDadosPJResponse> ' +
	                    '</soap:Body> ' +
                    '</soap:Envelope>';
    }
 
    return sXML;   
}

function formatStr(str)
{
      var aLowerCase = new Array('DO', 'DA', 'DOS', 'DAS', 'DE', 'PARA', 'P/', 'E', 'A');
      var retVal = '';
      var i = 0;
      str = removeDiatricsOnJava(trimStr(str));
    aStr = str.split(' ');
    
    for (i=0; i<aStr.length; i++)
    {
            if (aseek(aLowerCase, aStr[i]) >= 0)
            {
                  retVal += (retVal == '' ? '' : ' ') + aStr[i].toLowerCase();
                  continue;
            }
            
            retVal += (retVal == '' ? '' : ' ') + formatWord(aStr[i]);
    }
    
    return retVal;
}

function formatWord(sWord)
{
      return sWord.substr(0, 1).toUpperCase() + sWord.substr(1, sWord.length + 1).toLowerCase();
}

function validacaoDados_AtualizaPessoa()
{   
	var aEmpresaData = getCurrEmpresaData();	
	
	var strPars = new String(); 

    strPars = '?nValidacaoDadoID='+escape(glb_ValidacaoDadoID);        
    strPars += '&nPaisID='+escape(aEmpresaData[1]);
                      
   try
    {
        dsoAtualizaPessoa.URL = SYS_ASPURLROOT + '/serversidegenEx/atualizapessoaconfirmacaocadastro.aspx' + strPars;    
        dsoAtualizaPessoa.ondatasetcomplete = validacaoDados_AtualizaPessoa_DSC;
        dsoAtualizaPessoa.refresh();    
    }
    catch(e)
    {
        if ( window.top.overflyGen.Alert('Erro na atualiza��o dos dados.') == 0 )
			return null;
		
		consultaSerasaEnded(false);    
			
        return;			
    }   
}
function validacaoDados_AtualizaPessoa_DSC() {
    if ((dsoAtualizaPessoa.recordset['Resultado'].value != null) && (dsoAtualizaPessoa.recordset['Resultado'].value != ''))
    {
        if ( window.top.overflyGen.Alert(dsoAtualizaPessoa.recordset['Resultado'].value) == 0 )
            return null;        
    }
    else
    {
        if ( window.top.overflyGen.Alert ('Consulta realizada com sucesso') == 0 )
            return null;
    }
   
    consultaSerasaEnded(true);    
}
