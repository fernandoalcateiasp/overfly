// Objeto OverHtmlMounter===================================================

function _InitializeHtmlMounter() {
    this.sTextBase = '';
    this.sInnerHTML = '';
    this.bInnerHTMLMounted = false;
    this.nParseCounter = 0;
    this.aIDTextHint = null;
    this.aTextHintAuto = null;
    this.aAnchorText = null;
    this.sBgColor_Incl = '#98FB98';
    //this.sBgColor_Alt = '#EEE8AA';
    //this.sBgColor_Alt = '#E5DE9C';
    this.sBgColor_Alt = '#FFFF00';
    this.sBgColor_Excl = '#FFA07A';
    this.sBgColor_Duv = '#58ACFA';
    this.nIncremental = 0;
}

function _setArrayIDTextHintManual(aArray) {
    this.aIDTextHint = null;
    this.aIDTextHint = aArray;
}

function _setArrayTextHintAuto(aArray) {
    this.aTextHintAuto = null;
    this.aTextHintAuto = aArray;
}

function _SetTextBase(textBase) {
    if (textBase == null)
        textBase = '';

    this.sTextBase = textBase;
    this.bInnerHTMLMounted = false;
}

function _GetInnerHTML() {
    return this.sInnerHTML;
}

function __setBGDefinedColors(sText) {
    var sOpenTagFont_Incl = '<+>';
    var sCloseTagFont_Incl = '</+>';

    var sOpenTagFont_Alt = '<' + String.fromCharCode(42) + '>';
    var sCloseTagFont_Alt = '</' + String.fromCharCode(42) + '>';

    var sOpenTagFont_Excl = '<->';
    var sCloseTagFont_Excl = '</->';

    var sOpenTagFont_Duv = '<' + String.fromCharCode(63) + '>';
    var sCloseTagFont_Duv = '</' + String.fromCharCode(63) + '>';

    var sTmpText = '';

    var i;
    var numLoops = 8;
    var sTagToFind = '';
    var sTagToReplace = '';
    var sTagOpenPart1 = '<FONT STYLE=' + String.fromCharCode(34) + 'BACKGROUND-COLOR:';
    var sTagClosePart1 = '</FONT>';

    sTmpText = sText;

    for (i = 0; i < numLoops; i++) {
        if (i == 0) {
            sTagToFind = sOpenTagFont_Incl;
            sTagToReplace = sTagOpenPart1 + this.sBgColor_Incl + String.fromCharCode(34) + '>';
        }
        else if (i == 1) {
            sTagToFind = sCloseTagFont_Incl;
            sTagToReplace = sTagClosePart1;
        }
        else if (i == 2) {
            sTagToFind = sOpenTagFont_Alt;
            sTagToReplace = sTagOpenPart1 + this.sBgColor_Alt + String.fromCharCode(34) + '>';
        }
        else if (i == 3) {
            sTagToFind = sCloseTagFont_Alt;
            sTagToReplace = sTagClosePart1;
        }
        else if (i == 4) {
            sTagToFind = sOpenTagFont_Excl;
            sTagToReplace = sTagOpenPart1 + this.sBgColor_Excl + String.fromCharCode(34) + '>';
        }
        else if (i == 5) {
            sTagToFind = sCloseTagFont_Excl;
            sTagToReplace = sTagClosePart1;
        }
        else if (i == 6) {
            sTagToFind = sOpenTagFont_Duv;
            sTagToReplace = sTagOpenPart1 + this.sBgColor_Duv + String.fromCharCode(34) + '>';
        }
        else if (i == 7) {
            sTagToFind = sCloseTagFont_Duv;
            sTagToReplace = sTagClosePart1;
        }

        while (sTmpText.indexOf(sTagToFind) >= 0) {
            sTmpText = sTmpText.substr(0, sTmpText.indexOf(sTagToFind)) + sTagToReplace +
				sTmpText.substr(sTmpText.indexOf(sTagToFind) + sTagToFind.length);
        }
    }

    return sTmpText;
}

function _mountInnerHTML() {
    if (this.bInnerHTMLMounted)
        return this.bInnerHTMLMounted;

    if ((this.sTextBase == null) || (this.sTextBase == '')) {
        this.sInnerHTML = '';
        this.bInnerHTMLMounted = false;

        return this.bInnerHTMLMounted;
    }

    var sTmpText = this.sTextBase;
    var sTmpText_1 = sTmpText;
    var nAbertura, nFechamento, nSeparador, nOnlyID, nHasID;
    var sChar;
    var sTheID;

    sTmpText = this._setBGDefinedColors(sTmpText_1);

    this.bInnerHTMLMounted = false;
    this.sInnerHTML = '';

    this.nParseCounter = 0;

    this.nIncremental = 10000000;

    while (sTmpText != '') {
        nAbertura = sTmpText.indexOf('{');
        nSeparador = sTmpText.indexOf('^');
        nFechamento = sTmpText.indexOf('}');
        nOnlyID = sTmpText.indexOf('{^I(');
        nHasID = sTmpText.indexOf('^I(');

        this.nIncremental++;

        if (nAbertura >= 0) {
            if ((nSeparador > nAbertura) && (nFechamento > nSeparador)) {
                if ((nHasID >= 0) && (nHasID < nFechamento)) {
                    sTheID = '';
                    sChar = '';

                    nHasID += 3;

                    while (sChar != ')') {
                        sTheID = sTheID + sChar;

                        sChar = sTmpText.substr(nHasID++, 1);
                    }
                }
                else {
                    sTheID = this.nIncremental.toString();
                }

                this.sInnerHTML = this.sInnerHTML + sTmpText.substr(0, nAbertura) + this._createDivAnchor('_D_' + sTheID.toString());

                if (!(nOnlyID == nAbertura)) {
                    // this.sInnerHTML = this.sInnerHTML + sTmpText.substr(0,nAbertura) + 
                    // 	this._parseStrHTML(sTmpText.substr(nAbertura, nFechamento-nAbertura+1), '_A_' + sTheID.toString());
                    this.sInnerHTML = this.sInnerHTML +
						this._parseStrHTML(sTmpText.substr(nAbertura, nFechamento - nAbertura + 1), '_A_' + sTheID.toString());

                }

                sTmpText = sTmpText.substr(nFechamento + 1);
            }
            else {
                this.sInnerHTML = this.sInnerHTML + sTmpText.substr(0, Math.max(nAbertura, nSeparador, nFechamento));
                sTmpText = sTmpText.substr(Math.max(nAbertura, nSeparador, nFechamento) + 1);
            }
        }
        else {
            this.sInnerHTML = this.sInnerHTML + sTmpText;
            sTmpText = '';
        }
    }

    this.bInnerHTMLMounted = true;

    return this.bInnerHTMLMounted;
}

function _createDivsForImgsMaps() {
    var i, j;
    var collAreas, elemArea, elemDiv;
    var aCoords;
    var collImgs;
    var refImg, refMap;
    var imgLeft, imgTop;

    collAreas = window.document.getElementsByTagName('AREA');
    collImgs = window.document.getElementsByTagName('IMG');

    if (collAreas == null)
        return null;

    for (i = 0; i < collAreas.length; i++) {
        elemArea = collAreas.item(i);

        this.nIncremental++;
        sDivID = '_D_' + this.nIncremental;

        elemDiv = this._createDivAnchorDynamic(sDivID);

        // associa a area e seu div correspondente atravez attributos
        elemArea.setAttribute('divAssociated', elemDiv.id, 1);
        elemArea.setAttribute('numericOfDivAssociated', this.nIncremental, 1);
        elemDiv.setAttribute('areaAssociated', elemArea.id, 1);

        // posiciona o div
        // obtem as coordenadas da area
        aCoords = null;
        aCoords = elemArea.coords.split(',');
        arealeft = aCoords[0];
        areatop = aCoords[1];

        // obtem o map e verifica qual imagem esta usando aquele map
        // para obter as coordenadas da imagem
        refMap = elemArea.parentElement;

        for (j = 0; j < collImgs.length; j++) {
            refImg = collImgs.item(j);

            if (refImg.useMap.substr(1) == refMap.id) {
                imgLeft = refImg.offsetLeft;
                imgTop = refImg.offsetTop;
                break;
            }
        }

        with (elemDiv.style) {
            left = parseInt(aCoords[0], 10) + imgLeft;
            top = parseInt(aCoords[1], 10) + imgTop;
        }
    }
}

function __parseStrHTML(strToParse, sAnchorID) {
    var sReturn = '';
    var aParsedStr = new Array();
    var sText = '';
    var sHint = '';
    var sGoTo = '';
    var sAnchor = '';
    var bBtnVoltar = false;
    var sBtnVoltar = '';
    var sDivIDAnchorAss = '';
    var i = 0;
    var nPesq;

    this.nParseCounter++;

    // Remove as chaves
    strToParse = strToParse.substr(1);
    strToParse = strToParse.substr(0, strToParse.length - 1);

    // Alteracao de codigo em 06/06/2003
    // para n�o remover # em texto, so em comando
    // Remove o tralha
    // strToParse = removeCharFromString(strToParse, '#');
    // Final de alteracao de codigo em 06/06/2003

    nPesq = strToParse.indexOf('^H(#');
    if (nPesq >= 0) {
        strToParse = strToParse.substr(0, nPesq + 3) + strToParse.substr(nPesq + 4, strToParse.length);
    }

    // Da parse nos comandos
    aParsedStr = strToParse.split('^');
    // Trap, nao tem nenhum comando
    if (aParsedStr.length == 0) {
        sReturn = strToParse;
        return sReturn;
    }

    // Exemplo de sintaxe de comando completo
    // {#12^H(#10)^G(423)^A(123)^I(456)^R()}
    // ou
    // {Texto^H(Texto)^G(423)^A(123)^I(456)^R()}
    // ou
    // {^I(456)}Texto
    // O objeto esta sendo modificado conforme abaixo
    // Substitui
    // {#12^H(#10)^G(423)^A(123)^I(456)}
    // ou
    // {Texto^H(Texto)^G(423)^A(123)^I(456)}
    // Por
    // {Texto^H(#Texto)^G(423)^A(123)^I(456)}
    // #Texto significa que o termo e o hint (se houver) vao para
    // a TABELA 3 - DEFINICOES

    // O texto
    sText = this._getTextPart(aParsedStr[0], 1);
    for (i = 1; i < aParsedStr.length; i++) {
        // O simulacro de botao Voltar
        if (aParsedStr[i].indexOf('R()') >= 0) {
            bBtnVoltar = true;
            /*
			if ( aParsedStr[i].indexOf(')') < aParsedStr[i].indexOf('R(') )
				sBtnVoltar = '';
			else	
				sBtnVoltar = this._getTextPart( aParsedStr[i].substr(2, aParsedStr[i].indexOf(')')-2), 2);
			*/
        }
            // O hint
        else if (aParsedStr[i].indexOf('H(') >= 0)
            if (aParsedStr[i].indexOf(')') < aParsedStr[i].indexOf('H('))
                sHint = '';
            else
                sHint = this._getTextPart(aParsedStr[i].substr(2, aParsedStr[i].indexOf(')') - 2), 2);
            // O jump para outra pagina
        else if (aParsedStr[i].indexOf('G(') >= 0) {
            if (aParsedStr[i].indexOf(')') < aParsedStr[i].indexOf('G('))
                sGoTo = '';
            else
                sGoTo = aParsedStr[i].substr(aParsedStr[i].indexOf('G(') + 2, aParsedStr[i].indexOf(')') - 2);
        }
            // o jump na mesma pagina
        else if (aParsedStr[i].indexOf('A(') >= 0) {
            if (aParsedStr[i].indexOf(')') < aParsedStr[i].indexOf('A('))
                sAnchor = '';
            else
                sAnchor = '\'' + aParsedStr[i].substr(aParsedStr[i].indexOf('A(') + 2, aParsedStr[i].indexOf(')') - 2) + '\'';
        }
    }

    sReturn = '<A ID=' + '\'' + sAnchorID + '\'' + ' ';

    if (sHint != '')
        sReturn += 'TITLE=' + '\'' + sHint + '\'' + ' ';
    else {
        if (sGoTo != '')
            sReturn += 'TITLE=' + '\'' + this._hintFromProgrammer(sGoTo) + '\'' + ' ';
    }

    sReturn += 'HREF=' + String.fromCharCode(34);

    if (sGoTo != '') {
        sDivIDAnchorAss = '_D_' + sAnchorID.substr(3);
        sDivIDAnchorAss = sAnchorID.substr(3);

        if (sAnchor != '') {
            if (sGoTo != '0')
                sReturn += 'javascript:goto(' + '\'' + sDivIDAnchorAss + '\'' + ', ' + sGoTo + ', ' + sAnchor + ', 1)';
            else
                sReturn += 'javascript:gotoAnchor(' + '\'' + sDivIDAnchorAss + '\'' + ', ' + sAnchor + ', 1)';
        }
        else
            sReturn += 'javascript:goto(' + '\'' + sDivIDAnchorAss + '\'' + ', ' + sGoTo + ', null, 1)';
    }
    else {
        sDivIDAnchorAss = '_D_' + sAnchorID.substr(3);
        sDivIDAnchorAss = sAnchorID.substr(3);

        if (sAnchor != '')
            sReturn += 'javascript:gotoAnchor(' + '\'' + sDivIDAnchorAss + '\'' + ', ' + sAnchor + ', 1)';
        else {
            if (bBtnVoltar)
                sReturn += sBtnVoltar + ' javascript:gotoBtnBack()';
            else
                sReturn += 'javascript:gotoNull()';
        }
    }

    sReturn += String.fromCharCode(34) + '>' + sText + '</A>';

    return sReturn;
}

function gotoBtnBack() {
    if (!window.parent.btnVoltar.disabled)
        window.parent.btn_onclick(window.parent.btnVoltar);
}

function __getTextPart(sText, nTipo) {
    var nID = 0;
    var sRetText = '';

    if (sText == '')
        return sText;

    // Nao tem tralha retorna como texto
    if (sText.indexOf('#') < 0)
        return sText;

    // Tem tralha e nao seguida de numero
    if (sText.indexOf('#') >= 0) {
        if (isNaN(sText.substr(sText.indexOf('#') + 1)))
            return sText;

        nID = parseInt(sText.substr(sText.indexOf('#') + 1), 10);

        if (this.aIDTextHint.length == 0)
            return sText;

        sRetText = this._loopInArray(this.aIDTextHint, nID, nTipo);

        if (sRetText == null)
            return sText;
        else
            return sRetText;
    }
}

function __loopInArray(aArr, nID, nTipo) {
    var i;

    for (i = 0; i < aArr.length; i++) {
        if (aArr[i][0] == nID) {
            return aArr[i][nTipo];
            break;
        }
    }

    return null;
}

function __createDivAnchor(sDivID) {
    return '<DIV ID=' + '\'' + sDivID + '\'' + ' ' +
	       'CLASS=' + '\'' + 'divAnchor' + '\'' + '></DIV>';
}

function __createDivAnchorDynamic(sDivID) {
    var elem;

    elem = document.createElement('DIV');
    elem.name = sDivID;
    elem.className = 'divAnchor';
    elem.id = sDivID;

    // acrescenta os elementos
    window.document.body.appendChild(elem);

    return elem;
}

function __hintFromProgrammer(sGoTo) {
    var retVal = '';

    var i;
    var aArr = this.aTextHintAuto;

    for (i = 0; i < aArr.length; i++) {
        if (aArr[i][0] == sGoTo) {
            retVal = aArr[i][1];
            break;
        }
    }

    return retVal;
}

function __getAnchor(sAnchor) {
    var i;

    var coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        if (coll[i].id == '_D_' + sAnchor.toString()) {
            return coll[i];
        }
    }

    return null;
}

function _heigthHTMLInPixels() {
    var i;

    var coll = window.document.getElementsByTagName('DIV');

    for (i = 0; i < coll.length; i++) {
        if (coll[i].id == '_D_LAST_DIV_') {
            return coll[i].offsetTop;
        }
    }

    return 0;
}

// Exemplo de sintaxe de comando completo
// {#12^H(#10)^G(423)^A(123)^I(456)}
// ou
// {Texto^H(Texto)^G(423)^A(123)^I(456)}
// ou
// {^I(456)}Texto
// O objeto esta sendo modificado conforme abaixo
// Substitui
// {#12^H(#10)^G(423)^A(123)^I(456)}
// ou
// {Texto^H(Texto)^G(423)^A(123)^I(456)}
// Por
// {Texto^H(#Texto)^G(423)^A(123)^I(456)}
// #Texto significa que o termo e o hint (se houver) vao para
// a TABELA 3 - DEFINICOES

function _OverHtmlMounter() {
    // metodos para uso do programador
    this.initializeHtmlMounter = _InitializeHtmlMounter;
    this.setArrayIDTextHintManual = _setArrayIDTextHintManual;
    this.setArrayTextHintAuto = _setArrayTextHintAuto;
    this.setTextBase = _SetTextBase;
    this.getInnerHTML = _GetInnerHTML;
    this.mountInnerHTML = _mountInnerHTML;
    this.heigthHTMLInPixels = _heigthHTMLInPixels;
    this.createDivsForImgsMaps = _createDivsForImgsMaps;

    // variaveis internas do objeto
    this.sTextBase = new String();
    this.sInnerHTML = new String();
    this.bInnerHTMLMounted = false;
    this.nParseCounter = 0;
    this.aIDTextHint = null;
    this.aTextHintAuto = null;
    this.aAnchorText = null;
    this.sBgColor_Incl = '#98FB98';
    //this.sBgColor_Alt = '#EEE8AA'; // Solicitado por Eduardo Rodrigues - VRM 01/07/2016
    //this.sBgColor_Alt = '#E5DE9C'; // Solicitado por Eduardo Rodrigues e Marco Fortunado - GHSR 13/02/2017
    this.sBgColor_Alt = '#FFFF00';
    this.sBgColor_Excl = '#FFA07A';
    this.sBgColor_Duv = '#58ACFA';
    this.nIncremental = 0;

    // funcoes internas do objeto
    this._setBGDefinedColors = __setBGDefinedColors;
    this._parseStrHTML = __parseStrHTML;
    this._loopInArray = __loopInArray;
    this._createDivAnchor = __createDivAnchor;
    this._getAnchor = __getAnchor;
    this._createDivAnchorDynamic = __createDivAnchorDynamic;
    this._hintFromProgrammer = __hintFromProgrammer;

    this._parseStrHTML = __parseStrHTML;
    this._getTextPart = __getTextPart;

    // Executa na criacao do objeto
    _InitializeHtmlMounter();
}

// Final do Objeto OverHtmlMounter==========================================