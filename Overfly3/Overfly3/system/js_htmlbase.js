/********************************************************************
js_htmlbase.js

Library javascript de funcoes para htmls, elementos e frames

AS FUNCOES PREVEEM QUE:
1. TODO O SISTEMA TEM APENAS UM NIVEL DE FRAMES
2. TODOS OS FRAMES SAO IFRAMES
********************************************************************/

/********************************************************************
INDICE DAS FUNCOES:
Frames:
    getFrameInHtmlTop(frameID)
    getPageFrameInHtmlTop(frameID)
    showFrameInHtmlTop(frameID, cmdShow)
    showAllFramesInHtmlTop(cmdShow)
    moveFrameInHtmlTop(frameID, frameRect)
    getRectFrameInHtmlTop(frameID)
    getExtFrameID(thisWin)
    showExtFrame(thisWin, cmdShow)
    moveExtFrame(thisWin, frameRect)
Htmls:
    getFrameIdByHtmlId(htmlID)
    loadURLInFrame(frameID, urlPath)
    htmlIdsVisibles(win)
    getHtmlId()
    getBodyRef()
Divs:
    showDiv(htmlID, divID, cmdShow)
    showDivArray(htmlID, divIDArray, cmdShow) 
    mostRightElemInDiv(divRef)
    mostBottomElemInDiv(divRef)
Elements:
    trimAllImputFields(win)
    labelAssociate(idControl)
    focusNextFld()
Selects:
    selOptByValueInSelect(theHTML, comboID, optionValue)
    getCurrDataSelect(htmlID, comboID)
    keyDownInSel()
    preventBkspcInSel(win)
Carregamento de paginas de forms no browser corrente ou em browsers filhos
    loadPage(idElement, param1, param2)
    openForm(whoCallTheFunc, windowTopName, formName, arqStart, frameStart, newChildBrowser)
Dicionario:
	getDicCurrLang()
	setDicCurrLang(nLang)
	translateInterface(win, nLangTo)
	translateTerm(sTerm, nLangTo)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Retorna uma referencia a um frame do html top.

Parametros:
frameID             id do frame a retornar a referencia

Retornos:
null se o frame nao foi encontrado, em caso de sucesso retorna a refe_
rencia ao frame.

Nota:
A fun��o percorre todo o sistema, desta forma, se tiver mais de um
frame no html top com o mesmo ID, a fun��o retorna a referencia ao
primeiro frame encontrado.
********************************************************************/
function getFrameInHtmlTop(frameID)
{
    var winNav = window.top;
    var i;
    
    if (winNav.frames != null)
    {
        for (i=0; i<winNav.frames.length; i++)
        {
            if (winNav.document.all.tags("IFRAME").item(i).id == frameID)
            {
                // retorna referencia ao frame
                return winNav.document.all.tags("IFRAME").item(i);
                break;
            }
        }
    }
    return null;
}

/********************************************************************
Retorna uma referencia ao html contido em um frame do html top.

Parametros:
frameID             id do frame a retornar a referencia ao html

Retornos:
null se o frame nao foi encontrado, em caso de sucesso retorna a refe_
rencia ao html contido no frame.
Se o frame nao contem html retorna ''.

Nota:
A fun��o percorre todo o sistema, desta forma, se tiver mais de um
frame no html top com o mesmo ID, a fun��o retorna a referencia ao
html do primeiro frame encontrado.
********************************************************************/
function getPageFrameInHtmlTop(frameID)
{
    var winNav = window.top;
    var i;
       
    if (winNav.frames != null)
    {
        for ( i=0; i<winNav.frames.length; i++ )
        {
            if ( winNav.document.all.tags("IFRAME").item(i).id == frameID )
            {
                if (winNav.document.all.tags("IFRAME").item(i).src == '')
                {
                    // o frame n�o tem html carregado
                    return '';
                }
                // o frame tem html carregado
                return winNav.document.frames(i);
            }
        }
    }
    return null;
}

/********************************************************************
Retorna o id do frame que contem um dado html.

Parametros:
htmlID              id da tag do html a retornar

Retornos:
null se n�o encontrou o frame ou o id do frame

Nota:
A funcao percorre todo o sistema, desta forma, se tiver mais de um
html com o mesmo ID, a fun��o retorna uma refer�ncia ao primeiro 
que encontrar, contido em frame.
********************************************************************/
function getFrameIdByHtmlId(htmlID)
{
    var wndTop = window.top;
    var i = 0;
    var j = 0;    
    var htmlTagColl;

    if ( htmlID == null )
        return null;
        
    // nao tem html?
    if (wndTop == null)
        return null;
        
    // atua sobre wndTop?
    if (wndTop.document.getElementById(htmlID))
        return wndTop.document.getElementById(htmlID).id;
        
    // verifica se wndTop tem frames e para cada frame encontrado
    // verifica se tem html carregado. Se tem html verifica se o ID do
    // html == htmlID, se for atua sobre.
    if (wndTop.frames != null)
    {
        for ( i=0; i<wndTop.frames.length; i++ )
        {
            if (wndTop.document.all.tags("IFRAME").item(i).src != '')
            {
                htmlTagColl = wndTop.frames.item(i).document.all.tags('HTML');
                for(j=0; j<htmlTagColl.length; j++)
                {
                    if (htmlTagColl.item(j).id == htmlID)
                    {
                        //return wndTop.frames.item(i).name;
                        return wndTop.document.all.tags("IFRAME").item(i).id;
                    }    
                }
            }    
        }
    }
    
    return null;
}

/********************************************************************
Carrega um html em um dado frame do html top.
O html top e aquele que contem os frames mais externos do sistema.

Parametros:
frameID             id do frame a tornar visivel ou invisivel
urlPath             full path da p�gina a carregar. NOTA: este
                    fullpath tem que ser absoluto, ou seja,
                    o caminho do html que contem o frame ate a
                    p�gina a ser carregada.

Retornos:
nenhum

Nota:
A fun��o percorre todo o sistema, desta forma, se tiver mais de um
frame no html top com o mesmo ID, a fun��o retorna a referencia ao
primeiro frame encontrado.
********************************************************************/
function loadURLInFrame(frameID, urlPath)
{
    if ( (urlPath == null) || (urlPath == '') )
        return null;
    
    var frameToFill;
    
    frameToFill = getFrameInHtmlTop(frameID);
    
    if (frameToFill != null)
    {
        if ( frameToFill.src == '' )
            frameToFill.src = urlPath;
        else
        {
            if ( urlPath.indexOf('http://') >=0 )
                top.frames(frameToFill.name).document.location.replace(urlPath);
            else
                top.frames(frameToFill.name).document.location.replace(SYS_PAGESURLROOT + '/' + urlPath);
        }            
    }    
}

/********************************************************************
Esta funcao torna visivel ou invisivel um frame do html top.
O html top e aquele que contem os frames mais externos do sistema.

Parametros:
frameID             id do frame a tornar visivel ou invisivel
cdmShow             true (torna visivel)/false (torna invisivel)

Retornos:
zero se n�o atuou sobre nenhum frame ou o n�mero de frames sobre os
quais atuou.

Nota:
A fun��o atua sobre todos os frames do html mais externo do sistema,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre todos os encontrados.
********************************************************************/
function showFrameInHtmlTop(frameID, cmdShow)
{
    var wndTop = window.top;
    var i = 0;
    var retVal = 0;
    var framesColl = 0;
    
    // nao tem html?
    if (wndTop == null)
        return retVal;
   
    // loop nos frames
    framesColl = wndTop.document.all.tags("IFRAME");
    for ( i=0; i<framesColl.length; i++ )
    {
        framesColl.item(i).style.zIndex = 0;
        
        if (framesColl.item(i).id == frameID)
        {
            (cmdShow && true)? framesColl.item(i).style.visibility = 'visible' : framesColl.item(i).style.visibility = 'hidden';
            
            if (framesColl.item(i).style.visibility == 'visible')
            {
                framesColl.item(i).style.zIndex = 1;
                framesColl.item(i).focus();
            }
            else
            {
				framesColl.item(i).style.zIndex = -1;
			}
				
            retVal++;
        }
    }

    return retVal;
}

/********************************************************************
Torna visivel ou invisivel todos os frames do html top.
O html top e aquele que contem os frames mais externos do sistema.

Parametros:
cdmShow             true (torna visivel)/false (torna invisivel)

Retornos:
zero se n�o atuou sobre nenhum frame ou o n�mero de frames sobre os
quais atuou.

Nota:
A fun��o atua sobre todos os frames do html mais externo do sistema,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre todos os encontrados.
********************************************************************/
function showAllFramesInHtmlTop(cmdShow)
{
    var wndTop = window.top;
    var i = 0;
    var retVal = 0;
    var framesColl = 0;
    
    // nao tem html?
    if (wndTop == null)
        return retVal;
   
    // loop nos frames
    framesColl = wndTop.document.all.tags("IFRAME");
    for ( i=0; i<framesColl.length; i++ )
    {
        (cmdShow && true)? framesColl.item(i).style.visibility = 'visible' : framesColl.item(i).style.visibility = 'hidden';
        retVal++;
    }

    return retVal;
}

/********************************************************************
Move e redimensiona um frame do html top.
O html top e aquele que contem os frames mais externos do browser.

Parametros:
frameID            id do frame a tornar visivel ou invisivel
frameRect          array das coordenadas e dimensoes do frame
                   frameRect[0] = left em pixels
                   frameRect[1] = top em pixels
                   frameRect[2] = width em pixels
                   frameRect[3] = height em pixels

Retornos:
zero se n�o atuou sobre nenhum frame ou o n�mero de frames sobre os
quais atuou.

Nota:
A fun��o atua sobre todos os frames do html mais externo do browser,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre todos os encontrados.
********************************************************************/
function moveFrameInHtmlTop(frameID, frameRect)
{
    var wndTop = window.top;
    var i = 0;
    var retVal = 0;
    var framesColl = 0;
    
    // nao tem html?
    if (wndTop == null)
        return retVal;
   
    // loop nos frames
    framesColl = wndTop.document.all.tags("IFRAME");
    for ( i=0; i<framesColl.length; i++ )
    {
        if (framesColl.item(i).id == frameID)
        {
            switch (frameRect.length)
            {
                case 4:
                {
                    framesColl.item(i).style.height = frameRect[3];
                    framesColl.item(i).style.width = frameRect[2];
                    framesColl.item(i).style.top = frameRect[1];
                    framesColl.item(i).style.left = frameRect[0];
                }    
                break;
                
                case 3:
                {
                    framesColl.item(i).style.width = frameRect[2];
                    framesColl.item(i).style.top = frameRect[1];
                    framesColl.item(i).style.left = frameRect[0];
                }
                
                case 2:
                {
                    framesColl.item(i).style.top = frameRect[1];
                    framesColl.item(i).style.left = frameRect[0];
                }    
                break;
                
                case 1:
                {
                    framesColl.item(i).style.left = frameRect[0];
                }    
                break;
            }    
            retVal++;
        }
    }

    return retVal;
}

/********************************************************************
Retorna as dimensoes um frame do html top.
O html top e aquele que contem os frames mais externos do sistema.

Parametros:
frameID             id do frame a retornar as dimensoes

Retornos:             o retorno e um array do tipo:
                    frameRect[0] = left em pixels
                    frameRect[1] = top em pixels
                    frameRect[2] = width em pixels
                    frameRect[3] = height em pixels
                    ou zero se n�o encontrou o frame solicitado.

Nota:
A fun��o atua sobre todos os frames do html mais externo do sistema,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre todos os encontrados.

ATENCAO: PARA A FUNCAO FUNCIONAR NAO BASTA DEFINIR AS DIMENSOES DOS
FRAMES NO ARQUIVO CSS QUE ACOMPANHA O HTML. E PRECISO FAZER O MESMO
NO WINDOW ONLOAD DO HTML.
********************************************************************/
function getRectFrameInHtmlTop(frameID)
{
    var wndTop = window.top;
    var i = 0;
    var j = 0;
    var rectVal = new Array();
    var framesColl = 0;
    
    // nao tem html?
    if (wndTop == null)
        return rectVal;
   
    // loop nos frames
    framesColl = wndTop.document.all.tags("IFRAME");
        
    for ( i=0; i<framesColl.length; i++ )
    {
        if (framesColl.item(i).id == frameID)
        {
            rectVal[j] = parseInt(framesColl.item(i).currentStyle.left, 10);
            if ( isNaN(rectVal[j]))
                rectVal[j] = 0;
            j++;
            rectVal[j] = parseInt(framesColl.item(i).currentStyle.top, 10);
            if ( isNaN(rectVal[j]))
                rectVal[j] = 0;
            j++;
            rectVal[j] = parseInt(framesColl.item(i).currentStyle.width, 10);
            if ( isNaN(rectVal[j]))
                rectVal[j] = 0;
            j++;
            rectVal[j] = parseInt(framesColl.item(i).currentStyle.height, 10);
            if ( isNaN(rectVal[j]))
                rectVal[j] = 0;
        }
    }
    
    return rectVal;
}

/********************************************************************
Retorna o id do frame que contem um html.

Parametros:
thisWin             window do html ou asp onde a funcao foi chamada.

Retornos:
o id do frame ou null se nao ha frame externo.

Nota:
A fun��o atua sobre todos os frames do html mais externo do sistema,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre o primeiro encontrado.
Esta funcao usa a funcao interna da biblioteca:
getFrameIdByFrameNameInHtmlTop(thisWin, frameName)
********************************************************************/
function getExtFrameID(thisWin)
{
    return getFrameIdByFrameNameInHtmlTop(thisWin, thisWin.document.parentWindow.name);
}

/********************************************************************
Mostra ou esconde o frame que contem um html.

Parametros:
thisWin             window do html ou asp onde a funcao foi chamada.
cmdShow             true (mostra o frame) / false (esconde o frame) 

Retornos:
nenhum

Nota:
A fun��o atua sobre todos os frames do html mais externo do sistema,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre o primeiro encontrado.
Esta funcao usa a funcao interna da biblioteca:
getFrameIdByFrameNameInHtmlTop(thisWin, frameName)
********************************************************************/
function showExtFrame(thisWin, cmdShow)
{
    // recupera o id do frame que contem o html
    var extFrame = getFrameIdByFrameNameInHtmlTop(thisWin, thisWin.document.parentWindow.name);
    if ( extFrame != null )
    {
        showFrameInHtmlTop(extFrame, cmdShow);
        thisWin.focus();
    }
}

/********************************************************************
Esta funcao move e redimensiona o frame que contem um html.

Parametros:
thisWin             window do html ou asp onde a funcao foi chamada.
frameRect           array das coordenadas e dimensoes do frame
                    frameRect[0] = left em pixels
                    frameRect[1] = top em pixels
                    frameRect[2] = width em pixels
                    frameRect[3] = height em pixels

Retornos:
nenhum

Nota:
A fun��o atua sobre todos os frames do html mais externo do sistema,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre o primeiro encontrado.
Esta funcao usa a funcao interna da biblioteca:
getFrameIdByFrameNameInHtmlTop(thisWin, frameName)
********************************************************************/
function moveExtFrame(thisWin, frameRect)
{
    // recupera o id do frame que contem o html
    var extFrame = getFrameIdByFrameNameInHtmlTop(thisWin, thisWin.document.parentWindow.name);
    if ( extFrame != null )
        moveFrameInHtmlTop(extFrame, frameRect);
}

/********************************************************************
Retorna o id da tag HTML da pagina carregada no modal

Parametros:
nenhum

Retornos:
'' ou o id da tag HTML
********************************************************************/
function htmlLoadedInModal()
{
    var pgInFrame = getPageFrameInHtmlTop('frameModal');
        
    if ( pgInFrame != '' )
        return pgInFrame.getHtmlId();
    
    return '' ;
}

/********************************************************************
Esta funcao torna visivel ou invisivel um div de um dado html.

Parametros:
htmlID              id do html que contem o div
divID               id do div
cdmShow             true (torna visivel)/false (torna invisivel)

Retornos:
zero se n�o atuou sobre nenhum div ou o n�mero de divs sobre os
quais atuou

Nota:
A fun��o percorre todo o sistema, desta forma, se tiver mais de um
html com o mesmo ID e/ou divs com o mesmo id no html,
a fun��o atua sobre todos os divs encontrados.
********************************************************************/
function showDiv(htmlID, divID, cmdShow)
{
    var wndTop = window.top;
    var htmlScript = null;
    var i = 0;
    var j = 0;
    var elem = null;
    var retVal = 0;
        
    // nao tem html?
    if (wndTop == null)
        return retVal;
        
    // atua sobre wndTop?
    htmlScript = wndTop.document.getElementById(htmlID); 
    if (htmlScript)
    {
        // loop nos divs
        elem = htmlScript.getElementsByTagName('DIV');
        for ( j=0; j<elem.length; j++ )
        {
            if ( elem(j).id == divID )
            {
                (cmdShow && true)? elem(j).style.visibility = 'visible' : elem(j).style.visibility = 'hidden';
                retVal++;
            }
        }
    }
    
    // verifica se wndTop tem frames e para cada frame encontrado
    // verifica se tem html carregado. Se tem html verifica se o ID do
    // html == htmlID, se for procura pelo div cujo id == divID.
    if (wndTop.frames != null)
    {
        for ( i=0; i<wndTop.frames.length; i++ )
        {
            if (wndTop.document.all.tags("IFRAME").item(i).src != '')
            {
                // o frame tem html carregado
                htmlScript = wndTop.document.frames(i).document.getElementById(htmlID); 
                
                if (htmlScript)
                {
                    // loop nos divs
                    elem = htmlScript.getElementsByTagName('DIV');
                    for ( j=0; j<elem.length; j++ )
                    {
                        if(elem(j).id == divID)
                        {
                            (cmdShow && true)? elem(j).style.visibility = 'visible' : elem(j).style.visibility = 'hidden';
                            retVal++;
                        }
                    }
                        
                }               
            }
        }
    }
    
    return retVal;
}

/********************************************************************
Atua sobre um array de divs, tornando visivel ou invisivel os divs do arrays.

Parametros:
htmlID              id do html que contem os divs
divIDArray          array de ids dos divs
cdmShow             true (torna visivel)/false (torna invisivel)

Retornos:
zero se n�o atuou sobre nenhum div ou o n�mero de divs sobre os
quais atuou

Nota:
A fun��o percorre todo o sistema, desta forma, se tiver mais de um
html com o mesmo ID e/ou divs com o mesmo id no html,
a fun��o atua sobre todos os divs encontrados.
********************************************************************/
function showDivArray(htmlID, divIDArray, cmdShow)
{
    var wndTop = window.top;
    var htmlScript = null;
    var i = 0;
    var j = 0;
    var k = 0;
    var elem = null;
    var retVal = 0;
        
    // nao tem html?
    if (wndTop == null)
        return retVal;
        
    // atua sobre wndTop?
    htmlScript = wndTop.document.getElementById(htmlID); 
    if (htmlScript)
    {
        // loop nos divs do html
        elem = htmlScript.getElementsByTagName('DIV');
        for ( j=0; j<elem.length; j++ )
        {
            // loop no array de divs
            for (k=0; k<divIDArray.length; k++)
            {
                if ( elem(j).id == divIDArray[k] )
                {
                    (cmdShow && true)? elem(j).style.visibility = 'visible' : elem(j).style.visibility = 'hidden';
                    retVal++;
                }
            }
        }
    }
    
    // verifica se wndTop tem frames e para cada frame encontrado
    // verifica se tem html carregado. Se tem html verifica se o ID do
    // html == htmlID, se for procura pelo div cujo id == divID.
    if (wndTop.frames != null)
    {
        for ( i=0; i<wndTop.frames.length; i++ )
        {
            if (wndTop.document.all.tags("IFRAME").item(i).src != '')
            {
                // o frame tem html carregado
                htmlScript = wndTop.document.frames(i).document.getElementById(htmlID); 
                
                if (htmlScript)
                {
                    // loop nos divs do html
                    elem = htmlScript.getElementsByTagName('DIV');
                    for ( j=0; j<elem.length; j++ )
                    {
                    
                        // loop no array de divs
                        for (k=0; k<divIDArray.length; k++)
                        {
                            if(elem(j).id == divIDArray[k])
                            {
                                (cmdShow && true)? elem(j).style.visibility = 'visible' : elem(j).style.visibility = 'hidden';
                                retVal++;
                            }
                        }    
                    }
                        
                }               
            }
        }
    }
    
    return retVal;
}

/********************************************************************
Retorna o elemento cjo extremo direito esta mais distante da borda
esquerda do div que o contem
********************************************************************/        
function mostRightElemInDiv(divRef)
{
	if ( divRef == null )
		return null;
	
	try
	{	
		if ( window.document.getElementById(divRef.id) == null )	
			return null;
	}
	catch(e)
	{
		return null;
	}
		
	var i;
	var coll, elem;
	var nCurrentMostRight;
	var nPreviousMostRight = 0;
	var elemMostRight = null;
	
	coll = divRef.children;
	
	if ( coll.length > 0 )
		elemMostRight = coll.item(0);
	
	for( i=0; i<coll.length; i++ )
	{
		elem = coll.item(i);
		nCurrentMostRight = parseInt(elem.currentStyle.left, 10) + 
		                    parseInt(elem.currentStyle.width, 10);
		
		if ( nCurrentMostRight >= nPreviousMostRight )
		{
			elemMostRight = elem;
			nPreviousMostRight = nCurrentMostRight;
		}
	}
	
	return elemMostRight;
}        

/********************************************************************
Retorna o elemento cjo extremo inferior esta mais distante da borda
superior do div que o contem
********************************************************************/        
function mostBottomElemInDiv(divRef)
{
	if ( divRef == null )
		return null;
	
	try
	{	
		if ( window.document.getElementById(divRef.id) == null )	
			return null;
	}
	catch(e)
	{
		return null;
	}
		
	var i;
	var coll, elem;
	var nCurrentMostBottom;
	var nPreviousMostBottom = 0;
	var elemMostBottom = null;
	
	coll = divRef.children;
	
	if ( coll.length > 0 )
		elemMostBottom = coll.item(0);
	
	for( i=0; i<coll.length; i++ )
	{
		elem = coll.item(i);
		nCurrentMostBottom = parseInt(elem.currentStyle.top, 10) + 
		                    parseInt(elem.currentStyle.height, 10);
		
		if ( nCurrentMostBottom >= nPreviousMostBottom )
		{
			elemMostBottom = elem;
			nPreviousMostBottom = nCurrentMostBottom;
		}
	}
	
	return elemMostBottom;
}        

/********************************************************************
Carrega uma pagina asp/html em um dado frame.

Parametros:
idElement           id do elemento que chamou a funcao
param1              array: window top name, frame a usar, URL da p�gina
param2              livre

Retorno:
nenhum
********************************************************************/
function loadPage(idElement, param1, param2)
{
    // esconde o frame para carregar a pagina
    showFrameInHtmlTop(param1[1], false);
    // carrega a pagina
    loadURLInFrame(param1[1], param1[2]);
}

/********************************************************************
Abre um form no browser corrente ou em um browser filho,
com toda sua interface.

Parametros:
whoCallTheFunc      string, quem chamou a funcao
windowTopName       nome do window top onde vai carregagar o form
=== os tres parametros a seguir saem da TABELA DE FORMS abaixo ===
formName            o nome do form
arqStart            arquivo inicial do form (o arquivo que controla
                    o carregamento do form). Passar o arquivo com o
                    path que vai do frameStart ate o arquivo
frameStart          frame onde carregar o arqStart                    
newChildBrowser     true (carrega em um novo browser filho)
                    false (carrega no browser corrente)

Retornos:
nenhum

TABELA DE FORMS
Descricao             formName        Arq Start          frame Start
---------             --------        ---------          -----------
Tipos auxiliares      TIPOSAUX        tiposaux01.asp     frameSup01
********************************************************************/
function openForm(whoCallTheFunc, windowTopName, formName, arqStart, frameStart, newChildBrowser)
{
    sendJSMessage(whoCallTheFunc, JS_FORMOPEN, new Array(windowTopName, formName, arqStart, frameStart, newChildBrowser), null);
}

/********************************************************************
Seleciona um option em um combo de um html

Parametros:
htmlID          id do html
comboID         id do combo no html
optionValue     o value do option a ser selecionado

Retorno:
true se sucesso, caso contrario false
********************************************************************/
function selOptByValueInSelect(htmlID, comboID, optionValue)
{
    // pega referencia ao frame do html
    var wnd = null;
    var frameID = null;
     
    frameID = getFrameIdByHtmlId(htmlID);
    if ( !frameID)
        return false;
    
    // pega referencia ao window contido no frame
    
    wnd = getPageFrameInHtmlTop(frameID);
    if (wnd)
        return selByOptValueInSel(wnd, comboID, optionValue);
    
    // falhou?
    return false;    
}

/********************************************************************
Retorna os dados de um option em um combo de um html

Parametros:
htmlID          id do html
comboID         id do combo no html

Retorno:
se sucesso      array: indice do option selecionado,
                       value do option selecionado,
                       texto do option selecionado
se falha        null;
********************************************************************/
function getCurrDataSelect(htmlID, comboID)
{
    // Pega referencia ao frame do html
    var wnd = null;
    var frameID = null;
    var cmb = null;
     
    frameID = getFrameIdByHtmlId(htmlID);
    if ( !frameID )
        return null;
    
    // Pega referencia ao window contido no frame
    
    wnd = getPageFrameInHtmlTop(frameID);
    if ( wnd )
    {
        cmb = wnd.document.getElementById(comboID);
        // Falhou?
        if ( cmb == null )
            return null;
        // Trap de erro
        if ( cmb.selectedIndex == -1 )
            return null;
                
        return ( new Array(cmb.selectedIndex, cmb.value,
                 cmb.options.item(cmb.selectedIndex).text) );
    }    
    
    // Falhou?
    return null;    
}

/********************************************************************
Trima os campos input de um html

Parametros:
win             window que chama a funcao

Retorno:
nenhum
********************************************************************/
function trimAllImputFields(win)
{
    var i;
    var coll = win.document.getElementsByTagName('INPUT');
    
    for (i=0; i<coll.length; i++)
    {
        if ( (coll.item(i).type).toUpperCase() == 'TEXT' )
        {
			try{
				coll.item(i).value = trimCarriageReturnAtAll(trimStr(coll.item(i).value));
			}
			catch(e)
			{
				;
			}

			try{
				coll.item(i).value = filtraString(trimStr(coll.item(i).value));
			}
			catch(e)
			{
				;
			}
        }
    }
    
    coll = win.document.getElementsByTagName('TEXTAREA');
    
    for (i=0; i<coll.length; i++)
    {
        coll.item(i).value = trimStr(coll.item(i).value);
        coll.item(i).value = filtraString(coll.item(i).value);
    }

}

/********************************************************************
Trima os campos input de um html, aceitando BULLET e TAB

Parametros:
win             window que chama a funcao

Retorno:
nenhum
********************************************************************/
function trimAllImputFields_Exception(win) {
    var i;
    var coll = win.document.getElementsByTagName('INPUT');

    for (i = 0; i < coll.length; i++) {
        if ((coll.item(i).type).toUpperCase() == 'TEXT') {
            try {
                coll.item(i).value = trimCarriageReturnAtAll(trimStr(coll.item(i).value));
            }
            catch (e) {
                ;
            }

            try {
                coll.item(i).value = filtraString(trimStr(coll.item(i).value));
            }
            catch (e) {
                ;
            }
        }
    }

    coll = win.document.getElementsByTagName('TEXTAREA');

    for (i = 0; i < coll.length; i++) {
        coll.item(i).value = trimStr(coll.item(i).value);
        coll.item(i).value = filtraString_Exception(coll.item(i).value);
    }

}

/********************************************************************
Copia os dados do recordset para os controles HTML que sao linkados
a campos (linkados pelas propriedades: DATASRC E DATAFLD.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function updateHTMLFields()
{
    var coll = null;
    var sTipo = '';
    var sDataSrc = '';
    var sDataFld = '';
    var i = 0;
    var valorCampo = null;
	var field;
	
    // Pega todos as tags que podem ser linkadas a recordset
    coll = window.document.getElementsByTagName('INPUT');
    for ( i=0; i<coll.length; i++ )
    {
		sTipo = coll.item(i).type.toUpperCase();
		
		if ((sTipo != 'TEXT') && (sTipo != 'CHECKBOX') && (sTipo != 'RADIO'))
		{
			continue;
		}
    
        sDataSrc = coll.item(i).getAttribute('dataSrc', 1);
        sDataSrc = sDataSrc.substring(1, sDataSrc.length);
        sDataFld = coll.item(i).getAttribute('dataFld', 1);

        if ((sDataSrc != null) && (sDataSrc != '') && 
            (sDataFld != null) && (sDataFld != ''))
        {
            try {
                valorCampo = eval(sDataSrc + '.recordset[\'' + sDataFld + '\'].value;');
                field = eval(sDataSrc + '.recordset[\'' + sDataFld + '\'];');
            }
            catch(e) {
				if (window.top.glb_devMode && SYS_DEBUGMODE)
					alert("Campo n�o encontrado " + sDataSrc + "." + sDataFld);
				else
					continue;
            }
            
            if (sTipo == 'TEXT')
            {
				if(((field.type == dataTypesCode["decimal"]) ||
					(field.type == dataTypesCode["float"]) ||
					(field.type == dataTypesCode["real"]) ||
					(field.type == dataTypesCode["money"]) ||
					(field.type == dataTypesCode["numeric"]) ||
					(field.type == dataTypesCode["smallmoney"])) &&
					(valorCampo != null))
				{
					coll.item(i).value = field.valueAsStrFloat();
				}
				else if (valorCampo != null)
				{
					coll.item(i).value = valorCampo;
				}
				else
				{
					coll.item(i).value = '';
				}
            }
            else if (sTipo == 'CHECKBOX')
            {
				if (valorCampo != null)
					coll.item(i).checked = valorCampo;
				else
					coll.item(i).checked = false;
            }
            else if (sTipo == 'RADIO')
            {
				coll.item(i).checked = (valorCampo == coll.item(i).value);
            }
        }
    }
    
    // Pega todos as tags que podem ser linkadas a recordset
    coll = window.document.getElementsByTagName('TEXTAREA');
    for ( i=0; i<coll.length; i++ )
    {
        sDataSrc = coll.item(i).getAttribute('dataSrc', 1);
        sDataSrc = sDataSrc.substring(1, sDataSrc.length);
        sDataFld = coll.item(i).getAttribute('dataFld', 1);

        if ((sDataSrc != null) && (sDataSrc != '') && 
            (sDataFld != null) && (sDataFld != ''))
        {
            try {
                valorCampo = eval(sDataSrc + '.recordset[\'' + sDataFld + '\'].value;');
            }
            catch(e) {
				if (window.top.glb_devMode && SYS_DEBUGMODE)
					alert("Campo n�o encontrado " + sDataSrc + "." + sDataFld);
				else
					continue;
            }
            
			if (valorCampo != null)
				coll.item(i).value = valorCampo;
			else
				coll.item(i).value = '';
        }
    }

    // Pega todos as tags que podem ser linkadas a recordset
    coll = window.document.getElementsByTagName('SELECT');
    for ( i=0; i<coll.length; i++ )
    {
        sDataSrc = coll.item(i).getAttribute('dataSrc', 1);
        sDataSrc = sDataSrc.substring(1, sDataSrc.length);
        sDataFld = coll.item(i).getAttribute('dataFld', 1);

        if ((sDataSrc != null) && (sDataSrc != '') && 
            (sDataFld != null) && (sDataFld != ''))
        {
            try {
                valorCampo = eval(sDataSrc + '.recordset[\'' + sDataFld + '\'].value;');
            }
            catch(e) {
				if (window.top.glb_devMode && SYS_DEBUGMODE)
					alert("Campo n�o encontrado " + sDataSrc + "." + sDataFld);
				else
					continue;
            }

            if (valorCampo == null)
                coll.item(i).selectedIndex = -1;
            else
                selOptByValueInSelect(getHtmlId(), coll.item(i).name, valorCampo);
        }
    }
}

var camposTratados = new Array();
function atualizaCampo(dsoName, fieldName, elem) {
	// Se h� campos tratados para o DSO e o campo j� foi tratado e o elemento pai est� oculto.
	if((camposTratados[dsoName] != null) && (camposTratados[dsoName][fieldName] != null) && (elem.parentElement.style.visibility == "hidden")) {
		return false;
	}

	if(camposTratados[dsoName] == null) {
		camposTratados[dsoName] = new Array();
	}
	
	if(camposTratados[dsoName][fieldName] == null) {
		camposTratados[dsoName][fieldName] = 1;
	}
	
	return true;
}

/********************************************************************
Copia os dados dos controles HTML para o recordset que sao linkados
a campos (linkados pelas propriedades: DATASRC E DATAFLD.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function updateRecordFromHTMLFields()
{
    var coll = null;
    var sTipo = '';
    var sDataSrc = '';
    var sDataFld = '';
    var toRead;
    var i = 0;
    var valorCampo = null;
    
    var dataSource;
    
	// Pega todos as tags que podem ser linkadas a recordset
	coll = window.document.getElementsByTagName('INPUT');
	for ( i=0; i<coll.length; i++ )
	{
		sTipo = coll.item(i).type.toUpperCase();
		
		if ((sTipo != 'TEXT') && (sTipo != 'CHECKBOX') && (sTipo != 'RADIO'))
		{
			continue;
		}

		sDataSrc = coll.item(i).getAttribute('dataSrc', 1);
		sDataSrc = sDataSrc.substring(1, sDataSrc.length);
		sDataFld = coll.item(i).getAttribute('dataFld', 1);
		toRead = coll.item(i).getAttribute('toread', 1);

		// Verifica se pode atualizar o campo.
		if(!atualizaCampo(sDataSrc, sDataFld, coll.item(i))) continue;
		
		// Retira o prefixo de campos data para atualizar o registro.
		if(sDataFld.substr(0, 2) == "V_") {
			sDataFld = sDataFld.substr(2, sDataFld.langth);
		}
		
		// N�o atualiza campos que s�o apenas para leitura.
		if(toRead != null) {
			continue;
		}
		
		if ((sDataSrc != null) && (sDataSrc != '') && 
			(sDataFld != null) && (sDataFld != ''))
		{
			try {
				dataSource = eval(sDataSrc);
			}
			catch(e) {
				throw "O datasource " + sDataSrc + "n�o existe.";
				break;
			}
            
			try{
				if (sTipo == 'TEXT')
				{
					valorCampo = coll.item(i).value;
						
					if (trimStr(valorCampo) == '')
						valorCampo = null;

					dataSource.recordset[sDataFld].value = valorCampo;
				}
				else if (sTipo == 'CHECKBOX')
					dataSource.recordset[sDataFld].value = coll.item(i).checked ? 1 : 0;
				else if (sTipo == 'RADIO' && coll.item(i).checked)
					dataSource.recordset[sDataFld].value = coll.item(i).value;
			}
			catch(e) {
				if (window.top.glb_devMode && SYS_DEBUGMODE)
					alert("Campo n�o encontrado " + sDataSrc + "." + sDataFld);
				else
					continue;
			}
		}
	}
    
	// Pega todos as tags que podem ser linkadas a recordset
	coll = window.document.getElementsByTagName('TEXTAREA');
	for ( i=0; i<coll.length; i++ )
	{
		sDataSrc = coll.item(i).getAttribute('dataSrc', 1);
		sDataSrc = sDataSrc.substring(1, sDataSrc.length);
		sDataFld = coll.item(i).getAttribute('dataFld', 1);

		// Verifica se pode atualizar o campo.
		if(!atualizaCampo(sDataSrc, sDataFld, coll.item(i))) continue;
		
		try {
			dataSource = eval(sDataSrc);
		}
		catch(e) {
			throw "O datasource " + sDataSrc + "n�o existe.";
			break;
		}
        
		if ((sDataSrc != null) && (sDataSrc != '') && 
			(sDataFld != null) && (sDataFld != ''))
		{
			try{
				valorCampo = trimStr(coll.item(i).value);
				valorCampo = (valorCampo == '' ? null : valorCampo);
			    
				dataSource.recordset[sDataFld].value = valorCampo;
			}
			catch(e) {
				if (window.top.glb_devMode && SYS_DEBUGMODE)
					alert("Campo n�o encontrado " + sDataSrc + "." + sDataFld);
				else
					continue;
			}
		}
	}

	// Pega todos as tags que podem ser linkadas a recordset
	coll = window.document.getElementsByTagName('SELECT');
	for ( i=0; i<coll.length; i++ )
	{
		sDataSrc = coll.item(i).getAttribute('dataSrc', 1);
		sDataSrc = sDataSrc.substring(1, sDataSrc.length);
		sDataFld = coll.item(i).getAttribute('dataFld', 1);

		// Verifica se pode atualizar o campo.
		if(!atualizaCampo(sDataSrc, sDataFld, coll.item(i))) continue;
		
		if ((sDataSrc != null) && (sDataSrc != '') && 
			(sDataFld != null) && (sDataFld != ''))
		{
			try {
				dataSource = eval(sDataSrc);
			}
			catch(e) {
				throw "O datasource " + sDataSrc + "n�o existe.";
				break;
			}
	        
			try{
				if(coll.item(i).selectedIndex == -1)
					dataSource.recordset[sDataFld].value = null;
				else
					dataSource.recordset[sDataFld].value = coll.item(i).value;
			}
			catch(e) {
				if (window.top.glb_devMode && SYS_DEBUGMODE)
					alert("Campo n�o encontrado " + sDataSrc + "." + sDataFld);
				else
					continue;
			}
		
		}
	}
}

/********************************************************************
Esta funcao retorna o texto do primeiro previousSibling que e tag P .

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function labelAssociate(idControl)
{
    var i;
    var elem, prevElem;
        
    elem = window.document.getElementById(idControl);
    
    if (elem == null)
        return '';
    
    prevElem = elem.previousSibling;
    
    if ( prevElem == null )
        return '';
        
    while ( prevElem.tagName.toUpperCase() != 'P' )
    {
        prevElem = prevElem.previousSibling;
        
        if ( prevElem == null )
            return '';
    }
    
    return prevElem.innerText;
}

/********************************************************************
Coloca foco no campo seguinte

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function focusNextFld()
{
    var elem;
        
    if (event.keyCode == 13)
    {
        elem = this.nextSibling;
        
        while (true)
        {
            elem = elem.nextSibling;
            
            if ( elem == null )
                break;
            
            if ( (typeof(elem.tagName)).toUpperCase() == 'UNDEFINED' )
                continue;
                
            if ( ((elem.tagName).toUpperCase() != 'P') && ((elem.tagName).toUpperCase() != 'HR') )
            {
                if ( elem.currentStyle.visibility != 'hidden' )
                {
                    if (elem.disabled == false )
                    {
                        window.focus();
                        elem.focus();
                        break;
                    }
                }
            }
        }
    }
}

/********************************************************************
Retorna os htmls ids cujos frames estao visiveis em
um html principal de um browser

Parametros:
win             window que chama a funcao

Retorno:
nenhum
********************************************************************/
function htmlIdsVisibles(win)
{
    var frameColl = win.top.document.getElementsByTagName('IFRAME');
    var i, j = 0;
    var htmlScript;
    var htmlTagColl;
    var htmlVisibleIDArray = new Array();
    
    for (i=0; i<frameColl.length; i++)
    {
        if ( frameColl.item(i).style.visibility == 'visible' )    
        {
            htmlScript = getPageFrameInHtmlTop(frameColl.item(i).id);
            htmlTagColl = htmlScript.document.getElementsByTagName('HTML');
            htmlVisibleIDArray[j] = htmlTagColl[0].id;
            j++;
        }
    }
    
    return htmlVisibleIDArray;
}

/********************************************************************
Retorna o id da tag html de um arquivo
********************************************************************/
function getHtmlId()
{
    return window.document.getElementsByTagName('HTML').item(0).id;
}

/********************************************************************
Retorna uma referencia a tag body de um arquivo
********************************************************************/
function getBodyRef()
{
    return window.document.getElementsByTagName('BODY').item(0);
}

/********************************************************************
Previne navegacao para a pagina anterior caso o primeiro
elemento de um form seja um combo, aplicando o evento onkeydown
em selects
********************************************************************/
function preventBkspcInSel(win)
{
    var i, coll;
    
    coll = win.document.getElementsByTagName('SELECT');
    
    for (i=0; i<coll.length; i++)
    {
        coll.item(i).onkeydown = keyDownInSel;
    }
}

/********************************************************************
Previne navegacao para a pagina anterior caso o primeiro
elemento de uma pagina seja um combo
********************************************************************/
function keyDownInSel()
{
    if (event.keyCode == 8)
        return false;
}

/*******************************************************************
Funcoes do dicionario
********************************************************************/
/********************************************************************
Obtem o valor numerico da lingua corrente
Parametros - Nenhum
Retorno    - O numerico da lingua corrente para translado ou
             0 (zero) se erro
********************************************************************/
function getDicCurrLang()
{
	return sendJSMessage('AnyHtml', JS_MSGRESERVED, 'GETCURRLANGDIC', null);
}

/********************************************************************
Altera o valor numerico da lingua corrente
Parametros - nLang -  numerico da nova ligua corrente para translado
Retorno    - true se sucesso ou false se erro
********************************************************************/
function setDicCurrLang(nLang)
{
	return sendJSMessage('AnyHtml', JS_MSGRESERVED, 'SETCURRLANGDIC', nLang);
}

/********************************************************************
Funcao de uso da automacao. Translada todos os <p> de um html.
Parametros - win     : referencia do window do html a transladar
             nLangto : numerico da lingua a transladar,
                       se = null usa a lingua corrente do sistema
                       para translado
Retorno    - nenhum
NOTA: a lingua base do dicionario e o portugues, se nLangTo == 246
a funcao nao executa

MODELO PARA OS ARQUIVOS QUE FIZEREM USO DE TERMO DE DICIONARIO
ALTERNATIVO:
Definir o array abaixo como global ao arquivo
var _termAltInDic = new Array();
_termAltInDic[0] = new Array("lblRegistroID.innerHTML", "A");

********************************************************************/
function translateInterface(win, nLangTo)
{
	if ( win == null )
		return null;
	
	if ( (nLangTo == 246) || (nLangTo == 0) )
		return null;
		
	if ( nLangTo == null )	
		nLangTo = getDicCurrLang();
		
	var i, j, elem, currTerm, iElemArr;
	var sTermCurr = '';
	var sTermTrans = '';
	
	iElemArr = 0;
	
	// Portugues -> Ingles
	if ( nLangTo == 245 )
		iElemArr = 1;
		
	if ( iElemArr == 0 )	
		return null;

	j = win.document.all.length;
	 		
	for (i=0; i<j; i++)
    {
        elem = win.document.all.item(i);
        
        // Translada hints
        if ( (elem.title != null) || (elem.title != '') )
        {
			sTermCurr = elem.title;
			
			if ( !((sTermCurr == null) || (sTermCurr == '')) )
			{
				sTermCurr = sTermCurr.toUpperCase();
				sTermTrans = sendJSMessage('AnyHtml', JS_MSGRESERVED, 'GETTERMINDIC', new Array(sTermCurr, iElemArr));
				
				if ( !((sTermTrans == null) || (sTermTrans == '')) )
					elem.title = sTermTrans;	
			}
        }
        
        // Translada <P>
        if ( elem.tagName.toUpperCase() == 'P' )
        {
			sTermCurr = elem.innerText;
			
			if ( (sTermCurr == null) || (sTermCurr == '') )
				continue;
				
			sTermCurr = sTermCurr.toUpperCase();
			
			sTermTrans = sendJSMessage('AnyHtml', JS_MSGRESERVED, 'GETTERMINDIC', new Array(sTermCurr, iElemArr));
			
			if ( (sTermTrans == null) || (sTermTrans == '') )
				continue;
				
			elem.innerText = sTermTrans;	
        }
        // Translada <INPUT type=button>
        else if ( (elem.tagName.toUpperCase() == 'INPUT') && (elem.type.toUpperCase() == 'BUTTON') )
        {
			sTermCurr = elem.value;
			
			if ( (sTermCurr == null) || (sTermCurr == '') )
				continue;
				
			sTermCurr = sTermCurr.toUpperCase();
			
			sTermTrans = sendJSMessage('AnyHtml', JS_MSGRESERVED, 'GETTERMINDIC', new Array(sTermCurr, iElemArr));
			
			if ( (sTermTrans == null) || (sTermTrans == '') )
				continue;
				
			elem.value = sTermTrans;	
        }
    }
    
    // Translada termos dos dicionarios alternativos
    // usa array _termAltInDic onde:
    // _termAltInDic[n] = new Array(id do elemento a transladar, tipo do termo no dicionario)
    try
    {
		j = _termAltInDic.length;
		for(i = 0; i<j; i++)
		{
			sTermCurr = eval(_termAltInDic[i][0]);
			
			if ( (sTermCurr != null) || (sTermCurr != '') )
			{
				sTermCurr = sTermCurr.toUpperCase();
				eval( _termAltInDic[i][0] + "='" + sendJSMessage('AnyHtml', JS_MSGRESERVED, 'GETTERMINDIC', new Array(sTermCurr, iElemArr, _termAltInDic[i][1])) + "'" );
			}
		}
    }
    catch (e)
    {
		;
    }
}

/********************************************************************
Funcao de uso da automacao e do programador.
Translada um termo em portugues para uma lingua.
Parametros - sTerm    : termo a transladar
             nLangto  : numerico da lingua a transladar,
                       se = null usa a lingua corrente do sistema
                       para translado
             sTermUse : uso do termo, define o dicionario a usar
                        (null, 'A', etc)
Retorno    -
NOTA: a lingua base do dicionario e o portugues, se nLangTo == 246
a funcao nao executa o translado e retorna o termo passado como esta
********************************************************************/
function translateTerm(sTerm, nLangTo, sTermUse)
{
	if ( (sTerm == null) || (sTerm == '') )
		return sTerm;
	
	if ( (typeof(nLangTo)).toUpperCase() == 'UNDEFINED' )
		nLangTo = null;
		
	if ( (typeof(sTermUse)).toUpperCase() == 'UNDEFINED' )
		nLangTo = null;
	
	if ( (nLangTo == 246) || (nLangTo == 0) )
		return sTerm;
		
	if ( nLangTo == null )	
		nLangTo = getDicCurrLang();
		
	if ( sTermUse != null )	
		sTermUse = sTermUse.toUpperCase();
		
	var sTermCurr = sTerm.toUpperCase();
	var sTermTrans = '';
	var iElemArr = 0;
	
	// Portugues -> Ingles
	if ( nLangTo == 245 )
		iElemArr = 1;
		
	if ( iElemArr == 0 )	
		return sTerm;
				
	sTermTrans = sendJSMessage('AnyHtml', JS_MSGRESERVED, 'GETTERMINDIC', new Array(sTermCurr, iElemArr, sTermUse));
	
	if ( (sTermTrans == null) || (sTermTrans == '') && (!((sTermUse == null) || (sTermUse == ''))) )
	{
		__translateTerm(sTerm, nLangTo, sTermUse);	
	}	
	
	if ( (sTermTrans == null) || (sTermTrans == '') )
		return sTerm;
		
	return sTermTrans;
}

function __translateTerm(sTerm, nLangTo, sTermUse)
{
	if ( (sTerm == null) || (sTerm == '') )
		return sTerm;
	
	if ( (typeof(nLangTo)).toUpperCase() == 'UNDEFINED' )
		nLangTo = null;
		
	if ( (typeof(sTermUse)).toUpperCase() == 'UNDEFINED' )
		nLangTo = null;
	
	if ( (nLangTo == 246) || (nLangTo == 0) )
		return sTerm;
		
	if ( nLangTo == null )	
		nLangTo = getDicCurrLang();
		
	if ( sTermUse != null )	
		sTermUse = sTermUse.toUpperCase();
		
	var sTermCurr = sTerm.toUpperCase();
	var sTermTrans = '';
	var iElemArr = 0;
	
	// Portugues -> Ingles
	if ( nLangTo == 245 )
		iElemArr = 1;
		
	if ( iElemArr == 0 )	
		return sTerm;
				
	sTermTrans = sendJSMessage('AnyHtml', JS_MSGRESERVED, 'GETTERMINDIC', new Array(sTermCurr, iElemArr, sTermUse));
	
	if ( (sTermTrans == null) || (sTermTrans == '') )
		return sTerm;
		
	return sTermTrans;
}

// FINAL DE IMPLEMENTACAO DAS FUNCOES

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****

/********************************************************************
Seleciona um option pelo seu value em um combo
********************************************************************/
function selByOptValueInSel(wnd, comboID, optionValue)
{
    var retVal = false;

    var i, j, coll = wnd.document.all.tags('SELECT');
    
    for (i=0; i<coll.length; i++)
    {
         if (coll(i).id == comboID)
         {
            for (j=coll(i).length - 1; j>=0; j--)
            {
                if (coll(i).item(j).value == optionValue)
                {
                    coll(i).item(j).selected = true;
                    retVal = true;
                    break;
                }
            }    
         }
    }
    
    return retVal;
}

/********************************************************************
Esta funcao retorna o id de um frame, dado o seu nome.

Parametros:
thisWin             window do html ou asp onde a funcao foi chamada.
frameName           nome do frame a obter o id

Retornos:
O id do frame ou null se nao encontra o frame

Nota:
A fun��o atua sobre todos os frames do html mais externo do sistema,
desta forma, se tiver mais de um frame no html com o mesmo ID,
a fun��o atua sobre o primeiro encontrado.
********************************************************************/
function getFrameIdByFrameNameInHtmlTop(thisWin, frameName)
{
    var retVal = null;
    var wndTop = thisWin.top;
    
    if (wndTop == null)
        return retVal;
    
    var i;
    var coll = wndTop.document.all.tags('IFRAME');
    
    for (i=0; i<coll.length; i++)
    {
        if(coll.item(i).name.toUpperCase() == frameName.toUpperCase())
        {
           retVal = coll.item(i).id; 
           break;
        }   
    }
    
    return retVal;
}
// FINAL DE FUNCOES INTERNAS DA LIBRARY *****************************