/********************************************************************
js_browsers.js
Library javascript de funcoes basicas para browsers
********************************************************************/

// CONSTANTES *******************************************************
var glb_REFWINCHILDBROWSERSLOADED = new Array();
var glb_WINCHILDNAME = new Array();
var glb_WINROOTNAME = 'OVERFLYCHILD';

// FINAL DE CONSTANTES **********************************************

/********************************************************************
INDICE DAS FUNCOES:
    openChildBrowser(strPars)
    removeChildBrowserFromControl(childBrowserWinName)
    thereAreChildrenWindows()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Abre um browser filho.

Parametros:
strPars     - string do formato
                "?formName=PESSOAS
                &arqStart=http%3A//jose/overfly3/basico/pessoas/pessoassup01.asp
                &frameStart=frameSup01"
formType        - o tipo de conjunto de paginas a carregar no browser
            - null  - form
            - 1     - telefonia
            - 2     - aniversariantes

Retorno:
Nenhum
********************************************************************/
function openChildBrowser(strPars, formType)
{
    var i;
    var empresaData;
    var j = glb_REFWINCHILDBROWSERSLOADED.length;
    var winWidth;
    var winHeight;
    var winLeft;
    var winTop;
    
    // supoe nenhum elemento livre nos arrays;
    var freeIndex = glb_REFWINCHILDBROWSERSLOADED.length;
    
    // procura pelo primeiro elemento livre no array
    for (i=0; i<j; i++)  
    {
        // tem elemento livre, usa
        if ( glb_REFWINCHILDBROWSERSLOADED[i] == null )
        {
            freeIndex = i;
            break;
        }    
    }
    
    if (strPars != null)
        if (strPars.indexOf('frameStart=OverflyBI') > 0)
            formType = 3;
    
    // nome da janela
    var fracPart = (Math.random()).toString();
    fracPart = fracPart.substr(fracPart.indexOf('.') + 1);
    
    glb_WINCHILDNAME[freeIndex] = glb_WINROOTNAME + freeIndex.toString() + fracPart;

    if ( formType == null )
    {
        // acrescenta o userID aos parametros de carregamento
        // do browser filho
        if ( strPars == null )
            strPars = '?'; 
        else    
            strPars += '&'; 
    
        strPars += 'userID='+escape(glb_CURRUSERID.toString());
        // acrescenta a empresa corrente aos parametros do carregamento
        empresaData = sendJSMessage('statusbarchildHtml', JS_WIDEMSG, '__EMPRESADATA', null);
    
        strPars += '&empresaID='+escape(empresaData[0].toString());

        glb_REFWINCHILDBROWSERSLOADED[freeIndex] = 
                                window.open(SYS_PAGESURLROOT + '/childbrowser/childmain.aspx'+strPars,
                                            glb_WINCHILDNAME[freeIndex],
                                            BROWSERCHILD_INTERFACE, true);
    }                                
    else if ( formType == 1 )
    {
        strPars = ''; 
        strPars = '?nUserID=' + escape(glb_CURRUSERID.toString());
        empresaData = sendJSMessage('statusbarmainHtml', JS_WIDEMSG, '__EMPRESADATA', null);
    
        strPars += '&nEmpresaID=' + escape(empresaData[0].toString());
        
        // O OCX em uso da Voice
        strPars += '&VOICE_OCX_CLSID=' + escape(VOICE_OCX_CLSID);
        
        winWidth = MAX_FRAMEWIDTH;
        winHeight = 198 - (3 * 15) - 2;
        winLeft = window.screen.availWidth - winWidth - (ELEM_GAP + 2);
        winTop = window.screen.availHeight - winHeight - (5 * ELEM_GAP);
                    
        glb_REFWINCHILDBROWSERSLOADED[freeIndex] = 
                                window.open(SYS_PAGESURLROOT + '/telefonia/telefonia.asp' + strPars,
                                glb_WINCHILDNAME[freeIndex],
                                (BROWSERCHILD_INTERFACE +
                                    ', width = ' + winWidth.toString() +
                                    ', height = ' + winHeight.toString() +
                                    ', left = ' + winLeft.toString() +
                                    ', top = ' + winTop.toString() +
                                    ';'), true);
    }
    else if ( formType == 2 )
    {
        if ( strPars == null )
            strPars = ''; 
        
        //Deprecacao do browser de aniversariantes
        return null;
        
        winWidth = 350;
        winHeight = 196;
        winLeft = 0;
        winTop = 0;
                    
        glb_REFWINCHILDBROWSERSLOADED[freeIndex] = 
                                window.open(SYS_PAGESURLROOT + '/aniversariantes/aniversariantes.asp' + strPars,
                                glb_WINCHILDNAME[freeIndex],
                                (BROWSERCHILD_INTERFACE +
                                    ', width = ' + winWidth.toString() +
                                    ', height = ' + winHeight.toString() +
                                    ', left = ' + winLeft.toString() +
                                    ', top = ' + winTop.toString() +
                                    ';'), true);
    }
    else if ( formType == 3 )
    {
        empresaData = sendJSMessage('statusbarchildHtml', JS_WIDEMSG, '__EMPRESADATA', null);
        winWidth = 800;
        winHeight = 600;
        winLeft = 0;
        winTop = 0;
        var _debugMode = false;
        var _BI_URL_SITE = '';
        
        if (_debugMode)
            _BI_URL_SITE = 'http://localhost/olap/cubebrowser.aspx';
        else
            _BI_URL_SITE = 'http://bi.overfly.com.br/cubebrowser.aspx';

        strPars = '?EmpresaID=' + escape(empresaData[0]) +
            '&UsuarioID=' + escape(glb_CURRUSERID);

        //
        glb_REFWINCHILDBROWSERSLOADED[freeIndex] = 
                                window.open(_BI_URL_SITE + strPars,
                                glb_WINCHILDNAME[freeIndex],
                                (BROWSERCHILD_INTERFACE +
                                    ', width = ' + winWidth.toString() +
                                    ', height = ' + winHeight.toString() +
                                    ', left = ' + winLeft.toString() +
                                    ', top = ' + winTop.toString() +
                                    ';'), true);
        sendJSMessage('cubeBrowserHtml', JS_CHILDBROWSERLOADED, null, null);
    }
    else if ((formType == 4) || (formType == 5)) 
    {
        if (strPars == null)
            strPars = '';

        strPars = '?nUsuarioID=' + escape(glb_CURRUSERID.toString());

        if (formType == 4) {
            strPars += '&LidasBit=0';
        }
        else if (formType == 5) {
            strPars += '&LidasBit=1';
        }

        glb_REFWINCHILDBROWSERSLOADED[freeIndex] =
                                window.open(SYS_PAGESURLROOT + '/notificacoes/notificacoes.asp' + strPars,
                                glb_WINCHILDNAME[freeIndex],
                                BROWSERCHILD_NOTIFICATION + ', width = 720, height = 600;', true);
    }
}

/********************************************************************
Remove a referencia de um browser filho dos
arrays de controle, no fechamento do browser filho.

Parametros:
childBrowserWinName - nome da janela principal do browser que vai fechar

Retorno:
true      - a referencia do browser foi removida
false     - a referencia a este browser nao foi encontrada
********************************************************************/
function removeChildBrowserFromControl(childBrowserWinName)
{
    var i = 0;
    
    if ( window.opener && !window.opener.closed )
    {    
        // verifica se este window existe no array de windows name
        for (i=0; i<window.opener.glb_WINCHILDNAME.length; i++)
        {
            if ( window.opener.glb_WINCHILDNAME[i] != null )
            {
                if ( window.opener.glb_WINCHILDNAME[i].toUpperCase() ==  childBrowserWinName.toUpperCase())
                {   
                    // anula o nome da janela e o valor do elemento nos arrays
                    // correspondentes
                    window.opener.glb_WINCHILDNAME[i] = null;        
                    window.opener.glb_REFWINCHILDBROWSERSLOADED[i] = null;
                    
                    return true;
                }
            }
        }
    }
    
    // fail
    return false;
}

/********************************************************************
Informa se existem browsers filhos

Parametros:
nenhum

Retorno:
true      - existem browsers filhos
false     - nao existem browsers filhos
********************************************************************/
function thereAreChildrenWindows()
{
    var i = 0;
    var j = glb_REFWINCHILDBROWSERSLOADED.length;
    var retVal = false;
    
    for (i=0; i<j; i++)  
    {
        if ( glb_REFWINCHILDBROWSERSLOADED[i] != null )   
        {
            retVal = true;
            break;
        }    
    }    
    
    return retVal;
}
