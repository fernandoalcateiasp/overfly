// OBJETO OVERLISTDATA **********************************************

function _overListDataInitializeList()
{
	var i;
	
	this.listLength = 0;
	
	for (i=0; i<this.aList.length; i++)
		this.aList[i] = null;
}

function _overListDataAddInList(elemToAdd)
{
	var i;
		
	for (i=0; i<this.aList.length; i++)
	{
		if (this.aList[i] == null)	
		{
			this.aList[i] = elemToAdd;
			
			this.listLength = i + 1;
			
			return true;
		}
	}
	
	this.aList[this.aList.length] = elemToAdd;
	
	this.listLength = this.aList.length;
	
	return true;	
}

function _overListDataGetInList(nListIndex)
{
	if (nListIndex > (this.aList.length - 1) )
		return null;
	
	var i;
	
	for (i=0; i<this.aList.length; i++)
	{
		if ( i == nListIndex )
			return this.aList[i];
	}
	
	return null;
}

function _OverListData()
{
	// metodos para uso do programador
	this.initializeList = _overListDataInitializeList;
	this.addInList = _overListDataAddInList;
	this.getInList = _overListDataGetInList;
	
	// variaveis para uso do programador
	this.listLength = 0;

	// variaveis internas do objeto
	this.aList = new Array();
}

// FINAL DE OBJETO OVERLISTDATA *************************************