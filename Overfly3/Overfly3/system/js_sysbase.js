/********************************************************************
js_sysbase.js

Library javascript de funcoes basicas de sistema
********************************************************************/

// CONSTANTES *******************************************************

var __idElement, __param1, __param2, __especChildBrowserName;

var __sysbaseTimer = null;
var __sysbaseTimer2 = null;

// FINAL DE CONSTANTES **********************************************

/********************************************************************
INDICE DAS FUNCOES:
    sendJSMessage(idElement, msg, param1, param2)
    sendJSMsgFromChildToMother_Browser(idElement, msg, param1, param2)
    sendJSMsgFromMotherToChild_Browser(idElement, msg, param1, param2)
    sendJSCarrier(idElement, param1, param2, especChildBrowserName)
    sendJSCarrierFromMotherToChild_Browser(childBrowserName, idElement,
                                                param1, param2,
                                                especChildBrowserName)        
    setConnection(dso)
    getStrConnection()
    
    setPrinterPaperParams(objPrt)
    
    dealWithObjects_Load()
    dealWithObjects_Unload()

NOTAS IMPORTANTES:

O SCRIPT ID wndJSProc acima e palavra reservada e nao pode ser usado
como ID em nenhum outro elemento de asp, html ou js ou qualquer outro
arquivo do sistema.

A funcao sendJSMessage(idElement, msg, param1, param2) � cascateada
para todos os arquivos que tenham a funcao wndJSProc.
Se a mensagem for tratada e retornar um objecto ou valor nao nulo,
a cascata para no arquivo em que isto for feito e retorna.
Caso nenhum retorno em nenhum arquivo for definido como diferente de
null, a cascata vai ate o ultimo arquivo que tenha a funcao wndJSProc.

Os valores do msg estao definidos no arquivo js_constants.js
********************************************************************/

/********************************************************************
Esta funcao manda uma mensagem para todos htmls do sistema que
tenham a funcao wndJSProc implementada.
A funcao wndJSProc que intercepta as mensagens est� abaixo definida.

Parametros:
idElement           id do elemento que mandou a mensagem.
                    Pode ser array (ex: html id + elem id),
                    assinatura de funcao, etc
msg                 constante definida para o sistema
param1              opcional. Primeiro parametro que acompanha
                    a mensagem.
param2              opcional. Segundo parametro que acompanha a
                    mensagem.
Os parametros param1 e param2 podem ser qualquer tipo, inclusive
arrays.

Retorno:
A fun��o retorna null se nao foi interceptada, caso contrario
o usuario pode retornar o que quiser.
IMPORTANTE:
A funcao para o loop do wndJSProc, no ponto em que for tratada e
tiver um retorno != null
********************************************************************/
function sendJSMessage(idElement, msg, param1, param2)
{
    var wndTop = window.top;
    var htmlScript = null;
    var i = 0;
    var retVal = null;
    
    // tem html?
    if (wndTop != null)
    {    
        // wndTop tem wndJSProc? Se tem passa a mensagem
        htmlScript = wndTop.document.getElementById('wndJSProc'); 
        if (htmlScript)
        {
            retVal = wndTop.wndJSProc(idElement, msg, param1, param2);
            // se a mensagem foi interceptada e tratada retorna
            if (retVal != null)
                return retVal;
        }
    
        // verifica se wndTop tem frames e para cada frames encontrado
        // verifica se tem html carregado. Se tem asp/html verifica se o
        // asp/html tem wndJSProc? Se tem passa a mensagem
        if (wndTop.frames != null)
        {
            for ( i=0; i<wndTop.frames.length; i++ )
            {
                if (wndTop.document.all.tags("IFRAME").item(i).src != '')
                {
                    try
                    {
                        // o frame tem html carregado
                        htmlScript = wndTop.document.frames(i).document.getElementById('wndJSProc'); 
                        if (htmlScript)
                        {
                            try
                            {
                                retVal = wndTop.document.frames(i).wndJSProc(idElement, msg, param1, param2);
                                // se a mensagem foi interceptada e tratada retorna
                                if (retVal != null)
                                    return retVal;
                            }
                            catch(e)
                            {
                                if (window.top.glb_devMode && SYS_DEBUGMODE)
                                    alert('sendJSMessage.bp1' + e + msg + param1 + param2);
                            
                                return null;
                            }
                        }               
                    }    
                    catch(e)    
                    {
                        if (window.top.glb_devMode && SYS_DEBUGMODE)
                            alert('sendJSMessage.bp2' + e + msg + param1 + param2);

                       continue;
                    }
                }
            }
        }
    }
        
    if ( retVal != null )
        return retVal;
    
    // propaga se for o caso
    retVal = sendJSMsgFromChildToMother_Browser(idElement, msg, param1, param2);
    if ( retVal != null )
        return retVal;
        
    // propaga se for o caso quando a origem da invocacao
    // ocorreu no browser mae
    retVal = sendJSMsgFromMotherToChild_Browser(idElement, msg, param1, param2);
    
    return retVal;
}

/********************************************************************
Esta funcao propaga o sendJSMessage de uma pagina no browser filho,
para o browser mae.
Esta funcao e interna da biblioteca e nao pode ser usada diretamente
em hipotese alguma.
********************************************************************/
function sendJSMsgFromChildToMother_Browser(idElement, msg, param1, param2)
{
    var wndTop = window.top.window.opener;
    var htmlScript = null;
    var i = 0;
    var retVal = null;

    // tem html?
    if (wndTop != null)
    {    
        // wndTop tem wndJSProc? Se tem passa a mensagem
        try
        {
            htmlScript = wndTop.document.getElementById('wndJSProc'); 
            
            if (htmlScript)
            {
                retVal = wndTop.wndJSProc(idElement, msg, param1, param2);
                // se a mensagem foi interceptada e tratada retorna
                if (retVal != null)
                    return retVal;
            }
    
            // verifica se wndTop tem frames e para cada frames encontrado
            // verifica se tem html carregado. Se tem asp/html verifica se o
            // asp/html tem wndJSProc? Se tem passa a mensagem
            if (wndTop.frames != null)
            {
                for ( i=0; i<wndTop.frames.length; i++ )
                {
                        if (wndTop.document.all.tags("IFRAME").item(i).src != '')
                        {
                            // o frame tem html carregado
                            htmlScript = wndTop.document.frames(i).document.getElementById('wndJSProc'); 
                            if (htmlScript)
                            {
                                retVal = wndTop.document.frames(i).wndJSProc(idElement, msg, param1, param2);
                                // se a mensagem foi interceptada e tratada retorna
                                if (retVal != null)
                                    return retVal;
                            }               
                        }
                }
            }
        }
        catch(e)
        {
            retVal = null;
        }
    }    

    // propaga se for o caso    
    return sendJSMsgFromMotherToChild_Browser(idElement, msg, param1, param2);
}

/********************************************************************
Esta funcao propaga o sendJSMessage de uma pagina no browser mae,
para os browsers filhos.
Esta funcao e interna da biblioteca e nao pode ser usada diretamente
em hipotese alguma.
********************************************************************/
function sendJSMsgFromMotherToChild_Browser(idElement, msg, param1, param2)
{
    var wndTop = null;
    var htmlScript = null;
    var i = 0;
    var j = 0;
    var retVal = null;

    var mainChildArray;
    
    // o array de browsers filhos
    try
    {
        if (window.top)
            mainChildArray = window.top.glb_REFWINCHILDBROWSERSLOADED;
    }
    catch(e)
    {
        return null;
    }

    try
    {
        for (j=0; j<mainChildArray.length; j++)
        {
            if (mainChildArray[j] == null)
                continue;
                
            wndTop = mainChildArray[j];
    
            // tem html?
            if (wndTop != null)
            {    
                
                // wndTop tem wndJSProc? Se tem passa a mensagem
                try
                {
                    htmlScript = wndTop.document.getElementById('wndJSProc'); 
                                       
                    if (htmlScript)
                    {
                        retVal = wndTop.wndJSProc(idElement, msg, param1, param2);
                        // se a mensagem foi interceptada e tratada retorna
                        if (retVal != null)
                            return retVal;
                    }
    
                    // verifica se wndTop tem frames e para cada frames encontrado
                    // verifica se tem html carregado. Se tem asp/html verifica se o
                    // asp/html tem wndJSProc? Se tem passa a mensagem
                    if (wndTop.frames != null)
                    {
                        for ( i=0; i<wndTop.frames.length; i++ )
                        {
                                if (wndTop.document.all.tags("IFRAME").item(i).src != '')
                                {
                                    // o frame tem html carregado
                                    htmlScript = wndTop.document.frames(i).document.getElementById('wndJSProc'); 
                                    if (htmlScript)
                                    {
                                        retVal = wndTop.document.frames(i).wndJSProc(idElement, msg, param1, param2);
                                        // se a mensagem foi interceptada e tratada retorna
                                        if (retVal != null)
                                            return retVal;
                                    }               
                                }
                        }
                    }
                }
                catch(e)
                {
                    retVal = null;
                }
            }    
        }
    }    
    catch(e)    
    {
        return retVal;
    }
    
    return retVal;
}


/********************************************************************
Funcao de comunicacao do carrier:
Chamada de qualquer arquivo do form.

Localizacao:
js_sysbase.js

Chama:
Funcao intermediaria do carrier
goAheadWithsendJSCarrier(idElement, param1, param2, especChildBrowserName)
do arquivo js_sysbase.js

Parametros:
idElement             - Obrigatorio. Id do html que manda o carrier.
param1                - De uso do programador. Qualquer objeto ou null.
param2                - De uso do programador. Qualquer objeto ou null.
especChildBrowserName - De uso do programador. Nome de um browser filho
                        para receber o carrier. Se null o carrier sera
                        propagado para todos os browsers

Retorno               - null se nao tem browser filho
********************************************************************/
function sendJSCarrier(idElement, param1, param2, especChildBrowserName)
{
    var wndTop = window.top;
    
    if (wndTop == null)
        return null;
    
    // Chama funcao do browser filho no childmain.aspx
    // wndTop.sendJsCarrierFromForm(idElement, param1, param2, especChildBrowserName);
    
    __idElement = idElement;
    __param1 = param1 ;
    __param2 = param2;
    __especChildBrowserName = especChildBrowserName;
    
    __sysbaseTimer = window.setInterval('goAheadWithSendJSCarrier()', 10, 'JavaScript');
}

/********************************************************************
Funcao intermediaria do carrier:
Chamada de qualquer arquivo do form.

Localizacao:
js_sysbase.js

Chama:
Funcao sendJsCarrierFromForm(idElement, param1, param2)
do arquivo childmain.aspx do browser filho que contem o form.

Parametros:
idElement             - Obrigatorio. Id do html que manda o carrier.
param1                - De uso do programador. Qualquer objeto ou null.
param2                - De uso do programador. Qualquer objeto ou null.
especChildBrowserName - De uso do programador. Nome de um browser filho
                        para receber o carrier. Se null o carrier sera
                        propagado para todos os browsers

Retorno               - null se nao tem browser filho
********************************************************************/
function goAheadWithSendJSCarrier(idElement, param1, param2, especChildBrowserName)
{
    if ( __sysbaseTimer != null )
    {
        window.clearInterval(__sysbaseTimer);
        __sysbaseTimer = null;
    }
    
    var wndTop = window.top;
    
    if (wndTop == null)
        return null;
    
    lockInterface(true);
    
    // garante destravar o form se algum problema no carrier
    __sysbaseTimer2 = window.setInterval('__unlockFormInterface()', 10 * 1000, 'JavaScript'); 
        
    // Chama funcao do browser filho no childmain.aspx
    wndTop.sendJsCarrierFromForm(__idElement, __param1, __param2, __especChildBrowserName);
}

/********************************************************************
Destrava forcosamente o form de origem se o carrier nao retorna em 10 segs
********************************************************************/
function __unlockFormInterface()
{
    if ( __sysbaseTimer2 != null )
    {
        window.clearInterval(__sysbaseTimer2);
        __sysbaseTimer2 = null;
    }
    
    lockInterface(false);
}

/********************************************************************
Funcao de comunicacao do carrier:
Chamada do arquivo overfly.asp.
Veio da funcao sendJsCarrierFromChildBrowser definida no arquivo
overfly.asp, que veio da funcao
sendJsCarrierFromForm definida no arquivo childmain.aspx, que veio da
funcao sendJSCarrier definida no arquivo js_sysbase.js e chamada
em qualquer arquivo de form.

Localizacao:
js_sysbase.js.

Chama:
Propaga para todos os browsers filhos que nao sejam o browser filho
cujo arquivo asp do form originou o carrier.
No final prograga para o browser filho que originou o carrier.

Parametros:
childBrowserName        - Obrigatorio. Nome do browser filho que chamou a funcao.
                          Na cadeia de funcoes do carrier este nme e colocado
                          na passagem pelo html top do browser filho que mandou
                          o carrier.
idElement               - Obrigatorio. Id do html que manda o carrier.
param1                  - De uso do programador. Qualquer objeto ou null.
param2                  - De uso do programador. Qualquer objeto ou null.
especChildBrowserName   - De uso do programador. O carrier so sera mandado
                          para este browser filho.

Retorno                 - sempre null
********************************************************************/
function sendJSCarrierFromMotherToChild_Browser(childBrowserName, idElement, param1, param2, especChildBrowserName)
{
    var temp = null;
    var wndTop = null;
    var wndRet = null;
    var htmlScript = null;
    var i = 0;
    var j = 0;
    var retVal = null;
    var retValSaved = null;
    var msg = JS_CARRIERCAMING;
    var mainChildArray;
    var childBrowserNameIntercept = null;
    var carrierProcByForm = false;
    
    // Obtem referencia ao array de browsers filhos
    try
    {
        // trapeado em 21/10/2002
        //if (window.top)
            mainChildArray = window.top.glb_REFWINCHILDBROWSERSLOADED;
    }
    catch(e)
    {
        return null;
    }

    try
    {
        // Propaga para os browsers filhos que nao sao o da origem do carrier
        // na ordem direta
        
        // Modificacao em 03/12/08
        carrierProcByForm = false;
        
        for (j=0; j<mainChildArray.length; j++)
        {
            // O carrier foi interceptado por um form. Modificado em 03/12/08
            if (carrierProcByForm == true)
            {
                wndTop = mainChildArray[j];

                if (wndTop != null)
                {   
                    // Guarda referencia ao browser filho que contem o form
                    // que mandou o carrier
                    try
                    {
                        if ( wndTop.name == childBrowserName )
                        {
                            wndRet = wndTop;
                            continue;
                        }    
                    }
                    catch(e)
                    {
                        ;
                    }    
                }
                continue;
            }
            // O carrier foi interceptado por um form. Final de modificado em 03/12/08
            
            // Salta elementos nulos do array de browsers filhos
            if (mainChildArray[j] == null)
                continue;
                
            wndTop = mainChildArray[j];
    
            // Examina e processa cada browser filho e seu form
            if (wndTop != null)
            {   
                // Guarda referencia ao browser filho que contem o form
                // que mandou o carrier
                try
                {
                    if ( wndTop.name == childBrowserName )
                    {
                        wndRet = wndTop;
                        continue;
                    }    
                    
                    // Propaga apenas para um browser filho especifico
                    if ( especChildBrowserName != null )
                    {
                        if ( wndTop.name != especChildBrowserName )
                            continue;
                    }
                }
                catch(e)    
                {
                    continue;
                }                                        
                
                // Cada browser filho que nao e o que contem o form que
                // mandou o carrier
                try
                {
                    // Referencia a tag de wndJSProc do browser filho
                    htmlScript = wndTop.document.getElementById('wndJSProc'); 
                    
                    // Se o browser filho tem wndJSProc, invoca
                    if (htmlScript)
                    {
                        retVal = wndTop.wndJSProc(new Array(idElement, childBrowserName), msg, param1, param2);
                        // O html top do browser filho interceptou o carrier.
                        if ( retVal != null )
                        {
                            retValSaved = retVal;
                            childBrowserNameIntercept = wndTop.name;
                        }    
                    }
    
                    // Verifica se o browser filho tem frames e para cada frame
                    // verifica se tem html carregado. Se tem asp/html carregado, verifica se o
                    // asp/html tem wndJSProc, se tem invoca.
                    if (wndTop.frames != null)
                    {
                        for ( i=0; i<wndTop.frames.length; i++ )
                        {
                            if (wndTop.document.all.tags("IFRAME").item(i).src != '')
                            {
                                try
                                {
                                    // Frame tem html/asp carregado
                                    htmlScript = wndTop.document.frames(i).document.getElementById('wndJSProc'); 
                                    if (htmlScript)
                                    {
                                        retVal = wndTop.document.frames(i).wndJSProc(new Array(idElement, childBrowserName), msg, param1, param2);

                                        // O html/asp interceptou o carrier.
                                        if ( retVal != null )
                                        {
                                            retValSaved = retVal;
                                            childBrowserNameIntercept = wndTop.name;
                                            // Modificacao em 03/12/08
                                            carrierProcByForm = true; 
                                        }   
                                    }               
                                }
                                catch(e)    
                                {
                                    continue;
                                }
                            }
                        }
                    }
                }
                catch(e)
                {
                    // Se erro retorna nulo.
                    retValSaved = null;
                }
            }    
        }
    }    
    catch(e)    
    {
        return null;
    }

    retVal = retValSaved;
    
    // Retorna para o browser filho que deu origem e nele retorna ao
    // form que contem
    // Variavel retVal contem o retorno do form e identificador do browser
    // filho que fez a intercepcao
    
    // Do lado que faz a intercepcao
    // sai uma variavel que tem que ser sempre um array com tres elementos.
    // Na biblioteca e a variavel retVal.
    // retVal[0}   - Obrigatorio - Identificador (htmlID) do arquivo do form
    //                             que interceptou o carrier ou null, se nenhum
    //                             arquivo do form interceptou o carrier.
    // retVal[1]   - Obrigatorio - Mensagem um de retorno de quem interceptou
    //                             o carrier ou null.
    // retVal[2]   - Obrigatorio - Mensagem dois de retorno de quem interceptou
    //                             o carrier ou null.
    
    // Do lado que mandou o carrier
    // chega uma mensagem sempre com os seguintes parametros
    // idElement   - Sempre um array de dois elementos
    //               idElement[0] - identificador (htmlID) do arquivo do form
    //                              ou null (retVal[0]).
    //               idElement[1] - nome do browser filho que interceptou
    // param1      - retval[1]
    // param2      - retVal[2]
    
    // Se nenhum browser filho ou form contido em browser filho
    // interceptar o carrier, retVal volta um array
    // com os quatro elementos nulos.

    // Tratamento da variavel retVal
    // retval e nulo, ou seja, ninguem interceptou
    if ( retVal == null )
        retVal = new Array(null, null, null);

    // retVal nao e array    
    if ( (typeof(retVal.length)).toUpperCase() != 'NUMBER' )
        return null;

    // retVal e array mas nao tem tres elementos
    if ( retVal.length != 3 )    
        return null;

    // Retorna carrier a quem deu a origem ao mesmo
    if ( wndRet != null )    
    {
        temp = retVal[0];
        retVal[0] = new Array(temp, childBrowserNameIntercept);
        
        wndTop = wndRet;
        msg = JS_CARRIERRET;
        
        // Retorna para o browser filho que deu origem ao carrier
        try
        {
            htmlScript = wndTop.document.getElementById('wndJSProc'); 

            // Se o browser filho tem wndJSProc, invoca
            if (htmlScript)
            {
                retValOrig = wndTop.wndJSProc( retVal[0], msg,
                                               retVal[1], retVal[2] );
                // Se a mensagem foi interceptada e tratada encerra
                if (retValOrig != null)
                    return null;
            }

            // Verifica se o browser filho tem frames e para cada frame
            // verifica se tem html carregado. Se tem asp/html carregado, verifica se o
            // asp/html tem wndJSProc, se tem invoca.
            if (wndTop.frames != null)
            {
                for ( i=0; i<wndTop.frames.length; i++ )
                {
                        if (wndTop.document.all.tags("IFRAME").item(i).src != '')
                        {
                            // O frame tem html/asp carregado
                            htmlScript = wndTop.document.frames(i).document.getElementById('wndJSProc'); 
                            if (htmlScript)
                            {
                                retValOrig = wndTop.document.frames(i).wndJSProc( retVal[0], msg,
                                                                                  retVal[1], retVal[2] );

                                // O html/asp interceptou a mensagem, encerra
                                if (retValOrig != null)
                                {
                                    return null;
                                }
                            }               
                        }
                }
            }
        }
        catch(e)
        {
            retValOrig = null;
        }
    
    }

    return null;
}

/********************************************************************
Ajusta layout das impressoes
********************************************************************/
function setPrinterPaperParams(prtRef)
{

	var nUserID = getCurrUserID();
	var aEmpresaData;
	
	var factorPortrait = 1;
	var factorLandscape = 1;

	aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');

	prtRef.PaperType = window.top.overflyGen.PaperTypeSetOnDefaultPrinter;
	
	if ( prtRef.PaperType == '' )
		prtRef.PaperType = 'A4';
    
    factorPortrait = (215.9 - _MARGIN_LEFT - _MARGIN_RIGHT_PORTRAIT - 4 - 4) / (210 - _MARGIN_LEFT - _MARGIN_RIGHT_PORTRAIT);
	factorLandscape = (279.4 - _MARGIN_LEFT_LANDSCAPE - _MARGIN_RIGHT_LANDSCAPE - 4) / (297 - _MARGIN_LEFT_LANDSCAPE - _MARGIN_RIGHT_LANDSCAPE);
	    
    if ( (prtRef.PaperType).toUpperCase() == 'LETTER' )
    {
		if ( prtRef.LandScape == true )
			prtRef.ScaleColPosFactor = factorLandscape;
		else
			prtRef.ScaleColPosFactor = factorPortrait;
			
		prtRef.MarginLeft = _MARGIN_LEFT + 4;
		prtRef.MarginLeftLandScape = _MARGIN_LEFT_LANDSCAPE + 4;
		prtRef.MarginRightPortrait = _MARGIN_RIGHT_PORTRAIT + 4;
		prtRef.MarginRightLandScape = _MARGIN_RIGHT_LANDSCAPE;
    }
    else
    {
		prtRef.MarginLeft = _MARGIN_LEFT;
		prtRef.MarginLeftLandScape = _MARGIN_LEFT_LANDSCAPE;
		prtRef.MarginRightPortrait = _MARGIN_RIGHT_PORTRAIT;
		prtRef.MarginRightLandScape = _MARGIN_RIGHT_LANDSCAPE;
    }
	
	prtRef.MarginTop = _MARGIN_TOP;
    prtRef.MarginBotton = _MARGIN_BOTTON;
    
}

/********************************************************************
Gera randomico inteiro em um intervalo
********************************************************************/
function genRndInInterval( nFrom, nTo)
{
	var nDif = Math.abs((nTo - nFrom));
	
	nDif = nDif * Math.random();

	return Math.ceil((nFrom + nDif));
}

// FUN��ES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****

// FINAL DE FUN��ES INTERNAS DA LIBRARY *****************************

/********************************************************************
Configura a string de conexao de um objeto RDS

Parametros              - referencia ao objeto RDS

Retorno                 - irrelevante
********************************************************************/
function setConnection(dso)
{
    dso.host = SYS_PAGESDOMINIUM + "/WSOverflyRDS/overflyRDS.asmx";
}

/********************************************************************
Retorna a string de conexao

Parametros              - nenhum

Retorno                 - string de conexao 
********************************************************************/
function getStrConnection(nTimeOut)
{
	var sRet = '';
	
	if ( nTimeOut == null )
		nTimeOut = (6 * 60);
    
    sRet = sendJSMessage(getHtmlId(), JS_WIDEMSG, JS_STRCONN, null);
    sRet += 'Internet Timeout=' + (nTimeOut * 1000) + '; ';
    sRet += ' Command Properties=' + '\'' + 'Command Time Out=' + nTimeOut.toString() + '\'';
    
    return sRet;
}

/****************
' Create a new instance of the RDS DataControl
    Set m_oRDC = New RDS.DataControl

    ' Setup the RDS DataControl properties
    m_oRDC.Server = "http://carlp2"
    m_oRDC.InternetTimeout = 360000  ' Milliseconds, 6 mins (360 secs)
    m_oRDC.Connect = "Provider=sqloledb;" & _
                     "Data Source=carlp3;" & _
                     "Initial Catalog=pubs;" & _
                     "User Id=sa;" & _
                     "Password=;" & _                      
                     "Command Properties='Command Time Out=360'" 
' 6 mins (360 secs)

****************/


/********************************************************************
Retorna o campo Description do array tabelas x campos int

Parametros:
param1	- nome da tabela
param2  - nome do campo

Retornos:
O campo Description
********************************************************************/
function intFieldDescription(param1, param2)
{
	if ( (param1 == null) || (param2 == null) || (param1 == '') || ( param2 == '') )
		return -1;
		
	var nRet = -1;
	var i = 0;
	var iStart = 0;
	var j = glb_intFields.length;
	var firstLetter = 0;
	
	param1 = param1.toUpperCase();	
	param2 = param2.toUpperCase();
		
	firstLetter = param1.charCodeAt(0);
	
	iStart = getFirstLetterTablePos(firstLetter);
		
	// Seek ASC
	for	( i = iStart; i < j; i++ )
	{
		if ( glb_intFields[i][0] == param1 )
		{
			if ( glb_intFields[i][1] == param2 )
			{
				nRet = glb_intFields[i][3];
				break;
			}
		}
	}
	
	return nRet;
}

/********************************************************************
Monta array da posicao do primeiro d�gito de um dado array.
O array dado precisa estar indexado.

Parametros:
	- aArrBase     - referencia array dado
	- aArrFirstDig - array da posicao dos primeiros digitos
                   - pode ser um array de array de dois elementos, ou
                   - se executar apenas uma vez,
                   - preferencialmente um ponteiro de variavel null
Retornos:
    - true se sucesso ou false
********************************************************************/
function _mountArrayFirstDigOfArray(aArrBase, aArrFirstDig)
{
	if ( aArrBase == null )
		return false;
		
	if ( aArrBase.length == 0 )
		return false;
	
	if ( aArrFirstDig == null )
	{
		aArrFirstDig = new Array(null);
		aArrFirstDig[0] = new Array(null, null);
		aArrFirstDig[0][0] = null;
		aArrFirstDig[0][1] = null;
	}
	
	var bRet = false;	
	var i;
	var j;
	var firstDig;
	var arrFirstDigLen = 0;
	
	try
	{	
		bRet = false;	
		i = 0;
		j = aArrBase.length;
		k = 0;
		m = aArrFirstDig.length;
		firstDig = null;
		
		for	( i = 0; i < j; i++ )
		{
			if ( aArrBase[i][0].charCodeAt(0) == firstDig )
				continue;

			firstDig = (aArrBase[i][0]).charCodeAt(0);

			if ( k > (m - 1) )
			{
				m++;
				aArrFirstDig[k] = new Array(null, null);
			}

			aArrFirstDig[k][0] = firstDig;
			aArrFirstDig[k][1] = i;
			k++;	
		}
		
		for ( i = k; i < m; i++ )
		{
			aArrFirstDig[i][0] = null;
			aArrFirstDig[i][1] = null;
		}
		
		bRet = true;
	}
	catch (e)
	{
		;
	}
	
	return bRet;
}

/********************************************************************
Obtem a posicao do primeiro digito em um array da posicao do
primeiro d�gito de um dado array.

Parametros:
	- charFirstLetter - char da letra a obter a posicao
	- aArrFirstDig    - array da posicao
Retornos:
	- 1 ou a posicao se encontrada
********************************************************************/
function _getFirstDigOfArray( charFirstLetter, aArrFirstDig )
{
	var i = 0;
	var j = 0;
	var nRet = -1;
	
	j = glb_intFieldsPos.length;

	for ( i = 0; i < j; i++)
	{
		if ( aArrFirstDig[i][0] == charFirstLetter )
			{
				nRet = aArrFirstDig[i][1];
				break;
			}
	}
	
	return nRet;
}

/*
IE que exige click em objeto que nao seja o grid
*/
function dealWithObjects_Load()
{
	if (document.getElementsByTagName)
	{ 
		// Get all the tags of type object in the page. 
		var __objs = document.getElementsByTagName("object"); 
		var __iCounter;
		for (__iCounter=0; __iCounter<__objs.length; __iCounter++) 
		{ 
			// Get the HTML content of each object tag 
			// and replace it with itself. 
			try
			{
			    if (__objs[__iCounter].classid.toUpperCase() != "CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072" )
    				__objs[__iCounter].outerHTML = __objs[__iCounter].outerHTML; 
			}
			catch (e)
			{
				;
			}
		} 
	} 
}

/*
IE que exige click em objeto grid
*/
function dealWithGrid_Load()
{
	if (document.getElementsByTagName)
	{ 
		// Get all the tags of type object in the page. 
		var __objs = document.getElementsByTagName("object"); 
		var __iCounter;
		for (__iCounter=0; __iCounter<__objs.length; __iCounter++) 
		{ 
			// Get the HTML content of each object tag 
			// and replace it with itself JUST ONCE. 
			try
			{
				if (__objs[__iCounter].classid.toUpperCase() == "CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072" )
				{
			        __objs[__iCounter].outerHTML = __objs[__iCounter].outerHTML; 
				}
			}
			catch (e)
			{
				;
			}
		} 
	} 
}

function dealWithObjects_Unload()
{
	if (document.getElementsByTagName)
	{ 
		// Get all the tags of type object in the page. 
		var __objs = document.getElementsByTagName("object"); 
		var __iCounter;
		for (__iCounter=0; __iCounter<__objs.length; __iCounter++) 
		{ 
			// Clear out the HTML content of each object tag 
			// to prevent an IE memory leak issue. 
			try
			{
				__objs[__iCounter].outerHTML = null; 
			}
			catch (e)
			{
				;
			}
		} 
	} 
}
