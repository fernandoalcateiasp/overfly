/********************************************************************
js_PPC_cookie.js reduzida para WEB

Library javascript para cookies
********************************************************************/

/********************************************************************
Cria e coloca (ou modifica) um cookie
	cookieName - nome do cookie
	cookieValue - valor a ser colocado no cookie
	lifeTime - tempo de validade do cookie em segundos
********************************************************************/
function setCookie( cookieName, cookieValue, lifeTime )
{
	if ( (cookieName == null) || (cookieName == "") ||
	     (cookieValue == null) || (cookieValue == "") ||
	     (lifeTime == null) || (lifeTime == "") )
	     return false;
	
    try
    {
        var i;

        var expDate = new Date();
        
        expDate.setTime(expDate.getTime() + (lifeTime * 1000));
        
        window.document.cookie = cookieName + '=' + escape(cookieValue) + ';' +
            'expires=' + expDate.toGMTString();
            
        return true;    
    }
    catch(e)
    {
		return false;    
    }
}

/********************************************************************
Recupera o conteudo de um cookie
********************************************************************/
function getCookie( cookieName )
{
	if ( (cookieName == null) || (cookieName == "") )
	     return null;
	     
    try
    {
        var cookieValue = null;
        if( getCookCount(cookieName) != 0 )
        {
            cookieValue = getCookieNum(cookieName, getCookCount(cookieName));
            if ( cookieValue != null )
                return cookieValue;
        }    
    }
    catch (e)
    {
		return null;
    }
}

/********************************************************************
Funcao auxiliar para cookie
********************************************************************/
function getCookCount (cookieName)
{
    var result = 0;
    var myCookie = ' ' + window.document.cookie + ';';
    var searchName = ' ' + cookieName + '=';
    var nameLength = searchName.length;
    var startOfCookie = myCookie.indexOf(searchName);
        
    while ( startOfCookie != -1 )
    {
        result += 1;
        startOfCookie = myCookie.indexOf(searchName, startOfCookie + nameLength);
    }
        
    return result;
}

/********************************************************************
Funcao auxiliar para cookie
********************************************************************/
function getCookieNum(cookieName, cookieNum)
{
    var result = null;
    
    if ( cookieNum >=1 )
    {
        var myCookie = ' ' + window.document.cookie + ';';
        var searchName = ' ' + cookieName + '=';
        var nameLength = searchName.length;
        var startOfCookie = myCookie.indexOf(searchName);
        var cntr = 0;
        
        for ( cntr = 1; cntr < cookieNum; cntr++ )
            startOfCookie = myCookie.indexOf(searchName, startOfCookie + nameLength);
            
        if ( startOfCookie != -1 )    
        {
            startOfCookie += nameLength;
            var endOfCookie = myCookie.indexOf(';', startOfCookie);
            result = unescape(myCookie.substring(startOfCookie, endOfCookie));
        }
        return result;
    }
    return result;
}
