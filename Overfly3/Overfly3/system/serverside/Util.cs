using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using OVERFLYSVRCFGLib;

namespace Overfly3.systemEx.serverside
{
	public class Util
	{
		private static OverflyMTS objSvrCfg = new OverflyMTS();
		
		public static bool IsNumeric(string value)
		{
			// Testa se � inteiro
			try
			{
				int.Parse(value);
			}
			catch(Exception)
			{
				// Testa se � um n�mero de ponto flutuante.
				try
				{
					double.Parse(value);
				}
				catch(Exception)
				{
					return false;
				}
			}
			
			return true;
		}

		private static string pagesURLRoot = null;
		public static string PagesURLRoot
		{
			get
			{
				if(pagesURLRoot == null)
				{
					pagesURLRoot = objSvrCfg.PagesURLRoot(
						System.Configuration.ConfigurationManager.AppSettings["application"]);
				}
				
				return pagesURLRoot;
			}
		}
	}
}
