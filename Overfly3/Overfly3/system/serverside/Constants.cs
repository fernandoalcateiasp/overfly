using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using java.lang;

namespace Overfly3.systemEx.serverside
{
	public class Constants
	{
		public static java.lang.Boolean FALSE = new java.lang.Boolean(false);
		public static java.lang.Boolean TRUE = new java.lang.Boolean(true);

		public static Integer INT_ZERO = new Integer(0);
		public static java.lang.Double DBL_ZERO = new java.lang.Double(0);
		
		public static string STR_EMPTY = "";

		public static java.lang.Boolean[] BOOL_EMPTY_SET = new java.lang.Boolean[0];
		public static Integer[] INT_EMPTY_SET = new Integer[0];
		public static java.lang.Double[] DBL_EMPTY_SET = new java.lang.Double[0];
		public static string[] STR_EMPTY_SET = new string[0];
	}
}
