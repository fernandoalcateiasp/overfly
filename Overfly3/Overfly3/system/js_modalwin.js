/********************************************************************
js_modalwin.js

Library javascript para o xxxmodal01.asp
********************************************************************/

/********************************************************************
INDICE DAS FUNCOES:
    showModalWin(htmlPath, dimModalWin)
    lockInterface(cmdLock)
    isInterfaceLocked()
    restoreInterfaceFromModal()
********************************************************************/

// As variaveis globais abaixo sao de uso exclusivo desta library
// VARIAVEIS GLOBAIS ************************************************

var __win_Timer__ = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Mostra um html/asp no frame modal

Parametros:
htmlPath            - o path do arquivo, no sistema
dimModalWin         - array: largura, altura do frameModal
sAspIdExpect        - null sempre recarrega o asp do servidor
                    - != null n�o recarrega se == htmlID da pagina
                    - atualmente carregada

Retorno:
nenhum
********************************************************************/
function showModalWin(htmlPath, dimModalWin, sAspIdExpect)
{
    // trava a interface
    lockInterface_Fase1(true);   
    
    // o modal window e contido no frameModal
    // e posicionado e dimensionado em funcao do toolbar inferior
    
    var frameTBInf = null;
    var rectTBInf = new Array(0, 0, 0, 0);
    var rectModal = new Array(0, 0, 0, 0);
    var modalFrame = null;
    var pgInFrame = null;
    var htmlIDInFrame = null;
    var nCallerPos = 0;
    
    frameTBInf = getFrameIdByHtmlId('controlbarinfHtml');
    
    if ( frameTBInf )
        rectTBInf = getRectFrameInHtmlTop(frameTBInf);
    
    if ( rectTBInf && ( dimModalWin != null ))    
    {
        // valores minimos e maximos para as dimensoes da janela
        dimModalWin[0] = ((dimModalWin[0] < (MAX_FRAMEWIDTH / 3) ) ? (MAX_FRAMEWIDTH / 3) : dimModalWin[0]);
        dimModalWin[1] = ((dimModalWin[1] < (MAX_FRAMEHEIGHT / 3) ) ? (MAX_FRAMEHEIGHT / 3) : dimModalWin[1]);

        rectModal[0] = (rectTBInf[2] - dimModalWin[0]) / 2;
        rectModal[1] = rectTBInf[1] - dimModalWin[1] / 2;
    }
    
    if ( dimModalWin )    
    {
        rectModal[2] = dimModalWin[0];
        rectModal[3] = dimModalWin[1];
    }
    
    moveFrameInHtmlTop('frameModal', rectModal);
        
    modalFrame = getFrameInHtmlTop('frameModal');
    
    if ( modalFrame != null )
    {
        modalFrame.style.zIndex = 2;
    } 

    if ( (sAspIdExpect != null) && ((typeof(sAspIdExpect)).toUpperCase() == 'STRING' ) )
    {
        pgInFrame = getPageFrameInHtmlTop('frameModal');
        
        if ( pgInFrame != '' )
        {
            htmlIDInFrame = pgInFrame.getHtmlId();
            
            if ( sAspIdExpect.toUpperCase() == htmlIDInFrame.toUpperCase())
            {
                try
                {
                    nCallerPos = htmlPath.indexOf('?');
                    
                    if ( nCallerPos >= 0 )
                    {
                        nCallerPos += (('?sCaller=').length);
                        pgInFrame.glb_sCaller = htmlPath.substr(nCallerPos);
                        
						// Codigo acrescentado 26/08/2003
                        nCallerPos = (pgInFrame.glb_sCaller).indexOf('&');
                        
                        if ( nCallerPos >= 0 )
                        {
							pgInFrame.glb_sCaller = (pgInFrame.glb_sCaller).substr(0, nCallerPos);
                        }
                        // Final de Codigo acrescentado 26/08/2003
                    }
					
					try
					{
						// Garante reabilitar botao close
						// se existe na modal
						pgInFrame.btnCloseWin.disabled = false;                    
					}
					catch(e)
					{
						;
					}
                    pgInFrame.refreshParamsAndDataAndShowModalWin();
                }
                catch(e)
                {
                    ;
                }    

                return null;
            }    
        }    
    }    
    
    if ( htmlPath.indexOf('http://') >=0 )
        loadURLInFrame('frameModal', htmlPath);
    else
        loadURLInFrame('frameModal', SYS_PAGESURLROOT + htmlPath);
            
    //lockInterface_Fase1(true);
}

/********************************************************************
Restaura o status da interface do browser filho corrente,
identificando os frames a restaurar e o status dos elementos dos htmls contidos.

Parametros:
bForce  - forca destravar a interface, mesmo com a modal invisivel

Retorno:
true - se retaurou
false - nao restaurou ou se ja estava restaurada
********************************************************************/
function restoreInterfaceFromModal(bForce)
{
    if ( bForce == true )
    {
        return lockInterface_Fase1(false);
    }
    
    var retVal = false;
    
    // Modal esta aberta
    if ( window.top.document.getElementById('frameModal').currentStyle.visibility == 'visible' )
    {
        showFrameInHtmlTop('frameModal', false);    
        
        // modificado por Jose em 27/08/2001
        retVal = lockInterface_Fase1(false);
        
        //retVal = true;
        //__win_Timer__ = window.setInterval('lockInterface_Fase1(false)', 10, 'JavaScript');
    }
    
    return retVal;
}

/********************************************************************
Trava ou destrava a interface do browser filho corrente.

Parametros:
cdmLock         - true (trava) / false (destrava)

Retorno:
se sucesso true, caso contrario false
********************************************************************/
function lockInterface(cmdLock)
{
    if (cmdLock == true)
        return lockInterface_Fase1(true);
    else if (cmdLock == false)   
        return lockInterface_Fase1(false);
}

/********************************************************************
Informa se a interface do browser filho corrente esta travada.

Parametros:
nenhum

Retorno:
true se esta travada, caso contrario false
********************************************************************/
function isInterfaceLocked()
{
    return sendJSMessage('anyHtml', JS_CHILDBROWSERLOCKED, false, true);
}

// FINAL DE IMPLEMENTACAO DAS FUNCOES

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****

/********************************************************************
Esta funcao inicia o travamento ou destravamento da interface
do browser filho corrente, identificando os frames a travar os
elementos dos htmls contidos.

Parametros:
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function lockInterface_Fase1(cmdLock)
{
    if ( __win_Timer__ != null )
    {
        window.clearInterval(__win_Timer__);
        __win_Timer__ = null;
    }

    // verifica o estado de travamento da interface
    var interfaceLocked = sendJSMessage('anyHtml', JS_CHILDBROWSERLOCKED, false, true);
    
    // compara com a operacao desejada
    if ( cmdLock == interfaceLocked )
        return false;
        
    // seta o flag do browser filho de travamento de interface
    sendJSMessage('anyHtml', JS_CHILDBROWSERLOCKED, true, true);

    var i = 0;
    var winNav = window.top;
    
    // loop nos htmls carregados nos frames do browser filho
    if (winNav.frames != null)
    {
        for ( i=0; i<winNav.frames.length; i++ )
        {
            switch (winNav.document.all.tags("IFRAME").item(i).id.toUpperCase())
            {
                case ('FRAMESTATUSBAR') :            
                {
                    // tratamento especial
                    // nao destrava se o detalhe esta visivel
                    var fSupisVisible = ascan(htmlIdsVisibles(window.top),'sup01Html',true) >= 0;
                    if ( cmdLock == true )
                        sendJSMessage('AnyHtml',JS_STBARGEN, 'CMBSTATUS', cmdLock);
                    else if ( (cmdLock == false) && (fSupisVisible == false) )
                        sendJSMessage('AnyHtml',JS_STBARGEN, 'CMBSTATUS', cmdLock);
                }
                break;
                
                case ('FRAMEFASTBUTTONS') :            
                {
                    // Removido para fora do for
                    // para impedir que a funcao
                    // lockControls dos html de forms
                    // inverta o estado da barra de botoes
                    // de acesso rapido
                    ;
                }
                break;
                
                case ('FRAMEMODAL') :            
                {
                    ;
                }
                break;
                
                case ('FRAMEBARSUP') :            
                {
                    if ( cmdLock )
                        lockControlBar('sup');
                    else
                        unlockControlBar('sup');    
                }
                break;
                
                case ('FRAMEBARINF') :            
                {
                    if ( cmdLock )
                        lockControlBar('inf');
                    else
                        unlockControlBar('inf');    
                }
                break;
                
                default :   // o frame nao e de sistema
                {
                    if (winNav.document.all.tags("IFRAME").item(i).src != '')
                    {
                        // o frame tem html carregado
                        winNav.document.frames(i).lockControls(cmdLock);
                    }
                }
            }
        }
        
        // Conforme comentario em case ('FRAMEFASTBUTTONS') :            
        try
        {
            if ( cmdLock )
                lockFastButtons();
            else
                unlockFastButtons();
        }    
        catch(e)
        {
            ;
        }
    }
    
    return true;    
}
// FINAL DE FUNCOES INTERNAS DA LIBRARY *****************************
