
<!-- METADATA TYPE="typelib" NAME="Microsoft CDO for NTS 1.2 library" UUID="{0E064ADD-9D99-11D0-ABE5-00AA0064D470}" VERSION="1.2"-->
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
%>

<%
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	Dim i, sMailBody, sTo
	Dim objSendMail
    Dim rsData, strSQL
	Dim txtNome, txtEmail, txtEmpresa, txtFormName, txtContexto, txtToolBar, txtLastButton, txtAnotherWindow, txtErrorWindow, txtErrorDescription
	
	sMailBody = "Error Report" & Chr(13) & Chr(10) & Chr(13) & Chr(10)

	'-5 sao os 5 parametros default de paginas ASP
	For i = 1 To Request.QueryString.Count
        sMailBody = sMailBody & Chr(32) & Mid(Request.QueryString.Key(i),4) & ":" & Chr(32) & _
            Request.QueryString(i).Item & vbCrLf
	Next
	
	txtEmail = ""
	
    For i = 1 To Request.QueryString("txtEmail").Count
        txtEmail = Request.QueryString("txtEmail")(i)
	Next

	sTo = "marcio.rodrigues@alcateia.com.br"
    'sTo = "jose.gracia@alcateia.com.br"
        
    'Manda o e mail
    Set objSendMail = CreateObject("CDONTS.NewMail")
             
    With objSendMail
        .From = txtEmail
        .To = sTo
        .Cc = ""
        .Bcc = ""
        .Subject = "Error Report"
        .Body = sMailBody
        .BodyFormat = CdoMailFormatText
        .Importance = CdoNormal
        .Send
    End With

    Set rsData = Server.CreateObject("ADODB.Recordset")

    strSQL = "SELECT 'e-mail was sent' AS Msg"
                  
    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

    rsData.Save Response, adPersistXML

    rsData.Close
    Set rsData = Nothing
    
    Set objSendMail = Nothing
%>
