<HTML>
<HEAD>
<TITLE>Overfly - Report Error</TITLE>
<script LANGUAGE="JavaScript" SRC="http://www.overfly.com.br/overfly/errorreport/js_overdso.js"></script>
<script LANGUAGE="JavaScript" SRC="http://www.overfly.com.br/overfly/system/js_strings.js"></script>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<!--
var dsoError = new overdso();

function btnEnviar_onclick()
{
	btnEnviar.disabled = true;
	
	var sMsg = '';
	var objFocus = null;
	
	if (trimStr(txtNome.value) == '')
	{
		sMsg = 'Field (Name) was not supplied.';
		objFocus = window.document.getElementById('txtNome');
	}
	else if (trimStr(txtEMail.value) == '')
	{
		sMsg = 'Field (E-Mail) was not supplied.';
		objFocus = window.document.getElementById('txtEMail');
	}
	else if (trimStr(txtCompany.value) == '')
	{
		sMsg = 'Field (Company) was not supplied.';
		objFocus = window.document.getElementById('txtCompany');
	}
	else if (trimStr(txtFormName.value) == '')
	{
		sMsg = 'Field (Form Name) was not supplied.';
		objFocus = window.document.getElementById('txtFormName');
	}	
	else if (trimStr(txtContext.value) == '')
	{
		sMsg = 'Field (Context) was not supplied.';
		objFocus = window.document.getElementById('txtContext');
	}	
	else if (trimStr(txtErrorDescription.value) == '')
	{
		sMsg = 'Field (Error Description) was not supplied.';
		objFocus = window.document.getElementById('txtErrorDescription');
	}	

	if (sMsg != '')
	{
		alert(sMsg);
		btnEnviar.disabled = false;
		window.focus();
		objFocus.focus();
		return null;
	}

    var strPars = '';
    
    strPars += '?txtNome=' + escape(txtNome.value);
    strPars += '&txtEmail=' + escape(txtEMail.value);
	strPars += '&txtEmpresa=' + escape(txtCompany.value);
	strPars += '&txtFormName=' + escape(txtFormName.value);
	strPars += '&txtContexto=' + escape(txtContext.value);
	strPars += '&txtToolBar=' + escape(txtToolBar.value);
	strPars += '&txtLastButton=' + escape(txtLastButton.value);
	strPars += '&txtAnotherWindow=' + escape(txtAnotherWindow.value);
	strPars += '&txtErrorWindow=' + escape(txtErrorWindow.value);
	strPars += '&txtErrorDescription=' + escape(txtErrorDescription.value);

    dsoError.URL = 'http://www.overfly.com.br/overfly/errorreport/sendmailerror.asp' + strPars;
    //dsoError.URL = 'http://jose/overfly3/errorreport/sendmailerror.asp' + strPars;
    dsoError.ondatasetcomplete = 'dsoError_DSC()';
    dsoError.refresh();
}

function dsoError_DSC()
{
	alert('The e-mail was sent.');
	btnEnviar.disabled = true;
}

//-->
</SCRIPT>

</HEAD>
<BODY>
<FONT face="Verdana" size="4">Overfly Error Report Form</FONT>
<TABLE style='position:relative; font:normal 10pt Verdana;top=20' border='0' cellpadding='0' cellspacing='0' width='100%' height='100%' align='center'>
<TR style='background-Color:black; color:white'>
	<TD>1 - Personal Data<BR></TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'><BR>Name<BR>
	<input type="text" id="txtNome" name="txtNome" style="width=200px"></input><BR>
	E-Mail<BR>
	<input type="text" id="txtEMail" name="txtEMail" style="width=250px"></input><BR>	
	<BR></TD>
</TR>

<TR style='background-Color:black; color:white'>
	<TD>2 - Company name<BR></TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'><BR>
		<IMG ID='Img1' SRC='CompanyName.jpg' WIDTH='338' HEIGHT='98' STYLE="cursor:hand BORDER='0'" title='teste'></IMG>
	</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'>Company name which you were logged when the error ocurred<BR>
	<input type="text" id="txtCompany" name="txtCompany"></input><BR><BR></TD>
</TR>

<TR style='background-Color:black; color:white'>
	<TD>3 - Form Name<BR></TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'><BR>
		<IMG ID='Img1' SRC='FormName.jpg' WIDTH='794' HEIGHT='112' STYLE="cursor:hand BORDER='0'" title='teste'></IMG>
	</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'>Form Name<BR>
	<input type="text" id="txtFormName" name="txtFormName"></input><BR><BR></TD>
</TR>

<TR style='background-Color:black; color:white'>
	<TD>4 - Context</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'><BR>
		<IMG ID='Img1' SRC='Context.jpg' WIDTH='338' HEIGHT='98' STYLE="cursor:hand BORDER='0'" title='teste'></IMG>
	</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'>Context<BR>
	<input type="text" id="txtContext" name="txtContext"></input><BR><BR></TD>
</TR>

<TR style='background-Color:black; color:white'>
	<TD>5 - Last pressed button</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'><BR>
		<IMG ID='Img1' SRC='ButtonPressed1.jpg' WIDTH='486' HEIGHT='341' STYLE="cursor:hand BORDER='0'" title='teste'></IMG>
		<IMG ID='Img1' SRC='ButtonPressed2.jpg' WIDTH='486' HEIGHT='341' STYLE="cursor:hand BORDER='0'" title='teste'></IMG>
	</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'>Tollbar(1,2 or 3)&nbsp&nbspLast pressed button(between 1 and 15)<BR>
	<input type="text" id="txtToolBar" name="txtToolBar" style='width=110px'></input>
	<input type="text" id="txtLastButton" name="txtLastButton"></input><BR>
	If the error ocurred in another window, describe below which window and button<BR>
	<input type="text" id="txtAnotherWindow" name="txtAnotherWindow" style='width=590px'></input><BR><BR></TD>
</TR>

<TR style='background-Color:black; color:white'>
	<TD>6 - Error Window</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'><BR>
		<IMG ID='Img1' SRC='ErroMessage1.jpg' WIDTH='159' HEIGHT='122' STYLE="cursor:hand BORDER='0'" title='teste'></IMG>
		<IMG ID='Img1' SRC='ErroMessage2.jpg' WIDTH='524' HEIGHT='152' STYLE="cursor:hand BORDER='0'" title='teste'></IMG>
	</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'>If you have seen any system error message describe it below:<BR>
	<input type="text" id="txtErrorWindow" name="txtErrorWindow" style='width=590px'></input><BR><BR></TD>
</TR>

<TR style='background-Color:black; color:white'>
	<TD>7 - Error Description</TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'><BR>Describe how the error ocurred<BR>
	<textarea id="txtErrorDescription" name="txtErrorDescription" style='width=590px; height=120px'></textarea><BR><BR></TD>
</TR>
<TR>
	<TD style='position=relative; top : 0px ; width:100%; height:80%; valign:MIDDLE'>
		<input type="button" id="btnEnviar" Value='Send' name="btnEnviar" style='width:85px' onclick="btnEnviar_onclick()"></input>
	</TD>
</TR>

</TABLE>
<BR>

</BODY>
</HTML>
