/********************************************************************
js_overdso.js reduzida para WEB

Library javascript do grid

********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var __glb_ctrl = new Array();
var __glb_nIndex = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// EXECUCOES GLOBAIS ************************************************

// FINAL DE EXECUCOES GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES:

Funcoes de uso do programador:
    overdso()
    refresh()
    clearData()
    
    MoveFirst()
    MoveLast()
    MovePrevious()
    MoveNext()
    
    FieldValue(fldName)

Propriedades de uso do programador    
    URL
    ondatasetcomplete
    
    BOF
    EOF
    RecordCount

Funcoes disparadas por eventos, colocadas no codigo do programador
    
Funcoes internas da library    

Objetos internos da library

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Cria objeto dso para manobra de xml
Parametros:
    nenhum
Retorno:
    instancia do objeto overdso
********************************************************************/
function overdso()
{
    // objeto XMLDOM
    this.xmlDoc = new ActiveXObject('microsoft.XMLDOM');
    
    // o indice do objeto
    this.index = __glb_nIndex;
    __glb_nIndex++;
    
    // funcao de datasetcomplete do cliente
    this.ondatasetcomplete = null;
    
    // URL a chamar no servidor
    this.URL = null;
    
    // refrescar os dados no objeto XMLDOM
    this.refresh = __refresh;
    this.Refresh = __refresh;

    // remover os dados no objeto XMLDOM
    this.clearData = __clearData;
    this.ClearData = __clearData;

    // BOF, EOF e RecordCount
    this.BOF = true;
    this.EOF = true;
    
    this.recordCount = 0;
    this.RecordCount = 0;

    this.moveFirst = __moveFirst;
    this.moveLast = __moveLast;
    this.movePrevious = __movePrevious;
    this.moveNext = __moveNext;
    this.MoveFirst = __moveFirst;
    this.MoveLast = __moveLast;
    this.MovePrevious = __movePrevious;
    this.MoveNext = __moveNext;
    
    this.FieldValue = __fieldValue;
    this.fieldValue = __fieldValue;
    
    // VARIAVEIS E FUNCOES INTERNAS DO OBJETO -----------------------
    
    // variavel de timer
    this.timerInterval = null;
    
    // integridade dos dados
    this.dataIntegrity = false;

    // XML data pointers and parameters
    this.xmlDataTag = null;
    this.xmlSchemaTag = null;
    this.xmlCurrRow = null;
    this.xmlDataTagRows = null;
    this.xmlRowIndex = -1;

    this.initializeXMLDataPointersAndParams_ =
        __initializeXMLDataPointersAndParams;
        
    this.examineXMLArrived_ = __examineXMLArrived;
}

/********************************************************************
Envia requisicao ao servidor para obter dados no formato xml
Parametros:
    nenhum
Retorno:
    true se parametros necessarios estao disponiveis, caso
    contrario false
********************************************************************/
function __refresh()
{
    if ( (this.ondatasetcomplete == null) || (this.ondatasetcomplete == '') )
        return false;
    
    if ( (this.URL == null) || (this.URL == '') )
        return false;
 
    this.initializeXMLDataPointersAndParams_();
    
    __glb_ctrl[this.index] = this;
    
    this.timerInterval = window.setInterval('__glb_readStateChanged(' + '\'' + this.index + '\'' + ')', 50, 'javascript');       
    
    this.xmlDoc.load(this.URL);    
    
    return true;
}

/********************************************************************
Remove o no de dados do XMLDOM
Parametros:
    nenhum
Retorno:
    irrelevante
********************************************************************/
function __clearData()
{
    if ( this.RecordCount == 0 )
        return null;
    
    // Provisorio    
    this.BOF = true;
    this.EOF = true;
    this.RecordCount = 0;
    
    this.xmlDoc.lastChild.removeChild(this.xmlDoc.lastChild.lastChild);
}

/********************************************************************
FUNCAO GLOBAL ESTATICA
Controla retorno do xml com dados do servidor
Parametros:
    nenhum
Retorno:
    true se parametros necessarios estao disponiveis, caso
    contrario false
********************************************************************/
function __glb_readStateChanged(nIndex)
{
    try    
    {
        var readyState = __glb_ctrl[nIndex].xmlDoc.readyState;
        
        if ( readyState == 4 )
        {
            window.clearInterval(__glb_ctrl[nIndex].timerInterval);
            __glb_ctrl[nIndex].timerInterval = null;
            
            if (__glb_ctrl[nIndex].xmlDoc.parseError.errorCode != 0)
                __glb_ctrl[nIndex].dataIntegrity = false;
            else
                __glb_ctrl[nIndex].dataIntegrity = true;
                
            __glb_ctrl[nIndex].examineXMLArrived_();
            
            eval(__glb_ctrl[nIndex].ondatasetcomplete);
            
            __glb_ctrl[nIndex] = null;
        }
    }
    catch(e)
    {
        ;
    }
}

/********************************************************************
Inicializa ponteiros de dados e parametros do XML
Parametros:
    nenhum
Retorno:
    true/false
********************************************************************/
function __initializeXMLDataPointersAndParams()
{
    this.xmlSchemaTag = null;
    this.xmlDataTag = null;
    this.xmlDataTagRows = null;
    this.xmlCurrRow = null;
    this.BOF = true;
    this.EOF = true;
    this.RecordCount = 0;
    this.xmlRowIndex = -1;
}    

/********************************************************************
Examina XML que acabou de chegar e seta parametros do objeto
Parametros:
    nenhum
Retorno:
    irrelevante
********************************************************************/
function __examineXMLArrived()
{
    this.xmlSchemaTag = this.xmlDoc.lastChild.firstChild;
    this.xmlDataTag = this.xmlDoc.lastChild.lastChild;
    this.xmlDataTagRows = this.xmlDoc.lastChild.lastChild.childNodes;
    
    // O XML nao tem dados
    if ( this.xmlDataTagRows.length == 0 )
    {
        this.BOF = true;
        this.EOF = true;
        this.RecordCount = 0;
    }
    // O XML tem dados
    else
    {
        this.BOF = false;
        this.EOF = false;
        this.RecordCount = this.xmlDataTagRows.length;
        
        // posiciona ponteiro de rows na primeira linha
        this.xmlRowIndex = 0;
        this.xmlCurrRow = this.xmlDataTagRows.item(this.xmlRowIndex);
    }
}

/********************************************************************
Retorna valor de um campo da linha corrente, em funcao do nome do
campo
Parametros:
    fldName     - o nome do campo
Retorno:
    o valor do campo ou null
********************************************************************/
function __fieldValue(fldName)
{
    if ( this.RecordCount == 0 )
        return null;
        
    if ( fldName == null )
    {
        fldName = '';
    }
    
    fldName = fldName.toString();
    
    if ( this.BOF )
    {
        alert('BOF founded');
        return null;
    }    
    
    if ( this.EOF )
    {
        // alert('EOF founded');
        return null;
    }    
    
    if ( this.xmlCurrRow == null )
    {
        alert('No data row');
        return null;
    }
    
    var i, attr;
                    
    for ( i=0; i<this.xmlCurrRow.attributes.length; i++ )               
    {
        attr = this.xmlCurrRow.attributes.item(i);
        if ( attr.nodeName == fldName )
            return attr.nodeValue;
    }
    
    return null;      
}

/********************************************************************
Torna a primeira linha a linha corrente
Parametros:
    nenhum
Retorno:
    true/false
********************************************************************/
function __moveFirst()
{
    // Movimenta se tem dados
    if ( this.RecordCount == 0 )
        return false;
    
    this.xmlRowIndex = 0;
        
    this.BOF = false;    
    this.EOF = false;
    
    this.xmlCurrRow = this.xmlDataTagRows.item(this.xmlRowIndex);
    
    return true;
}

/********************************************************************
Torna a ultima linha a linha corrente
Parametros:
    nenhum
Retorno:
    true/false
********************************************************************/
function __moveLast()
{
    // Movimenta se tem dados
    if ( this.RecordCount == 0 )
        return false;
    
    this.xmlRowIndex = this.RecordCount - 1;
        
    this.BOF = false;    
    this.EOF = false;
    
    this.xmlCurrRow = this.xmlDataTagRows.item(this.xmlRowIndex);
    
    return true;
}

/********************************************************************
Torna a linha anterior a linha corrente
Parametros:
    nenhum
Retorno:
    true/false
********************************************************************/
function __movePrevious()
{
    // Movimenta se tem mais de uma linha
    if ( this.RecordCount < 2 )
    {
        this.BOF = true;
        return false;
    }    
    
    if ( this.xmlRowIndex == 0 )
    {
        this.BOF = true;
        return false;
    }    
    
    this.xmlRowIndex--;
    
    if ( this.xmlRowIndex == 0 )
        this.BOF = true;
    else    
        this.BOF = false;
    
    this.EOF = false;        
    
    this.xmlCurrRow = this.xmlDataTagRows.item(this.xmlRowIndex);
    
    return true;
}

/********************************************************************
Torna a linha seguinte a linha corrente
Parametros:
    nenhum
Retorno:
    true/false
********************************************************************/
function __moveNext()
{
    // Movimenta se tem mais de uma linha
    if ( this.RecordCount < 2 )
    {
        this.EOF = true;
        return false;
    }    
    
    if ( this.xmlRowIndex == (this.RecordCount - 1) )
    {
        this.EOF = true;
        return false;
    }    
    
    this.xmlRowIndex++;   
    
    this.BOF = false;
    
    if ( this.xmlRowIndex == (this.RecordCount) )
        this.EOF = true;        
    else
        this.EOF = false;
    
    this.xmlCurrRow = this.xmlDataTagRows.item(this.xmlRowIndex);
    
    return true;
}