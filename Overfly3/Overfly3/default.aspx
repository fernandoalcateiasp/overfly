﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="default.aspx.cs" Inherits="Overfly3._default" %>

<%         
    string pagesURLRoot;
    string strConn;
    string ApplicationName, aplicacaoRoot;
    bool DevMode;

    OVERFLYSVRCFGLib.OverflyMTS objSvrCfg = new OVERFLYSVRCFGLib.OverflyMTS();

    WSData.dataInterface oDataInterface = new WSData.dataInterface(
           System.Configuration.ConfigurationManager.AppSettings["application"]
       );
    pagesURLRoot = objSvrCfg.PagesURLRoot(
           System.Configuration.ConfigurationManager.AppSettings["application"]);

    aplicacaoRoot = pagesURLRoot;

    DevMode = objSvrCfg.DevMode(System.Configuration.ConfigurationManager.AppSettings["application"]);
    strConn = objSvrCfg.DataBaseStrConn(System.Configuration.ConfigurationManager.AppSettings["application"]);

    ApplicationName = oDataInterface.ApplicationName;

    string rsServerrRedirect, strSQLServerRedirect, IP, TipoIP;
    int bRedirecionar;


    Response.Write("<script ID=" + (char)34 + "serverCfgVars" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
    Response.Write("\r\n");

    Response.Write("var __APP_NAME__ = " + (char)39 + ApplicationName + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __PAGES_DOMINIUM__ = " + (char)39 + objSvrCfg.PagesDominium(System.Configuration.ConfigurationManager.AppSettings["application"]) + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __PAGES_URLROOT__ = " + (char)39 + objSvrCfg.PagesURLRoot(System.Configuration.ConfigurationManager.AppSettings["application"]) + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __DATABASE_DOMINIUM__ = " + (char)39 + objSvrCfg.DatabaseDominium(System.Configuration.ConfigurationManager.AppSettings["application"]) + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __DATABASE_ASPURLROOT__ = " + (char)39 + objSvrCfg.DatabaseASPPagesURLRoot(System.Configuration.ConfigurationManager.AppSettings["application"]) + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("var __DATABASE_WSURLROOT__ = " + (char)39 + objSvrCfg.PagesDominium(System.Configuration.ConfigurationManager.AppSettings["application"]) + "/WSOverflyRDS" + (char)39 + ";");
    Response.Write("\r\n");
    Response.Write("</script>");
    Response.Write("\r\n");

    string ip = Request.ServerVariables["REMOTE_ADDR"];
    // string sMACAddress = GetMACAddress();
    string sMachineName = System.Environment.MachineName;

    TipoIP = "E";//IP Externo

    if (DevMode)
    {
        ip = "172.16.10.20";//Trap para ip locaL
    }

    if ((ip.Substring(0, 3) == "127") || (ip.Substring(0, 3) == "172"))
    {
        TipoIP = "I"; //IP Interno;
    }

    aplicacaoRoot = aplicacaoRoot.Replace("/commonpages", "");
    aplicacaoRoot = aplicacaoRoot.Replace("/com", "");
    aplicacaoRoot = aplicacaoRoot.Replace("/co", "");

    var sTipoEI = (TipoIP == "I" ? "interno" : "externo");

    Aplicacao = DataInterfaceObj.getRemoteData("SELECT Aplicacao " +
                                "FROM SistemaAplicacoes a " +
                                "WHERE Aplicacao IN ('" + aplicacaoRoot + "') AND " + sTipoEI + " = 1 ");

    if (Aplicacao.Tables[1].Rows.Count >= 1)
    {
        bRedirecionar = 1;
    }
    else
    {
        bRedirecionar = 0;
    }

    // Nao redirecionar mais
    bRedirecionar = 0;

    if ((redirecionado == 0) && (bRedirecionar == 1))
    {
        string nRedirectTo;

        Login = DataInterfaceObj.getRemoteData("SELECT TOP 1 a.AplicacaoID " +
                 "FROM LOGs_Login a " +
                     "INNER JOIN SistemaAplicacoes b ON b.AplicacaoID=a.AplicacaoID " +
                 "WHERE " + sTipoEI + " = 1 " +
                 "ORDER BY LOGID DESC");

        if (Login.Tables[1].Rows.Count >= 1)
        {
            nRedirectTo = Login.Tables[1].Rows[0]["AplicacaoID"].ToString();
        }
        else
        {
            nRedirectTo = "0";
        }

        SQLAplicacoes = DataInterfaceObj.getRemoteData("SELECT TOP 1 Aplicacao " +
                                "FROM SistemaAplicacoes a " +
                                "WHERE AplicacaoID > " + nRedirectTo + " AND a.Habilitado = 1 AND " + sTipoEI + " = 1 " +
                                "ORDER BY AplicacaoID");

        if (SQLAplicacoes.Tables[1].Rows.Count >= 1)
        {
            nRedirectTo = SQLAplicacoes.Tables[1].Rows[0]["Aplicacao"].ToString() + "/default.aspx?Redirecionado=1";
            Response.Redirect(nRedirectTo);
            Response.End();
        }
        else
        {
            SQLAplicacoes = DataInterfaceObj.getRemoteData("SELECT TOP 1 Aplicacao " +
                                "FROM SistemaAplicacoes a " +
                                "WHERE a.Habilitado = 1 AND " + sTipoEI + " = 1 " +
                                "ORDER BY AplicacaoID");

            nRedirectTo = SQLAplicacoes.Tables[1].Rows[0]["Aplicacao"].ToString() + "/default.aspx?Redirecionado=1";
            Response.Redirect(nRedirectTo);
            Response.End();
        }
    }
%>

<%
        /*  Function ReplaceText(baseStr, patrn, replStr)
        Dim regEx                         ' Create variables.
        Set regEx = New RegExp            ' Create regular expression.
        regEx.Pattern = patrn             ' Set pattern.
        regEx.IgnoreCase = True           ' Make case insensitive.
        regEx.Global = True               ' Replace globally
        ReplaceText = regEx.Replace(baseStr, replStr)   ' Make replacement.
    End Function*/
%>

<html id="overflyHtml" name="overflyHtml">

<head>

    <title>Overfly</title>

    <%
        //Links de estilo, bibliotecas da automacao e especificas
        Response.Write("<LINK REL=" + (char)34 + "stylesheet" + (char)34 + " HREF=" + (char)34 + pagesURLRoot + "/overfly.css" + (char)34 + "type=" + (char)34 + "text/css" + (char)34 + ">");
        Response.Write("\r\n");

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/Defines.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CRecordsetParser.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CDatatransportSystem.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CReturnParam.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CFieldStructure.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CField.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CFields.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CRecordset.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CDatatransport.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/overflyRDSClient/CReturnParam.js" + (char)34 + "></script>");
        Response.Write("\r\n");

        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_sysbase.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_browsers.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_constants.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_htmlbase.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/system/js_strings.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/overfly.js" + (char)34 + "></script>");
        Response.Write("\r\n");
        Response.Write("<script LANGUAGE=" + (char)34 + "JavaScript" + (char)34 + " SRC=" + (char)34 + pagesURLRoot + "/images/toolbarimages.js" + (char)34 + "></script>");
        Response.Write("\r\n");
    %>

    <%
        //'********************************************************************
        //Base de dicionario de termos do sistema
        //'Portuguese - 246
        //'English    - 245

        Response.Write("\r\n");
        Response.Write("<script ID=" + (char)34 + "variousVars" + (char)34 + " LANGUAGE=" + (char)34 + "javascript" + (char)34 + ">");
        Response.Write("\r\n");
        //'Lingua corrente a ser usada
        //'Atualmente fixada a lingua portuguesa
        //'A variavel abaixo troca de valor toda vez que o usuario loga
        //'logo o valor abaixo sempre muda (poderiamos colocar = 0 na linha abaixo)
        Response.Write("\r\n");
        Response.Write("var _glb_currDicLang = 246;");
        Response.Write("\r\n");

        int i, j;
        string sCurrUso, sArrName, sArrElem = "", pDataUsoFld_Uso;
        pDataUsoFld_Uso = "";

        //Array dos usos dos termos do dicionario


        rsDataUso = DataInterfaceObj.getRemoteData("DECLARE @IdiomaDeID INT, @IdiomaParaID INT " +
            "SELECT @IdiomaDeID = 246, @IdiomaParaID = 245 " +
            "SELECT DISTINCT ISNULL(b.Uso, SPACE(0)) AS [Uso] " +
            "FROM Dicionario a WITH(NOLOCK), Dicionario_Traducoes b WITH(NOLOCK) " +
            "WHERE (a.EstadoID = 2 AND a.IdiomaID = @IdiomaDeID AND a.TermoID = b.TermoID AND " +
                "b.IdiomaID = @IdiomaParaID)" +
            "UNION " +
            "SELECT DISTINCT ISNULL(b.Uso, SPACE(0)) AS [Uso] " +
            "FROM Dicionario a WITH(NOLOCK), Dicionario_Traducoes b WITH(NOLOCK) " +
            "WHERE (a.EstadoID = 2 AND a.IdiomaID = @IdiomaParaID AND a.TermoID = b.TermoID AND " +
                "b.IdiomaID = @IdiomaDeID)" +
            "ORDER BY [Uso]");

        //'rsDataUso.Fields("Uso")
        if (rsDataUso.Tables[1].Rows.Count > 1)
        {
            pDataUsoFld_Uso = rsDataUso.Tables[1].Rows[0]["Uso"].ToString();
        }
        Response.Write("\r\n");

        //Arrays dos termos do dicionario
        rsDataDicTerms = DataInterfaceObj.getRemoteData("DECLARE @IdiomaDeID INT, @IdiomaParaID INT " +
                                                        "SELECT @IdiomaDeID = 246, @IdiomaParaID = 245 " +
                                                        "SELECT UPPER(a.Termo) AS [246], b.Traducao AS [245], ISNULL(b.Uso, SPACE(0)) AS [Uso] " +
                                                        "FROM Dicionario a WITH(NOLOCK), Dicionario_Traducoes b WITH(NOLOCK) " +
                                                        "WHERE (a.EstadoID = 2 AND a.IdiomaID = @IdiomaDeID AND a.TermoID = b.TermoID AND " +
                                                            "b.IdiomaID = @IdiomaParaID) " +
                                                        "UNION ALL " +
                                                        "SELECT UPPER(b.Traducao) AS [246], a.Termo AS [245], ISNULL(b.Uso, SPACE(0)) AS [Uso] " +
                                                        "FROM Dicionario a WITH(NOLOCK), Dicionario_Traducoes b WITH(NOLOCK) " +
                                                        "WHERE (a.EstadoID = 2 AND a.IdiomaID = @IdiomaParaID AND a.TermoID = b.TermoID AND " +
                                                            "b.IdiomaID = @IdiomaDeID) " +
                                                        "ORDER BY [Uso], [246]");

        string pDataDicTermsFld_Uso, pDataDicTermsFld_245, pDataDicTermsFld_246;

        pDataDicTermsFld_Uso = "";
        pDataDicTermsFld_245 = "";
        pDataDicTermsFld_246 = "";

        if (rsDataDicTerms.Tables[1].Rows.Count > 1)
        {
            pDataDicTermsFld_Uso = rsDataDicTerms.Tables[1].Rows[0]["Uso"].ToString();
            pDataDicTermsFld_245 = rsDataDicTerms.Tables[1].Rows[0]["245"].ToString();
            pDataDicTermsFld_246 = rsDataDicTerms.Tables[1].Rows[0]["246"].ToString();
        }

        for (i = 0; rsDataUso != null && i < rsDataUso.Tables[1].Rows.Count; i++)
        {
            if (rsDataUso.Tables[1].Rows[i]["Uso"].ToString() == null)
            {
                sCurrUso = null;
            }
            else if (rsDataUso.Tables[1].Rows[i]["Uso"].ToString() == "")
            {
                sCurrUso = "";
            }
            else
                sCurrUso = rsDataUso.Tables[1].Rows[i]["Uso"].ToString();

            //Cria os arrays dos termos do dicionario ************************
            sArrName = "__dataDic_";
            sArrElem = sArrName;

            if (sCurrUso != null)
            {
                sArrName = sArrName + sCurrUso;
            }

            Response.Write("var " + sArrName + " = new Array();");

            Response.Write("\r\n");

            int icount = 0;

            //'Preenche os arrays dos termos do dicionario
            for (int idic = 0; rsDataDicTerms != null && idic < rsDataDicTerms.Tables[1].Rows.Count; idic++)
            {
                pDataDicTermsFld_Uso = rsDataDicTerms.Tables[1].Rows[idic]["Uso"].ToString();
                pDataDicTermsFld_245 = rsDataDicTerms.Tables[1].Rows[idic]["245"].ToString();
                pDataDicTermsFld_246 = rsDataDicTerms.Tables[1].Rows[idic]["246"].ToString();


                if (sCurrUso == pDataDicTermsFld_Uso)
                {
                    //Escreve cada elementos do array
                    Response.Write(sArrName + "[" + icount + "] = ");

                    Response.Write("new Array(");

                    if (pDataDicTermsFld_246 == null)
                    {
                        Response.Write("null, ");
                    }
                    else
                    {
                        Response.Write((char)34 + (pDataDicTermsFld_246).ToString() + (char)34 + ", ");
                    }

                    if (pDataDicTermsFld_245 == null)
                    {
                        Response.Write("null, ");
                    }
                    else
                    {
                        Response.Write((char)34 + (pDataDicTermsFld_245).ToString() + (char)34);
                    }

                    Response.Write(" ); ");
                    Response.Write("\r\n");
                    icount += 1;
                }
                else
                {
                    icount = 0;
                }
            }
            //Final de cria os arrays dos termos do dicionario ***************	
            Response.Write("\r\n");
        }

        //'Cria os arrays dos dicionarios ************************
        sArrName = "__arrDics";
        Response.Write("var " + sArrName);
        Response.Write(" = new Array();");
        Response.Write("\r\n");

        string sArrElem2 = "";


        for (i = 0; rsDataUso != null && i < rsDataUso.Tables[1].Rows.Count; i++)
        {
            pDataUsoFld_Uso = rsDataUso.Tables[1].Rows[i]["Uso"].ToString();

            //Preenche os arrays dos dicionarios ************************
            if (pDataUsoFld_Uso.ToString() == null)
            {
                sCurrUso = null;
            }
            else if (pDataUsoFld_Uso.ToString() == "")
            {
                sCurrUso = "";
            }
            else
                sCurrUso = pDataUsoFld_Uso.ToString();

            if (sCurrUso != null)
            {
                sArrElem2 = sArrElem + sCurrUso;
            }

            Response.Write("__arrDics[" + i + "] = new Array(");

            if (pDataUsoFld_Uso == null)
                Response.Write("null");
            else if (pDataUsoFld_Uso == "")
            {
                Response.Write("null");
            }
            else
                Response.Write((char)34 + pDataUsoFld_Uso + (char)34);

            Response.Write(", ");

            Response.Write(sArrElem2);

            Response.Write(");");

            Response.Write("\r\n");

        }
        Response.Write("\r\n");
        Response.Write("</script>");
      
    %>

    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>

</head>

<body id="overflyBody" name="overflyBody" language="javascript" onload="return window_onload()" onbeforeunload="return window_onbeforeunload()" onunload="return window_onunload()">
    <iframe id="frameStatusBar" name="frameStatusBar" frameborder="no"></iframe>
    <iframe id="frameFastButtons" name="frameFastButtons" frameborder="no"></iframe>
    <iframe id="frameMainMenu" name="frameMainMenu" frameborder="no"></iframe>
    <iframe id="frameGen01" name="frameGen01" frameborder="no"></iframe>
    <iframe id="frameGen02" name="frameGen02" frameborder="no"></iframe>
    <iframe id="frameGen03" name="frameGen03" frameborder="no"></iframe>
    <iframe id="frameGen04" name="frameGen04" frameborder="no"></iframe>

</body>

</html>
