/********************************************************************
modalcontroledocumento.js

Library javascript para o modalcontroledocumento.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_nTipoFirstDocToLoad = null;
var glb_loadDocumentoTimer = null;
var glb_oNavList = new _OverList();
var glb_nEmpresaID = 0;
var glb_sAnchorDest = '';
var dsoGravaHTML = new CDatatransport('dsoGravaHTML');
var dsoCancelarEstudo = new CDatatransport('dsoCancelarEstudo');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
FUNCOES GERAIS:

window_onload_prg()
mountModalAccordingToContext()
loadPageFromServerInFrame(nDocumentoID, sAnchorID, nTipoDocumento, bNavInSameDoc)
strParsToLoadPageFromServer(nDocumentoID, nTipoDocumento, sAnchorID, nAction )

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Funcao do programador chamada pela automacao.
Window on Load de uso do programador
********************************************************************/
function window_onload_prg() 
{
    ;
}    

/********************************************************************
Funcao do programador chamada pela automacao.
Esconde e desabilita os controles nao usados no contexto corrente
Faz a montagem da tela dos controles usados no contexto corrente
********************************************************************/
function mountModalAccordingToContext()
{
	with (btnCanc)
	{
		style.left = 0;
		style.top = 0;
		style.height = 0;
		style.width = 0;
		style.visibility = 'hidden';
		disabled = true;
	}

	adjustElementsInForm([['lblFiltro', 'txtFiltro', 4, 1, 0, 20],
	                      ['lblDocumento','selDocumento', 50, 1,-10],
                          ['lblVersao','selVersao',9,1],
                          ['lbldtEmissao','txtdtEmissao',10,1],
                          ['lblTotPags','txtTotPags',3,1],
                          ['btnVoltar', 'btn', 62, 1, 5],
                          ['btnCancelarEstudo', 'btn', 92, 1, -50],
                          ['btnLimparAlteracoes', 'btn', 62, 1, 93],
                          ['btnEditarVisualizar', 'btn', 62, 1, 5]], null, null, true);
	
	lblDocumento.style.width = 'auto';
	txtdtEmissao.readOnly = true;
	txtTotPags.readOnly = true;
	txtFiltro.maxLength = 3;
	
	//txtVersao.value = '';
	clearComboEx(['selVersao']);
	txtdtEmissao.value = '';
	btnVoltar.title = '';

	btnOK.style.visibility = 'hidden';
	btnOK.disabled = true;

	btnVoltar.style.height = parseInt(btnOK.currentStyle.height, 10);
	btnCancelarEstudo.style.height = parseInt(btnOK.currentStyle.height, 10);
	btnLimparAlteracoes.style.height = parseInt(btnOK.currentStyle.height, 10);
	btnEditarVisualizar.style.height = parseInt(btnOK.currentStyle.height, 10);
	btnVoltar.value = 'Retornar';
}

/********************************************************************
Funcao do programador chamada pela automacao.
Carrega as paginas do servidor.

Parametros:
	nDocumentoID - id do documento a carregar
	sAnchorID - navegacao automatica para um achor da pagina a ser
	            executado apos o carregamento
	nTipoDocumento - tipo do documento
	bNavInSameDoc - flag para executar o carregamento ou nao.
	                Eventualmente sera executada apenas uma navegacao
	                para un anchor
********************************************************************/
function loadPageFromServerInFrame(nDocumentoID, sAnchorID, nTipoDocumento, bNavInSameDoc)
{
	if ( glb_loadDocumentoTimer != null )
	{
		window.clearInterval(glb_loadDocumentoTimer);
		glb_loadDocumentoTimer = null;
	}
	
	// Automacao - nao mexer na chamada abaixo
	loadPageFromServerInFrame_Aut(nDocumentoID, sAnchorID, nTipoDocumento, bNavInSameDoc);
}


/********************************************************************
Funcao do programador chamada pela automacao 
Define parametros para carregar documento do servidor
Aqui usamos a variavel glb_sCallOrigin
********************************************************************/
function strParsToLoadPageFromServer(nDocumentoID, nTipoDocumento, sAnchorID, nAction )
{
    var nUserID = getCurrUserID();

	var strPars = new String();
	
	strPars = '?nEmpresaID=' + escape(glb_nEmpresaID);
	strPars += '&nDocumentoID=' + escape(nDocumentoID);
	
	// Regras em 29/09/2003
	// Se origem da chamada e estudo, preferencialmente so navega entre estudos
	// (o documentotemplate.asp tem regra especifica para contrariar
	// esta preferencia).
	// Se origem da chamada e texto, preferencialmente so navega entre textos
	// (o documentotemplate.asp tem regra especifica para contrariar
	// esta preferencia).
	if ( glb_sCallOrigin == 'E' )
	{
		strPars += '&nTipoDocToLoad=' + escape('-1');
	}
	else
	{
		if ( nTipoDocumento == null )
			strPars += '&nTipoDocToLoad=' + escape('NULL');
		else
		    strPars += '&nTipoDocToLoad=' + escape(nTipoDocumento);
	}
	
	if (window.top.formID != 7110)
        glb_sCallOrigin2 = 'btn3';

	strPars += '&nUserID=' + escape(nUserID);
	strPars += '&nAction=' + escape(nAction);
	strPars += '&sAnchorID=' + escape((sAnchorID == null ? '__0' : sAnchorID));
	strPars += '&sCallOrigin2=' + escape(glb_sCallOrigin2);
	
	return strPars;
}