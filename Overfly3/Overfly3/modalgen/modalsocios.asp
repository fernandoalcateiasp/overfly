
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalsociosHtml" name="modalsociosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
      
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/modalsocios.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Dim i, sCaller, nPessoaID, nTipoPessoaID, nCreditoID, nUserID

sCaller = ""
nPessoaID = 0
nTipoPessoaID = 0
nUserID = 0

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

If ( sCaller <> "" ) Then
    sCaller = UCase(sCaller)
End If

'nPessoaID
For i = 1 To Request.QueryString("nPessoaID").Count    
    nPessoaID = Request.QueryString("nPessoaID")(i)
Next

'nTipoPessoaID
For i = 1 To Request.QueryString("nTipoPessoaID").Count    
    nTipoPessoaID = Request.QueryString("nTipoPessoaID")(i)
Next

'nCreditoID
For i = 1 To Request.QueryString("nCreditoID").Count
    nCreditoID = Request.QueryString("nCreditoID")(i)
Next

'nUserID
For i = 1 To Request.QueryString("nUserID").Count    
    nUserID = Request.QueryString("nUserID")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nPessoaID = " & nPessoaID & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoPessoaID = " & nTipoPessoaID & ";"
Response.Write vbcrlf

Response.Write "var glb_nCreditoID = " & nCreditoID & ";"
Response.Write vbcrlf

'Necessario para compatibilidade da automacao
Response.Write "var glb_nUserID = " & nUserID & ";"

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf

%>

<script id="wndJSProc" language="javascript">
</script>

<script language="javascript" for="fg" event="ValidateEdit">
    js_modalsocio_ValidateEdit();
</script>

<script language="javascript" for="fg" event="KeyPress">
    js_modalsocioKeyPress(arguments[0]);
</script>

<script language="javascript" for="fg" event="AfterEdit">
    js_modalsocio_AfterEdit(arguments[0], arguments[1]);
</script>

<script language="javascript" for="fg" event="AfterRowColChange">
    js_modalsocio_AfterRowColChange(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
    js_modalsocioBeforeEdit(fg, fg.Row, fg.Col);
</SCRIPT>

<script language="javascript" for="fg" event="DblClick">
    js_fg_modalsocioDblClick(fg, fg.Row, fg.Col);
</script>

<script language="javascript" for="fg" event="BeforeMouseDown">
    js_fg_modalsocioBeforeMouseDown(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</script>

<script language="javascript" for="fg" event="BeforeRowColChange">
    js_modalgerenciarprodutoscotador_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</script>

<script language="javascript" for="fg" event="AfterSort">
    fg_AfterSort();
</script>

<script language="javascript" for="fg" event="BeforeSort">
    fg_BeforeSort();
</script>

<script language="javascript" for="fg" event="MouseDown">
    fg_MouseDown_cotador(fg.Row, fg.Col);
</script>

<script language="javascript" for="fg" event="MouseMove">
    js_fg_modalsocioMouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
</script>

<script language="javascript" for="fg" event="EnterCell">
    js_fg_EnterCell(fg);
</script>

</head>

<body id="modalsociosBody" name="modalsociosBody" LANGUAGE="javascript" onload="return window_onload()">
    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>

    <div id="divSocios" name="divSocios" class="divGeneral">
        <input type="button" id="btnResumo" name="btnResumo" value="Resumo" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnIncluir" name="btnIncluir" value="Novo" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnExcluir" name="btnExcluir" value="Excluir" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnGravar" name="btnGravar" value="Gravar" language="javascript" onclick="return btn_onclick(this)" class="btns" />
        <input type="button" id="btnListar" name="btnListar" value="Listar" language="javascript" onclick="return btn_onclick(this)" class="btns" />
    </div>
    
    <!-- Divs de grid -->
    <div id="divGrids" name="divGrids" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" viewastext></object>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
</body>

</html>
