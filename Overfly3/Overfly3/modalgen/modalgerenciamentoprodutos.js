/********************************************************************
modalgerenciamentoprodutos.js

Library javascript para o modalgerenciamentoprodutos.asp
********************************************************************/

// VARIAVEIS GLOBAIS - Inicio ***************************************

var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_dataGridWasChanged = false;  // Controla se algum dado do grid foi alterado
var glb_getServerData = 0;
var glb_sFiltroDireitos = '';
var glb_sReadOnly = '';
var glb_sReadOnlyClassFiscal = '';
var glb_sCustosReadOnly = '*';
var glb_PrecoBaseMin_ReadOnly = '';
var glb_NCM_ReadOnly = '';
var glb_Pagamento_ReadOnly = '*';
var glb_PagamentoDias_ReadOnly = '*';
var glb_Fim_ReadOnly = '';
var glb_LoteID_ReadOnly = '';
var glb_PE_ReadOnly = '';
var glb_PV_ReadOnly = '';
var glb_MultiploCompra_ReadOnly = '';
var glb_refreshGrid = null;
var glb_refreshGrid2 = null;
var glb_timerUnloadWin = null;
var glb_nDOSsCmbs = 0;
var glb_Proporcional = 1;
var glb_divFGExtended = 0;
var glb_divFGCollapse = 0;
var glb_divFG1Fornecedor = 258;
var glb_divFG2Fornecedor = 90;
var glb_bDivFGIsCollapse = false;
var glb_PassedOneInDblClick = false;
var glb_EnableChkOK = true;
var glb_nLastStateClicked = null;
var glb_nMoedaFaturamentoID = 0;
var glb_sMoeda = '';
var glb_bFillGridAfterImport = false;
var glb_nTopGrid = 0;
var glb_nRegistros = 0;
var glb_sFiltroContexto = '';
var glb_nProprietarioID = 0;
var glb_nAlternativoID = 0;
var glb_sMetodoPesquisa = '';
var glb_sChavePesquisa = '';
var glb_sChavePesquisa2 = '';
var glb_sChavePesquisa3 = '';
var glb_sArgumento = '';
var glb_sOperator = '';
var glb_sPesquisa = '';
var glb_sFiltro = '';
var glb_sInvertido = '';
var glb_sFiltroEspecifico = '';
var glb_sFiltroFamilia = '';
var glb_sFiltroMarca = '';
var glb_sFiltroLinha = '';
var glb_sFiltroProprietario = '';
var glb_sOrdem = '';
var glb_nValue = 0;
var glb_Excel = 0;
var glb_Inicio = true;

var glb_HouveAlteracao = false;

// Contantes de servi�os.
var CICLO_DE_VIDA = 1;
var PREVEVISAO_DE_VENDAS = 2;
var FORMACAO_DE_PRECO = 3;
//var INFORMACOES_BASICAS = 4;
var GERENCIA_DE_ESTOQUE = 5;
var INTERNET = 6;
var INCONSISTENCIAS = 7;
var GERAL = 8;

// Constatnes de filtros espec�ficos.
var COMPRAS_PENDENTES = 41019;
var ARGUMENTO_VENDAS = 41020;

// Constantes de Argumentos de Venda.
var AV_NADA = -2;
var AV_SEM = -1;
var AV_OK = 1;
var AV_NOK = 0;
var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbMoeda = new CDatatransport("dsoCmbMoeda");
var dsoFiltroEspecifico = new CDatatransport("dsoFiltroEspecifico");
var dsoFamilia = new CDatatransport("dsoFamilia");
var dsoMarca = new CDatatransport("dsoMarca");
var dsoLinha = new CDatatransport("dsoLinha");
var dsoLote = new CDatatransport("dsoLote");
var dsoPtax = new CDatatransport("dsoPtax");
var dsoLinguaArgVendas = new CDatatransport("dsoLinguaArgVendas");
var dsoPrevisaoVendas = new CDatatransport("dsoPrevisaoVendas");
var dsoCmbAtualizacao = new CDatatransport("dsoCmbAtualizacao");
var dsoDireitos = new CDatatransport("dsoDireitos");
var dsoDireitosClassFiscal = new CDatatransport("dsoDireitosClassFiscal");
var dsoDireitosB3 = new CDatatransport("dsoDireitosB3");
var dsoFornecedores = new CDatatransport("dsoFornecedores");
var dsoProjetos = new CDatatransport("dsoProjetos");
var dsoClassFiscal = new CDatatransport("dsoClassFiscal");
var dsoImpostos = new CDatatransport("dsoImpostos");
var dsoTransacao = new CDatatransport("dsoTransacao");
var dsoPrazo = new CDatatransport("dsoPrazo");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoArgumentoVenda = new CDatatransport("dsoArgumentoVenda");
var dsoArgumentoVendaTotais = new CDatatransport("dsoArgumentoVendaTotais");
var dsoDataFields = new CDatatransport("dsoDataFields");

// VARIAVEIS GLOBAIS - Fim ******************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
fieldExists(dso, fldName)

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalgerenciamentoprodutos.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalgerenciamentoprodutos.ASP

js_fg_modalgerenciamentoprodutosBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalgerenciamentoprodutosDblClick( grid, Row, Col)
js_modalgerenciamentoprodutosKeyPress(KeyAscii)
js_modalgerenciamentoprodutos_AfterRowColChange
js_modalgerenciamentoprodutos_ValidateEdit()
js_modalgerenciamentoprodutos_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES
// function window_onload() - Inicio (Configura o html) *************
function window_onload() {
    dealWithObjects_Load();
    dealWithGrid_Load();

    __adjustFramePrintJet();

    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    glb_nRegistros = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    glb_nProprietarioID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selProprietariosPL.value');

    // glb_nAlternativoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selAlternativo.value');
    glb_nAlternativoID = 0;  // Ativar a linha de cima

    // glb_sFiltroContexto = getCmbCurrDataInControlBar('sup', 1);
    glb_sFiltroContexto = '(a.TipoRelacaoID = 61)';  // Ativar a linha de cima CRIAR DSO PARA EXECUTAR O FILTRO

    glb_sMetodoPesquisa = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selMetodoPesq.value');

    glb_sChavePesquisa = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selPesquisa.value');

    if (glb_sChavePesquisa == 'aRelacaoID')
        glb_sChavePesquisa = 'a.RelacaoID';
    else if (glb_sChavePesquisa == 'aSujeitoID')
        glb_sChavePesquisa = 'a.SujeitoID';
    else if (glb_sChavePesquisa == 'dFantasia')
        glb_sChavePesquisa = '(SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = a.SujeitoID))';
    else if (glb_sChavePesquisa == 'aObjetoID')
        glb_sChavePesquisa = 'a.ObjetoID';
    else if (glb_sChavePesquisa == 'eConceito')
        glb_sChavePesquisa = 'a.RelacaoID';
    //else if (glb_sChavePesquisa == 'eConceito')
    //{
    //    glb_sChavePesquisa = '(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ObjetoID))';
    //    glb_sChavePesquisa2 = 'a.ObjetoID';
    //    glb_sChavePesquisa3 = '(SELECT aa.Conceito FROM Conceitos aa WITH(NOLOCK) INNER JOIN Conceitos bb WITH(NOLOCK) ON (aa.ConceitoID = bb.MarcaID) WHERE (bb.ConceitoID = a.ObjetoID)) ';

    //}

    //glb_sArgumento = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'txtArgumento.value');

    if (glb_sArgumento.length > 0) {
        if (glb_sMetodoPesquisa == 'DEF') {
            if ((glb_sArgumento.substring(0, 1) == '%') || (glb_sArgumento.substring(glb_sArgumento.length, 1) == '%'))
                glb_sOperator = 'LIKE';
            else {
                if (glb_sOrdem == 'DESC')
                    glb_sOperator = '<=';
                else
                    glb_sOperator = '>=';
            }

            glb_sArgumento = ('\'' + glb_sArgumento + '\'');
        }
        else {
            glb_sOperator = 'LIKE';

            if (glb_sMetodoPesquisa == 'COM')
                glb_sArgumento = ('\'' + glb_sArgumento + '%\'');
            else if (glb_sMetodoPesquisa == 'TERM')
                glb_sArgumento = ('\'%' + glb_sArgumento + '\'');
            else if (glb_sMetodoPesquisa == 'CONT')
                glb_sArgumento = ('\'%' + glb_sArgumento + '%\'');
        }

        if (glb_sChavePesquisa == '(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ObjetoID))') {
            glb_sPesquisa = (' AND ((' + glb_sChavePesquisa + ' ' + glb_sOperator + ' ' + glb_sArgumento + ') OR ' +
            '(' + glb_sChavePesquisa2 + ' ' + glb_sOperator + ' ' + glb_sArgumento + ') OR ' +
            '(' + glb_sChavePesquisa3 + ' ' + glb_sOperator + ' ' + glb_sArgumento + ')) ');
        }
        else
            glb_sPesquisa = (' AND (' + glb_sChavePesquisa + ' ' + glb_sOperator + ' ' + glb_sArgumento + ') ');
    }

    if (glb_nProprietarioID != 0)
        glb_sFiltroProprietario = ' AND (a.ProprietarioID = ' + glb_nProprietarioID + ')';
    else
        glb_sFiltroProprietario = '';

    glb_sFiltro = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'txtFiltro.value');
    glb_sFiltro = (glb_sFiltro == '' ? '' : (' AND (' + glb_sFiltro + ') '));


    glb_sInvertido = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'chkOrdem.checked');
    glb_sOrdem = (glb_sInvertido ? 'DESC' : '');

    // ajusta o body do html
    with (modalgerenciamentoprodutosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // Preenche as linguas.
    preencherLinguaArgVendas();

    // configuracao inicial do html
    setupPage();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 0;
    modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

}
// function window_onload() - Fim ***********************************

// function setupPage() - Inicio (Configuracao inicial do html) *****
function setupPage() {
    secText('Gerenciamento de produtos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    // desabilita o botao OK
    btnOK.disabled = true;

    // Ajusta elementos no divControls	

    adjustElementsInForm([
	        ['lblPesquisa', 'chkPesquisa', 3, 1, -7],
	        ['lblServico', 'selServico', 19, 1, -2],
	        ['lblFiltroEspecifico', 'selFiltroEspecifico', 19, 1, -4],
	        ['lblColunas', 'chkColunas', 3, 1, -6],
    	    ['lblCampos', 'chkCampos', 3, 1, -6],
            ['lblPercentualPV', 'txtPercentualPV', 4, 2, -8],
            ['lblEnc', 'chkEnc', 3, 2, 2],
            ['lblFornecedor', 'selFornecedor', 20, 2, -2],
            ['lblTransacao', 'selTransacao', 9, 2],
            ['lblFinanciamento', 'selFinanciamento', 17, 2],
            ['lblProjeto', 'selProjeto', 23, 2],
            //Campos incluidos para o Lote. Inicio. BJBN
            ['lblConsolidar', 'chkConsolidar', 3, 2],
            ['lblLote', 'selLote', 10, 2],
            ['lblDescricao', 'txtDescricao', 10, 2],
	        //Campos incluidos para o Lote. Fim. BJBN
            ['lblPtax', 'txtPtax', 6, 2]
    ], null, null, true);

    selFamilia.style.visibility = 'hidden';
    selMarca.style.visibility = 'hidden';
    selLinha.style.visibility = 'hidden';

    lblColunas.style.visibility = 'inherit';
    chkColunas.style.visibility = 'inherit';
    chkColunas.onclick = showHideCols;
    btnComprar.style.height = btnOK.offsetHeight;
    btnComprar.style.width = 48;
    btnBatch.style.height = btnOK.offsetHeight;
    btnBatch.style.width = 48;
    lblPercentualPV.style.visibility = 'inherit';
    txtPercentualPV.style.visibility = 'inherit';
    txtPercentualPV.onkeypress = verifyNumericEnterNotLinked;
    txtPercentualPV.onkeyup = txtPercentualPV_onkeyup;
    txtPercentualPV.setAttribute('verifyNumPaste', 1);
    txtPercentualPV.setAttribute('thePrecision', 3, 1);
    txtPercentualPV.setAttribute('theScale', 0, 1);
    txtPercentualPV.setAttribute('minMax', new Array(0, 999), 1);
    txtPercentualPV.onfocus = selFieldContent;
    txtPercentualPV.maxLength = 3;
    txtPercentualPV.value = '100';

    selFiltroEspecifico.onchange = limpaGrid;
    selServico.onchange = selServico_onchange;
    chkPesquisa.onclick = chkPesquisa_onclick;
    chkEnc.onclick = chkEnc_onclick;
    selFamilia.onchange = selFamilia_onchange;
    selMarca.onchange = selMarca_onchange;
    btnCalcularPV.onclick = btnCalcularPV_onclick;

    lblLote.style.width = 80;
    selLote.onchange = selLote_onchange;

    btnImprimir.style.height = btnComprar.offsetHeight;
    btnImprimir.style.width = 48;

    btnExcel.style.height = btnComprar.offsetHeight;
    btnExcel.style.width = 48;

    lblCampos.style.visibility = 'inherit';
    chkCampos.style.visibility = 'inherit';
    chkCampos.checked = false;
    lblCampos.onclick = lblCampos_onclick;
    chkCampos.onclick = chkCampos_onclick;

    //Pesquisa
    lblFamilia.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selFamilia.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selFamilia.disabled = true;
    lblMarca.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selMarca.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selMarca.disabled = true;
    lblLinha.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selLinha.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selLinha.disabled = true;

    // Ajusta o tamanho do combo das linguas.
    with (selIdiomaArgumentosVenda.style) {
        width = 85;
    }

    // Ajusta o tamanho do combo dos filtros dos argumentos de venda.
    with (lblFiltroArgumentosVenda.style) {
        visibility = 'hidden';
    }

    with (selFiltroArgumentosVenda.style) {
        width = 75;
        visibility = 'hidden';
    }
    selFiltroArgumentosVenda.setAttribute("clausulaWhere", "", 1);


    // ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;
        height = selServico.offsetTop + selServico.offsetHeight;
    }

    selTransacao.disabled = true;
    //selTransacao.onchange = selTransacao_onchange;

    lblProjeto.style.top = lblServico.offsetTop;
    selProjeto.style.top = selServico.offsetTop;
    selProjeto.style.height = 70;

    selFinanciamento.disabled = true;

    selFornecedor.disabled = true;
    selFornecedor.onchange = selFornecedor_onchange;

    // Aqui ajusta os campos nos divs (funcao adjustElementsInForm)	

    adjustElementsInForm([['lblCustoMedio', 'txtCustoMedio', 11, 1, -10, -10],
						  ['lblCustoReposicao', 'txtCustoReposicao', 11, 1],
                          ['lblUltimaEntrada', 'txtUltimaEntrada', 10, 1, 10],
                          ['lblUltimaSaida', 'txtUltimaSaida', 10, 1],
                          ['lblDias', 'txtDias', 4, 1],
                          ['lblDisponibilidade', 'txtDisponibilidade', 10, 1],
                          ['lblEstoqueInternet', 'txtEstoqueInternet', 6, 1, -10]], null, null, true);

    // alignButtons();
    // A altura do div � funcao do campo mais baixo
    // trocar o numero duro abaixo
    var divsInfHeight = txtEstoqueInternet.offsetTop + txtEstoqueInternet.offsetHeight;
    var divsInfBottom = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10);
    var divInfTop = divsInfBottom - divsInfHeight;



    //divControlsGerProd
    with (divControlsGerProd.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divInfTop;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = divsInfHeight;
        visibility = 'hidden';
    }

    with (divArgumentoVendas.style) {
        border = 'none';
        backgroundColor = 'transparent';
        visibility = 'hidden';

        top = 450;
        left = 70;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = divsInfHeight;
    }
    with (lblArgumentoDeVenda.style) {
        visibility = 'inherit';
        top = 0;
        left = 0;
    }
    with (txtArgumentoDeVenda.style) {
        visibility = 'inherit';
        top = parseInt(lblArgumentoDeVenda.currentStyle.top, 10) + 15;
        left = lblArgumentoDeVenda.currentStyle.left;

        width = 713;
        height = 100;
    }
    with (lblOK.style) {
        visibility = 'inherit';
        top = lblArgumentoDeVenda.currentStyle.top;
        left = parseInt(lblArgumentoDeVenda.currentStyle.left, 10) + parseInt(txtArgumentoDeVenda.currentStyle.width, 10) + 15;
    }
    with (chkOK.style) {
        visibility = 'inherit';
        top = parseInt(lblOK.currentStyle.top, 10) + 10;
        left = parseInt(lblOK.style.left) - 73;
    }

    with (lblQuantOK.style) {
        visibility = 'inherit';
        top = parseInt(lblArgumentoDeVenda.currentStyle.top);
        left = parseInt(lblArgumentoDeVenda.currentStyle.left) + 140;
        width = 560;
    }

    //As duas alturas do divFG
    var divFG_top = divControls.offsetTop + divControls.offsetHeight + ELEM_GAP;
    glb_nTopGrid = divFG_top;
    glb_divFGExtended = Math.abs(parseInt(btnOK.currentStyle.top, 10) +
	                             parseInt(btnOK.currentStyle.height, 10) - divFG_top) + 10;
    glb_divFGCollapse = glb_divFGExtended - divsInfHeight - ELEM_GAP - 10;

    // ajusta o divFG
    with (divFG.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFG_top;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = glb_divFGExtended;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) - 2;
        height = parseInt(divFG.style.height, 10) - 2;
    }

    with (divControlsGerProd.style) {
        left = ELEM_GAP;
        top = parseInt(divFG.style.height) + divFG_top;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 30;
    }

    // ajusta o divFG2
    with (divFG2.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFG_top;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = glb_divFG2Fornecedor;
        visibility = 'hidden';
    }

    with (fg2.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10) - 2;
        height = parseInt(divFG2.style.height, 10) - 2;
    }

    collapseExtendedDivFG();

    // Esconde o botao fechar
    btnCanc.style.visibility = 'hidden';

    // Muda o botao OK de posicao
    btnComprar.insertAdjacentElement('AfterEnd', btnOK);
    with (btnOK.style) {
        width = 48;
    }

    btnOK.insertAdjacentElement('AfterEnd', btnListar);
    with (btnListar.style) {
        width = 48;
        height = btnOK.offsetHeight;
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;
    fg2.Redraw = 2;

    glb_getServerData = 7;

    setConnection(dsoCmbMoeda);
    dsoCmbMoeda.SQL = 'SELECT c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, c.SimboloMoeda AS Ordem, b.Faturamento, c.SimboloMoeda AS Moeda ' +
        'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
        'WHERE a.SujeitoID = ' + aEmpresa[0] + ' ' +
        'AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 ' +
        'AND a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID';

    dsoCmbMoeda.ondatasetcomplete = dsoServerData_DSC;
    dsoCmbMoeda.Refresh();


    // Contexto Produtos - SFI Grupo-Gerente de Produto
    setConnection(dsoDireitos);
    dsoDireitos.SQL = 'SELECT TOP 1 Direitos.Alterar1, Direitos.Alterar2 ' +
	    'FROM RelacoesPesRec RelPesRec WITH(NOLOCK), RelacoesPesRec_Perfis RelPesRecPerfis WITH(NOLOCK), Recursos Perfis WITH(NOLOCK), Recursos_Direitos Direitos WITH(NOLOCK) ' +
	    'WHERE (RelPesRec.SujeitoID IN ((SELECT ' + glb_nUserID +
			' UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = ' + glb_nUserID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
			'RelPesRec.ObjetoID = 999 AND ' +
	    	'RelPesRec.TipoRelacaoID = 11 AND RelPesRec.RelacaoID = RelPesRecPerfis.RelacaoID AND  ' +
	    	'RelPesRecPerfis.EmpresaID = ' + aEmpresa[0] + ' AND RelPesRecPerfis.PerfilID = Perfis.RecursoID AND  ' +
	    	'Perfis.EstadoID = 2 AND Perfis.RecursoID = Direitos.PerfilID AND Direitos.RecursoMaeID = 2131 AND ' +
	    	'Direitos.RecursoID = 21072) ' +
	    'ORDER BY Direitos.Alterar2 DESC, Direitos.Alterar1 DESC';

    dsoDireitos.ondatasetcomplete = dsoServerData_DSC;
    dsoDireitos.Refresh();

    // Contexto Produtos - SFS Grupo-Relacao entr Pessoas/Conceitos
    setConnection(dsoDireitosClassFiscal);
    dsoDireitosClassFiscal.SQL = 'SELECT TOP 1 Direitos.Alterar1, Direitos.Alterar2 ' +
	    'FROM RelacoesPesRec RelPesRec WITH(NOLOCK), RelacoesPesRec_Perfis RelPesRecPerfis WITH(NOLOCK), Recursos Perfis WITH(NOLOCK), Recursos_Direitos Direitos WITH(NOLOCK) ' +
	    'WHERE (RelPesRec.SujeitoID IN ((SELECT ' + glb_nUserID +
			' UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WHERE (UsuarioParaID = ' + glb_nUserID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
			'RelPesRec.ObjetoID = 999 AND ' +
	    	'RelPesRec.TipoRelacaoID = 11 AND RelPesRec.RelacaoID = RelPesRecPerfis.RelacaoID AND  ' +
	    	'RelPesRecPerfis.EmpresaID = ' + aEmpresa[0] + ' AND RelPesRecPerfis.PerfilID = Perfis.RecursoID AND  ' +
	    	'Perfis.EstadoID = 2 AND Perfis.RecursoID = Direitos.PerfilID AND Direitos.RecursoMaeID = 2131 AND ' +
	    	'Direitos.RecursoID = 21060) ' +
	    'ORDER BY Direitos.Alterar2 DESC, Direitos.Alterar1 DESC';

    dsoDireitosClassFiscal.ondatasetcomplete = dsoServerData_DSC;
    dsoDireitosClassFiscal.Refresh();

    // Direitos para o Checkbox OK
    setConnection(dsoDireitosB3);
    dsoDireitosB3.SQL = 'SELECT TOP 1 Direitos.Alterar1, Direitos.Alterar2 ' +
	    'FROM RelacoesPesRec RelPesRec WITH(NOLOCK), RelacoesPesRec_Perfis RelPesRecPerfis WITH(NOLOCK), Recursos Perfis WITH(NOLOCK), Recursos Contextos WITH(NOLOCK), Recursos_Direitos Direitos WITH(NOLOCK) ' +
	    'WHERE (RelPesRec.SujeitoID IN ((SELECT ' + glb_nUserID +
			' UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WITH(NOLOCK) WHERE (UsuarioParaID = ' + glb_nUserID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
			'RelPesRec.ObjetoID = 999 AND ' +
	    	'RelPesRec.TipoRelacaoID = 11 AND RelPesRec.RelacaoID = RelPesRecPerfis.RelacaoID AND  ' +
	    	'RelPesRecPerfis.EmpresaID = ' + aEmpresa[0] + ' AND RelPesRecPerfis.PerfilID = Perfis.RecursoID AND  ' +
	    	'Perfis.EstadoID = 2 AND Perfis.RecursoID = Direitos.PerfilID AND Direitos.RecursoMaeID = 21060 AND ' +
	    	'Direitos.RecursoID = 40003 AND Direitos.ContextoID = Contextos.RecursoID AND Contextos.RecursoID = 2131) ' +
	    'ORDER BY Direitos.Alterar1 DESC, Direitos.Alterar2 DESC';

    dsoDireitosB3.ondatasetcomplete = dsoDireitosB3_DSC;
    dsoDireitosB3.Refresh();

    setConnection(dsoFornecedores);
    dsoFornecedores.SQL = 'SELECT 0 AS fldID, SPACE(1) AS fldName ' +
		'UNION ALL ' +
		'SELECT DISTINCT c.PessoaID AS fldID, c.Fantasia AS fldName ' +
		    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
		        'INNER JOIN RelacoesPesCon_Fornecedores b WITH(NOLOCK) ON (b.RelacaoID = a.RelacaoID) ' +
		            'INNER JOIN Pessoas c ON (c.PessoaID = b.FornecedorID) ' +
		    'WHERE (a.EstadoID NOT IN (4,5)) ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroProprietario + ' ' +
            'GROUP BY c.PessoaID, c.Fantasia ' +
		'ORDER BY fldName';

    dsoFornecedores.ondatasetcomplete = dsoServerData_DSC;
    dsoFornecedores.Refresh();

    setConnection(dsoProjetos);
    dsoProjetos.SQL =   //'SELECT 0 AS fldID, SPACE(1) AS fldName ' +
		                //'UNION ALL ' +
                        'SELECT a.LoteID AS fldID, a.Descricao AS fldName ' +
                            'FROM Lotes a WITH(NOLOCK) ' +
                            'WHERE ((a.CodigoCRM IS NOT NULL) AND (a.EstadoID = 41)) ' +
                            'ORDER BY fldName';

    dsoProjetos.ondatasetcomplete = dsoServerData_DSC;
    dsoProjetos.Refresh();

    setConnection(dsoCmbAtualizacao);
    dsoCmbAtualizacao.SQL = selectString(false, true);
    dsoCmbAtualizacao.ondatasetcomplete = dsoServerData_DSC;
    dsoCmbAtualizacao.Refresh();

    setConnection(dsoClassFiscal);
    dsoClassFiscal.SQL = 'SELECT 0 AS ConceitoID, SPACE(1) AS Conceito ' +
        'UNION ALL ' +
        'SELECT ConceitoID, Conceito ' +
        'FROM Conceitos WITH(NOLOCK) ' +
        'WHERE EstadoID IN (2,4) AND TipoConceitoID=309 ' +
        'ORDER BY Conceito';

    dsoClassFiscal.ondatasetcomplete = dsoServerData_DSC;
    dsoClassFiscal.Refresh();

    chkCampos_onclick();
    alignButtons();
}
// function setupPage() - Fim ***************************************

function ArgumentoDeVenda_onkeypress() {
    chkOK.checked = false;
    houveAlteracaoArgVenda();
}

function houveAlteracaoArgVenda() {
    btnOK.disabled = false;
    glb_HouveAlteracao = true;
}

function chkPesquisa_onclick() {
    if (chkPesquisa.checked) {
        adjustElementsInForm([
	            ['lblPesquisa', 'chkPesquisa', 3, 1, -7],
	            ['lblFamilia', 'selFamilia', 19, 1, -2],
	            ['lblMarca', 'selMarca', 19, 1, -2],
	            ['lblLinha', 'selLinha', 19, 1, -2]], null, null, true);

        alignButtons();

        lblServico.style.visibility = 'hidden';
        selServico.style.visibility = 'hidden';
        lblFiltroEspecifico.style.visibility = 'hidden';
        selFiltroEspecifico.style.visibility = 'hidden';
        lblColunas.style.visibility = 'hidden';
        chkColunas.style.visibility = 'hidden';
        lblCampos.style.visibility = 'hidden';
        chkCampos.style.visibility = 'hidden';
        lblFamilia.style.visibility = 'inherit';
        selFamilia.style.visibility = 'inherit';
        lblMarca.style.visibility = 'inherit';
        selMarca.style.visibility = 'inherit';
        lblLinha.style.visibility = 'inherit';
        selLinha.style.visibility = 'inherit';
        lblProjeto.style.visibility = 'hidden';
        selProjeto.style.visibility = 'hidden';

        lblFiltroArgumentosVenda.style.visibility = 'hidden';
        selFiltroArgumentosVenda.style.visibility = 'hidden';

        preencheFamilia();
        preencheMarca();
    }
    else {
        lblServico.style.visibility = 'inherit';
        selServico.style.visibility = 'inherit';
        lblFiltroEspecifico.style.visibility = 'inherit';
        selFiltroEspecifico.style.visibility = 'inherit';
        lblColunas.style.visibility = 'inherit';
        chkColunas.style.visibility = 'inherit';
        lblCampos.style.visibility = 'inherit';
        chkCampos.style.visibility = 'inherit';
        lblFamilia.style.visibility = 'hidden';
        selFamilia.style.visibility = 'hidden';
        lblMarca.style.visibility = 'hidden';
        selMarca.style.visibility = 'hidden';
        lblLinha.style.visibility = 'hidden';
        selLinha.style.visibility = 'hidden';

        if (selServico.value == INTERNET) {
            lblFiltroArgumentosVenda.style.visibility = 'inherit';
            selFiltroArgumentosVenda.style.visibility = 'inherit';
        }

        adjustElementsInForm([
	        ['lblPesquisa', 'chkPesquisa', 3, 1, -7],
	        ['lblServico', 'selServico', 19, 1, -2],
	        ['lblFiltroEspecifico', 'selFiltroEspecifico', 19, 1, -2],
	        ['lblColunas', 'chkColunas', 3, 1, -2],
    	    ['lblCampos', 'chkCampos', 3, 1, -7],
            ['lblPercentualPV', 'txtPercentualPV', 4, 2, -8],
            ['lblEnc', 'chkEnc', 3, 2, 2],
            ['lblFornecedor', 'selFornecedor', 20, 2, -2],
            ['lblTransacao', 'selTransacao', 9, 2],
            ['lblFinanciamento', 'selFinanciamento', 17, 2],
            ['lblConsolidar', 'chkConsolidar', 3, 2],
            ['lblLote', 'selLote', 10, 2],
            ['lblDescricao', 'txtDescricao', 10, 2],
            ['lblPtax', 'txtPtax', 6, 2]
        ], null, null, true);

        alignButtons();
    }
}

function chkEnc_onclick() {
    chkCampos.disabled = (chkEnc.checked ? true : false);
    chkColunas.disabled = (chkEnc.checked ? true : false);

    //SAS0081 - Pedidos Open Micrososft
    if ((chkEnc.checked) && (selFornecedor.value == 10003)) {
        btnBatch.style.visibility = 'inherit';
    }
    else {
        btnBatch.style.visibility = 'hidden';
    }

    if (chkEnc.checked) {
        if (chkPesquisa.checked) {
            lblProjeto.style.visibility = 'hidden';
            selProjeto.style.visibility = 'hidden';
        }
        else {
            lblProjeto.style.visibility = 'inherit';
            selProjeto.style.visibility = 'inherit';
        }

        lblConsolidar.style.visibility = 'inherit';
        chkConsolidar.style.visibility = 'inherit';
        lblLote.style.visibility = 'inherit';
        selLote.style.visibility = 'inherit';
        selLote_onchange();
        btnAplicar.style.visibility = 'inherit';
        lblPtax.style.visibility = 'inherit';
        txtPtax.style.visibility = 'inherit';
        preencheLote();
        cotacaoPTAX();
    }

    else {
        lblProjeto.style.visibility = 'hidden';
        selProjeto.style.visibility = 'hidden';
        lblConsolidar.style.visibility = 'hidden';
        chkConsolidar.style.visibility = 'hidden';
        lblLote.style.visibility = 'hidden';
        selLote.style.visibility = 'hidden';
        btnAplicar.style.visibility = 'hidden';
        lblDescricao.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';
        lblPtax.style.visibility = 'hidden';
        txtPtax.style.visibility = 'hidden';

        LimpaLoteGrid();
    }

    alignButtons();
}


function FGExtended() {
    return (glb_Proporcional * glb_divFGExtended);
}

function FGCollapse() {
    return (glb_Proporcional * glb_divFGCollapse);
}

// function alignButtons() - Inicio *********************************
function alignButtons() {
    var nTopStart = 0;
    var nNextLeft = 0;
    var nNextLeft2 = 0;
    var nServico = selServico.value;
    var nFiltroEspecifico = selFiltroEspecifico.value;

    // Define a visibilidade do argumento de vendas.
    var argVendasIsInherit = (!chkPesquisa.checked) && (nServico == INTERNET) && (selFiltroEspecifico.value == ARGUMENTO_VENDAS);
    lblIdiomaArgumentosVenda.style.visibility = argVendasIsInherit ? 'inherit' : 'hidden';
    selIdiomaArgumentosVenda.style.visibility = argVendasIsInherit ? 'inherit' : 'hidden';

    // Ajusta a vari�vel proporcional.
    if (argVendasIsInherit && (selIdiomaArgumentosVenda.value != "0")) glb_Proporcional = 0.80;
    else glb_Proporcional = 1;

    // Ajusta os componentes.
    if (chkPesquisa.checked) {
        nTopStart = selLinha.offsetTop;
        nNextLeft = (lblLinha.offsetLeft + lblLinha.offsetWidth + 119);

        btnListar.style.visibility = 'inherit';
        btnListar.style.top = nTopStart;
        btnListar.style.left = nNextLeft;
        nNextLeft = btnListar.offsetLeft + btnListar.offsetWidth + 5;

        lblConsolidar.style.left = 475;
        chkConsolidar.style.left = 475;
        nNextLeft2 = chkConsolidar.offsetLeft + chkConsolidar.offsetWidth + 5;

        lblLote.style.left = nNextLeft2 + 5;
        selLote.style.left = nNextLeft2 + 5;
        nNextLeft2 = selLote.offsetLeft + selLote.offsetWidth + 5;

        lblDescricao.style.left = nNextLeft2 + ELEM_GAP;
        txtDescricao.style.left = nNextLeft2 + ELEM_GAP;
        nNextLeft2 = txtDescricao.offsetLeft + txtDescricao.offsetWidth + 5;

        lblPtax.style.left = nNextLeft2 + 5;
        txtPtax.style.left = nNextLeft2 + 5;
        nNextLeft2 = txtPtax.offsetLeft + txtPtax.offsetWidth + 5;

        btnAplicar.style.left = nNextLeft2 + 5;

        btnOK.style.visibility = 'hidden';
        btnImprimir.style.visibility = 'hidden';
        btnExcel.style.visibility = 'hidden';
        btnCalcularPV.style.visibility = 'hidden';
        btnComprar.style.visibility = 'hidden';
        btnBatch.style.visibility = 'hidden';

        // .
        divFG.style.height = FGExtended();
        fg.style.height = FGExtended() - 2;
        divControlsGerProd.style.visibility = 'hidden';
        divArgumentoVendas.style.visibility = 'inherit';
    }
    else {

        lblConsolidar.style.left = 668;
        chkConsolidar.style.left = 665;
        nNextLeft2 = chkConsolidar.offsetLeft + chkConsolidar.offsetWidth + 5;

        lblLote.style.left = nNextLeft2 + 1;
        selLote.style.left = nNextLeft2 + 1;
        nNextLeft2 = selLote.offsetLeft + selLote.offsetWidth + 1;

        lblDescricao.style.left = nNextLeft2 + ELEM_GAP;
        txtDescricao.style.left = nNextLeft2 + ELEM_GAP;
        nNextLeft2 = txtDescricao.offsetLeft + txtDescricao.offsetWidth + 3;

        lblPtax.style.left = nNextLeft2 + 3;
        txtPtax.style.left = nNextLeft2 + 3;
        nNextLeft2 = txtPtax.offsetLeft + txtPtax.offsetWidth + 3;

        btnAplicar.style.left = nNextLeft2 + 3;

        // Se o combo de argumento de vendas estiver vis�vel.
        if (argVendasIsInherit) {
            nTopStart = chkColunas.offsetTop;
            nNextLeft = (lblColunas.offsetLeft + lblColunas.offsetWidth + 2);

            // Esconde o check de campos.
            lblCampos.style.visibility = 'hidden';
            chkCampos.style.visibility = 'hidden';
            // Esconde o div dos campos abaixo do grid.
            divControlsGerProd.style.visibility = 'hidden';

            lblIdiomaArgumentosVenda.style.visibility = 'inherit';
            lblIdiomaArgumentosVenda.style.top = nTopStart - 12;
            lblIdiomaArgumentosVenda.style.left = nNextLeft;

            selIdiomaArgumentosVenda.style.visibility = 'inherit';
            selIdiomaArgumentosVenda.style.top = nTopStart + 2;
            selIdiomaArgumentosVenda.style.left = nNextLeft;

            /*
			 * Posiciona o combo dos filtro dos argumentos de vendas.
			 */
            nTopStart = selIdiomaArgumentosVenda.offsetTop - 2;
            nNextLeft = (lblIdiomaArgumentosVenda.offsetLeft + lblIdiomaArgumentosVenda.offsetWidth + 2) - 71;

            lblFiltroArgumentosVenda.style.visibility = 'inherit';
            lblFiltroArgumentosVenda.style.top = nTopStart - 12;
            lblFiltroArgumentosVenda.style.left = nNextLeft;

            selFiltroArgumentosVenda.style.visibility = 'inherit';
            selFiltroArgumentosVenda.style.top = nTopStart + 2;
            selFiltroArgumentosVenda.style.left = nNextLeft;

            nNextLeft = selFiltroArgumentosVenda.offsetLeft + selFiltroArgumentosVenda.offsetWidth + 6;

            // Define se mostra a caixa de texto do argumento de venda.
            divArgumentoVendas.style.visibility = (selIdiomaArgumentosVenda.value != "0") ? 'inherit' : "hidden";
        }
        else {
            nTopStart = chkCampos.offsetTop;
            nNextLeft = (lblCampos.offsetLeft + lblCampos.offsetWidth + 4);

            // Mostra o check de campos.
            lblCampos.style.visibility = 'inherit';
            chkCampos.style.visibility = 'inherit';

            // Esconde o div dos campos abaixo do grid.
            divControlsGerProd.style.visibility = chkCampos.checked ? 'inherit' : 'hidden';

            lblFiltroArgumentosVenda.style.visibility = 'hidden';
            selFiltroArgumentosVenda.style.visibility = 'hidden';

            divArgumentoVendas.style.visibility = 'hidden';
        }

        if (chkEnc.checked)
            nNextLeft += 257;

        // Botao Listar
        btnListar.style.visibility = 'inherit';
        btnListar.style.top = nTopStart;
        btnListar.style.left = nNextLeft;
        nNextLeft = btnListar.offsetLeft + btnListar.offsetWidth + 3;

        // Botao OK
        btnOK.style.visibility = 'inherit';
        btnOK.style.top = nTopStart;
        btnOK.style.left = nNextLeft;
        nNextLeft = btnOK.offsetLeft + btnOK.offsetWidth + 3;

        // Botao Imprimir
        btnImprimir.style.visibility = 'inherit';
        btnImprimir.style.top = nTopStart;
        btnImprimir.style.left = nNextLeft;
        nNextLeft = btnImprimir.offsetLeft + btnImprimir.offsetWidth + 3;
        btnImprimir.disabled = ((fg.rows > 1) ? false : true);

        // Botao Excel
        btnExcel.style.visibility = 'inherit';
        btnExcel.style.top = nTopStart;
        btnExcel.style.left = nNextLeft;
        nNextLeft = btnExcel.offsetLeft + btnExcel.offsetWidth + 3;
        btnExcel.disabled = ((fg.rows > 1) ? false : true);

        // Botao Calcular PV
        if ((nServico == PREVEVISAO_DE_VENDAS) || (nServico == GERAL)) {
            btnCalcularPV.style.visibility = 'inherit';
            btnCalcularPV.style.top = nTopStart;
            btnCalcularPV.style.left = nNextLeft;
            nNextLeft = btnCalcularPV.offsetLeft + btnCalcularPV.offsetWidth + 3;
        }
        else {
            btnCalcularPV.style.visibility = 'hidden';
        }

        // Botao Gerenc. de estoque
        if ((nServico == GERENCIA_DE_ESTOQUE) && (nFiltroEspecifico == COMPRAS_PENDENTES)) {
            btnComprar.style.visibility = 'inherit';
            btnComprar.style.top = nTopStart;
            btnComprar.style.left = nNextLeft;
            nNextLeft = btnComprar.offsetLeft + btnComprar.offsetWidth + 3;

            btnBatch.style.top = nTopStart;
            btnBatch.style.left = nNextLeft;
            nNextLeft = btnBatch.offsetLeft + btnBatch.offsetWidth + 3;

            divFG.style.top = glb_nTopGrid + 47;

            if (glb_bDivFGIsCollapse) {
                divFG.style.height = FGCollapse() - 47;
            }
            else {
                divFG.style.height = FGExtended() - 47;
            }

            lblPercentualPV.style.visibility = 'inherit';
            txtPercentualPV.style.visibility = 'inherit';
            lblFornecedor.style.visibility = 'inherit';
            selFornecedor.style.visibility = 'inherit';
            lblTransacao.style.visibility = 'inherit';
            selTransacao.style.visibility = 'inherit';
            lblFinanciamento.style.visibility = 'inherit';
            selFinanciamento.style.visibility = 'inherit';
            //Novos campos para o Lote. Inicio. BJBN

            btnAplicar.style.top = nTopStart + 48;
            btnAplicar.style.left = nNextLeft - 52;
            btnAplicar.style.width = 60;

            if (chkEnc.checked) {
                lblProjeto.style.visibility = 'inherit';
                selProjeto.style.visibility = 'inherit';
                lblConsolidar.style.visibility = 'inherit';
                chkConsolidar.style.visibility = 'inherit';
                lblLote.style.visibility = 'inherit';
                selLote.style.visibility = 'inherit';
                selLote_onchange();

                btnAplicar.style.visibility = 'inherit';

                preencheLote();
            }
            //Novos campos para o Lote. Fim. BJBN

        }
        else {
            btnComprar.style.visibility = 'hidden';
            btnBatch.style.visibility = 'hidden';

            divFG.style.top = glb_nTopGrid;

            if (divControlsGerProd.style.visibility == 'inherit') glb_Proporcional = 1;
            else if ((divArgumentoVendas.style.visibility == 'inherit') && (selIdiomaArgumentosVenda.value != "0")) glb_Proporcional = 0.80;

            if (glb_bDivFGIsCollapse || (nServico == INTERNET)) {
                if ((divArgumentoVendas.style.visibility == 'inherit') || (divControlsGerProd.style.visibility == 'inherit'))
                    divFG.style.height = FGCollapse();
                else
                    divFG.style.height = FGExtended();
            }
            else {
                divFG.style.height = FGExtended();
            }

            lblPercentualPV.style.visibility = 'hidden';
            txtPercentualPV.style.visibility = 'hidden';
            lblFornecedor.style.visibility = 'hidden';
            selFornecedor.style.visibility = 'hidden';
            lblTransacao.style.visibility = 'hidden';
            selTransacao.style.visibility = 'hidden';
            lblFinanciamento.style.visibility = 'hidden';
            selFinanciamento.style.visibility = 'hidden';
            //Novos campos para o Lote. Inicio. BJBN
            lblProjeto.style.visibility = 'hidden';
            selProjeto.style.visibility = 'hidden';
            lblConsolidar.style.visibility = 'hidden';
            chkConsolidar.style.visibility = 'hidden';
            lblLote.style.visibility = 'hidden';
            selLote.style.visibility = 'hidden';
            lblDescricao.style.visibility = 'hidden';
            txtDescricao.style.visibility = 'hidden';
            btnAplicar.style.visibility = 'hidden';
            //Novos campos para o Lote. Fim. BJBN
            lblPtax.style.visibility = 'hidden';
            txtPtax.style.visibility = 'hidden';
        }

        fg.style.height = divFG.offsetHeight - 2;
    }

}
// function alignButtons() - Fim ************************************

function txtPercentualPV_onkeyup() {
    if (event.keyCode == 13)
        fillGridData();
}

function chkCampos_onclick() {
    collapseExtendedDivFG();
    alignButtons();
}

/********************************************************************
Mostra/esconde div inferior
********************************************************************/
function collapseExtendedDivFG() {
    var nRowLine = fg.Row;

    fg.Redraw = 0;

    if (glb_bDivFGIsCollapse && chkCampos.style.visibility == 'hidden') {
        divControlsGerProd.style.visibility = 'hidden';

        fg.style.height = (FGExtended() - 2);
        divFG.style.height = FGExtended();
        divFG2.style.visibility = 'hidden';
    }
    else {
        fg.style.height = (FGCollapse() - 2);
        divFG.style.height = FGCollapse();

        divControlsGerProd.style.top = 405;
        divControlsGerProd.style.left = 13;
        divControlsGerProd.style.visibility = 'inherit';
    }

    if (fg.Rows > 2)
        fg.TopRow = nRowLine;

    fg.Redraw = 2;

    glb_bDivFGIsCollapse = !glb_bDivFGIsCollapse;
}

/********************************************************************
Retorno do servidor
********************************************************************/
function dsoServerData_DSC() {
    glb_getServerData--;

    if (glb_getServerData != 0)
        return null;

    if (!((dsoCmbMoeda.recordset.BOF) && (dsoCmbMoeda.recordset.EOF))) {
        while (!dsoCmbMoeda.recordset.EOF) {
            if (dsoCmbMoeda.recordset['Faturamento'].value) {
                glb_nMoedaFaturamentoID = dsoCmbMoeda.recordset['fldID'].value;
                glb_sMoeda = dsoCmbMoeda.recordset['Moeda'].value;
                break;
            }
            dsoCmbMoeda.recordset.MoveNext();
        }
        dsoCmbMoeda.recordset.MoveFirst();
    }

    var a1 = 0;
    var a2 = 0;
    var ClassFiscalA1 = 0;
    var ClassFiscalA2 = 0;

    fillCmbs();

    if (!(dsoDireitos.recordset.BOF && dsoDireitos.recordset.EOF)) {
        a1 = (dsoDireitos.recordset['Alterar1'].value == true ? 1 : 0);
        a2 = (dsoDireitos.recordset['Alterar2'].value == true ? 1 : 0);
    }

    if (!(dsoDireitosClassFiscal.recordset.BOF && dsoDireitosClassFiscal.recordset.EOF)) {
        ClassFiscalA1 = (dsoDireitosClassFiscal.recordset['Alterar1'].value == true ? 1 : 0);
        ClassFiscalA1 = (dsoDireitosClassFiscal.recordset['Alterar2'].value == true ? 1 : 0);
    }

    // Gerenciamento de produtos    
    if (a2 == 0 && a1 == 0)
        glb_sReadOnly = '*';
    else if (a2 == 0 && a1 == 1)
        glb_sFiltroDireitos = ' AND a.ProprietarioID= ' + glb_nUserID + ' ';
    else if (a2 == 1 && a1 == 0)
        glb_sFiltroDireitos = ' AND ' + glb_nUserID + ' =(CASE WHEN a.ProprietarioID=' + glb_nUserID + ' THEN a.ProprietarioID ' +
                                                'WHEN a.AlternativoID=' + glb_nUserID + ' THEN a.AlternativoID ' +
                                                'WHEN a.AlternativoID=(SELECT TOP 1 grupo.ObjetoID FROM RelacoesPessoas grupo WITH(NOLOCK) ' +
                                                                      'WHERE grupo.TipoRelacaoID=34 AND grupo.EstadoID=2 ' +
                                                                      'AND grupo.SujeitoID=' + glb_nUserID + ' ' +
                                                                      'AND grupo.ObjetoID=a.AlternativoID) THEN ' + glb_nUserID + ' ' +
                                                'ELSE 0 ' +
                                           'END) ';

    if ((ClassFiscalA1 == 0) && (ClassFiscalA2 == 0))
        glb_sReadOnlyClassFiscal = '*';

    preencheFiltroEspecifico();

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

function fillCmbs() {
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selFornecedor, selProjeto];
    var aDSOsDunamics = [dsoFornecedores, dsoProjetos];

    clearComboEx(['selFornecedor']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        aDSOsDunamics[i].recordset.moveFirst();
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].disabled = (aCmbsDynamics[i].options.length == 0);
    }

    return null;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
    }
    else if (controlID == 'btnComprar') {
        if (VerificaLoteGrid())
            btn_Comprar_Clicked();
        else
            if (window.top.overflyGen.Alert('Existem linhas a serem compradas sem Lote definido.') == 0)
                return null;
    }
    else if (controlID == 'btnBatch') {
        btn_Batch_Clicked();
    }
    else if (controlID == 'btnAplicar') {
        btn_Aplicar_Clicked();
    }
    else if (controlID == 'btnImprimir') {
        btn_Imprimir_Clicked();
    }
    else if (controlID == 'btnExcel') {
        btn_Excel_Clicked();
    }
    else if (controlID == 'btnListar') {
        fillGridData();
    }
        // codigo privado desta janela
    else if (controlID == 'btnCanc') {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    if (glb_refreshGrid != null) {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    // mostra a janela modal
    // fillGridData();
    showExtFrame(window, true);
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var i;
    var bHasRowsInGrid = fg.Rows > 2;

    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
    btnComprar.disabled = true;


    if ((selFornecedor.options.length > 0 && selFornecedor.value != 0) && (selTransacao.options.length > 0 && selTransacao.value != 0) &&
		(selFinanciamento.options.length > 0 && selFinanciamento.value != 0)) {
        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0) {
                btnComprar.disabled = false;
                //btnBatch.disabled = true;
            }
        }
    }
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData()
{
    if (glb_refreshGrid2 != null)
    {
        window.clearInterval(glb_refreshGrid2);
        glb_refreshGrid2 = null;
    }

    lockControlsInModalWin(true);

    // zera o grid
    fg.Rows = 1;

    // zera as informa��es dos campos
    txtCustoMedio.value = "";
    txtCustoReposicao.value = "";
    txtUltimaEntrada.value = "";
    txtUltimaSaida.value = "";
    txtDias.value = "";
    txtDisponibilidade.value = "";
    txtEstoqueInternet.value = "";

    glb_dataGridWasChanged = false;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    strSQL = selectString(false);

    if (strSQL != null) {
        try {
            dsoGrid.SQL = strSQL;
            dsoGrid.ondatasetcomplete = fillGridData_DSC;
            dsoGrid.Refresh();
        }
        catch (e) {
            alert('erro');
        }
    }
}

/********************************************************************
Obtem string de select semelhante ao do pesqlist

Parametros:
nenhum

Retorno:
string de select
********************************************************************/
function selectString(bToExcel, bComboDataSemelhante) {
    var i = 0;
    var strSQL = '';
    var sFiltroFornecedor = '';
    var sFiltroComprar = '';
    var sCustoComprar = '';
    var sPercentualPV = 'NULL';
    var sFiltroData = '';
    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');
    var nWhereTransacao = '';
    var selFiltroArgumentosVendaWhere = '';
    var selFiltroEspecificoFiltro = "";
    var sAGrid = "";

    bComboDataSemelhante = (bComboDataSemelhante == null ? false : bComboDataSemelhante);

    if ((trimStr(txtPercentualPV.value) != '') && (!isNaN(txtPercentualPV.value)))
        sPercentualPV = txtPercentualPV.value;

    if (selFornecedor.value != 0)
        sFiltroFornecedor = ' AND (SELECT COUNT(*) FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID AND FornecedorID = ' + selFornecedor.value + ')) > 0';

    // Servico 5 Comprar
    if (selServico.value == GERENCIA_DE_ESTOQUE)
        sFiltroComprar = ' AND a.PrevisaoVendas >= 0 ';

    if ((selFiltroEspecifico.value != 0) && (!chkEnc.checked)) {
        glb_sFiltroEspecifico = ' AND (' + selFiltroEspecifico.options[selFiltroEspecifico.selectedIndex].getAttribute('Filtro', 1) + ')';
        selFiltroEspecificoFiltro = selFiltroEspecifico.options[selFiltroEspecifico.selectedIndex].getAttribute('Filtro', 1);
    }
    else
        glb_sFiltroEspecifico = '';

    if (selFamilia.value != 0)
        glb_sFiltroFamilia = ' AND ((SELECT aa.ProdutoID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = ' + selFamilia.value + ')';
    else
        glb_sFiltroFamilia = '';

    if (selMarca.value != 0)
        glb_sFiltroMarca = ' AND ((SELECT aa.MarcaID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = ' + selMarca.value + ')';
    else
        glb_sFiltroMarca = '';

    if (selLinha.value != 0)
        glb_sFiltroLinha = ' AND ((SELECT aa.LinhaProdutoID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = ' + selLinha.value + ')';
    else
        glb_sFiltroLinha = '';

    if (selFornecedor.value != 0) {
        //SAS0081 - Pedidos Open Micrososft
        if ((selFornecedor.value == 10003) && (chkEnc.checked)) {
            sCustoComprar = 'CONVERT(NUMERIC(11,2),(SELECT TOP 1 dbo.fn_Preco_Cotacao(704, ' + glb_nMoedaFaturamentoID + ', ' +
			'NULL, GETDATE()) * aa.CustoFOB ' +
			'FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) ' +
			'WHERE aa.RelacaoID=a.RelacaoID AND aa.FornecedorID=' + selFornecedor.value +
			'ORDER BY aa.Ordem)) AS CustoComprar, ';
        }
        else {
            sCustoComprar = 'CONVERT(NUMERIC(11,2),(SELECT TOP 1 dbo.fn_Preco_Cotacao(aa.MoedaID, ' + glb_nMoedaFaturamentoID + ', ' +
			'NULL, GETDATE()) * aa.CustoReposicao ' +
			'FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) ' +
			'WHERE aa.RelacaoID=a.RelacaoID AND aa.FornecedorID=' + selFornecedor.value +
			'ORDER BY aa.Ordem)) AS CustoComprar, ';
        }
    }
    else {
        sCustoComprar = '0 AS CustoComprar, ';
    }

    //Classifica��o fiscal ReadOnly depende do direito.
    var sNCM = 0;
    if ((glb_sReadOnlyClassFiscal == '') && (!glb_Excel))
        sNCM = 'a.ClassificacaoFiscalID AS NCM, ';
    else if ((glb_sReadOnlyClassFiscal != '') && (!glb_Excel)) {
        sNCM = 'a.ClassificacaoFiscalID, ' +
            '(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ClassificacaoFiscalID)) AS NCM, ';
    }
    else if (glb_Excel) {
        sNCM = '(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ClassificacaoFiscalID)) AS NCM, ';

    }


    if (!chkEnc.checked) {
        if (glb_Inicio == true) {
            strSQL = 'SET NOCOUNT ON ' +
		         'DECLARE @Table2 TABLE (RelacaoID INT) ' +
                 'INSERT INTO @Table2 ' +
 	                'SELECT TOP 1  a.RelacaoID ' +
 	                 'FROM RelacoesPesCon a WITH(NOLOCK) ';

        }
        else {
            strSQL = 'SET NOCOUNT ON ' +
		         'DECLARE @Table2 TABLE (RelacaoID INT) ' +
                 'INSERT INTO @Table2 ' +
	        'SELECT TOP ' + glb_nRegistros + ' a.RelacaoID ' +
 	                 'FROM RelacoesPesCon a WITH(NOLOCK) ';
        }

        strSQL += 'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(aa.RelacaoID, NULL, 1, GETDATE()), 1)) AS ProdutoInconsistencia, aa.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon aa WITH(NOLOCK)) AS ProdutosInconsistentes ON (ProdutosInconsistentes._RelacaoID = a.RelacaoID) ' +
				        'INNER JOIN (SELECT dbo.fn_Produto_Estoque(bb.SujeitoID, bb.ObjetoID, 341, NULL, GETDATE(), NULL, NULL, NULL) AS Estoque, bb.RelacaoID AS _RelacaoID ' +
				                'FROM RelacoesPesCon bb WITH(NOLOCK)) AS Estoque ON (Estoque._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT dbo.fn_Produto_Comprar(cc.SujeitoID, cc.ObjetoID, NULL, GETDATE(), 10) AS Compras, cc.RelacaoID AS _RelacaoID ' +
				               'FROM  RelacoesPesCon cc WITH(NOLOCK)) AS ComprasPendentes ON (ComprasPendentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT dbo.fn_Produto_Comprar(dd.SujeitoID, dd.ObjetoID, NULL, GETDATE(), 20) AS Excesso, dd.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon dd WITH(NOLOCK)) AS EstoqueExcesso ON (EstoqueExcesso._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ee.RelacaoID, 10, 1, GETDATE()), 1)) AS EstadoInconsistencia, ee.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon ee WITH(NOLOCK)) AS EstadosInconsistentes ON (EstadosInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ff.RelacaoID, 20, 1, GETDATE()), 1)) AS CVPInconsistencia, ff.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon ff WITH(NOLOCK)) AS CVPInconsistentes ON (CVPInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(gg.RelacaoID, 30, 1, GETDATE()), 1)) AS PVInconsistencia, gg.RelacaoID AS _RelacaoID  ' +
				                'FROM  RelacoesPesCon gg WITH(NOLOCK)) AS PVInconsistentes ON (PVInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(hh.RelacaoID, 40, 1, GETDATE()), 1)) AS MCInconsistencia, hh.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon hh WITH(NOLOCK)) AS MCInconsistentes ON (MCInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ii.RelacaoID, 50, 1, GETDATE()), 1)) AS InternetInconsistencia, ii.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon ii WITH(NOLOCK)) AS InternetInconsistentes ON (InternetInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(jj.RelacaoID, 60, 1, GETDATE()), 1)) AS PagamentoInconsistencia, jj.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon jj WITH(NOLOCK)) AS PagamentosInconsistentes ON (PagamentosInconsistentes._RelacaoID = a.RelacaoID) ' +
                        'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(kk.RelacaoID, 70, 1, GETDATE()), 1)) AS CustosInconsistencia, kk.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon kk WITH(NOLOCK)) AS CustosInconsistentes ON (CustosInconsistentes._RelacaoID = a.RelacaoID) ' +
 	                 'WHERE (';

        if (selFiltroArgumentosVenda.getAttribute("clausulaWhere") != "") {
            strSQL += selFiltroArgumentosVenda.getAttribute("clausulaWhere") + ") AND (";
        }

        if (selServico.value != GERAL) {
            if (aGrid != null) {
                strSQL += 'a.RelacaoID IN (';
                sAGrid += 'a.RelacaoID IN (';


                for (i = 0; i < aGrid.length; i++) {
                    strSQL += aGrid[i];
                    sAGrid += aGrid[i];

                    if (i != (aGrid.length - 1)) {
                        strSQL += ', ';
                        sAGrid += ', ';

                    }
                    else {
                        strSQL += ') ';
                        sAGrid += ') ';
                    }
                }

            }
            else {
                strSQL += '(1=2)';
                sAGrid += '(1=2)';
            }

            if (glb_sFiltroEspecifico != "") {
                strSQL += glb_sFiltroEspecifico + ') ';
                sAGrid += glb_sFiltroEspecifico + ') ';
            }
            else {
                strSQL += ') ';
                sAGrid += ') ';
            }
        }
        else {
            strSQL += 'a.RelacaoID IN (';
            sAGrid += 'a.RelacaoID IN (';

            for (i = 0; i < aGrid.length; i++) {
                strSQL += aGrid[i];
                sAGrid += aGrid[i];

                if (i != (aGrid.length - 1)) {
                    strSQL += ', ';
                    sAGrid += ', ';
                }
                else {
                    strSQL += ') ';
                    sAGrid += ') ';
                }
            }
            strSQL += sFiltroFornecedor + ') ';
            sAGrid += sFiltroFornecedor + ') ';
        }

        strSQL += 'SELECT ' +

        // Dados comuns inicio
        'a.RelacaoID AS RelacaoID, a.ObjetoID, a.EstadoID AS EstadoID, a.SujeitoID AS SujeitoID, ' +
        '(SELECT RecursoAbreviado FROM Recursos WITH(NOLOCK) WHERE RecursoID=a.EstadoID) AS Estado, ' +
        '(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ObjetoID)) AS Produto, ' +
        '(SELECT p.Conceito FROM Conceitos p WITH(NOLOCK) INNER JOIN Conceitos m WITH(NOLOCK) ON p.ConceitoID = m.MarcaID AND a.ObjetoID = m.ConceitoID) AS Marca, ' +
        '(SELECT Iniciais FROM RelacoesPesRec WITH(NOLOCK) ' +
            'WHERE ((TipoRelacaoID = 11) AND (ObjetoID = 999) AND (SujeitoID = a.ProprietarioID))) AS Proprietario, ' +
        '(SELECT Iniciais FROM RelacoesPesRec WITH(NOLOCK) ' +
            'WHERE ((TipoRelacaoID = 11) AND (ObjetoID = 999) AND (SujeitoID = a.AlternativoID))) AS Alternativo, ' +
        'a.Observacao, a.UsuarioID AS _UsuarioID, ' +

        // Ciclo de vida
        'a.dtInicio, a.dtFim, (DATEDIFF(dd, GETDATE(), a.dtFim)) AS DiasFim, ' +
        'a.dtControle, (DATEDIFF(dd, GETDATE(), a.dtControle)) AS DiasControle, ' +
        'a.dtMediaPagamento, (DATEDIFF(dd, GETDATE(), a.dtMediaPagamento)) AS DiasPagamento, ' +
        '(CONVERT(VARCHAR(10), dbo.fn_Produto_Movimento_Kardex(a.SujeitoID, a.ObjetoID, 1, 341), ' + DATE_SQL_PARAM + ')) AS dtUltimaEntrada, ' +
        '(CONVERT(VARCHAR(10), dbo.fn_Produto_Movimento_Kardex(a.SujeitoID, a.ObjetoID, 2, 341), ' + DATE_SQL_PARAM + ')) AS dtUltimaSaida, ' +
        '(DATEDIFF(dd, dbo.fn_Produto_Movimento_Kardex(a.SujeitoID, a.ObjetoID, 2, 341), GETDATE())) AS DiasUltimaSaida, ' +
        '(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -3, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -3, 0, 0), 1)) AS VendasMes3, ' +
        '(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -2, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -2, 0, 0), 1)) AS VendasMes2, ' +
        '(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, -1, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3, -1, 0, 0), 1)) AS VendasMes1, ' +
        '(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3,  0, 1, 0), dbo.fn_Data_Dia(GETDATE(), 3,  0, 0, 0), 1)) AS VendasMes0, ' +
        '(dbo.fn_Produto_Totaliza(a.SujeitoID, a.ObjetoID, dbo.fn_Data_Dia(GETDATE(), 3, 0, 1, 0), ' +
            'dbo.fn_Data_Dia(GETDATE(), 3, 0, 0, 0), 2)) AS MediaVendas13, ' +

        // Previsao de vendas				        
        'a.PrevisaoVendas, ' +
        '0 AS Contribuicao, ' +
        '0 AS PlanejamentoVendasTotal, ' +
        '0 AS Variacao, ' +
        'dbo.fn_Produto_Custo(a.RelacaoID, GETDATE(), ' +
            '(SELECT TOP 1 MoedaID  ' +
                'FROM Recursos WITH(NOLOCK) ' +
                'WHERE RecursoID = 999),NULL ,1) AS CustoMedio, ' +
        '((dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 341, NULL, NULL, NULL, 375, NULL)) * (dbo.fn_Produto_Custo(a.RelacaoID, GETDATE(),(SELECT TOP 1 MoedaID  FROM Recursos WITH(NOLOCK) WHERE RecursoID = 999),null, 1))) AS Custo_Total, ' +
        'a.MargemPadrao, a.MargemContribuicao, ' +
        '0 AS MargemReal, ' +
        'a.MargemMinima, 0 AS MargemMedia, ' +
        '0 AS PrecoBase, ' +
        '0 AS PrecoBaseMinimo, 0 AS PrecoBaseMedio, ' +
        'a.PrecoMinimo AS PrecoMinimo, ' +
        'dbo.fn_Preco_PrecoLista(a.SujeitoID,a.ObjetoID, NULL, 13, 647, GETDATE(), 0, 0, 1, 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, ' +
            ' NULL, NULL, NULL, NULL, a.MargemContribuicao, NULL, NULL, 2, 0, NULL,NULL, ' +
            'dbo.fn_EmpresaPessoaTransacao_CFOP(a.SujeitoID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, a.ObjetoID, NULL)) AS Preco, ' +
        '0 AS PrecoConcorrentes, ' +

        // Informacoes basicas				    
        'a.MultiploVenda, a.MultiploCompra, a.PrazoEstoque, ' + sNCM +
        // ???  
        '(SELECT aa.SimboloMoeda FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.MoedaEntradaID)) AS MoedaEntrada, ' +
        '(SELECT TOP 1 bb.SimboloMoeda ' +
            'FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK), Conceitos bb WITH(NOLOCK) ' +
            'WHERE ((aa.RelacaoID = a.RelacaoID) AND (aa.MoedaID = bb.ConceitoID)) ' +
            'ORDER BY aa.Ordem) AS MoedaReposicao, ' +

        // Compras
        '0 AS QuantidadeComprar, ' + sCustoComprar +
        '(CONVERT(NUMERIC(5), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, ' + sPercentualPV + ', GETDATE(), 30))) AS ComprarExcesso, ' +
        '(SELECT TOP 1 aa.CustoReposicao FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) WHERE (aa.RelacaoID = a.RelacaoID) ORDER BY aa.Ordem) AS CustoReposicao1, ' +
        'SPACE(0) AS Projeto, SPACE(0) AS PartNumber, ' +
        '(CONVERT(NUMERIC(3), dbo.fn_Produto_Comprar(a.SujeitoID, a.ObjetoID, NULL, GETDATE(), 17))) AS PrazoPagamento1, ' +
        '(SELECT TOP 1 aa.PrazoReposicao FROM RelacoesPesCon_Fornecedores aa WITH(NOLOCK) WHERE (aa.RelacaoID = a.RelacaoID) ORDER BY aa.Ordem) AS PrazoReposicao1, ' +
        '(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 341, NULL, NULL, NULL, 375, NULL)) AS Estoque, ' +
        '(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 351, NULL, NULL, NULL, 375, NULL)) AS ReservaCompra, ' +
        '(dbo.fn_Produto_Estoque(a.SujeitoID, a.ObjetoID, 354, NULL, NULL, NULL, 375, NULL)) AS ReservaVendaConfirmada, ' +
        '(dbo.fn_Produto_PrevisaoEntregaContratual(a.ObjetoID, a.SujeitoID, 1, NULL, 0)) AS PrevisaoEntrega, ' +
        'NULL AS LoteID, ' +
        'NULL AS Lote, ' +
        //'0 AS Aplicar, ' +

        // Internet
        'a.MargemPublicacao, ' +
        '0 AS PrecoInternet, ' +

        'a.Publica, a.PublicaPreco, a.PercentualPublicacaoEstoque, ' +
        '(dbo.fn_Produto_EstoqueWeb(a.SujeitoID, a.ObjetoID, 356, NULL, GETDATE(),NULL)) AS EstoqueInternet, ' +
        'a.PrazoTroca, a.PrazoGarantia, a.PrazoAsstec, a.MargemMinima AS MMInternet, ' +

        // Cores
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 10, 2, GETDATE())) AS Est_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 50, 2, GETDATE())) AS Int_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 60, 2, GETDATE())) AS Pag_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 61, 2, GETDATE())) AS Pag_Auditoria1, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 62, 2, GETDATE())) AS Pag_Auditoria2, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 41, 2, GETDATE())) AS MC_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 42, 2, GETDATE())) AS MPub_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 52, 2, GETDATE())) AS PP_Auditoria, ' +
        //'(SUBSTRING(dbo.fn_Produto_Auditoria(a.RelacaoID, 40, 1, GETDATE()), 3, 1)) AS MPub_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 31, 2, GETDATE())) AS PV_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 21, 2, GETDATE())) AS CVPInicio_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 22, 2, GETDATE())) AS CVPFim_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 23, 2, GETDATE())) AS CVPControle_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 32, 2, GETDATE())) AS PE_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 33, 2, GETDATE())) AS MV_Auditoria, ' +
        '(dbo.fn_Produto_Auditoria(a.RelacaoID, 34, 2, GETDATE())) AS MuC_Auditoria ' +

    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
        'INNER JOIN @Table2 b ON (b.RelacaoID = a.RelacaoID) ' +
    'ORDER BY ' + glb_sChavePesquisa + ' ' + glb_sOrdem + ' ' +
    'SET NOCOUNT OFF';

    }
    else {
        //Quando for a transacao de servi�o, deve olhar para a 215. BJBN
        if (selTransacao.value == 211)
            nWhereTransacao = 'AND aa.TransacaoID = 215';
        else
            nWhereTransacao = 'AND aa.TransacaoID = 115';

        var sJoinLotesItens = 'LEFT OUTER JOIN Lotes_Itens f WITH(NOLOCK) ON (f.PedItemID = c.PedItemID) ' +
                              'LEFT OUTER JOIN Lotes g WITH(NOLOCK) ON (f.LoteID = g.LoteID) ';
        var sFiltroLotesItens = ' AND (f.PedItemID IS NULL)';

        if (selProjeto.value != "") {
            sJoinLotesItens = 'INNER JOIN Lotes_Itens f WITH(NOLOCK) ON (f.PedItemID = c.PedItemID) ' +
                              'INNER JOIN Lotes g WITH(NOLOCK) ON (f.LoteID = g.LoteID) ';
            sFiltroLotesItens = '';

            for (i = 0; i < selProjeto.length; i++) {
                if ((selProjeto.options[i].selected == true) && (selProjeto.options[i].value != "")) {
                    sFiltroLotesItens += '/' + selProjeto.options[i].value;
                }
            }
            sFiltroLotesItens = (sFiltroLotesItens == '' ? '' : sFiltroLotesItens + '/');

            sFiltroLotesItens = " AND ('" + sFiltroLotesItens + "' LIKE '%/' + CONVERT(VARCHAR(10), f.LoteID) + '/%') ";
            //" AND (dbo.fn_Lotes_ProdutosQuantidades(f.LoteID, c.ProdutoID, 3, NULL) > 0) ";
        }

        strSQL = ((selProjeto.value == "") ? '' : 'IF EXISTS(SELECT 1 FROM tempdb..sysobjects WHERE id = object_id(\'tempdb..#TempTable\')) DROP TABLE #TempTable ');

        //Pedidos
        strSQL += 'SELECT c.ProdutoID AS ProdutoID, ' +
	            '(SELECT p.Conceito FROM Conceitos p WITH(NOLOCK) INNER JOIN Conceitos m WITH(NOLOCK) ON p.ConceitoID = m.MarcaID AND c.ProdutoID = m.ConceitoID) AS Marca, ' +
	            'e.RecursoAbreviado AS Estado, d.Conceito AS Produto, aa.NumeroProjeto AS Projeto, d.PartNumber AS PartNumber, ' +
	            'dbo.fn_Pessoa_Iniciais(a.ProprietarioID) AS Proprietario, dbo.fn_Pessoa_Iniciais(a.AlternativoID) AS Alternativo, ' + sCustoComprar + ' ' +
                ((selProjeto.value == "") ? 'c.Quantidade AS Quantidade, ' : '0 AS Quantidade, ') +
                'c.ValorUnitario AS Unitario, aa.PedidoID, b.RecursoAbreviado AS Estado2, ' +
	            '(CASE WHEN aa.PessoaID = aa.ParceiroID AND aa.Observacoes LIKE \'%<OPEN:%\' THEN dbo.fn_Pessoa_Fantasia(' +
	            '(CONVERT(INT,REPLACE((REPLACE((SUBSTRING((SUBSTRING(aa.Observacoes, CHARINDEX(\'<OPEN:\', aa.Observacoes), (LEN(CONVERT(VARCHAR(MAX),aa.Observacoes)) - (CHARINDEX(\'<OPEN:\', aa.Observacoes) - 1)))), ' +
			    '0, (CHARINDEX(\'>\', (SUBSTRING(aa.Observacoes, CHARINDEX(\'<OPEN:\', aa.Observacoes), (LEN(CONVERT(VARCHAR(MAX),aa.Observacoes)) - (CHARINDEX(\'<OPEN:\', aa.Observacoes) - 1))))) + 1))), \'<OPEN:\',SPACE(0))),\'>\',SPACE(0)))),0) ELSE ' +
	            'dbo.fn_Pessoa_Fantasia(aa.PessoaID, 0) END) AS Cliente, ' +
	            'dbo.fn_Pessoa_Fantasia(aa.ParceiroID, 0) AS Revenda, ' +
	            'dbo.fn_Pessoa_Fantasia(aa.ProprietarioID, 0) AS Vendedor, aa.TransacaoID AS Transacao, aa.Observacao, ' +
	            'c.PedItemID AS PedItemID, ' +
                'ISNULL(g.LoteID, \'\') AS LoteID, ' +
			    'ISNULL(g.Descricao, \'\') AS Lote, ' +
                'f.LoteID AS ProjetoLoteID, ' +
                'dbo.fn_Produto_Internacional(a.RelacaoID, 1) AS Internacional ' +
            ((selProjeto.value == "") ? '' : 'INTO #TempTable ') +
            'FROM Pedidos aa WITH(NOLOCK) ' +
                    'INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = aa.EstadoID) ' +
                    'INNER JOIN Pedidos_Itens c WITH(NOLOCK) ON (c.PedidoID = aa.PedidoID) ' +
                    'INNER JOIN RelacoesPesCon a WITH(NOLOCK) ON (a.ObjetoID = c.ProdutoID) ' +
                    'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Recursos e WITH(NOLOCK) ON (e.RecursoID = a.EstadoID) ' +
                    sJoinLotesItens +
            'WHERE ((aa.EstadoID = 33) AND (a.SujeitoID = aa.EmpresaID) AND (c.EhEncomenda = 1) ' + nWhereTransacao +
                sFiltroLotesItens + ' AND (c.PedItemEncomendaID IS NULL) AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' +
                glb_sFiltro + glb_sFiltroEspecifico + ' ' + glb_sFiltroDireitos + ' ' +
                sFiltroFornecedor + ' ' + glb_sFiltroFamilia + ' ' + glb_sFiltroMarca + ' ' +
                glb_sFiltroLinha + ' ' + glb_sFiltroProprietario + ' ' +
			    'AND (aa.EmpresaID = ' + glb_aEmpresaData[0] + ')) ' +
			'ORDER BY ' + glb_sChavePesquisa + ' ' + glb_sOrdem + ' ';

        if (selProjeto.value != "") {
            strSQL += 'UPDATE #TempTable ' +
                            'SET Quantidade = (CASE WHEN (Internacional = 0) ' +
                                                'THEN dbo.fn_Lotes_ProdutosQuantidades(ProjetoLoteID, ProdutoID,  3, NULL) ' +
                                                'ELSE dbo.fn_Lotes_ProdutosQuantidades(ProjetoLoteID, ProdutoID, 15, NULL) END) ' +
                        'SELECT ProdutoID, Marca, Estado, Produto, Projeto, PartNumber, Proprietario, Alternativo, CustoComprar, Quantidade, Unitario, PedidoID, Estado2, Cliente, Revenda, Vendedor, Transacao, Observacao, PedItemID, LoteID, Lote, ProjetoLoteID, Internacional FROM #TempTable WHERE (Quantidade > 0) ';
        }
    }

    with (txtAreaTrace) {
        style.left = parseInt(btnCanc.currentStyle.left, 10) +
                     parseInt(btnCanc.currentStyle.width, 10) + ELEM_GAP;
        style.top = parseInt(btnCanc.currentStyle.top, 10);
        style.width = 100;
        style.height = 24;
        style.visibility = 'hidden';
        value = strSQL;
    }

    // Somente para debug
    if (txtAreaTrace.style.visibility != 'hidden')
        showExtFrame(window, true);

    if (bToExcel) {
        var geraExcelID = 1;
        var formato = 2; //Excel
        var sLinguaLogada = getDicCurrLang();

        var strParameters = "RelatorioID=" + geraExcelID + "&Formato=" + formato + "&bToExcel=" + (bToExcel ? true : false) + "&sLinguaLogada=" + sLinguaLogada + "&glb_sEmpresaFantasia=" + glb_aEmpresaData[3] +
                            "&selFornecedor=" + selFornecedor.value + "&selServico=" + selServico.value + "&selFiltroEspecifico=" + selFiltroEspecifico.value + "&chkEnc=" + (chkEnc.checked ? true : false) +
                            "&selFiltroEspecificoFiltro=" + selFiltroEspecificoFiltro + "&selFamilia=" + selFamilia.value + "&selMarca=" + selMarca.value + "&selLinha=" + selLinha.value +
                            "&glb_nMoedaFaturamentoID=" + glb_nMoedaFaturamentoID + "&glb_sReadOnlyClassFiscal=" + glb_sReadOnlyClassFiscal + "&glb_Excel=" + (glb_Excel == 1 ? true : false) +
                            "&glb_Inicio=" + (glb_Inicio ? true : false) + "&glb_nRegistros=" + glb_nRegistros + "&glb_sFiltroEspecifico=" + glb_sFiltroEspecifico + "&sAGrid=" + sAGrid +
                            "&sPercentualPV=" + sPercentualPV + "&glb_sChavePesquisa=" + glb_sChavePesquisa + "&glb_sOrdem=" + glb_sOrdem + "&selTransacao=" + selTransacao.value +
                            "&selProjeto=" + selProjeto.value + "&sFiltroLotesItens=" + sFiltroLotesItens + "&glb_sFiltroContexto=" + glb_sFiltroContexto + "&glb_sPesquisa=" + glb_sPesquisa +
                            "&glb_sFiltro=" + glb_sFiltro + "&glb_sFiltroDireitos=" + glb_sFiltroDireitos + "&glb_sFiltroFamilia=" + glb_sFiltroFamilia +
                            "&glb_sFiltroProprietario=" + glb_sFiltroProprietario + "&glb_aEmpresaData0=" + glb_aEmpresaData[0] + "&selFiltroArgumentosVendaWhere=" + selFiltroArgumentosVendaWhere;

        glb_Excel = 0;
        if (glb_Inicio == true)
            glb_Inicio = false;

        return SYS_PAGESURLROOT + '/serversidegenEx/ReportsGrid_gerenciamentoprodutos.aspx?' + strParameters;
    }
    else if (!bToExcel) {

        glb_Excel = 0;
        if (glb_Inicio == true)
            glb_Inicio = false;

        return strSQL;
    }




}

/********************************************************************
Obtem string de select para o label dos totais.

Parametros:
nenhum

Retorno:
string de select
********************************************************************/
function selectArgumentosVendaTotalizador() {
    var i = 0;
    var strSQL = '';
    var sFiltroFornecedor = '';
    var sFiltroComprar = '';
    var sCustoComprar = '';
    var sPercentualPV = 'NULL';
    var sFiltroData = '';
    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');

    if ((trimStr(txtPercentualPV.value) != '') && (!isNaN(txtPercentualPV.value)))
        sPercentualPV = txtPercentualPV.value;

    if (selFornecedor.value != 0)
        sFiltroFornecedor = ' AND (SELECT COUNT(*) FROM RelacoesPesCon_Fornecedores WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID AND FornecedorID = ' + selFornecedor.value + ')) > 0';

    // Servico 5 Comprar
    if (selServico.value == GERENCIA_DE_ESTOQUE)
        sFiltroComprar = ' AND a.PrevisaoVendas >= 0 ';

    if ((selFiltroEspecifico.value != 0) && (!chkEnc.checked))
        glb_sFiltroEspecifico = ' AND (' + selFiltroEspecifico.options[selFiltroEspecifico.selectedIndex].getAttribute('Filtro', 1) + ')';
    else
        glb_sFiltroEspecifico = '';

    if (selFamilia.value != 0)
        glb_sFiltroFamilia = ' AND ((SELECT aa.ProdutoID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = ' + selFamilia.value + ')';
    else
        glb_sFiltroFamilia = '';

    if (selMarca.value != 0)
        glb_sFiltroMarca = ' AND ((SELECT aa.MarcaID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = ' + selMarca.value + ')';
    else
        glb_sFiltroMarca = '';

    if (selLinha.value != 0)
        glb_sFiltroLinha = ' AND ((SELECT aa.LinhaProdutoID FROM Conceitos aa WITH(NOLOCK) WHERE (aa.ConceitoID = a.ObjetoID)) = ' + selLinha.value + ')';
    else
        glb_sFiltroLinha = '';

    // Come�a a montar o select totalizador.    
    strSQL = 'SET NOCOUNT ON ' +

			'DECLARE @Table2 TABLE (RelacaoID INT) ' +
			'DECLARE @TotalProduto FLOAT ' +
			'DECLARE @TotalArgVenda FLOAT ' +
			'DECLARE @TotalArgVendaOK FLOAT ' +
			'DECLARE @IdiomaID INT ' +

			'SET @IdiomaID = ' + selIdiomaArgumentosVenda.value + ' ' +

			'INSERT INTO @Table2 ' +
				'SELECT a.RelacaoID ' +
				'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                  'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(aa.RelacaoID, NULL, 1, GETDATE()), 1)) AS ProdutoInconsistencia, aa.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon aa WITH(NOLOCK)) AS ProdutosInconsistentes ON (ProdutosInconsistentes._RelacaoID = a.RelacaoID) ' +
				        'INNER JOIN (SELECT dbo.fn_Produto_Estoque(bb.SujeitoID, bb.ObjetoID, 341, NULL, GETDATE(), NULL, NULL, NULL) AS Estoque, bb.RelacaoID AS _RelacaoID ' +
				                'FROM RelacoesPesCon bb WITH(NOLOCK)) AS Estoque ON (Estoque._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT dbo.fn_Produto_Comprar(cc.SujeitoID, cc.ObjetoID, NULL, GETDATE(), 10) AS Compras, cc.RelacaoID AS _RelacaoID ' +
				               'FROM  RelacoesPesCon cc WITH(NOLOCK)) AS ComprasPendentes ON (ComprasPendentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT dbo.fn_Produto_Comprar(dd.SujeitoID, dd.ObjetoID, NULL, GETDATE(), 20) AS Excesso, dd.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon dd WITH(NOLOCK)) AS EstoqueExcesso ON (EstoqueExcesso._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ee.RelacaoID, 10, 1, GETDATE()), 1)) AS EstadoInconsistencia, ee.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon ee WITH(NOLOCK)) AS EstadosInconsistentes ON (EstadosInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ff.RelacaoID, 20, 1, GETDATE()), 1)) AS CVPInconsistencia, ff.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon ff WITH(NOLOCK)) AS CVPInconsistentes ON (CVPInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(gg.RelacaoID, 30, 1, GETDATE()), 1)) AS PVInconsistencia, gg.RelacaoID AS _RelacaoID  ' +
				                'FROM  RelacoesPesCon gg WITH(NOLOCK)) AS PVInconsistentes ON (PVInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(hh.RelacaoID, 40, 1, GETDATE()), 1)) AS MCInconsistencia, hh.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon hh WITH(NOLOCK)) AS MCInconsistentes ON (MCInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(ii.RelacaoID, 50, 1, GETDATE()), 1)) AS InternetInconsistencia, ii.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon ii WITH(NOLOCK)) AS InternetInconsistentes ON (InternetInconsistentes._RelacaoID = a.RelacaoID) ' +
		                'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(jj.RelacaoID, 60, 1, GETDATE()), 1)) AS PagamentoInconsistencia, jj.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon jj WITH(NOLOCK)) AS PagamentosInconsistentes ON (PagamentosInconsistentes._RelacaoID = a.RelacaoID) ' +
                        'INNER JOIN (SELECT (LEFT(dbo.fn_Produto_Auditoria(kk.RelacaoID, 70, 1, GETDATE()), 1)) AS CustosInconsistencia, kk.RelacaoID AS _RelacaoID ' +
				                'FROM  RelacoesPesCon kk WITH(NOLOCK)) AS CustosInconsistentes ON (CustosInconsistentes._RelacaoID = a.RelacaoID) ' +
				'WHERE (';

    if (selServico.value != GERAL) {
        strSQL += glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro +
			glb_sFiltroEspecifico + ' ' + glb_sFiltroDireitos + ' ' + sFiltroFornecedor + ' ' +
			glb_sFiltroFamilia + ' ' + glb_sFiltroMarca + ' ' + glb_sFiltroLinha + ' ' +
			glb_sFiltroProprietario + ' ' + 'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ')) ';
    }
    else {
        strSQL += 'a.RelacaoID IN (';

        for (i = 0; i < aGrid.length; i++) {
            strSQL += aGrid[i];

            if (i != (aGrid.length - 1))
                strSQL += ', ';
            else
                strSQL += ') ';
        }
        strSQL += sFiltroFornecedor + ') ';
    }

    strSQL +=
		// Total de produtos.
		'SELECT @TotalProduto = COUNT(1) FROM @Table2 ' +

		// Total de produtos com argumentos de venda
		'SELECT @TotalArgVenda = COUNT(1) ' +
		'FROM @Table2 ' +
			'INNER JOIN dbo.RelacoesPesCon WITH(NOLOCK) ON [@Table2].RelacaoID = dbo.RelacoesPesCon.RelacaoID ' +
			'INNER JOIN dbo.Conceitos_ArgumentosVenda WITH(NOLOCK) ON dbo.RelacoesPesCon.ObjetoID = dbo.Conceitos_ArgumentosVenda.ConceitoID ' +
		'WHERE ' +
			'dbo.Conceitos_ArgumentosVenda.IdiomaID = @IdiomaID ' +

		// Total de produtos com argumentos de venda OK
		'SELECT @TotalArgVendaOK = COUNT(1) ' +
		'FROM @Table2 ' +
			'INNER JOIN dbo.RelacoesPesCon WITH(NOLOCK) ON [@Table2].RelacaoID = dbo.RelacoesPesCon.RelacaoID ' +
			'INNER JOIN dbo.Conceitos_ArgumentosVenda WITH(NOLOCK) ON dbo.RelacoesPesCon.ObjetoID = dbo.Conceitos_ArgumentosVenda.ConceitoID ' +
		'WHERE ' +
			'dbo.Conceitos_ArgumentosVenda.IdiomaID = @IdiomaID AND ' +
			'dbo.Conceitos_ArgumentosVenda.OK = 1 ' +

		// Gera as percentagens.
		'IF(@TotalProduto > 0) ' +
			'SELECT  ' +
				'@TotalProduto AS TotalProduto,  ' +
				'100*(@TotalArgVenda/@TotalProduto) AS PercentArgVenda,  ' +
				'100*(@TotalArgVendaOK/@TotalProduto) AS PercentArgVendaOK ' +
		'ELSE ' +
			'SELECT  ' +
				'@TotalProduto AS TotalProduto,  ' +
				'0 AS PercentArgVenda,  ' +
				'0 AS PercentArgVendaOK ' +

		'SET NOCOUNT OFF ';

    return strSQL;
}

/********************************************************************
Fecha a janela se mostra-la
********************************************************************/
function unloadThisWin() {
    if (glb_timerUnloadWin != null) {
        window.clearInterval(glb_timerUnloadWin);
        glb_timerUnloadWin = null;
    }

    if (window.top.overflyGen.Alert('Nenhum registro dispon�vel.') == 0)
        return null;

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, 'NOREG');
}

function preencherLinguaArgVendas() {
    setConnection(dsoLinguaArgVendas);

    dsoLinguaArgVendas.SQL = "SELECT ItemID as ID, ItemMasculino as Lingua, EhDefault " +
		"FROM TiposAuxiliares_Itens WITH(NOLOCK) " +
		"WHERE TipoID = 48 AND EstadoID = 2";

    dsoLinguaArgVendas.ondatasetcomplete = preencherLinguaArgVendas_DSC;

    dsoLinguaArgVendas.Refresh();
}

function preencherLinguaArgVendas_DSC() {
    var optionStr, optionValue;

    clearComboEx(['selIdiomaArgumentosVenda']);

    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = '0';
    selIdiomaArgumentosVenda.add(oOption);

    dsoLinguaArgVendas.recordset.moveFirst();

    while (!dsoLinguaArgVendas.recordset.EOF) {
        optionStr = dsoLinguaArgVendas.recordset['Lingua'].value;
        optionValue = dsoLinguaArgVendas.recordset['ID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        if (dsoLinguaArgVendas.recordset["EhDefault"].value == 1) {
            oOption.selected = true;
            selIdiomaArgumentosVenda.setAttribute("oldSelected", optionValue, 1);
        }

        selIdiomaArgumentosVenda.add(oOption);

        dsoLinguaArgVendas.recordset.moveNext();
    }
}

function preencheFiltroEspecifico() {
    fg.rows = 1;

    setConnection(dsoFiltroEspecifico);
    dsoFiltroEspecifico.SQL = 'SELECT a.SujeitoID AS SujeitoID, b.RecursoFantasia AS NomeFiltro, a.Ordem, a.Observacoes, b.Filtro AS Filtro ' +
	                    'FROM RelacoesRecursos a WITH(NOLOCK) ' +
		                    'INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.SujeitoID) ' +
	                    'WHERE ((a.ObjetoID = 21060) AND (b.TipoRecursoID = 5) AND (b.ClassificacaoID = 12)) AND ' +
	                        '(a.Observacoes LIKE \'%[' + selServico.value + ']%\') ' +
	                    'ORDER BY a.Ordem, b.Recurso';

    dsoFiltroEspecifico.ondatasetcomplete = preencheFiltroEspecifico_DSC;
    dsoFiltroEspecifico.Refresh();
}

function preencheFiltroEspecifico_DSC() {
    var optionStr, optionValue;

    clearComboEx(['selFiltroEspecifico']);

    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = '';
    selFiltroEspecifico.add(oOption);

    dsoFiltroEspecifico.recordset.moveFirst();

    while (!dsoFiltroEspecifico.recordset.EOF) {
        optionStr = dsoFiltroEspecifico.recordset['NomeFiltro'].value;
        optionValue = dsoFiltroEspecifico.recordset['SujeitoID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.setAttribute('Filtro', dsoFiltroEspecifico.recordset['Filtro'].value, 1);
        selFiltroEspecifico.add(oOption);
        dsoFiltroEspecifico.recordset.MoveNext();
    }

    /*
	if (selServico.value == INCONSISTENCIAS)
        selOptByValueInSelect(getHtmlId(), 'selFiltroEspecifico', 41018);
    else
	*/

    if (selServico.value == GERAL)
        selFiltroEspecifico.selectedIndex = 0;

    //else    
    //	selOptByValueInSelect(getHtmlId(), 'selFiltroEspecifico', 41020);
}

function preencheFamilia() {
    glb_nValue = selFamilia.value;

    setConnection(dsoFamilia);

    var sWHERE = '';

    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ');

    if (selMarca.value != 0)
        sWHERE += (' AND (b.MarcaID = ' + selMarca.value + ') ');

    dsoFamilia.SQL = 'SELECT 0 AS FamiliaID, \'\' AS Familia ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS FamiliaID, c.Conceito AS Familia ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Familia';

    dsoFamilia.ondatasetcomplete = preencheFamilia_DSC;
    dsoFamilia.Refresh();
}

function preencheFamilia_DSC() {
    clearComboEx(['selFamilia']);

    if (!(dsoFamilia.recordset.BOF || dsoFamilia.recordset.EOF)) {
        dsoFamilia.recordset.MoveFirst();

        while (!dsoFamilia.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoFamilia.recordset['Familia'].value;;
            oOption.value = dsoFamilia.recordset['FamiliaID'].value;
            selFamilia.add(oOption);
            dsoFamilia.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selFamilia', glb_nValue);

        selFamilia.disabled = ((selFamilia.length <= 1) ? true : false);
    }
}

function preencheMarca() {
    glb_nValue = selMarca.value;

    setConnection(dsoMarca);

    var sWHERE = '';

    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ');

    if (selFamilia.value != 0)
        sWHERE += (' AND (b.ProdutoID = ' + selFamilia.value + ') ');

    dsoMarca.SQL = 'SELECT 0 AS MarcaID, \'\' AS Marca ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS MarcaID, c.Conceito AS Marca ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.MarcaID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Marca';

    dsoMarca.ondatasetcomplete = preencheMarca_DSC;
    dsoMarca.Refresh();
}

function preencheMarca_DSC() {
    clearComboEx(['selMarca']);

    if (!(dsoMarca.recordset.BOF || dsoMarca.recordset.EOF)) {
        dsoMarca.recordset.MoveFirst();

        while (!dsoMarca.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoMarca.recordset['Marca'].value;;
            oOption.value = dsoMarca.recordset['MarcaID'].value;
            selMarca.add(oOption);
            dsoMarca.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selMarca', glb_nValue);

        selMarca.disabled = ((selMarca.length <= 1) ? true : false);
    }
}

function preencheLinha() {
    glb_nValue = selLinha.value;

    setConnection(dsoLinha);

    var sWHERE = '';

    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ' +
        'AND (b.MarcaID = ' + selMarca.value + ') ');

    dsoLinha.SQL = 'SELECT 0 AS LinhaID, \'\' AS Linha ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS LinhaID, c.Conceito AS Linha ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.LinhaProdutoID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Linha';

    dsoLinha.ondatasetcomplete = preencheLinha_DSC;
    dsoLinha.Refresh();
}

function preencheLinha_DSC() {
    clearComboEx(['selLinha']);

    if (!(dsoLinha.recordset.BOF || dsoLinha.recordset.EOF)) {
        dsoLinha.recordset.MoveFirst();

        while (!dsoLinha.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoLinha.recordset['Linha'].value;;
            oOption.value = dsoLinha.recordset['LinhaID'].value;
            selLinha.add(oOption);
            dsoLinha.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selLinha', glb_nValue);

        selLinha.disabled = ((selLinha.length <= 1) ? true : false);
    }
}

function btnCalcularPV_onclick() {
    if (fg.Rows > 2) {
        var LinhasEditar = 0;

        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0)
                LinhasEditar += 1;
        }

        if (LinhasEditar == 0) {
            window.top.overflyGen.Alert('Selecione um produto.');
            return null;
        }
        else {
            var strPars = '';

            strPars = '?nEmpresaID=' + dsoGrid.recordset['SujeitoID'].value;
            nDataLen = 0;

            for (i = 2; i < fg.Rows; i++) {
                if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0) {
                    strPars += '&nProdutoID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'ObjetoID*', i)));
                    nDataLen++;
                }
            }

            dsoPrevisaoVendas.URL = SYS_ASPURLROOT + '/serversidegenEx/calcularpv.aspx' + strPars + '&nDataLen=' + escape(nDataLen);;
            dsoPrevisaoVendas.ondatasetcomplete = btnCalcularPV_onclick_DSC;
            dsoPrevisaoVendas.refresh();
        }
    }
    else
        return null;
}

function btnCalcularPV_onclick_DSC() {
    fillGridData();

    if (window.top.overflyGen.Alert(dsoPrevisaoVendas.recordset['Resultado'].value) == 0)
        return null;
}

function selFamilia_onchange() {
    preencheMarca();
}

function selMarca_onchange() {
    preencheFamilia();
    preencheLinha();
}

function limpaGrid() {
    chkEnc.checked = false;
    fg.rows = 1;
    alignButtons();
}

function selServico_onchange() {
    var bFillGrid = false;
    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');

    // Oculta o combo do filtro do argumento de venda caso necess�rio.
    selFiltroArgumentosVenda.style.visibility = ((selServico.value == INTERNET) ? 'inherit' : 'hidden');
    lblFiltroArgumentosVenda.style.visibility = selFiltroArgumentosVenda.style.visibility;

    if ((selServico.value == GERAL) && (aGrid == null)) {
        window.top.overflyGen.Alert('O servi�o geral n�o est� dispon�vel porque n�o h� produtos na listagem do PesCon.');
        selServico.value = CICLO_DE_VIDA;
        lockInterface(false);
    }

    fg.rows = 1;

    sVisibility = ((selServico.value == GERENCIA_DE_ESTOQUE) ? 'inherit' : 'hidden');
    lblFornecedor.style.visibility = sVisibility;
    selFornecedor.style.visibility = sVisibility;
    selFornecedor.disabled = (selFornecedor.options.length == 0);
    lblTransacao.style.visibility = sVisibility;
    selTransacao.style.visibility = sVisibility;
    selTransacao.disabled = (selTransacao.options.length == 0);
    lblFinanciamento.style.visibility = sVisibility;
    selFinanciamento.style.visibility = sVisibility;
    selFinanciamento.disabled = (selFinanciamento.options.length == 0);
    btnComprar.style.visibility = sVisibility;
    btnBatch.style.visibility = sVisibility;

    //Novos campos Lote. Inicio. BJBN
    lblProjeto.style.visibility = sVisibility;
    selProjeto.style.visibility = sVisibility;
    lblConsolidar.style.visibility = sVisibility;
    chkConsolidar.style.visibility = sVisibility;
    lblLote.style.visibility = sVisibility;
    selLote.style.visibility = sVisibility;
    lblDescricao.style.visibility = sVisibility;
    txtDescricao.style.visibility = sVisibility;
    btnAplicar.style.visibility = sVisibility;
    //Novos campos Lote. Fim. BJBN
    lblPtax.style.visibility = sVisibility;
    txtPtax.style.visibility = sVisibility;

    if (selServico.value == GERAL) {
        clearComboEx(['selFiltroEspecifico']);
        selFiltroEspecifico.disabled = true;
    }
    else {
        selFiltroEspecifico.disabled = false;
        preencheFiltroEspecifico();
    }

    alignButtons();

    if (selServico.value == GERENCIA_DE_ESTOQUE) {
        if (bFillGrid != false)
            fillGridData();
    }
    else {
        selFornecedor.selectedIndex = 0;
        chkEnc.checked = false;
    }
}

function selIdiomaArgumentosVenda_onchange() {
    salvaArgumentos();

    if (selIdiomaArgumentosVenda.value == "0") {
        lblFiltroArgumentosVenda.disabled = true;
        selFiltroArgumentosVenda.disabled = true;

        divArgumentoVendas.style.visibility = 'hidden';

        glb_Proporcional = 1;

        divFG.style.height = FGExtended();
        fg.style.height = FGExtended() - 2;
    }
    else {
        lblFiltroArgumentosVenda.disabled = false;
        selFiltroArgumentosVenda.disabled = false;

        divArgumentoVendas.style.visibility = 'inherit';

        glb_Proporcional = 0.8;

        divFG.style.height = FGCollapse();
        fg.style.height = FGCollapse() - 2;
    }

    consultaArgumentos();

    selIdiomaArgumentosVenda.setAttribute("oldSelected", selIdiomaArgumentosVenda.value, 1);
}

function selFiltroArgumentosVenda_onchange() {
    if (selFiltroArgumentosVenda.value == 0) {
        selFiltroArgumentosVenda.setAttribute(
			"clausulaWhere",
			" ArgumentosVenda IS NULL ",
			1);
    }
    else if (selFiltroArgumentosVenda.value == 1 || selFiltroArgumentosVenda.value == 2) {
        selFiltroArgumentosVenda.setAttribute(
			"clausulaWhere",
			"dbo.fn_Produto_ArgumentosVendaOK(a.ObjetoID, " + selIdiomaArgumentosVenda.value + ") = " + selFiltroArgumentosVenda.value,
			1);
    }
    else {
        selFiltroArgumentosVenda.setAttribute(
			"clausulaWhere",
			"",
			1);
    }
}

function chkOK_onchange() {
    houveAlteracaoArgVenda();
    salvaArgumentos();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) {
        if (window.top.overflyGen.Alert('Nenhum registro dispon�vel.') == 0)
            return null;

        txtArgumentoDeVenda.value = "";
        chkOK.checked = false;

        lockControlsInModalWin(false);
        fg.Rows = 1;
        return null;
    }

    showExtFrame(window, true);
    lockControlsInModalWin(true);
    fillGridData_DSC2();
    //    glb_refreshGrid2 = window.setInterval('fillGridData_DSC2()', 500, 'JavaScript');
}

function fillGridData_DSC2() {
    if (glb_refreshGrid2 != null) {
        window.clearInterval(glb_refreshGrid2);
        glb_refreshGrid2 = null;
    }

    var dTFormat = '';
    var i, j;
    var bgColorGreen, bgColorYellow, bgColorRed, bgColorPurple, bgColorBlack;
    var sPaintFiedNumber = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    fg.Editable = false;
    fg.Redraw = 0;
    fg.Rows = 1;
    fg.FontSize = '8';
    fg.ExplorerBar = 0;

    // Dados de Gerencia de Produto    
    if (selFiltroEspecifico.value == 0)
        preencheFiltroEspecifico();

    // Consulta dos argumentos de venda.
    var argumentosVendaDoConceito = "SELECT ConArgumentoID,ConceitoID,IdiomaID,ArgumentosVenda,OK FROM Conceitos_ArgumentosVenda WITH(NOLOCK) WHERE ConceitoID in (";

    if (!chkEnc.checked) {
        var aLabels, aMaskReadOnly, aMaskEdit, aHiddenCols, aAlignCols, FocusCol;

        // Variaveis para tratar colunas dinamicamente readonly
        glb_PrecoBaseMin_ReadOnly = '';
        glb_NCM_ReadOnly = '';
        glb_Pagamento_ReadOnly = '*';
        glb_PagamentoDias_ReadOnly = '*';
        glb_Fim_ReadOnly = '';
        glb_LoteID_ReadOnly = '';
        glb_PE_ReadOnly = '';
        glb_PV_ReadOnly = '';
        glb_MultiploCompra_ReadOnly = '';

        aLabels = [

                // Colunas comuns iniciais
                'ID',                                   //00 - 0
                'E',                                    //01 - 1
                'Produto',                              //02 - 2
                'Marca',                                //03 - 3
                'OK',                                   //04 - 7
                'GP',                                   //05 - 8
                'Esp',                                  //06 - 9

                // Colunas CVP
                'In�cio',                               //07 - 10
                'Fim',                                  //08 - 11
                'Dias',                                 //09 - 12
                'Controle',                             //10 - 13
                'Dias',                                 //11 - 14

                // Colunas Custo
                'Quant',                                //12 - 54
                'Custo ' + glb_sMoeda,                  //13 - 55
                'Custo Total ' + glb_sMoeda,            //14 - 56
                'Custo M�dio',                          //15 - 20

                // Colunas Margem Contribui��o
                'MP',                                    //16 - 39
                'MC',                                    //17 - 40
                'MR',                                    //18 - 41
                'MPub',                                  //19 - 69
                'MMin',                                  //20 - 42
                'MMed',                                  //21 - 43
                'Contribui��o',                          //22 - 31
                'Contribui��o Total',                    //23 - 32

                // Colunas Pre�o
                'Preco Base',                            //24 - 44
                'Preco Base Min',                        //25 - 45
                'Preco Base Med',                        //26 - 46
                'Preco Min',                             //27 - 47
                'Pre�o',                                 //28 - 29
                'Pre�o Total',                           //29 - 30
                'Pre�o Internet',                        //30 - 71
                'Preco Conc',                            //31 - 49

                // Colunas Dados do Fornecedor 1
                'Custo Rep1',                            //32 - 58
                'PP1',                                   //33 - 61
                'PR1',                                   //34 - 62

                // Colunas Estoque
                'RC',                                    //35 - 66
                'RVC',                                   //36 - 67
                'Previs�o Entrega',                       //37 - 68
                'Estoque',                               //38 - 19
                'Co/Ex',                                 //39 - 57

                // Colunas Movimenta��o de Produto
                'Pagamento',                             //40 - 15
                'Dias',                                  //41 - 16
                'Ult Entrada',                           //42 - 17
                'Ult Sa�da',                             //43 - 18

                // Colunas Vendas
                'VM3',                                   //44 - 23
                'VM2',                                   //45 - 24
                'VM1',                                   //46 - 25
                'VM0',                                   //47 - 26
                'MV13',                                  //48 - 27

                // Colunas Suprimento
                'PV',                                    //49 - 28
                'PE',                                    //50 - 52
                'MV',                                    //51 - 50
                'MC',                                    //52 - 51

                // Colunas Publica��es Internet
                'PI',                                    //53 - 72
                'PP',                                    //54 - 73
                'PE%',                                   //55 - 74
                'Est Int',                               //56 - 75
                'Troca',                                 //57 - 76
                'Gar',                                   //58 - 77
                'Asstec',                                //59 - 78

                // Colunas Projeto
                'LoteID',                                //60 - 4
                'Lote',                                  //61 - 5
                'Aplicar',                               //62 - 6
                'Projeto',                               //63 - 59

                // Colunas comuns finais
                'PartNumber',                            //64 - 60
                'NCM',                                   //65 - 53
                'Observa��o',                            //66 - 79

                // Walking Dead
                'Dias',                                  //67 - 22
                'Planejamento Total',                    //68 - 34
                'Var',                                   //69 - 35
                'Custo Total',                           //70 - 21
                'Custo M�dio',                           //71 - 38
                'MC',                                    //72 - 33
                'MMin',                                  //73 - 70
                'Preco',                                 //74 - 48
                'Estoque',                               //75 - 65
                'Estoque',                               //76 - 36
                'Co/Ex',                                 //77 - 37
                'PV',                                    //78 - 64
                'PE',                                    //79 - 63

                // Colunas escondidas                      
                'RelacaoID',                             //80 - 80
                'abrevCor',                              //81 - 81

                'Est_Auditoria',                         //82 - 82
                'Int_Auditoria',                         //83 - 83
                'Pag_Auditoria1',                        //84 - 84
                'MC_Auditoria',                          //85 - 85
                'PV_Auditoria',                          //86 - 86
                'CVPInicio_Auditoria',                   //87 - 87
                'CVPFim_Auditoria',                      //88 - 88
                'CVPControle_Auditoria',                 //89 - 89
                'PE_Auditoria',                          //90
                'MV_Auditoria',                          //91
                'MuC_Auditoria',                         //92
                'MPub_Auditoria',                        //93
                'PP_Auditoria',                          //94
                'Pag_Auditoria2'];                       //95
        
        aMaskReadOnly = [
            // Colunas comuns iniciais
            '9999999999',                   // 'ID'                                 //00 - 00
            '',                             // 'E'                                  //01 - 01
            '',                             // 'Produto'                            //02 - 02
            '',                             // 'Marca'                              //03 - 03
            '',                             // 'OK'                                 //04 - 07
	        '',                             // 'GP'                                 //05 - 08
	        '',                             // 'Esp'                                //06 - 09

            // Colunas CVP
            '99/99/9999',                   // 'In�cio'                             //07 - 10
            '99/99/9999',                   // 'Fim'                                //08 - 11
            '9999',                         // 'Dias'                               //09 - 12
            '99/99/9999',                   // 'Controle'                           //10 - 13
            '9999',                         // 'Dias'                               //11 - 14

            // Colunas Custo
	        '99999',                        // 'Quant'                              //12 - 54
	        '999999999.99',                 // 'Custo ' + glb_sMoeda                //13 - 55
	        '999999999.99',                 // 'Custo Total ' + glb_sMoeda          //14 - 56
            '999999999.99',                 // 'CM'                                 //15 - 20

            // Colunas Margem Contribui��o
            '#99.99',                       // 'MP'                                 //16 - 39
            '#99.99',                       // 'MC'                                 //17 - 40
            '#99.99',                       // 'MR'                                 //18 - 41
            '#99.99',                       // 'M Pub'                              //19 - 69
            '#99.99',                       // 'MMin'                               //20 - 42
            '#99.99',                       // 'MMed'                               //21 - 43
            '999999999.99',                 // 'Contribui��o'                       //22 - 31
            '999999999.99',                 // 'Contribui��o Total'                 //23 - 32

            // Colunas Pre�o
            '999999999.99',                 // 'Preco Base'                         //24 - 44
            '999999999.99',                 // 'Preco Base Min'                     //25 - 45
            '999999999.99',                 // 'Preco Base Med'                     //26 - 46
            '999999999.99',                 // 'Preco Minimo'                       //27 - 47
            '999999999.99',                 // 'Pre�o'                              //28 - 29
            '999999999.99',                 // 'Pre�o Total'                        //29 - 30
            '999999999.99',                 // 'Pre�o Internet'                     //30 - 71
            '999999999.99',                 // 'Preco Conc'                         //31 - 49

            // Colunas Dados do Fornecedor 1
            '999999999.99',                 // 'Custo Rep1'                         //32 - 58
            '999',                          // 'PP1'                                //33 - 61
            '999',                          // 'PR1'                                //34 - 62

            // Colunas Estoque
            '999999',                       // 'RC'                                 //35 - 66
            '999999',                       // 'RVC'                                //36 - 67
            '',                             // 'Previs�o Entrega'                    //37 - 68
            '#9999',                        // Qtde dispon�vel em Estoque           //38 - 19
	        '999999',                       // 'Co/Ex'                              //39 - 57

            // Colunas Movimenta��o de Produto
            '99/99/9999',                   // 'Pagamento'                          //40 - 15
            '9999',                         // 'Dias'                               //41 - 16
            '99/99/9999',                   // 'Ult Entrada'                        //42 - 17
            '99/99/9999',                   // 'Ult Sa�da'                          //43 - 18

            // Colunas Vendas
            '#9999',                        // 'VM3'                                //44 - 23
            '#9999',                        // 'VM2'                                //45 - 24
            '#9999',                        // 'VM1'                                //46 - 25
            '#9999',                        // 'VM0'                                //47 - 26
            '#9999',                        // 'MV13'                               //48 - 27

            // Colunas Suprimento
            '999999',                       // 'PV'                                 //49 - 28
            '999',                          // 'PE'                                 //50 - 52
            '999',                          // 'MV'                                 //51 - 50
            '999',                          // 'MC'                                 //52 - 51

            // Colunas Publica��es Internet
            '',                             // 'PI'                                 //53 - 72
            '',                             // 'PP'                                 //54 - 73
            '999',                          // 'PE%'                                //55 - 74
            '999999',                       // 'Est Int'                            //56 - 75
            '999',                          // 'Troca'                              //57 - 76
            '999',                          // 'Gar'                                //58 - 77
            '999',                          // 'Asstec'                             //59 - 78

            // Colunas Projeto
            '',                             // 'LoteID'                             //60 - 04
            '',                             // 'Lote'                               //61 - 05
            '',                             // 'Aplicar'                            //62 - 06
	        '',                             // 'Projeto'                            //63 - 59

            // Colunas comuns finais
	        '',                             // 'PartNumber'                         //64 - 60
            '',                             // 'NCM'                                //65 - 53
            '',                             // 'Observa��o'                         //66 - 79

            // Walking Dead
            '9999',                         // 'Dias'                               //67 - 22  
            '999999999.99',                 // 'Planejamento Total'                 //68 - 34
            '#99.99',                       // 'Var'                                //69 - 35
            '999999999.99',                 // 'CT'                                 //70 - 21
            '999999999.99',                 // 'Custo M�dio'                        //71 - 38      
            '#99.99',                       // 'MC'                                 //72 - 33
            '#99.99',                       // 'M Min'                              //73 - 70
            '999999999.99',                 // 'Preco'                              //74 - 48
            '999999',                       // 'Estoque'                            //75 - 65
            '#9999',                        // 'Estoque'                            //76 - 36
            '#9999',                        // 'Co/Ex'                              //77 - 37
            '999999',                       // 'PV'                                 //78 - 64
            '999',                          // 'PE'                                 //79 - 63            

            // Colunas escondidas
            '9999999999',                   // 'RelacaoID'                          //80 - 80
            '',                             // 'abrevCor'                           //81 - 81

            '',                             //'Est_Auditoria',                //82 - 82
            '',                             //'Int_Auditoria',                //83 - 83
            '',                             //'Pag_Auditoria1',                //84 - 84
            '',                             //'MC_Auditoria',                 //85 - 85
            '',                             //'PV_Auditoria',                 //86 - 86
            '',                             //'CVPInicio_Auditoria',          //87 - 87
            '',                             //'CVPFim_Auditoria',             //88 - 88
            '',                             //'CVPControle_Auditoria'         //89 - 89 
            '',                             //'PE_Auditoria',                 //90
            '',                             //'MV_Auditoria',                 //91
            '',                             //'MuC_Auditoria',                //92
            '',                             //'MPub_Auditoria',               //93
            '',                             //'PP_Auditoria'                  //94
            ''];                            //'Pag_Auditoria2',               //95

        aMaskEdit = [

            // Colunas comuns iniciais
            '##########',                   // 'ID'                                 //00 - 00
            '',                             // 'E'                                  //01 - 01
            '',                             // 'Produto'                            //02 - 02
            '',                             // 'Marca'                              //03 - 03
            '',                             // 'OK'                                 //04 - 07
	        '',                             // 'GP'                                 //05 - 08
	        '',                             // 'Esp'                                //06 - 09

            // Colunas CVP
            dTFormat,                       // 'In�cio'                             //07 - 10
            dTFormat,                       // 'Fim'                                //08 - 11
            '####',                         // 'Dias'                               //09 - 12
            dTFormat,                       // 'Controle'                           //10 - 13
            '####',                         // 'Dias'                               //11 - 14

            // Colunas Custo
	        '#####',                        // 'Quant'                              //12 - 54
	        '###,###,##0.00',               // 'Custo ' + glb_sMoeda                //13 - 55
	        '###,###,##0.00',               // 'Custo Total ' + glb_sMoeda          //14 - 56
            '###,###,##0.00',               // 'CM'                                 //15 - 20

            // Colunas Margem Contribui��o
            '##0.00',                       // 'MP'                                 //16 - 39
            '##0.00',                       // 'MC'                                 //17 - 40
            '##0.00',                       // 'MR'                                 //18 - 41
            '##0.00',                       // 'M Pub'                              //19 - 69
            '##0.00',                       // 'MMin'                               //20 - 42
            '##0.00',                       // 'MMed'                               //21 - 43
            '###,###,##0.00',               // 'Contribui��o'                       //22 - 31
            '###,###,##0.00',               // 'Contribui��o Total'                 //23 - 32

            // Colunas Pre�o
            '###,###,##0.00',               // 'Preco Base'                         //24 - 44
            '###,###,##0.00',               // 'Preco Base Min'                     //25 - 45
            '###,###,##0.00',               // 'Preco Base Med'                     //26 - 46
            '###,###,##0.00',               // 'Preco Minimo'                       //27 - 47
            '###,###,##0.00',               // 'Pre�o'                              //28 - 29
            '###,###,##0.00',               // 'Pre�o Total'                        //29 - 30
            '###,###,##0.00',               // 'Pre�o Internet'                     //30 - 71
            '###,###,##0.00',               // 'Preco Conc'                         //31 - 49

            // Colunas Dados do Fornecedor 1
            '###,###,##0.00',               // 'Custo Rep1'                         //32 - 58
            '###',                          // 'PP1'                                //33 - 61
            '###',                          // 'PR1'                                //34 - 62

            // Colunas Estoque
            '######',                       // 'RC'                                 //35 - 66
            '######',                       // 'RVC'                                //36 - 67
            '',                             // 'Previs�o Entrega'                    //37 - 68
            '#9999',                        // Qtde dispon�vel em Estoque           //38 - 19
	        '999999',                       // 'Co/Ex'                              //39 - 57

            // Colunas Movimenta��o de Produto
            dTFormat,                       // 'Pagamento'                          //40 - 15
            '####',                         // 'Dias'                               //41 - 16
            dTFormat,                       // 'Ult Entrada'                        //42 - 17
            dTFormat,                       // 'Ult Sa�da'                          //43 - 18

            // Colunas Vendas
            '#####',                        // 'VM3'                                //44 - 23
            '#####',                        // 'VM2'                                //45 - 24
            '#####',                        // 'VM1'                                //46 - 25
            '#####',                        // 'VM0'                                //47 - 26
            '#####',                        // 'MV13'                               //48 - 27

            // Colunas Suprimento
            '######',                       // 'PV'                                 //49 - 28
            '###',                          // 'PE'                                 //50 - 52
            '###',                          // 'MV'                                 //51 - 50
            '###',                          // 'MC'                                 //52 - 51

            // Colunas Publica��es Internet
            '',                             // 'PI'                                 //53 - 72
            '',                             // 'PP'                                 //54 - 73
            '###',                          // 'PE%'                                //55 - 74
            '######',                       // 'Est Int'                            //56 - 75
            '###',                          // 'Troca'                              //57 - 76
            '###',                          // 'Gar'                                //58 - 77
            '###',                          // 'Asstec'                             //59 - 78

            // Colunas Projeto
            '',                             // 'LoteID'                             //60 - 04
            '',                             // 'Lote'                               //61 - 05
            '',                             // 'Aplicar'                            //62 - 06
	        '',                             // 'Projeto'                            //63 - 59

            // Colunas comuns finais
	        '',                             // 'PartNumber'                         //64 - 60
            '',                             // 'NCM'                                //65 - 53
            '',                             // 'Observa��o'                         //66 - 79

            // Walking Dead
            '####',                         // 'Dias'                               //67 - 22  
            '###,###,##0.00',               // 'Planejamento Total'                 //68 - 34
            '##0.00',                       // 'Var'                                //69 - 35
            '###,###,##0.00',               // 'CT'                                 //70 - 21
            '###,###,##0.00',               // 'Custo M�dio'                        //71 - 38      
            '##0.00',                       // 'MC'                                 //72 - 33
            '##0.00',                       // 'M Min'                              //73 - 70
            '###,###,##0.00',               // 'Preco'                              //74 - 48
            '######',                       // 'Estoque'                            //75 - 65
            '#####',                        // 'Estoque'                            //76 - 36
            '#####',                        // 'Co/Ex'                              //77 - 37
            '######',                       // 'PV'                                 //78 - 64
            '###',                          // 'PE'                                 //79 - 63            

            // Colunas escondidas
            '##########',                   // 'RelacaoID'                          //80 - 80
            '',                            // 'abrevCor'                           //81 - 81

            '',                             //'Est_Auditoria',                //82 - 82
            '',                             //'Int_Auditoria',                //83 - 83
            '',                             //'Pag_Auditoria1',                //84 - 84
            '',                             //'MC_Auditoria',                 //85 - 85
            '',                             //'PV_Auditoria',                 //86 - 86
            '',                             //'CVPInicio_Auditoria',          //87 - 87
            '',                             //'CVPFim_Auditoria',             //88 - 88
            '',                             //'CVPControle_Auditoria'         //89 - 89 
            '',                             //'PE_Auditoria',                 //90
            '',                             //'MV_Auditoria',                 //91
            '',                             //'MuC_Auditoria',                //92
            '',                             //'MPub_Auditoria',               //93
            '',                             //'PP_Auditoria'                  //94
            ''];                            //'Pag_Auditoria2',               //95
        
        aAlignCols = [9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 38, 39, 41, 44, 45, 46, 47, 48, 49, 50, 51, 52, 55, 56, 57, 58, 59, 60, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79];

        // Ciclo de vida
        if (selServico.value == CICLO_DE_VIDA) {
            aHiddenCols = [12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 39, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 65, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95];
            FocusCol = 9;

            glb_PV_ReadOnly = '*';
        }

            // Previsao de vendas
        else if (selServico.value == PREVEVISAO_DE_VENDAS) {
            aHiddenCols = [7, 10, 11, 12, 13, 14, 15, 16, 18, 19, 20, 21, 24, 25, 26, 27, 30, 31, 32, 33, 34, 35, 36, 37, 42, 43, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 65, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95];
            FocusCol = 9;

            glb_Fim_ReadOnly = '*';
        }

            // Formacao de precos
        else if (selServico.value == FORMACAO_DE_PRECO) {
            aHiddenCols = [7, 8, 9, 10, 11, 12, 13, 14, 22, 23, 29, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95];
            FocusCol = 9;

            if (chkColunas.checked) {
                glb_PrecoBaseMin_ReadOnly = '*';
                glb_NCM_ReadOnly = '*';
            }
        }
            /* Comentado conforme solicitado pelo Eduardo Rodrigues. LIV 23/11/2017.
            // Informacoes basicas
            else if (selServico.value == INFORMACOES_BASICAS) {
                aHiddenCols = [4, 5, 6, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 80, 81];
                FocusCol = 9;
            }
            */
            // Gerenciamento de estoque
        else if (selServico.value == GERENCIA_DE_ESTOQUE) {
            aHiddenCols = [7, 10, 11, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 40, 41, 42, 43, 44, 45, 46, 47, 48, 51, 53, 54, 55, 56, 57, 58, 59, 65, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95];
            FocusCol = 9;

            glb_Fim_ReadOnly = '*';

            if (chkColunas.checked) {
                glb_PE_ReadOnly = '*';
                glb_PV_ReadOnly = '*';
                glb_MultiploCompra_ReadOnly = '*';
            }
        }

            // Internet
        else if (selServico.value == INTERNET) {
            aHiddenCols = [7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 60, 61, 62, 63, 65, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95];
            FocusCol = 9;
        }

            // Inconsistencias
        else if (selServico.value == INCONSISTENCIAS) {
            aHiddenCols = [12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 65, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95];
            FocusCol = 9;

            glb_Pagamento_ReadOnly = '';
            glb_PagamentoDias_ReadOnly = '';
            glb_PV_ReadOnly = '*';
        }

            // Geral
        else if (selServico.value == GERAL) {
            aHiddenCols = [12, 13, 14, 18, 21, 23, 24, 25, 26, 27, 29, 35, 36, 42, 43, 44, 45, 46, 47, 48, 56, 57, 58, 59, 60, 61, 62, 63, 67, 68, 69, 70, 71, 72, 73, 74, 75, 76, 77, 78, 79, 80, 81, 82, 83, 84, 85, 86, 87, 88, 89, 90, 91, 92, 93, 94, 95];
            FocusCol = 9;
            glb_NCM_ReadOnly = '*';
        }

        headerGrid(fg, aLabels, aHiddenCols);

        var i = 0;
        fg.Redraw = 0;
        fg.FrozenCols = 5;

        // Colunas comuns iniciais
        fg.ColKey(i++) = 'ObjetoID*';                                                                                   //00 - 00  
        fg.ColKey(i++) = 'Estado*';                                                                                     //01 - 01
        fg.ColKey(i++) = 'Produto*';                                                                                    //02 - 02
        fg.ColKey(i++) = 'Marca*';                                                                                      //03 - 03
        fg.ColKey(i++) = 'ChkOK';                                                                                       //04 - 07
        fg.ColKey(i++) = 'Proprietario*';                                                                               //05 - 08
        fg.ColKey(i++) = 'Alternativo*';                                                                                //06 - 09

        // Colunas CVP
        fg.ColKey(i++) = 'dtInicio' + glb_sReadOnly;                                                                    //07 - 10
        fg.ColKey(i++) = 'dtFim' + ((glb_sReadOnly == '*') ? glb_sReadOnly : glb_Fim_ReadOnly);                         //08 - 11
        fg.ColKey(i++) = 'DiasFim*';                                                                                    //09 - 12
        fg.ColKey(i++) = 'dtControle' + glb_sReadOnly;                                                                  //10 - 13
        fg.ColKey(i++) = 'DiasControle*';                                                                               //11 - 14

        // Colunas Custo
        fg.ColKey(i++) = 'QuantidadeComprar';                                                                           //12 - 54
        fg.ColKey(i++) = 'CustoComprar';                                                                                //13 - 55
        fg.ColKey(i++) = 'CustoTotalComprar*';                                                                          //14 - 56
        fg.ColKey(i++) = 'CustoMedio*';                                                                                 //15 - 20

        // Colunas Margem de Contribui��o
        fg.ColKey(i++) = 'MargemPadrao' + glb_sReadOnly;                                                                //16 - 39
        fg.ColKey(i++) = 'MargemContribuicao' + glb_sReadOnly;                                                          //17 - 40
        fg.ColKey(i++) = 'MargemReal*';                                                                                 //18 - 41
        fg.ColKey(i++) = 'MargemPublicacao' + glb_sReadOnly;                                                            //19 - 69
        fg.ColKey(i++) = 'MargemMinima' + glb_sReadOnly;                                                                //20 - 42
        fg.ColKey(i++) = 'MargemMedia*';                                                                                //21 - 43
        fg.ColKey(i++) = 'Contribuicao*';                                                                               //22 - 31
        fg.ColKey(i++) = 'ContribuicaoTotal*';                                                                          //23 - 32

        // Colunas Pre�o
        fg.ColKey(i++) = 'PrecoBase';                                                                                   //24 - 44
        fg.ColKey(i++) = 'PrecoBaseMinimo' + ((glb_sReadOnly == '*') ? glb_sReadOnly : glb_PrecoBaseMin_ReadOnly);      //25 - 45


        fg.ColKey(i++) = 'PrecoBaseMedio*';                                                                             //26 - 46
        fg.ColKey(i++) = 'PrecoMinimo',                                                                                 //27 - 47 - Adicionado coluna pre�o minimo solicitado pelo Marco. BJBN 13/08/2012
        fg.ColKey(i++) = 'Preco_2*';                                                                                    //28 - 29
        fg.ColKey(i++) = 'PrecoTotal_2*';                                                                               //29 - 30
        fg.ColKey(i++) = 'PrecoInternet*';                                                                              //30 - 71
        fg.ColKey(i++) = 'PrecoConcorrentes*';                                                                          //31 - 49

        // Colunas Dados do Fornecedor 1
        fg.ColKey(i++) = 'CustoReposicao*';                                                                             //32 - 58
        fg.ColKey(i++) = 'PrazoPagamento*';                                                                             //33 - 61
        fg.ColKey(i++) = 'PrazoReposicao*';                                                                             //34 - 62

        // Colunas Estoque
        fg.ColKey(i++) = 'ReservaCompra*';                                                                              //35 - 66
        fg.ColKey(i++) = 'ReservaVendaConfirmada*';                                                                     //36 - 67
        fg.ColKey(i++) = 'PrevisaoEntrega*';                                                                            //37 - 68
        fg.ColKey(i++) = 'Estoque*';                                                                                    //38 - 19
        fg.ColKey(i++) = 'ComprarExcesso*';                                                                             //39 - 57

        // Colunas Movimenta��o de Produto
        fg.ColKey(i++) = 'dtMediaPagamento' + glb_Pagamento_ReadOnly;                                                   //40 - 15
        fg.ColKey(i++) = 'DiasPagamento' + glb_PagamentoDias_ReadOnly;                                                                              //41 - 16
        fg.ColKey(i++) = 'dtUltimaEntrada*';                                                                            //42 - 17
        fg.ColKey(i++) = 'dtUltimaSaida*';                                                                              //43 - 18

        // Colunas Vendas
        fg.ColKey(i++) = 'VendasMes3*';                                                                                 //44 - 23
        fg.ColKey(i++) = 'VendasMes2*';                                                                                 //45 - 24
        fg.ColKey(i++) = 'VendasMes1*';                                                                                 //46 - 25
        fg.ColKey(i++) = 'VendasMes0*';                                                                                 //47 - 26
        fg.ColKey(i++) = 'MediaVendas13*';                                                                              //48 - 27

        // Colunas Suprimento
        fg.ColKey(i++) = 'PrevisaoVendas' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PV_ReadOnly);                   //49 - 28
        fg.ColKey(i++) = 'PrazoEstoque' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PE_ReadOnly);                     //50 - 52
        fg.ColKey(i++) = 'MultiploVenda' + glb_sReadOnly;                                                               //51 - 50
        fg.ColKey(i++) = 'MultiploCompra' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_MultiploCompra_ReadOnly);       //52 - 51

        // Colunas Publica��es Internet
        fg.ColKey(i++) = 'Publica' + glb_sReadOnly;                                                                     //53 - 72
        fg.ColKey(i++) = 'PublicaPreco' + glb_sReadOnly;                                                                //54 - 73
        fg.ColKey(i++) = 'PercentualPublicacaoEstoque' + glb_sReadOnly;                                                 //55 - 74
        fg.ColKey(i++) = 'EstoqueInternet*';                                                                            //56 - 75
        fg.ColKey(i++) = 'PrazoTroca' + glb_sReadOnly;                                                                  //57 - 76
        fg.ColKey(i++) = 'PrazoGarantia' + glb_sReadOnly;                                                               //58 - 77
        fg.ColKey(i++) = 'PrazoAsstec' + glb_sReadOnly;                                                                 //59 - 78

        // Colunas Projeto
        fg.ColKey(i++) = 'LoteID*';                                                                                     //60 - 04
        fg.ColKey(i++) = 'Lote*';                                                                                       //61 - 05
        fg.ColKey(i++) = 'Aplicar';                                                                                     //62 - 06
        fg.ColKey(i++) = 'Projeto';                                                                                     //63 - 59

        // Colunas comuns finais
        fg.ColKey(i++) = 'PartNumber*';                                                                                 //64 - 60
        fg.ColKey(i++) = 'NCM' + (glb_sReadOnlyClassFiscal == '*' ? glb_sReadOnlyClassFiscal : glb_NCM_ReadOnly);       //65 - 53
        fg.ColKey(i++) = 'Observacao' + glb_sReadOnly;                                                                  //66 - 79

        // Walking Dead
        fg.ColKey(i++) = 'DiasUltimaSaida*';                                                                            //67 - 22
        fg.ColKey(i++) = 'PlanejamentoVendasTotal*';                                                                    //68 - 34
        fg.ColKey(i++) = 'Variacao*';                                                                                   //69 - 35
        fg.ColKey(i++) = 'Custo_Total*';                                                                                //70 - 21
        fg.ColKey(i++) = 'CustoMedio*';                                                                                 //71 - 38
        fg.ColKey(i++) = 'MargemContribuicao_2*';                                                                       //72 - 33
        fg.ColKey(i++) = 'MMInternet';                                                                                  //73 - 70
        fg.ColKey(i++) = 'Preco*';                                                                                      //74 - 48
        fg.ColKey(i++) = 'Estoque*';                                                                                    //75 - 65
        fg.ColKey(i++) = 'Estoque_2*';                                                                                  //76 - 36
        fg.ColKey(i++) = 'ComprarExcesso_2*';                                                                           //77 - 37
        fg.ColKey(i++) = 'PrevisaoVendas_2*';                                                                           //78 - 64
        fg.ColKey(i++) = 'PrazoEstoque_2*';                                                                             //79 - 63

        // Colunas escondidas
        fg.ColKey(i++) = 'RelacaoID*';                                                                                  //80 - 80
        fg.ColKey(i++) = 'abrevCor';                                                                                    //81 - 81

        fg.ColKey(i++) = 'Est_Auditoria';                                                                               //82 - 82
        fg.ColKey(i++) = 'Int_Auditoria';                                                                               //83 - 83
        fg.ColKey(i++) = 'Pag_Auditoria1';                                                                               //84 - 84
        fg.ColKey(i++) = 'MC_Auditoria';                                                                                //85 - 85
        fg.ColKey(i++) = 'PV_Auditoria';                                                                                //86 - 86
        fg.ColKey(i++) = 'CVPInicio_Auditoria';                                                                         //87 - 87
        fg.ColKey(i++) = 'CVPFim_Auditoria';                                                                            //88 - 88
        fg.ColKey(i++) = 'CVPControle_Auditoria';                                                                       //89 - 89
        fg.ColKey(i++) = 'PE_Auditoria';                                                                                //90
        fg.ColKey(i++) = 'MV_Auditoria';                                                                                //91
        fg.ColKey(i++) = 'MuC_Auditoria';                                                                               //92
        fg.ColKey(i++) = 'MPub_Auditoria';                                                                              //93
        fg.ColKey(i++) = 'PP_Auditoria';                                                                                //94
        fg.ColKey(i++) = 'Pag_Auditoria2';                                                                              //95

        showHideCols();

        if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
            dsoGrid.recordset.MoveFirst();

        while (!dsoGrid.recordset.EOF) {
            fg.Rows = fg.Rows + 1;
            i = 0;

            // Acrescenta o ID do produto no dso dos argumentos de venda.
            if (dsoGrid.recordset['ObjetoID'].value != null) {
                argumentosVendaDoConceito += dsoGrid.recordset['ObjetoID'].value;
            }

            // Colunas comuns iniciais
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['ObjetoID'].value == null ? '' : dsoGrid.recordset['ObjetoID'].value);                                                                                                      //00 - 00
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Estado'].value == null ? '' : dsoGrid.recordset['Estado'].value);                                                                                                          //01 - 01
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Produto'].value == null ? '' : dsoGrid.recordset['Produto'].value);                                                                                                        //02 - 02
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Marca'].value == null ? '' : dsoGrid.recordset['Marca'].value);                                                                                                            //03 - 03
            fg.TextMatrix(fg.Rows - 1, i++) = 0;  // OK                                                                                                                                                                                      //04 - 07
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Proprietario'].value == null ? '' : dsoGrid.recordset['Proprietario'].value);                                                                                              //05 - 08
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Alternativo'].value == null ? '' : dsoGrid.recordset['Alternativo'].value);                                                                                                //06 - 09

            // Colunas CVP
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['dtInicio'].value == null ? '' : dsoGrid.recordset['dtInicio'].asDatetime(dTFormat));                                                                                       //07 - 10
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['dtFim'].value == null ? '' : dsoGrid.recordset['dtFim'].asDatetime(dTFormat));                                                                                             //08 - 11
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['DiasFim'].value == null ? '' : dsoGrid.recordset['DiasFim'].value);                                                                                                        //09 - 12
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['dtControle'].value == null ? '' : dsoGrid.recordset['dtControle'].asDatetime(dTFormat));                                                                                   //10 - 13
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['DiasControle'].value == null ? '' : dsoGrid.recordset['DiasControle'].value);                                                                                              //11 - 14

            // Colunas Custo
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['QuantidadeComprar'].value == null ? '' : dsoGrid.recordset['QuantidadeComprar'].value);                                                                                    //12 - 54
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CustoComprar'].value == null ? '' : dsoGrid.recordset['CustoComprar'].value);                                                                                              //13 - 55
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CustoComprar'].value == null ? '' : (dsoGrid.recordset['CustoComprar'].value * dsoGrid.recordset['QuantidadeComprar'].value));                                   //14 - 56
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CustoMedio'].value == null ? '' : dsoGrid.recordset['CustoMedio'].value);                                                                                                  //15 - 20

            // Colunas Margem Contribui��o
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MargemPadrao'].value == null ? '' : dsoGrid.recordset['MargemPadrao'].value);                                                                                              //16 - 40
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MargemContribuicao'].value == null ? '' : dsoGrid.recordset['MargemContribuicao'].value);                                                                                  //17 - 41
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MargemReal'].value == null ? '' : dsoGrid.recordset['MargemReal'].value);                                                                                                  //18 - 42
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MargemPublicacao'].value == null ? '' : dsoGrid.recordset['MargemPublicacao'].value);                                                                                      //19 - 69
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MargemMinima'].value == null ? '' : dsoGrid.recordset['MargemMinima'].value);                                                                                              //20 - 43
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MargemMedia'].value == null ? '' : dsoGrid.recordset['MargemMedia'].value);                                                                                                //21 - 44
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Contribuicao'].value == null ? '' : dsoGrid.recordset['Contribuicao'].value);                                                                                              //22 - 32
            fg.TextMatrix(fg.Rows - 1, i++) = ((dsoGrid.recordset['Contribuicao'].value * dsoGrid.recordset['PrevisaoVendas'].value) == null ? '' : (dsoGrid.recordset['Contribuicao'].value * dsoGrid.recordset['PrevisaoVendas'].value));                                                                                              //23 - 32


            // Colunas Pre�o
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrecoBase'].value == null ? '' : dsoGrid.recordset['PrecoBase'].value);                                                                                                    //24 - 45
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrecoBaseMinimo'].value == null ? '' : dsoGrid.recordset['PrecoBaseMinimo'].value);                                                                                        //25 - 46
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrecoBaseMedio'].value == null ? '' : dsoGrid.recordset['PrecoBaseMedio'].value);                                                                                          //26 - 47
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrecoMinimo'].value == null ? '' : dsoGrid.recordset['PrecoMinimo'].value);                                                                                                //27 - 29
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Preco'].value == null ? '' : dsoGrid.recordset['Preco'].value);                                                                                                            //28 - 29
            fg.TextMatrix(fg.Rows - 1, i++) = ((dsoGrid.recordset['Preco'].value * dsoGrid.recordset['PrevisaoVendas'].value) == null ? '' : (dsoGrid.recordset['Preco'].value * dsoGrid.recordset['PrevisaoVendas'].value));                //29 - 30
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrecoInternet'].value == null ? '' : dsoGrid.recordset['PrecoInternet'].value);                                                                                            //30 - 71
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrecoConcorrentes'].value == null ? '' : dsoGrid.recordset['PrecoConcorrentes'].value);                                                                                    //31 - 49

            // Colunas Dados do Fornecedor 1
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CustoReposicao1'].value == null ? '' : dsoGrid.recordset['CustoReposicao1'].value);                                                                                        //32 - 58
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrazoPagamento1'].value == null ? '' : dsoGrid.recordset['PrazoPagamento1'].value);                                                                                        //33 - 61
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrazoReposicao1'].value == null ? '' : dsoGrid.recordset['PrazoReposicao1'].value);                                                                                        //34 - 62

            // Colunas Estoque
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['ReservaCompra'].value == null ? '' : dsoGrid.recordset['ReservaCompra'].value);                                                                                            //35 - 66
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['ReservaVendaConfirmada'].value == null ? '' : dsoGrid.recordset['ReservaVendaConfirmada'].value);                                                                          //36 - 67
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrevisaoEntrega'].value == null ? '' : dsoGrid.recordset['PrevisaoEntrega'].value);                                                                                        //37 - 68
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Estoque'].value == null ? '' : dsoGrid.recordset['Estoque'].value);                                                                                                        //38 - 19
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['ComprarExcesso'].value == null ? '' : dsoGrid.recordset['ComprarExcesso'].value);                                                                                          //39 - 57

            // Colunas Movimenta��o de Produto
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['dtMediaPagamento'].value == null ? '' : dsoGrid.recordset['dtMediaPagamento'].asDatetime(dTFormat));                                                                       //40 - 15
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['DiasPagamento'].value == null ? '' : dsoGrid.recordset['DiasPagamento'].value);                                                                                            //41 - 16
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['dtUltimaEntrada'].value == null ? '' : dsoGrid.recordset['dtUltimaEntrada'].value);                                                                                        //42 - 17
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['dtUltimaSaida'].value == null ? '' : dsoGrid.recordset['dtUltimaSaida'].value);                                                                                            //43 - 18

            // Colunas Vendas
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['VendasMes3'].value == null ? '' : dsoGrid.recordset['VendasMes3'].value);                                                                                                  //44 - 23
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['VendasMes2'].value == null ? '' : dsoGrid.recordset['VendasMes2'].value);                                                                                                  //45 - 24
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['VendasMes1'].value == null ? '' : dsoGrid.recordset['VendasMes1'].value);                                                                                                  //46 - 25
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['VendasMes0'].value == null ? '' : dsoGrid.recordset['VendasMes0'].value);                                                                                                  //47 - 26
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MediaVendas13'].value == null ? '' : dsoGrid.recordset['MediaVendas13'].value);                                                                                            //48 - 27

            // Colunas Suprimento
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrevisaoVendas'].value == null ? '' : dsoGrid.recordset['PrevisaoVendas'].value);                                                                                          //49 - 28
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrazoEstoque'].value == null ? '' : dsoGrid.recordset['PrazoEstoque'].value);                                                                                              //50 - 52
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MultiploVenda'].value == null ? '' : dsoGrid.recordset['MultiploVenda'].value);                                                                                            //51 - 50
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MultiploCompra'].value == null ? '' : dsoGrid.recordset['MultiploCompra'].value);                                                                                          //52 - 51

            // Colunas Publica��es Internet
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Publica'].value == null ? '' : dsoGrid.recordset['Publica'].value);                                                                                                        //53 - 72
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PublicaPreco'].value == null ? '' : dsoGrid.recordset['PublicaPreco'].value);                                                                                              //54 - 73
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PercentualPublicacaoEstoque'].value == null ? '' : dsoGrid.recordset['PercentualPublicacaoEstoque'].value);                                                                //55 - 74
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['EstoqueInternet'].value == null ? '' : dsoGrid.recordset['EstoqueInternet'].value);                                                                                        //56 - 75
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrazoTroca'].value == null ? '' : dsoGrid.recordset['PrazoTroca'].value);                                                                                                  //57 - 76
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrazoGarantia'].value == null ? '' : dsoGrid.recordset['PrazoGarantia'].value);                                                                                            //58 - 77
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrazoAsstec'].value == null ? '' : dsoGrid.recordset['PrazoAsstec'].value);                                                                                                //59 - 78

            // Colunas Projeto
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['LoteID'].value == null ? '' : dsoGrid.recordset['LoteID'].value);                                                                                                          //60 - 04
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Lote'].value == null ? '' : dsoGrid.recordset['Lote'].value);                                                                                                              //61 - 05
            fg.TextMatrix(fg.Rows - 1, i++) = 0; //Aplicar                                                                                                                                                                                   //62 - 06
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Projeto'].value == null ? '' : dsoGrid.recordset['Projeto'].value);                                                                                                        //63 - 59

            // Colunas comuns finais
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PartNumber'].value == null ? '' : dsoGrid.recordset['PartNumber'].value);                                                                                                  //64 - 60
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['NCM'].value == null ? '' : dsoGrid.recordset['NCM'].value);                                                                                                                //65 - 53
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Observacao'].value == null ? '' : dsoGrid.recordset['Observacao'].value);                                                                                                  //66 - 79

            // Walking Dead
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['DiasUltimaSaida'].value == null ? '' : dsoGrid.recordset['DiasUltimaSaida'].value);                                                                                        //67 - 22
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PlanejamentoVendasTotal'].value == null ? '' : dsoGrid.recordset['PlanejamentoVendasTotal'].value);                                                                        //68 - 34
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Variacao'].value == null ? '' : dsoGrid.recordset['Variacao'].value);                                                                                                      //69 - 35
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Custo_Total'].value == null ? '' : dsoGrid.recordset['Custo_Total'].value);                                                                                                //70 - 21
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CustoMedio'].value * dsoGrid.recordset['Estoque'].value);                                                                                                                  //71 - 38
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MargemContribuicao'].value == null ? '' : dsoGrid.recordset['MargemContribuicao'].value);                                                                                  //72 - 33
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MMInternet'].value == null ? '' : dsoGrid.recordset['MMInternet'].value);                                                                                                  //73 - 70
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Preco'].value == null ? '' : dsoGrid.recordset['Preco'].value);                                                                                                            //74 - 48
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Estoque'].value == null ? '' : dsoGrid.recordset['Estoque'].value);                                                                                                        //75 - 65
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Estoque'].value == null ? '' : dsoGrid.recordset['Estoque'].value);                                                                                                        //76 - 36
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['ComprarExcesso'].value == null ? '' : dsoGrid.recordset['ComprarExcesso'].value);                                                                                          //77 - 37
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrevisaoVendas'].value == null ? '' : dsoGrid.recordset['PrevisaoVendas'].value);                                                                                          //78 - 64
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PrazoEstoque'].value == null ? '' : dsoGrid.recordset['PrazoEstoque'].value);                                                                                              //79 - 63

            // Colunas escondidas
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['RelacaoID'].value == null ? '' : dsoGrid.recordset['RelacaoID'].value);                                                                                                    //80 - 80
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Pag_Auditoria'].value == null ? '' : (dsoGrid.recordset['Pag_Auditoria'].value).substr(0, 1));                                                                             //81 - 81

            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Est_Auditoria'].value == null ? '' : dsoGrid.recordset['Est_Auditoria'].value);                                                                                           //82 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Int_Auditoria'].value == null ? '' : dsoGrid.recordset['Int_Auditoria'].value);                                                                                           //83 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Pag_Auditoria1'].value == null ? '' : dsoGrid.recordset['Pag_Auditoria1'].value);                                                                                           //84 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MC_Auditoria'].value == null ? '' : dsoGrid.recordset['MC_Auditoria'].value);                                                                                             //85 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PV_Auditoria'].value == null ? '' : dsoGrid.recordset['PV_Auditoria'].value);                                                                                             //86 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CVPInicio_Auditoria'].value == null ? '' : dsoGrid.recordset['CVPInicio_Auditoria'].value);                                                                               //87 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CVPFim_Auditoria'].value == null ? '' : dsoGrid.recordset['CVPFim_Auditoria'].value);                                                                                     //88 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CVPControle_Auditoria'].value == null ? '' : dsoGrid.recordset['CVPControle_Auditoria'].value);                                                                           //89 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PE_Auditoria'].value == null ? '' : dsoGrid.recordset['PE_Auditoria'].value);                                                                                             //90
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MV_Auditoria'].value == null ? '' : dsoGrid.recordset['MV_Auditoria'].value);                                                                                             //91 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MuC_Auditoria'].value == null ? '' : dsoGrid.recordset['MuC_Auditoria'].value);                                                                                           //92 
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['MPub_Auditoria'].value == null ? '' : dsoGrid.recordset['MPub_Auditoria'].value);                                                                                         //93
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PP_Auditoria'].value == null ? '' : dsoGrid.recordset['PP_Auditoria'].value);                                                                                             //94
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Pag_Auditoria2'].value == null ? '' : dsoGrid.recordset['Pag_Auditoria2'].value);                                                                                         //95 
            

            dsoGrid.recordset.moveNext();

            // Coloca a v�rgula para separar os campos.
            if (!dsoGrid.recordset.EOF) {
                argumentosVendaDoConceito += ",";
            }
        }

        // Fecha a consulta dos argumentos de venda.
        argumentosVendaDoConceito += ")";

        paintReadOnlyCols(fg);

        bgColorGreen = 0X90EE90;
        bgColorYellow = 0X8CE6F0;
        bgColorOrange = 0X60A4F4;
        bgColorRed = 0X7280FA;
        bgColorPurple = 0XFF8080;
        bgColorBlack = 00000000;

        if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
            dsoGrid.recordset.MoveFirst();

        i = 0;
        while (!dsoGrid.recordset.EOF) {
            fg.FillStyle = 1;

            if (dsoGrid.recordset['Est_Auditoria'].value != null)
                fg.Cell(6, i + 1, getColIndexByColKey(fg, 'Estado*'), i + 1, getColIndexByColKey(fg, 'Estado*')) = (dsoGrid.recordset['Est_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['Est_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['Est_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['Est_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['Est_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));

            if (dsoGrid.recordset['Int_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'Publica' + glb_sReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['Int_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['Int_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['Int_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['Int_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['Int_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['CVPInicio_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'dtInicio' + glb_sReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['CVPInicio_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['CVPInicio_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['CVPInicio_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['CVPInicio_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['CVPInicio_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['CVPFim_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'dtFim' + ((glb_sReadOnly == '*') ? glb_sReadOnly : glb_Fim_ReadOnly));
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));

                sPaintFiedNumber = getColIndexByColKey(fg, 'DiasFim*');
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['CVPFim_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['CVPControle_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'dtControle' + glb_sReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));

                sPaintFiedNumber = getColIndexByColKey(fg, 'DiasControle*');
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['CVPControle_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['PV_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'PrevisaoVendas' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PV_ReadOnly));
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['PV_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['PV_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['PV_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['PV_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['PV_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['Pag_Auditoria1'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'dtMediaPagamento' + glb_Pagamento_ReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['Pag_Auditoria1'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'DiasPagamento' + glb_PagamentoDias_ReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['Pag_Auditoria1'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['MC_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'MargemContribuicao' + glb_sReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['MC_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['MC_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['MC_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['MC_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['MC_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['MPub_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'MargemPublicacao' + glb_sReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['MPub_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['MPub_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['MPub_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['MPub_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['MPub_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['PP_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'PublicaPreco' + glb_sReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['PP_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['PP_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['PP_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['PP_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['PP_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            // Comecou aqui
            if (dsoGrid.recordset['PE_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'PrazoEstoque' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PE_ReadOnly));
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['PE_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['PE_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['PE_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['PE_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['PE_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['MV_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'MultiploVenda' + glb_sReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['MV_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['MV_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['MV_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['MV_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['MV_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }

            if (dsoGrid.recordset['MuC_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'MultiploCompra' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_MultiploCompra_ReadOnly));
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['MuC_Auditoria'].value.substring(0, 1) == '1' ? bgColorGreen : (dsoGrid.recordset['MuC_Auditoria'].value.substring(0, 1) == '2' ? bgColorYellow :
                    (dsoGrid.recordset['MuC_Auditoria'].value.substring(0, 1) == '3' ? bgColorOrange : (dsoGrid.recordset['MuC_Auditoria'].value.substring(0, 1) == '4' ? bgColorRed : (dsoGrid.recordset['MuC_Auditoria'].value.substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
            }
            // Acabou aqui

            /*
            if (dsoGrid.recordset['MPub_Auditoria'].value != null) {
                sPaintFiedNumber = getColIndexByColKey(fg, 'MargemPublicacao' + glb_sReadOnly);
                fg.Cell(6, i + 1, sPaintFiedNumber, i + 1, sPaintFiedNumber) = (dsoGrid.recordset['MPub_Auditoria'].value == '1' ? bgColorGreen : (dsoGrid.recordset['MPub_Auditoria'].value == '2' ? bgColorYellow :
                    (dsoGrid.recordset['MPub_Auditoria'].value == '3' ? bgColorOrange : (dsoGrid.recordset['MPub_Auditoria'].value == '4' ? bgColorRed : (dsoGrid.recordset['MPub_Auditoria'].value == '5' ? bgColorPurple : bgColorBlack)))));
            }
            */
            dsoGrid.recordset.moveNext();
            i++;
        }

        fg.ColDataType(getColIndexByColKey(fg, 'ChkOK')) = 11; // format boolean (checkbox)
        fg.ColDataType(getColIndexByColKey(fg, 'Publica')) = 11; // format boolean (checkbox)
        fg.ColDataType(getColIndexByColKey(fg, 'PublicaPreco')) = 11; // format boolean (checkbox)
        fg.ColDataType(getColIndexByColKey(fg, 'Aplicar')) = 11; // format boolean (checkbox)

        putMasksInGrid(fg, aMaskReadOnly, aMaskEdit);

        alignColsInGrid(fg, aAlignCols);
        Totaliza();

        //Se o Usuario tem direito no sup do PesCon pode alterar NCM
        if (glb_sReadOnlyClassFiscal == '')
            // insertcomboData(fg, 45, dsoClassFiscal, 'Conceito', 'ConceitoID');
            insertcomboData(fg, getColIndexByColKey(fg, 'NCM' + (glb_sReadOnlyClassFiscal == '*' ? glb_sReadOnlyClassFiscal : glb_NCM_ReadOnly)), dsoClassFiscal, 'Conceito', 'ConceitoID');

        if (selServico.value == GERENCIA_DE_ESTOQUE) {
            fg.Col = getColIndexByColKey(fg, 'QuantidadeComprar');
            fg.Sort = 2;
        }
        else {
            fg.Select(2, getColIndexByColKey(fg, 'Produto*'));
            fg.Sort = 1;
        }

        fg.Col = FocusCol;
        fg.Redraw = 2;

        fg.ExplorerBar = 5;

        // se nenhuma linha foi selecionada, seleciona a primeira
        // se a mesma existe
        if (fg.Rows > 2)
            fg.Row = 2;

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
        fg.Redraw = 2;

        fg_modalgerenciamentoprodutosAfterRowColChange();

        if (fg.Rows > 2)
            fg.Editable = true;

        lockControlsInModalWin(false);

        fg.ColWidth(getColIndexByColKey(fg, 'QuantidadeComprar')) = fg.ColWidth(getColIndexByColKey(fg, 'QuantidadeComprar'));
        fg.ColWidth(getColIndexByColKey(fg, 'CustoComprar')) = fg.ColWidth(getColIndexByColKey(fg, 'CustoComprar')) * 1.2;
        fg.ColWidth(getColIndexByColKey(fg, 'Projeto')) = fg.ColWidth(getColIndexByColKey(fg, 'Projeto'));
        fg.ColWidth(getColIndexByColKey(fg, 'PartNumber*')) = fg.ColWidth(getColIndexByColKey(fg, 'PartNumber*'));
        btnImprimir.disabled = ((fg.rows > 1) ? false : true);
        btnExcel.disabled = ((fg.rows > 1) ? false : true);

        // se tem linhas no grid, coloca foco no grid
        if (fg.Rows > 2) {
            window.focus();
            fg.focus();
            fg.Redraw = 2;
            fg.Col = 0;
            fg.Col = 9;
            fg.TopRow = fg.Rows - 1;
            fg.TopRow = 1;
        }

        // ajusta estado dos botoes
        setupBtnsFromGridState();

        // Controla se o grid esta em construcao
        glb_GridIsBuilding = false;
    }
    else {
        var aLabels, aMaskReadOnly, aMaskEdit, aHiddenCols, aAlignCols, FocusCol;

        aLabels = [
            // Dados comuns inicio
            'ID',
            'E',
            'Produto',
            'Marca',

            //Campos Lote
            'LoteID',
            'Lote',
            'Aplicar',

            //Campos Lote
            'OK',
	        'GP',
	        'Esp',

	        // Pedidos
            'Qtd Compra',
            'Custo', // 10
            'Projeto',
            'PartNumber',
            'Custo Total',
            'Qtd Venda',
            'Unitario',  // 13
            'Valor Total',
            'Pedido',
            'E',
            'Cliente',
            'Revenda',
            'Vendedor',
            'Transa��o',
            'Observa��o', // 20
            'PedItemID',
            'ProjetoLoteID',
            'Internacional'];

        aMaskReadOnly = [
        // Dados comuns
        '9999999999',  // 'ProdutoID'
        '',  // 'E'
        '',  // 'Produto'
        '',  // 'Marca'

        //Campos Lotes
        '',  // 'LoteID'
        '',  // 'Lote'
        '',  // 'Aplicar'

        //Campos Lotes
        '',  // 'OK'
        '',  // 'GP'
        '',  // 'Esp'

        // Pedidos
        '99999',  // 'Quantidade'
        '999999999.99',  // 'Custo R$'
        '', //Projeto
        '', //PartNumber
        '999999999.99',  // 'Custo Total R$'
        '99999',  // 'Quantidade'
        '999999999.99',  // 'Unitario'
        '999999999.99',  // 'Valor Total'
        '9999999999',  // 'Pedido'
        '',  // 'E'
        '',  // 'Cliente'
        '', //Revenda
        '',  // 'Vendedor'
        '999',  // 'Transa��o'
        '', // 'Observa��o'
        '9999999999',   // 'PedItemID'
        '',  // ProjetoLoteID
        ''];  // Internacional

        aMaskEdit = [
            // Dados comuns
            '##########',  // 'ProdutoID'
            '',  // 'E'
            '',  // 'Produto'
            '',  // 'Marca'
            //Campos Lotes
            '',  // 'LoteID'
            '',  // 'Lote'
            '',  // 'Aplicar'
            //Campos Lotes
            '',  // 'OK'
	        '',  // 'GP'
	        '',  // 'Esp'

	        // Pedidos
            '#####',  // 'Quantidade'
            '###,###,##0.00',  // 'Custo R$'
            '', //Projeto
            '', //PartNumber
            '###,###,##0.00',  // 'Custo Total R$'
            '#####',  // 'Quantidade'
            '###,###,##0.00',  // 'Unitario'
            '###,###,##0.00',  // 'Valor Total'
            '##########',  // 'Pedido'
            '',  // 'E'
            '',  // 'Cliente'
            '', //Revenda
            '',  // 'Vendedor'
            '###',  // 'Transa��o'
            '',  // 'Observa��o'
            '##########',     // 'PedItemID'
            '', // ProjetoLoteID
            '']; // Internacional

        aAlignCols = [10, 11, 14, 15, 16, 17];
        aHiddenCols = [22, 26, 27];

        headerGrid(fg, aLabels, aHiddenCols);

        var i = 0;
        fg.Redraw = 0;
        fg.FrozenCols = 8;

        // Dados comuns inicio
        fg.ColKey(i++) = 'ProdutoID*';
        fg.ColKey(i++) = 'Estado*';
        fg.ColKey(i++) = 'Produto*';
        fg.ColKey(i++) = 'Marca*';
        //Campos Lotes
        fg.ColKey(i++) = 'LoteID*';
        fg.ColKey(i++) = 'Lote*';
        fg.ColKey(i++) = 'Aplicar';
        //Campos Lotes
        fg.ColKey(i++) = 'ChkOK';
        fg.ColKey(i++) = 'Proprietario*';
        fg.ColKey(i++) = 'Alternativo*';

        // Pedidos
        fg.ColKey(i++) = 'QuantidadeComprar';
        fg.ColKey(i++) = 'CustoComprar';
        fg.ColKey(i++) = 'Projeto';
        fg.ColKey(i++) = 'PartNumber*';
        fg.ColKey(i++) = 'CustoTotalComprar*';
        fg.ColKey(i++) = 'QuantidadeVenda*';
        fg.ColKey(i++) = 'Unitario*';  // 10
        fg.ColKey(i++) = 'Valor_Total*';
        fg.ColKey(i++) = 'PedidoID*';
        fg.ColKey(i++) = 'Estado*';
        fg.ColKey(i++) = 'Cliente*';
        fg.ColKey(i++) = 'Revenda*';
        fg.ColKey(i++) = 'Vendedor*';
        fg.ColKey(i++) = 'Transacao*';
        fg.ColKey(i++) = 'Observacao*';
        fg.ColKey(i++) = 'PedItemID*';
        fg.ColKey(i++) = 'ProjetoLoteID*';
        fg.ColKey(i++) = 'Internacional*';

        while (!dsoGrid.recordset.EOF) {
            // Acrescenta o ID do produto no dso dos argumentos de venda.
            if (dsoGrid.recordset['ProdutoID'].value != null) {
                argumentosVendaDoConceito += dsoGrid.recordset['ProdutoID'].value;
            }

            fg.Rows = fg.Rows + 1;
            i = 0;

            // Dados comuns inicio
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['ProdutoID'].value == null ? '' : dsoGrid.recordset['ProdutoID'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Estado'].value == null ? '' : dsoGrid.recordset['Estado'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Produto'].value == null ? '' : dsoGrid.recordset['Produto'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Marca'].value == null ? '' : dsoGrid.recordset['Marca'].value);
            //Campos Lote
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['LoteID'].value == null ? '' : dsoGrid.recordset['LoteID'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Lote'].value == null ? '' : dsoGrid.recordset['Lote'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = 0; // Aplicar
            //Campos Lote
            fg.TextMatrix(fg.Rows - 1, i++) = 0;  // OK
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Proprietario'].value == null ? '' : dsoGrid.recordset['Proprietario'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Alternativo'].value == null ? '' : dsoGrid.recordset['Alternativo'].value);

            // Pedidos
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Quantidade'].value == null ? '' : dsoGrid.recordset['Quantidade'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CustoComprar'].value == null ? '' : dsoGrid.recordset['CustoComprar'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Projeto'].value == null ? '' : dsoGrid.recordset['Projeto'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PartNumber'].value == null ? '' : dsoGrid.recordset['PartNumber'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['CustoComprar'].value * dsoGrid.recordset['Quantidade'].value); //Custo_Total
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Quantidade'].value == null ? '' : dsoGrid.recordset['Quantidade'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Unitario'].value == null ? '' : dsoGrid.recordset['Unitario'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Unitario'].value * dsoGrid.recordset['Quantidade'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PedidoID'].value == null ? '' : dsoGrid.recordset['PedidoID'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Estado2'].value == null ? '' : dsoGrid.recordset['Estado2'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Cliente'].value == null ? '' : dsoGrid.recordset['Cliente'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Revenda'].value == null ? '' : dsoGrid.recordset['Revenda'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Vendedor'].value == null ? '' : dsoGrid.recordset['Vendedor'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Transacao'].value == null ? '' : dsoGrid.recordset['Transacao'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Observacao'].value == null ? '' : dsoGrid.recordset['Observacao'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['PedItemID'].value == null ? '' : dsoGrid.recordset['PedItemID'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['ProjetoLoteID'].value == null ? '' : dsoGrid.recordset['ProjetoLoteID'].value);
            fg.TextMatrix(fg.Rows - 1, i++) = (dsoGrid.recordset['Internacional'].value == null ? '' : dsoGrid.recordset['Internacional'].value);


            dsoGrid.recordset.moveNext();

            //SAS0081 - Pedidos Open Micrososft
            if ((chkEnc.checked) && (selFornecedor.value != 10003)) {
                fg.ColHidden(getColIndexByColKey(fg, 'Projeto')) = true;
                fg.ColHidden(getColIndexByColKey(fg, 'PartNumber*')) = true;
            }

            // Coloca a v�rgula para separar os campos.
            if (!dsoGrid.recordset.EOF) {
                argumentosVendaDoConceito += ",";
            }
        }

        // Fecha a consulta dos argumentos de venda.
        argumentosVendaDoConceito += ")";

        paintReadOnlyCols(fg);

        bgColorGreen = 0X90EE90;
        bgColorYellow = 0X8CE6F0;
        bgColorOrange = 0X60A4F4;
        bgColorRed = 0X7280FA;
        bgColorPurple = 0XFF8080;

        if (!(dsoGrid.recordset.BOF && dsoGrid.recordset.EOF))
            dsoGrid.recordset.MoveFirst();

        fg.MergeCells = 4;
        fg.MergeCol(0) = true;

        fg.ColDataType(getColIndexByColKey(fg, 'ChkOK')) = 11; // format boolean (checkbox)
        fg.ColDataType(getColIndexByColKey(fg, 'Aplicar')) = 11; // format boolean (checkbox)

        putMasksInGrid(fg, aMaskReadOnly, aMaskEdit);
        alignColsInGrid(fg, aAlignCols);
        Totaliza();


        fg.Col = 9;
        fg.Redraw = 2;
        fg.ExplorerBar = 5;

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
        fg.Redraw = 0;

        fg_modalgerenciamentoprodutosAfterRowColChange();
        if (fg.Rows > 2)
            fg.Editable = true;

        lockControlsInModalWin(false);

        fg.ColWidth(getColIndexByColKey(fg, 'QuantidadeComprar')) = fg.ColWidth(getColIndexByColKey(fg, 'QuantidadeComprar'));
        fg.ColWidth(getColIndexByColKey(fg, 'CustoComprar')) = fg.ColWidth(getColIndexByColKey(fg, 'CustoComprar')) * 1.2;
        fg.ColWidth(getColIndexByColKey(fg, 'Projeto')) = fg.ColWidth(getColIndexByColKey(fg, 'Projeto'));
        fg.ColWidth(getColIndexByColKey(fg, 'PartNumber*')) = fg.ColWidth(getColIndexByColKey(fg, 'PartNumber*'));

        btnImprimir.disabled = ((fg.rows > 1) ? false : true);
        btnExcel.disabled = ((fg.rows > 1) ? false : true);

        //se tem linhas no grid, coloca foco no grid
        if (fg.Rows > 2) {
            window.focus();
            fg.focus();
            fg.Redraw = 2;
            fg.Col = 0;
            fg.Col = 9;
            fg.TopRow = fg.Rows - 1;
            fg.TopRow = 1;
        }

        // ajusta estado dos botoes
        setupBtnsFromGridState();

        // Controla se o grid esta em construcao
        glb_GridIsBuilding = false;

    }

    setConnection(dsoArgumentoVenda);

    if (selFiltroArgumentosVenda.value != AV_SEM)
        dsoArgumentoVenda.SQL = argumentosVendaDoConceito;
    else
        dsoArgumentoVenda.SQL = "select null as ConArgumentoID,null as ConceitoID,null as IdiomaID,null as ArgumentosVenda,null as OK";

    dsoArgumentoVenda.ondatasetcomplete = dsoArgumentoVenda_DSC;
    dsoArgumentoVenda.refresh();

    if ((!glb_GridIsBuilding) && (!chkEnc.checked)) {
        campInferior();
    }
}
function campInferior() {
    if (fg.Row > 1) {
        txtCustoMedio.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CustoMedio*'));
        txtCustoReposicao.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'CustoReposicao*'));
        txtUltimaEntrada.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtUltimaEntrada*'));
        txtUltimaSaida.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtUltimaSaida*'));
        txtDias.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DiasUltimaSaida*'));
        txtDisponibilidade.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'PrevisaoEntrega*'));
        txtEstoqueInternet.value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EstoqueInternet*'));
    }
}

function dsoArgumentoVenda_DSC() {
    consultaArgumentos();

    // Calcula os totalizadores.
    setConnection(dsoArgumentoVendaTotais);
    dsoArgumentoVendaTotais.SQL = selectArgumentosVendaTotalizador();
    dsoArgumentoVendaTotais.ondatasetcomplete = dsoArgumentoVendaTotais_DSC;
    dsoArgumentoVendaTotais.refresh();
}

function dsoArgumentoVendaTotais_DSC() {
    // Declara e inicializa as vari�veis.
    var TotalProduto;
    var PercentArgVenda;
    var PercentArgVendaOK;


    // Obt�m os valores calculados.
    if (!dsoArgumentoVendaTotais.recordset.EOF && !dsoArgumentoVendaTotais.recordset.BOF) {
        dsoArgumentoVendaTotais.recordset.MoveFirst();

        TotalProduto = dsoArgumentoVendaTotais.recordset["TotalProduto"].value;
        PercentArgVenda = dsoArgumentoVendaTotais.recordset["PercentArgVenda"].value;
        PercentArgVendaOK = dsoArgumentoVendaTotais.recordset["PercentArgVendaOK"].value;
    }

    // Transforma os valores em percentuais.
    var pow10 = Math.pow(10, 1);
    PercentArgVenda = Math.round(PercentArgVenda * pow10) / pow10;
    PercentArgVendaOK = Math.round(PercentArgVendaOK * pow10) / pow10;

    // Monta o label.
    lblQuantOK.innerText = TotalProduto + " produtos, " +
		Math.round(PercentArgVenda) + "% com argumentos de venda, " +
		Math.round(PercentArgVendaOK) + "% com argumentos de venda OK";

    lblQuantOK.innerHTML = lblQuantOK.innerText;
}

function dsoDireitosB3_DSC() {
    var nB3A1;
    nB3A1 = dsoDireitosB3.recordset["Alterar1"].value;

    if (nB3A1 == 0)
        chkOK.disabled = true;
}

function showHideCols() {
    // Campos do PV

    if (!chkEnc.checked) {

        // Servico 1 Ciclo de vida
        if (selServico.value == CICLO_DE_VIDA) {
            fg.ColHidden(getColIndexByColKey(fg, 'dtUltimaEntrada*')) = !chkColunas.checked;                                                                            //42 - 'UltEntrada'
            fg.ColHidden(getColIndexByColKey(fg, 'dtUltimaSaida*')) = !chkColunas.checked;                                                                              //43 - 'UltSa�da'
            fg.ColHidden(getColIndexByColKey(fg, 'VendasMes3*')) = !chkColunas.checked;                                                                                 //44 - VM3
            fg.ColHidden(getColIndexByColKey(fg, 'VendasMes2*')) = !chkColunas.checked;                                                                                 //45 - VM2
            fg.ColHidden(getColIndexByColKey(fg, 'VendasMes1*')) = !chkColunas.checked;                                                                                 //46 - VM1
            fg.ColHidden(getColIndexByColKey(fg, 'VendasMes0*')) = !chkColunas.checked;                                                                                 //47 - VM0
            fg.ColHidden(getColIndexByColKey(fg, 'MediaVendas13*')) = !chkColunas.checked;                                                                              //48 - MV13
            fg.ColHidden(getColIndexByColKey(fg, 'PartNumber*')) = !chkColunas.checked;                                                                                 //64 - PartNumber
        }

            // Servico 2 Previsao de vendas
        else if (selServico.value == PREVEVISAO_DE_VENDAS) {
            fg.ColHidden(getColIndexByColKey(fg, 'MargemContribuicao' + glb_sReadOnly)) = !chkColunas.checked;                                                          //17		'MC'
            fg.ColHidden(getColIndexByColKey(fg, 'Contribuicao*')) = !chkColunas.checked;                                                                               //22		'Contribui��o'
            fg.ColHidden(getColIndexByColKey(fg, 'ContribuicaoTotal*')) = !chkColunas.checked;                                                                          //23		'Contribui��oTotal'
            fg.ColHidden(getColIndexByColKey(fg, 'Preco_2*')) = !chkColunas.checked;                                                                                    //28		'Pre�o'
            fg.ColHidden(getColIndexByColKey(fg, 'PrecoTotal_2*')) = !chkColunas.checked;                                                                               //29		'Pre�oTotal'
            fg.ColHidden(getColIndexByColKey(fg, 'ComprarExcesso*')) = !chkColunas.checked;                                                                             //39		'Co/Ex'
            fg.ColHidden(getColIndexByColKey(fg, 'VendasMes3*')) = !chkColunas.checked;                                                                                 //44		'VM3'
            fg.ColHidden(getColIndexByColKey(fg, 'VendasMes2*')) = !chkColunas.checked;                                                                                 //45		'VM2'
            fg.ColHidden(getColIndexByColKey(fg, 'VendasMes1*')) = !chkColunas.checked;                                                                                 //46		'VM1'
            fg.ColHidden(getColIndexByColKey(fg, 'VendasMes0*')) = !chkColunas.checked;                                                                                 //47		'VM0'
            fg.ColHidden(getColIndexByColKey(fg, 'PartNumber*')) = !chkColunas.checked;                                                                                 //64		'PartNumber'
        }

            // Servico 3 Formacao de precos
        else if (selServico.value == FORMACAO_DE_PRECO) {
            fg.ColHidden(getColIndexByColKey(fg, 'MargemReal*')) = !chkColunas.checked;                                                                                 //18		'MR'
            fg.ColHidden(getColIndexByColKey(fg, 'MargemMedia*')) = !chkColunas.checked;                                                                                //21		'MMed'
            fg.ColHidden(getColIndexByColKey(fg, 'PrecoBase')) = !chkColunas.checked;                                                                                   //24		'PrecoBase'
            fg.ColHidden(getColIndexByColKey(fg, 'PrecoBaseMinimo' + ((glb_sReadOnly == '*') ? glb_sReadOnly : glb_PrecoBaseMin_ReadOnly))) = !chkColunas.checked;      //25		'PrecoBaseMin'
            fg.ColHidden(getColIndexByColKey(fg, 'PrecoBaseMedio*')) = !chkColunas.checked;                                                                             //26		'PrecoBaseMed'
            fg.ColHidden(getColIndexByColKey(fg, 'PrecoMinimo')) = !chkColunas.checked;                                                                                 //27		'Preco Minimo'
            fg.ColHidden(getColIndexByColKey(fg, 'PartNumber*')) = !chkColunas.checked;                                                                                 //64		'PartNumber'
            fg.ColHidden(getColIndexByColKey(fg, 'NCM' + (glb_sReadOnlyClassFiscal == '*' ? glb_sReadOnlyClassFiscal : glb_NCM_ReadOnly))) = !chkColunas.checked;       //65		'NCM'
        }

            // Servico 5 Gerenciamento de estoque
        else if (selServico.value == GERENCIA_DE_ESTOQUE) {
            fg.ColHidden(getColIndexByColKey(fg, 'PrazoPagamento*')) = !chkColunas.checked;                                                                             //33		'PP1'
            fg.ColHidden(getColIndexByColKey(fg, 'PrazoReposicao*')) = !chkColunas.checked;                                                                             //34		'PR1'
            fg.ColHidden(getColIndexByColKey(fg, 'ReservaCompra*')) = !chkColunas.checked;                                                                              //35		'RC'
            fg.ColHidden(getColIndexByColKey(fg, 'ReservaVendaConfirmada*')) = !chkColunas.checked;                                                                     //36		'RVC'
            fg.ColHidden(getColIndexByColKey(fg, 'PrevisaoEntrega*')) = !chkColunas.checked;                                                                            //37		'Previs�oEntrega'
            fg.ColHidden(getColIndexByColKey(fg, 'PrevisaoVendas' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PV_ReadOnly))) = !chkColunas.checked;                   //49		'PV'
            fg.ColHidden(getColIndexByColKey(fg, 'PrazoEstoque' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PE_ReadOnly))) = !chkColunas.checked;                     //50		'PE'
            fg.ColHidden(getColIndexByColKey(fg, 'MultiploCompra' + glb_sReadOnly)) = !chkColunas.checked;                                                              //52		'MC'
        }

            // Servico 6 Internet
        else if (selServico.value == INTERNET) {
            fg.ColHidden(getColIndexByColKey(fg, 'PrazoTroca' + glb_sReadOnly)) = !chkColunas.checked;                                                                  //57		'Troca'
            fg.ColHidden(getColIndexByColKey(fg, 'PrazoGarantia' + glb_sReadOnly)) = !chkColunas.checked;                                                               //58		'Gar'
            fg.ColHidden(getColIndexByColKey(fg, 'PrazoAsstec' + glb_sReadOnly)) = !chkColunas.checked;                                                                 //59		'Asstec'
            fg.ColHidden(getColIndexByColKey(fg, 'PartNumber*')) = !chkColunas.checked;                                                                                 //64		'PartNumber'
        }

            // Servico 7 Inconsist�ncias
        else if (selServico.value == INCONSISTENCIAS) {
            fg.ColHidden(getColIndexByColKey(fg, 'PartNumber*')) = !chkColunas.checked;                                                                                 //64		'PartNumber'
        }

        else if (selServico.value == GERAL) {
            fg.ColHidden(getColIndexByColKey(fg, 'PrecoConcorrentes*')) = !chkColunas.checked;                                                                          //31		'PrecoConc'
            fg.ColHidden(getColIndexByColKey(fg, 'CustoReposicao*')) = !chkColunas.checked;                                                                             //32		'CustoRep1'
            fg.ColHidden(getColIndexByColKey(fg, 'PrazoPagamento*')) = !chkColunas.checked;                                                                             //33		'PP1'
            fg.ColHidden(getColIndexByColKey(fg, 'PrazoReposicao*')) = !chkColunas.checked;                                                                             //34		'PR1'
            fg.ColHidden(getColIndexByColKey(fg, 'PrevisaoEntrega*')) = !chkColunas.checked;                                                                            //37		'Previs�oEntrega'
            fg.ColHidden(getColIndexByColKey(fg, 'PartNumber*')) = !chkColunas.checked;                                                                                 //64		'PartNumber'
            fg.ColHidden(getColIndexByColKey(fg, 'NCM' + (glb_sReadOnlyClassFiscal == '*' ? glb_sReadOnlyClassFiscal : glb_NCM_ReadOnly))) = !chkColunas.checked;       //65		'NCM'
        }
    }
    else
        return null;
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var i = 0;
    glb_dataGridWasChanged = false;
    lockControlsInModalWin(true);
    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;

    try {
        dsoGrid.SubmitChanges();
    }
    catch (e) {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887) {
            if (window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0)
                return null;
        }

        fg.Rows = 1;
        lockControlsInModalWin(false);

        setupBtnsFromGridState();
        // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
        glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(false)', 50, 'JavaScript');
        return null;
    }

    lockControlsInModalWin(true);

    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {

    salvaArgumentos();
    dsoArgumentoVenda.ondatasetcomplete = dsoGravaArgumentoVenda_DSC;
    dsoArgumentoVenda.SubmitChanges();
    dsoArgumentoVenda.Refresh();

    lockControlsInModalWin(false);

    glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(false)', 50, 'JavaScript');
}

function dsoGravaArgumentoVenda_DSC() {
    consultaArgumentos();
}

/********************************************************************
Verifica se um dado campo existe em um dado dso

Parametros:
dso         - referencia ao dso
fldName     - nome do campo a verificar a existencia
                      
Retorno:
true se o campo existe, caso contrario false
********************************************************************/
function fieldExists(dso, fldName) {
    var retVal = false;
    var i;

    for (i = 0; i < dso.recordset.Fields.Count; i++) {
        if (dso.recordset.Fields[i].Name == fldName)
            retVal = true;
    }

    return retVal;
}

/********************************************************************
Usuario clicou o label do checkbox de mostrar detalhe
********************************************************************/
function lblCampos_onclick() {
    if (chkCampos.disabled)
        return true;

    chkCampos.checked = !chkCampos.checked;

    chkCampos_onclick();
}

/********************************************************************
Usuario clicou o botao comprar
********************************************************************/
function btn_Comprar_Clicked() {
    var strPars = '';
    var i = 0;
    var sError = '';

    lockControlsInModalWin(true);

    if (verificaProdutoLote()) {

        var bPedidoNacional = false;
        var bPedidoInternacional = false;

        strPars = '?nUserID=' + escape(glb_nUserID) +
		    '&nDepositoID=' + escape(glb_aEmpresaData[0]) +
		    '&nEmpresaID=' + escape(glb_aEmpresaData[0]) +
		    '&nPessoaID=' + escape(selFornecedor.value) +
		    '&nTransacaoID=' + escape(selTransacao.value) +
		    '&nFinanciamentoID=' + escape(selFinanciamento.value) +
		    '&nEncomenda=' + ((chkEnc.checked) ? 1 : 0) +
	        '&nConsolidar=' + ((chkConsolidar.checked) ? 1 : 0);


        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0) {

                strPars += '&nProdutoID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID*')));

                if ((chkEnc.checked) && (selProjeto.value == ""))
                    strPars += '&nQuantidade=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeVenda*')));
                else
                    strPars += '&nQuantidade=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeComprar')));

                strPars += '&nCusto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'CustoComprar')));
                strPars += '&nPedItemVendaID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PedItemID*')));

                if (chkEnc.checked) {
                    if (selProjeto.value > 0) {
                        strPars += '&nLoteID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'ProjetoLoteID*')));
                        strPars += '&sDescricao=' + escape('');
                    }
                    else {
                        strPars += '&nLoteID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'LoteID*')));
                        strPars += '&sDescricao=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'Lote*')));
                    }
                }
                else {
                    strPars += '&nLoteID=' + escape('');
                    strPars += '&sDescricao=' + escape('');
                }

                if ((chkEnc.checked) && (selFornecedor.value == 10003)) {
                    strPars += '&sProjetoID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'Projeto')));
                }
                else {
                    //if (selProjeto.value > 0)
                    //    strPars += '&sProjetoID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, '???????????')));
                    //else
                    strPars += '&sProjetoID=' + escape('');
                }

                strPars += '&nInternacional=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'Internacional*')));

                if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeComprar')) > fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeVenda*'))) {
                    var nQuantidadeExtra = (fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeComprar')) - fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeVenda*')));

                    strPars += '&nProdutoID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID*')));
                    strPars += '&nQuantidade=' + nQuantidadeExtra;
                    strPars += '&nCusto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'CustoComprar')));
                    strPars += '&nPedItemVendaID=' + 0;
                }

                if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeComprar')) <= 0) {
                    sError = 'Quantidade menor que 0 no produto ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID*'));
                    break;
                }
                else if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'CustoComprar')) <= 0) {
                    sError = 'Custo menor que 0 no produto ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID*'));
                    break;
                }
                else if ((chkEnc.checked) && (fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeComprar')) < fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeVenda*'))) && (selProjeto.value == "")) {
                    sError = 'Quantidade de compra do produto ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'ProdutoID*')) + ' ' +
                        'menor que quantidade do pedido de venda. ';
                    break;
                }

                if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Internacional*')) == "0")
                    bPedidoNacional = true;
                else if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Internacional*')) == "1")
                    bPedidoInternacional = true;
            }
        }

        strPars += '&nGeraPedidoNacional=' + escape(bPedidoNacional ? "1" : "0");
        strPars += '&nGeraPedidoInternacional=' + escape(bPedidoInternacional ? "1" : "0");

        if (sError != '') {
            if (window.top.overflyGen.Alert(sError) == 0)
                return null;

            lockControlsInModalWin(false);
            return null;
        }

        dsoGrava.URL = SYS_ASPURLROOT + '/serversidegenEx/gerarpedidocompra.aspx' + strPars;
        dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
        dsoGrava.refresh();
    }
    else {
        if (window.top.overflyGen.Alert('A Quantidade do Lote � insuficiente para atender a todas as compras aplicadas.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function sendDataToServer_DSC() {
    var nRetorno = null;
    var _retMsg = null;

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        if (dsoGrava.recordset.Fields["PedidoID"].value != null) {

            var pedidos = "";

            for (var i = 0; i < dsoGrava.recordset.RecordCount() ; i++) {
                pedidos += (pedidos == "" ? "" : " e ") + dsoGrava.recordset.Fields["PedidoID"].value;
            }

            _retMsg = window.top.overflyGen.Confirm('Gerado o Pedido: ' + pedidos + '\n' + ((dsoGrava.recordset.RecordCount() > 1) ? "" : 'Detalhar Pedido?'));

            if (_retMsg == 0)
                return null;
            else if (_retMsg == 1) {
                if (dsoGrava.recordset.RecordCount() > 1) {
                    fillGridData();
                    return null;
                }
                else
                    sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(glb_aEmpresaData[0], dsoGrava.recordset.Fields["PedidoID"].value));
            }
        }
        else {
            _retMsg = window.top.overflyGen.Alert('Pedido n�o foi gerado.');

            if (_retMsg == 0)
                return null;
        }
        fillGridData();
    }
}

/********************************************************************
Usuario trocou o option selecionado no combo de fornecedores
********************************************************************/
function selFornecedor_onchange() {
    adjustLabelsCombos();
    if (selFornecedor.value != 0)
        fillTransacao_Prazo();
    else {
        clearComboEx(['selTransacao', 'selFinanciamento']);
        selTransacao.disabled = true;
        selFinanciamento.disabled = true;
        fillGridData();
    }
    //SAS0081 - Pedidos Open Micrososft
    if ((chkEnc.checked) && (selFornecedor.value == 10003)) {
        btnBatch.style.visibility = 'inherit';
    }
    else {
        btnBatch.style.visibility = 'hidden';
    }
}

/********************************************************************
Usuario trocou o option selecionado no combo de fornecedores
********************************************************************/
function fillTransacao_Prazo() {
    var strSQLSelect, strSQLFrom, strSQLWhere, strSQLOrderBy;
    var nEmpresaID = glb_aEmpresaData[0];
    var aEmpresaData = getCurrEmpresaData();

    clearComboEx(['selTransacao', 'selFinanciamento']);

    glb_nDOSsCmbs = 2;

    // parametrizacao do dso dsoTransacao
    setConnection(dsoTransacao);

    strSQLSelect = 'SELECT ' +
			'a.OperacaoID AS fldID, ' +
			'dbo.fn_Tradutor(a.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) AS fldName, ' +
			'dbo.fn_Tradutor(a.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) AS Transacao ';

    strSQLFrom = 'FROM Operacoes a WITH(NOLOCK) ';

    strSQLWhere = 'WHERE ' +
			'(a.TipoOperacaoID = 652 AND a.EstadoID = 2 AND a.Nivel = 3 AND ' +
			'a.Entrada = 1 AND a.Fornecedor = 1 AND a.MetodoCustoID = 372) ';

    strSQLOrderBy = 'ORDER BY ISNULL(a.Ordem, 999), a.OperacaoID ';

    dsoTransacao.SQL = strSQLSelect + strSQLFrom + strSQLWhere + strSQLOrderBy;

    dsoTransacao.ondatasetcomplete = fillTransacao_Prazo_DSC;
    dsoTransacao.Refresh();

    // parametrizacao do dso dsoPrazo
    setConnection(dsoPrazo);

    strSQLSelect = 'SELECT ' +
			'c.FinanciamentoID AS fldID, ' +
			'c.Financiamento AS fldName ';
    strSQLFrom = 'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_FinanciamentosPadrao b WITH(NOLOCK), FinanciamentosPadrao c WITH(NOLOCK) ';
    strSQLWhere = 'WHERE ' +
			'(a.ObjetoID = 999 AND a.TipoRelacaoID = 12 AND a.SujeitoID = ' + nEmpresaID + ' AND ' +
			'a.RelacaoID = b.RelacaoID AND b.EstadoID = 2 AND b.FinanciamentoID = c.FinanciamentoID AND ' +
			'c.EstadoID = 2) ';

    strSQLOrderBy = 'ORDER BY c.Ordem ';

    dsoPrazo.SQL = strSQLSelect + strSQLFrom + strSQLWhere + strSQLOrderBy;

    dsoPrazo.ondatasetcomplete = fillTransacao_Prazo_DSC;
    dsoPrazo.Refresh();
}

function fillTransacao_Prazo_DSC() {
    glb_nDOSsCmbs--;

    if (glb_nDOSsCmbs > 0)
        return null;

    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selTransacao, selFinanciamento];
    var aDSOsDunamics = [dsoTransacao, dsoPrazo];

    // Inicia o carregamento de combos dinamicos (selSujeitoID, selObjetoID, selFiliadorID_07)
    //
    for (i = 0; i < aCmbsDynamics.length; i++) {
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }

        if (aCmbsDynamics[i].options.length > 0)
            aCmbsDynamics[i].selectedIndex = 0;

        aCmbsDynamics[i].disabled = (aCmbsDynamics[i].options.length == 0);
    }

    fillGridData();
}

// EVENTOS DE GRID **************************************************

function js_fg_ModalGerenciamentoProdutosBeforeRowColChange(fg, oldRow, oldCol, newRow, newCol, arg5) {

    var bForceNewLine = true;

    if (oldRow != newRow) {
        salvaArgumentos();
    }
    js_fg_BeforeRowColChange(fg, oldRow, oldCol, newRow, newCol, arg5, bForceNewLine);
}

function fg_modalgerenciamentoprodutosAfterRowColChange(oldRow, oldCol, newRow, newCol) {
    if ((!glb_GridIsBuilding) && (!chkEnc.checked)) {
        campInferior();
    }

    if (oldRow != newRow) {
        consultaArgumentos();

    }

    fg_AfterRowColChange();
}

/********************************************************************
Filtra os argumentos de venda para o produto do grid.
********************************************************************/
function consultaArgumentos() {
    // N�o processa se o filtro espec�fico n�o for PV > 0.
    if (selFiltroEspecifico.value != "41020") {
        return;
    }

    var produtoField = (!chkEnc.checked) ? "ObjetoID*" : "ProdutoID*";

    if (fg.Row > 1 && getColIndexByColKey(fg, produtoField) != -1 && dsoArgumentoVenda.SQL != "" && selFiltroArgumentosVenda.value != AV_SEM) {
        txtArgumentoDeVenda.value = "";
        chkOK.checked = false;

        dsoArgumentoVenda.recordset.setFilter('ConceitoID=' +
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, produtoField)) + " AND " +
			"IdiomaID = " + selIdiomaArgumentosVenda.value);

        if (!dsoArgumentoVenda.recordset.EOF) {
            txtArgumentoDeVenda.value = dsoArgumentoVenda.recordset["ArgumentosVenda"].value;
            chkOK.checked = (dsoArgumentoVenda.recordset["OK"].value == 1);
        }

        // Libera o filtro.
        dsoArgumentoVenda.recordset.setFilter("");
    }
}

/********************************************************************
Salva os argumentos de venda para o produto do grid.
********************************************************************/
function salvaArgumentos() {
    // N�o processa se o filtro espec�fico n�o for PV > 0.
    if (selFiltroEspecifico.value != "41020") {
        return;
    }

    var produtoField = (!chkEnc.checked) ? "ObjetoID*" : "ProdutoID*";

    if (fg.Row > 1 && getColIndexByColKey(fg, produtoField) != -1 && dsoArgumentoVenda.SQL != "") {
        dsoArgumentoVenda.recordset.setFilter("ConceitoID=" +
			fg.TextMatrix(fg.Row, getColIndexByColKey(fg, produtoField)) + " AND " +
			"IdiomaID = " + selIdiomaArgumentosVenda.getAttribute("oldSelected"));

        // Atualiza os dadas alterados.
        //if(glb_HouveAlteracao && !dsoArgumentoVenda.recordset.EOF && (dsoArgumentoVenda.recordset["OK"].value != chkOK.checked || dsoArgumentoVenda.recordset["ArgumentosVenda"].value != txtArgumentoDeVenda.value))
        if (glb_HouveAlteracao && (!dsoArgumentoVenda.recordset.EOF)) {
            dsoArgumentoVenda.recordset["ArgumentosVenda"].value = txtArgumentoDeVenda.value;
            dsoArgumentoVenda.recordset["OK"].value = chkOK.checked ? 1 : 0;
        }
            // Grava dados novos.
        else if (glb_HouveAlteracao) {
            dsoArgumentoVenda.recordset.addNew();

            dsoArgumentoVenda.recordset["ConceitoID"].value = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, produtoField));
            dsoArgumentoVenda.recordset["IdiomaID"].value = selIdiomaArgumentosVenda.value;
            dsoArgumentoVenda.recordset["ArgumentosVenda"].value = txtArgumentoDeVenda.value;
            dsoArgumentoVenda.recordset["OK"].value = chkOK.checked ? 1 : 0;

            glb_HouveAlteracao = false;
        }

        // Libera o filtro.
        dsoArgumentoVenda.recordset.setFilter("");
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerenciamentoprodutosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    if (Button != 1)
        return true;

    glb_nLastStateClicked = Shift;
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerenciamentoprodutosDblClick(grid, Row, Col) {

    if (fg.Col != getColIndexByColKey(fg, 'ChkOK')) {
        var empresa = getCurrEmpresaData();
        var relacaoID = fg.TextMatrix(Row, getColIndexByColKey(fg, 'RelacaoID'));

        // Manda o id da relacao entre a empresa e o produto a detalhar
        if (dsoGrid.recordset.fields.count > 1)
            sendJSCarrier(getHtmlId(), 'SHOWRELPESCON', new Array(empresa[0], relacaoID));
    }

    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    glb_PassedOneInDblClick = true;

    if (fg.Col == getColIndexByColKey(fg, 'ChkOK')) {
        for (i = 2; i < fg.Rows; i++)
            fg.TextMatrix(i, getColIndexByColKey(fg, 'ChkOK')) = glb_EnableChkOK;
        glb_EnableChkOK = !glb_EnableChkOK;
    }

    Totaliza();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerenciamentoprodutosKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalgerenciamentoprodutos_ValidateEdit() {
    ;
}

function Totaliza() {
    var i = 0;
    var nTotalQuantidadeOK = 0;
    var nTotalQuantidade = 0;
    var nTotalVendasMes3 = 0;
    var nTotalVendasMes2 = 0;
    var nTotalVendasMes1 = 0;
    var nTotalVendasMes0 = 0;
    var nTotalMediaVendas13 = 0;
    var nTotalPrevisaoVendas = 0;
    var nTotalPrecoTotal_2 = 0;
    var nTotalContribuicaoTotal = 0;
    var nTotalEstoque_2 = 0;
    var nTotalComprarExcesso_2 = 0;
    var nCustoTotalComprar = 0;
    var nMediaDiasFim = 0.0;                        // Media
    var nMediaDiasControle = 0.0;                   // Media
    var nMediaDiasPagamento = 0.0;                  // Media
    var nMediaMargemContribuicao = 0.0;             // Media
    var nMediaMargemContribuicaoSuprimento = 0.0;   // Media
    var nMediaMargemMedia = 0.0;                    // Media
    var nMediaMargemMinima = 0.0;                   // Media
    var nMediaMargemPadrao = 0.0;                   // Media
    var nMediaMargemPublicacao = 0.0;               // Media
    var nMediaMargemReal = 0.0;                     // Media
    var nMediaMultiploVenda = 0.0;                  // Media
    var nMediaPrazoEstoque = 0.0;                   // Media
    var nMediaPrazoPagamento = 0.0;                 // Media
    var nMediaPrazoReposicao = 0.0;                 // Media
    var nTotalComprarExcesso = 0;
    var nTotalEstoqueInternet = 0;
    var nTotalEstoque = 0;
    var nTotalReservaCompra = 0;
    var nTotalReservaVendaConfirmada = 0;
    //var nCustoTotalComprar = 0;
    //var nTotalContribuicaoTotal = 0;
    //var nTotalPrecoTotal_2 = 0;
    //var nTotalPrevisaoVendas = 0;

    var nSavedRow = 0;

    if (fg.Rows < 2)
        return;

    fg.Redraw = 0;
    nSavedRow = fg.Row;


    if (!chkEnc.checked) {
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true,
	        [
                // As operacoes possiveis sao 'S' soma 'C' count e 'M' media

                [getColIndexByColKey(fg, 'Estado*'), '######', 'C'],
	            [getColIndexByColKey(fg, 'Produto*'), '######', 'C'],
		        [getColIndexByColKey(fg, 'VendasMes3*'), '######0', 'S'],
		        [getColIndexByColKey(fg, 'VendasMes2*'), '######0', 'S'],
		        [getColIndexByColKey(fg, 'VendasMes1*'), '######0', 'S'],
		        [getColIndexByColKey(fg, 'VendasMes0*'), '######0', 'S'],
		        [getColIndexByColKey(fg, 'MediaVendas13*'), '######0', 'S'],
		        [getColIndexByColKey(fg, 'PrevisaoVendas' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PV_ReadOnly)), '######0', 'S'],
		        [getColIndexByColKey(fg, 'PrecoTotal_2*'), '###,###,##0.00', 'S'],
		        [getColIndexByColKey(fg, 'ContribuicaoTotal*'), '###,###,##0.00', 'S'],
		        [getColIndexByColKey(fg, 'Estoque_2*'), '######0', 'S'],
		        [getColIndexByColKey(fg, 'ComprarExcesso_2*'), '######0', 'S'],
		        [getColIndexByColKey(fg, 'CustoTotalComprar*'), '###,###,##0.00', 'S'],

                [getColIndexByColKey(fg, 'DiasFim*'), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'DiasControle*'), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'DiasPagamento' + glb_PagamentoDias_ReadOnly), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'MargemContribuicao' + glb_sReadOnly), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'MargemMedia*'), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'MargemMinima' + glb_sReadOnly), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'MargemPadrao' + glb_sReadOnly), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'MargemPublicacao' + glb_sReadOnly), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'MargemReal*'), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'MultiploVenda' + glb_sReadOnly), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'PrazoEstoque' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PE_ReadOnly)), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'PrazoPagamento*'), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'PrazoReposicao*'), '#####.#', 'M'],
                [getColIndexByColKey(fg, 'ComprarExcesso*'), '#######', 'S'],
                [getColIndexByColKey(fg, 'EstoqueInternet*'), '#######', 'S'],
                [getColIndexByColKey(fg, 'Estoque*'), '#######', 'S'],
                [getColIndexByColKey(fg, 'ReservaCompra*'), '#######', 'S'],
                [getColIndexByColKey(fg, 'ReservaVendaConfirmada*'), '#######', 'S']

	        ], true);

        glb_totalCols__ = true;
        for (i = 2; i < fg.Rows; i++) {
            nTotalQuantidade++;

            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0) {
                nTotalQuantidadeOK++;
                nTotalVendasMes3 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMes3*'));
                nTotalVendasMes2 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMes2*'));
                nTotalVendasMes1 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMes1*'));
                nTotalVendasMes0 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMes0*'));
                nTotalMediaVendas13 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MediaVendas13*'));
                nTotalPrevisaoVendas += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrevisaoVendas' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PV_ReadOnly)));
                nTotalPrecoTotal_2 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoTotal_2*'));
                nTotalContribuicaoTotal += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ContribuicaoTotal*'));
                nTotalEstoque_2 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Estoque_2*'));
                nTotalComprarExcesso_2 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ComprarExcesso_2*'));
                nCustoTotalComprar += fg.ValueMatrix(i, getColIndexByColKey(fg, 'CustoTotalComprar*'));

                nMediaDiasFim += fg.ValueMatrix(i, getColIndexByColKey(fg, 'DiasFim*'));
                nMediaDiasControle += fg.ValueMatrix(i, getColIndexByColKey(fg, 'DiasControle*'));
                nMediaDiasPagamento += fg.ValueMatrix(i, getColIndexByColKey(fg, 'DiasPagamento' + glb_PagamentoDias_ReadOnly));
                nMediaMargemContribuicao += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MargemContribuicao' + glb_sReadOnly));
                nMediaMargemContribuicaoSuprimento += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MultiploCompra' + glb_sReadOnly));
                nMediaMargemMedia += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MargemMedia*'));
                nMediaMargemMinima += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MargemMinima' + glb_sReadOnly));
                nMediaMargemPadrao += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MargemPadrao' + glb_sReadOnly));
                nMediaMargemPublicacao += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MargemPublicacao' + glb_sReadOnly));
                nMediaMargemReal += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MargemReal*'));
                nMediaMultiploVenda += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MultiploVenda' + glb_sReadOnly));
                nMediaPrazoEstoque += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrazoEstoque' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PE_ReadOnly)));
                nMediaPrazoPagamento += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrazoPagamento*'));
                nMediaPrazoReposicao += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrazoReposicao*'));
                nTotalComprarExcesso += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ComprarExcesso*'));
                nTotalEstoqueInternet += fg.ValueMatrix(i, getColIndexByColKey(fg, 'EstoqueInternet*'));
                nTotalEstoque += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Estoque*'));
                nTotalReservaCompra += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ReservaCompra*'));
                nTotalReservaVendaConfirmada += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ReservaVendaConfirmada*'));

            }
        }

        if (fg.Rows > 2) {
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Estado*')) = nTotalQuantidadeOK;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Produto*')) = nTotalQuantidade;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'VendasMes3*')) = nTotalVendasMes3;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'VendasMes2*')) = nTotalVendasMes2;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'VendasMes1*')) = nTotalVendasMes1;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'VendasMes0*')) = nTotalVendasMes0;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MediaVendas13*')) = nTotalMediaVendas13;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'PrevisaoVendas' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PV_ReadOnly))) = nTotalPrevisaoVendas;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'PrecoTotal_2*')) = nTotalPrecoTotal_2;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'ContribuicaoTotal*')) = nTotalContribuicaoTotal;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Estoque_2*')) = nTotalEstoque_2;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'ComprarExcesso_2*')) = nTotalComprarExcesso_2;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'CustoTotalComprar*')) = nCustoTotalComprar;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'DiasFim*')) = ((nMediaDiasFim == 0) ? 0 : nMediaDiasFim / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'DiasControle*')) = ((nMediaDiasControle == 0) ? 0 : nMediaDiasControle / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'DiasPagamento' + glb_PagamentoDias_ReadOnly)) = ((nMediaDiasPagamento == 0) ? 0 : nMediaDiasPagamento / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemContribuicao' + glb_sReadOnly)) = ((nMediaMargemContribuicao == 0) ? 0 : nMediaMargemContribuicao / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MultiploCompra' + glb_sReadOnly)) = ((nMediaMargemContribuicaoSuprimento == 0) ? 0 : nMediaMargemContribuicaoSuprimento / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemMedia*')) = ((nMediaMargemMedia == 0) ? 0 : nMediaMargemMedia / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemMinima' + glb_sReadOnly)) = ((nMediaMargemMinima == 0) ? 0 : nMediaMargemMinima / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemPadrao' + glb_sReadOnly)) = ((nMediaMargemPadrao == 0) ? 0 : nMediaMargemPadrao / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemPublicacao' + glb_sReadOnly)) = ((nMediaMargemPublicacao == 0) ? 0 : nMediaMargemPublicacao / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemReal*')) = ((nMediaMargemReal == 0) ? 0 : nMediaMargemReal / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MultiploVenda' + glb_sReadOnly)) = ((nMediaMultiploVenda == 0) ? 0 : nMediaMultiploVenda / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'PrazoEstoque' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PE_ReadOnly))) = ((nMediaPrazoEstoque == 0) ? 0 : nMediaPrazoEstoque / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'PrazoPagamento*')) = ((nMediaPrazoPagamento == 0) ? 0 : nMediaPrazoPagamento / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'PrazoReposicao*')) = ((nMediaPrazoReposicao == 0) ? 0 : nMediaPrazoReposicao / nTotalQuantidadeOK);
            fg.TextMatrix(1, getColIndexByColKey(fg, 'ComprarExcesso*')) = nTotalComprarExcesso;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'EstoqueInternet*')) = nTotalEstoqueInternet;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'Estoque*')) = nTotalEstoque;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'ReservaCompra*')) = nTotalReservaCompra;
            fg.TextMatrix(1, getColIndexByColKey(fg, 'ReservaVendaConfirmada*')) = nTotalReservaVendaConfirmada;

        }
    }
    else {
        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true,
	        [
	            [getColIndexByColKey(fg, 'CustoTotalComprar*'), '###,###,##0.00', 'S']
	        ], true);

        glb_totalCols__ = true;
        for (i = 2; i < fg.Rows; i++) {
            nTotalQuantidade++;

            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0) {
                nCustoTotalComprar += fg.ValueMatrix(i, getColIndexByColKey(fg, 'CustoTotalComprar*'));
            }
        }

        if (fg.Rows > 2) {
            fg.TextMatrix(1, getColIndexByColKey(fg, 'CustoTotalComprar*')) = nCustoTotalComprar;
        }
    }

    fg.Row = nSavedRow;
    fg.Redraw = 2;
}

function btn_Imprimir_Clicked() {
    if (!chkEnc.checked) {
        var aEmpresaData = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
        var gridLine = fg.gridLines;
        var sMsg = aEmpresaData[6] + '     ';
        fg.gridLines = 2;

        sMsg += translateTerm('Posi��o de produtos', null) + '     ';

        var oldColWidth = fg.ColWidth(getColIndexByColKey(fg, 'Observacao' + glb_sReadOnly));
        fg.ColWidth(getColIndexByColKey(fg, 'Observacao' + glb_sReadOnly)) = oldColWidth * 1.6;

        var aCmbFiltro = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 2)');
        if (aCmbFiltro != null)
            sMsg += '(' + aCmbFiltro[2] + ')' + '     ';

        sMsg += getCurrDate();

        fg.PrintGrid(sMsg, false, 2, 0, 450);
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Produto*')) = '';
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Observacao' + glb_sReadOnly)) = '';
        fg.ColWidth(getColIndexByColKey(fg, 'Observacao' + glb_sReadOnly)) = oldColWidth;

        fg.gridLines = gridLine;
    }
    else
        return null;
}

function btn_Excel_Clicked() {
    glb_Excel = 1;
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = selectString(true);
}

function btn_Batch_Clicked() {

    var strPars = '';

    strPars = '?nEmpresaID=' + escape(glb_aEmpresaData[0]);
    strPars += '&nUsuarioID=' + escape(glb_nUserID);

    dsoDataFields.URL = SYS_ASPURLROOT + '/serversidegenEx/enviararquivobatch.aspx' + strPars;
    dsoDataFields.ondatasetcomplete = sendDataToServer_DSCArquivo;
    dsoDataFields.refresh();
}

function sendDataToServer_DSCArquivo() {
    var nRetorno = null;
    var _retMsg = null;

    if (!(dsoDataFields.recordset.BOF && dsoDataFields.recordset.EOF)) {
        if (dsoDataFields.recordset.Fields["Resultado"].value != null) {

            if (dsoDataFields.recordset.Fields["Resultado"].value == 2) {
                _retMsg = window.top.overflyGen.Alert('Existem arquivos sem dados do Cliente Final.');
            }
            else if (dsoDataFields.recordset.Fields["Resultado"].value == 3) {
                _retMsg = window.top.overflyGen.Alert('Arquivo BatchMS n�o gerado.');
            }
            else if (dsoDataFields.recordset.Fields["Resultado"].value == 4) {
                _retMsg = window.top.overflyGen.Alert('Arquivo BatchMS_Desconto n�o gerado.');
            }
            else if (dsoDataFields.recordset.Fields["Resultado"].value == 5) {
                _retMsg = window.top.overflyGen.Alert('N�o h� arquivos a serem gerados.');
            }
            else {
                _retMsg = window.top.overflyGen.Alert('Arquivo(s) Gerado(s).');
            }

            if (_retMsg == 0)
                return null;
        }
        else {
            _retMsg = window.top.overflyGen.Alert('Problema ao criar o Batch.');

            if (_retMsg == 0)
                return null;
        }
    }

    fillGridData();
}


function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear() + " ";
    s += padL(d.getHours().toString(), 2, '0') + ":";
    s += padL(d.getMinutes().toString(), 2, '0');

    return (s);
}

/********************************************************************
Evento de grid particular desta pagina 
********************************************************************/
function js_modalgerenciamentoprodutos_AfterEdit(Row, Col) {
    var nType;

    if ((Col == getColIndexByColKey(fg, 'QuantidadeComprar')) ||
		 (Col == getColIndexByColKey(fg, 'CustoComprar'))) {
        fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
        if (chkEnc.checked) {
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'CustoTotalComprar*')) = (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'CustoComprar')) * fg.ValueMatrix(Row, getColIndexByColKey(fg, 'QuantidadeComprar')));
        }
        Totaliza();
        return;
    }

    if (fg.Editable) {
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.MoveFirst();

        if (!chkEnc.checked)
            dsoGrid.recordset.Find('RelacaoID', fg.TextMatrix(Row, getColIndexByColKey(fg, 'RelacaoID*')));

        if (Col == getColIndexByColKey(fg, 'ChkOK')) {
            glb_dataGridWasChanged = true;
            setupBtnsFromGridState();
            Totaliza();
        }
        else if (!(dsoGrid.recordset.EOF)) {

            // LOG - sempre altera o UsuarioID
            if (LOG_ACT_FLD != null) {
                if (fieldExists(dsoGrid, LOG_ACT_FLD))
                    dsoGrid.recordset[LOG_ACT_FLD].value = glb_USERID;
            }

            // Operacao feita somente para garantir o valor do estadoid na trigger
            if (!chkEnc.checked) {
                if (fieldExists(dsoGrid, 'EstadoID') || fieldExists(dsoGrid, 'EstadoID'))
                    dsoGrid.recordset['EstadoID'].value = dsoGrid.recordset['EstadoID'].value;
            }

            if (fieldExists(dsoGrid, fg.ColKey(Col)))
                nType = dsoGrid.recordset[fg.ColKey(Col)].type;
            else
                return true;

            // Se decimal , numerico , int, bigint ou boolean
            if ((nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20)) {
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
                dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
            }
                // Tratamento de data
            else if (nType == 135) {
                if (fg.TextMatrix(fg.Row, Col) != '') {
                    if (chkDataEx(fg.TextMatrix(fg.Row, Col)) == null) {
                        // a data e nula, zera o campo
                        fg.TextMatrix(fg.Row, Col) = '';
                        return true;
                    }

                    // verifica se a mascara de digitacao tem hora e se tiver
                    // e o usuario nao digitou estripa a hora completa ajusta
                    // a mascara: '99/99/9999 99:99'
                    theMask = fg.ColEditMask(Col);

                    // a mascara tem hora?
                    dateMask = '';
                    hourMask = '';
                    if (theMask.lastIndexOf(':') >= 0) {
                        dateMask = trimStr(theMask.substr(0, theMask.lastIndexOf(' ')));
                        hourMask = trimStr(theMask.substr(theMask.lastIndexOf(' ')));
                    }

                    if (hourMask != '') {
                        strDate = '';
                        strHour = '';

                        strDate = trimStr((fg.TextMatrix(fg.Row, Col)).substr(0, theMask.lastIndexOf(' ')));
                        strHour = trimStr((fg.TextMatrix(fg.Row, Col)).substr(theMask.lastIndexOf(' ')));

                        // O usuario nao digitou a parte de hora e minuto
                        if (strHour == ':')
                            fg.TextMatrix(fg.Row, Col) = strDate + ' 00:00';
                    }

                    if (trimStr(fg.TextMatrix(fg.Row, Col)) == '/  /')
                        dsoGrid.recordset[fg.ColKey(Col)].value = null;
                    else if (chkDataEx(fg.TextMatrix(fg.Row, Col)) == false) {
                        if (window.top.overflyGen.Alert('Data inv�lida!') == 0)
                            return null;

                        fg.TextMatrix(fg.Row, Col) = '';

                        return -1;
                    }
                    else
                        dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);
                }
            }
            else
                dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);

            glb_dataGridWasChanged = true;
            setupBtnsFromGridState();
        }
    }
}

function js_fg_modalgerenciamentoprodutosMouseMove(grid, Button, Shift, X, Y) {

    var currRow = fg.MouseRow;
    var currCol = fg.MouseCol;

    var colDePara = null;

    if (currRow > 0 && currCol > 0) {

        if (fg.ColKey(currCol) == 'Estado*')
            colDePara = getColIndexByColKey(fg, 'Est_Auditoria');

        if (fg.ColKey(currCol) == 'Publica' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'Int_Auditoria');

        if (fg.ColKey(currCol) == 'dtInicio' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'CVPInicio_Auditoria');

        if (fg.ColKey(currCol) == 'dtFim' + ((glb_sReadOnly == '*') ? glb_sReadOnly : glb_Fim_ReadOnly))
            colDePara = getColIndexByColKey(fg, 'CVPFim_Auditoria');

        if (fg.ColKey(currCol) == 'dtControle' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'CVPControle_Auditoria');

        if (fg.ColKey(currCol) == 'PrazoEstoque' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PE_ReadOnly))
            colDePara = getColIndexByColKey(fg, 'PE_Auditoria');

        if (fg.ColKey(currCol) == 'MultiploVenda' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'MV_Auditoria');

        if (fg.ColKey(currCol) == 'MultiploCompra' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_MultiploCompra_ReadOnly))
            colDePara = getColIndexByColKey(fg, 'MuC_Auditoria');

        if (fg.ColKey(currCol) == 'PrevisaoVendas' + (glb_sReadOnly == '*' ? glb_sReadOnly : glb_PV_ReadOnly))
            colDePara = getColIndexByColKey(fg, 'PV_Auditoria');

        if (fg.ColKey(currCol) == 'dtMediaPagamento' + glb_Pagamento_ReadOnly)
            colDePara = getColIndexByColKey(fg, 'Pag_Auditoria1');

        if (fg.ColKey(currCol) == 'DiasPagamento' + glb_PagamentoDias_ReadOnly)
            colDePara = getColIndexByColKey(fg, 'Pag_Auditoria1');

        if (fg.ColKey(currCol) == 'MargemContribuicao' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'MC_Auditoria');

        if (fg.ColKey(currCol) == 'MargemPublicacao' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'MPub_Auditoria');

        if (fg.ColKey(currCol) == 'PublicaPreco' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'PP_Auditoria');

        if (colDePara != null)
            glb_aCelHint = [[currRow, currCol, fg.TextMatrix(currRow, colDePara)]];
    }
}

function fg_MouseDown_gerenciamentoProdutos(nRow, nCol) {

    // Soh aceita Ctrl, shift e alt
    if ((glb_nLastStateClicked != 1) && (glb_nLastStateClicked != 2) && (glb_nLastStateClicked != 4))
        return null;

    var sColValueToCheck = '';
    var nSavedRow = 0;
    var i = 0;
    var bEnableChkOK = false;

    if (fg.Col == getColIndexByColKey(fg, 'ChkOK')) {
        bEnableChkOK = (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ChkOK')) != 0);
        nSavedRow = nRow;

        // Shift
        if (glb_nLastStateClicked == 1)
            sColValueToCheck = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Proprietario*'));
            // Ctrl
        else if ((glb_nLastStateClicked == 2) && (!chkEnc.checked))
            sColValueToCheck = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'abrevCor'));
            // Alt
        else if (glb_nLastStateClicked == 4)
            sColValueToCheck = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Estado*'));

        if (sColValueToCheck != '') {
            for (i = 2; i < fg.Rows; i++) {
                // Shift
                if (glb_nLastStateClicked == 1) {
                    if (sColValueToCheck == fg.TextMatrix(i, getColIndexByColKey(fg, 'Proprietario*')))
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'ChkOK')) = bEnableChkOK;
                }
                    // Ctrl
                else if ((glb_nLastStateClicked == 2) && (!chkEnc.checked)) {
                    if (sColValueToCheck == fg.TextMatrix(i, getColIndexByColKey(fg, 'abrevCor')))
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'ChkOK')) = bEnableChkOK;
                }
                    // Alt
                else if (glb_nLastStateClicked == 4) {
                    if (sColValueToCheck == fg.TextMatrix(i, getColIndexByColKey(fg, 'Estado*')))
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'ChkOK')) = bEnableChkOK;
                }
            }

            glb_EnableChkOK = !glb_EnableChkOK;
        }

        Totaliza();
    }

    glb_nLastStateClicked = null;
}

// FINAL DE EVENTOS DE GRID *****************************************

function adjustLabelsCombos() {
    setLabelOfControl(lblFornecedor, selFornecedor.value);
}

function window_onunload() {
    dealWithObjects_Unload();
}

function preencheLote() {
    var sSQL = '';

    if (selProjeto.value != "") {
        sSQL += 'SELECT 0 AS LoteID, \'\' AS Descricao, 0 AS ProdutoID, 0 AS Quantidade ' +
                'UNION ALL ' +
                'SELECT a.LoteID, a.Descricao, b.ProdutoID, SUM(dbo.fn_Lotes_ProdutosQuantidades(a.LoteID, b.ProdutoID, 3, NULL)) AS Quantidade ' +
                    'FROM Lotes a WITH(NOLOCK) ' +
                        'LEFT OUTER JOIN Lotes_Produtos b WITH(NOLOCK) ON (b.LoteID = a.LoteID) ' +
                    'WHERE (a.EstadoID = 41) AND (a.ProprietarioID = ' + glb_nUserID + ' OR a.AlternativoID = ' + glb_nUserID + ') ' +
                    'GROUP BY a.LoteID, a.Descricao, b.ProdutoID ' +
                    'ORDER BY LoteID';
    } else {
        sSQL += 'SELECT 0 AS LoteID, \'\' AS Descricao, 0 AS ProdutoID, 0 AS Quantidade ' +
                'UNION ALL ' +
                'SELECT a.LoteID, a.Descricao, b.ProdutoID, SUM(dbo.fn_Lotes_ProdutosQuantidades(a.LoteID, b.ProdutoID, 3, NULL)) AS Quantidade ' +
                    'FROM Lotes a WITH(NOLOCK) ' +
                        'LEFT OUTER JOIN Lotes_Produtos b WITH(NOLOCK) ON (b.LoteID = a.LoteID) ' +
                    'WHERE (a.EstadoID = 41) AND (a.ProprietarioID = ' + glb_nUserID + ' OR a.AlternativoID = ' + glb_nUserID + ') ' +
                            'AND (a.EmpresaID = ' + glb_aEmpresaData[0] + ') ' +
                    'GROUP BY a.LoteID, a.Descricao, b.ProdutoID ' +
                    'ORDER BY LoteID';
    }


        

    setConnection(dsoLote);
    dsoLote.SQL = sSQL;
    dsoLote.ondatasetcomplete = preencheLote_DSC;
    dsoLote.Refresh();
}

function preencheLote_DSC() {
    var nLoteAnteriorID = 0;

    clearComboEx(['selLote']);

    if (!(dsoLote.recordset.BOF || dsoLote.recordset.EOF)) {
        dsoLote.recordset.MoveFirst();

        while (!dsoLote.recordset.EOF) {
            if ((nLoteAnteriorID != dsoLote.recordset['LoteID'].value) || (nLoteAnteriorID == 0)) {
                var oOption = document.createElement("OPTION");
                oOption.text = dsoLote.recordset['Descricao'].value;
                oOption.value = dsoLote.recordset['LoteID'].value;
                oOption.setAttribute('Filtro', dsoFiltroEspecifico.recordset['Filtro'].value, 1);
                selLote.add(oOption);
            }

            nLoteAnteriorID = dsoLote.recordset['LoteID'].value;
            dsoLote.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selLote', glb_nValue);

        selLote.disabled = ((selLote.length <= 1) ? true : false);
    }
}

function cotacaoPTAX() {
    var sSQL = 'SELECT CONVERT(NUMERIC(11,4),dbo.fn_Preco_Cotacao(704, ' + glb_nMoedaFaturamentoID + ', NULL, GETDATE())) AS DolarPTAX';

    setConnection(dsoPtax);
    dsoPtax.SQL = sSQL;
    dsoPtax.ondatasetcomplete = cotacaoPTAX_DSC;
    dsoPtax.Refresh();
}

function cotacaoPTAX_DSC() {

    var nDolarPtax = 0;

    if (!(dsoPtax.recordset.BOF || dsoPtax.recordset.EOF)) {
        dsoPtax.recordset.MoveFirst();

        nDolarPtax = dsoPtax.recordset['DolarPTAX'].value;

        txtPtax.value = nDolarPtax;
        txtPtax.disabled = true;

    }
}

function verificaProdutoLote(nLoteID) {
    var aProdutoSaldoLote = new Array();
    var nIndice = 0;
    var nQuantidadeLote = 0;
    var nQuantidadeGrid = 0;
    var nInicio = 0;
    var nFinal = 0;
    var nProdutoID = 0;
    var bLoteGrid = true;
    var sLote = '';
    var bVerificaLote = false;
    var sCampoVerificar;

    if (nLoteID != null)
        bLoteGrid = false;

    if (!bLoteGrid)
        sCampoVerificar = 'Aplicar';
    else
        sCampoVerificar = 'chkOK';

    for (icount = 2; icount < fg.Rows; icount++) {
        bVerificaLote = fg.ValueMatrix(icount, getColIndexByColKey(fg, sCampoVerificar));

        if (bVerificaLote) {
            if (bLoteGrid)
                nLoteID = fg.TextMatrix(icount, getColIndexByColKey(fg, 'LoteID*'));

            if (!chkEnc.checked)
                nProdutoID = fg.TextMatrix(icount, getColIndexByColKey(fg, 'ObjetoID*'));
            else if (chkEnc.checked)
                nProdutoID = fg.TextMatrix(icount, getColIndexByColKey(fg, 'ProdutoID*'));

            nQuantidadeGrid = fg.ValueMatrix(icount, getColIndexByColKey(fg, 'QuantidadeComprar'));

            if (nLoteID != '')
                dsoLote.recordset.setFilter('LoteID = ' + nLoteID + ' AND ProdutoID = ' + nProdutoID);

            if ((!(dsoLote.recordset.BOF || dsoLote.recordset.EOF)) || (nLoteID == 0)) {
                if (nLoteID != 0) {
                    nQuantidadeLote = dsoLote.recordset['Quantidade'].value;
                    sLote = dsoLote.recordset['Descricao'].value;
                }
                else
                    sLote = txtDescricao.value;

                nIndice = aseek_prg(aProdutoSaldoLote, nLoteID, 0, nProdutoID, 1);

                if (nIndice == null) {
                    nIndice = ((aProdutoSaldoLote == null) ? 0 : aProdutoSaldoLote.length);
                    aProdutoSaldoLote[nIndice] = new Array();

                    aProdutoSaldoLote[nIndice][0] = nLoteID;
                    aProdutoSaldoLote[nIndice][1] = nProdutoID;

                    if (nLoteID == 0)
                        aProdutoSaldoLote[nIndice][2] = 0;
                    else
                        aProdutoSaldoLote[nIndice][2] = (nQuantidadeLote - nQuantidadeGrid);
                }
                else if (nIndice != null) {
                    nQuantidadeLote = aProdutoSaldoLote[nIndice][2];

                    if (nLoteID == 0)
                        aProdutoSaldoLote[nIndice][2] = 0;
                    else
                        aProdutoSaldoLote[nIndice][2] = (nQuantidadeLote - nQuantidadeGrid);
                }

                if ((aProdutoSaldoLote[nIndice][2] >= 0) && (!bLoteGrid)) {
                    fg.TextMatrix(icount, getColIndexByColKey(fg, 'LoteID*')) = nLoteID;
                    fg.TextMatrix(icount, getColIndexByColKey(fg, 'Lote*')) = sLote;
                }
            }
        }
    }

    if (aProdutoSaldoLote != null)
        nFinal = aProdutoSaldoLote.length - 1;

    if (nFinal <= nInicio)
        return true;

    while (nInicio <= nFinal) {
        if (aProdutoSaldoLote[nInicio][2] < 0)
            return false;

        nInicio = nInicio + 1;
    }

    return true;
}

function aseek_prg(aArray, strToSeek1, strToSeek2, nArrayDimension1, nArrayDimension2) {
    if (aArray == null)
        return null;

    var nInicio = 0;
    var nFinal = aArray.length - 1;
    var nRetorno = 0;

    if (nFinal <= nInicio)
        return null;

    while (nInicio <= nFinal) {
        if ((aArray[nInicio][nArrayDimension1] == strToSeek1) && (aArray[nInicio][nArrayDimension2] == strToSeek2)) {
            nRetorno = nInicio;
            break;
        }

        nInicio = nInicio + 1;
    }

    return nRetorno;
}

function btn_Aplicar_Clicked() {
    var nLoteID = selLote.value;

    verificaProdutoLote(nLoteID);
}

function selLote_onchange() {
    if (selLote.value == 0) {
        lblDescricao.style.visibility = 'inherit';
        txtDescricao.style.visibility = 'inherit';

        lblLote.innerText = 'Lote';
    }
    else {
        lblDescricao.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';

        lblLote.innerText = 'Lote ' + selLote.value.toString();
    }
}

function VerificaLoteGrid() {
    var bReturn = true;

    if (chkEnc.checked) {
        for (i = 2; i < fg.Rows; i++) {
            if ((fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0)
                && (fg.TextMatrix(i, getColIndexByColKey(fg, 'LoteID*')) == '')
                && (fg.TextMatrix(i, getColIndexByColKey(fg, 'ProjetoLoteID*')) == '')) {
                bReturn = false;
                break;
            }
        }
    }

    return bReturn;
}

function LimpaLoteGrid() {
    for (i = 2; i < fg.Rows; i++) {
        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'LoteID*')) != '') {
            fg.TextMatrix(i, getColIndexByColKey(fg, 'LoteID*')) = '';
            fg.TextMatrix(i, getColIndexByColKey(fg, 'Lote*')) = '';
        }
    }
}

function reports_onreadystatechange() {
    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
    (frameReport.contentWindow.document.readyState == 'interactive') ||
    (frameReport.contentWindow.document.readyState == 'complete')) {
        lockControlsInModalWin(false);
    }
}