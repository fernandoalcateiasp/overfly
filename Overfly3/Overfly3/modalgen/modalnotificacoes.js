/********************************************************************
modalgerenciamentoprodutos.js

Library javascript para o modalgerenciamentoprodutos.asp
********************************************************************/

// VARIAVEIS GLOBAIS - Inicio ***************************************

// Constatnes de filtros específicos.

// Constantes de Argumentos de Venda.

// VARIAVEIS GLOBAIS - Fim ******************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
fieldExists(dso, fldName)

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalgerenciamentoprodutos.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalgerenciamentoprodutos.ASP

js_fg_modalgerenciamentoprodutosBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalgerenciamentoprodutosDblClick( grid, Row, Col)
js_modalgerenciamentoprodutosKeyPress(KeyAscii)
js_modalgerenciamentoprodutos_AfterRowColChange
js_modalgerenciamentoprodutos_ValidateEdit()
js_modalgerenciamentoprodutos_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES
// function window_onload() - Inicio (Configura o html) *************
function window_onload()
{
    dealWithObjects_Load();
    dealWithGrid_Load();
    
	__adjustFramePrintJet();
	
    window_onload_1stPart();

    // ajusta o body do html
    with (modalnotificacoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
        style.width = 680;
    }

	// configuracao inicial do html
	setupPage();

	modalFrame = getFrameInHtmlTop('frameModal');
	modalFrame.style.top = 50;
	modalFrame.style.left = 140;

	btnOK.style;
	// mostra a janela modal com o arquivo carregado
	showExtFrame(window, true);
     
}
// function window_onload() - Fim ***********************************

// function setupPage() - Inicio (Configuracao inicial do html) *****
function setupPage()
{
    secText('Notificações', 1);
    
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 0;
    modalFrame.style.left = 150;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    eLeft = 2;
    eTop = 2;
    rQuote = 0;

    btnOK.disabled = true;
    btnCanc.disabled = true;

    with (btnOK.style) {
        //visibility = 'inherit';
        visibility = 'hidden';
    }

    with (btnCanc.style) {
        //visibility = 'inherit';
        visibility = 'hidden';
    }
    // ajusta o divNotificacoes
    with (divNotificacoes.style)
    {
        border = 'black';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = 0;
        top = 23;
        width = 717;    
        height = 515;
    }

       
     //glb_getServerData = 5;
    
}
// function setupPage() - Fim ***************************************






/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

/********************************************************************
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    if (glb_refreshGrid != null)
    {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }

    // @@ atualizar interface aqui
    setupBtnsFromGridState();
    
    // mostra a janela modal
    // fillGridData();
    showExtFrame(window, true);
}

/********************************************************************
Verifica se um dado campo existe em um dado dso

Parametros:
dso         - referencia ao dso
fldName     - nome do campo a verificar a existencia
                      
Retorno:
true se o campo existe, caso contrario false
********************************************************************/
function fieldExists(dso, fldName)
{
    var retVal = false;
    var i;
    
    for ( i=0; i< dso.recordset.Fields.Count(); i++ )
    {
        if ( dso.recordset.Fields[i].Name == fldName )
            retVal = true;
    }
    
    return retVal;
}



/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalgerenciamentoprodutosBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
{
	if ( Button != 1 )
		return true;
	
	glb_nLastStateClicked = Shift;
    return true;
}

function getCurrDate()
{
   var d, s = "";
   d = new Date();
   
   if (DATE_SQL_PARAM == 103)
   {
		s += padL(d.getDate().toString(),2,'0') + "/";
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
   }
   else
   {
		s += padL((d.getMonth() + 1).toString(),2,'0') + "/";
		s += padL(d.getDate().toString(),2,'0') + "/";
   }
   
   s += d.getYear() + " ";
   s += padL(d.getHours().toString(),2,'0') + ":";
   s += padL(d.getMinutes().toString(),2,'0');

   return(s);
}

function adjustLabelsCombos()
{
    setLabelOfControl(lblFornecedor, selFornecedor.value);    
}

function window_onunload()
{
    dealWithObjects_Unload();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    
    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id)
        btnCanc.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);
}

