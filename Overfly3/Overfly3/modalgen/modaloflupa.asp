<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
        
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
        
    Set objSvrCfg = Nothing
%>

<html id="modaloflupaHtml" name="modaloflupaHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'Captura o filePos (INF ou SUP), o labelToUse, formID, comboID
'formID e comboID vao voltar

Dim i, filePos, labelToUse, formID, comboID

For i = 1 To Request.QueryString("filePos").Count    
    filePos = Request.QueryString("filePos")(i)
Next

Response.Write "var glb_filePos = '" & CStr(filePos) & "';"
Response.Write vbcrlf

For i = 1 To Request.QueryString("labelToUse").Count    
    labelToUse = Request.QueryString("labelToUse")(i)
Next

Response.Write "var glb_labelToUse = '" & CStr(labelToUse) & "';"
Response.Write vbcrlf

For i = 1 To Request.QueryString("formID").Count    
    formID = Request.QueryString("formID")(i)
Next

Response.Write "var glb_formID = " & CStr(formID) & ";"
Response.Write vbcrlf

For i = 1 To Request.QueryString("comboID").Count    
    comboID = Request.QueryString("comboID")(i)
Next

Response.Write "var glb_comboID = '" & CStr(comboID) & "';"
Response.Write vbcrlf

'Necessario para compatibilidade da automacao
Response.Write "var glb_USERID = 0;"

Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
        
    // ajusta o body do html
    with (modaloflupaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	// coloca foco no campo Pesquisa
	window.focus();
	txtPesquisa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Caption da janela modal
    secText('Preencher Combo', 1);
        
	var tempLeft = ELEM_GAP;
    var elem;

    // divExtPesquisa
    elem = window.document.getElementById('divExtPesquisa');
    with (elem.style)
    {
        visibility = 'visible';
        backgroundColor = 'transparent';
        width = 300;
        height = 50;
        top = 50;
        left = 19;
        tempLeft = parseInt(left, 10);
    }

    // divPesquisa
    elem = window.document.getElementById('divPesquisa');
    with (elem.style)
    {
        backgroundColor = 'transparent';
        width = FONT_WIDTH * 20;
        height = 40;
        top = 10;
        left = ((205 - 150)/ 2);
        tempLeft = parseInt(left, 10) +  parseInt(width, 10) +  ELEM_GAP
    }

    // txtPesquisa
    elem = window.document.getElementById('txtPesquisa');
    elem.disabled = false;
    with (elem.style)
    {
        width = FONT_WIDTH * 21 + 3;
        elem.maxLength = 20;
    }
    
    lblPesquisa.innerText = glb_labelToUse;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    var param1 = 'SUP';
    
    if ( glb_filePos == 'SUP' )
        param1 = 'S';
    else if ( glb_filePos == 'INF' )
        param1 = 'I';
    
    // esta funcao trava o html contido na janela modal
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        lockControlsInModalWin(true);

        sendJSMessage( getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + param1,
                       new Array( document.getElementById('txtPesquisa').value,
                                  glb_comboID, glb_formID ) );
    }
    else
        sendJSMessage( getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + param1, null );
}

//-->
</script>

</head>

<body id="modaloflupaBody" name="modaloflupaBody" LANGUAGE="javascript" onload="return window_onload()">
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <div id="divExtPesquisa" name="divExtPesquisa" class="divExternPesq">
        <div id="divPesquisa" name="divPesquisa" class="divGeneralPesq">
            <p id="lblPesquisa" name="lblPesquisa" class="lblGeneralPesq">Sujeito</p>
            <input type="text" id="txtPesquisa" name="txtPesquisa" class="fldGeneralPesq">
		</div>
	</div>		
</body>

</html>
