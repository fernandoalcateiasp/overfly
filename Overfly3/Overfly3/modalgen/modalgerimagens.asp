<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalgerimagensHtml" name="modalgerimagensHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modalgerimagens.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/modalgerimagens.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf

Response.Write "var glb_USERID = 0;"
Response.Write vbcrlf

Dim i, nRegistroID, nSubFormID, nTipoRegistroID, sCaller

nRegistroID = 0
nSubFormID = 0
nTipoRegistroID = 0
sCaller = ""

For i = 1 To Request.QueryString("nRegistroID").Count    
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

For i = 1 To Request.QueryString("nSubFormID").Count    
    nSubFormID = Request.QueryString("nSubFormID")(i)
Next

For i = 1 To Request.QueryString("nTipoRegistroID").Count    
    nTipoRegistroID = Request.QueryString("nTipoRegistroID")(i)
Next

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

Response.Write "var glb_nRegistroID=" & CStr(nRegistroID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoRegistroID=" & CStr(nTipoRegistroID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nSubFormID=" & CStr(nSubFormID) & ";"
Response.Write vbcrlf

Response.Write "var glb_sCaller='" & sCaller & "';"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>
<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<SCRIPT LANGUAGE=javascript FOR=openFile EVENT=OnFileDblClkOrEnter>
<!--
 openFile_OnFileDblClkOrEnter();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=openFile EVENT=OnSaveFileSuccess>
<!--
 openFile_OnSaveFileSuccess();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=openFile EVENT=OnSaveFileError>
<!--
 openFile_OnSaveFileError(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=openFile EVENT=OnEraseFileSuccess>
<!--
 openFile_OnEraseFileSuccess();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=openFile EVENT=OnEraseFileError>
<!--
 openFile_OnEraseFileError(arguments[0]);
//-->
</SCRIPT>

</head>

<body id="modalgerimagensBody" name="modalgerimagensBody" LANGUAGE="javascript" onload="return window_onload()">
	<p id="lblOverOpenFile" name="lblOverOpenFile" class="lblGeneral">Explorar no disco</p>

    <!-- Object Open File -->
    <OBJECT CLASSID="clsid:7DCF9BCC-7272-491F-8D43-04E37D30DB61" ID="openFile" HEIGHT="0" WIDTH="0" VIEWASTEXT>
    </OBJECT>

	<p id="lblOrigemImagem" name="lblOrigemImagem" class="lblGeneral">Imagem do Banco</p>
	<p id="lblRegistroSemImagem" name="lblRegistroSemImagem" class="lblGeneral">N�o tem</p>
	<IMG ID="img_Imagem" name="img_Imagem" class="imgGeneral" Language=javascript onload="return img_onload()" onerror="return img_onerror()"></IMG>

	<IMG ID="img_Imagem_Dims" name="img_Imagem_Dims" class="imgGeneral" Language=javascript onload="return imgDims_onload()" onerror="return imgDims_onerror()"></IMG>
	
	<p id="lblRelatorioImagem" name="lblRelatorioImagem" class="lblGeneral"></p>

    <input type="button" id="btnEditarVizualizar" name="btnEditarVizualizar" value="" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnApagar" name="btnApagar" value="Apagar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    <input type="button" id="btnRefresh" name="btnRefresh" value="Refresh" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    
    <p id="lblTamanhoImagem" name="lblTamanhoImagem" class="lblGeneral" title="Tamanho da imagem">Tamanho</p>
	<select id="selTamanhoImagem" name="selTamanhoImagem"  class="fldGeneral">
	</select>

    <p id="lblOrdemImagem" name="lblOrdemImagem" class="lblGeneral" title="Ordem da imagem">Ordem</p>
	<select id="selOrdemImagem" name="selOrdemImagem"  class="fldGeneral">
	</select>
	
	<input type="button" id="btnResumo" name="btnResumo" value="Resumo" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
	
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
