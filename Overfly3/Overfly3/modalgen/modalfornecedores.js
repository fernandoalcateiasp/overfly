/********************************************************************
modalfornecedores.js

Library javascript para o modalfornecedores.asp
********************************************************************/

// VARIAVEIS GLOBAIS - Inicio ***************************************

var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_dataGridWasChanged = false;  // Controla se algum dado do grid foi alterado
var glb_getServerData = 0;
var glb_sFiltroDireitos = '';
var glb_sReadOnly = '';
var glb_sCustosReadOnly = '*';
var glb_refreshGrid = null;
var glb_refreshGrid2 = null;
var glb_timerUnloadWin = null;
var glb_nDOSsCmbs = 0;
var glb_divFGExtended = 0;
var glb_divFGCollapse = 0;
var glb_divFG1Fornecedor = 258;
var glb_divFG2Fornecedor = 90;
var glb_bDivFGIsCollapse = false;
var glb_PassedOneInDblClick = false;
var glb_EnableChkOK = true;
var glb_nLastStateClicked = null;
var glb_nMoedaFaturamentoID = 0;
var glb_sMoeda = '';
var glb_bFillGridAfterImport = false;
var glb_nTopGrid = 0;
var glb_nRegistros = 0;
var glb_sFiltroContexto = '';
var glb_nProprietarioID = 0;
var glb_nAlternativoID = 0;
var glb_sMetodoPesquisa = '';
var glb_sChavePesquisa = '';
var glb_sChavePesquisa2 = '';
var glb_sArgumento = '';
var glb_sOperator = '';
var glb_sPesquisa = '';
var glb_sFiltro = '';
var glb_sInvertido = '';
var glb_sFiltroEspecifico = '';
var glb_sFiltroFamilia = '';
var glb_sFiltroMarca = '';
var glb_sFiltroLinha = '';
var glb_sFiltroProprietario = '';
var glb_sOrdem = '';
var glb_nValue = 0;
var glb_nFocusLinha = 0;



var dsoGrid = new CDatatransport("dsoGrid");
var dsoCmbMoeda = new CDatatransport("dsoCmbMoeda");
var dsoFiltroEspecifico = new CDatatransport("dsoFiltroEspecifico");
var dsoFamilia = new CDatatransport("dsoFamilia");
var dsoMarca = new CDatatransport("dsoMarca");
var dsoLinha = new CDatatransport("dsoLinha");
var dsoPrevisaoVendas = new CDatatransport("dsoPrevisaoVendas");
var dsoCmbAtualizacao = new CDatatransport("dsoCmbAtualizacao");
var dsoDireitos = new CDatatransport("dsoDireitos");
var dsoFornecedores = new CDatatransport("dsoFornecedores");
var dsoClassFiscal = new CDatatransport("dsoClassFiscal");
var dsoImpostos = new CDatatransport("dsoImpostos");
var dsoTransacao = new CDatatransport("dsoTransacao");
var dsoPrazo = new CDatatransport("dsoPrazo");
var dsoGrava = new CDatatransport("dsoGrava");
var dsoPortoOrigem = new CDatatransport("dsoPortoOrigem");
var dsoViaTransporte = new CDatatransport("dsoViaTransporte");
// VARIAVEIS GLOBAIS - Fim ******************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
cmb_onchange()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
getCurrEmpresaData()
setupBtnsFromGridState()
fillGridData()
fillGridData_DSC()
saveDataInGrid()
saveDataInGrid_DSC()
fieldExists(dso, fldName)

EVENTOS DE GRID

DEFINIDAS NO ARQUIVO modalfornecedores.ASP
Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    fg_AfterRowColChange()
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub
FINAL DE DEFINIDAS NO ARQUIVO modalfornecedores.ASP

js_fg_modalfornecedoresBeforeMouseDown(fg, Button, Shift, X, Y, Cancel)
js_fg_modalfornecedoresDblClick( grid, Row, Col)
js_modalfornecedoresKeyPress(KeyAscii)
js_modalfornecedores_AfterRowColChange
js_modalfornecedores_ValidateEdit()
js_modalfornecedores_AfterEdit(Row, Col)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// function window_onload() - Inicio (Configura o html) *************
function window_onload() {
    dealWithObjects_Load();
    dealWithGrid_Load();

    //__adjustFramePrintJet();

    window_onload_1stPart();

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    glb_nRegistros = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    glb_nProprietarioID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selProprietariosPL.value');

    // glb_nAlternativoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selAlternativo.value');
    glb_nAlternativoID = 0;  // Ativar a linha de cima

    // glb_sFiltroContexto = getCmbCurrDataInControlBar('sup', 1);
    glb_sFiltroContexto = '(a.TipoRelacaoID = 61)';  // Ativar a linha de cima CRIAR DSO PARA EXECUTAR O FILTRO

    glb_sMetodoPesquisa = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selMetodoPesq.value');

    glb_sChavePesquisa = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selPesquisa.value');

    if (glb_sChavePesquisa == 'aRelacaoID')
        glb_sChavePesquisa = 'a.RelacaoID';
    else if (glb_sChavePesquisa == 'aSujeitoID')
        glb_sChavePesquisa = 'a.SujeitoID';
    else if (glb_sChavePesquisa == 'dFantasia')
        glb_sChavePesquisa = '(SELECT Fantasia FROM Pessoas WITH(NOLOCK) WHERE (PessoaID = a.SujeitoID))';
    else if (glb_sChavePesquisa == 'aObjetoID')
        glb_sChavePesquisa = 'a.ObjetoID';
    else if (glb_sChavePesquisa == 'eConceito') {
        glb_sChavePesquisa = '(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ObjetoID))';
        glb_sChavePesquisa2 = 'a.ObjetoID';
    }

    glb_sArgumento = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'txtArgumento.value');

    if (glb_sArgumento.length > 0) {
        if (glb_sMetodoPesquisa == 'DEF') {
            if ((glb_sArgumento.substring(0, 1) == '%') || (glb_sArgumento.substring(glb_sArgumento.length, 1) == '%'))
                glb_sOperator = 'LIKE';
            else {
                if (glb_sOrdem == 'DESC')
                    glb_sOperator = '<=';
                else
                    glb_sOperator = '>=';
            }

            glb_sArgumento = ('\'' + glb_sArgumento + '\'');
        }
        else {
            glb_sOperator = 'LIKE';

            if (glb_sMetodoPesquisa == 'COM')
                glb_sArgumento = ('\'' + glb_sArgumento + '%\'');
            else if (glb_sMetodoPesquisa == 'TERM')
                glb_sArgumento = ('\'%' + glb_sArgumento + '\'');
            else if (glb_sMetodoPesquisa == 'CONT')
                glb_sArgumento = ('\'%' + glb_sArgumento + '%\'');
        }

        if (glb_sChavePesquisa == '(SELECT Conceito FROM Conceitos WITH(NOLOCK) WHERE (ConceitoID = a.ObjetoID))') {
            glb_sPesquisa = (' AND ((' + glb_sChavePesquisa + ' ' + glb_sOperator + ' ' + glb_sArgumento + ') OR ' +
            '(' + glb_sChavePesquisa2 + ' ' + glb_sOperator + ' ' + glb_sArgumento + ')) ');
        }
        else
            glb_sPesquisa = (' AND (' + glb_sChavePesquisa + ' ' + glb_sOperator + ' ' + glb_sArgumento + ') ');
    }

    glb_sFiltro = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'txtFiltro.value');
    glb_sFiltro = (glb_sFiltro == '' ? '' : (' AND (' + glb_sFiltro + ') '));


    glb_sInvertido = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'chkOrdem.checked');
    glb_sOrdem = (glb_sInvertido ? 'DESC' : '');

    // ajusta o body do html
    with (modalfornecedoresBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
}
// function window_onload() - Fim ***********************************

// function setupPage() - Inicio (Configuracao inicial do html) *****
function setupPage() {

    secText('Dados de Fornecedores', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 2;
    var eTop = 1;
    var rQuote = 0;
    var btnWidth = 78;
    var aEmpresa = getCurrEmpresaData();

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    eLeft = 2;
    eTop = 2;
    rQuote = 0;


    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 52;
    modalFrame.style.left = 0;

    // desabilita o botao OK
    btnOK.disabled = true;

    // Ajusta elementos no divControls	

    adjustElementsInForm([
	        ['lblPesquisa', 'chkPesquisa', 3, 1, -7],
	        ['lblServico', 'selServico', 19, 1, -2],
	        ['lblFiltroEspecifico', 'selFiltroEspecifico', 19, 1, -4],
	        ['lblColunas', 'chkColunas', 3, 1, -6],
    	    ['lblCampos', 'chkCampos', 3, 1, -6],
            ['lblPercentualPV', 'txtPercentualPV', 4, 2, -2],
            ['lblFornecedor', 'selFornecedor', 20, 2],
            ['lblTransacao', 'selTransacao', 9, 2],
            ['lblFinanciamento', 'selFinanciamento', 17, 2]
            //['btnComprar','btn',btnOK.offsetWidth - FONT_WIDTH,2,-219]
    ], null, null, true);

    adjustElementsInForm([['lblFornecedor2', 'selFornecedor2', 13, 1, -10],
						  ['lblImpostos', 'chkImpostos', 3, 1, -4],
						  ['lblRetCustoReposicao', 'chkRetCustoReposicao', 3, 1, -7],
						  ['lblModelo', 'chkModelo', 3, 1, -4],
						  ['lblPartNumber', 'chkPartNumber', 3, 1, -3],
						  ['lbldtAtualizacao', 'seldtAtualizacao', 12, 1, -5],
						  ['lblDataSemelhante', 'chkDataSemelhante', 3, 1, -2]]);

    lblImpostos.style.visibility = 'inherit';
    chkImpostos.style.visibility = 'inherit';
    chkImpostos.onclick = chkImpostos_onclick;
    lblRetCustoReposicao.style.visibility = 'inherit';
    chkRetCustoReposicao.style.visibility = 'inherit';
    chkRetCustoReposicao.onclick = chkRetCustoReposicao_onclick;
    lblFornecedor2.style.visibility = 'inherit';
    selFornecedor2.style.visibility = 'inherit';
    selFornecedor2.onchange = selFornecedor2_onchange;
    lblModelo.style.visibility = 'inherit';
    chkModelo.style.visibility = 'inherit';
    chkModelo.checked = true;
    chkModelo.onclick = showHideModeloPartNumber;
    lblPartNumber.style.visibility = 'inherit';
    chkPartNumber.style.visibility = 'inherit';
    chkPartNumber.onclick = showHideModeloPartNumber;
    lbldtAtualizacao.style.visibility = 'inherit';
    seldtAtualizacao.style.visibility = 'inherit';
    seldtAtualizacao.onchange = seldtAtualizacao_onchange;
    seldtAtualizacao.disabled = true;
    lblDataSemelhante.style.visibility = 'inherit';
    chkDataSemelhante.style.visibility = 'inherit';
    chkDataSemelhante.disabled = true;
    chkDataSemelhante.onclick = chkDataSemelhante_onclick;
    selFamilia.style.visibility = 'hidden';
    selMarca.style.visibility = 'hidden';
    selLinha.style.visibility = 'hidden';

    lblColunas.style.visibility = 'hidden';
    chkColunas.style.visibility = 'hidden';
    chkColunas.onclick = showHideCols;
    btnComprar.style.height = btnOK.offsetHeight;
    btnComprar.style.width = 64;
    lblPercentualPV.style.visibility = 'hidden';
    txtPercentualPV.style.visibility = 'hidden';
    txtPercentualPV.onkeypress = verifyNumericEnterNotLinked;
    txtPercentualPV.onkeyup = txtPercentualPV_onkeyup;
    txtPercentualPV.setAttribute('verifyNumPaste', 1);
    txtPercentualPV.setAttribute('thePrecision', 3, 1);
    txtPercentualPV.setAttribute('theScale', 0, 1);
    txtPercentualPV.setAttribute('minMax', new Array(0, 999), 1);
    txtPercentualPV.onfocus = selFieldContent;
    txtPercentualPV.maxLength = 3;
    txtPercentualPV.value = '100';

    selFiltroEspecifico.onchange = limpaGrid;
    selServico.onchange = selServico_onchange;
    chkPesquisa.onclick = chkPesquisa_onclick;
    selFamilia.onchange = selFamilia_onchange;
    selMarca.onchange = selMarca_onchange;
    btnCalcularPV.onclick = btnCalcularPV_onclick;

    btnExportar.style.height = btnComprar.offsetHeight;
    btnExportar.style.width = 64;
    btnExportar.style.visibility = 'inherit';

    //btnImportar.style.left = btnComprar.offsetLeft - ((5 + btnComprar.offsetWidth) * 2);
    //btnImportar.style.top = btnComprar.offsetTop;
    btnImportar.style.height = btnComprar.offsetHeight;
    btnImportar.style.width = 64;
    btnImportar.style.visibility = 'inherit';

    //btnImprimir.style.left = btnComprar.offsetLeft;
    //btnImprimir.style.top = btnComprar.offsetTop;
    btnImprimir.style.height = btnComprar.offsetHeight;
    btnImprimir.style.width = 64;

    //btnExcel.style.left = btnComprar.offsetLeft - 5 - btnComprar.offsetWidth;
    //btnExcel.style.top = btnComprar.offsetTop;
    btnExcel.style.height = btnComprar.offsetHeight;
    btnExcel.style.width = 64;

    lblCampos.style.visibility = 'hidden';
    chkCampos.style.visibility = 'hidden';
    chkCampos.checked = false;
    lblCampos.onclick = lblCampos_onclick;
    chkCampos.onclick = chkCampos_onclick;

    //Pesquisa
    lblFamilia.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selFamilia.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selFamilia.disabled = true;
    lblMarca.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selMarca.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selMarca.disabled = true;
    lblLinha.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selLinha.style.visibility = ((!chkPesquisa.checked) ? 'hidden' : 'inherit');
    selLinha.disabled = true;

    lblServico.style.visibility = 'hidden';
    selServico.style.visibility = 'hidden';
    lblFiltroEspecifico.style.visibility = 'hidden';
    selFiltroEspecifico.style.visibility = 'hidden';
    lblPesquisa.style.visibility = 'hidden';
    chkPesquisa.style.visibility = 'hidden';
    lblFornecedor.style.visibility = 'hidden';
    selFornecedor.style.visibility = 'hidden';
    lblFinanciamento.style.visibility = 'hidden';
    selFinanciamento.style.visibility = 'hidden';
    lblTransacao.style.visibility = 'hidden';
    selTransacao.style.visibility = 'hidden';

    // ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10);
        width = modWidth - 2 * ELEM_GAP - 6;
        height = selServico.offsetTop + selServico.offsetHeight;
    }

    selTransacao.disabled = true;
    //selTransacao.onchange = selTransacao_onchange;

    selFinanciamento.disabled = true;

    selFornecedor.disabled = true;
    selFornecedor.onchange = selFornecedor_onchange;

    // Aqui ajusta os campos nos divs (funcao adjustElementsInForm)	

    adjustElementsInForm([['lblCustoMedio', 'txtCustoMedio', 11, 1, -10, -10],
						  ['lblCustoReposicao', 'txtCustoReposicao', 11, 1],
                          ['lblUltimaEntrada', 'txtUltimaEntrada', 10, 1, 10],
                          ['lblUltimaSaida', 'txtUltimaSaida', 10, 1],
                          ['lblDias', 'txtDias', 4, 1],
                          ['lblDisponibilidade', 'txtDisponibilidade', 10, 1],
                          ['lblEstoqueWeb', 'txtEstoqueWeb', 6, 1, -10]], null, null, true);

    // alignButtons();
    // A altura do div � funcao do campo mais baixo
    // trocar o numero duro abaixo
    var divsInfHeight = txtEstoqueWeb.offsetTop + txtEstoqueWeb.offsetHeight;
    var divsInfBottom = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10);
    var divInfTop = divsInfBottom - divsInfHeight;

    //divControlsGerProd
    with (divControlsGerProd.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divInfTop;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = divsInfHeight;
        visibility = 'hidden';
    }

    //divControlsFornec
    with (divControlsFornec.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divInfTop;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = divsInfHeight;
        visibility = 'hidden';
    }

    //As duas alturas do divFG
    var divFG_top = divControls.offsetTop + divControls.offsetHeight + ELEM_GAP;
    glb_nTopGrid = divFG_top;
    glb_divFGExtended = Math.abs(parseInt(btnOK.currentStyle.top, 10) +
	                             parseInt(btnOK.currentStyle.height, 10) - divFG_top);
    glb_divFGCollapse = glb_divFGExtended - divsInfHeight - ELEM_GAP;

    // ajusta o divFG
    with (divFG.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFG_top;
        width = modWidth - 2 * ELEM_GAP - 6;
        // height = parseInt(btnOK.currentStyle.top, 10) + parseInt(btnOK.currentStyle.height, 10) - parseInt(top, 10);
        height = glb_divFGExtended;
    }

    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) - 2;
        height = parseInt(divFG.style.height, 10) - 2;
    }

    // ajusta o divFG2
    with (divFG2.style) {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = divFG_top;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = glb_divFG2Fornecedor;
        visibility = 'hidden';
    }

    with (fg2.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10) - 2;
        height = parseInt(divFG2.style.height, 10) - 2;
    }

    /*hr_L_FGBorder.style.visibility = 'hidden';
    hr_R_FGBorder.style.visibility = 'hidden';
    hr_B_FGBorder.style.visibility = 'hidden';*/

    collapseExtendedDivFG();

    with (divUpload.style) {
        border = 'solid';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = parseInt(divControls.style.left, 10);
        top = parseInt(divControls.style.top, 10);
        width = parseInt(divControls.style.width, 10);
        height = parseInt(divControls.style.height, 10);
    }

    divUpload.style.visibility = 'hidden';
    var iframe = document.getElementById("I1");
    iframe.style.width = 0;

    // Posiciona botao Cancelar
    with (btnCancelar) {
        style.top = 18.5;
        style.width = 60;
        style.height = 21;
        left = parseInt(divControls.style.left, 10);
    }

    btnCancelar.onclick = btnCancelar_onclick;

    // Esconde o botao fechar
    btnCanc.style.visibility = 'hidden';

    // Muda o botao OK de posicao
    btnComprar.insertAdjacentElement('AfterEnd', btnOK);
    with (btnOK.style) {
        //left = btnComprar.offsetLeft + btnComprar.offsetWidth + 5;
        //top = btnComprar.offsetTop;
        width = 64;
    }

    btnOK.insertAdjacentElement('AfterEnd', btnListar);
    with (btnListar.style) {
        width = 64;
        height = btnOK.offsetHeight;
        //left = btnOK.offsetLeft + btnOK.offsetWidth + 5;
        //top = btnOK.offsetTop;
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;
    fg2.Redraw = 2;

    glb_getServerData = 7;

    setConnection(dsoCmbMoeda);
    dsoCmbMoeda.SQL = 'SELECT c.ConceitoID AS fldID, c.SimboloMoeda AS fldName, c.SimboloMoeda AS Ordem, b.Faturamento, c.SimboloMoeda AS Moeda ' +
        'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Moedas b WITH(NOLOCK), Conceitos c WITH(NOLOCK) ' +
        'WHERE a.SujeitoID = ' + aEmpresa[0] + ' ' +
        'AND a.ObjetoID = 999 AND a.TipoRelacaoID = 12 ' +
        'AND a.RelacaoID = b.RelacaoID AND b.MoedaID = c.ConceitoID';

    dsoCmbMoeda.ondatasetcomplete = dsoServerData_DSC;
    dsoCmbMoeda.Refresh();

    setConnection(dsoDireitos);
    dsoDireitos.SQL = 'SELECT TOP 1 Direitos.Alterar1, Direitos.Alterar2 ' +
	    'FROM RelacoesPesRec RelPesRec WITH(NOLOCK), RelacoesPesRec_Perfis RelPesRecPerfis WITH(NOLOCK), Recursos Perfis WITH(NOLOCK), Recursos_Direitos Direitos WITH(NOLOCK) ' +
	    'WHERE (RelPesRec.SujeitoID IN ((SELECT ' + glb_nUserID +
			' UNION ALL SELECT UsuarioDeID FROM DelegacaoDireitos WHERE (UsuarioParaID = ' + glb_nUserID + ' AND GETDATE() BETWEEN dtInicio AND dtFim))) AND ' +
			'RelPesRec.ObjetoID = 999 AND ' +
	    	'RelPesRec.TipoRelacaoID = 11 AND RelPesRec.RelacaoID = RelPesRecPerfis.RelacaoID AND  ' +
	    	'RelPesRecPerfis.EmpresaID = ' + aEmpresa[0] + ' AND RelPesRecPerfis.PerfilID = Perfis.RecursoID AND  ' +
	    	'Perfis.EstadoID = 2 AND Perfis.RecursoID = Direitos.PerfilID AND Direitos.RecursoMaeID = 2131 AND ' +
	    	'Direitos.RecursoID = ' + (glb_nModalType == 9 ? '21072' : '21065') + ') ' +
	    'ORDER BY Direitos.Alterar2 DESC, Direitos.Alterar1 DESC';

    dsoDireitos.ondatasetcomplete = dsoServerData_DSC;
    dsoDireitos.Refresh();

    setConnection(dsoFornecedores);
    dsoFornecedores.SQL = 'SELECT 0 AS fldID, SPACE(1) AS fldName ' +
		'UNION ALL ' +
		'SELECT b.PessoaID AS fldID, b.Fantasia AS fldName ' +
		'FROM RelacoesPesCon_Fornecedores a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
		'WHERE (a.FornecedorID = b.PessoaID) AND a.RelacaoID IN (0';

    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');

    aGrid = (aGrid == null ? new Array() : aGrid);
    for (i = 0; i < aGrid.length; i++) {
        dsoFornecedores.SQL = dsoFornecedores.SQL + aGrid[i];

        if (i != (aGrid.length - 1))
            dsoFornecedores.SQL = dsoFornecedores.SQL + ',';
    }

    dsoFornecedores.SQL = dsoFornecedores.SQL + ') ';
    dsoFornecedores.SQL = dsoFornecedores.SQL +
		'GROUP BY b.PessoaID, b.Fantasia ' +
		'ORDER BY fldName';

    dsoFornecedores.ondatasetcomplete = dsoServerData_DSC;
    dsoFornecedores.Refresh();

    setConnection(dsoCmbAtualizacao);
    dsoCmbAtualizacao.SQL = selectString(false, true);
    dsoCmbAtualizacao.ondatasetcomplete = dsoServerData_DSC;
    dsoCmbAtualizacao.Refresh();

    setConnection(dsoClassFiscal);
    dsoClassFiscal.SQL = 'SELECT 0 AS ConceitoID, SPACE(1) AS Conceito ' +
        'UNION ALL ' +
        'SELECT ConceitoID, Conceito ' +
        'FROM Conceitos WITH(NOLOCK) ' +
        'WHERE EstadoID IN (2,4) AND TipoConceitoID=309 ' +
        'ORDER BY Conceito';

    dsoClassFiscal.ondatasetcomplete = dsoServerData_DSC;
    dsoClassFiscal.Refresh();

    setConnection(dsoPortoOrigem);
    dsoPortoOrigem.SQL = 'SELECT 0 AS PortoOrigemID, SPACE(1) AS PortoOrigem ' +
                            'UNION ALL ' +
                         'SELECT DISTINCT a.PessoaID AS PortoOrigemID, a.Fantasia AS PortoOrigem ' +
                            'FROM Pessoas a WITH(NOLOCK) ' +
                                'INNER JOIN Importacao b WITH(NOLOCK) ON (b.PortoOrigemID = a.PessoaID) ' +
                            'WHERE a.EstadoID = 2 ' +
                            'ORDER BY PortoOrigem';
    dsoPortoOrigem.ondatasetcomplete = dsoServerData_DSC;
    dsoPortoOrigem.Refresh();

    setConnection(dsoViaTransporte);
    dsoViaTransporte.SQL = 'SELECT 0 AS ViaTransporteID, SPACE(1) AS ViaTransporte ' +
                         'UNION ALL ' +
                         'SELECT ItemID AS ViaTransporteID, ItemMasculino AS ViaTransporte ' +
                            'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                            'WHERE TipoID = 405 and EstadoID = 2 and filtro like \'%<I>%\' ' +
                            'ORDER BY ViaTransporte';
    dsoViaTransporte.ondatasetcomplete = dsoServerData_DSC;
    dsoViaTransporte.Refresh();

    chkCampos_onclick();

    alignButtons();
}
// function setupPage() - Fim ***************************************

function chkPesquisa_onclick() {
    if (chkPesquisa.checked) {
        // fg.rows = 1;        

        adjustElementsInForm([
	            ['lblPesquisa', 'chkPesquisa', 3, 1, -7],
	            ['lblFamilia', 'selFamilia', 19, 1, -2],
	            ['lblMarca', 'selMarca', 19, 1, -2],
	            ['lblLinha', 'selLinha', 19, 1, -2]], null, null, true);

        alignButtons();

        lblServico.style.visibility = 'hidden';
        selServico.style.visibility = 'hidden';
        lblFiltroEspecifico.style.visibility = 'hidden';
        selFiltroEspecifico.style.visibility = 'hidden';
        lblColunas.style.visibility = 'hidden';
        chkColunas.style.visibility = 'hidden';
        lblCampos.style.visibility = 'hidden';
        chkCampos.style.visibility = 'hidden';
        lblFamilia.style.visibility = 'inherit';
        selFamilia.style.visibility = 'inherit';
        lblMarca.style.visibility = 'inherit';
        selMarca.style.visibility = 'inherit';
        lblLinha.style.visibility = 'inherit';
        selLinha.style.visibility = 'inherit';

        preencheFamilia();
        preencheMarca();
    }
    else {
        // fg.rows = 1;               

        lblServico.style.visibility = 'inherit';
        selServico.style.visibility = 'inherit';
        lblFiltroEspecifico.style.visibility = 'inherit';
        selFiltroEspecifico.style.visibility = 'inherit';
        lblColunas.style.visibility = 'inherit';
        chkColunas.style.visibility = 'inherit';
        lblCampos.style.visibility = 'inherit';
        chkCampos.style.visibility = 'inherit';
        lblFamilia.style.visibility = 'hidden';
        selFamilia.style.visibility = 'hidden';
        lblMarca.style.visibility = 'hidden';
        selMarca.style.visibility = 'hidden';
        lblLinha.style.visibility = 'hidden';
        selLinha.style.visibility = 'hidden';

        adjustElementsInForm([
	        ['lblPesquisa', 'chkPesquisa', 3, 1, -7],
	        ['lblServico', 'selServico', 19, 1, -2],
	        ['lblFiltroEspecifico', 'selFiltroEspecifico', 19, 1, -2],
	        ['lblColunas', 'chkColunas', 3, 1, -2],
    	    ['lblCampos', 'chkCampos', 3, 1, -7],
            ['lblPercentualPV', 'txtPercentualPV', 4, 2, -2],
            ['lblFornecedor', 'selFornecedor', 20, 2],
            ['lblTransacao', 'selTransacao', 9, 2],
            ['lblFinanciamento', 'selFinanciamento', 17, 2]
        ], null, null, true);

        alignButtons();
    }
}

function alignButtons() {

    if (glb_nModalType == 10) {
        var nTopStart = chkDataSemelhante.offsetTop;
        var nNextLeft = (lblDataSemelhante.offsetLeft + lblCampos.offsetWidth + 38);

        btnComprar.style.visibility = 'hidden';
        btnCalcularPV.style.visibility = 'hidden';

        btnListar.style.visibility = 'inherit';
        btnListar.style.top = nTopStart;
        btnListar.style.left = nNextLeft;
        nNextLeft = btnListar.offsetLeft + btnListar.offsetWidth + 5;

        btnOK.style.visibility = 'inherit';
        btnOK.style.top = nTopStart;
        btnOK.style.left = nNextLeft;
        nNextLeft = btnOK.offsetLeft + btnOK.offsetWidth + 3;

        btnExportar.style.visibility = 'inherit';
        btnExportar.style.top = nTopStart;
        btnExportar.style.left = nNextLeft;
        nNextLeft = btnExportar.offsetLeft + btnExportar.offsetWidth + 3;

        btnImportar.style.visibility = 'inherit';
        btnImportar.style.top = nTopStart;
        btnImportar.style.left = nNextLeft;
        nNextLeft = btnImportar.offsetLeft + btnImportar.offsetWidth + 3;

        btnExcel.style.visibility = 'inherit';
        btnExcel.style.top = nTopStart;
        btnExcel.style.left = nNextLeft;
        nNextLeft = btnExcel.offsetLeft + btnExcel.offsetWidth + 3;

        btnImprimir.style.visibility = 'inherit';
        btnImprimir.style.top = nTopStart;
        btnImprimir.style.left = nNextLeft;
        nNextLeft = btnImprimir.offsetLeft + btnImprimir.offsetWidth + 3;
    }
    else {
        var nTopStart = chkDataSemelhante.offsetTop;
        var nNextLeft = (lblDataSemelhante.offsetLeft + lblCampos.offsetWidth + 38);

        btnComprar.style.visibility = 'hidden';
        btnCalcularPV.style.visibility = 'hidden';

        btnExportar.style.visibility = 'inherit';
        btnExportar.style.top = nTopStart;
        btnExportar.style.left = nNextLeft;
        nNextLeft = btnExportar.offsetLeft + btnExportar.offsetWidth + 3;

        btnImportar.style.visibility = 'inherit';
        btnImportar.style.top = nTopStart;
        btnImportar.style.left = nNextLeft;
        nNextLeft = btnImportar.offsetLeft + btnImportar.offsetWidth + 3;

        btnExcel.style.visibility = 'inherit';
        btnExcel.style.top = nTopStart;
        btnExcel.style.left = nNextLeft;
        nNextLeft = btnExcel.offsetLeft + btnExcel.offsetWidth + 3;

        btnImprimir.style.visibility = 'inherit';
        btnImprimir.style.top = nTopStart;
        btnImprimir.style.left = nNextLeft;
        nNextLeft = btnImprimir.offsetLeft + btnImprimir.offsetWidth + 3;

        btnOK.style.visibility = 'inherit';
        btnOK.style.top = nTopStart;
        btnOK.style.left = nNextLeft;
        nNextLeft = btnOK.offsetLeft + btnOK.offsetWidth + 3;

        btnListar.style.visibility = 'inherit';
        btnListar.style.top = nTopStart;
        btnListar.style.left = nNextLeft;
        nNextLeft = btnListar.offsetLeft + btnListar.offsetWidth + 5;
    }
}

function chkImpostos_onclick() {
    collapseExtendedDivFG();
    if (chkImpostos.checked)
        fillGridImpostos();
}

function chkDataSemelhante_onclick() {
    fillGridData();
}

function chkRetCustoReposicao_onclick() {
    fillGridData();
}

function txtPercentualPV_onkeyup() {
    if (event.keyCode == 13)
        fillGridData();
}

function showHideModeloPartNumber() {
    fg.ColHidden(getColIndexByColKey(fg, 'Modelo*')) = !chkModelo.checked;
    fg.ColHidden(getColIndexByColKey(fg, 'PartNumber*')) = !chkPartNumber.checked;
}

function chkCampos_onclick() {
    collapseExtendedDivFG();
    alignButtons();
}

/********************************************************************
Mostra/esconde div inferior
********************************************************************/
function collapseExtendedDivFG() {
    var nRowLine = fg.Row;

    fg.Redraw = 0;

    if (glb_bDivFGIsCollapse) {
        divControlsGerProd.style.visibility = 'hidden';
        divControlsFornec.style.visibility = 'hidden';

        fg.style.height = glb_divFGExtended - 2;
        divFG.style.height = glb_divFGExtended;
        divFG2.style.visibility = 'hidden';
    }
    else {
        fg.style.height = glb_divFG1Fornecedor - 2;
        divFG.style.height = glb_divFG1Fornecedor;
        divFG2.style.top = divFG.offsetTop + divFG.offsetHeight + ELEM_GAP;
        divFG2.style.visibility = 'inherit';
        divControlsFornec.style.visibility = 'inherit';
    }

    if (fg.Rows > 2)
        fg.TopRow = nRowLine;

    fg.Redraw = 2;

    glb_bDivFGIsCollapse = !glb_bDivFGIsCollapse;
}

/********************************************************************
Retorno do servidor
********************************************************************/
function dsoServerData_DSC() {
    glb_getServerData--;

    if (glb_getServerData != 0)
        return null;

    if (!((dsoCmbMoeda.recordset.BOF) && (dsoCmbMoeda.recordset.EOF))) {
        while (!dsoCmbMoeda.recordset.EOF) {
            if (dsoCmbMoeda.recordset['Faturamento'].value) {
                glb_nMoedaFaturamentoID = dsoCmbMoeda.recordset['fldID'].value;
                glb_sMoeda = dsoCmbMoeda.recordset['Moeda'].value;
                break;
            }
            dsoCmbMoeda.recordset.MoveNext();
        }
        dsoCmbMoeda.recordset.MoveFirst();
    }

    var a1 = 0;
    var a2 = 0;

    fillCmbs();

    if (!(dsoDireitos.recordset.BOF && dsoDireitos.recordset.EOF)) {
        a1 = (dsoDireitos.recordset['Alterar1'].value == true ? 1 : 0);
        a2 = (dsoDireitos.recordset['Alterar2'].value == true ? 1 : 0);
    }


    if (a2 == 0 && a1 == 0) {
        glb_sReadOnly = '*';
        glb_sCustosReadOnly = '*';
    }
    else if (a2 == 0 && a1 == 1)
        glb_sFiltroDireitos = ' AND (SELECT TOP 1 ProprietarioID FROM RelacoesPesCon WHERE (RelacaoID = a.RelacaoID))= ' + glb_nUserID + ' ';
    else if (a2 == 1 && a1 == 0)
        glb_sFiltroDireitos = ' AND ' + glb_nUserID +
            ' =(CASE WHEN (SELECT TOP 1 ProprietarioID FROM RelacoesPesCon WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID))=' + glb_nUserID + ' ' +
                    'THEN (SELECT TOP 1 ProprietarioID FROM RelacoesPesCon WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID)) ' +
                    'WHEN (SELECT TOP 1 AlternativoID FROM RelacoesPesCon WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID))=' + glb_nUserID + ' ' +
                    'THEN (SELECT TOP 1 AlternativoID FROM RelacoesPesCon WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID)) ' +
                    'WHEN (SELECT TOP 1 AlternativoID FROM RelacoesPesCon WITH(NOLOCK) WHERE (RelacaoID = a.RelacaoID))= ' +
                        '(SELECT TOP 1 grupo.ObjetoID FROM RelacoesPessoas grupo WITH(NOLOCK) ' +
                                'WHERE grupo.TipoRelacaoID=34 AND grupo.EstadoID=2 ' +
                                'AND grupo.SujeitoID=' + glb_nUserID + ' ' +
                                'AND grupo.ObjetoID=(SELECT TOP 1 AlternativoID FROM RelacoesPesCon WHERE (RelacaoID = a.RelacaoID))) ' +
                    'THEN ' + glb_nUserID + ' ' +
                    'ELSE 0 ' +
                    'END) ';

    fillCmbAtualizacao();

    preencheFiltroEspecifico();

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

function fillCmbs() {
    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selFornecedor, selFornecedor2];
    var aDSOsDunamics = [dsoFornecedores, dsoFornecedores];

    clearComboEx(['selFornecedor', 'selFornecedor2']);

    for (i = 0; i < aCmbsDynamics.length; i++) {
        aDSOsDunamics[i].recordset.moveFirst();
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }

        aCmbsDynamics[i].disabled = (aCmbsDynamics[i].options.length == 0);
    }

    return null;
}

function fillCmbAtualizacao() {
    var i;
    var optionStr, optionValue;
    clearComboEx(['seldtAtualizacao']);

    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = '';
    seldtAtualizacao.add(oOption);

    dsoCmbAtualizacao.recordset.moveFirst();
    while (!dsoCmbAtualizacao.recordset.EOF) {
        if (dsoCmbAtualizacao.recordset['fldID'].value != null) {
            optionStr = dsoCmbAtualizacao.recordset['fldName'].value;
            optionValue = dsoCmbAtualizacao.recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            seldtAtualizacao.add(oOption);
        }

        dsoCmbAtualizacao.recordset.MoveNext();
    }

    seldtAtualizacao.disabled = (seldtAtualizacao.options.length == 1);

    if (glb_bFillGridAfterImport) {
        glb_bFillGridAfterImport = false;
        fillGridData();
    }

    return null;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        saveDataInGrid();
        glb_nFocusLinha = fg.Row;

    }
    else if (controlID == 'btnComprar') {
        btn_Comprar_Clicked();
    }
    else if (controlID == 'btnImprimir') {
        btn_Imprimir_Clicked();
    }
    else if (controlID == 'btnExcel') {
        btn_Excel_Clicked();
    }
    else if (controlID == 'btnListar') {
        fillGridData();
    }
    else if (controlID == 'btnExportar') {
        exportarPrecos();
    }
    else if (controlID == 'btnImportar') {
        importarPrecos();
    }
        // codigo privado desta janela
    else if (controlID == 'btnCanc') {
        var _retMsg;
        var sMsg = '';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        //restoreInterfaceFromModal();
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    if (glb_refreshGrid != null) {
        window.clearInterval(glb_refreshGrid);
        glb_refreshGrid = null;
    }

    // @@ atualizar interface aqui
    setupBtnsFromGridState();

    // mostra a janela modal
    fillGridData();
    showExtFrame(window, true);
}

/********************************************************************
Trava/destrava todos os botoes
********************************************************************/
function setupBtnsFromGridState() {
    var i;
    var bHasRowsInGrid = fg.Rows >= 2;

    btnOK.disabled = !(bHasRowsInGrid && glb_dataGridWasChanged);
    btnComprar.disabled = true;

    if ((glb_nModalType == 9) && (selFornecedor.options.length > 0 && selFornecedor.value != 0) &&
		(selTransacao.options.length > 0 && selTransacao.value != 0) && (selFinanciamento.options.length > 0 && selFinanciamento.value != 0)) {
        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0)
                btnComprar.disabled = false;
        }
    }
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData() {
    lockControlsInModalWin(true);

    // zera o grid
    fg.Rows = 1;

    glb_dataGridWasChanged = false;

    // parametrizacao do dso dsoGrid
    setConnection(dsoGrid);

    strSQL = selectString(false);

    if (strSQL != null) {
        try {
            dsoGrid.SQL = strSQL;
            dsoGrid.ondatasetcomplete = fillGridData_DSC;
            dsoGrid.Refresh();
        }
        catch (e) {
            alert('erro');
        }
    }
}

/********************************************************************
Obtem string de select semelhante ao do pesqlist

Parametros:
nenhum

Retorno:
string de select
********************************************************************/
function selectString(bToExcel, bComboDataSemelhante) {
    var i = 0;
    var strSQL = '';
    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');
    aGrid = (aGrid == null ? new Array() : aGrid);
    var sFiltroFornecedor = '';
    var sFiltroComprar = '';
    var sCustoComprar = '';
    var sPercentualPV = 'NULL';
    var sFiltroData = '';
    var sAGrid = '';

    bComboDataSemelhante = (bComboDataSemelhante == null ? false : bComboDataSemelhante);

    if ((trimStr(txtPercentualPV.value) != '') && (!isNaN(txtPercentualPV.value)))
        sPercentualPV = txtPercentualPV.value;

    if ((glb_nModalType == 10) && (selFornecedor2.value != 0))
        sFiltroFornecedor = ' AND (a.FornecedorID = ' + selFornecedor2.value + ')';

    if (aGrid == null)
        return null;

    var selFiltroEspecificoFiltro = "";

    if (selFiltroEspecifico.value != 0)
        selFiltroEspecificoFiltro = selFiltroEspecifico.options[selFiltroEspecifico.selectedIndex].getAttribute('Filtro', 1);

    if (bComboDataSemelhante)
        strSQL = 'SELECT DISTINCT CONVERT(VARCHAR,dtAtualizacao,' + DATE_SQL_PARAM + ') AS fldName, ' +
			'CONVERT(VARCHAR,dtAtualizacao,101) AS fldID, dbo.fn_Data_Zero(dtAtualizacao) AS dtAtualizacao ';
    else {
        strSQL = 'SELECT ' +
			'a.RelPesConFornecID, a.RelacaoID, ' +
			'(SELECT f.ObjetoID FROM RelacoesPesCon f WITH(NOLOCK) WHERE a.RelacaoID=f.RelacaoID) AS ObjetoID, ' +
			'(SELECT e.RecursoAbreviado FROM RelacoesPesCon c WITH(NOLOCK), Recursos e WITH(NOLOCK) WHERE a.RelacaoID=c.RelacaoID AND c.EstadoID=e.RecursoID) AS Estado, ' +
			'(SELECT b.Conceito FROM Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK) WHERE a.RelacaoID=c.RelacaoID AND b.ConceitoID=c.ObjetoID) AS Produto, ' +
			'(SELECT d.Fantasia FROM Pessoas d WITH(NOLOCK) WHERE d.PessoaID=a.FornecedorID) AS Fornecedor, ' +
			'(SELECT b.Modelo FROM Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK) WHERE a.RelacaoID=c.RelacaoID AND b.ConceitoID=c.ObjetoID) AS Modelo, ' +
			'(SELECT b.PartNumber FROM Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK) WHERE a.RelacaoID=c.RelacaoID AND b.ConceitoID=c.ObjetoID) AS PartNumber, ' +
			'a.Ordem,a.MoedaID, a.CustoFOB, a.CustoReposicao, CONVERT(VARCHAR(10), dbo.fn_Produto_Datas(a.RelacaoID, NULL, 8), 103) AS dtAtualizacao, a.MoedaRebateID, a.RebateCliente, a.ClassificacaoFiscalID, a.PrazoPagamento, a.PrazoReposicao, ' +
			'a.Observacao, a.PrazoTroca, a.PrazoGarantia, a.PrazoAsstec, a.AsstecProprio, a.AsstecTerceirizado, ' +
            ' (SELECT     dbo.fn_Preco_Preco(PesCon.SujeitoID, PesCon.ObjetoID, NULL, Fornecedores.CustoReposicao, 0, NULL, a.MoedaID, GETDATE(), ' +
            ' 1, 0, 0, 1, PesCon.SujeitoID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL) ' +
            '    FROM RelacoesPesCon_Fornecedores Fornecedores WITH(NOLOCK) ' +
            '        INNER JOIN RelacoesPesCon PesCon WITH(NOLOCK) ON (PesCon.RelacaoID = Fornecedores.RelacaoID)' +
            '  WHERE  (Fornecedores.RelPesConFornecID = a.RelPesConFornecID))   as Preco,' +
            ' a.FatorInternacaoEntrada AS FIE, ' +
            ' a.FatorInternacaoSaida, a.PortoOrigemID, a.ViaTransporteID, ' +
            'a.AsstecTerceirizado, ' +
			'dbo.fn_RelPesConFornec_AliquotaNaoInc(a.RelPesConFornecID) AS AliquotaNaoInc, ' +
            //Colunas acrescentadas
           'a.ValidadeCustoFOB, DATEDIFF(dd, dbo.fn_Produto_Datas(a.RelacaoID, GETDATE(), 8), GETDATE()) AS DiasFOB, ' +
            'a.FIEAutomatico, DATEDIFF(dd, dbo.fn_Produto_Datas(a.RelacaoID, GETDATE(), 7), GETDATE()) AS DiasFIE, ' +

            // Auditoria
            '(CASE WHEN (a.Ordem = 1) THEN (dbo.fn_Produto_Auditoria(a.RelacaoID, 71, 2, GETDATE())) ELSE NULL END) AS CustoFOB_Auditoria, ' +
            '(CASE WHEN (a.Ordem = 1) THEN (dbo.fn_Produto_Auditoria(a.RelacaoID, 72, 2, GETDATE())) ELSE NULL END) AS FIE_Auditoria, ' +
            '(CASE WHEN (a.Ordem = 1) THEN (dbo.fn_Produto_Auditoria(a.RelacaoID, 73, 2, GETDATE())) ELSE NULL END) AS Auto_Auditoria, ' +
            '(CASE WHEN (a.Ordem = 1) THEN (dbo.fn_Produto_Auditoria(a.RelacaoID, 74, 2, GETDATE())) ELSE NULL END) AS CustoReposicao_Auditoria, ' +
            '(CASE WHEN (a.Ordem = 1) THEN (dbo.fn_Produto_Auditoria(a.RelacaoID, 81, 2, GETDATE())) ELSE NULL END) AS PR_Auditoria, ' +
            '(CASE WHEN (a.Ordem = 1) THEN (dbo.fn_Produto_Auditoria(a.RelacaoID, 84, 2, GETDATE())) ELSE NULL END) AS PP_Auditoria, ' +
            '(CASE WHEN (a.Ordem = 1) THEN (dbo.fn_Produto_Auditoria(a.RelacaoID, 82, 2, GETDATE())) ELSE NULL END) AS PO_Auditoria, ' +
            '(CASE WHEN (a.Ordem = 1) THEN (dbo.fn_Produto_Auditoria(a.RelacaoID, 83, 2, GETDATE())) ELSE NULL END) AS VT_Auditoria ';

        if ((seldtAtualizacao.selectedIndex >= 1) && (!bComboDataSemelhante)) {
            if (chkDataSemelhante.checked)
                sFiltroData = ' AND dbo.fn_Data_Zero(a.dtAtualizacao) = ' + '\'' + seldtAtualizacao.value + '\'' + ' ';
            else
                sFiltroData = ' AND (dbo.fn_Data_Zero(a.dtAtualizacao) <> ' + '\'' + seldtAtualizacao.value + '\'' + ' OR a.dtAtualizacao IS NULL) ';
        }
    }
    strSQL += 'FROM RelacoesPesCon_Fornecedores a WITH(NOLOCK) ' +
        'WHERE a.RelacaoID IN (0';


    for (i = 0; i < aGrid.length; i++) {
        strSQL = strSQL + aGrid[i];
        sAGrid = sAGrid + aGrid[i];

        if (i != (aGrid.length - 1)) {
            strSQL = strSQL + ',';
            sAGrid = sAGrid + ',';
        }
    }

    strSQL = strSQL + ')' + sFiltroFornecedor + sFiltroComprar + sFiltroData;

    strSQL = strSQL + glb_sFiltroDireitos;

    if (!bComboDataSemelhante)
        strSQL = strSQL + ' ORDER BY a.Ordem, Fornecedor, a.RelacaoID';
    else
        strSQL = strSQL + ' ORDER BY dtAtualizacao DESC ';


    with (txtAreaTrace) {
        style.left = parseInt(btnCanc.currentStyle.left, 10) +
                     parseInt(btnCanc.currentStyle.width, 10) + ELEM_GAP;
        style.top = parseInt(btnCanc.currentStyle.top, 10);
        style.width = 100;
        style.height = 24;
        style.visibility = 'hidden';
        //style.visibility = 'visible';
    }

    // Somente para debug
    if (txtAreaTrace.style.visibility != 'hidden')
        showExtFrame(window, true);

    if (bToExcel) {
        var geraExcelID = 1;
        var formato = 2; //Excel
        var sLinguaLogada = getDicCurrLang();

        var strParameters = "RelatorioID=" + geraExcelID + "&Formato=" + formato + "&bToExcel=" + (bToExcel ? true : false) + "&sLinguaLogada=" + sLinguaLogada + "&glb_sEmpresaFantasia=" + glb_aEmpresaData[3] +
                            "&glb_nModalType=" + glb_nModalType + "&selFornecedor=" + selFornecedor.value + "&selFornecedor2=" + selFornecedor2.value + "&selServico=" + selServico.value +
                            "&selFiltroEspecifico=" + selFiltroEspecifico.value + "&selFiltroEspecificoFiltro=" + selFiltroEspecificoFiltro + "&selFamilia=" + selFamilia.value + "&selMarca=" + selMarca.value +
                            "&selLinha=" + selLinha.value + "&glb_nProprietarioID=" + glb_nProprietarioID + "&glb_nRegistros=" + glb_nRegistros + "&sPercentualPV=" + sPercentualPV +
                            "&glb_sFiltroContexto=" + glb_sFiltroContexto + "&glb_sPesquisa=" + glb_sPesquisa + "&glb_sFiltro=" + glb_sFiltro + "&glb_sFiltroDireitos=" + "&glb_aEmpresaData0=" + glb_aEmpresaData[0] +
                            "&glb_sChavePesquisa=" + glb_sChavePesquisa + "&glb_sOrdem=" + glb_sOrdem + "&bComboDataSemelhante=" + (bComboDataSemelhante ? true : false) +
                            "&seldtAtualizacaoSelectedIndex=" + seldtAtualizacao.selectedIndex + "&seldtAtualizacaoValue=" + seldtAtualizacao.value +
                            "&chkDataSemelhante=" + (chkDataSemelhante.checked ? true : false) + "&sAGrid=" + sAGrid + "&glb_nMoedaFaturamentoID=" + glb_nMoedaFaturamentoID;

        return SYS_PAGESURLROOT + '/serversidegenEx/ReportsGrid_fornecedores.aspx?' + strParameters;
    }
    else {
        return strSQL;
    }
}

/********************************************************************
Fecha a janela se mostra-la
********************************************************************/
function unloadThisWin() {
    if (glb_timerUnloadWin != null) {
        window.clearInterval(glb_timerUnloadWin);
        glb_timerUnloadWin = null;
    }
    if (window.top.overflyGen.Alert('Nenhum registro dispon�vel.') == 0)
        return null;

    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, 'NOREG');
}
function preencheFiltroEspecifico() {
    fg.rows = 1;

    setConnection(dsoFiltroEspecifico);
    dsoFiltroEspecifico.SQL = 'SELECT a.SujeitoID AS SujeitoID, b.RecursoFantasia AS NomeFiltro, a.Ordem, a.Observacoes, b.Filtro AS Filtro ' +
	                    'FROM RelacoesRecursos a WITH(NOLOCK) ' +
		                    'INNER JOIN Recursos b WITH(NOLOCK) ON (b.RecursoID = a.SujeitoID) ' +
	                    'WHERE ((a.ObjetoID = 21060) AND (b.TipoRecursoID = 5) AND (b.ClassificacaoID = 12)) AND ' +
	                        '(a.Observacoes LIKE \'%[' + selServico.value + ']%\') ' +
	                    'ORDER BY a.Ordem, b.Recurso';

    dsoFiltroEspecifico.ondatasetcomplete = preencheFiltroEspecifico_DSC;
    dsoFiltroEspecifico.Refresh();
}

function preencheFiltroEspecifico_DSC() {
    var optionStr, optionValue;

    clearComboEx(['selFiltroEspecifico']);

    var oOption = document.createElement("OPTION");
    oOption.text = '';
    oOption.value = '';
    selFiltroEspecifico.add(oOption);

    dsoFiltroEspecifico.recordset.moveFirst();

    while (!dsoFiltroEspecifico.recordset.EOF) {
        optionStr = dsoFiltroEspecifico.recordset['NomeFiltro'].value;
        optionValue = dsoFiltroEspecifico.recordset['SujeitoID'].value;
        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        oOption.setAttribute('Filtro', dsoFiltroEspecifico.recordset['Filtro'].value, 1);
        selFiltroEspecifico.add(oOption);
        dsoFiltroEspecifico.recordset.MoveNext();
    }

    if (selServico.value == 7)
        selOptByValueInSelect(getHtmlId(), 'selFiltroEspecifico', 41018);
    else
        selOptByValueInSelect(getHtmlId(), 'selFiltroEspecifico', 41020);

}

function preencheFamilia() {
    glb_nValue = selFamilia.value;

    setConnection(dsoFamilia);

    var sWHERE = '';

    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ');

    if (selMarca.value != 0)
        sWHERE += (' AND (b.MarcaID = ' + selMarca.value + ') ');

    dsoFamilia.SQL = 'SELECT 0 AS FamiliaID, \'\' AS Familia ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS FamiliaID, c.Conceito AS Familia ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Familia';

    dsoFamilia.ondatasetcomplete = preencheFamilia_DSC;
    dsoFamilia.Refresh();
}

function preencheFamilia_DSC() {
    clearComboEx(['selFamilia']);

    if (!(dsoFamilia.recordset.BOF || dsoFamilia.recordset.EOF)) {
        dsoFamilia.recordset.moveFirst();

        while (!dsoFamilia.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoFamilia.recordset['Familia'].value;;
            oOption.value = dsoFamilia.recordset['FamiliaID'].value;
            selFamilia.add(oOption);
            dsoFamilia.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selFamilia', glb_nValue);

        selFamilia.disabled = ((selFamilia.length <= 1) ? true : false);
    }
}

function preencheMarca() {
    glb_nValue = selMarca.value;

    setConnection(dsoMarca);

    var sWHERE = '';

    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ');

    if (selFamilia.value != 0)
        sWHERE += (' AND (b.ProdutoID = ' + selFamilia.value + ') ');

    dsoMarca.SQL = 'SELECT 0 AS MarcaID, \'\' AS Marca ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS MarcaID, c.Conceito AS Marca ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.MarcaID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Marca';

    dsoMarca.ondatasetcomplete = preencheMarca_DSC;
    dsoMarca.Refresh();
}

function preencheMarca_DSC() {
    clearComboEx(['selMarca']);

    if (!(dsoMarca.recordset.BOF || dsoMarca.recordset.EOF)) {
        dsoMarca.recordset.moveFirst();

        while (!dsoMarca.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoMarca.recordset['Marca'].value;;
            oOption.value = dsoMarca.recordset['MarcaID'].value;
            selMarca.add(oOption);
            dsoMarca.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selMarca', glb_nValue);

        selMarca.disabled = ((selMarca.length <= 1) ? true : false);
    }
}

function preencheLinha() {
    glb_nValue = selLinha.value;

    setConnection(dsoLinha);

    var sWHERE = '';

    sWHERE = ('AND ' + glb_sFiltroContexto + ' ' + glb_sPesquisa + ' ' + glb_sFiltro + ' ' + glb_sFiltroEspecifico + ' ' +
	    glb_sFiltroDireitos + ' ' + glb_sFiltroProprietario + ' ' +
	    'AND (a.SujeitoID = ' + glb_aEmpresaData[0] + ') ' +
        'AND (b.MarcaID = ' + selMarca.value + ') ');

    dsoLinha.SQL = 'SELECT 0 AS LinhaID, \'\' AS Linha ' +
	    'UNION ' +
	    'SELECT DISTINCT c.ConceitoID AS LinhaID, c.Conceito AS Linha ' +
    	    'FROM RelacoesPesCon a WITH(NOLOCK) ' +
                'INNER JOIN Conceitos b WITH(NOLOCK)  ON (b.ConceitoID = a.ObjetoID) ' +
                    'INNER JOIN Conceitos c WITH(NOLOCK)  ON (c.ConceitoID = b.LinhaProdutoID) ' +
            'WHERE ((a.TipoRelacaoID = 61) AND (a.EstadoID = 2)) ' + sWHERE +
        'ORDER BY Linha';

    dsoLinha.ondatasetcomplete = preencheLinha_DSC;
    dsoLinha.Refresh();
}

function preencheLinha_DSC() {
    clearComboEx(['selLinha']);

    if (!(dsoLinha.recordset.BOF || dsoLinha.recordset.EOF)) {
        dsoLinha.recordset.moveFirst();

        while (!dsoLinha.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoLinha.recordset['Linha'].value;;
            oOption.value = dsoLinha.recordset['LinhaID'].value;
            selLinha.add(oOption);
            dsoLinha.recordset.MoveNext();
        }

        selOptByValueInSelect(getHtmlId(), 'selLinha', glb_nValue);

        selLinha.disabled = ((selLinha.length <= 1) ? true : false);
    }
}

function btnCalcularPV_onclick() {
    if (fg.Rows > 2) {
        var strPars = '';

        strPars = '?nEmpresaID=' + dsoGrid.recordset['_SujeitoID'].value;

        for (i = 2; i < fg.Rows; i++) {
            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0)
                strPars += '&nProdutoID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'ObjetoID*')));
        }

        dsoPrevisaoVendas.URL = SYS_ASPURLROOT + '/serversidegenEx/calcularpv.aspx' + strPars;
        dsoPrevisaoVendas.ondatasetcomplete = btnCalcularPV_onclick_DSC;
        dsoPrevisaoVendas.refresh();
    }
    else
        return null;
}
function btnCalcularPV_onclick_DSC() {
    fillGridData();
}

/*
function zeraSelMarca()
{
    if (selFamilia.value == 0)
    {
        selMarca.value = 0;    
        zeraSelLinha();
    }
}

function zeraSelLinha()
{   
    if (selMarca.value != 0)
    {
        selLinha.disabled = false;
        preencheLinha();
    }
    else
    {
        selLinha.disabled = true;
        selLinha.value = 0;
    }
}
*/

function selFamilia_onchange() {
    //    zeraSelMarca();
    preencheMarca();
}

function selMarca_onchange() {
    //    zeraSelLinha();
    preencheFamilia();
    preencheLinha();
}

function limpaGrid() {
    fg.rows = 1;
    alignButtons();
}

function selServico_onchange() {
    var bFillGrid = false;

    fg.rows = 1;

    sVisibility = ((selServico.value == 5) ? 'inherit' : 'hidden');
    lblFornecedor.style.visibility = sVisibility;
    selFornecedor.style.visibility = sVisibility;
    selFornecedor.disabled = (selFornecedor.options.length == 0);
    lblTransacao.style.visibility = sVisibility;
    selTransacao.style.visibility = sVisibility;
    selTransacao.disabled = (selTransacao.options.length == 0);
    lblFinanciamento.style.visibility = sVisibility;
    selFinanciamento.style.visibility = sVisibility;
    selFinanciamento.disabled = (selFinanciamento.options.length == 0);
    btnComprar.style.visibility = sVisibility;

    preencheFiltroEspecifico();

    alignButtons();

    if (selServico.value == 5) {
        // ESTA DANDO PROBLEMA COM ESTE bFillGrid
        if (bFillGrid != false)
            fillGridData();
    }
    else {
        selFornecedor.selectedIndex = 0;
    }
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC() {
    if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF) {
        if (window.top.overflyGen.Alert('Nenhum registro dispon�vel.') == 0)
            return null;

        lockControlsInModalWin(false);
        fg.Rows = 1;
        return null;
    }

    showExtFrame(window, true);
    lockControlsInModalWin(true);
    glb_refreshGrid2 = window.setInterval('fillGridData_DSC2()', 10, 'JavaScript');
}

function fillGridData_DSC2() {
    if (glb_refreshGrid2 != null) {
        window.clearInterval(glb_refreshGrid2);
        glb_refreshGrid2 = null;
    }

    // restoreInterfaceFromModal(true);

    var dTFormat = '';
    var aGrid = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'retArrayGrid()');
    var i, j;
    var bgColorGreen, bgColorYellow, bgColorRed, bgColorPurple, bgColorBlack;
    var sPaintFiedNumber = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    fg.Editable = false;
    fg.Redraw = 0;
    fg.Rows = 1;
    fg.FontSize = '8';
    fg.ExplorerBar = 0;

    // Dados de Fornecedores
    headerGrid(fg, ['Fornecedor',                       //0 
                   'ID',                                //1 
                   'Est',                               //2 
                   'Produto',                           //3 
                   'Modelo',                            //4 
                   'Part Number',                       //5 
                   'Ordem',                             //6 
                   'Moeda',                             //7 
                   'Val',                               //8 
                   'Custo FOB',                         //9 
                   'Atualiza��o',                       //10
                   'Dias',                              //11
                   'Custo Rep',                         //12
                   'Auto',                              //13
                   'FIE',                               //14
                   'Dias',                              //15
                   'Pre�o',                             //16
                   'FIS',                               //17

                   'PP',                                //18    //23
                   'PR',                                //19    //24

                   'Porto Origem',                      //20    //18
                   'Via Transporte',                    //21    //19
                   'Moeda',                             //22    //20
                   'Reb Cli',                           //23    //21
                   'NCM',                               //24    //22


                   'Troca',                             //25    //26
                   'Gar',                               //26    //27
                   'Asstec',                            //27    //28
                   'AP',                                //28    //29
                   'AT',                                //29    //30

                   'Observa��o',                        //30    //25

                   'AliquotaNaoInc',                    //31
                   'RelPesConFornecID',                 //32
                   'CustoFOB_Auditoria',                //33
                   'FIE_Auditoria',                     //34
                   'CustoReposicao_Auditoria',          //35
                   'PR_Auditoria',                      //36
                   'PO_Auditoria',                      //37
                   'VT_Auditoria',                      //38                            
                   'Auto_Auditoria',                    //39
                   'PP_Auditoria'], [31, 32, 33, 34, 35, 36, 37, 38, 39, 40]); //40                            
    
    glb_aCelHint = [[0, 2, 'Estado'],
            [0, 7, 'Moeda Fornecedor'],
            [0, 8, 'Validade do Custo FOB'],
            [0, 9, 'Custo FOB'],
            [0, 11, 'Dias desde a �ltima atualiza��o do Custo FOB'],
            [0, 12, 'Custo de Reposi��o'],
            [0, 13, 'FIE calculado automaticamente?'],
            [0, 14, 'Fator de Interna��o de Entrada'],
            [0, 15, 'Dias desde a �ltima atualiza��o do FIE'],
            [0, 15, 'Pre�o do produto com margem de contribui��o zero e prazo de estoque zero (Moeda Fornecedor)'],
            [0, 16, 'Fator de Interna��o de Sa�da'],
            [0, 18, 'Prazo de Pagamento (em dias corridos)'],
            [0, 19, 'Prazo (em dias �teis) que levamos para comprar e o produto estar dispon�vel para vendas'],
            [0, 20, 'Porto de Origem da Importa��o'],
            [0, 21, 'Via de Transporte da Importa��o'],
            [0, 23, 'Rebate Cliente'],
            [0, 25, 'Prazo de Troca (em meses)'],
            [0, 26, 'Prazo de Garantia (em meses)'],
            [0, 27, 'Prazo de Assist�ncia T�cnica (em meses)'],
            [0, 28, 'Assist�ncia T�cnica Pr�pria'],
            [0, 29, 'Assist�ncia T�cnica Terceirizada']];

    fg.ColKey(0) = 'Fornecedor*';
    fg.ColKey(1) = 'ObjetoID*';
    fg.ColKey(2) = 'Estado*';
    fg.ColKey(3) = 'Produto*';
    fg.ColKey(4) = 'Modelo*';
    fg.ColKey(5) = 'PartNumber*';
    fg.ColKey(6) = 'Ordem' + glb_sReadOnly;
    fg.ColKey(7) = 'MoedaID' + glb_sReadOnly;
    fg.ColKey(8) = 'ValidadeCustoFOB';
    fg.ColKey(9) = 'CustoFOB';
    fg.ColKey(10) = 'dtAtualizacao*';
    fg.ColKey(11) = 'DiasFOB*';
    fg.ColKey(12) = 'CustoReposicao*';//+ glb_sCustosReadOnly;
    fg.ColKey(13) = 'FIEAutomatico*';
    fg.ColKey(14) = 'FIE*';
    fg.ColKey(15) = 'DiasFIE*';
    fg.ColKey(16) = 'Preco*';
    fg.ColKey(17) = 'FatorInternacaoSaida*';

    fg.ColKey(18) = 'PrazoPagamento' + glb_sReadOnly;
    fg.ColKey(19) = 'PrazoReposicao' + glb_sReadOnly;

    fg.ColKey(20) = 'PortoOrigemID';
    fg.ColKey(21) = 'ViaTransporteID';
    fg.ColKey(22) = 'MoedaRebateID' + glb_sReadOnly;
    fg.ColKey(23) = 'RebateCliente' + glb_sReadOnly;
    fg.ColKey(24) = 'ClassificacaoFiscalID' + glb_sReadOnly;

    fg.ColKey(25) = 'PrazoTroca' + glb_sReadOnly;
    fg.ColKey(26) = 'PrazoGarantia' + glb_sReadOnly;
    fg.ColKey(27) = 'PrazoAsstec' + glb_sReadOnly;
    fg.ColKey(28) = 'AsstecProprio' + glb_sReadOnly;
    fg.ColKey(29) = 'AsstecTerceirizado' + glb_sReadOnly;
    fg.ColKey(30) = 'Observacao' + glb_sReadOnly;

    fg.ColKey(31) = 'AliquotaNaoInc';
    fg.ColKey(32) = 'RelPesConFornecID';
    fg.ColKey(33) = 'CustoFOB_Auditoria';
    fg.ColKey(34) = 'FIE_Auditoria';
    fg.ColKey(35) = 'CustoReposicao_Auditoria';

    fg.ColKey(36) = 'PR_Auditoria';
    fg.ColKey(37) = 'PO_Auditoria';
    fg.ColKey(38) = 'VT_Auditoria';
    fg.ColKey(39) = 'Auto_Auditoria';
    fg.ColKey(40) = 'PP_Auditoria';


    j = 0;
    for (i = 0; i < aGrid.length; i++) {
        dsoGrid.recordset.MoveFirst();
        dsoGrid.recordset.Find('RelacaoID', aGrid[i]);


        if (!(dsoGrid.recordset.BOF || dsoGrid.recordset.EOF)) {
            while ((!dsoGrid.recordset.EOF) && (dsoGrid.recordset['RelacaoID'].value == aGrid[i])) {

                fg.Rows = fg.Rows + 1;

                fg.TextMatrix(j + 1, 0) = (dsoGrid.recordset['Fornecedor'].value == null ? '' : dsoGrid.recordset['Fornecedor'].value);
                fg.TextMatrix(j + 1, 1) = (dsoGrid.recordset['ObjetoID'].value == null ? '' : dsoGrid.recordset['ObjetoID'].value);
                fg.TextMatrix(j + 1, 2) = (dsoGrid.recordset['Estado'].value == null ? '' : dsoGrid.recordset['Estado'].value);
                fg.TextMatrix(j + 1, 3) = (dsoGrid.recordset['Produto'].value == null ? '' : dsoGrid.recordset['Produto'].value);
                fg.TextMatrix(j + 1, 4) = (dsoGrid.recordset['Modelo'].value == null ? '' : dsoGrid.recordset['Modelo'].value);
                fg.TextMatrix(j + 1, 5) = (dsoGrid.recordset['PartNumber'].value == null ? '' : dsoGrid.recordset['PartNumber'].value);
                fg.TextMatrix(j + 1, 6) = (dsoGrid.recordset['Ordem'].value == null ? '' : dsoGrid.recordset['Ordem'].value);
                fg.TextMatrix(j + 1, 7) = (dsoGrid.recordset['MoedaID'].value == null ? '' : dsoGrid.recordset['MoedaID'].value);
                fg.TextMatrix(j + 1, 8) = (dsoGrid.recordset['ValidadeCustoFOB'].value == null ? '' : dsoGrid.recordset['ValidadeCustoFOB'].value);
                fg.TextMatrix(j + 1, 9) = (dsoGrid.recordset['CustoFOB'].value == null ? '' : dsoGrid.recordset['CustoFOB'].value);

                if ((chkRetCustoReposicao.checked) && (dsoGrid.recordset['CustoFOB'].value != null))
                    fg.TextMatrix(j + 1, 9) = dsoGrid.recordset['CustoFOB'].value * (1 + (dsoGrid.recordset['AliquotaNaoInc'].value / 100));

                fg.TextMatrix(j + 1, 10) = (dsoGrid.recordset['dtAtualizacao'].value == null ? '' : dsoGrid.recordset['dtAtualizacao'].value);
                fg.TextMatrix(j + 1, 11) = (dsoGrid.recordset['DiasFOB'].value == null ? '' : dsoGrid.recordset['DiasFOB'].value);
                fg.TextMatrix(j + 1, 12) = (dsoGrid.recordset['CustoReposicao'].value == null ? '' : dsoGrid.recordset['CustoReposicao'].value);

                if ((chkRetCustoReposicao.checked) && (dsoGrid.recordset['CustoReposicao'].value != null))
                    fg.TextMatrix(j + 1, 12) = dsoGrid.recordset['CustoReposicao'].value * (1 + (dsoGrid.recordset['AliquotaNaoInc'].value / 100));

                fg.TextMatrix(j + 1, 13) = (dsoGrid.recordset['FIEAutomatico'].value == null ? '' : dsoGrid.recordset['FIEAutomatico'].value);
                fg.TextMatrix(j + 1, 14) = (dsoGrid.recordset['FIE'].value == null ? '' : dsoGrid.recordset['FIE'].value);
                fg.TextMatrix(j + 1, 15) = (dsoGrid.recordset['DiasFIE'].value == null ? '' : dsoGrid.recordset['DiasFIE'].value);
                fg.TextMatrix(j + 1, 16) = (dsoGrid.recordset['Preco'].value == null ? '' : dsoGrid.recordset['Preco'].value);
                fg.TextMatrix(j + 1, 17) = (dsoGrid.recordset['FatorInternacaoSaida'].value == null ? '' : dsoGrid.recordset['FatorInternacaoSaida'].value);

                fg.TextMatrix(j + 1, 18) = (dsoGrid.recordset['PrazoPagamento'].value == null ? '' : dsoGrid.recordset['PrazoPagamento'].value);
                fg.TextMatrix(j + 1, 19) = (dsoGrid.recordset['PrazoReposicao'].value == null ? '' : dsoGrid.recordset['PrazoReposicao'].value);

                fg.TextMatrix(j + 1, 20) = (dsoGrid.recordset['PortoOrigemID'].value == null ? '' : dsoGrid.recordset['PortoOrigemID'].value);
                fg.TextMatrix(j + 1, 21) = (dsoGrid.recordset['ViaTransporteID'].value == null ? '' : dsoGrid.recordset['ViaTransporteID'].value);
                fg.TextMatrix(j + 1, 22) = (dsoGrid.recordset['MoedaRebateID'].value == null ? '' : dsoGrid.recordset['MoedaRebateID'].value);
                fg.TextMatrix(j + 1, 23) = (dsoGrid.recordset['RebateCliente'].value == null ? '' : dsoGrid.recordset['RebateCliente'].value);
                fg.TextMatrix(j + 1, 24) = (dsoGrid.recordset['ClassificacaoFiscalID'].value == null ? '' : dsoGrid.recordset['ClassificacaoFiscalID'].value);

                fg.TextMatrix(j + 1, 25) = (dsoGrid.recordset['PrazoTroca'].value == null ? '' : dsoGrid.recordset['PrazoTroca'].value);
                fg.TextMatrix(j + 1, 26) = (dsoGrid.recordset['PrazoGarantia'].value == null ? '' : dsoGrid.recordset['PrazoGarantia'].value);
                fg.TextMatrix(j + 1, 27) = (dsoGrid.recordset['PrazoAsstec'].value == null ? '' : dsoGrid.recordset['PrazoAsstec'].value);
                fg.TextMatrix(j + 1, 28) = (dsoGrid.recordset['AsstecProprio'].value == null ? '' : dsoGrid.recordset['AsstecProprio'].value);
                fg.TextMatrix(j + 1, 29) = (dsoGrid.recordset['AsstecTerceirizado'].value == null ? '' : dsoGrid.recordset['AsstecTerceirizado'].value);
                fg.TextMatrix(j + 1, 30) = (dsoGrid.recordset['Observacao'].value == null ? '' : dsoGrid.recordset['Observacao'].value);

                fg.TextMatrix(j + 1, 31) = (dsoGrid.recordset['AliquotaNaoInc'].value == null ? '' : dsoGrid.recordset['AliquotaNaoInc'].value);
                fg.TextMatrix(j + 1, 32) = (dsoGrid.recordset['RelPesConFornecID'].value == null ? '' : dsoGrid.recordset['RelPesConFornecID'].value);
                fg.TextMatrix(j + 1, 33) = (dsoGrid.recordset['CustoFOB_Auditoria'].value == null ? '' : dsoGrid.recordset['CustoFOB_Auditoria'].value);
                fg.TextMatrix(j + 1, 34) = (dsoGrid.recordset['FIE_Auditoria'].value == null ? '' : dsoGrid.recordset['FIE_Auditoria'].value);
                fg.TextMatrix(j + 1, 35) = (dsoGrid.recordset['CustoReposicao_Auditoria'].value == null ? '' : dsoGrid.recordset['CustoReposicao_Auditoria'].value);

                fg.TextMatrix(j + 1, 36) = (dsoGrid.recordset['PR_Auditoria'].value == null ? '' : dsoGrid.recordset['PR_Auditoria'].value);
                fg.TextMatrix(j + 1, 37) = (dsoGrid.recordset['PO_Auditoria'].value == null ? '' : dsoGrid.recordset['PO_Auditoria'].value);
                fg.TextMatrix(j + 1, 38) = (dsoGrid.recordset['VT_Auditoria'].value == null ? '' : dsoGrid.recordset['VT_Auditoria'].value);
                fg.TextMatrix(j + 1, 39) = (dsoGrid.recordset['Auto_Auditoria'].value == null ? '' : dsoGrid.recordset['Auto_Auditoria'].value);
                fg.TextMatrix(j + 1, 40) = (dsoGrid.recordset['PP_Auditoria'].value == null ? '' : dsoGrid.recordset['PP_Auditoria'].value);

                j++;
                dsoGrid.recordset.MoveNext();
            }
        }
    }

    fg.ColDataType(13) = 11; // format boolean (checkbox) FIEAUTOMATICO
    fg.ColDataType(28) = 11; // format boolean (checkbox) ASSTEC PROPRIO
    fg.ColDataType(29) = 11; // format boolean (checkbox) ASSTEC TERCEIRO

    putMasksInGrid(fg, ['9999999999',       //0        
                        '',                 //1        
                        '',                 //2        
                        '',                 //3        
                        '',                 //4        
                        '',                 //5        
                        '999999',           //6        
                        '',                 //7        
                        '',                 //8        
                        '999999999.99',     //9
                        '99/99/9999',       //10       
                        '',                 //11       
                        '#99999999.99',     //12       
                        '',                 //13
                        '999999999.9999',   //14
                        '',                 //15
                        '999999999.99',     //16
                        '999999999.9999',   //17

                        '999',              //18    //23       
                        '999',              //19    //24       

                        '',                 //20    //18
                        '',                 //21    //19       
                        '',                 //22    //20       
                        '999999999.99',     //23    //21       
                        '',                 //24    //22       

                        '999',              //25       
                        '999',              //26       
                        '999',              //27       
                        '',                 //28       
                        '',                 //29       
                        '',                 //30       
                        '',                 //31       
                        ''],                //32       


                        ['##########',      //0    
                        '',                 //1    
                        '',                 //2    
                        '',                 //3    
                        '',                 //4    
                        '',                 //5    
                        '######',           //6    
                        '',                 //7    
                        '',                 //8    
                        '###,###,##0.00',   //9 
                        dTFormat,           //10
                        '',                 //11   
                        '###,###,##0.00',   //12   
                        '',                 //13
                        '###,###,##0.0000', //14
                        '',                 //15
                        '###,###,##0.00',   //16
                        '###,###,##0.0000', //17

                        '###',              //18    //23   
                        '',                 //19    //24   

                        '',                 //20    //18
                        '',                 //21    //19
                        '###,###,##0.00',   //22    //20   
                        '',                 //23    //21   
                        '###',              //24    //22   

                        '###',              //25   
                        '###',              //26   
                        '###',              //27   
                        '',                 //28   
                        '',                 //29   
                        '',                 //30   
                        '',                 //31   
                        '',                 //32
                        '',                 //33
                        '',                 //34
                        '']);               //35

    alignColsInGrid(fg, [5, 6, 9, 11, 12, 14, 15, 16, 17, 18, 19, 23, 24, 26, 27]);
    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    insertcomboData(fg, 7, dsoCmbMoeda, 'fldName', 'fldID');
    insertcomboData(fg, 22, dsoCmbMoeda, 'fldName', 'fldID');
    insertcomboData(fg, 24, dsoClassFiscal, 'Conceito', 'ConceitoID');
    insertcomboData(fg, 20, dsoPortoOrigem, 'PortoOrigem', 'PortoOrigemID');
    insertcomboData(fg, 21, dsoViaTransporte, 'ViaTransporte', 'ViaTransporteID');


    paintReadOnlyCols(fg);
    showHideModeloPartNumber();

    bgColorGreen = 0X90EE90;
    bgColorYellow = 0X8CE6F0;
    bgColorOrange = 0X60A4F4;
    bgColorRed = 0X7280FA;
    bgColorPurple = 0XFF8080;
    bgColorBlack = 00000000;

    /*
    if (!(fg.recordset.BOF && dsoGrid.recordset.EOF))
        dsoGrid.recordset.MoveFirst();
    */
    i = 1;
    while (i < fg.rows) {
        fg.FillStyle = 1;

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoFOB_Auditoria')) != "") {
            sPaintFiedNumber = getColIndexByColKey(fg, 'CustoFOB');
            fg.Cell(6, i, sPaintFiedNumber, i, sPaintFiedNumber) = (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoFOB_Auditoria')).substring(0, 1) == '1' ? bgColorGreen : (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoFOB_Auditoria')).substring(0, 1) == '2' ? bgColorYellow :
                (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoFOB_Auditoria')).substring(0, 1) == '3' ? bgColorOrange : (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoFOB_Auditoria')).substring(0, 1) == '4' ? bgColorRed : (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoFOB_Auditoria')).substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
        }

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'FIE_Auditoria')) != "") {
            sPaintFiedNumber = getColIndexByColKey(fg, 'FIE*');
            fg.Cell(6, i, sPaintFiedNumber, i, sPaintFiedNumber) = (fg.TextMatrix(i, getColIndexByColKey(fg, 'FIE_Auditoria')).substring(0, 1) == '1' ? bgColorGreen : (fg.TextMatrix(i, getColIndexByColKey(fg, 'FIE_Auditoria')).substring(0, 1) == '2' ? bgColorYellow :
                (fg.TextMatrix(i, getColIndexByColKey(fg, 'FIE_Auditoria')).substring(0, 1) == '3' ? bgColorOrange : (fg.TextMatrix(i, getColIndexByColKey(fg, 'FIE_Auditoria')).substring(0, 1) == '4' ? bgColorRed : (fg.TextMatrix(i, getColIndexByColKey(fg, 'FIE_Auditoria')).substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
        }

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'Auto_Auditoria')) != "") {
            sPaintFiedNumber = getColIndexByColKey(fg, 'FIEAutomatico*');
            fg.Cell(6, i, sPaintFiedNumber, i, sPaintFiedNumber) = (fg.TextMatrix(i, getColIndexByColKey(fg, 'Auto_Auditoria')).substring(0, 1) == '1' ? bgColorGreen : (fg.TextMatrix(i, getColIndexByColKey(fg, 'Auto_Auditoria')).substring(0, 1) == '2' ? bgColorYellow :
                (fg.TextMatrix(i, getColIndexByColKey(fg, 'Auto_Auditoria')).substring(0, 1) == '3' ? bgColorOrange : (fg.TextMatrix(i, getColIndexByColKey(fg, 'Auto_Auditoria')).substring(0, 1) == '4' ? bgColorRed : (fg.TextMatrix(i, getColIndexByColKey(fg, 'Auto_Auditoria')).substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
        }

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'PP_Auditoria')) != "") {
            sPaintFiedNumber = getColIndexByColKey(fg, 'PrazoPagamento' + glb_sReadOnly);
            fg.Cell(6, i, sPaintFiedNumber, i, sPaintFiedNumber) = (fg.TextMatrix(i, getColIndexByColKey(fg, 'PP_Auditoria')).substring(0, 1) == '1' ? bgColorGreen : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PP_Auditoria')).substring(0, 1) == '2' ? bgColorYellow :
                (fg.TextMatrix(i, getColIndexByColKey(fg, 'PP_Auditoria')).substring(0, 1) == '3' ? bgColorOrange : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PP_Auditoria')).substring(0, 1) == '4' ? bgColorRed : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PP_Auditoria')).substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
        }

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoReposicao_Auditoria')) != "") {
            sPaintFiedNumber = getColIndexByColKey(fg, 'CustoReposicao*');
            fg.Cell(6, i, sPaintFiedNumber, i, sPaintFiedNumber) = (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoReposicao_Auditoria')).substring(0, 1) == '1' ? bgColorGreen : (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoReposicao_Auditoria')).substring(0, 1) == '2' ? bgColorYellow :
                (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoReposicao_Auditoria')).substring(0, 1) == '3' ? bgColorOrange : (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoReposicao_Auditoria')).substring(0, 1) == '4' ? bgColorRed : (fg.TextMatrix(i, getColIndexByColKey(fg, 'CustoReposicao_Auditoria')).substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
        }

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'PR_Auditoria')) != "") {
            sPaintFiedNumber = getColIndexByColKey(fg, 'PrazoReposicao' + glb_sReadOnly);
            fg.Cell(6, i, sPaintFiedNumber, i, sPaintFiedNumber) = (fg.TextMatrix(i, getColIndexByColKey(fg, 'PR_Auditoria')).substring(0, 1) == '1' ? bgColorGreen : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PR_Auditoria')).substring(0, 1) == '2' ? bgColorYellow :
                (fg.TextMatrix(i, getColIndexByColKey(fg, 'PR_Auditoria')).substring(0, 1) == '3' ? bgColorOrange : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PR_Auditoria')).substring(0, 1) == '4' ? bgColorRed : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PR_Auditoria')).substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
        }

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'PO_Auditoria')) != "") {
            sPaintFiedNumber = getColIndexByColKey(fg, 'PortoOrigemID');
            fg.Cell(6, i, sPaintFiedNumber, i, sPaintFiedNumber) = (fg.TextMatrix(i, getColIndexByColKey(fg, 'PO_Auditoria')).substring(0, 1) == '1' ? bgColorGreen : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PO_Auditoria')).substring(0, 1) == '2' ? bgColorYellow :
                (fg.TextMatrix(i, getColIndexByColKey(fg, 'PO_Auditoria')).substring(0, 1) == '3' ? bgColorOrange : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PO_Auditoria')).substring(0, 1) == '4' ? bgColorRed : (fg.TextMatrix(i, getColIndexByColKey(fg, 'PO_Auditoria')).substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
        }

        if (fg.TextMatrix(i, getColIndexByColKey(fg, 'VT_Auditoria')) != "") {
            sPaintFiedNumber = getColIndexByColKey(fg, 'ViaTransporteID');
            fg.Cell(6, i, sPaintFiedNumber, i, sPaintFiedNumber) = (fg.TextMatrix(i, getColIndexByColKey(fg, 'VT_Auditoria')).substring(0, 1) == '1' ? bgColorGreen : (fg.TextMatrix(i, getColIndexByColKey(fg, 'VT_Auditoria')).substring(0, 1) == '2' ? bgColorYellow :
                (fg.TextMatrix(i, getColIndexByColKey(fg, 'VT_Auditoria')).substring(0, 1) == '3' ? bgColorOrange : (fg.TextMatrix(i, getColIndexByColKey(fg, 'VT_Auditoria')).substring(0, 1) == '4' ? bgColorRed : (fg.TextMatrix(i, getColIndexByColKey(fg, 'VT_Auditoria')).substring(0, 1) == '5' ? bgColorPurple : bgColorBlack)))));
        }

        i++;
    }

    fg.Col = fg.Cols - 1;

    // Usuario tem ou nao direito de edicao 
    if (glb_sReadOnly == '')
        fg.Col = 4;

    fg.Redraw = 2;
    fg.FrozenCols = fg.Cols - 1;
    fg.FrozenCols = 4;
    fg.Redraw = 0;

    if (glb_nFocusLinha != 0) {
        fg.Row = glb_nFocusLinha;
    }


    fg.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if ((fg.Rows > 1) && (glb_nFocusLinha == 0))
        fg.Row = 1;

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fg_modalfornecedoresAfterRowColChange();
    if (fg.Rows >= 2)
        fg.Editable = true;

    lockControlsInModalWin(false);

    seldtAtualizacao.disabled = (seldtAtualizacao.options.length == 1);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 2) {
        window.focus();
        fg.focus();
    }
    else {
        ;
    }

    if (chkImpostos.checked)
        fillGridImpostos();

    // ajusta estado dos botoes
    setupBtnsFromGridState();
}

function showHideCols() {
    // Campos do PV

    // Servico 1 Ciclo de vida
    if (selServico.value == 1) {
        fg.ColHidden(getColIndexByColKey(fg, 'dtMediaPagamento*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'DiasPagamento*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'dtUltimaEntrada*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'dtUltimaSaida*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'DiasUltimaSaida*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'VendasMes3*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'VendasMes2*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'VendasMes1*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'VendasMes0*')) = !chkColunas.checked;
    }

        // Servico 2 Previsao de vendas
    else if (selServico.value == 2) {
        fg.ColHidden(getColIndexByColKey(fg, 'VendasMes3*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'VendasMes2*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'VendasMes1*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'VendasMes0*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'Contribuicao*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'MargemContribuicao_2*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'Estoque_2*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'ComprarExcesso_2*')) = !chkColunas.checked;
    }

        // Servico 3 Formacao de precos
    else if (selServico.value == 3) {

        fg.ColHidden(getColIndexByColKey(fg, 'MargemPadrao')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'MargemReal*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'MargemMedia*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrecoBase*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrecoBaseMedio*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrecoConcorrentes*')) = !chkColunas.checked;
    }

        // Servico 4 Dados basicos
    else if (selServico.value == 4) {
        //N�o a campos dependentes do Col
    }

        // Servico 5 Compras
    else if (selServico.value == 5) {
        fg.ColHidden(getColIndexByColKey(fg, 'PrazoPagamento*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrazoReposicao*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrazoEstoque_2*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrevisaoVendas_2*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'Estoque*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'ReservaCompra*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'ReservaVendaConfirmada*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrevisaoEntrega*')) = !chkColunas.checked;

    }

        // Servico 6 Internet
    else if (selServico.value == 6) {
        fg.ColHidden(getColIndexByColKey(fg, 'EstoqueInternet*')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrazoTroca')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrazoGarantia')) = !chkColunas.checked;
        fg.ColHidden(getColIndexByColKey(fg, 'PrazoAsstec')) = !chkColunas.checked;
    }
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var i = 0;
    glb_dataGridWasChanged = false;
    lockControlsInModalWin(true);
    /*
        if ((glb_nModalType == 10) && (chkRetCustoReposicao.checked) && (!((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF))))
        {
            dsoGrid.recordset.MoveFirst();
            
            while (!dsoGrid.recordset.EOF)
            { 
                dsoGrid.recordset['CustoReposicao'].value = (dsoGrid.recordset['CustoReposicao'].value /
                    (1 + (dsoGrid.recordset['AliquotaNaoInc'].value / 100)));
    
                dsoGrid.recordset['CustoFOB'].value = (dsoGrid.recordset['CustoFOB'].value /
                    (1 + (dsoGrid.recordset['AliquotaNaoInc'].value / 100)));
    
                dsoGrid.recordset.MoveNext();
            }
        }
    */
    try {
        dsoGrid.SubmitChanges();
    }
    catch (e) {
        // Numero de erro qdo o registro foi alterado ou removido por outro usuario
        if (e.number == -2147217887) {
            if (window.top.overflyGen.Alert('Este registro acaba de ser alterado ou removido.') == 0)
                return null;
        }

        fg.Rows = 1;
        lockControlsInModalWin(false);

        setupBtnsFromGridState();
        // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null ); 
        glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(false)', 50, 'JavaScript');
        return null;
    }

    dsoGrid.ondatasetcomplete = saveDataInGrid_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
********************************************************************/
function saveDataInGrid_DSC() {
    lockControlsInModalWin(false);

    glb_refreshGrid = window.setInterval('refreshParamsAndDataAndShowModalWin(false)', 50, 'JavaScript');

    // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
}

/********************************************************************
Verifica se um dado campo existe em um dado dso

Parametros:
dso         - referencia ao dso
fldName     - nome do campo a verificar a existencia
                      
Retorno:
true se o campo existe, caso contrario false
********************************************************************/
function fieldExists(dso, fldName) {
    var retVal = false;
    var i;

    for (i = 0; i < dso.recordset.Fields.Count; i++) {
        if (dso.recordset.Fields[i].Name == fldName)
            retVal = true;
    }

    return retVal;
}

/********************************************************************
Usuario clicou o label do checkbox de mostrar detalhe
********************************************************************/
function lblCampos_onclick() {
    if (chkCampos.disabled)
        return true;

    chkCampos.checked = !chkCampos.checked;

    chkCampos_onclick();
}

/********************************************************************
Usuario clicou o botao comprar
********************************************************************/
function btn_Comprar_Clicked() {
    var strPars = '';
    var i = 0;
    var sError = '';

    strPars = '?nUserID=' + escape(glb_nUserID) +
		'&nDepositoID=' + escape(glb_aEmpresaData[0]) +
		'&nEmpresaID=' + escape(glb_aEmpresaData[0]) +
		'&nPessoaID=' + escape(selFornecedor.value) +
		'&nTransacaoID=' + escape(selTransacao.value) +
		'&nFinanciamentoID=' + escape(selFinanciamento.value);

    // parei aki marco
    for (i = (glb_nModalType == 9 ? 2 : 1) ; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0) {
            strPars += '&nProdutoID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'ObjetoID*')));
            strPars += '&nQuantidade=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeComprar')));
            strPars += '&nCusto=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'CustoComprar')));

            if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'QuantidadeComprar')) <= 0) {
                sError = 'Quantidade menor que 0 no produto ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'ObjetoID*'));
                break;
            }
            else if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'CustoComprar')) <= 0) {
                sError = 'Custo menor que 0 no produto ' + fg.TextMatrix(i, getColIndexByColKey(fg, 'ObjetoID*'));
                break;
            }
        }
    }

    if (sError != '') {
        if (window.top.overflyGen.Alert(sError) == 0)
            return null;

        lockInterface(false);
        return null;
    }

    dsoGrava.URL = SYS_ASPURLROOT + '/serversidegenEx/gerarpedidocompra.aspx' + strPars;
    dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
    dsoGrava.refresh();
}

function sendDataToServer_DSC() {
    var nRetorno = null;
    var _retMsg = null;

    if (!(dsoGrava.recordset.BOF && dsoGrava.recordset.EOF)) {
        if (dsoGrava.recordset.Fields["PedidoID"].value != null) {
            _retMsg = window.top.overflyGen.Confirm('Gerado o Pedido: ' + dsoGrava.recordset.Fields["PedidoID"].value + '\n' +
														'Detalhar Pedido?');

            if (_retMsg == 0)
                return null;
            else if (_retMsg == 1)
                sendJSCarrier(getHtmlId(), 'SHOWPEDIDO', new Array(glb_aEmpresaData[0], dsoGrava.recordset.Fields["PedidoID"].value));
        }
        else {
            _retMsg = window.top.overflyGen.Alert('Pedido n�o foi gerado.');

            if (_retMsg == 0)
                return null;
        }
    }

    fillGridData();
}

/********************************************************************
Usuario trocou o option selecionado no combo de fornecedores
********************************************************************/
function selFornecedor_onchange() {
    adjustLabelsCombos();
    if (selFornecedor.value != 0)
        fillTransacao_Prazo();
    else {
        clearComboEx(['selTransacao', 'selFinanciamento']);
        selTransacao.disabled = true;
        selFinanciamento.disabled = true;
        fillGridData();
    }
}

/********************************************************************
Usuario trocou o option selecionado no combo de fornecedores
********************************************************************/
function selFornecedor2_onchange() {
    glb_sCustosReadOnly = ((selFornecedor2.selectedIndex <= 0) || (glb_sReadOnly == '*') ? '*' : '');
    adjustLabelsCombos();
    fillGridData();
}

function seldtAtualizacao_onchange() {
    if (seldtAtualizacao.selectedIndex > 0)
        chkDataSemelhante.disabled = false;
    else {
        chkDataSemelhante.disabled = true;
        chkDataSemelhante.checked = false;
    }
    fillGridData();
}

function fillTransacao_Prazo() {
    var strSQLSelect, strSQLFrom, strSQLWhere, strSQLOrderBy;
    var nEmpresaID = glb_aEmpresaData[0];
    var aEmpresaData = getCurrEmpresaData();

    clearComboEx(['selTransacao', 'selFinanciamento']);

    glb_nDOSsCmbs = 2;

    // parametrizacao do dso dsoTransacao
    setConnection(dsoTransacao);

    strSQLSelect = 'SELECT ' +
			'a.OperacaoID AS fldID, ' +
			'dbo.fn_Tradutor(a.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) AS fldName, ' +
			'dbo.fn_Tradutor(a.Operacao, ' + aEmpresaData[7] + ', ' + aEmpresaData[8] + ', NULL) AS Transacao ';

    strSQLFrom = 'FROM Operacoes a WITH(NOLOCK) ';

    strSQLWhere = 'WHERE ' +
			'(a.TipoOperacaoID = 652 AND a.EstadoID = 2 AND a.Nivel = 3 AND ' +
			'a.Entrada = 1 AND a.Fornecedor = 1 AND a.MetodoCustoID = 372) ';

    strSQLOrderBy = 'ORDER BY ISNULL(a.Ordem, 999), a.OperacaoID ';

    dsoTransacao.SQL = strSQLSelect + strSQLFrom + strSQLWhere + strSQLOrderBy;

    dsoTransacao.ondatasetcomplete = fillTransacao_Prazo_DSC;
    dsoTransacao.Refresh();

    // parametrizacao do dso dsoPrazo
    setConnection(dsoPrazo);

    strSQLSelect = 'SELECT ' +
			'c.FinanciamentoID AS fldID, ' +
			'c.Financiamento AS fldName ';
    strSQLFrom = 'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_FinanciamentosPadrao b WITH(NOLOCK), FinanciamentosPadrao c WITH(NOLOCK) ';
    strSQLWhere = 'WHERE ' +
			'(a.ObjetoID = 999 AND a.TipoRelacaoID = 12 AND a.SujeitoID = ' + nEmpresaID + ' AND ' +
			'a.RelacaoID = b.RelacaoID AND b.EstadoID = 2 AND b.FinanciamentoID = c.FinanciamentoID AND ' +
			'c.EstadoID = 2) ';

    strSQLOrderBy = 'ORDER BY c.Ordem ';

    dsoPrazo.SQL = strSQLSelect + strSQLFrom + strSQLWhere + strSQLOrderBy;

    dsoPrazo.ondatasetcomplete = fillTransacao_Prazo_DSC;
    dsoPrazo.Refresh();
}

function fillTransacao_Prazo_DSC() {
    glb_nDOSsCmbs--;

    if (glb_nDOSsCmbs > 0)
        return null;

    var i;
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selTransacao, selFinanciamento];
    var aDSOsDunamics = [dsoTransacao, dsoPrazo];

    // Inicia o carregamento de combos dinamicos (selSujeitoID, selObjetoID, selFiliadorID_07)
    //
    for (i = 0; i < aCmbsDynamics.length; i++) {
        while (!aDSOsDunamics[i].recordset.EOF) {
            optionStr = aDSOsDunamics[i].recordset['fldName'].value;
            optionValue = aDSOsDunamics[i].recordset['fldID'].value;
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            aCmbsDynamics[i].add(oOption);
            aDSOsDunamics[i].recordset.MoveNext();
        }

        if (aCmbsDynamics[i].options.length > 0)
            aCmbsDynamics[i].selectedIndex = 0;

        aCmbsDynamics[i].disabled = (aCmbsDynamics[i].options.length == 0);
    }

    fillGridData();
}

// EVENTOS DE GRID **************************************************

function fg_modalfornecedoresAfterRowColChange(oldRow, oldCol, newRow, newCol) {
    if (!glb_GridIsBuilding) {

        // Se trocou de linha, preenche o grid de impostos
        if ((chkImpostos.checked) && (oldRow != newRow)) {
            fillGridImpostos();
            return null;
        }
    }

    fg_AfterRowColChange();
}

function fillGridImpostos() {
    lockControlsInModalWin(true);
    setConnection(dsoImpostos);
    var nRelPesConFornecID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'RelPesConFornecID'));

    dsoImpostos.SQL = 'SELECT d.Conceito AS GrupoImposto, c.Imposto AS Imposto, b.Aliquota AS Aliquota, ' +
		'b.BaseCalculo AS BaseCalculo ' +
		'FROM RelacoesPesCon_Fornecedores a WITH(NOLOCK), RelacoesPesCon_Impostos b WITH(NOLOCK), Conceitos c WITH(NOLOCK), Conceitos d WITH(NOLOCK) ' +
		'WHERE (a.RelPesConFornecID = b.RelPesConFornecID AND a.RelPesConFornecID=' + nRelPesConFornecID + ' AND ' +
			'b.ImpostoID = c.ConceitoID AND a.GrupoImpostoID = d.ConceitoID)';

    dsoImpostos.ondatasetcomplete = dsoImpostos_DSC;
    dsoImpostos.Refresh();
}

function dsoImpostos_DSC() {
    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2);
    fg2.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    fg2.Editable = false;
    fg2.Redraw = 0;
    fg2.Rows = 1;

    fg2.ExplorerBar = 0;

    // Dados de Gerencia de Produto
    headerGrid(fg2, ['Grupo Imposto',
                   'Imposto',
                   'Al�quota',
                   'Base C�lc'], []);

    fillGridMask(fg2, dsoImpostos, ['GrupoImposto*',
							  'Imposto*',
							  'Aliquota' + glb_sReadOnly,
							  'BaseCalculo' + glb_sReadOnly],
							  ['', '', '999.99', '999.999'],
							  ['', '', '###.00', '###.000']);

    alignColsInGrid(fg2, [2, 3]);

    fg2.MergeCells = 4;
    fg2.MergeCol(0) = true;
    fg2.MergeCol(1) = true;

    fg2.Redraw = 0;
    paintReadOnlyCols(fg2);
    fg2.Redraw = 2;
    fg2.Col = 1;
    fg2.ExplorerBar = 5;

    // se nenhuma linha foi selecionada, seleciona a primeira
    // se a mesma existe
    if (fg2.Rows > 1)
        fg2.Row = 1;

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);
    fg2.Redraw = 2;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg2.Rows > 1) {
        window.focus();
        fg.focus();
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalfornecedoresBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    if (Button != 1)
        return true;

    glb_nLastStateClicked = Shift;
    return true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalfornecedoresDblClick(grid, Row, Col) {
    var ColKey = fg.ColKey(Col);
    var Conteudo = fg.ValueMatrix(Row, Col);

    if (!(ColKey.indexOf('*') >= 0)) {
        _retMsg = window.top.overflyGen.Confirm('Campo: ' + ColKey + '. Deseja replicar o conte�do para todas as linhas deste grid?');

        if (_retMsg == 1)
            replicaConteudo(grid, Row, Col, Conteudo);
    }

    if (glb_nModalType != 4)
        return null;

    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    glb_PassedOneInDblClick = true;

    if (fg.Col == getColIndexByColKey(fg, 'ChkOK')) {
        for (i = 2; i < fg.Rows; i++)
            fg.TextMatrix(i, getColIndexByColKey(fg, 'ChkOK')) = glb_EnableChkOK;

        glb_EnableChkOK = !glb_EnableChkOK;
    }

    Totaliza();
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfornecedoresKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfornecedores_ValidateEdit() {
    ;
}

function Totaliza() {
    var i = 0;
    var nTotalQuantidadeOK = 0;
    var nTotalQuantidade = 0;
    var nTotalVendasMes3 = 0;
    var nTotalVendasMes2 = 0;
    var nTotalVendasMes1 = 0;
    var nTotalVendasMes0 = 0;
    var nTotalMediaVendas13 = 0;
    var nTotalPrevisaoVendas = 0;
    var nTotalPrecoTotal_2 = 0;
    var nTotalContribuicaoTotal = 0;
    var nTotalEstoque_2 = 0;
    var nTotalComprarExcesso_2 = 0;

    var nSavedRow = 0;

    fg.Redraw = 0;
    nSavedRow = fg.Row;

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true,
	    [
	        [getColIndexByColKey(fg, 'Estado*'), '######', 'C'],
	        [getColIndexByColKey(fg, 'Produto*'), '######', 'C'],
		    [getColIndexByColKey(fg, 'VendasMes3*'), '######0', 'S'],
		    [getColIndexByColKey(fg, 'VendasMes2*'), '######0', 'S'],
		    [getColIndexByColKey(fg, 'VendasMes1*'), '######0', 'S'],
		    [getColIndexByColKey(fg, 'VendasMes0*'), '######0', 'S'],
		    [getColIndexByColKey(fg, 'MediaVendas13*'), '######0', 'S'],
		    [getColIndexByColKey(fg, 'PrevisaoVendas' + glb_sReadOnly), '######0', 'S'],
		    [getColIndexByColKey(fg, 'PrecoTotal_2*'), '###,###,##0.00', 'S'],
		    [getColIndexByColKey(fg, 'ContribuicaoTotal*'), '###,###,##0.00', 'S'],
		    [getColIndexByColKey(fg, 'Estoque_2*'), '######0', 'S'],
		    [getColIndexByColKey(fg, 'ComprarExcesso_2*'), '######0', 'S']

	    ], true);

    glb_totalCols__ = true;
    for (i = 2; i < fg.Rows; i++) {
        nTotalQuantidade++;

        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'ChkOK')) != 0) {
            nTotalQuantidadeOK++;
            nTotalVendasMes3 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMes3*'));
            nTotalVendasMes2 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMes2*'));
            nTotalVendasMes1 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMes1*'));
            nTotalVendasMes0 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMes0*'));
            nTotalMediaVendas13 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'MediaVendas13*'));
            nTotalPrevisaoVendas += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrevisaoVendas' + glb_sReadOnly));
            nTotalPrecoTotal_2 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoTotal_2*'));
            nTotalContribuicaoTotal += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ContribuicaoTotal*'));
            nTotalEstoque_2 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Estoque_2*'));
            nTotalComprarExcesso_2 += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ComprarExcesso_2*'));

        }
    }

    if (fg.Rows > 2) {
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Estado*')) = nTotalQuantidadeOK;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Produto*')) = nTotalQuantidade;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'VendasMes3*')) = nTotalVendasMes3;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'VendasMes2*')) = nTotalVendasMes2;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'VendasMes1*')) = nTotalVendasMes1;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'VendasMes0*')) = nTotalVendasMes0;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'MediaVendas13*')) = nTotalMediaVendas13;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'PrevisaoVendas' + glb_sReadOnly)) = nTotalPrevisaoVendas;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'PrecoTotal_2*')) = nTotalPrecoTotal_2;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ContribuicaoTotal*')) = nTotalContribuicaoTotal;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Estoque_2*')) = nTotalEstoque_2;
        fg.TextMatrix(1, getColIndexByColKey(fg, 'ComprarExcesso_2*')) = nTotalComprarExcesso_2;

    }

    fg.Row = nSavedRow;
    fg.Redraw = 2;
}

function btn_Imprimir_Clicked() {
    var aEmpresaData = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    var gridLine = fg.gridLines;
    var sMsg = aEmpresaData[6] + '     ';
    fg.gridLines = 2;

    if (glb_nModalType == 9) {
        sMsg += translateTerm('Posi��o de produtos', null) + '     ';

        var oldColWidth = fg.ColWidth(getColIndexByColKey(fg, 'Observacao' + glb_sReadOnly));
        fg.ColWidth(getColIndexByColKey(fg, 'Observacao' + glb_sReadOnly)) = oldColWidth * 1.6;

        var aCmbFiltro = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCmbCurrDataInControlBar(' + '\'' + 'sup' + '\'' + ', 2)');
        if (aCmbFiltro != null)
            sMsg += '(' + aCmbFiltro[2] + ')' + '     ';

        sMsg += getCurrDate();

        fg.PrintGrid(sMsg, false, 2, 0, 450);
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Produto*')) = '';
        fg.TextMatrix(1, getColIndexByColKey(fg, 'Observacao' + glb_sReadOnly)) = '';
        fg.ColWidth(getColIndexByColKey(fg, 'Observacao' + glb_sReadOnly)) = oldColWidth;
    }
    else
        fg.PrintGrid('Dados de Fornecedores', false, 2, 0, 450);

    fg.gridLines = gridLine;
}

function btn_Excel_Clicked() {
    lockControlsInModalWin(true);

    var frameReport = document.getElementById("frmReport");
    frameReport.onreadystatechange = reports_onreadystatechange;
    frameReport.contentWindow.location = selectString(true);
}

function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear() + " ";
    s += padL(d.getHours().toString(), 2, '0') + ":";
    s += padL(d.getMinutes().toString(), 2, '0');

    return (s);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalfornecedores_AfterEdit(Row, Col) {
    afterEdit_grid(Row, Col);
}

function fg_MouseDown_fornecedores(nRow, nCol) {
    if (glb_nModalType != 4)
        return null;

    // Soh aceita Ctrl, shift e alt
    if ((glb_nLastStateClicked != 1) && (glb_nLastStateClicked != 2) && (glb_nLastStateClicked != 4))
        return null;

    var sColValueToCheck = '';
    var nSavedRow = 0;
    var i = 0;
    var bEnableChkOK = false;

    if (fg.Col == getColIndexByColKey(fg, 'ChkOK')) {
        bEnableChkOK = (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ChkOK')) != 0);
        nSavedRow = nRow;

        // Shift
        if (glb_nLastStateClicked == 1)
            sColValueToCheck = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Proprietario*'));
            // Ctrl
        else if (glb_nLastStateClicked == 2)
            sColValueToCheck = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'abrevCor'));
            // Alt
        else if (glb_nLastStateClicked == 4)
            sColValueToCheck = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Estado*'));

        if (sColValueToCheck != '') {
            for (i = 2; i < fg.Rows; i++) {
                // Shift
                if (glb_nLastStateClicked == 1) {
                    if (sColValueToCheck == fg.TextMatrix(i, getColIndexByColKey(fg, 'Proprietario*')))
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'ChkOK')) = bEnableChkOK;
                }
                    // Ctrl
                else if (glb_nLastStateClicked == 2) {
                    if (sColValueToCheck == fg.TextMatrix(i, getColIndexByColKey(fg, 'abrevCor')))
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'ChkOK')) = bEnableChkOK;
                }
                    // Alt
                else if (glb_nLastStateClicked == 4) {
                    if (sColValueToCheck == fg.TextMatrix(i, getColIndexByColKey(fg, 'Estado*')))
                        fg.TextMatrix(i, getColIndexByColKey(fg, 'ChkOK')) = bEnableChkOK;
                }
            }

            glb_EnableChkOK = !glb_EnableChkOK;
        }

        Totaliza();
    }

    glb_nLastStateClicked = null;
}

// FINAL DE EVENTOS DE GRID *****************************************

function exportarPrecos() {
    //lockControlsInModalWin(true);

    var relacoes = "";
    var strParameters = "";
    var sLinguaLogada = getDicCurrLang();

    dsoGrid.recordset.MoveFirst();

    if (!(dsoGrid.recordset.BOF || dsoGrid.recordset.EOF)) {
        while (!dsoGrid.recordset.EOF) {

            relacoes += dsoGrid.recordset['RelacaoID'].value;

            dsoGrid.recordset.MoveNext();

            if (!dsoGrid.recordset.EOF)
                relacoes += ',';
        }

        dsoGrid.recordset.MoveFirst();
    }

    strParameters =  "sRelacoes=" + escape(relacoes);
    strParameters += "&sLinguaLogada=" + escape(sLinguaLogada);

    var frmExportarPrecos = document.getElementById("frmExportarPrecos");
    frmExportarPrecos.onreadystatechange = export_onreadystatechange;
    frmExportarPrecos.contentWindow.location = SYS_PAGESURLROOT + '/serversidegenEx/fornecedorExportaPrecos.aspx?' + strParameters;;
}

function importarPrecos() {

    divControls.style.visibility = 'hidden';
    divUpload.style.visibility = 'inherit';
    var iframe = document.getElementById("I1");
    iframe.style.width = 974;

    var nEmpresaData = getCurrEmpresaData();

    strPars = '?nUserID=' + escape(glb_nUserID);
    strPars += '&nPaisID=' + escape(nEmpresaData[1]);

    window.open(SYS_ASPURLROOT + '/serversidegenEx/importadadosfornecedor.aspx' + strPars, 'I1');

    btnImportar.disabled = true;
}

function btnCancelar_onclick(gravacaoOK, mensagem) {

    divUpload.style.visibility = 'hidden';
    divControls.style.visibility = 'inherit';

    var iframe = document.getElementById("I1");
    iframe.style.width = 0;

    btnImportar.disabled = false;
    btnListar.disabled = false;
    
    lockControlsInModalWin(false);

    if (mensagem != null) {
        if (window.top.overflyGen.Alert(mensagem) == 0)
            return null;
    }

    if (gravacaoOK != null)
        fillGridData();
}

function adjustLabelsCombos() {
    setLabelOfControl(lblFornecedor, selFornecedor.value);
    setLabelOfControl(lblFornecedor2, selFornecedor2.value);
}

function window_onunload() {
    dealWithObjects_Unload();
}

function replicaConteudo(grid, Row, Col, Conteudo) {
    for (var i = 1; i < fg.Rows; i++) {
        if (i != Row) {
            fg.TextMatrix(i, Col) = Conteudo;
            afterEdit_grid(i, Col);
        }
    }
}

function afterEdit_grid(Row, Col) {
    var nType;

    if (glb_nModalType == 9) {

        if ((Col == getColIndexByColKey(fg, 'QuantidadeComprar')) ||
                (Col == getColIndexByColKey(fg, 'CustoComprar'))) {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
            fg.TextMatrix(Row, getColIndexByColKey(fg, 'CustoTotalComprar*')) = (fg.ValueMatrix(Row, getColIndexByColKey(fg, 'CustoComprar')) * fg.ValueMatrix(Row, getColIndexByColKey(fg, 'QuantidadeComprar')));
            Totaliza();
            return;
        }
    }

    if (fg.Editable) {
        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.moveFirst();

        if (glb_nModalType == 9)
            dsoGrid.recordset.Find('_RelacaoID', fg.TextMatrix(Row, getColIndexByColKey(fg, 'RelacaoID*')));
        else
            dsoGrid.recordset.Find('RelPesConFornecID', fg.TextMatrix(Row, getColIndexByColKey(fg, 'RelPesConFornecID')));

        if (Col == getColIndexByColKey(fg, 'ChkOK')) {
            setupBtnsFromGridState();
            Totaliza();
        }
        else if (!(dsoGrid.recordset.EOF)) {

            // LOG - sempre altera o UsuarioID
            if (LOG_ACT_FLD != null) {
                if (fieldExists(dsoGrid, LOG_ACT_FLD))
                    dsoGrid.recordset[LOG_ACT_FLD].value = glb_USERID;
            }

            // Operacao feita somente para garantir o valor do estadoid na trigger
            if (fieldExists(dsoGrid, '_EstadoID') || fieldExists(dsoGrid, '_EstadoID'))
                dsoGrid.recordset['_EstadoID'].value = dsoGrid.recordset['_EstadoID'].value;

            if (fieldExists(dsoGrid, fg.ColKey(Col)))
                nType = dsoGrid.recordset[fg.ColKey(Col)].type;
            else
                return true;

            // Se decimal , numerico , int, bigint ou boolean
            if ((nType == 131) || (nType == 14) || (nType == 11) || (nType == 3) || (nType == 20)) {
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
                dsoGrid.recordset[fg.ColKey(Col)].value = fg.ValueMatrix(Row, Col);
                if (glb_nModalType == 10) {
                    if (Col == getColIndexByColKey(fg, 'CustoFOB' + glb_sCustosReadOnly)) {
                        dsoGrid.recordset['dtAtualizacao'].value = getCurrDate();
                        if (chkRetCustoReposicao.checked) {
                            dsoGrid.recordset['CustoFOB'].value = (dsoGrid.recordset['CustoFOB'].value /
                                (1 + (dsoGrid.recordset['AliquotaNaoInc'].value / 100)));
                        }
                    }
                    else if (Col == getColIndexByColKey(fg, 'CustoReposicao*')) {
                        dsoGrid.recordset['dtAtualizacao'].value = getCurrDate();
                        if (chkRetCustoReposicao.checked) {
                            dsoGrid.recordset['CustoReposicao'].value = (dsoGrid.recordset['CustoReposicao'].value /
                                (1 + (dsoGrid.recordset['AliquotaNaoInc'].value / 100)));
                        }
                    }
                }
            }
                // Tratamento de data
            else if (nType == 135) {
                if (fg.TextMatrix(fg.Row, Col) != '') {
                    if (chkDataEx(fg.TextMatrix(fg.Row, Col)) == null) {
                        // a data e nula, zera o campo
                        fg.TextMatrix(fg.Row, Col) = '';
                        return true;
                    }

                    // verifica se a mascara de digitacao tem hora e se tiver
                    // e o usuario nao digitou estripa a hora completa ajusta
                    // a mascara: '99/99/9999 99:99'
                    theMask = fg.ColEditMask(Col);

                    // a mascara tem hora?
                    dateMask = '';
                    hourMask = '';
                    if (theMask.lastIndexOf(':') >= 0) {
                        dateMask = trimStr(theMask.substr(0, theMask.lastIndexOf(' ')));
                        hourMask = trimStr(theMask.substr(theMask.lastIndexOf(' ')));
                    }

                    if (hourMask != '') {
                        strDate = '';
                        strHour = '';

                        strDate = trimStr((fg.TextMatrix(fg.Row, Col)).substr(0, theMask.lastIndexOf(' ')));
                        strHour = trimStr((fg.TextMatrix(fg.Row, Col)).substr(theMask.lastIndexOf(' ')));

                        // O usuario nao digitou a parte de hora e minuto
                        if (strHour == ':')
                            fg.TextMatrix(fg.Row, Col) = strDate + ' 00:00';
                    }

                    if (trimStr(fg.TextMatrix(fg.Row, Col)) == '/  /')
                        dsoGrid.recordset[fg.ColKey(Col)].value = null;
                    else if (chkDataEx(fg.TextMatrix(fg.Row, Col)) == false) {
                        if (window.top.overflyGen.Alert('Data inv�lida!') == 0)
                            return null;

                        fg.TextMatrix(fg.Row, Col) = '';

                        return -1;
                    }
                    else
                        dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);
                }
            }
            else
                dsoGrid.recordset[fg.ColKey(Col)].value = fg.TextMatrix(Row, Col);

            glb_dataGridWasChanged = true;
            setupBtnsFromGridState();
        }
    }
}

function reports_onreadystatechange() {
    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
    (frameReport.contentWindow.document.readyState == 'interactive') ||
    (frameReport.contentWindow.document.readyState == 'complete')) {
        lockControlsInModalWin(false);
    }
}

function export_onreadystatechange() {
    var frmExportarPrecos = document.getElementById("frmExportarPrecos");

    if ((frmExportarPrecos.contentWindow.document.readyState == 'loaded') ||
    (frmExportarPrecos.contentWindow.document.readyState == 'interactive') ||
    (frmExportarPrecos.contentWindow.document.readyState == 'complete')) {
        lockControlsInModalWin(false);
    }
}

function js_fg_modalfornecedoresMouseMove(grid, Button, Shift, X, Y) {

    var currRow = fg.MouseRow;
    var currCol = fg.MouseCol;

    var colDePara = null;

    if (currRow > 0 && currCol > 0) {

        if (fg.ColKey(currCol) == 'CustoFOB')
            colDePara = getColIndexByColKey(fg, 'CustoFOB_Auditoria');

        if (fg.ColKey(currCol) == 'CustoReposicao*')
            colDePara = getColIndexByColKey(fg, 'CustoReposicao_Auditoria');

        if (fg.ColKey(currCol) == 'PrazoReposicao' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'PR_Auditoria');

        if (fg.ColKey(currCol) == 'PortoOrigemID')
            colDePara = getColIndexByColKey(fg, 'PO_Auditoria');

        if (fg.ColKey(currCol) == 'ViaTransporteID')
            colDePara = getColIndexByColKey(fg, 'VT_Auditoria');

        if (fg.ColKey(currCol) == 'FIE*')
            colDePara = getColIndexByColKey(fg, 'FIE_Auditoria');

        if (fg.ColKey(currCol) == 'FIEAutomatico*')
            colDePara = getColIndexByColKey(fg, 'Auto_Auditoria');
        
        if (fg.ColKey(currCol) == 'PrazoPagamento' + glb_sReadOnly)
            colDePara = getColIndexByColKey(fg, 'PP_Auditoria');

        if (colDePara != null)
            glb_aCelHint = [[currRow, currCol, fg.TextMatrix(currRow, colDePara)]];
    }
}