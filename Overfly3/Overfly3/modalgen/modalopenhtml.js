/********************************************************************
modalopenhtml.js

Library javascript para o modalopenhtml.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoSolicitacaoCadastro = new CDatatransport("dsoSolicitacaoCadastro");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
FUNCOES GERAIS:

window_onload()
btn_onclick(ctl)
adjustFrame()
showDocumentFrame(action)
setupPage()
loadPageFromServerInFrame(nDocumentoID, sTitulo)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES
/********************************************************************
Window on Load - Configura o html
********************************************************************/
function window_onload()
{
	// garante o caret nao permanecer em campo do pesqlist
	if (glb_sCaller == 'PL')
	{
		sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'window.focus()');
		sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'fg.focus()');
	}	
	
    window_onload_1stPart();
    
    //Resumo de Pessoa
    if (glb_nTipoID==3)
    {
        var nB3A1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "getCurrRightValue('SUP','B3A1')");
        var nB3A2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "getCurrRightValue('SUP','B3A2')");
        
        var nB4A1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "getCurrRightValue('SUP','B4A1')");
        var nB4A2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "getCurrRightValue('SUP','B4A2')");
        
        var nB5C1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "getCurrRightValue('SUP','B5C1')");
        var nB5C2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "getCurrRightValue('SUP','B5C2')");
                
        var EhResponsavel = 0;
        
        if (glb_sCaller == 'PL')
            EhResponsavel = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Responsavel'))");
        else if (glb_sCaller == 'S')
            EhResponsavel = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['Responsavel'].value");            
        
        if ((nB3A1==0) || (nB3A2==0))
            btnImagem.disabled = true;    
            
        if ((nB4A1==0) || (nB4A2==0))     
            btnConfirmacaoCadastro.disabled = true;    
            
        if (!(((nB5C1==1)&&(nB5C2==1)) || ((EhResponsavel==1)&&(nB5C1==1)&&(nB5C2==0))))
        {
            btnImprimir.disabled = true;
            btnEnviarEmail.disabled = true;
        }
    }
    else
    {
        btnSolicitacaoCadastro.style.visibility = 'hidden';
        btnImagem.style.visibility = 'hidden';
        btnConfirmacaoCadastro.style.visibility = 'hidden';
        btnEnviarEmail.style.visibility = 'hidden';
        btnImprimir.style.visibility = 'hidden';
    }
    
    // trava a interface
    lockControlsInModalWin(true);
    
	var elemToFocus;
    var aEmpresaData;
    var frameRect;
    
    // ajusta o body do html
    with (modalopenHtmlBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
    
    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
		frameRect[1] += 3;
		moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }
    
    // ajusta o frame de documento e carrega o primeiro documento
	adjustFrame();
		
	loadPageFromServerInFrame(10, 'ABC');
	
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // sempre dar foco, no minimo na janela e em um controle da janela
    window.focus();

    elemToFocus = btnCanc;

	if ( !elemToFocus.disabled && (elemToFocus.currentStyle.visibility != 'hidden') )
	    elemToFocus.focus();

    // Define TOP da modal
	modalFrame = getFrameInHtmlTop('frameModal');
	modalFrame.style.top = 50;
}    

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        // btnCanc.focus();
        ;
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    if (ctl.id == btnSolicitacaoCadastro.id )
    {
        solicitacaoCadastro();
    }    
    else if (ctl.id == btnImagem.id )
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
        //loadModalImagem();
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "__openModalDocumentos('SUP', " + glb_nRegistroID + ")");
    }    
    else if (ctl.id == btnConfirmacaoCadastro.id )
    {
        openModalConfirmacaoCadastro();
    }    
    else if (ctl.id == btnEnviarEmail.id )
    {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
        openModalEmailConfirmacao();
    }   
    else if (ctl.id == btnImprimir.id )        
    {
        imprimirConteudo();     
    }
    else if (ctl.id == btnSocios.id) {
        var nPessoaID = 0;
        var nTipoPessoaID = 0;
        var nCreditoID = 0;

        if (glb_sCaller == 'PL') {
            nPessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID'))");
            nTipoPessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoPessoaID'))");
            nCreditoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ID'))");
        }
        else {
            nPessoaID = (sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ParceiroID'].value") == null) ?
            sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID'))") :
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['ParceiroID'].value");

            nTipoPessoaID = (sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoPessoaID'].value") == null) ?
            sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoPessoaID'))") :
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['TipoPessoaID'].value");

            nCreditoID = (sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['CreditoID'].value") == null) ?
            sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ID'))") :
            sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['CreditoID'].value");;
        }
        openModalSocios(nPessoaID, nTipoPessoaID, nCreditoID);
    } 
    else if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null );
    }
    else if (ctl.id == btnCanc.id )
    {
		// 2. O usuario clicou o botao Fechar
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
	}
}

/********************************************************************
Ajusta o frame que carrega as paginas e carrega a primeira pagina
********************************************************************/
function adjustFrame()
{
	var oFrame = null;
	var hGap = ELEM_GAP;
	var vGap = ELEM_GAP;
	var i;
	var coll;

    // todos os iframes
    coll = document.all.tags('IFRAME');

    for (i=0;i<coll.length; i++)
    {
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);
			
			with (oFrame.style)
			{
				visibility = 'hidden';
				left = hGap;
				top = parseInt(divMod01.currentStyle.top, 10) + 
				      parseInt(divMod01.currentStyle.height, 10) + vGap;
				width = parseInt(divMod01.currentStyle.width, 10) - (2 * hGap);
				
				//Resumo Pessoa
                if (glb_nTipoID==3)				
                {
				    //height = 410;
				    height = parseInt(btnOK.currentStyle.top, 10) - parseInt(oFrame.currentStyle.top, 10);
                }
                else				             
                {
                    if ((glb_nTipoID == 12) && ((glb_sCaller == 'S') || (glb_sCaller == 'PL')))
                    {
                        top = parseInt(divMod01.currentStyle.top, 10) +
                            parseInt(divMod01.currentStyle.height, 10) + (vGap * 4);
                    }

                    height = ( parseInt(btnOK.currentStyle.top, 10) +
						       parseInt(btnOK.currentStyle.height, 10) + vGap) -
				             parseInt(oFrame.currentStyle.top, 10);
                }

				backgroundColor = 'white';
			}

			break;
        }
    }
}

/********************************************************************
Mostra ou esconde o frame que carrega o documento
********************************************************************/
function showDocumentFrame(action)
{
	var coll, i, oFrame;
	
	// todos os iframes
    coll = document.all.tags('IFRAME');
    
    for (i=0;i<coll.length; i++)
    {
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);
			
			with (oFrame.style)
			{
				if ( action )
					visibility = 'inherit';
				else	
					visibility = 'hidden';
			}
			
			break;
        }
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    var modWidth = 0;
    var modHeight = 0;
                         
    // largura e altura da janela modal
    if (glb_nTipoID == 3)
        frameRect = [10, 57, 990, 540];
    else
        frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    //Resumo de Pessoa
    if ((glb_nTipoID == 3) || ((glb_nTipoID == 12) && ((glb_sCaller == 'S') || (glb_sCaller == 'PL'))))
    {
        redimAndReposicionModalWin(modWidth, modHeight, false);

        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

	secText(glb_sTitulo, 1);

	btnOK.style.visibility = 'hidden';
	btnOK.disabled = true;

	btnCanc.style.height = parseInt(btnOK.currentStyle.height, 10);
	btnCanc.value = 'Fechar';
	btnCanc.style.visibility = 'hidden';
    centerBtnInModal(btnCanc);
    btnSocios.style.visibility = ((glb_nTipoID == 12) && ((glb_sCaller == 'S') || (glb_sCaller == 'PL'))) ? 'inherit' : 'hidden';

	
	adjustElementsInForm([
	    ['btnSolicitacaoCadastro','btn',130,1,134,-2],
		['btnImagem','btn',60,1,10],
		['btnConfirmacaoCadastro','btn',130,1,10],
		['btnEnviarEmail','btn',80,1,10],
		['btnImprimir','btn',60,1,10],
		['btnSocios','btn',80,1,10]
	], null, null, true);		
	
	with (divButton.style)
    {
    	visibility = 'inherit';    	        
        top = (modHeight - (parseInt(btnConfirmacaoCadastro.currentStyle.height, 10) + 25)) - 10;        
        width = modWidth-10;
        height = btnConfirmacaoCadastro.currentStyle.height;
    }

    with (divButtonSup.style)
    {
        visibility = 'inherit';
        top = ELEM_GAP;           
        left = (ELEM_GAP - 20);
        width = modWidth - 10;
        height = btnConfirmacaoCadastro.currentStyle.height;
	}

	if (glb_nTipoID == 3)
	{
	    btnSolicitacaoCadastro.style.left = 247;
	    btnImagem.style.left = parseInt(btnSolicitacaoCadastro.currentStyle.left, 10) + parseInt(btnSolicitacaoCadastro.currentStyle.width, 10) + 10;
	    btnConfirmacaoCadastro.style.left = parseInt(btnImagem.currentStyle.left, 10) + parseInt(btnImagem.currentStyle.width, 10) + 10;
	    btnEnviarEmail.style.left = parseInt(btnConfirmacaoCadastro.currentStyle.left, 10) + parseInt(btnConfirmacaoCadastro.currentStyle.width, 10) + 10;
	    btnImprimir.style.left = parseInt(btnEnviarEmail.currentStyle.left, 10) + parseInt(btnEnviarEmail.currentStyle.width, 10) + 10;
    }
    else if ((glb_nTipoID == 12) && ((glb_sCaller == 'S') || (glb_sCaller == 'PL')))
    {
        btnSocios.style.left = 20;
    }
}

/********************************************************************
Carrega as paginas do servidor.

Parametros:
	nDocumentoID - id do registro a carregar
	sTitulo - Titulo da Janela
********************************************************************/
function loadPageFromServerInFrame(nDocumentoID, sTitulo) {
    
	var i, coll, oFrame;
	var strPars = '';

    var empresa = getCurrEmpresaData();
    var nEmpresaID = empresa[0];
    var nUserID = getCurrUserID();

    // todos os iframes
    coll = document.all.tags('IFRAME');

    // Carrega novo documento se for o caso
	for (i=0;i<coll.length; i++)
	{
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);
			oFrame.tabIndex = -1;
			strPars = '?nRegistroID=' + escape(glb_nRegistroID);
			strPars += '&nTipoID=' + escape(glb_nTipoID);
			strPars += '&nTemFoto=' + escape(glb_nTemFoto);
			strPars += '&nEmpresaID=' + escape(nEmpresaID);
			strPars += '&nUsuarioID=' + escape(nUserID);

			oFrame.src = SYS_PAGESURLROOT + '/modalgen/documentohtml.asp' + strPars;

			break;
	    }
	}
}

/********************************************************************
Criado pelo programador
Abre a JM

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function openModalEmailConfirmacao()
{
    var htmlPath = SYS_PAGESURLROOT + '/basico/pessoas/modalpages/modalemailcadastro.asp';

    var strPars = new String();
    strPars = '?sCaller=' + escape('S');
    strPars += '&nPessoaID=' + escape(glb_nRegistroID);

    showModalWin( (htmlPath + strPars) , new Array(0, 0) );
}

function loadModalImagem()
{
	var strPars = new String();
	var nTipoPessoaID = '';

    if (glb_sCaller	== 'S')
	    nTipoPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['TipoPessoaID'].value");
    else if (glb_sCaller == 'PL')
        nTipoPessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoPessoaID'))");
	
    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')') );

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')') );

	strPars = '?nRegistroID=' + escape(glb_nRegistroID);
	strPars += '&nTipoRegistroID=' + escape(nTipoPessoaID);
	strPars += '&sCaller=' + escape(glb_sCaller);
	
	
	//DireitoEspecifico
    //Pessoas->F�sicas/jur�dicas->Modal Imagens
    //20100 SFS-Grupo Pessoas -> A1&&A2 -> Permite abrir a modal para gerenciar imagens, caso contrario apenas mostra a imagem cadastrada.
    if ( (nA1 == 1) && (nA2 == 1) )
    {
		htmlPath = SYS_ASPURLROOT + '/modalgen/modalgerimagens.asp'+strPars;
    	
		//showModalWin(htmlPath, new Array(524, 488));
		showModalWin(htmlPath, new Array(724, 488));
    }
    else
    {
		htmlPath = SYS_ASPURLROOT + '/modalgen/modalshowimagens.asp'+strPars;
    
		showModalWin(htmlPath, new Array(0,0));
    }
}

/********************************************************************
Carrega as paginas do servidor.

Parametros:
	nDocumentoID - id do registro a carregar
	sTitulo - Titulo da Janela
********************************************************************/
function imprimirConteudo()
{
	window.frames(0).imprimirResumo();
	
    // esta funcao destrava o html contido na janela modal
    lockControlsInModalWin(false);
}

function openModalConfirmacaoCadastro()
{       
    var htmlPath;
    var strPars = new String();
    var nWidth = 790;
    var nHeight = 514;        
    var aEmpresa = getCurrEmpresaData();

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape(glb_sCaller);     
    strPars += '&nRegistroID=' + escape(glb_nRegistroID);
    strPars += '&nEmpresaPaisID=' + escape(aEmpresa[1]);
    

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/basico/pessoas/modalpages/modalconfirmacaocadastro.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth,nHeight));   
}

function solicitacaoCadastro()
{
    var strPars = '?nPessoaID=' + escape(glb_nRegistroID);
    lockControlsInModalWin(true);
	dsoSolicitacaoCadastro.URL = SYS_ASPURLROOT + '/basico/pessoas/serverside/solicitacaocadastro.aspx' + strPars;
	dsoSolicitacaoCadastro.ondatasetcomplete = dsoSolicitacaoCadastro_DSC;
	dsoSolicitacaoCadastro.Refresh();
}

function dsoSolicitacaoCadastro_DSC() {
    if (!((dsoSolicitacaoCadastro.recordset.BOF) || (dsoSolicitacaoCadastro.recordset.EOF)))
    {
        window.top.overflyGen.Alert(dsoSolicitacaoCadastro.recordset['PosicaoFila'].value);
		lockControlsInModalWin(false);
		return null;
    }
    
    lockControlsInModalWin(false);
}

/********************************************************************
Funcao criada pelo programador.
Chama a Modal S�cios

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalSocios(nPessoaID, nTipoPessoaID, nCreditoID) {

    var htmlPath;
    var strPars = new String();
    var nWidth = 1000;
    var nHeight = 600;
    var userID = getCurrUserID();
    nPessoaID = (nPessoaID < 0) ? nPessoaID * -1 : nPessoaID;

    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape(glb_sCaller);  
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nTipoPessoaID=' + escape(nTipoPessoaID);
    strPars += '&nCreditoID=' + escape(nCreditoID);
    strPars += '&nUserID=' + escape(userID);

    // carregar a modal
    htmlPath = SYS_ASPURLROOT + '/modalgen/modalsocios.asp' + strPars;
    showModalWin(htmlPath, new Array(nWidth, nHeight));
}