
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    
    'Forca recarregamento da pagina
    Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
    
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, discPrgRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
        
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    discPrgRoot = objSvrCfg.RootLogicAppPath(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	'Esta funcao retorna a string de pesquisa no banco
	'de uma imagem. Se a imagem nao existe, retorna ""
	'IMPORTANTE: Esta funcao deve ficar apos o trecho de codigo ASP
	'que cria a variavel strConn
	Function imageSourceString (nFormID, nSubFormID, nRegistroID, nTamanho, nOrdem, sVersao)
		Dim rsData, strSQLGet
		Dim strRoot, srvName, strVersao, strOrder
		
		Set rsData = Server.CreateObject("ADODB.Recordset")		
		strVersao = ""
        strOrder = ""
	
	    If (nFormID = 7110 AND nSubFormID = 26000) Then	        
	        If (IsNull(sVersao) OR sVersao="") Then
	            'strOrder = "a.Versao IS NULL AND "            
                strOrder = ", (CASE WHEN a.Versao IS NULL THEN 0 ELSE 1 END), a.Versao DESC "
            Else
                'strVersao = "a.Versao = " & CStr(sVersao) & " AND "
                strOrder = ", (CASE a.Versao WHEN " & CStr(sVersao) & " THEN 0 ELSE 1 END), a.Versao DESC "
            End If	            
	    End If	            
	    
        strSQLGet = "SELECT TOP 1 a.DocumentoID AS ImagemID FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] a WITH(NOLOCK) " & _
            "WHERE a.FormID = " & CStr(nFormID) & " AND "& _
            "a.SubFormID = " & CStr(nSubFormID) & " AND "& _
            strVersao & "a.RegistroID = " & CStr(nRegistroID) & " AND " & _
            "a.Arquivo IS NOT NULL AND " & _
            "a.TipoArquivoID = 1451 " & _
            "ORDER BY a.DocumentoID" & strOrder
	    
		rsData.Open strSQLGet, strConn, adOpenStatic , adLockReadOnly, adCmdText
	
		If (rsData.RecordCount = 0) Then
			rsData.Close
			Set rsData = Nothing
			imageSourceString = ""
			Exit Function
		End If
		
		rsData.Close
		Set rsData	 = Nothing
	
		srvName = Request.ServerVariables("SERVER_NAME")
		
		If ( Instr(1,UCase(srvName), ".COM", 1)= 0	) Then
			strRoot = "http:/" & "/localhost/overfly3"
		Else
			strRoot = pagesURLRoot	
		End If
	
        imageSourceString = strRoot & "/serversidegen/imageblob.asp" & _
                            "?nFormID=" & CStr(nFormID) & _
                            "&nSubFormID=" & CStr(nSubFormID) & _
                            "&nRegistroID=" & CStr(nRegistroID)	& _
                            "&sVersao=" & CStr(sVersao)
	End Function
%>

<%
Function ReplaceText(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceText = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function

Function MatchText(baseStr, patrn)
    Dim regEx, Match, Matches    ' Create variable.
    Set regEx = New RegExp       ' Create regular expression.
    regEx.Pattern = patrn        ' Set pattern.
    regEx.IgnoreCase = True      ' Set case insensitivity.
    regEx.Global = True          ' Set global applicability.
    Set Matches = regEx.Execute(baseStr)   ' Execute search.
       
    MatchText = Matches.Count
End Function
%>

<html id="modaldocumentotemplateHtml" name="modaldocumentotemplateHtml">

<head>

<title></title>

<%
	Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/documentotemplate.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_overlist.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_overhtmlmounter.js" & Chr(34) & "></script>" & vbCrLf
    
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodaltemplatedoc.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/documentotemplate.js" & Chr(34) & "></script>" & vbCrLf
%>


<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

'btn3' -> Procedimento
'btn4' -> Bot�o 4
Dim sCallOrigin2

Dim i, nEmpresaID, nDocumentoID, nTipoDocumentoID, nTipoDocToLoad, sTexto, nFormatoData, sTitleWindow, sState
Dim nAction, sAnchorID
Dim nUserID
Dim nCounter
Dim sSignificado
Dim IndiceID
Dim sUsuarioLogado
Dim dtEmissaoDocumento

Dim rsData, rsData2, rsData3, rsData4, rsData5, rsData6, rsDataCheck
Dim strSQL, strSQL2, strSQL3, strSQLCheck
Dim rsSPCommand
Dim rsDataIPars
Dim strSQLIPars
Dim nEstadoID, sTransicao
Dim bFlag, nAreaDocMapID
Dim sImageWebFullPath
Dim docDestino
Dim ancoraDestino
Dim nEstadoFN1
Dim nEstadoFN2
Dim sTitulo
Dim sColor
Dim sFiltro
Dim sVersaoDocumento
Dim nOrdemID
Dim sBackground

sCallOrigin2 = ""
sAnchorID = ""
nEmpresaID = 0
nDocumentoID = 0
nTipoDocumentoID = 0
nTipoDocToLoad = 0
sTexto = ""
nFormatoData = 103
sTitleWindow = ""
nAction = -2
nUserID = 0
IndiceID = -1
sState = ""
bFlag = False
nEstadoFN1 = 0
nEstadoFN2 = 0
sTitulo = ""
sColor = "Transparent"
sFiltro = ""
'nOrdemID = 0
'sBackground = "#0000FF"

For i = 1 To Request.QueryString("sCallOrigin2").Count
    sCallOrigin2 = Request.QueryString("sCallOrigin2")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nDocumentoID").Count
    nDocumentoID = Request.QueryString("nDocumentoID")(i)
Next

For i = 1 To Request.QueryString("nTipoDocToLoad").Count
    nTipoDocToLoad = Request.QueryString("nTipoDocToLoad")(i)
Next

For i = 1 To Request.QueryString("sAnchorID").Count
    sAnchorID = Request.QueryString("sAnchorID")(i)
Next

If CStr(nTipoDocToLoad) = "NULL" Then
	nTipoDocToLoad = Null
End If

For i = 1 To Request.QueryString("nFormatoData").Count
    nFormatoData = Request.QueryString("nFormatoData")(i)
Next

For i = 1 To Request.QueryString("nAction").Count
    nAction = Request.QueryString("nAction")(i)
Next

For i = 1 To Request.QueryString("nUserID").Count
    nUserID = Request.QueryString("nUserID")(i)
Next

Response.Write "var glb_sCallOrigin2 = " & Chr(39) & sCallOrigin2 & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nDocumentoID = " & CStr(nDocumentoID) & ";"
Response.Write vbcrlf
Response.Write "var glb_Documento = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_Versao = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_dtEmissao = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_sTexto = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_sTitleWindow = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nAction = " & CStr(nAction) & ";"
Response.Write vbcrlf
Response.Write "var glb_sAnchorID = " & Chr(39) & sAnchorID & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_aDocumentos = new Array();"
Response.Write vbcrlf
Response.Write "var glb_aReferencias = new Array();"
Response.Write vbcrlf
Response.Write "var glb_sDocumentos = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf

If ( IsNull(nTipoDocToLoad) ) Then
	Response.Write "glb_nTipoDocToLoad = null;"
Else
    nTipoDocToLoad = ReplaceText( nTipoDocToLoad, ",", ".")
    
	Response.Write "glb_nTipoDocToLoad = " & CStr(nTipoDocToLoad) & ";"
End If

Response.Write vbcrlf

Response.Write "var glb_ImgsNumber = 0;" & vbCrLf
Response.Write vbcrlf

Set rsDataCheck = Server.CreateObject("ADODB.Recordset")
	
strSQLCheck = "SELECT dbo.fn_Documento_Treinamentos(" & CStr(nUserID) & ", NULL) AS Documentos, " & _
			"dbo.fn_Documento_Estado(a.DocumentoID, 1) AS EstadoFN1, dbo.fn_Documento_Estado(a.DocumentoID, 2) AS EstadoFN2, " & _
			"dbo.fn_Pessoa_Fantasia(" & CStr(nUserID) & ", 0) AS UsuarioLogado, " & _
			"CONVERT(VARCHAR, GETDATE(), 103) + SPACE(1) + CONVERT(VARCHAR, GETDATE(), 108) AS dtEmissaoDocumento " & _
		"FROM Documentos a WITH(NOLOCK) " & _
		"WHERE a.DocumentoID = " & CStr(nDocumentoID)
			
rsDataCheck.Open strSQLCheck, strConn, adOpenDynamic, adLockReadOnly, adCmdText
	
If ( Not(rsDataCheck.BOF AND rsDataCheck.EOF)) Then

	If (Not IsNull(rsDataCheck.Fields("Documentos").Value) ) Then
		Response.Write "glb_sDocumentos = " & Chr(39) & rsDataCheck.Fields("Documentos").Value & Chr(39) & ";"
		Response.Write vbcrlf
	End If

	If (Not IsNull(rsDataCheck.Fields("EstadoFN1").Value) ) Then
		nEstadoFN1 = CInt(rsDataCheck.Fields("EstadoFN1").Value)
	Else
		nEstadoFN1 = 0
	End If
			
	If (Not IsNull(rsDataCheck.Fields("EstadoFN2").Value) ) Then
		nEstadoFN2 = CInt(rsDataCheck.Fields("EstadoFN2").Value)
	Else
		nEstadoFN2 = 0
	End If
	
    sUsuarioLogado = rsDataCheck.Fields("UsuarioLogado").Value
    dtEmissaoDocumento = rsDataCheck.Fields("dtEmissaoDocumento").Value
	
    'Manda estado para documentotemplate.js
    Response.Write vbcrlf
    Response.Write "var glb_nEstadoID = " & nEstadoFN1 & ";"
    Response.Write vbcrlf
	
End If

rsDataCheck.Close
Set rsDataCheck = Nothing

Set rsData = Server.CreateObject("ADODB.Recordset")
    
'Documento desativo
If ((Not IsNull(nTipoDocToLoad)) AND (nTipoDocToLoad > 0)) Then
	strSQL = "SELECT 2 AS EstadoID, '" & "Obsoleto" & "' AS Estado, a.DocumentoID, b.DocumentoAbreviado, (CONVERT(VARCHAR(6), b.DocumentoID) + SPACE(1) + " & _
			"ISNULL(b.DocumentoAbreviado + SPACE(1), SPACE(0)) + b.Documento) AS Documento, b.Documento AS sDocumento, dbo.fn_Numero_Formata(a.Versao, 1, 1, 101) AS Versao, " & _
			"CONVERT(VARCHAR, a.dtEmissao, " & CStr(nFormatoData) & ") AS dtEmissao, CONVERT(VARCHAR, a.dtDesativacao, " & CStr(nFormatoData) & ") AS dtDesativacao, " & _
			"a.Texto, c.ItemMasculino AS TitleWindow, b.TipoDocumentoID, " & _
			"d.Fantasia AS Proprietario, (CASE WHEN a.Treinamento = 1 THEN 'Sim' ELSE 'N�o' END) AS Treinamento " & _
		"FROM Documentos_Desativos a WITH(NOLOCK) " & _
		    "INNER JOIN Documentos b WITH(NOLOCK) ON (b.DocumentoID = a.DocumentoID) " & _
		    "INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = b.TipoDocumentoID) " & _
		    "INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.ProprietarioID) " & _
		"WHERE ((a.DocumentoID = " & CStr(nDocumentoID) & ") AND (a.Versao = " & CStr(nTipoDocToLoad) & ")) "

    sTitulo = "Documento Desativado em"
	sState = "Desativo"			
	sColor = "#FFB6C1"
	sBackground = "#FFE9EC"
	sFiltro = " AND ISNULL(dbo.fn_Documento_Estado(a.DocumentoID, 2), 2) = 2 "
	nOrdemID = 3
'Documento Atual
ElseIf (IsNull(nTipoDocToLoad)) Then
	strSQL = "SELECT a.EstadoID, c.RecursoFantasia AS Estado, a.DocumentoID, a.DocumentoAbreviado, (CONVERT(VARCHAR(6), a.DocumentoID) + SPACE(1) + " & _
			"ISNULL(a.DocumentoAbreviado + SPACE(1), SPACE(0)) + a.Documento) AS Documento, a.Documento AS sDocumento, dbo.fn_Numero_Formata(a.Versao, 1, 1, 101) AS Versao, " & _
			"CONVERT(VARCHAR, a.dtEmissao, " & CStr(nFormatoData) & ") AS dtEmissao, NULL AS dtDesativacao, "
	
	If ( CInt(nEstadoFN2) = 0 ) Then
		strSQL = strSQL & "a.Estudo AS Texto, "
		sTitulo = "Documento em Estudo" 
		sState = "Estudo"
		sColor = "#F0E68C"
		sBackground = "#F9F5D0"
		nOrdemID = 1
	Else
		strSQL = strSQL & "a.Texto AS Texto, "
		sTitulo = "Documento Ativo" 
		sState = ""		
		sColor = "#90EE90"
		sBackground = "#FFFFFF"
		nOrdemID = 2
	End If
	
	strSQL = strSQL & "b.ItemMasculino AS TitleWindow, " & _
			"a.TipoDocumentoID, d.Fantasia AS Proprietario, (CASE WHEN a.Treinamento = 1 THEN 'Sim' ELSE 'N�o' END) AS Treinamento " & _
			"FROM Documentos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK), Pessoas d WITH(NOLOCK) " & _
			"WHERE a.DocumentoID = " & CStr(nDocumentoID) & " AND a.TipoDocumentoID=b.ItemID AND " & _
			"c.RecursoID=ISNULL(dbo.fn_Documento_Estado(a.DocumentoID, 2), dbo.fn_Documento_Estado(a.DocumentoID, 1)) AND " & _
			"a.ProprietarioID = d.PessoaID "
	sFiltro = " AND ISNULL(dbo.fn_Documento_Estado(a.DocumentoID, 2), 2) = 2 "

'Em Estudo
Else
	strSQL = "SELECT a.EstadoID, c.RecursoFantasia AS Estado, a.DocumentoID, a.DocumentoAbreviado, (CONVERT(VARCHAR(6), a.DocumentoID) + SPACE(1) + " & _
			"ISNULL(a.DocumentoAbreviado + SPACE(1), SPACE(0)) + a.Documento) AS Documento, a.Documento AS sDocumento, dbo.fn_Numero_Formata(a.Versao, 1, 1, 101) AS Versao, " & _ 
			"CONVERT(VARCHAR, a.dtEmissao, " & CStr(nFormatoData) & ") AS dtEmissao, NULL AS dtDesativacao, " 
			
	If ( (CInt(nEstadoFN1) = 91) OR (CInt(nEstadoFN1) = 92) OR (CInt(nEstadoFN1) = 93) ) Then
		strSQL = strSQL & "a.Estudo AS Texto, "
		sTitulo = "Documento em Estudo"
		sState = "Estudo"
		sColor = "#F0E68C"
		sBackground = "#F9F5D0"
		nOrdemID = 1
	Else
		strSQL = strSQL & "a.Texto AS Texto, "
		sTitulo = "Documento Ativo"
		sState = ""		
		sColor = "#90EE90"
		sBackground = "#FFFFFF"
		nOrdemID = 2
	End If
	
	strSQL = strSQL & "b.ItemMasculino AS TitleWindow, a.TipoDocumentoID, d.Fantasia AS Proprietario, " & _
	        "(CASE WHEN a.Treinamento = 1 THEN 'Sim' ELSE 'N�o' END) AS Treinamento " & _
			"FROM Documentos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK), Recursos c WITH(NOLOCK), Pessoas d WITH(NOLOCK) " & _
			"WHERE a.DocumentoID = " & CStr(nDocumentoID) & " AND a.TipoDocumentoID=b.ItemID AND " & _
			"c.RecursoID=dbo.fn_Documento_Estado(a.DocumentoID, 1) AND " & _
			"a.ProprietarioID = d.PessoaID "

	sFiltro = " AND dbo.fn_Documento_Estado(a.DocumentoID, 1) NOT IN (1, 5) "
End If

strSQL = strSQL & " UNION ALL SELECT a.EstadoID, SPACE(0) AS Estado, a.DocumentoID, a.DocumentoAbreviado, (CONVERT(VARCHAR(6), a.DocumentoID) + SPACE(1) + " & _
		"ISNULL(a.DocumentoAbreviado + SPACE(1), SPACE(0)) + a.Documento) AS Documento, a.Documento AS sDocumento, NULL  AS Versao, NULL, NULL, NULL, NULL, NULL, NULL, NULL " & _
	"FROM Documentos a WITH(NOLOCK) WHERE (a.DocumentoID <> " & CStr(nDocumentoID) & sFiltro & ") " & _
	"ORDER BY a.DocumentoID "

rsData.Open strSQL, strConn, adOpenStatic, adLockReadOnly, adCmdText
            
Response.Write vbcrlf

Response.Write "var glb_nOrdemID = " & nOrdemID & ";"
Response.Write vbcrlf

rsData.Filter = "DocumentoID = " & CStr (nDocumentoID)

If (Not rsData.EOF) Then
	
	rsData.MoveLast
	rsData.MoveFirst

	If ( Not IsNull(rsData.Fields("Documento").Value)) Then
		Response.Write "glb_Documento = " & Chr(34) & rsData.Fields("Documento").Value & Chr(34) & ";"
	End If

	If ( Not IsNull(rsData.Fields("TipoDocumentoID").Value)) Then
		nTipoDocumentoID = rsData.Fields("TipoDocumentoID").Value
	End If

	If ( Not IsNull(rsData.Fields("Versao").Value)) Then
		Response.Write "glb_Versao = " & Chr(34) & rsData.Fields("Versao").Value & Chr(34) & ";"
		
		If (nTipoDocToLoad > 0 OR IsNull(nTipoDocToLoad)) Then
		    sVersaoDocumento = rsData.Fields("Versao").Value
        End If		    
	End If

	If ( Not IsNull(rsData.Fields("dtEmissao").Value)) Then
		Response.Write "glb_dtEmissao = " & Chr(34) & rsData.Fields("dtEmissao").Value & Chr(34) & ";"
	End If

	If ( Not IsNull(rsData.Fields("TitleWindow").Value)) Then
		If (sState <> "") Then
			Response.Write "glb_sTitleWindow = " & Chr(34) & rsData.Fields("TitleWindow").Value & " (" & sState & ")" & Chr(34) & ";"
		Else
			Response.Write "glb_sTitleWindow = " & Chr(34) & rsData.Fields("TitleWindow").Value & Chr(34) & ";"
		End If
	End If
	
	If ( Not IsNull(rsData.Fields("dtDesativacao").Value) ) Then
	    sTitulo = sTitulo & " " & rsData.Fields("dtDesativacao").Value
    End If

	sTexto = "<FONT STYLE='BACKGROUND-COLOR:" & sColor & "'> " & _
				"<TABLE BORDER=2 CELLSPACING=0 CLASS='lblTable'> " & _
				    "<TR><TH COLSPAN=8>" & sTitulo & "</TH></TR>" & _
					"{<TR>^I(" & CStr(IndiceID) & ")} " & _
						"<TH>Tipo</TH> " & _
						"<TH>ID</TH> "

	sTexto = sTexto & "<TH>Documento</TH> "

    If ( Not IsNull(rsData.Fields("DocumentoAbreviado").Value) ) Then
	    sTexto = sTexto & "<TH>Abrev</TH> "
	End If
	
	sTexto = sTexto & "<TH>Vers�o</TH> " & _
					  "<TH>Emiss�o</TH> " & _
                      "<TH>Aprova��o</TH> " & _
                      "<TH>Treinamento</TH> " & _
                      "</TR> "
	
	sTexto = sTexto & "<TR> " & _
						"<TH>" & rsData.Fields("TitleWindow").Value & "</TH> " & _
						"<TH>" & rsData.Fields("DocumentoID").Value & "</TH> "

	sTexto = sTexto & "<TH>" & rsData.Fields("sDocumento").Value & "</TH> "

	If ( Not IsNull(rsData.Fields("DocumentoAbreviado").Value) ) Then
		sTexto = sTexto & "<TH>" & rsData.Fields("DocumentoAbreviado").Value & "</TH> "
	End If

    sTexto = sTexto & "<TH>" & rsData.Fields("Versao").Value & "</TH> "
					  
	If (Not isNull(rsData.Fields("dtEmissao").Value)) Then
		sTexto = sTexto & "<TH>" & rsData.Fields("dtEmissao").Value & "</TH> "
		sTexto = sTexto & "<TH>" & rsData.Fields("Proprietario").Value & "</TH> "
		sTexto = sTexto & "<TH>" & rsData.Fields("Treinamento").Value & "</TH> "
	Else
		sTexto = sTexto & "<TH>&nbsp</TH> "
		sTexto = sTexto & "<TH>&nbsp</TH> "
		sTexto = sTexto & "<TH>&nbsp</TH> "
	End If

	sTexto = sTexto & "</TR> " & _
			"</TABLE> " & _
		"</FONT> " & _
		"<BR><BR>"
		
    IndiceID = IndiceID + 1		
		
	'0 Indice - Inicio
	If (CLng(nTipoDocumentoID) <= 803) Then
		Set rsData6 = Server.CreateObject("ADODB.Recordset")
	
		Set rsSPCommand = Server.CreateObject("ADODB.Command")

		With rsSPCommand
			.CommandTimeout = 60 * 10
		    .ActiveConnection = strConn
		    .CommandText = "sp_Documentos_Dados"
		    .CommandType = adCmdStoredProc
		    .Parameters.Append( .CreateParameter("@DocumentoID", adInteger, adParamInput, 8 , nDocumentoID) )
			' PARA ACOMPANHAR ALTERACAO DO PARAMETRO "nTipoDocToLoad", FEITA NA INTERFACE EM 11/09.		    
		    '.Parameters.Append( .CreateParameter("@Versao", adInteger, adParamInput, 8 , nTipoDocToLoad) )
		    If ( IsNull(nTipoDocToLoad) ) Then 
				.Parameters.Append( .CreateParameter("@Versao", adInteger, adParamInput, 8 , null) )
			Else	
				.Parameters.Append( .CreateParameter("@Versao", adInteger, adParamInput, 8 , nTipoDocToLoad) )
		    End If
		    .Parameters.Append( .CreateParameter("@TipoResultado", adInteger, adParamInput, 8 , 0) )
		    Set rsData6 = .Execute
		End With

        'IndiceID = IndiceID + 1
		sTexto = sTexto & "{<B>" & " SUM�RIO</B>^I(" & CStr(IndiceID) & ")}<BR><BR>"
		If (rsData6.Fields.Count <> 0) Then		
			If (Not rsData6.EOF) Then
					
				While (Not rsData6.EOF)

					sTexto = sTexto & "{" & rsData6.Fields("Indice").Value & "^A(" & _
						rsData6.Fields("Ancora").Value & ")}<BR>"
					rsData6.MoveNext

				Wend
					
			Else
				sTexto = sTexto & "N�o aplic�vel.<BR>"
			End If
		Else
			sTexto = sTexto & "N�o aplic�vel.<BR>"
		End If
	
		sTexto = sTexto & "<BR><BR>"

		rsData6.Close
	
		Set	rsData6 = Nothing

	End If
	
    '0 Indice - Fim

	'1 Fluxo dos Processos - Inicio
	'Atualmemnte implantado so para o form Documentos da qualidade
    sImageWebFullPath = imageSourceString (7110, 26000, CLng(nDocumentoID), null, null, sVersaoDocumento)

    IndiceID = IndiceID + 1
    
    If (nDocumentoID > 100) Then	
        If ( CLng(nTipoDocumentoID) <= 803 ) Then
		    sTexto = sTexto & "{<B>" & CStr(IndiceID) & " FLUXO DOS PROCESSOS</B>^I(" & CStr(IndiceID) & ")}<BR><BR>"
	    Else
		    sTexto = sTexto & "{<B>" & CStr(IndiceID) & " EXEMPLO</B>^I(" & CStr(IndiceID) & ")}<BR><BR>"
	    End If

	    bFlag = False
	
	    If (sImageWebFullPath <> "") Then
	        sTexto = sTexto & "<IMG ID='IMG_FLUXPROC' NAME='IMG_FLUXPROC' " & _
                     "STYLE=" & Chr(34) & "CURSOR:'default';BORDER-STYLE:solid;BORDER-WIDTH:1px" & Chr(34) & " " & _
                     "SRC='" & sImageWebFullPath & "' ALIGN='center' " & _
				     "USEMAP='#Map1'></IMG>"
		    bFlag = True				 
	    Else
		    sTexto = sTexto & "N�o aplic�vel."
	    End If    
    Else
        bFlag = False
    End If

	If (bFlag) Then
		'Obtem lista de coordenadas e outros parametros da imagem
		Set rsDataIPars = Server.CreateObject("ADODB.Recordset")

        Dim nVersao

        nVersao = ""
    
        If ( IsNull(nTipoDocToLoad) ) Then
            nVersao = "(SELECT Versao FROM Documentos WITH(NOLOCK) WHERE (DocumentoID = " & CStr(nDocumentoID) & ")) "
        Else
            If (nTipoDocToLoad < 0) Then
                nVersao = "NULL "
            Else
                nVersao = CStr(nTipoDocToLoad)
            End If
        End If

        ' Se Documento em Estudo, Adequa��o, Aprova��o - Busca vers�o de estudo
        ' Se busca do mapeamento � para uma vers�o anterior � atual, busca mapeamento da vers�o em quest�o
        '   Sen�o a primeira vers�o de grande significancia (Numeros inteiros: 1.0, 2.0, 3.0)
        strSQLIPars =   "DECLARE @Versao NUMERIC(5,2) " & _
                        "SET @Versao = " & CStr(nVersao) & " " & _
                        "SELECT *  " & _
                            "FROM Documentos_Mapeamentos a WITH(NOLOCK) " & _
                            "WHERE a.DocumentoID = " & CStr(nDocumentoID) & " " & _
                                "AND (((((@Versao IS NULL) AND (a.Versao IS NULL))) OR (Versao = @Versao)) " & _
                                    "OR (a.Versao = (SELECT CAST(MAX(Versao) AS INT) " & _
                                                        "FROM Documentos_Mapeamentos WITH(NOLOCK) " & _
                                                        "WHERE ((DocumentoID = " & CStr(nDocumentoID) & ") AND (Versao <= @Versao))))) " & _
                            "ORDER BY a.X1, a.Y1 "

'        strSQLIPars = "SELECT * FROM Documentos_Mapeamentos a WITH(NOLOCK) WHERE (a.DocumentoID = " & CStr(nDocumentoID) & ") " & _
'					  "ORDER BY a.X1, a.Y1 "

		rsDataIPars.Open strSQLIPars, strConn, adOpenStatic, adLockReadOnly, adCmdText
	
		If (Not (rsDataIPars.BOF AND rsDataIPars.EOF)) Then
		
			sTexto = sTexto & "<MAP ID='Map1' NAME='Map1'>"
			
			While (Not rsDataIPars.EOF)
				
				If ( Not ((IsNull(rsDataIPars.Fields("X1").Value)) OR _
					     (IsNull(rsDataIPars.Fields("Y1").Value)) OR _
					     (IsNull(rsDataIPars.Fields("X2").Value)) OR _
					     (IsNull(rsDataIPars.Fields("Y2").Value))) ) Then
					     
					'DocMapeamentoID
					nAreaDocMapID = CLng(rsDataIPars.Fields("DocMapeamentoID").Value)

					sTexto = sTexto & "<AREA ID='_AREA_" & CStr(nAreaDocMapID) & "' " & _
						"NAME='_AREA_" & CStr(nAreaDocMapID) & "' " & _
						"STYLE='CURSOR:hand' SHAPE='rect' "

					sTexto = sTexto & "COORDS='" & rsDataIPars.Fields("X1").Value & "," & _
						rsDataIPars.Fields("Y1").Value & "," & _
						rsDataIPars.Fields("X2").Value & "," & _
						rsDataIPars.Fields("Y2").Value & "'"

					If ( Not IsNull(rsDataIPars.Fields("Hint").Value) ) Then
						sTexto = sTexto & "TITLE = '" & rsDataIPars.Fields("Hint").Value & "' "
					End If

					sTexto = sTexto & "Language='Javascript' "

					'parametros da funcao onclick_AreaMap
					'ref a area, imgAssociada, docDestino, ancoraDestino

					If ( IsNull(rsDataIPars.Fields("DocumentoAncoraID").Value) ) Then
						docDestino = "null"
					Else
						docDestino = rsDataIPars.Fields("DocumentoAncoraID").Value
					End If
					
					If ( IsNull(rsDataIPars.Fields("Ancora").Value) ) Then
						ancoraDestino = "null"
					Else
						ancoraDestino = rsDataIPars.Fields("Ancora").Value
					End If
										
					 sTexto = sTexto & "onclick = 'return onclick_AreaMap(" & _
					                               "_AREA_" & CStr(nAreaDocMapID) & ", " & _
												   "IMG_FLUXPROC" & ", " & _
												   CStr(docDestino) & ", " & _
												   CStr(ancoraDestino) & ", " & _
												   "1" & _
					                               ")' "
					
					sTexto = sTexto & "onmouseover = 'return onmouseover_AreaImg(this, IMG_FLUXPROC)' " & _
							 "onmouseout = 'return onmouseout_AreaImg(this, IMG_FLUXPROC)' "
							 
					sTexto = sTexto & ">"
				
					rsDataIPars.MoveNext()
			
				End If
				
			Wend
			
			sTexto = sTexto & "</MAP>"
				 
		End If
						 
		rsDataIPars.Close
		Set rsDataIPars = Nothing
	
	End If
	'1 Fluxo dos Processos - Fim

    'Texto - Inicio
    sTexto = sTexto & "<BR><BR><BR>"
    
	sTexto = sTexto & Trim(rsData.Fields("Texto").Value)

	Set rsData4 = Server.CreateObject("ADODB.Recordset")
		    
	Set rsSPCommand = Server.CreateObject("ADODB.Command")

	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_Documentos_Dados"
	    .CommandType = adCmdStoredProc
	    .Parameters.Append( .CreateParameter("@DocumentoID", adInteger, adParamInput, 8 , nDocumentoID) )
	    .Parameters.Append( .CreateParameter("@Versao", adInteger, adParamInput, 8 , nTipoDocToLoad) )
	    .Parameters.Append( .CreateParameter("@TipoResultado", adInteger, adParamInput, 8 , 1) )
	    Set rsData4 = .Execute
	End With
	
    'Texto - Fim
    
    '5 Responsabilidades - Inicio
    Set rsData2= Server.CreateObject("ADODB.Recordset")
	    
	strSQL2 = "SELECT c.RecursoFantasia AS Perfil, " & _
			"CASE a.Aprovacao WHEN 1 THEN 'X' ELSE '-' END AS Aprovacao, " & _
			"CASE a.Estudo WHEN 1 THEN 'X' ELSE '-' END AS Estudo, " & _
			"CASE a.Execucao WHEN 1 THEN 'X' ELSE '-' END AS Execucao " & _
			"FROM Documentos_Perfis a WITH(NOLOCK), Documentos b WITH(NOLOCK), Recursos c WITH(NOLOCK) " & _
			"WHERE (a.DocumentoID = " & CStr(nDocumentoID) & " AND a.DocumentoID = b.DocumentoID AND a.PerfilID = c.RecursoID) " & _
			"ORDER BY a.Aprovacao DESC, a.Estudo DESC, a.PerfilID"

	rsData2.Open strSQL2, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
	
	'sTexto = sTexto & "<BR>{<B>5 RESPONSABILIDADES</B>^I(5)}<BR><BR>"
	
	If (rsData2.Fields.Count <> 0) Then		
		If (Not rsData2.EOF) Then
		    sTexto = sTexto & "<BR><BR>"
			sTexto = sTexto & "<TABLE BORDER=1 CELLSPACING=0 CLASS='lblTable'>" & _
					"<COLGROUP>" & _
					"<COL ALIGN=CENTER>" & _ 
					"<COL ALIGN=CENTER>" & _
					"<COL ALIGN=CENTER>" & _
					"<COL ALIGN=CENTER>" & _
					"<COL ALIGN=CENTER>" & _
					"<TR>" & _
						"<TH>Respons�vel</TH>" & _
						"<TH>{Aprova��o^H(Respons�vel pela aprova��o do documento?)}</TH>" & _
						"<TH>{Estudo^H(Participa do grupo de estudo?)}</TH>" & _
						"<TH>{Execu��o^H(Respons�vel pela execu��o/cumprimento do documento?)}</TH>" & _
					"</TR>"
				
			While (Not rsData2.EOF)

				sTexto = sTexto & "<TR>"
				sTexto = sTexto & "<TD ALIGN=LEFT>" & rsData2.Fields("Perfil").Value & "</TD>"
				sTexto = sTexto & "<TD>" & rsData2.Fields("Aprovacao").Value & "</TD>"
				sTexto = sTexto & "<TD>" & rsData2.Fields("Estudo").Value & "</TD>"
				sTexto = sTexto & "<TD>" & rsData2.Fields("Execucao").Value & "</TD>"
				sTexto = sTexto & "</TR>"

				rsData2.MoveNext
			Wend
				
			sTexto = sTexto & "</TABLE>"
		Else
			'sTexto = sTexto & "N�o aplic�vel.<BR>"
		End If
	Else
		'sTexto = sTexto & "N�o aplic�vel.<BR>"
	End If
	
	
    sTexto = sTexto & "<BR>"
    
	rsData2.Close
	Set rsData2 = Nothing
	
	'5 Responsabilidades - Fim
	
    '6 Defini��es - Inicio
	Set rsData3 = Server.CreateObject("ADODB.Recordset")
		    
	Set rsSPCommand = Server.CreateObject("ADODB.Command")

	With rsSPCommand
		.CommandTimeout = 60 * 10
	    .ActiveConnection = strConn
	    .CommandText = "sp_Documentos_Dados"
	    .CommandType = adCmdStoredProc
	    .Parameters.Append( .CreateParameter("@DocumentoID", adInteger, adParamInput, 8 , nDocumentoID) )
	    .Parameters.Append( .CreateParameter("@Versao", adInteger, adParamInput, 8 , nTipoDocToLoad) )
	    .Parameters.Append( .CreateParameter("@TipoResultado", adInteger, adParamInput, 8 , 2) )
	    Set rsData3 = .Execute
	End With
	
	'Monta Array com os documentos
	nCounter = 0
	
	rsData.Filter = ""
	rsData.MoveLast
	rsData.MoveFirst

	While (NOT rsData.EOF)
		Response.Write "glb_aDocumentos[" & CLng(nCounter) & "] = new Array();"
		Response.Write vbcrlf
		Response.Write "glb_aDocumentos[" & CLng(nCounter) & "][0] = " & rsData.Fields("DocumentoID").Value & ";"
		Response.Write vbcrlf
		Response.Write "glb_aDocumentos[" & CLng(nCounter) & "][1] = " & Chr(34) & rsData.Fields("Documento").Value & Chr(34) & ";"
		Response.Write vbcrlf
		Response.Write "glb_aDocumentos[" & CLng(nCounter) & "][2] = " & Chr(34) & rsData.Fields("EstadoID").Value & Chr(34) & ";"
		rsData.MoveNext
		nCounter = nCounter + 1
	Wend

    If (nDocumentoID > 100) Then
        IndiceID = 6

	    sTexto = sTexto & "{<BR><B>" & CStr(IndiceID) & " DEFINI��ES</B>^I(" & CStr(IndiceID) & ")}<BR><BR>"
	    If (rsData4.Fields.Count <> 0) Then		
		    If (Not rsData4.EOF) Then
			    sTexto = sTexto & "<TABLE BORDER=1 CELLSPACING=0 CLASS='lblTable'>" & _
					    "<COLGROUP>" & _
					    "<COL ALIGN=CENTER>" & _ 
					    "<COL ALIGN=CENTER>" & _
					    "<TR>" & _
						    "<TH>Termo</TH>" & _
						    "<TH>Significado</TH>" & _
					    "</TR>"
				
			    While (Not rsData4.EOF)

				    sTexto = sTexto & "<TR>"
				    sTexto = sTexto & "<TD ALIGN=LEFT>" & rsData4.Fields("Termo").Value & "</TD>"
				
				    If ( NOT IsNull(rsData4.Fields("Significado").Value) ) Then
					    sTexto = sTexto & "<TD ALIGN=LEFT>" & rsData4.Fields("Significado").Value & "</TD>"
				    Else
					    sTexto = sTexto & "<TD ALIGN=LEFT>&nbsp</TD>"
				    End If
				
				    sTexto = sTexto & "</TR>"
				    rsData4.MoveNext
			    Wend
				
			    sTexto = sTexto & "</TABLE>"
		    Else
			    sTexto = sTexto & "N�o aplic�vel.<BR>"			
		    End If
	    Else
		    sTexto = sTexto & "N�o aplic�vel.<BR>"
	    End If
	
	    sTexto = sTexto & "<BR>"

        rsData4.Close
        Set rsData4 = Nothing
    
	    '6 Defini��es - Fim
	
	    '7 Refer�ncias - Inicio
	    IndiceID = 7
	
	    sTexto = sTexto & "<BR>{<B>" & CStr(IndiceID) & " REFER�NCIAS</B>^I(" & CStr(IndiceID) & ")}<BR><BR>"
	    If (rsData3.Fields.Count <> 0) Then		
		    If (Not rsData3.EOF) Then
			    sTexto = sTexto & "<TABLE BORDER=1 CELLSPACING=0 CLASS='lblTable'>" & _
					    "<TR>" & _
						    "<TH NOWRAP>Tipo</TH>" & _
						    "<TH NOWRAP>Tipo de Documento</TH>" & _
						    "<TH NOWRAP>ID</TH>" & _
						    "<TH NOWRAP>Abrev</TH>" & _
						    "<TH NOWRAP>Documento</TH>" & _
						    "<TH NOWRAP>Item</TH>" & _
					    "</TR>"
			
			    nCounter = 0
			
			    While (Not rsData3.EOF)
				    sTexto = sTexto & "<TR>"
				    sTexto = sTexto & "<TD>" & rsData3.Fields("TipoReferencia").Value & "</TD>"
				    sTexto = sTexto & "<TD>" & rsData3.Fields("TipoDocumento").Value & "</TD>"

				    If (Not isNull(rsData3.Fields("DocumentoID").Value)) Then
				
					    sTexto = sTexto & "<TD>" & "{" & CStr(rsData3.Fields("DocumentoID").Value) & _
						    "^G(" & CStr(rsData3.Fields("DocumentoID").Value) & ")}</TD>"
						
					    Response.Write "glb_aReferencias[" & CLng(nCounter) & "] = new Array();"
					    Response.Write vbcrlf
					    Response.Write "glb_aReferencias[" & CLng(nCounter) & "][0] = " & rsData3.Fields("DocumentoID").Value & ";"
					    Response.Write vbcrlf
					    Response.Write "glb_aReferencias[" & CLng(nCounter) & "][1] = " & Chr(34) & CStr(rsData3.Fields("Documento").Value) & Chr(34) & ";"
					    Response.Write vbcrlf
					
					    nCounter = nCounter + 1

				    Else
					    sTexto = sTexto & "<TD>-</TD>"
				    End If

				    If (Not isNull(rsData3.Fields("DocumentoAbreviado").Value)) Then
					    sTexto = sTexto & "<TD>" & rsData3.Fields("DocumentoAbreviado").Value & "</TD>"
				    Else
					    sTexto = sTexto & "<TD>&nbsp</TD>"
				    End If
					
				    sTexto = sTexto & "<TD>" & CStr(rsData3.Fields("Documento").Value) & "</TD>"
				    sTexto = sTexto & "<TD>" & CStr(rsData3.Fields("Norma").Value) & "</TD>"
				    sTexto = sTexto & "</TR>"
				    rsData3.MoveNext
			    Wend
				
			    sTexto = sTexto & "</TABLE>"
		    Else
			    sTexto = sTexto & "N�o aplic�vel.<BR>"
		    End If
	    Else
		    sTexto = sTexto & "N�o aplic�vel.<BR>"
	    End If
	
	    sTexto = sTexto & "<BR>"

        '7 Refer�ncias - Fim

        '8 Documentos - Inicio
        Set rsData5= Server.CreateObject("ADODB.Recordset")

        Dim srvname

        srvName = Request.ServerVariables("SERVER_NAME")

        strSQL3 = "SELECT a.DocumentoID, a.Nome, CONVERT(VARCHAR(15), dbo.fn_Numero_Formata(a.Tamanho,0,1,103)) AS Tamanho, CONVERT(VARCHAR, a.dtDocumento, " & CStr(nFormatoData) & ") AS dtDocumento " & _
	        "FROM [OVERFLYBASE_EXT2].Overfly_Documentos.dbo.Documentos a WITH(NOLOCK) " & _
	        "WHERE FormID=7110 AND SubformID=26000 AND RegistroID = " & CStr(nDocumentoID) & " AND TipoArquivoID = 1451 " & _
	        "ORDER BY a.Nome"

        rsData5.Open strSQL3, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        IndiceID = 8

        sTexto = sTexto & "<BR>{<B>" & CStr(IndiceID) & " DOCUMENTOS</B>^I(" & CStr(IndiceID) & ")}<BR><BR>"
    
        If (rsData5.Fields.Count <> 0) Then
            If (Not rsData5.EOF) Then
	        
	            sTexto = sTexto & "<TABLE BORDER=1 CELLSPACING=0 CLASS='lblTable'>" & _
			            "<TR>" & _
				            "<TH NOWRAP>Nome</TH>" & _
				            "<TH NOWRAP>Tamanho (KB)</TH>" & _
				            "<TH NOWRAP>Data</TH>" & _
			            "</TR>"
			
	            While (Not rsData5.EOF)

		            sTexto = sTexto & "<TR>"
		            sTexto = sTexto & "<TD ALIGN=LEFT><A HREF='javascript:openDocument(" & CStr(rsData5.Fields("DocumentoID").Value) & ");'>" & CStr(rsData5.Fields("Nome").Value) & "</TD>"
		            sTexto = sTexto & "<TD ALIGN=RIGHT>" & rsData5.Fields("Tamanho").Value & "</TD>"
		            sTexto = sTexto & "<TD ALIGN=LEFT>" & rsData5.Fields("dtDocumento").Value & "</TD>"
		            sTexto = sTexto & "</TR>"
		
		            rsData5.MoveNext
		
	            Wend
			
	            sTexto = sTexto & "</TABLE>"
            Else
		        sTexto = sTexto & "N�o aplic�vel.<BR>"
            End If
        Else
	        sTexto = sTexto & "N�o aplic�vel.<BR>"
	    End If

        sTexto = sTexto & "<BR>"

        rsData5.Close
        Set rsData5 = Nothing

        '8 Documentos - Fim

        '9 Estados - Inicio

        nEstadoID = 0
        sTransicao = ""

        Set rsData6= Server.CreateObject("ADODB.Recordset")
	
        strSQL3 = "SELECT g.RecursoID AS EstadoID, g.RecursoFantasia AS Estado, g.RecursoAbreviado AS Abrev, " & _ 
		        "ISNULL(i.RecursoAbreviado, CHAR(45)) AS Transicao, h.EhDefault " & _ 
	        "FROM Documentos a WITH(NOLOCK), Recursos b WITH(NOLOCK), RelacoesRecursos c WITH(NOLOCK), Recursos d WITH(NOLOCK), " & _ 
		        "Recursos e WITH(NOLOCK), RelacoesRecursos f WITH(NOLOCK) LEFT OUTER JOIN RelacoesRecursos_Estados h WITH(NOLOCK) ON (f.RelacaoID = h.RelacaoID) " & _
									           "LEFT OUTER JOIN Recursos i WITH(NOLOCK) ON (h.RecursoID = i.RecursoID), Recursos g WITH(NOLOCK) " & _ 
	        "WHERE (a.DocumentoID = " & CStr(nDocumentoID) & " AND a.ContextoID = b.RecursoID AND b.EstadoID = 2 AND " & _ 
		        "b.RecursoID = c.ObjetoID AND c.TipoRelacaoID = 1 AND c.SujeitoID = d.RecursoID AND d.Principal = 1 AND " & _ 
		        "c.MaquinaEstadoID = e.RecursoID AND e.EstadoID = 2 AND f.EstadoID = 2 AND e.RecursoID = f.ObjetoID AND " & _ 
		        "f.TipoRelacaoID = 3 AND f.SujeitoID = g.RecursoID) " & _ 
	        "ORDER BY f.Ordem, h.RelRecEstadoID "

        rsData6.Open strSQL3, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        If (CLng(nTipoDocumentoID) = 805) Then
	        IndiceID = 9
	
	        sTexto = sTexto & "<BR>{<B>" & CStr(IndiceID) & " ESTADOS</B>^I(" & CStr(IndiceID) & ")}<BR><BR>"
			
	        If (rsData6.EOF) Then
		        sTexto = sTexto & "N�o aplic�vel.<BR>"
	        Else
		        sTexto = sTexto & "<TABLE BORDER=1 CELLSPACING=0 CLASS='lblTable'>" & _
				        "<COLGROUP>" & _
				        "<COL ALIGN=CENTER>" & _ 
				        "<COL ALIGN=CENTER>" & _
				        "<COL ALIGN=CENTER>" & _
				        "<COL ALIGN=CENTER>" & _
				        "<TR>" & _
					        "<TH>ID</TH>" & _
					        "<TH>Estado</TH>" & _
					        "<TH>Abrev</TH>" & _
					        "<TH>Transi��o</TH>" & _
				        "</TR>"
				
		        While (Not rsData6.EOF)

			        sTexto = sTexto & "<TR>"
			        sTexto = sTexto & "<TD ALIGN=RIGHT>" & CStr(rsData6.Fields("EstadoID").Value) & "</TD>"
			        sTexto = sTexto & "<TD ALIGN=LEFT>" & rsData6.Fields("Estado").Value & "</TD>"
			        sTexto = sTexto & "<TD ALIGN=LEFT>" & rsData6.Fields("Abrev").Value & "</TD>"

			        nEstadoID = CLng(rsData6.Fields("EstadoID").Value)

			        sTransicao = ""

			        Do While (NOT rsData6.EOF)
		
				        If (nEstadoID <> CLng(rsData6.Fields("EstadoID").Value)) Then
					        Exit Do
				        End If

				        If (rsData6.Fields("EhDefault").Value) Then
					        sTransicao = sTransicao & "<B>" & rsData6.Fields("Transicao").Value & "</B>"
				        Else
					        sTransicao = sTransicao & rsData6.Fields("Transicao").Value
				        End If

				        rsData6.MoveNext

				        If (NOT rsData6.EOF) Then
					        If (nEstadoID = CLng(rsData6.Fields("EstadoID").Value)) Then
						        sTransicao = sTransicao & ", "
					        End If
				        End If
			        Loop
				
			        sTexto = sTexto & "<TD ALIGN=LEFT>" & sTransicao & "</TD>"
			        sTexto = sTexto & "</TR>"
		        Wend
				
		        sTexto = sTexto & "</TABLE>"

	        End If

	        sTexto = sTexto & "<BR>"
        End If

        rsData6.Close
        Set rsData6 = Nothing

        '9 Estados - Fim
    End If

	sTexto = sTexto & "<TABLE BORDER=2 CELLSPACING=0 CLASS='lblTable'>"
	sTexto = sTexto & "<TR>"
    sTexto = sTexto & "<TD>"
    sTexto = sTexto & "Documento emitido em " & dtEmissaoDocumento & " por " & sUsuarioLogado
    sTexto = sTexto & "</TD>"
	sTexto = sTexto & "</TR>"
	sTexto = sTexto & "</TABLE>"
	sTexto = sTexto & "<BR>"

    If ( Not IsNull(sTexto)) Then
        sTexto = ReplaceText( sTexto, Chr(13) & Chr(10), "")
        sTexto = ReplaceText( sTexto, Chr(34), "")
    End If
			
    If ( Not IsNull(sTexto)) Then
        sTexto = ReplaceText(sTexto, "<IMG", "<IMG language='JavaScript' " & _
				 "onmouseout='return onmouseout_Img(this)' " & _
				 "onmouseover='return onmouseover_Img(this)' " & _
				 "onmousemove='return onmousemove_Img(this)' " & _
				 "onclick='return onclick_Img(this)' " & _
				 "onload='return img_onload()' " & _
				 "onerror='return img_onload()' ")
    End If

    Response.Write "glb_ImgsNumber += " & (MatchText(sTexto, "img_onload") / 2) & ";" & vbCrLf
    Response.Write "glb_sTexto = " & Chr(34) & sTexto & Chr(34) & ";" & vbCrLf
    
    If (nDocumentoID > 100) Then
        rsData3.Close
        Set rsData3 = Nothing
    End If
    
    Set rsSPCommand = Nothing
    
End If

rsData.Close
Set rsData = Nothing
    
Response.Write "</script>"
Response.Write vbcrlf
%>
<script ID="wndJSProc" LANGUAGE="javascript">
<!--
    //-->
</script>

</head>
<!--style="background-color:#ffffff"-->
<body id="modaldocumentotemplateBody" name="modaldocumentotemplateBody" LANGUAGE="javascript" onload="return window_onload()"

<% Response.Write "style=background-color:" & sBackground & ">" %>

	<font face="Tahoma, Verdana, Helvetica, sans-serif">
	<div id="divDocumento" name="divDocumento" class="fldGeneral"></div>
	</font>
	<div id="divAreaSensi" name="divAreaSensi" class="fldAreaSensi"  LANGUAGE="javascript" onmouseout="return divAreaSensi_onmouseout(this)" onmouseclick="returndivAreaSensi_onmouseclick(this)"></div>
</body>

</html>
