<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalfornecedoresHtml" name="modalfornecedoresHtml">

<head>

    <title></title>

    <%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modalfornecedores.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
   
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/modalfornecedores.js" & Chr(34) & "></script>" & vbCrLf
    %>

    <%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nModalType

sCaller = ""
nModalType = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nModalType").Count
    nModalType = Request.QueryString("nModalType")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nModalType = " & CStr(nModalType) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
    %>

    <script id="wndJSProc" language="javascript">
<!--

    //-->
    </script>

    <!-- //@@ Eventos de grid -->
    <!-- fg -->
    <script language="javascript" for="fg" event="ValidateEdit">
<!--
    js_modalfornecedores_ValidateEdit();
    //-->
    </script>

    <script language="javascript" for="fg" event="AfterEdit">
<!--
    js_modalfornecedores_AfterEdit(arguments[0], arguments[1]);
    //-->
    </script>

    <script language="javascript" for="fg" event="KeyPress">
<!--
    js_modalfornecedoresKeyPress(arguments[0]);
    //-->
    </script>

    <script language="javascript" for="fg" event="DblClick">
<!--
    js_fg_modalfornecedoresDblClick(fg, fg.Row, fg.Col);
    //-->
    </script>

    <script language="javascript" for="fg" event="BeforeMouseDown">
<!--
    js_fg_modalfornecedoresBeforeMouseDown(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    //-->
    </script>

    <script language="javascript" for="fg" event="EnterCell">
<!--
    js_fg_EnterCell(fg);
    //-->
    </script>
    <script language="javascript" for="fg" event="AfterRowColChange">
<!--
    fg_modalfornecedoresAfterRowColChange(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
    //-->
    </script>
    <script language="javascript" for="fg" event="BeforeRowColChange">
<!--
    js_fg_BeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
    //-->
    </script>
    <script language="javascript" for="fg" event="AfterSort">
<!--
    fg_AfterSort();
    //-->
    </script>
    <script language="javascript" for="fg" event="BeforeSort">
<!--
    fg_BeforeSort();
    //-->
    </script>
    <script language="javascript" for="fg" event="MouseDown">
<!--
    fg_MouseDown_fornecedores(fg.Row, fg.Col);
    //-->
    </script>

    <script language="javascript" for="fg" event="MouseMove">
<!--
    js_fg_modalfornecedoresMouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    //-->
    </script>

</head>

<body id="modalfornecedoresBody" name="modalfornecedoresBody" language="javascript" onload="return window_onload()" onunload="window_onunload()">

    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>

    <!-- Div title bar -->
    <div id="divControls" name="divControls" class="divGeneral">
        <!-- Campos para botao 3 -->
        <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral" title="Efetuar pesquisa?">Pesq</p>
        <input type="checkbox" id="chkPesquisa" name="chkPesquisa" class="fldGeneral" title="Efetuar pesquisa?">
        <p id="lblServico" name="lblServico" class="lblGeneral">Servi�o</p>
        <select id="selServico" name="selServico" class="fldGeneral">
            <option value="1" selected>Ciclo de vida</option>
            <option value="2">Previs�o de vendas</option>
            <option value="3">Forma��o de pre�os</option>
            <option value="4">Informa��es b�sicas</option>
            <option value="5">Gerenc. de estoque</option>
            <option value="6">Internet</option>
            <option value="7">Inconsist�ncias</option>
        </select>
        <p id="lblFiltroEspecifico" name="lblFiltroEspecifico" class="lblGeneral">Filtro</p>
        <select id="selFiltroEspecifico" name="selFiltroEspecifico" class="fldGeneral"></select>
        <p id="lblFamilia" name="lblFamilia" class="lblGeneral">Familia</p>
        <select id="selFamilia" name="selFamilia" class="fldGeneral"></select>
        <p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
        <select id="selMarca" name="selMarca" class="fldGeneral"></select>
        <p id="lblLinha" name="lblLinha" class="lblGeneral">Linha</p>
        <select id="selLinha" name="selLinha" class="fldGeneral"></select>
        <p id="lblColunas" name="lblColunas" class="lblGeneral" title="Exibir colunas extras no grid?">Col</p>
        <input type="checkbox" id="chkColunas" name="chkColunas" class="fldGeneral" title="Exibir colunas extras no grid?">
        <p id="lblCampos" name="lblCampos" class="lblGeneral" title="Exibir campos extras abaixo do grid?">Camp</p>
        <input type="checkbox" id="chkCampos" name="chkCampos" class="fldGeneral" title="Exibir campos extras abaixo do grid?">
        <p id="lblPercentualPV" name="lblPercentualPV" class="lblGeneral">%PV</p>
        <input type="text" id="txtPercentualPV" name="txtPercentualPV" class="fldGeneral" title="Percentual de ajuste de previs�o de vendas"></input>
        <p id="lblFornecedor" name="lblFornecedor" class="lblGeneral" title="N�mero de revis�o do documento">Fornecedor</p>
        <select id="selFornecedor" name="selFornecedor" class="fldGeneral"></select>
        <p id="lblTransacao" name="lblTransacao" class="lblGeneral">Transa��o</p>
        <select id="selTransacao" name="selTransacao" class="fldGeneral"></select>
        <p id="lblFinanciamento" name="lblFinanciamento" class="lblGeneral">Financiamento</p>
        <select id="selFinanciamento" name="selFinanciamento" class="fldGeneral"></select>
        <!-- Campos para botao 4 -->
        <p id="lblFornecedor2" name="lblFornecedor2" class="lblGeneral">Fornecedor</p>
        <select id="selFornecedor2" name="selFornecedor2" class="fldGeneral"></select>
        <p id="lblImpostos" name="lblImpostos" class="lblGeneral" title="Mostrar impostos?">Imp</p>
        <input type="checkbox" id="chkImpostos" name="chkImpostos" class="fldGeneral" title="Mostrar impostos?">
        <p id="lblRetCustoReposicao" name="lblRetCustoReposicao" class="lblGeneral" title="Custo de reposi��o com impostos n�o inclusos (IPI)?">Custos</p>
        <input type="checkbox" id="chkRetCustoReposicao" name="chkRetCustoReposicao" class="fldGeneral" title="Custo de reposi��o com impostos n�o inclusos (IPI)?">
        <p id="lblModelo" name="lblModelo" class="lblGeneral" title="Mostrar coluna Modelo?">Mod</p>
        <input type="checkbox" id="chkModelo" name="chkModelo" class="fldGeneral" title="Mostrar coluna Modelo?">
        <p id="lblPartNumber" name="lblPartNumber" class="lblGeneral" title="Mostrar coluna Part Number?">PN</p>
        <input type="checkbox" id="chkPartNumber" name="chkPartNumber" class="fldGeneral" title="Mostrar coluna Part Number?">
        <p id="lbldtAtualizacao" name="lbldtAtualizacao" class="lblGeneral" title="Data da �ltima atualiza��o do custo de reposi��o">Atualiza��o</p>
        <select id="seldtAtualizacao" name="seldtAtualizacao" class="fldGeneral"></select>
        <p id="lblDataSemelhante" name="lblDataSemelhante" class="lblGeneral" title="Somente esta data ou exceto ela?">Data</p>
        <input type="checkbox" id="chkDataSemelhante" name="chkDataSemelhante" class="fldGeneral" title="Somente esta data ou exceto ela?">
        <input type="button" id="btnImportar" name="btnImportar" value="Importar" language="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnExportar" name="btnExportar" value="Exportar" language="javascript" onclick="return btn_onclick(this)" class="btns"></input>

        <input type="button" id="btnListar" name="btnListar" value="Listar" language="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnCalcularPV" name="btnCalcularPV" value="Calcular PV" language="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnComprar" name="btnComprar" value="Comprar" language="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnImprimir" name="btnImprimir" value="Imprimir" language="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnExcel" name="btnExcel" value="Excel" language="javascript" onclick="return btn_onclick(this)" class="btns"></input>
    </div>

    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" viewastext>
        </object>
        <!-- <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img> -->
    </div>

    <!-- Div do grid -->
    <div id="divFG2" name="divFG2" class="divGeneral">
        <object classid="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg2" name="fg2" viewastext>
        </object>
        <!-- <img id="Img1" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="Img2" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="Img3" name="hr_B_FGBorder" class="lblGeneral"></img> -->
    </div>

    <!-- Div Controls Gerenciamento de produtos -->
    <div id="divControlsGerProd" name="divControlsGerProd" class="divGeneral">
        <p id="lblCustoMedio" name="lblCustoMedio" class="lblGeneral" title="">Custo M�dio</p>
        <input type="text" id="txtCustoMedio" name="txtCustoMedio" class="fldGeneral" title=""></input>
        <p id="lblCustoReposicao" name="lblCustoReposicao" class="lblGeneral" title="">Custo Reposi��o</p>
        <input type="text" id="txtCustoReposicao" name="txtCustoReposicao" class="fldGeneral" title=""></input>
        <p id="lblUltimaEntrada" name="lblUltimaEntrada" class="lblGeneral" title="">�ltima Entrada</p>
        <input type="text" id="txtUltimaEntrada" name="txtUltimaEntrada" class="fldGeneral" title=""></input>
        <p id="lblUltimaSaida" name="lblUltimaSaida" class="lblGeneral" title="">�ltima Saida</p>
        <input type="text" id="txtUltimaSaida" name="txtUltimaSaida" class="fldGeneral" title=""></input>
        <p id="lblDias" name="lblDias" class="lblGeneral" title="">Dias</p>
        <input type="text" id="txtDias" name="txtDias" class="fldGeneral" title=""></input>
        <p id="lblDisponibilidade" name="lblDisponibilidade" class="lblGeneral" title="">Disponibilidade</p>
        <input type="text" id="txtDisponibilidade" name="txtDisponibilidade" class="fldGeneral" title=""></input>
        <p id="lblEstoqueWeb" name="lblEstoqueWeb" class="lblGeneral" title="">Est Web</p>
        <input type="text" id="txtEstoqueWeb" name="txtEstoqueWeb" class="fldGeneral" title=""></input>
    </div>

    <!-- Div Controls Fornecedores -->
    <div id="divControlsFornec" name="divControlsFornec" class="divGeneral">
    </div>

    <input type="button" id="btnOK" name="btnOK" value="Gravar" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <textarea id="txtAreaTrace" name="txtAreaTrace" class="fldGeneral"></textarea>

    <div id="divUpload" name="divUpload" class="divGeneral">
        <input type="button" id="btnCancelar" name="btnCancelar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <iframe src ="" id="I1" frameborder="0"  name="I1" height="45" width="974" SCROLLING="NO" ></iframe>         
     </div>

    <iframe id="framePrintJet" name="framePrintJet" class="theFrames" frameborder="no"></iframe>

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>

    <iframe id="frmExportarPrecos" name="frmExportarPrecos" style="display:none" frameborder="no"></iframe>
</body>

</html>
