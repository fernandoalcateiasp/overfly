/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
        
    // ajusta o body do html
    with (modalofpaginginfBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // configuracao inicial do html
    setupPage();   
            
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	// coloca foco no campo Argumento
	window.focus();
	txtArgumento.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Caption da janela modal
    secText('Pesquisa', 1);
        
	var tempLeft = ELEM_GAP;
    var elem;
    
    // Pesquisa -----------------------------------------------------
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 24;
        width = 410;
        height = 102;
    }
    
    adjustElementsInForm([['lblPesquisa','selPesquisa',15,1],
                          ['lblOrdem','chkOrdem',3,1],
                          ['lblRegistros','selRegistros',7,1],
                          ['lblArgumento','txtArgumento',20,1],
                          ['lblFiltro','txtFiltro',49,2]],null,null,true);
                          
    // Seleciona conteudo do campo quando em foco
    txtArgumento.onfocus = selTextInControl;
    txtFiltro.onfocus = selTextInControl;                                            
                          
    fillPesqElements();                      
}

/********************************************************************
Seleciona conteudo de um controle quando o mesmo recebe foco
********************************************************************/
function selTextInControl()
{
    this.select();
}

function fillPesqElements()
{
    var i;
    var sOptionStr,nOptionValue;
    var aParams = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL, 
                                'new Array(glb_aCmbKeyPesq,glb_bCheckInvert,' +
                                'glb_sArgument,glb_sFilterPesq,' +
                                'glb_scmbKeyPesqSel,glb_scmbLinePerPageSel)');
    
    // Preenche o combo de chave de pesquisa
    clearComboEx(['selPesquisa']);
    if ( (aParams[0] != null) && ((typeof(aParams[0])).toUpperCase() == 'OBJECT') )
    {
        for (i=0; i<(aParams[0]).length; i++)
        {
            optionStr = aParams[0][i][0];
	        optionValue = aParams[0][i][1];
            var oOption = document.createElement("OPTION");
            oOption.text = optionStr;
            oOption.value = optionValue;
            
            selPesquisa.add(oOption);
            selPesquisa[aParams[0][i][1]] = (aParams[0][i][0].toUpperCase() == "DATA");
        }
    }
    
    // Preenche o campo Invertido
    if (aParams[1] == true)
        chkOrdem.checked = true;
    else    
        chkOrdem.checked = false;
        
    // Preenche o campo Argumento
    if (aParams[2] != null)
        txtArgumento.value = aParams[2];
    else    
        txtArgumento.value = '';

    // Preenche o campo Filtro
    if (aParams[3] != null)
        txtFiltro.value = aParams[3];
    else    
        txtFiltro.value = '';
        
    // Seleciona o primeiro item do combo de chave de pesquisa
    selPesquisa.selectedIndex = 0;
    for ( i = 0; i<selPesquisa.options.length; i++ )
    {
        // e retirado o primeiro caractere do value do combo abaixo 
        // (caracter de controle do tipo de dado do campo chave) para
        // comparacao do valor que vem do inf
        if ( (selPesquisa.options[i].value).substr(1) == aParams[4] )
        {
            selPesquisa.selectedIndex = i;
            break;
        }
    }

    // Seleciona o primeiro item do combo de linhas por pagina
    selRegistros.selectedIndex = 0;
    for ( i = 0; i<selRegistros.options.length; i++ )
    {
        if ( selRegistros.options[i].value == aParams[5] )
        {
            selRegistros.selectedIndex = i;
            break;
        }
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    var controlID = ctl.id;
    var param1 = 'SUP';
    var sString = txtArgumento.value;
    var sType = (selPesquisa.value).substr(0,1);
    
    if ( glb_filePos == 'SUP' )
        param1 = 'S';
    else if ( glb_filePos == 'INF' )
        param1 = 'I';
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        if ( ((document.getElementById('txtArgumento').value != '') &&
              (! window.top.chkTypeData( sType, sString ))) ||
             (selPesquisa.options[selPesquisa.value] && !chkDataEx(txtArgumento.value)) )
        {
            if ( window.top.overflyGen.Alert ('O campo Argumento cont�m tipo de dado incompativel.') == 0 )
                return null;
                
            document.getElementById('txtArgumento').focus();
            return true;
        }
    
        lockControlsInModalWin(true);

        sendJSMessage( getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + param1,
                       new Array( selPesquisa.value, chkOrdem.checked, 
                                  selRegistros.value, txtArgumento.value,
                                  txtFiltro.value) );
    }
    else
        sendJSMessage( getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + param1, null );
}

