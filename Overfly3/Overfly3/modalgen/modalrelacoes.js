/********************************************************************
modalrelacoes.js

Library javascript para o modalrelacoes.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var modPesqrelacoes_Timer = null;

var dsoPesq = new CDatatransport("dsoPesq");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

NOTA: Ver tambem o arquivo commonrelacoes.js

FUNCOES GERAIS:
window_onload()
setupPage()
txtPesquisa_onKeyPress()
txtPesquisa_ondigit(ctl)
btnFindPesquisa_onclick(ctl)
btn_onclick(ctl)
startPesq(strPesquisa)
dataToReturn()

Eventos de grid particulares desta janela:
fg_ModPesqrelacoesDblClick()
fg_ModPesqrelacoesKeyPress(KeyAscii)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalrelacoesBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPesquisa').disabled == false )
        txtPesquisa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    var sTemp = glb_sLabel;
        
    secText('Selecionar ' + sTemp, 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    // ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        height = 40;
    }
    
    // txtPesquisa
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtPesquisa.style)
    {
        left = 0;
        top = 16;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtPesquisa.onkeypress = txtPesquisa_onKeyPress;
    
    // btnFindPesquisa
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPesquisa.style.top, 10);
        left = parseInt(txtPesquisa.style.left, 10) + parseInt(txtPesquisa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    
    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
    
    lblPesquisa.innerText = sTemp;
}

/********************************************************************
Usuario pressionou tecla no campo txtPesquisa
********************************************************************/
function txtPesquisa_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPesqrelacoes_Timer = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }
}

/********************************************************************
Caracter digitado no campo txtPesquisa
********************************************************************/
function txtPesquisa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

/********************************************************************
Usuario clicou o botao btnFindPesquisa
********************************************************************/
function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqrelacoes_Timer != null )
    {
        window.clearInterval(modPesqrelacoes_Timer);
        modPesqrelacoes_Timer = null;
    }
    
    txtPesquisa.value = trimStr(txtPesquisa.value);
    
    changeBtnState(txtPesquisa.value);

    if (btnFindPesquisa.disabled)
    {
        lockControlsInModalWin(false);
        return;
    }    
    
    startPesq("'" + txtPesquisa.value + "%'");
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqrelacoesDblClick()
{
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqrelacoesKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, dataToReturn() );    
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
}

/********************************************************************
Chamada de pagina asp no servidor para obter dados para preencher grid

As pesquisas possiveis:
1. Relacao Entre Pessoas (glb_nFormID == 1220)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Clientelia Asstec
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 22)
    1.3. Clientelia Transporte
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 23)
    1.4. Clientelia Banco
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 24)
    1.5. Clientelia Servicos
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 25)
    1.6. Outras Relacoes
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID != 22) &&
            (glb_nTipoRelacaoID != 23) &&
            (glb_nTipoRelacaoID != 24) &&
            (glb_nTipoRelacaoID != 25)
    1.7. Filiador de Programa de Marketing 
        (glb_sComboID == 'selFiliadorID_07')
2. Relacao Entre Conceitos (glb_nFormID == 2120)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Clientelia Asstec
        (glb_sQuemENaRel == 'OBJ')
3. Relacao Entre Recursos (glb_nFormID == 1120)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Objeto
        (glb_sQuemENaRel == 'OBJ')
4. Relacao Entre Pessoas e Conceitos (glb_nFormID == 2130)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Objeto
        (glb_sQuemENaRel == 'OBJ')
5. Relacao Entre Pessoas e Recursos (glb_nFormID == 1130)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')

********************************************************************/
function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);
    
    var strPars = new String();
    strPars = '?';
    strPars += 'nFormID='  + escape(glb_nFormID);
    strPars += '&sComboID='  + escape(glb_sComboID);
    strPars += '&nEmpresaID='  + escape(glb_nEmpresaID);
    strPars += '&sQuemENaRel=' + escape(glb_sQuemENaRel);
    strPars += '&nTipoRelacaoID=' + glb_nTipoRelacaoID;
    strPars += '&nRegExcluidoID=' + glb_nRegExcluidoID;
    strPars += '&strToFind='+escape(strPesquisa);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/serversidegenEx/pesqrelacoes.aspx' + strPars;
	
	dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

/********************************************************************
Retorno de pagina asp no servidor com dados para preencher grid

Os retornos possiveis:
1. Relacao Entre Pessoas (glb_nFormID == 1220)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Clientelia Asstec
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 22)
    1.3. Clientelia Transporte
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 23)
    1.4. Clientelia Banco
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 24)
    1.5. Clientelia Servicos
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 25)
    1.6. Outras Relacoes
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID != 22) &&
            (glb_nTipoRelacaoID != 23) &&
            (glb_nTipoRelacaoID != 24) &&
            (glb_nTipoRelacaoID != 25)
    1.7. Filiador de Programa de Marketing 
        (glb_sComboID == 'selFiliadorID_07')
2. Relacao Entre Conceitos (glb_nFormID == 2120)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Clientelia Asstec
        (glb_sQuemENaRel == 'OBJ')
3. Relacao Entre Recursos (glb_nFormID == 1120)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Objeto
        (glb_sQuemENaRel == 'OBJ')
4. Relacao Entre Pessoas e Conceitos (glb_nFormID == 2130)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Objeto
        (glb_sQuemENaRel == 'OBJ')
5. Relacao Entre Pessoas e Recursos (glb_nFormID == 1130)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')

********************************************************************/
function dsopesq_DSC() {
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    if ( glb_nFormID == 1220 )
    {
        if (glb_sQuemENaRel == 'SUJ')
        {
            headerGrid(fg,['Nome',
                           'ID',
                           'Fantasia',
                           'Sexo'], [3]);

            fillGridMask(fg,dsoPesq,['fldCompleteName',
                                     'fldID',
                                     'fldName',
                                     'Sexo'],
                                     ['','','','']);
        }
        else if ( (glb_sQuemENaRel == 'OBJ') &&
                  ((glb_nTipoRelacaoID == 22) ||
                   (glb_nTipoRelacaoID == 23) ||
                   (glb_nTipoRelacaoID == 24)) )
        {
            headerGrid(fg,['Nome',
                           'ID',
                           'Fantasia'], []);

            fillGridMask(fg,dsoPesq,['fldCompleteName',
                                     'fldID',
                                     'fldName'],
                                     ['','','']);
        
        }
        else if ( (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 25) )
        {
            headerGrid(fg,['Nome',
                           'ID',
                           'Fantasia',
                           'Servi�o'], [3]);

            fillGridMask(fg,dsoPesq,['fldCompleteName',
                                     'fldID',
                                     'fldName',
                                     'Servico'],
                                     ['','','','']);
        }
        else if ( (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID != 22) &&
                  (glb_nTipoRelacaoID != 23) &&
                  (glb_nTipoRelacaoID != 24) &&
                  (glb_nTipoRelacaoID != 25) )
        {
            headerGrid(fg,['Nome',
                           'ID',
                           'Fantasia'], []);

            fillGridMask(fg,dsoPesq,['fldCompleteName',
                                     'fldID',
                                     'fldName'],
                                     ['','','']);
        }
        else if ( glb_sComboID == 'selFiliadorID_07' )
        {
            headerGrid(fg,['Nome',
                           'ID',
                           'Fantasia'], []);

            fillGridMask(fg,dsoPesq,['fldCompleteName',
                                     'fldID',
                                     'fldName'],
                                     ['','','']);
        }
    }
    
    // Visitas
    if ( glb_nFormID == 5240)
    {
        headerGrid(fg,['Nome',
                       'ID',
                       'Fantasia',
                       'Sexo'], [3]);

        fillGridMask(fg,dsoPesq,['fldCompleteName',
                                 'fldID',
                                 'fldName',
                                 'Sexo'],
                                 ['','','','']);
    }
    
    if (glb_nFormID == 1130)
    {
        if (glb_sQuemENaRel == 'SUJ')
        {
            headerGrid(fg,['Nome',
                           'ID',
                           'Fantasia'], []);

            fillGridMask(fg,dsoPesq,['fldCompleteName',
                                     'fldID',
                                     'fldName'],
                                     ['','','']);
        }                                     
        else if (glb_sQuemENaRel == 'OBJ')
        {
            headerGrid(fg,['Recurso',
                           'ID'], []);

            fillGridMask(fg,dsoPesq,['fldName',
                                     'fldID'],
                                     ['','']);
        }
    }
    else if (glb_nFormID == 1120)
    {
        if (glb_sQuemENaRel == 'SUJ')
        {
            headerGrid(fg,['Recurso',
                           'ID'], []);

            fillGridMask(fg,dsoPesq,['fldName',
                                     'fldID'],
                                     ['','']);
        }                                     
        else if (glb_sQuemENaRel == 'OBJ')
        {
            headerGrid(fg,['Recurso',
                           'ID'], []);

            fillGridMask(fg,dsoPesq,['fldName',
                                     'fldID'],
                                     ['','']);
        }
    }
    else if (glb_nFormID == 2120)
    {
        if (glb_sQuemENaRel == 'SUJ')
        {
            headerGrid(fg,['Conceito',
                           'ID'], []);

            fillGridMask(fg,dsoPesq,['fldName',
                                     'fldID'],
                                     ['','']);
        }                                     
        else if (glb_sQuemENaRel == 'OBJ')
        {
            headerGrid(fg,['Conceito',
                           'ID'], []);

            fillGridMask(fg,dsoPesq,['fldName',
                                     'fldID'],
                                     ['','']);
        }
    }
    else if (glb_nFormID == 2130)
    {
        if (glb_sQuemENaRel == 'SUJ')
        {
            headerGrid(fg,['Nome',
                           'ID',
                           'Fantasia'], []);

            fillGridMask(fg,dsoPesq,['fldCompleteName',
                                     'fldID',
                                     'fldName'],
                                     ['','','']);
        }                                     
        else if (glb_sQuemENaRel == 'OBJ')
        {
            headerGrid(fg,['Conceito',
                           'ID'], []);

            fillGridMask(fg,dsoPesq,['fldName',
                                     'fldID'],
                                     ['','']);
        }
    }
    else if ((glb_nFormID == 5150)||(glb_nFormID == 5160))
    {
		if (glb_sQuemENaRel == 'SUJ')
		{
			headerGrid(fg,['Nome',
						   'ID',
						   'Identificador',
						   'Fantasia',
						   'Documento',
						   'Cidade',
						   'UF',
						   'Pa�s'], []);

			fillGridMask(fg,dsoPesq,['fldCompleteName',
									 'fldID',
									 'Identificador',
									 'fldName',
									 'Documento',
									 'Cidade',
									 'UF',
									 'Pais'],
									 ['','','','','','','','']);
		}
		else
		{
			headerGrid(fg,['Nome',
						   'ID',
						   'Fantasia',
						   'Documento',
						   'Cidade',
						   'UF',
						   'Pa�s'], []);

			fillGridMask(fg,dsoPesq,['fldCompleteName',
									 'fldID',
									 'fldName',
									 'Documento',
									 'Cidade',
									 'UF',
									 'Pais'],
									 ['','','','','','','']);
		}
	}
    else if ((glb_nFormID == 7140) || (glb_nFormID == 7150) || 
			 (glb_nFormID == 7180) || (glb_nFormID == 12210) )
    {
		headerGrid(fg,['Nome',
					   'ID',
					   'Fantasia',
					   'EMail',
					   'DDI',
					   'DDD',
					   'Numero'], [3, 4, 5, 6]);

		fillGridMask(fg,dsoPesq,['fldCompleteName',
								 'fldID',
								 'fldName',
								 'EMail',
								 'DDI',
								 'DDD',
								 'Numero'],
								 ['','','','','','','']);
	}

	// Importa��o
	if(glb_nFormID == 5310) {
			headerGrid(fg,['Nome',
					   'ID',
					   'Fantasia'], []);

		fillGridMask(fg,dsoPesq,['fldCompleteName',
								 'fldID',
								 'fldName'],
								 ['','','']);
	}	
	
    alignColsInGrid(fg,[1]);
                             
    fg.FrozenCols = 1;
    
    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        btnOK.disabled = false;
    }    
    else
        btnOK.disabled = true;    
        
    // writeInStatusBar('child', 'cellMode', 'XXXXXXXX');    
}

/********************************************************************
Monta e retorna array de dados a serem enviados para o sub form que chamou
a modal

1. Relacao Entre Pessoas (glb_nFormID == 1220)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Clientelia Asstec
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 22)
    1.3. Clientelia Transporte
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 23)
    1.4. Clientelia Banco
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 24)
    1.5. Clientelia Servicos
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 25)
    1.6. Outras Relacoes
        (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID != 22) &&
            (glb_nTipoRelacaoID != 23) &&
            (glb_nTipoRelacaoID != 24) &&
            (glb_nTipoRelacaoID != 25)
    1.7. Filiador de Programa de Marketing 
        (glb_sComboID == 'selFiliadorID_07')        
2. Relacao Entre Conceitos (glb_nFormID == 2120)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Clientelia Asstec
        (glb_sQuemENaRel == 'OBJ')
3. Relacao Entre Recursos (glb_nFormID == 1120)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Objeto
        (glb_sQuemENaRel == 'OBJ')
4. Relacao Entre Pessoas e Conceitos (glb_nFormID == 2130)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')
    1.2. Objeto
        (glb_sQuemENaRel == 'OBJ')
5. Relacao Entre Pessoas e Recursos (glb_nFormID == 1130)
    1.1. Sujeito
        (glb_sQuemENaRel == 'SUJ')

********************************************************************/
function dataToReturn()
{
    var aData = new Array();
            
    switch(glb_nFormID) {
		case 1220:
		{
			if (glb_sQuemENaRel == 'SUJ')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
				aData[5] = fg.TextMatrix(fg.Row, 3);
			}
			else if ( (glb_sQuemENaRel == 'OBJ') &&
					  ((glb_nTipoRelacaoID == 22) ||
					   (glb_nTipoRelacaoID == 23) ||
					   (glb_nTipoRelacaoID == 24)) )
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
			}
			else if ( (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID == 25) )
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
				aData[5] = fg.TextMatrix(fg.Row, 3);
			}
			else if ( (glb_sQuemENaRel == 'OBJ') && (glb_nTipoRelacaoID != 22) &&
					  (glb_nTipoRelacaoID != 23) &&
					  (glb_nTipoRelacaoID != 24) &&
					  (glb_nTipoRelacaoID != 25) )
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
			}
			else if ( glb_sComboID == 'selFiliadorID_07' )
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
			}
			
			break;
		}
		
		case 1130:
		{
			if (glb_sQuemENaRel == 'SUJ')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
			}
			else if (glb_sQuemENaRel == 'OBJ')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 0);
			}
			
			break;
		}
		
		// Visitas
		case 5240:
		{
			if (glb_sQuemENaRel == 'PESSOA')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
				aData[5] = fg.TextMatrix(fg.Row, 3);
			}
			else if (glb_sQuemENaRel == 'PROSPECT')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
				aData[5] = fg.TextMatrix(fg.Row, 3);
			}
			
			break;
		}
		
		case 2120:
		case 1120:
		{
			if (glb_sQuemENaRel == 'SUJ')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 0);
			}
			else if (glb_sQuemENaRel == 'OBJ')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 0);
			}
			
			break;
		}
		
		case 2130:
		{
			if (glb_sQuemENaRel == 'SUJ')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 2);
			}
			else if (glb_sQuemENaRel == 'OBJ')
			{
				aData[0] = glb_sQuemENaRel;
				aData[1] = glb_nTipoRelacaoID;
				aData[2] = glb_sComboID;
				aData[3] = fg.TextMatrix(fg.Row, 1);
				aData[4] = fg.TextMatrix(fg.Row, 0);
			}
			
			break;
		}
		
		case 5150:
		case 5160:
		{
			// id do combo
			aData[0] = glb_sComboID;
			// id da pessoa
			aData[1] = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'fldID'));
			// fantasia da pessoa
			aData[2] = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'fldName'));

			if (glb_sQuemENaRel == 'SUJ')        
				aData[3] = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Identificador'));
	    
			break;    
		}
		
		case 7140:
		case 7150:
		case 7180:
		case 12210:
		{
			// id do combo
			aData[0] = glb_sComboID;
			// id da pessoa
			aData[1] = fg.TextMatrix(fg.Row, 1);
			// fantasia da pessoa
			aData[2] = fg.TextMatrix(fg.Row, 2);
			// e-mail da pessoa
			aData[3] = fg.TextMatrix(fg.Row, 3);
			// DDI
			aData[4] = fg.TextMatrix(fg.Row, 4);
			// DDD
			aData[5] = fg.TextMatrix(fg.Row, 5);
			// Numero do Telefone
			aData[6] = fg.TextMatrix(fg.Row, 6);
			
			break;
		}
		
		case 5310:
		{
			aData[0] = glb_sQuemENaRel;
			aData[1] = glb_nTipoRelacaoID;
			aData[2] = glb_sComboID;
			aData[3] = fg.TextMatrix(fg.Row, 1);
			aData[4] = fg.TextMatrix(fg.Row, 0);
			
			break;
		}
	}

    return aData;
}
