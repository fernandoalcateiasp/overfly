<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    
    'Forca recarregamento da pagina
    Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
    
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modaltermousoHtml" name="modaltermousoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_overlist.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodalcontroledoc.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/modalcontroledocumento.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i
Dim sCaller
Dim sTitulo
Dim nDocumentoID
Dim nTipoDocToLoad
Dim sAnchorID
Dim sCallOrigin

'Regra definida em 29/09/2003 para atender navegacao preferencial,
'procurar glb_sCallOrigin nos arquivos que compoem a maquina
'de navegacao.
'A variavel sCallOrigin e um literal cujos valores sao:
'sCallOrigin = 'T' - texto
'sCallOrigin = 'E' - estudo
'sCallOrigin = 'D' - desativo

sCaller = ""
sTitulo = ""
nDocumentoID = 0
nTipoDocToLoad = Null
sAnchorID = ""
sCallOrigin = ""

For i = 1 To Request.QueryString("sCaller").Count
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("sTitulo").Count
    sTitulo = Request.QueryString("sTitulo")(i)
Next

For i = 1 To Request.QueryString("nDocumentoID").Count
    nDocumentoID = Request.QueryString("nDocumentoID")(i)
Next

For i = 1 To Request.QueryString("nTipoDocToLoad").Count
    nTipoDocToLoad = Request.QueryString("nTipoDocToLoad")(i)
Next

For i = 1 To Request.QueryString("sAnchorID").Count
    sAnchorID = Request.QueryString("sAnchorID")(i)
Next

For i = 1 To Request.QueryString("sCallOrigin").Count
    sCallOrigin = Request.QueryString("sCallOrigin")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_sTitulo = " & Chr(39) & sTitulo & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nDocumentoID = " & nDocumentoID & ";"
Response.Write vbcrlf

Response.Write "var glb_sAnchorID__ = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf

if ( Not IsNull(sAnchorID) ) Then
	Response.Write "glb_sAnchorID__ = " & Chr(39) & sAnchorID & Chr(39) & ";"
End If	

Response.Write "var glb_sCallOrigin = " & Chr(39) & sCallOrigin & Chr(39) & ";"
Response.Write vbcrlf

Response.Write vbcrlf

'Tipo do documento a ser carregado.
' < 0 -> Estudo
' Null -> Atual
' > 0 -> Revisao
Response.Write "var glb_nTipoDocToLoad = " & Chr(39) & nTipoDocToLoad & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</script>

</head>

<body id="modaltermousoBody" name="modaltermousoBody" LANGUAGE="javascript" onload="return window_onload()">

    <p id="lblDocumento" name="lblDocumento" class="lblGeneral">Documento</p>
    <select id="selDocumento" name="selDocumento" class="fldGeneral"></select>
    <p id="lblRevisao" name="lblRevisao" class="lblGeneral" title="N�mero de revis�o do documento">Rev</p>
    <input type="text" id="txtRevisao" name="txtRevisao" class="fldGeneral" title="N�mero de revis�o do documento"></input>
    <p id="lbldtEmissao" name="lbldtEmissao" class="lblGeneral">Emiss�o</p>
    <input type="text" id="txtdtEmissao" name="txtdtEmissao" class="fldGeneral"></input>
    <p id="lblTotPags" name="lblTotPags" class="lblGeneral">P�g</p>
    <input type="text" id="txtTotPags" name="txtTotPags" class="fldGeneral" title="N�mero estimado de p�ginas"></input>

	<iframe id="frameDocumento" name="frameDocumento" class="theFrames"></iframe>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

</body>

</html>
