/********************************************************************
modalatendimento.js

Library javascript para o modalatendimento.aspx
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
// Variaveis de eventos de grid
var glb_OcorrenciasTimerInt = null;
var glb_PassedOneInDblClick = false;
var glb_aSendDataToServer = new Array();
var glb_aDadosSelecionados = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;
var glb_GravacaoPendente = false;
var glb_getServerData = 0;
var glb_MeioAtendimentoID = 0;
var glb_rewriteInterval = null;
var glb_nPesAtendimentoID = null;
var glb_aDivControls = null;
var glb_aDivControlsFone = null;
var glb_dtAtendimentoInicio = null;
var glb_dtAtendimentoFim = null;
var glb_AtendimentoTimer = null;
var glb_nPessoaGravacaoID = 0;
var glb_sColunaClassificacao = 'ClassificacaoID*';
var glb_txtPesquisa = '';
var glb_chkAtender1 = null;
var glb_bDadosContatoImcompletos = false;
var glb_nSelProprietarioValue = null;
var glb_nEmaClienteID = null;
var glb_nObservacoes = null;
var glb_bRegistraObservacao = false;
var glb_nLinhaSelecionadaFg2 = null;
var glb_nQuantidadeAtendimentosPendentes = 0;
var glb_bExibeAtendimentoAnteriores = false;
var glb_bCarregaProdutosOferta = false;
var glb_nPessoaFocoID = null;

var glb_DsoToReturn = null;
var dsoGrid = new CDatatransport('dsoGrid');
var dsoGrava = new CDatatransport('dsoGrava');
var dsoGrid2 = new CDatatransport('dsoGrid2');
var dsoGrid3 = new CDatatransport('dsoGrid3');
var dsoGrid4 = new CDatatransport('dsoGrid4');
var dsoGrid5 = new CDatatransport('dsoGrid5');
var dsoDetalhesCliente = new CDatatransport('dsoDetalhesCliente');
var dsoDetalhesCliente2 = new CDatatransport('dsoDetalhesCliente2');
var dsoClassificacao = new CDatatransport('dsoClassificacao');
var dsoAgendarHorario = new CDatatransport('dsoAgendarHorario');
var dsoGravarAgendamento = new CDatatransport('dsoGravarAgendamento');
var dsoAtendimentoAnterior = new CDatatransport('dsoAtendimentoAnterior');
var dsoListaPrecos = new CDatatransport('dsoListaPrecos');
var dsoWeb = new CDatatransport('dsoWeb');
var dsoAtendimentosAnteriores = new CDatatransport('dsoAtendimentosAnteriores');

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
fillGridData()
fillGridData2()
fillGridData_DSC()
fillGridData2_DSC()
paintCellsSpecialyReadOnly()

js_fg_modalatendimentoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel)
js_modalatendimentoKeyPress(KeyAscii)
js_modalatendimento_ValidateEdit()
js_fg_modalatendimentoEnterCell (fg)
js_fg_AfterRowColmodalatendimento (grid, OldRow, OldCol, NewRow, NewCol)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');

    window_onload_1stPart();

    glb_aDadosSelecionados = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_aDadosSelecionados');

    // configuracao inicial do html
    setupPage();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('CRM (beta)', 1);


    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var btnWidth = 78;
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 41;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    // desabilita o botao OK e Cancelar
    btnOK.disabled = true;
    btnCanc.disabled = true;
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    // Ajusta o DivControls
    adjustElementsInForm([
        ['lblServicos', 'selServicos', 12, 1, -10, -10],
        ['lblProprietario', 'selProprietario', 27, 1],
        ['lblEstados', 'selEstados', 3, 1],
        ['lblClassificacao', 'selClassificacao', 19, 1],
    	['lblPesquisa', 'txtPesquisa', 21, 1],
        ['btnListar', 'btn', btn_width - 8, 1, 14],
        ['btnGravar', 'btn', btn_width - 8, 1],
        ['btnIniciarAtendimento', 'btn', btn_width - 8, 1],
        ['btnFinalizar', 'btn', btn_width - 8, 1],
        ['lblData', 'selData', 12, 2, -10],
        ['lblOferta', 'selOferta', 27, 2],
        ['lblAnalitico', 'chkAnalitico', 3, 2, 196],
        ['lblAtivo', 'chkAtivo', 3, 2, -14],
        ['lblCredito', 'chkCredito', 3, 2, -5],
        ['lblAtender', 'chkAtender1', 3, 2, -5],
        ['lblAtender', 'chkAtender2', 3, 2, -34],
        ['lblDetalhes', 'chkDetalhes1', 3, 2, -25],
        ['lblDetalhes', 'chkDetalhes2', 3, 2, -40],
        ['btnSite', 'btn', btn_width - 8, 2, -22],
        ['btnAtendimentosAnteriores', 'btn', btn_width + 10, 2],
        ['btnProximo', 'btn', btn_width, 2]], null, null, true);

    // Ajusta o DivControls
    //adjustElementsInForm([
    //    ['lblServicos', 'selServicos', 12, 1, -10, -10],
    //    ['lblProprietario', 'selProprietario', 19, 1],
    //    ['lblEstados', 'selEstados', 3, 1],
    //    ['lblClassificacao', 'selClassificacao', 19, 1],
    //	  ['lblPesquisa', 'txtPesquisa', 28, 1],
    //    ['btnListar', 'btn', btn_width - 8, 1, 22],
    //    ['btnSite', 'btn', btn_width - 8, 1],
    //    ['btnProximo', 'btn', btn_width - 8, 1],
    //    ['btnIniciarAtendimento', 'btn', btn_width - 8, 1],
    //    ['lblData', 'selData', 12, 2, -10],
    //    ['lblAnalitico', 'chkAnalitico', 3, 2, 358],
    //    ['lblAtivo', 'chkAtivo', 3, 2, -14],
    //    ['lblAtender', 'chkAtender1', 3, 2, -5],
    //    ['lblCredito', 'chkCredito', 3, 2, -4],
    //    ['lblDetalhes', 'chkDetalhes1', 3, 2, -7],
    //    ['lblDetalhes', 'chkDetalhes2', 3, 2, -40],
    //    ['btnGravar', 'btn', btn_width - 8, 2, 49],//['btnGravar', 'btn', btn_width - 8, 2, 2],
    //    ['btnAtendimentosAnteriores', 'btn', btn_width + 10, 2],
    //    ['btnFinalizar', 'btn', btn_width - 8, 2]], null, null, true);

    chkAnalitico.checked = true;
    chkAnalitico.disabled = true;

    selClassificacao.style.height = 83;
    lblAtender.style.left = parseInt(chkAtender1.style.left, 10) + 4;
    lblDetalhes.style.left = parseInt(chkDetalhes1.style.left, 10) + 4;

    btnProximo.setAttribute('state', 0, 1);
    btnIniciarAtendimento.setAttribute('state', 0, 1);

    btnFinalizar.disabled = true;

    // status=0 iniciar, =1-finalizar
    setbtnIniciarAtendimentoStatus(0);

    // Ajusta o divDetalhesCliente
    adjustElementsInForm([['lblClienteDesde', 'txtClienteDesde', 11, 1, -10, -10],
        ['lblAnos', 'txtAnos', 4, 1, -5],
		['lblPedidosFaturados', 'txtPedidosFaturados', 6, 1, -5],
		['lblFaturamentoAcumulado', 'txtFaturamentoAcumulado', 11, 1, -5],
		['lblContribuicaoAcumulada', 'txtContribuicaoAcumulada', 11, 1, -5],
		['lblContribuicaoAcumuladaRecebida', 'txtContribuicaoAcumuladaRecebida', 11, 1, -5],
		['lblAtraso', 'txtAtraso', 5, 1, -5],
		['lblMaiorCompra', 'txtMaiorCompra', 17, 1, -5],
		['lblDiasUltimaCompra', 'txtDiasUltimaCompra', 5, 1, -5],
		['lblDataUltimaCompra', 'txtDataUltimaCompra', 17, 1, -5],
		['lblMaiorAcumulo', 'txtUltimaCompra', 17, 1, -5]], null, null, true);

    // Ajusta o DivTelefones/URLs
    adjustElementsInForm([['lblDDITelefone', 'txtDDITelefone', 4, 1, -10, -10],
		['lblDDDComercial', 'txtDDDComercial', 4, 1, -3],
		['lblFoneComercial', 'txtFoneComercial', 9, 1, -10],
		['lblDDDFaxCelular', 'txtDDDFaxCelular', 4, 1, -3],
		['lblFaxCelular', 'txtFaxCelular', 9, 1, -10],
		['lblEmail', 'txtEmail', 38, 1, -2],
		['lblSite', 'txtSite', 38, 1, -2]], null, null, true);

    // Ajusta o divFinalizarAtendimento
    adjustElementsInForm([['lblMeioAtendimento', 'selMeioAtendimento', 18, 2, 0, 0],
        ['lblReagendamento', 'selReagendamento', 18, 2],
		['lblAgendar', 'txtAgendar', 18, 2],
        ['lblAtendimentoOK', 'chkAtendimentoOK', 3, 2],
		['btnSolicitacaoCredito', 'btn', 148, 3, 391, -14],
		['lblObservacoes', 'txtObservacoes', 25, 4, 0, -8]], null, null, true);

    chkAtendimentoOK.checked = true;
    chkDetalhes1.checked = true;

    detalhesClienteReadOnly();

    btnSolicitacaoCredito.style.visibility = 'hidden';

    selData.onchange = selData_onchange;

    selOferta.onchange = selOferta_onchange;

    txtObservacoes.style.width = 485;
    txtObservacoes.style.height = 150;
    txtObservacoes.onfocus = selFieldContent;
    txtObservacoes.maxLength = 1000;
    txtObservacoes.onkeypress = txtObservacoes_onKeyPress;

    txtAtendimentosAnteriores.onkeydown = txtAtendimentosAnteriores_onkeydown;

    btnConfirmarFinalizacao.style.top = txtObservacoes.offsetTop + txtObservacoes.offsetHeight + 10;
    btnConfirmarFinalizacao.style.left = ((txtObservacoes.offsetLeft + txtObservacoes.offsetWidth) / 2) - btnConfirmarFinalizacao.offsetWidth - 5;
    btnConfirmarFinalizacao.value = 'OK';

    btnCancelarFinalizacao.style.top = txtObservacoes.offsetTop + txtObservacoes.offsetHeight + 10;
    btnCancelarFinalizacao.style.left = btnConfirmarFinalizacao.offsetLeft + btnConfirmarFinalizacao.offsetWidth + 10;
    btnCancelarFinalizacao.value = 'Cancelar';

    selEstados.style.height = 70;
    selEstados.onchange = limpaGrid;

    selClassificacao.onchange = limpaGrid;
    selProprietario.onchange = selProprietario_onchange;

    if (selProprietario.value != 0)
        lblProprietario.innerText = 'Propriet�rio ' + selProprietario.value;

    txtPesquisa.maxLength = 40;
    txtPesquisa.onkeypress = txtPesquisa_onKeyPress;

    selServicos.onchange = selServicos_onChange;
    chkAtivo.onclick = selServicos_onChange;
    chkDetalhes1.onclick = chkDetalhes1_onclick;
    chkDetalhes2.onclick = chkDetalhes2_onclick;

    selSeguradora.onchange = limpaGrid;
    selMarca.onchange = limpaGrid;
    chkPedidosPendentes.onclick = limpaGrid;

    selSeguradora.selectedIndex = 0;


    txtLimiteMinimo.onkeypress = verifyNumericEnterNotLinked;
    txtLimiteMinimo.onkeyup = txtPesquisa_onKeyPress;
    txtLimiteMinimo.setAttribute('verifyNumPaste', 1);
    txtLimiteMinimo.setAttribute('thePrecision', 11, 1);
    txtLimiteMinimo.setAttribute('theScale', 2, 1);
    txtLimiteMinimo.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtLimiteMinimo.onfocus = selFieldContent;
    txtLimiteMinimo.maxLength = 11;

    txtLimiteMaximo.onkeypress = verifyNumericEnterNotLinked;
    txtLimiteMaximo.onkeyup = txtPesquisa_onKeyPress;
    txtLimiteMaximo.setAttribute('verifyNumPaste', 1);
    txtLimiteMaximo.setAttribute('thePrecision', 11, 1);
    txtLimiteMaximo.setAttribute('theScale', 2, 1);
    txtLimiteMaximo.setAttribute('minMax', new Array(0, 999999999.99), 1);
    txtLimiteMaximo.onfocus = selFieldContent;
    txtLimiteMaximo.maxLength = 11;

    txtDiasVencidos.onkeypress = verifyNumericEnterNotLinked;
    txtDiasVencidos.onkeyup = txtPesquisa_onKeyPress;
    txtDiasVencidos.setAttribute('verifyNumPaste', 1);
    txtDiasVencidos.setAttribute('thePrecision', 10, 1);
    txtDiasVencidos.setAttribute('theScale', 0, 1);
    txtDiasVencidos.setAttribute('minMax', new Array(0, 9999), 1);
    txtDiasVencidos.onfocus = selFieldContent;
    txtDiasVencidos.maxLength = 4;

    txtAgendar.onkeypress = verifyDateTimeNotLinked;
    txtAgendar.onkeydown = txtAgendar_onkeydown;
    txtAgendar.setAttribute('verifyNumPaste', 1);
    txtAgendar.setAttribute('thePrecision', 10, 1);
    txtAgendar.setAttribute('theScale', 0, 1);
    txtAgendar.setAttribute('minMax', new Array(0, 9999999999999999), 1);
    txtAgendar.onfocus = selFieldContent;
    txtAgendar.maxLength = 16;

    txtObservacoes.maxLength = 2000;
    txtObservacoes.onfocus = selFieldContent;

    txtDDITelefone.onkeypress = verifyNumericEnterNotLinked;
    txtDDITelefone.onkeyup = txtFoneControls_onKeyup;
    txtDDITelefone.setAttribute('verifyNumPaste', 1);
    txtDDITelefone.setAttribute('thePrecision', 10, 1);
    txtDDITelefone.setAttribute('theScale', 0, 1);
    txtDDITelefone.setAttribute('minMax', new Array(0, 9999), 1);
    txtDDITelefone.onfocus = selFieldContent;
    txtDDITelefone.maxLength = 4;

    txtDDDComercial.onkeypress = verifyNumericEnterNotLinked;
    txtDDDComercial.onkeyup = txtFoneControls_onKeyup;
    txtDDDComercial.setAttribute('verifyNumPaste', 1);
    txtDDDComercial.setAttribute('thePrecision', 10, 1);
    txtDDDComercial.setAttribute('theScale', 0, 1);
    txtDDDComercial.setAttribute('minMax', new Array(0, 9999), 1);
    txtDDDComercial.onfocus = selFieldContent;
    txtDDDComercial.maxLength = 4;

    txtFoneComercial.onkeypress = verifyNumericEnterNotLinked;
    txtFoneComercial.onkeyup = txtFoneControls_onKeyup;
    txtFoneComercial.ondblclick = txtFoneComercial_ondblclick;
    txtFoneComercial.setAttribute('verifyNumPaste', 1);
    txtFoneComercial.setAttribute('thePrecision', 10, 1);
    txtFoneComercial.setAttribute('theScale', 0, 1);
    txtFoneComercial.setAttribute('minMax', new Array(0, 999999999), 1);
    txtFoneComercial.onfocus = selFieldContent;
    txtFoneComercial.maxLength = 9;

    txtDDDFaxCelular.onkeypress = verifyNumericEnterNotLinked;
    txtDDDFaxCelular.onkeyup = txtFoneControls_onKeyup;
    txtDDDFaxCelular.setAttribute('verifyNumPaste', 1);
    txtDDDFaxCelular.setAttribute('thePrecision', 10, 1);
    txtDDDFaxCelular.setAttribute('theScale', 0, 1);
    txtDDDFaxCelular.setAttribute('minMax', new Array(0, 9999), 1);
    txtDDDFaxCelular.onfocus = selFieldContent;
    txtDDDFaxCelular.maxLength = 4;

    txtFaxCelular.onkeypress = verifyNumericEnterNotLinked;
    txtFaxCelular.onkeyup = txtFoneControls_onKeyup;
    txtFaxCelular.ondblclick = txtFaxCelular_ondblclick;
    txtFaxCelular.setAttribute('verifyNumPaste', 1);
    txtFaxCelular.setAttribute('thePrecision', 10, 1);
    txtFaxCelular.setAttribute('theScale', 0, 1);
    txtFaxCelular.setAttribute('minMax', new Array(0, 999999999), 1);
    txtFaxCelular.onfocus = selFieldContent;
    txtFaxCelular.maxLength = 9;

    txtEmail.maxLength = 80;
    txtEmail.onkeyup = txtFoneControls_onKeyup;
    txtEmail.ondblclick = txtEmail_ondblclick;

    txtSite.maxLength = 80;
    txtSite.onkeyup = txtFoneControls_onKeyup;
    txtSite.ondblclick = txtSite_ondblclick;

    selReagendamento.onchange = selReagendamento_onchange;

    // Ajusta o divControls
    with (divControls.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
        width = selEstados.offsetLeft + selEstados.offsetWidth + ELEM_GAP;
        height = (selEstados.offsetTop + selEstados.offsetHeight);
    }
    divControls.setAttribute('bEnable', true, 1);

    // ajusta o divFG
    with (divFG.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = 217;
    }

    // ajusta o grid    
    with (fg.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    // ajusta o divDetalhesCliente
    with (divDetalhesCliente.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFG.currentStyle.top, 10) + parseInt(divFG.currentStyle.height, 10) + ELEM_GAP;
        //top = 354;
        width = txtAnos.offsetLeft + txtAnos.offsetWidth + ELEM_GAP;
        height = 1.18 * (txtAnos.offsetTop + txtAnos.offsetHeight);
    }

    // ajusta o divTelefones/URLs
    with (divControlsFone.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divDetalhesCliente.currentStyle.top, 10) + parseInt(divDetalhesCliente.currentStyle.height, 10) + ELEM_GAP;
        //top = 354;
        width = txtDDITelefone.offsetLeft + txtDDITelefone.offsetWidth + ELEM_GAP;
        height = 1.18 * (txtDDITelefone.offsetTop + txtDDITelefone.offsetHeight);
    }
    divControlsFone.setAttribute('bEnable', true, 1);

    // ajusta o divAtendimento   
    with (divFinalizarAtendimento.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 242;
        top = divFG.currentStyle.top;
        width = txtObservacoes.offsetLeft + txtObservacoes.offsetWidth + ELEM_GAP;
        height = 200;
    }

    // ajusta o divFG2
    with (divFG2.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP + 35;
        top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6 - 35;
        height = 200;
    }

    // lblPendente e chkPendente
    lblPendente.style.left = ELEM_GAP;
    lblPendente.style.top = parseInt(divControls.currentStyle.top, 10) + parseInt(divControls.currentStyle.height, 10) + ELEM_GAP;
    chkPendente.style.left = -2;
    chkPendente.style.top = parseInt(lblPendente.currentStyle.top, 10) + parseInt(lblPendente.currentStyle.height, 10) - 3;
    chkPendente.style.width = 40;
    chkPendente.onclick = chkPendente_onclick;

    // lblCliente e chkCliente
    lblCliente.style.left = ELEM_GAP;
    lblCliente.style.top = parseInt(lblPendente.currentStyle.top, 10) + parseInt(lblPendente.currentStyle.height, 10) + parseInt(chkPendente.currentStyle.height, 10);
    chkCliente.style.left = -2;
    chkCliente.style.top = parseInt(lblCliente.currentStyle.top, 10) + parseInt(lblCliente.currentStyle.height, 10) - 3;
    chkCliente.style.width = 40;
    chkCliente.onclick = chkPendente_onclick;

    // ajusta o grid    
    with (fg2.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10);
        height = parseInt(divFG2.style.height, 10);
    }

    // ajusta o divFG5 - Ofertas
    with (divFG5.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = 447;
        width = 250;
        height = 90;
    }

    // ajusta o grid    
    with (fg5.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG5.style.width, 10);
        height = parseInt(divFG5.style.height, 10);
    }

    // ajusta o divFG3
    with (divFG3.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = parseInt(divFG5.currentStyle.left, 10) + ELEM_GAP + parseInt(divFG5.currentStyle.width, 10);
        top = 447;
        width = parseInt(divFG.currentStyle.width, 10) - ELEM_GAP - parseInt(divFG5.currentStyle.width, 10);
        height = 90;
    }

    // ajusta o grid    
    with (fg3.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG3.style.width, 10);
        height = parseInt(divFG3.style.height, 10);
    }

    // ajusta o txtAtendimentosAnteriores
    with (txtAtendimentosAnteriores.style) {
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFG2.currentStyle.top, 10) + parseInt(divFG2.currentStyle.height, 10) + ELEM_GAP;
        width = 380; //(modWidth - (3 * ELEM_GAP)) / 2; //509 965 - 482, 483
        height = 200;
    }

    // ajusta o divFG4
    with (divFG4.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP + parseInt(txtAtendimentosAnteriores.currentStyle.width, 10) + ELEM_GAP;
        top = parseInt(txtAtendimentosAnteriores.currentStyle.top, 10);
        width = (modWidth - (3 * ELEM_GAP)) - parseInt(txtAtendimentosAnteriores.currentStyle.width, 10) - 6;
        height = parseInt(txtAtendimentosAnteriores.currentStyle.height, 10);;
    }

    // ajusta o grid    
    with (fg4.style) {
        left = 0;
        top = 0;
        width = parseInt(divFG4.style.width, 10);
        height = parseInt(divFG4.style.height, 10);
    }

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(txtAtendimentosAnteriores.currentStyle.width, 10) * 18;
    fg2.Redraw = 2;

    startGridInterface(fg3);
    fg3.Cols = 0;
    fg3.Cols = 1;
    fg3.ColWidth(0) = parseInt(txtAtendimentosAnteriores.currentStyle.width, 10) * 26;
    fg3.Redraw = 2;

    startGridInterface(fg4);
    fg4.Cols = 0;
    fg4.Cols = 1;
    fg4.ColWidth(0) = parseInt(divFG4.currentStyle.width, 10) * 28;
    fg4.Redraw = 2;

    startGridInterface(fg5);
    fg5.Cols = 0;
    fg5.Cols = 1;
    fg5.ColWidth(0) = parseInt(divFG5.currentStyle.width, 10) * 15;
    fg5.Redraw = 2;

    lblFinalizarAtendimento.style.top = 20;
    lblFinalizarAtendimento.style.left = (divFinalizarAtendimento.offsetWidth / 2) - 76;
    lblFinalizarAtendimento.innerHTML = lblFinalizarAtendimento.innerHTML.bold();

    setbtnAtendimentosAnteriores(0);

    if ((glb_nA1 == 1) && (glb_nA2 == 1))
        glb_sColunaClassificacao = 'ClassificacaoID*';

    glb_getServerData = 1;

    var bCarregaReceptivo = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bCarregaReceptivo');

    if (bCarregaReceptivo) {
        chkAtivo.checked = false;
    }

    glb_nEmaClienteID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nEmaClienteID');
    glb_nObservacoes = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nObservacoes');

    if (glb_nEmaClienteID == '')
        glb_nEmaClienteID = null;

    if (glb_nObservacoes == '')
        glb_nObservacoes = null;

    visibilityControls();

    setConnection(dsoClassificacao);

    dsoClassificacao.SQL = 'SELECT ItemID AS ClassificacaoID, ItemMasculino AS Classificacao, ' +
        'CONVERT(VARCHAR(8000), Observacoes) AS Observacoes ' +
        'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
        'WHERE (EstadoID=2 AND TipoID = 13 AND ' +
            'dbo.fn_Direitos_TiposAuxiliares(ItemID, NULL, ' + glb_USERID + ', GETDATE()) > 0) ' +
        'ORDER BY Ordem ';

    dsoClassificacao.ondatasetcomplete = dsoServerData_DSC;
    dsoClassificacao.Refresh();
}

function setbtnAtendimentosAnteriores(nStatus) {
    if (nStatus != null)
        btnAtendimentosAnteriores.state = nStatus;

    btnAtendimentosAnteriores.value = (btnAtendimentosAnteriores.state == 0 ? 'Atendimentos' : 'Voltar');
    btnAtendimentosAnteriores.title = (btnAtendimentosAnteriores.state == 0 ? 'Mostrar atendimentos anteriores' : 'Voltar para o grid de atendimento');
}

function setbtnIniciarAtendimentoStatus(nStatus) {
    if (nStatus != null)
        btnIniciarAtendimento.state = nStatus;

    if (btnIniciarAtendimento.state == 0) {
        btnIniciarAtendimento.value = 'Iniciar';
        btnIniciarAtendimento.title = 'Iniciar atendimento';
    }
    else if (btnIniciarAtendimento.state == 1) {
        btnIniciarAtendimento.value = 'Continuar';
        btnIniciarAtendimento.title = 'Continuar atendimento';
    }
}

function selProprietario_onchange() {
    lblProprietario.innerText = 'Propriet�rio ' + selProprietario.value;

    if (selProprietario.value == 0) {
        if (chkAtivo.checked) {
            selProprietario.value = glb_nSelProprietarioValue;

            if (window.top.overflyGen.Alert('Selecionar um propriet�rio.') == 0)
                return null;
        }
        else {
            chkAtivo.checked = false;
            selServicos_onChange();
            chkAtivo.disabled = true;
            limpaGrid();
        }

    }
    else {
        glb_nSelProprietarioValue = selProprietario.value;
        chkAtivo.disabled = false;
        limpaGrid();
    }

}

function txtFoneComercial_ondblclick() {
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');

    if ((txtDDDComercial.value == '') || (txtFoneComercial.value.length < 8))
        return true;

    lockInterface(true);
    dialByModalPage(txtDDDComercial.value, txtFoneComercial.value, nUserID);
    lockInterface(false);

}

function txtFaxCelular_ondblclick() {
    var nTipoPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoPessoaID*'));
    var nUserID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrUserID()');

    if ((txtFaxCelular.value.length < 8) || (txtDDDFaxCelular.value.length < 1) || (nTipoPessoaID != 51))
        return true;

    lockInterface(true);
    dialByModalPage(txtDDDFaxCelular.value, txtFaxCelular.value, nUserID);
    lockInterface(false);
}

function txtEmail_ondblclick() {
    if ((trimStr(txtEmail.value) != '') &&
          (trimStr(txtEmail.value) != '_@_.com.br') &&
          (trimStr(txtEmail.value) != '@_.com.br') &&
          (trimStr(txtEmail.value) != '_@.com.br') &&
          (trimStr(txtEmail.value) != '@.com.br'))
        window.open('mailto:' + txtEmail.value);
}


function txtSite_ondblclick() {
    if ((trimStr(txtSite.value) != '') &&
          (trimStr(txtSite.value) != 'http:/' + '/www._.com.br') &&
          (trimStr(txtSite.value) != 'http:/' + '/www_.com.br') &&
          (trimStr(txtSite.value) != 'http:/' + '/www._com.br') &&
          (trimStr(txtSite.value) != 'http:/' + '/www.com.br'))
        window.open(txtSite.value);
}

function dialByModalPage(sDDD, sNumero, nPessoaID) {
    sDDD = sDDD.toString();
    sNumero = sNumero.toString();
    nPessoaID = parseInt(nPessoaID, 10);

    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

function txtAgendar_onkeydown() {
    if (((event.keyCode >= 48) && (event.keyCode <= 57)) || ((event.keyCode >= 96) && (event.keyCode <= 105))) {
        if (txtAgendar.value.length == 2)
            txtAgendar.value = txtAgendar.value + '/';
        else if (txtAgendar.value.length == 5)
            txtAgendar.value = txtAgendar.value + '/';
        else if (txtAgendar.value.length == 10)
            txtAgendar.value = txtAgendar.value + ' ';
        else if (txtAgendar.value.length == 13)
            txtAgendar.value = txtAgendar.value + ':';
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // codigo privado desta janela
    var _retMsg;
    var sMsg = '';
    var iTipoQuantSelected = 0;

    if (sMsg != '') {
        _retMsg = window.top.overflyGen.Confirm(sMsg);

        if ((_retMsg == 0) || (_retMsg == 2))
            return null;
    }

    if (controlID == btnListar.id)
        fillGridData();
    else if (controlID == btnGravar.id) {
        if (txtAtendimentosAnteriores.style.visibility == 'hidden')
            saveDataInGrid();
        else
            saveAtendimentosAnteriores();
    }
    else if (controlID == btnIncluir.id)
        incluiSeguro();
    else if (controlID == btnExcluir.id)
        excluiSeguro();
    else if (controlID == btnSite.id)
        openSiteWeb();
    else if (controlID == btnSolicitacaoCredito.id) {
        if (window.top.overflyGen.Alert('Solicita��o de cr�dito realizada.') == 0)
            return null;
    }
    else if (controlID == btnProximo.id)
        proximoAtendimento();
    else if (controlID == btnIniciarAtendimento.id) {
        if (glb_nState == 1) {
            salvaDadosSelecionados();
            sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
        }
        else {
            setLinkFoneUrl();

            var sMsg = '';

            if (getCellValueByColKey(fg, 'OK', fg.Row) == 0)
                sMsg += '- Planejamento de vendas n�o aprovado\n';

            if (glb_bDadosContatoImcompletos)
                sMsg += '- Dados de contatos incompletos\n';

            if (sMsg != '') {
                _retMsg = window.top.overflyGen.Confirm('O cliente tem as seguintes pend�ncias:\n' + sMsg + '\nDeseja iniciar o atendimento assim mesmo?', 1);

                if ((_retMsg == 0) || (_retMsg == 2))
                    return null;
                else
                    atender();
            }
            else
                atender();

            //sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'atender()');
            //sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'setTimerAgendamentoStatus(true)');
        }
    }

    else if (controlID == btnFinalizar.id)
        atender();
    else if (controlID == btnAtendimentosAnteriores.id)
        atendimentosAnteriores();
    else if (controlID == btnConfirmarFinalizacao.id)
        confirmarFinalizacaoAtendimento();
    else if (controlID == btnCancelarFinalizacao.id)
        cancelarFinalizacaoAtendimento();
    else if (controlID == btnCanc.id) {
        salvaDadosSelecionados();

        if (glb_nState != 1) {
            glb_nEmaClienteID = null;
            sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nEmaClienteID=null');
            sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nObservacoes=null');
        }

        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer) {
    // @@ atualizar parametros e dados aqui
    lockControlsInModalWin(true);
}

/********************************************************************
Solicitar dados do grid ao servidor
********************************************************************/
function fillGridData(nPessoaGravacaoID) {
    if (glb_OcorrenciasTimerInt != null) {
        window.clearInterval(glb_OcorrenciasTimerInt);
        glb_OcorrenciasTimerInt = null;
    }

    lockControlsInModalWin(true);

    var strPars = '';
    var nItensSelected = 0;
    var i = 0;

    var nRegistros = (((selServicos.value == 272) && (chkAtivo.checked)) ? 1 : sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value'));
    var sEstados = '';
    var nProprietario = (selProprietario.value == 0 ? '' : selProprietario.value);
    //var nClassificacaoID = (selClassificacao.value == 0 ? '' : selClassificacao.value);
    var sClassificacao = '';

    for (i = 0; i < selClassificacao.length; i++) {
        if (selClassificacao.options[i].selected == true && selClassificacao.options[i].value != "") {
            sClassificacao += '/' + selClassificacao.options[i].value;
        }
    }

    sClassificacao = ((sClassificacao == '' || sClassificacao == '/0') ? '' : sClassificacao + '/');

    var sPesquisa = trimStr(txtPesquisa.value);
    var nTipoResultado = (selServicos.value == 272 ? 3 : null);
    var nTipoAtendimento = (chkAtivo.checked ? 281 : 282);

    for (i = 0; i < selEstados.length; i++) {
        if (selEstados.options[i].selected == true)
            sEstados += '/' + selEstados.options[i].value;
    }
    sEstados = (sEstados == '' ? '' : sEstados + '/');

    var sData = putDateInMMDDYYYY(selData.options[selData.selectedIndex].innerText);

    var bAtender1 = chkAtender1.checked;
    var bAtender2 = chkAtender2.checked;
    var bCredito = chkCredito.checked;

    if ((glb_nState == 1)) {
        var bAtender1 = false;
        var bCredito = false;
    }

    var nEmailID = ((selOferta.value > 0) ? selOferta.value.toString() : '');

    strPars += '?EmpresaID=' + escape(glb_aEmpresaData[0]);
    strPars += '&RegistroID=' + escape(nRegistros);
    strPars += '&sEstados=' + escape(sEstados);
    strPars += '&nProprietario=' + escape(nProprietario);
    strPars += '&sClassificacao=' + escape(sClassificacao);
    strPars += '&sPesquisa=' + escape(sPesquisa);
    strPars += '&nTipoResultado=' + escape(nTipoResultado);
    strPars += '&nTipoAtendimento=' + escape(nTipoAtendimento);
    strPars += '&ndtData=' + escape('');
    strPars += '&nPessoaGravacaoID=' + escape(nPessoaGravacaoID == null ? '' : nPessoaGravacaoID);
    strPars += '&nUsuarioID=' + escape(glb_nUserID);
    strPars += '&sData=' + escape(sData);
    strPars += '&bAtender1=' + escape(bAtender1);
    strPars += '&bAtender2=' + escape(bAtender2);
    strPars += '&bCredito=' + escape(bCredito);
    strPars += '&nEmailID=' + escape(nEmailID);

    dsoGrid.URL = SYS_ASPURLROOT + '/serversidegenEx/listaragendamento.aspx' + strPars;
    dsoGrid.ondatasetcomplete = fillGridData_DSC;
    dsoGrid.Refresh();
}

/********************************************************************
Retorno do servidor de solicitar dados do grid
********************************************************************/
function fillGridData_DSC()
{
    if (((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF)) || (dsoGrid.recordset.fields.Count == 1))
    {
        fg.Rows = 1;

        if (chkAtivo.checked)
        {
            if (window.top.overflyGen.Alert("N�o h� cliente a ser atendido em modo Ativo") == 0)
                return null;
        }

        lockControlsInModalWin(false);
        return;
    }
    else if (dsoGrid.recordset['Atender'].value != 1)
    {
        fg.Rows = 1;

        if (chkAtivo.checked)
        {
            var _retMsg = window.top.overflyGen.Confirm('Este cliente n�o est� habilitado para atendimento em modo ativo. Deseja atender assim mesmo?', 1);

            if ((_retMsg == 0) || (_retMsg == 2))
            {
                lockControlsInModalWin(false);

                chkAtivo.checked = false;
                selServicos_onChange();

                return;
            }
        }
    }

    window.focus();

    var dTFormat = '';
    var sTexto = '';
    var nServico = (selServicos.value == 272 ? 3 : null);
    var aHidenCols;
    var nEmpresaData = getCurrEmpresaData();
    var nPaisID = nEmpresaData[1];


    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm';

    aHidenCols = new Array(1, 7, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41);

    if (chkAtivo.checked)
        aHidenCols = new Array(12, 13, 14, 15, 17, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41);

    var i;

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '8';
    fg.FrozenCols = 0;
    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['Vendedor',                 // 0
                    'Tipo',                     // 1
                    'Cliente',                  // 2
                    'ID',                       // 3
                    'E',                        // 4
                    'Codigo CRM',               // 5
                    'Classifica��o',            // 6
                    'Cargo',                    // 7
                    'Agendamento',              // 8
                    'R',                        // 9
                    'F',                        // 10
                    'V',                        // 11
                    'R*',                       // 12
                    'F*',                       // 13
                    'V*',                       // 14
                    'Meta Vendas',              // 15
                    'Saldo Vendas',             // 16
                    'Limite Cr�dito',           // 17
                    'Saldo Cr�dito',            // 18
                    'Aprovador',                // 19
                    'Aprova��o',                // 20
                    'OK',                       // 21
                    'TipoPessoaID',             // 22
                    'TipoID',                   // 23
                    'ProprietarioID',           // 24
                    'Propriet�rio',             // 25
                    'Observacao',               // 26
                    'DDIComercial',             // 27
				    'DDDComercial',             // 28
				    'NumeroComercial',          // 29
				    'DDDFaxCelular',            // 30
				    'NumeroFaxCelular',         // 31
				    'Email',                    // 32
				    'Site',                     // 33
                    'ClassificacaoInterna',     // 34
                    'ClassificacaoExterna',     // 35
                    'Atender',                  // 36
                    'ClienteDesde',             // 37
                    'RecenciaAnterior',         // 38
                    'FrequenciaAnterior',       // 39
                    'ValorAnterior',            // 40
                    'CreditoVencimento',        // 41
                    'PesRFVID'], aHidenCols);   // 42

    fillGridMask(fg, dsoGrid, ['Vendedor*',                     // 0 
                               'Tipo*',                         // 1
                               'Fantasia*',                     // 2 
   				               'PessoaID*',                     // 3 
                               'Estado*',                       // 4
                               'CodigoCRM*',                    // 5 
                               glb_sColunaClassificacao,        // 6 
                               'Cargo*',                        // 7 
                               'dtAgendamento*',                // 8
                               'Recencia*',                     // 9
                               'Frequencia*',                   // 10
                               'Valor*',                        // 11
                               'RecenciaMeta*',                 // 12
                               'FrequenciaMeta*',               // 13
                               'ValorMeta*',                    // 14
                               'VendasMeta*',                   // 15
                               'SaldoVendas*',                  // 16
                               'LimiteCredito*',                // 17
                               'SaldoCredito*',                 // 18
                               'Aprovador*',                    // 19
                               'OK',                            // 20
                               'ModificacaoOK',                 // 21
   				               'TipoPessoaID*',                 // 22
   				               'TipoID*',                       // 23
                               'ProprietarioID*',               // 24
                               'Proprietario*',                 // 25
				               'Observacao*',                   // 26
                               'DDIComercial',                  // 27
				               'DDDComercial',                  // 28
				               'NumeroComercial',               // 29
				               'DDDFaxCelular',                 // 30
				               'NumeroFaxCelular',              // 31
				               'Email',                         // 32
				               'Site',                          // 33
                               'ClassificacaoInterna*',         // 34
                               'ClassificacaoExterna*',         // 35
                               'Atender*',                      // 36
                               'ClienteDesde',                  // 37
                               'RecenciaAnterior*',             // 38
                               'FrequenciaAnterior*',           // 39
                               'ValorAnterior*',                // 40
                               'CreditoVencimento',             // 41
                               'PesRFVID*'],                    // 42
					          ['', '', '', '', '', '', '', '', '99/99/9999 99:99', '', '', '', '', '', '', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', ''],
                              ['', '', '', '', '', '', '', '', dTFormat, '', '', '', '', '', '', '###,###,###.##', '###,###,###.##', '###,###,###.##', '###,###,###.##', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', '']);

    //Linha de Totais
    gridHasTotalLine(fg, '', 0xC0C0C0, null, true,
        [[getColIndexByColKey(fg, 'Fantasia*'), '######', 'C'],
        [getColIndexByColKey(fg, 'Recencia*'), '#.#', 'M'],
        [getColIndexByColKey(fg, 'Frequencia*'), '#.#', 'M'],
        [getColIndexByColKey(fg, 'Valor*'), '#.#', 'M'],
        [getColIndexByColKey(fg, 'RecenciaMeta*'), '#.#', 'M'],
        [getColIndexByColKey(fg, 'FrequenciaMeta*'), '#.#', 'M'],
        [getColIndexByColKey(fg, 'ValorMeta*'), '#.#', 'M'],
        [getColIndexByColKey(fg, 'VendasMeta*'), '###,###,###.##', 'S'],
        [getColIndexByColKey(fg, 'SaldoVendas*'), '###,###,###.##', 'S'],
        [getColIndexByColKey(fg, 'LimiteCredito*'), '###,###,###.##', 'S'],
        [getColIndexByColKey(fg, 'SaldoCredito*'), '###,###,###.##', 'S']]);

    glb_aCelHint = [[0, 1, 'P-Cliente, C-Contato'],
                    [0, 4, 'Estado'],
                    [0, 5, 'C�digo do cliente no sistema CRM'],
                    [0, 6, 'Classifica��o do cliente'],
                    [0, 7, 'Cargo do Contato'],
                    [0, 8, 'Data de agendamento do pr�ximo cliente'],
                    [0, 9, 'Rec�ncia. �ltima compra do cliente'],
                    [0, 10, 'Frequ�ncia. Frequ�ncia de compra do cliente'],
                    [0, 11, 'Valor. Volume de compras do cliente'],
                    [0, 12, 'Rec�ncia Meta para o per�odo (semana)'],
                    [0, 13, 'Frequ�ncia Meta para o per�odo (semana)'],
                    [0, 14, 'Valor Meta para o per�odo (semana)'],
                    [0, 15, 'Meta de vendas para o per�odo (semana) ' + (nPaisID == 130 ? '(R$)' : '(US$)')],
                    [0, 16, 'Quanto falta para atingir a meta de vendas no pr�odo (semana) ' + (nPaisID == 130 ? '(R$)' : '(US$)')],
                    [0, 17, 'Limite de cr�dito estabelecido para o cliente ' + (nPaisID == 130 ? '(R$)' : '(US$)')],
                    [0, 18, 'Limite de cr�dito do cliente dispon�vel para uso ' + (nPaisID == 130 ? '(R$)' : '(US$)')],
                    [0, 19, 'Colaborador que aprovou planejamento de atendimento do cliente'],
                    [0, 20, 'Planejamento de atendimento do cliente aprovado'],
                    [0, 21, 'Dado pendente de grava��o']];

    // Pinta celula
    if (fg.Rows > 1)
    {
        var i;
        var nColAprovacao = getColIndexByColKey(fg, 'OK');
        var nColRecenciaMeta = getColIndexByColKey(fg, 'RecenciaMeta*');
        var nColFrequenciaMeta = getColIndexByColKey(fg, 'FrequenciaMeta*');
        var nColValorMeta = getColIndexByColKey(fg, 'ValorMeta*');
        var nColVendasMeta = getColIndexByColKey(fg, 'VendasMeta*');
        var nColRecencia = getColIndexByColKey(fg, 'Recencia*');
        var nColFrequencia = getColIndexByColKey(fg, 'Frequencia*');
        var nColValor = getColIndexByColKey(fg, 'Valor*');
        var nColLimiteCredito = getColIndexByColKey(fg, 'LimiteCredito*');
        var nPrimeiraColuna = getColIndexByColKey(fg, 'Vendedor*');
        var nUltimaColuna = getColIndexByColKey(fg, 'ClassificacaoID*');
        var nBranco = 0xFFFFFF;
        var nVerdeClaro = 0xBCD9B0; 
        var nVerdeEscuro = 0x5D9B76; // 358254 (excel escuro)
        var nCinzaClaro = 0xD7D7D7;
        var nCinzaEscuro = 0xC0C0C0;
        var nAmarelo = 0xA3F3EB;
        var nVerdeFundo = 0xBCD9B0; // 0xBADA8C
        var nVerdeFonte = 0x008200;
        var nVermelhoFundo = 0xB3B3FF;
        var nVermelhoFonte = 0x0000FF;

        //var nVerdeteste = 0xB3FF95;

        /*
           Cores do grid s�o em BRG. Para converter, pegue a cor em RGB e inverta os valores, exemplo:
           Cor amarela em RGB: #FFFF00
           Cor amarela em BRG: #00FFFF
           E para uso no grid, substitua o "#" por "0x"
        */

        for (i = 1; i < fg.Rows; i++)
        {
            var bAtender = (getCellValueByColKey(fg, 'Atender*', i) == 1);
            var bCredito = (parseFloat(getCellValueByColKey(fg, 'SaldoCredito*', i).replace(",", ".")) > 0);

            // 1-Credito Vencido, 2-Credito vence esta semana, 3-Credito ira vencer no futuro
            var nCreditoVencimento = getCellValueByColKey(fg, 'CreditoVencimento', i);

            if (i >= 2)
            {
                if ((bCredito) && (bAtender))
                    fg.Cell(6, i, nPrimeiraColuna, i, nUltimaColuna) = nVerdeClaro;
                else if ((bCredito) && (!bAtender))
                    fg.Cell(6, i, nPrimeiraColuna, i, nUltimaColuna) = nVerdeEscuro;
                else if ((!bCredito) && (bAtender))
                    fg.Cell(6, i, nPrimeiraColuna, i, nUltimaColuna) = nCinzaClaro;
                else
                    fg.Cell(6, i, nPrimeiraColuna, i, nUltimaColuna) = nCinzaEscuro;

                // Credito Vencido
                if (nCreditoVencimento == 1)
                    fg.Cell(6, i, nColLimiteCredito, i, nColLimiteCredito) = nVermelhoFundo;
                // Credito vence esta semana
                else if (nCreditoVencimento == 2)
                    fg.Cell(6, i, nColLimiteCredito, i, nColLimiteCredito) = nAmarelo;

                // Celulas edit�veis de branco
                if (fg.ValueMatrix(i, nColAprovacao) == 0) {
                    fg.Cell(6, i, nColRecenciaMeta, i, nColRecenciaMeta) = nBranco; //0XDCDCDC
                    fg.Cell(6, i, nColFrequenciaMeta, i, nColFrequenciaMeta) = nBranco;
                    fg.Cell(6, i, nColValorMeta, i, nColValorMeta) = nBranco;
                    fg.Cell(6, i, nColVendasMeta, i, nColVendasMeta) = nBranco;
                }

                // Primeira celula
                if (selProprietario.value > 0)
                    fg.Cell(6, i, 0, i, 0) = nCinzaEscuro;
                else
                    fg.Cell(6, i, 0, i, 0) = nBranco;
            }

            // Colunas RFV
            var nRecencia = parseFloat(getCellValueByColKey(fg, 'Recencia*', i).replace(",", "."));
            var nFrequencia = parseFloat(getCellValueByColKey(fg, 'Frequencia*', i).replace(",", "."));
            var nValor = parseFloat(getCellValueByColKey(fg, 'Valor*', i).replace(",", "."));
            var nRecenciaAnterior = parseFloat(getCellValueByColKey(fg, 'RecenciaAnterior*', i).replace(",", "."));
            var nFrequenciaAnterior = parseFloat(getCellValueByColKey(fg, 'FrequenciaAnterior*', i).replace(",", "."));
            var nValorAnterior = parseFloat(getCellValueByColKey(fg, 'ValorAnterior*', i).replace(",", "."));
            var nRecenciaMeta = parseFloat(getCellValueByColKey(fg, 'RecenciaMeta*', i).replace(",", "."));
            var nFrequenciaMeta = parseFloat(getCellValueByColKey(fg, 'FrequenciaMeta*', i).replace(",", "."));
            var nValorMeta = parseFloat(getCellValueByColKey(fg, 'ValorMeta*', i).replace(",", "."));

            //Verde claro	Menor que o RFV obtido na semana anterior
            //Cinza	        Igual ao RFV obtido na semana anterior
            //Rosa	        Maior que o RFV obtido na semana anterior

            if (i >= 2) {
                // Rec�ncia - Cor de fundo
                if (nRecencia < nRecenciaAnterior)
                    fg.Cell(6, i, nColRecencia, i, nColRecencia) = nVerdeFundo;
                else if (nRecencia > nRecenciaAnterior)
                    fg.Cell(6, i, nColRecencia, i, nColRecencia) = nVermelhoFundo;

                // Frequ�ncia - Cor de fundo
                if (nFrequencia < nFrequenciaAnterior)
                    fg.Cell(6, i, nColFrequencia, i, nColFrequencia) = nVerdeFundo;
                else if (nFrequencia > nFrequenciaAnterior)
                    fg.Cell(6, i, nColFrequencia, i, nColFrequencia) = nVermelhoFundo;

                // Valor - Cor de fundo
                if (nValor < nValorAnterior)
                    fg.Cell(6, i, nColValor, i, nColValor) = nVerdeFundo;
                else if (nValor > nValorAnterior)
                    fg.Cell(6, i, nColValor, i, nColValor) = nVermelhoFundo;
            }

            //Verde escuro	Menor que o RFV meta do m�s	
            //Preto	        Igual ao RFV meta do m�s	
            //Vermelho	    Maior que o RFV meta do m�s	

            // Rec�ncia - Cor de fonte
            if (nRecencia < nRecenciaMeta) {
                fg.Select(i, nColRecencia, i, nColRecencia);
                fg.FillStyle = 1;
                fg.CellForeColor = nVerdeFonte;
            }
            else if (nRecencia > nRecenciaMeta) {
                fg.Select(i, nColRecencia, i, nColRecencia);
                fg.FillStyle = 1;
                fg.CellForeColor = nVermelhoFonte;
            }

            // Frequ�ncia - Cor de fonte
            if (nFrequencia < nFrequenciaMeta) {
                fg.Select(i, nColFrequencia, i, nColFrequencia);
                fg.FillStyle = 1;
                fg.CellForeColor = nVerdeFonte;
            }
            else if (nFrequencia > nFrequenciaMeta) {
                fg.Select(i, nColFrequencia, i, nColFrequencia);
                fg.FillStyle = 1;
                fg.CellForeColor = nVermelhoFonte;
            }

            // Valor - Cor de fonte
            if (nValor < nValorMeta) {
                fg.Select(i, nColValor, i, nColValor);
                fg.FillStyle = 1;
                fg.CellForeColor = nVerdeFonte;
            }
            else if (nValor > nValorMeta) {
                fg.Select(i, nColValor, i, nColValor);
                fg.FillStyle = 1;
                fg.CellForeColor = nVermelhoFonte;
            }

            if (i >= 2) {
                // Coloca negrito nas colunas RFV
                fg.Select(i, nColRecencia, i, nColValor);
                fg.FillStyle = 1;
                fg.CellFontBold = true;

                // Faz hint dos campos RFV com o RFV Anterior
                glb_aCelHint.push([i, nColRecencia, 'Rec�ncia anterior: ' + nRecenciaAnterior.toString()],
                                  [i, nColFrequencia, 'Frequ�ncia anterior: ' + nFrequenciaAnterior.toString()],
                                  [i, nColValor, 'Valor anterior: ' + nValorAnterior.toString()]);
            }
        }
    }

    // Colunas fixas
    fg.FrozenCols = 4;

    // Merge das celulas
    fg.MergeCells = 4;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;

    fg.Redraw = 0;

    alignColsInGrid(fg, [3, 5, 15, 16, 17, 18], [1, 4, 9, 10, 11, 12, 13, 14]);

    // Combo de Classifica��o
    insertcomboData(fg, getColIndexByColKey(fg, glb_sColunaClassificacao), dsoClassificacao, '*Classificacao|Observacoes', 'ClassificacaoID');

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    fg.ExplorerBar = 5;

    sTexto = 'Atendimento: ' + (fg.Rows - 2) + ' ';

    if (fg.Rows <= 3)
        sTexto += 'cliente';
    else {
        if (chkAtivo.checked)
            sTexto += 'clientes/contatos';
        else
            sTexto += 'clientes';
    }

    if ((glb_dtAtendimentoInicio != null) && (glb_dtAtendimentoFim != null))
        sTexto = sTexto + '  [' + tempoDecorrido(glb_dtAtendimentoInicio, glb_dtAtendimentoFim) + ']';

    //secText(sTexto, 1);

    lockControlsInModalWin(false);

    fg.Editable = true;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fg.Redraw = 2;

    setBtnState();
    valueBtnAgendar(true);

    js_fg_modalatendimentoEnterCell(fg);

    controlsFoneReadOnly();

    if (glb_nState == 1) {
        btnFinalizar.disabled = false;
    }

    if ((fg.Rows > 1) && (glb_nState == 1)) {
        var sParceiro = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Fantasia*'));

        secText('CRM (beta) - ' + sParceiro, 1);
        fg.Row = 2;
    }
    else
        secText('CRM (beta)', 1);

    if (fg.Rows > 1) {
        fg.focus();
        fg.col = 2;
        fg.row = 2;
    }

    if (glb_nState != 1)
        btnFinalizar.disabled = true;

    var bCarregaModoFinalizar = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bCarregaModoFinalizar');

    if (bCarregaModoFinalizar) {
        atender();

        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bCarregaModoFinalizar = false');
    }

    if ((glb_nEmaClienteID != null) && (fg.Rows > 1)) {
        var nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));

        fillGridData5(nPessoaID);
    }

    // Se listagem veio do Ativo com dblClick, lista e coloca cursor no cliente que estava selecionado antes. 
    if (glb_nPessoaFocoID != null) {
        for (i = 2; i < fg.Rows; i++) {
            if (glb_nPessoaFocoID == getCellValueByColKey(fg, 'PessoaID*', i)) {
                fg.row = i;
                fg.TopRow = i;
            }
        }

        glb_nPessoaFocoID = null;
    }
}

function atender() {
    var sAlert = '';
    var sdtAgendamento = '';
    var nPessoaID = 0;
    var sClassificacaoInterna = '';
    var sClassificacaoExterna = '';
    var nProprietarioid = null;

    if (fg.Row > 0)
        nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));

    if (nPessoaID <= 0)
        sAlert = 'Selecione um cliente no grid.';
    else if (selData.value > 0)
        sAlert = 'Para iniciar atendimento, selecionar a data mais recente.';

    if (btnIniciarAtendimento.state == 2) {
        setbtnIniciarAtendimentoStatus(1);
        glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);
        btnIniciarAtendimento.disabled = false;
    }

    // Finalizar atendimento
    if (btnIniciarAtendimento.state == 1) {
        limpaCamposFinalizacaoAtendimento();
        setbtnIniciarAtendimentoStatus(2);
        visibilityControls();
        btnIniciarAtendimento.disabled = true;

        if (glb_nState == 1) {
            btnFinalizar.disabled = true;
            //btnIniciarAtendimento.disabled = true;

            verificaAtendimentosAnteriores(nPessoaID);
        }

        return null;
    }

    if (sAlert.length > 0) {
        if (window.top.overflyGen.Alert(sAlert) == 0)
            return null;
        return null;
    }

    lblFinalizarAtendimento.innerHTML = 'Finalizar atendimento'.bold();

    var sObservacao = trimStr(txtObservacoes.value);
    var nAtendimento = (chkAtivo.checked ? 281 : 282);

    lockControlsInModalWin(true);

    nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));

    sClassificacaoInterna = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ClassificacaoInterna*'));
    sClassificacaoExterna = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ClassificacaoExterna*'));

    sClassificacaoInterna = (sClassificacaoInterna == '' ? 'NULL' : '\'' + sClassificacaoInterna + '\'');
    sClassificacaoExterna = (sClassificacaoExterna == '' ? 'NULL' : '\'' + sClassificacaoExterna + '\'');

    var selParceiroID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selParceiroID');

    var bEhCliente = false;

    for (i = 0; i < selParceiroID.length; i++) {
        if (selParceiroID.options[i].value == nPessoaID) {
            bEhCliente = true;
        }
    }

    if (!bEhCliente) {
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_bEhCliente = false');
    }

    strPars = '?nPessoaID=' + escape(nPessoaID);
    strPars += '&nStatus=' + escape(btnIniciarAtendimento.state == 0 ? '0' : '1');
    strPars += '&nEmpresaID=' + escape(glb_aEmpresaData[0]);
    strPars += '&nUserID=' + escape(glb_nUserID);
    strPars += '&nServicoAtendimentoID=' + escape(272);
    strPars += '&nTipoAtendimentoID=' + escape(nAtendimento);

    nProprietarioid = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ProprietarioID*'));

    if (nProprietarioid == glb_nUserID)
        glb_DsoToReturn = 1;
    else
        glb_DsoToReturn = 2;

    try {
        dsoGravarAgendamento.URL = SYS_ASPURLROOT + '/serversidegenEx/gravaragendamento.aspx' + strPars;
        dsoGravarAgendamento.ondatasetcomplete = atender_DSC;
        dsoGravarAgendamento.Refresh();

        // Verifica se cliente n�o consta no combo de parceiros do pesqlist
        var selParceiroID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selParceiroID');

        var bClienteCombo = false;

        for (i = 0; i < selParceiroID.length; i++) {
            if (selParceiroID.options[i].value == nPessoaID) {
                bClienteCombo = true;
            }
        }

        setConnection(dsoDetalhesCliente2);

        if ((nProprietarioid != glb_nUserID) || (bClienteCombo == false)) {
            dsoDetalhesCliente2.SQL =
                  'SELECT DISTINCT a.Nome AS fldName, a.PessoaID AS fldID, a.Fantasia + ISNULL(SPACE(1) + \'(\' + dbo.fn_Pessoa_Documento(a.PessoaID, NULL, NULL, 101, 111, 0), SPACE(0)) + \')\' AS Fantasia, ' +
	                                        'b.ListaPreco AS ListaPreco, c.FinanciamentoID, 0 AS EmpresaSistema, ' +
	                                        'dbo.fn_Financiamento_PMP(c.FinanciamentoID,NULL,NULL,NULL) AS PMP, c.NumeroParcelas AS NumeroParcelas, ' +
			                                'dbo.fn_RelacoesPessoas_FinanciamentoPadrao(b.SujeitoID, b.ObjetoID, 1) AS FinanciamentoPadrao, ' +
			                                'ISNULL(' + sClassificacaoInterna + ', SPACE(0)) AS ClassificacaoInterna, ' +
			                                'ISNULL(' + sClassificacaoExterna + ', SPACE(0)) AS ClassificacaoExterna ' +
                        'FROM Pessoas a WITH(NOLOCK) ' +
                            'INNER JOIN Pessoas_Creditos aa WITH(NOLOCK) ON aa.PessoaID = a.PessoaID ' +
                            'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON a.PessoaID = b.SujeitoID ' +
		                    'INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON (ISNULL(aa.FinanciamentoLimiteID, 2) = c.FinanciamentoID) ' +
                        'WHERE a.PessoaID = ' + nPessoaID + ' ' + 'AND b.ObjetoID = ' + glb_aEmpresaData[0] + ' AND aa.PaisID = ' + glb_aEmpresaData[1];

            dsoDetalhesCliente2.ondatasetcomplete = atender_DSC;
            dsoDetalhesCliente2.refresh();
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('N�o foi poss�vel gravar o atendimento, tente novamente.') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function atender_DSC() {
    glb_DsoToReturn--;

    if (glb_DsoToReturn > 0)
        return null;

    var aRetornarPesqList = new Array();

    glb_nPesAtendimentoID = dsoGravarAgendamento.recordset['PesAtendimentoID'].value;

    if (!((dsoDetalhesCliente2.recordset.BOF) || (dsoDetalhesCliente2.recordset.EOF))) {
        aRetornarPesqList[aRetornarPesqList.length] = 1;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['Fantasia'].value;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['fldID'].value;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['ListaPreco'].value;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['EmpresaSistema'].value;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['PMP'].value;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['NumeroParcelas'].value;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['FinanciamentoPadrao'].value;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['ClassificacaoInterna'].value;
        aRetornarPesqList[aRetornarPesqList.length] = dsoDetalhesCliente2.recordset['ClassificacaoExterna'].value;
        aRetornarPesqList[aRetornarPesqList.length] = glb_nPesAtendimentoID;
    }
    else {
        aRetornarPesqList[aRetornarPesqList.length] = 2;
        aRetornarPesqList[aRetornarPesqList.length] = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));
        aRetornarPesqList[aRetornarPesqList.length] = glb_nPesAtendimentoID;
    }

    if ((glb_nPesAtendimentoID == 0) || (glb_nPesAtendimentoID == null)) {
        try {
            lockControlsInModalWin(false);

            // Altera o status de Iniciar para finalizar
            setbtnIniciarAtendimentoStatus(1);
            visibilityControls();
        }
        catch (e) {
            if (window.top.overflyGen.Alert(e.message) == 0)
                return null;

            lockControlsInModalWin(false);
        }
    }
    else {
        salvaDadosSelecionados();
        //setTimerAgendamentoStatus(true);
        //sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'atender()');
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, aRetornarPesqList);
    }
}

// EVENTOS DE GRID **************************************************
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalatendimentoBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    ;
}
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_modalatendimentoEnterCell(grid) {
    if (glb_GridIsBuilding)
        return;

    if ((grid.Rows <= 0) || (grid.Row <= 0))
        return;

    var nTipoPessoaID = grid.ValueMatrix(grid.Row, getColIndexByColKey(grid, 'TipoPessoaID*'));
    var sTipoID = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'TipoID*'));
    var sEmail = '';
    var sSite = '';

    limparDetalhesCliente();

    // P ou C
    if ((sTipoID == 1) || (sTipoID == 4))
    {
        txtDDITelefone.disabled = false;
        txtDDDComercial.disabled = false;
        txtFoneComercial.disabled = false;
        txtDDDFaxCelular.disabled = false;
        txtFaxCelular.disabled = false;
        txtEmail.disabled = false;
        txtSite.disabled = false;
        lblFaxCelular.innerText = 'Celular';

        if (nTipoPessoaID == 52)
            lblFaxCelular.innerText = 'Fax';

        txtClienteDesde.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'ClienteDesde'));
        txtDDITelefone.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'DDIComercial'));
        txtDDDComercial.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'DDDComercial'));
        txtFoneComercial.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'NumeroComercial'));
        txtDDDFaxCelular.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'DDDFaxCelular'));
        txtFaxCelular.value = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'NumeroFaxCelular'));

        sEmail = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'Email'));
        sSite = grid.TextMatrix(grid.Row, getColIndexByColKey(grid, 'Site'));

        if (sEmail == '')
            txtEmail.value = '_@_.com.br';
        else
            txtEmail.value = sEmail;

        if (sSite == '')
            if (sTipoID == 1)
                txtSite.value = 'http:/' + '/www._.com.br';
            else
                txtSite.value = '';
        else
            txtSite.value = sSite;
    }
    else {
        txtClienteDesde.value = '';
        txtDDITelefone.value = '';
        txtDDDComercial.value = '';
        txtFoneComercial.value = '';
        txtDDDFaxCelular.value = '';
        txtFaxCelular.value = '';
        txtEmail.value = '';
        txtSite.value = '';

        txtClienteDesde.readOnly = true;
        txtDDITelefone.disabled = true;
        txtDDDComercial.disabled = true;
        txtFoneComercial.disabled = true;
        txtDDDFaxCelular.disabled = true;
        txtFaxCelular.disabled = true;
        txtEmail.disabled = true;
        txtSite.disabled = true;
    }

    setLinkFoneUrl(1);
}
/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalatendimentoKeyPress(KeyAscii) {
    if (KeyAscii == 13) {
        _retMsg = window.top.overflyGen.Confirm("Deseja iniciar um atendimento?", 1);

        if ((_retMsg == 0) || (_retMsg == 2))
            return null;
        else
            btn_onclick(btnIniciarAtendimento);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalatendimento_BeforeEdit(grid, row, col) {
    if (glb_GridIsBuilding)
        return;

    var fldDet = get_FieldDetails(dsoGrid, grid.ColKey(col));

    // limita qtd de caracteres digitados em uma celula do grid    
    if (fldDet == null)   // erro, permite digitar cerca 32 k caracteres
        grid.EditMaxLength = 0;
    else if ((fldDet[0] == 200) || (fldDet[0] == 129))  // varchar ou char        
        grid.EditMaxLength = fldDet[1];
    else if (fldDet[0] == 3) // int
    {
        if ((col == getColIndexByColKey(grid, 'RecenciaMeta*')) || (col == getColIndexByColKey(grid, 'FrequenciaMeta*')) || (col == getColIndexByColKey(grid, 'ValorMeta*')))
            grid.EditMaxLength = 1;
        else
            grid.EditMaxLength = fldDet[1];
    }
    else
        grid.EditMaxLength = 0;
}


/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalatendimento_ValidateEdit(grid, row, col) {
    if (glb_GridIsBuilding)
        return;
}

function fg_DblClick(grid, row, col) {

    // Preenche coluna OK ou Aprova��o com dblClick
    if ((fg.Row < 2) && ((col == getColIndexByColKey(fg, 'OK')) || (col == getColIndexByColKey(fg, 'ModificacaoOK')))) {
        var bModificacaoOK, bOK;

        if (fg.Rows > 2) {
            bOK = fg.TextMatrix(3, getColIndexByColKey(fg, 'OK'));
            bModificacaoOK = fg.TextMatrix(3, getColIndexByColKey(fg, 'ModificacaoOK'));
        }

        for (var i = 2; i < grid.Rows; i++) {
            if (col == getColIndexByColKey(fg, 'OK')) {
                if (bOK == 0)
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 1;
                else
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = 0;

                fg.TextMatrix(i, getColIndexByColKey(fg, 'ModificacaoOK')) = 1;
                glb_GravacaoPendente = true;
            }

            if (col == getColIndexByColKey(fg, 'ModificacaoOK')) {
                if (bModificacaoOK == 0)
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'ModificacaoOK')) = 1;
                else
                    fg.TextMatrix(i, getColIndexByColKey(fg, 'ModificacaoOK')) = 0;

                glb_GravacaoPendente = true;
            }
        }

        setBtnState();
    }

    // Qualquer outra coluna n�o faz nada no dblClick
    if (fg.Row < 2)
        return true;

    // Se dblClick foi na coluna Agendamento, gera agendamento para data atual
    if (col == getColIndexByColKey(fg, 'dtAgendamento*')) {

        var contador = 0;

        for (var i = 2; i < fg.Rows; i++) {
            if (getCellValueByColKey(fg, 'dtAgendamento*', i) != '') {
                contador++;
            }
        }

        // Se est� tirando o agendamento, reduz um do contador sen�o aumenta um
        if ((getCellValueByColKey(fg, 'dtAgendamento*', fg.row) != '') && (contador > 0))
            contador--;
        else
            contador++;

        if (contador > 15) {
            var _retMsg = window.top.overflyGen.Confirm('Limite de 15 agendamentos excedido.\nDeseja criar mais um agendamento assim mesmo?', 1);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
            else
                atenderEFinalizarAtendimento(true);
        }
        else
            atenderEFinalizarAtendimento(true);
    }

    // Se dblCLick foi na coluna Cliente, vai para atendimento ativo com a pessoa selecionada
    else if (col == getColIndexByColKey(fg, 'Fantasia*')) {
        // Se est� em moto Ativo, vai para Receptivo e lista
        if (chkAtivo.checked) {
            glb_nPessoaFocoID = getCellValueByColKey(fg, 'PessoaID*', 2);

            chkAtivo.checked = false;
            selServicos_onChange(nPessoaID);
            fillGridData();
        }
        // Se est� em moto Receptivo, vai para Ativo e lista
        else {
            var nPessoaID = getCellValueByColKey(fg, 'PessoaID*', fg.Row);

            chkAtivo.checked = true;
            selServicos_onChange(nPessoaID);
        }
    }

    // Carrega dados transacionais
    else if (chkDetalhes1.checked) {
        lockControlsInModalWin(true);

        var nPessoaID = getCellValueByColKey(fg, 'PessoaID*', fg.Row);
        var nEmpresaID = glb_aEmpresaData[0];
        var nEmpresaData = getCurrEmpresaData();

        setConnection(dsoDetalhesCliente);
        dsoDetalhesCliente.SQL = 'SELECT CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 11, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ')) AS Anos, ' +
                'dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 10, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ') AS PedidosFaturados, ' +
                'dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 1, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ') AS FaturamentoAcumulado, ' +
                'dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 2, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ') AS ContribuicaoAcumulada, ' +
                'dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 3, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ') AS ContribuicaoAcumuladaRecebida, ' +
                'CONVERT(NUMERIC(11), dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 9, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ')) AS MaiorAtraso, ' +
                'RIGHT(CONVERT(VARCHAR, dbo.fn_Pessoa_Data(' + nPessoaID + ', -' + nEmpresaID + ', NULL, 3, GETDATE()-10000, GETDATE()), 103), 7) AS dtMaiorCompra, ' +
                'dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 5, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ') AS ValorMaiorCompra, ' +
                'CONVERT(INT, dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 18, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ')) AS diasUltimaCompra, ' +
                'RIGHT(CONVERT(VARCHAR, dbo.fn_Pessoa_Data(' + nPessoaID + ', -' + nEmpresaID + ', NULL, 2, GETDATE()-10000, GETDATE()), 103), 7) AS dtUltimaCompra, ' +
                'dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 4, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ') AS ValorUltimaCompra, ' +
                'RIGHT(CONVERT(VARCHAR, dbo.fn_Pessoa_Data(' + nPessoaID + ', -' + nEmpresaID + ', NULL, 4, GETDATE()-10000, GETDATE()), 103), 7) AS dtMaiorAcumulo, ' +
                'dbo.fn_Pessoa_Totaliza(' + nPessoaID + ', -' + nEmpresaID + ', 6, GETDATE()-10000, GETDATE(), ' + (nEmpresaData[1] == 130 ? '0' : '1') + ') AS ValorMaiorAcumulo ';

        dsoDetalhesCliente.ondatasetcomplete = dsoDetalhesCliente_DSC;
        dsoDetalhesCliente.refresh();
    }

    // Ofertas do cliente e Historico de Pedidos
    else if (chkDetalhes2.checked) {
        fg3.Rows = 1;
        var nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));
        fillGridData3(nParceiroID);
        fillGridData5(nParceiroID);
    }
}

function dsoDetalhesCliente_DSC() {
    var nAnos = 0;
    var nPedidosFaturados = 0;
    var nFaturamentoAcumulado = '';
    var nContribuicaoAcumulada = '';
    var nContribuicaoAcumuladaRecebida = '';
    var nMaiorAtraso = 0;
    var sdtMaiorCompra = '';
    var nValorMaiorCompra = '';
    var ndiasUltimaCompra = 0;
    var sdtUltimaCompra = '';
    var nValorUltimaCompra = '';
    var sdtMaiorAcumulo = '';
    var nValorMaiorAcumulo = '';
    var sMilharSep = '.';
    var sCentSep = ',';

    if (!((dsoDetalhesCliente.recordset.BOF) || (dsoDetalhesCliente.recordset.EOF))) {
        if (DATE_SQL_PARAM == 101) {
            sMilharSep = ',';
            sCentSep = '.';
        }

        nAnos = dsoDetalhesCliente.recordset['Anos'].value;
        nPedidosFaturados = dsoDetalhesCliente.recordset['PedidosFaturados'].value;
        nFaturamentoAcumulado = dsoDetalhesCliente.recordset['FaturamentoAcumulado'].value;
        nContribuicaoAcumulada = dsoDetalhesCliente.recordset['ContribuicaoAcumulada'].value;
        nContribuicaoAcumuladaRecebida = dsoDetalhesCliente.recordset['ContribuicaoAcumuladaRecebida'].value;
        nMaiorAtraso = dsoDetalhesCliente.recordset['MaiorAtraso'].value;
        sdtMaiorCompra = dsoDetalhesCliente.recordset['dtMaiorCompra'].value;
        nValorMaiorCompra = dsoDetalhesCliente.recordset['ValorMaiorCompra'].value;
        ndiasUltimaCompra = dsoDetalhesCliente.recordset['diasUltimaCompra'].value;
        sdtUltimaCompra = dsoDetalhesCliente.recordset['dtUltimaCompra'].value;
        nValorUltimaCompra = dsoDetalhesCliente.recordset['ValorUltimaCompra'].value;
        sdtMaiorAcumulo = dsoDetalhesCliente.recordset['dtMaiorAcumulo'].value;
        nValorMaiorAcumulo = dsoDetalhesCliente.recordset['ValorMaiorAcumulo'].value;

        txtAnos.value = (nAnos == null ? 0 : nAnos);
        txtPedidosFaturados.value = (nPedidosFaturados == null ? 0 : nPedidosFaturados);
        txtFaturamentoAcumulado.value = formatCurrencyValue(nFaturamentoAcumulado.toString(), sMilharSep, sCentSep);
        txtContribuicaoAcumulada.value = formatCurrencyValue(nContribuicaoAcumulada.toString(), sMilharSep, sCentSep);
        txtContribuicaoAcumuladaRecebida.value = formatCurrencyValue(nContribuicaoAcumuladaRecebida.toString(), sMilharSep, sCentSep);
        txtAtraso.value = (nMaiorAtraso == null ? 0 : nMaiorAtraso);
        txtMaiorCompra.value = (sdtMaiorCompra == null ? '' : sdtMaiorCompra + '  ') + formatCurrencyValue(nValorMaiorCompra.toString(), sMilharSep, sCentSep);
        txtDiasUltimaCompra.value = (ndiasUltimaCompra == null ? 0 : ndiasUltimaCompra);
        txtDataUltimaCompra.value = (sdtUltimaCompra == null ? '' : sdtUltimaCompra + '  ') + formatCurrencyValue(nValorUltimaCompra.toString(), sMilharSep, sCentSep);
        txtUltimaCompra.value = (sdtMaiorAcumulo == null ? '' : sdtMaiorAcumulo + '  ') + formatCurrencyValue(nValorMaiorAcumulo.toString(), sMilharSep, sCentSep);
    }

    lockControlsInModalWin(false);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalatendimento_AfterEdit(Row, Col) {
    if (glb_GridIsBuilding)
        return;

    var nData = null;
    var nData = null;

    if ((Row > 0) && (Col > 0) && (Col != getColIndexByColKey(fg, 'ModificacaoOK'))) {
        fg.TextMatrix(Row, getColIndexByColKey(fg, 'ModificacaoOK')) = 1;
        glb_GravacaoPendente = true;
        setBtnState();
    }
}

function js_fg2_modalatendimento_AfterEdit(Row, Col) {
    if (glb_GridIsBuilding)
        return;

    if (fg2.row > 0) {
        var nColOK = getColIndexByColKey(fg2, 'AtendimentoOK');

        if (nColOK == Col) {
            btnGravar.disabled = false;

            fg2.TextMatrix(fg2.Row, getColIndexByColKey(fg2, 'Gravar')) = 1;
        }
    }
}

/********************************************************************
Salva as linhas alteradas no grid
********************************************************************/
function saveDataInGrid() {
    var empresa = getCurrEmpresaData();
    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;
    var bCB = 0;
    var bCG = 0;
    var bES = 0;
    var sEmail = '';
    var sSite = '';
    var nRegistro = (((selServicos.value == 272) && (chkAtivo.checked)) ? 1 : sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value'));
    var nParceiroID = 0;

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;
    glb_GravacaoPendente = false;
    glb_nPessoaGravacaoID = 0;

    if (fg.Row > 0)
        nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));

    if (nParceiroID <= 0) {
        if (window.top.overflyGen.Alert('Selecione um cliente.') == 0)
            return null;
    }

    var bPlanejamentoIncompleto = false;

    for (var i = 2; i < fg.Rows; i++) {
        if ((getCellValueByColKey(fg, 'ModificacaoOK', i) != 0) && (getCellValueByColKey(fg, 'OK', i) != 0)
            && ((getCellValueByColKey(fg, 'RecenciaMeta*', i) == '')
                || (getCellValueByColKey(fg, 'FrequenciaMeta*', i) == '')
                || (getCellValueByColKey(fg, 'ValorMeta*', i) == '')
                || (getCellValueByColKey(fg, 'VendasMeta*', i) == ''))) {
            bPlanejamentoIncompleto = true;
            break;
        }
    }

    if (bPlanejamentoIncompleto) {
        if (window.top.overflyGen.Alert('Planejamento incompleto, preencha todos os dados para aprova��o.') == 0)
            return null;
        return null;
    }

    lockControlsInModalWin(true);

    // Carteira - Ativo
    if ((selServicos.value == 272) && (fg.Rows > 1) && (glb_nState == 1))
        glb_nPessoaGravacaoID = nParceiroID;

    for (i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'ModificacaoOK', i) != 0) {
            nBytesAcum = strPars.length;

            if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
                nBytesAcum = 0;
                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&DataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }

                strPars += '?nUsuarioID=' + escape(glb_nUserID);
            }

            strPars += '&nEmpresaid=' + escape(empresa[0]);
            strPars += '&nServico=' + escape(selServicos.value == 272 ? 3 : 0);
            strPars += '&nClassificacaoID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, glb_sColunaClassificacao)));
            strPars += '&nRegistroID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'PessoaID*')));
            strPars += '&nTipoID=' + escape(getCellValueByColKey(fg, 'TipoID*', i));
            strPars += '&nProprietarioID=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'ProprietarioID*')));
            strPars += '&sTipoPessoaID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'TipoPessoaID*')));
            strPars += '&sDDITelefone=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'DDIComercial')));
            strPars += '&sDDDComercial=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'DDDComercial')));
            strPars += '&sTelComercial=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'NumeroComercial')));
            strPars += '&sDDDFaxCelular=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'DDDFaxCelular')));
            strPars += '&sFaxCelular=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'NumeroFaxCelular')));
            strPars += '&nRecenciaMeta=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'RecenciaMeta*')));
            strPars += '&nFrequenciaMeta=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'FrequenciaMeta*')));
            strPars += '&nValorMeta=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'ValorMeta*')));
            strPars += '&nVendasMeta=' + escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'VendasMeta*')));
            strPars += '&nPesRFVID=' + escape(fg.TextMatrix(i, getColIndexByColKey(fg, 'PesRFVID*')));
            strPars += '&bAprovacao=' + escape((getCellValueByColKey(fg, 'OK', i) != 0) ? 'true' : 'false');

            //private String ValorCOFINS;
            //public String nValorCOFINS
            //{
            //    get { return ValorCOFINS; }
            //    set { ValorCOFINS = value; }
            //}

            sEmail = fg.TextMatrix(i, getColIndexByColKey(fg, 'Email'));
            sSite = fg.TextMatrix(i, getColIndexByColKey(fg, 'Site'));

            if ((sEmail == '_@_.com.br') ||
                (sEmail == '@_.com.br') ||
                (sEmail == '_@.com.br') ||
                (sEmail == '@.com.br'))
                strPars += '&sEmail=';
            else
                strPars += '&sEmail=' + escape(sEmail);

            if ((sSite == 'http:/' + '/www._.com.br') ||
                (sSite == 'http:/' + '/www_.com.br') ||
                (sSite == 'http:/' + '/www._com.br') ||
                (sSite == 'http:/' + '/www.com.br'))
                strPars += '&sSite=';
            else
                strPars += '&sSite=' + escape(sSite);

            nDataLen++;
        }
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&DataLen=' + escape(nDataLen);

    sendDataToServer();
}

function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            dsoGrava.URL = SYS_ASPURLROOT + '/serversidegenEx/gravaalteracaoatendimento.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
            dsoGrava.ondatasetcomplete = sendDataToServer_DSC;
            dsoGrava.refresh();
        }
        else {
            if (!((dsoGrava.recordset.BOF) || (dsoGrava.recordset.EOF))) {
                var erro;

                try {
                    erro = parseInt(dsoGrava.recordset['Resultado'].value);
                } catch (e) {
                    if ((dsoGrava.recordset['Resultado'].value != null) &&
                    (trimStr(dsoGrava.recordset['Resultado'].value) != '')) {
                        if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                            return null;
                    }
                }
            }

            lockControlsInModalWin(false);
            glb_OcorrenciasTimerInt = window.setInterval('fillGridData(' + glb_nPessoaGravacaoID.toString() + ')', 10, 'JavaScript');
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('fillGridData(' + glb_nPessoaGravacaoID.toString() + ')', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

function detalhalistaPrecos_DSC() {
    var empresa = getCurrEmpresaData();
    var aListaPrecoData = new Array();

    if (!(dsoListaPrecos.recordset.BOF && dsoListaPrecos.recordset.EOF)) {
        aListaPrecoData[0] = dsoListaPrecos.recordset['Fantasia'].value;
        aListaPrecoData[1] = dsoListaPrecos.recordset['PessoaID'].value;
        aListaPrecoData[2] = dsoListaPrecos.recordset['UFID'].value;
        aListaPrecoData[3] = dsoListaPrecos.recordset['EmpresaSistema'].value;
        aListaPrecoData[4] = dsoListaPrecos.recordset['ListaPreco'].value;
        aListaPrecoData[5] = dsoListaPrecos.recordset['PMP'].value;
        aListaPrecoData[6] = dsoListaPrecos.recordset['NumeroParcelas'].value;
        aListaPrecoData[7] = dsoListaPrecos.recordset['FinanciamentoPadrao'].value;

        // variavel global no sup para controlar quem mandou o carrier
        sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'glb_CarrierSendListaPrecoAtentimento = true');
        // Manda o id da pessoa a detalhar 
        sendJSCarrier(getHtmlId(), 'SETPESSOAPRICELIST', new Array(empresa[0], aListaPrecoData, glb_nPesAtendimentoID, glb_dtAtendimentoInicio, (chkAtivo.checked ? 281 : 282)));
    }

    lockInterface(false);
}

function incluiSeguro() {
    var sErro = '';
    var strPars = '';

    if (fg.Row < 1)
        sErro = 'Selecione uma linha do tipo R no grid para incluir um seguro de cr�dito';
    else if (getCellValueByColKey(fg, 'TipoID*', fg.Row) != 2)
        sErro = 'Selecione uma linha do tipo R no grid para incluir um seguro de cr�dito';
    else if (selSeguradora.selectedIndex <= 0)
        sErro = 'Selecione uma seguradora';

    if (sErro != '') {
        if (window.top.overflyGen.Alert(sErro) == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    var nRegistros = (((selServicos.value == 272) && (chkAtivo.checked)) ? 1 : sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value'));

    strPars = '?RelacaoID=' + escape(nRegistros);
    strPars += '&SeguradoraID=' + escape(selSeguradora.value);
    strPars += '&MarcaID=' + escape(selMarca.selectedIndex > 0 ? selMarca.value : 0);
    strPars += '&UsuarioID=' + escape(glb_nUserID);
    strPars += '&Excluir=' + escape(0);

    dsoGrava.URL = SYS_ASPURLROOT + '/serversidegenEx/criasegurocredito.aspx' + strPars;
    dsoGrava.ondatasetcomplete = segurocredito_DSC;
    dsoGrava.refresh();
}

function selReagendamento_onchange() {
    agendarHorario();
}

function agendarHorario() {
    var sMinutos = selReagendamento.value;

    if (sMinutos == '0') {
        txtAgendar.value = '';
        return null;
    }

    lockControlsInModalWin(true);

    setConnection(dsoAgendarHorario);

    dsoAgendarHorario.SQL = 'SELECT CONVERT(VARCHAR(10),(dbo.fn_Data_ProjetaHora(GETDATE(), 0, 0, ' + sMinutos + ', 0, ' + glb_aEmpresaData[0] + ')),' + DATE_SQL_PARAM + ') + \' \' + ' +
	                            'CONVERT(VARCHAR(5),(dbo.fn_Data_ProjetaHora(GETDATE(), 0, 0, ' + sMinutos + ', 0, ' + glb_aEmpresaData[0] + ')),108) AS Data';

    dsoAgendarHorario.ondatasetcomplete = agendarHorario_DSC;
    dsoAgendarHorario.Refresh();
}

function agendarHorario_DSC() {
    if (!((dsoAgendarHorario.recordset.BOF) || (dsoAgendarHorario.recordset.EOF))) {
        if ((dsoAgendarHorario.recordset['Data'].value != null) && (dsoAgendarHorario.recordset['Data'].value) != '')
            txtAgendar.value = dsoAgendarHorario.recordset['Data'].value;
    }

    lockControlsInModalWin(false);
}

function segurocredito_DSC() {
    if (!((dsoGrava.recordset.BOF) || (dsoGrava.recordset.EOF))) {
        if ((dsoGrava.recordset['Resultado'].value != null) &&
            (trimStr(dsoGrava.recordset['Resultado'].value) != '')) {
            if (window.top.overflyGen.Alert(dsoGrava.recordset['Resultado'].value) == 0)
                return null;
        }
    }

    glb_OcorrenciasTimerInt = window.setInterval('fillGridData()', 10, 'JavaScript');
}

function js_fg_modalatendimento_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    if (glb_GridIsBuilding)
        return;

    //Forca celula read-only    
    //if ( glb_FreezeRolColChangeEvents )
    //return true;

    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);

    fg.Editable = true;

    if (cellIsLocked(NewRow, NewCol)) {
        fg.Editable = false;
    }

    glb_validRow = NewRow;
    glb_validCol = NewCol;
}

function cellIsLocked(nRow, nCol) {
    var retVal = false;

    // Clientes
    if ((getCellValueByColKey(fg, 'OK', nRow) == 0) &&
             ((nCol == getColIndexByColKey(fg, 'RecenciaMeta*')) ||
             (nCol == getColIndexByColKey(fg, 'FrequenciaMeta*')) ||
             (nCol == getColIndexByColKey(fg, 'ValorMeta*')) ||
             (nCol == getColIndexByColKey(fg, 'VendasMeta*'))))
        retVal = false;
    else if (fg.ColKey(nCol).substr(fg.ColKey(nCol).length - 1, 1) == '*') {
        retVal = true;
    }
        // RelPessoas
    else if (getCellValueByColKey(fg, 'TipoID*', nRow) == 2) {
        if ((nCol == getColIndexByColKey(fg, glb_sColunaClassificacao)) ||
             (nCol == getColIndexByColKey(fg, 'FaturamentoAnual')) ||
             (nCol == getColIndexByColKey(fg, 'NumeroFuncionarios')) ||
             (nCol == getColIndexByColKey(fg, 'PotencialCredito')))
            retVal = true;
    }
        // Seguro Credito
    else if (getCellValueByColKey(fg, 'TipoID*', nRow) == 3) {
        if ((nCol == getColIndexByColKey(fg, glb_sColunaClassificacao)) ||
             (nCol == getColIndexByColKey(fg, 'FaturamentoAnual')) ||
             (nCol == getColIndexByColKey(fg, 'NumeroFuncionarios')) ||
             (nCol == getColIndexByColKey(fg, 'PotencialCredito')))
            retVal = true;
        else if ((glb_nA1 == 0) || (glb_nA2 == 0))
            retVal = true;
    }
        // Contatos
    else if (getCellValueByColKey(fg, 'TipoID*', nRow) == 4) {
        if ((nCol == getColIndexByColKey(fg, glb_sColunaClassificacao)) ||
             (nCol == getColIndexByColKey(fg, 'FaturamentoAnual')) ||
             (nCol == getColIndexByColKey(fg, 'NumeroFuncionarios')) ||
             (nCol == getColIndexByColKey(fg, 'PotencialCredito')))
            retVal = true;
    }

    return retVal;
}

function excluiSeguro() {
    if (fg.Row < 1) {
        lockControlsInModalWin(false);
        return null;
    }
    else if (getCellValueByColKey(fg, 'TipoID*', fg.Row) != 3) {
        if (window.top.overflyGen.Alert('S� � permitido excluir linhas to tipo "S".') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }

    var _retMsg = window.top.overflyGen.Confirm('Deseja realmente excluir este seguro ?');
    var nRegistros = (((selServicos.value == 272) && (chkAtivo.checked)) ? 1 : sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value'));

    if (_retMsg == 1) {
        lockControlsInModalWin(true);

        strPars = '?RelacaoID=' + escape(nRegistros);
        strPars += '&SeguradoraID=' + escape(0);
        strPars += '&MarcaID=' + escape(0);
        strPars += '&UsuarioID=' + escape(glb_nUserID);
        strPars += '&Excluir=' + escape(1);

        dsoGrava.URL = SYS_ASPURLROOT + '/serversidegenEx/criasegurocredito.aspx' + strPars;
        dsoGrava.ondatasetcomplete = segurocredito_DSC;
        dsoGrava.refresh();
    }
}

function js_fg_modalatendimentoAfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return;

    setBtnState();

    if ((chkDetalhes2.checked) && (OldRow != NewRow)) {
        fg3.Rows = 1;
        fg5.Rows = 1;
        glb_nEmaClienteID = null;
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nEmaClienteID=null');
    }

    if (selProprietario.value > 0) {
        var nCinzaEscuro = 0xC0C0C0;
        fg.Cell(6, 2, 0, 2, 0) = nCinzaEscuro;
    }

    valueBtnAgendar(true);
}

function setBtnState() {
    var bListar = false;
    var bInclui = false;
    var bExclui = false;

    if (btnIniciarAtendimento.state >= 1)
        bListar = true;

    btnListar.disabled = bListar;

    btnGravar.disabled = !glb_GravacaoPendente;

    if (fg.Row > 0) {
        if (getCellValueByColKey(fg, 'TipoID*', fg.Row) == 2)
            bInclui = true;

        if (getCellValueByColKey(fg, 'TipoID*', fg.Row) == 3)
            bExclui = true;
    }

    btnIncluir.disabled = (!bInclui) || (glb_nA1 == 0) || (glb_nA2 == 0);
    btnExcluir.disabled = (!bExclui) || (glb_nA1 == 0) || (glb_nA2 == 0);
}

function txtPesquisa_onKeyPress() {
    glb_txtPesquisa = txtPesquisa.value;

    if (event.keyCode == 13) {
        limpaGrid();
        fillGridData();
    }
}

function txtObservacoes_onKeyPress() {
    if (txtObservacoes.value != '')
        chkAtendimentoOK.checked = false;
}

function txtAtendimentosAnteriores_onkeydown() {
    if (getCellValueByColKey(fg2, 'AtendimentoOK', fg2.Row) == 0) {
        btnGravar.disabled = false;
    }
}

/********************************************************************
Retorno do servidor
********************************************************************/
function dsoServerData_DSC() {
    glb_getServerData--;

    if (glb_getServerData != 0)
        return null;

    // ajusta o body do html
    with (modalatendimentoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    // mostra a modal
    showExtFrame(window, true);

    carregaDadosSelecinados();

    selServicos_onChange();

    if ((glb_sCallFrom == 'ListaPreco') && (glb_nState == 1)) {
        glb_nPesAtendimentoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nPesAtendimentoID');
        limpaCamposFinalizacaoAtendimento();
        setbtnIniciarAtendimentoStatus(1);
        visibilityControls();

        if (glb_nEmaClienteID != null)
            glb_bRegistraObservacao = true;
    }

    var nParceiroID = parseInt(sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selParceiroID.value'));

    if (nParceiroID > 0)
        fillGridData(nParceiroID);

    setBtnState();

    if (fg.Rows > 1) {
        fg.focus();
        fg.col = 2;
    }
    else
        txtPesquisa.focus();
}

/********************************************************************
Alteracao nos texts de Telefones e urls
********************************************************************/
function txtFoneControls_onKeyup() {
    var nTipoPessoaID = 0;

    if (fg.Rows > 1) {
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'ClienteDesde')) = txtClienteDesde.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DDIComercial')) = txtDDITelefone.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DDDComercial')) = txtDDDComercial.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'NumeroComercial')) = txtFoneComercial.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'DDDFaxCelular')) = txtDDDFaxCelular.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'NumeroFaxCelular')) = txtFaxCelular.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Email')) = txtEmail.value;
        fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Site')) = txtSite.value;

        js_modalatendimento_AfterEdit(fg.Row, fg.Col);
    }

    if (this == txtEmail) {
        if (txtEmail.value == '')
            txtEmail.value = '_@_.com.br';
    }

    if (this == txtSite) {
        if (txtSite.value == '')
            txtSite.value = 'http:/' + '/www._.com.br';
    }

    setLinkFoneUrl();
}

function setLinkFoneUrl(fromGrid) {
    var nTipoPessoaID = '';
    var sTipoID = '';
    var sAlert = '';

    if (fg.Row > 0) {
        nTipoPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoPessoaID*'));
        sTipoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'TipoID*'));
    }

    // P ou C
    if ((sTipoID == 1) || (sTipoID == 4)) {
        if (txtDDITelefone.value.length > 0)
            txtDDITelefone.style.backgroundColor = 'transparent';
        else {
            txtDDITelefone.style.backgroundColor = 'salmon';
            sAlert += '\n- DDI';
        }

        if (txtDDDComercial.value != '')
            txtDDDComercial.style.backgroundColor = 'transparent';
        else {
            txtDDDComercial.style.backgroundColor = 'salmon';
            sAlert += '\n- DDD comercial';
        }

        if (txtFoneComercial.value.length == 8)
            txtFoneComercial.style.backgroundColor = 'transparent';
        else {
            txtFoneComercial.style.backgroundColor = 'salmon';
            sAlert += '\n- Telefone comercial';
        }

        if (nTipoPessoaID == 51) {
            if (txtDDDFaxCelular.value != '')
                txtDDDFaxCelular.style.backgroundColor = 'transparent';
            else {
                txtDDDFaxCelular.style.backgroundColor = 'salmon';
                sAlert += '\n- DDD ' + (nTipoPessoaID == 51 ? 'celular' : 'fax');
            }

            if (txtFaxCelular.value.length >= 8)
                txtFaxCelular.style.backgroundColor = 'transparent';
            else {
                txtFaxCelular.style.backgroundColor = 'salmon';
                sAlert += '\n- Telefone ' + (nTipoPessoaID == 51 ? 'celular' : 'fax');
            }
        }
        else {
            txtDDDFaxCelular.style.backgroundColor = 'transparent';
            txtFaxCelular.style.backgroundColor = 'transparent';
        }

        if ((trimStr(txtEmail.value) != '') &&
	             (trimStr(txtEmail.value) != '_@_.com.br') &&
	             (trimStr(txtEmail.value) != '@_.com.br') &&
	             (trimStr(txtEmail.value) != '_@.com.br') &&
	             (trimStr(txtEmail.value) != '@.com.br'))
            txtEmail.style.backgroundColor = 'transparent';
        else {
            txtEmail.style.backgroundColor = 'salmon';
            sAlert += '\n- E-mail';
        }

        if ((trimStr(txtSite.value) != '') &&
	             (trimStr(txtSite.value) != 'http:/' + '/www._.com.br') &&
	             (trimStr(txtSite.value) != 'http:/' + '/www_.com.br') &&
	             (trimStr(txtSite.value) != 'http:/' + '/www._com.br') &&
	             (trimStr(txtSite.value) != 'http:/' + '/www.com.br'))
            txtSite.style.backgroundColor = 'transparent';
        // 'P' - pessoa
        else if (sTipoID == 1) {
            txtSite.style.backgroundColor = 'salmon';
            //sAlert += '\n- Site';
        }
        // 'C' - Contato
        else if (sTipoID == 4)
            txtSite.style.backgroundColor = 'transparent';

        if ((txtDDDComercial.value.length > 0) && (txtFoneComercial.value.length == 8)) {
            txtFoneComercial.style.cursor = 'hand';
            txtFoneComercial.style.color = 'blue';
        }
        else {
            txtFoneComercial.style.cursor = 'default';
            txtFoneComercial.style.color = 'black';
        }

        if ((txtDDDFaxCelular.value.length > 0) && (txtFaxCelular.value.length == 8) && (nTipoPessoaID == 51)) {
            txtFaxCelular.style.cursor = 'hand';
            txtFaxCelular.style.color = 'blue';
        }
        else {
            txtFaxCelular.style.cursor = 'default';
            txtFaxCelular.style.color = 'black';
        }

        if ((trimStr(txtEmail.value) != '') &&
	             (trimStr(txtEmail.value) != '_@_.com.br') &&
	             (trimStr(txtEmail.value) != '@_.com.br') &&
	             (trimStr(txtEmail.value) != '_@.com.br') &&
	             (trimStr(txtEmail.value) != '@.com.br')) {
            txtEmail.style.cursor = 'hand';
            txtEmail.style.color = 'blue';
        }
        else {
            txtEmail.style.cursor = 'default';
            txtEmail.style.color = 'black';
        }

        if ((trimStr(txtSite.value) != '') &&
	             (trimStr(txtSite.value) != 'http:/' + '/www._.com.br') &&
	             (trimStr(txtSite.value) != 'http:/' + '/www_.com.br') &&
	             (trimStr(txtSite.value) != 'http:/' + '/www._com.br') &&
	             (trimStr(txtSite.value) != 'http:/' + '/www.com.br')) {
            txtSite.style.cursor = 'hand';
            txtSite.style.color = 'blue';
        }
        else {
            txtSite.style.cursor = 'default';
            txtSite.style.color = 'black';
        }
    }
    else {
        txtDDITelefone.style.backgroundColor = 'transparent';
        txtDDDComercial.style.backgroundColor = 'transparent';

        txtFoneComercial.style.backgroundColor = 'transparent';
        txtFoneComercial.style.cursor = 'default';
        txtFoneComercial.style.color = 'black';

        txtDDDFaxCelular.style.backgroundColor = 'transparent';

        txtFaxCelular.style.backgroundColor = 'transparent';
        txtFaxCelular.style.cursor = 'default';
        txtFaxCelular.style.color = 'black';

        txtEmail.style.backgroundColor = 'transparent';
        txtEmail.style.cursor = 'default';
        txtEmail.style.color = 'black';

        txtSite.style.backgroundColor = 'transparent';
        txtSite.style.cursor = 'default';
        txtSite.style.color = 'black';
    }

    if (sAlert != '')
        glb_bDadosContatoImcompletos = true;

    else
        glb_bDadosContatoImcompletos = false;

    /*
    if ((sAlert != '') && (chkAtivo.checked) && (fromGrid == true))
    {
        if (window.top.overflyGen.Alert('Dados de contato incompletos:' + sAlert) == 0) {
            return null;
        }

        chkDetalhes1.checked = true;
        chkDetalhes1_onclick();
    }*/
}

/********************************************************************
Trata a visibilidade do divs
********************************************************************/
function visibilityControls() {
    var sStateControlsFone = 'hidden';
    var sStateDetalhesCliente = 'hidden';
    var sStateControlsFone = 'hidden';
    var sStateAtendimento = 'hidden';
    var sFinalizarAtendimento = 'hidden';
    var sStateAtendimentoAnteriores = 'hidden';
    var sDivFg = 'inherit';
    var sDivFg3 = 'hidden';
    var iHeightControlsFone = 0;
    var iHeightAtendimento = 0;
    var iHeightFG = 0;
    var iHeightFG3 = 0;
    var iTopDetalhesCliente = 0;
    var iTopControlsFone = 0;
    var iTopAtendimento = 0;
    var iTopFG3 = 0;
    var iWidthAtendimentosAnteriores;
    var iWidthFg4;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var nServico = (selServicos.value == 272 ? 3 : null);
    var nAtendimento = (chkAtivo.checked ? 281 : 282);

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    //Cadastro ou Credito
    if ((nServico == 1) || (nServico == 2)) {
        sStateDetalhesCliente = 'inherit';
        sStateControlsFone = 'inherit';
        iHeightControlsFone = (1.18 * (txtDDITelefone.offsetTop + txtDDITelefone.offsetHeight));
        iTopControlsFone = parseInt(divDetalhesCliente.currentStyle.top, 10) + parseInt(divDetalhesCliente.currentStyle.height, 10) + ELEM_GAP;
    }
        // Carteira
    else if (nServico == 3) {
        //btnFinalizar.disabled = true;
        if (chkDetalhes1.checked) {
            sStateAtendimento = 'inherit';
            sStateDetalhesCliente = 'inherit';
            sStateControlsFone = 'inherit';
            iHeightControlsFone = (1.18 * (txtDDITelefone.offsetTop + txtDDITelefone.offsetHeight));
            iHeightAtendimento = (1.18 * (txtAgendar.offsetTop + txtAgendar.offsetHeight));
            iHeightFG = 310;
            iTopDetalhesCliente = divFG.offsetTop + iHeightFG + ELEM_GAP;
            iTopControlsFone = iTopDetalhesCliente + parseInt(divDetalhesCliente.style.height, 10) + ELEM_GAP;
            iTopAtendimento = iTopControlsFone + iHeightControlsFone + ELEM_GAP - 10;
        }
        else if (chkDetalhes2.checked) {
            sDivFg3 = 'inherit';
            iHeightFG = 250;
            iTopFG3 = divFG.offsetTop + iHeightFG + ELEM_GAP;
            iHeightFG3 = 160;
        }
        else {
            sDivFg3 = 'hidden';
            sStateAtendimento = 'hideen';
            sStateDetalhesCliente = 'hidden';
            sStateControlsFone = 'hidden';
            iHeightFG = 410;
        }

        if (btnIniciarAtendimento.state == 1) {
            if (glb_nState == 1) {
                sDivFg = 'inherit';
                ///sDivFg3 = 'hidden';
                ///sStateDetalhesCliente = 'hidden';
                ///sStateControlsFone = 'hidden';
                sFinalizarAtendimento = 'hidden';
                enableDivControls(divControls, glb_aDivControls, false);
                btnIniciarAtendimento.disabled = false;
                btnAtendimentosAnteriores.disabled = false;
                btnSite.disabled = false;
                chkDetalhes1.disabled = false;
                chkDetalhes2.disabled = false;
            }
            else {
                sDivFg = 'inherit';
                //sDivFg3 = 'hidden';
                //sStateDetalhesCliente = 'inherit';
                //sStateControlsFone = 'inherit';
                sFinalizarAtendimento = 'hidden';
                glb_aDivControls = enableDivControls(divControls, glb_aDivControls, false);
                btnIniciarAtendimento.disabled = false;
            }
        }
        else if (btnIniciarAtendimento.state == 2) {
            sDivFg = 'hidden';
            sDivFg3 = 'hidden';
            sStateControlsFone = 'hidden';
            sStateDetalhesCliente = 'hidden';
            sFinalizarAtendimento = 'inherit';
            glb_aDivControls = enableDivControls(divControls, glb_aDivControls, false);
            btnIniciarAtendimento.disabled = true;
            btnAtendimentosAnteriores.disabled = true;
        }
        else {
            glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);

            /*if (selData.value > 0)
                btnIniciarAtendimento.disabled = true;
            else
                btnIniciarAtendimento.disabled = false;*/

            btnAtendimentosAnteriores.disabled = false;
        }

        btnListar.disabled = (btnIniciarAtendimento.state >= 1);

        //Mostra atendimentos anteriores
        if (btnAtendimentosAnteriores.state == 1) {
            sDivFg = 'hidden';
            sDivFg3 = 'hidden';
            sStateControlsFone = 'hidden';
            sStateDetalhesCliente = 'hidden';
            sFinalizarAtendimento = 'hidden';
            sStateAtendimentoAnteriores = 'inherit';

            iWidthAtendimentosAnteriores = 380;
            iWidthFg4 = (modWidth - (3 * ELEM_GAP)) - iWidthAtendimentosAnteriores - 6;

            btnFinalizar.disabled = true;
        }
        else {
            iWidthAtendimentosAnteriores = 485;
            btnFinalizar.disabled = false;
        }
    }

    proximoAgendar();

    divDetalhesCliente.style.visibility = sStateDetalhesCliente;
    divDetalhesCliente.style.top = iTopDetalhesCliente;
    divControlsFone.style.visibility = sStateControlsFone;
    divControlsFone.style.top = iTopControlsFone;
    divFinalizarAtendimento.style.visibility = sFinalizarAtendimento;
    divFG.style.visibility = sDivFg;

    txtAtendimentosAnteriores.style.visibility = sStateAtendimentoAnteriores;
    txtAtendimentosAnteriores.style.width = iWidthAtendimentosAnteriores;
    fg3.style.visibility = sStateAtendimentoAnteriores;

    lblPendente.style.visibility = (sStateAtendimentoAnteriores == 'hidden' ? 'hidden' : 'visible');
    chkPendente.style.visibility = (sStateAtendimentoAnteriores == 'hidden' ? 'hidden' : 'visible');
    lblCliente.style.visibility = (sStateAtendimentoAnteriores == 'hidden' ? 'hidden' : 'visible');
    chkCliente.style.visibility = (sStateAtendimentoAnteriores == 'hidden' ? 'hidden' : 'visible');

    divFG2.style.visibility = sStateAtendimentoAnteriores;
    divFG4.style.visibility = sStateAtendimentoAnteriores;
    divFG4.style.width = (iWidthFg4 == null ? iWidthAtendimentosAnteriores : iWidthFg4);
    fg4.style.width = (iWidthFg4 == null ? iWidthAtendimentosAnteriores : iWidthFg4);;

    divFG.style.height = iHeightFG;
    fg.style.height = iHeightFG;

    divFG3.style.visibility = sDivFg3;
    fg3.style.visibility = sDivFg3;
    divFG3.style.top = iTopFG3;
    divFG3.style.height = iHeightFG3;
    fg3.style.height = (parseInt(iHeightFG3) > 0 ? parseInt(iHeightFG3) - 10 : iHeightFG3);

    divFG5.style.visibility = sDivFg3;
    fg5.style.visibility = sDivFg3;
    divFG5.style.top = iTopFG3;
    divFG5.style.height = iHeightFG3;
    fg5.style.height = (parseInt(iHeightFG3) > 0 ? parseInt(iHeightFG3) - 10 : iHeightFG3);

    fg.Redraw = 0;

    drawBordersAroundTheGrid(fg);
    fg.Redraw = 2;
    limpaTextAtendimento();
}

function limpaTextAtendimento() {
    if (glb_nObservacoes != null)
        txtObservacoes.value = glb_nObservacoes + '\n';
    else
        txtObservacoes.value = '';

    txtAgendar.value = '';
}
/*************************************************************
Limpa grid e campos dos dados de telefone e urls
**************************************************************/
function limpaGrid() {
    glb_GravacaoPendente = false;

    fg.Rows = 1;

    if ((fg.Rows > 1) && (glb_nState == 1)) {
        var sParceiro = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Fantasia*'));

        secText('CRM (beta) - ' + sParceiro, 1);
    }
    else
        secText('CRM (beta)', 1);

    limparDetalhesCliente();

    txtClienteDesde.value = '';
    txtDDITelefone.value = '';
    txtDDDComercial.value = '';
    txtFoneComercial.value = '';
    txtDDDFaxCelular.value = '';
    txtFaxCelular.value = '';
    txtEmail.value = '';
    txtSite.value = '';

    setBtnState();

    setLinkFoneUrl();

    controlsFoneReadOnly();
}

function selServicos_onChange(nPessoaGravacaoID) {
    var btn_width = parseInt(btnOK.currentStyle.width, 10) - ELEM_GAP + 2;
    var sState = 'hidden';

    lblPedidosPendentes.style.visibility = sState;
    chkPedidosPendentes.style.visibility = sState;
    lblSeguradora.style.visibility = sState;
    selSeguradora.style.visibility = sState;
    lblMarca.style.visibility = sState;
    selMarca.style.visibility = sState;
    lblLimiteMinimo.style.visibility = sState;
    txtLimiteMinimo.style.visibility = sState;
    lblLimiteMaximo.style.visibility = sState;
    txtLimiteMaximo.style.visibility = sState;
    lblDiasVencidos.style.visibility = sState;
    txtDiasVencidos.style.visibility = sState;

    //btnSite.style.visibility = sState;
    btnIncluir.style.visibility = sState;
    btnExcluir.style.visibility = sState;
    lblOk.style.visibility = sState;
    chkOk.style.visibility = sState;
    btnProximo.style.visibility = sState;
    btnIniciarAtendimento.style.visibility = sState;
    btnAtendimentosAnteriores.style.visibility = sState;

    if (selServicos.value == 272) {
        // Ativo
        if (chkAtivo.checked) {
            glb_chkAtender1 = chkAtender1.checked;

            glb_txtPesquisa = txtPesquisa.value;
            txtPesquisa.value = '';
            chkAtender1.disabled = true;
            chkAtender1.checked = true;
            chkCredito.disabled = true;
            chkCredito.checked = false;
        }
        else
            chkAtender1.checked = glb_chkAtender1;

        btnProximo.style.visibility = 'inherit';
        btnIniciarAtendimento.style.visibility = 'inherit';
        btnAtendimentosAnteriores.style.visibility = 'inherit';
    }

    var nParceiroID;

    if (fg.row > 0)
        nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));

    limpaGrid();
    setBtnState();
    visibilityControls();

    if ((selServicos.value == 272) && (chkAtivo.checked)) {
        fg3.Rows = 1;
        //fillGridData(nParceiroID);
        fillGridData(nPessoaGravacaoID);
    }
}

function selData_onchange() {
    if (selData.value > 0) {
        chkAtivo.checked = false;
        chkAtivo.disabled = true;
        selServicos_onChange();
    }
    else {
        chkAtivo.disabled = false;
        //btnIniciarAtendimento.disabled = false;
        visibilityControls();
        btnGravar.disabled = true;
    }

    fg.Rows = 1;
}

function selOferta_onchange() {
    fg.Rows = 1;
}

function detalhesClienteReadOnly() {
    var readOnly = true;

    txtAnos.readOnly = readOnly;
    txtPedidosFaturados.readOnly = readOnly;
    txtFaturamentoAcumulado.readOnly = readOnly;
    txtContribuicaoAcumulada.readOnly = readOnly;
    txtContribuicaoAcumuladaRecebida.readOnly = readOnly;
    txtAtraso.readOnly = readOnly;
    txtMaiorCompra.readOnly = readOnly;
    txtDiasUltimaCompra.readOnly = readOnly;
    txtDataUltimaCompra.readOnly = readOnly;
    txtUltimaCompra.readOnly = readOnly;
}

function limparDetalhesCliente() {
    txtAnos.value = '';
    txtPedidosFaturados.value = '';
    txtFaturamentoAcumulado.value = '';
    txtContribuicaoAcumulada.value = '';
    txtContribuicaoAcumuladaRecebida.value = '';
    txtAtraso.value = '';
    txtMaiorCompra.value = '';
    txtDiasUltimaCompra.value = '';
    txtDataUltimaCompra.value = '';
    txtUltimaCompra.value = '';
}

/************************************************************
*************************************************************/
function controlsFoneReadOnly() {
    var readOnly = true;

    if (fg.Rows > 1)
        readOnly = false;

    txtClienteDesde.readOnly = true;
    txtDDITelefone.readOnly = readOnly;
    txtDDDComercial.readOnly = readOnly;
    txtFoneComercial.readOnly = readOnly;
    txtDDDFaxCelular.readOnly = readOnly;
    txtFaxCelular.readOnly = readOnly;
    txtEmail.readOnly = readOnly;
    txtSite.readOnly = readOnly;
}

function verificaData(sData) {
    var sData = trimStr(sData);
    var bDataIsValid = true;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
            return null;

        lockControlsInModalWin(false);
        return false;
    }
    return true;
}

function finalizarAtendimento() {
    var sdtAgendamento = txtAgendar.value;
    var sMeioAtendimentoID = (selMeioAtendimento.value < 1 ? '' : selMeioAtendimento.value);
    var sObservacoes = trimStr(txtObservacoes.value);
    var nAtendimento = (chkAtivo.checked ? 281 : 282);
    var sAtendimentoOK = (chkAtendimentoOK.checked ? 1 : 0);
    var sProdutosCotados = '';

    sObservacoes = replaceStr(sObservacoes, '\'', '');
    sObservacoes = replaceStr(sObservacoes, '"', '');

    if (!verificaData(sdtAgendamento))
        return null;
    
    if ((txtAgendar.value == '') && (chkAtendimentoOK.checked == false)) {
        if (window.top.overflyGen.Alert('Atendimento n�o OK e sem data de agendamento.') == 0)
            return null;

        return null;
    }
        
    sdtAgendamento = normalizeDate_DateTime(sdtAgendamento, true);
    sdtAgendamento = (sdtAgendamento == null ? '' : sdtAgendamento);

    sProdutosCotados = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sClipBoardBuffer');

    lockControlsInModalWin(true);

    strPars = '?nStatus=' + escape('1');
    strPars += '&nMeioAtendimentoID=' + escape(sMeioAtendimentoID);
    strPars += '&sAtendimentoOK=' + escape(sAtendimentoOK);
    strPars += '&sObservacoes=' + escape(sObservacoes);
    strPars += '&sDtAgendamento=' + escape(sdtAgendamento);
    strPars += '&nPesAtendimentoID=' + escape(glb_nPesAtendimentoID);
    strPars += '&sProdutos=' + escape(sProdutosCotados);

    try {
        dsoGravarAgendamento.URL = SYS_ASPURLROOT + '/serversidegenEx/gravaragendamento.aspx' + strPars;
        dsoGravarAgendamento.ondatasetcomplete = finalizarAtendimento_DSC;
        dsoGravarAgendamento.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('N�o foi poss�vel finalizar o atendimento, tente novamente.') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function atenderEFinalizarAtendimento(bDblClickAgendamento) {
    var sAlert = '';
    var nPessoaID = 0;
    var sObservacao = '';
    var nAtendimento = null;
    var sdtAgendamento = txtAgendar.value;
    var sMeioAtendimentoID = (selMeioAtendimento.value < 1 ? '' : selMeioAtendimento.value);

    if (fg.Row > 0) {
        if (chkAtivo.checked)
            nPessoaID = fg.ValueMatrix(2, getColIndexByColKey(fg, 'PessoaID*'));
        else
            nPessoaID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));
    }

    if (nPessoaID <= 0)
        sAlert = 'Selecione um cliente no grid';

    if (sAlert.length > 0) {
        if (window.top.overflyGen.Alert(sAlert) == 0)
            return null;
        return null;
    }

    var bTemAgendamento = (fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'dtAgendamento*')) != '');

    if (bDblClickAgendamento) {
        var d = new Date();

        if (bTemAgendamento)
            sdtAgendamento = null;
        else
            sdtAgendamento = ("00" + (d.getMonth() + 1)).slice(-2) + "/" +
                             ("00" + d.getDate()).slice(-2) + "/" +
                             d.getFullYear() + " " +
                             ("00" + d.getHours()).slice(-2) + ":" +
                             ("00" + d.getMinutes()).slice(-2) + ":" +
                             ("00" + d.getSeconds()).slice(-2) + "." +
                             ("00" + d.getMilliseconds()).slice(-3);
    }
    else
        sdtAgendamento = normalizeDate_DateTime(sdtAgendamento, true);
    
    sdtAgendamento = (sdtAgendamento == null ? '' : sdtAgendamento);

    sObservacao = trimStr(txtObservacoes.value);
    nAtendimento = (chkAtivo.checked ? 281 : 282);

    lockControlsInModalWin(true);

    strPars = '?nPessoaID=' + escape(nPessoaID);
    //strPars += '&nStatus=' + escape(glb_nState == 1 ? '4' : '3');
    strPars += '&nStatus=' + escape(glb_nState == 1 ? '4' : ((bTemAgendamento) ? '6' : '3'));
    strPars += '&nEmpresaID=' + escape(glb_aEmpresaData[0]);
    strPars += '&nUserID=' + escape(glb_nUserID);
    strPars += '&nServicoAtendimentoID=' + escape(272);
    strPars += '&nTipoAtendimentoID=' + escape(nAtendimento);
    strPars += '&nMeioAtendimentoID=' + escape(sMeioAtendimentoID);
    strPars += '&sAtendimentoOK=' + escape(0);
    strPars += '&sObservacoes=' + escape(sObservacao);
    strPars += '&sDtAgendamento=' + escape(sdtAgendamento);
    strPars += '&nPesAtendimentoID=' + escape('');
    strPars += '&sProdutos=' + escape('');

    try {
        dsoGravarAgendamento.URL = SYS_ASPURLROOT + '/serversidegenEx/gravaragendamento.aspx' + strPars;
        dsoGravarAgendamento.ondatasetcomplete = atenderEFinalizarAtendimento_DSC;
        dsoGravarAgendamento.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('N�o foi poss�vel gravar o atendimento, tente novamente.') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function atenderEFinalizarAtendimento_DSC() {
    lockControlsInModalWin(false);
    glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);
    btnIniciarAtendimento.disabled = false;
    chkOk.checked = false;
    // Altera o status de Finalizando para iniciar
    setbtnIniciarAtendimentoStatus(0);
    visibilityControls();
    limpaTextAtendimento();
    fillGridData();
    glb_nState = null;

    if ((fg.Rows > 1) && (glb_nState == 1)) {
        var sParceiro = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Fantasia*'));

        secText('CRM (beta) - ' + sParceiro, 1);
    }
    else
        secText('CRM (beta)', 1);
}

function finalizarAtendimento_DSC() {
    //if ((glb_sCallFrom == 'ListaPreco') && (glb_nState == 1))
    //{
    //    lockControlsInModalWin(false);
    //    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_sClipBoardBuffer=\'\';');
    //    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , true);
    //}        
    //else
    //{

    lockControlsInModalWin(false);
    glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);
    btnIniciarAtendimento.disabled = false;
    // Altera o status de Finalizando para iniciar
    setbtnIniciarAtendimentoStatus(0);
    visibilityControls();
    btnFinalizar.disabled = true;
    limpaTextAtendimento();

    if ((fg.Rows > 1) && (glb_nState == 1)) {
        var sParceiro = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Fantasia*'));

        secText('CRM (beta) - ' + sParceiro, 1);
    }
    else
        secText('CRM (beta)', 1);

    // Verifica se cliente tem atendimento pendentes
    if (glb_nQuantidadeAtendimentosPendentes > 0) {
        var _retMsg = window.top.overflyGen.Confirm('Existem atendimentos anteriores pendentes deste cliente.\nDeseja verificar ?', 0);

        if (_retMsg == 1) {
            glb_bExibeAtendimentoAnteriores = true;
            atendimentosAnteriores();
        }
        else {
            if (chkAtivo.checked) {
                glb_nState = 0;
                proximoAtendimento();
            }
        }
    }

    if (chkAtivo.checked)
        proximoAtendimento();
    //else if (!glb_bExibeAtendimentoAnteriores)
    //    fillGridData();

    glb_nState = null;

    glb_nEmaClienteID = null;
    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nEmaClienteID=null');
    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "glb_sClipBoardBuffer=''");
    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'finalizarAtendimento_DSC()');
    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nObservacoes=null');
    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "txtArgumento.value=''");
}

function saveAtendimentosAnteriores() {

    glb_nLinhaSelecionadaFg2 = fg2.row;

    lockControlsInModalWin(true);

    var strPars = '';
    var contador = 0;

    // Conta quantas grava��es far�
    for (var i = 1; i < fg2.Rows; i++) {
        if (getCellValueByColKey(fg2, 'Gravar', i) == 1) {
            contador++;
        }
    }

    // Se alterou apenas uma linha no grid, grava obseva��es.
    if (contador == 1) {
        var sObservacoes = trimStr(txtAtendimentosAnteriores.value);

        sObservacoes = replaceStr(sObservacoes, '\'', '');
        sObservacoes = replaceStr(sObservacoes, '"', '');

        var nPesAtendimentoID = getCellValueByColKey(fg2, 'PesAtendimentoID', fg2.row);
        var sAtendimentoOK = (getCellValueByColKey(fg2, 'AtendimentoOK', fg2.row) == 0 ? 0 : 1);

        strPars = '?nStatus=' + escape('5');
        strPars += '&sAtendimentoOK=' + escape(sAtendimentoOK);
        strPars += '&sObservacoes=' + escape(sObservacoes);
        strPars += '&nPesAtendimentoID=' + escape(nPesAtendimentoID);
    }
    else {
        var count = 0;

        for (var i = 1; i < fg2.Rows; i++) {
            // Se linha foi alterada
            if (getCellValueByColKey(fg2, 'Gravar', i) == 1) {
                var nPesAtendimentoID = getCellValueByColKey(fg2, 'PesAtendimentoID', i);
                var sAtendimentoOK = (getCellValueByColKey(fg2, 'AtendimentoOK', i) == 0 ? 0 : 1);

                strPars += ((count == 0) ? '?' : '&') + 'nStatus=' + escape('5');
                strPars += '&sAtendimentoOK=' + escape(sAtendimentoOK);
                strPars += '&nPesAtendimentoID=' + escape(nPesAtendimentoID);

                count++;
            }
        }
    }

    try {
        dsoAtendimentoAnterior.URL = SYS_ASPURLROOT + '/serversidegenEx/gravaragendamento.aspx' + strPars;
        dsoAtendimentoAnterior.ondatasetcomplete = saveAtendimentosAnteriores_DSC;
        dsoAtendimentoAnterior.Refresh();
    }
    catch (e) {
        if (window.top.overflyGen.Alert('N�o foi poss�vel gravar o atendimento, tente novamente.') == 0)
            return null;

        lockControlsInModalWin(false);
    }
}

function saveAtendimentosAnteriores_DSC() {

    lockControlsInModalWin(false);
    
    var nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));
    fillGridData2(nParceiroID);
}


function atendimentosAnterioresReadOnly() {
    if (getCellValueByColKey(fg2, 'AtendimentoOK', fg2.row) != 0)
        txtAtendimentosAnteriores.readOnly = true;
    else
        txtAtendimentosAnteriores.readOnly = false;
}

function cancelarFinalizacaoAtendimento() {
    //if ((glb_sCallFrom == 'ListaPreco') && (glb_nState == 1))
    //    sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, false );
    //else
    //{
    glb_aDivControls = enableDivControls(divControls, glb_aDivControls, true);
    btnIniciarAtendimento.disabled = false;

    if (glb_nState == 1)
        setbtnIniciarAtendimentoStatus(1);
    else
        setbtnIniciarAtendimentoStatus(0);

    visibilityControls();

    if (glb_nState == 1)
        btnFinalizar.disabled = false;
    else
        btnFinalizar.disabled = true;
    //}
}

function enableDivControls(divCtrl, aDivControls, bEnable) {
    selEstados.disabled = !bEnable;
    selClassificacao.disabled = !bEnable;
    selProprietario.disabled = !bEnable;
    selOferta.disabled = !bEnable;
    selData.disabled = !bEnable;
    txtPesquisa.disabled = !bEnable;
    btnListar.disabled = !bEnable;
    chkOk.disabled = !bEnable;

    /*validar*/
    btnSite.disabled = !bEnable;
    selServicos.disabled = !bEnable;
    txtLimiteMinimo.disabled = !bEnable;
    txtLimiteMaximo.disabled = !bEnable;

    if (selData.value > 0) {
        btnGravar.disabled = true;
        //btnIniciarAtendimento.disabled = true;
        btnFinalizar.disabled = true;
        btnProximo.disabled = true;
        chkAtivo.disabled = true;
    }
    else {
        btnGravar.disabled = !bEnable;
        btnProximo.disabled = !bEnable;
        btnIniciarAtendimento.disabled = !bEnable;
        chkAtivo.disabled = !bEnable;
    }

    if (txtAtendimentosAnteriores.style.visibility == 'inherit') {
        chkDetalhes1.disabled = !bEnable;
        chkDetalhes2.disabled = !bEnable;
    }

    //btnAtendimentosAnteriores.disabled = !bEnable;
    //btnSite.disabled = !bEnable;

    if (!chkAtivo.checked) {
        chkAtender1.disabled = !bEnable;
        chkAtender2.disabled = !bEnable;
        chkCredito.disabled = !bEnable;
    }
}

/*
Inicia/Finaliza o timer que controla o tempo de um atendimento
Parametros: bInicia: 1-Inicia, 0-Finaliza
*/
function setTimerAgendamentoStatus(bInicia) {
    if (bInicia)
        glb_AtendimentoTimer = window.setInterval('atualizaLabelTimer()', 1000, 'JavaScript');
    else if (glb_AtendimentoTimer != null) {
        window.clearInterval(glb_AtendimentoTimer);
        glb_AtendimentoTimer = null;
    }
}

function atualizaLabelTimer() {
    if (glb_dtAtendimentoInicio == null) {
        if ((fg.Rows > 1) && (glb_nState == 1)) {
            var sParceiro = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Fantasia*'));

            secText('CRM (beta) - ' + sParceiro, 1);
        }
        else
            secText('CRM (beta)', 1);
    }
    else {
        var dtAtendimentoFim = new Date();
        glb_dtAtendimentoFim = dtAtendimentoFim.getTime();

        var sTexto = 'CRM (beta)';

        //if (fg.Rows == 2)
        //    sTexto += ': ' + (fg.Rows - 1) + ' registro';    	    
        //else if	(fg.Rows > 2)    
        //    sTexto += ': ' + (fg.Rows - 1) + ' registros';

        sTexto = sTexto + ' [' + tempoDecorrido(glb_dtAtendimentoInicio, glb_dtAtendimentoFim) + ']';
        secText(sTexto, 1);
    }
}

function openSiteWeb() {
    var nUserID = glb_nUserID;
    var sPagina = 'vitrine.aspx';
    var nParceiroID = '';

    if ((glb_sCallFrom == 'ListaPreco') && (glb_nState == 1))
        nParceiroID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selParceiroID.value');
    else if (fg.Row > 0)
        nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));

    if (nParceiroID > 0) {
        strPars = '?nUserID=' + escape(nUserID) +
		    '&Pagina=' + escape(sPagina) +
		    '&ParceiroID=' + escape(nParceiroID) +
            '&EmpresaID=' + escape(glb_aEmpresaData[0]) +
		    '&PessoaID=' + escape(nParceiroID);

        dsoWeb.URL = SYS_ASPURLROOT + '/serversidegenEx/rashtoopensite.aspx' + strPars;
        dsoWeb.ondatasetcomplete = openSiteWeb_DSC;
        dsoWeb.Refresh();
    }
    else {
        if (window.top.overflyGen.Alert('Escolha um cliente.') == 0)
            return null;

        lockControlsInModalWin(false);
        return null;
    }
}

function openSiteWeb_DSC() {
    var sSite;
    var sHash;

    if (!((dsoWeb.recordset.BOF) || (dsoWeb.recordset.BOF))) {
        sHash = dsoWeb.recordset['sHashCode'].value;

        if ((sHash == null) || (sHash == "")) {
            if (window.top.overflyGen.Alert(dsoWeb.recordset['Erro ao abrir site. Tente mais tarde.'].value) == 0)
                return null;

            lockControlsInModalWin(false);
            return null;
        }
        else {
            if (sHash != null) {
                sSite = 'http:/' + '/www.alcateia.com.br/alcateiav3/web/aspx/Login.aspx?hc=' + sHash;
                //sSite = 'http:/' + '/172.25.100.181/AlcateiaV3/web/aspx/Login.aspx?hc=' + sHash;
            }

            window.open(sSite);

            lockControlsInModalWin(false);
            return null;
        }
    }
}

function proximoAtendimento() {
    var nParceiroID = 0;

    if (fg.Row > 0) {
        if (chkAtivo.checked)
            nParceiroID = fg.ValueMatrix(2, getColIndexByColKey(fg, 'PessoaID*'));
        else
            nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));
    }

    if (nParceiroID <= 0) {
        if (window.top.overflyGen.Alert('Selecione um cliente.') == 0)
            return null;

        return null;
    }

    if (glb_nState != 1)
        lblFinalizarAtendimento.innerHTML = 'Agendar atendimento'.bold();

    //mostra div de finalizacao de atendimento
    if (chkOk.checked) {
        limpaCamposFinalizacaoAtendimento();
        setbtnIniciarAtendimentoStatus(2);
        visibilityControls();
        btnIniciarAtendimento.disabled = true;
        return null;
    }
        //Inicia e finaliza atendimento automaticamente
    else
        atenderEFinalizarAtendimento();
}

function confirmarFinalizacaoAtendimento() {
    if ((glb_sCallFrom == 'ListaPreco') && (glb_nState == 1))
        finalizarAtendimento();
    else if (chkOk.checked)
        atenderEFinalizarAtendimento();
    else
        finalizarAtendimento();
}

function limpaCamposFinalizacaoAtendimento() {
    selMeioAtendimento.value = 0;
    selReagendamento.value = 0;
    txtAgendar.value = '';
    
    if (glb_nObservacoes != null)
        txtObservacoes.value = glb_nObservacoes + '\n';
    else
        txtObservacoes.value = '';
}

function atendimentosAnteriores() {
    var nParceiroID = 0;
    var sAlert = '';

    if ((glb_sCallFrom == 'ListaPreco') && (glb_nState == 1))
        nParceiroID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selParceiroID.value');
    else if (fg.Row > 0)
        nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));

    if (nParceiroID <= 0)
        sAlert = 'Selecione um cliente no grid';

    if (sAlert.length > 0) {
        if (window.top.overflyGen.Alert(sAlert) == 0)
            return null;

        return null;
    }

    if (btnAtendimentosAnteriores.state == 0) {
        setbtnAtendimentosAnteriores(1);
        visibilityControls();
        enableDivControls(null, null, false);
        chkPendente.checked = true;
        chkCliente.checked = true;
        fillGridData2(nParceiroID);
    }
    else {
        fg2.Rows = 1;
        txtAtendimentosAnteriores.value = '';

        setbtnAtendimentosAnteriores(0);
        visibilityControls();

        if ((btnIniciarAtendimento.state != 2) && (glb_nState != 1))
            enableDivControls(null, null, true);

        if (glb_bExibeAtendimentoAnteriores)
        {
            glb_bExibeAtendimentoAnteriores = false;

            if (chkAtivo.checked) {
                glb_nState = 0;
                proximoAtendimento();
            }
            else
                fillGridData();
        }
    }
}

function fillGridData2(nParceiroID) {
    hr_L_FGBorder.style.visibility = 'hidden';
    hr_R_FGBorder.style.visibility = 'hidden';
    hr_B_FGBorder.style.visibility = 'hidden';

    lockControlsInModalWin(true);

    setConnection(dsoGrid2);

    var sSQL = 'SELECT DISTINCT TOP 100 dbo.fn_Pessoa_Fantasia(a.PessoaID, 0) AS Fantasia, ' +
                            'CONVERT(VARCHAR(10), a.dtInicio, 103) + SPACE(1) + CONVERT(VARCHAR(5), a.dtInicio, 108) AS DataHora, ' +
		                    'dbo.fn_Data_DuracaoExtenso(dbo.fn_Data_DuracaoDecimal(a.dtInicio, a.dtFim, NULL), 4, 2, 1, 2) AS Duracao, ' +
		                    'dbo.fn_Pessoa_Fantasia(a.UsuarioID, 0) AS Colaborador, ' +
		                    'dbo.fn_TipoAuxiliar_Item(a.TipoAtendimentoID, 0) AS TipoAtendimento, ' +
		                    'dbo.fn_TipoAuxiliar_Item(a.MeioAtendimentoID, 0) AS MeioAtendimento, ' +
		                    'ISNULL(a.AtendimentoOK, 0) AS AtendimentoOK, ' +
		                    'CONVERT(VARCHAR(10), a.dtAgendamento, 103) + SPACE(1) +  CONVERT(VARCHAR(5), a.dtAgendamento, 108) AS dtAgendamento, ' +
		                    'dbo.fn_Atendimento_Resumo(a.PesAtendimentoID) AS Resumo, CONVERT(BIT, 0) AS Gravar, PesAtendimentoID ' +
                        'FROM Pessoas_Atendimentos a WITH(NOLOCK) ';

                        if ((selProprietario.value > 0) && (!chkCliente.checked))
                            sSQL += 'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.SujeitoID = a.PessoaID) ' +
                                    'WHERE (b.ProprietarioID = ' + selProprietario.value + ') AND (b.TipoRelacaoID = 21) AND (b.EstadoID = 2) ';
                        else
                            sSQL += 'WHERE (1=1) ';

                        if (chkCliente.checked)
                            sSQL += 'AND (a.PessoaID = ' + nParceiroID + ') ';

                        if (chkPendente.checked)
                            sSQL += 'AND (ISNULL(a.AtendimentoOK, 0) = 0) ';

                        sSQL += 'ORDER BY Fantasia, a.PesAtendimentoID DESC';

    dsoGrid2.SQL = sSQL;
    dsoGrid2.ondatasetcomplete = fillGridData2_DSC;
    dsoGrid2.Refresh();
}

function fillGridData2_DSC() {
    if ((dsoGrid2.recordset.BOF) && (dsoGrid2.recordset.EOF)) {
        fg2.Rows = 1;
        lockControlsInModalWin(false);
        return;
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm';

    fg2.Redraw = 0;
    //fg2.Editable = false;
    fg2.Editable = true;
    startGridInterface(fg2);

    fg.FontSize = '8';
    fg.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg2, ['Pessoa',
	               'Data',
                   'Dura��o',
                   'Colaborador',
   				   'Atendimento',
				   'Meio',
                   'OK',
   				   'Agendamento',
   				   'Resumo',
                   'Gravar',
	               'PesAtendimentoID'], [8, 9, 10]);

    fillGridMask(fg2, dsoGrid2, ['Fantasia',
                   'DataHora',
                   'Duracao',
                   'Colaborador',
                   'TipoAtendimento',
				   'MeioAtendimento',
                   'AtendimentoOK',
                   'dtAgendamento',
                   'Resumo',
                   'Gravar',
                   'PesAtendimentoID'],
				   ['', '99/99/9999 99:99', '', '', '', '', '', '99/99/9999 99:99', '', '', ''],
				   ['', dTFormat, '', '', '', '', '', dTFormat, '', '', '']);

    glb_aCelHint = [[0, 5, 'Meio de atendimento'],
                    [0, 6, 'Atendimento foi bem sucedido?']];

    // Pinta celula
    if (fg2.Rows > 0) {
        var nPrimeiraColuna = getColIndexByColKey(fg2, 'Fantasia');
        var nUltimaColuna = getColIndexByColKey(fg2, 'PesAtendimentoID');
        var nAmarelo = 0xA3F3EB;
        var nCinzaClaro = 0xD7D7D7;

        for (var i = 1; i < fg2.Rows; i++)
            if (getCellValueByColKey(fg2, 'AtendimentoOK', i) != 1)
                fg2.Cell(6, i, nPrimeiraColuna, i, nUltimaColuna) = nAmarelo;

        if ((fg2.Rows > 0) && (chkCliente.checked))
            fg2.Cell(6, 1, 0, 1, 0) = nCinzaClaro;
    }

    fg2.MergeCells = 4;
    fg2.MergeCol(0) = true;

    fg2.Redraw = 0;

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);

    fg2.ExplorerBar = 5;

    lockControlsInModalWin(false);

    hr_L_FGBorder.style.visibility = 'inherit';
    hr_R_FGBorder.style.visibility = 'inherit';
    hr_B_FGBorder.style.visibility = 'inherit';

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;
    fg2.Editable = true;

    fg2.Redraw = 2;

    if (fg2.Rows > 0) {
        fg2.focus();
        fg2.col = 2;

        if (glb_nLinhaSelecionadaFg2 != null) {
            
            // Se a linha n�o existe mais no grid (por n�o estar mais pendente), coloca na linha 1
            try {
                fg2.row = glb_nLinhaSelecionadaFg2;
            } catch (e) {
                fg2.row = 1;
            }

            glb_nLinhaSelecionadaFg2 = null;
        }
    }

    js_fg2_modalatendimentoEnterCell(fg2);
}

function fillGridData3(nParceiroID) {
    lockControlsInModalWin(true);

    setConnection(dsoGrid3);

    dsoGrid3.SQL = 'SELECT DISTINCT TOP 10 d.Conceito AS Familia, dbo.fn_Produto_Descricao2(b.PedItemID, NULL, 20) AS Produto, c.ConceitoID, e.dtNotaFiscal, a.PedidoID, ' +
                            'dbo.fn_Recursos_Campos(a.EstadoID, 1) AS Estado, e.NotaFiscal, b.Quantidade, b.ValorUnitario, (b.ValorUnitario * b.Quantidade) AS ValorTotal ' +
                        'FROM Pedidos a WITH(NOLOCK) ' +
                            'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.PedidoID = a.PedidoID) ' +
                            'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = b.ProdutoID) ' +
                            'INNER JOIN Conceitos d WITH(NOLOCK) ON (d.ConceitoID = c.ProdutoID) ' +
                            'INNER JOIN NotasFiscais e WITH(NOLOCK) ON (e.NotaFiscalID = a.NotaFiscalID) ' +
                        'WHERE a.ParceiroID = ' + nParceiroID + ' AND a.EstadoID > 26 AND e.dtNotaFiscal >= GETDATE()-180 ' +
                        'ORDER BY dtNotaFiscal DESC';

    dsoGrid3.ondatasetcomplete = fillGridData3_DSC;
    dsoGrid3.Refresh();
}

function fillGridData3_DSC() {
    if ((dsoGrid3.recordset.BOF) && (dsoGrid3.recordset.EOF)) {
        lockControlsInModalWin(false);
        return;
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm';

    fg3.Redraw = 0;
    fg3.Editable = false;
    startGridInterface(fg3);

    fg3.FontSize = '8';
    fg3.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg3, ['Data',
   				   'Pedido',
                   'E',
				   'NF',
                   'Fam�lia',
	               'Produto',
                   'ID',
                   'Quant',
   				   'Valor',
   				   'Valor Total'], []);

    fillGridMask(fg3, dsoGrid3, ['dtNotaFiscal*',
                                'PedidoID*',
                                'Estado*',
                                'NotaFiscal*',
                                'Familia*',
                                'Produto*',
                                'ConceitoID*',
                                'Quantidade*',
                                'ValorUnitario*',
                                'ValorTotal*'],
					            ['', '', '', '', '99/99/9999 99:99', '', '', '', '999999999.99', '999999999.99'],
					            ['', '', '', '', dTFormat, '', '', '', '###,###,###.00', '###,###,###.00']);

    //alignColsInGrid(fg, [1, 2, 5, 6, 7, 8]);

    fg3.MergeCells = 4;
    fg3.MergeCol(0) = true;
    fg3.MergeCol(1) = true;
    fg3.MergeCol(2) = true;
    fg3.MergeCol(3) = true;
    fg3.MergeCol(4) = true;

    fg3.Redraw = 0;

    fg3.AutoSizeMode = 0;
    fg3.AutoSize(0, fg3.Cols - 1);

    //fg3.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fg3.Redraw = 2;
}

function fillGridData4(nPesAtendimentoID) {
    lockControlsInModalWin(true);

    setConnection(dsoGrid4);

    dsoGrid4.SQL = 'SELECT dbo.fn_Produto_Descricao2(a.ProdutoID, NULL, 12) AS Produto, a.ProdutoID, b.Fantasia AS Empresa, ' +
                            'Quantidade, c.SimboloMoeda AS Moeda, a.Preco, a.Imposto, a.Prazo ' +
                        'FROM Pessoas_Atendimentos_Produtos a WITH(NOLOCK) ' +
                            'LEFT JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmpresaID) ' +
                            'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.MoedaID) ' +
                        'WHERE (a.PesAtendimentoID = ' + nPesAtendimentoID + ') ' +
                        'ORDER BY Produto ';

    dsoGrid4.ondatasetcomplete = fillGridData4_DSC;
    dsoGrid4.Refresh();
    fg2.focus();
}

function fillGridData4_DSC() {
    if ((dsoGrid4.recordset.BOF) && (dsoGrid4.recordset.EOF)) {
        lockControlsInModalWin(false);
        fg2.focus();
        atendimentosAnterioresReadOnly();
        return;
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm';

    fg4.Redraw = 0;
    fg4.Editable = false;
    startGridInterface(fg4);

    fg4.FontSize = '8';

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg4, ['ID',
                     'Empresa',
                     'Quantidade',
                     'Moeda',
                     'Preco',
                     'Imposto',
                     'Prazo',
                     'Produto'], []);

    fillGridMask(fg4, dsoGrid4, ['ProdutoID',
                                 'Empresa',
                                 'Quantidade',
                                 'Moeda',
                                 'Preco',
                                 'Imposto',
                                 'Prazo',
                                 'Produto'],
					            ['', '', '9999999999', '', '999999999.99', '999999999.99', '999999999.99', ''],
					            ['', '', '##########', '', '###,###,###.00', '###,###,###.00', '##########', '']);

    fg4.FrozenCols = 0;
    fg4.FrozenCols = 1;

    alignColsInGrid(fg, [1, 3, 5, 6]);

    fg4.Redraw = 0;

    fg4.AutoSizeMode = 0;
    fg4.AutoSize(0, fg4.Cols - 1);

    fg4.ExplorerBar = 5;

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fg4.Redraw = 2;
    fg2.focus();
    atendimentosAnterioresReadOnly();
}

function fillGridData5(nParceiroID) {
    lockControlsInModalWin(true);

    setConnection(dsoGrid5);

    dsoGrid5.SQL = "SELECT '' AS Assunto, NULL AS EmailID, NULL AS CanalID, NULL AS EmaClienteID " +
                   "UNION ALL " +
                   "SELECT DISTINCT SUBSTRING(a.Assunto, (PATINDEX('% - %', a.Assunto) + 3), LEN(a.Assunto)) + ' (' + LEFT(CONVERT(VARCHAR(20), a.dtEnvio, 103), 5) + ')' AS Assunto, " +
                           "a.EmailID, b.CanalID, b.EmaClienteID " +
                       "FROM EmailMarketing a WITH(NOLOCK) " +
                           "CROSS APPLY (SELECT TOP 1 aa.EmaClienteID, aa.CanalID FROM EmailMarketing_Clientes aa WHERE (aa.EmailID = a.EmailID) AND (aa.PessoaID = " + nParceiroID + ")) AS b " +
                       "WHERE ((a.dtValidade > GETDATE()) AND (a.EstadoID = 63)) " +
                       "ORDER BY EmailID ";

    //AND (b.Body IS NOT NULL)

    dsoGrid5.ondatasetcomplete = fillGridData5_DSC;
    dsoGrid5.Refresh();
}

function fillGridData5_DSC() {
    if ((dsoGrid5.recordset.BOF) && (dsoGrid5.recordset.EOF)) {
        lockControlsInModalWin(false);
        return;
    }

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy hh:mm';

    fg5.Redraw = 0;
    fg5.Editable = false;
    startGridInterface(fg5);

    fg5.FontSize = '8';
    fg5.FrozenCols = 0;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg5, ['Oferta',
                    'CanalID',
                    'EmailID',
                    'EmaClienteID'], [1, 2, 3]);

    fillGridMask(fg5, dsoGrid5, ['Assunto*',
                                'CanalID*',
                                'EmailID*',
                                'EmaClienteID*'],
					            ['', '', '', ''],
                                ['', '', '', '']);

    fg5.Redraw = 0;

    fg5.AutoSizeMode = 0;
    fg5.AutoSize(0, fg5.Cols - 1);

    lockControlsInModalWin(false);

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    fg5.Redraw = 2;

    var nEmailID;

    // Seleciona ultima oferta selecionada ao sair
    if (fg5.Rows > 0) {
        if (glb_nEmaClienteID != null) {
            for (var i = 1; i < fg5.Rows; i++) {
                var nEmaClienteID = getCellValueByColKey(fg5, 'EmaClienteID*', i);
            
                if (glb_nEmaClienteID == nEmaClienteID) {
                    fg5.row = i;
                    nEmailID = getCellValueByColKey(fg5, 'EmailID*', i);
                }
            }
        }
        else if (selOferta.value > 0) {
            for (var i = 1; i < fg5.Rows; i++) {
                nEmailID = getCellValueByColKey(fg5, 'EmailID*', i);

                if (nEmailID == selOferta.value) {
                    fg5.row = i;
                }
            }
        }
        else
            fg5.row = 1;
    }
    
    if (glb_bRegistraObservacao)
        trataObservacao(nEmailID);

    fg5.focus();
}

function fg5_DblClick(grid, row, col) {
    if (fg5.Rows > 0)
        visualizarEmail();
}

function visualizarEmail() {
    var nEmailID = getCellValueByColKey(fg5, 'EmailID*', fg5.Row);
    var nEmaClienteID = getCellValueByColKey(fg5, 'EmaClienteID*', fg5.Row);
    var nCanalID = getCellValueByColKey(fg5, 'CanalID*', fg5.Row);

    var strPars = '';

    strPars += '?nEmailID=' + escape(nEmailID) +
        '&nCanalID=' + escape(nCanalID) +
        '&nEmaClienteID=' + escape(nEmaClienteID) +
        '&bChamadaModalCRM=' + escape(1);

    window.open(SYS_PAGESURLROOT + '/modmarketing/subcampanhas/emailmarketing/serverside/template.aspx' + strPars);
}

function js_fg5_modalatendimentoAfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return;

    if (fg5.row > 0) {
        var nEmaClienteID = getCellValueByColKey(fg5, 'EmaClienteID*', fg5.row);

        glb_nEmaClienteID = (nEmaClienteID == '' ? null : nEmaClienteID);
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nEmaClienteID=' + glb_nEmaClienteID);
    }
}

function js_fg2_modalatendimentoAfterRowColChange(grid, OldRow, OldCol, NewRow, NewCol) {
    if (glb_GridIsBuilding)
        return;

    if (fg2.row > 0) {
        var nColOK = getColIndexByColKey(fg2, 'AtendimentoOK');

        if (nColOK == NewCol)
            fg2.Editable = true;
        else
            fg2.Editable = false;
    }
}

function js_fg2_modalatendimentoEnterCell(grid) {
    var sResumo = fg2.TextMatrix(fg2.Row, getColIndexByColKey(fg2, 'Resumo'));
    var nPesAtendimentoID = fg2.TextMatrix(fg2.Row, getColIndexByColKey(fg2, 'PesAtendimentoID'));

    txtAtendimentosAnteriores.value = sResumo;

    atendimentosAnterioresReadOnly();

    //btnGravar.disabled = true;

    fg4.Rows = 1;
    fillGridData4(nPesAtendimentoID);
    fg2.focus();

    glb_validRow = grid.row;
    glb_validCol = grid.col;
}

function proximoAgendar() {
    var nAtendimento = (chkAtivo.checked ? 281 : 282);
    var sValueBtnAgendar = '';

    sValueBtnAgendar = valueBtnAgendar(false);

    btnProximo.value = (nAtendimento == 281 ? 'Pr�ximo' : sValueBtnAgendar);
    btnProximo.title = (nAtendimento == 281 ? '' : 'Agendar atendimentos futuros');

    if (btnIniciarAtendimento.state == 2) {
        if (btnProximo.state == 0) {
            selReagendamento.value = 15;
            selReagendamento_onchange();
        }
        else {
            selReagendamento.value = 0;
            selReagendamento_onchange();
        }
    }
    else {
        chkOk.checked = (nAtendimento == 282);
        chkOk.disabled = (nAtendimento == 282);
    }
}

function salvaDadosSelecionados() {
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['selServicos', selServicos.value];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['selData', selData.value];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['selProprietario', selProprietario.value];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['selOferta', selOferta.value];

    for (i = 0; i < selEstados.length; i++) {
        if (selEstados.options[i].selected == true)
            glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['selEstados', selEstados.options[i].value];
    }

    for (i = 0; i < selClassificacao.length; i++) {
        if (selClassificacao.options[i].selected == true)
            glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['selClassificacao', selClassificacao.options[i].value];
    }

    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['txtPesquisa', txtPesquisa.value];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['chkAnalitico', chkAnalitico.checked];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['chkAtivo', chkAtivo.checked];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['chkCredito', chkCredito.checked];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['chkAtender1', chkAtender1.checked];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['chkAtender2', chkAtender2.checked];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['chkDetalhes1', chkDetalhes1.checked];
    glb_aDadosSelecionados[glb_aDadosSelecionados.length] = ['chkDetalhes2', chkDetalhes2.checked];

    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_aDadosSelecionados=' + glb_aDadosSelecionados);
}

function carregaDadosSelecinados() {
    var i = 0;
    var j = 0;
    var k = 0;
    var l = 0;
    var teste = null;
    var field = '';
    var fieldOld = '';
    var selfield = null;

    for (i = 0; i < glb_aDadosSelecionados.length; i++) {
        field = glb_aDadosSelecionados[i][0];

        if ((field.substr(0, 3) == 'sel') && (fieldOld != field))
            deselectAllList(eval(field));

        for (j = 0; j <= [glb_aDadosSelecionados[i][j]].length; j++) {
            if (j > 0) {
                if (field.substr(0, 3) == 'sel') {
                    selfield = eval(field);

                    if (selfield.multiple) {
                        for (l = 0; l < selfield.length; l++) {
                            if (selfield.options(l).value == glb_aDadosSelecionados[i][j])
                                eval(selfield.options(l).selected = true);
                        }
                    }
                    else
                        selOptByValueInSelect(getHtmlId(), field, glb_aDadosSelecionados[i][j]);
                }
                else if (field.substr(0, 3) == 'txt')
                    eval(field + '.value=\'' + glb_aDadosSelecionados[i][j].toString() + '\'');
                else if (field.substr(0, 3) == 'chk')
                    eval(field + '.checked=' + glb_aDadosSelecionados[i][j].toString() + '');
            }
        }

        fieldOld = field;
    }

    if (selProprietario.value != 0)
        lblProprietario.innerText = 'Propriet�rio ' + selProprietario.value;

    glb_nSelProprietarioValue = selProprietario.value;
}

function deselectAllList(CONTROL) {
    for (var i = 0; i < CONTROL.length; i++) {
        CONTROL.options[i].selected = false;
    }
}

function chkDetalhes1_onclick() {
    if (chkDetalhes1.checked)
        chkDetalhes2.checked = false;

    visibilityControls();
}

function chkDetalhes2_onclick() {
    if (chkDetalhes2.checked)
        chkDetalhes1.checked = false;
    else {
        glb_nEmaClienteID = null;
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nEmaClienteID=null');
    }
     
    visibilityControls();

    //if (chkDetalhes2.checked) {
    //    var nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));
    //    fillGridData3(nParceiroID);
    //}
}

function chkPendente_onclick() {

    // S� lista todos os clientes que tiverem atendimentos pendentes
    if (chkPendente.checked) {
        chkCliente.disabled = false;
    }
    else {
        chkCliente.checked = true;
        chkCliente.disabled = true;
    }
        
    var nParceiroID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID*'));
    
    fillGridData2(nParceiroID);
}

function valueBtnAgendar(setValue) {
    var sValue = 'Agendar...';
    btnProximo.state = 1;

    // ver aqui se est� em modo ativo, se sim, tem de se chamar 'proximo'
    if (chkAtivo.checked)
        sValue = 'Pr�ximo';

    else if (fg.Rows > 2) {

        var dtAgendamento = getCellValueByColKey(fg, 'dtAgendamento*', fg.row);

        if (dtAgendamento != null) {

            var dia = dtAgendamento.substr(0, 2);
            var mes = dtAgendamento.substr(3, 2);

            dtAgendamento = mes + '/' + dia + dtAgendamento.substr(5, 11);

            dtAgendamento = new Date(dtAgendamento.toString().replace(/-/g, "/"));

            var dtAtual = new Date();

            if (dtAgendamento < dtAtual) {
                sValue = 'Adiar...';
                btnProximo.state = 0;
            }
            else if (dtAgendamento > dtAtual) {
                sValue = 'Reagendar...';
                btnProximo.state = 2;
            }
        }
    }

    if (setValue)
        btnProximo.value = sValue;
    else
        return sValue;
}

function trataObservacao(nEmailID) {
    //<#1070-OK> <#1075>

    var tag = '<#' + nEmailID + '>';
    var tagOk = '<#' + nEmailID + '-OK>';

    if (glb_nObservacoes == null)
        glb_nObservacoes = tag;
    else if ((glb_nObservacoes.indexOf(tag) < 0) && (glb_nObservacoes.indexOf(tagOk) < 0))
        glb_nObservacoes = glb_nObservacoes + ' ' + tag;

    sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "glb_nObservacoes='" + glb_nObservacoes + "'");

    glb_bRegistraObservacao = false;
}

function verificaAtendimentosAnteriores(nPessoaID) {
    setConnection(dsoAtendimentosAnteriores);

    dsoAtendimentosAnteriores.SQL = 'SELECT COUNT(1) AS QuantidadeAtendimentos ' +
                                        'FROM Pessoas_Atendimentos WITH(NOLOCK) ' +
                                        'WHERE ((PessoaID = ' + nPessoaID.toString() + ') AND (ISNULL(AtendimentoOK, 0) = 0) AND (PesAtendimentoID <> ' + glb_nPesAtendimentoID + '))';
                                        //'WHERE ((PessoaID = ' + nPessoaID.toString() + ') AND (AtendimentoOK = 0) AND (UsuarioID = ' + glb_USERID.toString() + '))';

    dsoAtendimentosAnteriores.ondatasetcomplete = verificaAtendimentosAnteriores_DSC;
    dsoAtendimentosAnteriores.Refresh();
}

function verificaAtendimentosAnteriores_DSC() {
    if (!((dsoAtendimentosAnteriores.recordset.BOF) || (dsoAtendimentosAnteriores.recordset.EOF))) {
        if ((dsoAtendimentosAnteriores.recordset['QuantidadeAtendimentos'].value != null) && (dsoAtendimentosAnteriores.recordset['QuantidadeAtendimentos'].value) != '')
            glb_nQuantidadeAtendimentosPendentes = dsoAtendimentosAnteriores.recordset['QuantidadeAtendimentos'].value;
    }
}