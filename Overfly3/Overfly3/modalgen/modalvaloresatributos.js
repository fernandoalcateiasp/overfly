/********************************************************************
modalvaloresatributos.js

Library javascript para o modalvaloresatributos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_LASTLINESELID = '';

// Dsos genericos para banco de dados
var dsoGen01 = new CDatatransport("dsoGen01");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    getDataInServer();
        
    // mostrar a janela movido para o final de preenchimento do grid
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I', null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Valores dos Atributos', 1);
    
    loadDataAndTreatInterface();    
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
        
    // Ajusta botoes OK/Cancel para esta janela
    // O botao cancela nao e usado neste asp
    with (btnCanc.style)
    {
        visibility = 'hidden';
        width = 0;
        height = 0;
    }
    
    btnCanc.disabled = true;
    
    // Por default o botao OK vem destravado e muda de posicao
    btnOK.disabled = false;
    // move o btnOK de posicao e muda seu caption
    btnOK.value = 'Fechar';
    btnOK.style.visibility = 'hidden';
    
    with (btnOK.style)
    {
        left = (widthFree - parseInt(btnOK.currentStyle.width, 10)) / 2;
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - 2 * ELEM_GAP;    
        height = parseInt(btnOK.currentStyle.top, 10) +
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Obtem os dados do grid no servidor
********************************************************************/
function getDataInServer()
{
    var strSQL ;
    
    // Form RelPesRec
    if ( glb_nFormID == 1130 )
    {
		strSQL = 'SELECT DISTINCT c.Fantasia AS Empresa, e.ItemID AS ID, e.ItemMasculino AS Atributo, ' +
					'dbo.fn_Direitos_Valor(a.SujeitoID, c.PessoaID, a.SujeitoID, e.ItemID, 1, GETDATE()) AS Minimo, ' +
					'dbo.fn_Direitos_Valor(a.SujeitoID, c.PessoaID, a.SujeitoID, e.ItemID, 0, GETDATE()) AS Maximo  ' +
				 'FROM RelacoesPesRec a WITH(NOLOCK), RelacoesPesRec_Perfis b WITH(NOLOCK), Pessoas c WITH(NOLOCK), Recursos_Atributos d WITH(NOLOCK), TiposAuxiliares_Itens e WITH(NOLOCK) ' +
				 'WHERE (a.RelacaoID = ' + glb_nRelacaoID  + ' AND a.RelacaoID = b.RelacaoID AND b.EmpresaID = c.PessoaID AND ' +
					'b.PerfilID = d.PerfilID AND d.AtributoID = e.ItemID) ' +
				 'ORDER BY c.Fantasia, e.ItemID ';
	}
    // Form Recursos
    else if ( glb_nFormID == 1110 )
    {
		strSQL = 'SELECT DISTINCT d.Fantasia AS Empresa, c.ItemID AS ID, c.ItemMasculino AS Atributo, '+
					'dbo.fn_Direitos_Valor(-b.PerfilID, a.SujeitoID, -5, c.ItemID, 1, GETDATE()) AS Minimo, ' +
					'dbo.fn_Direitos_Valor(-b.PerfilID, a.SujeitoID, -5, c.ItemID, 0, GETDATE()) AS Maximo ' +
			     'FROM RelacoesPesRec a WITH(NOLOCK), Recursos_Atributos b WITH(NOLOCK), TiposAuxiliares_Itens c WITH(NOLOCK), Pessoas d WITH(NOLOCK) ' +
                 'WHERE (a.TipoRelacaoID = 12 AND a.EstadoID = 2 AND a.ObjetoID = 999 AND ' +
                    'b.PerfilID = ' + glb_nRecursoID + ' AND b.AtributoID = c.ItemID AND a.SujeitoID = d.PessoaID) ' +
			     'ORDER BY d.Fantasia, c.ItemID';
	}
    
    setConnection(dsoGen01);
    
    dsoGen01.SQL = strSQL;
    dsoGen01.ondatasetcomplete = fillModalGrid_DSC;
    dsoGen01.refresh();
}

/********************************************************************
Preenchimento do grid de impostos
********************************************************************/
function fillModalGrid_DSC() {
    var i;
    
    fg.Redraw = 0;
    
    fg.Editable = false;

    headerGrid(fg,['Empresa',
                   'ID',
                   'Atributo',
                   'M�nimo',
                   'M�ximo', '_calc_LineID_10'],[5]);
    
    fillGridMask(fg,dsoGen01,['Empresa',
                              'ID',
                              'Atributo',
                              'Minimo',
                              'Maximo',
                              '_calc_LineID_10'],
                              ['','','','','',''],
                              ['','','','###,###,###,##0.00','###,###,###,##0.00','']);
       
    alignColsInGrid(fg,[3,4]);                           

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);

	// Alterar o tipo de merge para o amigo Dudu
	//fg.MergeCells = 4;
    fg.MergeCells = 1;
    
    fg.MergeCol(0) = true;
    
    fg.ExplorerBar = 5;

	fg.Editable = false;
	
	for (i=1; i<fg.Rows; i++)
		fg.TextMatrix(i, getColIndexByColKey(fg, '_calc_LineID_10')) = i;

    fg.Redraw = 2;
            
    with (modalvaloresatributosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no grid
    fg.focus();
}

function fg_modalValoresAtributos_BeforeSort()
{
	if (fg.Row > 0)
        glb_LASTLINESELID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, '_calc_LineID_10'));
    else
        glb_LASTLINESELID = '';
}

function fg_modalValoresAtributos_AfterSort()
{
	var i;
	var colIndex = getColIndexByColKey(fg, '_calc_LineID_10');
    
    if ( (glb_LASTLINESELID != '') && (fg.Rows > 1) )
    {
        for (i=1; i<fg.Rows; i++)
        {
            if (fg.TextMatrix(i, colIndex) == glb_LASTLINESELID)
            {
                fg.TopRow = i;
                fg.Row = i;        
                break;
            }    
        }
    }
    
    for(i=0; i<fg.Cols; i++)
    {
		if ( i != fg.Col )
			fg.MergeCol(i) = false;
		else	
			fg.MergeCol(i) = true;
    }
}