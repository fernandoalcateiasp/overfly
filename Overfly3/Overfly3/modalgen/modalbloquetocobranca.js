/********************************************************************
modalbloquetocobranca.js

Library javascript para o modalbloquetocobranca.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_nCurrBoletoIndex = 0;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
FUNCOES GERAIS:

window_onload()
btn_onclick(ctl)
adjustFrame()
showDocumentFrame(action)
setupPage()
loadPageFromServerInFrame()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES
/********************************************************************
Window on Load - Configura o html
********************************************************************/
function window_onload()
{
	// garante o caret nao permanecer em campo do pesqlist
	if (glb_sCaller == 'PL')
	{
		sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'window.focus()');
		sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'fg.focus()');
	}	

	if ( glb_aRegistroID.length == 1 )
	{
		btnBoleto.style.visibility = 'hidden';
		btnBoleto.disabled = true;
	}	

    window_onload_1stPart();

    // trava a interface
    lockControlsInModalWin(true);
    
    var elem, elemBase;
	var elemToFocus;
    var aEmpresaData;
    var frameRect;
    
    // ajusta o body do html
    with (modalbloquetocobrancaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
    
    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
		frameRect[1] += 3;
		moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }
    
    // ajusta o frame de documento
	adjustFrame();
	
	if ( glb_aRegistroID != null )
	{
		// Reposiciona os botoes OK e boleto se for o caso
		if ( glb_aRegistroID.length > 1 ) 
		{
			elem = btnOK;
			with (elem.style)
			{
				left = elem.offsetLeft + (elem.offsetWidth / 2) + ELEM_GAP;
			}
			
			elem = btnBoleto;
			elemBase = btnOK;
			with (elem.style)		
			{
				width = elemBase.offsetWidth;
				height = elemBase.offsetHeight;
				top = elemBase.offsetTop;
				left = elemBase.offsetLeft - elem.offsetWidth - (2 * ELEM_GAP);
				visibility = 'inherit';
			}
		}
	
		glb_nCurrBoletoIndex = 0;
			
		// carrega o primeiro boleto
		loadPageFromServerInFrame(null);
	}
	else
	{
		// trava a interface
		lockControlsInModalWin(false);
		btnOK.disabled = true;
	}
	
	elem = chkReciboEntrega;
	elemBase = btnOK;
	with (elem.style)
	{
		left = ELEM_GAP - 3;
		top = elemBase.offsetTop;
		width = 8 * 3;
		height = 8 * 3;
	}
	elem.onclick = chkReciboEntrega_onclick;
			
	elem = lblReciboEntrega;
	elemBase = chkReciboEntrega;
	with (elem.style)
	{
		left = elemBase.offsetLeft + elemBase.offsetWidth + (ELEM_GAP /2);
		top = elemBase.offsetTop + 5;
		width = 'auto';
	}
	elem.onclick = lblReciboEntrega_onclick;
		
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // sempre dar foco, no minimo na janela e em um controle da janela
    window.focus();

    elemToFocus = btnCanc;

	if ( !elemToFocus.disabled && (elemToFocus.currentStyle.visibility != 'hidden') )
		elemToFocus.focus();
}    

/********************************************************************
Usuario clicou o label do checkbox de recibo de entrega
********************************************************************/
function lblReciboEntrega_onclick()
{
	if ( chkReciboEntrega.disabled )
		return true;
		
	chkReciboEntrega.checked = ! chkReciboEntrega.checked;	
	
	chkReciboEntrega_onclick();
}

/********************************************************************
Usuario clicou o checkbox de recibo de entrega
********************************************************************/
function chkReciboEntrega_onclick()
{
	loadPageFromServerInFrame(chkReciboEntrega);	
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        // btnCanc.focus();
        ;
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        // sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null );
        
        imprimirConteudo();
    }
    else if (ctl.id == btnCanc.id )
    {
		// 2. O usuario clicou o botao Fechar
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
	}
	else if ( ctl.id == btnBoleto.id )
	{
		// carrega boleto
		loadPageFromServerInFrame(btnBoleto);
	}
}

/********************************************************************
Ajusta o frame que carrega as paginas e carrega a primeira pagina
********************************************************************/
function adjustFrame()
{
	var oFrame = null;
	var hGap = ELEM_GAP;
	var vGap = ELEM_GAP;
	var i;
	var coll;

    // todos os iframes
    coll = document.all.tags('IFRAME');

    for (i=0;i<coll.length; i++)
    {
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);
			
			with (oFrame.style)
			{
				visibility = 'hidden';
				left = hGap;
				top = parseInt(divMod01.currentStyle.top, 10) + 
				      parseInt(divMod01.currentStyle.height, 10) + vGap;
				width = parseInt(divMod01.currentStyle.width, 10) - (2 * hGap);
				height = ( parseInt(btnOK.currentStyle.top, 10) -
						   parseInt(oFrame.currentStyle.top, 10) - vGap);

				backgroundColor = 'white';
				
				borderStyle = 'inset';
				borderWidth = 2;
			}
						
			break;
        }
    }
}

/********************************************************************
Mostra ou esconde o frame que carrega o documento
********************************************************************/
function showDocumentFrame(action)
{
	var coll, i, oFrame;
	
	// todos os iframes
    coll = document.all.tags('IFRAME');
    
    for (i=0;i<coll.length; i++)
    {
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);
			
			with (oFrame.style)
			{
				if ( action )
					visibility = 'inherit';
				else	
					visibility = 'hidden';
			}
			
			break;
        }
    }
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	secText(glb_sTitulo, 1);
	
	btnOK.value = 'Imprimir';
	btnOK.disabled = true;
	centerBtnInModal(btnOK);

	btnCanc.style.height = parseInt(btnOK.currentStyle.height, 10);
	btnCanc.value = 'Fechar';
	btnCanc.style.visibility = 'hidden';
	centerBtnInModal(btnCanc);
}

/********************************************************************
Carrega o boleto do servidor.

Parametros
	objCaller	 - null na carga da janela
	             - btnBoleto
	             - chkReciboEntrega
********************************************************************/
function loadPageFromServerInFrame(objCaller)
{
	var i, coll, oFrame;
	var strPars = '';
	var nCurrBoletoIndex = glb_nCurrBoletoIndex;

	if ( objCaller != null )
	{
		if ( objCaller.id == btnBoleto.id )
		{
			if ( glb_nCurrBoletoIndex < (glb_aRegistroID.length - 1) )
				glb_nCurrBoletoIndex++;
			else	
				glb_nCurrBoletoIndex = 0;
		}	
	}
	
	valueOfBtnBoleto();

    // todos os iframes
    coll = document.all.tags('IFRAME');

    // Carrega novo documento se for o caso
	for (i=0;i<coll.length; i++)
	{
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);
			oFrame.tabIndex = -1;
			strPars = '?nRegistroID=' + escape(glb_aRegistroID[glb_nCurrBoletoIndex]);
			
			if (!chkReciboEntrega.checked)
				strPars += '&nNotRecEntrega=' + escape(1);

			oFrame.src = SYS_PAGESURLROOT + '/modalgen/bloquetocobranca.asp' + strPars;
	
			break;
	    }
	}
}

/********************************************************************
Ajusta interface do botao btnBoleto
********************************************************************/
function valueOfBtnBoleto()
{
	var nBolNumber = (glb_nCurrBoletoIndex + 1);
	
	if (nBolNumber > glb_aRegistroID.length)
		nBolNumber = 1;

	btnBoleto.value = 'Boleto ' + nBolNumber.toString() +
			          ' / ' + 
	                  (glb_aRegistroID.length).toString();
}

/********************************************************************
Carrega as paginas do servidor.

Parametros:
	nDocumentoID - id do registro a carregar
	sTitulo - Titulo da Janela
********************************************************************/
function imprimirConteudo()
{
	window.frames(0).imprimirBoleto();
	
    // esta funcao destrava o html contido na janela modal
    lockControlsInModalWin(false);

}