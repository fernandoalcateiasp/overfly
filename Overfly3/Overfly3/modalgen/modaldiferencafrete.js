/********************************************************************
modaldiferencafrete.js

Library javascript para o modaldiferencafrete.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var dsoPesq = new CDatatransport("dsoPesq");
var dsoSaldoTotal = new CDatatransport("dsoSaldoTotal");
var dsoSaldoParcial = new CDatatransport("dsoSaldoParcial");

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('modaldiferencafreteBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // coloca foco no campo apropriado
    if (document.getElementById('txtdtInicio').disabled == false)
        txtdtInicio.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    // texto da secao01
    secText('Diferen�a Parcelas/Faturas', 1);

    // ajusta elementos da janela
    var elem;
    var temp;

    // ajusta o divDiferenca
    elem = window.document.getElementById('divDiferenca');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 80;
    }

    // txtSaldo (caixa de texto do saldo geral)
    elem = window.document.getElementById('txtSaldo');
    elem.maxLength = 15;  // aceita vinte e hum caracteres de digitacao
    with (elem.style) {
        left = 0;
        top = 16;
        width = ((elem.maxLength + 8) * FONT_WIDTH - 4);
        heigth = 24;

        txtSaldo.onkeypress = verifyNumericEnterNotLinked;
        txtSaldo.setAttribute('verifyNumPaste', 1);
        txtSaldo.setAttribute('thePrecision', 11, 1);
        txtSaldo.setAttribute('theScale', 2, 1);
        txtSaldo.setAttribute('minMax', new Array(0, SaldoTotal()), 1);
        txtSaldo.onfocus = selFieldContent;
        txtSaldo.maxLength = 15;
    }

    // lblSaldoParcial
    elem = window.document.getElementById('lblSaldoParcial');
    with (elem.style) {
        top = 44;
    }

    // txtSaldoParcial
    elem = window.document.getElementById('txtSaldoParcial');
    elem.maxLength = 15;  // aceita vinte e hum caracteres de digitacao
    with (elem.style) {
        top = 60;
        left = 0;
        width = ((elem.maxLength + 8) * FONT_WIDTH - 4);
        heigth = 24;

        txtSaldoParcial.onkeypress = verifyNumericEnterNotLinked;
        txtSaldoParcial.setAttribute('verifyNumPaste', 1);
        txtSaldoParcial.setAttribute('thePrecision', 11, 1);
        txtSaldoParcial.setAttribute('theScale', 2, 1);
        txtSaldoParcial.setAttribute('minMax', new Array(0, SaldoTotal()), 1);
        txtSaldoParcial.onfocus = selFieldContent;
        txtSaldoParcial.maxLength = 15;

/* antigo
        txtSaldoParcial.onkeypress = verifyNumericEnterNotLinked;
        txtSaldoParcial.setAttribute('thePrecision', 10, 1);
        txtSaldoParcial.setAttribute('theScale', 0, 1);
        txtSaldoParcial.setAttribute('verifyNumPaste', 1);
        txtSaldoParcial.setAttribute('minMax', new Array(1, 9999999999), 1);
        SaldoParcial();
*/
    }

    // lbldtInicio
    elem = window.document.getElementById('lbldtInicio');
    with (elem.style) {
        top = 44;
        left = 300;
    }

    // txtdtInicio
    elem = window.document.getElementById('txtdtInicio');
    elem.maxLength = 10;  // aceita vinte e hum caracteres de digitacao
    with (elem.style) {
        top = 60;
        left = 300;
        width = 100;
        heigth = 24;

        txtdtInicio.onkeypress = verifyDateTimeNotLinked;
        txtdtInicio.setAttribute('verifyNumPaste', 1);
        txtdtInicio.setAttribute('thePrecision', 10, 1);
        txtdtInicio.setAttribute('theScale', 0, 1);
        txtdtInicio.setAttribute('minMax', new Array(0, 9999999999), 1);
        txtdtInicio.onfocus = selFieldContent;
        txtdtInicio.maxLength = 10;
    }

    // lbldtFim
    elem = window.document.getElementById('lbldtFim');
    with (elem.style) {
        top = 44;
        left = 410;
    }

    // txtdtFim
    elem = window.document.getElementById('txtdtFim');
    elem.maxLength = 10;
    with (elem.style) {
        top = 60;
        left = 410;
        width = 100;
        heigth = 24;

        txtdtFim.onkeypress = verifyDateTimeNotLinked;
        txtdtFim.setAttribute('verifyNumPaste', 1);
        txtdtFim.setAttribute('thePrecision', 10, 1);
        txtdtFim.setAttribute('theScale', 0, 1);
        txtdtFim.setAttribute('minMax', new Array(0, 9999999999), 1);
        txtdtFim.onfocus = selFieldContent;
        txtdtFim.maxLength = 10;
    }

     // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    btnFindPesquisa.disabled = true;

    with (elem.style) {
        top = 60;
        left = 520;
        width = 80;
        height = 24;
    }

    // ajusta o divParcela
    elem = window.document.getElementById('divParcela');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        top = parseInt(document.getElementById('divDiferenca').style.top) + parseInt(document.getElementById('divDiferenca').style.height) + ELEM_GAP;
        left = ELEM_GAP;
        width = 600;
        height = 410;
    }

    // Grid
    elem = document.getElementById('fgParcela');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divParcela').style.width);
        height = parseInt(document.getElementById('divParcela').style.height);
    
    }
    
    // Bot�es Ok/Cancelar
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    startGridInterface(fgParcela);

    //headerGrid(grid,sHeader,aColInvisible)
    headerGrid(fgParcela, ['dtRegistro', 'Tipo', 'Valor'], []);

    fgParcela.Redraw = 2;

    //startPesq();
}


function txtdtInicio_ondigit(ctl) {
    changeBtnState(ctl.value);

    if (trimStr(txtdtInicio.value) != '')
        changeBtnState(txtdtInicio.value);

    if (event.keyCode == 13)
        btn_onclick(btnFindPesquisa);
}

function txtdtFim_ondigit(ctl) {
    changeBtnState(ctl.value);

    if (trimStr(txtdtFim.value) != '')
        changeBtnState(txtdtFim.value);

    if (event.keyCode == 13)
        btn_onclick(btnFindPesquisa);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    
    // 1. Bot�o OK
    if (ctl.id == btnOK.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);
    // 2. Botao Cancela
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_PL', null);
    else if (ctl.id = 'btnFindPesquisa') 
    {
        txtdtInicio.value = trimStr(txtdtInicio.value);
        txtdtFim.value = trimStr(txtdtFim.value);

        if ((txtdtInicio.value != "" && txtdtFim.value != "") &&
            ((chkDataEx(txtdtInicio.value) == true) && (chkDataEx(txtdtFim.value) == true)))
            changeBtnState(txtdtInicio.value);
        else 
        {
            window.top.overflyGen.Alert('Data inv�lida.');
            btnFindPesquisa.disabled = true;
        }

        if (btnFindPesquisa.disabled) {
            lockControlsInModalWin(false);
            return;
        }

        startPesq();
    }
}

function startPesq() {
    lockControlsInModalWin(true);
    
    var strPars = new String();
    var nEmpresaID = getCurrEmpresaData();

    strPars = '?';
    strPars += 'nEmpresaID=';
    strPars += escape(nEmpresaID[0].toString());
    strPars += '&ndtInicio=';
    strPars += escape((normalizeDate_DateTime(txtdtInicio.value, true)).toString());
    strPars += '&ndtFim=';
    strPars += escape((normalizeDate_DateTime(txtdtFim.value, true)).toString());
    strPars += '&nSaldo=';

    setConnection(dsoPesq);

    //Tipo, dtRegistro, Valor
    dsoPesq.URL = SYS_ASPURLROOT + '/serversidegenEx/listadiferencafrete.aspx' + strPars;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() {

    var dtFormat;
    
    startGridInterface(fgParcela);
    fgParcela.FrozenCols = 0;

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_GridIsBuilding = true;

    //headerGrid(grid,sHeader,aColInvisible)
    headerGrid(fgParcela, ['dtRegistro', 'Tipo', 'Valor'], []);

    glb_GridIsBuilding = false;

    fillGridMask(fgParcela, dsoPesq, ['dtRegistro', 'Tipo', 'Valor'], ['99/99/9999', '', ''], [dTFormat, '', '(###,###,###.##)']);

    // Linha de Totais
    gridHasTotalLine(fgParcela, 'Total', 0xC0C0C0, null, true,
                        [[getColIndexByColKey(fgParcela, 'Valor'), '###,###,###,##0.00', 'S']]);


    fgParcela.Redraw = 0;
    fgParcela.AutoSizeMode = 0;
    fgParcela.AutoSize(0, fgParcela.Cols - 1);
    fgParcela.ExplorerBar = 5;

    alignColsInGrid(fgParcela, [2]);

    for (i = 2; i < fgParcela.Rows; i++) {
        if (fgParcela.ValueMatrix(i, getColIndexByColKey(fgParcela, 'Valor')) < 0) {
            fgParcela.Select(i, getColIndexByColKey(fgParcela, 'Valor'), i, getColIndexByColKey(fgParcela, 'Valor'));
            fgParcela.FillStyle = 1;
            fgParcela.CellForeColor = 0X0000FF;
        }
    }

    // Merge de Colunas
    fgParcela.MergeCells = 1;
    fgParcela.MergeCol(0) = true;

    fgParcela.Redraw = 2;

    // Mostra saldo parcial
    SaldoParcial();

    // Destrava controles da modal
    lockControlsInModalWin(false);

    window.focus();
}


function SaldoTotal() {
    lockControlsInModalWin(true);

    //var nEmpresaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[' + '\'' + 'EmpresaID' + '\'' + '].value');

    var strPars = new String();
    var nEmpresaID = getCurrEmpresaData();

    setConnection(dsoSaldoTotal);

    strPars = '?';
    strPars += 'nEmpresaID=';
    strPars += escape(nEmpresaID[0].toString());
    strPars += '&ndtInicio=';
    strPars += '&ndtFim=';
    strPars += '&nSaldo=1';

    dsoSaldoTotal.URL = SYS_ASPURLROOT + '/serversidegenEx/listadiferencafrete.aspx' + strPars;
    dsoSaldoTotal.ondatasetcomplete = dsoSaldoTotal_DSC;
    dsoSaldoTotal.Refresh();
}

function dsoSaldoTotal_DSC() {
    var sResultado = dsoSaldoTotal.recordset['Resultado'].value;

    lockControlsInModalWin(false);
    window.focus();
    btnFindPesquisa.disabled = true;
    
    // Coloca o foco de volta no txtdtInicio
    if (document.getElementById('txtdtInicio').disabled == false)
        txtdtInicio.focus();

    txtSaldo.value = sResultado;
}

function SaldoParcial() {
    lockControlsInModalWin(true);

    if (txtdtInicio.value != "" && txtdtFim.value != "") {
        var strPars = new String();
        var nEmpresaID = getCurrEmpresaData();

        setConnection(dsoSaldoParcial);

        strPars = '?';
        strPars += 'nEmpresaID=';
        strPars += escape(nEmpresaID[0].toString());
        strPars += '&ndtInicio=';
        strPars += escape((normalizeDate_DateTime(txtdtInicio.value, true)).toString());
        strPars += '&ndtFim=';
        strPars += escape((normalizeDate_DateTime(txtdtFim.value, true)).toString());
        strPars += '&nSaldo=1';

        dsoSaldoParcial.URL = SYS_ASPURLROOT + '/serversidegenEx/listadiferencafrete.aspx' + strPars;
        dsoSaldoParcial.ondatasetcomplete = dsoSaldoParcial_DSC;
        dsoSaldoParcial.Refresh();
    }
    else {
        txtSaldoParcial.value = '';
        return;
    }
}

function dsoSaldoParcial_DSC() {
    var sResultado = dsoSaldoParcial.recordset['Resultado'].value;

    lockControlsInModalWin(false);
    window.focus();

    if (sResultado != null)
        txtSaldoParcial.value = sResultado;
    else
        txtSaldoParcial.value = '';

    // Coloca o foco de volta no txtdtInicio
    if (document.getElementById('txtdtInicio').disabled == false)
        txtdtInicio.focus();
}