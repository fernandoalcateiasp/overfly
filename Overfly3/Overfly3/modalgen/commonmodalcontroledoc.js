/********************************************************************
commonmodalcontroledoc.js

Library javascript para o modalcontroledocumento.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_editMode = false;
var glb_sTitleWindow = '';
var glb_windowDocumentos = null;
var glb_nOrdemID;
//var glb_VersaoReset = true;
var glb_VersaoReset = true;
var glb_fillHTML = null;
var glb_nB4I;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
FUNCOES GERAIS:

window_onload()
window_onload_finalPart()
setupPage()
loadPageFromServerInFrame_Aut(nDocumentoID, sAnchorID, nTipoDocumento, bNavInSameDoc)
adjustFrameAndLoad_1st_Documentos()
resetInterfaceFields()
btn_onclick(ctl)
dataToReturn()
showDocumentFrame(action)
saveCurrentPosInNavList(nDocumentoID, sAnchorID, nTipoDocumento, nAuto)
removeCurrentPosInNavList()
btnVoltarStatusAndHint()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Window on Load - Configura o html
********************************************************************/
function window_onload() {
    // garante o caret nao permanecer em campo do pesqlist
    if (glb_sCaller == 'PL') {
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'window.focus()');
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'fg.focus()');
    }

    glb_sAnchorDest = glb_sAnchorID__;

    // initializa lista de documentos navegados
    glb_oNavList.initializeList();

    if ((glb_sAnchorID__ != null) && (glb_sAnchorID__ != ''))
        glb_oNavList.sAnchorInFirstPage = glb_sAnchorID__;

    window_onload_1stPart();

    txtdtEmissao.readOnly = true;
    txtTotPags.readOnly = true;

    btnVoltar.disabled = true;

    btnCancelarEstudo.style.visibility = (glb_editMode ? 'inherit' : 'hidden');
    btnLimparAlteracoes.style.visibility = (glb_editMode ? 'inherit' : 'hidden');

    // trava a interface
    lockControlsInModalWin(true);

    //window_onload_prg();

    window_onload_finalPart();

    window_onload_prg();
}

/********************************************************************
Window on Load - Configura o html - parte final
********************************************************************/
function window_onload_finalPart() {
    var elemToFocus;
    var aEmpresaData;
    var frameRect;

    // ajusta o body do html
    with (modalcontroledocumentoBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
    glb_nEmpresaID = aEmpresaData[0];

    // configuracao inicial do html
    setupPage();

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 40;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    // ajusta o frame de documento e carrega o primeiro documento
    adjustFrameAndLoad_1st_Documentos();

    // carrega html de edi��o
    adjustFrameAndLoad_HTML();

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // sempre dar foco, no minimo na janela e em um controle da janela
    window.focus();

    elemToFocus = btnCanc;

    if (!elemToFocus.disabled && (elemToFocus.currentStyle.visibility != 'hidden'))
        elemToFocus.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    //setSecText();

    mountModalAccordingToContext();
}

/********************************************************************
Carrega as paginas do servidor.

Parametros:
	nDocumentoID - id do documento a carregar
	sAnchorID - navegacao automatica para um achor da pagina a ser
	            executado apos o carregamento
	nTipoDocumento - tipo do documento
	bNavInSameDoc - flag para executar o carregamento ou nao.
	                Eventualmente sera executada apenas uma navegacao
	                para un anchor
********************************************************************/
function loadPageFromServerInFrame_Aut(nDocumentoID, sAnchorID, nTipoDocumento, bNavInSameDoc) {
    var nAction;
    var oFrame = null;
    var hGap = ELEM_GAP;
    var vGap = ELEM_GAP;
    var strPars;
    var i;
    var bIsFirstDoc;
    var numElemInList;

    secText('', 1);

    if (nDocumentoID == null)
        nDocumentoID = glb_nDocumentoID;

    nAction = glb_oNavList.nDirection;

    if ((parseInt(glb_oNavList.qtyElemsInList(), 10) == 0) && (nAction == 0))
        nTipoDocumento = glb_nTipoDocToLoad;
        // CONFORME PEDIDO DO EDUARDO, EM 23/04 
        // O ELSE ABAIXO OBRIGA CARREGAR SEMPRE DOCUMENTOS EM ESTUDO
        // APOS O PRIMEIRO DOCUMENTO CARREGADO.
        // NA NAVEGACAO PARA TRAZ, O TIPO DO PRIMEIRO DOCUMENTO
        // TAMBEM SERA RESPEITADO
    else {
        // ALTERADO EM 11/09, PARA CARREGAR OBEDECENDO A SEGUINTE CONDICAO:
        // DOCUMENTO NA REV 0, CARREGA O ESTUDO
        // DOCUMENTO ACIMA DA REV 0, CARREGA O TEXTO
        // nTipoDocumento = -1;
        // ALTERADO EM 26/09, NAO MUDA MAIS O TIPO DE DOCUMENTO,
        // CONFORME LINHA ABAIXO
        // nTipoDocumento = 'NULL';

        // Se a navegacao e para traz e se o documento que vai ser
        // carregado e o primeiro documento que foi carregado
        // na navegacao para frente, carrega o documento com o mesmo
        // tipo usado para carregar o primeiro documento
        if (nAction == -1) {
            bIsFirstDoc = true;
            numElemInList = parseInt(glb_oNavList.qtyElemsInList(), 10);

            for (i = (numElemInList - 1) ; i >= 0; i--) {
                if ((glb_oNavList.aList[i][0] != null) && (glb_oNavList.aList[i][1] != null)) {
                    if (glb_oNavList.aList[i][0] != glb_nDocumentoID) {
                        bIsFirstDoc = false;
                        break;
                    }
                }
            }

            if (bIsFirstDoc)
                nTipoDocumento = glb_nTipoFirstDocToLoad;
        }
    }

    if (nAction == null)
        nAction = 0;

    // todos os iframes
    coll = document.all.tags('IFRAME');

    // Carrega novo documento se for o caso
    for (i = 0; i < coll.length; i++) {
        if ((coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO') {
            oFrame = coll.item(i);

            oFrame.tabIndex = -1;

            if (bNavInSameDoc == true) {
                // esta funcao destrava o html contido na janela modal
                lockControlsInModalWin(false);

                window.frames(0).forceAnchorNavByFrame(nAction, (sAnchorID == null ? '__0' : sAnchorID));
            }
            else {
                resetInterfaceFields();

                strPars = strParsToLoadPageFromServer(nDocumentoID, nTipoDocumento, sAnchorID, nAction);

                oFrame.src = SYS_PAGESURLROOT + '/modalgen/documentotemplate.asp' + strPars;
            }

            break;
        }
    }
}

/********************************************************************
Limpa campos da interface antes de carregar novo documento
********************************************************************/
function resetInterfaceFields() {
    clearComboEx(['selVersao']);
    //txtVersao.value = '';
    txtdtEmissao.value = '';
    txtTotPags.value = '';
}

/********************************************************************
Ajusta o frame que carrega as paginas e carrega a primeira pagina
********************************************************************/
function adjustFrameAndLoad_1st_Documentos() {
    var oFrame = null;
    var hGap = ELEM_GAP;
    var vGap = ELEM_GAP;
    var i;
    var coll;

    // todos os iframes
    coll = document.all.tags('IFRAME');

    for (i = 0; i < coll.length; i++) {
        if ((coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO') {
            oFrame = coll.item(i);

            with (oFrame.style) {
                visibility = 'hidden';
                left = hGap;
                top = parseInt(txtdtEmissao.currentStyle.top, 10) +
				      parseInt(txtdtEmissao.currentStyle.height, 10) + vGap;
                width = parseInt(divMod01.currentStyle.width, 10) - (2 * hGap);
                height = parseInt(btnOK.currentStyle.top, 10) +
						 parseInt(btnOK.currentStyle.height, 10) -
				         parseInt(oFrame.currentStyle.top, 10) + (hGap / 2);

                backgroundColor = 'white';
            }

            //oFrame.scrolling = 'yes';

            glb_nTipoFirstDocToLoad = glb_nTipoDocToLoad;

            glb_loadDocumentoTimer = window.setInterval('loadPageFromServerInFrame()', 100, 'JavaScript');

            break;
        }
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();

    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, dataToReturn());
    }
        // 2. O usuario clicou o botao Voltar
    else if (ctl.id == btnVoltar.id) {
        var nDocCurrentID;

        var v0;
        var v1;
        var v2;
        var v3;
        var relockInterface = false;

        v0 = glb_oNavList.currentInList()[0];
        v1 = glb_oNavList.currentInList()[1];
        v2 = glb_oNavList.currentInList()[2];

        if ((v0 == null) && (v1 == null) && (v2 == null)) {
            // forca navegacao para anchor de primeira pagina
            // se o anchor existe
            if (glb_oNavList.sAnchorInFirstPage != null) {
                var objToScroll;

                objToScroll = window.frames(0).glb_overHTMLMounter._getAnchor(glb_oNavList.sAnchorInFirstPage.toString());

                if (objToScroll)
                    window.frames(0).window.scrollTo(0, objToScroll.offsetTop);

                glb_oNavList.sAnchorInFirstPage = null;
            }
            else
                window.frames(0).window.scrollTo(0, 0);


            // esta funcao destrava o html contido na janela modal
            lockControlsInModalWin(false);

            removeCurrentPosInNavList();

            return;
        }

        // verifica se a navegacao a ser feita e um anchor automatico
        // ou seja, colocado por carregamento de pagina, se for deleta		
        if (glb_oNavList.currentInList()[3] == 1) {
            removeCurrentPosInNavList();

            v0 = glb_oNavList.currentInList()[0];
            v1 = glb_oNavList.currentInList()[1];
            v2 = glb_oNavList.currentInList()[2];

            if ((v0 == null) && (v1 == null) && (v2 == null)) {
                window.scrollTo(0, 0);

                // esta funcao destrava o html contido na janela modal
                lockControlsInModalWin(false);

                return;
            }
        }

        nDocCurrentID = window.frames(0).glb_nDocumentoID;

        v0 = glb_oNavList.currentInList()[0];
        v1 = glb_oNavList.currentInList()[1];
        v2 = glb_oNavList.currentInList()[2];
        v3 = (nDocCurrentID == glb_oNavList.currentInList()[0]);

        if (glb_oNavList.qtyElemsInList() > 0) {
            if (modalWinIsLocked()) {
                lockControlsInModalWin(false);
                relockInterface = true;
            }

            removeCurrentPosInNavList();

            if (relockInterface)
                lockControlsInModalWin(true);
        }

        glb_oNavList.nDirection = -1;

        loadPageFromServerInFrame(v0, v1, v2, v3);
    }
        // 3. Bota�o Editar / Visualizar
    else if (ctl.id == btnEditarVisualizar.id) {
        btnEditarVisualizar_onclick();
    }
        // 4. Bot�o Limpar
    else if (ctl.id == btnLimparAlteracoes.id) {
        btnLimparAlteracoes_onclick();
    }
        // 5. Bot�o Cancelar Estudo
    else if (ctl.id == btnCancelarEstudo.id) {
        btnCancelarEstudo_onclick();
    }
        // 6. O usuario clicou o botao Fechar
    else if (ctl.id == btnCanc.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, dataToReturn());
    }
}

/********************************************************************
Retorno de dados para o form ao fechar a modal
********************************************************************/
function dataToReturn() {
    return null;
}

/********************************************************************
Mostra ou esconde o frame que carrega o documento
********************************************************************/
function showDocumentFrame(action) {
    var coll, i;

    // todos os iframes
    coll = document.all.tags('IFRAME');

    for (i = 0; i < coll.length; i++) {
        if ((coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO') {
            oFrame = coll.item(i);

            with (oFrame.style) {
                if (action)
                    visibility = 'inherit';
                else
                    visibility = 'hidden';
            }

            break;
        }
    }
}

/********************************************************************
Grava na pilha de acoes do objeto que controla a navegacao

Parametros:
	nDocumentoID - ID do documento carregado
	sAnchorID - ID do anchor executado
	nTipoDocumento - Tipo do documento a ser carregado:
					< 0 -> Estudo
					null -> Atual
					> 0 -> Versao
	nAuto - 1 ou 0 . Informa se uma navegacao para anchor foi executada
	        por automaticamente (apos carregar uma pagina)
	        ou por uma acao do usuario.
********************************************************************/
function saveCurrentPosInNavList(nDocumentoID, sAnchorID, nTipoDocumento, nAuto) {
    if ((sAnchorID == null) || (sAnchorID == ''))
        sAnchorID = '__0';

    if (nAuto == null)
        nAuto = 0;

    if (glb_oNavList.nDirection >= 0)
        glb_oNavList.addAndMakeCurrentInList(new Array(nDocumentoID, sAnchorID, nTipoDocumento, nAuto),
		             'btnVoltarStatusAndHint()');
}

/********************************************************************
Remove na pilha de acoes do objeto que controla a navegacao.
E necessario sempre um registro null, null, null, 0 na pilha
********************************************************************/
function removeCurrentPosInNavList() {
    if (glb_oNavList.qtyElemsInList() == 0) {
        glb_oNavList.addAndMakeCurrentInList(new Array(null, null, null, 0));
        return null;
    }

    glb_oNavList.removeCurrentInList('btnVoltarStatusAndHint()');

    // a pilha nao pode ficar zerada
    if (glb_oNavList.qtyElemsInList() == 0)
        glb_oNavList.addAndMakeCurrentInList(new Array(null, null, null, 0));

    return null;
}

/********************************************************************
Habilita/Desabilita e controe o hint do botao btnVoltar
********************************************************************/
function btnVoltarStatusAndHint() {
    var nBtnCounter = 1;

    btnVoltar.disabled = true;
    btnVoltar.title = '';

    for (i = 0; i < glb_oNavList.qtyElemsInList() ; i++) {
        if ((glb_oNavList.aList[i][0] == null) && (glb_oNavList.aList[i][1] == null)) {
            btnVoltar.disabled = false;
            continue;
        }

        if (glb_oNavList.aList[i][3] == 0) {
            btnVoltar.disabled = false;

            nBtnCounter++;
        }
    }

    if (nBtnCounter >= 1)
        btnVoltar.title = (nBtnCounter).toString() + ' retorno' + (nBtnCounter > 1 ? 's' : '') + ' ' +
			'pendente' + (nBtnCounter > 1 ? 's' : '');

    if (btnVoltar.disabled) {
        btnVoltar.title = '';
    }
}

/********************************************************************
Habilita/Desabilita controle de edi��o do HTML exibido
********************************************************************/
function btnEditarVisualizar_onclick() {
    glb_editMode = !glb_editMode;

    if (glb_editMode) {
        btnEditarVisualizar.value = 'Visualizar';
        divHTML.style.visibility = 'visible';
        glb_nDocumentoID = selDocumento.options.item(selDocumento.selectedIndex).value;

        dsoGravaHTML_DSC();
    }
    else {
        btnEditarVisualizar.value = 'Editar';
        divHTML.style.visibility = 'hidden';

        var sbody = txtHTML.value;

        sbody = sbody.replace(/'/g, "''");

        if (glb_nOrdemID == 1)
            dsoGravaHTML.recordset['Estudo'].value = sbody;
        else if (glb_nOrdemID == 2)
            dsoGravaHTML.recordset['Texto'].value = sbody;

        dsoGravaHTML.SubmitChanges();
        dsoGravaHTML.ondatasetcomplete = dsoGravaHTML_DSC;
        dsoGravaHTML.Refresh();

        glb_windowDocumentos.selDocumento_onchange('btnEditarVisualizar', true);
    }
}

function dsoGravaHTML_DSC() {
    lockControlsInModalWin(false);
    configuraControles();

    glb_fillHTML = window.setInterval('fillTextAreaHTML()', 100, 'JavaScript');
}

function setSecText() {
    var sSecText = glb_sTitleWindow + (glb_editMode ? ' - Editar' : '');

    secText(sSecText, 1);
}

function adjustFrameAndLoad_HTML() {
    var oFrame = null;
    var hGap = ELEM_GAP;
    var vGap = ELEM_GAP;
    var i;
    var coll;

    // todos os iframes
    coll = document.all.tags('IFRAME');
    oFrame = coll.item(0);

    with (divHTML.style) {
        left = hGap;
        top = parseInt(txtdtEmissao.currentStyle.top, 10) +
				      parseInt(txtdtEmissao.currentStyle.height, 10) + vGap;
        width = parseInt(divMod01.currentStyle.width, 10) - (2 * hGap);
        height = parseInt(btnOK.currentStyle.top, 10) +
						 parseInt(btnOK.currentStyle.height, 10) -
				         parseInt(oFrame.currentStyle.top, 10) + (hGap / 2);

        backgroundColor = 'white';
    };

    with (txtHTML.style) {
        left = hGap;
        top = parseInt(txtdtEmissao.currentStyle.top, 10) +
				      parseInt(txtdtEmissao.currentStyle.height, 10) + vGap;
        width = parseInt(divMod01.currentStyle.width, 10) - (2 * hGap);
        height = parseInt(btnOK.currentStyle.top, 10) +
						 parseInt(btnOK.currentStyle.height, 10) -
				         parseInt(oFrame.currentStyle.top, 10) + (hGap / 2);

        backgroundColor = 'white';
    };
    divHTML.style.visibility = 'hidden';

    fillTextAreaHTML();
}

function fillTextAreaHTML(nDocumentoID) {
    if (glb_fillHTML != null) {
        window.clearInterval(glb_fillHTML);
        glb_fillHTML = null;
    }

    setConnection(dsoGravaHTML);

    if (!nDocumentoID > 0)
        nDocumentoID = glb_nDocumentoID;

    dsoGravaHTML.SQL = 'SELECT a.DocumentoID, a.EstadoID, a.Estudo, a.Texto FROM Documentos a WITH(NOLOCK) WHERE (a.DocumentoID = ' + nDocumentoID + ')';
    dsoGravaHTML.ondatasetcomplete = fillTextAreaHTML_DSC;
    dsoGravaHTML.Refresh();
}

function fillTextAreaHTML_DSC() {
    if (glb_nOrdemID == 1)
        txtHTML.value = dsoGravaHTML.recordset['Estudo'].value;
    else if (glb_nOrdemID == 2)
        txtHTML.value = dsoGravaHTML.recordset['Texto'].value;
}

function configuraControles() {
    // lockControlsInModalWin(false);
    //var nEstadoID = selDocumento.options.item(selDocumento.selectedIndex).getAttribute('EstadoID', 1);

    // Configura��es iniciais
    selDocumento.disabled = false;
    selVersao.disabled = false;
    btnVoltar.style.visibility = 'visible';
    btnLimparAlteracoes.style.visibility = 'hidden';
    btnCancelarEstudo.style.visibility = 'hidden';
    btnEditarVisualizar.style.visibility = 'visible';
    glb_nB4I = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrRightValue(\'SUP\',\'B4I\')');

    // Chamada pelo Bot�o 3
    if (glb_sCallOrigin2 == 'btn3') {
        btnEditarVisualizar.style.visibility = 'hidden';
    }
        // Chamada pelo Bot�o 4
    else {
        // Modo Visualizar
        if (!glb_editMode) {
            // Vers�o Atual
            if (glb_nOrdemID == 2) {
                // Direito do bot�o Editar
                if (glb_nB4I != 1) {
                    btnEditarVisualizar.style.visibility = 'hidden';
                }
            }
            // Vers�es Anteriores
            if (glb_nOrdemID == 3) {
                btnEditarVisualizar.style.visibility = 'hidden';
            }
        }
            // Modo Editar
        else {
            selDocumento.disabled = true;
            selVersao.disabled = true;
            btnVoltar.style.visibility = 'hidden';

            // Vers�o Estudo
            if (glb_nOrdemID == 1) {
                btnCancelarEstudo.style.visibility = 'visible';
                btnLimparAlteracoes.style.visibility = 'visible';
            }
        }
    }

    // Configura t�tulo da modal
    setSecText();
}

function btnCancelarEstudo_onclick() {
    var nDocumentoID = glb_nDocumentoID;
    var strPars = '';
    var nUsuarioID = getCurrUserID();

    var _retConf = window.top.overflyGen.Confirm('Deseja realmente cancelar o estudo?');

    if (_retConf == 0)
        return null;
    else if (_retConf == 1) {
        lockControlsInModalWin(true);

        setConnection(dsoCancelarEstudo);

        strPars = '?nDocumentoID=' + escape(nDocumentoID);
        strPars += '&nUsuarioID=' + escape(nUsuarioID);

        dsoCancelarEstudo.URL = SYS_ASPURLROOT + '/serversidegenEx/cancelarestudo.aspx' + strPars;
        dsoCancelarEstudo.ondatasetcomplete = btnCancelarEstudo_DSC;
        dsoCancelarEstudo.Refresh();
    }
}

function btnCancelarEstudo_DSC() {
    lockControlsInModalWin(false);

    if (!((dsoCancelarEstudo.recordset.BOF) && (dsoCancelarEstudo.recordset.EOF))) {
        if (dsoCancelarEstudo.recordset['Resultado'].value != null && dsoCancelarEstudo.recordset['Resultado'].value != '') {
            if (window.top.overflyGen.Alert(dsoCancelarEstudo.recordset['Resultado'].value) == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Estudo cancelado.') == 0)
                return null;

            btnEditarVisualizar_onclick();
            glb_windowDocumentos.selDocumento_onchange('btnCancelarEstudo');
        }

        return null;
    }
}

function btnLimparAlteracoes_onclick() {

    var _retConf = window.top.overflyGen.Confirm('Deseja limpar o �ndice "2 - Altera��es" e todas altera��es marcadas no texto?');
       
    if (_retConf == 1) {
        //return null;
        var sbody = txtHTML.value;

        // Troca tags de altera��o no texto
        sbody = sbody.split("<+>").join("");
        sbody = sbody.split("</+>").join("");
        sbody = sbody.split("<" + String.fromCharCode(42) + ">").join("");
        sbody = sbody.split("</" + String.fromCharCode(42) + ">").join("");
        sbody = sbody.split("<->").join("");
        sbody = sbody.split("</->").join("");

        // Limpa indice "2 - ALTERA��ES"
        var textoInicial = "{<B>2 ALTERA��ES</B>^I(2)}<BR>";
        var textoFinal = "{<B>3 OBJETIVOS</B>^I(3)}<BR>";

        var inicio = sbody.indexOf(textoInicial) + textoInicial.length;
        var fim = sbody.indexOf(textoFinal);
        var alteracoes = sbody.slice(inicio, fim);

        sbody = sbody.replace(alteracoes, "\n<BR>\n<BR>\n\n");

        txtHTML.value = sbody;
    }

    lockControlsInModalWin(false);
}