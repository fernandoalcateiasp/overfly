/********************************************************************
modalpropalt.js

Library javascript para o modalpropalt.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var modPesqPropAlt_Timer = null;

var dsoPesq = new CDatatransport("dsoPesq");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalpropaltBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();   
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtPesquisa').disabled == false )
        txtPesquisa.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    var sCaller;
    
    if ( glb_sCaller == 'PROP' )
        sCaller = 'Proprietário';
    else
        sCaller = 'Alternativo';
        
    secText('Selecionar ' + sCaller, 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    
    // ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divMod01.style.height, 10) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        height = 40;
        
    }
    
    txtPesquisa.maxLength = 30;  // aceita vinte e hum caracteres de digitacao
    with (txtPesquisa.style)
    {
        left = 0;
        top = 16;
        width = (txtPesquisa.maxLength + 8) * FONT_WIDTH - 4;
        heigth = 24;
    }
    
    txtPesquisa.onkeypress = txtPesquisa_onKeyPress;
    
    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style)
    {
        top = parseInt(txtPesquisa.style.top, 10);
        left = parseInt(txtPesquisa.style.left, 10) + parseInt(txtPesquisa.style.width, 10) + 2;
        width = 80;
        height = 24;
    }
    
	lblPropAltMesmo.style.top = lblPesquisa.offsetTop;
	lblPropAltMesmo.style.left = btnFindPesquisa.offsetLeft + btnFindPesquisa.offsetWidth + ELEM_GAP;
	chkPropAltMesmo.style.width = FONT_WIDTH * 3;
	chkPropAltMesmo.style.height = FONT_WIDTH * 3;
	chkPropAltMesmo.style.top = lblPesquisa.offsetTop + 16;
	chkPropAltMesmo.style.left = lblPropAltMesmo.offsetLeft - 3;
	
	lblPropAltMesmo.style.visibility = (glb_sCaller == 'PROP' ? 'inherit' : 'hidden');
	chkPropAltMesmo.style.visibility = (glb_sCaller == 'PROP' ? 'inherit' : 'hidden');

    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    with (divFG.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divPesquisa.style.top, 10) + parseInt(divPesquisa.style.height, 10) + ELEM_GAP;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }
    
    startGridInterface(fg);

    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia'], []);
    fg.Redraw = 2;
    
    lblPesquisa.innerText = sCaller;
}

function txtPesquisa_onKeyPress()
{
    if ( event.keyCode == 13 )
    {
        lockControlsInModalWin(true);
        modPesqPropAlt_Timer = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
    }
}


function txtPesquisa_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqPropAlt_Timer != null )
    {
        window.clearInterval(modPesqPropAlt_Timer);
        modPesqPropAlt_Timer = null;
    }
    
    txtPesquisa.value = trimStr(txtPesquisa.value);
    
    changeBtnState (txtPesquisa.value);

    if (btnFindPesquisa.disabled)
    {
        lockControlsInModalWin(false);
        return;
    }    
    
    startPesq(txtPesquisa.value);
}

function fg_ModPesqPropAltDblClick()
{
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModPesqPropAltKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        lockControlsInModalWin(true);
        sendJSMessage( getHtmlId(), JS_DATAINFORM, glb_sCaller, new Array( fg.TextMatrix(fg.Row, 1), 
			fg.TextMatrix(fg.Row, 2), chkPropAltMesmo.checked) );
    }
    else
        restoreInterfaceFromModal();
}

function startPesq(strPesquisa)
{
    lockControlsInModalWin(true);

	var nCurrNumRegs = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    
    var strPars = new String();
    strPars = '?';
    strPars += 'sCaller=' + escape(glb_sCaller.toUpperCase());
    strPars += '&strToFind=' + escape(strPesquisa);
    strPars += '&nNumeroRegistros=' + escape(nCurrNumRegs);
    
    dsoPesq.URL = SYS_ASPURLROOT + '/serversidegenEx/fillpropalt.aspx' + strPars;
	
	dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

function dsopesq_DSC() {
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    headerGrid(fg,['Nome',
                   'ID',
                   'Fantasia'], []);

    fillGridMask(fg,dsoPesq,['Nome',
                             'PessoaID',
                             'Fantasia'],
                             ['','','']);
    alignColsInGrid(fg,[1]);
                             
    fg.FrozenCols = 1;
    
    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
        btnOK.disabled = false;
    }    
    else
        btnOK.disabled = true;    
}
