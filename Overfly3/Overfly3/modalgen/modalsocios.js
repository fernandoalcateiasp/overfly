/********************************************************************
modalsocios.js

TSA - A cria��o da Modal de S�cios se deve para o setor Financeiro
      poder analisar os s�cios de cada empresa e poder ter uma an�lise
      de cr�dito mais assertiva.
*************************************************************************/
//VARIAVEIS GLOBAIS ************************************************
var glb_nCurrFormID = null;
var glb_nCurrSubFormID = null;
var glb_aEmpresaData = null;
var glb_bNovoSocio = null;
var glb_nUserID = 0;
var glb_getServerData = 0;

//Eventos de envio ao BD *************************************************
var dsoGridSocios = new CDatatransport("dsoGridSocios");
var dsoDadosSocio = new CDatatransport("dsoDadosSocio");
var dsoGravaSocio = new CDatatransport("dsoGravaSocio");
var dsoExcluiSocio = new CDatatransport("dsoExcluiSocio");
var dsoCmbGridSocioPais = new CDatatransport("dsoCmbGridSocioPais");
var dsoCmbGridSocioTipoPessoa = new CDatatransport("dsoCmbGridSocioTipoPessoa");

//Configura o html *******************************************************
function window_onload() {
    window_onload_1stPart();

    glb_nCurrFormID = window.top.formID;
    glb_nCurrSubFormID = window.top.subFormID;
    glb_aEmpresaData = getCurrEmpresaData();

    // ajusta o body do html
    with (modalsociosBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
    window.focus();

    glb_nUserID = glb_USERID;
    return glb_nCurrFormID;
}

//Configuracao inicial do html *******************************************
function setupPage() {
    // texto da secao
    secText('S�cios', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 68;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }

    // desabilita o botao OK/Cancel
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';
    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';
    btnGravar.disabled = true;

    // ajusta o divSocios
    with (divSocios.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP - 35;
        width = modWidth - 2 * ELEM_GAP;
        height = 90;
    }


    if (glb_nCreditoID > 0) {
        // Ajusta elementos da divSocios
        adjustElementsInForm([['btnResumo', 'btn', btnOK.offsetWidth - 16, 1, 0, 10],
                                ['btnIncluir', 'btn', btnOK.offsetWidth - 16, 1, 10],
                                ['btnExcluir', 'btn', btnOK.offsetWidth - 16, 1, 10],
                                ['btnGravar', 'btn', btnOK.offsetWidth - 16, 1, 10],
                                ['btnListar', 'btn', btnOK.offsetWidth - 16, 1, 10]], null, null, true);
    }
    else {
        // Ajusta elementos da divSocios
        adjustElementsInForm([['btnIncluir', 'btn', btnOK.offsetWidth - 16, 1, 0, 10],
                                ['btnExcluir', 'btn', btnOK.offsetWidth - 16, 1, 10],
                                ['btnGravar', 'btn', btnOK.offsetWidth - 16, 1, 10],
                                ['btnListar', 'btn', btnOK.offsetWidth - 16, 1, 10]], null, null, true);
    }

    // Alinha bot�es � direita
    //window.document.getElementById('btnListar').style.left = frameRect[2] - btnListar.offsetWidth - 16 - ELEM_GAP - 72;
    //window.document.getElementById('btnGravar').style.left = frameRect[2] - btnGravar.offsetWidth - 16 - ELEM_GAP;

    elem = window.document.getElementById('divGrids');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        width = modWidth - (3 * ELEM_GAP / 2) - 170;
        height = 560;
        left = ELEM_GAP - 1;
        top = 67;
    }

    elem = window.document.getElementById('fg');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        width = modWidth - (7 * ELEM_GAP / 2) + 3 + 10;
        height = 524;
        left = 0;
        top = -4;
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.Redraw = 2;

    //Pessoa Fis�ca
    if (glb_nTipoPessoaID == 51)
        btnIncluir.disabled = true;

    if (!(glb_nCreditoID > 0))
    {
        btnResumo.style.visibility = 'hidden';
        btnResumo.disabled = true;
    }

    startDynamicCmbsGridsSocios();
    showExtFrame(window, true);
}

//Solicitar dados dos grids ao servidor **********************************
function fillGridSocios() {

    lockControlsInModalWin(true);

    setConnection(dsoGridSocios);

    if (glb_nTipoPessoaID == 51) {
        var sReferecencia = 'ObjetoID';
        var sReferecencia2 = 'SujeitoID';
    }
    else {
        var sReferecencia = 'SujeitoID';
        var sReferecencia2 = 'ObjetoID';
    }

    if (glb_bNovoSocio)
        var strSQL = 'SELECT 0 [PesDocumentoID], 0 [RelacaoID], SPACE(0) [PaisID], SPACE(0) [TipoPessoaID], SPACE(0) [DocumentoFederal], SPACE(0) [Nome], SPACE(0) [PessoaID], SPACE(0) [Participacao], SPACE(0) [dtAdmissao], NULL [ContratoSocial], SPACE(0) [Observacao]';
    else
        var strSQL = 'SELECT DISTINCT b.PesDocumentoID, c.RelacaoID, e.PaisID, d.TipoPessoaID, b.Numero [DocumentoFederal], dbo.fn_Pessoa_Fantasia(a.PessoaID, 0) [Nome], ' +
                            'a.PessoaID, c.Participacao, c.dtAdmissao, c.ContratoSocial, c.Observacao ' +
                        'FROM dbo.fn_Empresas_Relacionadas_tbl (' + glb_nPessoaID + ') a ' +
                            'INNER JOIN Pessoas_Documentos b ON (b.PessoaID = a.PessoaID) ' +
                            'INNER JOIN RelacoesPessoas c WITH(NOLOCK) ON (((c.' + sReferecencia + ' = a.PessoaID) AND ' +
                                                                            '(c.' + sReferecencia2 + ' = ' + glb_nPessoaID + ')) OR ' +
                                                                            '((c.' + sReferecencia2 + ' = a.PessoaID) AND (c.' + sReferecencia + ' = ' + glb_nPessoaID + ')))' +
                            'INNER JOIN Pessoas d WITH(NOLOCK) ON(d.PessoaID = a.PessoaID) ' +
                            'INNER JOIN Pessoas_Documentos e WITH(NOLOCK) ON(e.PessoaID = d.PessoaID) ' +
	                    'WHERE (b.TipoDocumentoID IN (101, 111) AND c.TipoRelacaoID = 29 AND c.EstadoID = 2)';

        dsoGridSocios.SQL = strSQL;
        dsoGridSocios.ondatasetcomplete = fillGridSocios_DSC;
        dsoGridSocios.Refresh();
        window.focus();
}

//Retorno do servidor de solicita��o dos dados dos grids *****************
function fillGridSocios_DSC() {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var hiddencolumns = glb_bNovoSocio ? [0, 9, 10] : [9, 10];

    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);

    fg.FontSize = '9';
    fg.FrozenCols = 0;
    fg.BorderStyle = 1;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = true;

    headerGrid(fg, ['ID',
                    'Pa�s',
                    'Tipo',
                    'Nome',
                    'Documento Federal',
                    'Participa��o (%)',
                    'Admiss�o',
                    'CS',
                    'Observa��o',
                    'PesDocumentoID',
                    'RelacaoID'],
                    hiddencolumns);

    fillGridMask(fg, dsoGridSocios, ['PessoaID' + (!glb_bNovoSocio ? '*' : ''),
                                     'PaisID' + (!glb_bNovoSocio ? '*' : ''),
                                     'TipoPessoaID' + (!glb_bNovoSocio ? '*' : ''),
                                     'Nome' + (!glb_bNovoSocio ? '*' : ''),
                                     'DocumentoFederal' + (!glb_bNovoSocio ? '*' : ''),
                                     'Participacao' + (!glb_bNovoSocio ? '*' : ''),
                                     'dtAdmissao' + (!glb_bNovoSocio ? '*' : ''),
                                     'ContratoSocial' + (!glb_bNovoSocio ? '*' : ''),
                                     'Observacao' + (!glb_bNovoSocio ? '*' : ''),
                                     'PesDocumentoID',
                                     'RelacaoID'],
                                     ['999999', '', '', '', '99999999999999', '999.99', '99/99/9999', '', '', '', ''],
                                     ['######', '', '', '',  '##############', '###.##', dTFormat, '', '', '', '']);

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[5, '###.##', 'S']]);

    insertcomboData(fg, getColIndexByColKey(fg, 'PaisID'), dsoCmbGridSocioPais, 'Pais', 'PaisID');
    insertcomboData(fg, getColIndexByColKey(fg, 'TipoPessoaID'), dsoCmbGridSocioTipoPessoa, 'TipoPessoa', 'TipoPessoaID');

    //alignColsInGrid(fg, alinharDireita);

    fg.Redraw = 0;
    fg.ExplorerBar = 5;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.ColWidth(getColIndexByColKey(fg, 'Nome')) = 100 * 18;    
    fg.ColWidth(getColIndexByColKey(fg, 'PaisID')) = 100 * 18;
    fg.ColWidth(getColIndexByColKey(fg, 'TipoPessoaID')) = 100 * 18;

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;

    lockControlsInModalWin(false);

    // se tem linhas no grid, coloca foco no grid
    if (fg.Rows > 1) {
        btnExcluir.disabled = false;

        if (glb_bNovoSocio) {
            fg.Editable = true;
            btnGravar.disabled = false;
            btnExcluir.disabled = true;
        }
        else {
            btnGravar.disabled = true;
            btnExcluir.disabled = false;
        }

        window.focus();
        fg.focus();
    }
    else {
        btnExcluir.disabled = true;
    }

    fg.ColDataType(7) = 11;
    fg.Redraw = 2;
    glb_aCelHint = [[0, 7, 'Est� no contrato social?']];

}

//Metodos onclick ********************************************************
function btn_onclick(ctl) {
    window.focus();

    if (ctl.id == 'btnResumo') {
        window.top.openModalHTML(glb_nCreditoID, 'Resumo do Cr�dito', 'S', 12);
    }
    else if (ctl.id == 'btnListar') {
        glb_bNovoSocio = false;
        btnGravar.disabled = true;

        fillGridSocios();
    }
    else if (ctl.id == 'btnIncluir') {
        glb_bNovoSocio = true;
        btnGravar.disabled = false;

        fillGridSocios();
    }
    else if (ctl.id == 'btnGravar')
    {
        verificaDadosSocios(1);
        glb_bNovoSocio = false;

    }
    else if (ctl.id == 'btnExcluir') {
        var nColRelacaoID = null;
        var nRelacaoID = null;

        if (window.top.overflyGen.Confirm('Realmente deseja excluir este s�cio ?') == 2)
            return null;
        else {
            nColRelacaoID = getColIndexByColKey(fg, 'RelacaoID');
            nRelacaoID = fg.TextMatrix(fg.Row, nColRelacaoID);

            excluirSocio(nRelacaoID);
        }
    }
    else {
        if (glb_nCreditoID > 0)
            window.top.openModalHTML(glb_nCreditoID, 'Resumo do Cr�dito', 'S', 12);
        else
        // esta funcao fecha a janela modal e destrava a interface
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    }
}

//Salva as linhas alteradas nos grids ************************************
function saveDataInGridSocios() {
    var nRow = 2;
    var strPars = '';
    var nEmpresaID = glb_nPessoaID;
    var nPaisID = trimStr(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'PaisID')));
    var nTipoPessoaID = trimStr(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'TipoPessoaID')));
    var sNome = trimStr(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Nome')));
    var sDocumentoFederal = trimStr(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'DocumentoFederal')));
    var nParticipacao = fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'Participacao'));
    var dtAdmissao = trimStr(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'dtAdmissao')));
    var bContratoSocial = (fg.ValueMatrix(nRow, getColIndexByColKey(fg, 'ContratoSocial')) == -1) ? "1" : "0";
    var sObservacao = trimStr(fg.TextMatrix(nRow, getColIndexByColKey(fg, 'Observacao')));

    dtAdmissao = dtAdmissao == '' ? '' : dateFormatToSearch(trimStr(dtAdmissao));
    //sObservacao = sObservacao == '' ? 'NULL' : dateFormatToSearch(trimStr(dtAdmissao));

    if (nParticipacao == 0) {
        //Cancel
        if (window.top.overflyGen.Confirm('Realmente deseja criar associa��o sem percentual de participa��o ?') == 2)
            return null;
    }

    setConnection(dsoGravaSocio);

    strPars = '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPaisID=' + escape(nPaisID);
    strPars += '&nTipoPessoaID=' + escape(nTipoPessoaID);
    strPars += '&sDocumento=' + escape(sDocumentoFederal);
    strPars += '&sNome=' + escape(sNome);
    strPars += '&nParticipacao='+ escape(nParticipacao);
    strPars += '&sdtAdmissao=' + escape(dtAdmissao);
    strPars += '&bContratoSocial=' + escape(bContratoSocial);
    strPars += '&nUsuarioID=' + escape(glb_nUserID);
    strPars += '&sObservacao=' + escape(sObservacao);

    dsoGravaSocio.URL = SYS_ASPURLROOT + '/serversidegenEx/gravarSocio.aspx' + strPars;
    dsoGravaSocio.ondatasetcomplete = saveDataInGridSocios_DSC;
    dsoGravaSocio.refresh();

    lockControlsInModalWin(true);
}

//Retorno do servidor, apos salvar as linhas alteradas nos grids *********
function saveDataInGridSocios_DSC() {

    lockControlsInModalWin(false);

    var sResultado = dsoGravaSocio.recordset['Resultado'].value;

    sResultado = (sResultado == null ? '' : sResultado);

    if (sResultado.length > 0) {
        if (window.top.overflyGen.Alert(sResultado) == 0)
            return null;
    }
    else {
        if (window.top.overflyGen.Alert('Socio inclu�do com sucesso.') == 0)
            return null;

        fillGridSocios();
    }
}

function dateAdd(p_Interval, p_Number, p_Date) {

    /*
    //Ajuste string para formato americano.
    if (aEmpresa[1] == 130) {
        var dia = p_Date.substring(0, 2)
        var mes = p_Date.substring(3, 5)
        var ano = p_Date.substring(6, 10)

        p_Date = mes + '/' + dia + '/' + ano
    }
    */

    /*if (DATE_FORMAT == "DD/MM/YYYY")
        putDateInMMDDYYYY2(p_Date);*/

    p_Date = normalizeDate_DateTime(p_Date, (DATE_FORMAT == "DD/MM/YYYY" ? true : false));

    if (!new Date(p_Date)) { return "invalid date: '" + p_Date + "'"; }
    if (isNaN(p_Number)) { return "invalid number: '" + p_Number + "'"; }

    p_Number = new Number(p_Number);
    var dt = new Date(p_Date);

    switch (p_Interval.toLowerCase()) {
        case "yyyy": {
            dt.setFullYear(dt.getFullYear() + p_Number);
            break;
        }
        case "q": {
            dt.setMonth(dt.getMonth() + (p_Number * 3));
            break;
        }
        case "m": {
            dt.setMonth(dt.getMonth() + p_Number);
            break;
        }
        case "y":			// day of year
        case "d":			// day
        case "w": {		// weekday
            dt.setDate(dt.getDate() + p_Number);
            break;
        }
        case "ww": {	// week of year
            dt.setDate(dt.getDate() + (p_Number * 7));
            break;
        }
        case "h": {
            dt.setHours(dt.getHours() + p_Number);
            break;
        }
        case "n": {		// minute
            dt.setMinutes(dt.getMinutes() + p_Number);
            break;
        }
        case "s": {
            dt.setSeconds(dt.getSeconds() + p_Number);
            break;
        }
        case "ms": {	// JS extension
            dt.setMilliseconds(dt.getMilliseconds() + p_Number);
            break;
        }
        default: {
            return "invalid interval: '" + p_Interval + "'";
        }
    }

    if (DATE_FORMAT == "DD/MM/YYYY")
        return dataFormatada(dt, 1);
    else
        return dataFormatada(dt, 2);
}

function dataFormatada(data, tipo) {
    var dia = data.getDate();
    if (dia.toString().length == 1)
        dia = "0" + dia;
    var mes = data.getMonth() + 1;
    if (mes.toString().length == 1)
        mes = "0" + mes;
    var ano = data.getFullYear();

    if (tipo == 1)
        return dia + "/" + mes + "/" + ano;
    else if (tipo == 2)
        return mes + "/" + dia + "/" + ano;
}

//Valida datas
function verificaData(sData) {
    var sMsgErro = '';
    var sData = trimStr(sData);
    var bDataIsValid = true;
    var dtHoje = null;

    if (sData != '')
        bDataIsValid = chkDataEx(sData);

    if (!bDataIsValid) {
        sMsgErro = 'Data inv�lida.';
    }
    else {
        dtHoje = parseInt(ComparisonDate(getCurrDate(), null)); //dtHoje formatada como YYYYMMDD para compara��o
        sData = parseInt(ComparisonDate(sData, null)); //dtAdmissao formatada como YYYYMMDD para compara��o

        if (dtHoje < sData)
            sMsgErro = 'Data de admiss�o n�o pode ser maior que a data atual.';
    }
    return sMsgErro;
}

function verificaParticipacaoSocio(nParticipacao) {
    var sMsgErro = '';

    if (nParticipacao > 100)
        sMsgErro = 'Percentual maximo de participa��o � de 100,00%';

    return sMsgErro;
}

//Trata o CGC/CPF digitado (Retorno:o numDoc tratado)********************
function treatsNumDoc(numDoc) {
    var strRet = '';
    var strTemp = '';
    var rExp;
    var i;

    strTemp = trimStr(numDoc);

    rExp = /\D/g;
    strTemp = strTemp.split(rExp);

    for (i = 0; i < strTemp.length; i++)
        strRet += strTemp[i];

    return strRet;
}

//Valida o CGC/CPF digitado no banco de dados****************************
function verificaDadosSocios(nTipoID) {
    /*
     nTipoID
     0 - Verifica Doc
     1 - Grava s�cio
     */

    var nColDocumentoFederal = getColIndexByColKey(fg, 'DocumentoFederal');
    var nDocumentoFederal = treatsNumDoc(trimStr(fg.TextMatrix(2, nColDocumentoFederal)));
    var nPaisID = fg.TextMatrix(2, getColIndexByColKey(fg, 'PaisID'));
    var nTipoPessoaID = fg.TextMatrix(2, getColIndexByColKey(fg, 'TipoPessoaID'));
    var nTipoDocumentoID = null;
    var sMensagemErro = '';
    var Enter = '\n';

    if (!(nPaisID > 0))
    {
        sMensagemErro += 'Favor escolher o pa�s de origem do s�cio.' + Enter;
    }

    if (!(nTipoPessoaID > 0)) {
        sMensagemErro += 'Favor escolher o tipo de s�cio.' + Enter;
    }

    if ((!((nDocumentoFederal.length == 11) || (nDocumentoFederal.length == 14)) && (nPaisID == 130)) || (nDocumentoFederal.length < 3 && (nPaisID != 130)))
    {
        sMensagemErro += 'Favor digitar um n�mero de documento federal v�lido.';
    }

    if (sMensagemErro.length == 0) {
        lockControlsInModalWin(true);

        fg.TextMatrix(2, nColDocumentoFederal) = nDocumentoFederal;

        if (nTipoPessoaID == 52)
            nTipoDocumentoID = 111;
        else
            nTipoDocumentoID = 101;

        setConnection(dsoDadosSocio);

        dsoDadosSocio.SQL = "DECLARE @Verificacao INT, @Registros INT " +
                             "DECLARE @DadosSocio TABLE(ID INT IDENTITY(1, 1), PesDocumentoID INT, RelacaoID INT, DocumentoFederal VARCHAR(20), Nome VARCHAR(20), PessoaID INT, " +
                                                        "Participacao NUMERIC(5, 2), dtAdmissao DATETIME, Observacao VARCHAR(30), Verificacao INT, TipoID INT, TipoDocumentoID INT) " +

            "INSERT INTO @DadosSocio " +
                "SELECT NULL [PesDocumentoID], NULL [RelacaoID], NULL [DocumentoFederal], NULL [Nome], NULL [PessoaID], NULL [Participacao], NULL [dtAdmissao], " +
                        "NULL [Observacao], dbo.fn_Documento_Verifica('" + nDocumentoFederal + "', " + nPaisID + ", " + nTipoDocumentoID + ") [Verificacao], " +
                        nTipoID + " [TipoID], " + nTipoDocumentoID + " [TipoDocumentoID] " +
            "UNION ALL " +
            "SELECT DISTINCT a.PesDocumentoID, b.RelacaoID, a.Numero[DocumentoFederal], dbo.fn_Pessoa_Fantasia(a.PessoaID, 0) [Nome], a.PessoaID, " +
                            "b.Participacao, b.dtAdmissao, b.Observacao, NULL [Verificacao], " + nTipoID + " [TipoID], " + nTipoDocumentoID + " [TipoDocumentoID] " +
                "FROM Pessoas_Documentos a " +
                    "LEFT JOIN RelacoesPessoas b ON ((b.ObjetoID = " + glb_nPessoaID + ") AND (b.TipoRelacaoID = 29) AND(b.EstadoID = 2) AND b.SujeitoID = a.PessoaID) " +
                    "LEFT JOIN dbo.fn_Empresas_Relacionadas_tbl(" + glb_nPessoaID + ") c ON((b.SujeitoID = c.PessoaID) AND(a.PessoaID = c.PessoaID)) " +
            "WHERE(a.TipoDocumentoID IN (101, 111) AND a.Numero = '" + nDocumentoFederal + "'" + (glb_bNovoSocio ? " AND a.PaisID = " + nPaisID : "") + ") " +

            "SET @Registros = @@ROWCOUNT " +

            "IF(@Registros > 1) " +
            "BEGIN " +
                "SELECT @Verificacao = Verificacao " +
                "FROM @DadosSocio " +
                "WHERE ID = 1 " +

                "UPDATE @DadosSocio SET Verificacao = @Verificacao " +

                "DELETE @DadosSocio WHERE ID = 1 " +
            "END " +

            "SELECT * " +
                "FROM @DadosSocio ";

        dsoDadosSocio.ondatasetcomplete = verificaDadosSocios_DSC;
        dsoDadosSocio.Refresh();

    }
    else {
        if (window.top.overflyGen.Alert(sMensagemErro) == 0)
            return null;

        return false;
    }
}

//Retorno do servidor da funcao que verifica o numero do documento*******
function verificaDadosSocios_DSC() {

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    var nRow = 2;
    var nTipoID = dsoDadosSocio.recordset['TipoID'].value;
    var nTipoDocumentoID = dsoDadosSocio.recordset['TipoDocumentoID'].value;
    var nVerificacaoDoc = dsoDadosSocio.recordset['Verificacao'].value;
    var nRelacaoID = dsoDadosSocio.recordset['RelacaoID'].value;
    var nPessoaID = dsoDadosSocio.recordset['PessoaID'].value;

    var nColPessoaID = getColIndexByColKey(fg, 'PessoaID');
    var nColParticipacao = getColIndexByColKey(fg, 'Participacao');
    var nColdtObservacao = getColIndexByColKey(fg, 'Observacao');
    var nColNome = getColIndexByColKey(fg, 'Nome');
    var nColdtAdmissao = getColIndexByColKey(fg, 'dtAdmissao');
    var nColPaisID = getColIndexByColKey(fg, 'PaisID');
    var nColTipoPessoaID = getColIndexByColKey(fg, 'TipoPessoaID');

    var dtAdmissao = null;
    var sNome = null;
    var nPaisID = fg.TextMatrix(nRow, nColPaisID);
    var nTipoPessoaID = fg.TextMatrix(nRow, nColTipoPessoaID);


    var sMsgErro = '';
    var sMsgErroDoc = '';
    var sMensagemAviso = '';
    var _retConf = null;
    var sSujeito = (nTipoPessoaID == 51 ? 'Pessoa' : 'Empresa');
    var Enter = '\n';

    //Pessoa/Empresa existente
    if (((nVerificacaoDoc > 0) && (nPaisID == 130)) || (nPessoaID > 0)) {
        // Atualiza grid com os dados existentes no banco de dados
        fg.TextMatrix(nRow, nColNome) = (dsoDadosSocio.recordset['Nome'].value == null ? fg.TextMatrix(nRow, nColNome) : dsoDadosSocio.recordset['Nome'].value);
        fg.TextMatrix(nRow, nColPessoaID) = (nPessoaID == null ? fg.TextMatrix(nRow, nColPessoaID) : nPessoaID);
        fg.TextMatrix(nRow, nColParticipacao) = (dsoDadosSocio.recordset['Participacao'].value == null ? fg.TextMatrix(nRow, nColParticipacao) : dsoDadosSocio.recordset['Participacao'].value);
        fg.TextMatrix(nRow, nColdtAdmissao) = (dsoDadosSocio.recordset['dtAdmissao'].value == null ? fg.TextMatrix(nRow, nColdtAdmissao) : dsoDadosSocio.recordset['dtAdmissao'].asDatetime(dTFormat));
        fg.TextMatrix(nRow, nColdtObservacao) = (dsoDadosSocio.recordset['Observacao'].value == null ? fg.TextMatrix(nRow, nColdtObservacao) : dsoDadosSocio.recordset['Observacao'].value);
        fg.TextMatrix(nRow, nColPaisID) = (nPaisID == null ? fg.TextMatrix(nRow, nColPaisID) : nPaisID);
        fg.TextMatrix(nRow, nColTipoPessoaID) = (nTipoPessoaID == null ? fg.TextMatrix(nRow, nColTipoPessoaID) : nTipoPessoaID);
    }

    dtAdmissao = trimStr(fg.TextMatrix(nRow, nColdtAdmissao));
    sNome = trimStr(fg.TextMatrix(nRow, nColNome));
    nParticipacao = fg.ValueMatrix(nRow, nColParticipacao);

    if (sNome.length == 0)
        sMsgErro += 'Favor digitar o nome do s�cio.' + Enter;

    if (dtAdmissao.length > 0) {

        sMsgErro += verificaData(dtAdmissao);

        if (sMsgErro.length > 0)
            sMsgErro += Enter;
    }
    else
        sMsgErro += 'Deve-se preencher a data de admiss�o' + Enter;

    if (nParticipacao > 0)
    {
        sMsgErro += verificaParticipacaoSocio(nParticipacao);

        if ((sMsgErro.length > 0) && (sMsgErro.substring(sMsgErro.length-2, sMsgErro.length) != Enter))
            sMsgErro += Enter;
    }

    if (!(dsoDadosSocio.recordset.BOF && dsoDadosSocio.recordset.EOF))
    {
        // Documento federal Inv�lido
        if (nVerificacaoDoc == 0)
        {
            sMsgErroDoc += 'N�mero do documento federal inv�lido.' + Enter;
        }
        // Documento federal j� cadastrado
        else if ((nVerificacaoDoc != 0) && (sMsgErro.length == 0))
        {
            if (nRelacaoID > 0) {
                sMensagemAviso = 'Est� ' + sSujeito + ' j� � s�cia ! ' + Enter + ' Deseja listar os s�cios ?';

                _retConf = window.top.overflyGen.Confirm(sMensagemAviso);

                if (_retConf == 1) {
                    glb_bNovoSocio = false;
                    fillGridSocios();
                }
                else {
                    lockControlsInModalWin(false);
                    return null;
                }
            }
            else if ((nTipoID == 0) && (sMsgErro.length == 0)) {
                sMensagemAviso = sSujeito + ' j� cadastrada.' + Enter + 'Deseja torn�-la s�cio ?';

                _retConf = window.top.overflyGen.Confirm(sMensagemAviso);

                if (_retConf == 1)
                    saveDataInGridSocios();
                else {
                    lockControlsInModalWin(false);
                    return null;
                }
            }
            else if ((nTipoID == 1) && (sMsgErro.length == 0)) {
                saveDataInGridSocios();
            }
        }

        if (((sMsgErro.length > 0) || (sMsgErroDoc.length > 0)) && (nTipoID == 1)) {
            sMsgErro += sMsgErroDoc;

            if (window.top.overflyGen.Alert(sMsgErro) == 0)
                return null;
        }
        else if ((sMsgErroDoc.length > 0) && (nTipoID == 0)) {
            if (window.top.overflyGen.Alert(sMsgErroDoc) == 0)
                return null;
        }
    }

    lockControlsInModalWin(false);
    return null;
}

function excluirSocio(nRelacaoID)
{
    lockControlsInModalWin(true);
    setConnection(dsoExcluiSocio);

    dsoExcluiSocio.SQL = "DECLARE @Resultado VARCHAR(800) = NULL, @EstadoID INT " +
                         "SELECT @EstadoID = a.EstadoID FROM RelacoesPessoas a WITH(NOLOCK) WHERE a.RelacaoID = " + nRelacaoID + " " +
                         "BEGIN TRY " +
                         "BEGIN TRANSACTION SocioDeletar " +
                            "IF (@EstadoID = 2) " + 
                            "UPDATE RelacoesPessoas SET EstadoID = 4, UsuarioID = " + glb_nUserID + " WHERE RelacaoID = " + nRelacaoID + " " +
                         "END TRY " +
                         "BEGIN CATCH " +
                            "SET @Resultado = ERROR_MESSAGE() " +
                         "END CATCH " +

                         "IF(@Resultado IS NULL) " +
                            "COMMIT TRANSACTION SocioDeletar " +
                         "ELSE " +
                            "ROLLBACK TRANSACTION SocioDeletar " +

                        "SELECT @Resultado [Resultado] ";

    dsoExcluiSocio.ondatasetcomplete = excluirSocio_DSC;
    dsoExcluiSocio.Refresh();
}

function excluirSocio_DSC()
{
    var _retAlert = null;

    if (dsoExcluiSocio.recordset['Resultado'].value == null)
        _retAlert = window.top.overflyGen.Alert('S�cio exclu�do com sucesso !');
    else
        _retAlert = window.top.overflyGen.Alert(dsoExcluiSocio.recordset['Resultado'].value);

    fillGridSocios();
}

function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear();

    return (s);
}

/********************************************************************
Fun��o para compara��o de data
EX: Par�metro 01/23/2012
    Retorno 20120123
    
for�ar formato => 1 = ingles
********************************************************************/
function ComparisonDate(par, formato) {
    //formato 0 = portugues; 1 = ingles
    var sfinalDate = "";
    var aParts = new Array();

    try {
        aParts = par.split('/');
        if (aParts[0].length == par.length)
            aParts = par.split('-');

        if (aParts[0].length == 1)
            aParts[0] = "0" + aParts[0];  //Dia      

        if (aParts[1].length == 1)
            aParts[1] = "0" + aParts[1];  //M�s

        if (aParts[2].length == 2)
            aParts[2] = "20" + aParts[2]; //Ano

        //Se o S.O. estiver em Portugu�s
        if (DATE_SQL_PARAM == 103)
            sfinalDate = aParts[2].toString() + aParts[1].toString() + aParts[0].toString();
        //Se o S.O. estiver em outro idioma exceto Portugu�s
        else
            sfinalDate = aParts[2].toString() + aParts[0].toString() + aParts[1].toString();

        //For�a o formato a ser ingles, sobrescrevendo a data gerada no trecho anterior
        if (formato == 1)
            sfinalDate = aParts[2].toString() + aParts[0].toString() + aParts[1].toString();
    }
    catch (e) {
        sfinalDate = '0';
    }

    return (sfinalDate);
}

//Inclus�o de itens no grid de produtos
function startDynamicCmbsGridsSocios() {

    glb_getServerData = 2;

    // parametrizacao do dso CmbGridProdutos
    setConnection(dsoCmbGridSocioPais);

    dsoCmbGridSocioPais.SQL = "SELECT LocalidadeID [PaisID], Localidade [Pais] " +
                                   "FROM Localidades " +
                                   "WHERE TipoLocalidadeID = 203 AND EstadoID = 2 AND LocalidadeID IN(130, 167) " +
                               "UNION ALL " +
                               "SELECT LocalidadeID[PaisID], Localidade[Pais] " +
                                   "FROM Localidades " +
                                   "WHERE TipoLocalidadeID = 203 AND EstadoID = 2 AND LocalidadeID NOT IN(130, 167) ";

    dsoCmbGridSocioPais.ondatasetcomplete = startDynamicCmbsGridsSocios_DSC;
    dsoCmbGridSocioPais.Refresh();

    // parametrizacao do dso CmbGridProdutos
    setConnection(dsoCmbGridSocioTipoPessoa);

    dsoCmbGridSocioTipoPessoa.SQL = "SELECT ItemID [TipoPessoaID], ItemMasculino [TipoPessoa] " +
                                        "FROM TiposAuxiliares_Itens " +
                                        "WHERE TipoID = 12 AND EstadoID = 2 AND ItemID IN(51, 52)";

    dsoCmbGridSocioTipoPessoa.ondatasetcomplete = startDynamicCmbsGridsSocios_DSC;
    dsoCmbGridSocioTipoPessoa.Refresh();

}

function startDynamicCmbsGridsSocios_DSC() {
    glb_getServerData--;

    if (glb_getServerData != 0)
        return null;

    fillGridSocios();
}

//Metodos do grid ********************************************************
function js_modalsocio_AfterEdit(Row, Col) {
    var sMsgErro = '';

    if (getColIndexByColKey(fg, 'DocumentoFederal') == Col) {
        var nDocumentoFederal = trimStr(fg.TextMatrix(Row, Col));

        if (nDocumentoFederal.length > 0)
            verificaDadosSocios(0);
    }
    else if (getColIndexByColKey(fg, 'dtAdmissao') == Col) {
        var dtAdimissao = trimStr(fg.TextMatrix(Row, Col));

        if (dtAdimissao.length > 0) {
            sMsgErro = verificaData(dtAdimissao);

            if (sMsgErro.length > 0)
                if (window.top.overflyGen.Alert(sMsgErro) == 0)
                    return null;
        }
    }
    else if (getColIndexByColKey(fg, 'Participacao') == Col) {
        fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

        var nParticipacao = fg.ValueMatrix(Row, Col);

        sMsgErro = verificaParticipacaoSocio(nParticipacao);

        if (sMsgErro.length > 0) {
            if (window.top.overflyGen.Alert(sMsgErro) == 0)
                return null;
        }
    }

}

function js_modalsocio_AfterRowColChange(oldRow, oldCol, newRow, newCol) {
    fg_AfterRowColChange();
}

function js_modalsocioBeforeEdit(grid, row, col) {
    if (getColIndexByColKey(fg, 'Nome') == col)
        grid.EditMaxLength = 20;
    if (getColIndexByColKey(fg, 'DocumentoFederal') == col)
        grid.EditMaxLength = 14;
    if (getColIndexByColKey(fg, 'Participacao') == col)
        grid.EditMaxLength = 3;
    else if (getColIndexByColKey(fg, 'Observacao') == col)
        grid.EditMaxLength = 30;
}

function js_modalsocio_ValidateEdit() {
    ;
}

function js_modalsocioKeyPress(KeyAscii) {
    ;
}

function js_fg_modalsocioDblClick(grid, Row, Col) {
    ;
}

function js_fg_modalsocioBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    return true;
}

function fg_MouseDown_cotador(nRow, nCol) {
    ;
}

function js_fg_modalsocioMouseMove(grid, Button, Shift, X, Y) {
    ;
}

function js_modalgerenciarprodutoscotador_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
}