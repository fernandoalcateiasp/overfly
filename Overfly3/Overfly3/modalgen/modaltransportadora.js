/********************************************************************
modalcliente.js

Library javascript para o modalTransportadora.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var modPesqCli_TimerHandler = null;

var dsoPesq = new CDatatransport('dsoPesq');
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    var elem = document.getElementById('modaltransportadoraBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    // coloca foco no campo apropriado
    if ( document.getElementById('txtTransportadora').disabled == false )
        txtTransportadora.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Selecionar Transportadora', 1);
    
    // ajusta elementos da janela
    var elem;
    var temp;
    
    // ajusta o divCidade
    elem = window.document.getElementById('divCidade');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;    
        temp = parseInt(width);
        height = 40;
        
    }

    adjustElementsInForm([['lblChavePesquisa', 'selChavePesquisa', 12, 1, -10],
                          ['lblTransportadora', 'txtTransportadora', 50, 1, -2]],
                          null,null,true);

    var aOptions = [[0,'ID'],
                    [1,'Nome'],
                    [2,'Fantasia'],
                    [3,'Documento']];

    for (i=0; i<aOptions.length; i++)
    {
        var oOption = document.createElement("OPTION");
        oOption.value = aOptions[i][0];
        oOption.text = aOptions[i][1];
        selChavePesquisa.add(oOption);
    }
    
    selChavePesquisa.selectedIndex = 1;

    txtTransportadora.onkeypress = txtTransportadora_onKeyPress;
    
    // btnFindPesquisa
    elem = window.document.getElementById('btnFindPesquisa');
    elem.disabled = true;

    with (elem.style)
    {
        top = parseInt(document.getElementById('txtTransportadora').style.top);
        left = parseInt(document.getElementById('txtTransportadora').style.left) + parseInt(document.getElementById('txtTransportadora').style.width) + ELEM_GAP;
        width = 80;
        height = 24;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divCidade').style.top) + parseInt(document.getElementById('divCidade').style.height) + ELEM_GAP;
        width = temp + 321;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }
    
    startGridInterface(fg);

    headerGrid(fg, ['ID', 'Nome', 'Fantasia', 'Documento'], []);
    
    fg.Redraw = 2;
}

function txtTransportadora_onKeyPress()
{
    if ( event.keyCode == 13 )
        modPesqCli_TimerHandler = window.setInterval('btnFindPesquisa_onclick()', 30, 'JavaScript');
}

function txtTransportadora_ondigit(ctl)
{
    changeBtnState(ctl.value);
}

function btnFindPesquisa_onclick(ctl)
{
    if ( modPesqCli_TimerHandler != null )
    {
        window.clearInterval(modPesqCli_TimerHandler);
        modPesqCli_TimerHandler = null;
    }

    txtTransportadora.value = trimStr(txtTransportadora.value);
    
    changeBtnState(txtTransportadora.value);

    if (btnFindPesquisa.disabled)
        return;
    
    startPesq(txtTransportadora.value);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModCliKeyPress(KeyAscii)
{
    if ( fg.Row < 1 )
        return true;
 
    if ( (KeyAscii == 13) && (fg.Row > 0) && (btnOK.disabled == false) )
        btn_onclick(btnOK);
 
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_ModCliDblClick()
{
    // Se tem cidade selecionada e o botao ok esta liberado,
    // chama o botao OK
    
    if ( (fg.Row >= 1 )&& (btnOK.disabled == false) )
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // Esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

	// 1. Object usu�rio clicou onblur bot�o OK.
    if (ctl.id == btnOK.id )
    {
    	sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, new Array(
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, "ID")),
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, "Nome")),
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, "Fantasia")),
                    fg.TextMatrix(fg.Row, getColIndexByColKey(fg, "Documento"))));
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    

}

/********************************************************************
Pesquisa as transportadoras.
********************************************************************/
function startPesq(strPesquisa) {
	if (strPesquisa == null || strPesquisa == "") {
		return;
	}
	
	lockControlsInModalWin(true);

	var aEmpresa = getCurrEmpresaData();
	var nCurrEmpresa = aEmpresa[0];

    var strPars =
		"?EmpresaID=" + glb_nEmpresaID + 
		"&UserID=" + glb_USERID  + 
		"&SelChavePesquisa=" + selChavePesquisa.value +
		"&DadoPesquisa=" + strPesquisa;
		
    setConnection(dsoPesq);

	// Configura o SQL principal
    dsoPesq.SQL = "SELECT DISTINCT b.PessoaID AS ID, b.Nome, b.Fantasia, c.Numero as Documento " +
		"FROM RelacoesPessoas a WITH(NOLOCK) " +
			"INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ObjetoID) " +
			"LEFT OUTER JOIN Pessoas_Documentos c WITH(NOLOCK) ON (c.PessoaID = b.PessoaID) " +
			"INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON (c.TipoDocumentoID = d.ItemID)" +
		"WHERE ";
					
	// Insere o filtro de pesquisa.
	if (selChavePesquisa.value == 0 && strPesquisa != "") { // ID
		dsoPesq.SQL += "b.PessoaID = " + strPesquisa + " AND ";
	} else if (selChavePesquisa.value == 1 && strPesquisa != "") { // Nome
		dsoPesq.SQL += "b.Nome LIKE '%" + strPesquisa + "%' AND ";
	} else if (selChavePesquisa.value == 2 && strPesquisa != "") { // Fantasia
		dsoPesq.SQL += "b.Fantasia LIKE '%" + strPesquisa + "%' AND ";
	} else if (selChavePesquisa.value == 3 && strPesquisa != "") { // Documento
		dsoPesq.SQL += "c.Numero = '" + strPesquisa + "' AND ";
	} 
	
	// Insere o restante do where
	dsoPesq.SQL += /*"a.EstadoID = 2 AND  " + */
		"c.TipoDocumentoID = 111 AND " +
		"a.SujeitoID = " + nCurrEmpresa + " AND  " +
		"a.TipoRelacaoID = 23 AND " +
		"b.EstadoID <> 5 ";  
			
	// Insere a ordena��o pelo filtro de pesquisa.
	dsoPesq.SQL += "ORDER BY b.Fantasia ";
		
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.refresh();
}

function dsopesq_DSC() {
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    headerGrid(fg,['ID','Nome','Fantasia','Documento'], []);
               
    fillGridMask(fg,dsoPesq,['ID','Nome','Fantasia','Documento'],['','','','']);

    fg.FrozenCols = 1;

    lockControlsInModalWin(false);
    
    // destrava botao OK se tem linhas no grid
    if (fg.Rows > 1)
    {
        window.focus();
        fg.focus();
    
        btnOK.disabled = false;
    }    
    else
        btnOK.disabled = true;    
}
