<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalatendimentoHtml" name="modalatendimentoHtml">
    <head>

        <title></title>

        <%
            'Links de estilo, bibliotecas da automacao e especificas
            Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modalatendimento.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
            
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
            
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
			Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
            
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
            Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/modalatendimento.js" & Chr(34) & "></script>" & vbCrLf
        %>

        <%
        'Script de variaveis globais

            Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
            Response.Write vbcrlf
            Response.Write vbcrlf

            Dim i, sCaller, sCallFrom, sCurrDateFormat, nState, nEmpresaID, nUserID, nA1, nA2, nB7A1, nB7A2, nParceiroID

            sCaller = ""
            sCallFrom = ""
            nState = 0
            nEmpresaID = 0
            nUserID = 0
            nA1 = 0
            nA2 = 0
            nB7A1 = 0
            nB7A2 = 0
            nParceiroID = 0

            For i = 1 To Request.QueryString("sCaller").Count    
                sCaller = Request.QueryString("sCaller")(i)
            Next
            
            For i = 1 To Request.QueryString("sCallFrom").Count    
                sCallFrom = Request.QueryString("sCallFrom")(i)
            Next

            For i = 1 To Request.QueryString("nState").Count    
                nState = Request.QueryString("nState")(i)
            Next                        

            For i = 1 To Request.QueryString("nEmpresaID").Count    
                nEmpresaID = Request.QueryString("nEmpresaID")(i)
            Next

            For i = 1 To Request.QueryString("nUserID").Count    
                nUserID = Request.QueryString("nUserID")(i)
            Next

            For i = 1 To Request.QueryString("nA1").Count    
                nA1 = Request.QueryString("nA1")(i)
            Next

            For i = 1 To Request.QueryString("nA2").Count    
                nA2 = Request.QueryString("nA2")(i)
            Next

            For i = 1 To Request.QueryString("nB7A1").Count    
                nB7A1 = Request.QueryString("nB7A1")(i)
            Next

            For i = 1 To Request.QueryString("nB7A2").Count    
                nB7A2 = Request.QueryString("nB7A2")(i)
            Next

            For i = 1 To Request.QueryString("nParceiroID").Count    
                nParceiroID = Request.QueryString("nParceiroID")(i)
            Next

            Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
            Response.Write vbcrlf
            Response.Write "var glb_sCallFrom= " & Chr(39) & CStr(sCallFrom) & Chr(39) & ";"
            Response.Write vbcrlf
            Response.Write "var glb_nState= " & Chr(39) & CStr(nState) & Chr(39) & ";"
            Response.Write vbcrlf            
            Response.Write "var glb_nA1 = " & Chr(39) & CStr(nA1) & Chr(39) & ";"
            Response.Write vbcrlf
            Response.Write "var glb_nA2 = " & Chr(39) & CStr(nA2) & Chr(39) & ";"
            Response.Write vbcrlf
            Response.Write "var glb_nB7A1 = " & Chr(39) & CStr(nB7A1) & Chr(39) & ";"
            Response.Write vbcrlf
            Response.Write "var glb_nB7A2 = " & Chr(39) & CStr(nB7A2) & Chr(39) & ";"
            Response.Write vbcrlf
            Response.Write "var glb_nParceiroID = " & Chr(39) & CStr(nParceiroID) & Chr(39) & ";"
            Response.Write vbcrlf

            'Formato corrente de data
            For i = 1 To Request.QueryString("sCurrDateFormat").Count
                scurrDateFormat = Request.QueryString("sCurrDateFormat")(i)
            Next

            If (sCurrDateFormat = "DD/MM/YYYY") Then
                Response.Write "var glb_dCurrDate = '" & Day(Date) & "/" & Month(Date)& "/" & Year(Date) & "';" & vbcrlf
            ElseIf (sCurrDateFormat = "MM/DD/YYYY") Then
                Response.Write "var glb_dCurrDate = '" & Month(Date) & "/" & Day(Date)& "/" & Year(Date) & "';" & vbcrlf
            ElseIf (sCurrDateFormat = "YYYY/MM/DD/") Then
                Response.Write "var glb_dCurrDate = '" & Year(Date) & "/" & Month(Date) & "/" & Day(Date) & "';" & vbcrlf
            Else
                Response.Write "var glb_dCurrDate = '';" & vbcrlf
            End If

            Response.Write "</script>"
            Response.Write vbcrlf
        %>

        <script ID="wndJSProc" LANGUAGE="javascript">

        </script>

        <!-- //@@ Eventos de grid -->
        <!-- fg -->
        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
            if (glb_GridIsBuilding)
                return;

            js_modalatendimento_ValidateEdit(fg, fg.Row, fg.Col)
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
            if (glb_GridIsBuilding)
                return;

            js_modalatendimento_AfterEdit (arguments[0], arguments[1]);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
            if (glb_GridIsBuilding)
                return;

            js_modalatendimentoKeyPress(arguments[0]);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
            if (glb_GridIsBuilding)
                return;

            fg_DblClick(fg, fg.Row, fg.Col);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=AfterRowColChange>
            if (glb_GridIsBuilding)
                return;

            js_fg2_modalatendimentoAfterRowColChange (fg2, arguments[0], arguments[1], arguments[2], arguments[3]);
        </SCRIPT>
        
        <SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=AfterEdit>
            if (glb_GridIsBuilding)
                return;

            js_fg2_modalatendimento_AfterEdit(arguments[0], arguments[1]);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg5 EVENT=DblClick>
            if (glb_GridIsBuilding)
                return;

            fg5_DblClick(fg5, fg5.Row, fg5.Col);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg5 EVENT=AfterRowColChange>
            if (glb_GridIsBuilding)
                return;

            js_fg5_modalatendimentoAfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>

            if (glb_GridIsBuilding)
                return;

            js_modalatendimento_BeforeEdit(fg, fg.Row, fg.Col);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
            if (glb_GridIsBuilding)
                return;

            js_fg_modalatendimentoBeforeMouseDown (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
            js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
            if (glb_GridIsBuilding)
                return;

            js_fg_modalatendimentoEnterCell (fg);   
            js_fg_EnterCell (fg);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
            if (glb_GridIsBuilding)
                return;

            js_fg_modalatendimentoAfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
            js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
            
            //Para celulas checkbox que sao readonly
            treatCheckBoxReadOnly2 (dsoGrid, fg, arguments[0], arguments[1], arguments[2], arguments[3]);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
            if (glb_GridIsBuilding)
                return;

            js_fg_modalatendimento_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
        </SCRIPT>

        <!-- //@@ Eventos de grid -->
        <!-- fg2 -->
        <SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=MouseMove>
            js_fg_MouseMove(fg2, arguments[0], arguments[1], arguments[2], arguments[3]);
        </SCRIPT>

        <SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=EnterCell>
            if (glb_GridIsBuilding)
                return;
             
            js_fg2_modalatendimentoEnterCell (fg2);   
            js_fg_EnterCell (fg2);
        </SCRIPT>

        <SCRIPT ID="vbFunctions" LANGUAGE="vbscript">
            Function daysBetween(date1, date2)
                daysBetween = DateDiff("d",date1,date2)
            End Function
        </SCRIPT>
    </head>

    <body id="modalatendimentoBody" name="modalatendimentoBody" LANGUAGE="javascript" onload="return window_onload()">
        <!-- Div Title Bar -->
        <div id="divMod01" name="divMod01" class="divGeneral">
            <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
        </div>    

        <!-- Div Controles -->
        <div id="divControls" name="divControls" class="divGeneral">
            <%
                Dim strSQL, rsData, j
            
                Set rsData = Server.CreateObject("ADODB.Recordset")
            %>		 
            <!-- Combo Servico -->
		    <p id="lblServicos" name="lblServicos" class="lblGeneral">Servi�o</p>
		    <select id="selServicos" name="selServicos" class="fldGeneral">
                <!--<option value='0'>Vendas</option>-->
		    
                <%
                    strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, EhDefault " & _
                                "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                                "WHERE ((TipoID = 50) AND (EstadoID = 2))"

	                rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	        If (Not (rsData.BOF AND rsData.EOF) ) Then
        		        While Not (rsData.EOF)
		        	        Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'" )

        			        If (rsData.Fields("EhDefault").Value) Then
		        	            Response.Write(" SELECTED")
        			        End If
    			
		        	        Response.Write(">" & rsData.Fields("fldName").Value & "</option>" )
        			        rsData.MoveNext()
		                WEnd
        	        End If

	                rsData.Close
                %>
		    </select>

            <!-- Combo Data -->
		    <p id="lblData" name="lblData" class="lblGeneral">Data</p>
		    <select id="selData" name="selData" class="fldGeneral">
                <%
                    strSQL = "DECLARE @DataFormato INT " & _
                             "SET @DataFormato = (CASE dbo.fn_Pessoa_Localidade(" & CStr(nEmpresaID) & ", 1, NULL, NULL) WHEN 130 THEN 103 ELSE 101 END) " & _
                             "SELECT DISTINCT TOP 10 CONVERT(VARCHAR(10), dtData, @DataFormato) AS fldName, dtData " & _
	                             "FROM Pessoas_RFV WITH(NOLOCK) " & _
	                             "ORDER BY dtData DESC "

        	        j = 0
                
                    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	                If (Not (rsData.BOF AND rsData.EOF) ) Then
        		        While Not (rsData.EOF)
		                    Response.Write("<option value='" & CStr(j) & "'")

    		                If (j = 0)Then
	        	                Response.Write(" SELECTED>" )
                            Else
    		                    Response.Write(">" )
        			        End If

		        	        Response.Write( rsData.Fields("fldName").Value & "</option>" )
        			    
                            j = j + 1
                            rsData.MoveNext()
        		        WEnd
	                End If

	                rsData.Close
                %>
		    </select>

            <!-- Combo Proprietario  -->
   		    <p id="lblProprietario" name="lblProprietario" class="lblGeneral">Propriet�rio</p>
		    <select id="selProprietario" name="selProprietario" class="fldGeneral">
            <!--
                A1
                - Op��o em branco
                - O pr�prio vendedor

                A2
                - Op��o em branco
                - Vendedores da equipe do usuario logado
                - Equipe que vendedor pertence (comentado)

                A1 e A2
                - Op��o em branco
                - O pr�prio vendedor
                - Todos os vendedores da equipe
                - Equipe que vendedor pertence (comentado)
            -->
            <%
                If ((CInt(nB7A1) = 1) AND (CInt(nB7A2) = 0)) Then
                    strSQL = "SELECT 0 AS Ordem, 0 AS fldID, SPACE(0) AS fldName " & _
                             "UNION ALL " & _
                             "SELECT DISTINCT 0 AS Ordem, a.PessoaID AS fldID, a.Fantasia AS fldName " & _
	                             "FROM Pessoas a WITH(NOLOCK) " & _
	                             "WHERE (a.PessoaID=" & CStr(nUserID) & ") " & _
                                 "ORDER BY Ordem, fldName"
                ElseIf ((CInt(nB7A1) = 0) AND (CInt(nB7A2) = 1)) Then
                    strSQL = "SELECT 0 AS Ordem, 0 AS fldID, SPACE(0) AS fldName " & _
                             "UNION ALL " & _
                             "SELECT DISTINCT 0 AS Ordem, d.PessoaID AS fldID, d.Fantasia AS fldName " & _
    	                         "FROM RelacoesPessoas a WITH(NOLOCK) " & _
    		                         "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (a.ObjetoID = b.ObjetoID) " & _
    		                         "INNER JOIN Pessoas d WITH(NOLOCK) ON (b.SujeitoID = d.PessoaID) " & _
    		                         "INNER JOIN Pedidos e WITH(NOLOCK) ON (d.PessoaID = e.ProprietarioID) " & _
    	                         "WHERE (a.TipoRelacaoID = 34 AND a.EstadoID = 2 AND b.TipoRelacaoID = 34 AND b.EstadoID = 2 AND a.SujeitoID=" & CStr(nUserID) & ") " & _
    		                         "AND dbo.fn_Colaborador_Dado(d.PessoaID, NULL, NULL, 4) = 651 " & _
                            "UNION " & _
                            "SELECT DISTINCT 0 AS Ordem, d.PessoaID AS fldID, d.Fantasia AS fldName " & _
    	                        "FROM RelacoesPessoas a WITH(NOLOCK) " & _
    		                        "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON ((b.ObjetoID = a.ObjetoID) AND (dbo.fn_Colaborador_Dado(b.SujeitoID, NULL, NULL, 4) = 651)) " & _
    		                        "INNER JOIN Pessoas d WITH(NOLOCK) ON (b.SujeitoID = d.PessoaID) " & _
    	                        "WHERE (a.TipoRelacaoID = 34 AND a.EstadoID = 2 AND b.TipoRelacaoID = 34 AND b.EstadoID = 2 AND a.SujeitoID=" & CStr(nUserID) & ") " & _
    	                        "ORDER BY Ordem, fldName "
                Else
                    strSQL = "SELECT 0 AS Ordem, 0 AS fldID, SPACE(0) AS fldName " & _
                             "UNION ALL " & _
                             "SELECT DISTINCT 0 AS Ordem, b.PessoaID AS fldID, b.Fantasia AS fldName " & _
	                             "FROM Pessoas a WITH(NOLOCK) " & _
		                             "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) " & _
	                             "WHERE b.PessoaID=" & CStr(nUserID) & " " & _
                             "UNION " & _
                             "SELECT DISTINCT 0 AS Ordem, b.PessoaID AS fldID, b.Fantasia AS fldName " & _
	                             "FROM RelacoesPessoas a WITH(NOLOCK) " & _
		                             "INNER JOIN Pessoas b WITH(NOLOCK) ON (a.ProprietarioID = b.PessoaID) " & _
	                             "WHERE ((a.TipoRelacaoID = 21) AND (a.EstadoID = 2) AND (b.EstadoID = 2)) " & _
	                             "ORDER BY Ordem, fldName "
                End If

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	    If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
		        	    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'" )

        			    If (CDbl(rsData.Fields("fldID").Value) = CDbl(nUserID)) Then
		        	        Response.Write(" SELECTED")
        			    End If
    			
		        	    Response.Write(">" & rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
		            WEnd
        	    End If

	            rsData.Close
            %>										
		    </select>

            <!-- Combo Oferta  -->
   		    <p id="lblOferta" name="lblOferta" class="lblGeneral">Oferta</p>
		    <select id="selOferta" name="selOferta" class="fldGeneral">
            <%
                strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem "  & _
                         "UNION ALL "  & _
                         "SELECT TOP 10 a.EmailID AS fldID, " & _ 
                                "SUBSTRING(Assunto, (PATINDEX('% #%', Assunto) + 1), LEN(Assunto)) + ' (' + LEFT(CONVERT(VARCHAR(20), a.dtEnvio, 103), 5) + ')' AS fldName, " & _ 
                                "1 AS Ordem " & _ 
                            "FROM EmailMarketing a WITH(NOLOCK) " & _ 
                            "WHERE ((a.EmpresaID = " & CStr(nEmpresaID) & ") AND (a.EstadoID = 63) AND (a.dtValidade > GETDATE())) " & _
                            "ORDER BY Ordem, fldName "

        	    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	            If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
		                Response.Write("<option value='" & rsData.Fields("fldID").Value & "'")

    		            If (rsData.Fields("fldID").Value = "0") Then
	        	            Response.Write(" SELECTED>" )
                        Else
    		                Response.Write(">" )
        			    End If   

		        	    Response.Write( rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
        		    WEnd

	            End If

	            rsData.Close
            %>	
		    </select>

            <!-- Combo Estados  -->
		    <p id="lblEstados" name="lblEstados" class="lblGeneral" title="Estado do cliente">Est</p>
		    <select id="selEstados" name="selEstados" class="fldGeneral" title="Estado do cliente" MULTIPLE>
    <%
                strSQL = "SELECT b.RecursoID AS fldID, b.RecursoAbreviado AS fldName " & _
                             "FROM RelacoesRecursos a WITH(NOLOCK) " & _ 
	                             "INNER JOIN Recursos b WITH(NOLOCK) ON a.SujeitoID = b.RecursoID " & _ 
                            "WHERE (a.ObjetoID = 301 AND a.TipoRelacaoID = 3 AND b.RecursoID NOT IN (5)) " & _ 
                            "ORDER BY a.Ordem "

        	    rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

	            If (Not (rsData.BOF AND rsData.EOF) ) Then
        		    While Not (rsData.EOF)
		                Response.Write("<option value='" & rsData.Fields("fldID").Value & "'")

    		            If (rsData.Fields("fldID").Value = "2")Then
	        	            Response.Write(" SELECTED>" )
                        Else
    		                Response.Write(">" )
        			    End If   

		        	    Response.Write( rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
        		    WEnd

	            End If

	            rsData.Close
    %>										
		    </select>	

            <!-- Combo Classifica��o  -->
   		    <p id="lblClassificacao" name="lblClassificacao" class="lblGeneral">Classifica��o</p>
		    <select id="selClassificacao" name="selClassificacao" class="fldGeneral" multiple>
    <%
                strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem AS Ordem " & _
                            "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
                            "WHERE (EstadoID = 2 AND TipoID = 13 AND Filtro LIKE '%<CLI>%' AND " & _
                                "dbo.fn_Direitos_TiposAuxiliares(ItemID, NULL, " & CStr(nUserID) & ", GETDATE()) > 0) " & _
                            "ORDER BY Ordem"

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	    If (Not (rsData.BOF AND rsData.EOF) ) Then
		            While Not (rsData.EOF)
        			    Response.Write( "<option value='" & rsData.Fields("fldID").Value & "'>" )
		        	    Response.Write( rsData.Fields("fldName").Value & "</option>" )
        			    rsData.MoveNext()
		            WEnd
	            End If

	            rsData.Close
    %>										
		    </select>

            <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Pesquisa</p>
            <input type="text" id="txtPesquisa" name="txtPesquisa" class="fldGeneral"></input>
            
            <p id="lblAnalitico" name="lblAnalitico" title="Modo Anal�tico ou Sint�tico?" class="lblGeneral">Anal�tico</p>
            <input type="checkbox" id="chkAnalitico" name="chkAnalitico" title="Modo Anal�tico ou Sint�tico?" class="fldGeneral"></input>

            <p id="lblAtivo" name="lblAtivo" title="Atendimento Ativo ou Receptivo?" class="lblGeneral">Ativo</p>
            <input type="checkbox" id="chkAtivo" name="chkAtivo" title="Atendimento Ativo ou Receptivo?" class="fldGeneral"></input>

            <p id="lblAtender" name="lblAtender" class="lblGeneral">Atender</p>
            <input type="checkbox" id="chkAtender1" name="chkAtender1" title="Listar somente os clientes a serem atendidos ou todos?" class="fldGeneral"></input>
            <input type="checkbox" id="chkAtender2" name="chkAtender2" title="Listar somente os clientes com atendimentos pendentes ou todos?" class="fldGeneral"></input>

            <p id="lblCredito" name="lblCredito" title="Listar somente os clientes que tem cr�dito ou todos?" class="lblGeneral">Cr�dito</p>
            <input type="checkbox" id="chkCredito" name="chkCredito" title="Listar somente os clientes que tem cr�dito ou todos?" class="fldGeneral"></input>

            <p id="lblDetalhes" name="lblDetalhes" class="lblGeneral">Detalhes</p>
            <input type="checkbox" id="chkDetalhes1" name="chkDetalhes1" title="Mostrar dados transacionais e de contato do cliente?" class="fldGeneral"></input>
            <input type="checkbox" id="chkDetalhes2" name="chkDetalhes2" title="Mostrar ofertas e hist�rico de compras do cliente?" class="fldGeneral"></input>

            <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            <input type="button" id="btnSite" name="btnSite" value="Site" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Acessar site da empresa"></input>
            <input type="button" id="btnProximo" name="btnProximo" value="Pr�ximo" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            <input type="button" id="btnIniciarAtendimento" name="btnIniciarAtendimento" value="Atender" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
                        
            <input type="button" id="btnGravar" name="btnGravar" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            <input type="button" id="btnAtendimentosAnteriores" name="btnAtendimentosAnteriores" value="Atendimentos" title="Mostrar atendimentos anteriores" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            <input type="button" id="btnFinalizar" name="btnFinalizar" value="Finalizar..." LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Finalizar antendimento"></input>

            <!-- ----------------- -->

            <p id="lblPedidosPendentes" name="lblPedidosPendentes" title="S� pedidos pendentes?" class="lblGeneral">PP</p>
            <input type="checkbox" id="chkPedidosPendentes" name="chkPedidosPendentes" title="S� pedidos pendentes?" class="fldGeneral"></input>         

            <p id="lblSeguradora" name="lblSeguradora" class="lblGeneral">Seguradora</p>
		    <select id="selSeguradora" name="selSeguradora" class="fldGeneral"></select>

		    <p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
		    <select id="selMarca" name="selMarca" class="fldGeneral"></select>				        

            <p id="lblLimiteMinimo" name="lblLimiteMinimo" class="lblGeneral">Lim min</p>
            <input type="text" id="txtLimiteMinimo" name="txtLimiteMinimo" title="Limite de cr�dito m�nimo" class="fldGeneral"></input>         

            <p id="lblLimiteMaximo" name="lblLimiteMaximo" class="lblGeneral">Lim max</p>
            <input type="text" id="txtLimiteMaximo" name="txtLimiteMaximo" title="Limite de cr�dito m�ximo" class="fldGeneral"></input>         
            
            <p id="lblOk" name="lblOk" class="lblGeneral" title="Incluir informa��es complemetares ao atendimento">OK</p>
            <input type=checkbox id="chkOk" name="chkOk" class="fldGeneral" title="Incluir informa��es complemetares no atendimento"></input>         
            
            <p id="lblDiasVencidos" name="lblDiasVencidos" class="lblGeneral">DV</p>
            <input type="text" id="txtDiasVencidos" name="txtDiasVencidos" title="N�mero de dias vencidos das informa��es de cr�dito" class="fldGeneral"></input>         

            <input type="button" id="btnIncluir" name="btnIncluir" value="I" title="Incluir seguro de cr�dito" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
            <input type="button" id="btnExcluir" name="btnExcluir" value="E" title="Excluir seguro de cr�dito" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        </div>    

        <!-- Div Grid -->
        <div id="divFG" name="divFG" class="divGeneral">
            <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT></object>
        </div>    
        
        <!-- Div Detalhes do Cliente-->
        <div id="divDetalhesCliente" name="divDetalhesCliente" class="divGeneral">

            <p id="lblClienteDesde" name="lblClienteDesde" class="lblGeneral">Cliente Desde</p>
            <input type="text" id="txtClienteDesde" name="txtClienteDesde" class="fldGeneral"></input>

            <p id="lblAnos" name="lblAnos" class="lblGeneral" title="Tempo (em anos) que � cliente">Anos</p>
            <input type="text" id="txtAnos" name="txtAnos" class="fldGeneral" title="Tempo (em anos) que � cliente"></input>         

            <p id="lblPedidosFaturados" name="lblPedidosFaturados" class="lblGeneral" title="N�meros de pedidos faturados">Pedidos</p>
            <input type="text" id="txtPedidosFaturados" name="txtPedidosFaturados" class="fldGeneral" title="N�meros de pedidos faturados"></input>         

            <p id="lblFaturamentoAcumulado" name="lblFaturamentoAcumulado" class="lblGeneral" title="Faturamento acumulado (US$)">Fat Acum</p>
            <input type="text" id="txtFaturamentoAcumulado" name="txtFaturamentoAcumulado" class="fldGeneral" title="Faturamento acumulado (US$)"></input>           

            <p id="lblContribuicaoAcumulada" name="lblContribuicaoAcumulada" class="lblGeneral" title="Contribui��o acumulada (US$)">Contr Acum</p>
            <input type="text" id="txtContribuicaoAcumulada" name="txtContribuicaoAcumulada" class="fldGeneral" title="Contribui��o acumulada (US$)"></input>         

            <p id="lblContribuicaoAcumuladaRecebida" name="lblContribuicaoAcumuladaRecebida" class="lblGeneral" title="Contribui��o acumulada recebida (US$)">Contr Acum Rec</p>
            <input type="text" id="txtContribuicaoAcumuladaRecebida" name="txtContribuicaoAcumuladaRecebida" class="fldGeneral" title="Contribui��o acumulada recebida (US$)"></input>         

            <p id="lblAtraso" name="lblAtraso" class="lblGeneral" title="Maior atraso (em dias)">Atraso</p>
            <input type="text" id="txtAtraso" name="txtAtraso" class="fldGeneral" title="Maior atraso (em dias)"></input>         

            <p id="lblMaiorCompra" name="lblMaiorCompra" class="lblGeneral" title="Data e valor (US$) da maior compra">Maior compra</p>
            <input type="text" id="txtMaiorCompra" name="txtMaiorCompra" class="fldGeneral" title="Data e valor (US$) da maior compra"></input>                             
            
            <p id="lblDiasUltimaCompra" name="lblDias" class="lblGeneral" title="Dias da �ltima compra">Dias</p>
            <input type="text" id="txtDiasUltimaCompra" name="txtDias" class="fldGeneral" title="Dias da �ltima compra"></input>                             
            
            <p id="lblDataUltimaCompra" name="lblDataUltimaCompra" class="lblGeneral" title="Data e valor (US$) da �ltima compra">�ltima compra</p>
            <input type="text" id="txtDataUltimaCompra" name="txtDataUltimaCompra" class="fldGeneral" title="Data e valor (US$) da �ltima compra"></input>
            
            <p id="lblMaiorAcumulo" name="lblMaiorAcumulo" class="lblGeneral" title="Data e valor (US$) do maior ac�mulo">Maior ac�mulo</p>
            <input type="text" id="Text1" name="txtUltimaCompra" class="fldGeneral" title="Data e valor (US$) do maior ac�mulo"></input>
        </div>    
        
        <!-- Div Telefones/URLs -->
        <div id="divControlsFone" name="divControlsFone" class="divGeneral">
            <p id="lblDDITelefone" name="lblDDITelefone" class="lblGeneral">DDI</p>
            <input type="text" id="txtDDITelefone" name="txtDDITelefone" class="fldGeneral"></input>         
            <p id="lblDDDComercial" name="lblDDDComercial" class="lblGeneral">DDD</p>
            <input type="text" id="txtDDDComercial" name="txtDDDComercial" class="fldGeneral"></input>         
            <p id="lblFoneComercial" name="lblFoneComercial" class="lblGeneral">Comercial</p>
            <input type="text" id="txtFoneComercial" name="txtFoneComercial" class="fldGeneral"></input>           
            <p id="lblDDDFaxCelular" name="lblDDDFaxCelular" class="lblGeneral">DDD</p>
            <input type="text" id="txtDDDFaxCelular" name="txtDDDFaxCelular" class="fldGeneral"></input>         
            <p id="lblFaxCelular" name="lblFaxCelular" class="lblGeneral">Celular</p>
            <input type="text" id="txtFaxCelular" name="txtFaxCelular" class="fldGeneral"></input>         
            <p id="lblEmail" name="lblEmail" class="lblGeneral">E-mail</p>
            <input type="text" id="txtEmail" name="txtEmail" class="fldGeneral"></input>         
            <p id="lblSite" name="lblSite" class="lblGeneral">Site</p>
            <input type="text" id="txtSite" name="txtSite" class="fldGeneral"></input>                             
        </div>

        <!-- Div Finalizar Atendimento-->
        <div id="divFinalizarAtendimento" name="divFinalizarAtendimento" class="divGeneral" style="top:-25px;">

            <p id="lblFinalizarAtendimento" name="lblFinalizarAtendimento" class="lblGeneral" style="font-size=12">Finalizar atendimento</p>

            <p id="lblMeioAtendimento" name="lblMeioAtendimento" class="lblGeneral">Meio de atendimento</p>
            <select id="selMeioAtendimento" name="selMeioAtendimento" class="fldGeneral">
            <%
                strSQL = "SELECT 0 AS fldID, SPACE(0) AS fldName, 0 AS Ordem " & _
                            "UNION ALL " & _ 
                        "SELECT ItemID AS fldID, ItemMasculino AS fldName, Ordem " & _
	                    "FROM TiposAuxiliares_Itens WITH(NOLOCK) " & _
	                    "WHERE TipoID = 52 AND EstadoID=2 " & _
	                    "ORDER BY Ordem"

	            rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

                If (Not (rsData.BOF AND rsData.EOF) ) Then
		            While Not (rsData.EOF)
                        Response.Write("<option value='" & rsData.Fields("fldID").Value & "'")

	                    If ((rsData.Fields("fldID").Value = "0") AND (rsData.Fields("Ordem").Value = "0"))Then
    	                    Response.Write(" SELECTED>" )
                        Else
	                        Response.Write(">" )
			            End If   

        	            Response.Write( rsData.Fields("fldName").Value & "</option>" )
			            rsData.MoveNext()
		            WEnd

                End If

                rsData.Close
            %>										
            </select>        

            <p id="lblReagendamento" name="lblReagendamento" class="lblGeneral">Agendamento</p>
            <select id="selReagendamento" name="selReagendamento" class="fldGeneral">
                        <option value="0" SELECTED></option>
                        <option value="15">15 minutos</option>
                        <option value="30">30 minutos</option>
                        <option value="60">1 hora</option>
                        <option value="120">2 horas</option>
                        <option value="1440">1 dia</option>
                        <option value="2880">2 dias</option>
		    </select>
		    	
		    <input type="button" id="btnSolicitacaoCredito" name="btnSolicitacaoCredito" value="Solicitar an�lise de cr�dito" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns" title="Solicitar an�lise de cr�dito?"/>

            <p id="lblAgendar" class="lblGeneral" >Agendar para</p>
            <input type="text" id="txtAgendar" name="txtAgendar" class="fldGeneral"/>

            <p id="lblAtendimentoOK" name="lblAtendimentoOK" title="Atendimento foi bem sucedido?" class="lblGeneral">OK</p>
            <input type="checkbox" id="chkAtendimentoOK" name="chkAtendimentoOK" title="Atendimento foi bem sucedido?" class="fldGeneral"></input>

            <p id="lblObservacoes" class="lblGeneral" >Observa��es</p>
            <textarea id="txtObservacoes" name="txtObservacoes" class="fldGeneral"></textarea>        
    
		    <input type="button" id="btnConfirmarFinalizacao" name="btnConfirmarFinalizacao" value="OK" onclick="return btn_onclick(this)" class="btns"/>
		    <input type="button" id="btnCancelarFinalizacao" name="btnCancelarFinalizacao" value="Cancelar" onclick="return btn_onclick(this)" class="btns"/>
        </div>    

        <p id="lblPendente" name="lblPendente" title="Listar somente os atendimentos pendentes ou todos?" class="lblGeneral">Pend</p>
        <input type="checkbox" id="chkPendente" name="chkPendente" title="Listar somente os atendimentos pendentes ou todos?" class="fldGeneral"></input>    

        <p id="lblCliente" name="lblCliente" title="Listar os atendimentos pendentes somente do cliente selecionado ou de todos?" class="lblGeneral">Cli</p>
        <input type="checkbox" id="chkCliente" name="chkCliente" title="Listar os atendimentos pendentes somente do cliente selecionado ou de todos?" class="fldGeneral"></input>    
        
        <!-- Div FG2-->
        <div id="divFG2" name="divFG2" class="divGeneral">
            <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg2" name="fg2" VIEWASTEXT></object>
            <img id="Img1" name="hr_L_FGBorder" class="lblGeneral"></img>
            <img id="Img2" name="hr_R_FGBorder" class="lblGeneral"></img>
            <img id="Img3" name="hr_B_FGBorder" class="lblGeneral"></img>
        </div>        
    
        <!-- Div FG Hist�rico -->
        <div id="divFG3" name="divFG3" class="divGeneral">
            <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg3" name="fg3" VIEWASTEXT></object>
        </div>

        <!-- AtendimentoAnteriores-->
        <textarea id="txtAtendimentosAnteriores" name="txtAtendimentosAnteriores" class="fldGeneral"></textarea>

        <div id="divFG4" name="divFG4" class="divGeneral">
            <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg4" name="fg4" VIEWASTEXT></object>
        </div>    

        <!-- Div FG Ofertas -->
        <div id="divFG5" name="divFG5" class="divGeneral">
            <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg5" name="fg5" VIEWASTEXT></object>
        </div>

        <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        
    </body>
</html>
