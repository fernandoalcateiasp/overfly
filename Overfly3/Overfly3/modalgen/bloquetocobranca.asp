
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    
    'Forca recarregamento da pagina
    Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
    
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Dim sURLRoot
    If (Mid(pagesURLRoot, 1, 10) <> "http://www") Then
		sURLRoot = "http://localhost/overfly3"
	Else
		sURLRoot = pagesURLRoot
	End If
		
    Set objSvrCfg = Nothing
%>

<%
Function ReplaceText(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceText = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function

Function MatchText(baseStr, patrn)
    Dim regEx, Match, Matches    ' Create variable.
    Set regEx = New RegExp       ' Create regular expression.
    regEx.Pattern = patrn        ' Set pattern.
    regEx.IgnoreCase = True      ' Set case insensitivity.
    regEx.Global = True          ' Set global applicability.
    Set Matches = regEx.Execute(baseStr)   ' Execute search.
       
    MatchText = Matches.Count
End Function
%>

<html id="bloquetocobrancaHtml" name="bloquetocobrancaHtml">

<head>

<title></title>

<%
	Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/bloquetocobranca.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/bloquetocobranca.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim srvName, sBarCodeSrc

Dim i, nRegistroID
Dim sLogBancoSrc, nBancoID, nTransacaoID
Dim sNomeBanco, sCodigoDVBanco
Dim sVencimento, sAgenciaCodCedente, sEspecie, sQuantidade
Dim sValorDocumento, sDescAbat, sMoraMultaJur
Dim sValorCobrado, sNossoNumero, sTipoDocumento, sNumeroDocumento
Dim sSacado, sDocumentoSacado, sMensagemSacado
Dim sLinhaDigitavel
Dim sLocalPagamento
Dim sCedente
Dim sDataDocumento, sEspecieDoc, sAceite, sDataProcessamento
Dim sNumContResp, sCarteira, sValor
Dim sInstrucoes
Dim sSacadoComEndereco
Dim sCodigoBarras
Dim sSacadorAval

Dim nNotRecEntrega

nRegistroID = 0
sLogBancoSrc = ""
nBancoID = 0
nTransacaoID = 0
sNomeBanco = ""
sCodigoDVBanco = ""
sVencimento = ""
sAgenciaCodCedente = ""
sEspecie = ""
sQuantidade = ""
sValorDocumento = ""
sDescAbat = ""
sMoraMultaJur = ""
sValorCobrado = ""
sNossoNumero = ""
sTipoDocumento = ""
sNumeroDocumento = ""
sSacado = ""
sDocumentoSacado = ""
sMensagemSacado = ""
sLinhaDigitavel = ""
sLocalPagamento = ""
sCedente = ""
sDataDocumento = ""
sEspecieDoc = ""
sAceite = ""
sDataProcessamento = ""
sNumContResp = ""
sCarteira = ""
sValor = ""
sInstrucoes = ""
sSacadoComEndereco = ""
sCodigoBarras = ""
sSacadorAval = ""
nNotRecEntrega = 0

For i = 1 To Request.QueryString("nRegistroID").Count
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

For i = 1 To Request.QueryString("nNotRecEntrega").Count
    nNotRecEntrega = Request.QueryString("nNotRecEntrega")(i)
Next

Response.Write "var glb_bNotRecEntrega = null;"
Response.Write vbCrLf

If ( nNotRecEntrega <> 0 ) Then
	Response.Write "glb_bNotRecEntrega = true;"	
Else
	Response.Write "glb_bNotRecEntrega = false;"
End If

Response.Write vbCrLf

'====================================================================
'Definicoes de variaveis, operacoes de banco e 
'liberacao de objetos de banco, fazer aqui

Dim rsData, strSQL

Set rsData = Server.CreateObject("ADODB.Recordset")
	
strSQL = "SELECT (CASE j.BancoID WHEN 604 THEN 237 ELSE j.BancoID END) AS BancoID, (CASE j.BancoID WHEN 604 THEN 'BRADESCO' WHEN 422 THEN 'BRADESCO' WHEN 637 THEN 'SANTANDER' WHEN 104 THEN 'ECON�MICA FEDERAL' ELSE UPPER(dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 5)) END) AS NomeBanco, " & _
		"(CASE dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 1) WHEN '604' THEN '237' WHEN '422' THEN '237' WHEN '637' THEN '033' ELSE dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 1) END) + " & _
		"(CASE m.Codigo WHEN '033' THEN '-7' WHEN '237' THEN '-2' WHEN '604' THEN '-2' WHEN '422' THEN '-2' WHEN '637' THEN '-7' WHEN '341' THEN '-7' ELSE '-9' END) AS CodigoDVBanco, " & _
		"(CASE  j.BancoID WHEN 422 THEN 'PAG�VEL EM QUALQUER BANCO AT� A DATA DO VENCIMENTO' ELSE 'QUALQUER BANCO AT� O VENCIMENTO' END ) AS LocalPagamento, " & _
		"CONVERT(VARCHAR(10), a.dtVencimento, 103) AS Vencimento, " & _
		"(CASE WHEN j.BancoID = 604 THEN '    ' + (SELECT UPPER(Banco) FROM dbo.Bancos WHERE BancoID = j.BancoID) + ' / ' + UPPER(c.Fantasia) WHEN j.BancoID = 422 THEN '    ' + 'BANCO SAFRA 581607890001/28' WHEN j.BancoID = 399 THEN '    ' + UPPER(c.Nome) + ' - ' + dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 111, 0) ELSE '    ' + UPPER(c.Nome) END) AS Cedente, " & _
		"(CASE c.TipoPessoaID WHEN 51 THEN 'CPF: ' ELSE 'CNPJ: ' END) AS TipoDocumentoCedente,  " & _
		"dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 111, 0) AS DocumentoCedente,  " & _
		"o.CEP AS CEPCedente,  " & _
		"(ISNULL(o.Endereco, SPACE(0)) + ISNULL(SPACE(1) + o.Numero, SPACE(0))) AS EnderecoCedente, " & _
        "(ISNULL(SPACE(1) + o.Bairro, SPACE(0)) + ' - ' + ISNULL(SPACE(1) + dbo.fn_Localidade_Dado(o.CidadeID,1), SPACE(0)) + ' - ' + ISNULL(SPACE(1) + dbo.fn_Localidade_Dado(o.UFID,2), SPACE(0))) AS BairroCidadeUFCedente, " & _
		"(CASE WHEN m.Codigo = '033'  " & _
		"THEN (dbo.fn_Pad(dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 21), 7, '0', 'L') + CHAR(47) + " & _
			"dbo.fn_Pad(SUBSTRING(n.CodigoTransmissao, 5 , 8), 14, '0', 'L')) " & _
		"WHEN (m.Codigo = '422' AND a.EmpresaID = 2) THEN '2372-8 / 33404-9'" & _
        "WHEN (m.Codigo = '422' AND a.EmpresaID = 10) THEN '23728 / 102261 '" & _
        "WHEN (m.Codigo = '422' AND a.EmpresaID = 21) THEN '23728 / 102431 '" & _
	    "WHEN m.Codigo = '745' THEN dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 21) + CHAR(47) + n.ContaCobranca + n.DVContaCobranca " & _
	    "WHEN m.Codigo = '604' THEN dbo.fn_ContaBancaria_Nome(61, 21) + CHAR(47) + n.ContaCobranca + '-' + n.DVContaCobranca " & _
        "WHEN m.Codigo = '637'  THEN (dbo.fn_Pad(dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 21), 4, '0', 'L') + CHAR(47) + dbo.Fn_pad('484501' , 6, '0', 'L') + '- 3') " &_
	    "ELSE dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 7) END) AS Agencia, " & _ 		
		"CONVERT(VARCHAR(10), a.dtEmissao, 103) AS dtDocumento, a.Duplicata AS NumeroDocumento, " & _
		"NULL AS EspecieDocumento, NULL AS Aceite, CONVERT(VARCHAR(10), a.dtEmissao, 103) AS dtProcessamento, " & _
		"(CASE j.BancoID WHEN 237 THEN a.Carteira + '/' + LEFT(a.NossoNumero,LEN(a.NossoNumero)-1) + '-' + RIGHT(a.NossoNumero,1) " & _
		"WHEN 604 THEN a.Carteira + '/' + LEFT(a.NossoNumero, LEN(a.NossoNumero) - 1) + '-' + RIGHT(a.NossoNumero, 1) " & _
		"WHEN 1 THEN (CASE n.Layout WHEN 1 THEN LEFT(a.NossoNumero, LEN(a.NossoNumero) - 1) + '-' + RIGHT(a.NossoNumero, 1) WHEN 2 THEN a.NossoNumero END) " & _
		"WHEN 422 THEN '09/' + RIGHT(CONVERT(VARCHAR(10), dtEmissao, 103), 2) + NossoNumero + '-' + dbo.fn_Digito_Calcula('09' + RIGHT(CONVERT(VARCHAR(10), dtEmissao, 103), 2) + NossoNumero, 3, NULL, 11, 2, 2, 7,NULL, NULL, NULL, NULL, NULL, 11, 'X') " & _
        "WHEN 341 THEN '0' + (LEFT(a.NossoNumero,LEN(a.NossoNumero)-1) + '-' + RIGHT(a.NossoNumero,1)) " & _
		" ELSE LEFT(a.NossoNumero,LEN(a.NossoNumero)-1) + '-' + RIGHT(a.NossoNumero,1) END) AS NossoNumero, NULL AS NumeroContaResponsavel, " & _		
		"(CASE j.BancoID WHEN 422 THEN '09' WHEN 637 THEN '05RCR' ELSE a.Carteira END) + (CASE j.BancoID WHEN 1 THEN ISNULL('-' + n.VariacaoCarteira,'') WHEN 33 THEN ISNULL(n.VariacaoCarteira,'') ELSE SPACE(0) END) AS Carteira, " & _				
		"ISNULL(CONVERT(NUMERIC(2), a.Instrucao2), 5) AS Instrucoes,b.SimboloMoeda AS Especie, (CASE WHEN j.BancoID = 1 THEN NULL ELSE 0 END) AS Quantidade, " & _
		"NULL AS Valor, CONVERT(VARCHAR(13), a.Valor) AS ValorDocumento, " & _
		"REPLACE(CONVERT(VARCHAR(13), CONVERT(NUMERIC(11,2), dbo.fn_DivideZero(a.Valor, k.DiasBase) * (l.TaxaMultaAtraso/100))),'.',',') AS JurosMora," & _
		"UPPER(d.Nome) AS NomeSacado, " & _
		"(CASE d.TipoPessoaID WHEN 51 THEN 'CPF: ' ELSE 'CNPJ: ' END) AS TipoDocumento, " & _
		"dbo.fn_Pessoa_Documento(d.PessoaID, NULL, NULL, 101, 111, 0) AS DocumentoSacado, e.CEP AS CEPSacado, " & _
		"f.CodigoLocalidade2 AS UFSacado, g.Localidade AS CidadeSacado, e.Bairro AS BairroSacado, " & _
		"(ISNULL(e.Endereco, SPACE(0)) + ISNULL(SPACE(1) + e.Numero, SPACE(0)) + ISNULL(SPACE(1) + " & _
		"e.Complemento, SPACE(0))) AS EnderecoSacado, dbo.fn_Financeiro_CodigoBarra(a.FinanceiroID, 1) AS CodigoBarra, " & _
		"dbo.fn_Financeiro_CodigoBarra(a.FinanceiroID, 3) AS LinhaDigitavel, " & _
		"(CASE WHEN j.BancoID = 604 THEN UPPER(c.Nome) WHEN j.BancoID = 422 THEN UPPER(c.Nome) + ' CNPJ:' + dbo.fn_Pessoa_Documento(c.PessoaID, NULL, NULL, 101, 111, 0) ELSE NULL END) AS SacadorAvalista, " & _
        "ISNULL((SELECT TOP 1 p.TransacaoID FROM Pedidos p WITH(NOLOCK) WHERE a.PedidoID = p.PedidoID), 0) AS TransacaoID " &_
	"FROM Financeiro a " & _ 
	            "INNER JOIN Conceitos b WITH(NOLOCK) ON a.MoedaID = b.ConceitoID " & _ 
	            "INNER JOIN Pessoas c WITH(NOLOCK) ON a.EmpresaID = c.PessoaID " & _ 
	            "INNER JOIN Pessoas d WITH(NOLOCK) ON a.PessoaID = d.PessoaID " & _ 
	            "INNER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON d.PessoaID = e.PessoaID " & _
	            "INNER JOIN Localidades f WITH(NOLOCK) ON e.UFID = f.LocalidadeID " & _ 
	            "INNER JOIN Localidades g WITH(NOLOCK) ON e.CidadeID = g.LocalidadeID " & _
		        "INNER JOIN RelacoesPessoas_Contas h WITH(NOLOCK) ON a.RelPesContaID = h.RelPesContaID " & _ 
		        "INNER JOIN RelacoesPessoas i WITH(NOLOCK) ON h.RelacaoID = i.RelacaoID " & _ 
		        "INNER JOIN Pessoas j WITH(NOLOCK) ON i.ObjetoID=j.PessoaID " & _ 
		        "INNER JOIN RelacoesPesRec k WITH(NOLOCK) ON c.PessoaID = k.SujeitoID " & _ 
		        "INNER JOIN RelacoesPesRec_Financ l WITH(NOLOCK) ON k.RelacaoID=l.RelacaoID " & _
		        "INNER JOIN Bancos m WITH(NOLOCK) ON j.BancoID = m.BancoID " & _
		        "INNER JOIN RelacoesPesRec_Cobranca n WITH(NOLOCK) ON k.RelacaoID = n.RelacaoID " & _				
		        "INNER JOIN dbo.Pessoas_Enderecos o WITH(NOLOCK) ON c.PessoaID = o.PessoaID " & _			
	"WHERE (a.FinanceiroID = " & CStr(nRegistroID) & " AND " & _
		"e.Ordem = 1 AND e.EndFaturamento = 1 AND a.MoedaID = l.MoedaID AND a.RelPesContaID = n.RelPesContaID AND " & _
		"k.TipoRelacaoID = 12 AND k.ObjetoID = 999 " & _
		"AND ((a.RelPesContaID = 1 AND (LEN(a.nossonumero) = 17 AND n.EstadoID = 2) OR (LEN(a.nossonumero) = 12 AND n.EstadoID = 3))    OR ( a.relpescontaid <> 1 )))"

rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText
	
If (Not rsData.EOF) Then
    nBancoID = rsData.Fields("BancoID").Value
	nTransacaoID = rsData.Fields("TransacaoID").Value

	If (nBancoID <> "745") Then
	    sNomeBanco = rsData.Fields("NomeBanco").Value
	Else    
	    sNomeBanco = " "
	End If
	sCodigoDVBanco = rsData.Fields("CodigoDVBanco").Value
	sVencimento = rsData.Fields("Vencimento").Value
	sAgenciaCodCedente = rsData.Fields("Agencia").Value
	sEspecie = rsData.Fields("Especie").Value
	sQuantidade = rsData.Fields("Quantidade").Value
	sValorDocumento = rsData.Fields("ValorDocumento").Value
	sDescAbat = ""
	sMoraMultaJur = ""
	sValorCobrado = ""
	sNossoNumero = rsData.Fields("NossoNumero").Value
	sTipoDocumento = rsData.Fields("TipoDocumento").Value
	sNumeroDocumento = rsData.Fields("NumeroDocumento").Value
	sSacado = rsData.Fields("NomeSacado").Value
	sDocumentoSacado = sTipoDocumento & _
		rsData.Fields("DocumentoSacado").Value
	sLinhaDigitavel = rsData.Fields("LinhaDigitavel").Value
	
	'Bando HSBC
	If (nBancoID = "399") Then 
	    sCedente = rsData.Fields("Cedente").Value
	    sLocalPagamento = "PAGAR EM QUALQUER AG�NCIA BANC�RIA AT� O VENCIMENTO OU CANAIS DO HSBC"
	    sEspecieDoc = "PD"
        sCarteira = "CSB"
        sQuantidade = ""
    ElseIf (nBancoID = "341") Then
        sNomeBanco = "Banco Ita� SA"
        sLocalPagamento = "QUALQUER BANCO AT� O VENCIMENTO. AP�S O VENCIMENTO PAGUE SOMENTE NO ITA�"
        sEspecieDoc = "DP"
        sCarteira = rsData.Fields("Carteira").Value
        sNossoNumero = rsData.Fields("Carteira").Value + "/" + rsData.Fields("NossoNumero").Value
   ElseIf (nBancoID = "237") Then        
        sLocalPagamento = "PAG�VEL PREFERENCIALMENTE NAS AG�NCIAS DO BRADESCO"
        sEspecieDoc = "DM"
        sCarteira = rsData.Fields("Carteira").Value        
        sQuantidade = ""
    Else
        sLocalPagamento = "QUALQUER BANCO AT� O VENCIMENTO"
        sEspecieDoc = "DM"
        sCarteira = rsData.Fields("Carteira").Value
    End If	    
    
	
	sDataDocumento = rsData.Fields("dtDocumento").Value
	
	sAceite = "N"
	sDataProcessamento = rsData.Fields("dtProcessamento").Value
	If (nBancoID = "745") Then 
	    sNumContResp = "Cliente"
	    sEspecieDoc = "DMI"
	    sCedente = rsData.Fields("Cedente").Value & " - " & sTipoDocumento & _
	        rsData.Fields("DocumentoCedente") & "\n" & _
	        rsData.Fields("EnderecoCedente") & " - CEP:" & rsData.Fields("CEPCedente") 
	ElseIf (nBancoID = "1") Then 
	    sCedente = rsData.Fields("Cedente").Value & " - " & sTipoDocumento & _
	        rsData.Fields("DocumentoCedente") & "\n" & rsData.Fields("EnderecoCedente") & " - CEP:" & rsData.Fields("CEPCedente") 
    ElseIf (nBancoID = "422") Then      
        sCedente = rsData.Fields("Cedente").Value
        sLocalPagamento = rsData.Fields("LocalPagamento").Value
        sQuantidade = ""
        sEspecieDoc = "DM"
        sCarteira = rsData.Fields("Carteira").Value
        sNumContResp = "CIP130"
	Else
	    sNumContResp = ""
	    sCedente = rsData.Fields("Cedente").Value	
	End If 
	
	sValor = ""
	'Alterar o campo abaixo
    If (nBancoID = "237") Then    
	    sInstrucoes = "JUROS POR DIA DE ATRASO R$ " & CStr(rsData.Fields("JurosMora").Value) & "\n" & _
	                  "PROTESTAR NO 4. DIA AP�S O VENCIMENTO"
	                  '"PROTESTAR NO " & CStr(rsData.Fields("Instrucoes").Value) & ". DIA UTIL APOS O VENCIMENTO"
    ElseIf (nBancoID = "745" or nBancoID = "33") Then
	    sInstrucoes = "JUROS POR DIA DE ATRASO R$ " & CStr(rsData.Fields("JurosMora").Value) & "\n" & _
	                  "PROTESTAR NO 4. DIA CORRIDO A CONTAR DO VENCIMENTO\n\n"
    ElseIf (nBancoID = "422") Then
        nBancoID = "237"
        sMensagemSacado = "\nESTE BOLETO REPRESENTA DUPLICATA CEDIDA FIDUCIARIAMENTE AO BANCO SAFRA S/A, FICANDO VEDADO O PAGAMENTO DE QUALQUER OUTRA FORMA QUE N�O ATRAV�S DO PRESENTE BOLETO.\n"
        sInstrucoes = sMensagemSacado & "\nN�O DISPENSAR MORA DE R$ " & CStr(rsData.Fields("JurosMora").Value) & " por dia por dia de atraso.\n" & _
	                  "PROTESTAR NO 5� DIA �TIL AP�S O VENCIMENTO"
    ElseIf (nBancoID = "1") Then
        sMensagemSacado = "    " & rsData.Fields("Cedente").Value & "\n    " & sTipoDocumento & _
	        rsData.Fields("DocumentoCedente") & "\n    " & rsData.Fields("EnderecoCedente") & " - CEP:" & rsData.Fields("CEPCedente") 
        sInstrucoes = "JUROS POR DIA DE ATRASO R$ " & CStr(rsData.Fields("JurosMora").Value) & "\n" & _
	                  "PROTESTAR NO 5. DIA UTIL AP�S O VENCIMENTO"	        
    ElseIf (nBancoID = "341") Then
        sMensagemSacado = rsData.Fields("Cedente").Value & "\n    " & rsData.Fields("EnderecoCedente") & rsData.Fields("BairroCidadeUFCedente") & "\n    CEP:" & rsData.Fields("CEPCedente")
        sInstrucoes = "JUROS POR DIA DE ATRASO R$ " & CStr(rsData.Fields("JurosMora").Value) & "\n" & _
	                  "PROTESTAR NO 5. DIA UTIL AP�S O VENCIMENTO"
    Else
        sInstrucoes = "JUROS POR DIA DE ATRASO R$ " & CStr(rsData.Fields("JurosMora").Value) & "\n" & _
	                  "PROTESTAR NO 5. DIA UTIL AP�S O VENCIMENTO"
	                  '"PROTESTAR NO " & CStr(rsData.Fields("Instrucoes").Value) & ". DIA UTIL APOS O VENCIMENTO"
    End If

    If (nTransacaoID = "215") Then    
        sInstrucoes = sInstrucoes & "\n" & "EM CASO DE RETEN��O DE IMPOSTOS, FAVOR UTILIZAR O CAMPO ABATIMENTO PARA ABATER O VALOR TOTAL DE IMPOSTOS RETIDOS"
    End If

	sSacadoComEndereco = rsData.Fields("NomeSacado").Value & " - " & sTipoDocumento & _
		rsData.Fields("DocumentoSacado").Value & "\n" & _
		rsData.Fields("EnderecoSacado").Value & " " & rsData.Fields("BairroSacado").Value & "\n" & _
		rsData.Fields("CEPSacado").Value & " " & rsData.Fields("CidadeSacado").Value & " " & _
		rsData.Fields("UFSacado").Value
	sCodigoBarras = rsData.Fields("CodigoBarra").Value
	sSacadorAval = rsData.Fields("SacadorAvalista").Value
End If

rsData.Close
Set rsData = Nothing

'Valores fixos para desenvolvimento da pagina
'nBancoID = 1
'sNomeBanco = "BANCO DO BRASIL"
'sCodigoDVBanco = "001-9"
'sVencimento = "10/10/2003"
'sAgenciaCodCedente = "01769-8/000000011022-1"
'sEspecie = "R$"
'sQuantidade = "0,00"
'sValorDocumento = "2.876,00"
'sDescAbat = "1,25"
'sMoraMultaJur = "2,41"
'sValorCobrado = "1.234,56"
'sNossoNumero = "18847241745-2"
'sNumeroDocumento = "014485A"
'sSacado = "WCK COM E REPRESENTACOES
'sDocumentoSacado = "CNPJ: 02.744.304/0001-99"
'sLinhaDigitavel = "0091.88473  24174.517698  00011.022175   1  21960000287600"
'sLocalPagamento = "QUALQUER BANCO AT� O VENCIMENTO"
'sCedente = "ABANO RJ DISTRIBUIDORA LTDA"
'sDataDocumento = "09/10/2003"
'sEspecieDoc = "DM"
'sAceite = "N"
'sDataProcessamento = "09/12/2004"
'sNumContResp = "12321-6"
'sCarteira = "17-019"
'sValor = "1.000,00"
'sInstrucoes = "JUROS DE MORA DE R$8,26 POR DIA CORRIDO\n" & _
'              "PROTESTAR NO 7o. DIA CORRIDO APOS O VENCIMENTO"
'sSacadoComEndereco = "WCK COM E REPRESENTACOES - CNPJ: 02.744.304/0001-99\n" & _
'                     "R. RONALDO SCAPINE, 765 JD. DA PENHA\n" & _
'                     "29.060-760 - VITORIA ES"
'sCodigoBarras = "00196219600002950001884724174317690001102217"

'Final de definicoes de variaveis, operacoes de banco e 
'liberacao de objetos de banco, fazer aqui
'====================================================================

'O logotipo do Banco
sLogBancoSrc = sURLRoot & "/serversidegenEx/imageblob.aspx?nFormID=9220&" & _
			   "nSubFormID=28230&nTamanho=2&nRegistroID=" & Cstr(nBancoID)

Response.Write "var glb_sLogBancoSrc = " & Chr(34) & sLogBancoSrc & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sNomeBanco = " & Chr(34) & sNomeBanco & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sCodigoDVBanco = " & Chr(34) & sCodigoDVBanco & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sVencimento = " & Chr(34) & sVencimento & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf    

Response.Write "var glb_sAgenciaCodCedente = " & Chr(34) & sAgenciaCodCedente & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sEspecie = " & Chr(34) & sEspecie & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sQuantidade = " & Chr(34) & sQuantidade & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sValorDocumento = " & Chr(34) & sValorDocumento & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sDescAbat = " & Chr(34) & sDescAbat & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sMoraMultaJur = " & Chr(34) & sMoraMultaJur & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                
    
Response.Write "var glb_sValorCobrado = " & Chr(34) & sValorCobrado & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sNossoNumero = " & Chr(34) & sNossoNumero & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sNumeroDocumento = " & Chr(34) & sNumeroDocumento & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sSacado = " & Chr(34) & sSacado & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sDocumentoSacado = " & Chr(34) & sDocumentoSacado & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sMensagemSacado = " & Chr(34) & sMensagemSacado & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sLinhaDigitavel = " & Chr(34) & sLinhaDigitavel & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sLocalPagamento = " & Chr(34) & sLocalPagamento & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf                

Response.Write "var glb_sCedente = " & Chr(34) & sCedente & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sDataDocumento = " & Chr(34) & sDataDocumento & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sEspecieDoc = " & Chr(34) & sEspecieDoc & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sAceite = " & Chr(34) & sAceite & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sDataProcessamento = " & Chr(34) & sDataProcessamento & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sNumContResp = " & Chr(34) & sNumContResp & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sCarteira = " & Chr(34) & sCarteira & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sValor = " & Chr(34) & sValor & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sInstrucoes = " & Chr(34) & sInstrucoes & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sSacadoComEndereco = " & Chr(34) & sSacadoComEndereco & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_sCodigoBarras = " & Chr(34) & sCodigoBarras & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "var glb_SacadorAval = " & Chr(34) & sSacadorAval & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

srvName = Request.ServerVariables("SERVER_NAME")
		
If ( Instr(1,UCase(srvName), ".COM", 1)= 0	) Then
	sBarCodeSrc = "http:/" & "/localhost/overfly3"
Else
	sBarCodeSrc = pagesURLRoot	
End If

Response.Write "var glb_sBarCodeSrc = " & Chr(34) & sBarCodeSrc & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "</script>"
Response.Write vbcrlf
%>
<script ID="wndJSProc" LANGUAGE="javascript">
<!--
    //-->
</script>

</head>

<body id="bloquetocobrancaBody" name="bloquetocobrancaBody" LANGUAGE="javascript" onload="return window_onload()">
	<div id="divMarginLeftTop" name="divMarginLeftTop" class="divExtern"></div>

	<div id="divBoleto" name="divBoleto" class="divExtern">
		<div id="divReciboEntrega" name="divReciboEntrega" class="divExtern">
			<div id="divRecEntr_L1" name="divRecEntr_L1" class="divExtern">
				<div id="divRecEntr_LogBanco" name="divRecEntr_LogBanco" class="divExtern">
					<img ID="img_LogBanco_RecEntr" name="img_LogBanco_RecEntr" class="imgGeneral" Language=javascript onload="return img_onload(this)" onerror="return img_onerror(this)"></IMG>
				</div>
				<p id="lblRecEntr_L1_NomeBanco" name="lblRecEntr_L1_NomeBanco" class="lblGeneral"></p>
				<p id="lblRecEntr_L1_CodigoDVBanco" name="lblRecEntr_L1_CodigoDVBanco" class="lblGeneral"></p>
				<p id="lblRecEntr_L1_RecEntr" name="lblRecEntr_L1_RecEntr" class="lblGeneral"></p>
			</div>
			<div id="divRecEntr_L2" name="divRecEntr_L2" class="divExtern">
				<p id="lblRecEntr_L2_Vcto_Label" name="lblRecEntr_L2_Vcto_Label" class="lblGeneral"></p>
				<p id="lblRecEntr_L2_Vcto_Value" name="lblRecEntr_L2_Vcto_Value" class="lblGeneral"></p>
				<p id="lblRecEntr_L2_AgCodCed_Label" name="lblRecEntr_L2_AgCodCed_Label" class="lblGeneral"></p>
				<p id="lblRecEntr_L2_AgCodCed_Value" name="lblRecEntr_L2_AgCodCed_Value" class="lblGeneral"></p>
				<p id="lblRecEntr_L2_Especie_Label" name="lblRecEntr_L2_Especie_Label" class="lblGeneral"></p>
				<p id="lblRecEntr_L2_Especie_Value" name="lblRecEntr_L2_Especie_Value" class="lblGeneral"></p>
				<p id="lblRecEntr_L2_Quantidade_Label" name="lblRecEntr_L2_Quantidade_Label" class="lblGeneral"></p>
				<p id="lblRecEntr_L2_Quantidade_Value" name="lblRecEntr_L2_Quantidade_Value" class="lblGeneral"></p>
			</div>
			<div id="divRecEntr_L3" name="divRecEntr_L3" class="divExtern">
				<p id="lblRecEntr_L3_VlrDoc_Label" name="lblRecEntr_L3_VlrDoc_Label" class="lblGeneral"></p>
				<p id="lblRecEntr_L3_VlrDoc_Value" name="lblRecEntr_L3_VlrDoc_Value" class="lblGeneral"></p>
				<p id="lblRecEntr_L3_NossoNumero_Label" name="lblRecEntr_L3_NossoNumero_Label" class="lblGeneral"></p>
				<p id="lblRecEntr_L3_NossoNumero_Value" name="lblRecEntr_L3_NossoNumero_Value" class="lblGeneral"></p>
			</div>
			<div id="divRecEntr_L4" name="divRecEntr_L4" class="divExtern">
				<p id="lblRecEntr_L4_Sacado_Label" name="lblRecEntr_L4_Sacado_Label" class="lblGeneral"></p>
				<p id="lblRecEntr_L4_Sacado_Value" name="lblRecEntr_L4_Sacado_Value" class="lblGeneral"></p>
			</div>
			<div id="divRecEntr_L5" name="divRecEntr_L5" class="divExtern">
				<p id="lblRecEntr_L5_AssRecebedor" name="lblRecEntr_L5_AssRecebedor" class="lblGeneral"></p>
				<p id="lblRecEntr_L5_DataEntrega" name="lblRecEntr_L5_DataEntrega" class="lblGeneral"></p>
			</div>
		</div>
		<div id="divReciboSacado" name="divReciboSacado" class="divExtern">
			<div id="divRecSac_L1" name="divRecSac_L1" class="divExtern">
				<div id="divRecSac_LogBanco" name="divRecSac_LogBanco" class="divExtern">
					<img ID="img_LogBanco_RecSac" name="img_LogBanco_RecSac" class="imgGeneral" Language=javascript onload="return img_onload(this)" onerror="return img_onerror(this)"></IMG>
				</div>
				<p id="lblRecSac_L1_NomeBanco" name="lblRecSac_L1_NomeBanco" class="lblGeneral"></p>
				<p id="lblRecSac_L1_CodigoDVBanco" name="lblRecSac_L1_CodigoDVBanco" class="lblGeneral"></p>
				<p id="lblRecSac_L1_RecSac" name="lblRecSac_L1_RecSac" class="lblGeneral"></p>
			</div>
			<div id="divRecSac_L2" name="divRecSac_L2" class="divExtern">
				<p id="lblRecSac_L2_Vcto_Label" name="lblRecSac_L2_Vcto_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L2_Vcto_Value" name="lblRecSac_L2_Vcto_Value" class="lblGeneral"></p>
				<p id="lblRecSac_L2_AgCodCed_Label" name="lblRecSac_L2_AgCodCed_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L2_AgCodCed_Value" name="lblRecSac_L2_AgCodCed_Value" class="lblGeneral"></p>
				<p id="lblRecSac_L2_Especie_Label" name="lblRecSac_L2_Especie_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L2_Especie_Value" name="lblRecSac_L2_Especie_Value" class="lblGeneral"></p>
				<p id="lblRecSac_L2_Quantidade_Label" name="lblRecSac_L2_Quantidade_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L2_Quantidade_Value" name="lblRecSac_L2_Quantidade_Value" class="lblGeneral"></p>
			</div>
			<div id="divRecSac_L3" name="divRecSac_L3" class="divExtern">
				<p id="lblRecSac_L3_VlrDoc_Label" name="lblRecSac_L3_VlrDoc_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L3_VlrDoc_Value" name="lblRecSac_L3_VlrDoc_Value" class="lblGeneral"></p>
				<p id="lblRecSac_L3_DescAbat_Label" name="lblRecSac_L3_DescAbat_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L3_DescAbat_Value" name="lblRecSac_L3_DescAbat_Value" class="lblGeneral"></p>
				<p id="lblRecSac_L3_MoraMultaJur_Label" name="lblRecSac_L3_MoraMultaJur_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L3_MoraMultaJur_Value" name="lblRecSac_L3_MoraMultaJur_Value" class="lblGeneral"></p>
			</div>
			<div id="divRecSac_L4" name="divRecSac_L4" class="divExtern">
				<p id="lblRecSac_L4_VlrCobrado_Label" name="lblRecSac_L4_VlrCobrado_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L4_VlrCobrado_Value" name="lblRecSac_L4_VlrCobrado_Value" class="lblGeneral"></p>
				<p id="lblRecSac_L4_NossoNumero_Label" name="lblRecSac_L4_NossoNumero_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L4_NossoNumero_Value" name="lblRecSac_L4_NossoNumero_Value" class="lblGeneral"></p>
				<p id="lblRecSac_L4_NumeroDocumento_Label" name="lblRecSac_L4_NumeroDocumento_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L4_NumeroDocumento_Value" name="lblRecSac_L4_NumeroDocumento_Value" class="lblGeneral"></p>
			</div>
			<div id="divRecSac_L5" name="divRecSac_L5" class="divExtern">
				<p id="lblRecSac_L5_Sacado_Label" name="lblRecSac_L5_Sacado_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L5_Sacado_Value" name="lblRecSac_L5_Sacado_Value" class="lblGeneral"></p>
			</div>
		    <div id="divRecSac_L6" name="divRecSac_L6" class="divExtern">
				<p id="lblRecSac_L6_MsgSacado_Label" name="lblRecSac_L6_MsgSacado_Label" class="lblGeneral"></p>
				<p id="lblRecSac_L6_MsgSacado_Value" name="lblRecSac_L6_MsgSacado_Value" class="lblGeneral"></p>
			</div>
			<div id="divRecSac_L7" name="divRecSac_L7" class="divExtern">
				<p id="lblRecSac_L7_AutentiMec" name="lblRecSac_L7_AutentiMec" class="lblGeneral"></p>
			</div>
		</div>
		<div id="divFichaCompensacao" name="divFichaCompensacao" class="divExtern">
			<div id="divFichComp_L1" name="divFichComp_L1" class="divExtern">
				<div id="divFichComp_LogBanco" name="divFichComp_LogBanco" class="divExtern">
					<img ID="img_LogBanco_FichComp" name="img_LogBanco_FichComp" class="imgGeneral" Language=javascript onload="return img_onload(this)" onerror="return img_onerror(this)"></IMG>
				</div>
				<p id="lblFichComp_L1_NomeBanco" name="lblFichComp_L1_NomeBanco" class="lblGeneral"></p>
				<p id="lblFichComp_L1_CodigoDVBanco" name="lblFichComp_L1_CodigoDVBanco" class="lblGeneral"></p>
				<p id="lblFichComp_L1_LinDigit" name="lblFichComp_L1_LinDigit" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L2" name="divFichComp_L2" class="divExtern">
				<p id="lblFichComp_L2_LocalPgto_Label" name="lblFichComp_L2_LocalPgto_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L2_LocalPgto_Value" name="lblFichComp_L2_LocalPgto_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L2_Vcto_Label" name="lblFichComp_L2_Vcto_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L2_Vcto_Value" name="lblFichComp_L2_Vcto_Value" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L3" name="divFichComp_L3" class="divExtern">
				<p id="lblFichComp_L3_Cedente_Label" name="lblFichComp_L3_Cedente_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L3_Cedente_Value" name="lblFichComp_L3_Cedente_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L3_AgCodCed_Label" name="lblFichComp_L3_AgCodCed_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L3_AgCodCed_Value" name="lblFichComp_L3_AgCodCed_Value" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L4" name="divFichComp_L4" class="divExtern">
				<p id="lblFichComp_L4_DataDocumento_Label" name="lblFichComp_L4_DataDocumento_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L4_DataDocumento_Value" name="lblFichComp_L4_DataDocumento_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L4_NumeroDocumento_Label" name="lblFichComp_L4_NumeroDocumento_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L4_NumeroDocumento_Value" name="lblFichComp_L4_NumeroDocumento_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L4_EspecieDocumento_Label" name="lblFichComp_L4_EspecieDocumento_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L4_EspecieDocumento_Value" name="lblFichComp_L4_EspecieDocumento_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L4_Aceite_Label" name="lblFichComp_L4_Aceite_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L4_Aceite_Value" name="lblFichComp_L4_Aceite_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L4_DataProcessamento_Label" name="lblFichComp_L4_DataProcessamento_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L4_DataProcessamento_Value" name="lblFichComp_L4_DataProcessamento_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L4_NossoNumero_Label" name="lblFichComp_L4_NossoNumero_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L4_NossoNumero_Value" name="lblFichComp_L4_NossoNumero_Value" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L5" name="divFichComp_L5" class="divExtern">
				<p id="lblFichComp_L5_NumContResp_Label" name="lblFichComp_L5_NumContResp_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L5_NumContResp_Value" name="lblFichComp_L5_NumContResp_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Carteira_Label" name="lblFichComp_L5_Carteira_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Carteira_Value" name="lblFichComp_L5_Carteira_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Especie_Label" name="lblFichComp_L5_Especie_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Especie_Value" name="lblFichComp_L5_Especie_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Quantidade_Label" name="lblFichComp_L5_Quantidade_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Quantidade_Value" name="lblFichComp_L5_Quantidade_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Multiplic" name="lblFichComp_L5_Multiplic" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Valor_Label" name="lblFichComp_L5_Valor_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L5_Valor_Value" name="lblFichComp_L5_Valor_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L5_VlrDoc_Label" name="lblFichComp_L5_VlrDoc_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L5_VlrDoc_Value" name="lblFichComp_L5_VlrDoc_Value" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L6_R_1" name="divFichComp_L6_R_1" class="divExtern">
				<p id="lblFichComp_L6_R_1_Desconto_Label" name="lblFichComp_L6_R_1_Desconto_Label" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L6_R_2" name="divFichComp_L6_R_2" class="divExtern">
				<p id="lblFichComp_L6_R_2_OutrasDed_Label" name="lblFichComp_L6_R_2_OutrasDed_Label" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L6_R_3" name="divFichComp_L6_R_3" class="divExtern">
				<p id="lblFichComp_L6_R_3_MoraMultaJuros_Label" name="lblFichComp_L6_R_3_MoraMultaJuros_Label" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L6_R_4" name="divFichComp_L6_R_4" class="divExtern">
				<p id="lblFichComp_L6_R_4_OutrosAcrescimos_Label" name="lblFichComp_L6_R_4_OutrosAcrescimos_Label" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L6_R_5" name="divFichComp_L6_R_5" class="divExtern">
				<p id="lblFichComp_L6_R_5_ValorCobrado_Label" name="lblFichComp_L6_R_5_ValorCobrado_Label" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L6_L" name="divFichComp_L6_L" class="divExtern">
				<p id="lblFichComp_L6_L_Instrucoes_Label" name="lblFichComp_L6_L_Instrucoes_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L6_L_Instrucoes_Value" name="lblFichComp_L6_L_Instrucoes_Value" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L7" name="divFichComp_L7" class="divExtern">
				<p id="lblFichComp_L7_Sacado_Label" name="lblFichComp_L7_Sacado_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L7_Sacado_Value" name="lblFichComp_L7_Sacado_Value" class="lblGeneral"></p>
				<p id="lblFichComp_L7_SacadorAval" name="lblFichComp_L7_SacadorAval" class="lblGeneral"></p>
				<p id="lblFichComp_L7_CodBaixa" name="lblFichComp_L7_CodBaixa" class="lblGeneral"></p>
			</div>
			<div id="divFichComp_L8" name="divFichComp_L8" class="divExtern">
				<p id="lblFichComp_L8_Autentic_Mec" name="lblFichComp_L8_Autentic_Mec_Label" class="lblGeneral"></p>
				<p id="lblFichComp_L8_FichComp" name="lblFichComp_L8_FichComp" class="lblGeneral"></p>
			</div>
		
			<img ID="img_BarCode_FichComp" name="img_BarCode_FichComp" class="imgGeneral" Language=javascript onload="return img_onload(this)" onerror="return img_onerror(this)"></IMG>
		
		</div>
		
	</div>
		
	<div id="divMarginRightBottom" name="divMarginRightBottom" class="divExtern"></div>
	
	</body>

</html>
