
<%@ LANGUAGE=VBSCRIPT @EnableSessionState=False %>


<%
    Option Explicit
    Response.Expires = 0
    
    'Forca recarregamento da pagina
    Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
    
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
Function ReplaceText(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceText = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function

Function MatchText(baseStr, patrn)
    Dim regEx, Match, Matches    ' Create variable.
    Set regEx = New RegExp       ' Create regular expression.
    regEx.Pattern = patrn        ' Set pattern.
    regEx.IgnoreCase = True      ' Set case insensitivity.
    regEx.Global = True          ' Set global applicability.
    Set Matches = regEx.Execute(baseStr)   ' Execute search.
       
    MatchText = Matches.Count
End Function
%>

<html id="modaldocumentotemplateHtml" name="modaldocumentotemplateHtml">

<head>

<title></title>

<%
	Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/documentohtml.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/documentohtml.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nRegistroID, nTipoID, sTexto, nTemFoto, nEmpresaID, sImgSRC, nCounter, nUsuarioID, nVisao, dtVigencia
Dim nAction, sAnchorID

Dim rsData
Dim strSQL
Dim rsSPCommand
Dim nPaisID

nRegistroID = 0
nUsuarioID = 0
sTexto = ""
nTemFoto = 0
nEmpresaID = 0
sImgSRC = ""
nCounter = 1
nPaisID = 0

For i = 1 To Request.QueryString("nRegistroID").Count
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

For i = 1 To Request.QueryString("nTipoID").Count
    nTipoID = Request.QueryString("nTipoID")(i)
Next

For i = 1 To Request.QueryString("nTemFoto").Count
    nTemFoto = Request.QueryString("nTemFoto")(i)
Next

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next

For i = 1 To Request.QueryString("nUsuarioID").Count
    nUsuarioID = Request.QueryString("nUsuarioID")(i)
Next

For i = 1 To Request.QueryString("nVisao").Count
    nVisao = Request.QueryString("nVisao")(i)
Next

For i = 1 To Request.QueryString("dtVigencia").Count
    dtVigencia = Request.QueryString("dtVigencia")(i)
Next

Set rsData = Server.CreateObject("ADODB.Recordset")
	
Set rsSPCommand = Server.CreateObject("ADODB.Command")

With rsSPCommand
	.CommandTimeout = 60 * 10
    .ActiveConnection = strConn
    .CommandType = adCmdStoredProc
    
    If (CInt(nTipoID) = 1) Then
		.CommandText = "sp_RACP_Relatorio"
		.Parameters.Append( .CreateParameter("@RACPID", adInteger, adParamInput, 10 , nRegistroID) )
	ElseIf (CInt(nTipoID) = 2) Then
		.CommandText = "sp_RAI_Relatorio"
		.Parameters.Append( .CreateParameter("@RAIID", adInteger, adParamInput, 10 , nRegistroID) )
		.Parameters.Append( .CreateParameter("@TipoResultadoID", adInteger, adParamInput, 10 , 1) )
		nCounter = 4
	ElseIf (CInt(nTipoID) = 3) Then
		.CommandText = "sp_Pessoa_Resumo"
		.Parameters.Append( .CreateParameter("@PessoaID", adInteger, adParamInput, 10 , nRegistroID) )
		.Parameters.Append( .CreateParameter("@MostraContatos",adBoolean, adParamInput, 10, 1) )
		.Parameters.Append( .CreateParameter("@UsuarioID",adInteger, adParamInput, 10, nUsuarioID) )
		.Parameters.Append( .CreateParameter("@EmpresaID",adInteger, adParamInput, 10, nEmpresaID) )
	ElseIf (CInt(nTipoID) = 4) Then
		.CommandText = "sp_RRC_Relatorio"
		.Parameters.Append( .CreateParameter("@RRCID", adInteger, adParamInput, 10 , nRegistroID) )
	ElseIf (CInt(nTipoID) = 5) Then
		.CommandText = "sp_RAD_Relatorio"
		.Parameters.Append( .CreateParameter("@RADID", adInteger, adParamInput, 10 , nRegistroID) )
		.Parameters.Append( .CreateParameter("@TipoResultadoID", adInteger, adParamInput, 10 , 1) )
		nCounter = 4
	ElseIf (CInt(nTipoID) = 6) Then
		.CommandText = "sp_PSC_Relatorio"
		.Parameters.Append( .CreateParameter("@PSCID", adInteger, adParamInput, 10 , nRegistroID) )
	ElseIf (CInt(nTipoID) = 7) Then
		.CommandText = "sp_RET_Relatorio"
		.Parameters.Append( .CreateParameter("@RETID", adInteger, adParamInput, 10 , nRegistroID) )
		.Parameters.Append( .CreateParameter("@TipoResultadoID", adInteger, adParamInput, 10 , 1) )
		nCounter = 4
	ElseIf (CInt(nTipoID) = 8) Then
		.CommandText = "sp_RelacoesPessoas_Resumo"
		.Parameters.Append( .CreateParameter("@RelacaoID", adInteger, adParamInput, 10 , nRegistroID) )
		.Parameters.Append( .CreateParameter("@EmpresaID", adInteger, adParamInput, 10 , nEmpresaID) )
		.Parameters.Append( .CreateParameter("@IncluiDadosPessoa", adBoolean, adParamInput, 10 , 1) )
	ElseIf (CInt(nTipoID) = 9) Then
		.CommandText = "sp_Fopag_Resumo"
		.Parameters.Append( .CreateParameter("@FopagID", adInteger, adParamInput, 10 , nRegistroID) )
		.Parameters.Append( .CreateParameter("@DadosFopag", adBoolean, adParamInput, 10 , Null) )
		.Parameters.Append( .CreateParameter("@DadosVendas", adBoolean, adParamInput, 10 , Null) )
		.Parameters.Append(.CreateParameter("@UsuarioID",adInteger,adParamInput,10,nUsuarioID))
	ElseIf (CInt(nTipoID) = 10) Then
		.CommandText = "sp_AcoesMarketing_Resumo"
		.Parameters.Append( .CreateParameter("@AcaoID", adInteger, adParamInput, 10 , nRegistroID) )
	ElseIf (CInt(nTipoID) = 11) Then
	
		.CommandText = "sp_RegrasFiscais_Resumo"
		.Parameters.Append( .CreateParameter("@RegraFiscalID", adInteger, adParamInput, 10 , nRegistroID) )
		.Parameters.Append( .CreateParameter("@Visao", adInteger, adParamInput, 10 , nVisao) )
		
		If (dtVigencia = "") Then
		    .Parameters.Append( .CreateParameter("@Vigencia", adDate, adParamInput, 8 , Null) )
        Else		    
            .Parameters.Append( .CreateParameter("@Vigencia", adDate, adParamInput, 8 , dtVigencia) )
        End If
    ElseIf (CInt(nTipoID) = 12) Then
        strSQL = "SELECT dbo.fn_Pessoa_Localidade(" & CStr(nEmpresaID) & ", 1, NULL, NULL) AS PaisID"

        rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText

        nPaisID = rsData.Fields("PaisID").Value

	    rsData.Close
	    Set rsData = Nothing


	    .CommandText = "sp_Credito_Relatorio"
	    .Parameters.Append( .CreateParameter("@RegistroID", adInteger, adParamInput, 10 , nRegistroID) )
        .Parameters.Append( .CreateParameter("@PaisID", adInteger, adParamInput, 10 , nPaisID) )
	    .Parameters.Append( .CreateParameter("@TipoResultadoID", adInteger, adParamInput, 10 , 1) )
		
	End If
	
End With

If (CInt(nTipoID) = 9 OR CInt(nTipoID) = 10 OR CInt(nTipoID) = 11) Then
	Set rsData = rsSPCommand.Execute

    If (Not (rsData.BOF AND rsData.EOF)) Then
        While Not (rsData.EOF)
			sTexto = sTexto & rsData.Fields("Texto").Value
			rsData.MoveNext()
		WEnd
	End If
Else
	For i = 1 To nCounter
		If (CInt(nTipoID) = 2 OR CInt(nTipoID) = 5 OR CInt(nTipoID) = 7) Then
			rsSPCommand.Parameters("@TipoResultadoID") = i
		End If

		Set rsData = rsSPCommand.Execute

		If (rsData.Fields.Count <> 0) Then		
			If (Not rsData.EOF) Then
				sTexto = sTexto & rsData.Fields("Resultado").Value
			End If
		End If
	Next
End If

sTexto = ReplaceText( sTexto, Chr(13), "")
sTexto = ReplaceText( sTexto, Chr(34), "")
sTexto = ReplaceText( sTexto, Chr(10), "")
sTexto = ReplaceText( sTexto, "<TD></TD>", "<TD>&nbsp</TD>")

Response.Write "glb_sTexto = " & Chr(34) & sTexto & Chr(34) & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "glb_nTemFoto = " & nTemFoto & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "glb_nRegistroID = " & nRegistroID & ";" & vbCrLf
Response.Write vbCrLf

Response.Write "glb_nTipoID = " & nTipoID & ";" & vbCrLf
Response.Write vbCrLf

If (CInt(nTipoID) <> 9) Then
	rsData.Close
End If

Set rsData = Nothing
Set rsSPCommand = Nothing
    
Response.Write "</script>"
Response.Write vbcrlf
%>
<script ID="wndJSProc" LANGUAGE="javascript">
<!--

    //-->
</script>

</head>

<body id="modaldocumentotemplateBody" name="modaldocumentotemplateBody" LANGUAGE="javascript" onload="return window_onload()">
	<font face="Tahoma, Verdana, Helvetica, sans-serif">

	<div id="divDocumento" name="divDocumento" class="fldGeneral">	    
	</div>
	
	</font>
</body>

</html>
