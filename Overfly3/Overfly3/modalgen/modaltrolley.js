/********************************************************************
modaltrolley.js

Library javascript para o modaltrolley.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_aEmpresaData = null;
var glb_nUserID = 0;
var glb_nPessoaID = 0;
var glb_nParceiroID = 0;
var glb_nPessoaDefaultID = 0;
var glb_nVendedorDefaultID = 0;
var glb_MsgCamapanhaAlert = '';
var glb_AlteradoPessoa = false;
// Variavel para carregamento de combo dinamico

var glb_CounterCmbsDynamics = 0;
var glb_bAlteracaoPendente = false;

var glb_EnableChkOK = false;
var glb_atertSacolaAtual = false;
var glb_AtualizaEnableChkOK = true;
var glb_TransferenciaItem = false;
var glb_SacolaTransferenciaID = 0;
var glb_nItensTransferidos = 0;
var glb_deleteItem = false;
var glb_RecalculaResumo = false;
var glb_bGridResumo = false;
var glb_bLog = false;
var glb_nTransportadoraID = 0;
var glb_bFinalizaVenda = false;
var glb_bVoltarCheckout = false;
var glb_nItensAvaliar = 0;
var glb_nItensReprovados = 0;
var glb_bGravarCheckout = false;
var glb_bAprovacaoSacola = false;
var glb_bHistoricoMC = false;
var glb_nWidthDivFG = 0;
var glb_sMensagemAprovacao = '';
var glb_aMensagemAprovacao = new Array();
var glb_bFinalizarVenda = false;
var glb_nSacolaSelecionadaID = 0;
var glb_nSacolasSalvas = false;

var glb_TrolleyTimerInt = null;

// Variaveis de eventos de grid
var glb_PassedOneInDblClick = false;
var glb_LocalClickedIsReadOnly = false;
var glb_Master = null;
var glb_nFamiliaID = 0;
var glb_bSecondRefresh = false;
var glb_bConfirmPedido = false;
var glb_bMandaEmail = false;
var glb_divFGFullHeight = 0;
var glb_LastCmbChanged = null;
var glb_bGetMC = false;
var glb_deleteTimer = null;
var glb_nSacolaDSOs = 0;
var glb_bCombos2 = false;
var glb_bComprou = false;
var glb_nEmpresaData = getCurrEmpresaData();
var glb_sUF = '';
var glb_EnableEnc = false;
var glb_sPasteReadOnly = '';

// FINAL DE VARIAVEIS GLOBAIS ***************************************
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();
var glb_Quantidades = new Array();
var glb_nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));
var glb_nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));
var glb_nB10I = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'B10I' + '\'' + ')'));
var glb_bCotadorPesqList = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selFonteID.value') == 3 ? true : false);

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';

dsoEMail = new CDatatransport("dsoEMail");
dsoWeb = new CDatatransport("dsoWeb");
dsoCombos = new CDatatransport("dsoCombos");
dsoCombos2 = new CDatatransport("dsoCombos2");
dsoCombos3 = new CDatatransport("dsoCombos3");
dsoPurchaseItem = new CDatatransport("dsoPurchaseItem");
dsoSacola = new CDatatransport("dsoSacola");
dsoSacolasSalvas = new CDatatransport("dsoSacolasSalvas");
dsoSacolasSalvasItens = new CDatatransport("dsoSacolasSalvasItens");
dsoSelecionaSacola = new CDatatransport("dsoSelecionaSacola");
dsoGrid = new CDatatransport("dsoGrid");
dsoPrazo = new CDatatransport("dsoPrazo");
dsoGerarPedido = new CDatatransport("dsoGerarPedido");
dsoPesquisa = new CDatatransport("dsoPesquisa");
dsoFichaTecnica = new CDatatransport("dsoFichaTecnica");
dsoReferencias = new CDatatransport("dsoReferencias");
dsoUsuario = new CDatatransport("dsoUsuario");
dsoDelete = new CDatatransport("dsoDelete");
dsoEmpresas = new CDatatransport("dsoEmpresas");
dsoCFOPs = new CDatatransport("dsoCFOPs");
dsoBalanceCampaign = new CDatatransport('dsoBalanceCampaign');
dsoEmpresaSacola = new CDatatransport('dsoEmpresaSacola');
dsoCombosTransporte = new CDatatransport('dsoCombosTransporte');
dsoSacolaEntrega = new CDatatransport('dsoSacolaEntrega');
dsoSacolasSalvasCmb = new CDatatransport("dsoSacolasSalvasCmb");
dsoGridResumo = new CDatatransport("dsoGridResumo");
dsoHistoricoMC = new CDatatransport("dsoHistoricoMC");
dsoPropostaURL = new CDatatransport("dsoPropostaURL");
dsoAtualizaProdutos = new CDatatransport("dsoAtualizaProdutos");
dsoPrime = new CDatatransport("dsoPrime");
dsoCotadorPessoa = new CDatatransport("dsoCotadorPessoa");

/********************************************************************
INDICE DAS FUNCOES

window_onload()
setupPage()
chksOnClick()
btnsListarProdutosOnClick()
refreshLblConfiguradorMsg()
fillCmbsPesquisaData(cmbslave, cmbmaster)
selProdutoOuMarca_Changed(ctrl)
btnsFamiliaOnClick()
selEstadoProdutos_Changed()
selOutrosProdutos_Changed()
getDataGridProdutosConfigurador(nFamiliaID)
getDataGridProdutosConfigurador_DSC()
cmbsEvents()
btns_onclick(ctrl)
comutaFichaTecnicaMode()
comutaFichaTecnicaMode_DSC()
carrinhoEFichaTecnica(bShow)
comutaReferenciaMode()
comutaReferenciaMode_DSC()
comprar()
comprar_DSC()
comutaPesqMode()
disablePesquisaCtrls(bDisableCtrls)
cmb_onchange()
selVendedor_Changed()
selVendedor_Changed_DSC()
selParceiro_Changed()
selPessoa_Changed()
selPessoa_Changed_DSC()
selTransacaoID_Changed()
selFinanciamento_Changed()
selTransportadora_Changed();
selMeioTransporte_Changed();
selModalidade_Changed();
btn_onclick(ctl)
refreshParamsAndDataAndShowModalWin(fromServer)
fillDSOCmbs1()
fillDSOCmbs1_DSC()
fillDSOCmbs2()
fillDSOCmbs2_DSC()
getCurrEmpresaData()
lockUnlockAllCmbs(aDivRef)
setupBtnsCarrinhoFromGridState()
setupBtnsConfiguradorFromGridState()
purchaseItem()
purchaseItem_DSC()
txtPesquisa_ondigit(ctrl)
txtVariacao_ondigit(ctrl)
calcVariacao(ctrl)
writeDataInDso(nPrecoRevenda, nPrecoUnitario, nCarrinhoID)
fillGridData()
fillGridData_DSC()
calcMargemTotal()
calcTotItem(nLine, nLineException)
saveDataInGridCarrinho()
saveDataInGridCarrinho_DSC()
deleteDataInGridCarrinho()
emptyListTrolley()
gerarPedido()
gerarPedido_DSC()
drawButtonsFamilia()
seekInGrid(grid, sValue, sColKey)
img_onload()
img_onerror()
getFotoInServer_Finish(bError)
BalanceCampaign()

EVENTOS DE GRID DEFINIDOS NO ARQUIVO MODALTROLLEY.ASP

Grid fg

Definidos por SCRIPT

js_ModalTrolley_ValidateEdit()
fg_AfterEdit Row, Col 
js_ModalTrolleyKeyPress(KeyAscii);
js_fg_ModalTrolleyDblClick fg, fg.Row, fg.Col
fg_BeforeMouseDown Button, Shift, X, Y, Cancel
fg_EnterCell
fg_AfterRowColChange OldRow, OldCol, NewRow, NewCol
fg_BeforeRowColChange OldRow, OldCol, NewRow, NewCol, Cancel
fg_MouseMove Button, Shift, X, Y

Grid fg2
js_fg2_ModalTrolleyDblClick()
js_fg2_AfterRowColChange()

Translados dos Eventos de grid

Sub fg_BeforeRowColChange(OldRow, OldCol, NewRow, NewCol, Cancel)
    Cancel = js_fg_BeforeRowColChange (fg, OldRow, OldCol, NewRow, NewCol, Cancel)
End Sub

Sub fg_AfterRowColChange(OldRow, OldCol, NewRow, NewCol)
    js_fg_ModalTrolleyAfterRowColChange fg, OldRow, OldCol, NewRow, NewCol
End Sub

Sub fg_EnterCell()
    js_fg_EnterCell fg
End Sub

Sub fg_AfterEdit(Row, Col)
    js_ModalTrolley_AfterEdit Row, Col
End Sub

Sub fg_BeforeMouseDown(Button, Shift, X, Y, Cancel)
	'A definicao da variavel abaixo e particular desta janela,
	'pois a mesma tem dois grids e esta variavel impede
	'o usuario navegar para a linha de totalizacao deste grid
	glb_totalCols__ = true
    js_fg_ModalTrolleyBeforeMouseDown fg, Button, Shift, X, Y, Cancel
End Sub

Sub fg_MouseMove(Button, Shift, X, Y)
    js_fg_MouseMove fg, Button, Shift, X, Y
End Sub

FINAL DE EVENTOS DE GRID DEFINIDOS NO ARQUIVO MODALTROLLEY.ASP

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Carregamento da pagina
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    divPesquisa.style.visibility = 'hidden';
    divFG2.style.visibility = 'hidden';
    divFGResumo.style.visibility = 'hidden';
    fg2.style.visibility = 'hidden';
    fg2.className = 'fldGeneral';

    // ajusta o body do html
    with (modaltrolleyBody) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;
    modalFrame.style.left = 0;

    divSacolaSalvas.style.visibility = 'hidden';
    divCtlBar1.style.visibility = 'inherit';

    // Configuracao inicial do html
    setupPage();
    disablePesquisaCtrls(true);

    // mostra a modal
    refreshParamsAndDataAndShowModalWin(true);
}

function setupPage()
{
    secText('Sacola de Compras', 1);
    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 10;
    var eTop = 1;
    var eTopGap = 2;
    var rQuote = 0;
    var rQuoteGap = 3;
    var btnWidth = 78;
    var coll;
    var aEmpresaData = getCurrEmpresaData();

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    btnMC.style.visibility = 'hidden';
    btnAprovar.style.visibility = 'hidden';
    btnReprovar.style.visibility = 'hidden';

    with (divCtlBarGeral.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divMod01.style.height, 10);
        width = modWidth - 5;
        height = 193;
    }

    // largura e altura da janela modal
    with (divCtlBar1.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divMod01.style.height, 10);
        width = modWidth - 5;
        height = 193;
    }
    with (divSacolaSalvas.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divMod01.style.height, 10);
        width = modWidth - 5;
        height = 193;
    }

    // combo vendedor
    with (divCheckout.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divMod01.style.height, 10);
        width = modWidth - 5;
        height = 193;
        visibility = 'hidden';
    }

    var FIRSTLINEGAP = 10;
    divFG2.style.visibility = 'hidden';
    divFGResumo.visibility = 'hidden';
    divFGSacolasSalvas.style.visibility = 'hidden';

    with (divFGSacolasSalvas.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = 0;
        width = modWidth - 2 * ELEM_GAP - 6;
        top = 204;
        height = 213;
    }

    with (fgSacolasSalvas.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGSacolasSalvas.style.width, 10) - 1;
        height = parseInt(divFGSacolasSalvas.style.height, 10) - 1;
        visibility = 'hidden';
    }

    with (divFGResumo.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = 10;
        width = modWidth - 2 * ELEM_GAP - 6;
        top = 174;
        height = 350;
    }

    with (fgResumo.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFGResumo.style.width, 10) - 1;
        height = parseInt(divFGResumo.style.height, 10) - 1;
        visibility = 'hidden';
    }

    if (divSacolaSalvas.style.visibility != 'hidden')
    {
        with (btnSacolaAtual.style)
        {
            left = rQuote + FIRSTLINEGAP;
            top = eTop + parseInt(lbldtInicio.currentStyle.height, 10);
            width = btnWidth - 30;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnSacolaAtual.onclick = btns_onclick;

        with (selEstadoID.style)
        {
            left = rQuote + FIRSTLINEGAP - 5;
            top = eTop + parseInt(lbldtInicio.currentStyle.height, 10);
            width = 38;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblEstadoID.style)
        {
            left = parseInt(selEstadoID.currentStyle.left, 10);
            top = eTop;
            width = (lblEstadoID.innerText).length * FONT_WIDTH;
        }

        with (txtdtInicio.style)
        {
            left = rQuote + FIRSTLINEGAP - 5;
            top = eTop + parseInt(lbldtInicio.currentStyle.height, 10);
            width = 80;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lbldtInicio.style)
        {
            left = parseInt(txtdtInicio.currentStyle.left, 10);
            top = eTop;
            width = (lbldtInicio.innerText).length * FONT_WIDTH;
        }

        with (txtdtFim.style)
        {
            left = rQuote + FIRSTLINEGAP - 5;
            top = eTop + parseInt(lbldtFim.currentStyle.height, 10);
            width = 80;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lbldtFim.style)
        {
            left = parseInt(txtdtFim.currentStyle.left, 10);
            top = eTop;
            width = (lbldtFim.innerText).length * FONT_WIDTH;
        }
    }
    else
    {
        txtSacolaID.readOnly = true;
        with (txtSacolaID.style)
        {
            left = rQuote + FIRSTLINEGAP - 5;
            top = eTop + parseInt(lblSacolaID.currentStyle.height, 10);
            width = 80;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblSacolaID.style)
        {
            left = parseInt(txtSacolaID.currentStyle.left, 10);
            top = eTop;
            width = (lblSacolaID.innerText).length * FONT_WIDTH;
        }

        txtEst.readOnly = true;
        with (txtEst.style)
        {
            left = rQuote + FIRSTLINEGAP - 5;
            top = eTop + parseInt(lblEst.currentStyle.height, 10);
            width = 20;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblEst.style) {
            left = parseInt(txtEst.currentStyle.left, 10);
            top = eTop;
            width = (lblEst.innerText).length * FONT_WIDTH;
        }

        with (txtFonte.style) {
            //left = (parseInt(replaceStr(txtSacola.style.left, "px", "")) + parseInt(replaceStr(txtSacola.style.width, "px", "")) + (ELEM_GAP));
            left = rQuote + FIRSTLINEGAP - 5;
            top = eTop + parseInt(lblSacola.currentStyle.height, 10);
            width = 60;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblFonte.style) {
            left = parseInt(txtFonte.currentStyle.left, 10);
            top = eTop;
            width = (lblFonte.innerText).length * FONT_WIDTH;
        }

        with (txtSacola.style) {
            left = rQuote + FIRSTLINEGAP - 5;
            top = eTop + parseInt(lblSacola.currentStyle.height, 10);
            width = 160;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        txtSacola.onkeydown = txtSacola_onkeydown;

        with (lblSacola.style) {
            left = parseInt(txtSacola.currentStyle.left, 10);
            top = eTop;
            width = (lblSacola.innerText).length * FONT_WIDTH;
        }
    }

    with (selVendedor.style)
    {
        left = rQuote + FIRSTLINEGAP;
        top = eTop + parseInt(lblVendedor.currentStyle.height, 10);
        width = 130;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (lblVendedor.style)
    {
        left = parseInt(selVendedor.currentStyle.left, 10);
        top = eTop;
        width = (lblVendedor.innerText).length * FONT_WIDTH;
    }

    with (selPessoa.style)
    {
        left = rQuote + FIRSTLINEGAP;
        top = eTop + parseInt(lblPessoa.currentStyle.height, 10);

        if (glb_nSacolasSalvas)
            width = 127;
        else
            width = 156;

        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (lblPessoa.style)
    {
        left = parseInt(selPessoa.currentStyle.left, 10);
        top = eTop;
        width = (lblPessoa.innerText).length * FONT_WIDTH;
    }

    with (selParceiro.style)
    {
        left = rQuote + FIRSTLINEGAP;
        top = eTop + parseInt(lblParceiro.currentStyle.height, 10);

        if (glb_nSacolasSalvas)
            width = 127;
        else
            width = 156;

        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (lblParceiro.style)
    {
        left = parseInt(selParceiro.currentStyle.left, 10);
        top = eTop;
        width = (lblParceiro.innerText).length * FONT_WIDTH;
    }

    with (selUsuario.style)
    {
        left = rQuote + FIRSTLINEGAP;
        top = eTop + parseInt(lblUsuario.currentStyle.height, 10);
        width = 115;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (lblUsuario.style)
    {
        left = parseInt(selUsuario.currentStyle.left, 10);
        top = eTop;
        width = (lblUsuario.innerText).length * FONT_WIDTH;
    }

    if (divSacolaSalvas.style.visibility != 'hidden')
    {
        with (btnListar.style)
        {
            left = rQuote + FIRSTLINEGAP - 7;
            top = eTop + parseInt(lblUsuario.currentStyle.height, 10) - 2;
            width = btnWidth - 30;
            height = 24;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnListar.onclick = btns_onclick;

        with (btnGravarSacola.style)
        {
            left = rQuote + FIRSTLINEGAP - 7;
            top = eTop + parseInt(lblUsuario.currentStyle.height, 10) - 2;
            width = btnWidth - 20;
            height = 24;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnGravarSacola.onclick = btns_onclick;

        with (btnSelecionar.style) {
            left = rQuote + FIRSTLINEGAP - 7;
            top = eTop + parseInt(lblUsuario.currentStyle.height, 10) - 2;
            width = btnWidth - 20;
            height = 24;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }
        btnSelecionar.onclick = btns_onclick;
    }
    with (btnTelefonia.style)
    {
        left = rQuote + FIRSTLINEGAP;
        top = eTop + parseInt(lblUsuario.currentStyle.height, 10) - 2;
        width = 16;
        height = 24;
        visibility = 'hidden';
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnTelefonia.onclick = btns_onclick;

    with (btnCelular.style)
    {
        left = rQuote + 1;
        top = eTop + parseInt(lblUsuario.currentStyle.height, 10) - 2;
        width = 16;
        height = 24;
        visibility = 'hidden';
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnCelular.onclick = btns_onclick;

    with (btnProposta.style) {
        left = parseInt(selUsuario.currentStyle.left, 10) + 123;
        top = eTop + parseInt(lblUsuario.currentStyle.height, 10) - 2;
        width = 16;
        height = 24;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    btnProposta.onclick = btns_onclick;
    with (btnWEB.style) {
        left = parseInt(btnProposta.currentStyle.left, 10) + 23;
        top = eTop + parseInt(lblUsuario.currentStyle.height, 10) - 2;
        width = 16;
        height = 24;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    btnWEB.onclick = btns_onclick;
    btnWEB.setAttribute('pesqMode', 1, 1);

    eTop += 45;
    rQuote = 0;

    with (selTransacaoID.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblTransacao.currentStyle.height, 10);
        width = 220;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    //selTransacaoID.disabled = true;

    with (lblTransacao.style)
    {
        left = parseInt(selTransacaoID.currentStyle.left, 10);
        top = eTop;
        width = (lblTransacao.innerText).length * FONT_WIDTH;
    }

    with (selForma.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblForma.currentStyle.height, 10);
        width = FONT_WIDTH * 7;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (lblForma.style)
    {
        left = parseInt(selForma.currentStyle.left, 10);
        top = eTop;
        width = ((lblForma.innerText).length * FONT_WIDTH) - 10;
    }

    with (selFinanciamento.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblFinanciamento.currentStyle.height, 10);
        width = FONT_WIDTH * 17;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (lblFinanciamento.style)
    {
        left = parseInt(selFinanciamento.currentStyle.left, 10);
        top = eTop;
        width = ((lblFinanciamento.innerText).length * FONT_WIDTH) - 10;
    }

    with (selParcelas.style) {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblParcelas.currentStyle.height, 10);
        width = FONT_WIDTH * 5;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    with (lblParcelas.style) {
        left = parseInt(selParcelas.currentStyle.left, 10);
        top = eTop;
        width = ((lblParcelas.innerText).length * FONT_WIDTH) - 10;
    }
    with (txtPrazo1.style) {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblPrazo1.currentStyle.height, 10);
        width = FONT_WIDTH * 4;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    txtPrazo1.readOnly = true;
    with (lblPrazo1.style) {
        left = parseInt(txtPrazo1.currentStyle.left, 10);
        top = eTop;
        width = ((lblPrazo1.innerText).length * FONT_WIDTH) - 10;
    }
    with (txtIncremento.style) {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblIncremento.currentStyle.height, 10);
        width = FONT_WIDTH * 4;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    txtIncremento.readOnly = true;
    with (lblIncremento.style) {
        left = parseInt(txtIncremento.currentStyle.left, 10);
        top = eTop;
        width = ((lblIncremento.innerText).length * FONT_WIDTH) - 10;
    }
    with (txtPMP.style) {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblPMP.currentStyle.height, 10);
        width = FONT_WIDTH * 5;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    txtPMP.readOnly = true;
    with (lblPMP.style) {
        left = parseInt(txtPMP.currentStyle.left, 10);
        top = eTop;
        width = ((lblPMP.innerText).length * FONT_WIDTH) - 10;
    }
    with (txtAcrescimo.style) {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblAcrescimo.currentStyle.height, 10);
        width = FONT_WIDTH * 6;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    txtAcrescimo.readOnly = true;
    with (lblAcrescimo.style) {
        left = parseInt(txtAcrescimo.currentStyle.left, 10);
        top = eTop;
        width = ((lblAcrescimo.innerText).length * FONT_WIDTH) - 10;
    }
    with (txtMargemContribuicao.style) {
        left = rQuote + (ELEM_GAP);
        top = eTop + parseInt(lblMargemContribuicao.currentStyle.height, 10);
        width = FONT_WIDTH * 6;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (txtPrime.style) {
        top = eTop + parseInt(lblPrime.currentStyle.height, 10);
        width = 122;
        left = (parseInt(replaceStr(txtMargemContribuicao.style.left, "px", "")) + parseInt(replaceStr(txtMargemContribuicao.style.width, "px", "")) + (ELEM_GAP));
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    txtPrime.onkeydown = txtPrime_onkeydown;

    with (selContato.style) {
        top = eTop + parseInt(lblContato.currentStyle.height, 10);
        width = 100;
        left = (parseInt(replaceStr(txtPrime.style.left, "px", "")) + parseInt(replaceStr(txtPrime.style.width, "px", "")) + (ELEM_GAP));
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        height = 88;
    }

    txtMargemContribuicao.onkeydown = txtPrevisaoEntrega_onkeydown;
    txtMargemContribuicao.maxLength = 6;
    txtMargemContribuicao.readOnly = true;

    if (!((glb_nA1) && (glb_nA2)))
    {
        lblMargemContribuicao.style.visibility = 'hidden';
        txtMargemContribuicao.style.visibility = 'hidden';
    }

    with (lblMargemContribuicao.style)
    {
        left = parseInt(txtMargemContribuicao.currentStyle.left, 10);
        top = eTop;
        width = ((lblMargemContribuicao.innerText).length * FONT_WIDTH) - 10;
    }

    with (lblPrime.style) {
        left = parseInt(txtPrime.currentStyle.left, 10);
        top = eTop;
        width = ((lblPrime.innerText).length * FONT_WIDTH) - 10;
    }

    with (lblContato.style) {
        left = parseInt(selContato.currentStyle.left, 10);
        top = eTop;
        width = ((lblContato.innerText).length * FONT_WIDTH) - 10;
    }

    eTop += 45;
    rQuote = 0;
    with (txtPrevisaoEntrega.style) {
        left = rQuote + (ELEM_GAP);
        top = eTop + parseInt(lblPrevisaoEntrega.currentStyle.height, 10);
        width = FONT_WIDTH * 14;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    txtPrevisaoEntrega.onkeydown = txtPrevisaoEntrega_onkeydown;
    txtPrevisaoEntrega.maxLength = 19;
    txtPrevisaoEntrega.readOnly = true;
    with (lblPrevisaoEntrega.style) {
        left = parseInt(txtPrevisaoEntrega.currentStyle.left, 10);
        top = eTop;
        width = ((lblPrevisaoEntrega.innerText).length * FONT_WIDTH) - 10;
    }
    with (txtSeuPedido.style) {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblSeuPedido.currentStyle.height, 10);
        width = FONT_WIDTH * 14;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    txtSeuPedido.maxLength = 20;
    txtSeuPedido.onkeydown = txtSeuPedido_onkeydown;

    with (lblSeuPedido.style)
    {
        left = parseInt(txtSeuPedido.currentStyle.left, 10);
        top = eTop;
        width = ((lblSeuPedido.innerText).length * FONT_WIDTH) - 10;
    }

    with (txtObservacao.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblObservacao.currentStyle.height, 10);
        width = FONT_WIDTH * 16;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    txtObservacao.maxLength = 40;
    txtObservacao.onkeydown = txtObservacao_onkeydown;
    with (lblObservacao.style) {
        left = parseInt(txtObservacao.currentStyle.left, 10);
        top = eTop;
        width = ((lblObservacao.innerText).length * FONT_WIDTH) - 10;
    }
    with (txtComentarios.style) {
        left = rQuote + ELEM_GAP;
        top = eTop + parseInt(lblComentarios.currentStyle.height, 10);
        width = 484;
        height = 44;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    txtComentarios.onkeydown = txtComentarios_onkeydown;
    with (lblComentarios.style) {
        left = parseInt(txtComentarios.currentStyle.left, 10);
        top = eTop;
        width = ((lblComentarios.innerText).length * FONT_WIDTH) - 10;
    }
    with (divCtlBar2.style) 
    {
        border = 'none';
        backgroundColor = 'silver';
        left = 0;
        width = modWidth - 5;
        height = 27;
        top = modHeight - parseInt(height, 10) - 5;
    }
    eLeft = ELEM_GAP;
    eTop = 0;
    rQuote = 0;

    // desabilita e esconde o botao OK
    btnOK.disabled = true;
    btnOK.style.visibility = 'hidden';

    // centra e troca label do botao cancel
    divCtlBar2.appendChild(btnCanc);
    btnCanc.style.left = (modWidth - parseInt(btnCanc.currentStyle.width, 10)) / 2;
    btnCanc.value = 'Fechar';

    with (lblConfiguradorMsg.style) {
        left = eLeft + 5;
        top = eTop + eTopGap + 5;
        width = 40 * FONT_WIDTH;
        visibility = 'hidden';
        fontsize = '10pt';
        backgroundColor = 'transparent';
    }

    with (btnSacolasSalvas.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 25;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    btnSacolasSalvas.onclick = btns_onclick;

    with (btnFichaTecnica.style)
    {
        top = eTop + eTopGap;
        width = btnWidth;
        left = left = rQuote + 5;;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        visibility = 'inherit';
    }

    btnFichaTecnica.onclick = btns_onclick;
    btnFichaTecnica.setAttribute('FTMode', 0, 1);
    btnFichaTecnica.title = 'Se preferir, clique duas vezes nas colunas brancas do produto';

    with (btnLog.style)
    {
        top = eTop + eTopGap;
        width = btnWidth - 35;
        left = left = rQuote + 5;;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        visibility = 'inherit';
    }

    btnLog.onclick = btns_onclick;
    rQuote = rQuote + 12;

    with (btnRefresh.style) {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 15;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnRefresh.onclick = btns_onclick;

    with (txtVariacao.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 5;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        backgroundColor = 'white';
    }

    txtVariacao.onkeypress = verifyNumericEnterNotLinked;
    txtVariacao.setAttribute('verifyNumPaste', 1);
    txtVariacao.onfocus = selFieldContent;
    txtVariacao.setAttribute('thePrecision', 11, 1);
    txtVariacao.setAttribute('theScale', 2, 1);
    txtVariacao.setAttribute('minMax', new Array(-40, 999999999), 1);
    txtVariacao.title = 'Digite:\n� valor inferior a 100 para aplicar esta varia��o percentual aos itens da Sacola; ou\n� valor superior a 100 para aplicar este valor a Sacola.';

    with (btnGravar.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 15;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnGravar.onclick = btns_onclick;
    rQuote = rQuote + 12;

    if (btnAprovar.style.visibility == 'inherit')
    {
        with (btnMC.style)
        {
            left = rQuote + 5;
            top = eTop + eTopGap;
            width = btnWidth - 20;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnMC.onclick = btns_onclick;

        with (btnAprovar.style)
        {
            left = rQuote + 5;
            top = eTop + eTopGap;
            width = btnWidth - 20;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnAprovar.onclick = btns_onclick;

        with (btnReprovar.style)
        {
            left = rQuote + 5;
            top = eTop + eTopGap;
            width = btnWidth - 25;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnReprovar.onclick = btns_onclick;
    }

    with (selSacolasSalvas.style) {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = ((FONT_WIDTH * 15) + (btnWidth - 13));
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (btnExcluir.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 20;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnExcluir.onclick = btns_onclick;

    with (btnEsvaziar.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 25;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnEsvaziar.onclick = btns_onclick;
    rQuote = rQuote + 12;

    with (btnEstado.style)
    {
        top = eTop + eTopGap;
        width = btnWidth - 30;
        left = rQuote + 5;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnEstado.onclick = btns_onclick;

    with (btnSalvarSacola.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 25;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnSalvarSacola.onclick = btns_onclick;

    with (btnNotificar.style)
    {
        top = eTop + eTopGap;
        width = btnWidth - 20;
        left = rQuote + 5;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnNotificar.onclick = btns_onclick;
    btnNotificar.setAttribute('pesqMode', 1, 1);

    with (btnCheckout.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 18;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    btnCheckout.onclick = btns_onclick;

    with (btnReferencias.style)
    {
        top = eTop + eTopGap;
        width = btnWidth;
        left = parseInt(divCtlBar2.currentStyle.width, 10) - (parseInt(width, 10) * 5) - eLeft - (5 * 4);
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        visibility = 'hidden';
    }
    btnReferencias.onclick = btns_onclick;
    btnReferencias.setAttribute('RefMode', 0, 1);
    with (btnComprar.style) {
        top = eTop + eTopGap;
        width = btnWidth;
        left = parseInt(divCtlBar2.currentStyle.width, 10) - (parseInt(width, 10) * 3) - eLeft - (5 * 2);
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        visibility = 'hidden';
    }
    btnComprar.onclick = btns_onclick;
    with (btnPesquisa.style) {
        top = eTop + eTopGap;
        width = btnWidth;
        left = parseInt(divCtlBar2.currentStyle.width, 10) - (parseInt(width, 10) * 3) - eLeft - (5 * 2);
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        visibility = 'hidden';
    }
    btnPesquisa.onclick = btns_onclick;
    if (glb_nEmpresaID != 3) btnPesquisa.disabled = true;
    btnPesquisa.setAttribute('pesqMode', 0, 1);
    with (btnCanc.style) {
        top = eTop + eTopGap;
        width = btnWidth;
        left = parseInt(divCtlBar2.currentStyle.width, 10) - parseInt(width, 10) - eLeft;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        visibility = 'hidden';
    }

    // linha horizontal de arremate superior topLine
    // do divCtlBar2
    with (topLine.style)
    {
        color = 'gray';
        left = 0;
        top = 0;
        height = 1;
        width = parseInt(divCtlBar2.currentStyle.width, 10);
    }

    with (divFG2.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = 0;
        top = parseInt(divCtlBar1.style.top, 10) + parseInt(divCtlBar1.style.height, 10) - (ELEM_GAP * 6) + 1;
        width = modWidth - 2 * ELEM_GAP - 6;
        height = parseInt(divCtlBar2.currentStyle.top, 10) - parseInt(top, 10) - (ELEM_GAP * 4) + 5;
    }

    with (fg2.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10) - 1;
        height = parseInt(divFG2.style.height, 10) - 1;
        visibility = 'hidden';
    }

    if (divSacolaSalvas.style.visibility == 'hidden')
    {
        with (divFG.style)
        {
            border = 'solid';
            borderWidth = 1;
            backgroundColor = 'transparent';
            left = ELEM_GAP;
            top = parseInt(divCtlBar1.style.top, 10) + parseInt(divCtlBar1.style.height, 10) - (ELEM_GAP * 3.5);
            width = modWidth - 2 * ELEM_GAP - 6;
            height = parseInt(divCtlBar2.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
        }
    }
    else
    {
        with (divFG.style)
        {
            border = 'solid';
            borderWidth = 1;
            backgroundColor = 'transparent';
            left = ELEM_GAP;
            top = parseInt(divCtlBar1.style.top, 10) + (ELEM_GAP * 5);
            width = modWidth - 2 * ELEM_GAP - 6;
            height = parseInt(divCtlBar2.currentStyle.top, 10) - parseInt(top, 10) - (20 * ELEM_GAP);
        }
        with (divFGSacolasSalvas.style)
        {
            border = 'solid';
            borderWidth = 1;
            backgroundColor = 'blue';
            left = ELEM_GAP;
            width = modWidth - 2 * ELEM_GAP - 6;
            top = parseInt(divCtlBar1.style.top, 10) + (ELEM_GAP * 5) + parseInt(divFG.style.height, 10) + 30;
            height = parseInt(divCtlBar2.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP;
            visibility = 'inherit';
        }
        with (fgSacolasSalvas.style) {
            left = 0;
            top = 0;
            width = parseInt(divFGSacolasSalvas.style.width, 10) - 1;
            height = parseInt(divFGSacolasSalvas.style.height, 10) - 1;
            visibility = 'inherit';
        }
    }

    glb_divFGFullHeight = parseInt(divFG.currentStyle.height, 10);

    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) - 1;
        height = parseInt(divFG.style.height, 10) - 1;
    }

    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divCtlBar1.style.top, 10);
        width = modWidth - 2 * ELEM_GAP - 6;
        height = (parseInt(divCtlBar2.currentStyle.top, 10) - parseInt(top, 10) - ELEM_GAP) / 3;
    }
    with (img_Imagem.style) {
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        visibility = 'hidden';
    }

    eTop = 5;

    with (lblEstadoProdutos.style) {
        top = eTop;
        left = 4;
    }

    with (selEstadoProdutos.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblEstadoProdutos.currentStyle.height, 10);
        left = 4;
        width = FONT_WIDTH * 12;
        rQuote = parseInt(selEstadoProdutos.currentStyle.left, 10) + parseInt(selEstadoProdutos.currentStyle.width, 10);
    }
    with (lblProduto.style) {
        top = eTop;
        left = rQuote + rQuoteGap;
    }

    with (selProduto.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblProduto.currentStyle.height, 10);
        left = rQuote + rQuoteGap;
        width = FONT_WIDTH * 19;
        rQuote = parseInt(selProduto.currentStyle.left, 10) + parseInt(selProduto.currentStyle.width, 10);
    }
    with (lblMarca.style) {
        top = eTop;
        left = rQuote + rQuoteGap;
    }
    with (selMarca.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblMarca.currentStyle.height, 10);
        left = rQuote + rQuoteGap;
        width = FONT_WIDTH * 15;
        rQuote = parseInt(selMarca.currentStyle.left, 10) + parseInt(selMarca.currentStyle.width, 10);
    }
    with (lblMetodoPesquisa.style) {
        top = eTop;
        left = rQuote + rQuoteGap;
    }
    with (selMetodoPesquisa.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblMetodoPesquisa.currentStyle.height, 10);
        left = rQuote + rQuoteGap;
        width = FONT_WIDTH * 10;
        rQuote = parseInt(selMetodoPesquisa.currentStyle.left, 10) + parseInt(selMetodoPesquisa.currentStyle.width, 10);
    }
    with (lblChavePesquisa.style) {
        top = eTop;
        left = rQuote + rQuoteGap;
    }
    with (selChavePesquisa.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblMetodoPesquisa.currentStyle.height, 10);
        left = rQuote + rQuoteGap;
        width = FONT_WIDTH * 7;
        rQuote = parseInt(selChavePesquisa.currentStyle.left, 10) + parseInt(selChavePesquisa.currentStyle.width, 10);
    }
    with (lblPalavraChave.style) {
        top = eTop;
        left = rQuote + rQuoteGap;
    }
    with (txtPalavraChave.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblPalavraChave.currentStyle.height, 10);
        left = rQuote + rQuoteGap;
        width = FONT_WIDTH * 9;
        rQuote = parseInt(txtPalavraChave.currentStyle.left, 10) + parseInt(txtPalavraChave.currentStyle.width, 10) + (ELEM_GAP / 2);
    }
    with (lblListaEstoque.style) {
        top = eTop;
        left = rQuote;
    }
    with (chkListaEstoque.style) {
        top = eTop + 12;
        left = rQuote - 3;
        width = FONT_WIDTH * 2.8;
        rQuote = parseInt(chkListaEstoque.currentStyle.left, 10) + (FONT_WIDTH * 2.8) + ELEM_GAP;
    }
    chkListaEstoque.onclick = chksOnClick;
    chkListaEstoque.checked = true;
    with (lblPreco.style) {
        top = eTop;
        left = rQuote;
    }
    with (chkPreco.style) {
        top = eTop + 12;
        left = rQuote - 3;
        width = FONT_WIDTH * 2.8;
        rQuote = parseInt(chkPreco.currentStyle.left, 10) + (FONT_WIDTH * 2.8) + ELEM_GAP;
    }
    chkPreco.onclick = chksOnClick;
    chkPreco.checked = true;
    with (lblEstoque.style) {
        top = eTop;
        left = rQuote;
    }
    with (chkEstoque.style) {
        top = eTop + 12;
        left = rQuote - 3;
        width = FONT_WIDTH * 2.8;
        rQuote = parseInt(chkEstoque.currentStyle.left, 10) + (FONT_WIDTH * 2.8) + ELEM_GAP;
    }
    chkEstoque.onclick = chksOnClick;
    with (lblProdutosCompativeis.style) {
        top = eTop;
        left = rQuote - 2;
    }
    with (chkProdutosCompativeis.style) {
        top = eTop + 12;
        left = rQuote - 7;
        width = FONT_WIDTH * 2.8;
        rQuote = parseInt(chkProdutosCompativeis.currentStyle.left, 10) + 24;
    }
    chkProdutosCompativeis.onclick = chksOnClick;
    chkProdutosCompativeis.checked = glb_bTemProducao;
    with (btnListarProdutos) {
        style.left = rQuote;
        style.width = (FONT_WIDTH * 5) + 2;
        style.top = eTop + 10;
        visilibity = 'visible';
        rQuote = rQuote + (FONT_WIDTH * 5);
        disabled = false;
        onclick = btnsListarProdutosOnClick;
    }
    eTop += 42;
    rQuote = 2;
    coll = window.document.getElementsByTagName('INPUT');
    for (i = 0; i < coll.length; i++) {
        if (((coll.item(i)).type).toUpperCase() == 'BUTTON') {
            if (((coll.item(i)).id).toUpperCase() == 'BTNLISTARPRODUTOS') continue;
            if (coll.item(i).parentElement != null) {
                if ((coll.item(i).parentElement.id).toUpperCase() == 'DIVPESQUISA') {
                    elem = coll.item(i);
                    with (elem) {
                        className = 'btns';
                        style.left = rQuote;
                        style.width = (FONT_WIDTH * 3) + 3;
                        style.top = eTop + 5;
                        visilibity = 'visible';
                        rQuote = rQuote + ((FONT_WIDTH * 3) + 3) + 1;
                        disabled = false;
                        onclick = btnsFamiliaOnClick;
                    }
                }
            }
        }
    }

    with (selOutrosProdutos.style) {
        visibility = 'hidden';
        top = eTop + 5;
        left = rQuote + (rQuote == 2 ? 0 : 6);
        width = FONT_WIDTH * 22;
        rQuote = parseInt(chkEstoque.currentStyle.left, 10) + parseInt(chkEstoque.currentStyle.width, 10);
    }
    eTop += 33;
    rQuote = 0;
    with (lblConceitoConcreto.style) {
        top = eTop;
        left = rQuote;
    }
    with (txtConceitoConcreto.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblConceitoConcreto.currentStyle.height, 10);
        left = rQuote;
        width = FONT_WIDTH * 40;
        rQuote = parseInt(txtConceitoConcreto.currentStyle.left, 10) + parseInt(txtConceitoConcreto.currentStyle.width, 10) + (ELEM_GAP / 2);
    }
    txtConceitoConcreto.readOnly = true;
    with (lblMarca1.style) {
        top = eTop;
        left = rQuote;
    }
    with (txtMarca.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblMarca1.currentStyle.height, 10);
        left = rQuote;
        width = FONT_WIDTH * 40;
        rQuote = parseInt(txtMarca.currentStyle.left, 10) + parseInt(txtMarca.currentStyle.width, 10) + (ELEM_GAP / 2);
    }
    txtMarca.readOnly = true;
    with (lblModelo.style) {
        top = eTop;
        left = rQuote;
    }
    with (txtModelo.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblModelo.currentStyle.height, 10);
        left = rQuote;
        width = FONT_WIDTH * 18;
        rQuote = parseInt(txtModelo.currentStyle.left, 10) + parseInt(txtModelo.currentStyle.width, 10) + (ELEM_GAP / 2);
    }
    txtModelo.readOnly = true;
    with (lblDescricao.style) {
        top = eTop;
        left = rQuote;
    }
    with (txtDescricao.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblModelo.currentStyle.height, 10);
        left = rQuote;
        width = (FONT_WIDTH * 33) + 3;
        rQuote = parseInt(txtModelo.currentStyle.left, 10) + parseInt(txtModelo.currentStyle.width, 10) + (ELEM_GAP / 2);
    }
    txtDescricao.readOnly = true;
    with (lblArgumentosVenda.style) {
        eTop += 45;
        top = eTop;
        left = 3;
    }
    with (txtArgumentosVenda.style) {
        visibility = 'hidden';
        top = eTop + parseInt(lblArgumentosVenda.currentStyle.height, 10);
        left = 3;
        width = 741;
        height = 56;
    }

    txtArgumentosVenda.readOnly = true;
    txtFonte.readOnly = true;

    // Cores
    lblVendedor.style.color = 'green';
    lblPessoa.style.color = 'blue';
    lblParceiro.style.color = 'green';
    lblUsuario.style.color = 'green';
    // lblTransacao.style.color = 'green';
    lblForma.style.color = 'blue';
    lblFinanciamento.style.color = 'blue';
    lblObservacao.style.color = 'green';
    lblSeuPedido.style.color = 'green';
    lblSacola.style.color = 'green';
    lblPrevisaoEntrega.style.color = 'blue';
    lblComentarios.style.color = 'green';
    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;
    startGridInterface(fgSacolasSalvas);
    fgSacolasSalvas.Cols = 0;
    fgSacolasSalvas.Cols = 1;
    fgSacolasSalvas.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fgSacolasSalvas.Redraw = 2;
    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;
    fg2.Redraw = 2;
    eTop = parseInt(divFG2.style.top, 10) + parseInt(divFG2.style.height, 10) + ELEM_GAP;
    rQuote = 0;
    cmbsEvents();
}

/********************************************************************
Usuario clicou qualquer check box da interface
********************************************************************/
function chksOnClick() {
    fg2.Rows = 1;
    js_fg2_AfterRowColChange();
    setupBtnsConfiguradorFromGridState();
    drawButtonsFamilia();
}

/********************************************************************
Usuario clicou botao btnListarProdutos
********************************************************************/
function btnsListarProdutosOnClick() {
    glb_nFamiliaID = 0;
    selOutrosProdutos.selectedIndex = 0;
    getDataGridProdutosConfigurador();
}

/********************************************************************
Atualiza texto do campo lblConfiguradorMsg
********************************************************************/
function refreshLblConfiguradorMsg()
{
    var sMsg = '';
    var qtdItens = (fg.Rows - 2);

    qtdItens = (qtdItens < 0 ? 0 : qtdItens);

    if (fg.Rows > 1)
    {
        sMsg = 'Sacola de Compras: ' + qtdItens + ' itens   ' + 'Total: ' + glb_sMoeda + ' ' + getCellValueByColKey(fg, '_calc_PrecoTotal_11*', 1);

        if (getCellValueByColKey(fg, 'MargemContribuicao*', 1) != '')
        {
            if (glb_bGetMC)
                sMsg += ' (' + getCellValueByColKey(fg, 'MargemContribuicao*', 1) + '%)';
        }
    }

    lblConfiguradorMsg.innerText = sMsg;
}

/********************************************************
Funcao que preenche os combos

Parametros: cmbslave - referencia ao combo que deve ser carregado
            cmbmaster - referencia ao combo master
********************************************************/
function fillCmbsPesquisaData(cmbslave, cmbmaster) {
    var oOption;
    var nIndice;
    var sName = '';
    var sID = '';
    var sMasterID = '';
    var nMasterID = 0;
    var i;
    var slaveValue = cmbslave.value;
    var tempVal;
    clearComboEx([cmbslave.id]);
    if (cmbslave.id.toUpperCase() == 'SELESTADOPRODUTOS') nIndice = 0;
    else if (((cmbslave.id.toUpperCase() == 'SELPRODUTO') && (cmbmaster == null)) || ((cmbslave.id.toUpperCase() == 'SELMARCA') && (cmbmaster != null))) nIndice = 1;
    else nIndice = 2;
    if ((cmbslave.id.toUpperCase() == 'SELESTADOPRODUTOS') || (cmbslave.id.toUpperCase() == 'SELPRODUTO')) {
        sName = 2;
        sMasterID = 3;
        nMasterID = selMarca.value;
        sID = 1;
    } else {
        sName = 4;
        sMasterID = 1;
        nMasterID = selProduto.value;
        sID = 3;
    }
    for (i = 0; i < glb_aCmbs.length; i++) {
        if ((glb_aCmbs[i][0] == nIndice) && (lookUpValueInCmb(cmbslave, glb_aCmbs[i][sID]) == -1) && ((cmbmaster == null) || (glb_aCmbs[i][sMasterID] == nMasterID) || (glb_aCmbs[i][sID] == 0))) {
            oOption = document.createElement("OPTION");
            oOption.text = glb_aCmbs[i][sName];
            oOption.value = glb_aCmbs[i][sID];
            if (cmbslave.id.toUpperCase() == 'SELESTADOPRODUTOS') {
                oOption.setAttribute('Filtro', glb_aCmbs[i][6], 1);
            }
            cmbslave.add(oOption);
        }
    }
    if (cmbslave.id.toUpperCase() != 'SELESTADOPRODUTOS') {
        tempVal = lookUpValueInCmb(cmbslave, slaveValue);
        if (tempVal != -1) cmbslave.selectedIndex = tempVal;
    }
    cmbslave.disabled = (cmbslave.options.length == 0);
}

function selProdutoOuMarca_Changed(ctrl) {
    var oCmb = ctrl.id;
    if (!((oCmb.toUpperCase() == 'SELPRODUTO') || (oCmb.toUpperCase() == 'SELMARCA'))) return true;
    if (oCmb.toUpperCase() == 'SELPRODUTO') {
        if (ctrl.value == 0) {
            if (glb_Master == oCmb.toUpperCase()) fillCmbsPesquisaData(selMarca, null);
        } else {
            if (glb_Master == null || glb_Master == oCmb.toUpperCase()) fillCmbsPesquisaData(selMarca, oCmb);
        }
    } else if (oCmb.toUpperCase() == 'SELMARCA') {
        if (ctrl.value == 0) {
            if (glb_Master == oCmb.toUpperCase()) fillCmbsPesquisaData(selProduto, null);
        } else {
            if (glb_Master == null || glb_Master == oCmb.toUpperCase()) fillCmbsPesquisaData(selProduto, oCmb);
        }
    }
    if (glb_Master == null) glb_Master = oCmb.toUpperCase();
    if (ctrl.value == 0) {
        if (glb_Master == oCmb.toUpperCase()) glb_Master = null;
    }
}

/********************************************************************
Usuario trocou o option do select de Produto ou do de Marca
********************************************************************/
function btnsFamiliaOnClick() {
    var i;
    js_fg2_AfterRowColChange();
    if (selPessoa.value == '') return null;
    var nFamiliaID = this.id;
    if (chkProdutosCompativeis.checked) {
        for (i = 2; i < fg.Rows; i++)
        {
            if (getCellValueByColKey(fg, 'FamiliaID' + glb_sPasteReadOnly, i) == nFamiliaID)
            {
                if (window.top.overflyGen.Alert('Esta fam�lia ja foi adicionada no Sacola.') == 0)
                    return null;

                break;
            }
        }
    }
    selProduto.selectedIndex = 0;
    selOutrosProdutos.selectedIndex = 0;
    glb_nFamiliaID = nFamiliaID;
    getDataGridProdutosConfigurador(nFamiliaID);
}

/********************************************************************
Usuario trocou o option do select de Estado dos Produtos
********************************************************************/
function selEstadoProdutos_Changed() {
    return null;
}

/********************************************************************
Usuario trocou o option do select de Outros Produtos
********************************************************************/
function selOutrosProdutos_Changed() {
    if (selOutrosProdutos.value == 0) return null;
    if (selPessoa.value == '') return null;
    glb_nFamiliaID = selOutrosProdutos.value;
    getDataGridProdutosConfigurador(selOutrosProdutos.value);
}

/********************************************************************
Obtem dados no servidor para preencher o grid fg2 com produtos,
quando a interface esta em modo configurador

Parametros
	nFamiliaID - id da familia
********************************************************************/
function getDataGridProdutosConfigurador(nFamiliaID) {
    lockControlsInModalWin(true);
    var nPessoaID, nParceiroID, nFinanciamentoID, nFrete;
    var nListaPreco, bPreco, bListaEstoque, bEstoque, bProdutosCompativeis, nTop, nMarcaID;
    var sFiltroEstados;
    var sOrderBy, sPesquisa, sPalavraChave, bLike, nLoteID;
    nPessoaID = selPessoa.value;
    nParceiroID = selParceiro.value;
    nFinanciamentoID = selFinanciamento.value;
    nFrete = 0;
    if (nFrete == 0) {
        nMeioTransporteID = 'NULL';
        nTransportadoraID = 'NULL';
    }
    nListaPreco = selParceiro.options.item(selParceiro.selectedIndex).getAttribute('ListaPreco', 1);
    bPreco = (chkPreco.checked == true ? 1 : 0);
    bListaEstoque = (chkListaEstoque.checked == true ? 1 : 0);
    bEstoque = (chkEstoque.checked == true ? 1 : 0);
    bProdutosCompativeis = (chkProdutosCompativeis.checked == true ? 1 : 0);
    nTop = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selRegistros.value');
    nMarcaID = selMarca.value;
    sFiltroEstados = '';
    sFiltroEstados = selEstadoProdutos.options.item(selEstadoProdutos.selectedIndex).getAttribute('Filtro', 1);
    sOrderBy = '';
    sPesquisa = 'NULL';
    sPalavraChave = '';
    bLike = 0;
    nLoteID = 0;
    if (sFiltroEstados != '') sFiltroEstados = '\'' + ' AND ' + sFiltroEstados + '\'';
    else sFiltroEstados = 'NULL';
    if (nFamiliaID == null) {
        nFamiliaID = selProduto.value;
    }
    if (nFamiliaID == 0) nFamiliaID = 'NULL';
    if (nMarcaID == 0) nMarcaID = 'NULL';
    sPalavraChave = txtPalavraChave.value;
    if (selMetodoPesquisa.value == 'MAIORIGUAL') {
        bLike = 0;
        if (selChavePesquisa.value == 'ID') {
            if (trimStr(sPalavraChave) == '') sPalavraChave = '0';
            sPesquisa = ' AND a.ObjetoID >= ' + sPalavraChave + ' ';
        } else sPesquisa = ' AND b.Conceito >= ' + '\'' + sPalavraChave + '\'' + ' ';
        sPesquisa = '\'' + sPesquisa + '\'';
    } else {
        bLike = 1;
        if (selMetodoPesquisa.value == 'COMECACOM') sPalavraChave = '\'' + sPalavraChave + '%' + '\'';
        else if (selMetodoPesquisa.value == 'TERMINACOM') sPalavraChave = '\'' + '%' + sPalavraChave + '\'';
        else if (selMetodoPesquisa.value == 'CONTEM') sPalavraChave = '\'' + '%' + sPalavraChave + '%' + '\'';
    }
    if (selChavePesquisa.value == 'ID') sOrderBy = '\'' + 'ConceitoID' + '\'';
    else sOrderBy = '\'' + 'Conceito' + '\'';
    setConnection(dsoPesquisa);
    dsoPesquisa.SQL = 'EXEC sp_ListaPrecos_PesquisaProduto ' + glb_nEmpresaID + ', ' + nPessoaID + ', ' + nParceiroID + ', ' + nFinanciamentoID + ', ' + nFrete + ', ' + nMeioTransporteID + ', ' + nTransportadoraID + ', ' + nListaPreco + ', ' + bPreco + ', ' + bListaEstoque + ', ' + bEstoque + ', ' + bProdutosCompativeis + ', ' + nTop + ', ' + nMarcaID + ', ' + nFamiliaID + ', ' + sFiltroEstados + ', ' + bLike + ', ' + sPesquisa + ', ' + sPalavraChave + ', ' + sOrderBy + ', ' + glb_nUserID + ', ' + nLoteID;
    dsoPesquisa.ondatasetcomplete = getDataGridProdutosConfigurador_DSC;
    dsoPesquisa.refresh();
}

/********************************************************************
Retorno do servidor com dados para preencher o grid fg2 com produtos,
quando a interface esta em modo configurador
********************************************************************/
function getDataGridProdutosConfigurador_DSC() {
    var dTFormat = '';
    var bFTMode = false;
    var bRefMode = false;
    var i;
    if (DATE_FORMAT == "DD/MM/YYYY") dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY") dTFormat = 'mm/dd/yyyy';
    var nEmpresaAlternativaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'empresaAlternativa()');
    fg2.Redraw = 0;
    img_onerror();
    glb_GridIsBuilding = true;
    fg2.Editable = false;
    startGridInterface(fg2);
    fg2.FrozenCols = 0;
    headerGrid(fg2, ['ID', 'Est', 'Produto', 'Empresa', 'Qtd', 'Pre�o', 'Imp', 'MC', 'Disp', 'Disp 1', 'Disp 2', 'Equipe', 'Disponibilidade', 'Observa��o', 'ProdutoIncompativel', 'Familia', 'Marca', 'Modelo', 'Descricao', 'Foto', 'ArgumentosVenda', 'EmpresaID'], [14, 15, 16, 17, 18, 19, 20, 21]);
    fillGridMask(fg2, dsoPesquisa, ['ConceitoID*', 'RecursoAbreviado*', 'Conceito*', 'Empresa*', '_calc_Qtd_10', 'PrecoConvertido', 'Aliquota*', 'MC*', 'Disponivel*', 'Disponivel1*', 'Disponivel2*', 'EstoqueEquipe*', 'dtPrevisaoDisponibilidade*', 'Observacao*', 'ProdutoIncompativel', 'Familia', 'Marca', 'Modelo', 'Descricao', 'Foto', 'ArgumentosVenda', 'EmpresaID'], ['9999999999', '', '', '', '99999', '999999999.99', '999.99', '999.99', '999999', '999999', '999999', '999999', '', '', '', '', '', '', '', '', ''], ['##########', '', '', '', '#####', '###,###,##0.00', '##0.00', '##0.00', '######', '######', '######', '######', '', '', '', '', '', '', '', '', '']);

    fg2.Redraw = 0;

    // Merge de Colunas
    fg2.MergeCells = 4;
    fg2.MergeCol(-1) = true;
    fg2.FillStyle = 1;
    for (i = 1; i < fg2.Rows; i++) {
        if (getCellValueByColKey(fg2, 'ProdutoIncompativel', i) != '') fg2.Cell(6, i, 0, i, fg2.Cols - 1) = 0X7280FA;
        fg2.TextMatrix(i, getColIndexByColKey(fg2, '_calc_Qtd_10')) = 0;
    }
    fg2.FillStyle = 0;
    alignColsInGrid(fg2, [0, 4, 5, 6, 7, 8, 9, 10, 11]);
    paintReadOnlyCols(fg2);
    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);
    fg2.FrozenCols = 3;
    fg2.Redraw = 2;
    lockControlsInModalWin(false);
    glb_GridIsBuilding = false;

    // se tem linhas no grid, coloca foco no grid
    if (fg2.Rows > 1) {
        fg2.Row = 1;
        fg2.focus();
    }
    js_fg2_AfterRowColChange();
    bFTMode = (btnFichaTecnica.getAttribute('FTMode', 1) == 1);
    if (bFTMode) {
        btnFichaTecnica.setAttribute('FTMode', 0, 1);
        btnFichaTecnica.value = 'Ficha T�cnica';
        btnFichaTecnica.title = 'Se preferir, clique duas vezes na coluna Produto do produto';
    }
    bRefMode = (btnReferencias.getAttribute('RefMode', 1) == 1);
    if (bRefMode) {
        btnReferencias.setAttribute('RefMode', 0, 1);
        btnReferencias.value = 'Refer�ncias';
        btnReferencias.title = 'Se preferir, clique duas vezes na coluna ID do produto';
    }
    setupBtnsConfiguradorFromGridState();
    fg2.Editable = true;
}

/********************************************************************
Setup eventos dos combos
********************************************************************/
function cmbsEvents() {
    var coll = window.document.getElementsByTagName('SELECT');
    var i;
    for (i = 0; i < coll.length; i++) {
        coll.item(i).onchange = cmb_onchange;
    }
}

/********************************************************************
Usuario clicou botao que nao e o botao OK ou o botao Cancel.
Tambem nao e um dos botoes de familias de produtos, criados
dinamicamente.
********************************************************************/
function btns_onclick(ctrl)
{
    if (ctrl == null)
        ctrl = this;

    if (ctrl.disabled)
        return;

    var _retMsg;
    var sMsg = '';
    var nEstadoID;

    glb_nSacolasSalvas = false;

    if (ctrl == btnSacolaAtual)
    {
        glb_atertSacolaAtual = true;
        clearComboEx(['selVendedor']);
        alteraInterface(2);
    }

    if (ctrl == btnSacolasSalvas)
    {
        glb_nSacolasSalvas = true;
        alteraInterface(1);
    }

    if (ctrl == btnExcluir)
        sMsg = 'Excluir �tem?';
    else if (ctrl == btnEsvaziar)
        sMsg = 'Esvaziar sacola?';
    else if (ctrl == btnFinalizarVenda)
    {
        if (txtEst.value == 'C')
        {
            sMsg = 'Avan�ar sacola?';
        }            
        else
        {
            if (dsoSacola.recordset['TemPessoa'].value == '0') {
                window.top.overflyGen.Alert("N�o � poss�vel emitir pedido sem pessoa.");
                return null;
            }

            for (var contador = 1; contador < fg.Rows; contador++) {
                if (fg.TextMatrix(contador, getColIndexByColKey(fg, 'GravidadeInconsistencia*')) >= 1) {
                    window.top.overflyGen.Alert("Verifique as inconsistencias antes de emitir o pedido");
                    return null;
                    break;
                }
            }

            if (glb_bCotador) {
                if (dsoSacola.recordset['Prime'].value == "" || dsoSacola.recordset['Prime'].value == null) {
                    _retMsg = window.top.overflyGen.Confirm("Esta proposta n�o tem DealID.\r\nDeseja gerar o pedido assim mesmo?");
                    if ((_retMsg == 0) || (_retMsg == 2))
                        return null;
                }
            }

            if (dsoSacola.recordset['ClienteNotificado'].value == 0)
            {
               if (glb_bCotador) {
                   window.top.overflyGen.Alert("� necess�rio notificar cliente antes de finalizar venda.");
                   return null;
               }
               else {
                   sMsg = 'Notificar cliente?';

                   _retMsg = window.top.overflyGen.Confirm(sMsg);

                   /*
                   if ((_retMsg == 0) || (_retMsg == 2))
                       return null;
                   else
                   */
               
                   if (_retMsg == 1)
                   {
                       glb_bFinalizaVenda = true;
                       notificarCliente();
                       return null;
                   }
               }
           }

            sMsg = 'Finalizar venda?';
        }            
    }        
    else if (ctrl == btnEstado)
    {
        if (txtEst.value == 'C')
        {
            sMsg = 'Alterar o estado para P?';
            nEstadoID = 22;
        }
        else if (txtEst.value == 'A')
        {
            if ((glb_nItensAvaliar > 0) || (glb_nItensReprovados > 0))
            {
                sMsg = 'Alterar o estado para C?';
                nEstadoID = 21;

                if (glb_nItensAvaliar > 0)
                    sMsg = 'Existem itens a serem aprovados.\nAlterar estado para C, assim mesmo?';
            }
            else
            {
                sMsg = 'Alterar o estado para P?';
                nEstadoID = 22;
            }
        }
        else
        {
            sMsg = 'Alterar o estado para C?';
            nEstadoID = 21;
        }
    }
    else if (ctrl == btnAprovar)
    {
        var nMargemContribuicao = fg.ValueMatrix(fg.row, getColIndexByColKey(fg, 'MargemContribuicao*'));
        var nMargemMinima = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'MargemMinima'));

        if (nMargemMinima.length == 0)
        {
            if (glb_bCotador) {
                if (window.top.overflyGen.Alert('MC Minima n�o definida. \nDefinir MC Minima no campo Observa��es no Conceito da marca.') == 0)
                    return null;
            }
            else {
                if (window.top.overflyGen.Alert('MC Minima n�o definida. \nDefinir MC Minima em RelacoesPesCon.') == 0)
                    return null;
            }

            return null;
        }

        nMargemMinima = fg.ValueMatrix(fg.row, getColIndexByColKey(fg, 'MargemMinima'));

        if (nMargemContribuicao >= nMargemMinima)
        {
            if (window.top.overflyGen.Alert('N�o requer aprova��o') == 0)
                return null;
        
            return null;
        }


        //sMsg = 'Aprovar MC?';
    }
        
    else if (ctrl == btnReprovar)
    {
        var nMargemContribuicao = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'MargemContribuicao*'));
        var nMargemMinima = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'MargemMinima'));

        if (nMargemMinima.length == 0)
        {
            if (glb_bCotador) {
                if (window.top.overflyGen.Alert('MC Minima n�o definida. \nDefinir MC Minima no campo Observa��es no Conceito da marca.') == 0)
                    return null;
            }
            else {
                if (window.top.overflyGen.Alert('MC Minima n�o definida. \nDefinir MC Minima em RelacoesPesCon.') == 0)
                    return null;
            }

            return null;
        }

        if (nMargemContribuicao >= nMargemMinima) {
            if (window.top.overflyGen.Alert('N�o requer aprova��o') == 0)
                return null;

            return null;
        }

        //sMsg = 'Reprovar MC?';
    }        

    if (sMsg != '')
    {
        _retMsg = window.top.overflyGen.Confirm(sMsg);

        if ((_retMsg == 0) || (_retMsg == 2))
            return null;
    }

    if (ctrl == btnExcluir)
        deleteDataInGridSacola();
    else if (ctrl == btnEsvaziar)
        emptyListTrolley();
    else if (ctrl == btnGravar)
    {
        if (!(glb_MsgCamapanhaAlert == ''))
            window.top.overflyGen.Alert(glb_MsgCamapanhaAlert);

        glb_sMensagemAprovacao = '';

        for (i = 0; i < glb_aMensagemAprovacao.length; i++)
        {
            glb_sMensagemAprovacao = glb_sMensagemAprovacao + glb_aMensagemAprovacao[i][1];
        }

        if (!(glb_sMensagemAprovacao == ''))
        {
            glb_sMensagemAprovacao = glb_sMensagemAprovacao + 'Tem certeza?';
            _retMsg = window.top.overflyGen.Confirm(glb_sMensagemAprovacao);

            if ((_retMsg == 0) || (_retMsg == 2))
                return null;
        }

        glb_bAlteracaoPendente = false;
        glb_MsgCamapanhaAlert = '';
        glb_sMensagemAprovacao = '';
        glb_aMensagemAprovacao = new Array();
        saveDataInGridSacola();
    }
    else if (ctrl == btnCheckout)
    {
        glb_bAlteracaoPendente = false;
        realizarCheckout();
    }
    else if (ctrl == btnFinalizarVenda)
    {
        if (txtEst.value == 'C')
        {
            verificaErroMargem();
        }
        else
        {
            glb_bFinalizarVenda = true;
            SacolaEntrega(true);
        }
    }
    else if (ctrl == btnGravarCheckout)
    {
        glb_bGravarCheckout = true;
        SacolaEntrega(false);  
    }   
    else if (ctrl == btnVoltar)
    {
        checkoutVoltar();
    }
    else if (ctrl == btnRefresh)
    {
        glb_bAlteracaoPendente = false;
        glb_sMensagemAprovacao = '';
        glb_aMensagemAprovacao = new Array();
        glb_bGetMC = true;
        glb_bCombos2 = true;
        fillGridData();
    }
    else if (ctrl == btnPesquisa)
        comutaPesqMode();
    else if (ctrl == btnReferencias)
        comutaReferenciaMode();
    else if (ctrl == btnFichaTecnica)
    {
        if (btnMC.value == 'Voltar')
            mostraHistoricoMC();

        comutaFichaTecnicaMode();
    }
        
    else if (ctrl == btnComprar)
    {
        glb_bAlteracaoPendente = true;
        comprar();
    }
    else if (ctrl == btnNotificar)
    {
        if (txtEst.value == 'A')
        {
            if (window.top.overflyGen.Alert('Colaborador notificado') == 0)
                return null;

            return null;
        }

        notificarCliente();
    }
    else if (ctrl == btnProposta)
    {
        if (selUsuario.selectedIndex >= 0)
            acessaPropostaURL();
    }
    else if (ctrl == btnWEB)
    {
        if (selUsuario.selectedIndex >= 0)
        {
            openSiteWeb();
        }
        else
        {
            window.top.overflyGen.Alert('Para acessar o site, � necess�rio ter um contato com acesso � web.');
        }
    }
    else if (ctrl == btnTelefonia)
    {
        discar(1);
    }
    else if (ctrl == btnCelular)
    {
        discar(2);
    }
    else if ((ctrl == btnSelecionar) || (ctrl == btnSalvarSacola))
    {
        if (ctrl == btnSelecionar)
            SelecionarSacola(1);
        if (ctrl == btnSalvarSacola)
            SelecionarSacola(0);

        /*
        if (ctrl == btnSalvarSacola)
            alteraInterface(2);
        */
    }
    else if (ctrl == btnListar)
    {
        sacolasSalvas();
    }
    else if (ctrl == btnGravarSacola)
    {
        GravarSacolaSalva();
    }
    else if (ctrl == btnEstado)
    {
        alteraEstado(nEstadoID);
    }
    else if (ctrl == btnMC)
    {
        if (btnFichaTecnica.value == 'Voltar')
            comutaFichaTecnicaMode();

        mostraHistoricoMC();
    }
    else if (ctrl == btnAprovar)
    {
        aprovarMargemMinima(true);
    }
    else if (ctrl == btnReprovar)
    {
        aprovarMargemMinima(false);
    }
    else if (ctrl == btnLog)
    {
        if (glb_bLog)
        {
            setupBtnsSacolaFromGridState();
            btnEstado.disabled = false;
            btnMC.disabled = false;
            btnAprovar.disabled = !glb_bAprovacaoSacola;
            btnReprovar.disabled = !glb_bAprovacaoSacola;

            btnLog.value = 'Log';
            //alteraInterface(2);

            divFG.style.visibility = 'visible';
            fg.style.visibility = 'visible';

            divFG2.style.visibility = 'hidden';
            fg2.style.visibility = 'hidden';
        }            
        else
        {
            if (btnMC.value == 'Voltar')
                mostraHistoricoMC();

            btnExcluir.disabled = true;
            btnEsvaziar.disabled = true;
            btnGravar.disabled = true;
            btnCheckout.disabled = true;
            txtVariacao.readOnly = true;
            btnRefresh.disabled = true;
            btnSacolasSalvas.disabled = true;
            btnEstado.disabled = true;
            btnNotificar.disabled = true;
            btnSalvarSacola.disabled = true;
            btnFichaTecnica.disabled = true;
            btnMC.disabled = true;
            btnAprovar.disabled = true;
            btnReprovar.disabled = true;

            btnLog.value = 'Voltar';
            fillGridData_LOG();
        }

        glb_bLog = !glb_bLog;
    }
}

function openSiteWeb() {
    var sUsuario = glb_nUserID;
    var sPagina = 'vitrine.aspx';
    var nParceiroID = dsoSacola.recordset['ParceiroID'].value;
    var nEmpresaID = dsoSacola.recordset['EmpresaID'].value;
    var nUsuarioID = selUsuario.value;
    var nPessoaID = dsoSacola.recordset['PessoaID'].value;
    strPars = '?nUserID=' + escape(sUsuario) + '&Pagina=' + escape(sPagina) + '&ParceiroID=' + escape(nParceiroID) + '&EmpresaID=' + escape(nEmpresaID) + '&ContatoID=' + escape(nUsuarioID) + '&PessoaID=' + escape(nPessoaID);
    dsoWeb.URL = SYS_ASPURLROOT + '/serversidegenEx/rashtoopensite.aspx' + strPars;
    dsoWeb.ondatasetcomplete = openSiteWeb_DCS;
    dsoWeb.refresh();
}

function openSiteWeb_DCS() {
    var sSite;
    if (!((dsoWeb.recordset.BOF) || (dsoWeb.recordset.BOF))) {
        var sHash = dsoWeb.recordset['sHashCode'].value;
        if (dsoWeb.recordset['sHashCode'].value == null || sHash == "") {
            window.top.overflyGen.Alert('Cliente n�o possui acesso ao Site');
            lockControlsInModalWin(false);
            return null;
        }
        if (sHash != null) {
            sSite = 'http://www.alcateia.com.br/alcateiav3/web/aspx/Login.aspx?hc=' + sHash;
            window.open(sSite);
            return null;
        }
    }
}

function discar(nTipo) {
    if (selUsuario.selectedIndex < 0)
        return;

    var sTelefone = '';

    // Telefone
    if (nTipo == 1) {
        sTelefone = selUsuario.options(selUsuario.selectedIndex).getAttribute('Telefone', 1);
    }
        // Celular
    else if (nTipo == 2) {
        sTelefone = selUsuario.options(selUsuario.selectedIndex).getAttribute('Celular', 1);
    }

    if (trimStr(sTelefone) == '')
        return;

    sDDD = sTelefone.substr(2, 2);
    sNumero = sTelefone.substr(5);
    nPessoaID = parseInt(selUsuario.value, 10);
    sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
}

function sendMail_DSC()
{
    if (glb_bCotador) {
        var bClienteNotificado = false;

        for (var i = 1; i < selContato.length; i++) {
            if (selContato[i].selected == true) {
                bClienteNotificado = true;
            }
        }
        if (bClienteNotificado) {
            if (window.top.overflyGen.Alert('Cliente notificado') == 0)
                return null;
        }
        else {
            if (window.top.overflyGen.Alert('Proposta gerada sem notificar o cliente') == 0)
                return null;
        }
    }
    else {
        if (window.top.overflyGen.Alert('Cliente notificado') == 0)
            return null;
    }

    glb_bMandaEmail = false;
    glb_bAlteracaoPendente = false;
    glb_bGetMC = true;
    glb_bCombos2 = true;

    dsoSacola.recordset['ClienteNotificado'].value = 1;

    saveDataInGridSacola();

    // fillGridData();
}

/********************************************************************
Comuta o grid fg2 entre os modos de grid de produtos ou grid de
ficha tecnica.
Tambem esconde o grid fg2 quando em modo de ficha tecnica e mostrado
abaixo do grid de produtos quando a interface em modo de carrinho.
Tambem vai ao servidor obter os dados da ficha tecnica para preencher
o grid fg2.
********************************************************************/
function comutaFichaTecnicaMode()
{
    var bFTMode = (btnFichaTecnica.getAttribute('FTMode', 1) == 1);
    var nProdutoID = 0;

    lockControlsInModalWin(true);

    if (bFTMode)
    {
        btnFichaTecnica.setAttribute('FTMode', 0, 1);
        btnFichaTecnica.value = 'Ficha T�cnica';

        if (btnPesquisa.getAttribute('pesqMode', 1) == 0)
            btnFichaTecnica.title = 'Se preferir, clique duas vezes nas colunas brancas do produto';
        else
            btnFichaTecnica.title = 'Se preferir, clique duas vezes na coluna Produto do produto';

        // Se modo carrinho de compra
        if (btnPesquisa.getAttribute('pesqMode', 1) == 0)
        {
            SacolaEFichaTecnica(false);
            return true;
        }
        else
            getDataGridProdutosConfigurador_DSC();

        if (btnFichaTecnica.getAttribute('nCurrLineProdutos', 1) != null)
        {
            if (fg2.Rows > btnFichaTecnica.getAttribute('nCurrLineProdutos', 1))
            {
                fg2.TopRow = btnFichaTecnica.getAttribute('nCurrLineProdutos', 1);
                fg2.Row = btnFichaTecnica.getAttribute('nCurrLineProdutos', 1);
            }

            btnFichaTecnica.setAttribute('nCurrLineProdutos', null, 1);
        }
    }
    else
    {
        // Se modo carrinho de compra
        if (btnPesquisa.getAttribute('pesqMode', 1) == 0)
            nProdutoID = getCellValueByColKey(fg, 'ProdutoID*', fg.Row);
        else
            nProdutoID = getCellValueByColKey(fg2, 'ConceitoID*', fg2.Row);

        if (nProdutoID == '' || nProdutoID == null) {
            window.top.overflyGen.Alert('Produto sem cadastro');
            lockControlsInModalWin(false);
        }
        else {
            btnFichaTecnica.value = 'Voltar';
            btnFichaTecnica.title = '';
            btnFichaTecnica.setAttribute('FTMode', 1, 1);
            btnFichaTecnica.setAttribute('nCurrLineProdutos', fg2.Row, 1);

            setConnection(dsoFichaTecnica);
            dsoFichaTecnica.SQL =
                'SELECT dbo.fn_Produto_CaracteristicaOrdem(a.ProdutoID, a.CaracteristicaID) AS Ordem, ' +
                       'dbo.fn_Tradutor(b.Conceito, ' + glb_aEmpresaData[7] + ', ' + glb_aEmpresaData[8] + ', NULL) AS Caracteristica, ' +
                       'a.Valor AS Valor, ' +
                       'd.FichaTecnica ' +
                    'FROM Conceitos_Caracteristicas a WITH(NOLOCK) ' +
                        'INNER JOIN Conceitos b WITH(NOLOCK) ON (b.ConceitoID = a.CaracteristicaID) ' +
                        'INNER JOIN Conceitos c WITH(NOLOCK) ON (c.ConceitoID = a.ProdutoID) ' +
                        'INNER JOIN RelacoesConceitos d WITH(NOLOCK) ON ((d.SujeitoID = a.CaracteristicaID) AND (d.ObjetoID = c.ProdutoID)) ' +
                    'WHERE ((a.ProdutoID = ' + nProdutoID + ') AND (a.Checado = 1) AND (d.FichaTecnica = 1)) ' +

                'UNION ALL ' +
                'SELECT 1000 AS Ordem, ' + '\'' + 'Medidas do produto (L/A/P)' + '\'' + ' ' +
                    'AS Caracteristica, dbo.fn_Produto_DadosFichaTecnica(' + nProdutoID + ', ' + glb_aEmpresaData[0] + ', 1) AS Valor,' +
                    '1 AS FichaTecnica ' +
                'UNION ALL ' +
                'SELECT 1001 AS Ordem, ' + '\'' + 'Medidas embalagem (L/A/P)' + '\'' + ' ' +
                    'AS Caracteristica, dbo.fn_Produto_DadosFichaTecnica(' + nProdutoID + ', ' + glb_aEmpresaData[0] + ', 2) AS Valor,' +
                    '1 AS FichaTecnica ' +
                'UNION ALL ' +
                'SELECT 1002 AS Ordem, ' + '\'' + 'Pesos Bruto/L�quido' + '\'' + ' ' +
                    'AS Caracteristica, dbo.fn_Produto_DadosFichaTecnica(' + nProdutoID + ', ' + glb_aEmpresaData[0] + ', 3) AS Valor,' +
                    '1 AS FichaTecnica ' +
                'UNION ALL ' +
                'SELECT 1003 AS Ordem, ' + '\'' + 'Garantia' + '\'' + ' ' +
                    'AS Caracteristica, dbo.fn_Produto_DadosFichaTecnica(' + nProdutoID + ', ' + glb_aEmpresaData[0] + ', 4) AS Valor, ' +
                    '1 AS FichaTecnica ' +
                'ORDER BY Ordem';

            dsoFichaTecnica.ondatasetcomplete = comutaFichaTecnicaMode_DSC;
            dsoFichaTecnica.refresh();
        }
    }
}

/********************************************************************
Retorno do servidor apos obter os dados da ficha tecnica para preencher
o grid fg2.
********************************************************************/
function comutaFichaTecnicaMode_DSC()
{
    var nProdutoID = 0;
    var nFoto = 0;
    var strPars = new String();

    img_onerror();

    // Se modo carrinho de compra
    if (btnPesquisa.getAttribute('pesqMode', 1) == 0)
    {
        nProdutoID = getCellValueByColKey(fg, 'ProdutoID*', fg.Row);
        nFoto = getCellValueByColKey(fg, 'Foto' + glb_sPasteReadOnly, fg.Row);
    }
    else
    {
        nProdutoID = getCellValueByColKey(fg2, 'ConceitoID*', fg2.Row);
        nFoto = getCellValueByColKey(fg2, 'Foto', fg2.Row);
    }
    if (nFoto == 1)
    {
        strPars = '?nFormID=' + escape(2110);
        strPars += '&nSubFormID=' + escape(21000);
        strPars += '&nRegistroID=' + escape(nProdutoID);
        strPars += '&nTamanho=' + escape(3);

        // carrega a imagem do servidor pelo arquivo imageblob.asp
        if (trimStr((SYS_ASPURLROOT.toUpperCase())).lastIndexOf('OVERFLY3') > 0)
            img_Imagem.src = 'http' + ':/' + '/localhost/overfly3/serversidegenEx/imageblob.aspx' + strPars;
        else
            img_Imagem.src = SYS_ASPURLROOT + '/serversidegenEx/imageblob.aspx' + strPars;
    }

    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2);
    fg2.FrozenCols = 0;

    headerGrid(fg2, ['Ordem', 'Caracter�stica', 'Valor'], [0]);
    fillGridMask(fg2, dsoFichaTecnica, ['Ordem', 'Caracteristica', 'Valor'], ['', '', '']);
    alignColsInGrid(fg2, [0]);

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);

    if (fg2.Rows > 1)
    {
        if (fg2.Row > 0)
            fg2.Row = 1;
    }

    fg2.Redraw = 2;

    lockControlsInModalWin(false);

    js_fg2_AfterRowColChange();
    setupBtnsConfiguradorFromGridState();

    // Se modo carrinho de compra mostra o grid de ficha tecnica
    // sob o grid do carrinho    
    if (btnPesquisa.getAttribute('pesqMode', 1) == 0)
    {
        SacolaEFichaTecnica(true);
    }
}

/********************************************************************
Mostra ou esconde o grid de ficha tecnica sob o grid do carrinho
	bShow - true/false
********************************************************************/
function SacolaEFichaTecnica(bShow)
{
    /*
    divFG2.style.top = 204;
    divFG2.style.height = 213;
    */
    divFG2.style.backgroundColor = 'transparent';

    //fg2.style.height = parseInt(divFG2.style.height, 10) - 1;

    if (bShow)
    {
        fg.Redraw = 0;

        divFG.style.height = parseInt(divPesquisa.currentStyle.height, 10) - ELEM_GAP - 3;

        with (fg.style)
        {
            left = 0;
            top = 0;
            width = parseInt(divFG.style.width, 10) - 1;
            height = parseInt(divFG.style.height, 10) - 1;
        }

        fg.Redraw = 2;
        fg.TopRow = fg.Row;

        divFG.style.visibility = 'hidden';
        fg.style.visibility = 'hidden';

        divFG2.style.visibility = 'visible';
        fg2.style.visibility = 'visible';
        btnFichaTecnica.disabled = false;

        btnExcluir.disabled = true;
        btnEsvaziar.disabled = true;
        btnGravar.disabled = true;
        btnCheckout.disabled = true;
        txtVariacao.readOnly = true;
        btnSacolasSalvas.disabled = true;
        btnRefresh.disabled = true;
        btnLog.disabled = true;
        btnEstado.disabled = true;
        btnNotificar.disabled = true;
        btnSalvarSacola.disabled = true;
    }
    else
    {
        divFG.style.height = glb_divFGFullHeight;

        with (fg.style)
        {
            left = 0;
            top = 0;
            width = parseInt(divFG.style.width, 10) - 1;
            height = parseInt(divFG.style.height, 10) - 1;
        }

        divFG.style.visibility = 'visible';
        fg.style.visibility = 'visible';

        divFG2.style.visibility = 'hidden';
        fg2.style.visibility = 'hidden';
        lockControlsInModalWin(false);

        setupBtnsSacolaFromGridState();
    }
}

/********************************************************************
Obtem dados no servidor para preencher o grid fg2 com referencias
tecnicas.
********************************************************************/
function comutaReferenciaMode() {
    var bRefMode = (btnReferencias.getAttribute('RefMode', 1) == 1);
    lockControlsInModalWin(true);

    if (bRefMode) {
        btnReferencias.setAttribute('RefMode', 0, 1);
        btnReferencias.value = 'Refer�ncias';
        btnReferencias.title = 'Se preferir, clique duas vezes na coluna ID do produto';
        getDataGridProdutosConfigurador_DSC();
        if (btnReferencias.getAttribute('nCurrLineProdutos', 1) != null) {
            if (fg2.Rows > btnReferencias.getAttribute('nCurrLineProdutos', 1)) {
                fg2.TopRow = btnReferencias.getAttribute('nCurrLineProdutos', 1);
                fg2.Row = btnReferencias.getAttribute('nCurrLineProdutos', 1);
            }
            btnReferencias.setAttribute('nCurrLineProdutos', null, 1);
        }
    }
    else {
        btnReferencias.value = 'Voltar';
        btnReferencias.title = '';
        btnReferencias.setAttribute('RefMode', 1, 1);
        btnReferencias.setAttribute('nCurrLineProdutos', fg2.Row, 1);
        setConnection(dsoReferencias);

        var nProdutoID = getCellValueByColKey(fg2, 'ConceitoID*', fg2.Row);

        dsoReferencias.SQL = 'SELECT e.Objeto AS Referencia, b.ConceitoID AS ConceitoID, d.RecursoAbreviado AS Estado, b.Conceito AS Conceito ' +
            'FROM RelacoesConceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK), Recursos d WITH(NOLOCK), TiposRelacoes e WITH(NOLOCK) ' +
            'WHERE a.SujeitoID = ' + nProdutoID + ' AND a.EstadoID=2 AND ' +
				'a.ObjetoID=b.ConceitoID AND a.ObjetoID=c.ObjetoID AND c.TipoRelacaoID=61 AND c.EstadoID=d.RecursoID ' +
				'AND c.SujeitoID = ' + glb_aEmpresaData[0] + ' AND a.TipoRelacaoID=e.TipoRelacaoID ' +
				'UNION ALL ' +
				'SELECT e.Sujeito AS Referencia, b.ConceitoID AS ConceitoID, d.RecursoAbreviado AS Estado, ' +
				'b.Conceito AS Conceito ' +
				'FROM RelacoesConceitos a WITH(NOLOCK), Conceitos b WITH(NOLOCK), RelacoesPesCon c WITH(NOLOCK), Recursos d WITH(NOLOCK), TiposRelacoes e WITH(NOLOCK) ' +
				'WHERE a.ObjetoID=' + nProdutoID + ' AND a.EstadoID=2 AND ' +
				'a.SujeitoID=b.ConceitoID AND a.SujeitoID=c.ObjetoID ' +
				'AND c.TipoRelacaoID=61 AND c.EstadoID=d.RecursoID ' +
				'AND c.SujeitoID = ' + glb_aEmpresaData[0] + ' ' +
				'AND a.TipoRelacaoID=e.TipoRelacaoID ' +
				'ORDER BY Referencia,Conceito';

        dsoReferencias.ondatasetcomplete = comutaReferenciaMode_DSC;
        dsoReferencias.refresh();
    }
}

/********************************************************************
Retorno do servidor com dados para preencher o grid fg2 com referencias
tecnicas.
********************************************************************/
function comutaReferenciaMode_DSC() {
    fg2.Redraw = 0;

    img_onerror();

    fg2.Editable = false;
    startGridInterface(fg2);
    fg2.FrozenCols = 0;

    headerGrid(fg2, ['ID', 'Est', 'Conceito', 'Refer�ncia'], []);
    fillGridMask(fg2, dsoReferencias, ['ConceitoID', 'Estado', 'Conceito', 'Referencia'], ['', '', '']);
    alignColsInGrid(fg2, [0]);

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);

    fg2.Redraw = 2;

    lockControlsInModalWin(false);

    js_fg2_AfterRowColChange();
    setupBtnsConfiguradorFromGridState();
}

/********************************************************************
Vai ao servidor, e no retorno,
coloca no grid do carrinho, item selecionado no grid fg2 quando o
grid fg2 esta em modo de produtos.
Invocada no botao comprar ou em duplo clique no grid fg2
********************************************************************/
function comprar() {
    if (fg2.Row <= 0)
        return null;

    glb_bComprou = true;

    var nProdutoID = getCellValueByColKey(fg2, 'ConceitoID*', fg2.Row);
    var nEmpresaID = 0;
    var nQuantidade;
    var nPrecoRevenda;
    var nPrecoUnitario;
    var nChkLinesToBuy;
    var i;
    var strPars = new String();
    var nMoedaID = glb_nMoedaID;
    var nParceiroID = selParceiro.value;
    var nItemIndex = 1;

    if (glb_sCaller == 'S')
        sHTMLCaller = 'SUP_HTML';
    else
        sHTMLCaller = 'PESQLIST_HTML';

    var sFinalidade = sendJSMessage(sHTMLCaller, JS_DATAINFORM, EXECEVAL, '(selFinalidade.selectedIndex >= 0 ? selFinalidade.value : 0)');
    nChkLinesToBuy = chkLinesToBuy();

    if (nChkLinesToBuy < 0) {
        fg2.Row = nChkLinesToBuy * -1;
        return null;
    }

    lockControlsInModalWin(true);

    strPars = '';
    strPars += '?nUserID=' + escape(glb_nUserID);
    strPars += '&nPessoaID=' + escape(selPessoa.value);
    strPars += '&nParceiroID=' + escape(nParceiroID);
    strPars += '&nMoedaID=' + escape(nMoedaID);
    strPars += '&nFinanciamentoID=' + escape(selFinanciamento.value);
    strPars += '&nFrete=' + escape(0);
    strPars += '&sMeioTransporteID=' + escape(0);
    strPars += '&nDataLen=' + escape(nChkLinesToBuy);

    for (i = 1; i < fg2.Rows; i++) {
        nQuantidade = fg2.ValueMatrix(i, getColIndexByColKey(fg2, '_calc_Qtd_10'));
        if (nQuantidade <= 0) continue;
        nProdutoID = getCellValueByColKey(fg2, 'ConceitoID*', i);
        nLoteID = getCellValueByColKey(fg, 'LoteID', i);
        nPrecoInterno = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'PrecoConvertido'));
        nPrecoRevenda = nPrecoInterno;
        nPrecoUnitario = nPrecoInterno;
        glb_aEmpresaData = getCurrEmpresaData();
        nEmpresaID = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'EmpresaID'));
        glb_nUserID = glb_USERID;
        strPars += '&nEmpresaID=' + escape(nEmpresaID);
        strPars += '&nProdutoID=' + escape(nProdutoID);
        strPars += '&nLoteID=' + escape(nLoteID);
        strPars += '&nQuantidade=' + escape(nQuantidade);
        strPars += '&nPrecoInterno=' + escape(nPrecoInterno);
        strPars += '&nPrecoRevenda=' + escape(nPrecoInterno);
        strPars += '&nPrecoUnitario=' + escape(nPrecoInterno);
        strPars += '&sCFOPID=' + escape(0);
        strPars += '&sFinalidade=' + escape(sFinalidade);
        nItemIndex++;
    }

    dsoPurchaseItem.URL = SYS_ASPURLROOT + '/serversidegenEx/purchaseitemtrolley.aspx' + strPars;
    dsoPurchaseItem.ondatasetcomplete = comprar_DSC;
    dsoPurchaseItem.refresh();
}

function chkLinesToBuy() {
    var vlrFatMax = 0;
    var retVal = 0;
    var nQtdItensToBuy = 0;
    var nQuantidade, nPrecoRevenda;
    var nIntialRow = fg2.Row;

    for (i = 1; i < fg2.Rows; i++) {
        vlrFatMax = 0;
        nQuantidade = fg2.ValueMatrix(i, getColIndexByColKey(fg2, '_calc_Qtd_10'));
        nPrecoRevenda = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'PrecoConvertido'));

        if (nQuantidade <= 0)
            continue;
        if (isNaN(fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'PrecoConvertido')))) {
            if (window.top.overflyGen.Alert('Pre�o Convertido Inv�lido.') == 0) return null;
            retVal = i * -1;
            break;
        }
        if (fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'PrecoConvertido')) <= 0) {
            if (window.top.overflyGen.Alert('Pre�o Convertido Inv�lido.') == 0) return null;
            retVal = i * -1;
            break;
        }

        dsoPesquisa.recordset.moveFirst();
        dsoPesquisa.recordset.Find('ConceitoID ', getCellValueByColKey(fg2, 'ConceitoID*', i));

        if (!(dsoPesquisa.recordset.EOF))
            vlrFatMax = dsoPesquisa.recordset['PrecoConvertido'].value;

        if (nPrecoRevenda < (vlrFatMax / 2)) {
            if (window.top.overflyGen.Alert('Valor inferior ao permitido.') == 0) return null;
            retVal = i * -1;
            break;
        }
        else if (nPrecoRevenda > (vlrFatMax * 2)) {
            if (window.top.overflyGen.Alert('Valor superior ao permitido.') == 0) return null;
            retVal = i * -1;
            break;
        }

        nQtdItensToBuy++;
    }
    if ((retVal == 0) && (nQtdItensToBuy == 0) && (nIntialRow > 0)) {
        fg2.TextMatrix(nIntialRow, getColIndexByColKey(fg2, '_calc_Qtd_10')) = '1';
        retVal = 1;
    }
    else
        retVal = (retVal < 0 ? retVal : nQtdItensToBuy);

    return retVal;
}

/********************************************************************
Retorno do servidor para 
colocar no grid do carrinho, item selecionado no grid fg2 quando o
grid fg2 esta em modo de produtos.
********************************************************************/
function comprar_DSC(resultServ) {
    if ((resultServ != null) && (resultServ.code < 0)) {
        window.top.overflyGen.Alert(resultServ.message);
        lockControlsInModalWin(false);
        return null;
    }

    lockControlsInModalWin(false);

    if (!(dsoPurchaseItem.recordset.BOF && dsoPurchaseItem.recordset.EOF)) {
        if (!chkProdutosCompativeis.checked) {
            if (window.top.overflyGen.Alert(dsoPurchaseItem.recordset['Mensagem'].value) == 0) return null;
        }
    }
    else {
        if (window.top.overflyGen.Alert('Compra n�o efetuada.') == 0) return null;
    }

    glb_bGetMC = false;
    for (i = 1; i < fg2.Rows; i++)
        fg2.TextMatrix(i, getColIndexByColKey(fg2, '_calc_Qtd_10')) = 0;

    fillGridData();
}

/********************************************************************
Comuta a interface entre o modo de carrinho de compras e configurador.
Invocada po clique no botao btnPesquisa
********************************************************************/
function comutaPesqMode() {
    var i, bDisableCtrls;
    fg2.Rows = 1;

    img_onerror();

    // Se modo carrinho de compra
    if (btnPesquisa.getAttribute('pesqMode', 1) == 0) {
        divCtlBar1.style.visibility = 'hidden';
        secText('Sacola de Compras - Configurador de Produtos', 1);
        btnReferencias.style.visibility = 'visible';
        btnReferencias.setAttribute('RefMode', 0, 1);
        btnReferencias.value = 'Refer�ncias';
        btnReferencias.title = 'Se preferir, clique duas vezes na coluna ID do produto';

        with (btnFichaTecnica.style) {
            left = parseInt(btnReferencias.currentStyle.left, 10) + parseInt(btnReferencias.currentStyle.width, 10) + 5;
            visibility = 'visible';
        }

        btnFichaTecnica.disabled = true;

        if (fg.Rows > 2) {
            if (fg.Row > 1) btnFichaTecnica.disabled = false;
        }

        btnFichaTecnica.setAttribute('FTMode', 0, 1);
        btnFichaTecnica.value = 'Ficha T�cnica';
        btnFichaTecnica.title = 'Se preferir, clique duas vezes na coluna Produto do produto';
        btnComprar.style.visibility = 'visible';
        btnPesquisa.value = 'Sacola';
        divPesquisa.style.visibility = 'visible';
        divFG2.style.visibility = 'visible';
        fg2.style.visibility = 'visible';
        fg2.Cols = 1;
        fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;
        fg2.Redraw = 2;
        selEstadoProdutos.style.visibility = 'visible';
        selProduto.style.visibility = 'visible';
        selMarca.style.visibility = 'visible';
        selMetodoPesquisa.style.visibility = 'visible';
        selChavePesquisa.style.visibility = 'visible';
        txtPalavraChave.style.visibility = 'visible';
        selOutrosProdutos.style.visibility = 'visible';
        txtConceitoConcreto.style.visibility = 'visible';
        txtMarca.style.visibility = 'visible';
        txtModelo.style.visibility = 'visible';
        txtDescricao.style.visibility = 'visible';
        lblArgumentosVenda.style.visibility = 'visible';
        txtArgumentosVenda.style.visibility = 'visible';
        bDisableCtrls = false;
        btnPesquisa.setAttribute('pesqMode', 1, 1);
        divFG.style.visibility = 'hidden';
        fg.style.visibility = 'hidden';
        lblConfiguradorMsg.style.visibility = 'visible';
        btnExcluir.style.visibility = 'hidden';
        btnEsvaziar.style.visibility = 'hidden';
        btnGravar.style.visibility = 'hidden';
        btnCheckout.style.visibility = 'hidden';
        txtVariacao.style.visibility = 'hidden';
        btnRefresh.style.visibility = 'hidden';
    }
    else {
        secText('Sacola de Compras', 1);

        divCtlBar1.style.visibility = 'visible';
        SacolaEFichaTecnica(false);
        btnReferencias.style.visibility = 'hidden';

        with (btnFichaTecnica.style) {
            left = parseInt(btnRefresh.currentStyle.left, 10) + parseInt(btnRefresh.currentStyle.width, 10) + 5;
            visibility = 'visible';
        }

        btnFichaTecnica.setAttribute('FTMode', 0, 1);
        btnFichaTecnica.value = 'Ficha T�cnica';
        btnFichaTecnica.title = 'Se preferir, clique duas vezes nas colunas brancas do produto';
        btnFichaTecnica.disabled = true;

        if (fg.Rows > 2) {
            if (fg.Row > 1) btnFichaTecnica.disabled = false;
        }

        btnComprar.style.visibility = 'hidden';
        divPesquisa.style.visibility = 'hidden';
        divFG2.style.visibility = 'hidden';
        fg2.style.visibility = 'hidden';
        selEstadoProdutos.style.visibility = 'hidden';
        selProduto.style.visibility = 'hidden';
        selMarca.style.visibility = 'hidden';
        selMetodoPesquisa.style.visibility = 'hidden';
        selChavePesquisa.style.visibility = 'hidden';
        txtPalavraChave.style.visibility = 'hidden';
        selOutrosProdutos.style.visibility = 'hidden';
        txtConceitoConcreto.style.visibility = 'hidden';
        txtMarca.style.visibility = 'hidden';
        txtModelo.style.visibility = 'hidden';
        txtDescricao.style.visibility = 'hidden';
        lblArgumentosVenda.style.visibility = 'hidden';
        txtArgumentosVenda.style.visibility = 'hidden';
        bDisableCtrls = true;
        btnPesquisa.setAttribute('pesqMode', 0, 1);
        btnPesquisa.value = 'Configurador';
        divFG.style.visibility = 'visible';
        fg.style.visibility = 'visible';
        lblConfiguradorMsg.style.visibility = 'hidden';
        btnExcluir.style.visibility = 'visible';
        btnEsvaziar.style.visibility = 'visible';
        btnGravar.style.visibility = 'visible';
        btnCheckout.style.visibility = 'visible';
        txtVariacao.style.visibility = 'visible';
        btnRefresh.style.visibility = 'visible';

        if (glb_bComprou) {
            glb_bComprou = false;
            fillGridData();
            return null;
        }
        else
            fillGridData_DSC();
    }

    js_fg2_AfterRowColChange();
    disablePesquisaCtrls(bDisableCtrls);
    drawButtonsFamilia();
    setupBtnsConfiguradorFromGridState();
    refreshLblConfiguradorMsg();
}

/********************************************************************
Habilita ou desabilita os controles INPUT, SELECT e grid, 
do modo de configurador da interface

Parametros
	bDisableCtrls - true/false

********************************************************************/
function disablePesquisaCtrls(bDisableCtrls) {
    var i;
    var coll = window.document.getElementsByTagName('INPUT');

    for (i = 0; i < coll.length; i++) {
        if (coll.item(i).parentElement != null) {
            if ((coll.item(i).parentElement.id).toUpperCase() == 'DIVPESQUISA') {
                elem = coll.item(i);
                with (elem) {
                    disabled = bDisableCtrls;
                }
            }
        }
    }

    coll = window.document.getElementsByTagName('SELECT');

    for (i = 0; i < coll.length; i++) {
        if (coll.item(i).parentElement != null) {
            if ((coll.item(i).parentElement.id).toUpperCase() == 'DIVPESQUISA') {
                elem = coll.item(i);

                with (elem) {
                    disabled = bDisableCtrls;
                }
            }
        }
    }

    fg2.disabled = bDisableCtrls;
}

/********************************************************************
Usuario trocou item selecionado em um combo
********************************************************************/
function cmb_onchange()
{
    glb_LastCmbChanged = null;
    glb_bGetMC = false;

    if (this == selVendedor) {
        lockControlsInModalWin(true);
        glb_bAlteracaoPendente = false;
        glb_LastCmbChanged = selVendedor;
        selVendedor_Changed();
    }
    else if (this == selPessoa) {
        lockControlsInModalWin(true);
        glb_bAlteracaoPendente = false;
        glb_LastCmbChanged = selPessoa;
        selPessoa_Changed();
    }
    else if (this == selParceiro) {
        lockControlsInModalWin(true);
        glb_bAlteracaoPendente = false;
        glb_LastCmbChanged = selParceiro;
        selParceiro_Changed();
    }
    else if (this == selUsuario) {
        glb_bAlteracaoPendente = true;
        selUsuario_Changed();
    }
    else if (this == selTransacaoID)
    {
        glb_bAlteracaoPendente = true;
        selTransacaoID_Changed(true);
    }

    else if (this == selForma) {
        glb_bAlteracaoPendente = true;
        selForma_Changed();
    }
    else if (this == selFinanciamento) {
        glb_bAlteracaoPendente = true;
        selFinanciamento_Changed();
    }
    else if (this == selEstadoProdutos) {
        selEstadoProdutos_Changed();
    }
    else if (this == selProduto) {
        selProdutoOuMarca_Changed(this);
    }
    else if (this == selMarca) {
        selProdutoOuMarca_Changed(this);
    }
    else if (this == selOutrosProdutos) {
        selOutrosProdutos_Changed();
    }
    setupBtnsSacolaFromGridState();
}

function adjustLabelUsuario()
{
    var sHint = '';
    var sReturn = String.fromCharCode(13, 10);

    if (selUsuario.selectedIndex >= 0)
    {
        sHint = 'Telefone: ' + selUsuario.options(selUsuario.selectedIndex).getAttribute('Telefone', 1) + '   ';
        sHint += 'Celular: ' + selUsuario.options(selUsuario.selectedIndex).getAttribute('Celular', 1) + sReturn;
        sHint += 'E-mail: ' + selUsuario.options(selUsuario.selectedIndex).getAttribute('Email', 1) + sReturn;
        sHint += 'Acesso Web ID: ' + selUsuario.value + '   ';
        sHint += 'Senha: ' + selUsuario.options(selUsuario.selectedIndex).getAttribute('Senha', 1) + sReturn;
        sHint += selUsuario.options(selUsuario.selectedIndex).getAttribute('DadosAcesso', 1);
    }

    lblUsuario.title = sHint;
    setLabelOfControlEx(lblUsuario, selUsuario);
}

/********************************************************************
Usuario trocou o vendedor
********************************************************************/
function selVendedor_Changed()
{
    var nVendedorID = 0;
    var nPessoaID = 0;
    var nSacolaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nSacolaID');
    var nPessoaDefaultID = 0;
    var nPessoaAtendimentoID = 0;
    var nFonteID = 0;
    var nChavePessoaID = 0;
    var nParceiroID = 0;
    var bSemPessoa = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selChavePessoaID.value') == 2 ? true : false);

    if (nSacolaID > 0)
        sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'anulaSacola()');

    if (selVendedor.selectedIndex >= 0)
        nVendedorID = selVendedor.value;

    setLabelOfControlEx(lblVendedor, selVendedor);

    adjustLabelUsuario();

    clearComboEx(['selParceiro', 'selPessoa', 'selTransacaoID', 'selFinanciamento', 'selUsuario', 'selContato']);

    nPessoaAtendimentoID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selPessoaID.value');
    nFonteID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selFonteID.value');
    nChavePessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selChavePessoaID.value');
    nParceiroID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selParceiroID.value');

    if (!((dsoCombos.recordset.BOF) && (dsoCombos.recordset.EOF)))
    {
        dsoCombos.recordset.moveFirst();

        if ((nPessoaAtendimentoID != 0) && (nPessoaAtendimentoID != ''))
            dsoCombos.recordset.setFilter('VendedorID = ' + nVendedorID + ' AND PessoaID = ' + nPessoaAtendimentoID);
        else if (glb_nSacolaSelecionadaID != 0)
        {
            dsoCombos.recordset.setFilter('VendedorID = ' + nVendedorID + ' AND SacolaID = ' + glb_nSacolaSelecionadaID);
            glb_nSacolaSelecionadaID = 0;
        }
        else if (nFonteID == 3 && nChavePessoaID == 2)
        {
            dsoCombos.recordset.setFilter('VendedorID = ' + nVendedorID + ' AND ParceiroID = ' + nParceiroID);
        }
        else
            dsoCombos.recordset.setFilter('VendedorID = ' + nVendedorID);

        if (divSacolaSalvas.style.visibility != 'hidden')
        {
            var oOption = document.createElement("OPTION");
            oOption.text = "";
            oOption.value = 0;
            oOption.setAttribute('PessoaCidade', "", 1);
            oOption.setAttribute('PessoaUF', "", 1);
            selPessoa.add(oOption);
        }

        while (!dsoCombos.recordset.EOF)
        {
            nPessoaID = dsoCombos.recordset['PessoaID'].value;

            if (lookUpValueInCmb(selPessoa, nPessoaID) == -1)
            {
                if (nSacolaID > 0)
                {
                    if (dsoCombos.recordset['SacolaID'].value == nSacolaID)
                        nPessoaDefaultID = dsoCombos.recordset['PessoaID'].value;
                }                

                var oOption = document.createElement("OPTION");
                oOption.text = dsoCombos.recordset['Pessoa'].value;
                oOption.value = dsoCombos.recordset['PessoaID'].value;
                oOption.setAttribute('PessoaCidade', dsoCombos.recordset['PessoaCidade'].value, 1);
                oOption.setAttribute('PessoaUF', dsoCombos.recordset['PessoaUF'].value, 1);
                selPessoa.add(oOption);
            }

            dsoCombos.recordset.moveNext();
        }

        dsoCombos.recordset.setFilter('');
    }

    selPessoa.disabled = (selPessoa.options.length == 0);

    if ((bSemPessoa) && (!glb_AlteradoPessoa))
        glb_nPessoaDefaultID = glb_nPessoaID;

    if (nPessoaID != 0)
        selOptByValueInSelect(getHtmlId(), selPessoa.id, glb_nPessoaID);

    if ((!selPessoa.disabled) && (nPessoaDefaultID > 0))
        glb_nPessoaDefaultID = nPessoaDefaultID;

    if ((!selPessoa.disabled) && (glb_nPessoaDefaultID > 0))
        selOptByValueInSelect(getHtmlId(), selPessoa.id, glb_nPessoaDefaultID);

    //Reseta a var�avel global - Utilizada no momento em que o usu�rio gravou uma sacola de sem pessoa para com pessoa e clicou no bot�o voltar
    glb_AlteradoPessoa = false;

    setLabelOfControlEx(lblPessoa, selPessoa);
    selPessoa_Changed();
}

/********************************************************************
Usuario trocou o pessoa
********************************************************************/
function selPessoa_Changed()
{
    var nVendedorID = 0;
    var nPessoaID = 0;
    var nParceiroID = 0;
    var sCidade = '';
    var sUF = '';

    var nFonteID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selFonteID.value');
    var nChavePessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selChavePessoaID.value');
    var nParceiroID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selParceiroID.value');

    if (selPessoa.selectedIndex >= 0)
    {
        glb_nPessoaDefaultID = selPessoa.value;
        nPessoaID = selPessoa.value;
    }
        

    if (selVendedor.selectedIndex >= 0)
        nVendedorID = selVendedor.value;

    glb_sUF = '';
    setLabelOfControlEx(lblPessoa, selPessoa);

    if (selPessoa.selectedIndex >= 0) {
        sCidade = selPessoa.options.item(selPessoa.selectedIndex).getAttribute('PessoaCidade', 1);
        sUF = selPessoa.options.item(selPessoa.selectedIndex).getAttribute('PessoaUF', 1);
        glb_sUF = sUF;

        lblPessoa.title = sCidade + ' ' + sUF;
        selPessoa.title = sCidade + ' ' + sUF;
    }
    else {
        lblPessoa.title = '';
        selPessoa.title = '';
    }

    clearComboEx(['selParceiro', 'selTransacaoID', 'selFinanciamento', 'selUsuario', 'selContato']);

    if (!((dsoCombos.recordset.BOF) && (dsoCombos.recordset.EOF)))
    {
        dsoCombos.recordset.moveFirst();
        
        if (nFonteID == 3 && nChavePessoaID == 2) {
            dsoCombos.recordset.setFilter('VendedorID = ' + nVendedorID + ' AND ParceiroID = ' + nParceiroID + ' AND PessoaID = ' + nPessoaID);
        }
        else {
            dsoCombos.recordset.setFilter('VendedorID = ' + nVendedorID + ' AND PessoaID = ' + nPessoaID);
        }

        while (!dsoCombos.recordset.EOF) {
            nParceiroID = dsoCombos.recordset['ParceiroID'].value;

            if (lookUpParceiroCmb(dsoCombos.recordset['Parceiro'].value) == -1) {
                var oOption = document.createElement("OPTION");
                oOption.text = dsoCombos.recordset['Parceiro'].value;
                oOption.value = dsoCombos.recordset['ParceiroID'].value;
                oOption.setAttribute('SacolaID', dsoCombos.recordset['SacolaID'].value, 1);
                oOption.setAttribute('Sacola', dsoCombos.recordset['Sacola'].value, 1);
                oOption.setAttribute('ListaPreco', dsoCombos.recordset['ListaPreco'].value, 1);
                oOption.setAttribute('EmpresaID', dsoCombos.recordset['EmpresaID'].value, 1);
                oOption.setAttribute('OrigemSacolaID', dsoCombos.recordset['OrigemSacolaID'].value, 1);
                selParceiro.add(oOption);
            }

            dsoCombos.recordset.moveNext();
        }

        dsoCombos.recordset.setFilter('');
    }

    selParceiro.disabled = (selParceiro.options.length == 0);

    if (nParceiroID != 0)
        selOptByValueInSelect(getHtmlId(), selParceiro.id, glb_nParceiroID);

    selParceiro_Changed();
}

function lookUpParceiroCmb(sText) {
    var retVal = -1;
    var i = 0;

    for (i = 0; i < selParceiro.options.length; i++) {
        if (selParceiro.options[i].innerText == sText) {
            retVal = i;
            break;
        }
    }

    return retVal;
}

/********************************************************************
Usuario trocou o parceiro
********************************************************************/
function selParceiro_Changed() {
    setLabelOfControlEx(lblParceiro, selParceiro);
    adjustLabelUsuario();

    glb_bCombos2 = true;

    if (divSacolaSalvas.style.visibility != 'hidden')
        sacolasSalvas();
    else
        // glb_TrolleyTimerInt = window.setInterval('fillDSOCmbs2()', 30, 'JavaScript');
        glb_TrolleyTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
}

/********************************************************************
Usuario trocou o Contato
********************************************************************/
function selUsuario_Changed() {
    adjustLabelUsuario();
    lockUnlockAllCmbs([divCtlBar1, divCtlBar2]);
}

/********************************************************************
Usuario trocou o Transacao
********************************************************************/
function selTransacaoID_Changed(bEvent)
{
    var sTitle = '';

    if (selTransacaoID.selectedIndex >= 0)
        sTitle = selTransacaoID.options.item(selTransacaoID.selectedIndex).getAttribute('Transacao', 1);

    lblTransacao.title = sTitle;
    selTransacaoID.title = sTitle;

    if (bEvent)
        fg.Rows = 1;
}

/********************************************************************
Usuario trocou a Forma
********************************************************************/
function selForma_Changed() {
    setLabelOfControlEx(lblForma, selForma);
}

/********************************************************************
Usuario trocou o prazo
********************************************************************/
function selFinanciamento_Changed()
{
    var i;
    glb_bGetMC = false;
    setLabelOfControlEx(lblFinanciamento, selFinanciamento);


    txtMargemContribuicao.value = '';
    /*
    for (i = 1; i < fg.Rows; i++)
        fg.TextMatrix(i, getColIndexByColKey(fg, 'MargemContribuicao*')) = '';
    */

    fillDadosFinanciamento();
}

function fillDadosFinanciamento() {
    clearComboEx(['selParcelas']);

    if (selFinanciamento.selectedIndex >= 0) {
        var oOption = document.createElement("OPTION");
        oOption.text = selFinanciamento.options.item(selFinanciamento.selectedIndex).getAttribute('NumeroParcelas', 1);
        oOption.value = selFinanciamento.options.item(selFinanciamento.selectedIndex).getAttribute('NumeroParcelas', 1);
        selParcelas.add(oOption);
        selParcelas.selectedIndex = 0;

        txtPrazo1.value = selFinanciamento.options.item(selFinanciamento.selectedIndex).getAttribute('Prazo1', 1);
        txtIncremento.value = selFinanciamento.options.item(selFinanciamento.selectedIndex).getAttribute('Incremento', 1);
        txtPMP.value = selFinanciamento.options.item(selFinanciamento.selectedIndex).getAttribute('PMP', 1);
        txtAcrescimo.value = selFinanciamento.options.item(selFinanciamento.selectedIndex).getAttribute('Acrescimo', 1);
    }
    else {
        txtPrazo1.value = '';
        txtIncremento.value = '';
        txtPMP.value = '';
        txtAcrescimo.value = '';
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;

    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK') {
        lockControlsInModalWin(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, glb_sCaller, null);
    }
        // codigo privado desta janela
    else {
        var _retMsg;
        var sMsg = '';

        if (!btnGravar.disabled)
            sMsg = 'Fechar Sacola sem gravar?';

        if (sMsg != '') {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
            if ((_retMsg == 0) || (_retMsg == 2)) return false;
        }

        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_' + glb_sCaller), null);
    }
}

/********************************************************************
Funcao chamada pela automacao
Esta funcao so executa se a modal havia sido previamente executada,
ou seja, apos a primeira carga da modal.
Atualizar parametros e dados aqui e apos, mostrar a modal.

Parametros:
fromServer  - true, a modal esta vindo do servidor
            - null, a modal ja esta carregada

********************************************************************/
function refreshParamsAndDataAndShowModalWin(fromServer)
{
    var sHTMLCaller = '';
    var bSemPessoa = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selChavePessoaID.value') == 2 ? true : false);
    
    if (glb_sCaller == 'S')
        sHTMLCaller = 'SUP_HTML';
    else
        sHTMLCaller = 'PESQLIST_HTML';

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 50;
    modalFrame.style.left = 0;

    // @@ atualizar parametros e dados aqui
    glb_aEmpresaData = getCurrEmpresaData();
    glb_nUserID = glb_USERID;

    if (bSemPessoa && !glb_AlteradoPessoa)
        glb_nPessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nCotadorPessoaID');
    else
        glb_nPessoaID = ((glb_nPessoaID == null) || (glb_nPessoaID == '') ? 0 : glb_nPessoaID);

    glb_nParceiroID = sendJSMessage(sHTMLCaller, JS_DATAINFORM, EXECEVAL, 'selParceiroID.value');

    // @@ final de atualizar parametros e dados aqui

    // @@ atualizar interface aqui
    lockUnlockAllCmbs([divCtlBar1, divCtlBar2]);
    setupBtnsSacolaFromGridState();

    // @@ final de atualizar interface aqui

    if (divSacolaSalvas.style.visibility == 'hidden')
        fillDSOCmbs1(1);
    else if (!fromServer)
        alteraInterface(2); //fillDSOCmbs1(4);

    return null;
}

/************************************************************************************************
Vai ao servidor para buscar os dados e preencher os combos selVendedor, selParceiro, selPessoa
************************************************************************************************/
function fillDSOCmbs1(ntipo)
{
    glb_bCotadorPesqList = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selFonteID.value') == 3 ? true : false);

    if (glb_TrolleyTimerInt != null)
    {
        window.clearInterval(glb_TrolleyTimerInt);
        glb_TrolleyTimerInt = null;
    }

    var nVendedorID = 0;
    var strPars = new String();
    var bSemPessoa;

    strPars = '';
    bSemPessoa = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selChavePessoaID.value') == 2 ? true : false);
    glb_nParceiroID = ((glb_nParceiroID == null) || (glb_nParceiroID == '') ? 0 : glb_nParceiroID);

    if (bSemPessoa && !glb_AlteradoPessoa && glb_bCotadorPesqList)
        glb_nPessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'glb_nCotadorPessoaID');
    else
        glb_nPessoaID = ((glb_nPessoaID == null) || (glb_nPessoaID == '') ? 0 : glb_nPessoaID);

    if (selParceiro.selectedIndex >= 0)
        nEmpresaID = selParceiro.options.item(selParceiro.selectedIndex).getAttribute('EmpresaID', 1);
    else
        nEmpresaID = glb_nEmpresaID;

    if (selVendedor.selectedIndex >= 0)
        nVendedorID = selVendedor.value;

    strPars += '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nEmpresaPaisID=' + escape(glb_nEmpresaPaisID);
    strPars += '&nPessoaID=' + escape(glb_nPessoaID);
    strPars += '&nParceiroID=' + escape(glb_nParceiroID);

    if (ntipo == 1)
    {
        if (nVendedorID > 0)
            strPars += '&nUserID=' + escape(nVendedorID);
        else
            strPars += '&nUserID=' + escape(-glb_USERID);
    }        
    else if (ntipo == 4)
        strPars += '&nUserID=' + escape(glb_USERID);

    strPars += '&nTipo=' + escape(ntipo);

    dsoCombos.URL = SYS_ASPURLROOT + '/serversidegenEx/trolleycmbsdata.aspx' + strPars;
    dsoCombos.ondatasetcomplete = fillDSOCmbs1_DSC;
    dsoCombos.refresh();
}

/************************************************************************************************
Preenche os combos selVendedor, selParceiro, selPessoa, e vai buscar Transacao e Prazo
************************************************************************************************/
function fillDSOCmbs1_DSC()
{
    var nVendedorID = 0;
    glb_nVendedorDefaultID = 0;

    if (selVendedor.selectedIndex >= 0)
    {
        glb_nVendedorDefaultID = selPessoa.value;
    }    

    clearComboEx(['selVendedor', 'selPessoa', 'selParceiro', 'selTransacaoID', 'selFinanciamento', 'selContato']);

    if (glb_nPessoaID > 0)
    {
        dsoCombos.recordset.moveFirst();
        dsoCombos.recordset.setFilter('VendedorID=' + glb_nUserID);
    }
    while (!dsoCombos.recordset.EOF)
    {
        nVendedorID = dsoCombos.recordset['VendedorID'].value;
        if (lookUpValueInCmb(selVendedor, nVendedorID) == -1)
        {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCombos.recordset['Vendedor'].value;
            oOption.value = dsoCombos.recordset['VendedorID'].value;
            selVendedor.add(oOption);
        }

        dsoCombos.recordset.moveNext();
    }

    dsoCombos.recordset.setFilter('');

    selVendedor.disabled = (selVendedor.options.length == 0);

    if (glb_nVendedorDefaultID != 0)
        selOptByValueInSelect(getHtmlId(), selVendedor.id, glb_nVendedorDefaultID);
    else
        selOptByValueInSelect(getHtmlId(), selVendedor.id, glb_nUserID);

    setLabelOfControlEx(lblVendedor, selVendedor);

    // @@ atualizar interface aqui
    lockUnlockAllCmbs([divCtlBar1, divCtlBar2]);
    setupBtnsSacolaFromGridState();

    showExtFrame(window, true);
    window.focus();
    fg.focus();

    selVendedor_Changed();
}

/************************************************************************************************
Vai ao servidor para buscar os dados e preencher os combos inferiores
************************************************************************************************/
function fillDSOCmbs2()
{
    if (glb_TrolleyTimerInt != null)
    {
        window.clearInterval(glb_TrolleyTimerInt);
        glb_TrolleyTimerInt = null;
    }

    lockControlsInModalWin(true);

    var nPessoaID = 0;
    var nParceiroID = 0;
    var nEmpresaID = 0;
    var nVendedorID = 0;

    if (selParceiro.selectedIndex >= 0)
        nEmpresaID = selParceiro.options.item(selParceiro.selectedIndex).getAttribute('EmpresaID', 1);
    else
        nEmpresaID = glb_nEmpresaID;

    if (selPessoa.selectedIndex >= 0)
        nPessoaID = selPessoa.value;

    if (selParceiro.selectedIndex >= 0)
        nParceiroID = selParceiro.value;

    if (selVendedor.selectedIndex >= 0)
        nVendedorID = selVendedor.value;
    else
        nVendedorID = 0;

    var strPars = new String();

    strPars = '';
    strPars += '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nParceiroID=' + escape(nParceiroID);
    strPars += '&nUserID=' + escape(nVendedorID);
    strPars += '&nTipo=' + escape(2);

    dsoCombos2.URL = SYS_ASPURLROOT + '/serversidegenEx/trolleycmbsdata.aspx' + strPars;
    dsoCombos2.ondatasetcomplete = fillDSOCmbs2_DSC;
    dsoCombos2.refresh();
}

/************************************************************************************************
Preenche os combos selVendedor, selParceiro, selPessoa, e vai buscar Transacao e Prazo
************************************************************************************************/
function fillDSOCmbs2_DSC()
{
    lockControlsInModalWin(false);
    clearComboEx(['selUsuario', 'selTransacaoID', 'selForma', 'selFinanciamento']);

    var bDadosSacola = false;

    if (!((dsoCombos2.recordset.BOF) && (dsoCombos2.recordset.EOF))) {
        // Usuario
        dsoCombos2.recordset.moveFirst();
        dsoCombos2.recordset.setFilter('Indice = 1');

        while (!dsoCombos2.recordset.EOF)
        {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCombos2.recordset['Campo'].value;
            oOption.value = dsoCombos2.recordset['RegistroID'].value;
            oOption.setAttribute('Telefone', dsoCombos2.recordset['Telefone'].value, 1);
            oOption.setAttribute('Celular', dsoCombos2.recordset['Celular'].value, 1);
            oOption.setAttribute('Email', dsoCombos2.recordset['Email'].value, 1);
            oOption.setAttribute('Senha', dsoCombos2.recordset['Senha'].value, 1);
            oOption.setAttribute('DadosAcesso', dsoCombos2.recordset['DadosAcesso'].value, 1);
            selUsuario.add(oOption);
            dsoCombos2.recordset.moveNext();
        }

        selUsuario.disabled = (selUsuario.options.length == 0);

        dsoCombos2.recordset.setFilter('');

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF))
        {
            selUsuario.selectedIndex = -1;
        }
        else {
            if (dsoSacola.recordset.fields['UsuarioID'].value != null)
            {
                selOptByValueInSelect(getHtmlId(), 'selUsuario', dsoSacola.recordset['UsuarioID'].value);
            }
            else
            {
                selUsuario.selectedIndex = -1;
            }
        }

        adjustLabelUsuario();

        //Contatos
        clearComboEx(['selContato']);
        dsoCombos2.recordset.moveFirst();
        dsoCombos2.recordset.setFilter('Indice = 1');

        while (!dsoCombos2.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCombos2.recordset['Campo'].value;
            oOption.value = dsoCombos2.recordset['RegistroID'].value;
            oOption.setAttribute('Email', dsoCombos2.recordset['Email'].value, 1);
            selContato.add(oOption);
            dsoCombos2.recordset.moveNext();
        }

        selContato.disabled = (selContato.options.length == 0);

        dsoCombos2.recordset.setFilter('');

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF)) {
            selContato.selectedIndex = -1;
        }
        else {
            if (dsoSacola.recordset.fields['UsuarioID'].value != null) {
                selOptByValueInSelect(getHtmlId(), 'selContato', dsoSacola.recordset['UsuarioID'].value);
            }
            else {
                selContato.selectedIndex = -1;
            }
        }

        // Transacao
        dsoCombos2.recordset.moveFirst();
        dsoCombos2.recordset.setFilter('Indice = 2');
        //dsoCombos2.recordset.setFilter('EhServico = 0');

        while (!dsoCombos2.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCombos2.recordset['RegistroID'].value + ' ' + dsoCombos2.recordset['Campo'].value;
            oOption.value = dsoCombos2.recordset['RegistroID'].value;
            oOption.setAttribute('Transacao', dsoCombos2.recordset['Campo'].value, 1);
            selTransacaoID.add(oOption);
            dsoCombos2.recordset.moveNext();
        }

        dsoCombos2.recordset.setFilter('');

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF)) {
            selTransacaoID.selectedIndex = -1;
        }
        else {
            if (dsoSacola.recordset.fields['TransacaoID'].value != null)
                selOptByValueInSelect(getHtmlId(), 'selTransacaoID', dsoSacola.recordset['TransacaoID'].value);
            else
                selTransacaoID.selectedIndex = -1;
        }

        selTransacaoID_Changed(false);

        // Forma
        dsoCombos2.recordset.moveFirst();
        dsoCombos2.recordset.setFilter('Indice = 3');

        while (!dsoCombos2.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCombos2.recordset['CampoAbreviado'].value;
            oOption.value = dsoCombos2.recordset['RegistroID'].value;
            selForma.add(oOption);
            dsoCombos2.recordset.moveNext();
        }

        dsoCombos2.recordset.setFilter('');

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF)) {
            selForma.selectedIndex = -1;
        }
        else {
            if (dsoSacola.recordset.fields['FormaPagamentoID'].value != null)
                selOptByValueInSelect(getHtmlId(), 'selForma', dsoSacola.recordset['FormaPagamentoID'].value);
            else
                selForma.selectedIndex = -1;
        }

        setLabelOfControlEx(lblForma, selForma);

        // Financiamento
        dsoCombos2.recordset.moveFirst();
        dsoCombos2.recordset.setFilter('Indice = 4');

        while (!dsoCombos2.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCombos2.recordset['Campo'].value;
            oOption.value = dsoCombos2.recordset['RegistroID'].value;
            oOption.setAttribute('NumeroParcelas', dsoCombos2.recordset['NumeroParcelas'].value, 1);
            oOption.setAttribute('Prazo1', dsoCombos2.recordset['Prazo1'].value, 1);
            oOption.setAttribute('Incremento', dsoCombos2.recordset['Incremento'].value, 1);
            oOption.setAttribute('PMP', dsoCombos2.recordset['PMP'].value, 1);
            oOption.setAttribute('EmpresaSUP', dsoCombos2.recordset['EmpresaSUP'].value, 1);
            oOption.setAttribute('Variacao', dsoCombos2.recordset['Variacao'].value, 1);
            oOption.setAttribute('Acrescimo', dsoCombos2.recordset['sVariacao'].value, 1);
            selFinanciamento.add(oOption);
            dsoCombos2.recordset.moveNext();
        }

        dsoCombos2.recordset.setFilter('');

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF)) {
            selFinanciamento.selectedIndex = -1;
        }
        else {
            if (dsoSacola.recordset.fields['FinanciamentoID'].value != null)
                selOptByValueInSelect(getHtmlId(), 'selFinanciamento', dsoSacola.recordset['FinanciamentoID'].value);
            else
                selFinanciamento.selectedIndex = -1;
        }

        /************
		Movido temporariamente para o retorno da funcao fillDeposito.
		Remover apos unificar o retorno das funcoes que sao chamadas na selTransacaoID_Changed(); 		
		if (dsoCarrinho.recordset.fields['DepositoID'].value != null)
			selOptByValueInSelect(getHtmlId(), 'selDepositoID', dsoCarrinho.recordset['DepositoID'].value);
		else
			selDepositoID.selectedIndex = -1;
		*************/
        setLabelOfControlEx(lblFinanciamento, selFinanciamento);
        fillDadosFinanciamento();

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF))
        {
            txtSacolaID.value = '';
        }
        else
        {
            if (dsoSacola.recordset['SacolaID'].value != null)
                txtSacolaID.value = dsoSacola.recordset['SacolaID'].value;
            else
                txtSacolaID.value = '';
        }

        

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF))
        {
            txtSacola.value = '';
        }
        else {
            if (dsoSacola.recordset['Sacola'].value != null)
                txtSacola.value = dsoSacola.recordset['Sacola'].value;
            else
                txtSacola.value = '';
        }

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF))
        {
            txtEst.value = '';
        }
        else
        {
            if (dsoSacola.recordset['Estado'].value != null)
            {
                txtEst.value = dsoSacola.recordset['Estado'].value;
                txtEst.title = dsoSacola.recordset['EstadoID'].value + '-' + dsoSacola.recordset['EstadoTexto'].value;
            }                
            else
                txtEst.value = '';
        }        

        if (txtEst.value == 'C')
            btnFinalizarVenda.value = 'Avan�ar';
        else
            btnFinalizarVenda.value = 'Finalizar Venda';

        if (txtEst.value == 'A')
        {
            if (btnAprovar.style.visibility == 'hidden')
                interfaceAprovacao();
        }
        else
        {
            if (btnCheckout.style.visibility == 'hidden')                
                interfaceAprovacao();
        }

        btnEstado.disabled = true;

        if ((txtEst.value == 'P') || (txtEst.value == 'A'))
        {
            btnEstado.disabled = false;
        }

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF)) {
            txtObservacao.value = '';
        }
        else
        {
            if (dsoSacola.recordset['Observacao'].value != null)
                txtObservacao.value = dsoSacola.recordset['Observacao'].value;
            else
                txtObservacao.value = '';
        }

        if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF)) {
            txtSeuPedido.value = '';
        }
        else
        {
            if (dsoSacola.recordset['SeuPedido'].value != null)
                txtSeuPedido.value = dsoSacola.recordset['SeuPedido'].value;
            else
                txtSeuPedido.value = '';
        }
    }

    if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF))
    {
        txtPrevisaoEntrega.value = '';
    }
    else
    {
        if (dsoSacola.recordset['V_dtPrevisaoEntrega'].value != null)
            txtPrevisaoEntrega.value = dsoSacola.recordset['V_dtPrevisaoEntrega'].value;
        else
            txtPrevisaoEntrega.value = '';
    }

    if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF)) {
        txtComentarios.value = '';
    }
    else {
        if (dsoSacola.recordset['Observacoes'].value != null)
            txtComentarios.value = dsoSacola.recordset['Observacoes'].value;
        else
            txtComentarios.value = '';
    }

    // @@ atualizar interface aqui
    lockUnlockAllCmbs([divCtlBar1, divCtlBar2]);
    setupBtnsSacolaFromGridState();

    if (txtEst.value == 'P')
    {
        fg.Editable = false;
        selForma.disabled = true;
        selFinanciamento.disabled = true;
        btnExcluir.disabled = true;
        btnEsvaziar.disabled = true;
        selSacolasSalvas.disabled = true;
    }

    showExtFrame(window, true);

    window.focus();

    if (glb_LastCmbChanged != null) {
        if (!glb_LastCmbChanged.disabled) {
            try {
                glb_LastCmbChanged.focus();
            }
            catch (e) {
                ;
            }
        }
    }
    else {
        try {
            fg.focus();
        }
        catch (e) {
            ;
        }
    }
}

/********************************************************************
Trava/destrava todos os combos do div indicado
Parametros;
aDivRef         - array de divs a operar
********************************************************************/
function lockUnlockAllCmbs(aDivRef)
{
    var i, k, coll, elem;

    for (i = 0; i < aDivRef.length; i++)
    {
        coll = aDivRef[i].children;

        for (j = 0; j < coll.length; j++)
        {
            elem = coll.item(j);

            if ((elem.tagName).toUpperCase() == 'SELECT')
            {
                if ((elem.options.length > 0) && (elem.id != 'selParcelas'))
                    elem.disabled = false;
                else
                    elem.disabled = true;
            }
        }
    }

    //btnEmail.disabled = selContato.selectedIndex < 0;
    btnProposta.disabled = selUsuario.selectedIndex < 0;
    btnWEB.disabled = !((selPessoa.selectedIndex >= 0) && (selParceiro.selectedIndex >= 0));
    btnTelefonia.disabled = selUsuario.selectedIndex < 0;
    btnCelular.disabled = selUsuario.selectedIndex < 0;
    //selTransacaoID.disabled = true;

    if ((btnPesquisa.disabled) && (btnPesquisa.getAttribute('pesqMode', 1) == 1))
        comutaPesqMode();
}

/********************************************************************
Trava/destrava todos os botoes da barra de botoes em modo carrinho
********************************************************************/
function setupBtnsSacolaFromGridState()
{
    var bHasRowsInGrid = fg.Rows > 2;
    var nCurrLineInGrid = -1;
    var bAllCmbsSelected = true;

    bAllCmbsSelected = (selVendedor.selectedIndex >= 0) && (bAllCmbsSelected);
    bAllCmbsSelected = (selParceiro.selectedIndex >= 0) && (bAllCmbsSelected);
    bAllCmbsSelected = (selPessoa.selectedIndex >= 0) && (bAllCmbsSelected);
    bAllCmbsSelected = (selTransacaoID.selectedIndex >= 0) && (bAllCmbsSelected);

    if (selFinanciamento.options.length > 0)
        bAllCmbsSelected = (selFinanciamento.selectedIndex >= 0) && (bAllCmbsSelected);
    if (bHasRowsInGrid)
        nCurrLineInGrid = fg.Row;

    btnExcluir.disabled = !(nCurrLineInGrid > 1) && (txtEst.value == 'P');
    btnGravar.disabled = !((bAllCmbsSelected) && (glb_bAlteracaoPendente));
    btnFichaTecnica.disabled = !(nCurrLineInGrid > 1);
    btnCheckout.disabled = !(bHasRowsInGrid && bAllCmbsSelected && btnGravar.disabled && (!(txtEst.value == 'A')));
    btnSacolasSalvas.disabled = !(selParceiro.selectedIndex >= 0);
    btnSalvarSacola.disabled = !(bAllCmbsSelected && btnGravar.disabled);
    btnNotificar.disabled = !(bHasRowsInGrid && bAllCmbsSelected && btnGravar.disabled && (!(txtEst.value == 'C')));
    btnEsvaziar.disabled = !(nCurrLineInGrid > 1) && (txtEst.value == 'P');
    btnEstado.disabled = !(((nCurrLineInGrid > 1) && (txtEst.value == 'P')) || (txtEst.value == 'A'));
    btnLog.disabled = !(selParceiro.selectedIndex >= 0);
    txtVariacao.readOnly = !(bHasRowsInGrid && bAllCmbsSelected && btnGravar.disabled && (txtEst.value == 'P'));
    btnRefresh.disabled = !(selParceiro.selectedIndex >= 0);
}

/********************************************************************
Trava/destrava todos os botoes da barra de botoes em modo configurador
********************************************************************/
function setupBtnsConfiguradorFromGridState()
{
    var bFTMode = (btnFichaTecnica.getAttribute('FTMode', 1) == 1);
    var bRefMode = (btnReferencias.getAttribute('RefMode', 1) == 1);

    if (bFTMode)
    {
        btnFichaTecnica.disabled = false;
        btnComprar.disabled = true;
        btnReferencias.disabled = true;
        btnNotificar.disabled = true;
        btnSalvarSacola.disabled = true;
        selSacolasSalvas.disabled = true;
    }
    else if (bRefMode)
    {
        btnFichaTecnica.disabled = true;
        btnComprar.disabled = true;
        btnReferencias.disabled = false;
        btnNotificar.disabled = true;
        btnSalvarSacola.disabled = true;
        selSacolasSalvas.disabled = true;
    }
    else
    {
        // Se interface em modo de configurador
        if (btnPesquisa.getAttribute('pesqMode', 1) == 1)
        {
            btnFichaTecnica.disabled = (fg2.Row <= 0);
            btnReferencias.disabled = (fg2.Row <= 0);
            btnComprar.disabled = (fg2.Row <= 0);
            btnNotificar.disabled = (fg2.Row <= 0);
            btnSalvarSacola.disabled = (fg2.Row <= 0);
            selSacolasSalvas.disabled = (fg2.Row <= 0);
        }
    }
}

/********************************************************************
Usuario clicou Enter no campo txtPesquisa
********************************************************************/
function txtPesquisa_ondigit(ctrl) {
    if (event.keyCode == 13) {
        btnsListarProdutosOnClick();
    }
}

/********************************************************************
Usuario clicou Enter no campo txtPesquisa ou
digitou um caracter valido
********************************************************************/
function txtVariacao_ondigit(ctrl) {
    if (event.keyCode == 13)
        calcVariacao(ctrl);
}

/********************************************************************
Funcao auxiliar da funcao txtVariacao_ondigit(ctrl)
********************************************************************/
function calcVariacao(ctrl)
{
    var byPercent = false;
    var i;
    var nVariacao = 0;

    if ((ctrl.value == '') || (ctrl.value == '-'))
    {
        if (window.top.overflyGen.Alert('Valor inv�lido.') == 0)
            return null;

        window.focus();
        txtVariacao.focus();

        return null;
    }

    byPercent = parseFloat(ctrl.value) < 100;

    var nValorControle;
    var nPrecoInterno = 0;
    var nPrecoRevenda = 0;
    var nPrecoUnitario = 0;
    var nValorTotalFaturamento = 0;
    var nDiferenca = 0;
    var nQuantidade = 0;
    var nValorTotal = fg.ValueMatrix(1, getColIndexByColKey(fg, '_calc_PrecoTotal_11*'));

    nValorControle = parseFloat(ctrl.value);
    nValorControle = nValorControle;

    if (byPercent)
        nVariacao = nValorControle / 100;
    else
        nVariacao = (nValorControle / nValorTotal) - 1;

    if ((nVariacao <= -0.40) || (nVariacao >= 0.40))
    {
        if (window.top.overflyGen.Alert('Valor inv�lido.') == 0)
            return null;

        window.focus();
        fg.focus();
        txtVariacao.focus();
    }
    else
    {
        for (i = 2; i < fg.Rows; i++)
        {
            nQuantidade = fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly));
            nPrecoInterno = fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoInterno'));
            nPrecoInterno *= (1 + nVariacao);
            nPrecoInterno = roundNumber(nPrecoInterno, 2);
            nPrecoRevenda = fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoRevenda'));
            nPrecoRevenda *= (1 + nVariacao);
            nPrecoRevenda = roundNumber(nPrecoRevenda, 2);
            nPrecoUnitario = fg.ValueMatrix(i, getColIndexByColKey(fg, 'PrecoUnitario'));
            nPrecoUnitario *= (1 + nVariacao);
            nPrecoUnitario = roundNumber(nPrecoUnitario, 2);
            nValorTotalFaturamento += (nPrecoUnitario * nQuantidade);
            fg.TextMatrix(i, getColIndexByColKey(fg, 'PrecoInterno')) = nPrecoInterno;
            fg.TextMatrix(i, getColIndexByColKey(fg, 'PrecoRevenda')) = nPrecoRevenda;
            fg.TextMatrix(i, getColIndexByColKey(fg, 'PrecoUnitario')) = nPrecoUnitario;
            writeDataInDso(nPrecoInterno, nPrecoRevenda, nPrecoUnitario, getCellValueByColKey(fg, 'SacItemID', i));
        }

        setupBtnsSacolaFromGridState();

        if (!byPercent)
        {
            nDiferenca = nValorControle - nValorTotalFaturamento;
            fg.TextMatrix(fg.Rows - 1, getColIndexByColKey(fg, 'PrecoUnitario')) = nPrecoUnitario + nDiferenca;
            writeDataInDso(nPrecoInterno, nPrecoRevenda, nPrecoUnitario + nDiferenca, getCellValueByColKey(fg, 'SacItemID', fg.Rows - 1));
        }

        txtVariacao.value = '';
        window.focus();
        fg.focus();
        saveDataInGridSacola();
    }
}

/********************************************************************
Funcao auxiliar da funcao calcVariacao(ctrl)
********************************************************************/
function writeDataInDso(nPrecoInterno, nPrecoRevenda, nPrecoUnitario, nSacItemID)
{
    dsoGrid.recordset.moveFirst();
    dsoGrid.recordset.find(fg.ColKey(getColIndexByColKey(fg, 'SacItemID')), nSacItemID);

    if (!(dsoGrid.recordset.EOF))
    {
        dsoGrid.recordset['PrecoInterno'].value = nPrecoInterno;
        dsoGrid.recordset['PrecoRevenda'].value = nPrecoRevenda;
        dsoGrid.recordset['PrecoUnitario'].value = nPrecoUnitario;        
    }
}

/********************************************************************
Solicita dados ao servidor para o grid de produtos (fg)
Invocada em diversos pontos.
********************************************************************/
function fillGridData() 
{
    if (divSacolaSalvas.style.visibility == 'hidden')
        fillGridData_SacolaAtual();
    else
        lockControlsInModalWin(false);
}

function fillGridData_SacolaAtual()
{
    if (glb_TrolleyTimerInt != null)
    {
        window.clearInterval(glb_TrolleyTimerInt);
        glb_TrolleyTimerInt = null;
    }
    
    if (selParceiro.length > 0)
        glb_bCotador = (selParceiro.options(selParceiro.selectedIndex).getAttribute('OrigemSacolaID', 1) == '610' ? true : false);

    var nPessoaID = selPessoa.value;
    var nParceiroID = selParceiro.value;
    var nSacolaID = 0;
    var sSacola = '';
    var nEmpresaID = 0;
    var nEmpresaSUP = 0;
    var sAcrescimo = '';
    var nTaxaFinanceira = 0;
    var sDespesasUnitaria = 0;

    if (selParceiro.selectedIndex == -1)
    {
        fg.Rows = 1;
        lockUnlockAllCmbs([divCtlBar1, divCtlBar2]);
        setupBtnsSacolaFromGridState();
        glb_atertSacolaAtual = true;

        if (glb_atertSacolaAtual)
        {
            if (window.top.overflyGen.Alert('N�o existe Sacola Atual.') == 0) return null;
            glb_atertSacolaAtual = false;
        }

        alteraInterface(1);
    }
    else
    {
        glb_nSacolaDSOs = 2;
        nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);
        nEmpresaID = selParceiro.options.item(selParceiro.selectedIndex).getAttribute('EmpresaID', 1);

        if (nSacolaID == null)
        {
            if (glb_atertSacolaAtual)
            {
                if (window.top.overflyGen.Alert('N�o existe Sacola Atual.') == 0) return null;
                glb_atertSacolaAtual = false;
            }

            alteraInterface(1);
            return;
        }

        sSacola = selParceiro.options(selParceiro.selectedIndex).getAttribute('Sacola', 1);
        // secText('Sacola de Compras - ' + sSacola, 1);
        lockControlsInModalWin(true);
        fg.Rows = 1;
        setConnection(dsoGrid);

        dsoGrid.SQL = 'SELECT Sacola.*, dbo.fn_Pessoa_Fantasia(Sacola.EmpresaID, 0) AS Empresa, dbo.fn_Imposto_OpeIncidencia(25, Sacola.CFOPID, null) AS IncideST, ISNULL(Sacola.ICMSST , 0) AS ICMSSTUnitario,' +
                        'STR(dbo.fn_Produto_Estoque(Sacola.EmpresaID, Sacola.ProdutoID,-356,NULL,NULL,dbo.fn_Produto_EstoqueGlobal(Sacola.ProdutoID, ' +
						'Sacola.EmpresaID,' + nPessoaID + ',NULL),NULL, (CASE WHEN Sacola.LoteID > 0 THEN Sacola.LoteID ELSE NULL END)),6,0) AS Disp, ' +
						'dbo.fn_ListaPreco_SUP(Sacola.EmpresaID,Sacola.ProdutoID,' + nPessoaID + ',NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,' + nParceiroID + ', ' +
							'Sacola.PrecoInterno, ' + 'Sacola.PrecoRevenda, Sacola.FinalidadeID, Sacola.CFOPID, Sacola.Quantidade) AS ValorComissaoInterna,' +
						'dbo.fn_ListaPreco_SUP(Sacola.EmpresaID,Sacola.ProdutoID,' + nPessoaID + ',NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL,' + nParceiroID + ', ' +
							'Sacola.PrecoRevenda, Sacola.PrecoUnitario, Sacola.FinalidadeID, Sacola.CFOPID, Sacola.Quantidade) AS ValorComissaoRevenda, ' +
						'ISNULL(Sacola.Guia,SPACE(0)) AS Guia, ISNULL((Sacola.ICMSST * Sacola.Quantidade),0) AS ICMSST, ' +
						'(SELECT Finalidade.ItemMasculino ' +
							'FROM TiposAuxiliares_Itens Finalidade WITH(NOLOCK) ' +
							'WHERE Finalidade.TipoID = 418 AND Finalidade.ItemID = Sacola.FinalidadeID) AS Finalidade, ' +
                        '(SELECT aa.Fantasia ' +
							'FROM Pessoas aa WITH(NOLOCK) ' +
							'WHERE aa.PessoaID = Sacola.AprovadorID) AS Aprovador, ' +
                        '(SELECT Produtos.ConceitoID ' +
							'FROM Conceitos Produtos WITH(NOLOCK) ' +
							'WHERE Produtos.ConceitoID = Sacola.ProdutoID) AS ProdutoID, ' +
						'(SELECT TOP 1 Lote.LoteID FROM Lotes Lote WITH (NOLOCK) ' +
							' INNER JOIN dbo.Lotes_Produtos LotesProdutos WITH (NOLOCK) ON (Lote.LoteID = LotesProdutos.LoteID)' +
							' WHERE LotesProdutos.ProdutoID = Sacola.ProdutoID AND (Sacola.LoteID = Lote.LoteID) AND (Lote.EstadoID = 41)) AS Lote , ' +
						'(SELECT Estados.RecursoAbreviado ' +
							'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
								'INNER JOIN Recursos Estados WITH(NOLOCK) ON ProdutosEmpresa.EstadoID = Estados.RecursoID ' +
							'WHERE ProdutosEmpresa.ObjetoID = Sacola.ProdutoID AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
						'ProdutosEmpresa.SujeitoID = Sacola.EmpresaID) AS RecursoAbreviado, ' +
						'(SELECT dbo.fn_Pessoa_Fantasia(ProdutosEmpresa.ProprietarioID, 0) AS GP ' +
							'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
							'WHERE ProdutosEmpresa.ObjetoID = Sacola.ProdutoID AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
						        'ProdutosEmpresa.SujeitoID = Sacola.EmpresaID) AS GP, ' +
						'(SELECT dbo.fn_Pessoa_Fantasia(ProdutosEmpresa.AlternativoID, 0) AS Ass ' +
							'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
							'WHERE ProdutosEmpresa.ObjetoID = Sacola.ProdutoID AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
						        'ProdutosEmpresa.SujeitoID = Sacola.EmpresaID) AS Ass, ' +
						'(SELECT ProdutosEmpresa.ProprietarioID AS ProprietarioID ' +
							'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
							'WHERE ProdutosEmpresa.ObjetoID = Sacola.ProdutoID AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
						        'ProdutosEmpresa.SujeitoID = Sacola.EmpresaID) AS ProprietarioID, ' +
						'(SELECT ProdutosEmpresa.AlternativoID AS AlternativoID ' +
							'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
							'WHERE ProdutosEmpresa.ObjetoID = Sacola.ProdutoID AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
						        'ProdutosEmpresa.SujeitoID = Sacola.EmpresaID) AS AlternativoID, ' +
                        'dbo.fn_SacolasComprasItens_AssistenteProdutos(Sacola.SacItemID) AS Assistentes, ';


        if (glb_bCotador) {
            dsoGrid.SQL +=  '(SELECT dbo.fn_Cotador_Margem(MarcaID,2) AS MargemMinima ' +
		                        'FROM SacolasCompras_Itens Itens WITH(NOLOCK) ' +
		                            'INNER JOIN CorpProdutos WITH(NOLOCK) ON (CorpProdutos.CorpProdutoID = Itens.CorpProdutoID) ' +
		                        'WHERE Itens.SacItemID = Sacola.SacItemID) AS MargemMinima, ';
        }
        else {
            dsoGrid.SQL += '(SELECT ProdutosEmpresa.MargemMinima AS MargemMinima ' +
                'FROM RelacoesPesCon ProdutosEmpresa WITH(NOLOCK) ' +
                'WHERE ProdutosEmpresa.ObjetoID = Sacola.ProdutoID AND ProdutosEmpresa.TipoRelacaoID = 61 AND ' +
                    'ProdutosEmpresa.SujeitoID = Sacola.EmpresaID) AS MargemMinima, ';
        }


        dsoGrid.SQL += '(SELECT Produtos.Conceito ' +
							'FROM Conceitos Produtos WITH(NOLOCK) ' +
							'WHERE Produtos.ConceitoID = Sacola.ProdutoID) AS Produto, ';
        if (glb_bGetMC)
        {

            if (!glb_bCotador)
            dsoGrid.SQL += 'dbo.fn_Preco_MargemContribuicao(Sacola.EmpresaID, Sacola.ProdutoID, ' +
								'dbo.fn_Produto_Alternativo(Sacola.EmpresaID, ' + nPessoaID + ', ' + nParceiroID + ', 0), ' +
								'NULL, Sacola.PrecoUnitario, NULL, NULL, dbo.fn_Produto_Aliquota(Sacola.EmpresaID, ' +
								'ISNULL(dbo.fn_Produto_Alternativo(Sacola.EmpresaID, ' + nPessoaID + ', ' + nParceiroID + ', 0), Sacola.ProdutoID), ' +
								nPessoaID + ', NULL, NULL, NULL, NULL, Sacola.CFOPID), ' + glb_nMoedaID + ', GETDATE(), 1, NULL, 1, 1, 4, ' + nPessoaID + ', ' +
								'NULL, NULL, NULL, NULL, NULL, NULL, NULL, Sacola.FinalidadeID, Sacola.CFOPID, NULL, Sacola.LoteID, dbo.fn_Pessoa_Marketplace(' +
								nParceiroID + ', 3)) AS CustoBase, ' +

							'dbo.fn_Preco_MargemContribuicao(Sacola.EmpresaID, Sacola.ProdutoID, ' +
								'dbo.fn_Produto_Alternativo(Sacola.EmpresaID, ' + nPessoaID + ', ' + nParceiroID + ', 0), ' +
								'NULL, Sacola.PrecoUnitario,NULL,NULL, dbo.fn_Produto_Aliquota(Sacola.EmpresaID, ISNULL(dbo.fn_Produto_Alternativo(Sacola.EmpresaID, ' +
								nPessoaID + ', ' + nParceiroID + ', 0), Sacola.ProdutoID), ' + nPessoaID + ', NULL, NULL, NULL, NULL, Sacola.CFOPID), ' +
                        glb_nMoedaID + ', GETDATE(), 1, NULL, 1, 1, 3, ' + nPessoaID + ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, Sacola.FinalidadeID, Sacola.CFOPID, '+
                'NULL,Sacola.LoteID, dbo.fn_Pessoa_Marketplace(' + nParceiroID + ', 3)) AS PrecoBase, ';

            else
                dsoGrid.SQL += '(SELECT dbo.fn_Preco_MargemContribuicao(Sacola.EmpresaID, NULL, ' +
                        'dbo.fn_Cotador_ProdutoCompativel(Sacola.EmpresaID, dbo.fn_NCM_Descricao(NCMID, 1), TipoProdutoID, MarcaID, OrigemID), ' +
                        'NULL, Sacola.PrecoUnitario, NULL, NULL, dbo.fn_Produto_Aliquota(Sacola.EmpresaID, ' +
                        'ISNULL(dbo.fn_Produto_Alternativo(Sacola.EmpresaID, ' + nPessoaID + ', ' + nParceiroID + ', 0), Sacola.ProdutoID), ' +
                        nPessoaID + ', NULL, NULL, NULL, NULL, Sacola.CFOPID), ' + glb_nMoedaID + ', GETDATE(), 1, NULL, 1, 1, 4, ' + nPessoaID + ', ' +
                        'NULL, NULL, NULL, NULL, NULL, NULL, NULL, Sacola.FinalidadeID, Sacola.CFOPID, NULL, Sacola.LoteID, dbo.fn_Pessoa_Marketplace(' +
                        nParceiroID + ', 3)) FROM CorpProdutos WITH(NOLOCK) WHERE CorpProdutoID = Sacola.CorpProdutoID) AS CustoBase, ' +

                    '(SELECT dbo.fn_Preco_MargemContribuicao(Sacola.EmpresaID, NULL, ' +
                        'dbo.fn_Cotador_ProdutoCompativel(Sacola.EmpresaID, dbo.fn_NCM_Descricao(NCMID, 1), TipoProdutoID, MarcaID, OrigemID), ' +
                        'NULL, Sacola.PrecoUnitario,NULL,NULL, dbo.fn_Produto_Aliquota(Sacola.EmpresaID, ISNULL(dbo.fn_Produto_Alternativo(Sacola.EmpresaID, ' +
                        nPessoaID + ', ' + nParceiroID + ', 0), Sacola.ProdutoID), ' + nPessoaID + ', NULL, NULL, NULL, NULL, Sacola.CFOPID), ' +
								glb_nMoedaID + ', GETDATE(), 1, NULL, 1, 1, 3, ' + nPessoaID + ', NULL, NULL, NULL, NULL, NULL, NULL, NULL, Sacola.FinalidadeID, Sacola.CFOPID, ' +

                        'NULL,Sacola.LoteID, dbo.fn_Pessoa_Marketplace(' + nParceiroID + ', 3)) FROM CorpProdutos WITH(NOLOCK) WHERE CorpProdutoID = Sacola.CorpProdutoID) AS PrecoBase, ';


            dsoGrid.SQL += '0 AS ValorTotalImpostos, dbo.fn_SacolasCompras_Itens_Frete(Sacola.SacItemID, 1) AS ValorFreteTotalItem, ';

        }
        else
            dsoGrid.SQL += '1 AS CustoBase, 1 AS PrecoBase, 0 AS ValorTotalImpostos, 0 AS ValorFreteTotalItem, ';

        dsoGrid.SQL += '(SELECT Usuarios.Fantasia ' +
                            'FROM Pessoas Usuarios WITH(NOLOCK) ' +
                            'WHERE Usuarios.PessoaID=Sacola.UsuarioID) AS Usuario, ' +
                        '(SELECT Familias.ConceitoID ' +
                            'FROM Conceitos Produtos WITH(NOLOCK), Conceitos Familias WITH(NOLOCK) ' +
                            'WHERE Produtos.ConceitoID = Sacola.ProdutoID AND Produtos.ProdutoID=Familias.ConceitoID) AS FamiliaID, ' +
                        'CONVERT(BIT, dbo.fn_Imagem(2110, 21000, Sacola.ProdutoID, 2, NULL, NULL)) AS Foto ';

        if (glb_bCotador)
            dsoGrid.SQL += ', (GETDATE() + dbo.fn_Produto_PrevisaoEntregaAtual(NULL, NULL, Sacola.CorpProdutoID, NULL, NULL, NULL, NULL)) AS dtPrevisaoEntrega, ' +
                            '(SELECT Produto ' +
			                    'FROM CorpProdutos WITH(NOLOCK) ' +
			                    'WHERE CorpProdutoID = Sacola.CorpProdutoID) AS PartNumber, ' +
		                    '(SELECT SUBSTRING(bb.Observacao, CHARINDEX(\'-\', bb.Observacao, 0) + 1, LEN(bb.Observacao)) ' +
			                    'FROM CorpProdutos aa WITH(NOLOCK) ' +
                                    'INNER JOIN TiposAuxiliares_Itens bb WITH(NOLOCK) ON (bb.ItemID = aa.OrigemID) ' +
			                    'WHERE aa.CorpProdutoID = Sacola.CorpProdutoID) AS Origem, ' +
		                    '(SELECT dbo.fn_NCM_Descricao(NCMID, 1) ' +
			                    'FROM CorpProdutos WITH(NOLOCK) ' +
			                    'WHERE CorpProdutoID = Sacola.CorpProdutoID) AS NCM,' +
		                    '(SELECT PrecoLista ' +
			                    'FROM CorpProdutos WITH(NOLOCK)' +
			                    'WHERE CorpProdutoID = Sacola.CorpProdutoID) AS PrecoFob, ' +
		                    '(SELECT (PrecoLista * (1-(Desconto/100)))  ' +
                                'FROM CorpProdutos WITH(NOLOCK) ' +
                                'WHERE CorpProdutoID = Sacola.CorpProdutoID) AS PrecoFobDesconto, ' +
		                    'dbo.fn_DivideZero(Sacola.PrecoUnitario, ' +
							                    '((dbo.fn_Cotador_CustoReposicao(Sacola.EmpresaID, Sacola.CorpProdutoID, Sacola.Desconto, 0, Sacola.MoedaID)) + ' +
                                                'dbo.fn_Cotador_CustoReposicao_SubItens(Sacola.SacItemID, Sacola.MoedaID))) AS FIS, ' +
                            '0 AS IPI, ' +
                            '0 AS ISS, ' +
                            'dbo.fn_Cotador_Inconsistencias((SELECT Produto FROM CorpProdutos WITH (NOLOCK) WHERE CorpProdutoID = Sacola.CorpProdutoID),Sacola.Quantidade,Sacola.PrecoUnitario,NULL,\'1.0\',Sacola.Desconto,Sacola.MoedaID,Sacola.CorpProdutoID,Sacola.EmpresaID,NULL,3,0) AS Inconsistencia, ' +
                            'dbo.fn_Cotador_Inconsistencias((SELECT Produto FROM CorpProdutos WITH (NOLOCK) WHERE CorpProdutoID = Sacola.CorpProdutoID),Sacola.Quantidade,Sacola.PrecoUnitario,NULL,\'1.0\',Sacola.Desconto,Sacola.MoedaID,Sacola.CorpProdutoID,Sacola.EmpresaID,NULL,3,1) AS GravidadeInconsistencia ';
        else
            dsoGrid.SQL += ', (GETDATE() + dbo.fn_Produto_PrevisaoEntregaAtual(Sacola.ProdutoID, NULL, NULL, Sacola.EmpresaID, Sacola.Quantidade, Sacola.LoteID, NULL)) AS dtPrevisaoEntrega, ' +
                            'NULL AS PartNumber, ' +
		                    'NULL AS Origem, ' +
		                    'NULL AS NCM,' +
		                    '0 AS PrecoFob, ' +
                            '0 AS PrecoFobDesconto, ' +
                            '0 AS FIS, ' +
                            '0 AS IPI, ' +
                            '0 AS ISS, ' +
                            '\'\' AS Inconsistencia, ' +
                            '0 AS GravidadeInconsistencia ';

        dsoGrid.SQL += 'FROM SacolasCompras_Itens Sacola WITH(NOLOCK) ' +
                    'WHERE (Sacola.SacolaID = ' + nSacolaID + ') AND Sacola.PedItemID IS NULL ' +
                    'ORDER BY dbo.fn_Pessoa_Fantasia(Sacola.EmpresaID, 0), (SELECT TOP 1 a.ProdutoID FROM Conceitos a WITH(NOLOCK) WHERE (Sacola.ProdutoID = a.ConceitoID)), ' +
                        '(SELECT TOP 1 a.Conceito FROM Conceitos a WITH(NOLOCK) WHERE (Sacola.ProdutoID = a.ConceitoID)), Sacola.ProdutoID, Sacola.SacolaID';

        dsoGrid.ondatasetcomplete = fillGridData_DSC;
        dsoGrid.refresh();

        setConnection(dsoSacola);

        dsoSacola.SQL = "SELECT *, " +
                                "dbo.fn_Pessoa_Localidade(PessoaID, 2, NULL, NULL) AS UFID, " +
                                "dbo.fn_Recursos_Campos(EstadoID, 1) AS Estado, " +
                                "dbo.fn_Recursos_Campos(EstadoID, 2) AS EstadoTexto, " +
                                "(CONVERT(VARCHAR, dbo.fn_Sacola_PrevisaoEntregaAtual(SacolaID), " + DATE_SQL_PARAM + ") \+ SPACE(1) \+ " +
								"CONVERT(VARCHAR, dbo.fn_Sacola_PrevisaoEntregaAtual(SacolaID), 108)) AS V_dtPrevisaoEntrega, " +
								"dbo.fn_Pedido_Totais(-SacolaID, 5) AS PesoBruto, " +
                                "dbo.fn_TagValor(Observacoes,'PASTE','<',',',1) AS 'bPaste', " +
                                "dbo.fn_Cotador_Tipo_SacolaCompra(SacolaID, 2) AS 'ComPessoa', " +
                                "dbo.fn_Cotador_Tipo_SacolaCompra(SacolaID, 1) AS 'TemPessoa' " +
							"FROM SacolasCompras WITH(NOLOCK) WHERE SacolaID=" + nSacolaID;

        dsoSacola.ondatasetcomplete = fillGridData_DSC;
        dsoSacola.refresh();

        if (glb_bCotador) {
            txtPrime.style.visibility = '';
            lblPrime.style.visibility = '';

            setConnection(dsoPrime);
            dsoPrime.SQL = "SELECT TOP 1 " +
                                "(SELECT TOP 1 COUNT(aa.PedidoID) AS Primes " +
                                    "FROM Pedidos aa " +
                                        "INNER JOIN Pedidos_Itens bb WITH(NOLOCK) ON (bb.PedidoID = aa.PedidoID) " +
                                        "INNER JOIN Conceitos cc WITH(NOLOCK) ON (cc.ConceitoID = bb.ProdutoID) " +
                                    "WHERE aa.NumeroProjeto = a.Prime AND aa.EstadoID > 21 AND c.MarcaID = cc.MarcaID) AS PrimeTemPedido " +
                            "FROM SacolasCompras a WITH(NOLOCK) " +
                                "INNER JOIN SacolasCompras_Itens b WITH(NOLOCK) ON (a.SacolaID = b.SacolaID) INNER JOIN CorpProdutos c WITH(NOLOCK) ON (c.CorpProdutoID = b.CorpProdutoID) " +
                            "WHERE a.SacolaID=" + nSacolaID;
            dsoPrime.ondatasetcomplete = dsoPrime_DSC;
            dsoPrime.refresh();
        }
        else {
            txtPrime.style.visibility = 'hidden';
            lblPrime.style.visibility = 'hidden';
        }
    }
}
function dsoPrime_DSC()
{
    if ((dsoSacola.recordset.BOF) && (dsoSacola.recordset.EOF)) {
        txtPrime.value = '';
        txtPrime.disabled = false;
    }
    else {
        if (dsoSacola.recordset['Prime'].value != null)
            txtPrime.value = dsoSacola.recordset['Prime'].value;
        else
            txtPrime.value = '';

        txtPrime.disabled = (dsoPrime.recordset['PrimeTemPedido'].value > 0);
    }
}

/********************************************************************
Retorno do servidor da solicitacao de dados para o grid de produtos (fg)
********************************************************************/
function fillGridData_DSC()
{
    var nVermelhoFonte = 0x0000FF;
    var nVerdeFonte = 0x008200;
    var nAzul = 0xFF0000;

    var i;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_nSacolaDSOs--;

    if (glb_nSacolaDSOs > 0)
        return;

    glb_atertSacolaAtual = false;
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    fg.FontSize = '8';
    glb_GridIsBuilding = true;

    var aHoldCols;

    glb_sPasteReadOnly = (dsoSacola.recordset['bPaste'].value == 1 ? '*' : '');

    if (glb_bCotador)
    {
        if ((glb_nA1) && (glb_nA2))
            aHoldCols = [35, 36, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 52];
        else
            aHoldCols = [16, 17, 24, 25, 28, 29, 30, 35, 36, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 52];

        headerGrid(fg, ['Empresa', //0
                        'ID', //1
                        'PartNumber', //2
                        'E', //3
                        'OK', //4
                        'Enc', //5
                        'Finalidade', //6
                        'Quant', //7
                        'Lote', //8
                        'Disp', //9
                        'Previs�o Entrega', //10
                        'Desconto', //11
                        'Pre�o Int', //12
                        'Pre�o Rev', //13
                        'Pre�o Unit', //14
                        'Pre�o Total', //15
                        'MC', //16
                        'MC Min', //17
                        'CI', //18
                        'CR', //19
                        'IPI', //20
                        'ISS', //21
                        'Guia', //22
                        'ICMS-ST', //23
                        'Origem', //24
                        'NCM', //25
                        'Produto', //26
                        'CFOP', //27
                        'Pre�o Fob', //28
                        'Pre�o Fob Desconto', //29
                        'FIS', //30
                        'GP', //31
                        'Especialista', //32
                        'Aprovador', //33
                        'Aprova��o', //34
                        'CustoBase', //35
                        'PrecoBase', //36
                        'Data Pre�o', //37
                        'Val', //38
                        'Data           Hora     ', //39
                        'Usu�rio', //40
                        'FamiliaID', //41
                        'Foto', //42
                        'ValorTotalImpostos', //43
                        'ValorFreteTotalItem', //44
                        'SacolaID', //45
                        'IncideST', //46
                        'SacItemID', //47
                        'ICMSSTUnitario', //48
                        'Aprovado',//49
                        'CorpProdutoID', //50
                        'Inconsistencia', //51
                        'GravidadeInconsistencia'], aHoldCols); //52

        glb_aCelHint = [[0, 3, 'Estado do produto'],
                        [0, 4, 'Somente itens selecionados, ser�o inclu�dos no pedido.'],
                        [0, 5, 'Item encomenda'],
                        [0, 7, 'Quantidade'],
                        [0, 12, 'Pre�o interno'],
                        [0, 13, 'Pre�o revenda'],
                        [0, 14, 'Pre�o unit�rio'],
                        [0, 16, 'Margem de contribui��o'],
                        [0, 18, 'Comiss�o interna'],
                        [0, 19, 'Comiss�o revenda'],
                        [0, 38, 'Prazo de validade do item no Sacola'],
                        [0, 39, 'Data em que o �tem foi incluido no Sacola'],
                        [0, 40, 'Usu�rio que incluiu o �tem no Sacola']];

        fillGridMask(fg, dsoGrid, ['Empresa*', //0
                                   'ProdutoID*', //1
                                   'PartNumber*', //2
                                   'RecursoAbreviado*', //3
                                   'OK', //4
                                   'EhEncomenda' + glb_sPasteReadOnly, //5
                                   'Finalidade*', //6
                                   'Quantidade' + glb_sPasteReadOnly, //7
                                   'Lote*', //8
                                   'Disp*', //9
                                   'dtPrevisaoEntrega*', //10
                                   'Desconto' + glb_sPasteReadOnly, //11
                                   'PrecoInterno', //12
                                   'PrecoRevenda', //13
                                   'PrecoUnitario', //14
                                   '_calc_PrecoTotal_11*', //15
                                   'MargemContribuicao*', //16
                                   'MargemMinima*', //17
                                   'ValorComissaoInterna*', //18
                                   'ValorComissaoRevenda*', //19
                                   'IPI*', //20
                                   'ISS*', //21
                                   'Guia*', //22
                                   'ICMSST*', //23
                                   'Origem*', //24
                                   'NCM*', //25
                                   'Produto*', //26
                                   'CFOPID*', //27
                                   'PrecoFob*', //28
                                   'PrecoFobDesconto*', //29
                                   'FIS', //30
                                   'GP*', //31
                                   'Ass*', //32
                                   'Aprovador*', //33
                                   'dtAprovacao*', //34
                                   'CustoBase' + glb_sPasteReadOnly, //35
                                   'PrecoBase' + glb_sPasteReadOnly, //36
                                   'dtPreco' + glb_sPasteReadOnly, //37
                                   'Validade' + glb_sPasteReadOnly, //38
                                   'dtSacola*', //39
                                   'Usuario*', //40
                                   'FamiliaID' + glb_sPasteReadOnly, //41
                                   'Foto' + glb_sPasteReadOnly, //42
                                   'ValorTotalImpostos' + glb_sPasteReadOnly, //43
                                   'ValorFreteTotalItem' + glb_sPasteReadOnly, //44
                                   'SacolaID' + glb_sPasteReadOnly, //45
                                   'IncideST' + glb_sPasteReadOnly, //46
                                   'SacItemID', //47
                                   'ICMSSTUnitario' + glb_sPasteReadOnly, //48
                                   'Aprovado' + glb_sPasteReadOnly, //49
                                   'CorpProdutoID*', //50
                                   'Inconsistencia*', //51
                                   'GravidadeInconsistencia*'], //52
                                   ['', '', '', '', '', '', '', '', '999999', '999999', '99/99/9999', '9999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '9999.99', '9999.99', '999999999.99', '999999999.99', '9999.99', '999.99', '', '', '', '', '', '', '999999999.99', '999999999.99', '', '', '', '99/99/9999', '', '', '999999999.99', '99/99/9999', '99', '', '', '', '', '', '', '', '', '', '', '', '', '', '99'],
                                   ['', '', '', '', '', '', '', '', '######', '######', dTFormat, '####.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###0.00', '###0.00', '###,###,###.00', '###,###,###.00', '####.00', '###.00', '', '###,###,###.00', '', '', '', '', '###,###,###.00', '###,###,###.00', '###0.00', '', '', '', dTFormat, '', '', dTFormat, '', dTFormat + ' hh:mm:ss', '', '', '', '', '', '', '', '', '', '', '', '', '##']);

        alignColsInGrid(fg, [1, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 23, 25, 27, 28, 29, 30, 38]);

        fg.FrozenCols = 3;
    }
    else
    {
        if ((glb_nA1) && (glb_nA2))
            aHoldCols = [2, 11, 12, 21, 22, 25, 26, 27, 28, 29, 35, 36, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52];
        else
            aHoldCols = [2, 11, 12, 17, 18, 21, 22, 25, 26, 27, 28, 29, 35, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52];

        headerGrid(fg, ['Empresa', //0
                        'ID', //1
                        'PartNumber', //2
                        'E', //3
                        'Produto', //4
                        'OK', //5
                        'Enc', //6
                        'Finalidade', //7
                        'Quant', //8
                        'Lote', //9
                        'Disp', //10
                        'Previs�o Entrega', //11
                        'Desconto', //12
                        'Pre�o Int', //13
                        'Pre�o Rev', //14
                        'Pre�o Unit', //15
                        'Pre�o Total', //16
                        'MC', //17
                        'MC Min', //18
                        'CI', //19
                        'CR', //20
                        'IPI', //21
                        'ISS', //22
                        'Guia', //23
                        'ICMS-ST', //24
                        'Origem', //25
                        'NCM', //26
                        'FIS', //27
                        'Pre�o Fob', //28
                        'Pre�o Fob Desconto', //29
                        'CFOP', //30
                        'GP', //31
                        'Especialista', //32
                        'Aprovador', //33
                        'Aprova��o', //34
                        'CustoBase', //35
                        'PrecoBase', //36
                        'Data Pre�o', //37
                        'Val', //38
                        'Data           Hora     ', //39
                        'Usu�rio', //40
                        'FamiliaID', //41
                        'Foto', //42
                        'ValorTotalImpostos', //43
                        'ValorFreteTotalItem', //44
                        'SacolaID', //45
                        'IncideST', //46
                        'SacItemID', //47
                        'ICMSSTUnitario', //48
                        'Aprovado',//49
                        'CorpProdutoID', //50
                        'Inconsistencia', //51
                        'GravidadeInconsistencia'], aHoldCols); //52

        glb_aCelHint = [[0, 3, 'Estado do produto'],
                        [0, 5, 'Somente itens selecionados, ser�o inclu�dos no pedido.'],
                        [0, 6, 'Item encomenda'],
                        [0, 8, 'Quantidade'],
                        [0, 13, 'Pre�o interno'],
                        [0, 14, 'Pre�o revenda'],
                        [0, 15, 'Pre�o unit�rio'],
                        [0, 17, 'Margem de contribui��o'],
                        [0, 19, 'Comiss�o interna'],
                        [0, 20, 'Comiss�o revenda'],
                        [0, 38, 'Prazo de validade do item no Sacola'],
                        [0, 39, 'Data em que o �tem foi incluido no Sacola'],
                        [0, 40, 'Usu�rio que incluiu o �tem no Sacola']];

        fillGridMask(fg, dsoGrid, ['Empresa*', //0
                                   'ProdutoID*', //1
                                   'PartNumber*', //2
                                   'RecursoAbreviado*', //3
                                   'Produto*', //4
                                   'OK', //5
                                   'EhEncomenda' + glb_sPasteReadOnly, //6
                                   'Finalidade*', //7
                                   'Quantidade' + glb_sPasteReadOnly, //8
                                   'Lote*', //9
                                   'Disp*', //10
                                   'dtPrevisaoEntrega*', //11
                                   'Desconto' + glb_sPasteReadOnly, //12
                                   'PrecoInterno', //13
                                   'PrecoRevenda', //14
                                   'PrecoUnitario', //15
                                   '_calc_PrecoTotal_11*', //16
                                   'MargemContribuicao*', //17
                                   'MargemMinima*', //18
                                   'ValorComissaoInterna*', //19
                                   'ValorComissaoRevenda*', //20
                                   'IPI*', //21
                                   'ISS*', //22
                                   'Guia*', //23
                                   'ICMSST*', //24
                                   'Origem*', //25
                                   'NCM*', //26
                                   'FIS' + glb_sPasteReadOnly, //27
                                   'PrecoFob*', //28
                                   'PrecoFobDesconto*', //29
                                   'CFOPID*', //30
                                   'GP*', //31
                                   'Ass*', //32
                                   'Aprovador*', //33
                                   'dtAprovacao*', //34
                                   'CustoBase' + glb_sPasteReadOnly, //35
                                   'PrecoBase' + glb_sPasteReadOnly, //36
                                   'dtPreco' + glb_sPasteReadOnly, //37
                                   'Validade' + glb_sPasteReadOnly, //38
                                   'dtSacola*', //39
                                   'Usuario*', //40
                                   'FamiliaID' + glb_sPasteReadOnly, //41
                                   'Foto' + glb_sPasteReadOnly, //42
                                   'ValorTotalImpostos' + glb_sPasteReadOnly, //43
                                   'ValorFreteTotalItem' + glb_sPasteReadOnly, //44
                                   'SacolaID' + glb_sPasteReadOnly, //45
                                   'IncideST' + glb_sPasteReadOnly, //46
                                   'SacItemID', //47
                                   'ICMSSTUnitario' + glb_sPasteReadOnly, //48
                                   'Aprovado' + glb_sPasteReadOnly, //49
                                   'CorpProdutoID*', //50
                                   'Inconsistencia*', //51
                                   'GravidadeInconsistencia*'], //52
                                   ['', '', '', '', '', '', '', '', '', '999999', '999999', '99/99/9999', '9999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '9999.99', '9999.99', '999999999.99', '999999999.99', '9999.99', '999.99', '', '', '', '', '', '999999999.99', '999999999.99', '', '', '', '99/99/9999', '', '', '999999999.99', '99/99/9999', '99', '', '', '', '', '', '', '', '', '', '', '', '', '', '99'],
                                   ['', '', '', '', '', '', '', '', '', '######', '######', dTFormat, '####.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###0.00', '###0.00', '###,###,###.00', '###,###,###.00', '####.00', '###.00', '', '###,###,###.00', '', '', '###0.00', '###,###,###.00', '###,###,###.00', '', '', '', '', dTFormat, '', '', dTFormat, '', dTFormat + ' hh:mm:ss', '', '', '', '', '', '', '', '', '', '', '', '', '##']);

        alignColsInGrid(fg, [1, 8, 9, 10, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 24, 26, 27, 28, 29, 30, 38]);

        fg.FrozenCols = 5;
    }
    /*
    if ((fg.CellForeColor == nVermelhoFonte) || (fg.CellForeColor == nVerdeFonte) || (fg.CellForeColor == nAzul))
        fg.ForeColorSel = fg.CellForeColor;
    else
        fg.ForeColorSel = 0xFFFFFF;
    */

    verificaProprietarioAlternativo();

    

    fg.MergeCells = 1;
    fg.MergeCol(0) = true;

    for (i = 1; i < fg.Rows; i++)
    {
        calcTotItem(i);
    }

    //Chamada para a fun��o que pinta as fontes do grid
    // if ((glb_bGetMC) && ((glb_nA1) && (glb_nA2)))
    pintaFonte();

    gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'RecursoAbreviado*'), '######', 'C'],
														  [getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly), '######', 'S'],
														  [getColIndexByColKey(fg, '_calc_PrecoTotal_11*'), '###,###,###,###.00', 'S'],
														  [getColIndexByColKey(fg, 'ValorComissaoInterna*'), '###,###,###,###.00', 'S'],
														  [getColIndexByColKey(fg, 'ValorComissaoRevenda*'), '###,###,###,###.00', 'S']]);

    //Implementar uma forma de caulcular MC para itens sem ProdutoID

    //if (!glb_bCotador)
        calcMargemTotal();

    calcTotalColumns();

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);

    

    // Nao retirar o if abaixo, ela garante que o grid
    // so deixa editar a coluna 4
    if (fg.Rows > 1)
        fg.Col = getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly);

    // fg.Row = 1;

    // grid tem primeira linha de totalizacao
    if (glb_totalCols__)
    {
        if ((!fg.Editable) && (fg.Row == 1))
        {
            if (fg.Rows > 3)
            {
                fg.Row = 2;
            }
        }
    }

    if (fg.Rows > 1)
   {
       fg.Editable = true;

       if (selParceiro.options(selParceiro.selectedIndex).getAttribute('OrigemSacolaID', 1) == '610') {
           txtFonte.value = 'Cotador';
           glb_bCotador = true;
       }            
       else{
           txtFonte.value = 'Estoque';
           glb_bCotador = false;
       }
           
   }

    if (glb_aEmpresaData[1] != 130)
    {
        fg.ColHidden(getColIndexByColKey(fg, 'Guia*')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'ICMSST*')) = true;
    }
    else
    {
        fg.ColHidden(getColIndexByColKey(fg, 'Guia*')) = false;
        fg.ColHidden(getColIndexByColKey(fg, 'ICMSST*')) = false;
    }

    //Chamada para a fun��o que pinta as c�lulas do grid
    pintaCelulas();

    // Controla se o grid esta em construcao
    glb_GridIsBuilding = false;
    fg.Redraw = 2;

    if (glb_bSecondRefresh)
    {
        txtConceitoConcreto.value = '';
        txtMarca.value = '';
        txtModelo.value = '';
        txtDescricao.value = '';
        txtArgumentosVenda.value = '';
        fg2.Rows = 1;
    }

    if (glb_GridIsBuilding == false)
    {
        var j;
        var k = ((fg.rows) - 1);

        glb_Quantidades = new Array();

        for (j = 2; j <= k; j++)
        {
            glb_Quantidades[j - 2] = escape(fg.ValueMatrix(j, getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly)));
        }
    }

    if (glb_bCombos2)
    {
        glb_bCombos2 = false;
        glb_TrolleyTimerInt = window.setInterval('fillDSOCmbs2()', 30, 'JavaScript');
    }
    else
    {
        // trava destrava combos e ajusta estado dos botoes
        lockControlsInModalWin(false);
        lockUnlockAllCmbs([divCtlBar1, divCtlBar2]);
        setupBtnsSacolaFromGridState();
        drawButtonsFamilia();
        refreshLblConfiguradorMsg();
        window.focus();

        if (glb_LastCmbChanged != null)
        {
            if (!glb_LastCmbChanged.disabled)
            {
                try
                {
                    glb_LastCmbChanged.focus();
                }
                catch (e)
                {
                    ;
                }
            }
        }
        else
        {
            try
            {
                fg.focus();
                fg.row = 0;
            }
            catch (e)
            {
                ;
            }
        }
    }

    preencheSacolasSalvas();
    verificarOK();
}

/**********************************************************************************
Fun��o que pinta as celulas do Grid
***********************************************************************************/
function pintaCelulas()
{
    var bgCorLaranja = 0X60A4F4; 
    var bgCorVermelha = 0X7280FA;
    var bgCorAmarela = 0X8CE6F0;
    for (var contador = 1; contador < fg.Rows; contador++)
    {
        //Pinta as c�lulas de CFOP em que o imopsto ST incide na opera��o (CFOP)
        if (fg.TextMatrix(contador, getColIndexByColKey(fg, 'IncideST' + glb_sPasteReadOnly)) == 1)
            fg.Cell(6, contador, getColIndexByColKey(fg, 'CFOPID*'), contador, getColIndexByColKey(fg, 'CFOPID*')) = bgCorLaranja;

        //Pinta as c�lulas de produtos que tem Inconsistencia
        if (fg.TextMatrix(contador, getColIndexByColKey(fg, 'GravidadeInconsistencia*')) == 1)
            fg.Cell(6, contador, getColIndexByColKey(fg, 'PartNumber*'), contador, getColIndexByColKey(fg, 'PartNumber*')) = bgCorAmarela;

        if (fg.TextMatrix(contador, getColIndexByColKey(fg, 'GravidadeInconsistencia*')) == 2)
            fg.Cell(6, contador, getColIndexByColKey(fg, 'PartNumber*'), contador, getColIndexByColKey(fg, 'PartNumber*')) = bgCorVermelha;
    }
}

/**********************************************************************************************
Fun��o que verifica se h� �tens selecionados com e sem imposto ICMS-ST para gerar pedido

Regra: H� Estados brasileiros que exigem notas fiscais separadas para itens com ST de itens sem ST
**********************************************************************************************/
function verificaRegraImpostoST() {
    var nQtdeItensComST = 0;
    var nQtdeItensSemST = 0;
    var retorno = 'V�lida';

    //contador inicia em 2 para descontar as linhas de cabe�alho e de totais
    for (var contador = 2; contador < fg.Rows; contador++) {
        //Se item selecionado possui ST
        if ((fg.TextMatrix(contador, getColIndexByColKey(fg, 'IncideST' + glb_sPasteReadOnly)) == 1) && (fg.TextMatrix(contador, getColIndexByColKey(fg, 'OK')) == 1))
            nQtdeItensComST++;

        //Se item selecionado n�o possui ST
        if ((fg.TextMatrix(contador, getColIndexByColKey(fg, 'IncideST' + glb_sPasteReadOnly)) == 0) && (fg.TextMatrix(contador, getColIndexByColKey(fg, 'OK')) == 1))
            nQtdeItensSemST++;
    }

    if (nQtdeItensComST > 0 && nQtdeItensSemST > 0)
        retorno = 'Inv�lida';

    return retorno;
}

/********************************************************************
Calcula margem total dos produtos no grid do carrinho
********************************************************************/
function calcMargemTotal()
{
    var i;
    var nPrecoRevenda = 0;
    var nTotalContribuicao = 0;
    var nTotalCustoBase = 0;
    var nTotalPrecoBase = 0;
    var nTotalPrecoBaseLiquido = 0;
    var nQuantidade = 0;
    var nContribuicao = 0;
    var nCustoBase = 0;
    var nPrecoBase = 0;
    var nMargemContribuicao = 0;
    var nValorTotalOK = 0;
    var nValorTotalCustoBaseOK = 0;
    var nValorTotalPrecoBaseLiquidoOK = 0;
    var printTotal = true;
    var nValorFinanceiro = 0;
    var nValorFrete = 0;
    var nValorComissaoInterna = 0;
    var nValorComissaoRevenda = 0;
    var nFinanciamentoID = 0;
    var nEmpresaSUP = 0;
    var sAcrescimo = '';
    var nTaxaFinanceira = 0;
    var nPercentualFinanceiro = 0;
    var nMargemContribuicaoSacola = 0;

    txtMargemContribuicao.value = '';

    if (fg.Rows > 1)
        fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemContribuicao*')) = '';

    if ((glb_bGetMC) && (btnGravar.disabled))
    {
        // Financiamento
        nFinanciamentoID = dsoSacola.recordset.fields['FinanciamentoID'].value;

        if (nFinanciamentoID == null)
        {
            nFinanciamentoID = 2;

            dsoCombos2.recordset.moveFirst();
            dsoCombos2.recordset.setFilter('Indice = 4 AND RegistroID = ' + nFinanciamentoID);

            nEmpresaSUP = dsoCombos2.recordset['EmpresaSUP'].value;
            sAcrescimo = txtAcrescimo.value;
            nTaxaFinanceira = parseFloat(replaceStr(replaceStr(sAcrescimo, '%', ''), ',', '.'));
        }
        else
        {
            dsoCombos2.recordset.moveFirst();
            dsoCombos2.recordset.setFilter('Indice = 4 AND RegistroID = ' + nFinanciamentoID);
            nEmpresaSUP = dsoCombos2.recordset['EmpresaSUP'].value;
            nTaxaFinanceira = dsoCombos2.recordset['Variacao'].value;
            dsoCombos2.recordset.setFilter('');
        }

        // Verifica se � parceiro marketplace
        dsoCombos2.recordset.setFilter('Indice = 10');
        var marketplace = 0;

        while (!dsoCombos2.recordset.EOF)
        {
            marketplace = 1;
            dsoCombos2.recordset.moveNext();
        }

        dsoCombos2.recordset.setFilter('');
        nTaxaFinanceira = (nTaxaFinanceira + nEmpresaSUP) / 100;

        if (marketplace == 1)
            nTaxaFinanceira = 0;

        nPercentualFinanceiro = (1 - (1 / (1 + nTaxaFinanceira)));

        if (fg.Rows < 2)
            return null;

        for (i = 2; i < fg.Rows; i++)
        {
            nPrecoRevenda = parseFloat(replaceStr(getCellValueByColKey(fg, 'PrecoRevenda', i), ',', '.'));;
            nQuantidade = parseInt(replaceStr(getCellValueByColKey(fg, 'Quantidade' + glb_sPasteReadOnly, i), ',', '.'), 10);
            nCustoBase = parseFloat(replaceStr(getCellValueByColKey(fg, 'CustoBase' + glb_sPasteReadOnly, i), ',', '.'));
            nPrecoBase = parseFloat(replaceStr(getCellValueByColKey(fg, 'PrecoBase' + glb_sPasteReadOnly, i), ',', '.'));
            nValorComissaoInterna = parseFloat(replaceStr(getCellValueByColKey(fg, 'ValorComissaoInterna*', i), ',', '.'));
            nValorComissaoRevenda = parseFloat(replaceStr(getCellValueByColKey(fg, 'ValorComissaoRevenda*', i), ',', '.'));
            nValorFrete = parseFloat(replaceStr(getCellValueByColKey(fg, 'ValorFreteTotalItem' + glb_sPasteReadOnly, i), ',', '.'));
            nTotalPrecoBase = roundNumber(nQuantidade * nPrecoBase, 2);

            //Desconsidera o valor sup para calculo do valor financeiro
            nValorFinanceiro = roundNumber((nTotalPrecoBase - (nValorComissaoRevenda + nValorComissaoInterna)) * nPercentualFinanceiro, 2);
            nTotalPrecoBaseLiquido += nTotalPrecoBase - nValorFinanceiro - (nValorComissaoRevenda + nValorComissaoInterna) - nValorFrete;
            nTotalCustoBase += roundNumber(nQuantidade * nCustoBase, 2);

            if (fg.TextMatrix(i, getColIndexByColKey(fg, 'MargemContribuicao*')) == '')
                printTotal = false;
        }

        nTotalContribuicao = nTotalPrecoBaseLiquido - nTotalCustoBase;
        nMargemContribuicao = roundNumber((nTotalContribuicao / nTotalPrecoBaseLiquido) * 100, 2);

        if ((nTotalContribuicao < 0) && (nTotalPrecoBaseLiquido < 0))
            nMargemContribuicao = nMargemContribuicao * (-1);

        nMargemContribuicaoSacola = dsoSacola.recordset['MargemContribuicao'].value;

        txtMargemContribuicao.value = '';

        if (!fg.ColHidden(getColIndexByColKey(fg, 'MargemContribuicao*')))
            txtMargemContribuicao.value = padNumReturningStr(nMargemContribuicaoSacola, 2);

        if (printTotal)
            fg.TextMatrix(1, getColIndexByColKey(fg, 'MargemContribuicao*')) = nMargemContribuicaoSacola;
    }
}

/********************************************************************
Calcula linha de totais do grid
********************************************************************/
function calcTotItem(nLine, nLineException)
{
    var nQtd;
    var lIsEditing = (nLine == null);
    nLine = (nLine == null ? fg.Row : nLine);
    var nPrecoRevenda = 0;
    var nCustoBase = 0;
    var nPrecoBase = 0;
    var nEmpresaSUP = 0;
    var sAcrescimo = 0;
    var nTaxaFinanceira = 0;
    var nPercentualFinanceiro = 0;
    var nValorComissaoInterna = 0;
    var nValorComissaoRevenda = 0;
    var nValorFrete = 0;
    var nPrecoBaseTotal = 0;
    var nValorFinanceiro = 0;
    var nCustoBaseTotal = 0;
    var nPrecoBaseLiquidoTotal = 0;
    var nPrecoUnitario = 0;
    var nPrecoInterno = 0;
    var nFinanciamentoID = 0;

    if (nLine > 0)
    {
        nPrecoUnitario = parseFloat(replaceStr(fg.TextMatrix(nLine, getColIndexByColKey(fg, 'PrecoUnitario')), ',', '.'));
        nQtd = fg.ValueMatrix(nLine, getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly));

        if ((!isNaN(nQtd)) && (!isNaN(nPrecoUnitario)))
            fg.TextMatrix(nLine, getColIndexByColKey(fg, '_calc_PrecoTotal_11*')) = roundNumber(nQtd * nPrecoUnitario, 2);
        else
            fg.TextMatrix(nLine, getColIndexByColKey(fg, '_calc_PrecoTotal_11*')) = '';

        if ((nLineException != nLine) && (btnGravar.disabled))
        {
            if ((!((lIsEditing) && (fg.TextMatrix(nLine, getColIndexByColKey(fg, 'MargemContribuicao*')) == ''))) && (glb_bGetMC == true))
            {
                nPrecoInterno = parseFloat(replaceStr(fg.TextMatrix(nLine, getColIndexByColKey(fg, 'PrecoInterno')), ',', '.'));
                nPrecoRevenda = parseFloat(replaceStr(fg.TextMatrix(nLine, getColIndexByColKey(fg, 'PrecoRevenda')), ',', '.'));

                nCustoBase = parseFloat(replaceStr(fg.TextMatrix(nLine, getColIndexByColKey(fg, 'CustoBase' + glb_sPasteReadOnly)), ',', '.'));

                nPrecoBase = parseFloat(replaceStr(fg.TextMatrix(nLine, getColIndexByColKey(fg, 'PrecoBase' + glb_sPasteReadOnly)), ',', '.'));

                // Financiamento
                nFinanciamentoID = dsoSacola.recordset.fields['FinanciamentoID'].value;

                if (nFinanciamentoID == null)
                {
                    nFinanciamentoID = 2;

                    dsoCombos2.recordset.moveFirst();
                    dsoCombos2.recordset.setFilter('Indice = 4 AND RegistroID = ' + nFinanciamentoID);

                    nEmpresaSUP = dsoCombos2.recordset['EmpresaSUP'].value;
                    sAcrescimo = txtAcrescimo.value;
                    nTaxaFinanceira = parseFloat(replaceStr(replaceStr(sAcrescimo, '%', ''), ',', '.'));
                }
                else
                {
                    dsoCombos2.recordset.moveFirst();
                    dsoCombos2.recordset.setFilter('Indice = 4 AND RegistroID = ' + nFinanciamentoID);

                    nEmpresaSUP = dsoCombos2.recordset['EmpresaSUP'].value;
                    nTaxaFinanceira = dsoCombos2.recordset['Variacao'].value;

                    dsoCombos2.recordset.setFilter('');
                }

                nTaxaFinanceira = (nTaxaFinanceira + nEmpresaSUP) / 100;

                nPercentualFinanceiro = (1 - (1 / (1 + nTaxaFinanceira)));

                nValorComissaoInterna = parseFloat(replaceStr(getCellValueByColKey(fg, 'ValorComissaoInterna*', nLine), ',', '.'));

                nValorComissaoRevenda = parseFloat(replaceStr(getCellValueByColKey(fg, 'ValorComissaoRevenda*', nLine), ',', '.'));

                nValorFrete = parseFloat(replaceStr(getCellValueByColKey(fg, 'ValorFreteTotalItem' + glb_sPasteReadOnly, nLine), ',', '.'));

                nCustoBaseTotal = nCustoBase * nQtd;

                nPrecoBaseTotal = nPrecoBase * nQtd;

                //Desconsidera o valor sup para calculo do valor financeiro
                nValorFinanceiro = (nPrecoBaseTotal - (nValorComissaoRevenda + nValorComissaoInterna)) * nPercentualFinanceiro;

                nPrecoBaseLiquidoTotal = nPrecoBaseTotal - nValorFinanceiro - nValorFrete - (nValorComissaoRevenda + nValorComissaoInterna);

                nContribuicao = nPrecoBaseLiquidoTotal - nCustoBaseTotal;
                /*
                if ((isNaN(nPrecoRevenda)) || (isNaN(nPrecoUnitario)) || (nPrecoInterno == 0) || (nPrecoRevenda == 0) || (nPrecoUnitario == 0))
                    fg.TextMatrix(nLine, getColIndexByColKey(fg, '_calc_MC_6*')) = '';
                else if ((!isNaN(nContribuicao)) && (!isNaN(nPrecoBaseLiquidoTotal)) && (nPrecoBaseLiquidoTotal > 0))
                    fg.TextMatrix(nLine, getColIndexByColKey(fg, '_calc_MC_6*')) = roundNumber((parseFloat(nContribuicao) / parseFloat(nPrecoBaseLiquidoTotal)) * 100, 2);
                else
                    fg.TextMatrix(nLine, getColIndexByColKey(fg, '_calc_MC_6*')) = '';
                */
            }
        }
    }

    if (!lIsEditing) {
        ;
    }
}

/********************************************************************
Salva no servidor, as linhas alteradas no grid do carrinho
********************************************************************/
function saveDataInGridSacola()
{
    if (txtPrevisaoEntrega.value != '')
    {
        if (!chkDataEx(txtPrevisaoEntrega.value))
        {
            if (window.top.overflyGen.Alert('Data inv�lida.') == 0)
                return null;

            return null;
        }
    }

    lockControlsInModalWin(true);
    glb_nSacolaDSOs = 2;

    /*
    if (selUsuario.selectedIndex >= 0)
    {
        if (selUsuario.value > 0) dsoSacola.recordset['UsuarioID'].value = parseInt(selUsuario.value, 10);
    }
    */

    dsoSacola.recordset['TransacaoID'].value = (selTransacaoID.selectedIndex >= 0 ? parseInt(selTransacaoID.value, 10) : null);
    dsoSacola.recordset['FormaPagamentoID'].value = (selForma.selectedIndex >= 0 ? parseInt(selForma.value, 10) : null);
    dsoSacola.recordset['FinanciamentoID'].value = (selFinanciamento.selectedIndex >= 0 ? parseInt(selFinanciamento.value, 10) : null);
    dsoSacola.recordset['Observacao'].value = (txtObservacao.value != '' ? txtObservacao.value : null);
    dsoSacola.recordset['SeuPedido'].value = (txtSeuPedido.value != '' ? txtSeuPedido.value : null);
    dsoSacola.recordset['Sacola'].value = (txtSacola.value != '' ? txtSacola.value : null);
    dsoSacola.recordset['Observacoes'].value = (txtComentarios.value != '' ? txtComentarios.value : null);
    dsoSacola.recordset['Prime'].value = (txtPrime.value != '' ? txtPrime.value : null);

    dsoSacola.ondatasetcomplete = saveDataInGridSacola_DSC;
    dsoSacola.SubmitChanges();

    dsoGrid.ondatasetcomplete = saveDataInGridSacola_DSC;
    dsoGrid.SubmitChanges();
}

/********************************************************************
Retorno do servidor, apos salvar as linhas alteradas no grid
do carrinho
********************************************************************/
function saveDataInGridSacola_DSC()
{
    glb_nSacolaDSOs--;

    if (glb_nSacolaDSOs > 0)
        return;

    lockControlsInModalWin(false);

    if (glb_bVoltarCheckout)
    {
        glb_bVoltarCheckout = false;
        checkoutVoltar();
        return null;
    }

    if (glb_bFinalizaVenda)
    {
        glb_bFinalizaVenda = false;
        btns_onclick(btnFinalizarVenda);
        return null;
    }

    glb_bGetMC = false;
    glb_bCombos2 = true;

    if (glb_TransferenciaItem)
    {
        glb_TransferenciaItem = false;
        var sMsg = (glb_nItensTransferidos + ' itens transferidos para a sacola ' + selSacolasSalvas.options(selSacolasSalvas.selectedIndex).text + '.\nDeseja ver a sacola?');

        glb_nItensTransferidos = 0;

        if (sMsg != '')
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);

            if (_retMsg == 1)
            {
                glb_TrolleyTimerInt = window.setInterval('alteraInterface(1)', 30, 'JavaScript');
                return null;
            }

            selSacolasSalvas.value = 0;
            glb_SacolaTransferenciaID = 0;
        }
    }

    if (glb_deleteItem)
    {
        glb_TrolleyTimerInt = window.setInterval('fillDSOCmbs1(1)', 30, 'JavaScript');
        glb_deleteItem = false;
    }        
    else
        glb_TrolleyTimerInt = window.setInterval('fillGridData()', 30, 'JavaScript');
}

/********************************************************************
Deleta linha de registro no grid do carrinho e tambem no servidor
********************************************************************/
function deleteDataInGridSacola()
{
    glb_deleteItem = true;

    dsoGrid.recordset.moveFirst();
    dsoGrid.recordset.find(fg.ColKey(getColIndexByColKey(fg, 'SacItemID')), getCellValueByColKey(fg, 'SacItemID', fg.Row));

    if (!(dsoGrid.recordset.EOF))
    {
        dsoGrid.recordset.Delete();
        saveDataInGridSacola();
    }
}

/********************************************************************
Deleta corrente lista de registros no grid do carrinho e tambem
no servidor
********************************************************************/
function emptyListTrolley()
{
    if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
        return null;

    dsoGrid.recordset.moveFirst();

    while (true)
    {
        if (dsoGrid.recordset.BOF && dsoGrid.recordset.EOF)
            break;

        dsoGrid.recordset.Delete();
        dsoGrid.recordset.moveFirst();
    }

    dsoGrid.ondatasetcomplete = dsoGrid_DSC;
    dsoGrid.SubmitChanges();
}

function dsoGrid_DSC()
{
    glb_bGetMC = true;
    glb_bCombos2 = true;

    glb_TrolleyTimerInt = window.setInterval('fillDSOCmbs1(1)', 30, 'JavaScript');
}

/********************************************************************
Ida ao servidor para gerar o pedido em funcao do grid do carrinho
********************************************************************/
function gerarPedido()
{
    if (glb_TrolleyTimerInt != null)
    {
        window.clearInterval(glb_TrolleyTimerInt);
        glb_TrolleyTimerInt = null;
    }

    var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);
    var bAllCmbsSelected = true;

    bAllCmbsSelected = (selVendedor.selectedIndex >= 0) && (bAllCmbsSelected);
    bAllCmbsSelected = (selParceiro.selectedIndex >= 0) && (bAllCmbsSelected);
    bAllCmbsSelected = (selPessoa.selectedIndex >= 0) && (bAllCmbsSelected);
    bAllCmbsSelected = (selTransacaoID.selectedIndex >= 0) && (bAllCmbsSelected);
    bAllCmbsSelected = (selForma.selectedIndex >= 0) && (bAllCmbsSelected);

    if (selFinanciamento.options.length > 0)
        bAllCmbsSelected = (selFinanciamento.selectedIndex >= 0) && (bAllCmbsSelected);

    // So prossegue se todos os combos estao selecionados
    if (!bAllCmbsSelected)
        return null;

    lockControlsInModalWin(true);

    var strPars = new String();
    strPars = '';
    strPars += '?nSacolaID=' + escape(nSacolaID);

    dsoGerarPedido.URL = SYS_ASPURLROOT + '/serversidegenEx/trolleypedido.aspx' + strPars;
    dsoGerarPedido.ondatasetcomplete = gerarPedido_DSC;
    dsoGerarPedido.refresh();
}

/********************************************************************
Retorno do servidor, apos gerar o pedido
********************************************************************/
function gerarPedido_DSC()
{
    var _retMsg = null;
    var nRetorno = null;
    var sHTMLSupID = null;

    lockControlsInModalWin(false);

    if (!(dsoGerarPedido.recordset.BOF && dsoGerarPedido.recordset.EOF))
    {
        // Nao gerou o pedido
        if (dsoGerarPedido.recordset['Resultado'].value != null)
        {
            if ((dsoGerarPedido.recordset.fields['Resultado'].value).substr(0, 1) == '#' &&
				(dsoGerarPedido.recordset.fields['Resultado'].value).substr((dsoGerarPedido.recordset.fields['Resultado'].value).length - 1, 1) == '#' &&
				(glb_bTemProducao == false)) {
                if (!glb_bConfirmPedido) {
                    var _retConf = window.top.overflyGen.Confirm(dsoGerarPedido.recordset.fields['Resultado'].value);

                    if (_retConf == 0)
                        return null;

                    else if (_retConf == 1) {
                        glb_bConfirmPedido = true;
                        glb_TrolleyTimerInt = window.setInterval('gerarPedido()', 30, 'JavaScript');
                        return null;
                    }
                    else {
                        glb_bGetMC = false;
                        glb_bCombos2 = true;
                        fillGridData();
                        return null;
                    }
                }
                else
                    glb_bConfirmPedido == false;
            }
            else {
                if (window.top.overflyGen.Alert(dsoGerarPedido.recordset['Resultado'].value) == 0) return null;
                glb_bGetMC = false;
                glb_bCombos2 = true;
                fillGridData();
                return null;
            }
        }

        if (dsoGerarPedido.recordset.fields["PedidoID"].value != null)
        {
            if (dsoGerarPedido.recordset.fields["PedidoID"].value > 0)
            {
                if (selParceiro.selectedIndex >= 0)
                    nEmpresaID = selParceiro.options.item(selParceiro.selectedIndex).getAttribute('EmpresaID', 1);
                else
                    nEmpresaID = glb_nEmpresaID;

                _retMsg = window.top.overflyGen.Confirm('Gerado o Pedido: ' + dsoGerarPedido.recordset.fields["PedidoID"].value + '\n' + 'Detalhar Pedido?');

                if (_retMsg == 0)
                    return null;
                else if (_retMsg == 1)
                {
                    nRetorno = dsoGerarPedido.recordset.fields["PedidoID"].value;
                    sHTMLSupID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getHtmlId()');
                    sendJSCarrier(sHTMLSupID, 'SHOWPEDIDO', new Array(nEmpresaID, nRetorno));
                }

                // Refresh
                glb_bGetMC = false;
                glb_bCombos2 = true;

                // Ze - 25/03/2009
                glb_TrolleyTimerInt = window.setInterval('fillGridData()', 100, 'JavaScript');
            }
        }
    }
}

/********************************************************************
Cria os botoes dinamicos das familias
********************************************************************/
function drawButtonsFamilia() {
    var i;
    var coll = window.document.getElementsByTagName('INPUT');
    var nFamiliaID;
    var bHasSelected = false;
    var bAceitaMaisProdutos;
    var bBloqueiaMicroSemEste;

    for (i = 0; i < coll.length; i++) {
        if (coll.item(i).getAttribute('AceitaMaisProdutos', 1) != null) {
            nFamiliaID = coll.item(i).id;
            bHasSelected = (seekInGrid(fg, nFamiliaID, 'FamiliaID' + glb_sPasteReadOnly) >= 0);
            bAceitaMaisProdutos = (coll.item(i).getAttribute('AceitaMaisProdutos', 1) == 1);
            bBloqueiaMicroSemEste = (coll.item(i).getAttribute('BloqueiaMicroSemEste', 1) == 1);

            if (!chkProdutosCompativeis.checked) {
                coll.item(i).disabled = false;
                coll.item(i).style.color = 'black';
            }
            else if (bHasSelected) {
                if (bAceitaMaisProdutos) {
                    coll.item(i).disabled = false;
                    coll.item(i).style.color = 'green';
                } else {
                    coll.item(i).disabled = true;
                    coll.item(i).style.color = 'black';
                }
            }
            else {
                if (bBloqueiaMicroSemEste) {
                    coll.item(i).disabled = false;
                    coll.item(i).style.color = 'red';
                }
                else {
                    coll.item(i).disabled = false;
                    coll.item(i).style.color = 'black';
                }
            }
        }
    }

    if ((document.getElementById('2001') != null) && (document.getElementById('2008') != null)) {
        document.getElementById('2001').disabled = (document.getElementById('2001').disabled || document.getElementById('2008').disabled);
        document.getElementById('2008').disabled = (document.getElementById('2001').disabled || document.getElementById('2008').disabled);
    }
}

/********************************************************************
Procura nas linhas do grid por um valor, dado o ColKey da coluna
********************************************************************/
function seekInGrid(grid, sValue, sColKey) {
    var i, retVal = -1;

    if (grid.Rows <= 1)
        return retVal;

    for (i = 1; i < grid.Rows; i++) {
        if (getCellValueByColKey(grid, sColKey, i) == sValue) {
            retVal = i;
            break;
        }
    }

    return retVal;
}

/********************************************************************
Imagem carregou com sucesso
********************************************************************/
function img_onload() {
    getFotoInServer_Finish(false);
}

/********************************************************************
Imagem nao foi carregada
********************************************************************/
function img_onerror() {
    getFotoInServer_Finish(true);
}

/********************************************************************
Retorno do servidor com a foto
********************************************************************/
function getFotoInServer_Finish(bError)
{
    if (bError)
    {
        fg2.style.width = parseInt(divFG2.currentStyle.width, 10) - 1;

        with (img_Imagem.style)
        {
            left = 0;
            top = 0;
            width = 0;
            height = 0;
            visibility = 'hidden';
        }
    }
    else
    {
        fg2.style.width = 523;

        with (img_Imagem.style) {
            left = 526;
            top = 16;
            width = 213;
            height = 174;
            visibility = 'visible';
        }
    }
}

// EVENTOS DE GRID **************************************************

function js_fg_ModalTrolleyAfterRowColChange(grid, oldRow, oldCol, newRow, newCol)
{
    if (glb_GridIsBuilding)
        return null;

    if (divSacolaSalvas.style.visibility == 'inherit')
    {
        if (oldRow != newRow)
        {
            nSacolaID = fg.TextMatrix(newRow, getColIndexByColKey(fg, 'SacolaID*'));

            if (nSacolaID > 0)
                sacolasSalvasItens(nSacolaID);
        }
    }
    else
    {
        // grid tem primeira linha de totalizacao
        if (glb_totalCols__)
        {
            if ((!grid.Editable) && (newRow == 1))
            {
                if (grid.Rows > 2)
                    grid.Row = 2;
            }
            // If abaixo colocado para grids editaveis
            if ((grid.Editable) && (newRow == 1))
            {
                if (grid.Rows > 2)
                    grid.Row = 2;
            }
        }

        if (glb_bHistoricoMC)
            carregaGridHistorico();
        else if ((grid.Editable) && (fg2.currentStyle.visibility != 'hidden'))
        {
            if (oldRow != newRow)
            {
                comutaFichaTecnicaMode();
            }
        }
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_ModalTrolleyBeforeMouseDown(grid, Button, Shift, X, Y, Cancel) {
    if (grid.Rows <= 1)
        return true;

    glb_LastCmbChanged = null;

    if ((X > (grid.ColWidth(0) + grid.ColWidth(1))) && (X < (grid.ColWidth(0) + grid.ColWidth(1) + grid.ColWidth(2))) && (Y < grid.RowHeight(0)))
        glb_LocalClickedIsReadOnly = false;
    else
        glb_LocalClickedIsReadOnly = true;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_fg_ModalTrolleyDblClick(grid, Row, Col)
{
    var nProdutoID;

    nProdutoID = (fg.TextMatrix(Row, getColIndexByColKey(fg, 'ProdutoID')));

    if (divSacolaSalvas.style.visibility == 'inherit')
    {
        SelecionarSacola(1);
        return null;
    }

    if ((Row > 1) && (fg.Col != getColIndexByColKey(fg, 'EhEncomenda' + glb_sPasteReadOnly)) && (fg.Col != getColIndexByColKey(fg, 'OK')) && (nProdutoID > 0)) {
        if (!btnFichaTecnica.disabled) btns_onclick(btnFichaTecnica);
    }

    if (fg.Col == getColIndexByColKey(fg, 'OK'))
    {
        glb_AtualizaEnableChkOK = false;

        for (i = 2; i < fg.Rows; i++) {
            fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = glb_EnableChkOK;
            js_ModalTrolley_AfterEdit(i, getColIndexByColKey(fg, 'OK'));
        }

        glb_EnableChkOK = !glb_EnableChkOK;
        glb_AtualizaEnableChkOK = true;
    }
    if ((glb_LocalClickedIsReadOnly == true) && (fg.Col != getColIndexByColKey(fg, 'EhEncomenda' + glb_sPasteReadOnly)))
        return true;

    if (glb_PassedOneInDblClick == true) {
        glb_PassedOneInDblClick = false;
        return true;
    }

    // trap (grid esta vazio)
    if (grid.Rows <= 1)
        return true;

    var i;
    var bFill = true;

    grid.Editable = false;
    grid.Redraw = 0;

    lockControlsInModalWin(true);

    glb_PassedOneInDblClick = true;

    // limpa coluna se tem um check box checado
    for (i = 2; i < grid.Rows; i++) {
        if (grid.ValueMatrix(i, Col) != 0) {
            bFill = false;
            //break;
        }
    }

    for (i = 2; i < grid.Rows; i++) {
        if (bFill) {
            grid.TextMatrix(i, Col) = 1;
        } else {
            grid.TextMatrix(i, Col) = 0;
        }
    }

    if (fg.Col == getColIndexByColKey(fg, 'EhEncomenda' + glb_sPasteReadOnly))
        glb_bAlteracaoPendente = true;

    // Altera o campo do dso com o novo valor
    dsoGrid.recordset.moveFirst();

    while (!dsoGrid.recordset.EOF) {
        dsoGrid.recordset[grid.ColKey(Col).replace('*', '')].value = grid.TextMatrix(2, Col);
        dsoGrid.recordset.MoveNext();
    }

    dsoGrid.recordset.moveLast();
    dsoGrid.recordset[grid.ColKey(Col).replace('*', '')].value = grid.TextMatrix(2, Col);

    lockControlsInModalWin(false);

    setupBtnsSacolaFromGridState();
    grid.Editable = true;
    grid.Redraw = 2;
    window.focus();
    grid.focus();
}

function js_fg2_ModalTrolleyDblClick() {
    var bFTMode = (btnFichaTecnica.getAttribute('FTMode', 1) == 1);

    if (bFTMode) {
        comutaFichaTecnicaMode();
        return null;
    }

    var bRefMode = (btnReferencias.getAttribute('RefMode', 1) == 1);

    if (bRefMode) {
        comutaReferenciaMode();
        return null;
    }

    if (fg2.Row <= 0)
        return null;

    if (fg2.Col == getColIndexByColKey(fg2, 'PrecoConvertido')) {
        comprar();
    }
    else if (fg2.Col == getColIndexByColKey(fg2, '_calc_Qtd_10'))
        comutaFichaTecnicaMode();
}

function js_fg2_AfterRowColChange() {
    if (glb_GridIsBuilding)
        return null;

    var bFTMode = (btnFichaTecnica.getAttribute('FTMode', 1) == 1);

    if (bFTMode)
        return null;

    var bRefMode = (btnReferencias.getAttribute('RefMode', 1) == 1);

    if (bRefMode)
        return null;

    if (fg2.Rows <= 1) {
        txtConceitoConcreto.value = '';
        txtMarca.value = '';
        txtModelo.value = '';
        txtDescricao.value = '';
        txtArgumentosVenda.value = '';
        return null;
    }

    txtConceitoConcreto.value = getCellValueByColKey(fg2, 'Familia', fg2.Row);
    txtMarca.value = getCellValueByColKey(fg2, 'Marca', fg2.Row);
    txtModelo.value = getCellValueByColKey(fg2, 'Modelo', fg2.Row);
    txtDescricao.value = getCellValueByColKey(fg2, 'Descricao', fg2.Row);
    txtArgumentosVenda.value = getCellValueByColKey(fg2, 'ArgumentosVenda', fg2.Row);
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalTrolleyKeyPress(KeyAscii) {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalTrolley_ValidateEdit() {
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_ModalTrolley_AfterEdit(Row, Col)
{
    if (divSacolaSalvas.style.visibility == 'inherit')
    {
        return null;
    }

    var nQuantidade;

    var nICMSSTUnitario;
    var nPrecoInterno;
    var nPrecoRevenda;
    var nPrecoUnitario;
    var retVal;
    var vlrFatMax;
    var lineException = -1;
    var chkEncomenda = 0;
    var nColRecursoAbreviado = getColIndexByColKey(fg, 'RecursoAbreviado*');
    var nColdtPreco = getColIndexByColKey(fg, 'dtPreco' + glb_sPasteReadOnly);
    var nColQuantidade = getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly);
    var nColPrazo = getColIndexByColKey(fg, 'Prazo');
    var nColValidade = getColIndexByColKey(fg, 'Validade' + glb_sPasteReadOnly);
    var nColPrecoInterno = getColIndexByColKey(fg, 'PrecoInterno');
    var nColPrecoRevenda = getColIndexByColKey(fg, 'PrecoRevenda');
    var nColPrecoUnitario = getColIndexByColKey(fg, 'PrecoUnitario');
    var nColDesconto = getColIndexByColKey(fg, 'Desconto' + glb_sPasteReadOnly);
    var nColICMSSTUnitario = getColIndexByColKey(fg, 'ICMSSTUnitario' + glb_sPasteReadOnly);
    var nColICMSST = getColIndexByColKey(fg, 'ICMSST*');
    var nColValorComissaoRevenda = getColIndexByColKey(fg, 'ValorComissaoRevenda*');
    var nColValorComissaoInterna = getColIndexByColKey(fg, 'ValorComissaoInterna*');
    var nColMC = getColIndexByColKey(fg, 'MargemContribuicao*');
    var nColEncomenda = getColIndexByColKey(fg, 'EhEncomenda' + glb_sPasteReadOnly);
    var nColSacItemID = getColIndexByColKey(fg, 'SacItemID');
    var nColPrecoTotal = getColIndexByColKey(fg, '_calc_PrecoTotal_11*');

    if (fg.Editable) {
        glb_bAlteracaoPendente = true;

        //Prazo ou Validade
        if ((fg.col == nColPrazo) || (fg.col == nColValidade)) {
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

            if (fg.TextMatrix(Row, Col) == '')
                fg.TextMatrix(Row, Col) = '0';
        }
            //Quantidade
        else if (fg.col == nColQuantidade) {
            fg.TextMatrix(Row, nColQuantidade) = treatNumericCell(fg.TextMatrix(Row, nColQuantidade));
            fg.TextMatrix(Row, nColValorComissaoInterna) = '';
            fg.TextMatrix(Row, nColValorComissaoRevenda) = '';
            nICMSSTUnitario = parseFloat(fg.ValueMatrix(Row, nColICMSSTUnitario));
            nPrecoFatura = parseFloat(fg.ValueMatrix(Row, nColPrecoUnitario));
            nQuantidade = parseInt(fg.ValueMatrix(Row, Col), 10);
            BalanceCampaign();

            if (nQuantidade <= 0) {
                fg.TextMatrix(Row, Col) = '1';
                nQuantidade = 1;
            }

            if ((!isNaN(nQuantidade)) && (!isNaN(nPrecoFatura))) {
                fg.TextMatrix(Row, nColPrecoTotal) = roundNumber(nQuantidade * nPrecoFatura, 2);
                fg.TextMatrix(Row, nColICMSST) = roundNumber(nQuantidade * nICMSSTUnitario, 2);
            }
            else {
                // Preco Total
                fg.TextMatrix(Row, nColPrecoTotal) = '0.00';
                if (isNaN(nQuantidade)) fg.TextMatrix(Row, Col) = '';
            }

            calcTotItem(null, lineException);
        }
            //PrecoIterno, PrecoRevenda ou PrecoUnitario
        else if ((Col == nColPrecoInterno) || (Col == nColPrecoRevenda) || (Col == nColPrecoUnitario) || (Col == nColDesconto)) {
            lineException = Row;
            fg.TextMatrix(Row, nColMC) = '';
            fg.TextMatrix(Row, nColValorComissaoInterna) = '';
            fg.TextMatrix(Row, nColValorComissaoRevenda) = '';
            fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));

            if (fg.TextMatrix(Row, Col) == '' || (isNaN(fg.ValueMatrix(Row, Col))))
                fg.TextMatrix(Row, Col) = '0.00';

            if (fg.ValueMatrix(Row, Col) <= 0) {
                dsoGrid.recordset.moveFirst();
                dsoGrid.recordset.find(fg.ColKey(nColSacItemID), getCellValueByColKey(fg, 'SacItemID', Row));

                if (!(dsoGrid.recordset.EOF))
                    fg.TextMatrix(Row, Col) = dsoGrid.recordset[fg.ColKey(Col).replace('*', '')].value;
                else
                    fg.TextMatrix(Row, Col) = '0.00';
            }

            nPrecoInterno = parseFloat(fg.ValueMatrix(Row, nColPrecoInterno));
            nPrecoRevenda = parseFloat(fg.ValueMatrix(Row, nColPrecoRevenda));
            nPrecoUnitario = parseFloat(fg.ValueMatrix(Row, nColPrecoUnitario));
            nQuantidade = parseInt(fg.ValueMatrix(Row, nColQuantidade), 10);

            if (Col == nColPrecoInterno) {
                fg.TextMatrix(Row, nColPrecoRevenda) = nPrecoInterno;
                fg.TextMatrix(Row, nColPrecoUnitario) = nPrecoInterno;
            }
            else if (Col == nColPrecoRevenda) {
                if (nPrecoRevenda < nPrecoInterno) fg.TextMatrix(Row, nColPrecoInterno) = nPrecoRevenda;
                fg.TextMatrix(Row, nColPrecoUnitario) = nPrecoRevenda;
            }
            else if (Col == nColPrecoUnitario) {
                if (nPrecoUnitario < nPrecoRevenda) fg.TextMatrix(Row, nColPrecoRevenda) = nPrecoUnitario;
                if (nPrecoUnitario < nPrecoInterno) fg.TextMatrix(Row, nColPrecoInterno) = nPrecoUnitario;
            }
            if ((!isNaN(nQuantidade)) && (!isNaN(nPrecoUnitario)))
                fg.TextMatrix(Row, nColPrecoTotal) = roundNumber(nQuantidade * nPrecoUnitario, 2);
            else {
                // Preco Total
                fg.TextMatrix(Row, nColPrecoTotal) = '0.00';
                if (isNaN(nPrecoUnitario)) fg.TextMatrix(Row, Col) = '';
            }

            calcTotItem(null, lineException);
        }
        else if (fg.col == nColdtPreco) {
            if (fg.TextMatrix(Row, Col) != '') {
                if (!chkDataEx(fg.TextMatrix(Row, Col))) {
                    fg.TextMatrix(Row, Col) = '';
                }
            }
        }

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[nColRecursoAbreviado, '######', 'C'],
															  [nColQuantidade, '######', 'S'],
															  [nColPrecoTotal, '###,###,###,###.00', 'S'],
															  [nColValorComissaoInterna, '###,###,###,###.00', 'S'],
															  [nColValorComissaoRevenda, '###,###,###,###.00', 'S']], true);

        calcMargemTotal();

        // grid tem primeira linha de totalizacao
        if (glb_totalCols__) {
            if ((fg.Editable) && (fg.Row == 1)) {
                if (fg.Rows > 2) {
                    fg.Row = Row;
                }
            }
        }

        // Altera o campo do dso com o novo valor
        dsoGrid.recordset.moveFirst();
        dsoGrid.recordset.find(fg.ColKey(nColSacItemID), getCellValueByColKey(fg, 'SacItemID', Row));

        if (!(dsoGrid.recordset.EOF))
        {
            dsoGrid.recordset['UsuarioID'].value = glb_USERID;

            if (Col == nColdtPreco)
            {
                if (fg.TextMatrix(Row, Col) == '')
                    dsoGrid.recordset[fg.ColKey(Col).replace('*', '')].value = null;
                else
                    dsoGrid.recordset[fg.ColKey(Col).replace('*', '')].value = fg.TextMatrix(Row, Col);
            }
            else
                dsoGrid.recordset[fg.ColKey(Col).replace('*', '')].value = fg.ValueMatrix(Row, Col);


            if (glb_bCotador) {
                if (Col == nColDesconto)
                    dsoGrid.recordset[fg.ColKey(nColDesconto).replace('*', '')].value = fg.ValueMatrix(Row, nColDesconto);
                
            }
            
            if ((Col == nColPrecoInterno) || (Col == nColPrecoRevenda) || (Col == nColPrecoUnitario)) {
                dsoGrid.recordset[fg.ColKey(nColPrecoInterno)].value = fg.ValueMatrix(Row, nColPrecoInterno);
                dsoGrid.recordset[fg.ColKey(nColPrecoRevenda)].value = fg.ValueMatrix(Row, nColPrecoRevenda);
                dsoGrid.recordset[fg.ColKey(nColPrecoUnitario)].value = fg.ValueMatrix(Row, nColPrecoUnitario);
            }
            

            setupBtnsSacolaFromGridState();
        }

        calcTotalColumns();
        verificarOK();
    }
}

function calcTotalColumns() {
    var nItens = 0;
    var nQtd = 0;
    var nPrecoTotal = 0;
    var nValorComissaoRevenda = 0;
    var chkEhEncomenda = 0;
    var nICMSST = 0;

    if (fg.Rows < 3)
        return;

    var i = 0;

    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
            nItens++;
            nQtd += fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly));
            nPrecoTotal += fg.ValueMatrix(i, getColIndexByColKey(fg, '_calc_PrecoTotal_11*'));
            nValorComissaoRevenda += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ValorComissaoRevenda*'));
            nICMSST += fg.ValueMatrix(i, getColIndexByColKey(fg, 'ICMSST*'));
        }
    }

    fg.TextMatrix(1, getColIndexByColKey(fg, 'RecursoAbreviado*')) = nItens;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly)) = nQtd;
    fg.TextMatrix(1, getColIndexByColKey(fg, '_calc_PrecoTotal_11*')) = nPrecoTotal;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'ValorComissaoRevenda*')) = nValorComissaoRevenda;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'ICMSST*')) = nICMSST;
    fg.TextMatrix(1, getColIndexByColKey(fg, 'dtPreco' + glb_sPasteReadOnly)) = '';

    if (!(dsoSacola.recordset.BOF && dsoSacola.recordset.EOF)) {
        dsoSacola.recordset.moveFirst();
        fg.TextMatrix(1, getColIndexByColKey(fg, 'dtPreco' + glb_sPasteReadOnly)) = dsoSacola.recordset.fields['dtSacola'].asDatetime("dd/mm/yyyy");
    }
}

function txtSacola_onkeydown()
{
    glb_bAlteracaoPendente = true;
    setupBtnsSacolaFromGridState();
}

function txtPrime_onkeydown() {
    glb_bAlteracaoPendente = true;
    setupBtnsSacolaFromGridState();
}

function txtSeuPedido_onkeydown()
{
    glb_bAlteracaoPendente = true;
    setupBtnsSacolaFromGridState();
}

function txtObservacao_onkeydown() {
    glb_bAlteracaoPendente = true;
    setupBtnsSacolaFromGridState();
}

function txtPrevisaoEntrega_onkeydown() {
    glb_bAlteracaoPendente = true;
    setupBtnsSacolaFromGridState();
}

function txtComentarios_onkeydown() {
    glb_bAlteracaoPendente = true;
    setupBtnsSacolaFromGridState();
}

/********************************************************************
Funcao criada pelo programador
Vai ao servidor obter a qntd e o id do produto.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/


/********************************************************************
Funcao criada pelo programador
Retorno do servidor da funcao CheckbalanceCampaign()

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function BalanceCampaign() {
    var verifica_saldo = false;

    for (var n = 2; n <= ((fg.Rows) - 1) ; n++) {
        if (glb_Quantidades[n - 2] != fg.ValueMatrix(n, getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly)) && (fg.ValueMatrix(n, getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly)) != 0)) verifica_saldo = true;
    }

    if (verifica_saldo == false)
        return null;

    var i = 0;
    var strPars = '';

    for (i = 2; i <= fg.rows - 1; i++) {
        var nProdutoID = null;
        var nQuantidade = null;
        nProdutoID = escape(getCellValueByColKey(fg, 'ProdutoID*', i));
        nProdutoID = (nProdutoID == '0' ? 'NULL' : nProdutoID);
        strPars += (i == 2 ? '?' : '&') + 'nProdutoID=' + escape(nProdutoID);
        nQuantidade = escape(fg.ValueMatrix(i, getColIndexByColKey(fg, 'Quantidade' + glb_sPasteReadOnly)));
        strPars += '&nQuantidade=' + escape(nQuantidade);
    }

    dsoBalanceCampaign.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/balancecampaign.aspx' + strPars;
    dsoBalanceCampaign.ondatasetcomplete = BalanceCampaign_DSC;
    dsoBalanceCampaign.refresh();
}

function BalanceCampaign_DSC() {
    var saldocampanha;
    var ProdutoID;
    var quantidade;
    var i = false;

    if (!((dsoBalanceCampaign.recordset.BOF) && (dsoBalanceCampaign.recordset.EOF))) {
        dsoBalanceCampaign.recordset.moveFirst();

        while (!dsoBalanceCampaign.recordset.EOF) {
            saldocampanha = dsoBalanceCampaign.recordset['saldocampanha'].value;
            ProdutoID = dsoBalanceCampaign.recordset['ProdutoID'].value;
            quantidade = dsoBalanceCampaign.recordset['quantidade'].value;

            if (saldocampanha != null) {
                if (!(saldocampanha >= quantidade)) {
                    if (i == false) {
                        glb_MsgCamapanhaAlert = 'Haver� altera��o na margem, quantidade maior que saldo da campanha:' + "\n";
                        i = true;
                    }

                    glb_MsgCamapanhaAlert += "\n" + 'Saldo m�ximo: ' + saldocampanha + ', para o ProdutoID ' + ProdutoID + '.';
                }
            }

            dsoBalanceCampaign.recordset.MoveNext();
        }
    }
}
// FINAL DE EVENTOS DE GRID *****************************************

function alteraInterface(nTipo)
{
    glb_nSacolasSalvas = true;

    if (glb_TrolleyTimerInt != null)
    {
        window.clearInterval(glb_TrolleyTimerInt);
        glb_TrolleyTimerInt = null;
    }

    if (nTipo == 1)
    {
        glb_nSacolasSalvas = true;

        divSacolaSalvas.style.visibility = 'inherit';
        divCtlBar1.style.visibility = 'hidden';
        divCtlBar2.style.visibility = 'hidden';
        fillDSOCmbs1(4);
    }
    else if (nTipo == 2)
    {
        secText('Sacolas de Compras', 1);
        divSacolaSalvas.style.visibility = 'hidden';
        divCtlBar1.style.visibility = 'inherit';
        divCtlBar2.style.visibility = 'inherit';
        //clearComboEx(['selVendedor']);
        //fillDSOCmbs1(1);
    }

    lockControlsInModalWin(true);

    setupPage();
    refreshParamsAndDataAndShowModalWin(true);
}

function sacolasSalvas()
{
    if (glb_TrolleyTimerInt != null)
    {
        window.clearInterval(glb_TrolleyTimerInt);
        glb_TrolleyTimerInt = null;
    }

    if (selVendedor.selectedIndex >= 0)
    {
        var nParceiroID = selParceiro.value;
        var nPessoaID = selPessoa.value;
        var nVendedorID = selVendedor.value;
        var nUsuarioID = selUsuario.value;
        var dtInicio = (txtdtInicio.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtdtInicio.value, true) + '\'');
        var dtFim = (txtdtFim.value == '' ? 'NULL' : '\'' + normalizeDate_DateTime(txtdtFim.value, true) + '\'');
        var nEstadoID = selEstadoID.value;
        var sWhereData = '';
        var sFiltroUsuario = '';
        

        var nPessoaPesqListID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selPessoaID.value');

        /*
        if (nUsuarioID > 0)
            sFiltroUsuario = '(a.UsuarioID = ' + nUsuarioID + ') AND ';
        */

        glb_bCotadorPesqList = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selFonteID.value') == 3 ? true : false);

        if ((dtInicio != 'NULL') && (dtFim != 'NULL'))
            sWhereData = ' AND (a.dtSacola BETWEEN ' + dtInicio + ' AND dbo.fn_Data_Hora(' + dtFim + ', \'01/01/2001 23:59:59.997\')) ';
        else if (dtInicio != 'NULL')
            sWhereData = ' AND (a.dtSacola >= ' + dtInicio + ') ';
        else if (dtFim != 'NULL')
            sWhereData = ' AND (a.dtSacola <= dbo.fn_Data_Hora(' + dtFim + ', \'01/01/2001 23:59:59.997\')) ';

        if (nEstadoID != 0)
            sWhereData = sWhereData + ' AND (a.EstadoID = ' + nEstadoID + ') ';
        else
            sWhereData = sWhereData + ' AND (a.EstadoID IN (21, 22, 24)) ';

        if (glb_bCotadorPesqList)
            sWhereData = sWhereData + ' AND (a.OrigemSacolaID = 610) ';
        else
            sWhereData = sWhereData + ' AND (a.OrigemSacolaID <> 610) ';

        setConnection(dsoSacolasSalvas);

        if ((selParceiro.selectedIndex >= 0) && (selPessoa.selectedIndex >= 0))
            dsoSacolasSalvas.SQL = 'SELECT DISTINCT a.SacolaID, dbo.fn_Recursos_Campos(a.EstadoID, 1) AS Estado, a.dtSacola, a.Sacola AS SacolaGrid, a.Sacola, dbo.fn_TipoAuxiliar_Item(OrigemSacolaID, 0) AS OrigemSacola, ' +
                                            '(SELECT COUNT(1) FROM SacolasCompras_Itens aa WITH(NOLOCK) WHERE (aa.SacolaID = a.SacolaID)) AS Itens, a.TotalSacola AS Valor, ' +
                                            'dbo.fn_Pessoa_Fantasia(a.ParceiroID, 0) AS Revenda, dbo.fn_Cotador_Pessoa_Nome(a.SacolaID, 1) AS Cliente, dbo.fn_Pessoa_Fantasia(a.UsuarioID, 0) AS Usuario, ' +
                                            'dbo.fn_Pessoa_Fantasia(a.ProprietarioID, 0) AS Proprietario, GETDATE() AS Validade, a.EstadoID ' +
                                        'FROM SacolasCompras a WITH(NOLOCK) ' +
                                            'LEFT OUTER JOIN SacolasCompras_Itens b WITH(NOLOCK) ON (b.SacolaID = a.SacolaID) ' +
                                        'WHERE ((a.ParceiroID = ' + nParceiroID + ') AND (a.PessoaID = ' + nPessoaID + ') AND ' + sFiltroUsuario +
                                            '(dbo.fn_SacolasCompras_Atual(a.ParceiroID, a.PessoaID, ' + nVendedorID + ', a.SacolaID) IS NULL)) ' + sWhereData +
                                        'ORDER BY a.dtSacola DESC, SacolaGrid ';
        else if (nPessoaPesqListID == 0)
            dsoSacolasSalvas.SQL = 'SELECT DISTINCT a.SacolaID, dbo.fn_Recursos_Campos(a.EstadoID, 1) AS Estado, a.dtSacola, a.Sacola AS SacolaGrid, a.Sacola, dbo.fn_TipoAuxiliar_Item(OrigemSacolaID, 0) AS OrigemSacola, ' +
                                            '(SELECT COUNT(1) FROM SacolasCompras_Itens aa WITH(NOLOCK) WHERE (aa.SacolaID = a.SacolaID)) AS Itens, a.TotalSacola AS Valor, ' +
                                            'dbo.fn_Pessoa_Fantasia(a.ParceiroID, 0) AS Revenda, dbo.fn_Pessoa_Fantasia(a.PessoaID, 0) AS Cliente, dbo.fn_Pessoa_Fantasia(a.UsuarioID, 0) AS Usuario, ' +
                                            'dbo.fn_Pessoa_Fantasia(a.ProprietarioID, 0) AS Proprietario, GETDATE() AS Validade, a.EstadoID ' +
                                        'FROM SacolasCompras a WITH(NOLOCK) ' +
                                            'LEFT OUTER JOIN SacolasCompras_Itens b WITH(NOLOCK) ON (b.SacolaID = a.SacolaID) ' +
                                        'WHERE (((a.ProprietarioID = ' + nVendedorID + ') OR (a.UsuarioID = ' + nVendedorID + ')) AND ' + sFiltroUsuario +
                                            '(dbo.fn_SacolasCompras_Atual(a.ParceiroID, a.PessoaID, ' + nVendedorID + ', a.SacolaID) IS NULL)) ' + sWhereData +
                                        'ORDER BY a.dtSacola DESC, SacolaGrid ';

        if (dsoSacolasSalvas.SQL != '')
        {
            dsoSacolasSalvas.ondatasetcomplete = sacolasSalvas_DSC;
            dsoSacolasSalvas.refresh();
        }
        else
        {
            lockControlsInModalWin(false);
            secText('Sacolas Salvas', 1);
        }
    }
    else
    {
        lockControlsInModalWin(false);
        secText('Sacolas Salvas', 1);
    }
}

function sacolasSalvas_DSC()
{
    secText('Sacolas Salvas', 1);

    if (dsoSacolasSalvas.recordset.BOF && dsoSacolasSalvas.recordset.EOF)
    {
        fg.Rows = 1;
        fgSacolasSalvas.Rows = 1;
        lockControlsInModalWin(false);

        return null;
    }

    var nSacolaID = 0;
    var i = 0;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.Redraw = 0;
    fg.Editable = false;

    startGridInterface(fg);
    fg.FrozenCols = 0;
    fg.FontSize = '8';
    glb_GridIsBuilding = true;

    var aHoldCols = [12];
    headerGrid(fg, ['ID',
                    'Est',
                    'Sacola',
                    'Data',                    
                    'Itens',
                    'Valor',
                    'OrigemSacola',
                    'Revenda',
                    'Cliente',
                    'Usu�rio',
                    'Proprietario',
                    'Validade',
                    'EstadoID'], aHoldCols);

    glb_aCelHint = [[0, 0, 'Estado da Sacola'],
					[0, 3, 'Data da Sacola'],
					[0, 2, 'Nome da Sacola'],
					[0, 4, 'Itens da Sacola']];

    fillGridMask(fg, dsoSacolasSalvas, ['SacolaID*',
                                        'Estado*',
                                        'SacolaGrid',
                                        'dtSacola*',                                        
                                        'Itens*',
                                        'Valor*',
                                        'OrigemSacola*',
                                        'Revenda*',
                                        'Cliente*',
                                        'Usuario*',
                                        'Proprietario*',
                                        'Validade*',
                                        'EstadoID'],
                                        ['', '', '', '99/99/9999', '999999', '999999999.99', '', '', '', '', '', '99/99/9999', ''],
                                        ['', '', '', dTFormat, '######', '###,###,###.00', '', '', '', '', '', dTFormat, '']);

    alignColsInGrid(fg, [0, 4, 5]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.FrozenCols = 4;
    fg.Editable = true;
    glb_GridIsBuilding = false;
    fg.Redraw = 2;

    lockControlsInModalWin(false);

    if (glb_SacolaTransferenciaID > 0) {
        for (i = 1; i < fg.Rows; i++) {
            if (glb_SacolaTransferenciaID == fg.TextMatrix(i, getColIndexByColKey(fg, 'SacolaID*'))) {
                fg.row = i;
                break;
            }
        }
    }

    if (fg.rows > 1) {
        nSacolaID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'SacolaID*'));

        if (nSacolaID > 0)
            sacolasSalvasItens(nSacolaID);
    }
    if (glb_bCombos2) {
        glb_bCombos2 = false;
        glb_TrolleyTimerInt = window.setInterval('fillDSOCmbs2()', 30, 'JavaScript');
    }
}

function sacolasSalvasItens(nSacolaID)
{
    lockControlsInModalWin(true);

    setConnection(dsoSacolasSalvasItens);

    var sSelect, sFrom, sWhere, sOrderBy;

    sSelect = 'SELECT a.SacItemID, e.Fantasia AS Empresa, a.ProdutoID, d.RecursoAbreviado AS Estado, f.Conceito AS Produto, a.OK, a.EhEncomenda, a.FinalidadeID, a.CFOPID, a.Quantidade, a.LoteID, ' +
                    'STR(dbo.fn_Produto_Estoque(a.EmpresaID, a.ProdutoID,-356, NULL, NULL, dbo.fn_Produto_EstoqueGlobal(a.ProdutoID, a.EmpresaID, b.PessoaID, NULL), NULL, ' +
                    '(CASE WHEN a.LoteID > 0 THEN a.LoteID ELSE NULL END)), 6, 0) AS Disp, a.PrecoInterno, a.PrecoRevenda, a.PrecoUnitario, ' +
                    '(a.PrecoUnitario * a.Quantidade) AS PrecoTotal, dbo.fn_ListaPreco_SUP(a.EmpresaID, a.ProdutoID, b.PessoaID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ' +
                        'b.ParceiroID, a.PrecoInterno, a.PrecoRevenda, a.FinalidadeID, a.CFOPID, a.Quantidade) AS ValorComissaoInterna, ' +
                    'dbo.fn_ListaPreco_SUP(a.EmpresaID, a.ProdutoID, b.PessoaID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b.ParceiroID, a.PrecoRevenda, a.PrecoUnitario, ' +
                        'a.FinalidadeID, a.CFOPID, A.Quantidade) AS ValorComissaoRevenda, a.dtPreco, a.Validade, a.Guia, a.ICMSST, dbo.fn_Pessoa_Fantasia(c.ProprietarioID, 0) AS GP, ' +
                    'dbo.fn_Pessoa_Fantasia(c.AlternativoID, 0) AS Ass, a.MargemContribuicao, g.PedidoID ';

    sFrom = 'FROM SacolasCompras_Itens a WITH(NOLOCK) ' +
                'INNER JOIN SacolasCompras b WITH(NOLOCK) ON (b.SacolaID = a.SacolaID) ' +
                'LEFT OUTER JOIN RelacoesPesCon c WITH(NOLOCK) ON ((c.SujeitoID = a.EmpresaID) AND (c.ObjetoID = a.ProdutoID)) ' +
                'LEFT OUTER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID) ' +
                'INNER JOIN Pessoas e WITH(NOLOCK) ON (e.PessoaID = a.EmpresaID) ' +
                'LEFT JOIN Conceitos f WITH(NOLOCK) ON (f.ConceitoID = a.ProdutoID) ' +
                'LEFT OUTER JOIN Pedidos_Itens g WITH(NOLOCK) ON (g.PedItemID = a.PedItemID) ';

    sWhere = 'WHERE (a.SacolaID = ' + nSacolaID + ') ';

    sOrderBy = 'ORDER BY e.Fantasia, a.ProdutoID';

    if (glb_bCotadorPesqList) {
        sSelect += ', h.Produto AS PartNumber, ' +
                    'SUBSTRING(i.Observacao, CHARINDEX(\'-\', i.Observacao, 0) + 1, LEN(i.Observacao)) AS Origem ';

        sFrom += 'LEFT JOIN CorpProdutos h WITH(NOLOCK) ON (h.CorpProdutoID = a.CorpProdutoID)' +
                    'LEFT JOIN TiposAuxiliares_Itens i WITH(NOLOCK) ON (i.ItemID = h.OrigemID) ';
    }
    else
        sSelect += ', NULL AS PartNumber, ' +
                    "NULL AS Origem ";

    dsoSacolasSalvasItens.SQL = sSelect + sFrom + sWhere + sOrderBy;
    dsoSacolasSalvasItens.ondatasetcomplete = sacolasSalvasItens_DSC;
    dsoSacolasSalvasItens.refresh();
}

function sacolasSalvasItens_DSC()
{
    var i = 0;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fgSacolasSalvas.Redraw = 0;
    fgSacolasSalvas.Editable = false;

    startGridInterface(fgSacolasSalvas);

    fgSacolasSalvas.FrozenCols = 0;
    fgSacolasSalvas.FontSize = '8';
    glb_GridIsBuilding = true;

    var aHoldCols;

    if ((glb_nA1) && (glb_nA2))
        aHoldCols = glb_bCotadorPesqList == true ? [0] : [0, 3, 4];
    else
        aHoldCols = glb_bCotadorPesqList == true ? [0, 18] : [0, 3, 4, 18];

    headerGrid(fgSacolasSalvas, ['SacItemID', //0
                                 'Empresa', //1
                                 'ID', //2
                                 'PartNumber', //3
                                 'Origem', //4
                                 'E', //5
                                 'Produto', //6
                                 'OK', //7
                                 'Enc', //8
                                 'Finalidade', //9
                                 'CFOPID', //10
                                 'Quant', //11
                                 'Lote', //12
                                 'Disp', //13
                                 'Pre�o Int', //14
                                 'Pre�o Rev', //15
                                 'Pre�o Unit', //16
                                 'Pre�o Total', //17
                                 'MC', //18
                                 'GP', //19
                                 'Especialista', //20
                                 'CI', //21
                                 'CR', //22
                                 'Guia', //23
                                 'ICMSST', //24
                                 'Data Pre�o', //25
                                 'Val', //26
                                 'Pedido'], aHoldCols); //27

    glb_aCelHint = [[0, 0, 'Empresa'],
					[0, 1, 'ProdutoID']];

    fillGridMask(fgSacolasSalvas, dsoSacolasSalvasItens, ['SacItemID', //0
                                                          'Empresa', //1
                                                          'ProdutoID', //2,
                                                          'PartNumber', //3
                                                          'Origem', //4
                                                          'Estado', //5
                                                          'Produto', //6
                                                          'OK', //7
                                                          'EhEncomenda', //8
                                                          'FinalidadeID', //9
                                                          'CFOPID', //10
                                                          'Quantidade', //11
                                                          'LoteID', //12
                                                          'Disp', //13
                                                          'PrecoInterno', //14
                                                          'PrecoRevenda', //15
                                                          'PrecoUnitario', //16
                                                          'PrecoTotal', //17
                                                          'MargemContribuicao', //18
                                                          'GP', //19
                                                          'Ass', //20
                                                          'ValorComissaoInterna', //21
                                                          'ValorComissaoRevenda', //22
                                                          'Guia', //23
                                                          'ICMSST',//24
                                                          'dtPreco',//25
                                                          'Validade',//26
                                                          'PedidoID'],//27
                                                          ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999.99', '', '', '999999999.99', '999999999.99', '', '999999999.99', '99/99/9999', '', ''],
                                                          ['', '', '', '', '', '', '', '', '', '', '', '', '', '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###.00', '', '', '###,###,###.00', '###,###,###.00', '', '###,###,###.00', dTFormat, '', '']);

    //alignColsInGrid(fgSacolasSalvas, [0, 2, 8, 9, 10, 11, 12, 13, 14, 15, 16, 19, 21, 22, 23, 25]);
    alignColsInGrid(fgSacolasSalvas, [0, 2, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 21, 22, 23, 24, 25, 26, 27]);

    gridHasTotalLine(fgSacolasSalvas, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fgSacolasSalvas, 'ProdutoID'), '######', 'C'],
																	   [getColIndexByColKey(fgSacolasSalvas, 'Quantidade'), '######', 'S'],
																	   [getColIndexByColKey(fgSacolasSalvas, 'PrecoTotal'), '###,###,###,###.00', 'S']]);

    fgSacolasSalvas.MergeCells = 1;
    fgSacolasSalvas.MergeCol(1) = true;

    fgSacolasSalvas.AutoSizeMode = 0;
    fgSacolasSalvas.AutoSize(0, fgSacolasSalvas.Cols - 1);

    fgSacolasSalvas.FrozenCols = 4;

    glb_GridIsBuilding = false;

    fgSacolasSalvas.Redraw = 2;
    lockControlsInModalWin(false);

    fg.focus();
    fg.col = 2;
}

function SelecionarSacola(nTornarAtual)
{
    var strPars = '';
    var nSacolaID = 0;

    if (nTornarAtual == 1)
        nSacolaID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SacolaID*'));
    else
        nSacolaID = txtSacolaID.value;

    var nEstadoID = 0;
    var nVendedorID = selVendedor.value;
    var _retMsg = null;
    var nDuplicarSacola = 0;

    if (nTornarAtual == 1)
        nEstadoID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'EstadoID'));

    if (nEstadoID == 48)
    {
        _retMsg = window.top.overflyGen.Confirm('Esta sacola est� Fechada. \nDeseja duplicar a sacola?');

        if ((_retMsg == 0) || (_retMsg == 2))
            return null;
        else if (_retMsg == 1)
            nDuplicarSacola = 1;
    }

    glb_nSacolaSelecionadaID = nSacolaID;

    strPars = '?nSacolaID=' + escape(nSacolaID) +
              '&nVendedorID=' + escape(nVendedorID) +
              '&nTornarAtual=' + escape(nTornarAtual) +
              '&nDuplicarSacola=' + escape(nDuplicarSacola);

    setConnection(dsoSelecionaSacola);

    dsoSelecionaSacola.URL = SYS_ASPURLROOT + '/serversidegenEx/selecionaSacola.aspx' + strPars;
    dsoSelecionaSacola.ondatasetcomplete = SelecionarSacola_DSC;
    dsoSelecionaSacola.refresh();
}

function SelecionarSacola_DSC()
{
    if (dsoSelecionaSacola.recordset['Resultado'].value == '')
        alteraInterface(2);
    else
    {
        glb_nSacolaSelecionadaID = 0;

        if (window.top.overflyGen.Alert(dsoSelecionaSacola.recordset['Resultado'].value) == 0)
            return null;
    }    
}

function realizarCheckout()
{
    if (glb_bCotador) {
        var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);
        var strPars = '?nSacolaID=' + nSacolaID;

        dsoAtualizaProdutos.URL = SYS_ASPURLROOT + '/serversidegenEx/TrolleyAtualizaProduto.aspx' + strPars;
        dsoAtualizaProdutos.ondatasetcomplete = realizarCheckout_DSC;
        dsoAtualizaProdutos.refresh();
    }
    else {
        realizarCheckout_DSC();
    }
}
function realizarCheckout_DSC()
{
    if (glb_bCotador) {
        if ((dsoAtualizaProdutos.recordset['Resultado'].value != 'OK') && (txtEst.value == 'P')) {
            window.top.overflyGen.Alert('Aten��o! Existem produtos sem cadastro na sacola');
            btnFinalizarVenda.disabled = true;
        }
        else {
            btnFinalizarVenda.disabled = false;
        }
    }

    if (txtEst.value == 'C')
        btnGravarCheckout.style.visibility = 'inherit';
    else
        btnGravarCheckout.style.visibility = 'hidden';

    divSacolaSalvas.style.visibility = 'hidden';
    divCtlBar1.style.visibility = 'hidden';
    divCtlBar2.style.visibility = 'hidden';
    divCtlBarGeral.style.visibility = 'hidden';
    divFG.style.visibility = 'hidden';
    divFG2.style.visibility = 'hidden';
    divFGSacolasSalvas.style.visibility = 'hidden';
    divCheckout.style.visibility = 'inherit';

    if (!chkFrete.checked)
        setupPagecheckout(1);
    else
        setupPagecheckout(3);

    preencheEmpresa();

    if (dsoSacola.recordset['ComPessoa'].value == '0') {
        if (txtEst.value == 'P') {
            selCotadorPessoa.readOnly = true;
        }
        else {
            selCotadorPessoa.readOnly = false;           
        }    
        preencheCotadorPessoa();
    }
    else {
        selCotadorPessoa.style.visibility = 'hidden';
        lblCotadorPessoa.style.visibility = 'hidden';
    }
}

function checkoutVoltar()
{
    // verificaErroMargem();

    lblForma.style.visibility = 'inherit';
    lblForma.style.color = 'blue';
    selForma.style.visibility = 'inherit';

    if (txtEst.value != 'P')
        selForma.disabled = false;

    lblFinanciamento.style.visibility = 'inherit';
    lblFinanciamento.style.color = 'blue';
    selFinanciamento.style.visibility = 'inherit';

    if (txtEst.value != 'P')
        selFinanciamento.disabled = false;

    divCtlBar1.style.visibility = 'inherit';
    divCtlBar2.style.visibility = 'inherit';
    divCtlBarGeral.style.visibility = 'inherit';
    divFG.style.visibility = 'inherit';
    divFGSacolasSalvas.style.visibility = 'inherit';
    divCheckout.style.visibility = 'hidden';
    divFGResumo.style.visibility = 'hidden';
    alteraInterface(2);
}

function setupPagecheckout(nTipo) {
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 10;
    var eTop = 1;
    var eTopGap = 2;
    var rQuote = 0;
    var rQuoteGap = 3;
    var btnWidth = 78;
    var coll;
    var aEmpresaData = getCurrEmpresaData();

    lblNomePortador.style.visibility = 'hidden';
    txtNomePortador.style.visibility = 'hidden';
    lblRGPortador.style.visibility = 'hidden';
    txtRGPortador.style.visibility = 'hidden';
    lblPlacaPortador.style.visibility = 'hidden';
    txtPlacaPortador.style.visibility = 'hidden';
    lblUFPortador.style.visibility = 'hidden';
    txtUFPortador.style.visibility = 'hidden';
    txtNomeTransportadora.style.visibility = 'hidden';
    lblNomeTransportadora.style.visibility = 'hidden';
    txtContatoTransportadora.style.visibility = 'hidden';
    lblContatoTransportadora.style.visibility = 'hidden';
    txtTelefoneTransportadora.style.visibility = 'hidden';
    lblTelefoneTransportadora.style.visibility = 'hidden';
    lblIncluir.style.visibility = 'hidden';
    chkIncluir.style.visibility = 'hidden';

    txtNomePortador.maxLength = 20;
    txtRGPortador.maxLength = 15;
    txtPlacaPortador.maxLength = 7;
    txtUFPortador.maxLength = 2;

    lblForma.style.visibility = 'visible';
    lblForma.style.color = 'black';
    selForma.style.visibility = 'visible';
    selForma.disabled = true;
    lblFinanciamento.style.visibility = 'visible';
    lblFinanciamento.style.color = 'black';
    selFinanciamento.style.visibility = 'visible';
    selFinanciamento.disabled = true;


    if (nTipo == 1)
    {
        txtNomeTransportadora.style.visibility = 'inherit';
        lblNomeTransportadora.style.visibility = 'inherit';
        txtContatoTransportadora.style.visibility = 'inherit';
        lblContatoTransportadora.style.visibility = 'inherit';
        txtTelefoneTransportadora.style.visibility = 'inherit';
        lblTelefoneTransportadora.style.visibility = 'inherit';
    }

    if (nTipo == 2)
    {
        lblNomePortador.style.visibility = 'inherit';
        txtNomePortador.style.visibility = 'inherit';
        lblRGPortador.style.visibility = 'inherit';
        txtRGPortador.style.visibility = 'inherit';
        lblPlacaPortador.style.visibility = 'inherit';
        txtPlacaPortador.style.visibility = 'inherit';
        lblUFPortador.style.visibility = 'inherit';
        txtUFPortador.style.visibility = 'inherit';
    }

    if (nTipo == 3)
    {
        if (chkFrete.checked)
        {
            lblIncluir.style.visibility = 'inherit';
            chkIncluir.style.visibility = 'inherit';
        }
    }

    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    var FIRSTLINEGAP = 10;

    eTop += 30;

    with (btnVoltar.style)
    {
        left = rQuote + ELEM_GAP;
        top = eTop;
        width = btnWidth;
        height = 24;
        visibility = 'inherit';
    }

    btnVoltar.onclick = btns_onclick;

    with (btnGravarCheckout.style)
    {
        left = rQuote + ELEM_GAP + 880 - (btnWidth + 30);
        top = eTop;
        width = btnWidth + 20;
        height = 24;
    }

    btnGravarCheckout.onclick = btns_onclick;

    with (btnFinalizarVenda.style)
    {
        left = rQuote + ELEM_GAP + 880;
        top = eTop;
        width = btnWidth + 20;
        height = 24;
        visibility = 'inherit';
    }

    btnFinalizarVenda.onclick = btns_onclick;

    eTop += 30;

    with (selEmpresaID.style)
    {
        left = rQuote + FIRSTLINEGAP;
        top = eTop + parseInt(lblVendedor.currentStyle.height, 10);
        width = 320;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    selEmpresaID.onchange = selEmpresaID_onchange;

    with (lblEmpresaID.style)
    {
        left = parseInt(selEmpresaID.currentStyle.left, 10);
        top = eTop;
        width = (lblVendedor.innerText).length * FONT_WIDTH;
    }

    with (selForma.style)
    {
        left = rQuote + FIRSTLINEGAP;
        top = parseInt(selEmpresaID.currentStyle.top, 10) - 23;
        width = FONT_WIDTH * 7;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (lblForma.style)
    {
        left = parseInt(selForma.currentStyle.left, 10);
        top = eTop - 23;
        width = ((lblForma.innerText).length * FONT_WIDTH) - 10;
    }

    with (selFinanciamento.style)
    {
        left = rQuote + FIRSTLINEGAP;
        top = parseInt(selEmpresaID.currentStyle.top, 10) - 23;
        width = FONT_WIDTH * 17;
        rQuote = 0;
    }

    with (lblFinanciamento.style)
    {
        left = parseInt(selFinanciamento.currentStyle.left, 10);
        top = eTop - 23;
        width = ((lblFinanciamento.innerText).length * FONT_WIDTH) - 10;
    }

    with (selCotadorPessoa.style) {
        left = rQuote + FIRSTLINEGAP + 540;
        top = parseInt(selEmpresaID.currentStyle.top, 10);
        width = FONT_WIDTH * 20;
    }

    with (lblCotadorPessoa.style) {
        left = parseInt(selCotadorPessoa.currentStyle.left, 10);
        top = eTop;
        width = ((lblCotadorPessoa.innerText).length * FONT_WIDTH) - 10;
    }


    /*
    eTop += 30;

    with (lblTotalItens.style) {
        left = rQuote + 100;
        top = eTop + parseInt(lblFrete.currentStyle.height, 10);
        width = 500;
    }
    */

    eTop += 50;

    with (chkFrete.style)
    {
        left = rQuote + 5;
        top = eTop + parseInt(lblFrete.currentStyle.height, 10);
        width = 30;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    if (glb_aEmpresaData[1] != 130)
    {
        lblFrete.style.visibility = 'hidden';
        chkFrete.style.visibility = 'hidden';
    }

    chkFrete.onclick = chkFrete_onclick;

    with (lblFrete.style)
    {
        left = parseInt(chkFrete.currentStyle.left, 10) + FIRSTLINEGAP - 10;
        top = eTop;
        width = (lblVendedor.innerText).length * FONT_WIDTH;
    }

    // eTop += 45;

    with (selTransportadora.style)
    {
        left = rQuote + FIRSTLINEGAP - 5;
        top = eTop + parseInt(lblTransportadora.currentStyle.height, 10);
        width = 290;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    selTransportadora.onchange = selTransportadora_onchange;

    with (lblTransportadora.style)
    {
        left = parseInt(selTransportadora.currentStyle.left, 10);
        top = eTop;
        width = (((lblVendedor.innerText).length + 10) * FONT_WIDTH);
    }

    // rQuote = 0;

    if (nTipo == 1)
    {
        //eTop += 45;

        with (txtNomeTransportadora.style)
        {
            left = rQuote + ELEM_GAP;
            top = eTop + parseInt(lblNomeTransportadora.currentStyle.height, 10);
            width = FONT_WIDTH * 32;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblNomeTransportadora.style) {
            left = parseInt(txtNomeTransportadora.currentStyle.left, 10);
            top = eTop;
            width = ((lblNomeTransportadora.innerText).length * FONT_WIDTH) - 10;
        }

        // eTop += 45;

        with (txtContatoTransportadora.style) {
            left = rQuote + ELEM_GAP;
            top = eTop + parseInt(lblContatoTransportadora.currentStyle.height, 10);
            width = FONT_WIDTH * 31;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblContatoTransportadora.style)
        {
            left = parseInt(txtContatoTransportadora.currentStyle.left, 10);
            top = eTop;
            width = ((lblContatoTransportadora.innerText).length * FONT_WIDTH) - 10;
        }

        //eTop += 45;

        with (txtTelefoneTransportadora.style)
        {
            left = rQuote + ELEM_GAP;
            top = eTop + parseInt(lblTelefoneTransportadora.currentStyle.height, 10);
            width = FONT_WIDTH * 15;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblTelefoneTransportadora.style) {
            left = parseInt(txtTelefoneTransportadora.currentStyle.left, 10);
            top = eTop;
            width = ((lblTelefoneTransportadora.innerText).length * FONT_WIDTH) - 10;
        }
    }
    if (nTipo == 2)
    {
        //eTop += 45;

        with (txtNomePortador.style)
        {
            left = rQuote + ELEM_GAP;
            top = eTop + parseInt(lblNomeTransportadora.currentStyle.height, 10);
            width = FONT_WIDTH * 32;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblNomePortador.style)
        {
            left = parseInt(txtNomePortador.currentStyle.left, 10);
            top = eTop;
            width = ((lblNomeTransportadora.innerText).length * FONT_WIDTH) - 10;
        }

        //eTop += 45;

        with (txtRGPortador.style)
        {
            left = rQuote + ELEM_GAP;
            top = eTop + parseInt(lblContatoTransportadora.currentStyle.height, 10);
            width = FONT_WIDTH * 15;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblRGPortador.style)
        {
            left = parseInt(txtRGPortador.currentStyle.left, 10);
            top = eTop;
            width = ((lblContatoTransportadora.innerText).length * FONT_WIDTH) - 10;
        }

        //eTop += 45;

        with (txtPlacaPortador.style)
        {
            left = rQuote + ELEM_GAP;
            top = eTop + parseInt(lblTelefoneTransportadora.currentStyle.height, 10);
            width = FONT_WIDTH * 10;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblPlacaPortador.style) {
            left = parseInt(txtPlacaPortador.currentStyle.left, 10);
            top = eTop;
            width = ((lblTelefoneTransportadora.innerText).length * FONT_WIDTH) - 10;
        }

        //eTop += 45;

        with (txtUFPortador.style)
        {
            left = rQuote + ELEM_GAP;
            top = eTop + parseInt(lblTelefoneTransportadora.currentStyle.height, 10);
            width = FONT_WIDTH * 5;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        with (lblUFPortador.style)
        {
            left = parseInt(txtUFPortador.currentStyle.left, 10);
            top = eTop;
            width = ((lblTelefoneTransportadora.innerText).length * FONT_WIDTH) - 10;
        }
    }
    if (nTipo == 3)
    {
        with (chkIncluir.style)
        {
            left = rQuote + 5;
            top = eTop + parseInt(lblFrete.currentStyle.height, 10);
            width = 30;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        chkIncluir.onclick = chkIncluir_onclick;

        with (lblIncluir.style)
        {
            left = parseInt(chkIncluir.currentStyle.left, 10) + FIRSTLINEGAP - 10;
            top = eTop;
            width = (lblIncluir.innerText).length * FONT_WIDTH;
        }
    }

    eTop += 45;

    with (lblGridResumoItens.style)
    {
        left = 10;
        top = eTop;
        width = ((lblGridResumoItens.innerText).length * FONT_WIDTH);
    }

    if (txtEst.value == 'P') {
        txtNomePortador.disabled = true;
        txtRGPortador.disabled = true;
        txtPlacaPortador.disabled = true;
        txtUFPortador.disabled = true;
        txtNomeTransportadora.disabled = true;
        txtContatoTransportadora.disabled = true;
        txtTelefoneTransportadora.disabled = true;
        selTransportadora.disabled = true;
        chkFrete.disabled = true;
    }
    else {
        txtNomePortador.disabled = selTransportadora.disabled;
        txtRGPortador.disabled = selTransportadora.disabled;
        txtPlacaPortador.disabled = selTransportadora.disabled;
        txtUFPortador.disabled = selTransportadora.disabled;
        txtNomeTransportadora.disabled = selTransportadora.disabled;
        txtContatoTransportadora.disabled = selTransportadora.disabled;
        txtTelefoneTransportadora.disabled = selTransportadora.disabled;
    }
}

function preencheEmpresa()
{
    lockControlsInModalWin(true);
    var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);

    setConnection(dsoEmpresaSacola);

    var sSql = '';
    if (!glb_bCotador)
    {
        sSql = 'SELECT DISTINCT a.EmpresaID AS EmpresaID, b.Fantasia AS Empresa, SUM(a.PrecoUnitario * a.Quantidade) AS ValorTotal, COUNT(1) AS Itens, ISNULL(c.SacEntregaID, 0) AS SacEntregaID, ' +
                                        'c.TotalFrete, ISNULL(c.FretePago, 0) AS FretePago, ISNULL(c.TransportadoraID, 0) AS TransportadoraID, c.PortadorNome, c.PortadorDocumento, c.PortadorPlaca, ' +
                                        'c.PortadorUFVeiculo, c.TransportadoraNome, c.TransportadoraContato, c.TransportadoraTelefone, ISNULL(c.IncluirFretePreco, 1) AS IncluirFretePreco, ' +
                                        'ISNULL((SELECT CONVERT(BIT, COUNT(1)) ' +
                                                    'FROM SacolasCompras_Entregas aa WITH(NOLOCK) ' +
                                                        'INNER JOIN SacolasCompras_Itens bb WITH(NOLOCK) ON (bb.SacEntregaID = aa.SacEntregaID) ' +
                                                        'INNER JOIN RelacoesPesCon cc WITH(NOLOCK) ON ((cc.SujeitoID = bb.EmpresaID) AND (cc.ObjetoID = bb.ProdutoID)) ' +
                                                'WHERE (((aa.SacEntregaID = c.SacEntregaID) AND (ISNULL(bb.Aprovado, 0) = 0) AND ((bb.MargemContribuicao < cc.MargemMinima) OR (cc.MargemMinima IS NULL))) AND bb.PrecoUnitario > 0)), 0) AS ErroMC ' +
                                    'FROM SacolasCompras_Itens a WITH(NOLOCK) ' +
                                        'INNER JOIN Pessoas b WITH(NOLOCK)  ON (b.PessoaID = a.EmpresaID) ' +
                                        'LEFT OUTER JOIN SacolasCompras_Entregas c WITH(NOLOCK) ON (c.SacEntregaID = a.SacEntregaID) ' +
                                    'WHERE ((a.SacolaID = ' + nSacolaID + ') AND (b.EstadoID = 2) AND (a.PedItemID IS NULL) AND (a.OK = 1)) ' +
                                    'GROUP BY a.EmpresaID, b.Fantasia, c.TotalFrete, ISNULL(c.FretePago, 0), ISNULL(c.TransportadoraID, 0), c.PortadorNome, c.PortadorDocumento, c.PortadorPlaca, ' +
                                        'c.PortadorUFVeiculo, c.TransportadoraNome, c.TransportadoraContato, c.TransportadoraTelefone, ISNULL(c.IncluirFretePreco, 1), c.SacEntregaID ' +
                                    'HAVING SUM(a.PrecoUnitario * a.Quantidade) > 0 ' +
                                    'ORDER BY Empresa';
    }
    else
    {
        sSql = 'SELECT DISTINCT ' +
                    'a.EmpresaID AS EmpresaID, b.Fantasia AS Empresa, SUM(a.PrecoUnitario * a.Quantidade) AS ValorTotal, COUNT(1) AS Itens, ISNULL(c.SacEntregaID, 0) AS SacEntregaID, c.TotalFrete, ' +
                    'ISNULL(c.FretePago, 0) AS FretePago, ISNULL(c.TransportadoraID, 0) AS TransportadoraID, c.PortadorNome, c.PortadorDocumento, c.PortadorPlaca, c.PortadorUFVeiculo, ' +
                    'c.TransportadoraNome, c.TransportadoraContato, c.TransportadoraTelefone, ISNULL(c.IncluirFretePreco, 1) AS IncluirFretePreco, ' +
                    'ISNULL((SELECT CONVERT(BIT, COUNT(1)) ' +
                                'FROM SacolasCompras_Entregas aa WITH(NOLOCK) ' +
                                    'INNER JOIN SacolasCompras_Itens bb WITH(NOLOCK) ON (bb.SacEntregaID = aa.SacEntregaID) ' +
                                    'INNER JOIN CorpProdutos cc WITH(NOLOCK) ON (cc.CorpProdutoID = bb.CorpProdutoID) ' +
                                'WHERE (((aa.SacEntregaID = c.SacEntregaID) AND (ISNULL(bb.Aprovado, 0) = 0) AND ' +
                                        '((bb.MargemContribuicao < dbo.fn_Cotador_Margem(cc.MarcaID,2)) OR (dbo.fn_Cotador_Margem(cc.MarcaID,2) IS NULL))) AND bb.PrecoUnitario > 0)), 0) AS ErroMC ' +
                    'FROM SacolasCompras_Itens a WITH(NOLOCK) ' +
                        'INNER JOIN Pessoas b WITH(NOLOCK)  ON (b.PessoaID = a.EmpresaID) ' +
                        'LEFT OUTER JOIN SacolasCompras_Entregas c WITH(NOLOCK) ON (c.SacEntregaID = a.SacEntregaID) ' +
                        'INNER JOIN SacolasCompras d WITH(NOLOCK) ON (d.SacolaID = a.SacolaID) ' +
                    'WHERE ((a.SacolaID = ' + nSacolaID + ') AND (b.EstadoID = 2) AND (a.PedItemID IS NULL) AND (a.OK = 1)) ' +
                    'GROUP BY a.EmpresaID, b.Fantasia, c.TotalFrete, ISNULL(c.FretePago, 0), ISNULL(c.TransportadoraID, 0), c.PortadorNome, c.PortadorDocumento, c.PortadorPlaca, ' +
                                'c.PortadorUFVeiculo, c.TransportadoraNome, c.TransportadoraContato, c.TransportadoraTelefone, ISNULL(c.IncluirFretePreco, 1), c.SacEntregaID ' +
                    'HAVING SUM(a.PrecoUnitario * a.Quantidade) > 0 ' +
                    'ORDER BY Empresa ';
    }

    dsoEmpresaSacola.SQL = sSql;
    dsoEmpresaSacola.ondatasetcomplete = preencheEmpresa_DSC;
    dsoEmpresaSacola.Refresh();
}

function preencheEmpresa_DSC()
{
    clearComboEx(['selEmpresaID']);

    if (!(dsoEmpresaSacola.recordset.BOF || dsoEmpresaSacola.recordset.EOF))
    {
        glb_bFinalizarVenda = false;

        dsoEmpresaSacola.recordset.moveFirst();

        while (!dsoEmpresaSacola.recordset.EOF)
        {
            var oOption = document.createElement("OPTION");
            // oOption.text = dsoEmpresaSacola.recordset['Empresa'].value;
            oOption.text = dsoEmpresaSacola.recordset['Empresa'].value + ' - ' +
                                dsoEmpresaSacola.recordset['Itens'].value + ' itens * ' + glb_sMoeda + ' ' +
                                padNumReturningStr(roundNumber(dsoEmpresaSacola.recordset['ValorTotal'].value, 2), 2, ',');
            oOption.value = dsoEmpresaSacola.recordset['EmpresaID'].value;
            oOption.setAttribute('Empresa', dsoEmpresaSacola.recordset['Empresa'].value, 1);
            oOption.setAttribute('ValorTotal', dsoEmpresaSacola.recordset['ValorTotal'].value, 1);
            oOption.setAttribute('Itens', dsoEmpresaSacola.recordset['Itens'].value, 1);
            oOption.setAttribute('SacEntregaID', dsoEmpresaSacola.recordset['SacEntregaID'].value, 1);
            oOption.setAttribute('Erro', dsoEmpresaSacola.recordset['ErroMC'].value, 1);
            oOption.setAttribute('TotalFrete', dsoEmpresaSacola.recordset['TotalFrete'].value, 1);
            oOption.setAttribute('FretePago', dsoEmpresaSacola.recordset['FretePago'].value, 1);
            oOption.setAttribute('TransportadoraID', dsoEmpresaSacola.recordset['TransportadoraID'].value, 1);
            oOption.setAttribute('PortadorNome', dsoEmpresaSacola.recordset['PortadorNome'].value, 1);
            oOption.setAttribute('PortadorDocumento', dsoEmpresaSacola.recordset['PortadorDocumento'].value, 1);
            oOption.setAttribute('PortadorPlaca', dsoEmpresaSacola.recordset['PortadorPlaca'].value, 1);
            oOption.setAttribute('PortadorUFVeiculo', dsoEmpresaSacola.recordset['PortadorUFVeiculo'].value, 1);
            oOption.setAttribute('TransportadoraNome', dsoEmpresaSacola.recordset['TransportadoraNome'].value, 1);
            oOption.setAttribute('TransportadoraContato', dsoEmpresaSacola.recordset['TransportadoraContato'].value, 1);
            oOption.setAttribute('TransportadoraTelefone', dsoEmpresaSacola.recordset['TransportadoraTelefone'].value, 1);
            oOption.setAttribute('IncluirFretePreco', dsoEmpresaSacola.recordset['IncluirFretePreco'].value, 1);

            selEmpresaID.add(oOption);
            dsoEmpresaSacola.recordset.MoveNext();
        }

        selEmpresaID.disabled = ((selEmpresaID.length <= 1) ? true : false);

        if (txtEst.value == 'C')
            verificaAvancar();
    }
    else
    {
        checkoutVoltar();


        if (!glb_bFinalizarVenda)
        {
            if (window.top.overflyGen.Alert('Selecione um item.') == 0)
                return null;
        }        

        glb_bFinalizarVenda = false;


        return null;
    }

    selEmpresaID_onchange();
}
function preencheCotadorPessoa() {
    var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);

    clearComboEx(['selCotadorPessoa']);

    setConnection(dsoCotadorPessoa);

    dsoCotadorPessoa.SQL = 'DECLARE @EhContribuinte BIT, @SimplesNacional BIT, @UFID INT, @CidadeID INT,@TipoPessoaID INT,@ParceiroID INT, @PessoaID INT, @EmpresaID INT ' +
                            'SELECT @EhContribuinte = b.EhContribuinteICMS, @SimplesNacional = ISNULL(SimplesNacional,0), @UFID = c.UFID, @CidadeID = c.CidadeID, @TipoPessoaID = b.TipoPessoaID, ' +
                                    '@ParceiroID = a.ParceiroID, @PessoaID = a.PessoaID, @EmpresaID = a.EmpresaID ' +
                                'FROM SacolasCompras a WITH(NOLOCK) ' +
                                    'INNER JOIN Pessoas b WITH(NOLOCK) ON b.PessoaID = a.PessoaID ' +
                                    'INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON c.PessoaID = b.PessoaID ' +
                                'WHERE a.SacolaID = ' + nSacolaID + ' ' +

                            'SELECT @PessoaID AS PessoaID, dbo.fn_Cotador_Pessoa_Nome(' + nSacolaID + ', 1) AS Fantasia, @UFID AS UFID, 0 AS Ordem ' +
                            'UNION ALL ' +
                            'SELECT	b.PessoaID AS PessoaID, b.Fantasia + ISNULL(SPACE(1) + \'(\'+ dbo.fn_Pessoa_Documento(a.SujeitoID, NULL, NULL, 101, 111, 0), SPACE(0)) + \')\' AS Fantasia,  ' +
                                    'e.UFID, 1 AS Ordem ' +
                                'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                                    'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) ' +
                                    'INNER JOIN Pessoas_Creditos bb WITH(NOLOCK) ON (bb.PessoaID = b.PessoaID) ' +
                                    'INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON (ISNULL(bb.FinanciamentoLimiteID, 2) = c.FinanciamentoID) ' +
                                    'LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID=e.PessoaID AND e.Ordem=1) ' +
                                'WHERE (a.ObjetoID = @EmpresaID AND a.SujeitoID = @ParceiroID AND a.EstadoID = 2 AND a.TipoRelacaoID = 21 AND ' +
                                        'AND bb.PaisID = ' + glb_aEmpresaData[1] + ' b.EstadoID = 2 AND e.UFID = @UFID) ' +
                            'UNION ALL ' +
                            'SELECT	b.PessoaID AS PessoaID, b.Fantasia + ISNULL(SPACE(1) + \'(\'+ dbo.fn_Pessoa_Documento(a.SujeitoID, NULL, NULL, 101, 111, 0), SPACE(0)) + \')\' AS Fantasia, ' +
                                    'e.UFID, 2 AS Ordem ' +
                                'FROM RelacoesPessoas a WITH(NOLOCK) ' +
                                    'INNER JOIN Pessoas b WITH(NOLOCK) ON (a.SujeitoID = b.PessoaID) ' +
                                    'INNER JOIN Pessoas_Creditos bb WITH(NOLOCK) ON (bb.PessoaID = b.PessoaID) ' +
                                    'INNER JOIN FinanciamentosPadrao c WITH(NOLOCK) ON (ISNULL(bb.FinanciamentoLimiteID, 2) = c.FinanciamentoID) ' +
                                    'LEFT OUTER JOIN Pessoas_Enderecos e WITH(NOLOCK) ON (b.PessoaID=e.PessoaID AND e.Ordem=1) ' +
                                'WHERE (a.ObjetoID = @ParceiroID AND a.EstadoID = 2 AND  a.TipoRelacaoID = 21 AND  b.EstadoID = 2 AND b.EhContribuinteICMS = @EhContribuinte ' +
                                       'AND b.SimplesNacional = @SimplesNacional AND bb.PaisID = ' + glb_aEmpresaData[1] + ' AND e.UFID = @UFID AND e.CidadeID = @CidadeID AND ' +
                                       'b.TipoPessoaID = @TipoPessoaID) ' +
                                'ORDER BY Ordem, Fantasia ';

    dsoCotadorPessoa.ondatasetcomplete = preencheCotadorPessoa_DSC;
    dsoCotadorPessoa.Refresh();
}

function preencheCotadorPessoa_DSC() {

    if (!(dsoCotadorPessoa.recordset.BOF || dsoCotadorPessoa.recordset.EOF)) {
        dsoCotadorPessoa.recordset.moveFirst();

        while (!dsoCotadorPessoa.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoCotadorPessoa.recordset['Fantasia'].value;
            oOption.value = dsoCotadorPessoa.recordset['PessoaID'].value;
            selCotadorPessoa.add(oOption);
            dsoCotadorPessoa.recordset.MoveNext();
        }
    }
}

function fillCmbsTransporte()
{
    lockControlsInModalWin(false);
    clearComboEx(['selTransportadora']);

    var sFiltroFrete = '';

    if (chkFrete.checked)
        sFiltroFrete = ' AND FretePago = 1 ';
    else
        sFiltroFrete = ' AND FretePago = 0 ';

    if (!((dsoCombosTransporte.recordset.BOF) && (dsoCombosTransporte.recordset.EOF))) {
        dsoCombosTransporte.recordset.moveFirst();
        dsoCombosTransporte.recordset.setFilter('Indice = 5 ' + sFiltroFrete);

        while (!dsoCombosTransporte.recordset.EOF) {
            if (lookUpValueInCmb(selTransportadora, dsoCombosTransporte.recordset['RegistroID'].value) == -1) {
                var oOption = document.createElement("OPTION");

                oOption.text = dsoCombosTransporte.recordset['Campo'].value;
                oOption.value = dsoCombosTransporte.recordset['RegistroID'].value;
                oOption.setAttribute('Rodoviario', dsoCombosTransporte.recordset['Rodoviario'].value, 1);
                oOption.setAttribute('Aereo', dsoCombosTransporte.recordset['Aereo'].value, 1);
                oOption.setAttribute('Maritimo', dsoCombosTransporte.recordset['Maritimo'].value, 1);
                oOption.setAttribute('PP', dsoCombosTransporte.recordset['PP'].value, 1);
                oOption.setAttribute('PT', dsoCombosTransporte.recordset['PT'].value, 1);
                oOption.setAttribute('PC', dsoCombosTransporte.recordset['PC'].value, 1);
                oOption.setAttribute('TP', dsoCombosTransporte.recordset['TP'].value, 1);
                oOption.setAttribute('TT', dsoCombosTransporte.recordset['TT'].value, 1);
                oOption.setAttribute('MeioTransporteID', dsoCombosTransporte.recordset['MeioTransporteID'].value, 1);
                oOption.setAttribute('ValorFrete', dsoCombosTransporte.recordset['ValorFrete'].value, 1);
                selTransportadora.add(oOption);
            }

            dsoCombosTransporte.recordset.moveNext();
        }

        dsoCombosTransporte.recordset.setFilter('');
    }

    if (selTransportadora.options.length > 0)
    {
        selTransportadora.selectedIndex = 0;
        selTransportadora.disabled = false;

        if (glb_nTransportadoraID != 0)
        {
            selTransportadora.value = glb_nTransportadoraID;
        }
            
    }
    else
    {
        selTransportadora.selectedIndex = -1;
        selTransportadora.disabled = true;
    }

    setLabelOfControlEx(lblTransportadora, selTransportadora);

    selTransportadora_onchange();

    
    if ((txtFonte.value=='Cotador') && (fgResumo.rows > 1))
    {
        var bProduto = true;

        for (i = 2; i < fgResumo.Rows; i++)
        {
            if (!(fgResumo.ValueMatrix(i, getColIndexByColKey(fgResumo, 'ProdutoID')) > 0) && (fgResumo.ValueMatrix(i, getColIndexByColKey(fgResumo, 'PrecoUnitario')) > 0))
                bProduto = false;

            if (!(bProduto))
                break;
        }

        //Caso algum item n�o possua ProdutoID
        if (!(bProduto))
        {
            selTransportadora.selectedIndex = 1;
            selTransportadora.disabled = true;
        }
    }

    chkFrete.disabled = selTransportadora.disabled;
}

function chkFrete_onclick()
{
    verificaIncluir();

    fillCmbsTransporte();

    if (chkFrete.checked)
        setupPagecheckout(3);
    else
    {
        lblIncluir.style.visibility = 'hidden';
        chkIncluir.style.visibility = 'hidden';
    }

    //selTransportadora_onchange();
}

function fillDSOCmbs_Transporte()
{
    lockControlsInModalWin(true);

    var nPessoaID = 0;
    var nParceiroID = 0;
    var nEmpresaID = 0;
    var nVendedorID = 0;

    if (selEmpresaID.selectedIndex >= 0)
        nEmpresaID = selEmpresaID.value;
    else
        nEmpresaID = glb_nEmpresaID;

    if (selPessoa.selectedIndex >= 0)
        nPessoaID = selPessoa.value;

    if (selParceiro.selectedIndex >= 0)
        nParceiroID = selParceiro.value;

    if (selVendedor.selectedIndex >= 0)
        nVendedorID = selVendedor.value;
    else
        nVendedorID = 0;

    var strPars = new String();

    strPars = '';
    strPars += '?nEmpresaID=' + escape(nEmpresaID);
    strPars += '&nPessoaID=' + escape(nPessoaID);
    strPars += '&nParceiroID=' + escape(nParceiroID);
    strPars += '&nUserID=' + escape(nVendedorID);
    strPars += '&nTipo=' + escape(3);

    dsoCombosTransporte.URL = SYS_ASPURLROOT + '/serversidegenEx/trolleycmbsdata.aspx' + strPars;
    dsoCombosTransporte.ondatasetcomplete = fillCmbsTransporte;
    dsoCombosTransporte.refresh();
}

function selEmpresaID_onchange()
{
    var bIncluirFretePreco = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('IncluirFretePreco', 1);
    var bFretePago = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('FretePago', 1);
    var nTransportadoraID = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('TransportadoraID', 1);
    var nTotalFrete = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('TotalFrete', 1);
    var sPortadorNome = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('PortadorNome', 1);
    var sPortadorDocumento = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('PortadorDocumento', 1);;
    var sPortadorPlaca = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('PortadorPlaca', 1);
    var sPortadorUFVeiculo = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('PortadorUFVeiculo', 1);
    var sTransportadoraNome = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('TransportadoraNome', 1);
    var sTransportadoraContato = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('TransportadoraContato', 1);
    var sTransportadoraTelefone = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('TransportadoraTelefone', 1);
    
    glb_nTransportadoraID = 0;

    chkFrete.checked = bFretePago;

    if (!chkFrete.checked)
        setupPagecheckout(1);
    else
        setupPagecheckout(3);

    if (!(dsoGridResumo.recordset.BOF && dsoGridResumo.recordset.EOF))
        chkFrete_onclick();

    chkIncluir.checked = bIncluirFretePreco;

    if ((nTransportadoraID != 0) && (selTransportadora.length > 0)) {
        fillCmbsTransporte();

        selTransportadora.value = nTransportadoraID;

        glb_nTransportadoraID = nTransportadoraID;

        if (selTransportadora.selectedIndex > 0) {
            /*
            if (selTransportadora.options[selTransportadora.selectedIndex].text == 'O mesmo') {
                txtNomePortador.value = (sPortadorNome == null ? "" : sPortadorNome);
                txtRGPortador.value = (sPortadorDocumento == null ? "" : sPortadorDocumento);
                txtPlacaPortador.value = (sPortadorPlaca == null ? "" : sPortadorPlaca);
                txtUFPortador.value = (sPortadorUFVeiculo == null ? "" : sPortadorUFVeiculo);
            }
            else if ((!chkFrete.checked) && (selTransportadora.options[selTransportadora.selectedIndex].text == ''))
            {
                txtNomeTransportadora.value = (sTransportadoraNome == null ? "" : sTransportadoraNome);
                txtContatoTransportadora.value = (sTransportadoraContato == null ? "" : sTransportadoraContato);
                txtTelefoneTransportadora.value = (sTransportadoraTelefone == null ? "" : sTransportadoraTelefone);
            }
            
            else if (!chkFrete.checked)*/
            if (!chkFrete.checked)
                setupPagecheckout(3);
        }
        else if (!chkFrete.checked)
            setupPagecheckout(3);
    }
    else {
        selTransportadora.selectedIndex = 0;

        if (nTransportadoraID != 0)
            glb_nTransportadoraID = nTransportadoraID;
    }

    txtNomePortador.value = (sPortadorNome == null ? "" : sPortadorNome);
    txtRGPortador.value = (sPortadorDocumento == null ? "" : sPortadorDocumento);
    txtPlacaPortador.value = (sPortadorPlaca == null ? "" : sPortadorPlaca);
    txtUFPortador.value = (sPortadorUFVeiculo == null ? "" : sPortadorUFVeiculo);
    txtNomeTransportadora.value = (sTransportadoraNome == null ? "" : sTransportadoraNome);
    txtContatoTransportadora.value = (sTransportadoraContato == null ? "" : sTransportadoraContato);
    txtTelefoneTransportadora.value = (sTransportadoraTelefone == null ? "" : sTransportadoraTelefone);
        

    /*
    if (selTransportadora.selectedIndex >= 0)
        selTransportadora.selectedIndex = 0;
    */

    fillGridResumo();
}

function selTransportadora_onchange()
{
    var nValorFrete = 0;

    verificaIncluir();

    if (!glb_bGridResumo)
    {
        glb_RecalculaResumo = true;

        fillGridResumo_DSC();
    }
    else
        glb_bGridResumo = false;

    if (selTransportadora.selectedIndex >= 0)
    {
        if (selTransportadora.options[selTransportadora.selectedIndex].text == 'O mesmo')
            setupPagecheckout(2);
        else if ((!chkFrete.checked) && (selTransportadora.options[selTransportadora.selectedIndex].text == ''))
            setupPagecheckout(1);
        else if (!chkFrete.checked)
            setupPagecheckout(3);

        nValorFrete = selTransportadora.options(selTransportadora.selectedIndex).getAttribute('ValorFrete', 1);
    }

    
    var sEmpresa = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('Empresa', 1);
    var nValorTotal = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('ValorTotal', 1);
    var nItens = selEmpresaID.options(selEmpresaID.selectedIndex).getAttribute('Itens', 1);
    var nValorTotalGrid = fgResumo.ValueMatrix(1, getColIndexByColKey(fgResumo, 'PrecoTotal'));
    var nValorGrid = 0;
    var nPercentual = 0;
    var i = 0;

    if ((nValorFrete > 0) && (chkIncluir.checked))
    {
        nValorTotal = (nValorTotal + nValorFrete);

        for (i = 2; i < fgResumo.Rows; i++)
        {
            nValorGrid = fgResumo.ValueMatrix(i, getColIndexByColKey(fgResumo, 'PrecoTotal'));
            nValorGrid = (nValorGrid + (nValorFrete * (nValorGrid / nValorTotalGrid)));
            fgResumo.TextMatrix(i, getColIndexByColKey(fgResumo, 'PrecoTotal')) = nValorGrid;
        }

        gridHasTotalLine(fgResumo, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fgResumo, 'ProdutoID'), '######', 'C'],
                                                                    [getColIndexByColKey(fgResumo, 'Quantidade'), '######', 'S'],
                                                                    [getColIndexByColKey(fgResumo, 'PrecoTotal'), '###,###,###,###.00', 'S']]);
    }

    //lblTotalItens.innerText = nItens + ' itens * ' + glb_sMoeda + ' ' + padNumReturningStr(roundNumber(nValorTotal, 2), 2, ',');
    selEmpresaID.options(selEmpresaID.selectedIndex).text = sEmpresa + ' - ' + nItens + ' itens * ' + glb_sMoeda + ' ' + padNumReturningStr(roundNumber(nValorTotal, 2), 2, ',');

    glb_RecalculaResumo = false;
}

function SacolaEntrega(bFinalizarVenda) {
    var sPortadorNome = '';
    var sPortadorDocumento = '';
    var sPortadorPlaca = '';
    var sPortadorUFVeiculo = '';
    var sTransportadoraNome = '';
    var sTransportadoraContato = '';
    var sTransportadoraTelefone = '';

    if (((selTransportadora.options[selTransportadora.selectedIndex].text == 'O mesmo') && (!glb_bCotador)) || ((!chkFrete.checked) && (selTransportadora.options[selTransportadora.selectedIndex].text == ''))) {
        if (!verificaEntrega()) {
            glb_bGravarCheckout = false;

            if (dsoSacola.recordset['ComPessoa'].value == '0' && (dsoSacola.recordset['PessoaID'].value != selCotadorPessoa.value)) {
                atualizaCotadorPessoa();
            }

            return null;
        }
    }

    lockControlsInModalWin(true);

    var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);
    var nEmpresaID = selEmpresaID.value;
    var nTotalFrete = selTransportadora.options(selTransportadora.selectedIndex).getAttribute('ValorFrete', 1);

    if (nTotalFrete == null)
        nTotalFrete = '';

    var nMeioTransporteID = selTransportadora.options(selTransportadora.selectedIndex).getAttribute('MeioTransporteID', 1);

    if (nMeioTransporteID == null)
        nMeioTransporteID = '';

    var nFretePago = ((chkFrete.checked) ? 1 : 0);
    var nIncluirFretePreco = ((chkIncluir.checked) ? 1 : 0);

    var nTransportadoraID = selTransportadora.value;
    // S� preenche estes campos caso n�o tenha nenhuma transportadora selecionada
    if (nTransportadoraID == 0) {
        sTransportadoraNome = txtNomeTransportadora.value;
        sTransportadoraContato = txtContatoTransportadora.value;
        sTransportadoraTelefone = txtTelefoneTransportadora.value;
    }
        //ou seja a transportadora "O mesmo"
    else if (selTransportadora.options[selTransportadora.selectedIndex].text == 'O mesmo') {
        sPortadorNome = txtNomePortador.value;
        sPortadorDocumento = txtRGPortador.value;
        sPortadorPlaca = txtPlacaPortador.value;
        sPortadorUFVeiculo = txtUFPortador.value;
    }

    var strPars = '?nSacolaID=' + escape(nSacolaID) +
                  '&nEmpresaID=' + escape(nEmpresaID) +
                  '&nTotalFrete=' + escape(nTotalFrete) +
                  '&nFretePago=' + escape(nFretePago) +
                  '&nIncluirFretePreco=' + escape(nIncluirFretePreco) +
                  '&nTransportadoraID=' + escape(nTransportadoraID) +
                  '&nMeioTransporteID=' + escape(nMeioTransporteID) +
                  '&sPortadorNome=' + escape(sPortadorNome) +
                  '&sPortadorDocumento=' + escape(sPortadorDocumento) +
                  '&sPortadorPlaca=' + escape(sPortadorPlaca) +
                  '&sPortadorUFVeiculo=' + escape(sPortadorUFVeiculo) +
                  '&sTransportadoraNome=' + escape(sTransportadoraNome) +
                  '&sTransportadoraContato=' + escape(sTransportadoraContato) +
                  '&sTransportadoraTelefone=' + escape(sTransportadoraTelefone) +
                  '&sGerarPedido=' + escape(((bFinalizarVenda) ? '1' : '0')) +
                  '&nUsuario2ID=' + escape(glb_nUserID);

    setConnection(dsoSacolaEntrega);
    dsoSacolaEntrega.URL = SYS_ASPURLROOT + '/serversidegenEx/SacolaEntrega.aspx' + strPars;
    dsoSacolaEntrega.ondatasetcomplete = SacolaEntrega_DSC;
    dsoSacolaEntrega.refresh();
}

function SacolaEntrega_DSC()
{
    var _retMsg = null;
    var nRetorno = null;
    var sHTMLSupID = null;
    var nSacEntregaID = 0;
    var nPedidoID = 0;

    lockControlsInModalWin(false);

    selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('Erro', 0, 1);

    if (!(dsoSacolaEntrega.recordset.BOF && dsoSacolaEntrega.recordset.EOF))
    {
        if ((dsoSacolaEntrega.recordset['Mensagem'].value != null) && (dsoSacolaEntrega.recordset['Mensagem'].value != ''))
        {
            if (dsoSacolaEntrega.recordset['Mensagem'].value.indexOf('�tens com MC abaixo da m�nima') >= 0)
            {
                selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('Erro', 1, 1);
            }

            if (window.top.overflyGen.Alert(dsoSacolaEntrega.recordset['Mensagem'].value) == 0)
                return null;

            if (dsoSacolaEntrega.recordset["PedidoID"].value != null)
                nPedidoID = dsoSacolaEntrega.recordset["PedidoID"].value;

            if ((!glb_bGravarCheckout) && (nPedidoID == 0))
                return null;
        }

        if (glb_bGravarCheckout)
        {
            if (dsoSacolaEntrega.recordset.fields["SacEntregaID"].value != null)
            {
                nSacEntregaID = dsoSacolaEntrega.recordset.fields["SacEntregaID"].value;

                selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('SacEntregaID', nSacEntregaID, 1);

                verificaAvancar();
            }



            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('TotalFrete', dsoEmpresaSacola.recordset['TotalFrete'].value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('FretePago', chkFrete.checked, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('TransportadoraID', selTransportadora.value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('PortadorNome', txtNomePortador.value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('PortadorDocumento', txtRGPortador.value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('PortadorPlaca', txtPlacaPortador.value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('PortadorUFVeiculo', txtUFPortador.value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('TransportadoraNome', txtNomeTransportadora.value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('TransportadoraContato', txtContatoTransportadora.value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('TransportadoraTelefone', txtTelefoneTransportadora.value, 1);
            selEmpresaID.options(selEmpresaID.selectedIndex).setAttribute('IncluirFretePreco', chkIncluir.checked, 1);

            if (selEmpresaID.selectedIndex == (selEmpresaID.length - 1))
                selEmpresaID.selectedIndex = 0;
            else
                selEmpresaID.selectedIndex = (selEmpresaID.selectedIndex + 1);

            selEmpresaID_onchange();

            glb_bGravarCheckout = false;
        }

        if (dsoSacolaEntrega.recordset.fields["PedidoID"].value != null)
        {
            if (dsoSacolaEntrega.recordset.fields["PedidoID"].value > 0)
            {
                nEmpresaID = selEmpresaID.value;

                _retMsg = window.top.overflyGen.Confirm('Gerado o Pedido: ' + dsoSacolaEntrega.recordset.fields["PedidoID"].value + '\n' + 'Detalhar Pedido?');

                if (_retMsg == 0)
                    return null;
                else if (_retMsg == 1) {
                    nRetorno = dsoSacolaEntrega.recordset.fields["PedidoID"].value;
                    sHTMLSupID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getHtmlId()');
                    sendJSCarrier(sHTMLSupID, 'SHOWPEDIDO', new Array(nEmpresaID, nRetorno));
                }

                preencheEmpresa();
            }
        }
    }
    else
        glb_bGravarCheckout = false;


    if (dsoSacola.recordset['ComPessoa'].value == '0' && (dsoSacola.recordset['PessoaID'].value != selCotadorPessoa.value)) {
        atualizaCotadorPessoa();
    }
}
function atualizaCotadorPessoa() {
    var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);

    dsoSacola.recordset['PessoaID'].value = selCotadorPessoa.value;
    dsoSacola.recordset['Usuario2ID'].value = dsoSacola.recordset['UsuarioID'].value;
    dsoSacola.recordset['Observacoes'].value = dsoSacola.recordset['Observacoes'].value + '<CotadorComPessoa>';
    dsoSacola.recordset['ClienteNotificado'].value = '0';

    dsoSacola.ondatasetcomplete = dsoAtualizaCotadorPessoa_DSC;
    dsoSacola.SubmitChanges();
    dsoSacola.Refresh();
}
function dsoAtualizaCotadorPessoa_DSC() {

        glb_AlteradoPessoa = true;
        glb_nPessoaID = selCotadorPessoa.value;
        glb_nPessoaDefaultID = selCotadorPessoa.value;

}
function preencheSacolasSalvas()
{
    var nParceiroID = selParceiro.value;
    var nPessoaID = selPessoa.value;
    var nUsuarioID = selVendedor.value;
    var nEstadoID = dsoSacola.recordset['EstadoID'].value;
    var sWhere = '';

    glb_bCotadorPesqList = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selFonteID.value') == 3 ? true : false);

    if (glb_bCotadorPesqList)
        sWhere = 'AND (a.OrigemSacolaID = 610) ';
    else
        sWhere = 'AND (a.OrigemSacolaID <> 610) ';

    lockControlsInModalWin(true);

    setConnection(dsoSacolasSalvasCmb);

    dsoSacolasSalvasCmb.SQL = 'SELECT 0 AS SacolaID, SPACE(0) AS Sacola UNION ALL ' +
                                'SELECT a.SacolaID AS SacolaID, (ISNULL(a.Sacola, \'(Sacola \' + CONVERT(VARCHAR, a.SacolaID) + \')\')) AS Sacola ' +
                                    'FROM SacolasCompras a WITH(NOLOCK) ' +
                                    'WHERE ((a.PessoaID = ' + nPessoaID + ') AND (a.ParceiroID = ' + nParceiroID + ') AND (a.EstadoID = 21) AND (' + nEstadoID + ' = 21) ' +
                                        'AND (ISNULL(dbo.fn_SacolasCompras_Atual(a.ParceiroID, a.PessoaID, ' + nUsuarioID + ', a.SacolaID), 0) <> a.SacolaID) ' + sWhere + ') ' +
                                    'ORDER BY Sacola, SacolaID';

    dsoSacolasSalvasCmb.ondatasetcomplete = preencheSacolasSalvas_DSC;
    dsoSacolasSalvasCmb.Refresh();
}

function preencheSacolasSalvas_DSC()
{
    clearComboEx(['selSacolasSalvas']);

    if (!(dsoSacolasSalvasCmb.recordset.BOF || dsoSacolasSalvasCmb.recordset.EOF)) {
        dsoSacolasSalvasCmb.recordset.moveFirst();

        while (!dsoSacolasSalvasCmb.recordset.EOF) {
            var oOption = document.createElement("OPTION");
            oOption.text = dsoSacolasSalvasCmb.recordset['Sacola'].value;
            oOption.value = dsoSacolasSalvasCmb.recordset['SacolaID'].value;
            selSacolasSalvas.add(oOption);
            dsoSacolasSalvasCmb.recordset.MoveNext();
        }

        selSacolasSalvas.disabled = ((selSacolasSalvas.length <= 1) ? true : false);
    }

    selSacolasSalvas.onchange = selSacolasSalvas_onchange;
}

function tranferirItem() {
    var nSacItemID;
    var i = 0;
    if (selSacolasSalvas.value == 0)
    {
        if (window.top.overflyGen.Alert('Selecione uma sacola salva.') == 0)
            return null;

        return null;
    }

    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) != 0) {
            glb_nItensTransferidos++;
            nSacItemID = fg.TextMatrix(i, getColIndexByColKey(fg, 'SacItemID'));
            dsoGrid.recordset.moveFirst();
            dsoGrid.recordset.Find('SacItemID', nSacItemID);
            dsoGrid.recordset['SacolaID'].value = selSacolasSalvas.value;
        }
    }

    dsoGrid.ondatasetcomplete = saveDataInGridSacola_DSC;
    dsoGrid.SubmitChanges();
}

function verificarOK() {
    if (!glb_AtualizaEnableChkOK)
        return null;

    glb_EnableChkOK = false;

    for (i = 2; i < fg.Rows; i++) {
        if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'OK')) == 0) {
            glb_EnableChkOK = true;
        }
    }
}

function GravarSacolaSalva() {
    var nSacolaID;
    var sSacola;
    var i = 0;

    for (i = 1; i < fg.Rows; i++) {
        nSacolaID = fg.TextMatrix(i, getColIndexByColKey(fg, 'SacolaID*'));
        sSacola = fg.TextMatrix(i, getColIndexByColKey(fg, 'SacolaGrid'));
        dsoSacolasSalvas.recordset.moveFirst();
        dsoSacolasSalvas.recordset.Find('SacolaID', nSacolaID);
        if (dsoSacolasSalvas.recordset['SacolaGrid'].value != sSacola) dsoSacolasSalvas.recordset['Sacola'].value = (sSacola != '' ? sSacola : null);
    }

    dsoSacolasSalvas.ondatasetcomplete = GravarSacolaSalva_DSC;
    dsoSacolasSalvas.SubmitChanges();
}

function GravarSacolaSalva_DSC() {
    lockControlsInModalWin(false);
    glb_TrolleyTimerInt = window.setInterval('sacolasSalvas()', 30, 'JavaScript');
}

function selSacolasSalvas_onchange()
{
    if (selSacolasSalvas.value > 0)
    {
        var sMsg = 'Os itens selecionados (OK) ser�o transferidos para a sacola ' + selSacolasSalvas.options(selSacolasSalvas.selectedIndex).text + '.\nVoc� tem certeza?';
        if (sMsg != '')
        {
            _retMsg = window.top.overflyGen.Confirm(sMsg);
            if ((_retMsg == 0) || (_retMsg == 2))
            {
                selSacolasSalvas.value = 0;
                return null;
            }
        }

        glb_TransferenciaItem = true;
        glb_SacolaTransferenciaID = selSacolasSalvas.value;
        tranferirItem();
    }
}

function verificaEntrega()
{
    var bSemPessoa = (sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'selChavePessoaID.value') == 2 ? true : false);

    if (txtNomePortador.style.visibility == 'inherit')
    {
        if ((trimStr(txtNomePortador.value).length == 0) || (trimStr(txtRGPortador.value).length == 0) || (trimStr(txtPlacaPortador.value).length == 0) || (trimStr(txtUFPortador.value).length == 0))
        {
            //Mensagem n�o deve aparecer caso esteja gravando na condi��o Sem Pessoa - MTH 19/09/2018
            /*
             * Conforme conversado com o Daniel Gon�alves no Ticket#2018111296000521, a trava foi removida pois os vendedores estavam colocando informa��es aleatorias que faziam com que o pedido n�o 
             * fosse faturado. LYF 14/11/2018
            /*
            if (bSemPessoa && glb_bGravarCheckout) {
                return false;
            }
            else {
                if (window.top.overflyGen.Alert('Dados do Portador pendentes.') == 0)
                    return null;

                return false;
            }
            */
        }
    }
    
    if (txtNomeTransportadora.style.visibility == 'inherit')
    {
        if ((trimStr(txtNomeTransportadora.value).length == 0) || (trimStr(txtContatoTransportadora.value).length == 0) || (trimStr(txtTelefoneTransportadora.value).length == 0))
        {
            var nPessoaSacolaID = dsoSacola.recordset['PessoaID'].value;

            //Mensagem n�o deve aparecer caso esteja gravando na condi��o Sem Pessoa - MTH 19/09/2018
            if (bSemPessoa && glb_bGravarCheckout && selCotadorPessoa.value != nPessoaSacolaID) {
                return false;
            }
            else {
                if (window.top.overflyGen.Alert('Dados da Transportadora pendentes.') == 0)
                    return null;

                return false;
            }          
        }
    }

    return true;
}

function fillGridResumo()
{
    var sSelect, sFrom, sWhere;

    lockControlsInModalWin(true);

    var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);
    var nEmpresaID = selEmpresaID.value;

    setConnection(dsoGridResumo);

    sSelect = 'SELECT a.SacItemID, e.Fantasia AS Empresa, a.ProdutoID, d.RecursoAbreviado AS Estado, f.Conceito AS Produto, a.OK, a.EhEncomenda, a.FinalidadeID, a.CFOPID, a.Quantidade, ' +
                                        '(CASE WHEN a.LoteID > 0 THEN CONVERT(VARCHAR, a.LoteID) ELSE SPACE(0) END) AS LoteID, ' +
                                        '(GETDATE() + dbo.fn_Produto_PrevisaoEntregaAtual(a.ProdutoID, NULL, NULL, a.EmpresaID, a.Quantidade, a.LoteID, NULL)) AS dtPrevisaoEntrega, ' +
                                        'STR(dbo.fn_Produto_Estoque(a.EmpresaID, a.ProdutoID,-356, NULL, NULL, dbo.fn_Produto_EstoqueGlobal(a.ProdutoID, a.EmpresaID, b.PessoaID, NULL), NULL, ' +
                                        '(CASE WHEN a.LoteID > 0 THEN a.LoteID ELSE NULL END)), 6, 0) AS Disp, a.PrecoInterno, a.PrecoRevenda, a.PrecoUnitario, ' +
                                        '(a.PrecoUnitario * a.Quantidade) AS PrecoTotal, dbo.fn_ListaPreco_SUP(a.EmpresaID, a.ProdutoID, b.PessoaID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, ' +
                                            'b.ParceiroID, a.PrecoInterno, a.PrecoRevenda, a.FinalidadeID, a.CFOPID, a.Quantidade) AS ValorComissaoInterna, ' +
                                        'dbo.fn_ListaPreco_SUP(a.EmpresaID, a.ProdutoID, b.PessoaID, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, b.ParceiroID, a.PrecoRevenda, a.PrecoUnitario, ' +
                                            'a.FinalidadeID, a.CFOPID, A.Quantidade) AS ValorComissaoRevenda, a.dtPreco, a.Validade, a.Guia, a.ICMSST, a.MargemContribuicao, ';

    sFrom = 'FROM SacolasCompras_Itens a WITH(NOLOCK) ' +
                'INNER JOIN SacolasCompras b WITH(NOLOCK) ON (b.SacolaID = a.SacolaID) ' +
                'LEFT OUTER JOIN RelacoesPesCon c WITH(NOLOCK) ON ((c.SujeitoID = a.EmpresaID) AND (c.ObjetoID = a.ProdutoID)) ' +
                'LEFT OUTER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = c.EstadoID) ' +
                'INNER JOIN Pessoas e WITH(NOLOCK) ON (e.PessoaID = a.EmpresaID) ' +
                'LEFT JOIN Conceitos f WITH(NOLOCK) ON (f.ConceitoID = a.ProdutoID) ';

    if (glb_bCotador)
    {
        sSelect += "dbo.fn_Cotador_Margem(g.MarcaID, 2) AS MargemMinima, " +
                   "a.AprovadorID, a.dtAprovacao, ISNULL(a.Aprovado, 0) AS Aprovado, " +
                   "ISNULL(g.Produto, SPACE(0)) AS PartNumber, " +
                   "SUBSTRING(h.Observacao, CHARINDEX(\'-\', h.Observacao, 0) + 1, LEN(h.Observacao)) AS Origem, " +
                   "CASE WHEN ISNULL(g.CorpProdutoID, 0) > 0 THEN 'Cotador' WHEN a.LoteID > 0 THEN 'Lote' ELSE 'Comum' END AS Fonte ";

        sFrom += 'LEFT JOIN CorpProdutos g WITH(NOLOCK) ON (g.CorpProdutoID = a.CorpProdutoID) ' +
                    'LEFT JOIN TiposAuxiliares_Itens h WITH(NOLOCK) ON (h.ItemID = g.OrigemID) ';
    }
    else
    {
        sSelect += "c.MargemMinima," +
                    "a.AprovadorID, a.dtAprovacao, ISNULL(a.Aprovado, 0) AS Aprovado, " +
                    "NULL AS PartNumber, " +
                    "NULL AS Origem, " +
                    "NULL AS Fonte ";
    }

    sWhere = 'WHERE ((a.SacolaID = ' + nSacolaID + ') AND (a.EmpresaID = ' + nEmpresaID + ') AND (ISNULL(OK, 0) = 1) AND (a.PedItemID IS NULL)) ' +
                                'ORDER BY e.Fantasia, a.ProdutoID';

    dsoGridResumo.SQL = sSelect + sFrom + sWhere;

    dsoGridResumo.ondatasetcomplete = fillGridResumo_DSC;
    dsoGridResumo.refresh();
}

function fillGridResumo_DSC()
{
    var nVermelhoFonte = 0x0000FF;
    var nVerdeFonte = 0x008200;
    var nAzul = 0xFF0000;

    var i = 0;
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fgResumo.Redraw = 0;
    fgResumo.Editable = false;

    startGridInterface(fgResumo);

    fgResumo.FrozenCols = 0;
    fgResumo.FontSize = '8';
    glb_GridIsBuilding = true;

    //var aHoldCols = [0, 18, 19, 20, 21, 22];

    if (glb_bCotador)
        var aHoldCols = [0, 20, 21, 22, 23, 24];
    else
        var aHoldCols = [0, 2, 3, 20, 21, 22, 23, 24];

    headerGrid(fgResumo, ['SacItemID', //0
                                 'ID', //1
                                 'PartNumber', //2
                                 'Origem', //3
                                 'E', //4
                                 'Produto', //5
                                 'Enc', //6
                                 'Finalidade', //7
                                 'CFOPID', //8
                                 'Quant', //9
                                 'Lote', //10
                                 'Pre�o Int', //11
                                 'Pre�o Rev', //12
                                 'Pre�o Unit', //13
                                 'Pre�o Total', //14
                                 'CI', //15
                                 'CR', //16
                                 'Guia', //17
                                 'ICMSST', //18
                                 'Previsao Entrega', //19
                                 'MC', //20
                                 'MC Min', //21
                                 'AprovadorID', //22
                                 'dtAprovacao', //23
                                 'Aprovado'], aHoldCols); //24

    fillGridMask(fgResumo, dsoGridResumo, ['SacItemID', //0
                                           'ProdutoID', //1
                                           'PartNumber', //2
                                           'Origem', //3
                                           'Estado', //4
                                           'Produto', //5
                                           'EhEncomenda', //6
                                           'FinalidadeID', //7
                                           'CFOPID', //8
                                           'Quantidade', //9
                                           'LoteID', //10
                                           'PrecoInterno', //11
                                           'PrecoRevenda', //12
                                           'PrecoUnitario', //13
                                           'PrecoTotal', //14
                                           'ValorComissaoInterna', //15
                                           'ValorComissaoRevenda', //16
                                           'Guia', //17
                                           'ICMSST', //18
                                           'dtPrevisaoEntrega', //19
                                           'MargemContribuicao', //20
                                           'MargemMinima', //21
                                           'AprovadorID', //22
                                           'dtAprovacao', //23
                                           'Aprovado'],//24
                                           ['','', '', '', '', '', '', '', '', '', '', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '999999999.99', '', '999999999.99', '99/99/9999', '999.99', '999.99', '', '', ''],
                                           ['', '', '', '', '', '', '', '', '', '', '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '###,###,###.00', '', '###,###,###.00', dTFormat, '###.00', '###.00', '', '', '']);

    //Chamada para a fun��o que pinta as fontes do grid
    if ((glb_nA1) && (glb_nA2))
        pintaFonteResume();

    /*
    if ((fgResumo.CellForeColor == nVermelhoFonte) || (fgResumo.CellForeColor == nVerdeFonte) || (fgResumo.CellForeColor == nAzul))
        fgResumo.ForeColorSel = fgResumo.CellForeColor;
    else
        fgResumo.ForeColorSel = 0xFFFFFF;
    */

    //alignColsInGrid(fgResumo, [0, 1, 6, 7, 9, 10, 11, 12, 13, 14, 16]);
    alignColsInGrid(fgResumo, [0, 1, 8, 9, 11, 12, 13, 14, 15, 16, 18]);

    gridHasTotalLine(fgResumo, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fgResumo, 'ProdutoID'), '######', 'C'],
															    [getColIndexByColKey(fgResumo, 'Quantidade'), '######', 'S'],
															    [getColIndexByColKey(fgResumo, 'PrecoTotal'), '###,###,###,###.00', 'S']]);

    fgResumo.AutoSizeMode = 0;
    fgResumo.AutoSize(0, fgResumo.Cols - 1);

    glb_GridIsBuilding = false;

    fgResumo.Redraw = 2;

    divFGResumo.style.visibility = 'inherit';
    fgResumo.style.visibility = 'inherit';

    if (!glb_RecalculaResumo)
    {
        glb_bGridResumo = true;
        fillDSOCmbs_Transporte();
    }

    if (glb_bCotador)
        pintaProduto();

    lockControlsInModalWin(false);
}

function chkIncluir_onclick()
{
    fillGridResumo_DSC();
}

/**********************************************************************************
Fun��o que pinta as fontes do Grid
***********************************************************************************/
function pintaFonte()
{
    var nVermelhoFonte = 0x0000FF;
    var nVerdeFonte = 0x008200;
    var nAzul = 0xFF0000;
    var nCorFonte;
    var nAprovadorID;
    var dtAprovacao;
    var bAprovado;

    glb_nItensAvaliar = 0;
    glb_nItensReprovados = 0;

    for (var contador = 1; contador < fg.Rows; contador++)
    {
        //Pinta as c�lulas de CFOP em que o imopsto ST incide na opera��o (CFOP)
        if ((fg.ValueMatrix(contador, getColIndexByColKey(fg, 'MargemContribuicao*')) < fg.ValueMatrix(contador, getColIndexByColKey(fg, 'MargemMinima'))) || (fg.ValueMatrix(contador, getColIndexByColKey(fg, 'MargemMinima')) == 0))
        {
            fg.Select(contador, 0, contador, (fg.Cols - 1));
            fg.FillStyle = 1;

            nAprovadorID = fg.TextMatrix(contador, getColIndexByColKey(fg, 'Aprovador*'));
            dtAprovacao = fg.TextMatrix(contador, getColIndexByColKey(fg, 'dtAprovacao*'));
            bAprovado = (fg.TextMatrix(contador, getColIndexByColKey(fg, 'Aprovado' + glb_sPasteReadOnly)) != 0 ? 1 : 0);

            if ((dtAprovacao != null) && (dtAprovacao.length > 0))
            {
                fg.CellForeColor = nVermelhoFonte;
                    
                if (bAprovado)
                    fg.CellForeColor = nVerdeFonte;
                else
                    glb_nItensReprovados++;
            }
            else
            {
                fg.CellForeColor = nAzul;
                glb_nItensAvaliar++;
            }                
        }
    }
}

function pintaFonteResume()
{
    var nVermelhoFonte = 0x0000FF;
    var nVerdeFonte = 0x008200;
    var nAzul = 0xFF0000;
    var nCorFonte;
    var nAprovadorID;
    var dtAprovacao;
    var bAprovado;

    for (var contador = 1; contador < fgResumo.Rows; contador++)
    {
        //Pinta as c�lulas de CFOP em que o imopsto ST incide na opera��o (CFOP)
        if ((fgResumo.ValueMatrix(contador, getColIndexByColKey(fgResumo, 'MargemContribuicao')) < fgResumo.ValueMatrix(contador, getColIndexByColKey(fgResumo, 'MargemMinima'))) || (fg.ValueMatrix(contador, getColIndexByColKey(fg, 'MargemMinima')) == 0))
        {
            fgResumo.Select(contador, 0, contador, (fgResumo.Cols - 1));
            fgResumo.FillStyle = 1;

            nAprovadorID = fgResumo.TextMatrix(contador, getColIndexByColKey(fgResumo, 'AprovadorID'));
            dtAprovacao = fgResumo.TextMatrix(contador, getColIndexByColKey(fgResumo, 'dtAprovacao'));
            bAprovado = (fgResumo.TextMatrix(contador, getColIndexByColKey(fgResumo, 'Aprovado')) != 0 ? 1 : 0);

            if ((dtAprovacao != null) && (dtAprovacao.length > 0)) {
                fgResumo.CellForeColor = nVermelhoFonte;

                if (bAprovado)
                    fgResumo.CellForeColor = nVerdeFonte;
            }
            else
                fgResumo.CellForeColor = nAzul;
        }
    }
}

function interfaceAprovacao()
{
    var elem;
    var frameRect;
    var modWidth = 0;
    var modHeight = 0;
    var eLeft = 10;
    var eTop = 1;
    var eTopGap = 2;
    var rQuote = 0;
    var rQuoteGap = 3;
    var btnWidth = 78;
    var coll;
    var aEmpresaData = getCurrEmpresaData();

    if ((btnAprovar.style.visibility == 'hidden') && (btnExcluir.style.visibility != 'hidden'))
    {
        selSacolasSalvas.style.visibility = 'hidden';
        btnExcluir.style.visibility = 'hidden';
        btnEsvaziar.style.visibility = 'hidden';
        //btnEstado.style.visibility = 'hidden';
        //btnNotificar.style.visibility = 'hidden';
        btnSalvarSacola.style.visibility = 'hidden';
        btnCheckout.style.visibility = 'hidden';
        btnMC.style.visibility = 'inherit';
        btnAprovar.style.visibility = 'inherit';
        btnAprovar.disabled = !glb_bAprovacaoSacola;
        btnReprovar.style.visibility = 'inherit';
        btnReprovar.disabled = !glb_bAprovacaoSacola;
    }
    else
    {
        selSacolasSalvas.style.visibility = 'inherit';
        btnExcluir.style.visibility = 'inherit';
        btnEsvaziar.style.visibility = 'inherit';
        //btnEstado.style.visibility = 'inherit';
        btnNotificar.style.visibility = 'inherit';
        btnSalvarSacola.style.visibility = 'inherit';
        btnCheckout.style.visibility = 'inherit';
        btnAprovar.style.visibility = 'hidden';
        btnMC.style.visibility = 'hidden';
        btnReprovar.style.visibility = 'hidden';
    }

    with (btnSacolasSalvas.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 25;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }
    btnSacolasSalvas.onclick = btns_onclick;

    with (btnFichaTecnica.style)
    {
        top = eTop + eTopGap;
        width = btnWidth;
        left = left = rQuote + 5;;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        visibility = 'inherit';
    }

    btnFichaTecnica.onclick = btns_onclick;
    btnFichaTecnica.setAttribute('FTMode', 0, 1);
    btnFichaTecnica.title = 'Se preferir, clique duas vezes nas colunas brancas do produto';

    with (btnLog.style)
    {
        top = eTop + eTopGap;
        width = btnWidth - 35;
        left = left = rQuote + 5;;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        visibility = 'inherit';
    }

    btnLog.onclick = btns_onclick;
    rQuote = rQuote + 12;

    with (btnRefresh.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 15;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnRefresh.onclick = btns_onclick;

    with (txtVariacao.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 5;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
        backgroundColor = 'white';
    }

    txtVariacao.onkeypress = verifyNumericEnterNotLinked;
    txtVariacao.setAttribute('verifyNumPaste', 1);
    txtVariacao.onfocus = selFieldContent;
    txtVariacao.setAttribute('thePrecision', 11, 1);
    txtVariacao.setAttribute('theScale', 2, 1);
    txtVariacao.setAttribute('minMax', new Array(-40, 999999999), 1);
    txtVariacao.title = 'Digite:\n� valor inferior a 100 para aplicar esta varia��o percentual aos itens da Sacola; ou\n� valor superior a 100 para aplicar este valor a Sacola.';

    with (btnGravar.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 15;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnGravar.onclick = btns_onclick;
    rQuote = rQuote + 12;

    if (btnAprovar.style.visibility == 'inherit')
    {
        with (btnMC.style)
        {
            left = rQuote + 5;
            top = eTop + eTopGap;
            width = btnWidth - 15;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnMC.onclick = btns_onclick;

        with (btnAprovar.style)
        {
            left = rQuote + 5;
            top = eTop + eTopGap;
            width = btnWidth - 15;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnAprovar.onclick = btns_onclick;

        with (btnReprovar.style)
        {
            left = rQuote + 5;
            top = eTop + eTopGap;
            width = btnWidth - 15;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnReprovar.onclick = btns_onclick;

        with (btnEstado.style)
        {
            top = eTop + eTopGap;
            width = btnWidth - 30;
            left = rQuote + 5;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnEstado.onclick = btns_onclick;

        with (btnNotificar.style)
        {
            top = eTop + eTopGap;
            width = btnWidth - 30;
            left = rQuote + 5;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnNotificar.onclick = btns_onclick;
    }

    with (selSacolasSalvas.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = ((FONT_WIDTH * 15) + (btnWidth - 13));
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    with (btnExcluir.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 20;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnExcluir.onclick = btns_onclick;

    with (btnEsvaziar.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 25;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnEsvaziar.onclick = btns_onclick;
    rQuote = rQuote + 12;

    if (btnAprovar.style.visibility != 'inherit')
    {
        with (btnEstado.style)
        {
            top = eTop + eTopGap;
            width = btnWidth - 30;
            left = rQuote + 5;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnEstado.onclick = btns_onclick;

    }
    with (btnSalvarSacola.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 25;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnSalvarSacola.onclick = btns_onclick;

    if (btnAprovar.style.visibility != 'inherit')
    {
        with (btnNotificar.style)
        {
            top = eTop + eTopGap;
            width = btnWidth - 20;
            left = rQuote + 5;
            rQuote = parseInt(left, 10) + parseInt(width, 10);
        }

        btnNotificar.onclick = btns_onclick;
        btnNotificar.setAttribute('pesqMode', 1, 1);
    }

    with (btnCheckout.style)
    {
        left = rQuote + 5;
        top = eTop + eTopGap;
        width = btnWidth - 18;
        rQuote = parseInt(left, 10) + parseInt(width, 10);
    }

    btnCheckout.onclick = btns_onclick;
}

function alteraEstado(nEstadoID)
{
    dsoSacola.recordset['Usuario2ID'].value = glb_nUserID;
    dsoSacola.recordset['EstadoID'].value = nEstadoID;
    saveDataInGridSacola();
}

function aprovarMargemMinima(aprovar)
{
    var data = getCurrDate();
    var nSacItemID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'SacItemID'));
    var sEmpresa = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Empresa*'));
    var nProdutoID = fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'ProdutoID*'));
    var nMargemContribuicao = fg.ValueMatrix(fg.row, getColIndexByColKey(fg, 'MargemContribuicao*'));
    var i = 0;

    dsoGrid.recordset.moveFirst();
    dsoGrid.recordset.find(fg.ColKey(getColIndexByColKey(fg, 'SacItemID')), nSacItemID);

    if (!(dsoGrid.recordset.EOF))
    {
        dsoGrid.recordset['dtAprovacao'].value = data;
        dsoGrid.recordset['AprovadorID'].value = glb_nUserID;

        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'dtAprovacao*')) = data;
        fg.TextMatrix(fg.row, getColIndexByColKey(fg, 'Aprovador*')) = glb_nUserID;

        if (aprovar)
            dsoGrid.recordset['Aprovado'].value = 1;
        else
            dsoGrid.recordset['Aprovado'].value = 0;

        for (i = 0; i < glb_aMensagemAprovacao.length; i++)
        {
            if (glb_aMensagemAprovacao[i][0] == fg.row)
                break;
        }

        if (glb_aMensagemAprovacao.length == i)
        {
            glb_aMensagemAprovacao.length = (i + 1);
            glb_aMensagemAprovacao[i] = new Array(null, null);
        }

        glb_aMensagemAprovacao[i][0] = fg.row;
        glb_aMensagemAprovacao[i][1] = sEmpresa + ' ' + nProdutoID + ' MC: ' + nMargemContribuicao + '% - ' + ((aprovar) ? 'Aprovar' : 'Reprovar') + '\n';

        //glb_sMensagemAprovacao = glb_sMensagemAprovacao + sEmpresa + ' ' + nProdutoID + ' MC: ' + nMargemContribuicao + '% - ' + ((aprovar) ? 'Aprovar' : 'Reprovar') + '\n';

        if (fg.row < (fg.rows - 1))
            fg.row = (fg.row + 1);

        glb_bAlteracaoPendente = true;
        setupBtnsSacolaFromGridState();

        //saveDataInGridSacola();
    }
}

function getCurrDate()
{
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear() + " ";
    s += padL(d.getHours().toString(), 2, '0') + ":";
    s += padL(d.getMinutes().toString(), 2, '0');

    return (s);
}

// GRID LOG
function fillGridData_LOG()
{
    lockControlsInModalWin(true);

    var nSacolaID = txtSacolaID.value;
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dsoGrid);

    dsoGrid.SQL = 'SELECT a.*, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso, b.Fantasia, c.ItemAbreviado, d.RecursoAbreviado, dbo.fn_Pessoa_Fantasia(EmpresaID, 0) AS Emrpesa, ' +
                        'ProdutoID, e.Campo, (CASE a.EventoID WHEN 28 THEN SPACE(1) ELSE e.Conteudo END) AS Conteudo, dbo.fn_Log_Mudanca(e.LogDetalheID) AS Mudanca ' +
                    'FROM LOGs a WITH(NOLOCK) ' +
                        'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.UsuarioID) ' +
                        'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = a.EventoID) ' +
                        'INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = a.EstadoID) ' +
                        'LEFT OUTER JOIN LOGs_Detalhes e WITH(NOLOCK) ON (e.LOGID = a.LOGID) ' +
                        'LEFT OUTER JOIN SacolasCompras_Itens f WITH(NOLOCK) ON (f.SacItemID = a.RegistroDetalheID) ' +
                    'WHERE a.RegistroID = ' + nSacolaID + ' ' +
                        'AND ((a.FormID = 5220) AND (a.SubFormID = 40010)) ' +
                    'ORDER BY a.Data DESC';

    dsoGrid.ondatasetcomplete = fillGridDataLOG_DSC;
    dsoGrid.refresh();
}

function fillGridDataLOG_DSC()
{
    lockControlsInModalWin(false);

    var i;
    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg2.Redraw = 0;
    fg2.Editable = false;
    startGridInterface(fg2);
    fg2.FrozenCols = 0;
    fg2.FontSize = '8';
    glb_GridIsBuilding = true;

    headerGrid(fg2, ['Data           Hora     ',
                   'Colaborador',
                   'Evento',
                   'Est',
                   'Campo',
                   'De',
                   'Para',
                   'LOGID'], [7]);

    fillGridMask(fg2, dsoGrid, ['DataFuso',
                               'Fantasia',
                               'ItemAbreviado',
                               'RecursoAbreviado',
                               'Campo',
                               'Conteudo',
                               'Mudanca',
                               'LOGID'],
                                ['99/99/9999', '', '', '', '', '', '', ''],
                                [dTFormat + ' hh:mm:ss', '', '', '', '', '', '', '']);

    // Merge de Colunas
    fg2.MergeCells = 4;
    fg2.MergeCol(-1) = true;

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);

    divFG.style.visibility = 'hidden';
    fg.style.visibility = 'hidden';

    fg2.style.width = parseInt(divFG2.style.width, 10) - 1;
    divFG2.style.visibility = 'visible';
    fg2.style.visibility = 'visible';
    glb_GridIsBuilding = false;
}
// GRID LOG

function verificaErroMargem()
{
    var i = 0;
    var nEstadoID = 22;
    var _retMsg;

    for (i = 0; i <= (selEmpresaID.length - 1) ; i++)
    {
        if (selEmpresaID.options(i).getAttribute('Erro', 1) == 1)
        {
            _retMsg = window.top.overflyGen.Confirm('MC menor que MC Minima.\nSolicitar aprova��o?');

            if (_retMsg == 1)
                nEstadoID = 24;
            else
                nEstadoID = null;

            break;
        }
    }

    if (nEstadoID == null)
    {
        checkoutVoltar();
        return null;
    }

    glb_bVoltarCheckout = true;

    alteraEstado(nEstadoID);
}

function notificarCliente()
{
    var bPreco = false;
    var bCadastro = false;
    var _retMsg = null;
    // Verifica as inconsistencias
    for (var contador = 1; contador < fg.Rows; contador++) {
        //Pinta as c�lulas de produtos que tem Inconsistencia
        if (fg.TextMatrix(contador, getColIndexByColKey(fg, 'GravidadeInconsistencia*')) == 1) {
            window.top.overflyGen.Alert('Favor verificar os itens pintados de amarelo.');
            return null;
        }
    }

    lockControlsInModalWin(true);
    if (selFinanciamento.value != '' && selForma.value != '') {

        var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);
        var strPars = '';

        if (glb_bCotador) {
            //Envio dos e-mails a serem notificados
            var sEmailContatos = "";

            for (var i = 0; i < selContato.length; i++) {
                if (selContato[i].selected == true) {
                    if (sEmailContatos != "")
                        sEmailContatos += ";";

                    sEmailContatos += selContato[i].Email;
                }
            }

            if (sEmailContatos != "") {
                _retMsg = window.top.overflyGen.Confirm("Deseja realmente notificar o cliente?");

                if ((_retMsg == 0) || (_retMsg == 2)) {
                    lockControlsInModalWin(false);
                    return null;
                }
                    
            }
            else {
                _retMsg = window.top.overflyGen.Confirm("Deseja realmente gerar a proposta?");

                if ((_retMsg == 0) || (_retMsg == 2)) {
                    lockControlsInModalWin(false);
                    return null;
                }
            }

            nChkLinesToBuy = chkLinesToBuy();


            strPars = '?nSacolaID=' + nSacolaID + '&nLinguaLogada=' + glb_aEmpresaData[1] + '&sEmailContatos=' + sEmailContatos;

            dsoEMail.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/GeraProposta.aspx' + strPars;
            dsoEMail.ondatasetcomplete = sendMail_DSC;
            dsoEMail.refresh();
        }
        else {
            strPars = '?nSacolaID=' + nSacolaID;
            dsoEMail.URL = SYS_ASPURLROOT + '/serversidegenEx/emails/emailtabelapreco.aspx' + strPars;
            dsoEMail.ondatasetcomplete = sendMail_DSC;
            dsoEMail.refresh();
        }
    }
    else {
        window.top.overflyGen.Alert('Favor preencher os campos de financiamento e prazo de pagamento.');
        return null;
    }
}

function verificaAvancar()
{
    var bModalTravada = modalWinIsLocked();

    if (bModalTravada)
        lockControlsInModalWin(false);

    var i = 0;

    btnFinalizarVenda.disabled = false;
    
    for (i = 0; i <= (selEmpresaID.length - 1) ; i++) {
        if (selEmpresaID.options(i).getAttribute('SacEntregaID', 1) == 0) {
            btnFinalizarVenda.disabled = true;

            break;
        }
    }

    if (bModalTravada)
        lockControlsInModalWin(true);
}

function verificaProprietarioAlternativo()
{
    var sAssistentes = '';

    glb_bAprovacaoSacola = false;

    if (!((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF)))
    {
        dsoGrid.recordset.moveFirst();
        dsoGrid.recordset.setFilter('ProprietarioID = ' + glb_USERID + ' OR AlternativoID = ' + glb_USERID);

        if (!((dsoGrid.recordset.BOF) && (dsoGrid.recordset.EOF)))
            glb_bAprovacaoSacola = true;
        else
        {
            dsoGrid.recordset.setFilter('');
            dsoGrid.recordset.moveFirst();

            while (!dsoGrid.recordset.EOF)
            {
                sAssistentes = dsoGrid.recordset['Assistentes'].value;

                if (sAssistentes == null)
                    sAssistentes = '';

                if (sAssistentes.indexOf('|' + glb_USERID + '|') > 0)
                    glb_bAprovacaoSacola = true;

                dsoGrid.recordset.moveNext();
            }
        }

        dsoGrid.recordset.setFilter('');
        dsoGrid.recordset.moveFirst();
    }

    // RETIRAR antes de Publicar
    if ((glb_USERID == 1004) || (glb_nB10I == 1))
        glb_bAprovacaoSacola = true;
}

function mostraHistoricoMC()
{
    var modWidth = 0;

    // largura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));

    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    glb_nWidthDivFG = modWidth - 2 * ELEM_GAP - 6;

    if (!glb_bHistoricoMC)
    {
        carregaGridHistorico();
        btnMC.value = 'Voltar';
    }
    else
    {
        btnMC.value = 'MC';

        divFG2.style.visibility = 'hidden';
        fg2.style.visibility = 'hidden';

        divFG2.style.left = 0;
        divFG2.style.width = glb_nWidthDivFG;

        fg2.Redraw = 0;

        with (fg2.style)
        {
            left = 0;
            top = 0;
            width = parseInt(divFG2.style.width, 10) - 1;
            height = parseInt(divFG2.style.height, 10) - 1;
        }

        fg2.Redraw = 2;
        
        divFG.style.width = glb_nWidthDivFG;

        fg.Redraw = 0;

        with (fg.style)
        {
            left = 0;
            top = 0;
            width = parseInt(divFG.style.width, 10) - 1;
            height = parseInt(divFG.style.height, 10) - 1;
        }

        fg.Redraw = 2;
    }

    glb_bHistoricoMC = !glb_bHistoricoMC;
}

function carregaGridHistorico()
{
    lockControlsInModalWin(true);

    var nSacItemID = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'SacItemID'));

    setConnection(dsoHistoricoMC);

    dsoHistoricoMC.SQL = 'SELECT TOP 20 b.dtMovimento AS Data, c.PedidoID AS Pedido, d.Fantasia AS Cliente, b.Quantidade AS Quantidade, dbo.fn_PedidoItem_Totais(b.PedItemID, 7) AS Preco, ' +
                                'dbo.fn_PedidoItem_Totais(b.PedItemID, 9) AS MC, d.PessoaID, dbo.fn_Pessoa_Localidade(d.PessoaID, 2, NULL, NULL) AS UFID ' +
                            'FROM SacolasCompras_Itens a WITH(NOLOCK) ' +
                                'INNER JOIN Pedidos_Itens b WITH(NOLOCK) ON (b.ProdutoID = a.ProdutoID) ' +
                                'INNER JOIN Pedidos c WITH(NOLOCK) ON ((c.PedidoID = b.PedidoID) AND (c.EmpresaID = a.EmpresaID)) ' +
                                'INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = c.PessoaID) ' +
	                        'WHERE ((a.SacItemID = ' + nSacItemID + ') AND (c.TransacaoID IN (115, 215)) AND (c.EstadoID IN (29, 31, 32)) AND ' +
                                '(b.dtMovimento >= (GETDATE() - 90))) ' +
	                        'ORDER BY b.dtMovimento DESC';

    dsoHistoricoMC.ondatasetcomplete = mostraHistoricoMC_DSC;
    dsoHistoricoMC.refresh();
}

function mostraHistoricoMC_DSC()
{
    lockControlsInModalWin(false);

    var aHoldCols;

    divFG.style.width = (glb_nWidthDivFG / 2) + 80;

    fg.Redraw = 0;

    with (fg.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10) - 1;
        height = parseInt(divFG.style.height, 10) - 1;
    }

    fg.Redraw = 2;

    divFG2.style.width = ((glb_nWidthDivFG / 2) - 90);
    divFG2.style.left = (glb_nWidthDivFG / 2) + 90;

    fg2.Redraw = 0;

    with (fg2.style)
    {
        left = 0;
        top = 0;
        width = parseInt(divFG2.style.width, 10) - 1;
        height = parseInt(divFG2.style.height, 10) - 1;
    }

    fg2.Redraw = 2;

    var i;
    var dTFormat = '';
    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg2.Redraw = 0;
    fg2.Editable = false;

    startGridInterface(fg2);

    fg2.FrozenCols = 0;
    fg2.FontSize = '8';
    glb_GridIsBuilding = true;

    if ((glb_nA1) && (glb_nA2))
        aHoldCols = [6, 7];
    else
        aHoldCols = [5, 6, 7];

    headerGrid(fg2, ['Data',
                     'Pedido',
                     'Cliente',
                     'Quant',
                     'Preco',
                     'MC',
                     'PessoaID',
                     'UFID'], aHoldCols);

    fillGridMask(fg2, dsoHistoricoMC, ['Data',
                                       'Pedido',
                                       'Cliente',
                                       'Quantidade',
                                       'Preco',
                                       'MC',
                                       'PessoaID',
                                       'UFID'],
                                       ['99/99/9999', '', '', '', '999999999.99', '999.99', '', ''],
                                       [dTFormat, '', '', '', '###,###,###.00', '###.00', '', '']);

    pintaHistoricoMC();

    alignColsInGrid(fg2, [1, 3, 4, 5]);

    gridHasTotalLine(fg2, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg2, 'Pedido'), '######', 'C'],
                                                            [getColIndexByColKey(fg2, 'Quantidade'), '######', 'M'],
															[getColIndexByColKey(fg2, 'Preco'), '###,###,###,###.00', 'M'],
															[getColIndexByColKey(fg2, 'MC'), '###,###,###,###.00', 'M']]);

    fg2.ExplorerBar = 5;

    // Merge de Colunas
    fg2.MergeCells = 4;
    fg2.MergeCol(-1) = true;

    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);

    fg2.style.width = parseInt(divFG2.style.width, 10) - 1;
    divFG2.style.visibility = 'visible';
    fg2.style.visibility = 'visible';
    glb_GridIsBuilding = false;

    fg.focus();
}

function verificaIncluir()
{
    chkIncluir.disabled = false;

    if (chkFrete.checked)
    {
        if (selTransportadora.selectedIndex > 0)
        {
            chkIncluir.checked = true;
            chkIncluir.disabled = true;
        }
    }
}

function pintaHistoricoMC()
{
    var nPessoaID;
    var nUFID;
    var nPessoaSacolaID = dsoSacola.recordset['PessoaID'].value;
    var nUFPessoaSacolaID = dsoSacola.recordset['UFID'].value;
    var nVerdeFonte = 0x008200;
    var nAzul = 0xFF0000;

    for (i = 1; i < fg2.Rows; i++)
    {
        fg2.Select(i, 0, i, (fg2.Cols - 1));
        fg2.FillStyle = 1;

        nPessoaID = fg2.TextMatrix(i, getColIndexByColKey(fg2, 'PessoaID'));
        nUFID = fg2.TextMatrix(i, getColIndexByColKey(fg2, 'UFID'));

        if (nPessoaID == nPessoaSacolaID)
            fg2.CellForeColor = nVerdeFonte;
        else if (nUFID == nUFPessoaSacolaID)
            fg2.CellForeColor = nAzul;
    }
}
function acessaPropostaURL()
{
    var nSacolaID = selParceiro.options(selParceiro.selectedIndex).getAttribute('SacolaID', 1);

    strPars = '?nSacolaID=' + nSacolaID;
    dsoPropostaURL.URL = SYS_ASPURLROOT + '/modcomercial/subatendimento/listaprecos/serverside/AcessaProposta.aspx' + strPars;
    dsoPropostaURL.ondatasetcomplete = acessaPropostaURL_DSC;
    dsoPropostaURL.refresh();
}
function acessaPropostaURL_DSC() {
    var propostaURL = dsoPropostaURL.recordset['PropostaURL'].value;

    if (propostaURL == null) {
        window.top.overflyGen.Alert('Esta sacola ainda n�o possui nenhuma proposta.');
        return;
    }

    window.open(propostaURL);
}

function pintaProduto() {
    var bgCorAmarela = 0x00ffff;

    for (var i = 2; i < fgResumo.Rows; i++) {
        //Pinta as c�lulas sem produto cadastrado
        if ((fgResumo.TextMatrix(i, getColIndexByColKey(fgResumo, 'Produto')) == "") && (fgResumo.ValueMatrix(i, getColIndexByColKey(fgResumo, 'PrecoUnitario')) > 0))
            fgResumo.Cell(6, i, getColIndexByColKey(fgResumo, 'Produto'), i, getColIndexByColKey(fgResumo, 'Produto')) = bgCorAmarela;
    }
}
