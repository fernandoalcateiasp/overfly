/********************************************************************
modalgerimagens.js

Library javascript para o modalgerimagens.asp
********************************************************************/

/*******************************************************************
Para atender a WEB agora produto aceita ate 7 imagens, conforme
tabela:

Tipo	Dim px		Descricao					Default
1		142X116		Imagem do produto pequena	*
2		213x174		Imagem do produto media
3		355x290		Imagem do produto grande
4		355x290		Detalhe do produto 1
5		355x290		Detalhe do produto 2
6		355x290		Detalhe do produto 3
7		355x290		Detalhe do produto 4

Caracterizacao de imagem de produto:
FormID = 2110 SubFormID = 21000 TipoRegistroID = 303

*******************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_LASTLINESELID = '';

var glb_timerImagem = null;

// Tamanho maximo de arquivo que o ocx openfile grava
var glb_Max_File_Size_KB = 0;

// Dimensoes maximas da imagem
var glb_Max_Img_Width = 0;
var glb_Max_Img_Height = 0;

// Dimensoes do Thumbnail em pixels
var glb_Thumb_Width_PX = 0;
var glb_Thumb_Height_PX = 0;

var glb_nCurrFormID = 0;
var glb_nCurrSubFormID = 0;

var glb_FirstTime = false;

var glb_GerImagensTimer = null;

//Configura��o do Tamanho da Imagem
//Label Tamanho, ID do Tamanho, Ordem Maxima, Largura Maxima, Altura Maxima, Tamanho Maximo (KB)
var glb_cmbTamanhoData = new Array();

var glb_validFileDoubleClicked = '';

var glb_LastSelFocused = null;

var glb_ForceRealMode = false;

// Grava imagem no banco
var dsoSaveImage = new CDatatransport("dsoSaveImage");
// Relatorio da imagem
var dsoRelatorioImagem = new CDatatransport("dsoRelatorioImagem");
// Insert do registro na tabela Imagens caso n�o exista. BJBN 03/04/2013
var dsoInsertImagem = new CDatatransport("dsoInsertImagem");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_FirstTime = true;
	
	// O titulo do botao btnEditarVizualizar
	btnEditarVizualizar.value = 'Vizualizar';
	
	glb_nCurrFormID = window.top.formID;
	glb_nCurrSubFormID = window.top.subFormID;
	
    glb_nCurrSubFormID = (glb_nSubFormID == 0 ? glb_nCurrSubFormID : glb_nSubFormID);

	// Dimensoes do ThumbNail
	glb_Thumb_Width_PX = 142;
	glb_Thumb_Height_PX = 116;
		
	// Dimensoes maximas das imagens
	
	// Form Pessoas
	if ( glb_nCurrFormID == 1210 )
	{
		// SubForm Pessoa
		if (glb_nCurrSubFormID == 20100)
		{
			// Pessoa Fisica
			if ( glb_nTipoRegistroID == 51 )
			{
				// O ocx e sensivel aos seguintes tipos de arquivos
				openFile.PipeOfFileExt = 'JPG';
				
				// Parametros combos tamanho e ordem
				glb_cmbTamanhoData = [ ['Pequeno', 2, 1, 84, 112, 15] ];
				
			}
			// Pessoa Juridica
			else if ( glb_nTipoRegistroID == 52 )
			{
				// O ocx e sensivel aos seguintes tipos de arquivos
				openFile.PipeOfFileExt = 'JPG';
				
				// Parametros combos tamanho e ordem
				glb_cmbTamanhoData = [ ['Pequeno', 2, 1, 199, 50, 15],
				                       ['Grande', 4, 2, 700, 300, 40] ];
			}
		}	
	}

	// Form TiposAuxiliares
	else if ( glb_nCurrFormID == 1310 )
	{
		// SubForm Itens
		if (glb_nCurrSubFormID == 20201)
		{
            // O ocx e sensivel aos seguintes tipos de arquivos
			openFile.PipeOfFileExt = 'JPG';

			// Parametros combos tamanho e ordem
			glb_cmbTamanhoData = [['M�dio', 3, 1, 500, 400, 90], ['Grande', 4, 3, 700, 300, 80]];
		}	
	}

	// Form Conceitos	
	else if ( glb_nCurrFormID == 2110 )
	{
		// SubForm Conceito
		if (glb_nCurrSubFormID == 21000)
		{
			// Produto
			if ( glb_nTipoRegistroID == 303 )
			{
				// O ocx e sensivel aos seguintes tipos de arquivos
				openFile.PipeOfFileExt = 'JPG|GIF|PNG|JPEG';

				// Parametros combos tamanho e ordem
				glb_cmbTamanhoData = [ ['Pequeno', 2, 1, 150, 150, 15],
									   ['M�dio', 3, 1, 500, 400, 90],
									   ['Grande', 4, 5, 1000, 800, 280] ];
			}
			// Marca
			else if ( glb_nTipoRegistroID == 304 )
			{
				// O ocx e sensivel aos seguintes tipos de arquivos
				openFile.PipeOfFileExt = 'JPG';

				// Parametros combos tamanho e ordem
				glb_cmbTamanhoData = [['Pequeno', 2, 1, 90, 45, 15],
                                      ['M�dio', 3, 1, 500, 400, 90]];
				
			}
			    // Conceito Concreto
			else if (glb_nTipoRegistroID == 302) {
			    // O ocx e sensivel aos seguintes tipos de arquivos
			    openFile.PipeOfFileExt = 'JPG';

			    // Parametros combos tamanho e ordem
			    glb_cmbTamanhoData = [['Pequeno', 2, 1, 90, 45, 15],
                                      ['M�dio', 3, 1, 500, 400, 90]];

			}
		}
	}		
	
	// Form Vitrines	
	else if ( glb_nCurrFormID == 4210 )
	{
		// SubForm Vitrines
		if (glb_nCurrSubFormID == 23100)
		{
			// O ocx e sensivel aos seguintes tipos de arquivos
			openFile.PipeOfFileExt = 'JPG';

			// Parametros combos tamanho e ordem
			glb_cmbTamanhoData = [ ['Grande', 4, 1, 700, 300, 80] ];
		}
	}

	// Documento da qualidade tem caracteristicas particulares
	// e principais parametros da janela sao abaixo definidos
	// Form Documentos
	else if ( glb_nCurrFormID == 7110 )
	{
		// SubForm Documentos
		if (glb_nCurrSubFormID == 26000)
		{
			// O ocx e sensivel aos seguintes tipos de arquivos
			openFile.PipeOfFileExt = 'JPG|GIF';

			// Parametros combos tamanho e ordem
			glb_cmbTamanhoData = [ ['Grande', 4, 1, 900, 1000, 200] ];
			
		}
	}
	
	// Form QAF
	else if ( glb_nCurrFormID == 7180 )
	{
		// SubForm Documentos
		if (glb_nCurrSubFormID == 26210)
		{
			// O ocx e sensivel aos seguintes tipos de arquivos
			openFile.PipeOfFileExt = 'JPG|GIF';

			// Parametros combos tamanho e ordem
			glb_cmbTamanhoData = [ ['Grande', 4, 1, 419, 600, 80] ];
			
		}
	}

	// Bancos tem caracteristicas particulares
	// e principais parametros da janela sao abaixo definidos
	// Form Bancos
	else if ( glb_nCurrFormID == 9220 )
	{
		// SubForm Banco
		if (glb_nCurrSubFormID == 28230)
		{
			// O ocx e sensivel aos seguintes tipos de arquivos
			openFile.PipeOfFileExt = 'JPG|GIF';

			// Parametros combos tamanho e ordem
			glb_cmbTamanhoData = [ ['Pequeno', 2, 1, 69, 88, 15] ];
			
		}
	}
	
	// Form RET - 12220-> ID do form
	else if ( glb_nCurrFormID == 12220 )
	{
		// SubForm Participantes
		if (glb_nCurrSubFormID == 31153)
		{
			// O ocx e sensivel aos seguintes tipos de arquivos
			openFile.PipeOfFileExt = 'JPG|GIF';

			// Parametros combos tamanho e ordem
			glb_cmbTamanhoData = [ ['Grande', 4, 1, 419, 600, 80] ];
			
		}
	}
     // Form Publicacoes	
	else if (glb_nCurrFormID == 4240) {
	    // SubForm Publicacoes
	    if (glb_nCurrSubFormID == 23320) {
	        // O ocx e sensivel aos seguintes tipos de arquivos
	        openFile.PipeOfFileExt = 'JPG|GIF';

	        // Parametros combos tamanho e ordem
	        glb_cmbTamanhoData = [['Medio', 3, 1, 500, 400, 60]];
	    }
	}

	// Preenche combos
	fillCmbTamanho();

	openFile.AxBorderStyle = 1;
	openFile.ListViewStyle = 3;

	openFile.className = 'openFileClass';

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // mostrar a janela movido para eventos de carregamento da imagem
    loadDataAndTreatInterface();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // Os botoes btnEditarVizualizar, btnGravar, btnApagar, btnRefresh
    if ( ctl == btnEditarVizualizar )
    {
		glb_ForceRealMode = false;
		configureAndShowInterface();
		return true;
    }
    if ( ctl == btnGravar )
    {
		if ((openFile.FullFileName == null) || (openFile.FullFileName == ''))
		{
			strError = 'Nenhuma imagem selecionada.';
				
				if ( window.top.overflyGen.Alert(strError) == 0 )
					return null;
				
				lockControlsInModalWin(false);
					
				return true;
		}
		else if (glb_nTipoRegistroID != 303)
		{
		    if (openFile.FullFileName != glb_validFileDoubleClicked)
		    {
		        strError = 'Imagem selecionada incorretamente.';

		        if (window.top.overflyGen.Alert(strError) == 0)
		            return null;

		        lockControlsInModalWin(false);

		        return true;
		    }
		}

		insertImagem();
		return true;
    }
    else if ( ctl == btnApagar )
    {
		saveEraseImgInServer(true);
		return true;
    }
    else if ( ctl == btnRefresh )
    {
		loadImgFromServer();
		return true;
    }
        
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_' + glb_sCaller), null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {   
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_' + glb_sCaller), null );    
    }    
    else if (ctl.id == btnResumo.id )
    {
        var nPessoaID;
        
        if ((glb_nCurrFormID==1210)&&(glb_nCurrSubFormID==20100))
        {
            sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_' + glb_sCaller), null );    
            
            if (glb_sCaller=='PL')
                nPessoaID = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, "fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'PessoaID'))");
            if (glb_sCaller=='S')                
                nPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['PessoaID'].value");
            
            window.top.openModalHTML(nPessoaID, 'Resumo da Pessoa', glb_sCaller, 3, 1);
        }
    }

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Imagem', 1);
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var elem;
    var nNextX = 0;
    var nBtnGap = ELEM_GAP;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
    
    // campo de imagem
    with (img_Imagem.style)
	{
		width = glb_Thumb_Width_PX;
		height = glb_Thumb_Height_PX;
	}
	img_Imagem.title = 'Duplo clique para tamanho real';
	img_Imagem.style.cursor = 'hand';
	img_Imagem.ondblclick = img_Imagem_ondblclick;
	img_Imagem.setAttribute('ThumbMode', 1, 1);
	
	// campo de imagem para dimensoes
	with (img_Imagem_Dims.style)
	{
		left = 0;
		top = 0;
		width = 'auto';
		height = 'auto';
		visibility = 'hidden';
	}
			
    // lblOrigemImagem
    with (lblOrigemImagem.style)
    {
		backgroundColor = 'transparent';
		left = widthFree - parseInt(img_Imagem.currentStyle.width, 10) - ELEM_GAP;
		top = parseInt(divMod01.currentStyle.top, 10) + 
	          parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
	    
	    // necessario para o label nao dar word wrap
	    if ( parseInt(img_Imagem.currentStyle.width, 10) > 90 )      
			width = parseInt(img_Imagem.currentStyle.width, 10);
		else
			width = 90;
			
		width = 100;		
    }
    lblOrigemImagem.innerText = 'Imagem do Banco';
            
    // campo de imagem 2a. parte
    with (img_Imagem.style)
	{
		left = widthFree - parseInt(img_Imagem.currentStyle.width, 10) - ELEM_GAP;
		top = parseInt(lblOrigemImagem.currentStyle.top, 10) + 
	          parseInt(lblOrigemImagem.currentStyle.height, 10);
		visibility = 'visible';
	}
	
	// mensagem de registro sem imagem
	with (lblRegistroSemImagem.style)
    {
		backgroundColor = 'transparent';
		fontSize = '10pt';
		fontWeight = 'bold';
		textAlign = 'left';
		left = parseInt(img_Imagem.currentStyle.left, 10);
		width = parseInt(img_Imagem.currentStyle.width, 10);
		height = 36;
		top = parseInt(img_Imagem.currentStyle.top, 10);
		visibility = 'hidden';
    }
    
    // ajusta o ocx overOpenFile 2a. parte
    with (openFile.style)
	{
		position = 'absolute';
		left = ELEM_GAP;
		width = parseInt(img_Imagem.currentStyle.left, 10) - (2 * ELEM_GAP);
	}
    
    // lblOverOpenFile
    with (lblOverOpenFile.style)
    {
		backgroundColor = 'transparent';
		left = parseInt(openFile.currentStyle.left, 10);
		top = parseInt(divMod01.currentStyle.top, 10) + 
	          parseInt(divMod01.currentStyle.height, 10) + ELEM_GAP;
		width = parseInt(openFile.currentStyle.width, 10) + 50;
    }
    
    // ajusta o ocx overOpenFile 2a. parte
    with (openFile.style)
	{
		top = parseInt(lblOverOpenFile.currentStyle.top, 10) +
		      parseInt(lblOverOpenFile.currentStyle.height, 10);
		height = parseInt(btnOK. currentStyle.top, 10) -
		         parseInt(openFile.currentStyle.top) - (2 * ELEM_GAP);
		visibility = 'visible';
	}
	
	// relatorio de imagem
    with (lblRelatorioImagem.style)
    {
		backgroundColor = 'transparent';
		left = lblOrigemImagem.offsetLeft;
		top = img_Imagem.offsetTop + img_Imagem.offsetHeight + ELEM_GAP;
		width = lblOrigemImagem.offsetWidth;
		height = img_Imagem.offsetHeight;
    }
    

	// btnEditarVizualizar
	elem = btnEditarVizualizar;
    with (elem.style)
    {
		left = parseInt(openFile.currentStyle.left, 10);
		top = parseInt(btnOK.currentStyle.top, 10);
		nNextX = parseInt(elem.currentStyle.left, 10) +
		         parseInt(elem.currentStyle.width, 10);
    }
    
	// btnGravar
	elem = btnGravar;
    with (elem.style)
    {
		left = nNextX + nBtnGap;
		top = parseInt(btnOK.currentStyle.top, 10);
		nNextX = parseInt(elem.currentStyle.left, 10) +
		         parseInt(elem.currentStyle.width, 10);
    }
    
    // btnApagar
	elem = btnApagar;
    with (elem.style)
    {
		left = nNextX + nBtnGap;
		top = parseInt(btnOK.currentStyle.top, 10);
		nNextX = parseInt(elem.currentStyle.left, 10) +
		         parseInt(elem.currentStyle.width, 10);
    }    
    
    // btnRefresh
    elem = btnRefresh;
    with (elem.style)
    {
		left = nNextX + nBtnGap;
		top = parseInt(btnOK.currentStyle.top, 10);
		nNextX = parseInt(elem.currentStyle.left, 10) +
		         parseInt(elem.currentStyle.width, 10);
    }    
    
    // btnCanc
    elem = btnCanc;
    with (elem.style)
    {
		left = parseInt(img_Imagem.currentStyle.left, 10) +
			   parseInt(img_Imagem.currentStyle.width, 10) -
			   parseInt(elem.currentStyle.width, 10);
		top = parseInt(btnOK.currentStyle.top, 10);
		nNextX = parseInt(elem.currentStyle.left, 10) +
		         parseInt(elem.currentStyle.width, 10);
    }    
    elem.value = 'Fechar';
    
    elem = btnResumo;
    with (elem.style)
    {
		left = parseInt(img_Imagem.currentStyle.left, 10) +
			   parseInt(img_Imagem.currentStyle.width, 10) -
			   parseInt(elem.currentStyle.width, 10);
		top = parseInt(btnOK.currentStyle.top, 10);
		nNextX = parseInt(elem.currentStyle.left, 10) +
		         parseInt(elem.currentStyle.width, 10);
    }    
    
    if ((glb_nCurrFormID==1210)&&(glb_nCurrSubFormID==20100))
    {
        btnCanc.style.visibility = 'hidden';
        btnCanc.disabled = true;
    }   
    else
    {   
        btnResumo.style.visibility = 'hidden';
        btnResumo.disabled = true;
    }
     
    // btnOK desabilitado e oculto
    elem = btnOK;
    with (elem.style)
    {
		left = 0;
		top = 0;
		width = 0;
		height = 0;
		visibility = 'hidden';
    }
    elem.disabled = true;

    with (lblTamanhoImagem.style)
    {
    	backgroundColor = 'transparent';
    	width = 80;
    	left = btnRefresh.offsetLeft + btnRefresh.offsetWidth + ELEM_GAP;
    	top = btnRefresh.offsetTop - ELEM_GAP - 5;
    }
	
	with (selTamanhoImagem.style)
	{
		left = lblTamanhoImagem.offsetLeft;
		top = lblTamanhoImagem.offsetTop + lblTamanhoImagem.offsetHeight ;
		width = lblTamanhoImagem.offsetWidth;
	}
	
	selTamanhoImagem.onchange = selTamanhoOrdemImagem_onchange;
	
	with (lblOrdemImagem.style)
    {
    	backgroundColor = 'transparent';
    	width = 40;
		left = lblTamanhoImagem.offsetLeft + lblTamanhoImagem.offsetWidth + ELEM_GAP;
		top = lblTamanhoImagem.offsetTop;
    }
	
	with (selOrdemImagem.style)
	{
		left = lblOrdemImagem.offsetLeft;
		top = lblOrdemImagem.offsetTop + lblOrdemImagem.offsetHeight ;
		width = lblOrdemImagem.offsetWidth;
	}
	
	selOrdemImagem.onchange = selTamanhoOrdemImagem_onchange;
        
    loadImgFromServer();
}

/********************************************************************
Carrega blob da imagem do banco de dados do servidor
********************************************************************/
function loadImgFromServer()
{
	if ( glb_timerImagem != null )
	{
		window.clearInterval(glb_timerImagem);
		glb_timerImagem = null;
	}
	
	lockControlsInModalWin(true);
	
	lblOrigemImagem.innerText = 'Imagem do Banco';
	
	var strPars = new String();
	var nTamanho, nOrdem;
	var sVersao = '';
	var nEstadoID = 0;

	if ((glb_nCurrFormID == 7110) && (glb_nCurrSubFormID == 26000)) 
	{
	    if (glb_sCaller == 'S') 
	    {
	        nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");

	        if (nEstadoID == 2)
	            sVersao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Versao'].value");
	    }
	}
    
    strPars = '?nFormID=' + escape(glb_nCurrFormID);

    if (glb_nSubFormID != 0)
		strPars += '&nSubFormID=' + escape(glb_nSubFormID);
	else
		strPars += '&nSubFormID=' + escape(glb_nCurrSubFormID);

    strPars += '&nRegistroID=' + escape(glb_nRegistroID);
    
    nTamanho = parseInt((selTamanhoImagem.options[selTamanhoImagem.selectedIndex].value), 10);
    nOrdem = parseInt((selOrdemImagem.options[selOrdemImagem.selectedIndex].value), 10);    
    
    strPars += '&nTamanho=' + escape(nTamanho);
    strPars += '&nOrdem=' + escape(nOrdem);
    strPars += '&sVersao=' + escape(sVersao);
    
	// carrega a imagem do servidor pelo arquivo imageblob.asp
	if ( trimStr((SYS_ASPURLROOT.toUpperCase())).lastIndexOf('OVERFLY3') > 0 )
		img_Imagem.src = 'http' + ':/' + '/localhost/overfly3/serversidegenEx/imageblob.aspx' + strPars;
	else
		img_Imagem.src = SYS_ASPURLROOT + '/serversidegenEx/imageblob.aspx' + strPars;
		
	imagemRelatorio();	
}

/********************************************************************
Salva ou apagar blob da imagem no banco de dados do servidor
********************************************************************/
function saveEraseImgInServer(bErase)
{
	lockControlsInModalWin(true);
		
	var strError;
	
	// Na gravacao, valida as dimensoes da imagem (width && height)
	if ( bErase == false )
	{
		if ( openFile.FullFileName == '' )
		{
			strError = 'N�o tem arquivo de imagem selecionado.';
			
			lockControlsInModalWin(false);
			
			if ( window.top.overflyGen.Alert(strError) == 0 )
				return null;
			
			return true;
		}
		
		img_Imagem_Dims.src = openFile.FullFileName;

		return true;
    }
    var sVersao = '';
    var nEstadoID = 0;

    if ((glb_nCurrFormID == 7110) && (glb_nCurrSubFormID == 26000)) 
    {
        if (glb_sCaller == 'S') 
        {
            nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");

            if (nEstadoID == 2)
                sVersao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Versao'].value");
        }
    }
	
	// Na delecao de imagem grava direto
	var strDominium = SYS_PAGESDOMINIUM;
    
    var strConn = sendJSMessage(getHtmlId(), JS_WIDEMSG, JS_STRCONN, null);
    
    strConn += ' Command Properties=Command Time Out=600';

	var nSubForm = (glb_nSubFormID == 0 ? glb_nCurrSubFormID : glb_nSubFormID);
	
	var nTamanho = parseInt((selTamanhoImagem.options[selTamanhoImagem.selectedIndex].value), 10);
	var nOrdem = parseInt((selOrdemImagem.options[selOrdemImagem.selectedIndex].value), 10);
	var strVersao = (sVersao == '' ? 'a.Versao IS NULL AND ' : 'a.Versao = \'' + sVersao + '\' AND ');   
	
	var strSQL = 'SELECT TOP 1 a.Arquivo AS Imagem FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] a WITH(NOLOCK) ' +
	             'WHERE a.FormID = ' + glb_nCurrFormID + ' AND ' +
	             'a.SubFormID = ' +  nSubForm + ' AND ' +
	             'a.RegistroID = ' + glb_nRegistroID + ' AND ' + strVersao +
	             'a.TamanhoImagem = ' + nTamanho + ' AND ' +
	             'a.OrdemImagem = ' + nOrdem + ' AND ' +
                 'a.TipoArquivoID = 1451 ' +
	             'ORDER BY a.DocumentoID';
	
	var strFieldName = 'Imagem';
		
	openFile.SaveFileDataInDatabase(strDominium, strConn, strSQL, strFieldName, bErase);
}

/********************************************************************
Imagem carregou com sucesso - campo IMG para verificar dimensoes
antes de gravar
********************************************************************/
function imgDims_onload()
{
	var bErase = false;
	var bWidthOK = true;
	var bHeightOK = true;
	var strError;
	
	if ( img_Imagem_Dims.offsetWidth > glb_Max_Img_Width )
		bWidthOK = false;
		
	if ( img_Imagem_Dims.offsetHeight > glb_Max_Img_Height )
		bHeightOK = false;	
	
	// A imagem excedeu dimensoes, nao grava
	// glb_Max_Img_Width glb_Max_Img_Height
	if ( (bWidthOK == false) || (bHeightOK == false) )
	{
		strError = 'Erro.\nImagem excedeu as dimens�es m�ximas permitidas: ' + 
		           glb_Max_Img_Width.toString() + ' x ' +
		           glb_Max_Img_Height.toString() + ' pixels.' +
		           '\nGrava��o cancelada.';
			
		if ( window.top.overflyGen.Alert(strError) == 0 )
			return null;
		
		lockControlsInModalWin(false);

		return true;
	}

	var sVersao = '';
	var nEstadoID = 0;

	if ((glb_nCurrFormID == 7110) && (glb_nCurrSubFormID == 26000)) 
	{
	    if (glb_sCaller == 'S') 
	    {
	        nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");

	        if (nEstadoID == 2)
	            sVersao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Versao'].value");
	    }
	}

	// Grava a imagem no banco
	var strDominium = SYS_PAGESDOMINIUM;
    
    var strConn = sendJSMessage(getHtmlId(), JS_WIDEMSG, JS_STRCONN, null);
    
    strConn += ' Command Properties=Command Time Out=600';

    var nSubForm = (glb_nSubFormID == 0 ? glb_nCurrSubFormID : glb_nSubFormID);
    
    var nTamanho = parseInt((selTamanhoImagem.options[selTamanhoImagem.selectedIndex].value), 10);
    var nOrdem = parseInt((selOrdemImagem.options[selOrdemImagem.selectedIndex].value), 10);
    var strVersao = (sVersao == '' ? 'a.Versao IS NULL AND ' : 'a.Versao = \'' + sVersao + '\' AND ');   
        
    var strSQL = 'SELECT TOP 1 a.Arquivo AS Imagem FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] a WITH(NOLOCK) ' +
	             'WHERE a.FormID = ' + glb_nCurrFormID + ' AND ' +
	             'a.SubFormID = ' +  nSubForm + ' AND ' +
	             'a.RegistroID = ' + glb_nRegistroID + ' AND ' + strVersao +
	             'a.TamanhoImagem = ' + nTamanho + ' AND ' +
	             'a.OrdemImagem = ' + nOrdem + ' AND ' +
                 'a.TipoArquivoID = 1451 ' +
	             'ORDER BY a.DocumentoID';
	
	var strFieldName = 'Imagem';
		
	openFile.SaveFileDataInDatabase(strDominium, strConn, strSQL, strFieldName, bErase);
}

/********************************************************************
Imagem deu erro no carregamento - campo IMG para verificar dimensoes
antes de gravar
********************************************************************/
function imgDims_onerror()
{
	var strError = 'Erro ao tentar gravar imagem no banco.';
	
	lockControlsInModalWin(false);
			
	if ( window.top.overflyGen.Alert(strError) == 0 )
		return null;
					
	return true;
}

/********************************************************************
Imagem carregou com sucesso - disco ou banco
********************************************************************/
function img_onload()
{
	lockControlsInModalWin(false);
	
	getImagemInServer_Finish(false);
}

/********************************************************************
Imagem nao foi carregada - disco ou banco
********************************************************************/
function img_onerror()
{
	lockControlsInModalWin(false);
	
	getImagemInServer_Finish(true);
}

/********************************************************************
Retorno do servidor com a imagem
********************************************************************/
function getImagemInServer_Finish(bError)
{
	if ( bError )
	{
		lblRegistroSemImagem.style.visibility = 'visible';
		
		img_Imagem.style.visibility = 'hidden';
	}	
	else
	{
		lblRegistroSemImagem.style.visibility = 'hidden';
		
		img_Imagem.style.visibility = 'visible';
		
		if ( glb_FirstTime )
			img_Imagem_ondblclick();
	}
	
	glb_FirstTime = false;	
	
	with (modalgerimagensBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    if (glb_ForceRealMode)
	{
		configureAndShowInterface();
		glb_ForceRealMode = false;
	}

    glb_GerImagensTimer = window.setInterval('showImage()', 500, 'JavaScript');
}

/********************************************************************
Mostra a janela modal com o arquivo carregado
********************************************************************/
function showImage()
{
	if (glb_GerImagensTimer != null)
	{
		window.clearInterval(glb_GerImagensTimer);
		glb_GerImagensTimer = null;
	}
		
	// mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    try
    {
		glb_LastSelFocused.focus();
		glb_LastSelFocused = null;
    }
    catch(e)
    {
		;
    }
}

/********************************************************************
Duplo clique na lista de arquivos do objeto openFile
********************************************************************/
function openFile_OnFileDblClkOrEnter()
{
	var strError;
	
	glb_validFileDoubleClicked = '';
	
	if ((openFile.FullFileName == null) || (openFile.FullFileName == ''))
	{
		strError = 'Nenhuma imagem selecionada.';
			
			if ( window.top.overflyGen.Alert(strError) == 0 )
				return null;
			
			lockControlsInModalWin(false);
				
			return true;
	}
	
	if ( fileExtensionIsValid(openFile.FileExtension.toUpperCase()) )
	{
		if (openFile.FileSizeInBytes > glb_Max_File_Size_KB)
		{
			strError = 'Tamanho da imagem excede ' +
			            glb_Max_File_Size_KB.toString() + ' kbytes.';
			
			if ( window.top.overflyGen.Alert(strError) == 0 )
				return null;
				
			lockControlsInModalWin(false);
				
			return true;
		}
		
		glb_validFileDoubleClicked = openFile.FullFileName;
	
		lblOrigemImagem.innerText = 'Imagem do Disco';
	    img_Imagem.src = openFile.FullFileName;
	}     
	else
	{
		lblOrigemImagem.innerText = 'Imagem do Disco';
	    img_Imagem.src = '';
	}
}

/********************************************************************
Valida a extensao do arquivo no disco
********************************************************************/
function fileExtensionIsValid( sFileExt)
{
	sFileExt = (trimStr(sFileExt)).toUpperCase();
	
	if ( ((openFile.PipeOfFileExt).toUpperCase()).lastIndexOf(sFileExt) >=0 )
		return true;
	else
		return false;	
}

/********************************************************************
Objeto openFile teve sucesso ao salvar o blob da imagem no banco
do servidor
********************************************************************/
function openFile_OnSaveFileSuccess()
{
	lockControlsInModalWin(false);
	
	glb_timerImagem = window.setInterval('loadImgFromServer()', 50, 'JavaScript');
}

/********************************************************************
Objeto openFile nao conseguiu salvar o blob da imagem no banco
do servidor
********************************************************************/
function openFile_OnSaveFileError(nError)
{
	// nError posiveis
	// 1 - Dominio nulo
	// 2 - String de conexao nula
	// 3 - String de select nula
	// 4 - Nome do campo na tabela nulo
	// 5 - Nome do arquivo nulo
	// 6 - Banco nao executou o update no registro da tabela

	lockControlsInModalWin(false);	
	
	var strError;
	
	if(nError == 5)
		strError = 'N�o tem arquivo de imagem selecionado.';
	else
		strError = 'Erro ao tentar salvar imagem no banco de dados.';
	
	if ( window.top.overflyGen.Alert(strError) == 0 )
		return null;
		
	glb_timerImagem = window.setInterval('loadImgFromServer()', 50, 'JavaScript');	
}

/********************************************************************
Objeto openFile teve sucesso ao apagar o blob da imagem no banco
do servidor
********************************************************************/
function openFile_OnEraseFileSuccess()
{
	lockControlsInModalWin(false);
	
	glb_timerImagem = window.setInterval('loadImgFromServer()', 50, 'JavaScript');
}

/********************************************************************
Objeto openFile nao conseguiu apagar o blob da imagem no banco
do servidor
********************************************************************/
function openFile_OnEraseFileError(nError)
{
    // nError posiveis
    // 1 - Dominio nulo
    // 2 - String de conexao nula
    // 3 - String de select nula
    // 4 - Nome do campo na tabela nulo
    // 6 - Banco nao executou o update no registro da tabela

	lockControlsInModalWin(false);

	if ( window.top.overflyGen.Alert('Erro ao tentar apagar imagem no banco de dados.') == 0 )
		return null;
		
	glb_timerImagem = window.setInterval('loadImgFromServer()', 50, 'JavaScript');	
}

/********************************************************************
Usuario deu um duplo clique na imagem
********************************************************************/
function img_Imagem_ondblclick()
{
	glb_ForceRealMode = false;

	configureAndShowInterface();
}

/********************************************************************
Usuario deu um duplo clique na imagem
********************************************************************/
function configureAndShowInterface()
{ 
	//mode 1 - Thumb, 2 - real
	
	var mode = img_Imagem.getAttribute('ThumbMode', 1);	
	var imgWidth;
	var imgHeight;
	
	if ( mode == null )
		mode = 1;
		
	if ( mode == 1 )	
		img_Imagem.setAttribute('ThumbMode', 2, 1);	
	else if ( mode == 2 )	
		img_Imagem.setAttribute('ThumbMode', 1, 1);
		
	if ( glb_ForceRealMode )
	{
		mode = 1;
		img_Imagem.setAttribute('ThumbMode', 2, 1);	
	}
		
	// Change to real mode	
	if (mode == 1)
	{
		// Troca innerText do label do ocx overOpenFile
		lblOverOpenFile.innerText = lblOrigemImagem.innerText;
		
		// Esconde o label da imagem
		lblOrigemImagem.style.visibility = 'hidden';
		
		if ( img_Imagem.currentStyle.visibility != 'hidden' )
		{
			with ( img_Imagem.style )		
			{
				width = 0;
				height = 0;
			}	
			with ( img_Imagem.style )		
			{
				width = 'auto';
				height = 'auto';
				left = parseInt(openFile.currentStyle.left, 10);
			}
		
			imgWidth = img_Imagem.offsetWidth;
			imgHeight = img_Imagem.offsetHeight;
			
			// Restringe o tamanho maximo do campo img_Imagem
			// a 500 x 384 pixels
			if ( imgWidth > 500 )
				img_Imagem.style.width = 500;
			
			if ( imgHeight > 384 )
				img_Imagem.style.height = 384;
		
			lblOverOpenFile.innerText = lblOverOpenFile.innerText + ' ' +
										imgWidth.toString() + ' x ' +
										imgHeight.toString() + ' pixels';						
		}
		else
		{
			lblOverOpenFile.innerText = lblOverOpenFile.innerText + ' ' +
										'0' + ' x ' +
										'0' + ' pixels';									
										
			with ( lblRegistroSemImagem.style )		
			{
				left = parseInt(openFile.currentStyle.left, 10);
			}							
		}
		
		// Trava e esconde o ocx open file
		openFile.Enabled = false;
		openFile.style.visibility = 'hidden';
		
		// Trava os botoes
		btnGravar.disabled = true;
		btnApagar.disabled = true;
		btnRefresh.disabled = true;
		
		// Troca hint da imagem
		img_Imagem.title = 'Duplo clique para thumbnail';
		
		// Troca o titulo do botao btnEditarVizualizar
		btnEditarVizualizar.value = 'Editar';
		
		//Trava os combos de tamanho e ordem de imagem para produtos
		//selTamanhoImagem.disabled = true;
		//selOrdemImagem.disabled = true;
	}
	// Change to thumbnail mode
	else if (mode == 2)
	{
		// Restaura o innertext do label do ocx overOpenFile
		lblOverOpenFile.innerText = 'Explorar no disco';
		
		// Mostra o label da imagem
		lblOrigemImagem.style.visibility = 'inherit';
		
		with ( img_Imagem.style )		
		{
			width = glb_Thumb_Width_PX;
			height = glb_Thumb_Height_PX;
			left = parseInt(lblOrigemImagem.currentStyle.left, 10);
		}
		
		with ( lblRegistroSemImagem.style )		
		{
			left = parseInt(img_Imagem.currentStyle.left, 10);
		}							
		
		// Destrava e mostra o ocx open file
		openFile.Enabled = true;
		openFile.style.visibility = 'inherit';
		
		// Destrava os botoes
		btnGravar.disabled = false;
		btnApagar.disabled = false;
		btnRefresh.disabled = false;
		
		// Troca hint da imagem
		img_Imagem.title = 'Duplo clique para tamanho real';
		
		// Troca o titulo do botao btnEditarVizualizar
		btnEditarVizualizar.value = 'Vizualizar';
		
		//Destrava os combos de tamanho e ordem de imagem para produtos
		selTamanhoImagem.disabled = false;
		selOrdemImagem.disabled = false;
	}
}

/********************************************************************
Usuario mudou o option do combo de tamanho ou do combo de ordem de imagem
********************************************************************/
function selTamanhoOrdemImagem_onchange()
{
	glb_LastSelFocused = this;

	// Ajusta o combo de ordem
	if ( this == selTamanhoImagem )
		fillCmbOrdem();
	
	// Seta dimensoes maximas aceitas para gravacao
	if ( this == selOrdemImagem )
		max_Img_Dimensions();
		
	if ( openFile.style.visibility == 'hidden' )	
		glb_ForceRealMode = true;
	else
		glb_ForceRealMode = false;
	
	// Troca a imagem, se existir no banco	
	loadImgFromServer();	
}

/********************************************************************
Preenche o combo de tamanho
Parametros:
********************************************************************/
function fillCmbTamanho()
{
	var i;
	var oOption;
	
	// limpa o combo
	for (i = selTamanhoImagem.length - 1; i >= 0; i--)
    {
        selTamanhoImagem.remove(i);
    }
    
    // preenche o combo
    for (i = 0; i < glb_cmbTamanhoData.length; i++)
    {
		oOption = document.createElement("OPTION");
		oOption.text = glb_cmbTamanhoData[i][0];
		oOption.value = glb_cmbTamanhoData[i][1];
		
		oOption.setAttribute('OrdemMaxima', glb_cmbTamanhoData[i][2], 1);
		oOption.setAttribute('LarguraMaxima', glb_cmbTamanhoData[i][3], 1);
		oOption.setAttribute('AlturaMaxima', glb_cmbTamanhoData[i][4], 1);
		oOption.setAttribute('TamanhoMaximo', glb_cmbTamanhoData[i][5], 1);
		
		selTamanhoImagem.add(oOption);
    }
    
    selTamanhoImagem.selectedIndex = 0;
    
    fillCmbOrdem();
}

/********************************************************************
Preenche o combo de ordem
Parametros:
********************************************************************/
function fillCmbOrdem()
{
	var i;
	var nOrdemMaxima;
	var oOption;
	
	// limpa o combo
	for (i = selOrdemImagem.length - 1; i >= 0; i--)
    {
        selOrdemImagem.remove(i);
    }

	// obter o atributo
	nOrdemMaxima = parseInt(selTamanhoImagem.options[selTamanhoImagem.selectedIndex].getAttribute('OrdemMaxima', 1), 10);
    
    // preenche o combo
    for (i = 0; i < nOrdemMaxima; i++)
    {
		oOption = document.createElement("OPTION");
		oOption.text = (i + 1).toString();
		oOption.value = (i + 1);
		
		selOrdemImagem.add(oOption);
    }
    selOrdemImagem.selectedIndex = 0;
    
    max_Img_Dimensions();
}

/********************************************************************
Define maximas dimensoes da imagem
********************************************************************/
function max_Img_Dimensions()
{
	glb_Max_Img_Width = parseInt(selTamanhoImagem.options[selTamanhoImagem.selectedIndex].getAttribute('LarguraMaxima', 1), 10);
	glb_Max_Img_Height = parseInt(selTamanhoImagem.options[selTamanhoImagem.selectedIndex].getAttribute('AlturaMaxima', 1), 10);
	glb_Max_File_Size_KB = parseInt(selTamanhoImagem.options[selTamanhoImagem.selectedIndex].getAttribute('TamanhoMaximo', 1), 10);
}

/********************************************************************
Ida ao banco para obtencao do relatorio das imagens de um registro de um form
********************************************************************/
function imagemRelatorio()
{
	var nSubForm = (glb_nSubFormID == 0 ? glb_nCurrSubFormID : glb_nSubFormID);
	
	var strPars = '?nFormID=' + escape(glb_nCurrFormID) +
		'&nSubFormID=' + escape(nSubForm) +
		'&nConceitoID=' + escape(glb_nRegistroID);
	
	setConnection(dsoRelatorioImagem);
	
	dsoRelatorioImagem.URL = SYS_ASPURLROOT + '/serversidegenEx/imagemrelatorio.aspx' + strPars;
	dsoRelatorioImagem.ondatasetcomplete = imagemRelatorio_DSC;
	dsoRelatorioImagem.refresh();
}

/********************************************************************
Volta do banco do relatorio das imagens de um registro de um form
********************************************************************/
function imagemRelatorio_DSC() {
    if (!(dsoRelatorioImagem.recordset.BOF && dsoRelatorioImagem.recordset.EOF))
	{
		dsoRelatorioImagem.recordset.MoveFirst();
		
		if (dsoRelatorioImagem.recordset["ImagemResumo"].value != null)
			lblRelatorioImagem.innerText = 'Imagens gravadas:\n\n'  + dsoRelatorioImagem.recordset["ImagemResumo"].value;		
		else
			lblRelatorioImagem.innerText = '';
			
	}
	else
		lblRelatorioImagem.innerText = '';
}

//Insert do registro na tabela de Imagens caso n�o exista. BJBN 03/04/2013
function insertImagem() 
{
    lockControlsInModalWin(true);

    var strPars = new String();
    var nTamanho, nOrdem;
    var sVersao = '';
    var nEstadoID = 0;
	
    if ((glb_nCurrFormID == 7110) && (glb_nCurrSubFormID == 26000)) 
    {
        if (glb_sCaller == 'S') 
        {
            nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");

            if (nEstadoID == 2)
                sVersao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Versao'].value");
        }
    }

    strPars = '?nFormID=' + escape(glb_nCurrFormID);

    if (glb_nSubFormID != 0)
        strPars += '&nSubFormID=' + escape(glb_nSubFormID);
    else
        strPars += '&nSubFormID=' + escape(glb_nCurrSubFormID);

    strPars += '&nRegistroID=' + escape(glb_nRegistroID);

    nTamanho = parseInt((selTamanhoImagem.options[selTamanhoImagem.selectedIndex].value), 10);
    nOrdem = parseInt((selOrdemImagem.options[selOrdemImagem.selectedIndex].value), 10);    

    strPars += '&nTamanho=' + escape(nTamanho);
    strPars += '&nOrdem=' + escape(nOrdem);
    strPars += '&sVersao=' + escape(sVersao);

    dsoInsertImagem.URL = SYS_ASPURLROOT + '/serversidegenEx/insertimage.aspx' + strPars;
    dsoInsertImagem.ondatasetcomplete = insertImagem_DSC;
    dsoInsertImagem.refresh();
}

function insertImagem_DSC() 
{
    //Prossegue com o processo de grava��o. BJBN 03/04/2013
    saveEraseImgInServer(false);
}