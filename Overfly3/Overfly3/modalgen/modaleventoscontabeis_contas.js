/********************************************************************
modaleventoscontabeis_contas.js

Library javascript para o modaleventoscontabeis_contas.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Controla se as colunas OK e Observacoes devem ser readOnly e funcao
// do Estado da OP

// FINAL DE VARIAVEIS GLOBAIS ***************************************
// Dsos genericos para banco de dados
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoGen02 = new CDatatransport("dsoGen02");
var dsoGen03 = new CDatatransport("dsoGen03");
// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_pedidos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaleventoscontabeis_contas_AfterEdit(Row, Col)
{
	;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modaleventoscontabeis_contas_BeforeEdit(grid, row, col)
{
	;
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modaleventoscontabeis_contasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Contas', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        // backgroundColor = 'transparent';
        backgroundColor = 'red';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        // top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = parseInt(btnOK.currentStyle.top, 10) +
				 parseInt(btnOK.currentStyle.height, 10) -
                 parseInt(top, 10);
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    btnOK.value = 'Fechar';
	btnOK.style.left = (parseInt(document.getElementById('divFG').currentStyle.width,10) /2) -
					   (parseInt(btnOK.style.width, 10) / 2);

	btnOK.style.visibility = 'hidden';
	btnCanc.style.visibility = 'hidden';
					   
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modaleventoscontabeis_contasBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    fg.focus();
}

function startPesq()
{
    lockControlsInModalWin(true);
    // dbo.fn_LanConta_Totais(a.LanContaID, 5) AS TotalConta

    strSQL = 'SELECT ISNULL(f.Fantasia, dbo.fn_Pessoa_Fantasia(dbo.fn_EventosContabeis_Empresa(a.EventoID), 0)) AS Unidade, b.ContaID, b.Conta, dbo.fn_EventoContabil_Totais(a.EventoContaID, 5) AS TotalConta, ' +
			'ISNULL(c.Fantasia, SPACE(1)) AS Pessoa, ' +
			'ISNULL(dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4), SPACE(1)) AS ContaBancaria, ' +
			'ISNULL(d.Imposto, SPACE(1)) AS Imposto, a.Valor ' +
			'FROM EventosContabeis_Contas a WITH (NOLOCK) ' +
			    'INNER JOIN PlanoContas b WITH (NOLOCK) ON (a.ContaID = b.ContaID) ' +
			    'LEFT OUTER JOIN Pessoas c WITH (NOLOCK) ON (a.PessoaID = c.PessoaID) ' +
			    'LEFT OUTER JOIN Conceitos d WITH (NOLOCK) ON (a.ImpostoID = d.ConceitoID) ' +
			    'LEFT JOIN Lancamentos_Contas e WITH(NOLOCK) ON (a.LanContaID = e.LanContaID) ' +
		        'LEFT JOIN Pessoas f WITH(NOLOCK) ON (e.UnidadeID = f.PessoaID) ' + 
			'WHERE (a.EventoID = ' + glb_nEventoID + ') ' +
			'ORDER BY (CASE WHEN a.Valor < 0 THEN 0 ELSE 1 END), a.ContaID, Pessoa, ContaBancaria, ' +
			'Imposto, ABS(a.Valor)';

    setConnection(dsoGen01);

    dsoGen01.SQL = strSQL;
    dsoGen01.ondatasetcomplete = startPesq_DSC;
    dsoGen01.refresh();
}

function startPesq_DSC() {
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;
    
    headerGrid(fg,['Unidade',
                   'ID',
                   'Conta',
                   'Total',
                   'Pessoa',
                   'Bco/Ag/Cta',
                   'Imposto',
                   'Valor'], []);

    fillGridMask(fg,dsoGen01,['Unidade',
                              'ContaID',
							  'Conta',
							  'TotalConta',
							  'Pessoa',
							  'ContaBancaria' ,
							  'Imposto',
							  'Valor'],
                              ['','9999999999','','999999999.99','','','','999999999.99'],
                              ['','##########','','(#,###,###,##0.00)','','','','(#,###,###,##0.00)']);
	setTotalColuns();
    var bTemPessoa = false;
    var bTemConta = false;
    var bTemImposto = false;
    for ( i=1; i<fg.Rows; i++ )
    {
		if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'Valor')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'Valor'), i, getColIndexByColKey(fg, 'Valor'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }

		if (fg.ValueMatrix(i, getColIndexByColKey(fg, 'TotalConta')) < 0 )
		{
			fg.Select(i, getColIndexByColKey(fg, 'TotalConta'), i, getColIndexByColKey(fg, 'TotalConta'));
			fg.FillStyle = 1;
            fg.CellForeColor = 0X0000FF;
        }
        
        if (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Pessoa'))) != '' )
			bTemPessoa = true;

        if (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'ContaBancaria'))) != '' )
			bTemConta = true;

        if (trimStr(fg.TextMatrix(i, getColIndexByColKey(fg, 'Imposto'))) != '' )
			bTemImposto = true;
	}
	
	fg.ColHidden(getColIndexByColKey(fg, 'Pessoa')) = !bTemPessoa;
	fg.ColHidden(getColIndexByColKey(fg, 'ContaBancaria')) = !bTemConta;
	fg.ColHidden(getColIndexByColKey(fg, 'Imposto')) = !bTemImposto;

    alignColsInGrid(fg,[1,3,7]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    // Merge de Colunas
    fg.MergeCells = 4;
    
    // fg.MergeCol(-1) = true;
    fg.MergeCol(0) = true;
    fg.MergeCol(1) = true;
    fg.MergeCol(2) = true;
    fg.MergeCol(3) = true;
    fg.MergeCol(4) = true;
    fg.MergeCol(5) = true;
    fg.MergeCol(6) = true;
    
    fg.Redraw = 2;
    
    if (fg.Rows > 1)
		fg.Row = 1;
    
    lockControlsInModalWin(false);
    btnOK.disabled = false;
	fg.focus();
}

function setTotalColuns()
{
	var nOldRow = fg.Row;
	
	gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[getColIndexByColKey(fg, 'Valor'),'##########','S']]);
	
	if (nOldRow >= 2)
		fg.Row = nOldRow;

    var i;
    var bHasRowsInGrid = fg.Rows > 1;
    var nValue = 0;
    var nCounter = 0;
    var nKey = '';

	if (bHasRowsInGrid)
	{
		for (i=2; i<fg.Rows; i++)
		{
			if (nKey != fg.TextMatrix(i, getColIndexByColKey(fg, 'ContaID')))
			{
				nCounter++;
				nKey = fg.TextMatrix(i, getColIndexByColKey(fg, 'ContaID'));
			}
		}

		fg.TextMatrix(1, getColIndexByColKey(fg, 'TotalConta')) = fg.TextMatrix(1, getColIndexByColKey(fg, 'Valor'));
		fg.TextMatrix(1, getColIndexByColKey(fg, 'Conta')) = nCounter;
	}
}
