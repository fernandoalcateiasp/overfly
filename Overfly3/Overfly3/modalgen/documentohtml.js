/********************************************************************
documentotemplate.js

Library javascript para o documentotemplate.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
FUNCOES GERAIS:
window_onload()
setupPage()
openSite(site)
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload() {
    // configuracao inicial do html
    setupPage();

    divDocumento.innerHTML = glb_sTexto;

    with (modaldocumentotemplateBody) {
        style.visibility = 'hidden';
        // scroll = 'no';
        leftMargin = 5;
        topMargin = 0;
        style.width = '100%';
        style.height = '100%';
        //style.visibility = 'inherit';
    }

    if (glb_nTemFoto == 1)
        resolveFoto();
    else
        showModalDocumentWin();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    var i;
    var oOption;

    with (divDocumento.style) {
        top = 0;
        left = 0;
        width = '100%';
        height = '100%';
        paddingTop = 5;
        paddingLeft = 5;
        paddingRight = 5;
        paddingBottom = 5;
    }
}

/********************************************************************
Abre um URL em uma nova janela do browser
********************************************************************/
function openSite(site) {
    window.open(site);
}

/********************************************************************
Cria tag de foto e carrega a foto, se for o caso
********************************************************************/
function resolveFoto() {
    var strPars = new String();

    if (glb_nTipoID == 3) {
        createTagForFoto();
        strPars = '?nFormID=' + escape(1210);
        strPars += '&nSubFormID=' + escape(20100);
        strPars += '&nTamanho=' +escape(2);
    }
    else {
        showModalDocumentWin();
        return true;
    }

    strPars += '&nRegistroID=' + escape(glb_nRegistroID);

    // carrega a imagem do servidor pelo arquivo imageblob.asp
    if (trimStr((SYS_ASPURLROOT.toUpperCase())).lastIndexOf('OVERFLY3') > 0)
        img_Foto.src = 'http' + ':/' + '/localhost/overfly3/serversidegenEx/imageblob.aspx' + strPars;
    else
        img_Foto.src = SYS_ASPURLROOT + '/serversidegenEx/imageblob.aspx' + strPars;

}

/********************************************************************
Cria a TAG para foto
********************************************************************/
function createTagForFoto() {
    var elem;

    elem = document.createElement("IMG");
    elem.id = 'img_Foto';
    elem.name = 'img_Foto';
    elem.className = 'imagemGen';
    window.document.appendChild(elem);
    elem.onload = img_onload;
    elem.onerror = img_onerror;

    with (elem.style) {
        position = 'absolute';
        left = 0;
        top = 0;
        width = 0;
        height = 0;
        visibility = 'hidden';
    }

    divDocumento.appendChild(elem);
}

/********************************************************************
Mostra a pagina e seu frame
********************************************************************/
function showModalDocumentWin() {
    // Forca scroll para o topo da pagina
    modaldocumentotemplateBody.scrollIntoView(true);

    // esta funcao destrava o html contido na janela modal
    window.parent.lockControlsInModalWin(false);

    with (modaldocumentotemplateBody) {
        style.visibility = 'visible';
    }

    window.parent.showDocumentFrame(true);
}

/********************************************************************
Carregou imagem
********************************************************************/
function img_onload() {
    with (img_Foto.style) {
        height = 'auto';
        width = 'auto';
    }

    with (img_Foto.style) {
        left = divDocumento.offsetWidth - img_Foto.offsetWidth;
        top = 0;
        visibility = 'inherit';
    }

    showModalDocumentWin();
}

/********************************************************************
Erro ao carregar imagem
********************************************************************/
function img_onerror() {
    showModalDocumentWin();
}

function btn_onclick(ctl) {
    ;
}

function imprimirResumo() {
    window.focus();
    window.print();
}