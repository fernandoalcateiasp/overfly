/********************************************************************
commonmodalprint.js

Library javascript para o modalprint.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Ajusta label e campo de pesquisa

Parametros:
lblOver         - referencia ao label do elemento graficamente colocado
                  acima do label do campo de pesquisa
elemOver        - referencia ao elemento graficamente colocado
                  acima do label do campo de pesquisa
lblFiltroRef    - referencia ao label do campo de pesquisa
txtFiltroRef    - referencia ao campo de pesquisa
    
Retorno:
irrelevante
********************************************************************/
function adjustPesqElements(lblOver, elemOver, lblFiltroRef, txtFiltroRef)
{
    var elem = null;
    var elemlSwp = null;

    // ajusta e alinha o label lblFiltroRef e o campo texto txtFiltroRef
	// obtem as coordenadas do elemento acima do lblFiltroRef
	if ( (lblOver != null) && (elemOver != null) )	
	{
		elem = lblFiltroRef;
		with (elem.style)
		{
			backgroundColor = 'transparent' ;
			left = parseInt(lblOver.currentStyle.left);
			top = parseInt(elemOver.currentStyle.top) + parseInt(elemOver.currentStyle.height) + ELEM_GAP;
			width = (elem.innerText).length * FONT_WIDTH;
            height = 16;
		}
		elemlSwp = elem;
		elem = txtFiltroRef;
		with (elem.style)
		{
			left = parseInt(elemlSwp.currentStyle.left);
			top = parseInt(elemlSwp.currentStyle.top) + parseInt(elemlSwp.currentStyle.height);
			width = parseInt(selReports.currentStyle.width);
            height = 24;
		}
	}	
	else
	{
		lblFiltroRef.style.visibility = 'hidden' ;
		txtFiltroRef.style.visibility = 'hidden' ;
		txtFiltroRef.disabled = true;
	}	
	
	return null;
}

/********************************************************************
Funcao criada pelo programador.
Trima, critica e normaliza (completa hota/min/seg) em campo datetime.
Tambem da mensagens ao usuario e foca o campo se for o caso.
    
O que pode estar digitado no campo
1. Qualquer coisa nao valida
2. So a data valida ou nao
3. A data e a hora valida ou nao
4. A data, a hora e o minuto valida ou nao
5. A data, a hora, o minuto e o segundo valida ou nao

O campo sera completado para dd/mm/yyyy hh:mm:ss
ou para mm/dd/yyyy hh:mm:ss

Parametro:
ctlRef     - referencia ao campo a criticar

Retorno:
true e data/hora validas, caso contrario false
********************************************************************/
function criticAndNormTxtDataTime(ctlRef)
{
    var retVal = false;
    var dataEstoque = '';
    var rExp, aDataEstoque;
    
    // referencia ao campo e obrigatoria
    if ( ctlRef == null )
        return retVal;
    
    // trima conteudo do campo ctlRef
    ctlRef.value = trimStr(ctlRef.value);
    
    // Obtem  texto do label associado ao campo
    var txtLblAss = labelAssociate(ctlRef.id);
    
    // 1. Campo em branco nao aceita
    if ( ctlRef.value == '' )
    {
        if ( window.top.overflyGen.Alert('O campo ' + txtLblAss + ' est� em branco...') == 0 )
            return null;
     
        lockControlsInModalWin(false);      
        ctlRef.focus();
        return retVal;
    }
    
    dataCtlRef = ctlRef.value;
    
    // A funcao chkDataEx valida o campos se ele e uma data valida
    if (chkDataEx(dataCtlRef, true) == false)
    {
        if ( window.top.overflyGen.Alert('O campo ' + txtLblAss + ' n�o � v�lido...') == 0 )
            return null;
        
        lockControlsInModalWin(false);      
        ctlRef.focus();
        return retVal;    
    }
 
    // Normaliza o conteudo do campo ctlRef
    ctlRef.value = normalizeDate_DateTime(ctlRef.value);
    
    if ( (ctlRef.value != null) && (ctlRef.value != '') )
        retVal = true;

    return retVal;
}
