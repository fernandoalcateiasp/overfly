/********************************************************************
bloquetocobranca.js

Library javascript para o bloquetocobranca.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_pixelToMm = 1.4 * 3.76;
var glb_nLineTick = 1;
var glb_tempWidth = 120;
var glb_tempHeightInPX = 0;

var glb_CellsNames = '10pt';

var glb_imgLogBanco = 0;

var glb_sReciboSacado = 'Recibo do Pagador';

var glb_sVencimentoLabel = 'Vencimento';
var glb_sAgenciaCodCedenteLabel = 'Ag�ncia / C�digo Benefici�rio';
var glb_sEspecieLabel = 'Esp�cie';
var glb_sQuantidadeLabel = 'Quantidade';

var glb_sValorDocumentoLabel = '(=) Valor do Documento';
var glb_sDescAbatLabel = '(-) Desconto / Abatimento';
var glb_sMoraMultaJurLabel = '(+) Mora / Multa / Juros';

var glb_sValorCobradoLabel = '(=) Valor Cobrado';
var glb_sNossoNumeroLabel = 'Nosso N�mero';
var glb_sNumeroDocumentoLabel = 'N� do Documento';

var glb_sSacadoLabel = 'Pagador';

var glb_sMsgSacadoLabel = 'Mensagem Pagador';

var glb_sAutentiMec = 'Autentica��o mec�nica';

var glb_sReciboEntrega = 'Recibo de Entrega';

var glb_sAssRecebedor = 'Assinatura do Recebedor';
var glb_sDataEntrega = 'Data de Entrega';

var glb_sLocalPagamentoLabel = 'Local de pagamento';

var glb_sCedenteLabel = 'Benefici�rio';

var glb_sDataDocumentoLabel = 'Data do Documento';
var glb_sEspecieDocLabel = 'Esp�cie Doc.';
var glb_sAceiteLabel = 'Aceite';
var glb_sDataProcessamentoLabel = 'Data do Processameto';

var glb_sNumContRespLabel = 'Uso do Banco';
var glb_sCarteiraLabel = 'Carteira';
var glb_sValorLabel = 'Valor';

var glb_sInstrucoesLabel = 'INSTRU��ES (Todas as informa��es deste bloqueto s�o de exclusiva responsabilidade do Sacador/Avalista) ';

var glb_sDescontoLabel = '(-) Desconto / Abatimento';
var glb_OutrasDedLabel = '(-) Outras dedu��es';
var glb_MoraMultaJurosLabel = '(+) Mora / Multa / Juros';
var glb_OutrosAcrescimos = '(+) Outros acr�scimos';
var glb_ValorCobrado = '(=) Valor cobrado';

var glb_sSacadoComEnderecoLabel = 'Pagador:';
var glb_SacadorAvalLabel = 'Sacador / Avalista: ';
var glb_CodBaixaLabel = 'C�digo de baixa';

var glb_Autentic_MecLabel = 'Autentica��o mec�nica';
var glb_FichCompLabel = 'Ficha de Compensa��o';

// FINAL DE VARIAVEIS GLOBAIS ***************************************


// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Window on Load - Configura o html
********************************************************************/
function window_onload() {
    // configuracao inicial do html
    setupPage();

    formatTheCurrencyValues();

    drawSkeletonBoletoCobranca();
    drawReciboSacado();
    drawReciboEntrega();
    drawFichaCompensacao();

    if (glb_bNotRecEntrega == true) {
        divReciboEntrega.style.visibility = 'hidden';

        divReciboSacado.style.top = divReciboEntrega.offsetTop;
        divFichaCompensacao.style.top = divReciboSacado.offsetTop +
		                                divReciboSacado.offsetHeight;
        divBoleto.style.height = divFichaCompensacao.offsetTop +
		                      divFichaCompensacao.offsetHeight;

        divMarginRightBottom.style.top = divBoleto.offsetTop +
		                                 divBoleto.offsetHeight;
    }

    with (bloquetocobrancaBody) {
        style.visibility = 'hidden';
        // scroll = 'no';
        leftMargin = 0;
        topMargin = 0;
        style.width = '100%';
        style.height = '100%';
        //style.visibility = 'inherit';
        style.border = 'none';
    }

    // Forca scroll para o topo da pagina
    bloquetocobrancaBody.scrollIntoView(true);

    // Define source da imagem do codigo de barras    
    defineSourceBarCode();

    img_LogBanco_RecSac.src = glb_sLogBancoSrc;
}

function formatTheCurrencyValues() {
    glb_sValorDocumento = formatCurrencyValue(glb_sValorDocumento, '.', ',');
    glb_sMoraMultaJur = formatCurrencyValue(glb_sMoraMultaJur, '.', ',');
    glb_sQuantidade = formatCurrencyValue(glb_sQuantidade, '.', ',');
}

function setupPage() {
    with (img_BarCode_FichComp.style) {
        visibility = 'hidden';
    }
}

function drawSkeletonBoletoCobranca() {
    var elem, elemBase, elemBase1, elemBase2;

    // 0XC0C0C0;
    var divsBGColor = 'transparent';

    // o certo (medido pelo modelo) e 93
    var DIVS_RECIBOS_WIDTH = 86 * 2;
    /*joao retirar*/var DIVS_RECIBOS_HEIGHT = 70; //47;
    var DIV_FICHA_HEIGHT = 100;

    var divReciboSacado_WH = [Math.ceil(glb_pixelToMm * DIVS_RECIBOS_WIDTH), Math.ceil(glb_pixelToMm * DIVS_RECIBOS_HEIGHT)];
    var divReciboEntrega_WH = [Math.ceil(glb_pixelToMm * DIVS_RECIBOS_WIDTH), Math.ceil(glb_pixelToMm * DIVS_RECIBOS_HEIGHT)];
    var divFichaCompensacao_WH = [null, Math.ceil(glb_pixelToMm * DIV_FICHA_HEIGHT)];

    var MARGIN_LEFT = 0;//Math.ceil(glb_pixelToMm * 5);
    var MARGIN_TOP = Math.ceil(glb_pixelToMm * 10);
    var MARGIN_RIGHT = 0;//Math.ceil(glb_pixelToMm * 5);
    var MARGIN_BOTTOM = Math.ceil(glb_pixelToMm * 10);

    // O DIV DAS MARGENS LEFT - TOP
    elem = divMarginLeftTop;
    with (elem.style) {
        fontFamily = 'Tahoma,Arial,Sans-Serif';
        fontSize = '0px';

        left = 0;
        top = 0;

        width = MARGIN_LEFT;
        height = MARGIN_TOP;

        borderStyle = 'none';
        borderWidth = 0;

        backgroundColor = 'transparent';
    }

    // OS DIVS INTERNOS DO BOLETO
    elem = divReciboEntrega;
    with (elem.style) {
        left = 0;
        top = 0;

        width = divReciboEntrega_WH[0];
        height = divReciboEntrega_WH[1];

        borderLeftStyle = 'none';
        borderLeftWidth = 0;
        borderTopStyle = 'dotted';
        borderTopWidth = 1;
        borderRightStyle = 'none';
        borderRightWidth = 0;
        borderBottomStyle = 'none';
        borderBottomWidth = 0;

        backgroundColor = divsBGColor;
    }

    elem = divReciboSacado;
    elemBase = divReciboEntrega;
    with (elem.style) {
        left = 0;
        top = elemBase.offsetTop + elemBase.offsetHeight;

        width = divReciboSacado_WH[0];
        height = divReciboSacado_WH[1];

        borderLeftStyle = 'none';
        borderLeftWidth = 0;
        borderTopStyle = 'dotted';
        borderTopWidth = 1;
        borderRightStyle = 'none';
        borderRightWidth = 0;
        borderBottomStyle = 'none';
        borderBottomWidth = 0;

        backgroundColor = divsBGColor;
    }

    elem = divFichaCompensacao;
    elemBase = divReciboSacado;
    elemBase1 = divReciboEntrega;
    with (elem.style) {
        left = elemBase.offsetLeft;
        top = elemBase.offsetTop + elemBase.offsetHeight;

        width = elemBase.offsetWidth;
        height = divFichaCompensacao_WH[1];

        borderLeftStyle = 'none';
        borderLeftWidth = 0;
        borderTopStyle = 'dotted';
        borderTopWidth = 1;
        borderRightStyle = 'none';
        borderRightWidth = 0;
        borderBottomStyle = 'dotted';
        borderBottomWidth = 1;

        backgroundColor = divsBGColor;
    }

    // O DIV QUE E O BOLETO
    elem = divBoleto;
    elemBase = divReciboSacado;
    elemBase1 = divReciboEntrega;
    elemBase2 = divFichaCompensacao;
    with (elem.style) {
        left = divMarginLeftTop.offsetLeft + divMarginLeftTop.offsetWidth;
        top = divMarginLeftTop.offsetTop + divMarginLeftTop.offsetHeight;
        width = elemBase.offsetWidth;
        height = elemBase.offsetHeight + elemBase1.offsetHeight + elemBase2.offsetHeight;

        borderStyle = 'none';
        borderWidth = 0;

        backgroundColor = 'transparent';
    }

    // O DIV DAS MARGENS RIGHT - BOTTOM
    elem = divMarginRightBottom;
    elemBase = divBoleto;
    with (elem.style) {
        fontFamily = 'Tahoma,Arial,Sans-Serif';
        fontSize = '0px';

        left = elemBase.offsetLeft + elemBase.offsetWidth;
        top = elemBase.offsetTop + elemBase.offsetHeight;

        width = MARGIN_RIGHT;
        height = MARGIN_BOTTOM;

        borderStyle = 'none';
        borderWidth = 0;

        backgroundColor = 'transparent';
    }
}

function drawReciboSacado() {
    drawReciboSacado_Line1();
    drawReciboSacado_Line2();
    drawReciboSacado_Line3();
    drawReciboSacado_Line4();
    drawReciboSacado_Line5();
    drawReciboSacado_Line6();
    drawReciboSacado_Line7();
}

function drawReciboSacado_Line1() {
    var elem, elemBase, elemBase1, elemBase2;

    var divsLogBancoBGColor = 'transparent';
    var divsBGColor = 'transparent';
    var labelsBGColor = 'transparent';

    var LEFT_LINES = 0;
    var TOP_FIRST_LINE = Math.ceil(glb_pixelToMm * 7);

    var LINHA_HEIGHT = 6.5;

    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var LOG_BANCO_W = 5;
    var LOG_BANCO_H = 5;
    // o certo (medido pelo modelo) e 35
    var LINHA_CEL_1_WIDTH = 32;
    var LINHA_CEL_2_WIDTH = 13;
    var LINHA_CEL_3_WIDTH = LINHA_WIDTH - (LOG_BANCO_W +
	                                           LINHA_CEL_1_WIDTH +
	                                           LINHA_CEL_2_WIDTH);

    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var logBanco_WH = [Math.ceil(glb_pixelToMm * LOG_BANCO_W), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var img_LogBanco_RecSac_WH = [Math.ceil(glb_pixelToMm * LOG_BANCO_W), Math.ceil(glb_pixelToMm * LOG_BANCO_H)];
    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Primeira linha: Logotipo do Banco, Nome do Banco,
    // CodigoDV do Banco, Recibo do sacado
    drawLineType(divRecSac_L1,
	              LEFT_LINES,
	              TOP_FIRST_LINE,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Celula Logotipo do Banco ( 5 X 5 mm)
    elem = divRecSac_LogBanco;
    with (elem.style) {
        left = LEFT_LINES;
        top = 0;

        width = logBanco_WH[0];
        height = logBanco_WH[1];

        borderLeftStyle = 'none';
        borderLeftWidth = 0;
        borderTopStyle = 'none';
        borderTopWidth = 0;
        borderRightStyle = 'none';
        borderRightWidth = 0;
        borderBottomStyle = 'solid';
        borderBottomWidth = glb_nLineTick;

        backgroundColor = divsLogBancoBGColor;
        visibility = 'hidden';
    }

    // Imagem do Logotipo do Banco
    elem = img_LogBanco_RecSac;
    with (elem.style) {
        left = 0;
        top = 0;

        width = img_LogBanco_RecSac_WH[0];
        height = img_LogBanco_RecSac_WH[1];
        visibility = 'hidden';
    }

    // Celula: Nome do Banco
    elem = lblRecSac_L1_NomeBanco;
    elemBase = divRecSac_LogBanco;

    drawCellTypeOneLabel(elem, glb_sNomeBanco,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          2, 6, 0,
	                      //'Arial', '9pt', 'bold', 'left',
                          'Arial', '13pt', 'bold', 'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: CodigoDV do Banco
    elem = lblRecSac_L1_CodigoDVBanco;
    elemBase = lblRecSac_L1_NomeBanco;

    drawCellTypeOneLabel(elem, glb_sCodigoDVBanco,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          5, 4, 0,
	                      //'Arial', '11pt', 'bold', 'left',
                          'Arial', '15pt', 'bold', 'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Recibo do Sacado
    elem = lblRecSac_L1_RecSac;
    elemBase = lblRecSac_L1_CodigoDVBanco;

    drawCellTypeOneLabel(elem, glb_sReciboSacado,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          10, 6, 0,
	                      //'Arial', '9pt', 'bold', 'left',
                          'Arial', '13pt', 'bold', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawReciboSacado_Line2() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecSac_L2;
    previousLine = divRecSac_L1;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    // o certo (medido pelo modelo) e 16.5
    var LINHA_CEL_1_WIDTH = 15;
    // o certo (medido pelo modelo) e 36
    var LINHA_CEL_2_WIDTH = 34;
    // o certo (medido pelo modelo) e 18
    var LINHA_CEL_3_WIDTH = 15;
    var LINHA_CEL_4_WIDTH = LINHA_WIDTH - (LINHA_CEL_1_WIDTH +
														  LINHA_CEL_2_WIDTH +
														  LINHA_CEL_3_WIDTH);

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_4_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_4_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Vencimento
    elemLabel = lblRecSac_L2_Vcto_Label;
    elemValue = lblRecSac_L2_Vcto_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sVencimentoLabel,
	                      0,
						  elemValue, glb_sVencimento,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Agencia / Codigo Cedente
    elemLabel = lblRecSac_L2_AgCodCed_Label;
    elemValue = lblRecSac_L2_AgCodCed_Value;
    elemBase = lblRecSac_L2_Vcto_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sAgenciaCodCedenteLabel,
	                      4,
						  elemValue, glb_sAgenciaCodCedente,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          4, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Especie
    elemLabel = lblRecSac_L2_Especie_Label;
    elemValue = lblRecSac_L2_Especie_Value;
    elemBase = lblRecSac_L2_AgCodCed_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sEspecieLabel,
	                      4,
						  elemValue, glb_sEspecie,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          4, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Quantidade
    elemLabel = lblRecSac_L2_Quantidade_Label;
    elemValue = lblRecSac_L2_Quantidade_Value;
    elemBase = lblRecSac_L2_Especie_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sQuantidadeLabel,
	                      4,
						  elemValue, glb_sQuantidade,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_4_WH[0], linha_CEL_4_WH[1],
                          0, 12, 4,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawReciboSacado_Line3() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecSac_L3;
    previousLine = divRecSac_L2;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    // o certo (medido pelo modelo) e 30.5
    var LINHA_CEL_1_WIDTH = 29;
    // o certo (medido pelo modelo) e 28.5
    var LINHA_CEL_2_WIDTH = 29;
    var LINHA_CEL_3_WIDTH = LINHA_WIDTH - (LINHA_CEL_1_WIDTH +
														  LINHA_CEL_2_WIDTH);

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];


    // Celula: Valor do Documento
    elemLabel = lblRecSac_L3_VlrDoc_Label;
    elemValue = lblRecSac_L3_VlrDoc_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sValorDocumentoLabel,
	                      0,
						  elemValue, glb_sValorDocumento,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 4,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Desconto / Abatimento
    elemLabel = lblRecSac_L3_DescAbat_Label;
    elemValue = lblRecSac_L3_DescAbat_Value;
    elemBase = lblRecSac_L3_VlrDoc_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sDescAbatLabel,
	                      4,
						  elemValue, glb_sDescAbat,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          0, 12, 4,
	                  //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Mora / Multa Juros
    elemLabel = lblRecSac_L3_MoraMultaJur_Label;
    elemValue = lblRecSac_L3_MoraMultaJur_Value;
    elemBase = lblRecSac_L3_DescAbat_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sMoraMultaJurLabel,
	                      4,
						  elemValue, glb_sMoraMultaJur,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          0, 12, 4,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawReciboSacado_Line4() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecSac_L4;
    previousLine = divRecSac_L3;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';
    // o certo (medido pelo modelo) e 30.5
    var LINHA_CEL_1_WIDTH = 29;
    // o certo (medido pelo modelo) e 28.5
    var LINHA_CEL_2_WIDTH = 29;
    var LINHA_CEL_3_WIDTH = LINHA_WIDTH - (LINHA_CEL_1_WIDTH +
														  LINHA_CEL_2_WIDTH);

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];


    // Celula: Valor Cobrado
    elemLabel = lblRecSac_L4_VlrCobrado_Label;
    elemValue = lblRecSac_L4_VlrCobrado_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sValorCobradoLabel,
	                      0,
						  elemValue, glb_sValorCobrado,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 4,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Nosso Numero
    elemLabel = lblRecSac_L4_NossoNumero_Label;
    elemValue = lblRecSac_L4_NossoNumero_Value;
    elemBase = lblRecSac_L4_VlrCobrado_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sNossoNumeroLabel,
	                      4,
						  elemValue, glb_sNossoNumero,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          4, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Numero do Documento
    elemLabel = lblRecSac_L4_NumeroDocumento_Label;
    elemValue = lblRecSac_L4_NumeroDocumento_Value;
    elemBase = lblRecSac_L4_NossoNumero_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sNumeroDocumentoLabel,
	                      4,
						  elemValue, glb_sNumeroDocumento,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          4, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawReciboSacado_Line5() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecSac_L5;
    previousLine = divRecSac_L4;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var LINHA_CEL_1_WIDTH = LINHA_WIDTH;

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Sacado
    elemLabel = lblRecSac_L5_Sacado_Label;
    elemValue = lblRecSac_L5_Sacado_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sSacadoLabel,
	                      0,
						  elemValue, glb_sSacado + '\n' + glb_sDocumentoSacado,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);

    // Excepcionalidades
    with (elemValue.style) {
        left = elemLabel.offsetLeft + Math.ceil(glb_pixelToMm * 10);
        top = 0;
        width = elemValue.offsetWidth - parseInt(elemValue.currentStyle.left, 10) - 1;
        paddingTop = parseInt(elemLabel.currentStyle.paddingTop, 10);
    }

    with (elemLabel.style) {
        width = elemValue.offsetLeft;
    }
}

function drawReciboSacado_Line6() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 15;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecSac_L6;
    previousLine = divRecSac_L5;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var LINHA_CEL_1_WIDTH = LINHA_WIDTH;

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Sacado
    elemLabel = lblRecSac_L6_MsgSacado_Label;
    elemValue = lblRecSac_L6_MsgSacado_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel,
                          ((glb_sCodigoDVBanco == "001-9" || glb_sCodigoDVBanco == "341-9") ? "Benefici�rio" : "Mensagem Pagador"),
	                      0,
						  elemValue, glb_sMensagemSacado,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          (glb_sCodigoDVBanco == "341-9" ? 12 : 0), 12, 0,
    //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);

    // Excepcionalidades
    with (elemValue.style) {
        left = elemLabel.offsetLeft + Math.ceil(glb_pixelToMm * 10);
        top = 0;
        width = elemValue.offsetWidth - parseInt(elemValue.currentStyle.left, 10) - 1;
        paddingTop = parseInt(elemLabel.currentStyle.paddingTop, 10);
    }

    with (elemLabel.style) {
        width = elemValue.offsetLeft;
    }
}

function drawReciboSacado_Line7() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecSac_L7;
    previousLine = divRecSac_L6;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'none',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var LINHA_CEL_1_WIDTH = LINHA_WIDTH;

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Autenticacao Mecanica
    elem = lblRecSac_L7_AutentiMec;
    elemBase = null;

    drawCellTypeOneLabel(elem, glb_sAutentiMec,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 3, 0,
                          'Arial', '12px', 'normal', 'center',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawReciboEntrega() {
    drawReciboEntrega_Line1();
    drawReciboEntrega_Line2();
    drawReciboEntrega_Line3();
    drawReciboEntrega_Line4();
    drawReciboEntrega_Line5();
}

function drawReciboEntrega_Line1() {
    var elem, elemBase, elemBase1, elemBase2;

    var divsLogBancoBGColor = 'transparent';
    var divsBGColor = 'transparent';
    var labelsBGColor = 'transparent';

    // var LEFT_LINES = Math.ceil(glb_pixelToMm * 3);
    var LEFT_LINES = 0;
    var TOP_FIRST_LINE = Math.ceil(glb_pixelToMm * 7);

    var LINHA_HEIGHT = 6.5;

    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var LOG_BANCO_W = 5;
    var LOG_BANCO_H = 5;
    // o certo (medido pelo modelo) e 35
    var LINHA_CEL_1_WIDTH = 32;
    var LINHA_CEL_2_WIDTH = 13;
    var LINHA_CEL_3_WIDTH = LINHA_WIDTH - (LOG_BANCO_W +
	                                           LINHA_CEL_1_WIDTH +
	                                           LINHA_CEL_2_WIDTH);

    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var logBanco_WH = [Math.ceil(glb_pixelToMm * LOG_BANCO_W), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var img_LogBanco_RecEntr_WH = [Math.ceil(glb_pixelToMm * LOG_BANCO_W), Math.ceil(glb_pixelToMm * LOG_BANCO_H)];
    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Primeira linha: Logotipo do Banco, Nome do Banco,
    // CodigoDV do Banco, Recibo de Entrega
    drawLineType(divRecEntr_L1,
	              LEFT_LINES,
	              TOP_FIRST_LINE,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Celula Logotipo do Banco ( 5 X 5 mm)
    elem = divRecEntr_LogBanco;
    with (elem.style) {
        left = 0;
        top = 0;

        width = logBanco_WH[0];
        height = logBanco_WH[1];

        borderLeftStyle = 'none';
        borderLeftWidth = 0;
        borderTopStyle = 'none';
        borderTopWidth = 0;
        borderRightStyle = 'none';
        borderRightWidth = 0;
        borderBottomStyle = 'solid';
        borderBottomWidth = glb_nLineTick;

        backgroundColor = divsLogBancoBGColor;
        visibility = 'hidden';
    }

    // Imagem do Logotipo do Banco
    elem = img_LogBanco_RecEntr;
    with (elem.style) {
        left = 0;
        top = 0;

        width = img_LogBanco_RecEntr_WH[0];
        height = img_LogBanco_RecEntr_WH[1];
        visibility = 'hidden';
    }

    // Celula: Nome do Banco
    elem = lblRecEntr_L1_NomeBanco;
    elemBase = divRecEntr_LogBanco;

    drawCellTypeOneLabel(elem, glb_sNomeBanco,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          2, 6, 0,
	                      //'Arial', '9pt', 'bold', 'left',
                          'Arial', '13pt', 'bold', 'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: CodigoDV do Banco
    elem = lblRecEntr_L1_CodigoDVBanco;
    elemBase = lblRecEntr_L1_NomeBanco;

    drawCellTypeOneLabel(elem, glb_sCodigoDVBanco,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          5, 4, 0,
	                      //'Arial', '9pt', 'bold', 'left',
                          'Arial', '15pt', 'bold', 'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Recibo de Entrega
    elem = lblRecEntr_L1_RecEntr;
    elemBase = lblRecEntr_L1_CodigoDVBanco;

    drawCellTypeOneLabel(elem, glb_sReciboEntrega,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          10, 6, 0,
	                      //'Arial', '9pt', 'bold', 'left',
                          'Arial', '13pt', 'bold', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawReciboEntrega_Line2() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecEntr_L2;
    previousLine = divRecEntr_L1;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    // o certo (medido pelo modelo) e 16.5
    var LINHA_CEL_1_WIDTH = 15;
    // o certo (medido pelo modelo) e 36 
    var LINHA_CEL_2_WIDTH = 34;
    // o certo (medido pelo modelo) e 18
    var LINHA_CEL_3_WIDTH = 15;
    var LINHA_CEL_4_WIDTH = LINHA_WIDTH - (LINHA_CEL_1_WIDTH +
														  LINHA_CEL_2_WIDTH +
														  LINHA_CEL_3_WIDTH);

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_4_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_4_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Vencimento
    elemLabel = lblRecEntr_L2_Vcto_Label;
    elemValue = lblRecEntr_L2_Vcto_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sVencimentoLabel,
	                      0,
						  elemValue, glb_sVencimento,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Agencia / Codigo Cedente
    elemLabel = lblRecEntr_L2_AgCodCed_Label;
    elemValue = lblRecEntr_L2_AgCodCed_Value;
    elemBase = lblRecEntr_L2_Vcto_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sAgenciaCodCedenteLabel,
	                      4,
						  elemValue, glb_sAgenciaCodCedente,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          4, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Especie
    elemLabel = lblRecEntr_L2_Especie_Label;
    elemValue = lblRecEntr_L2_Especie_Value;
    elemBase = lblRecEntr_L2_AgCodCed_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sEspecieLabel,
	                      4,
						  elemValue, glb_sEspecie,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          4, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Quantidade
    elemLabel = lblRecEntr_L2_Quantidade_Label;
    elemValue = lblRecEntr_L2_Quantidade_Value;
    elemBase = lblRecEntr_L2_Especie_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sQuantidadeLabel,
	                      4,
						  elemValue, glb_sQuantidade,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_4_WH[0], linha_CEL_4_WH[1],
                          0, 12, 4,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawReciboEntrega_Line3() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecEntr_L3;
    previousLine = divRecEntr_L2;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    // o certo (medido pelo modelo) e 30.5
    var LINHA_CEL_1_WIDTH = 29;
    var LINHA_CEL_2_WIDTH = LINHA_WIDTH - (LINHA_CEL_1_WIDTH);

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Valor do Documento
    elemLabel = lblRecEntr_L3_VlrDoc_Label;
    elemValue = lblRecEntr_L3_VlrDoc_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sValorDocumentoLabel,
	                      0,
						  elemValue, glb_sValorDocumento,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 4,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Nosso Numero
    elemLabel = lblRecEntr_L3_NossoNumero_Label;
    elemValue = lblRecEntr_L3_NossoNumero_Value;
    elemBase = lblRecEntr_L3_VlrDoc_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sNossoNumeroLabel,
	                      4,
						  elemValue, glb_sNossoNumero,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          4, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawReciboEntrega_Line4() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecEntr_L4;
    previousLine = divRecEntr_L3;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var LINHA_CEL_1_WIDTH = LINHA_WIDTH;

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Sacado
    elemLabel = lblRecEntr_L4_Sacado_Label;
    elemValue = lblRecEntr_L4_Sacado_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sSacadoLabel,
	                      0,
						  elemValue, glb_sSacado + '\n' + glb_sDocumentoSacado,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
	                      //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);

    // Excepcionalidades
    with (elemValue.style) {
        left = elemLabel.offsetLeft + Math.ceil(glb_pixelToMm * 10);
        top = 0;
        width = elemValue.offsetWidth - parseInt(elemValue.currentStyle.left, 10) - 1;
        paddingTop = parseInt(elemLabel.currentStyle.paddingTop, 10);
    }

    with (elemLabel.style) {
        width = elemValue.offsetLeft;
    }
}

function drawReciboEntrega_Line5() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 89
    var LINHA_WIDTH = 83;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divRecEntr_L5;
    previousLine = divRecEntr_L4;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    // o certo (medido pelo modelo) e 64
    var LINHA_CEL_1_WIDTH = 58;
    var LINHA_CEL_2_WIDTH = LINHA_WIDTH - (LINHA_CEL_1_WIDTH);

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Assinatura do Recebedor
    elem = lblRecEntr_L5_AssRecebedor;
    elemBase = null;

    drawCellTypeOneLabel(elem, glb_sAssRecebedor,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 2, 0,
	                      //'Arial', '8px', 'normal', 'left',
                          'Arial', '10px', 'normal', 'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Data de Entrega
    elem = lblRecEntr_L5_DataEntrega;
    elemBase = lblRecEntr_L5_AssRecebedor;

    drawCellTypeOneLabel(elem, glb_sDataEntrega,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          4, 2, 0,
	                      //'Arial', '8px', 'normal', 'left',
                          'Arial', '10px', 'normal', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao() {
    drawFichaCompensacao_Line1();
    drawFichaCompensacao_Line2();
    drawFichaCompensacao_Line3();
    drawFichaCompensacao_Line4();
    drawFichaCompensacao_Line5();

    drawFichaCompensacao_Line6_R_1();
    drawFichaCompensacao_Line6_R_2();
    drawFichaCompensacao_Line6_R_3();
    drawFichaCompensacao_Line6_R_4();
    drawFichaCompensacao_Line6_R_5();

    drawFichaCompensacao_Line6_L();

    drawFichaCompensacao_Line7();
    drawFichaCompensacao_Line8();

    drawBarCode();
}

function drawFichaCompensacao_Line1() {
    var elem, elemBase, elemBase1, elemBase2;

    var divsLogBancoBGColor = 'transparent';
    var divsBGColor = 'transparent';
    var labelsBGColor = 'transparent';

    var LEFT_LINES = 0;
    var TOP_FIRST_LINE = Math.ceil(glb_pixelToMm * 1.5);

    var LINHA_HEIGHT = 6.5;

    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;
    var LOG_BANCO_W = 5;
    var LOG_BANCO_H = 5;
    // o certo (medido pelo modelo) e 37
    var LINHA_CEL_1_WIDTH = 39;
    // o certo (medido pelo modelo) e 13.5
    var LINHA_CEL_2_WIDTH = 12;
    var LINHA_CEL_3_WIDTH = LINHA_WIDTH - (LOG_BANCO_W +
	                                           LINHA_CEL_1_WIDTH +
	                                           LINHA_CEL_2_WIDTH);

    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var logBanco_WH = [Math.ceil(glb_pixelToMm * LOG_BANCO_W), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var img_LogBanco_FichComp_WH = [Math.ceil(glb_pixelToMm * LOG_BANCO_W), Math.ceil(glb_pixelToMm * LOG_BANCO_H)];
    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Primeira linha: Logotipo do Banco, Nome do Banco,
    // CodigoDV do Banco, Recibo do sacado
    drawLineType(divFichComp_L1,
	              LEFT_LINES,
	              TOP_FIRST_LINE,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Celula Logotipo do Banco ( 5 X 5 mm)
    elem = divFichComp_LogBanco;
    with (elem.style) {
        left = LEFT_LINES;
        top = 0;

        width = logBanco_WH[0];
        height = logBanco_WH[1];

        borderLeftStyle = 'none';
        borderLeftWidth = 0;
        borderTopStyle = 'none';
        borderTopWidth = 0;
        borderRightStyle = 'none';
        borderRightWidth = 0;
        borderBottomStyle = 'solid';
        borderBottomWidth = glb_nLineTick;

        backgroundColor = divsLogBancoBGColor;
        visibility = 'hidden';
    }

    // Imagem do Logotipo do Banco
    elem = img_LogBanco_FichComp;
    with (elem.style) {
        left = 0;
        top = 0;

        width = img_LogBanco_FichComp_WH[0];
        height = img_LogBanco_FichComp_WH[1];
        visibility = 'hidden';
    }

    // Celula: Nome do Banco
    elem = lblFichComp_L1_NomeBanco;
    elemBase = divFichComp_LogBanco;

    drawCellTypeOneLabel(elem, glb_sNomeBanco,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          2, 6, 0,
                          'Arial', '15pt', 'bold', 'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: CodigoDV do Banco
    elem = lblFichComp_L1_CodigoDVBanco;
    elemBase = lblFichComp_L1_NomeBanco;

    drawCellTypeOneLabel(elem, glb_sCodigoDVBanco,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          5, 6, 0,
                          'Arial', '15pt', 'bold', 'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Linha digitavel
    elem = lblFichComp_L1_LinDigit;
    elemBase = lblFichComp_L1_CodigoDVBanco;

    drawCellTypeOneLabel(elem, glb_sLinhaDigitavel,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          6, 6, 0,
                          'Arial', '15pt', 'bold', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line2() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L2;
    previousLine = divFichComp_L1;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';
    // o certo (medido pelo modelo) e 122
    var LINHA_CEL_1_WIDTH = glb_tempWidth;
    var LINHA_CEL_2_WIDTH = LINHA_WIDTH - (LINHA_CEL_1_WIDTH);

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Local de pagamento
    elemLabel = lblFichComp_L2_LocalPgto_Label;
    elemValue = lblFichComp_L2_LocalPgto_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sLocalPagamentoLabel,
	                      0,
						  elemValue, glb_sLocalPagamento,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
                          //'Arial', '7pt', 'bold',
	                      'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Vencimento
    elemLabel = lblFichComp_L2_Vcto_Label;
    elemValue = lblFichComp_L2_Vcto_Value;
    elemBase = lblFichComp_L2_LocalPgto_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sVencimentoLabel,
	                      4,
						  elemValue, glb_sVencimento,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          /*4, 12, 0,*/0, 12, 4,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line3() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L3;
    previousLine = divFichComp_L2;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';
    // o certo (medido pelo modelo) e 122
    var LINHA_CEL_1_WIDTH = glb_tempWidth;
    var LINHA_CEL_2_WIDTH = LINHA_WIDTH - (LINHA_CEL_1_WIDTH);

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Cedente
    elemLabel = lblFichComp_L3_Cedente_Label;
    elemValue = lblFichComp_L3_Cedente_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sCedenteLabel,
	                      0,
						  elemValue, glb_sCedente,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Excepcionalidades
    with (elemValue.style) {
        left = elemLabel.offsetLeft + Math.ceil(glb_pixelToMm * 10);
        top = 0;
        width = elemValue.offsetWidth - parseInt(elemValue.currentStyle.left, 10) - 1;
        paddingTop = parseInt(elemLabel.currentStyle.paddingTop, 10);
    }

    // Celula: Agencia / Codigo Cedente
    elemLabel = lblFichComp_L3_AgCodCed_Label;
    elemValue = lblFichComp_L3_AgCodCed_Value;
    elemBase = lblFichComp_L3_Cedente_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sAgenciaCodCedenteLabel,
	                      4,
						  elemValue, glb_sAgenciaCodCedente,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          /*4, 12, 0,*/0, 12, 4,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line4() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L4;
    previousLine = divFichComp_L3;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var LINHA_CEL_1_WIDTH = 27;
    var LINHA_CEL_2_WIDTH = 28;
    var LINHA_CEL_3_WIDTH = 19;
    var LINHA_CEL_4_WIDTH = 10;
    var LINHA_CEL_5_WIDTH = glb_tempWidth - (LINHA_CEL_1_WIDTH +
	                                         LINHA_CEL_2_WIDTH +
	                                         LINHA_CEL_3_WIDTH +
	                                         LINHA_CEL_4_WIDTH);
    var LINHA_CEL_6_WIDTH = LINHA_WIDTH - glb_tempWidth;

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_4_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_4_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_5_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_5_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_6_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_6_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Data do documento
    elemLabel = lblFichComp_L4_DataDocumento_Label;
    elemValue = lblFichComp_L4_DataDocumento_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sDataDocumentoLabel,
	                      0,
						  elemValue, glb_sDataDocumento,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Numero do documento
    elemLabel = lblFichComp_L4_NumeroDocumento_Label;
    elemValue = lblFichComp_L4_NumeroDocumento_Value;
    elemBase = lblFichComp_L4_DataDocumento_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sNumeroDocumentoLabel,
	                      4,
						  elemValue, glb_sNumeroDocumento,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          4, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Especie do documento
    elemLabel = lblFichComp_L4_EspecieDocumento_Label;
    elemValue = lblFichComp_L4_EspecieDocumento_Value;
    elemBase = lblFichComp_L4_NumeroDocumento_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sEspecieDocLabel,
	                      4,
						  elemValue, glb_sEspecieDoc,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          4, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Aceite
    elemLabel = lblFichComp_L4_Aceite_Label;
    elemValue = lblFichComp_L4_Aceite_Value;
    elemBase = lblFichComp_L4_EspecieDocumento_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sAceiteLabel,
	                      4,
						  elemValue, glb_sAceite,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_4_WH[0], linha_CEL_4_WH[1],
                          4, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Data do Processamento
    elemLabel = lblFichComp_L4_DataProcessamento_Label;
    elemValue = lblFichComp_L4_DataProcessamento_Value;
    elemBase = lblFichComp_L4_Aceite_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sDataProcessamentoLabel,
	                      4,
						  elemValue, glb_sDataProcessamento,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_5_WH[0], linha_CEL_5_WH[1],
                          4, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Excepcionalidade a partir desta linha
    elemValue.style.width = Math.ceil(glb_pixelToMm * glb_tempWidth) -
                                      (elemBase.offsetLeft + elemBase.offsetWidth);


    // Celula: Nosso numero
    elemLabel = lblFichComp_L4_NossoNumero_Label;
    elemValue = lblFichComp_L4_NossoNumero_Value;
    elemBase = lblFichComp_L4_DataProcessamento_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sNossoNumeroLabel,
	                      4,
						  elemValue, glb_sNossoNumero,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_6_WH[0], linha_CEL_6_WH[1],
                          /*4, 12, 0,*/0, 12, 4,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);

}

function drawFichaCompensacao_Line5() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L5;
    previousLine = divFichComp_L4;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var LINHA_CEL_1_WIDTH = 27;
    var LINHA_CEL_2_WIDTH = 18;
    var LINHA_CEL_3_WIDTH = 10;
    var LINHA_CEL_4_WIDTH = 30;
    var LINHA_CEL_5_WIDTH = glb_tempWidth - (LINHA_CEL_1_WIDTH +
	                                         LINHA_CEL_2_WIDTH +
	                                         LINHA_CEL_3_WIDTH +
	                                         LINHA_CEL_4_WIDTH);
    var LINHA_CEL_6_WIDTH = LINHA_WIDTH - glb_tempWidth;

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_2_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_2_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_3_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_3_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_4_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_4_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_5_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_5_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];
    var linha_CEL_6_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_6_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    // Celula: Numero da conta / Responsavel
    elemLabel = lblFichComp_L5_NumContResp_Label;
    elemValue = lblFichComp_L5_NumContResp_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sNumContRespLabel,
	                      0,
						  elemValue, glb_sNumContResp,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Carteira
    elemLabel = lblFichComp_L5_Carteira_Label;
    elemValue = lblFichComp_L5_Carteira_Value;
    elemBase = lblFichComp_L5_NumContResp_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sCarteiraLabel,
	                      4,
						  elemValue, glb_sCarteira,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_2_WH[0], linha_CEL_2_WH[1],
                          4, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Especie
    elemLabel = lblFichComp_L5_Especie_Label;
    elemValue = lblFichComp_L5_Especie_Value;
    elemBase = lblFichComp_L5_Carteira_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sEspecieLabel,
	                      4,
						  elemValue, glb_sEspecie,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_3_WH[0], linha_CEL_3_WH[1],
                          4, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Celula: Quantidade
    elemLabel = lblFichComp_L5_Quantidade_Label;
    elemValue = lblFichComp_L5_Quantidade_Value;
    elemBase = lblFichComp_L5_Especie_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sQuantidadeLabel,
	                      4,
						  elemValue, glb_sQuantidade,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_4_WH[0], linha_CEL_4_WH[1],
                          0, 12, 4,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // O sinal de multiplicacao
    lblFichComp_L5_Multiplic.innerText = 'X';
    with (lblFichComp_L5_Multiplic.style) {
        width = 10;
        height = 12;
        left = elemValue.offsetLeft + elemValue.offsetWidth -
		       (parseInt(lblFichComp_L5_Multiplic.currentStyle.width, 10) / 2);
        top = (linha_WH[1] - parseInt(lblFichComp_L5_Multiplic.currentStyle.height, 10)) / 2;

        fontFamily = 'Arial';
        //fontSize = '7pt';
        fontSize = '10pt';
        fontWeight = 'bold';
        textAlign = 'center';
        paddingTop = 1;

        backgroundColor = 'white';
        zIndex = 99;
    }

    // Celula: Valor
    elemLabel = lblFichComp_L5_Valor_Label;
    elemValue = lblFichComp_L5_Valor_Value;
    elemBase = lblFichComp_L5_Quantidade_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sValorLabel,
	                      4,
						  elemValue, glb_sValor,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_5_WH[0], linha_CEL_5_WH[1],
                          0, 12, 4,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);

    // Excepcionalidade a partir desta linha
    elemValue.style.width = Math.ceil(glb_pixelToMm * glb_tempWidth) -
                                      (elemBase.offsetLeft + elemBase.offsetWidth);


    // Celula: Valor do documento
    elemLabel = lblFichComp_L5_VlrDoc_Label;
    elemValue = lblFichComp_L5_VlrDoc_Value;
    elemBase = lblFichComp_L5_Valor_Value;

    drawCellTypeTwoLabel(elemLabel, glb_sValorDocumentoLabel,
	                      4,
						  elemValue, glb_sValorDocumento,
	                      elemBase.offsetLeft + elemBase.offsetWidth, 0,
	                      linha_CEL_6_WH[0], linha_CEL_6_WH[1],
                          0, 12, 4,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'right',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);

}

function drawFichaCompensacao_Line6_R_1() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;

    var linha_WH = [Math.ceil(glb_pixelToMm * (LINHA_WIDTH - glb_tempWidth)), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L6_R_1;
    previousLine = divFichComp_L5;

    var thisLineLeft = Math.ceil(glb_pixelToMm * glb_tempWidth) - 1;

    drawLineType(thisLine,
	              thisLineLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    glb_tempHeightInPX += linha_WH[1];

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var linha_CEL_1_WH = [linha_WH[0], linha_WH[1]];

    // Celula: Desconto
    elemLabel = lblFichComp_L6_R_1_Desconto_Label;
    elemValue = null;
    elemBase = null;

    drawCellTypeOneLabel(elemLabel, glb_sDescontoLabel,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          4, 2, 0,
                          //'Arial', '8px', 'normal', 'left',
                          'Arial', '11px', 'normal', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line6_R_2() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;

    var linha_WH = [Math.ceil(glb_pixelToMm * (LINHA_WIDTH - glb_tempWidth)), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L6_R_2;
    previousLine = divFichComp_L6_R_1;

    var thisLineLeft = Math.ceil(glb_pixelToMm * glb_tempWidth) - 1;

    drawLineType(thisLine,
	              thisLineLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');


    glb_tempHeightInPX += linha_WH[1];

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var linha_CEL_1_WH = [linha_WH[0], linha_WH[1]];

    // Celula: Outras deducoes
    elemLabel = lblFichComp_L6_R_2_OutrasDed_Label;
    elemValue = null;
    elemBase = null;

    drawCellTypeOneLabel(elemLabel, glb_OutrasDedLabel,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          4, 2, 0,
                          //'Arial', '8px', 'normal', 'left',
                          'Arial', '11px', 'normal', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line6_R_3() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;

    var linha_WH = [Math.ceil(glb_pixelToMm * (LINHA_WIDTH - glb_tempWidth)), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L6_R_3;
    previousLine = divFichComp_L6_R_2;

    var thisLineLeft = Math.ceil(glb_pixelToMm * glb_tempWidth) - 1;

    drawLineType(thisLine,
	              thisLineLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    glb_tempHeightInPX += linha_WH[1];

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var linha_CEL_1_WH = [linha_WH[0], linha_WH[1]];

    // Celula: Mora / Multa / Juros
    elemLabel = lblFichComp_L6_R_3_MoraMultaJuros_Label;
    elemValue = null;
    elemBase = null;

    drawCellTypeOneLabel(elemLabel, glb_MoraMultaJurosLabel,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          4, 2, 0,
                          //'Arial', '8px', 'normal', 'left',
                          'Arial', '11px', 'normal', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line6_R_4() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;

    var linha_WH = [Math.ceil(glb_pixelToMm * (LINHA_WIDTH - glb_tempWidth)), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L6_R_4;
    previousLine = divFichComp_L6_R_3;

    var thisLineLeft = Math.ceil(glb_pixelToMm * glb_tempWidth) - 1;

    drawLineType(thisLine,
	              thisLineLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    glb_tempHeightInPX += linha_WH[1];

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var linha_CEL_1_WH = [linha_WH[0], linha_WH[1]];

    // Celula: Outros Acrescimos
    elemLabel = lblFichComp_L6_R_4_OutrosAcrescimos_Label;
    elemValue = null;
    elemBase = null;

    drawCellTypeOneLabel(elemLabel, glb_OutrosAcrescimos,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          4, 2, 0,
                          //'Arial', '8px', 'normal', 'left',
                          'Arial', '11px', 'normal', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line6_R_5() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 6;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;

    var linha_WH = [Math.ceil(glb_pixelToMm * (LINHA_WIDTH - glb_tempWidth)), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L6_R_5;
    previousLine = divFichComp_L6_R_4;

    var thisLineLeft = Math.ceil(glb_pixelToMm * glb_tempWidth) - 1;

    drawLineType(thisLine,
	              thisLineLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    glb_tempHeightInPX += linha_WH[1];

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var linha_CEL_1_WH = [linha_WH[0], linha_WH[1]];

    // Celula: Valor Cobrado
    elemLabel = lblFichComp_L6_R_5_ValorCobrado_Label;
    elemValue = null;
    elemBase = null;

    drawCellTypeOneLabel(elemLabel, glb_ValorCobrado,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          4, 2, 0,
                          //'Arial', '8px', 'normal', 'left',
                          'Arial', '11px', 'normal', 'left',
                          'none', 'none',
                          'none', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line6_L() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var linha_WH = [Math.ceil(glb_pixelToMm * glb_tempWidth), glb_tempHeightInPX];

    thisLine = divFichComp_L6_L;
    previousLine = divFichComp_L5;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var linha_CEL_1_WH = [linha_WH[0], linha_WH[1]];

    // Celula: Instrucoes
    elemLabel = lblFichComp_L6_L_Instrucoes_Label;
    elemValue = lblFichComp_L6_L_Instrucoes_Value;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_sInstrucoesLabel,
	                      0,
						  elemValue, glb_sInstrucoes,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 12, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'solid', 'none',
                          labelsBGColor);
}

function drawFichaCompensacao_Line7() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 13;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L7;
    previousLine = divFichComp_L6_L;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'solid',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var LINHA_CEL_1_WIDTH = LINHA_WIDTH;
    var LINHA_CEL_1_HEIGHT = 9.8;

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_CEL_1_HEIGHT)];

    // Celula: Sacado
    elemLabel = lblFichComp_L7_Sacado_Label;
    elemValue = lblFichComp_L7_Sacado_Value;
    elemBase = null;

    var elemValueMarginLeft = Math.ceil(glb_pixelToMm * 10);

    drawCellTypeTwoLabel(elemLabel, glb_sSacadoComEnderecoLabel,
	                      0,
						  elemValue, glb_sSacadoComEndereco,
	                      0, 0,
	                      linha_CEL_1_WH[0] - elemValueMarginLeft, linha_CEL_1_WH[1],
                          0, 3, 0,
                          //'Arial', '7pt', 'bold',
                          'Arial', glb_CellsNames, 'bold',
                          'left',
                          'none', 'none',
                          'none', 'none',
                          'transparent');

    // Excepcionalidades
    elemLabel.style.paddingTop = parseInt(elemValue.currentStyle.paddingTop, 10);
    elemLabel.style.width = elemValueMarginLeft;

    elemValue.style.left = elemValueMarginLeft;

    // Celula: Sacador / Avalista
    elemLabel = lblFichComp_L7_SacadorAval;
    elemValue = null;
    elemBase = lblFichComp_L7_Sacado_Value;

    // Excepcionalidade
    var elemLabelLeft = 0;
    var elemLabelWidth = 65;
    var elemLabelHeight = 2.6;

    drawCellTypeOneLabel(elemLabel, glb_SacadorAvalLabel + glb_SacadorAval,
	                      0,
	                      elemBase.offsetTop + elemBase.offsetHeight,
	                      Math.ceil(glb_pixelToMm * elemLabelWidth),
	                      Math.ceil(glb_pixelToMm * elemLabelHeight),
                          0, 0, 0,
                          //'Arial', '8px', 'normal', 'left',
                          'Arial', '11px', 'normal', 'left',
                          'none', 'none',
                          'none', 'none',
                          'transparent');

    // Celula: Codigo de baixa
    elemLabel = lblFichComp_L7_CodBaixa;
    elemValue = null;
    elemBase = lblFichComp_L7_Sacado_Value;

    // Excepcionalidade
    elemLabelLeft = 130;
    elemLabelWidth = 30;
    elemLabelHeight = 2.6;

    drawCellTypeOneLabel(elemLabel, glb_CodBaixaLabel,
	                      Math.ceil(glb_pixelToMm * elemLabelLeft),
	                      elemBase.offsetTop + elemBase.offsetHeight,
	                      Math.ceil(glb_pixelToMm * elemLabelWidth),
	                      Math.ceil(glb_pixelToMm * elemLabelHeight),
                          0, 0, 0,
                          //'Arial', '8px', 'normal', 'left',
                          'Arial', '11px', 'normal', 'left',
                          'none', 'none',
                          'none', 'none',
                          'transparent');
}

function drawFichaCompensacao_Line8() {
    // Define e desenha a linha
    var thisLine, previousLine;

    var LINHA_HEIGHT = 5.1;
    // o certo (medido pelo modelo) e 183
    var LINHA_WIDTH = 172;
    var linha_WH = [Math.ceil(glb_pixelToMm * LINHA_WIDTH), Math.ceil(glb_pixelToMm * LINHA_HEIGHT)];

    thisLine = divFichComp_L8;
    previousLine = divFichComp_L7;

    drawLineType(thisLine,
	              previousLine.offsetLeft,
	              previousLine.offsetTop + previousLine.offsetHeight,
	              linha_WH[0], linha_WH[1],
	              'none', 'none', 'none', 'none',
	              'transparent');

    // Define e desenha as celulas	              
    var elemLabel;
    var elemValue, elemBase, elemBase1, elemBase2;

    var labelsBGColor = 'transparent';

    var LINHA_CEL_1_WIDTH = LINHA_WIDTH;
    var LINHA_CEL_1_HEIGHT = 5.1;

    var linha_CEL_1_WH = [Math.ceil(glb_pixelToMm * LINHA_CEL_1_WIDTH), Math.ceil(glb_pixelToMm * LINHA_CEL_1_HEIGHT)];

    // Celula: Autenticacao Mecanica e Ficha de compensacao
    elemLabel = lblFichComp_L8_Autentic_Mec;
    elemValue = lblFichComp_L8_FichComp;
    elemBase = null;

    drawCellTypeTwoLabel(elemLabel, glb_Autentic_MecLabel,
	                      0,
						  elemValue, glb_FichCompLabel,
	                      0, 0,
	                      linha_CEL_1_WH[0], linha_CEL_1_WH[1],
                          0, 3, 0,
                          //'Arial', '6pt', 'bold', 'left',
                          'Arial', '9pt', 'bold', 'left',
                          'none', 'none',
                          'none', 'none',
                          'transparent');

    // Excepcionalidades
    var elemLabelMarginLeft = Math.ceil(glb_pixelToMm * 107);
    var elemLabelWidth = Math.ceil(glb_pixelToMm * 25);
    var elemValueMarginLeft = Math.ceil(glb_pixelToMm * 133);
    var elemValueWidth = Math.ceil(glb_pixelToMm * 38);

    with (elemLabel.style) {
        paddingTop = parseInt(elemValue.currentStyle.paddingTop, 10);
        left = elemLabelMarginLeft;
        width = elemLabelWidth;
        //backgroundColor = 'yellow';
    }

    with (elemValue.style) {
        width = elemValueWidth;
        left = elemValueMarginLeft;
        //backgroundColor = 'yellow';
    }
}

function drawBarCode() {
    //var barCodeWidth_InMM = 111;
    //var barCodeHeight_InMM = 12;

    var barCodeWidth_InMM = 110.51;
    var barCodeHeight_InMM = 16.9;

    with (img_BarCode_FichComp.style) {
        left = 0;
        top = divFichComp_L8.offsetTop + divFichComp_L8.offsetHeight - 4;
        //Math.ceil(glb_pixelToMm * barCodeWidth_InMM)
        //width = 'auto';
        width = Math.ceil(glb_pixelToMm * barCodeWidth_InMM);
        //Math.ceil(glb_pixelToMm * barCodeHeight_InMM)
        //height = 'auto';
        height = Math.ceil(glb_pixelToMm * barCodeHeight_InMM);
    }
}

function drawLineType(elemLine, nLeft, nTop, nWidth, nHeight, sBorderLeftStyle, sBorderTopStyle, sBorderRightStyle, sBorderBottomStyle, sBGColor) {
    with (elemLine.style) {
        left = nLeft;
        top = nTop;

        width = nWidth;
        height = nHeight;

        borderLeftStyle = sBorderLeftStyle;
        borderLeftWidth = glb_nLineTick;
        borderTopStyle = sBorderTopStyle;
        borderTopWidth = glb_nLineTick;
        borderRightStyle = sBorderRightStyle;
        borderRightWidth = glb_nLineTick;
        borderBottomStyle = sBorderBottomStyle;
        borderBottomWidth = glb_nLineTick;

        backgroundColor = sBGColor;
    }
}

function drawCellTypeOneLabel(elemLabel, sText, nLeft, nTop, nWidth, nHeight, nPadLeft, nPadTop, nPadRight, sFontFamily, sFontSize, sFontWeight, sTextAlign, sBorderLeftStyle, sBorderTopStyle, sBorderRightStyle, sBorderBottomStyle, sBGColor) {
    elemLabel.innerText = sText;
    with (elemLabel.style) {
        left = nLeft;
        top = nTop;

        width = nWidth;
        height = nHeight;

        paddingLeft = nPadLeft;
        paddingTop = nPadTop;
        paddingRight = nPadRight;

        fontFamily = sFontFamily;
        fontSize = sFontSize;
        fontWeight = sFontWeight;
        textAlign = sTextAlign;

        borderLeftStyle = sBorderLeftStyle;
        borderLeftWidth = glb_nLineTick;
        borderTopStyle = sBorderTopStyle;
        borderTopWidth = glb_nLineTick;
        borderRightStyle = sBorderRightStyle;
        borderRightWidth = glb_nLineTick;
        borderBottomStyle = sBorderBottomStyle;
        borderBottomWidth = glb_nLineTick;

        backgroundColor = sBGColor;
    }
}

function drawCellTypeTwoLabel(elemCellLabel, sCellText, nLeftCellLabel, elemLabel, sText, nLeft, nTop, nWidth, nHeight, nPadLeft, nPadTop, nPadRight, sFontFamily, sFontSize, sFontWeight, sTextAlign, sBorderLeftStyle, sBorderTopStyle, sBorderRightStyle, sBorderBottomStyle, sBGColor) {
    elemCellLabel.innerText = sCellText;
    with (elemCellLabel.style) {
        left = nLeftCellLabel + nLeft;
        top = 0;

        width = nWidth - nLeftCellLabel - 1;
        height = 10;

        paddingLeft = 0;
        paddingTop = 2;

        fontFamily = sFontFamily;
        fontSize = '12px';
        fontWeight = 'normal';

        borderLeftStyle = 'none';
        borderLeftWidth = 0;
        borderTopStyle = 'none';
        borderTopWidth = 0;
        borderRightStyle = 'none';
        borderRightWidth = 0;
        borderBottomStyle = 'none';
        borderBottomWidth = 0;

        backgroundColor = 'transparent';
    }

    elemLabel.innerText = sText;
    with (elemLabel.style) {
        left = nLeft;
        top = nTop;

        width = nWidth;
        height = nHeight;

        paddingLeft = nPadLeft;
        paddingTop = nPadTop + 4;
        paddingRight = nPadRight;

        fontFamily = sFontFamily;
        fontSize = sFontSize;
        fontWeight = sFontWeight;

        textAlign = sTextAlign;

        borderLeftStyle = sBorderLeftStyle;
        borderLeftWidth = glb_nLineTick;
        borderTopStyle = sBorderTopStyle;
        borderTopWidth = glb_nLineTick;
        borderRightStyle = sBorderRightStyle;
        borderRightWidth = glb_nLineTick;
        borderBottomStyle = sBorderBottomStyle;
        borderBottomWidth = glb_nLineTick;

        backgroundColor = sBGColor;
    }
}

function img_onload(ctl) {
    if (ctl == img_LogBanco_RecSac) {
        divRecSac_LogBanco.style.visibility = 'inherit';
        ctl.style.visibility = 'inherit';
    }
    else if (ctl == img_LogBanco_RecEntr) {
        divRecEntr_LogBanco.style.visibility = 'inherit';
        ctl.style.visibility = 'inherit';
    }
    else if (ctl == img_LogBanco_FichComp) {
        divFichComp_LogBanco.style.visibility = 'inherit';
        ctl.style.visibility = 'inherit';
    }
    else if (ctl == img_LogBanco_FichComp) {
        divFichComp_LogBanco.style.visibility = 'inherit';
        ctl.style.visibility = 'inherit';
    }
    else if (ctl == img_BarCode_FichComp) {
        ctl.style.visibility = 'inherit';
    }

    showBoletoCobranca();
}

function img_onerror(ctl) {
    if (ctl == img_LogBanco_RecSac) {
        lblRecSac_L1_NomeBanco.style.left = divRecSac_LogBanco.offsetLeft;
    }
    else if (ctl == img_LogBanco_RecEntr) {
        lblRecEntr_L1_NomeBanco.style.left = divRecEntr_LogBanco.offsetLeft;
    }
    else if (ctl == img_LogBanco_FichComp) {
        lblFichComp_L1_NomeBanco.style.left = divFichComp_LogBanco.offsetLeft;
    }
    else if (ctl == img_BarCode_FichComp) {
        // Nao faz nada
        var i = 0;
    }

    showBoletoCobranca();
}

function showBoletoCobranca() {
    glb_imgLogBanco++;

    if (glb_imgLogBanco == 1) {
        img_LogBanco_RecEntr.src = glb_sLogBancoSrc;
        return null;
    }
    else if (glb_imgLogBanco == 2) {
        img_LogBanco_FichComp.src = glb_sLogBancoSrc;
        return null;
    }
    else if (glb_imgLogBanco == 3) {
        img_BarCode_FichComp.src = glb_sBarCodeSrc;
        return null;
    }

    if (glb_imgLogBanco != 4)
        return null;

    with (bloquetocobrancaBody) {
        style.visibility = 'visible';
    }

    window.parent.showDocumentFrame(true);

    // esta funcao destrava o html contido na janela modal
    window.parent.lockControlsInModalWin(false);
}

function defineSourceBarCode() {
    var strPars = new String();

    strPars = '?nBarCodeNumber=' + escape(glb_sCodigoBarras);
    strPars += '&nNarrowBarsWidth=' + escape(1);
    strPars += '&nNarrowBarsHeight=' + escape(49);
    strPars += '&nRatio=' + escape(3);

    glb_sBarCodeSrc += '/serversidegenEx/barcodeimage.asp' + strPars;
}

function imprimirBoleto() {
    window.focus();
    bloquetocobrancaBody.focus();
    window.print();
}

