<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modaldocumentosHtml" name="modaldocumentosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modaldocumentos.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/modaldocumentos.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nRegistroID, nA1, nA2, nC1, nC2, nE1, nE2, nI
Dim nFormID, nSubFormID, nUserID, nProprietarioAlternativo, nTipoRegistroID

nRegistroID = "0"
nTipoRegistroID = 0

For i = 1 To Request.QueryString("RegistroID").Count    
    nRegistroID = Request.QueryString("RegistroID")(i)
Next

For i = 1 To Request.QueryString("A1").Count    
    nA1 = Request.QueryString("A1")(i)
Next

For i = 1 To Request.QueryString("A2").Count    
    nA2 = Request.QueryString("A2")(i)
Next

For i = 1 To Request.QueryString("C1").Count
    nC1 = Request.QueryString("C1")(i)
Next

For i = 1 To Request.QueryString("C2").Count
    nC2 = Request.QueryString("C2")(i)
Next

For i = 1 To Request.QueryString("E1").Count
    nE1 = Request.QueryString("E1")(i)
Next

For i = 1 To Request.QueryString("E2").Count
    nE2 = Request.QueryString("E2")(i)
Next

For i = 1 To Request.QueryString("I").Count
    nI = Request.QueryString("I")(i)
Next

For i = 1 To Request.QueryString("FormID").Count
    nFormID = Request.QueryString("FormID")(i)
Next

For i = 1 To Request.QueryString("SubFormID").Count
    nSubFormID = Request.QueryString("SubFormID")(i)
Next

For i = 1 To Request.QueryString("UserID").Count
    nUserID = Request.QueryString("UserID")(i)
Next

For i = 1 To Request.QueryString("nTipoRegistroID").Count    
    nTipoRegistroID = Request.QueryString("nTipoRegistroID")(i)
Next

Dim rsSPCommand
Set rsSPCommand = Server.CreateObject("ADODB.Command")

With rsSPCommand
	.CommandTimeout = 60 * 10
	.ActiveConnection = strConn

	.CommandText = "sp_Documentos_Direito"
	.CommandType = adCmdStoredProc

	.Parameters.Append( .CreateParameter("@UsuarioID", adInteger, adParamInput) )
	.Parameters("@UsuarioID").Value = CLng(nUserID)

	.Parameters.Append( .CreateParameter("@FormID", adInteger, adParamInput) )
	.Parameters("@FormID").Value = CLng(nFormID)

	.Parameters.Append( .CreateParameter("@SubFormID", adInteger, adParamInput) )
	.Parameters("@SubFormID").Value = CLng(nSubFormID)

	.Parameters.Append( .CreateParameter("@RegistroID", adInteger, adParamInput) )
	.Parameters("@RegistroID").Value = CLng(nRegistroID)

	.Parameters.Append( .CreateParameter("@Resultado", adInteger, adParamOutput) )

	.Execute
End With

nProprietarioAlternativo = rsSPCommand.Parameters("@Resultado").Value

Response.Write "var glb_nRegistroID = " & CStr(nRegistroID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nA1 = " & CStr(nA1) & ";"
Response.Write vbcrlf

Response.Write "var glb_nA2 = " & CStr(nA2) & ";"
Response.Write vbcrlf

Response.Write "var glb_nC1 = " & CStr(nC1) & ";"
Response.Write vbcrlf

Response.Write "var glb_nC2 = " & CStr(nC2) & ";"
Response.Write vbcrlf

Response.Write "var glb_nE1 = " & CStr(nE1) & ";"
Response.Write vbcrlf

Response.Write "var glb_nE2 = " & CStr(nE2) & ";"
Response.Write vbcrlf

Response.Write "var glb_nI = " & CStr(nI) & ";"
Response.Write vbcrlf

Response.Write "var glb_nProprietarioAlternativo = " & CStr(nProprietarioAlternativo) & ";"
Response.Write vbcrlf

Response.Write "var glb_nSubFormID=" & CStr(nSubFormID) & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoRegistroID=" & CStr(nTipoRegistroID) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<SCRIPT LANGUAGE=javascript>
    function chama() {
        ListaArquivo();
    }
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 fg_modaldocumentos_AfterEdit (arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
 fg_modaldocumentosDblClick();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
    if (glb_GridIsBuilding)
        return;

    js_fg_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
//-->
</SCRIPT>

</head>

<body id="modaldocumentosBody" name="modaldocumentosBody" LANGUAGE="javascript" onload="return window_onload()">
     
     <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </object>
     </div>

    <div id="divImg" name="divImg" class="divGeneral">
        <IMG ID="img_Imagem" name="img_Imagem" class="imgGeneral" src=""></IMG>
    </div>

     <div id="divControls" name="divControls" class="paraNormal">
         <input type="button" id="btnNovo" name="btnNovo" value="Novo" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
         <input type="button" id="btnExcluir" name="btnExcluir" value="Excluir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
         <input type="button" id="btnDownload" name="btnDownload" value="Download" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
         <input type="button" id="btnRefresh" name="btnRefresh" value="Refresh" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
     </div>
     
     <div id="divUpload" name="divUpload" class="divGeneral">
        <input type="button" id="btnCancelar" name="btnCancelar" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <p id="lblTipoArquivo" name="lblTipoArquivo" class="lblGeneral"></p>
        <select id="selTipoArquivo" name="selTipoArquivo"  class="fldGeneral"></select>
        <p id="lblTamanhoImagem" name="lblTamanhoImagem" class="lblGeneral"></p>
        <select id="selTamanhoImagem" name="selTamanhoImagem"  class="fldGeneral"></select>
        <p id="lblOrdemImagem" name="lblOrdemImagem" class="lblGeneral"></p>
        <select id="selOrdemImagem" name="selOrdemImagem"  class="fldGeneral"></select>
        <div id="divIframe" name="divIframe" class="divGeneral" frameborder="0" ondblclick="previewImage(true)">
            <iframe id="I1" frameborder="0" name="I1" height="50" width="360" scrolling="no" ></iframe>
        </div>
     </div>
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Voltar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">

    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>

</body>

</html>
