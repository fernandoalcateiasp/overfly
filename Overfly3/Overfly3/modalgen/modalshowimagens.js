/********************************************************************
modalshowimagens.js

Library javascript para o modalshowimagens.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_LASTLINESELID = '';

var glb_nCurrFormID = 0;
var glb_nCurrSubFormID = 0;

var glb_showImagesTimer = null;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_nCurrFormID = window.top.formID;
	
	glb_nCurrSubFormID = window.top.subFormID;
	
	//alert( glb_nCurrFormID + '\n' + glb_nCurrSubFormID);
	
	// Para lista de precos alterar
	// Form e SubForm ids para conceitos
	if ( glb_nCurrFormID == 5220 )
		glb_nCurrFormID = 2110;
		
	if ( glb_nCurrSubFormID == 24230 )	
		glb_nCurrSubFormID = 21000;

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    loadImgFromServer();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_' + glb_sCaller), null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_' + glb_sCaller), null );    

}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Imagem', 1);
    
    with (img_Imagem.style)
	{
		width = '10';
		height = '10';
	}
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var elem;
    var nNextX = 0;
    var nBtnGap = ELEM_GAP;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre antes de mover o botao OK
    widthFree = modWidth - frameBorder;
    
    // campo de imagem
    with (img_Imagem.style)
	{
		left = (widthFree - img_Imagem.offsetWidth) / 2;
		top = topFree + ELEM_GAP;
		visibility = 'hidden';
	}
    
	// mensagem de registro sem imagem
	with (lblRegistroSemImagem.style)
    {
		backgroundColor = 'transparent';
		fontSize = '10pt';
		fontWeight = 'bold';
		textAlign = 'center';
		left = 0;
		width = widthFree;
		height = 36;
		top = parseInt(img_Imagem.currentStyle.top, 10);
		visibility = 'hidden';
    }
    
    // btnOK desabilitado e oculto
    elem = btnOK;
    with (elem.style)
    {
		left = 0;
		top = 0;
		width = 0;
		height = 0;
		visibility = 'hidden';
    }
    elem.disabled = true;
    
    // btnCanc e oculto
    elem = btnCanc;
    with (elem.style)
    {
		left = 0;
		top = 0;
		width = 0;
		height = 0;
		visibility = 'hidden';
    }
}

/********************************************************************
Carrega blob da imagem do banco de dados do servidor
********************************************************************/
function loadImgFromServer()
{
	lockControlsInModalWin(true);
	
	var strPars = new String();
	var sVersao = '';
	var nEstadoID = 0;
	var nTamanho = 2;

	if ((glb_nCurrFormID == 7110) && (glb_nCurrSubFormID == 26000)) 
	{
	    if (glb_sCaller == 'S') 
	    {
	        nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['EstadoID'].value");
	        nTamanho = 4;
	        
	        if (nEstadoID == 2) 
	        {
	            sVersao = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, "dsoSup01.recordset['Versao'].value");
	        }
	        
	    }
	}
    
    strPars = '?nFormID=' + escape(glb_nCurrFormID);
    
	if (glb_nSubFormID != 0)
		strPars += '&nSubFormID=' + escape(glb_nSubFormID);
	else
		strPars += '&nSubFormID=' + escape(glb_nCurrSubFormID);

    strPars += '&nRegistroID=' + escape(glb_nRegistroID);
    strPars += '&nTamanho=' + escape(nTamanho);
    strPars += '&sVersao=' + escape(sVersao);
    
	// carrega a imagem do servidor pelo arquivo imageblob.asp
	if ( trimStr((SYS_ASPURLROOT.toUpperCase())).lastIndexOf('OVERFLY3') > 0 )
		img_Imagem.src = 'http' + ':/' + '/localhost/overfly3/serversidegenEx/imageblob.aspx' + strPars;
	else
		img_Imagem.src = SYS_ASPURLROOT + '/serversidegenEx/imageblob.aspx' + strPars;
}

/********************************************************************
Imagem carregou com sucesso - disco ou banco
********************************************************************/
function img_onload()
{
	if (glb_showImagesTimer != null)
	{
		window.clearInterval(glb_showImagesTimer);
		
		glb_showImagesTimer = null;
	}
	else
	{
		glb_showImagesTimer = window.setInterval('img_onload()', 100, 'JavaScript');
		
		return true;
	}
	
	with (img_Imagem.style)
	{
		width = 'auto';
		height = 'auto';
	}
	
	var imgWidth = img_Imagem.offsetWidth;
	var imgHeight = img_Imagem.offsetHeight;
			
	// Restringe o tamanho maximo do campo img_Imagem
	// a 500 x 384 pixels
	if ( imgWidth > 500 )
		img_Imagem.style.width = 500;
			
	if ( imgHeight > 384 )
			img_Imagem.style.height = 384;

	lockControlsInModalWin(false);
	
	getImagemInServer_Finish(false);
}

/********************************************************************
Imagem nao foi carregada - disco ou banco
********************************************************************/
function img_onerror()
{
	lockControlsInModalWin(false);
	
	with (img_Imagem.style)
	{
		width = 0;
		height = 0;
	}
	
	getImagemInServer_Finish(true);
}

/********************************************************************
Retorno do servidor com a imagem
********************************************************************/
function getImagemInServer_Finish(bError)
{
	var nModalWidth;
	var nModalHeight;
	
	if ( bError )
	{
		nModalWidth = 200;
		nModalHeight = 96;
				
		// reajusta dimensoes e reposiciona a janela
		redimAndReposicionModalWin(nModalWidth, nModalHeight, false);
		
		// mostrar a janela movido para eventos de carregamento da imagem
		loadDataAndTreatInterface();
	
		lblRegistroSemImagem.style.visibility = 'visible';
		
		img_Imagem.style.visibility = 'hidden';
	}	
	else
	{
		nModalWidth = img_Imagem.offsetLeft + img_Imagem.offsetWidth +
		              img_Imagem.offsetLeft + 12;
		
		if ( nModalWidth < 120 )
			nModalWidth = 120;
				              
		nModalHeight = img_Imagem.offsetTop + img_Imagem.offsetHeight +
		               ELEM_GAP + btnCanc.offsetHeight + ELEM_GAP;
		
		if ( nModalHeight < 96 )
			nModalHeight = 96;
				
		// reajusta dimensoes e reposiciona a janela
		redimAndReposicionModalWin(nModalWidth, nModalHeight, false);
		
		// mostrar a janela movido para eventos de carregamento da imagem
		loadDataAndTreatInterface();
	
		lblRegistroSemImagem.style.visibility = 'hidden';
		
		img_Imagem.style.visibility = 'visible';
	}

	with (modalshowimagensBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
               
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}
