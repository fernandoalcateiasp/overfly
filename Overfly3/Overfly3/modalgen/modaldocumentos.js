/********************************************************************
modaldocumentos.js

Library javascript para o modaldocumentos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoListaArquivo = new CDatatransport("dsoListaArquivo");
var dsoExcluir = new CDatatransport("dsoExcluir");
var dsoArquivos = new CDatatransport("dsoArquivos");
var dsoTiposArquivos = new CDatatransport("dsoTiposArquivos");
var glb_nCurrFormID;
var glb_nCurrSubFormID;
var glb_nCurrUserID;
var glb_cmbTamanhoData = new Array();   //Label Tamanho, ID do Tamanho, Ordem Maxima, Largura Maxima, Altura Maxima, Tamanho Maximo (KB)
var glb_bFirstTimeCmbSelTipoArquivo = true;
var glb_permiteAlterarImagem = true;
var IMG_BRANCO = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP/##yH5BAEAAAAALAAAAAABAAEAAAIBRAA7".replace('#', '/').replace('#', '/');

// Dimensoes maximas da imagem
var glb_Max_Img_Width = 0;
var glb_Max_Img_Height = 0;
var glb_Max_File_Size_KB = 0;

//var aEmpresa = getCurrEmpresaData();
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES

FUNCOES GERAIS:
window_onload()
setupPage()
btn_onclick(ctl)
btnNovo_onclick()
btnExcluir_onclick()
btnDownload_onclick()
btnCancelar_onclick()
ListaArquivo()
ListaArquivo_DSC() 
Download()
Excluir()
Excluir_DSC()

Eventos de grid particulares desta janela:
js_modaldocumentos_AfterEdit(Row, Col)
fg_modaldocumentosDblClick()
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {

    glb_nCurrFormID = window.top.formID;
    glb_nCurrSubFormID = window.top.subFormID;

    glb_nCurrUserID = getCurrUserID();

    DireitosPastasInferiores();

    window_onload_1stPart();
    
    // ajusta o body do html

    with (modaldocumentosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    // configuracao inicial do html
    setupPage();
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    // carrega o grid
    ListaArquivo();
    
    // preenche combo de dimens�es de imagens
    fillCmbTamanhoData();

    // coloca foco no grid
    fg.focus();

    /*
    var glb_bA1 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoDireitos.recordset[' + '\'' + 'A1' + '\'' + '].value');
    var glb_bA2 = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoDireitos.recordset[' + '\'' + 'A2' + '\'' + '].value');


    if ((glb_bA1 == 0) && (glb_bA2 == 0) && (aEmpresa[1] == 167)) {

        btnNovo.disabled = true;
        btnExcluir.disabled = true;
    }
    */

    return glb_nCurrFormID;
}


/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    secText('Documentos', 1);

    // ajusta elementos da janela
    var elem;
    var frameRect;
    var modWidth = 0;

    // ajusta o divFG
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
    }

    with (divFG.style)
    {
        border = 'solid';
        borderWidth = 1;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = 35;
        width = modWidth - (ELEM_GAP * 20);
        height = 330;
    }

    with (divImg.style) {
        border = 'none';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = modWidth - (ELEM_GAP * 20) + 23;
        top = 35;
        width = 160;
        height = 160;
    }
    divImg.ondblclick = openImage;

    with (fg.style) 
    {
        left = 0;
        top = 0;
        width = parseInt(divFG.style.width, 10);
        height = parseInt(divFG.style.height, 10);
    }

    with (divUpload.style) 
    {
        border = 'solid';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(divFG.style.top, 10) + parseInt(divFG.style.height, 10) + 1;
        width = parseInt(divFG.style.width, 10);
        height = 50;
    }

    with (divControls.style) 
    {
        border = 'solid';
        borderWidth = 0;
        backgroundColor = 'transparent';
        left = 0;
        //top = divFG.offsetTop + divFG.offsetHeight + 1;
        top = parseInt(divFG.style.top, 10) + parseInt(divFG.style.height, 10) + 1;
        width = 100;
        height = 70;
    }
    
    // Posiciona botao Novo
    with (btnNovo) 
    {
        style.top = 17;
        style.width = 65;
        style.left = 254;
    }

    // Posiciona botao Excluir
    with (btnExcluir) 
    {
        style.top = parseInt(btnNovo.style.top, 10);
        style.width = 80;
        style.left = parseInt(btnNovo.style.left, 10) + parseInt(btnNovo.style.width, 10) + 10;
    }

    // Posiciona botao Download
    with (btnDownload) 
    {
        style.top = parseInt(btnNovo.style.top, 10);
        style.width = 80;
        style.left = parseInt(btnExcluir.style.left, 10) + parseInt(btnExcluir.style.width, 10) + 10;
    }

    with (btnRefresh) {
        style.top = parseInt(btnNovo.style.top, 10);
        style.width = 80;
        style.left = parseInt(btnDownload.style.left, 10) + parseInt(btnDownload.style.width, 10) + 10;
    }
    
    // Posiciona botao Cancelar
    with (btnCancelar)
    {
        style.top = 17;
        style.width = 60;
        style.height = 21;
        style.left = 0;
    }

    with (selTipoArquivo.style) {
        top = parseInt(btnCancelar.style.top, 10);
        left = parseInt(btnCancelar.style.left, 10) + parseInt(btnCancelar.style.width, 10) + ELEM_GAP;
        width = 170;
    }
    selTipoArquivo.onchange = selTipoArquivo_onchange;

    with (selTamanhoImagem.style) {
        top = parseInt(selTipoArquivo.style.top, 10);
        left = parseInt(selTipoArquivo.style.left, 10) + parseInt(selTipoArquivo.style.width, 10) + ELEM_GAP;
        width = 130;
    }
    selTamanhoImagem.onchange = selTamanhoImagem_onchange;

    with (selOrdemImagem.style) {
        top = parseInt(selTamanhoImagem.style.top, 10);
        left = parseInt(selTamanhoImagem.style.left, 10) + parseInt(selTamanhoImagem.style.width, 10) + ELEM_GAP;
        width = 50;
    }
    selOrdemImagem.onchange = selOrdemImagem_onchange;

    with (divIframe.style) {
        border = 'none';
        backgroundColor = 'transparent';
        top = parseInt(selTipoArquivo.style.top, 10) - 11;
        left = parseInt(selOrdemImagem.style.left, 10) + parseInt(selOrdemImagem.style.width, 10) + ELEM_GAP;
        width = parseInt(divUpload.style.width, 10) - (parseInt(selOrdemImagem.style.left, 10) + parseInt(selOrdemImagem.style.width, 10) + ELEM_GAP);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.style.width, 10) * 18;
    fg.Redraw = 2;
    
    btnOK.style.visibility = 'hidden';    
    btnCanc.value = 'Voltar';
    btnCanc.style.visibility = 'hidden';
    divUpload.style.visibility = 'hidden';
    selTamanhoImagem.style.visibility = 'hidden';
    selOrdemImagem.style.visibility = 'hidden';
    divIframe.style.visibility = 'hidden';

    btnNovo.disabled = !glb_nI;
    btnExcluir.disabled = true;
    btnNovo.onclick = btnNovo_onclick;
    btnExcluir.onclick = btnExcluir_onclick;
    btnDownload.onclick = btnDownload_onclick;
    btnRefresh.onclick = btnRefresh_onclick;
    btnCancelar.onclick = btnCancelar_onclick;

    PreencheCombos();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, ('OK_CALLFORM_PL'), null );
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
        // Voltando para pasta inferior do form
        // 20200: TipoAuxiliar - Itens, 31150: RET - Participantes
        if ((glb_nCurrSubFormID == 20200) || (glb_nCurrSubFormID == 31150)) {
            sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_INF'), null);
        }
        else
            sendJSMessage(getHtmlId(), JS_DATAINFORM, ('CANCEL_CALLFORM_PL'), null);
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function fg_modaldocumentos_AfterEdit(Row, Col) {

    var nExcluir = 0;
    var gi;
    var checked;
    
    for (i = 1; i < fg.rows; i++) 
    {
        if (fg.valueMatrix(i, getColIndexByColKey(fg, 'Ex')) != 0) {
            nExcluir = 1;
            break;
        }
    }

    if (glb_nE1 && glb_nE2) {
    	btnExcluir.disabled = false;
    	checked = false;
    	
    	for (gi = 1; gi < fg.Rows; gi++) {
    		checked |= (fg.ValueMatrix(gi, getColIndexByColKey(fg, "Ex")) != 0);
    	}

    	btnExcluir.disabled = !checked;
    } else if (((glb_nE1 == 0) && (glb_nE2 == 0)) || ((glb_nE1 == 0) && (glb_nProprietarioAlternativo != 2)) || ((glb_nE2 == 1) && (glb_nProprietarioAlternativo != 1))) {
    	btnExcluir.disabled = true;

    	for (gi = 1; gi < fg.Rows; gi++) {
    		checked = (fg.ValueMatrix(gi, getColIndexByColKey(fg, "Ex")) != 0);

    		if (checked) {
    			btnExcluir.disabled = (glb_nCurrUserID != fg.ValueMatrix(gi, getColIndexByColKey(fg, "UsuarioID")));

    			if (btnExcluir.disabled) {
    				break;
    			}
    		}
    	}
    }
}

function fg_modaldocumentosDblClick() 
{
    Download();
}
/********************************************************************
Eventos de botoes particular desta pagina
********************************************************************/
function btnNovo_onclick() 
{
    configureControls(btnNovo);

    loadIframe();
}
function btnExcluir_onclick()
{
    Excluir();
}
function btnDownload_onclick()
{
    Download();
}

function btnRefresh_onclick() {
    ListaArquivo();
    btnExcluir.disabled = true;
}

function btnCancelar_onclick() 
{
    fg.disabled = false;
    btnNovo.disabled = !glb_nI;

    configureControls(btnCancelar);
    
    // carrega o grid
    ListaArquivo();

    document.getElementById("I1").src = "";
}

/********************************************************************
Funcoes particulares desta pagina
********************************************************************/

function ListaArquivo()
{
    setConnection(dsoListaArquivo);
    dsoListaArquivo.SQL =
        'SELECT a.DocumentoID, a.Nome, dbo.fn_Documentos_ArquivoVerifica(a.FormID, a.SubFormID, a.RegistroID, a.TipoArquivoID) AS VerExisteArquivo, ' +
                'a.TipoArquivoID AS TipoID, c.ItemMasculino AS Tipo, CONVERT(BIT,0) AS Ex, ' +
                'a.Tamanho, ' +
                '(CASE a.TamanhoImagem WHEN 2 THEN \'P\' ' +
                                      'WHEN 3 THEN \'M\' ' +
                                      'WHEN 4 THEN \'G\' ' +
                    'ELSE \'\' END) AS TamanhoImagem, ' +
                'a.OrdemImagem AS OrdemImagem, ' +
                '(CONVERT(VARCHAR(10), a.dtDocumento, 103) + \' \' + CONVERT(VARCHAR(10), a.dtDocumento, 108)) AS dtDocumento, b.Fantasia AS Usuario,  ' +
                'b.PessoaID AS UsuarioID, b.TipoPessoaID ' +
        'FROM [OVERFLYBASE_EXT2].Overfly_Documentos.dbo.Documentos a WITH(NOLOCK) ' +
        'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.UsuarioID) ' +
        'INNER JOIN TiposAuxiliares_Itens c WITH(NOLOCK) ON (c.ItemID = a.TipoArquivoID) ' +
        'WHERE FormID= ' + glb_nCurrFormID + ' AND SubformID = ' + glb_nCurrSubFormID + 'AND RegistroID = ' + glb_nRegistroID + ' ' +
        'ORDER BY a.TamanhoImagem, a.OrdemImagem';
    dsoListaArquivo.ondatasetcomplete = ListaArquivo_DSC;    
    dsoListaArquivo.Refresh();
}

function ListaArquivo_DSC() {

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY HH:MM")
        dTFormat = 'dd/mm/yyyy hh:mm';
    else if (DATE_FORMAT == "MM/DD/YYYY HH:MM")
        dTFormat = 'mm/dd/yyyy hh:mm';
        
    if (!(dsoListaArquivo.recordset.EOF) || (dsoListaArquivo.recordset.BOF)) {
        startGridInterface(fg);

        glb_GridIsBuilding = true;

        fg.FontSize = '8';
        fg.FrozenCols = 0;

        headerGrid(fg, ['DocumentoID',
                       'VerExisteArquivo',
                       'TipoID',
                       'Tipo',
                       'Nome',
                       'Excluir',
                       'Tamanho(KB)',
                       'Tamanho Imagem',
                       'Ordem Imagem',
                       'Data',
                       'Usuario',
                       'UsuarioID',
                       'TipoPessoaID'], [0, 1, 2, 11, 12]);

        fillGridMask(fg, dsoListaArquivo, ['DocumentoID*',
                                           'VerExisteArquivo',
                                           'TipoID',
                                           'Tipo*',
                                           'Nome*',
                                           'Ex',
                                           'Tamanho*',
                                           'TamanhoImagem*',
                                           'OrdemImagem*',
                                           'dtDocumento*',
                                           'Usuario*',
                                           'UsuarioID',
                                           'TipoPessoaID'],
                       ['', '', '', '', '', '', '', '', '', '99/99/9999 99:99', '', ''],
				       ['', '', '', '', '', '', '', '', '', dTFormat, '', '']);

        fg.ColDataType(getColIndexByColKey(fg, 'Ex')) = 11; // format boolean (checkbox)

        //alignColsInGrid(fg, [3]);
        alignColsInGrid(fg, [6], [7, 8]);

        fg.Redraw = 2;
        fg.Editable = true;

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);

        lockControlsInModalWin(false);

        if (fg.Rows > 1) {
            btnDownload.disabled = false;
            window.focus();
            fg.focus();
        }
        else {
            btnDownload.disabled = true;
            img_Imagem.src = IMG_BRANCO;
        }

        glb_GridIsBuilding = false;
    }
    
    if (fg.row > 0)
        js_fg_AfterRowColChange(fg, fg.row, fg.col, fg.row, fg.col);
}

function js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {

    if (glb_GridIsBuilding)
        return;
    
    var img_Imagem = document.getElementById("img_Imagem");
    var tipoID = fg.textMatrix(NewRow, getColIndexByColKey(fg, 'TipoID'));

    // Se n�o � uma imagem, deixa preview em branco
    if (!(tipoID == 1451)) {
        img_Imagem.src = IMG_BRANCO;
    }
    else {
        var strPars;
        var nFileId = fg.textMatrix(NewRow, getColIndexByColKey(fg, 'DocumentoID*'));

        strPars = '?nFileID=' + escape(nFileId);

        // carrega a imagem do servidor pelo arquivo imageblob.asp
        if (trimStr((SYS_ASPURLROOT.toUpperCase())).lastIndexOf('OVERFLY3') > 0) {
            img_Imagem.src = 'http' + ':/' + '/localhost/overfly3/serversidegenEx/imageblob.aspx' + strPars;
        }
        else
            img_Imagem.src = SYS_ASPURLROOT + '/serversidegenEx/imageblob.aspx' + strPars;
    }
}

function Download() {

    var strPars;
    var nFileId;
    nFileId = fg.textMatrix(fg.row, getColIndexByColKey(fg, 'DocumentoID*'));
    strPars = '?nFileID=' + escape(nFileId);
    window.location = SYS_ASPURLROOT + '/serversidegenEx/arquivodownload.aspx' + strPars;
}

function Excluir()
{
    var sArquivoID='';

    for (i = 1; i < fg.rows; i++) 
    {
        if (fg.valueMatrix(i, getColIndexByColKey(fg, 'Ex')) != 0) 
        {
            if (sArquivoID == '') 
            {
                sArquivoID = fg.valueMatrix(i, getColIndexByColKey(fg, 'DocumentoID*'));
            }
            else 
            {
                sArquivoID = sArquivoID + ',' + fg.valueMatrix(i, getColIndexByColKey(fg, 'DocumentoID*'));
            }
            btnExcluir.disabled = true; 
        }
    }
    var strPars = '?sArquivoID=' + escape(sArquivoID);

    var _retMsg = window.top.overflyGen.Confirm('Voc� tem certeza?');

    if (_retMsg == 1) 
    {
        setConnection(dsoExcluir);
        dsoExcluir.URL = SYS_ASPURLROOT + '/serversidegenEx/arquivoexcluir.aspx' + strPars;
        dsoExcluir.ondatasetcomplete = Excluir_DSC;
        dsoExcluir.Refresh();
    }   
    else
        btnExcluir.disabled = false;
}

function Excluir_DSC() 
{
    ListaArquivo();
}

function PreencheCombos() {
    
    var nFormID = glb_nCurrFormID;
    var nC1 = glb_nC1;
    var nC2 = glb_nC2;
    var sFiltro = '';
    
    //Filtro para os documentos disponiveis - DMC 08/06/2011
    if (nFormID == 1210) 
        var nTipoPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selTipoRegistroID.value');

    if (((nC1 == 1) && (nC2 == 1)) || (nFormID == 1210))
        sFiltro = 'TipoID = 1300 AND Filtro LIKE \'%<' + nFormID + '>%\' OR ItemID = 1454';
    else 
        sFiltro = 'TipoID = 1300 AND ItemID = 1454';

    setConnection(dsoTiposArquivos);

    dsoTiposArquivos.SQL = 'SELECT ItemID, ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE ' + sFiltro;
    dsoTiposArquivos.ondatasetcomplete = PreencheCombos_DSC;
    dsoTiposArquivos.Refresh();
}
   
function PreencheCombos_DSC() {
    
    var oOption = document.createElement("OPTION");
    oOption.text = 'Selecione... ';
    oOption.value = 0;
    oOption.selected = true;
    selTipoArquivo.add(oOption);

    while (!dsoTiposArquivos.recordset.EOF) {
        var optionStr = dsoTiposArquivos.recordset['ItemMasculino'].value;
        var optionValue = dsoTiposArquivos.recordset['ItemID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;

        // Forms que dependiam de direitos para gerenciar imagens agora tem a regra agregada � glb_permiteAlterarImagem fazendo a mesma tarefa
        // Removido a condi��o que n�o permitia incluir anexo do tipo "Documentos" no Form "Publica��es". LYF 01/10/2018
        if (((optionValue != 1451) || (optionValue == 1451 && glb_permiteAlterarImagem)))
        {
            selTipoArquivo.add(oOption);
        }

        dsoTiposArquivos.recordset.MoveNext();
    }
}

function fillCmbTamanhoData() {
    glb_nCurrSubFormID = (glb_nSubFormID == 0 ? glb_nCurrSubFormID : glb_nSubFormID);

    var nA1 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                            ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A1' + '\'' + ')'));

    var nA2 = sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                                ('getCurrRightValue(' + '\'' + 'SUP' + '\'' + ',' + '\'' + 'A2' + '\'' + ')'));

    glb_permiteAlterarImagem = (nA1 == 1 && nA2 == 1);

    // Dimensoes maximas das imagens

    // Form Pessoas
    if (glb_nCurrFormID == 1210) {
        // SubForm Pessoa
        if (glb_nCurrSubFormID == 20100) {
            var nTipoPessoaID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selTipoRegistroID.value');

            // Pessoa Fisica
            if (nTipoPessoaID == 51) {
                // Parametros combos tamanho e ordem
                glb_cmbTamanhoData = [['Pequeno', 2, 1, 84, 112, 15]];

            }
            // Pessoa Juridica
            else if (nTipoPessoaID == 52) {
                // Parametros combos tamanho e ordem
                glb_cmbTamanhoData = [['Pequeno', 2, 1, 199, 50, 15],
                                       ['Grande', 4, 2, 700, 300, 40]];
            }
        }
    }

    // Form TiposAuxiliares
    else if (glb_nCurrFormID == 1310)
    {
        // SubForm Itens
        // 20200: TipoAuxiliar - Itens
        if (glb_nCurrSubFormID == 20200)
        {
            // Parametros combos tamanho e ordem
            glb_cmbTamanhoData = [['M�dio', 3, 1, 500, 400, 90],
                                  ['Grande', 4, 3, 700, 300, 80]];
        }
    }

    // Form Conceitos	
    else if (glb_nCurrFormID == 2110) {
        // SubForm Conceito
        if (glb_nCurrSubFormID == 21000) {
            var nTipoRegistroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'selTipoRegistroID.value');

            // Produto
            if (nTipoRegistroID == 303) {
                // Parametros combos tamanho e ordem
                glb_cmbTamanhoData = [['Pequeno', 2, 1, 150, 150, 15],
                                       ['M�dio', 3, 1, 500, 400, 90],
                                       ['Grande', 4, 5, 1000, 800, 280]];
            }
            // Marca
            else if (nTipoRegistroID == 304) {
                // Parametros combos tamanho e ordem
                glb_cmbTamanhoData = [['Pequeno', 2, 1, 360, 150, 30],
                                      ['M�dio', 3, 1, 500, 400, 90]];

            }
            // Conceito Concreto
            else if (nTipoRegistroID == 302) {
                // Parametros combos tamanho e ordem
                glb_cmbTamanhoData = [['Pequeno', 2, 1, 90, 45, 15],
                                      ['M�dio', 3, 1, 500, 400, 90]];

            }
        }
    }

    // Form Vitrines	
    else if (glb_nCurrFormID == 4210) {
        // SubForm Vitrines
        if (glb_nCurrSubFormID == 23100) {
            // Parametros combos tamanho e ordem
            glb_cmbTamanhoData = [['Grande', 4, 1, 700, 300, 80]];
        }
    }

    // Documento da qualidade tem caracteristicas particulares
    // e principais parametros da janela sao abaixo definidos
    // Form Documentos
    else if (glb_nCurrFormID == 7110) {
        // SubForm Documentos
        if (glb_nCurrSubFormID == 26000) {
            // Parametros combos tamanho e ordem
            glb_cmbTamanhoData = [['Grande', 4, 1, 900, 1000, 200]];

        }
    }

    // Form QAF
    else if (glb_nCurrFormID == 7180) {
        // SubForm Documentos
        if (glb_nCurrSubFormID == 26210) {
            // Parametros combos tamanho e ordem
            glb_cmbTamanhoData = [['Grande', 4, 1, 419, 600, 80]];

        }
    }

    // Bancos tem caracteristicas particulares
    // e principais parametros da janela sao abaixo definidos
    // Form Bancos
    else if (glb_nCurrFormID == 9220) {
        // SubForm Banco
        if (glb_nCurrSubFormID == 28230) {
            // Parametros combos tamanho e ordem
            glb_cmbTamanhoData = [['Pequeno', 2, 1, 69, 88, 15]];

        }
    }

    // Form RET - 12220-> ID do form
    else if (glb_nCurrFormID == 12220)
    {
        // SubForm Participantes
        // 31150: RET - Participantes
        if (glb_nCurrSubFormID == 31150)
        {
            // Parametros combos tamanho e ordem
            glb_cmbTamanhoData = [['Grande', 4, 1, 419, 600, 80]];

        }
    }
    // Form Publicacoes	
    else if (glb_nCurrFormID == 4240) {
        // SubForm Publicacoes
        if (glb_nCurrSubFormID == 23320) {
            // Parametros combos tamanho e ordem
            glb_cmbTamanhoData = [['Medio', 3, 10, 500, 400, 100],
                                  ['Grande', 4, 10, 2000, 600, 1000]];
        }
    }
}

function selTipoArquivo_onchange() {
    // Se for tipo Imagem
    if (selTipoArquivo.value == 1451) {
        
        // Se � a primeira vez que carrega os combos de imagem
        if (glb_bFirstTimeCmbSelTipoArquivo) {

            // Insere itens definidos por form no combo
            if (glb_cmbTamanhoData.length) {

                for (i = 0; i < glb_cmbTamanhoData.length; i++) {
                    oOption = document.createElement("OPTION");
                    oOption.text = glb_cmbTamanhoData[i][0];
                    oOption.value = glb_cmbTamanhoData[i][1];

                    oOption.setAttribute('OrdemMaxima', glb_cmbTamanhoData[i][2], 1);
                    oOption.setAttribute('LarguraMaxima', glb_cmbTamanhoData[i][3], 1);
                    oOption.setAttribute('AlturaMaxima', glb_cmbTamanhoData[i][4], 1);
                    oOption.setAttribute('TamanhoMaximo', glb_cmbTamanhoData[i][5], 1);

                    selTamanhoImagem.add(oOption);
                }
            }

            glb_bFirstTimeCmbSelTipoArquivo = false;
            selTamanhoImagem_onchange();
        }
    }

    configureControls(selTipoArquivo);
    loadIframe();
    selTipoArquivo.focus();
}

function selTamanhoImagem_onchange() {
    var i;
    var nOrdemMaxima;
    var oOption;

    // limpa o combo
    for (i = selOrdemImagem.length - 1; i >= 0; i--) {
        selOrdemImagem.remove(i);
    }

    // obter o atributo
    nOrdemMaxima = parseInt(selTamanhoImagem.options[selTamanhoImagem.selectedIndex].getAttribute('OrdemMaxima', 1), 10);

    // preenche o combo
    for (i = 0; i < nOrdemMaxima; i++) {
        oOption = document.createElement("OPTION");
        oOption.text = (i + 1).toString();
        oOption.value = (i + 1);

        selOrdemImagem.add(oOption);
    }

    selOrdemImagem.selectedIndex = 0;

    max_Img_Dimensions();
    loadIframe();
    selTamanhoImagem.focus();
}

function selOrdemImagem_onchange() {
    loadIframe();
}

function configureControls(ctl) {
    if ((ctl == btnNovo) || (ctl == selTipoArquivo)) {
        divControls.style.visibility = 'hidden';
        divUpload.style.visibility = 'inherit';
        divIframe.style.visibility = 'inherit';

        if (selTipoArquivo.value == 1451) {
            selTamanhoImagem.style.visibility = 'inherit';
            selOrdemImagem.style.visibility = 'inherit';
            divIframe.style.left = parseInt(selOrdemImagem.style.left, 10) + parseInt(selOrdemImagem.style.width, 10);
        }
        else {
            selTamanhoImagem.style.visibility = 'hidden';
            selOrdemImagem.style.visibility = 'hidden';
            divIframe.style.left = parseInt(selTipoArquivo.style.left, 10) + parseInt(selTipoArquivo.style.width, 10);
        }
    }
    else if (ctl == btnCancelar) {
        divControls.style.visibility = 'inherit';
        divUpload.style.visibility = 'hidden';
        divIframe.style.visibility = 'hidden';
    }
}

function loadIframe() {
    var strPars;
    var nFormID = glb_nCurrFormID;
    var nSubFormID = glb_nCurrSubFormID;
    var nUserID = glb_nCurrUserID;
    var sFiltro = '';
    var nTipoID = '';
    var i = 0;
    var nVerExisteArquivo = '';
    var nTipoArquivoID;

    // Limpa iframe para recarregar a pagina
    document.getElementById("I1").src = "";

    for (i = 1; i < fg.Rows; i++) {
        if (fg.textMatrix(i, getColIndexByColKey(fg, 'VerExisteArquivo')) == 0) {
            nTipoID += '|' + fg.TextMatrix(i, getColIndexByColKey(fg, 'TipoID'));
        }
    }
    nTipoID += '|';
    
    nTipoArquivoID = selTipoArquivo.value;

    strPars = '?nTipoArquivoID=' + escape(nTipoArquivoID);
    strPars += '&nFormID=' + escape(nFormID);
    strPars += '&nSubFormID=' + escape(nSubFormID);
    strPars += '&nRegistroID=' + escape(glb_nRegistroID);
    strPars += '&nUserID=' + escape(nUserID);
    strPars += '&nVerExisteArquivo=' + escape(nTipoID);
    strPars += '&nTamanhoImagem=' + escape(selTamanhoImagem.value);
    strPars += '&nOrdemImagem=' + escape(selOrdemImagem.value);
    strPars += '&nMaxFileSizeKB=' + escape(glb_Max_File_Size_KB);
    strPars += '&nMaxImgWidth=' + escape(glb_Max_Img_Width);
    strPars += '&nMaxImgHeight=' + escape(glb_Max_Img_Height);

    window.open(SYS_ASPURLROOT + '/serversidegenEx/arquivoupload.aspx' + strPars, 'I1');
}

function openImage() {
    var img_Imagem = document.getElementById("img_Imagem");
    var tipoID = fg.textMatrix(fg.row, getColIndexByColKey(fg, 'TipoID'));

    // Se n�o � uma imagem, deixa preview em branco
    if (!(tipoID == 1451)) {
        img_Imagem.src = IMG_BRANCO;
    }
    else {
        var strPars;
        var nFileId = fg.textMatrix(fg.row, getColIndexByColKey(fg, 'DocumentoID*'));

        strPars = '?nFileID=' + escape(nFileId);

        // carrega a imagem do servidor pelo arquivo imageblob.asp
        if (trimStr((SYS_ASPURLROOT.toUpperCase())).lastIndexOf('OVERFLY3') > 0)
            window.open('http' + ':/' + '/localhost/overfly3/serversidegenEx/imageblob.aspx' + strPars);
        else
            window.open(SYS_ASPURLROOT + '/serversidegenEx/imageblob.aspx' + strPars);
    }
}

function DireitosPastasInferiores() {

    // 20200: TipoAuxiliar - Itens, 31150: RET - Participantes
    if ((glb_nCurrSubFormID == 20200) || (glb_nCurrSubFormID == 31150))
    {
        glb_nA1 = 1;
        glb_nA2 = 1;
        glb_nC1 = 1;
        glb_nC2 = 1;
        glb_nI = 1;
        glb_nE1 = 1;
        glb_nE2 = 1;
    }
}

function max_Img_Dimensions() {
    glb_Max_Img_Width = parseInt(selTamanhoImagem.options[selTamanhoImagem.selectedIndex].getAttribute('LarguraMaxima', 1), 10);
    glb_Max_Img_Height = parseInt(selTamanhoImagem.options[selTamanhoImagem.selectedIndex].getAttribute('AlturaMaxima', 1), 10);
    glb_Max_File_Size_KB = parseInt(selTamanhoImagem.options[selTamanhoImagem.selectedIndex].getAttribute('TamanhoMaximo', 1), 10);
}
