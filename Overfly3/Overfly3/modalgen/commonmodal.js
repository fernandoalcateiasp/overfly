/********************************************************************
commonmodal.js

Library javascript para o modalxxx.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var __ELEMSTATE = new Array();  //array: controlId, state
var __ELEMCOUNT = 0;
var __INTERFACEISLOCKED = false;
var winModalPrint_Timer = null;

var glb_notBtnCloseInCaption = null;

var glb_LASTLINESELID = null;

var glb_bUseProgressBar = null;

// Array global que informa o tempo medio em segundos de execucao de cada
// Relatorio do sistema
var glb_aReportsTimeExecution =
	[[40101, 10],
	[40102,	10],
	[40103,	5],
	[40107,	10],
	[40301,	10],
	[40302,	10],
	[40303,	10],
	[40304,	10],
	[40305,	10],
	[40306,	10],
	[40307,	10],
	[40105,	10],
	[40106,	180],
	[40108, 20], // Extrato de Cliente PL
	[40109, 10], // "Seguro de Cr�dito"
	[40111,	10],	
	[40112,	60],
	[40113,	10],
	[40114,	10],
	[40121,	10],
	[40122,	10],
	[40123,	10],
	[40125,	60],
	[40126,	3],
	[40127,	5],
	[40128,	10],
	[40129,	6],
	[40130, 10],
	[40131,	10],
	[40132,	10],
	[40133,	10],
	[40134,	10],
	[40135,	10],
	[40136,	10],
	[40137,	10],
	[40138,	10],
	[40139,	10],
	[40140,	35],
	[40141,	10],
	[40142,	10],
	[40143,	10],
	[40145, 10],
    [40146, 10],
	[40148, 30],
	[40151,	10],
	[40151,	10],
	[40152,	10],
	[40153,	10],
	[40154,	10],
	[40161, 10],
	[40162, 10],
    [40164, 10],
	[40171,	10],
	[40172,	10],
	[40172,	10],
	[40173,	10],
	[40175,	10],
	[40176,	10],
	[40177,	10],
	[40178,	10],
	[40181,	10],
	[40182,	10],
	[40183,	10],
	[40184,	10],
	[40185,	10],
	[40186,	10],
	[40191, 10],
	[40192, 10],
    [40196, 10],
    [40220, 10],
	[40221, 10],
	[40222, 10],
	[40223, 10],
	[40224, 10],
	[40225, 10],


	[40195,	10],
	[40201,	10],
	[40202,	10],
	[40147, 10],
	[40211, 2], //Conhecimentos da Fatura. BJBN
	[40212, 10], //ConhecimentosFaltantes na Fatura. BJBN
	[40213, 10],
	[40214, 10],
    [40308, 10],
    [40309, 10],
    [40310, 10],
    [40311, 10],
    [40312, 10],
    [40313, 10],
    [40314, 10],
    [40316, 10]]; //Documentos com Diferen�as Percentuais. KAP

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Grid vai mudar o sort order

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_BeforeSort()
{
    if (fg.Row > 0)
        glb_LASTLINESELID = fg.TextMatrix(fg.Row, 0);
    else
        glb_LASTLINESELID = '';
}

/********************************************************************
Grid mudou o sort order

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function fg_AfterSort()
{
    var i;
    
    if ( (glb_LASTLINESELID != '') && (fg.Rows > 1) )
    {
        for (i=1; i<fg.Rows; i++)
        {
            if (fg.TextMatrix(i, 0) == glb_LASTLINESELID)
            {
                fg.TopRow = i;
                fg.Row = i;        
                break;
            }    
        }
    }
}

function fg_AfterRowColChange()
{
    // grid tem primeira linha de totalizacao
    if ( glb_totalCols__ )
    {
        if ( (!fg.Editable) && (fg.Row == 1) )
        {
            if ( fg.Rows > 2 )
                fg.Row = 2;
        }
        // If abaixo colocado para grids editaveis
        if ( (fg.Editable) && (fg.Row == 1) )
        {
            if ( fg.Rows > 2 )
                fg.Row = 2;
        }
    }    
}

/********************************************************************
Loop de mensagens
********************************************************************/
function wndJSProc(idElement, msg, param1, param2)
{
    switch (msg)
    {
        // Mensagens do programador
        case JS_DATAINFORM :
        {
            // Mensagens de retorno de dados para o programador
            if ((idElement == 'MODAL_HTML') && (param1 == EXECEVAL))
            {
                return eval(param2);
            }
        }
        break;
            
        default:
            return null;
    }
}

function divMod01_dragStart()
{

}

function divMod01_drag()
{
    var theModalFrame = getFrameInHtmlTop('frameModal');
    var theInfFrame = getFrameInHtmlTop('frameInf01');
    
    var newTop = parseInt(theModalFrame.currentStyle.top, 10) +
                 parseInt(divMod01.currentStyle.top, 10);
                 
    var maxBottom = parseInt(theInfFrame.currentStyle.top, 10) +
                         parseInt(theInfFrame.currentStyle.height, 10) -
                    (6 + 24);
    
    var newLeft = parseInt(theModalFrame.currentStyle.left, 10) +
                  parseInt(divMod01.currentStyle.left, 10);
                  
    var maxLeft = -1 * (parseInt(theModalFrame.currentStyle.width, 10) - ELEM_GAP);
    
    var maxRight = parseInt(theInfFrame.currentStyle.left) +
                   parseInt(theInfFrame.currentStyle.width) - ELEM_GAP;

    // maxima dragagem para cima e para baixo   
    if ( newTop < 0 )
        newTop = 0;
    else
    {
        if (newTop > maxBottom)
            newTop = maxBottom;    
    }    
    
    // maxima dragagem para esquerda e para a direita
    if ( newLeft < 0 )
    {
        if ( newLeft < maxLeft )
            newLeft = maxLeft;
    }
    else
    {
        if ( newLeft > maxRight )
            newLeft = maxRight;
    }
        
    theModalFrame.style.top = newTop;
    divMod01.style.top = 0;
    
    theModalFrame.style.left = newLeft;
    divMod01.style.left = 0;
}

function divMod01_dragEnd()
{
    
}

/********************************************************************
Funcao complementar da funcao window_onload
********************************************************************/
function window_onload_1stPart()
{
    var i;
    var coll;
    var bCreateBtnListar = true;
    
	// Ze em 17/03/08
	document.body.onunload = __window_OnUnload;
	dealWithObjects_Load();
	dealWithGrid_Load();
    
    // variavel do js_gridEx.js
    if (window.document.getElementById('fg') != null)
        glb_HasGridButIsNotFormPage = true;
    
    // ajusta parametros fixos dos dsos da pagina
    dsoFixedParams();
    
    // o id do usuario corrente
    glb_USERID = window.top.userID;
    
    // Movimento da janela modal ************************************
    // a barra de titulo divMod01
    divMod01.addBehavior(SYS_PAGESURLROOT + '/clientside/movable.htc');
    divMod01.style.mvBoundary = '0 0 0 0';
    
    divMod01.ondragstart = divMod01_dragStart;
    divMod01.ondrag = divMod01_drag;
    divMod01.ondragend = divMod01_dragEnd;
    // ***************************************************************
    
    // o frameModal
    var frameID = getExtFrameID(window);
    
    var rectFrame = new Array();
    if (frameID)
        rectFrame = getRectFrameInHtmlTop(frameID);

    // parametros de dimensoes dos elementos
    var divCount = 0;
    var divGap = 1;
    var divWidth = 0;
    
    if ( rectFrame )
        divWidth = rectFrame[2];
    
    // previne scroll horizontal do body da modal
    if ( divWidth > 4 )
        divWidth -= 4;
    
    var elem;
    
    // FG - grid
    // coloca classe no grid se existe na modal
	for (i=0; i<window.document.all.length; i++)
    {
        elem = window.document.all.item(i);    
	    
		if ( elem.tagName.toUpperCase() == 'OBJECT' )
		{
		    if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072')
		    {
				if ( (elem.className == null) || (elem.className == '' ) )
					elem.className = 'fldGeneral';
		    }
		}
	}	
    
    // ajusta o botao OK
    elem = window.document.getElementById('btnOK');
    
    // trava botao OK se a modal tem grid
    lockBtnOK();
    
    with (elem.style)
    {
        height = 24;
        width = 80;
        left = (rectFrame[2] / 2) - parseInt(width, 10) - (ELEM_GAP / 2);
        top = (rectFrame[3] - parseInt(height, 10)) - (2 * ELEM_GAP);
    }
    
    // ajusta o botao Canc
    elem = window.document.getElementById('btnCanc');
    with (elem.style)
    {
        height = 24;
        width = 80;
        left = (rectFrame[2] / 2) + (ELEM_GAP / 2);
        top = (rectFrame[3] - parseInt(height, 10)) - (2 * ELEM_GAP);
    }
    
    // Cria btnListar se nao existe ja criado por tag no asp 
    coll = window.document.getElementsByTagName('INPUT');
    
    bCreateBtnListar = true;
    
    for ( i=0; i<coll.length; i++ )
    {
		if ( coll.item(i).id.toUpperCase() == 'BTNLISTAR' )
		{
			bCreateBtnListar = false;
			break;
		}	
    }
    
    if ( bCreateBtnListar == true )
	    createAndAdjustBtnListar();     
       
    // ajusta o divMod01
    elem = window.document.getElementById('divMod01');
    with (elem.style)
    {
        height = 24;
        width = divWidth;
        left = 0;
    }
    
    // ajusta o paragrafo paraMod01
    elem = window.document.getElementById('paraMod01');
    with (elem.style)
    {
        fontSize = '10pt'; 
        //fontWeight = 'bold';
        left = 4;
        top = 2;
        width = divWidth - 2 * parseInt(left, 10);
    }
    
    // Previne backspace em selects para impedir retorno
    // a pagina anteriormente carregada
    preventBkspcInSel(window);
    
    // btn de close no caption
    if ( glb_notBtnCloseInCaption == null )
        createAndAdjustBtnCloseWin();
        
    // campo invisivel para uso geral
    // por exemplo foco em retorno de alert/confirm
    elem = window.document.getElementById('txtInvisible');
    
    if ( elem != null )
    {
		with (elem.style)
		{
			left = 0;
			top = 0;
			width = 0;
			height = 0;
		}
		elem.onkeypress = txtInvisible_keyPress;
    }
    
    // Cria e posiciona progress bar se a janela usa
    if ( glb_bUseProgressBar == true )
    {
		try
		{
			pb_CreateProgressBarInModal();
			
			var nProgressBarHeight = 20;
			
			rectFrame[1] -= (nProgressBarHeight / 2);
			rectFrame[3] += nProgressBarHeight;
			
			moveFrameInHtmlTop('frameModal', rectFrame);
			
			elem = window.document.getElementById('divMod01');
			
			pb_SetDimAndPosProgressBar( parseInt(elem.currentStyle.left, 10) + 2,
				                        rectFrame[3] - ( nProgressBarHeight + 2 ),
				                        parseInt(elem.currentStyle.width, 10) - 1,
				                        nProgressBarHeight);
				                        
			pb_ShowProgressBar(true);
		}
		catch(e)
		{
			;
		}
    }
    
    // Translada interface pelo dicionario do sistema
    translateInterface(window, null);
}

/********************************************************************
Seleciona ultimo relatorio utilizado
********************************************************************/
function selectReportInCombo()
{
	try
	{
		if (document.getElementById('selReports') == null)
			return null;

		if (glb_sCaller == 'S')
		{
			if (window.top.__glb_LastReportSelectedInSup != null)
			{
				selByOptValueInSel(window, 'selReports', window.top.__glb_LastReportSelectedInSup);
			}
		}
		else if (glb_sCaller == 'PL')
		{
			if (window.top.__glb_LastReportSelectedInPesqList != null)
			{
				selByOptValueInSel(window, 'selReports', window.top.__glb_LastReportSelectedInPesqList);
			}
		}
	}
	catch(e)
	{
		;
	}
}

/********************************************************************
Salva ultimo relatorio utilizado
********************************************************************/
function saveLastReportSelected()
{
	try
	{
		if (document.getElementById('selReports') == null)
			return null;

		if (glb_sCaller == 'S')
		{
			if (selReports.selectedIndex >= 0)
				window.top.__glb_LastReportSelectedInSup = selReports.value;
			else
				window.top.__glb_LastReportSelectedInSup = null;
		}
		else if (glb_sCaller == 'PL')
		{
			if (selReports.selectedIndex >= 0)
				window.top.__glb_LastReportSelectedInPesqList = selReports.value;
			else
				window.top.__glb_LastReportSelectedInPesqList = null;
		}
	}
	catch(e)
	{
		;
	}
}


/********************************************************************
Configuracao do keyPress do txtInvisible
********************************************************************/
function txtInvisible_keyPress()
{
    if ( event.keyCode == 13 )
    {
		this.value = '';
		event.keyCode = 0;
		window.top.overflyGen.BeepSoundBox(MB_ICONEXCLAMATION);
        return true;
    }    
}

/********************************************************************
Configuracao do keyPress do txtInvisible
********************************************************************/
function focusInvisibleFld()
{
    var elem;
    
    elem = window.document.getElementById('txtInvisible');
    
    if ( elem != null )
    {
		if ( (elem.currentStyle.visibility != 'hidden') &&
		     (elem.disabled == false) )
		{
		     window.focus();
		     elem.focus();
		}
    }
}

/********************************************************************
Scrolla o body do childMain para o topo
********************************************************************/
function scrollTopChildBase()
{
    sendJSMessage(getHtmlId(), JS_MSGRESERVED, 'ST_UP', null);
}    

/********************************************************************
Cria btnListar 
********************************************************************/
function createAndAdjustBtnListar()
{
    var elem = document.createElement('INPUT');
    elem.type = 'BUTTON';
    
    with (elem)
    {
        name = 'btnListar';
        id = 'btnListar';
        className = 'btns';
    }
    
    // acrescenta os elementos
    window.document.body.appendChild(elem);
    
    var btn = window.document.getElementById('btnListar');
    
    if ( btn == null )
        return null;
        
    with (btn)
    {
        style.visibility = 'hidden';
        disabled = true;
        style.top = 0;
        style.left = 0;
        style.width = 42;
        value = 'Listar';
        title = 'Listar';
    }    
}

/********************************************************************
Usar o  btnListar na modal.
Para isto a modal precisa definir a funcao btnListar_onClick
Parametros:
    bUse - true usa. Qualquer outro valor, inclusive null, nao usa
    parentRef - referencia ao elemento parent do botao. Se null usa
                o document.
    beforeThisElem - referencia ao elemento filho do elemento pai,
                   - antes do qual o elemento sera inserido. Se
                   - null 0 btnListar sera o ultimo elemento filho.            
                
Retorno:
    true se o botao ativa para uso
********************************************************************/
function useBtnListar(bUse, parentRef, beforeThisElem)
{
    if ( bUse != true )
        return null;
        
    var btn = window.document.getElementById('btnListar');
    
    if ( btn == null )
        return null;
        
    with (btn)
    {
        onclick = btnListar_onclick;
        style.visibility = 'inherit';
        disabled = false;
    }
    
    if ( parentRef != null )
    {
        if ( beforeThisElem == null )
            parentRef.appendChild(btnListar);       
        else
            parentRef.insertBefore(btnListar, beforeThisElem);
    }    
        
}

/********************************************************************
Cria btnCloseWin no title bar
********************************************************************/
function createAndAdjustBtnCloseWin()
{
    if ( window.document.getElementById('divMod01') == null )
        return null;

    var elem = document.createElement('INPUT');
    //elem.type = 'IMAGE';
    //elem.type = 'image';
    elem.type = 'button';
    
    //window.divMod01.appendChild(elem);
    window.document.appendChild(elem);
    
    with (elem)
    {
        name = 'btnCloseWin';
        id = 'btnCloseWin';
        className = 'btns';
    }
    
    var btn = window.document.getElementById('btnCloseWin');
    
    if ( btn == null )
        return null;
        
    with (btn)
    {
        style.width = 18;
        style.height = 16;
        style.top = 6;
        style.left = parseInt(divMod01.currentStyle.width, 10) - parseInt(style.width, 10) - 2;
        zIndex = 1000;
        
        //style.backgroundColor = 'red';
        style.fontFamily = 'Arial';
        style.fontWeight = 'bold';
        style.fontSize = '8px'; 
        value = 'X';
        title = 'Fechar';
        
        onclick = btnCloseWinClick;
    }    

}

/********************************************************************
Clique no botao btnCloseWin do title bar
********************************************************************/
function btnCloseWinClick()
{
    //if ( !btnCanc.disabled && !(btnCanc.currentStyle.visibility == 'hidden') )
    if ( !btnCloseWin.disabled )
    {
        btnCloseWin.disabled = true;
        if ( btn_onclick(btnCanc) == false)
			btnCloseWin.disabled = false;
        
    }    
}

/********************************************************************
Retorna o id da tag html
********************************************************************/
function getHtmlId()
{
    return document.getElementsByTagName('HTML').item(0).id;
}

/********************************************************************
Escreve texto em uma secao do statusbar da janela modal

Parametros:
text            - texto a escrever
section         - numero da secao, atualmente 1, 2 ou 3

Retorno:
nenhum
********************************************************************/
function secText(text, section)
{
    var elem;
    elem = window.document.getElementById('paraMod0' + section.toString());
    elem.innerText = translateTerm(text, null);
}

/********************************************************************
Trava botao ok se a modal tem grid
********************************************************************/
function lockBtnOK()
{
    var i, elem;
    
    for (i=0; i<window.document.all.length; i++)
    {
        elem = window.document.all.item(i);
        
        // controles ActiveX
        // o grid
        if ( elem.tagName.toUpperCase() == 'OBJECT' )
        {
            if (elem.classid.toUpperCase() == 'CLSID:D76D712E-4A96-11D3-BD95-D296DC2DD072')
            {
                btnOK.disabled = true;
                break;
            }
        }
    }
    
    return null;    
}

/********************************************************************
Seleciona conteudo de campo
********************************************************************/
function __selFieldContent(theField)
{
    theField.select();
}

/********************************************************************
Esta funcao habilita/desabilita todos os controles do html/asp corrente.

Parametros:
cmdLock         - true (desabilita) / false (habilita)

Retornos:
true se sucesso, caso contrario false
********************************************************************/
function lockControlsInModalWin(cmdLock)
{
    // verifica se a interface esta travada ou destravada
    if ( __INTERFACEISLOCKED == cmdLock )
        return false;

    // foco no frame modal    
    var winTopFrameModal = getFrameInHtmlTop('frameModal');
    
    if (winTopFrameModal != null && (winTopFrameModal.currentStyle.visibility != 'hidden') )
			winTopFrameModal.focus();
    
    __INTERFACEISLOCKED = cmdLock;

    __ELEMCOUNT = 0;
    var i;
    var elem;
    
    var coll;
    
    coll = window.document.getElementsByTagName('OBJECT');
    
    for ( i=0; i<coll.length; i++ )
    {
		adjustActiveXControl(coll.item(i), cmdLock);
    }
    
    coll = window.document.getElementsByTagName('INPUT');
    
    for ( i=0; i<coll.length; i++ )
    {
		elem = coll.item(i);
		
		adjustElement(elem, cmdLock);
		
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        if( (elem.type == 'image') && (elem.id != 'btnCloseWin') )
        {
            if (cmdLock == true)
                elem.src = glb_LUPA_IMAGES[1].src;
            else    
                elem.src = glb_LUPA_IMAGES[0].src;
        }
        // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    }
    
    coll = window.document.getElementsByTagName('SELECT');
    
    for ( i=0; i<coll.length; i++ )
    {
		elem = coll.item(i);
		
		adjustElement(elem, cmdLock);
    }
    
    coll = window.document.getElementsByTagName('TEXTAREA');
    
    for ( i=0; i<coll.length; i++ )
    {
		elem = coll.item(i);
		
		adjustElement(elem, cmdLock);
    }
/*    
    coll = window.document.getElementsByTagName('IMG');
    
    for ( i=0; i<coll.length; i++ )
    {
		elem = coll.item(i);
		
		adjustElement(elem, cmdLock);
    }
*/    
    // foco no frame modal se esta desabilitando os controles
    if ( (winTopFrameModal != null) && (cmdLock) )
    {
        //winTopFrameModal.style.visibility = 'visible';
		//winTopFrameModal.focus();
		
		if (winTopFrameModal != null && (winTopFrameModal.currentStyle.visibility != 'hidden') )
			winTopFrameModal.focus();
	}	
    
    return true;
}

/********************************************************************
Esta funcao informa se todos os controles do html/asp corrente estao
travados ou nao.

Parametros:
nenhum

Retornos:
true se a interface esta travada, caso contrario false
********************************************************************/
function modalWinIsLocked()
{
    return __INTERFACEISLOCKED;
}

/********************************************************************
Habilita / Desabilita botao de listagem
Parametros: strTest
********************************************************************/
function changeBtnState(strTest)
{
    if (strTest.length != 0)
    {
        if (btnFindPesquisa.disabled)
            btnFindPesquisa.disabled = false;
    }
    else
    {
        if (!btnFindPesquisa.disabled)
            btnFindPesquisa.disabled = true;
    }
}

// FUNCOES INTERNAS DA LIBRARY, NAO PODEM SER USADAS DIRETAMENTE ****

/********************************************************************
Esta funcao trava ou destrava um elemento de html/asp da interface
do browser filho corrente.

Parametros:
elem            - elemento corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustElement(elem, cmdLock)
{
    // cmdLock == true => desabilitar elemento
        
    if (cmdLock == true)
    {
        // desabilita o elemento
        if ( (elem.tagName.toUpperCase() == 'TEXTAREA') ||
             ((elem.tagName.toUpperCase() == 'INPUT') &&
               (elem.type.toUpperCase() == 'TEXT')) )
        {
			// salva status corrente
			__ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.readOnly);                  
            elem.readOnly = true;
        }    
        else
        {
			// salva status corrente
			__ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.disabled);           
            elem.disabled = true;
        }   
            
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        if ( (elem.tagName.toUpperCase() == 'TEXTAREA') ||
             ((elem.tagName.toUpperCase() == 'INPUT') &&
               (elem.type.toUpperCase() == 'TEXT')) )
            window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).readOnly = (__ELEMSTATE[__ELEMCOUNT])[1];
        else
            window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).disabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Esta funcao trava ou destrava um elemento ACTIVEX CONTROL
de html/asp da interface do browser filho corrente.

Parametros:
elem            - elemento ACTIVEX CONTROL corrente
cdmLock         - true (trava) / false (destrava)

Retorno:
nenhum
********************************************************************/
function adjustActiveXControl(elem, cmdLock)
{
    // cmdLock == true => desabilitar elemento
    
    if (cmdLock == true)
    {
        // salva status corrente
        __ELEMSTATE[__ELEMCOUNT] = new Array(elem.id, elem.Enabled);           
        // desabilita o elemento
        elem.Enabled = false;
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
    else    // restaura o status do elemento
    {
        // restaura status do elemento
        window.document.getElementById((__ELEMSTATE[__ELEMCOUNT])[0]).Enabled = (__ELEMSTATE[__ELEMCOUNT])[1];
        // incrementa contador de elementos
        __ELEMCOUNT++;
    }
}

/********************************************************************
Restaura interface da janela modal de impressao
********************************************************************/
function restoreModalPrint()
{
    if ( winModalPrint_Timer != null )
    {
        window.clearInterval(winModalPrint_Timer);
        winModalPrint_Timer = null;
    }
    
    lockControlsInModalWin(false);
}

function reports_onreadystatechange()
{

    var frameReport = document.getElementById("frmReport");

    if ((frameReport.contentWindow.document.readyState == 'loaded') ||
    (frameReport.contentWindow.document.readyState == 'interactive') ||
    (frameReport.contentWindow.document.readyState == 'complete')) {


        pb_StopProgressBar(true);
        lockControlsInModalWin(false);
    }
}

/********************************************************************
Restaura interface da janela modal de impressao
********************************************************************/
function setFocusInModalWin()
{
    window.focus();
}

/********************************************************************
Esta funcao retorna um array com os dados da empresa corrente.
Estes dados estao no statusbarmain.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
array   - array[0] - empresaID
        - array[1] - empresaPaisID
        - array[2] - empresaCidadeID
        - array[3] - nome de fantasia da empresa
        - array[4] - empresaUFID
        - array[5] - TipoEmpresaID
        - array[6] - nome completo da empresa
        - array[7] - idioma do sistema.
        - array[8] - idioma da empresa.

null    - nao tem empresa selecionada        
********************************************************************/
function getCurrEmpresaData()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__EMPRESADATA', null) );
}

/********************************************************************
Esta funcao retorna o id do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID do usuario ou 0 se nao tem usuario logado

********************************************************************/
function getCurrUserID()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSERID', null) );
}

/********************************************************************
Esta funcao retorna o e-mail do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID do usuario ou 0 se nao tem usuario logado

********************************************************************/
function getCurrUserEmail()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__CURRUSEREMAIL', null) );
}

/********************************************************************
Esta funcao retorna o ID da impressora de codigo de barras do usuario logado.
Este dado esta no overfly.asp.
Esta funcao esta implementada no js_common.js e no js_pesqlist.js
por questoes operacionais.

Parametros:
nenhum

Retorno:
o ID da impressora de codigo de barras do usuario logado.

********************************************************************/
function getCurrPrinterBarCode()
{
    return ( sendJSMessage(getHtmlId(), JS_WIDEMSG, '__PRINTERBARCODE', null) );
}

/********************************************************************
Coloca data no formato MMDDYYYY

Parametros:
string data ou qualquer outra string

Retorno:
string data convertida ou sDate se sDate nao e data
********************************************************************/
function dateFormatToSearch(sDate)
{
    // parece que e data, se nao for devolve como esta
    if ( chkDataEx(sDate) != true )    
        return sDate;
        
    // e data, se o sistema esta no formato MMDDYYYY devolve
    if ( DATE_FORMAT == "MM/DD/YYYY" )
        return sDate;
        
    // e data e o sistema esta no formato DDMMYYYY, inverte e devolve
    var rExp;
    var aString;
    var nDay, nMonth, nYear;
    
    rExp = /\D/g;
    aString = sDate.split(rExp);
    
    if ( (typeof(aString)).toUpperCase() != 'OBJECT' )
        return sDate;
        
    if ( aString.length != 3 )
        return sDate;
        
    nDay = parseInt(aString[0], 10);
    nMonth = parseInt(aString[1], 10);
    nYear = parseInt(aString[2], 10);    
    
    return (nMonth.toString() + '/' + nDay.toString() + '/' + nYear.toString());
}

/********************************************************************
Redimensiona e reposiciona janela modal, inclusive os botoes
OK e Cancel

Parametros:
	modalWidth - nova largura
	modalHeight - nova altura
	bUseMinOfSystem - repeita as dimensoes minimas do sistema

Retorno:
nenhum
********************************************************************/
function redimAndReposicionModalWin(modalWidth, modalHeight, bUseMinOfSystem)
{
    if ( bUseMinOfSystem == null )
		bUseMinOfSystem = false;
		
    // o modal window e contido no frameModal
    // e posicionado e dimensionado em funcao do toolbar inferior

	var elem;
    var frameTBInf = null;
    var rectTBInf = new Array(0, 0, 0, 0);
    var rectModal = new Array(0, 0, 0, 0);
    var modalFrame = null;

    frameTBInf = getFrameIdByHtmlId('controlbarinfHtml');
    
    if ( frameTBInf )
        rectTBInf = getRectFrameInHtmlTop(frameTBInf);
    
    if ( rectTBInf )    
    {
        // valores minimos e maximos para as dimensoes da janela
        if ( bUseMinOfSystem )
        {
			modalWidth = ((modalWidth < (MAX_FRAMEWIDTH / 3) ) ? (MAX_FRAMEWIDTH / 3) : modalWidth);
			modalHeight = ((modalHeight < (MAX_FRAMEHEIGHT / 3) ) ? (MAX_FRAMEHEIGHT / 3) : modalHeight);
		}

        rectModal[0] = (rectTBInf[2] - modalWidth) / 2;
        rectModal[1] = rectTBInf[1] - modalHeight / 2;
    }

    rectModal[2] = modalWidth;
    rectModal[3] = modalHeight;
    
    moveFrameInHtmlTop('frameModal', rectModal);
        
    modalFrame = getFrameInHtmlTop('frameModal');
    
    if ( modalFrame != null )
    {
        modalFrame.style.zIndex = 2;
    } 
    
    // Reposiciona e redimensiona o div
    // ajusta o divMod01
    elem = window.document.getElementById('divMod01');
    with (elem.style)
    {
        height = 24;
        width = rectModal[2];
        left = 0;
        top = 0;
    }
    
    // Reposiciona os botoes OK e Cancel
    // ajusta o botao OK
    elem = window.document.getElementById('btnOK');
    with (elem.style)
    {
        height = 24;
        width = 80;
        left = (rectModal[2] / 2) - parseInt(width, 10) - (ELEM_GAP / 2);
        top = (rectModal[3] - parseInt(height, 10)) - (2 * ELEM_GAP);
    }
    
    // ajusta o botao Canc
    elem = window.document.getElementById('btnCanc');
    with (elem.style)
    {
        height = 24;
        width = 80;
        left = (rectModal[2] / 2) + (ELEM_GAP / 2);
        top = (rectModal[3] - parseInt(height, 10)) - (2 * ELEM_GAP);
    }
    
    // ajusta o btnCloseWin se existe
    elem = window.document.getElementById('btnCloseWin');
    if ( elem != null )
		elem.style.left = parseInt(divMod01.currentStyle.width, 10) - parseInt(elem.currentStyle.width, 10) - 2;
}

function centerBtnInModal(btnRef)
{
	if ( window.top.frames.length < 8 )
		return null;

	var coll, i, oFrame;
	
	oFrame = null;
	
	// todos os iframes
    coll = window.top.document.all.tags('IFRAME');
    
    for (i=0;i<coll.length; i++)
    {
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEMODAL' )
		{
			oFrame = coll.item(i);
			break;
        }
    }
		
	if ( oFrame != null )
	{	
		btnRef.style.left = (parseInt(oFrame.currentStyle.width, 10) -
		                     parseInt(btnRef.currentStyle.width, 10)) / 2;
	}                     
}

/********************************************************************
Esta funcao deve se inserida no corpo de uma TAG <A> definida em um HTML.
Este HTML deve estar executando dentro de um frame de uma modal.
Parametros:
	sFnToCall - e o unico definido. Assinatura da funcao que sera
	invocada no PESQLIST, SUP ou INF do form.

NOTA IMPORTANTE: ESTA FUNCAO SO EXECUTA SE A PAGINA QUE CONTEM
O FRAME MODAL QUE POR SUA VEZ CONTEM A PAGINA EXIBIDA, TIVER
A VARIAVEL glb_sCaller DEFINIDA!!!
********************************************************************/
function userInputInHTML(sFnToCall)
{
	if ( (sFnToCall == null) || (sFnToCall == '') )
		return null;
	
	if ( glb_sCaller == null )
		return null;
		
	if ( userInputInHTML.arguments.length < 1 )	
		return null;
	
	// A assinatura da funcao a invocar
	var fnToInvoque = userInputInHTML.arguments[0] + '(';
	var i;
	
	if ( userInputInHTML.arguments.length == 1 )
		fnToInvoque += userInputInHTML.arguments[i].toString() + ')' ;
	else
	{
		for ( i=1; i<userInputInHTML.arguments.length; i++  )
		{
			fnToInvoque += userInputInHTML.arguments[i].toString();
			
			if ( i != (userInputInHTML.arguments.length - 1) )
				fnToInvoque += ', ';
			else
				fnToInvoque += ')';
		}	
	}

	try
	{		
		// Invoca a funcao	
		if ( glb_sCaller == 'S' )	
			eval('window.top.frames[4].' + fnToInvoque);
		else if ( glb_sCaller == 'PL' )
			eval('window.top.frames[5].' + fnToInvoque);
		else if ( glb_sCaller == 'I' )
			eval('window.top.frames[6].' + fnToInvoque);
	}
	catch(e)	
	{
		;
	}
}
/********************************************************************
Coloca data no formato MMDDYYYY. Especifica do pesqlist.

Parametros:
string data ou qualquer outra string

Retorno:
string data convertida ou sDate se sDate nao e data
********************************************************************/
function putDateInMMDDYYYY(sDate)
{
    // parece que e data, se nao for devolve como esta
    if ( chkDataEx(sDate) != true )    
        return sDate;
        
    // e data, se o sistema esta no formato MMDDYYYY devolve
    if ( DATE_FORMAT == "MM/DD/YYYY" )
        return sDate;
        
    // e data e o sistema esta no formato DDMMYYYY, inverte e devolve
    var rExp;
    var aString;
    var nDay, nMonth, nYear;
    var sRet = '';
    
    rExp = /\D/g;
    aString = sDate.split(rExp);
    
    if ( (typeof(aString)).toUpperCase() != 'OBJECT' )
        return sDate;
        
    if (aString.length > 6)
        return sDate;

    nDay = parseInt(aString[0], 10);
    nMonth = parseInt(aString[1], 10);
    nYear = parseInt(aString[2], 10);    
	
	sRet = (nMonth.toString() + '/' + nDay.toString() + '/' + nYear.toString());
	
	// Devolve mes, dia e ano, se nao tem parte de hora na string
	if ( aString.length == 3 )
		return sRet;

	if ( aString.length == 4 )
		sRet = sRet + ' ' + aString[3] + ':00:00';
	else if ( aString.length == 5 )
		sRet = sRet + ' ' + aString[3] + ':' + aString[4] + ':00';
	else if ( aString.length == 6 )
		sRet = sRet + ' ' + aString[3] + ':' + aString[4] + ':' + aString[5];

	// Devolve data com hora
	return sRet;
}

function __adjustFramePrintJet()
{
	var i, elem, coll;
    var dimArray = new Array();
    // todos os iframes
    coll = document.all.tags('IFRAME');
    
    // posi��o e dimens�es  dos frames
    dimArray[0] = 0;
    dimArray[1] = 0;
    dimArray[2] = 0;
    dimArray[3] = 0;
    
    for (i=0;i<coll.length; i++)
    {
        coll.item(i).style.visibility = 'hidden';
        coll.item(i).scrolling = 'no';
        coll.item(i).tabIndex = -1;
        moveFrameInHtmlTop(coll.item(i).id, dimArray);
        // originally no backgroundColor
        coll.item(i).style.backgroundColor = 'transparent';
    }

}

/*
Ze em 17/03/08
*/
function __window_OnUnload()
{
	try
	{
		saveLastReportSelected();
	}
	catch(e)
	{
		;
	}
	
    dealWithObjects_Unload();
}
