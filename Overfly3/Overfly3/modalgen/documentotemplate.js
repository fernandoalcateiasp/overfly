/********************************************************************
documentotemplate.js

Library javascript para o documentotemplate.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_overHTMLMounter = new _OverHtmlMounter();
var glb_countImgsNumber = 0;
var glb_showDocument_Timer = null;
var dsoDocumento = new CDatatransport('dsoDocumento');
var dsoVersao = new CDatatransport('dsoVersao');
//var glb_VersaoReset = true;
var glb_nOrdem;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
FUNCOES GERAIS:

window_onload_Prg()
setupControlsAfterImagesLoaded()
setupPage()
fillCombos()
selDocumento_onchange()
setupControlsOnGoToAnchorPageLoaded()
setupControlsOnGoToAnchor()
openSite(site)
openDocument(nDocumentoID)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Funcao chamada pela automacao.
Uso do programador.
Vai iniciar o window_onload.

Retorno - irrelevante
********************************************************************/
function window_onload_Prg()
{
    window.parent.glb_windowDocumentos = window;
    
    fillSelVersao();

	return null;
}

/********************************************************************
Funcao chamada pela automacao.
Uso do programador.
Configuracao final da pagina, apos carregar todas as imagens
********************************************************************/
function setupControlsAfterImagesLoaded()
{
    var nHeighMM;
    
    // Altura do documento em milimetros
    // nHeighMM = (divDocumento.offsetTop + divDocumento.offsetHeight) / 3.75;
    nHeighMM = (divDocumento.offsetTop + divDocumento.offsetHeight) / 6.00;
    
    // Total de Paginas
    window.parent.txtTotPags.value = Math.ceil( nHeighMM / 259 );
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	var i;
	var oOption;
	
	with (divDocumento.style)
	{
		top = 0;
		left = 0;
		width = '100%';
		height = '100%';
		paddingTop = 5;
		paddingLeft = 5;
		paddingRight = 5;
		paddingBottom = 5;
	}
	
	fillCombos();

	if (glb_sCallOrigin2 == 'btn3') {
	    window.parent.btnEditarVisualizar.style.visibility = 'hidden'; //hidden / visible
	}

	window.parent.selDocumento.value = glb_nDocumentoID;
	window.parent.txtFiltro.onkeyup = txtFiltro_onkeyup;
	window.parent.selDocumento.onchange = selDocumento_onchange;
	window.parent.selVersao.onchange = selVersao_onchange;
	//window.parent.txtVersao.value = glb_Versao;
	//selVersao - adicionar op��es no combo
	
	window.parent.txtdtEmissao.value = glb_dtEmissao;
	//window.parent.secText(secText, 1);
	window.parent.glb_sTitleWindow = glb_sTitleWindow;
	window.parent.setSecText();

	window.parent.lblDocumento.innerText = 'Documento ' + glb_sDocumentos;

	window.parent.glb_nOrdemID = glb_nOrdemID;
	window.parent.configuraControles();
}

/********************************************************************
Preenche o combo de documentos
********************************************************************/
function fillCombos()
{
	var i;
	var oOption;
	var nDocument;
	var sFiltro;

	// Preenche combo Documentos
	window.parent.selDocumento.disabled = true;

	window.parent.clearComboEx(['selDocumento']);

	sFiltro = window.parent.txtFiltro.value;

	for (i=0; i<glb_aDocumentos.length; i++)
	{
		window.parent.selDocumento.disabled = false;
        oOption = document.createElement("OPTION");
        oOption.value = glb_aDocumentos[i][0];
        nDocument = glb_aDocumentos[i][0];
        oOption.text = glb_aDocumentos[i][1];
        oOption.setAttribute('EstadoID', glb_aDocumentos[i][2], 1);
        if (window.parent.txtFiltro.value != "") {
            if (oOption.value.toUpperCase().indexOf(sFiltro) == 0)
                window.parent.selDocumento.add(oOption);
        }
        else
            window.parent.selDocumento.add(oOption);
    }
}

/********************************************************************
Usuario trocou o option do combo de documentos
********************************************************************/
function selDocumento_onchange(sCallerDoc, bEdicao) 
{
    window.parent.lockControlsInModalWin(true);

    /*if (glb_VersaoReset) {
        if (window.parent.selVersao.options.length > 0)
            window.parent.selVersao.selectedIndex = 0;
     
        glb_nTipoDocToLoad = null;
    }*/

    /*if (window.parent.glb_VersaoReset == null)
        window.parent.glb_VersaoReset = true;*/

    if (sCallerDoc == null)
        window.parent.glb_VersaoReset = true;

    var nEstadoID = window.parent.selDocumento.options.item(window.parent.selDocumento.selectedIndex).getAttribute('EstadoID', 1);

    if ((nEstadoID == 91) || (nEstadoID == 92) || (nEstadoID == 93))
        glb_nTipoDocToLoad = -1;
    else
        glb_nTipoDocToLoad = null;
	
	window.parent.glb_oNavList.initializeList();
	window.parent.btnVoltarStatusAndHint();

	var selectedIndex = window.parent.selVersao.selectedIndex;
	var nOrdem;

	if (selectedIndex < 0)
	    glb_nTipoDocToLoad = null;
    else
	    nOrdem = window.parent.selVersao.options.item(selectedIndex).getAttribute('Ordem', 1);

	if (glb_sCallOrigin2 == 'btn3')
	    glb_nTipoDocToLoad = null;
	else 
	{
	    if ((!window.parent.glb_VersaoReset) || (bEdicao))
	    {
	        if (nOrdem == 1)
	            glb_nTipoDocToLoad = -1;
	        else if (nOrdem == 2)
	            glb_nTipoDocToLoad = null;
	        else if (nOrdem == 3)
	            glb_nTipoDocToLoad = window.parent.selVersao.options.item(selectedIndex).text;
	    }
	}

	goto(null, window.parent.selDocumento.value, null);

	fillSelVersao();
}

/********************************************************************
Usuario trocou o option do combo de vers�es
********************************************************************/
function selVersao_onchange() 
{
    //glb_VersaoReset = false;

    window.parent.glb_VersaoReset = false;
    selDocumento_onchange('selVersao');
    
    //window.parent.glb_VersaoReset = false;
}

/********************************************************************
Chamada pela automacao.
De uso  do programador
Usuario navega para outra posicao dentro da pagina que acabou de ser
carregada
********************************************************************/
function setupControlsOnGoToAnchorPageLoaded()
{
	
}

/********************************************************************
Chamada pela automacao.
De uso  do programador
Usuario navega para outra posicao dentro da pagina
********************************************************************/
function setupControlsOnGoToAnchor()
{
	
}

/********************************************************************
Abre um URL em uma nova janela do browser
********************************************************************/
function openSite(site)
{
    window.open(site);
}

/********************************************************************
Abre um documento em uma nova janela
********************************************************************/
function openDocument(nDocumentoID)
{
    var sDocumento = window.parent.SYS_PAGESURLROOT + '/serversidegenEx/arquivodownload.aspx?nFileID=' + escape(nDocumentoID);
    window.open(sDocumento);
}

function fillSelVersao() 
{
    window.parent.lockControlsInModalWin(true);

    setConnection(dsoVersao);

    var strSQL = '';

    // Chamada pelo Bot�o 3 - Apenas Vers�o Atual
    // Chamada pelo Bot�o 4 - Vers�o Estudo, Vers�o Atual e Vers�es Anteriores
    if (glb_sCallOrigin2 == 'btn4')
        strSQL =
            'SELECT 1 AS Ordem, a.DocumentoID AS PKID, \'Estudo\' AS Versao ' +
                'FROM Documentos a WITH(NOLOCK) ' +
                'WHERE ((a.DocumentoID = ' + glb_nDocumentoID + ') AND (a.EstadoID IN (91,92,93))) ' +
            'UNION ALL ';

    strSQL = strSQL +
            'SELECT 2 AS Ordem, a.DocumentoID AS PKID, ISNULL(CONVERT(VARCHAR, a.Versao),\'\') AS Versao ' +
                'FROM Documentos a WITH(NOLOCK) ' +
                'WHERE (a.DocumentoID = ' + glb_nDocumentoID + ') ';

    if (glb_sCallOrigin2 == 'btn4')
        strSQL = strSQL +
            'UNION ALL ' +
            'SELECT 3 AS Ordem, a.DocDesativoID AS PKID, ISNULL(CONVERT(VARCHAR, a.Versao),\'\') AS Versao ' +
                'FROM Documentos_Desativos a WITH(NOLOCK) ' +
                'WHERE (a.DocumentoID = ' + glb_nDocumentoID + ') ';
                
    strSQL = strSQL +
            'ORDER BY Ordem, Versao DESC';

    dsoVersao.SQL = strSQL;
    dsoVersao.ondatasetcomplete = fillSelVersao_DSC;
    dsoVersao.Refresh();
}

function fillSelVersao_DSC() {
    window.parent.lockControlsInModalWin(false);

    if (glb_nTipoDocToLoad == -1)
        glb_nOrdem = 1;
    else if (glb_nTipoDocToLoad == null)
        glb_nOrdem = 2;
    /*else
        glb_nOrdem = 3;*/

    // Preenche combo Vers�es
    window.parent.selVersao.disabled = true;
    window.parent.clearComboEx(['selVersao']);

    while (!dsoVersao.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = dsoVersao.recordset['Versao'].value;
        oOption.value = dsoVersao.recordset['PKID'].value;
        oOption.setAttribute('Ordem', dsoVersao.recordset['Ordem'].value, 1);
        oOption.selected = ((glb_nTipoDocToLoad == dsoVersao.recordset['Versao'].value) ||
                                ((!(glb_nTipoDocToLoad == dsoVersao.recordset['Versao'].value)) && (dsoVersao.recordset['Ordem'].value == glb_nOrdem)));
        window.parent.selVersao.add(oOption);
        dsoVersao.recordset.MoveNext();
    }

    window.parent.selDocumento.value = glb_nDocumentoID;

    window.parent.configuraControles();

    if (window.parent.glb_VersaoReset) {
        window.parent.selDocumento.focus();
    }
    else
        window.parent.selVersao.focus();

    window.parent.fillTextAreaHTML(glb_nDocumentoID);
}


function txtFiltro_onkeyup() 
{
    if (window.parent.event.keyCode == 13) 
    {
        fillCombos();
        if (window.parent.selDocumento.value != "")
            selDocumento_onchange();
        //fillSelVersao();
    }
}
