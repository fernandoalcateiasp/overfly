<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    
    'Forca recarregamento da pagina
    Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
    
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalopenHtml" name="modalopenHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/modalopenhtml.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i
Dim sCaller
Dim sTitulo
Dim nRegistroID
Dim nTipoID
Dim nTemFoto

sCaller = ""
sTitulo = ""
nRegistroID = 0
nTipoID = 0
nTemFoto = 0

For i = 1 To Request.QueryString("sCaller").Count
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("sTitulo").Count
    sTitulo = Request.QueryString("sTitulo")(i)
Next

For i = 1 To Request.QueryString("nRegistroID").Count
    nRegistroID = Request.QueryString("nRegistroID")(i)
Next

For i = 1 To Request.QueryString("nTipoID").Count
    nTipoID = Request.QueryString("nTipoID")(i)
Next

For i = 1 To Request.QueryString("nTemFoto").Count
    nTemFoto = Request.QueryString("nTemFoto")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_sTitulo = " & Chr(39) & sTitulo & Chr(39) & ";"
Response.Write vbcrlf

Response.Write "var glb_nRegistroID = " & nRegistroID & ";"
Response.Write vbcrlf

Response.Write "var glb_nTipoID = " & nTipoID & ";"
Response.Write vbcrlf

Response.Write "var glb_nTemFoto = " & nTemFoto & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</script>

</head>

<body id="modalopenHtmlBody" name="modalopenHtmlBody" LANGUAGE="javascript" onload="return window_onload()">

    <div id="divButtonSup" name="divButtonSup" class="fldGeneral">
        <input type="button" id="btnSocios" name="btnSocios" value="Socios" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">                    
	</div>

	<iframe id="frameDocumento" name="frameDocumento" class="theFrames"></iframe>

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <div id="divButton" name="divButton" class="fldGeneral">
        <input type="button" id="btnSolicitacaoCadastro" name="btnSolicitacaoCadastro" value="Solicitação de cadastro" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    	<input type="button" id="btnImagem" name="btnImagem" value="Imagem" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnConfirmacaoCadastro" name="btnConfirmacaoCadastro" value="Confirmação de cadastro" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnEnviarEmail" name="btnEnviarEmail" value="Enviar e-mail" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">            
        <input type="button" id="btnImprimir" name="btnImprimir" value="Imprimir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">            
	</div>

</body>

</html>
