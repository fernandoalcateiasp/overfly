<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalgerenciamentoprodutosHtml" name="modalgerenciamentoprodutosHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modalgerenciamentoprodutos.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
   
     Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/modalgerenciamentoprodutos.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaller, nModalType

sCaller = ""
nModalType = 0

For i = 1 To Request.QueryString("sCaller").Count    
    sCaller = Request.QueryString("sCaller")(i)
Next

For i = 1 To Request.QueryString("nModalType").Count
    nModalType = Request.QueryString("nModalType")(i)
Next

Response.Write "var glb_sCaller = " & Chr(39) & CStr(sCaller) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nModalType = " & CStr(nModalType) & ";"
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

//-->
</script>

<!-- //@@ Eventos de grid -->
<!-- fg -->
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
<!--
 js_modalgerenciamentoprodutos_ValidateEdit();
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
<!--
 js_modalgerenciamentoprodutos_AfterEdit(arguments[0], arguments[1]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
<!--
 js_modalgerenciamentoprodutosKeyPress(arguments[0]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
<!--
 js_fg_modalgerenciamentoprodutosDblClick(fg, fg.Row, fg.Col);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
<!--
    js_fg_modalgerenciamentoprodutosMouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
    //-->
</SCRIPT>


<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeMouseDown>
<!--
js_fg_modalgerenciamentoprodutosBeforeMouseDown(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
<!--
	js_fg_EnterCell(fg);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
<!--
 fg_modalgerenciamentoprodutosAfterRowColChange(arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
<!--
 js_fg_ModalGerenciamentoProdutosBeforeRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4], true);
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR="fg" EVENT="AfterSort">
<!--
 fg_AfterSort();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR="fg" EVENT="BeforeSort">
<!--
 fg_BeforeSort();
//-->
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseDown>
<!--
 fg_MouseDown_gerenciamentoProdutos(fg.Row, fg.Col);
//-->
</SCRIPT>

</head>

<body id="modalgerenciamentoprodutosBody" name="modalgerenciamentoprodutosBody" LANGUAGE="javascript" onload="return window_onload()" onunload="window_onunload()">

    <!-- Div title bar -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
    <!-- Div title bar -->
    <div id="divControls" name="divControls" class="divGeneral">
		<!-- Campos para botao 3 -->
		<p id="lblPesquisa" name="lblPesquisa" class="lblGeneral" title="Efetuar pesquisa?">Pesq</p>
		<input type="checkbox" id="chkPesquisa" name="chkPesquisa" class="fldGeneral" title="Efetuar pesquisa?">
		<p id="lblServico" name="lblServico" class="lblGeneral">Servi�o</p>
		<select id="selServico" name="selServico" class="fldGeneral">
		    <option value=1 selected>Ciclo de vida</option>
            <option value=2>Previs�o de vendas</option>
            <option value=3>Forma��o de pre�os</option>
            <!--<option value=4>Informa��es b�sicas</option>-->
            <option value=5>Gerenc. de estoque</option>
            <option value=6>Internet</option>
            <option value=7>Inconsist�ncias</option>
            <option value=8>Geral</option>
		</select>						
		<p id="lblFiltroEspecifico" name="lblFiltroEspecifico" class="lblGeneral">Filtro</p>
		<select id="selFiltroEspecifico" name="selFiltroEspecifico" class="fldGeneral"></select>
		<p id="lblFamilia" name="lblFamilia" class="lblGeneral">Familia</p>
		<select id="selFamilia" name="selFamilia" class="fldGeneral"></select>				
		<p id="lblMarca" name="lblMarca" class="lblGeneral">Marca</p>
		<select id="selMarca" name="selMarca" class="fldGeneral"></select>	
		<p id="lblLinha" name="lblLinha" class="lblGeneral">Linha</p>
		<select id="selLinha" name="selLinha" class="fldGeneral"></select>				
		<p id="lblColunas" name="lblColunas" class="lblGeneral" title="Exibir colunas extras no grid?">Col</p>
		<input type="checkbox" id="chkColunas" name="chkColunas" class="fldGeneral" title="Exibir colunas extras no grid?">
		<p id="lblCampos" name="lblCampos" class="lblGeneral" title="Exibir campos extras abaixo do grid?">Camp</p>
		<input type="checkbox" id="chkCampos" name="chkCampos" class="fldGeneral" title="Exibir campos extras abaixo do grid?">
		
		<p id="lblIdiomaArgumentosVenda" name="lblIdiomaArgumentosVenda" class="lblGeneral">Idioma AV</p>
		<select id="selIdiomaArgumentosVenda" name="selIdiomaArgumentosVenda" class="fldGeneral" onchange="selIdiomaArgumentosVenda_onchange()"></select>
		
		<p id="lblFiltroArgumentosVenda" name="lblFiltroArgumentosVenda" class="lblGeneral">Filtro AV</p>
		<select id="selFiltroArgumentosVenda" name="selFiltroArgumentosVenda" class="fldGeneral" onchange="selFiltroArgumentosVenda_onchange()">
		    <option value=-2 selected> </option>
            <option value=-1>Sem AV</option>
            <option value=1>AV OK</option>
            <option value=0>AV N OK</option>
		</select>						
		
		<p id="lblPercentualPV" name="lblPercentualPV" class="lblGeneral">%PV</p>
		<input type="text" id="txtPercentualPV" name="txtPercentualPV" class="fldGeneral" title="Percentual de ajuste de previs�o de vendas"></input>
		<p id="lblFornecedor" name="lblFornecedor" class="lblGeneral" title="N�mero de revis�o do documento">Fornecedor</p>
		<select id="selFornecedor" name="selFornecedor" class="fldGeneral"></select>
		<p id="lblTransacao" name="lblTransacao" class="lblGeneral">Transa��o</p>
		<select id="selTransacao" name="selTransacao" class="fldGeneral"></select>
		<p id="lblFinanciamento" name="lblFinanciamento" class="lblGeneral">Financiamento</p>
		<select id="selFinanciamento" name="selFinanciamento" class="fldGeneral"></select>
		<p id="lblProjeto" name="lblProjeto" class="lblGeneral">Projeto</p>      
        <select id="selProjeto" name="selProjeto" class="fldGeneral" multiple></select>

		<p id="lblConsolidar" name="lblConsolidar" class="lblGeneral">Cons</p>
		<input type="checkbox" id="chkConsolidar" name="chkConsolidar" class="fldGeneral" title="Consolidar">
		<p id="lblLote" name="lblLote" class="lblGeneral">Lote           </p>
		<select id="selLote" name="selLote" class="fldGeneral"></select>
		<p id="lblDescricao" name="lblDescricao" class="lblGeneral">Descricao</p>
		<input type="text" id="txtDescricao" name="txtDescricao" class="fldGeneral" title="Descricao do Lote"></input>
		
		<p id="lblEnc" name="lblEnc" class="lblGeneral" title="Pedidos de encomenda">Enc</p>
		<input type="checkbox" id="chkEnc" name="chkEnc" class="fldGeneral" title="Pedidos de encomenda">
		<input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
		<input type="button" id="btnCalcularPV" name="btnCalcularPV" value="Calcular PV" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnComprar" name="btnComprar" value="Comprar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
		<input type="button" id="btnImprimir" name="btnImprimir" value="Imprimir" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnExcel" name="btnExcel" value="Excel" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnAplicar" name="btnAplicar" value="Aplicar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <input type="button" id="btnBatch" name="btnBatch" value="Batch" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns"></input>
        <p id="lblPtax" name="lblPtax" class="lblGeneral" title="D�lar PTAX">PTAX</p>
        <input type="text" id="txtPtax" name="txtPtax" class="fldGeneral" title="Cota��o do D�lar PTAX"></input>
    </div>    
    
    <!-- Div do grid -->
    <div id="divFG" name="divFG" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT></object>
    </div>    

    <!-- Div do grid -->
    <div id="divFG2" name="divFG2" class="divGeneral">
        <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg2" name="fg2" VIEWASTEXT></object>
    </div>    

    <!-- Div Texto Argumento de Vendas -->
    <div id="divArgumentoVendas" name="divArgumentoVendas" class="divGeneral">
		<p id="lblArgumentoDeVenda" name="lblArgumentoDeVenda" class="lblGeneral">Argumentos de Venda</p>
		<textarea id="txtArgumentoDeVenda" name="txtArgumentoDeVenda" class="fldGeneral" onkeypress="ArgumentoDeVenda_onkeypress()" DATASRC="" DATAFLD=""></textarea>

		<p id="lblOK" name="lblOK" class="lblGeneral">OK</p>
		<input type="checkbox" id="chkOK" name="chkOK" class="fldGeneral" onclick="chkOK_onchange()">
		
		<p id="lblQuantOK" name="lblQuantOK" class="lblGeneral"></p>
	</div>
    
    <!-- Div Controls Gerenciamento de produtos -->
    <div id="divControlsGerProd" name="divControlsGerProd" class="divGeneral">
		<p id="lblCustoMedio" name="lblCustoMedio" class="lblGeneral" title="">Custo M�dio</p>
		<input type="text" id="txtCustoMedio" name="txtCustoMedio" class="fldGeneral" title=""></input>
		<p id="lblCustoReposicao" name="lblCustoReposicao" class="lblGeneral" title="">Custo Reposi��o</p>
		<input type="text" id="txtCustoReposicao" name="txtCustoReposicao" class="fldGeneral" title=""></input>
		<p id="lblUltimaEntrada" name="lblUltimaEntrada" class="lblGeneral" title="">�ltima Entrada</p>
		<input type="text" id="txtUltimaEntrada" name="txtUltimaEntrada" class="fldGeneral" title=""></input>
		<p id="lblUltimaSaida" name="lblUltimaSaida" class="lblGeneral" title="">�ltima Saida</p>
		<input type="text" id="txtUltimaSaida" name="txtUltimaSaida" class="fldGeneral" title=""></input>
		<p id="lblDias" name="lblDias" class="lblGeneral" title="">Dias</p>
		<input type="text" id="txtDias" name="txtDias" class="fldGeneral" title=""></input>
		<p id="lblDisponibilidade" name="lblDisponibilidade" class="lblGeneral" title="">Disponibilidade</p>
		<input type="text" id="txtDisponibilidade" name="txtDisponibilidade" class="fldGeneral" title=""></input>
		<p id="lblEstoqueInternet" name="lblEstoqueInternet" class="lblGeneral" title="">Est Internet</p>
		<input type="text" id="txtEstoqueInternet" name="txtEstoqueInternet" class="fldGeneral" title=""></input>
    </div>    
            
    <input type="button" id="btnOK" name="btnOK" value="Gravar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <textarea id="txtAreaTrace" name="txtAreaTrace" class="fldGeneral"></textarea>
    
    <iframe id="framePrintJet" name="framePrintJet" class="theFrames" frameborder="no"></iframe>

    <iframe id="frmReport" name="frmReport" style="display:none" frameborder="no"></iframe>
</body>

</html>
