/********************************************************************
modalinclusaodocumentos.js

Library javascript para o modalinclusaodocumentos.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_timerInterval = null;
var glb_LastsFldPrimaryKeySelected = '';
var glb_bDataSaved = false;

// Dsos genericos para banco de dados
var dsoGen01 = new CDatatransport("dsoGen01");
var dsoIncluiItens = new CDatatransport("dsoIncluiItens");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalinclusaodocumentos_AfterRowColChange (fg, OldRow, OldCol, NewRow, NewCol)
{
    var firstLine = 0;
    
    if ( glb_totalCols__ != false )
        firstLine = 1;
        
    if (NewRow > firstLine)
    {
        fg.Row = NewRow;
    }
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalinclusaodocumentos_AfterEdit(Row, Col)
{
    ;
}

/********************************************************************
Evento de grid particular desta pagina
********************************************************************/
function js_modalinclusaodocumentos_DblClick()
{
	btn_onclick(btnOK);
}

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
    
    // ajusta o body do html
    with (modalinclusaodocumentosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

	glb_LastsFldPrimaryKeySelected = glb_sFldPrimaryKeyName;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);

    window.focus();
    
    onchange_selDocumentoID(selDocumentoID);
}

/********************************************************************
Usuario otrcou o option do combo
********************************************************************/
function onchange_selDocumentoID(ctl)
{
	fg.Rows = 1;

	startPesq();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', glb_bDataSaved );
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Inclus�o de Documento', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var elem;
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;
    
    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

	// ajusta elementos no divFields
    adjustElementsInForm([['lblDocumentoID','selDocumentoID',7,1,-10,-10]],null,null,true);

    // ajusta o divFields
    elem = window.document.getElementById('divFields');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        width = modWidth - frameBorder - (2 * ELEM_GAP);
        // altura e fixa para tres linhas de campos
        height = parseInt(selDocumentoID.currentStyle.top, 10) + 
				 parseInt(selDocumentoID.currentStyle.height, 10) + ELEM_GAP;
        y_gap = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
    }
    
    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = y_gap;
        width = modWidth - frameBorder - (2 * ELEM_GAP);    
        height = parseInt(btnOK.currentStyle.top, 10) -
                 parseInt(top, 10) - frameBorder - ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        visibility = 'visible';
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }
    
    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Editable = false;
    
    btnOK.disabled = true;
    
    btnCanc.style.left = parseInt(btnOK.currentStyle.left, 10) +
                         parseInt(btnOK.currentStyle.width, 10) + ELEM_GAP;
    
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;

    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();

    return true;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
	incluiItem();
}

function incluiItem()
{
    var strPars = new String();
    var i, j;
    
	var nRegistroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['" + glb_sFldPrimaryKeyName + "'].value");
	var nRegistroOrigemID = getCellValueByColKey(fg, glb_LastsFldPrimaryKeySelected, fg.Row);
    
    strPars = '?nDocumentoID=' + escape(glb_nDocumentoID);
    strPars += '&nRegistroID=' + escape(nRegistroID);
    strPars += '&nDocumentoOrigemID=' + escape(selDocumentoID.value);
    strPars += '&nRegistroOrigemID=' + escape(nRegistroOrigemID);

    // pagina asp no servidor saveitemspedido.asp
    dsoIncluiItens.URL = SYS_ASPURLROOT + '/serversidegenEx/incluiitem.aspx' + strPars;
    dsoIncluiItens.ondatasetcomplete = incluiItem_DSC;
    dsoIncluiItens.refresh();
}

function incluiItem_DSC() {
    var i, sMsg;
    
    sMsg = '';
    
    if ( !(dsoIncluiItens.recordset.BOF && dsoIncluiItens.recordset.EOF) )
    {
		if (dsoIncluiItens.recordset['fldresp'].value <= 0)
		{
			sMsg = 'Erro ao gravar.';
        }
    }
    else
    {
		sMsg = 'Erro ao gravar.';
    }

	if (sMsg != '')
	{
		if ( window.top.overflyGen.Alert(sMsg) == 0 )
		    return null;
	}    

	glb_bDataSaved = true;
	
	glb_timerInterval = window.setInterval ( 'startPesq()', 10 , 'JavaScript');
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalinclusaodocumentosBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    if (selDocumentoID.currentStyle.visibility != 'hidden')
        if (selDocumentoID.disabled == false)
            selDocumentoID.focus();
}

function startPesq()
{
    if ( glb_timerInterval != null )
    {
        window.clearInterval(glb_timerInterval);
        glb_timerInterval = null;
    }

	var aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'getCurrEmpresaData()');
	var nEmpresaID = aEmpresaData[0];
	var nRegistroID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,"dsoSup01.recordset['" + glb_sFldPrimaryKeyName + "'].value");
	
	btnOK.disabled = true;
    lockControlsInModalWin(true);
    
    setConnection(dsoGen01);

	glb_LastsFldPrimaryKeySelected = '';
    
    // RAD
    if (selDocumentoID.value == '560001')
    {
		glb_LastsFldPrimaryKeySelected = 'RADID';
  
		dsoGen01.SQL = 'SELECT a.RADID, b.RecursoAbreviado, NULL AS NaoConformidade, a.dtAnalise AS Data, a.dtVencimento, ' +
				'(SELECT TOP 1 d.Data FROM RAD c WITH(NOLOCK), LOGs d WITH(NOLOCK) ' +
					'WHERE(c.RADID = a.RADID AND c.EstadoID = 48 AND ' +
					'c.RADID = d.RegistroID AND d.FormID = 7120 AND ' +
					'd.SubFormID = 26030 AND d.EventoID = 30 AND d.EstadoID = 48) ORDER BY d.Data DESC) AS dtConclusao ' +
				'FROM RAD a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND ((' + glb_nDocumentoID  + ' <> 560001) OR (a.RADID <> ' + nRegistroID + ')) AND a.EstadoID = b.RecursoID AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoID=' + glb_nDocumentoID + ' AND RegistroID=' + nRegistroID +
					'AND DocumentoOrigemID=560001 AND RegistroOrigemID=a.RADID )) = 0 AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoOrigemID= ' + glb_nDocumentoID + ' AND RegistroOrigemID=' + nRegistroID +
					'AND DocumentoID=560001 AND RegistroID=a.RADID )) = 0 ) ' +
				'ORDER BY a.RADID';
	}
    // RET
    else if (selDocumentoID.value == '620004')
    {
		glb_LastsFldPrimaryKeySelected = 'RETID';
  
		dsoGen01.SQL = 'SELECT a.RETID, b.RecursoAbreviado, NULL AS NaoConformidade, a.dtData AS Data, a.dtVencimento, ' +
				'(SELECT TOP 1 d.Data FROM RET c WITH(NOLOCK), LOGs d WITH(NOLOCK) ' +
					'WHERE(c.RETID = a.RETID AND c.EstadoID = 48 AND ' +
					'c.RETID = d.RegistroID AND d.FormID = 12220 AND ' +
					'd.SubFormID = 31150 AND d.EventoID = 30 AND d.EstadoID = 48) ORDER BY d.Data DESC) AS dtConclusao ' +
				'FROM RET a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND ((' + glb_nDocumentoID  + ' <> 620004) OR (a.RETID <> ' + nRegistroID + ')) AND a.EstadoID = b.RecursoID AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoID=' + glb_nDocumentoID + ' AND RegistroID=' + nRegistroID +
					'AND DocumentoOrigemID=620004 AND RegistroOrigemID=a.RETID )) = 0 AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoOrigemID= ' + glb_nDocumentoID + ' AND RegistroOrigemID=' + nRegistroID +
					'AND DocumentoID=620004 AND RegistroID=a.RETID )) = 0 ) ' +
				'ORDER BY a.RETID';
	}
    // RRC
    else if (selDocumentoID.value == '723001')
    {
		glb_LastsFldPrimaryKeySelected = 'RRCID';
  
		dsoGen01.SQL = 'SELECT a.RRCID, b.RecursoAbreviado, NULL AS NaoConformidade, a.dtEmissao AS Data, a.dtVencimento, ' +
				'(SELECT TOP 1 d.Data FROM RRC c WITH(NOLOCK), LOGs d WITH(NOLOCK) ' +
					'WHERE(c.RRCID = a.RRCID AND c.EstadoID = 48 AND ' +
					'c.RRCID = d.RegistroID AND d.FormID = 7140 AND ' +
					'd.SubFormID = 26090 AND d.EventoID = 30 AND d.EstadoID = 48) ORDER BY d.Data DESC) AS dtConclusao ' +
				'FROM RRC a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND ((' + glb_nDocumentoID  + ' <> 723001) OR (a.RRCID <> ' + nRegistroID + ')) AND a.EstadoID = b.RecursoID AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoID=' + glb_nDocumentoID + ' AND RegistroID=' + nRegistroID +
					'AND DocumentoOrigemID=723001 AND RegistroOrigemID=a.RRCID )) = 0 AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoOrigemID= ' + glb_nDocumentoID + ' AND RegistroOrigemID=' + nRegistroID +
					'AND DocumentoID=723001 AND RegistroID=a.RRCID )) = 0 ) ' +
				'ORDER BY a.RRCID';
	}
    // PSC
    else if (selDocumentoID.value == '821001')
    {
		glb_LastsFldPrimaryKeySelected = 'PSCID';
  
		dsoGen01.SQL = 'SELECT a.PSCID, b.RecursoAbreviado, NULL AS NaoConformidade, a.dtEmissao AS Data, NULL AS dtVencimento, ' +
				'(SELECT TOP 1 d.Data FROM PSC c WITH(NOLOCK), LOGs d WITH(NOLOCK) ' +
					'WHERE(c.PSCID = a.PSCID AND c.EstadoID = 48 AND ' +
					'c.PSCID = d.RegistroID AND d.FormID = 7150 AND ' +
					'd.SubFormID = 26120 AND d.EventoID = 30 AND d.EstadoID = 48) ORDER BY d.Data DESC) AS dtConclusao ' +
				'FROM PSC a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND ((' + glb_nDocumentoID  + ' <> 821001) OR (a.PSCID <> ' + nRegistroID + ')) AND a.EstadoID = b.RecursoID AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoID=' + glb_nDocumentoID + ' AND RegistroID=' + nRegistroID +
					'AND DocumentoOrigemID=821001 AND RegistroOrigemID=a.PSCID )) = 0 AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoOrigemID= ' + glb_nDocumentoID + ' AND RegistroOrigemID=' + nRegistroID +
					'AND DocumentoID=821001 AND RegistroID=a.PSCID )) = 0 ) ' +
				'ORDER BY a.PSCID';
	}
    // RAI
    else if (selDocumentoID.value == '822001')
    {
		glb_LastsFldPrimaryKeySelected = 'RAIID';
  
		dsoGen01.SQL = 'SELECT a.RAIID, b.RecursoAbreviado, NULL AS NaoConformidade, a.dtAuditoria AS Data, a.dtVencimento, ' +
				'(SELECT TOP 1 d.Data FROM RAI c WITH(NOLOCK), LOGs d WITH(NOLOCK) ' +
					'WHERE(c.RAIID = a.RAIID AND c.EstadoID = 48 AND ' +
					'c.RAIID = d.RegistroID AND d.FormID = 7160 AND ' +
					'd.SubFormID = 26150 AND d.EventoID = 30 AND d.EstadoID = 48) ORDER BY d.Data DESC) AS dtConclusao ' +
				'FROM RAI a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
				'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND ((' + glb_nDocumentoID  + ' <> 822001) OR (a.RAIID <> ' + nRegistroID + ')) AND a.EstadoID = b.RecursoID AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoID=' + glb_nDocumentoID + ' AND RegistroID=' + nRegistroID +
					'AND DocumentoOrigemID=822001 AND RegistroOrigemID=a.RAIID )) = 0 AND ' +
					'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoOrigemID= ' + glb_nDocumentoID + ' AND RegistroOrigemID=' + nRegistroID +
					'AND DocumentoID=822001 AND RegistroID=a.RAIID )) = 0 ) ' +
				'ORDER BY a.RAIID';
	}
    // RACP
    else if (selDocumentoID.value == '852001')
    {
		glb_LastsFldPrimaryKeySelected = 'RACPID';
		  
		dsoGen01.SQL = 'SELECT a.RACPID, b.RecursoAbreviado, a.NaoConformidade, a.dtEmissao AS Data, a.dtVencimento, ' +
			'(SELECT TOP 1 d.Data FROM RACP c WITH(NOLOCK), LOGs d WITH(NOLOCK) ' +
			'WHERE(c.RACPID = a.RACPID AND c.EstadoID = 48 AND ' +
			'c.RACPID = d.RegistroID AND d.FormID = 7170 AND ' +
			'd.SubFormID = 26180 AND d.EventoID = 30 AND d.EstadoID = 48) ORDER BY d.Data DESC) AS dtConclusao ' +
		'FROM RACP a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
		'WHERE (a.EmpresaID = ' + nEmpresaID + ' AND ((' + glb_nDocumentoID  + ' <> 852001) OR (a.RACPID <> ' + nRegistroID + ')) AND a.EstadoID = b.RecursoID AND ' +
			'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoID=' + glb_nDocumentoID + ' AND RegistroID=' + nRegistroID + ' ' +
			'AND DocumentoOrigemID=852001 AND RegistroOrigemID=a.RACPID )) = 0 AND ' +
			'(SELECT Count(*) FROM DocumentosRelacionados WITH(NOLOCK) WHERE (DocumentoOrigemID=' + glb_nDocumentoID + ' AND RegistroOrigemID=' + nRegistroID + ' ' +
			'AND DocumentoID=852001 AND RegistroID=a.RACPID )) = 0 ) ' +
		'ORDER BY a.RACPID';
	}
	else
	{
		if ( window.top.overflyGen.Alert('Op��o n�o dispon�vel.') == 0 )
		    return null;

		lockControlsInModalWin(false);
		fg.Rows = 1;
		return null;
	}

    dsoGen01.ondatasetcomplete = startPesq_DSC;
    dsoGen01.Refresh();
}

function startPesq_DSC() {
    fg.Redraw = 0;
    fg.Editable = false;
    startGridInterface(fg);
    fg.FrozenCols = 0;

    var dTFormat = '';
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';

    headerGrid(fg,['ID',
				   'Est', 
				   'N�o-Conformidade',
				   'Data',
				   'Vencimento',
				   'Conclus�o'], []);

    fillGridMask(fg,dsoGen01,[glb_LastsFldPrimaryKeySelected,
							  'RecursoAbreviado', 
							  'NaoConformidade',
							  'Data',
							  'dtVencimento',
							  'dtConclusao'],
                              ['','','','99/99/9999','99/99/9999','99/99/9999'],
                              ['','','',dTFormat,dTFormat,dTFormat]);

    // Merge de Colunas
    fg.MergeCells = 4;
    fg.MergeCol(-1) = true;

    alignColsInGrid(fg,[1]);

    fg.AutoSizeMode = 0;
    fg.AutoSize(0,fg.Cols-1);
    
    fg.Redraw = 2;
    
    alignColsInGrid(fg,[0]);
    
    lockControlsInModalWin(false);
    
    btnOK.disabled = fg.Rows <= 1;
    
    // se tem linhas no grid, coloca foco no grid, caso contrario
    // coloca foco no txtNumeroSerie
    if (fg.Rows > 1)
		fg.focus();
    else
    {
        if ( !selDocumentoID.disabled )
			selDocumentoID.focus();
    }            
}
