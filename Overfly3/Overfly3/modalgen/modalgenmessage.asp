<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
        
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
        
    Set objSvrCfg = Nothing
%>

<html id="modalgenmessageHtml" name="modalgenmessageHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf                
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, sCaption, sMessage

i = 0
sCaption = ""
sMessage = ""

For i = 1 To Request.QueryString("sCaption").Count    
    sCaption = Request.QueryString("sCaption")(i)
Next

For i = 1 To Request.QueryString("sMessage").Count    
    sMessage = Request.QueryString("sMessage")(i)
Next

Response.Write "var glb_sCaption = '" & sCaption & "';"
Response.Write vbcrlf

Response.Write "var glb_sMessage = '" & sMessage & "';"
Response.Write vbcrlf

'Necessario para compatibilidade da automacao
Response.Write "var glb_USERID = 0;"

Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    // Ajusta a posicao e as dimensoes do frame que contem a janela
    adjustMessageModalWin();

    window_onload_1stPart();
    
    // ajusta o body do html
    with (modalgenmessageBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // configuracao inicial do html
    setupPage();   
        
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
	// coloca foco no btnOK
    btnOK.focus();
}

/********************************************************************
Ajusta a posicao e as dimensoes do frame que contem a janela.
Esta funcao assemelha-se a function showModalWin(htmlPath, dimModalWin)

Parametros:
nenhum

Retorno:
nenhum
********************************************************************/
function adjustMessageModalWin()
{
    // o modal window e contido no frameModal
    // e posicionado e dimensionado em funcao do toolbar inferior
    
    var dimModalWin = new Array();
    
    dimModalWin[0] = (2 * ELEM_GAP) + (glb_sMessage.length * FONT_WIDTH);
    dimModalWin[1] = 130;  
    
    var frameTBInf = null;
    var rectTBInf = new Array(0, 0, 0, 0);
    var rectModal = new Array(0, 0, 0, 0);
    var modalFrame = null;
    
    frameTBInf = getFrameIdByHtmlId('controlbarinfHtml');
    
    if ( frameTBInf )
        rectTBInf = getRectFrameInHtmlTop(frameTBInf);
    
    if ( rectTBInf && ( dimModalWin != null ))    
    {
        // valores minimos e maximos para as dimensoes da janela
        dimModalWin[0] = ((dimModalWin[0] < (MAX_FRAMEWIDTH / 3) ) ? (MAX_FRAMEWIDTH / 3) : dimModalWin[0]);
        //dimModalWin[1] = ((dimModalWin[1] < (MAX_FRAMEHEIGHT / 3) ) ? (MAX_FRAMEHEIGHT / 3) : dimModalWin[1]);

        rectModal[0] = (rectTBInf[2] - dimModalWin[0]) / 2;
        rectModal[1] = rectTBInf[1] - dimModalWin[1] / 2;
    }
    
    if ( dimModalWin )    
    {
        rectModal[2] = dimModalWin[0];
        rectModal[3] = dimModalWin[1];
    }
    
    moveFrameInHtmlTop('frameModal', rectModal);
        
    modalFrame = getFrameInHtmlTop('frameModal');
    
    if ( modalFrame != null )
    {
        modalFrame.style.zIndex = 2;
    } 
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Caption da janela modal
    if ( glb_sCaption == '' )
        secText('Overfly', 1);
    else
        secText(glb_sCaption, 1);
        
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul 
    // posicao verical dos botoes OK/Cancel nao e considerada nesta janela)
    heightFree = modHeight - topFree - frameBorder;    
    
    // largura livre
    widthFree = modWidth - frameBorder;    
    
    // Desabilita e esconde o botao Cancela
    btnCanc.disabled = true;
    btnCanc.style.visibility = 'hidden';
    
    // Move botao OK
    btnOK.style.left = (widthFree - parseInt(btnOK.style.width, 10)) / 2;
    
    // lblMessage
    lblMessage.innerText = glb_sMessage;
    with (lblMessage.style)
    {
        backgroundColor = 'transparent';
        fontSize = '10pt';
        textAlign = 'center';
        width = FONT_WIDTH * (lblMessage.innerText).length;
        height = 18;
        left = (widthFree - parseInt(width, 10)) / 2;
        //top = topFree + ((heightFree - parseInt(height, 10)) / 2);
        top = frameBorder +
               ( parseInt(divMod01.currentStyle.top, 10) +
                parseInt(divMod01.currentStyle.height, 10) ) +
               ( parseInt(btnOK.currentStyle.top) - 
                 parseInt(divMod01.currentStyle.top, 10) -
                 parseInt(divMod01.currentStyle.height, 10) -
                 parseInt(height, 10) ) / 2;
    }

}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    var controlID = ctl.id;
    
    // esta funcao fecha a janela modal e destrava a interface
    if (controlID == 'btnOK')
    {
        lockControlsInModalWin(true);
        restoreInterfaceFromModal();
    }
    else
        restoreInterfaceFromModal();
}

//-->
</script>

</head>

<body id="modalgenmessageBody" name="modalgenmessageBody" LANGUAGE="javascript" onload="return window_onload()">
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    

    <p id="lblMessage" name="lblMessage" class="lblGeneral"></p>
</body>

</html>
