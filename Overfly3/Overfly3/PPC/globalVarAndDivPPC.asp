<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim pagesURLRootPPC
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    pagesURLRootPPC = pagesURLRoot & "/PPC/"
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID name do arquivo -->
<HTML id="globalvaranddivHtml" name="globalvaranddivHtml">

<HEAD>

<STYLE type="text/css">
	BODY
	{
		BACKGROUND-COLOR: yellow;
	}
	.fldTitle
	{
		FONT-FAMILY: Tahoma,Arial,MS Sans-Serif;
		FONT-SIZE: 10pt;
		FONT-WEIGHT: bold;
		PADDING-LEFT: 2px;
		PADDING-RIGHT: 4px;
	}
	.fldSubTitle
	{
		FONT-FAMILY: Tahoma,Arial,MS Sans-Serif;
		FONT-SIZE: 10pt;
		FONT-WEIGHT: normal;
		PADDING-LEFT: 2px;
		PADDING-RIGHT: 4px;

	}
	.fldSubTitleRight
	{
		FONT-FAMILY: Tahoma,Arial,MS Sans-Serif;
		FONT-SIZE: 10pt;
		FONT-WEIGHT: normal;
		PADDING-LEFT: 2px;
		PADDING-RIGHT: 4px;
	}
	.divGeneral
	{
		position: absolute;
		left: 0px;
		top: 0px;
		width: 150px;
		height: 23;
		background-color: red;
		visibility: inherit;
	}
</STYLE>

<TITLE>Overfly</TITLE>

<%
	'Links de estilo, bibliotecas da automacao e especificas
    'Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRootPPC & "loginPPC.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    Response.Write "<SCRIPT LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRootPPC & "js_PPC_overdso.js" & Chr(34) & "></SCRIPT>" & vbCrLf
    Response.Write "<SCRIPT LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRootPPC & "js_PPC_strings.js" & Chr(34) & "></SCRIPT>" & vbCrLf
    
    'Variaveis globais usadas no PPC
    Response.Write "<SCRIPT ID=" & Chr(34) & "varsFromServer" & Chr(34) & " LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & ">"
    
    Response.Write "</SCRIPT>" & vbCrlf
%>

<SCRIPT ID=staticScript LANGUAGE=javascript >
<!--
var dsoLogin = null;
//-->
</SCRIPT>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<!--
	function window_onload()
	{
		;
	}
	
	// Eventos abaixo nao funcionam para PPC
	function img1_onload()
	{
		//alert("Carregou a imagem.");
	}
	
	function img1_onerror()
	{
		//alert("Erro no\ncarregamento da imagem.");
	}
	
	function img1_onreadystatechange()
	{
		//alert('x');
	}
	
	function img1_onpropertychange()
	{
		//alert('x');
	}
	// Final de eventos abaixo nao funcionam para PPC
	
	function btnDiv_onclick(ctl)
	{
		alert("ID do usuario logado\nguardado em mem�ria global: " + window.top.glb_USERID);
		
		if ( ctl == btnDiv1 )
		{
			alert("Imagem vindo de arquivo\nno disco do servidor.");
			img1.src = window.top.glb_PAGESURLROOTPPC + "willy.gif";
		}
		
		if ( ctl == btnDiv2 )
		{
			alert("Imagem vindo de campo\nde tabela no banco\ndo servidor.");
			
			var strPars = new String();
			strPars = '?nFormID=' + escape(2110);
			strPars += '&nSubFormID=' + escape(21000);
			strPars += '&nRegistroID=' + escape(3007);
	    
			// carrega a imagem do servidor pelo arquivo imageblob.asp
			if ( trimStr((window.top.glb_ASPURLROOTPPC.toUpperCase())).lastIndexOf('OVERFLY3') > 0 )
			{
				// usar para teste em PC
				//img1.src = 'http' + ':/' + '/localhost/overfly3/PPC/imageblob.asp' + strPars;
				// usar para teste em PPC
				img1.src = 'http' + ':/' + '/jose1/overfly3/PPC/imageblob.asp' + strPars;
			}
			else
			{
				img1.src = window.top.glb_ASPURLROOTPPC + 'imageblob.asp' + strPars;	                 
			}	
		}
	}

//-->
</SCRIPT>

</HEAD>
<BODY id="globalvaranddivBody" name="globalvaranddivBody" LANGUAGE=javascript onload="return window_onload()">
	<DIV id="divControls1" name="divControls1" class="divGeneral">
		<input type="button" id="btnDiv1" name="btnDiv1" value="Div 1" LANGUAGE=javascript onclick="return btnDiv_onclick(this)"></input>
		<input type="text" id="txtData" name="txtData" value="" size=8></input>
	</DIV>
	<DIV id="divControls2" name="divControls2" class="divGeneral">
		<input type="button" id="btnDiv2" name="btnDiv2" value="Div 2" LANGUAGE=javascript onclick="return btnDiv_onclick(this)"></input>
		<IMG id="img1" name="img1" LANGUAGE=javascript onpropertychange ="img1_onpropertychange()" onreadystatechange="return img1_onreadystatechange()" onload="return img1_onload()" onerror="return img1_onerror()"></IMG>
	</DIV>
</BODY>
</HTML>
