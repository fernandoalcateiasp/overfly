/********************************************************************
js_PPC_strings.js reduzida para WEB

Library javascript para strings
********************************************************************/

/********************************************************************
Trima as extremidades de uma string

Parametros:
strToTrim       :String a ser trimada

Retorno:        :String trimada
********************************************************************/
function trimStr(strToTrim)
{
	if ( strToTrim == null )
		return '';
	    
	if ( strToTrim == '' )
		return '';
	
	var i, len;
	var charac, strTrimmed = new String();
	
	// trima lado esquerdo
	len = strToTrim.length;
	    
	for (i=0; i<(len); i++)
	{
		charac = strToTrim.charAt(i);
		if ( charac != " " )
			break;
	}
	
	strTrimmed = strToTrim.substr(i);
	    
	// trima lado direito
	len = strTrimmed.length;
	    
	for (i=(len-1); i>=0; i--)
	{
		charac = strTrimmed.charAt(i);
		if ( charac != " " )
			break;
	}
	
	i++;
	
	strToTrim = strTrimmed.substr(0, i);
	
	return (strToTrim);
}
