<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim pagesURLRootPPC
    Dim MAX_USERNAME
	Dim MAX_SENHA
	
	MAX_USERNAME = 20
	MAX_SENHA = 20
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    pagesURLRootPPC = pagesURLRoot & "/PPC/"
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID name do arquivo -->
<HTML id="loginppcHtml" name="loginppcHtml">

<HEAD>

<TITLE>Overfly</TITLE>

<%
	'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRootPPC & "loginPPC.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    Response.Write "<SCRIPT LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRootPPC & "js_PPC_overdso.js" & Chr(34) & "></SCRIPT>" & vbCrLf
    Response.Write "<SCRIPT LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRootPPC & "js_PPC_cookie.js" & Chr(34) & "></SCRIPT>" & vbCrLf
    Response.Write "<SCRIPT LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRootPPC & "js_PPC_strings.js" & Chr(34) & "></SCRIPT>" & vbCrLf
        
    'Variaveis globais usadas no PPC
    Response.Write "<SCRIPT ID=" & Chr(34) & "varsFromServer" & Chr(34) & " LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & ">" & vbCrlf
    
    Response.Write "var glb_MAX_USERNAME=" & Cstr(MAX_USERNAME) & ";" & vbCrlf
	Response.Write "var glb_MAX_SENHA =" & Cstr(MAX_SENHA) & ";" & vbCrlf
	
	Response.Write "var glb_USERNAME =null;" & vbCrlf
    
    Response.Write "</SCRIPT>" & vbCrlf
%>

<SCRIPT ID=staticScript LANGUAGE=javascript >
<!--
var dsoLogin = null;
//-->
</SCRIPT>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<!--
	function window_onload()
	{
		dsoLogin = new overdso();
		
		txtUsuario.maxLength = glb_MAX_USERNAME;
		
		glb_USERNAME = getCookie("userName");
		
		if ( glb_USERNAME != null )
			txtUsuario.value = glb_USERNAME;
		
		txtSenha.maxLength = glb_MAX_SENHA;
		
		txtUsuario.focus();
	}
	
	function btnLogin_onclick()	
	{
		if ( txtUsuario.value == "" )
		{
			alert("Digite usu�rio.");
			txtUsuario.focus();
			return true;
		}

		if ( txtSenha.value == "" )
		{
			alert("Digite senha.");
			txtSenha.focus();
			return true;
		}

		lockFields(true);
		
		window.top.glb_USERID = null;
		
		// Login Overfly
		dsoLogin.URL = window.top.glb_PAGESURLROOT + "/login/serverside/validateuser.aspx?userName=" +
			escape(txtUsuario.value) + 
			"&senha=" + escape(txtSenha.value) +
			"&entrada=" + escape(0);
		
		dsoLogin.ondatasetcomplete = 'dsoLogin_DSC()';
		dsoLogin.Refresh();
	}
	
	function dsoLogin_DSC()
	{
		if ( dsoLogin.RecordCount > 0  )
		{
			dsoLogin.MoveFirst();
			
			window.top.glb_USERID = dsoLogin.FieldValue["PessoaID"];
			setCookie("userName", txtUsuario.value, ( 60 * 60 * 24));
			
			alert( "Login executado\ncom sucesso." );
   		}
		else
		{
			window.top.glb_USERID = null;
			
			alert("Erro nos dados\npara login.");
		}

		lockFields(false);
	}
	
	function lockFields(bLock)
	{
		if (bLock)
			divSubTitle.innerHTML = 'Login...';
		else
			divSubTitle.innerHTML = 'Login';

		txtUsuario.disabled = bLock;
		txtSenha.disabled = bLock;
		btnLogin.disabled = bLock;
	}

	function btnNewPage_onclick()
	{
		window.top.loadPage("globalVarAndDivPPC.asp");
	}

//-->
</SCRIPT>

</HEAD>
<BODY id="loginppcBody" name="loginppcBody" LANGUAGE=javascript onload="return window_onload()">
<TABLE border=1>
<TR>
	<TD class=fldTitle>Overfly</TD>
	<TD class=fldTitle>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</TD>
	<TD class=fldTitle>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</TD>
	<TD class=fldSubTitle><DIV id=divSubTitle>Login</DIV></TD>
	<TD class=fldTitleRight>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp</TD>
</TR>
</TABLE>
<BR>
<TABLE>
<TR>
	<TD>Usu�rio</TD>
	<TD><input type="text" id="txtUsuario" name="txtUsuario" value="" size=<%Response.Write CStr(MAX_USERNAME)%>></input></TD>
</TR>
<TR>
	<TD>Senha</TD>
	<TD><input type="password" id="txtSenha" name="txtSenha" value="" size=<%Response.Write CStr(MAX_SENHA)%>></input></TD>
</TR>
<TR>
	<TD></TD>
	<TD><input type="button" id="btnLogin" name="btnLogin" value="Login" LANGUAGE=javascript onclick="return btnLogin_onclick()"></input></TD>
</TR>
</TABLE>

<BR>
<TABLE ID="Table1">
<TR>
	<TD><input type="button" id="btnNewPage" name="btnNewPage" value="New Page" LANGUAGE=javascript onclick="return btnNewPage_onclick()"></input></TD>
</TR>
</TABLE>

</BODY>
</HTML>
