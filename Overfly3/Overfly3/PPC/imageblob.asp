
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
		
	Response.Expires = 0
	
	Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
	
	Response.Buffer = TRUE
	Response.Clear
	' Change the HTTP header to reflect that an image is being passed.
	Response.ContentType = "image/JPEG"

    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	Dim i, nFormID, nSubFormID, nRegistroID
	Dim rsData, strSQLGet, srtSQLInsert, bytChunk
			   
	nFormID = 0
	nSubFormID = 0
	nRegistroID = 0
		
	For i = 1 To Request.QueryString("nFormID").Count    
	  nFormID = Request.QueryString("nFormID")(i)
	Next
	
	For i = 1 To Request.QueryString("nSubFormID").Count    
	  nSubFormID = Request.QueryString("nSubFormID")(i)
	Next
			    
	For i = 1 To Request.QueryString("nRegistroID").Count    
	  nRegistroID = Request.QueryString("nRegistroID")(i)
	Next
	    
	Set rsData = Server.CreateObject("ADODB.Recordset")

	strSQLGet = "SELECT TOP 1 * FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] a WITH(NOLOCK) " & _
	            "WHERE a.FormID = " & CStr(nFormID) & " AND "& _
	            "a.SubFormID = " & CStr(nSubFormID) & " AND "& _
                "a.TipoArquivoID = 1451 AND "& _
	            "a.RegistroID = " & CStr(nRegistroID) & " " & _
	            "ORDER BY a.DocumentoID"
	
	rsData.Open strSQLGet, strConn, adOpenStatic, adLockOptimistic, adCmdText
	
	'If (rsData.RecordCount = 0) Then
    '    rsData.AddNew
    '    
    '    rsData.Fields("FormID") = nFormID
    '    rsData.Fields("SubFormID") = nSubFormID
    '    rsData.Fields("RegistroID") = nRegistroID
    '    
    '    rsData.Update
    '    rsData.Close
    '            
    '    rsData.Open strSQLGet, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText
	'End If
		
	If (rsData.EOF) Then
		Response.BinaryWrite ""
	Else
		Response.BinaryWrite rsData.Fields("Arquivo").Value
	End If
		
	rsData.Close

	Set rsData = Nothing

	Response.Flush
	
	Response.End
%>
