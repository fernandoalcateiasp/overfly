<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim pagesURLRootPPC
    Dim aspUrlRoot
    Dim aspUrlRootPPC
    
    Dim MAX_USERNAME
	Dim MAX_SENHA
	
	MAX_USERNAME = 20
	MAX_SENHA = 20
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    pagesURLRootPPC = pagesURLRoot & "/PPC/"
    
    aspUrlRoot = objSvrCfg.DatabaseASPPagesURLRoot(Application("appName"))
    aspUrlRootPPC = aspUrlRoot & "/PPC/"
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID name do arquivo -->
<HTML id="defaultppcHtml" name="defaultppcHtml">

<HEAD>

<TITLE>Overfly</TITLE>

<%
    'Variaveis globais usadas no PPC
    Response.Write "<SCRIPT ID=" & Chr(34) & "varsFromServer" & Chr(34) & " LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & ">"
    Response.Write "var glb_PAGESURLROOT=" & Chr(34) & pagesURLRoot & Chr(34) & ";" & vbCrlf
    Response.Write "var glb_PAGESURLROOTPPC=" & Chr(34) & pagesURLRootPPC & Chr(34) & ";" & vbCrlf
    
    Response.Write "var glb_ASPURLROOT=" & Chr(34) & aspUrlRoot & Chr(34) & ";" & vbCrlf
    Response.Write "var glb_ASPURLROOTPPC=" & Chr(34) & aspUrlRootPPC & Chr(34) & ";" & vbCrlf
    Response.Write vbcrlf
    
    Response.Write "</SCRIPT>" & vbCrlf
%>

<SCRIPT ID=staticScriptGlobal LANGUAGE=javascript >
<!--
	var glb_USERID = null;
//-->
</SCRIPT>

<SCRIPT ID=wndJSProc LANGUAGE=javascript>
<!--
	function loadPage(sURL)
	{
		if ( (sURL == null) || (sURL == "") )
			return false;
			
		window.frames.item(0).location = glb_PAGESURLROOTPPC + sURL;
	}
	
	function frameset_onload()
	{
		loadPage("loginPPC.asp");
	}
//-->
</SCRIPT>

</HEAD>

<FRAMESET id="defaultppcFrameSet" name="defaultppcFrameSet" LANGUAGE=javascript onload="return frameset_onload()">
	<FRAME id="defaultppcFrame" name="defaultppcFrame" scrolling=no>
	</FRAME>
</FRAMESET>

</HTML>
