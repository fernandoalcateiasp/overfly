/********************************************************************
especificinf.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);
    
    //@@
    if (folderID == 20008) // Observacoes
    {
        dso.SQL = 'SELECT a.TermoID, a.Observacoes FROM Dicionario a WITH(NOLOCK) WHERE '+
        sFiltro + 'a.TermoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    //@@
    else if (folderID == 20009) // Prop/Altern
    {
        dso.SQL = 'SELECT a.TermoID, a.ProprietarioID,a.AlternativoID FROM Dicionario a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.TermoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    // LOG
    else if (folderID == 20010)
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    // Traducoes
    else if (folderID == 20281)
    {
        dso.SQL = 'SELECT * ' +
                  'FROM Dicionario_Traducoes a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.TermoID = '+ idToFind + ' ' +
                  'ORDER BY a.Traducao, a.Uso ';
        return 'dso01Grid_DSC';
    }
    // Traducoes Pendentes
    else if (folderID == 20282)
    {
        dso.SQL = 'SELECT a.ConceitoID, a.Conceito, b.ItemMasculino ' +
                  'FROM Conceitos a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
	              'WHERE (a.TipoConceitoID IN (301, 302, 305) AND a.EstadoID = 2 AND a.TipoConceitoID = b.ItemID AND  ' +
	                	'a.Conceito NOT IN (SELECT Termo FROM Dicionario WITH(NOLOCK)) AND  ' +
	                	'a.Conceito NOT IN (SELECT Traducao FROM Dicionario_Traducoes WITH(NOLOCK))) ' +
	              'ORDER BY a.TipoConceitoID, a.Conceito ';

        return 'dso01Grid_DSC';    
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
    
    // LOG
    if (pastaID == 20010)
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    else if (pastaID == 20281)
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID, ItemMasculino FROM TiposAuxiliares_Itens WITH(NOLOCK) WHERE (EstadoID=2 AND TipoID=48) ORDER BY Ordem';
        }
    
    }
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(20281);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20281); //Traducoes

    vPastaName = window.top.getPastaName(20282);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20282); //Traducoes Pendentes

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
	if (folderID == 20281) // Inscricoes
		currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,2], ['TermoID', registroID]);

    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    // LOG
    if (folderID == 20010)
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Traducoes
    else if (folderID == 20281)
    {
        headerGrid(fg,['Idioma',
					   'Uso',
					   'Tradu��o',
                       'TerTraducaoID'], [3]);

        glb_aCelHint = [[0,1,'Tipo de uso do termo. Ex: A-Contabilidade']];

        fillGridMask(fg,currDSO,['IdiomaID',
								 'Uso',
								 'Traducao',
                                 'TerTraducaoID'],
                                 ['','','',''],
                                 ['','','','']);
		// Merge de Colunas
		fg.MergeCells = 4;
		fg.MergeCol(-1) = true;
    }
    // Traducoes Pendentes
    else if (folderID == 20282)
    {
        headerGrid(fg,['ID',
					   'Conceito',
                       'Tipo Conceito',
                       'ConceitoID'], [3]);

        fillGridMask(fg,currDSO,['ConceitoID*',
								 'Conceito*',
                                 'ItemMasculino*',
                                 'ConceitoID'],
                                 ['','','',''],
                                 ['','','','']);
    }

}
