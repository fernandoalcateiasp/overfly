/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Feriados
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
var glb_ColTexto = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.FeriadoID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Feriados a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.FeriadoID = ' + idToFind;
        return 'dso01JoinSup' + '_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20271) // Datas
    {
        dso.SQL = 'SELECT a.LocalidadeID, a.Dia, a.Mes, a.Ano, a.EhFeriado, ' +
                  'a.Trabalha, a.Observacao, a.FeriadoID, a.FerDataID ' +
                  'FROM Feriados_Datas a WITH(NOLOCK) ' +
                  'WHERE a.FeriadoID = ' + idToFind +
                  ' ORDER BY a.LocalidadeID, a.Ano DESC, a.Mes, a.Dia'; 
                  
        
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20272) // Regras
    {
        dso.SQL = 'SELECT a.LocalidadeID, a.SemanaDoMes, ' +
                  'a.DiaDaSemana, a.MesDoAno, a.Observacao, a.FeriadoID, a.FerRegraID ' +
                  'FROM Feriados_Regras a WITH(NOLOCK) ' +
                  'WHERE a.FeriadoID = ' + idToFind;
                     
        return 'dso01Grid_DSC';
    }
    
    
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
       
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Datas
    else if (pastaID == 20271)
    {
        if (dso.id == 'dso01GridLkp')
        {
            dso.SQL = 'SELECT a.LocalidadeID, b.Localidade ' +
	                  'FROM Feriados_Datas a WITH(NOLOCK) ' +
	                  'INNER JOIN Localidades B WITH(NOLOCK) ON a.LocalidadeID = B.LocalidadeID ' +
	                  'WHERE a.FeriadoID = ' + nRegistroID;
        }
    }
    else if (pastaID == 20272) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT a.LocalidadeID, b.Localidade ' +
	                  'FROM Feriados_Regras a WITH(NOLOCK) ' +
	                  'INNER JOIN Localidades B WITH(NOLOCK) ON a.LocalidadeID = B.LocalidadeID ' +
	                  'WHERE a.FeriadoID = ' + nRegistroID;
        }
    }      
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(20271);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20271); //Datas

    vPastaName = window.top.getPastaName(20272);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20272); //Regras

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        // Parametros:
        // grid --> O grid a ser tratado
        // dsoData --> Datasource
        // nLine --> Linha do grid a ser gravada
        // aFields --> Array que contem as colunas obrigatorias conforme a ordem das colunas no grid:
        // O registroID da tabela do grid n�o � passado, a automa��o trata.
        // aID --> string com o nome do campo que linka esta tabela na tabela do SUP, 
        // mais o valor do CampoID da tabela do SUP, se passar 0 nao grava
        // aColsHiddenWriteable --> Array das colunas escondidas que sao gravaveis

        // esta funcao retorna a linha corrente do grid 
        // -1 a gravacao nao sera feita
        // maior que 0 vai gravar o registro

        if (folderID == 20271) // Datas
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['FeriadoID', registroID], [7]);

        if (folderID == 20272) // Regras 
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['FeriadoID', registroID], [5]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var i = 0;
    var dTFormat = '';
    var currID = null;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

    else if (folderID == 20271) // Datas
    {
        headerGrid(fg, ['Localidade',
                        'Ano',
                        'M�s',
                        'Dia',
                        'Feriado',
                        'Trabalha',
                        'Observa��o',
                        'LocalidadeID',
                       'FerDataID'], [7,8]);

        glb_aCelHint = [[0, 4, 'Esta data � feriado?'],
                        [0, 5, 'Trabalha-se nesta data?']];

        fillGridMask(fg, currDSO, ['^LocalidadeID^dso01GridLkp^LocalidadeID^Localidade*',
                                   'Ano',
                                   'Mes',
                                   'Dia',
                                   'EhFeriado',
                                   'Trabalha',
                                   'Observacao',
                                   'LocalidadeID',
                                   'FerDataID'],
                                 ['', '', '', '', '', '', '', '',''],
                                 ['', '', '', '', '', '', '', '','']);

        // Merge de Colunas
        fg.MergeCells = 4;
//        fg.MergeCol(-1) = true;
        fg.mergecol(0) = true;
        fg.mergecol(1) = true;
        fg.mergecol(2) = true;
        fg.mergecol(3) = true;
        
        alignColsInGrid(fg);
        
        fg.autoSizeMode = 0;
        fg.autoSize(0, fg.Cols - 1);
        for (y = 1; y <= (fg.Rows - 1); y++) 
        {
            if (fg.TextMatrix(y, getColIndexByColKey(fg, '^LocalidadeID^dso01GridLkp^LocalidadeID^Localidade*')) == '')
                fg.TextMatrix(y, getColIndexByColKey(fg, '^LocalidadeID^dso01GridLkp^LocalidadeID^Localidade*')) = 'Mundial';
        }

        // Pinta campos M�s e Dia de vermelho caso venham vazios
        var nMes = getColIndexByColKey(fg, 'Mes');
        var nDia = getColIndexByColKey(fg, 'Dia');
        var nVermelho = 0X7280FA;
        var i;
        for (i = 1; i < fg.Rows; i++) {
            if ((fg.TextMatrix(i, nMes) == '') || (fg.TextMatrix(i, nDia) == ''))
                fg.Cell(6, i, nMes, i, nDia) = eval(nVermelho);
        }
    }
    else if (folderID == 20272) // Regras
    {
        headerGrid(fg, ['Localidade',
                        'SM',
                        'DS',
                        'MA',
                        'Observa��o',
                        'LocalidadeID',
                        'FerRegraID'], [5, 6]);

        glb_aCelHint = [[0, 1, 'Semana do m�s'],
                    [0, 2, 'Dia da semana'],
                    [0, 3, 'M�s do ano']];
                        
        fillGridMask(fg, currDSO, ['^LocalidadeID^dso01GridLkp^LocalidadeID^Localidade*',
                                   'SemanaDoMes',
                                   'DiaDaSemana',
                                   'MesDoAno',
                                   'Observacao',
                                   'LocalidadeID',
                                   'FerRegraID'],
								   ['', '', '', '', '', '', ''],
								   ['', '', '', '', '', '', '']);
    }
}
