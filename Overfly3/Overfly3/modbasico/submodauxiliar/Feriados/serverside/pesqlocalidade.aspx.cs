using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modbasico.submodauxiliar.Feriados.serverside
{
    public partial class pesqlocalidade : System.Web.UI.OverflyPage
    {
        private string strtoFind;
        private string tipoFeriadoID;

        private static string emptStr = "";

        protected string strToFind
        {
            set { strtoFind = (value != null) ? value : emptStr; }
        }

        protected string nTipoFeriadoID 
        {
            set { tipoFeriadoID = (value != null) ? value : ""; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        
        {
            string strSQL = "";

            strSQL = "SELECT TOP 100 Cidades.LocalidadeID, " +
			"(SELECT TOP 1 Localidade " +
                "FROM Localidades Localizacao WITH(NOLOCK) " +
				"WHERE Localizacao.LocalidadeID=Cidades.LocalizacaoID) AS Localizacao, Cidades.Localidade AS Cidade " +
            "FROM Localidades Cidades WITH(NOLOCK) " +
			"WHERE Cidades.TipoLocalidadeID= "+ tipoFeriadoID.ToString() + " AND Cidades.EstadoID=2 AND Cidades.Localidade >= '" + strtoFind.ToString() + "' " +
			"ORDER BY Localidade";

            // Executa o pacote
            //int rowsAffected = DataInterfaceObj.ExecuteSQLCommand(strSQL);

            WriteResultXML(DataInterfaceObj.getRemoteData(strSQL));
        }
    }
}
