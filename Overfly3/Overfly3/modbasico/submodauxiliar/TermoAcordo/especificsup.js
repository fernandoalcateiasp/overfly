/********************************************************************
js_especificsup.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de termoacordosup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;
    //alert(glb_verificaEstadoTermo);
    //@@
    
    var nUserID = getCurrUserID();

    if (newRegister == true) 
    {
         sSQL = 'SELECT TOP 1 *, CONVERT(VARCHAR,dtVigencia,' + DATE_SQL_PARAM + ') AS V_dtVigencia, ' +
               '(SELECT a.Fantasia FROM Pessoas a WITH (NOLOCK)' +
	                'INNER JOIN TermoAcordo b WITH (NOLOCK) ON ' +
		                '(b.ProprietarioID = a.PessoaID) ' +
	                'WHERE b.TermoAcordoID = z.TermoAcordoID) AS PropFantasia, ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas c WITH (NOLOCK) ' + 
               'WHERE c.EstadoID=2 AND c.TipoRelacaoID=34 AND c.SujeitoID= ' + nID + ' AND c.ObjetoID=z.TermoAcordoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM TermoAcordo z WITH (NOLOCK) ' +
               'WHERE z.ProprietarioID = ' + nID + ' ' +
               'ORDER BY z.TermoAcordoID DESC';
	}
    else
    {
        sSQL = 'SELECT TOP 1 *, CONVERT(VARCHAR,dtVigencia,' + DATE_SQL_PARAM + ' ) AS V_dtVigencia, ' +
               '(SELECT a.Fantasia FROM Pessoas a WITH (NOLOCK)' +
	                'INNER JOIN TermoAcordo b WITH (NOLOCK) ON ' +
		                '(b.ProprietarioID = a.PessoaID) ' +
	                'WHERE b.TermoAcordoID = z.TermoAcordoID) AS PropFantasia, ' + 
                       'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
    	       'WHEN (SELECT COUNT (*) FROM RelacoesPessoas c WITH (NOLOCK) ' +
               'WHERE c.EstadoID=2 AND c.TipoRelacaoID=34 AND c.SujeitoID= ' + nUserID + ' AND c.ObjetoID=z.TermoAcordoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM TermoAcordo z WITH (NOLOCK) ' +
               'WHERE z.TermoAcordoID = ' + nID + ' ' +
               'ORDER BY z.TermoAcordoID DESC';
	}
	
    setConnection(dso);
    
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;

    sql = 'SELECT TOP 1 *, CONVERT(VARCHAR, dtVigencia,' + DATE_SQL_PARAM + ') as V_dtVigencia, ' + 
          'SPACE(0) as PropFantasia, ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM TermoAcordo WHERE TermoAcordoID = 0';
    
    dso.SQL = sql;          
}              
