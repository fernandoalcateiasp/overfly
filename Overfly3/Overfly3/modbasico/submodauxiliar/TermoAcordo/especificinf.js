/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de TermoAcordoID
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;
var glb_ColTexto = 0;
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ((folderID == 20008) || (folderID == 20009)) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.TermoAcordoID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM TermoAcordo a WHERE ' +
        sFiltro + 'a.TermoAcordoID = ' + idToFind;
        return 'dso01JoinSup' + '_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' +
                  'FROM LOGs a WITH(NOLOCK) ' +
                  'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20261) // Texto
    {
        dso.SQL = 'SELECT *, LEFT(Texto, 30) + ' +
                        '(CASE Texto WHEN SPACE(0) THEN SPACE(0) ELSE \'...\' END) AS TextoAbreviado ' +
                    'FROM TermoAcordo_Texto WITH(NOLOCK) ' +
                    'WHERE TermoAcordoID = ' + idToFind;
        
        return 'dso01Grid_DSC';
    }
    else if (folderID == 20262) // Hist�rico
    {
        dso.SQL = 'SELECT a.TermoAcordoID, a.Rev, a.dtAlteracao, a.dtVigencia, ' + 
                         'b.ItemMasculino AS Idioma, a.IdiomaID, a.Texto, c.TipoTermoAcordoID ' +
					 'FROM TermoAcordo_Historico a WITH(NOLOCK) ' +
						  'INNER JOIN TiposAuxiliares_Itens b WITH(NOLOCK) ON(a.IdiomaID = b.ItemID) ' +
						  'INNER JOIN TermoAcordo c WITH(NOLOCK) ON(a.TermoAcordoID = c.TermoAcordoID) ' +
                     'WHERE a.TermoAcordoID = ' + idToFind;
                     
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
       
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Texto
    else if (pastaID == 20261)
    {
        if (dso.id == 'dsoCmb01Grid_01')
        {
            dso.SQL = 'SELECT ItemID AS fldID, ItemMasculino AS fldName FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE (EstadoID = 2 AND TipoID = 48' + ') ' +
                      'ORDER BY ordem ';
        }
    }        
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;
	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(20261);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20261); //Texto

    vPastaName = window.top.getPastaName(20262);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20262); //Hist�rico

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;

    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        // Parametros:
        // grid --> O grid a ser tratado
        // dsoData --> Datasource
        // nLine --> Linha do grid a ser gravada
        // aFields --> Array que contem as colunas obrigatorias conforme a ordem das colunas no grid:
        // O registroID da tabela do grid n�o � passado, a automa��o trata.
        // aID --> string com o nome do campo que linka esta tabela na tabela do SUP, 
        // mais o valor do CampoID da tabela do SUP, se passar 0 nao grava
        // aColsHiddenWriteable --> Array das colunas escondidas que sao gravaveis

        // esta funcao retorna a linha corrente do grid 
        // -1 a gravacao nao sera feita
        // maior que 0 vai gravar o registro
        
        glb_ColTexto = 3;
        
        if (folderID == 20261) // Texto
            currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0], ['TermoAcordoID', registroID], [glb_ColTexto]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var i = 0;
    var dTFormat = '';
    var currID = null;
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }

    else if (folderID == 20261) // Texto
    {
        fg.Editable = true;

        headerGrid(fg, ['_CALC_NoField_1',
                        'Idioma',
                       'Texto' + replicate(' ', 20),
                       'Texto',
                       'TextoTermoAcordoID'], [0, 3, 4]);

        fillGridMask(fg, currDSO, ['_CALC_NoField_1*',
                                 'IdiomaID',
                                 '_CALC_TextoAbreviado*',
                                 'Texto',
                                 'TextoTermoAcordoID'],
                                 ['', '', '', '', ''],
                                 ['', '', '', '', '']);

        if (!(currDSO.recordset.BOF && currDSO.recordset.EOF)) 
        {
            for (i = 1; i < fg.Rows; i++) 
            {
                currDSO.recordset.MoveFirst();

                currID = fg.TextMatrix(i, fg.Cols - 1);

                currDSO.recordset.find('TextoTermoAcordoID', currID);

                if (!currDSO.recordset.EOF) 
                {
                    if (currDSO.recordset['TextoAbreviado'].value != null)
                        fg.TextMatrix(i, getColIndexByColKey(fg, '_CALC_TextoAbreviado*')) = currDSO.recordset['TextoAbreviado'].value;
                    else
                        fg.TextMatrix(i, getColIndexByColKey(fg, '_CALC_TextoAbreviado*')) = '';
                }
                else
                    fg.TextMatrix(i, getColIndexByColKey(fg, '_CALC_TextoAbreviado*')) = '';
            }
        }
        alignColsInGrid(fg);

        fg.autoSizeMode = 0;
        fg.autoSize(0, fg.Cols - 1);

    }
    else if (folderID == 20262) // Hist�rico
    {
        headerGrid(fg, ['ID',
                        'Revis�o',
                        'Dt Altera��o',
                        'Dt Vig�ncia',
                        'Idioma',
                        'IdiomaID',
                        'Texto',
                        'TipoTermoAcordoID'], [5,7]);
        fillGridMask(fg, currDSO, ['TermoAcordoID',
                                   'Rev',
								   'dtAlteracao',
								   'dtVigencia',
								   'Idioma',
								   'IdiomaID',
								   'Texto',
								   'TipoTermoAcordoID'],
								   ['', '', '', '', '', '', '', ''],
								   ['', '', '', '', '', '', '', '']);
    }
}
