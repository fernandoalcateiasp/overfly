using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;

namespace Overfly3.modbasico.submodauxiliar.TermoAcordo.serverside
{
    public partial class clonarRegistro : System.Web.UI.OverflyPage
	{
		private string registroID = "";
        private string usuarioID = "";
        
		private string registroNovoID = null;
        private string mensagem = null;


        protected string sRegistroID
		{
            set { registroID = value != null ? value : ""; }
		}
        protected string nUserID
        {
            set { usuarioID = value != null ? value : ""; }
        }

        /* Roda a procedure sp_Clona_TermoAcordo*/
        private void clonaTermoAcordo()
		{
			ProcedureParameters[] parameters = new ProcedureParameters[4];

            parameters[0] = new ProcedureParameters("@RegistroID", System.Data.SqlDbType.VarChar, registroID);
            
            parameters[1] = new ProcedureParameters("@UsuarioID",System.Data.SqlDbType.VarChar, usuarioID);

            parameters[2] = new ProcedureParameters("@RegistroNovoID", System.Data.SqlDbType.Int, DBNull.Value, ParameterDirection.Output);

            parameters[3] = new ProcedureParameters("@Mensagem", System.Data.SqlDbType.VarChar, DBNull.Value, ParameterDirection.Output, 8000);
			
			DataInterfaceObj.execNonQueryProcedure("sp_Clona_TermoAcordo",	parameters);

            registroNovoID = parameters[2].Data.ToString();
            mensagem = parameters[3].Data.ToString();

		}

		protected override void PageLoad(object sender, EventArgs e)
		{
            clonaTermoAcordo();

			WriteResultXML(
				DataInterfaceObj.getRemoteData(
					"SELECT " + (registroNovoID != null ? "'" + registroNovoID + "' " : "NULL" ) + " AS RegistroID, " + 
                        (mensagem != null ? "'" + mensagem + "' " : "NULL") + " AS Mensagem"
				)
			);
		}
	}
}
