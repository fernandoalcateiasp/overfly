/********************************************************************
commonmodaltermoacordotemplate.js

Library javascript para o termoacordotemplate.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_aReferencias = null;
var glb_sAnchorID = null;
var glb_sAnchorDest = 0;


// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
FUNCOES GERAIS:

window_onload()
img_onload()
window_onload_afterImages()
onmouseout_Img(ctl)
onmouseover_Img(ctl)
onmousemove_Img(ctl)
onclick_Img(ctl)
divAreaSensi_onmouseout(ctl)
divAreaSensi_onmouseclick(ctl)
onmouseover_AreaImg(ctl, imgUsingArea)
onmouseout_AreaImg(ctl, imgUsingArea)
onclick_AreaMap( refArea, refImgAssociada, nDocDestino, nAncoraDestino, nDirection)
gotoFirstAnchorForced()
gotoNull()
gotoAnchorPageLoaded(sAnchorOrig, sAnchorDest)
goto(sElementOrigID, nDocDestID, sAnchorDestID, nDirection)
gotoAnchor(sAnchorOrig, sAnchorDest, nDirection)
forceAnchorNavByFrame( nAction, nDestinationAnchorID)
mountHtmlDocument()
 
********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	window_onload_Prg();
	
	glb_overHTMLMounter.setArrayTextHintAuto(glb_aReferencias);
	
	divAreaSensi_onmouseout();
	
	// configuracao inicial do html
    setupPage();
    
    mountHtmlDocument();

    with (termoacordotemplateBody)
    {
		style.visibility = 'hidden';
        // scroll = 'no';
        leftMargin = 5;
        topMargin = 0;
        style.width = '100%';
        style.height = '100%';
        //style.visibility = 'inherit';
    }
    
    if ( glb_ImgsNumber == 0 )
        glb_showDocument_Timer = window.setInterval( 'window_onload_afterImages()', 250, 'JavaScript');        
}

/********************************************************************
Final de carregamento a partir do banco 
de cada imagem inserida no texto
********************************************************************/
function img_onload()
{
    glb_countImgsNumber++;
    
    if ( glb_ImgsNumber == glb_countImgsNumber)
    {
        glb_showDocument_Timer = window.setInterval( 'window_onload_afterImages()', 250, 'JavaScript');        
	}        
}

/********************************************************************
Todas as imagens do documento vieram do servidor
e estao colocadas no HTML, ja com suas dimensoes reais.
********************************************************************/
function window_onload_afterImages()
{
	if (glb_showDocument_Timer != null)
	{
		window.clearInterval(glb_showDocument_Timer);
		glb_showDocument_Timer = null;
	}
	
	// cria e posiciona os divs para imagens mapeadas
	glb_overHTMLMounter.createDivsForImgsMaps();
	
    // Forca scroll para o topo da pagina
	termoacordotemplateBody.scrollIntoView(true);

	// esta funcao destrava o html contido na janela modal
	window.parent.lockControlsInModalWin(false);
    
    gotoAnchorPageLoaded(null, glb_sAnchorID);
    
    setupControlsAfterImagesLoaded();

    with (termoacordotemplateBody)
    {
		style.visibility = 'visible';
    }

	window.parent.showDocumentFrame(true);
}

/********************************************************************
Mouse saiu de uma imagem 
********************************************************************/
function onmouseout_Img(ctl)
{
	divAreaSensi_onmouseout(divAreaSensi);
}

/********************************************************************
Mouse esta sobre uma imagem
********************************************************************/
function onmouseover_Img(ctl)
{
}

/********************************************************************
Mouse esta se movendo sobre uma imagem
********************************************************************/
function onmousemove_Img(ctl)
{
}

/********************************************************************
Mouse foi clicado em uma imagem
********************************************************************/
function onclick_Img(ctl)
{
	//window.event
	//this.event
	//this.event.srcElement
}

/********************************************************************
Mouse esta sobre uma area sensibilizada da imagem
********************************************************************/
function onmouseover_AreaImg(ctl, imgUsingArea)
{
	with (divAreaSensi.style)
	{
		left = imgUsingArea.offsetLeft + ctl.offsetLeft;
		top = imgUsingArea.offsetTop + ctl.offsetTop;
		width = ctl.offsetWidth;
		height = ctl.offsetHeight;
		visibility = 'visible';
		zIndex = 999;
		cursor = 'hand';
	}
		
	imgUsingArea.style.cursor = 'hand';
}

/********************************************************************
Mouse saiu do div que estava sobre uma area sensibilizada da imagem
********************************************************************/
function divAreaSensi_onmouseout(ctl)
{
	if ( ctl == null )
		ctl = divAreaSensi;

	with (ctl.style)
	{
		visibility = 'hidden';
		zIndex = -1;
	}
}

/********************************************************************
Usuario clicou no div que estava sobre uma area sensibilizada da imagem
********************************************************************/
function divAreaSensi_onmouseclick(ctl)
{
	with (ctl.style)
	{
		visibility = 'hidden';
		zIndex = -1;
	}
}

/********************************************************************
Mouse saiu de uma area sensibilizada da imagem
********************************************************************/
function onmouseout_AreaImg(ctl, imgUsingArea)
{
	imgUsingArea.style.cursor = 'default';
}

/********************************************************************
Mouse foi clicado em uma area sensibilizada da imagem
********************************************************************/
function onclick_AreaMap( refArea, refImgAssociada, nDocDestino, nAncoraDestino, nDirection)
{
	if ( (nDocDestino == null) && (nAncoraDestino == null) )
		return true;

	var numericOfDivAssociated = refArea.getAttribute('numericOfDivAssociated', 1);

	if ( (nDocDestino != null) && (nAncoraDestino != null) )
		goto(numericOfDivAssociated, nDocDestino, nAncoraDestino, nDirection);
	else if ( nDocDestino != null )
		goto(numericOfDivAssociated, nDocDestino, null, nDirection);
	else if ( nAncoraDestino != null )
		gotoAnchor(numericOfDivAssociated, nAncoraDestino, nDirection);
	else
		gotoNull();
}

/********************************************************************
Funcao usada em HREF que nao navega
********************************************************************/
function gotoNull()
{
	return;
}

/********************************************************************
Forca a navegacao para um anchor da primeira pagina carregada
********************************************************************/
function gotoFirstAnchorForced()
{
	if ( window.parent.glb_sAnchorDest == '' )
		return;
	
	objToScroll = glb_overHTMLMounter._getAnchor(window.parent.glb_sAnchorDest.toString());

    if ( objToScroll )
		window.scrollTo( 0, objToScroll.offsetTop );
		
	window.parent.glb_sAnchorDest = '';	
}

/********************************************************************
Automacao tenta navegar para um anchor de uma pagina que acabou de
ser montada.
********************************************************************/
function gotoAnchorPageLoaded(sAnchorOrig, sAnchorDest)
{
	var objToScroll;
	var relockInterface = false;

	if (sAnchorDest == null)
	    sAnchorDest = 0;
	
	// garante que a pilha nao pode ficar zerada
	if ( window.parent.glb_oNavList.qtyElemsInList() == 0 )
	{
		window.parent.glb_oNavList.addAndMakeCurrentInList( new Array(null, null, null, 0) );
	}
	else
		window.parent.btnVoltarStatusAndHint();
	
	setupControlsOnGoToAnchorPageLoaded();

	objToScroll = glb_overHTMLMounter._getAnchor(sAnchorDest.toString());

	if (objToScroll)
    
    {
		window.scrollTo( 0, objToScroll.offsetTop );
		
		if (window.parent.modalWinIsLocked())
		{
			window.parent.lockControlsInModalWin(false);
			relockInterface = true;
		}

		window.parent.saveCurrentPosInNavList(glb_nTermoAcordoID, sAnchorDest, glb_nIdiomaID, 1);
			
		if ( relockInterface )
			window.parent.lockControlsInModalWin(true);
	}	

	gotoFirstAnchorForced();

    return;
}

/********************************************************************
Usuario navega para outra pagina.

********************************************************************/
function goto(sElementOrigID, nDocDestID, sAnchorDestID, nDirection)
{
	window.parent.lockControlsInModalWin(true);
	
	window.parent.showDocumentFrame(false);
	
	// Se esta navegando na mesma pagina a ancora origem e gravada
	// apenas se ela existe. 
	if (sElementOrigID != null)
	{
		// verifica se a navegacao anteriormente feita e um anchor automatico
		// ou seja, colocado por carregamento de pagina, 
		// remove a marca de automatico
		if ( window.parent.glb_oNavList.qtyElemsInList() > 0 )
			window.parent.glb_oNavList.currentInList()[3] = 0;
		
		window.parent.glb_oNavList.nDirection = 1;
		
		window.parent.lockControlsInModalWin(false);
		window.parent.saveCurrentPosInNavList(glb_nTermoAcordoID, sElementOrigID, glb_nIdiomaID);
		window.parent.lockControlsInModalWin(true);
	}

	if (nDirection != null)
	{
		window.parent.glb_oNavList.nDirection = nDirection;
	}	
	else		
		window.parent.glb_oNavList.nDirection = 1;
	
	window.parent.loadPageFromServerInFrame(nDocDestID, sAnchorDestID, null);
}

/********************************************************************
Usuario navega para outra posicao dentro da pagina
********************************************************************/
function gotoAnchor(sAnchorOrig, sAnchorDest, nDirection)
{
	var objToScroll;
	
	setupControlsOnGoToAnchor();
	
	objToScroll = glb_overHTMLMounter._getAnchor(sAnchorDest.toString());
	
	// Se tem objeto a scrollar
    if ( objToScroll )
    {
		// verifica se a navegacao anteriormente feita e um anchor automatico
		// ou seja, colocado por carregamento de pagina, 
		// remove a marca de automatico
		if ( window.parent.glb_oNavList.qtyElemsInList() > 0 )
			window.parent.glb_oNavList.currentInList()[3] = 0;

		window.scrollTo( 0, objToScroll.offsetTop );
		
		if (nDirection != null)
		{
			window.parent.glb_oNavList.nDirection = nDirection;
		}	
		
		// Quando a navegacao e para a frente e por clique do usuario
		// guarda a origem e o destino na pilha
		if ( window.parent.glb_oNavList.nDirection >= 0 )
		{
			if (sAnchorOrig != null)
			{
			    window.parent.saveCurrentPosInNavList(glb_nTermoAcordoID, sAnchorOrig, glb_nIdiomaID);
			    window.parent.saveCurrentPosInNavList(glb_nTermoAcordoID, sAnchorDest, glb_nIdiomaID, 1);
			}
		}
	}	

    return;
}

/********************************************************************
Navega para outro ponto da pagina ou scrolla para o topo da mesma.
Atualmente esta funcao e usada pelo frame na navegacao para traz,
quando o frame decide se carrega ou nao uma nova pagina
********************************************************************/
function forceAnchorNavByFrame( nAction, nDestinationAnchorID)
{
	glb_nAction = nAction;
	glb_sAnchorID = nDestinationAnchorID;
	
	// Forca scroll para o topo da pagina
	if ( nDestinationAnchorID == '__0' )
	{
	    window.scrollTo( 0, 0);
	}
	else
		gotoAnchor(null, glb_sAnchorID);    
}

/********************************************************************
Monta o HTML do documento e troca a cor do background da barra
superior da modal
********************************************************************/
function mountHtmlDocument()
{
	glb_overHTMLMounter.setTextBase(glb_sTexto);
	
	if (glb_overHTMLMounter.mountInnerHTML())
	    divTermoAcordo.innerHTML = glb_overHTMLMounter.getInnerHTML();
	else
	    divTermoAcordo.innerHTML = '';
}