/********************************************************************
modaltermoacordo.js

Library javascript para o modaltermoacordo.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_nTipoFirstDocToLoad = null;
var glb_loadDocumentoTimer = null;
var glb_oNavList = new _OverList();
var glb_nEmpresaID = 0;
var glb_sAnchorDest = '';
// FINAL DE VARIAVEIS GLOBAIS ***************************************

var aEmpresa = getCurrEmpresaData();
/********************************************************************
FUNCOES GERAIS:

window_onload_prg()
mountModalAccordingToContext()
loadPageFromServerInFrame(nTermoAcordoID, sAnchorID, nTipoDocumento, bNavInSameDoc)
strParsToLoadPageFromServer(nTermoAcordoID, nTipoDocumento, sAnchorID, nAction )

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Funcao do programador chamada pela automacao.
Window on Load de uso do programador
********************************************************************/
function window_onload_prg()
{
	;
}    

/********************************************************************
Funcao do programador chamada pela automacao.
Esconde e desabilita os controles nao usados no contexto corrente
Faz a montagem da tela dos controles usados no contexto corrente
********************************************************************/
function mountModalAccordingToContext()
{
	with (btnCanc)
	{
		style.left = 0;
		style.top = 0;
		style.height = 0;
		style.width = 0;
		style.visibility = 'hidden';
		disabled = true;
	}

	adjustElementsInForm([['lblIdioma', 'selIdiomaID', 14, 1, 0, 20],
	                      ['lblTermoacordo', 'selTermoacordo', 48, 1],
                          ['lblRevisao','txtRevisao',4,1],
                          ['lbldtVigencia', 'txtdtVigencia', 10, 1],
                          ['lblTotPags','txtTotPags',3,1],
                          ['btnVoltar','btn',62,1,5]], null, null, true);

	 
	lblTermoacordo.style.width = 'auto';
	txtRevisao.readOnly = true;
	txtdtVigencia.readOnly = true;
	txtTotPags.readOnly = true;

	txtRevisao.value = '';
	txtdtVigencia.value = '';
	btnVoltar.title = '';

	btnOK.style.visibility = 'hidden';
	btnOK.disabled = true;

	btnVoltar.style.height = parseInt(btnOK.currentStyle.height, 10);
	btnVoltar.value = 'Retornar';
	
}

function selIdiomaID_onchange() 
{
    nselIdiomaID = selIdiomaID.value;
}
/********************************************************************
Funcao do programador chamada pela automacao.
Carrega as paginas do servidor.

Parametros:
	nTermoAcordoID - id do documento a carregar
	sAnchorID - navegacao automatica para um achor da pagina a ser
	            executado apos o carregamento
	nTipoDocumento - tipo do documento
	bNavInSameDoc - flag para executar o carregamento ou nao.
	                Eventualmente sera executada apenas uma navegacao
	                para un anchor
********************************************************************/
function loadPageFromServerInFrame(nTermoAcordoID, sAnchorID, nselIdiomaID, bNavInSameDoc)
{
	if ( glb_loadDocumentoTimer != null )
	{
		window.clearInterval(glb_loadDocumentoTimer);
		glb_loadDocumentoTimer = null;
	}
	// Automacao - nao mexer na chamada abaixo
	loadPageFromServerInFrame_Aut(nTermoAcordoID, sAnchorID, nselIdiomaID, bNavInSameDoc);
}	

/********************************************************************
Funcao do programador chamada pela automacao 
Define parametros para carregar documento do servidor
Aqui usamos a variavel glb_sCallOrigin
********************************************************************/
function strParsToLoadPageFromServer(nTermoAcordoID, nselIdiomaID, sAnchorID, nAction)
{
    selIdiomaID.onchange = selIdiomaID_onchange;
    var nUserID = getCurrUserID();
	var strPars = new String();
	nselIdiomaID = selIdiomaID.value;

	selIdiomaID.selected = 247;
	
	strPars = '?nEmpresaID=' + escape(glb_nEmpresaID);
	strPars += '&nTermoAcordoID=' + escape(nTermoAcordoID);
    strPars += '&nIdiomaID=' + escape(nselIdiomaID);
	strPars += '&nUserID=' + escape(nUserID);
	strPars += '&nAction=' + escape(nAction);
	strPars += '&sAnchorID=' + escape((sAnchorID == null ? '__0' : sAnchorID));
	if (glb_sCaller == 'I') 
	{
	    strPars += '&sCaller=' + escape(glb_sCaller);
	}
	else 
	{
	    strPars += '&sCaller=NULL';
	}
	strPars += '&nRevisao=' + escape(glb_nRevisao);
	
	return strPars;
}
