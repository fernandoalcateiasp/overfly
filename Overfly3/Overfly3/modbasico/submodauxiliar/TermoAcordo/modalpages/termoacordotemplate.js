/********************************************************************
termoacordotemplate.js

Library javascript para o termoacordotemplate.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

var glb_overHTMLMounter = new _OverHtmlMounter();
var glb_countImgsNumber = 0;
var glb_showDocument_Timer = null;
var glb_aTermoAcordo = 0;
var glb_nTermoAcordoID = 0;
var glb_Revisao = '';
var glb_dtVigencia = '';
var glb_sTermoAcordo = '';
var glb_TermoAcordoIdioma = '';
var glb_sTexto = '';
var glb_ImgsNumber = '';
var dsoTermoAcordo = new CDatatransport("dsoTermoAcordo");
var dsoComboIdioma = new CDatatransport("dsoComboIdioma");
// FINAL DE VARIAVEIS GLOBAIS ***************************************
/********************************************************************
FUNCOES GERAIS:

window_onload_Prg()
setupControlsAfterImagesLoaded()
setupPage()
fillComboDocumento()
selTermoacordo_onchange()
setupControlsOnGoToAnchorPageLoaded()
setupControlsOnGoToAnchor()
openSite(site)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Funcao chamada pela automacao.
Uso do programador.
Vai iniciar o window_onload.

Retorno - irrelevante
********************************************************************/
function window_onload_Prg()
{
	return null;
}

/********************************************************************
Funcao chamada pela automacao.
Uso do programador.
Configuracao final da pagina, apos carregar todas as imagens
********************************************************************/
function setupControlsAfterImagesLoaded()
{
    var nHeighMM;
    
    // Altura do documento em milimetros
    nHeighMM = (divTermoAcordo.offsetTop + divTermoAcordo.offsetHeight) / 3.75;
    
    // Total de Paginas
    window.parent.txtTotPags.value = Math.ceil( nHeighMM / 257 );
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
	var i;
	var oOption;

	with (divTermoAcordo.style)
	{
		top = 0;
		left = 0;
		width = '100%';
		height = '100%';
		paddingTop = 5;
		paddingLeft = 5;
		paddingRight = 5;
		paddingBottom = 5;
	}

	fillComboTermoAcordo();

	window.parent.selTermoacordo.value = glb_nTermoAcordoID;
	window.parent.selTermoacordo.onchange = selTermoacordo_onchange;
	window.parent.selIdiomaID.onchange = selIdiomaID_onchange;
	window.parent.txtRevisao.value = glb_Revisao;
	window.parent.txtdtVigencia.value = glb_dtVigencia;

	try 
	{
	    glb_sTitleWindow = window.parent.glb_sTitulo;
	}
	catch (e) 
	{
	    ;
	}
	window.parent.secText(glb_sTitleWindow, 1);

	window.parent.lblTermoacordo.innerText = 'Termo de acordo ' + glb_sTermoAcordo;
}

function fillComboTermoAcordo()
{
	var i;
	var oOption;

	window.parent.selTermoacordo.disabled = true;
	
	window.parent.clearComboEx(['selTermoacordo']);

	for (i = 0; i<glb_aTermoAcordo.length; i++)
	{
		window.parent.selTermoacordo.disabled = false;
        oOption = document.createElement("OPTION");
        oOption.value = glb_aTermoAcordo[i][0];
        oOption.text = glb_aTermoAcordo[i][1];
        window.parent.selTermoacordo.add(oOption);
    }
}

/********************************************************************
Usuario trocou o option do combo de Termo de acordo
********************************************************************/
function selTermoacordo_onchange()
{
	window.parent.glb_oNavList.initializeList();
	
	window.parent.btnVoltarStatusAndHint();
	
	goto(null, window.parent.selTermoacordo.value, null);
}

function selIdiomaID_onchange() 
{
    window.parent.glb_oNavList.initializeList();

    window.parent.btnVoltarStatusAndHint();

    goto(null, window.parent.selTermoacordo.value, null);
}


/********************************************************************
Chamada pela automacao.
De uso  do programador
Usuario navega para outra posicao dentro da pagina que acabou de ser
carregada
********************************************************************/
function setupControlsOnGoToAnchorPageLoaded()
{
	
}

/********************************************************************
Chamada pela automacao.
De uso  do programador
Usuario navega para outra posicao dentro da pagina
********************************************************************/
function setupControlsOnGoToAnchor()
{
	
}

/********************************************************************
Abre um URL em uma nova janela do browser
********************************************************************/
function openSite(site)
{
    window.open(site);
}
