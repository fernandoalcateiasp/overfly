
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    
    'Forca recarregamento da pagina
    Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
    
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot, discPrgRoot
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    discPrgRoot = objSvrCfg.RootLogicAppPath(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
	'Esta funcao retorna a string de pesquisa no banco
	'de uma imagem. Se a imagem nao existe, retorna ""
	'IMPORTANTE: Esta funcao deve ficar apos o trecho de codigo ASP
	'que cria a variavel strConn
	Function imageSourceString (nFormID, nSubFormID, nRegistroID)
		Dim rsData, strSQLGet
		Dim strRoot, srvName
		Set rsData = Server.CreateObject("ADODB.Recordset")		
	
		strSQLGet = "SELECT TOP 1 a.DocumentoID AS ImagemID FROM [OVERFLYBASE_EXT2].[Overfly_Documentos].[dbo].[Documentos] a WITH(NOLOCK) " & _
	                "WHERE a.FormID = " & CStr(nFormID) & " AND "& _
	                "a.SubFormID = " & CStr(nSubFormID) & " AND "& _
	                "a.RegistroID = " & CStr(nRegistroID) & " AND " & _
	                "a.Arquivo IS NOT NULL " & _
                    "a.TipoArquivoID = 1451 " & _
	                "ORDER BY a.DocumentoID"
	
		rsData.Open strSQLGet, strConn, adOpenStatic , adLockReadOnly, adCmdText
	
		If (rsData.RecordCount = 0) Then
			rsData.Close
			Set rsData = Nothing
			imageSourceString = ""
			Exit Function
		End If
		
		rsData.Close
		Set rsData	 = Nothing
	
		srvName = Request.ServerVariables("SERVER_NAME")
		
		If ( Instr(1,UCase(srvName), ".COM", 1)= 0	) Then
			strRoot = "http:/" & "/localhost/overfly3"
		Else
			strRoot = pagesURLRoot	
		End If
	
        imageSourceString = strRoot & "/serversidegen/imageblob.asp" & _
                            "?nFormID=" & CStr(nFormID) & _
                            "&nSubFormID=" & CStr(nSubFormID) & _
                            "&nRegistroID=" & CStr(nRegistroID)	
	End Function
	
Function ReplaceText(baseStr, patrn, replStr)
    Dim regEx                         ' Create variables.
    Set regEx = New RegExp            ' Create regular expression.
    regEx.Pattern = patrn             ' Set pattern.
    regEx.IgnoreCase = True           ' Make case insensitive.
    regEx.Global = True               ' Replace globally
    ReplaceText = regEx.Replace(baseStr, replStr)   ' Make replacement.
End Function

Function MatchText(baseStr, patrn)
    Dim regEx, Match, Matches    ' Create variable.
    Set regEx = New RegExp       ' Create regular expression.
    regEx.Pattern = patrn        ' Set pattern.
    regEx.IgnoreCase = True      ' Set case insensitivity.
    regEx.Global = True          ' Set global applicability.
    Set Matches = regEx.Execute(baseStr)   ' Execute search.
    MatchText = Matches.Count
End Function
%>

<html id="termoacordotemplateHtml" name="termoacordotemplateHtml">

<head>

<title></title>

<%
	Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modbasico/submodauxiliar/TermoAcordo/modalpages/termoacordotemplate.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_overhtmlmounter.js" & Chr(34) & "></script>" & vbCrLf                    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
            
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modbasico/submodauxiliar/TermoAcordo/modalpages/commonmodaltermoacordotemplate.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modbasico/submodauxiliar/TermoAcordo/modalpages/termoacordotemplate.js" & Chr(34) & "></script>" & vbCrLf

'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, nEmpresaID, nTermoAcordoID, nIdiomaID, sTexto, nFormatoData, sTitleWindow, sState
Dim nAction, sAnchorID
Dim nUserID
Dim nCounter
Dim sSignificado
Dim rsDataIdioma
Dim nSemTexto

Dim rsData, rsData2, rsData3, rsDataPrincipal, rsData5, rsData6, rsDataCheck
Dim strSQL, strSQL2, strSQL3, strSQLCheck
Dim rsSPCommand
Dim rsDataIPars
Dim strSQLIPars
Dim nEstadoID, sTransicao
Dim bFlag, nAreaDocMapID
Dim sImageWebFullPath
Dim docDestino
Dim ancoraDestino
Dim nEstadoFN1
Dim nEstadoFN2
Dim sColor
Dim sFiltro
Dim sCaller
Dim nCaller
Dim nRevisao

sAnchorID = ""
nEmpresaID = 0
nTermoAcordoID = 0
nIdiomaID = 0
sTexto = ""
nFormatoData = 103
sTitleWindow = ""
nAction = -2
nUserID = 0
sState = ""
bFlag = False
nEstadoFN1 = 0
nEstadoFN2 = 0
sColor = "Transparent"
sFiltro = ""

For i = 1 To Request.QueryString("nEmpresaID").Count
    nEmpresaID = Request.QueryString("nEmpresaID")(i)
Next
For i = 1 To Request.QueryString("nTermoAcordoID").Count
    nTermoAcordoID = Request.QueryString("nTermoAcordoID")(i)
Next
For i = 1 To Request.QueryString("nIdiomaID").Count
    nIdiomaID = Request.QueryString("nIdiomaID")(i)
Next
For i = 1 To Request.QueryString("sAnchorID").Count
    sAnchorID = Request.QueryString("sAnchorID")(i)
Next
If CStr(nIdiomaID) = "NULL" Then
	nIdiomaID = Null
End If
For i = 1 To Request.QueryString("nFormatoData").Count
    nFormatoData = Request.QueryString("nFormatoData")(i)
Next
For i = 1 To Request.QueryString("nAction").Count
    nAction = Request.QueryString("nAction")(i)
Next
For i = 1 To Request.QueryString("nUserID").Count
    nUserID = Request.QueryString("nUserID")(i)
Next
For i = 1 To Request.QueryString("sCaller").Count
    sCaller = Request.QueryString("sCaller")(i)
Next
For i = 1 To Request.QueryString("nRevisao").Count
    nRevisao = Request.QueryString("nRevisao")(i)
Next

Response.Write "var glb_nTermoAcordoID = " & CStr(nTermoAcordoID) & ";"
Response.Write vbcrlf
Response.Write "var glb_nTermoAcordo = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_Revisao = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_dtVigencia = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_sTexto = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_sTitleWindow = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nAction = " & CStr(nAction) & ";"
Response.Write vbcrlf
Response.Write "var glb_sAnchorID = " & Chr(39) & sAnchorID & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_aTermoAcordo = new Array();"
Response.Write vbcrlf
Response.Write "var glb_TermoAcordoIdioma = new Array();"
Response.Write vbcrlf
Response.Write "var glb_aReferencias = new Array();"
Response.Write vbcrlf
Response.Write "var glb_sTermoAcordo = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf

If (IsNull(nIdiomaID)) Then
	Response.Write "glb_nIdiomaID = null;"
Else
	Response.Write "glb_nIdiomaID = " & CStr(nIdiomaID) & ";"
End If

if sCaller = "I" Then
    nCaller = 0
Else
    nCaller = 1
End If

if nCaller = 1 Then
    nRevisao = 0
End If

'Caso a empresa for Abano, utiliza o mesmo Termo da Alcateia
If (nEmpresaID = 10) Then
    nEmpresaID = 2
End If

Response.Write vbcrlf
Response.Write "var glb_ImgsNumber = 0;" & vbCrLf
Response.Write vbcrlf

Set rsData = Server.CreateObject("ADODB.Recordset")

'Combo - Termo de Acordo
if nCaller = 1 Then
    strSQL = "SELECT dbo.fn_Recursos_Campos(2,2) AS Estado, NULL AS TermoAbreviado, a.TermoAcordoID AS TermoAcordoID, " & _
				"CONVERT(VARCHAR,a.TipoTermoAcordoID) + SPACE(1) + ItemMasculino AS TermoAcordo, d.ItemMasculino AS sTermoAcordo, " & _
				"a.Revisao AS Revisao, CONVERT(VARCHAR,a.dtVigencia,103) AS dtVigencia, " & _
				"NULL AS Texto, NULL AS TitleWindow, NULL AS TipoTermoID, c.Fantasia AS Proprietario " & _
		"FROM TermoAcordo a WITH(NOLOCK) " & _
			 "INNER JOIN TermoAcordo_Texto b WITH(NOLOCK) ON a.TermoAcordoID = b.TermoAcordoID " & _
			 "INNER JOIN Pessoas c WITH(NOLOCK) ON a.ProprietarioID = c.PessoaID " & _
			 "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.TipoTermoAcordoID = d.ItemID " & _
	    "WHERE d.TipoID = 610 AND a.EmpresaID = " & nEmpresaID & "" & _
	"GROUP BY a.TermoAcordoID, a.Revisao, CONVERT(VARCHAR,a.dtVigencia,103), c.Fantasia, a.TipoTermoAcordoID, " & _
			 "d.ItemMasculino "
Else
strSQL = "SELECT dbo.fn_Recursos_Campos(2,2) AS Estado, NULL AS TermoAbreviado, a.TermoAcordoID AS TermoAcordoID, " & _
				"CONVERT(VARCHAR,a.TipoTermoAcordoID) + SPACE(1) + ItemMasculino AS TermoAcordo, d.ItemMasculino AS sTermoAcordo, " & _
				"b.Rev AS Revisao, CONVERT(VARCHAR,a.dtVigencia,103) AS dtVigencia, " & _
				"NULL AS Texto, NULL AS TitleWindow, NULL AS TipoTermoID, c.Fantasia AS Proprietario " & _
		"FROM TermoAcordo a WITH(NOLOCK) " & _
			 "INNER JOIN TermoAcordo_Historico b WITH(NOLOCK) ON a.TermoAcordoID = b.TermoAcordoID " & _
			 "INNER JOIN Pessoas c WITH(NOLOCK) ON a.ProprietarioID = c.PessoaID " & _
			 "INNER JOIN TiposAuxiliares_Itens d WITH(NOLOCK) ON a.TipoTermoAcordoID = d.ItemID " & _
	   "WHERE (d.TipoID = 610 AND b.Rev = " & nRevisao & ") " & _
	"GROUP BY a.TermoAcordoID, b.Rev, CONVERT(VARCHAR,a.dtVigencia,103), c.Fantasia, a.TipoTermoAcordoID, " & _
			 "d.ItemMasculino "
End If
			          
    sState = ""		
    sColor = "#F0E68C"		

rsData.Open strSQL, strConn, adOpenStatic, adLockReadOnly, adCmdText
            
Response.Write vbcrlf

rsData.Filter = "TermoAcordoID = " & CStr (nTermoAcordoID)

If (Not rsData.EOF) Then
	
	rsData.MoveLast
	rsData.MoveFirst

	If ( Not IsNull(rsData.Fields("TermoAcordo").Value)) Then
		Response.Write "glb_nTermoAcordo = " & Chr(34) & rsData.Fields("TermoAcordo").Value & Chr(34) & ";"
	End If

	If ( Not IsNull(rsData.Fields("TermoAcordoID").Value)) Then
		nTermoAcordoID = rsData.Fields("TermoAcordoID").Value
	End If

	If ( Not IsNull(rsData.Fields("Revisao").Value)) Then
		Response.Write "glb_Revisao = " & Chr(34) & rsData.Fields("Revisao").Value & Chr(34) & ";"
	End If

	If ( Not IsNull(rsData.Fields("dtVigencia").Value)) Then
		Response.Write "glb_dtVigencia = " & Chr(34) & rsData.Fields("dtVigencia").Value & Chr(34) & ";"
	End If

	If ( Not IsNull(rsData.Fields("TitleWindow").Value)) Then
		If (sState <> "") Then
			Response.Write "glb_sTitleWindow = " & Chr(34) & rsData.Fields("TitleWindow").Value & " (" & sState & ")" & Chr(34) & ";"
		Else
			Response.Write "glb_sTitleWindow = " & Chr(34) & rsData.Fields("TitleWindow").Value & Chr(34) & ";"
		End If
	End If


	
		'0 Indice
		Set rsData6 = Server.CreateObject("ADODB.Recordset")
	
		Set rsSPCommand = Server.CreateObject("ADODB.Command")

		With rsSPCommand
			.CommandTimeout = 60 * 10
		    .ActiveConnection = strConn
		    .CommandText = "sp_TermoAcordo_Dados"
		    .CommandType = adCmdStoredProc
		    .Parameters.Append( .CreateParameter("@TermoAcordoID", adInteger, adParamInput, 8 , nTermoAcordoID) )
		    If ( IsNull(nIdiomaID) ) Then 
				.Parameters.Append( .CreateParameter("@IdiomaID", adInteger, adParamInput, 8 , null) )
			Else	
				.Parameters.Append( .CreateParameter("@IdiomaID", adInteger, adParamInput, 8 , nIdiomaID) )
		    End If
		    .Parameters.Append( .CreateParameter("@TipoResultado", adInteger, adParamInput, 8 , 0) )
		    .Parameters.Append( .CreateParameter("@TipoTexto", adInteger, adParamInput, 8 , nCaller) )
		    .Parameters.Append( .CreateParameter("@Revisao", adInteger, adParamInput, 8 , nRevisao) )
		    Set rsData6 = .Execute
		End With
		
		If (rsData6.Fields.Count <> 0) Then		
			If (Not rsData6.EOF) Then
			sTexto = sTexto & "{<B>0 �NDICE</B>^I(0)}<BR><BR>"		
				While (Not rsData6.EOF)

					sTexto = sTexto & "{" & rsData6.Fields("Indice").Value & "^A(" & _
						rsData6.Fields("Ancora").Value & ")}<BR>"
					rsData6.MoveNext

				Wend
					
			Else
				sTexto = sTexto & ""
				nSemTexto = 1		
			End If
		Else
			sTexto = sTexto & ""
		End If
	
		sTexto = sTexto & "<BR><BR>"

		rsData6.Close   
	
		Set	rsData6 = Nothing
		
        if (nSemTexto <> 1) Then
		    sTexto = "<FONT STYLE='BACKGROUND-COLOR:" & sColor & "'> " & _
			"<TABLE ALIGN=CENTER BORDER=2 CELLSPACING=0 CLASS='lblTable'> " & _
				"{<TR>^I(-1)} " & _
					"<TH>Termo de Acordo</TH> " & _
					"<TH>Estado</TH> " & _
					"<TH>ID</TH> "

	        If ( Not IsNull(rsData.Fields("TermoAbreviado").Value) ) Then
		        sTexto = sTexto & "<TH>Abrev</TH> "
	        End If
        	
	        sTexto = sTexto & "<TH>Rev</TH> " & _
					          "<TH>Vig�ncia</TH> " & _
					          "<TH>Aprova��o</TH> "

	        sTexto = sTexto & "</TR> "
        	
	        sTexto = sTexto & "<TR> " & _
						        "<TH>" & rsData.Fields("sTermoAcordo").Value & "</TH> " & _
						        "<TH>" & rsData.Fields("Estado").Value & "</TH> " & _
						        "<TH>" & rsData.Fields("TermoAcordoID").Value & "</TH> "

	        sTexto = sTexto & "<TH>" & rsData.Fields("Revisao").Value & "</TH> "
        					  
	        If (Not isNull(rsData.Fields("dtVigencia").Value)) Then
		        sTexto = sTexto & "<TH>" & rsData.Fields("dtVigencia").Value & "</TH> "
	        Else
		        sTexto = sTexto & "<TH>&nbsp</TH> "
	        End If					  

	        If (Not isNull(rsData.Fields("dtVigencia").Value)) Then
		        sTexto = sTexto & "<TH>" & rsData.Fields("Proprietario").Value & "</TH> "
	        Else
		        sTexto = sTexto & "<TH>&nbsp</TH> "
	        End If					  

	        sTexto = sTexto & "</TR> " & _
			    "</TABLE> " & _
		    "</FONT> " & _
		    "<BR />"
		End If
		
		Set rsDataPrincipal = Server.CreateObject("ADODB.Recordset")
	
		Set rsSPCommand = Server.CreateObject("ADODB.Command")

		With rsSPCommand
			.CommandTimeout = 60 * 10
		    .ActiveConnection = strConn
		    .CommandText = "sp_TermoAcordo_Dados"
		    .CommandType = adCmdStoredProc
		    .Parameters.Append( .CreateParameter("@TermoAcordoID", adInteger, adParamInput, 8 , nTermoAcordoID) )
		    If ( IsNull(nIdiomaID) ) Then 
				.Parameters.Append( .CreateParameter("@IdiomaID", adInteger, adParamInput, 8 , null) )
			Else	
				.Parameters.Append( .CreateParameter("@IdiomaID", adInteger, adParamInput, 8 , nIdiomaID) )
		    End If
		    .Parameters.Append( .CreateParameter("@TipoResultado", adInteger, adParamInput, 8 , 2) )
		    .Parameters.Append( .CreateParameter("@TipoTexto", adInteger, adParamInput, 8 , nCaller) )
		    .Parameters.Append( .CreateParameter("@Revisao", adInteger, adParamInput, 8 , nRevisao) )
		    Set rsDataPrincipal = .Execute
		End With
		
		If (rsDataPrincipal.Fields.Count <> 0) Then		
			If (Not rsDataPrincipal.EOF) Then
					
				While (Not rsDataPrincipal.EOF)
                    sTexto = sTexto & rsDataPrincipal.Fields("Texto").Value & "<BR>"
					rsDataPrincipal.MoveNext
				Wend
					
			Else
				sTexto = sTexto & "N�o dispon�vel neste idioma."			
			End If
		Else
			sTexto = sTexto & ""
		End If
	
		sTexto = sTexto & "<BR><BR>"

		rsDataPrincipal.Close
		Set	rsDataPrincipal = Nothing




	sTexto = sTexto & Trim(rsData.Fields("Texto").Value)
     
	'Monta Array com os Termos de Acordo
	
	nCounter = 0
	
	rsData.Filter = ""
	rsData.MoveLast
	rsData.MoveFirst

	While (NOT rsData.EOF)
		Response.Write "glb_aTermoAcordo[" & CLng(nCounter) & "] = new Array();"
		Response.Write vbcrlf
		Response.Write "glb_aTermoAcordo[" & CLng(nCounter) & "][0] = " & rsData.Fields("TermoAcordoID").Value & ";"
		Response.Write vbcrlf
		Response.Write "glb_aTermoAcordo[" & CLng(nCounter) & "][1] = " & Chr(34) & rsData.Fields("TermoAcordo").Value & Chr(34) & ";"
		rsData.MoveNext
		nCounter = nCounter + 1
	Wend
	
    
	
    If ( Not IsNull(sTexto)) Then
        sTexto = ReplaceText( sTexto, Chr(13) & Chr(10), "")
        sTexto = ReplaceText( sTexto, Chr(34), "")
    End If
			
    If ( Not IsNull(sTexto)) Then
        sTexto = ReplaceText(sTexto, "<IMG", "<IMG language='JavaScript' " & _
				 "onmouseout='return onmouseout_Img(this)' " & _
				 "onmouseover='return onmouseover_Img(this)' " & _
				 "onmousemove='return onmousemove_Img(this)' " & _
				 "onclick='return onclick_Img(this)' " & _
				 "onload='return img_onload()' " & _
				 "onerror='return img_onload()' ")
    End If

    Response.Write "glb_ImgsNumber += " & (MatchText(sTexto, "img_onload") / 2) & ";" & vbCrLf
    Response.Write "glb_sTexto = " & Chr(34) & sTexto & Chr(34) & ";" & vbCrLf
    
    Set rsSPCommand = Nothing
    
End If

rsData.Close
Set rsData = Nothing

'rsDataIdioma.Close
'Set rsDataIdioma = Nothing

Response.Write "</script>"
Response.Write vbcrlf
%>
<script ID="wndJSProc" LANGUAGE="javascript">
<!--
//-->
</script>

</head>

<body id="termoacordotemplateBody" name="termoacordotemplateBody" LANGUAGE="javascript" onload="return window_onload()">

	<font face="Tahoma, Verdana, Helvetica, sans-serif">
	<div id="divTermoAcordo" name="divTermoAcordo" class="fldGeneral"></div>
	</font>
	<div id="divAreaSensi" name="divAreaSensi" class="fldAreaSensi"  LANGUAGE="javascript" onmouseout="return divAreaSensi_onmouseout(this)" onmouseclick="returndivAreaSensi_onmouseclick(this)"></div>
</body>

</html>
