<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    
    'Forca recarregamento da pagina
    Response.ExpiresAbsolute=#May 31,1996 13:30:15# 
    
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    Dim strConn
    Dim rsData, strSQL
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modaltermoacordoHtml" name="modaltermoacordoHtml">
<head>
    <title></title>
    <%
'Links de estilo, bibliotecas da automacao e especificas
Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modalgen/modal.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
           
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_overlist.js" & Chr(34) & "></script>" & vbCrLf
            
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modbasico/submodauxiliar/TermoAcordo/modalpages/commonmodaltermoacordo.js" & Chr(34) & "></script>" & vbCrLf
Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modbasico/submodauxiliar/TermoAcordo/modalpages/modaltermoacordo.js" & Chr(34) & "></script>" & vbCrLf
    %>

    <%
'Script de variaveis globais
Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

' Declare vari�veis da p�gina e define parametros para elas.
Dim i
Dim sCaller
Dim sTitulo
Dim nTermoAcordoID
Dim nIdiomaID
Dim sAnchorID
Dim sCallOrigin
Dim nTipoTermoAcordoModal
Dim nRevisao
Dim aEmpresa

sCaller = ""
sTitulo = ""
nTermoAcordoID = 0
sAnchorID = ""
sCallOrigin = ""
nTipoTermoAcordoModal = 0

' Recebe os paramtros do ChildMain
For i = 1 To Request.QueryString("sCaller").Count
    sCaller = Request.QueryString("sCaller")(i)
Next
For i = 1 To Request.QueryString("sTitulo").Count
    sTitulo = Request.QueryString("sTitulo")(i)
Next
For i = 1 To Request.QueryString("nTermoAcordoID").Count
    nTermoAcordoID = Request.QueryString("nTermoAcordoID")(i)
Next
For i = 1 To Request.QueryString("nIdiomaID").Count
    nIdiomaID = Request.QueryString("nIdiomaID")(i)
Next
For i = 1 To Request.QueryString("sAnchorID").Count
    sAnchorID = Request.QueryString("sAnchorID")(i)
Next
For i = 1 To Request.QueryString("sCallOrigin").Count
    sCallOrigin = Request.QueryString("sCallOrigin")(i)
Next
For i = 1 To Request.QueryString("nTipoTermoAcordoID").Count
    nTipoTermoAcordoModal = Request.QueryString("nTipoTermoAcordoID")(i)
Next
For i = 1 To Request.QueryString("nRevisao").Count
    nRevisao = Request.QueryString("nRevisao")(i)
Next
For i = 1 To Request.QueryString("aEmpresa").Count
    aEmpresa = Request.QueryString("aEmpresa")(i)
Next

'Alimenta os paramtros de vari�vies Globais
Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_sTitulo = " & Chr(39) & sTitulo & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nTermoAcordoID = " & nTermoAcordoID & ";"
Response.Write vbcrlf
Response.Write "var glb_nTermoAcordoPesqID = " & nTermoAcordoID & ";"
Response.Write vbcrlf
Response.Write "var glb_sAnchorID__ = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
if ( Not IsNull(sAnchorID) ) Then
	Response.Write "glb_sAnchorID__ = " & Chr(39) & sAnchorID & Chr(39) & ";"
End If	
Response.Write "var glb_sCallOrigin = " & Chr(39) & sCallOrigin & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nTipoTermoAcordoModal = " & Chr(39) & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nIdiomaID = " & Chr(39) & nIdiomaID & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "var glb_nRevisao = " & Chr(39) & nRevisao & Chr(39) & ";"
Response.Write vbcrlf
Response.Write "</script>"
Response.Write vbcrlf
    %>

    <script id="wndJSProc" language="javascript">
<!--
    //-->
    </script>
</head>
<body id="modaltermoacordoBody" name="modaltermoacordoBody" language="javascript" onload="return window_onload()">
    <p id="lblIdioma" name="lblIdioma" class="lblGeneral">Idioma</p>
    <select id="selIdiomaID" name="selIdiomaID" class="fldGeneral">
        <%'Carrega o ComboBox de idiomas
	      If(aEmpresa <> 7) Then
            Set rsData = Server.CreateObject("ADODB.Recordset")
			strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _
                        "WHERE (EstadoID = 2 AND TipoID = 48) " & _
                        "ORDER BY EhDefault DESC, ordem"
		        rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText
		        While Not rsData.EOF
			     Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
			     Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
			     rsData.MoveNext
		        Wend
		    rsData.Close
		    Set rsData = Nothing
		  Else
            Set rsData = Server.CreateObject("ADODB.Recordset")
			strSQL = "SELECT ItemID AS fldID, ItemMasculino AS fldName FROM dbo.TiposAuxiliares_Itens WITH(NOLOCK) " & _
                        "WHERE (EstadoID = 2 AND TipoID = 48) " & _
                        "ORDER BY ItemID ASC"
		        rsData.Open strSQL, strConn, adOpenDynamic, adLockReadOnly, adCmdText
		        While Not rsData.EOF
			     Response.Write "<option value =" & Chr(34) & rsData.Fields("fldID").Value & Chr(34)
			     Response.Write ">" & rsData.Fields("fldName").Value & "</option>" & vbCrlf
			     rsData.MoveNext
		        Wend
		    rsData.Close
		    Set rsData = Nothing
		  End If
        %>
    </select>
    <p id="lblTermoacordo" name="lblTermoacordo" class="lblGeneral">Termo de acordo</p>
    <select id="selTermoacordo" name="selTermoacordo" class="fldGeneral"></select>
    <p id="lblRevisao" name="lblRevisao" class="lblGeneral" title="N�mero de revis�o do documento">Rev</p>
    <input type="text" id="txtRevisao" name="txtRevisao" class="fldGeneral" title="N�mero de revis�o do documento"></input>
    <p id="lbldtVigencia" name="lbldtVigencia" class="lblGeneral">Vig�ncia</p>
    <input type="text" id="txtdtVigencia" name="txtdtVigencia" class="fldGeneral"></input>
    <p id="lblTotPags" name="lblTotPags" class="lblGeneral">P�g</p>
    <input type="text" id="txtTotPags" name="txtTotPags" class="fldGeneral" title="N�mero estimado de p�ginas"></input>
    <iframe id="frameDocumento" name="frameDocumento" class="theFrames"></iframe>
    <input type="button" id="btnOK" name="btnOK" value="OK" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnVoltar" name="btnVoltar" value="Voltar" language="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" language="javascript" onclick="return btn_onclick(this)" class="btns">

    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>
</body>
</html>


