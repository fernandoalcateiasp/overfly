/********************************************************************
modaltermoacordo.js

Library javascript para o modaltermoacordo.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************



// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
FUNCOES GERAIS:

window_onload()
window_onload_finalPart()
setupPage()
loadPageFromServerInFrame_Aut(nTermoAcordoID, sAnchorID, nTipoDocumento, bNavInSameDoc)
adjustFrameAndLoad_1st_Documentos()
resetInterfaceFields()
btn_onclick(ctl)
dataToReturn()
showDocumentFrame(action)
saveCurrentPosInNavList(nTermoAcordoID, sAnchorID, nTipoDocumento, nAuto)
removeCurrentPosInNavList()
btnVoltarStatusAndHint()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Window on Load - Configura o html
********************************************************************/
function window_onload()
{
	// garante o caret nao permanecer em campo do pesqlist
	if (glb_sCaller == 'PL')
	{
		sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'window.focus()');
		sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL, 'fg.focus()');
	}	
	
	glb_sAnchorDest = glb_sAnchorID__;
	
	// initializa lista de Termos navegados
  	glb_oNavList.initializeList();
	
	if ( (glb_sAnchorID__ != null) && (glb_sAnchorID__ != '') )
		glb_oNavList.sAnchorInFirstPage = glb_sAnchorID__;

    window_onload_1stPart();

	txtRevisao.readOnly = true;
	txtdtVigencia.readOnly = true;
    txtTotPags.readOnly = true;

	btnVoltar.disabled = true;

    // trava a interface
    lockControlsInModalWin(true);
    
    window_onload_prg();

    window_onload_finalPart();
}    

/********************************************************************
Window on Load - Configura o html - parte final
********************************************************************/
function window_onload_finalPart()
{
	var elemToFocus;
    var aEmpresaData;
    var frameRect;
    
    // ajusta o body do html
    with (modaltermoacordoBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

	aEmpresaData = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL,'getCurrEmpresaData()');
	glb_nEmpresaID = aEmpresaData[0];

    // configuracao inicial do html
    setupPage();
    
    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        frameRect[1] += 40;
		moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }
    
    // ajusta o frame de termos e carrega o primeiro termo
	adjustFrameAndLoad_1st_Documentos();

    // mostra a janela modal com o arquivo carregado
	showExtFrame(window, true);
	

    // sempre dar foco, no minimo na janela e em um controle da janela
    window.focus();

    elemToFocus = btnCanc;

	if ( !elemToFocus.disabled && (elemToFocus.currentStyle.visibility != 'hidden') )
	    elemToFocus.focus();
	
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() 
{
    // texto da secao01
    secText(glb_sTitulo, 1);
    
  	mountModalAccordingToContext();
}

/********************************************************************
Carrega as paginas do servidor.

Parametros:
	nTermoID - id do termo a carregar
	sAnchorID - navegacao automatica para um achor da pagina a ser
	            executado apos o carregamento
	nTipoTermo - tipo de termo
	bNavInSameDoc - flag para executar o carregamento ou nao.
	                Eventualmente sera executada apenas uma navegacao
	                para un anchor.
********************************************************************/
function loadPageFromServerInFrame_Aut(nTermoAcordoID, sAnchorID, nselIdiomaID, bNavInSameDoc)
{
	var nAction;
	var oFrame = null;
	var hGap = ELEM_GAP;
	var vGap = ELEM_GAP;
	var strPars;
	var i;
	var bIsFirstDoc;
	var numElemInList;
	
	secText('', 1);

	if (nTermoAcordoID == null)
	    nTermoAcordoID = glb_nTermoAcordoID;
	    //nTermoAcordoID = glb_nTermoAcordoPesqID;
		
	nAction = glb_oNavList.nDirection;	
		
	if ( (parseInt(glb_oNavList.qtyElemsInList(), 10) == 0) && (nAction == 0) )
	    nselIdiomaID = glb_nIdiomaID;
	// CONFORME PEDIDO DO EDUARDO, EM 23/04 
	// O ELSE ABAIXO OBRIGA CARREGAR SEMPRE DOCUMENTOS EM ESTUDO
	// APOS O PRIMEIRO DOCUMENTO CARREGADO.
	// NA NAVEGACAO PARA TRAZ, O TIPO DO PRIMEIRO DOCUMENTO
	// TAMBEM SERA RESPEITADO
	else
	{
		// ALTERADO EM 11/09, PARA CARREGAR OBEDECENDO A SEGUINTE CONDICAO:
		// DOCUMENTO NA REV 0, CARREGA O ESTUDO
		// DOCUMENTO ACIMA DA REV 0, CARREGA O TEXTO
		// nTipoDocumento = -1;
		// ALTERADO EM 26/09, NAO MUDA MAIS O TIPO DE DOCUMENTO,
		// CONFORME LINHA ABAIXO
		// nTipoDocumento = 'NULL';
		
		// Se a navegacao e para traz e se o documento que vai ser
		// carregado e o primeiro documento que foi carregado
		// na navegacao para frente, carrega o documento com o mesmo
		// tipo usado para carregar o primeiro documento
		if ( nAction == -1 )
		{
			bIsFirstDoc = true;
			numElemInList = parseInt(glb_oNavList.qtyElemsInList(), 10);
			
			for ( i=(numElemInList - 1); i>=0; i-- )
			{
				if ( (glb_oNavList.aList[i][0] != null) && (glb_oNavList.aList[i][1] != null) )
				{
					if ( glb_oNavList.aList[i][0] != glb_nTermoAcordoID )
					{
						bIsFirstDoc = false;
						break;
					}
				}		
			}
			
			if ( bIsFirstDoc )
			    nselIdiomaID = glb_nTipoFirstDocToLoad;
		}
	}	
		
	if ( nAction == null )
		nAction = 0;
	
    // todos os iframes
    coll = document.all.tags('IFRAME');
    
    // Carrega novo documento se for o caso
	for (i=0;i<coll.length; i++)
	{
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);

			oFrame.tabIndex = -1;
			
			if (bNavInSameDoc == true)
			{
				// esta funcao destrava o html contido na janela modal
				lockControlsInModalWin(false);
				
				window.frames(0).forceAnchorNavByFrame( nAction, (sAnchorID == null ? '__0' : sAnchorID));
			}
			else
			{
				resetInterfaceFields();

				strPars = strParsToLoadPageFromServer(nTermoAcordoID, nselIdiomaID, sAnchorID, nAction);

				oFrame.src = SYS_PAGESURLROOT + '/modbasico/submodauxiliar/TermoAcordo/modalpages/termoacordotemplate.asp' + strPars;
			}		
				
			break;
	    }
	}
}

/********************************************************************
Limpa campos da interface antes de carregar novo documento
********************************************************************/
function resetInterfaceFields()
{
	txtRevisao.value = '';
	txtdtVigencia.value = '';
	txtTotPags.value = '';
}

/********************************************************************
Ajusta o frame que carrega as paginas e carrega a primeira pagina
********************************************************************/
function adjustFrameAndLoad_1st_Documentos()
{
	var oFrame = null;
	var hGap = ELEM_GAP;
	var vGap = ELEM_GAP;
	var i;
	var coll;
	
    // todos os iframes
    coll = document.all.tags('IFRAME');
    
    for (i=0;i<coll.length; i++)
    {
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);
			
			with (oFrame.style)
			{
				visibility = 'hidden';
				left = hGap;
				top = parseInt(txtdtVigencia.currentStyle.top, 10) +
				      parseInt(txtdtVigencia.currentStyle.height, 10) + vGap;
				width = parseInt(divMod01.currentStyle.width, 10) - (2 * hGap);
				height = parseInt(btnOK.currentStyle.top, 10) +
						 parseInt(btnOK.currentStyle.height, 10) -
				         parseInt(oFrame.currentStyle.top, 10) + (hGap / 2);
				
				backgroundColor = 'white';
			}
			
			//oFrame.scrolling = 'yes';

			glb_nTipoFirstDocToLoad = glb_nIdiomaID;
			
			glb_loadDocumentoTimer = window.setInterval('loadPageFromServerInFrame()', 100, 'JavaScript');	
			
			break;
        }
    }
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);
    
    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, dataToReturn() );
    }
    // 2. O usuario clicou o botao Voltar
    else if (ctl.id == btnVoltar.id )
    {
		var nDocCurrentID;
		
		var v0;
		var v1;
		var v2;
		var v3;
		var relockInterface = false;
		
		v0 = glb_oNavList.currentInList()[0];
		v1 = glb_oNavList.currentInList()[1];
		v2 = glb_oNavList.currentInList()[2];
		
		if ( (v0 == null) && (v1 == null) && (v2 == null) )
		{
			// forca navegacao para anchor de primeira pagina
			// se o anchor existe
			if ( glb_oNavList.sAnchorInFirstPage != null )
			{
				var objToScroll;
				
				objToScroll = window.frames(0).glb_overHTMLMounter._getAnchor(glb_oNavList.sAnchorInFirstPage.toString());

				if ( objToScroll )
					window.frames(0).window.scrollTo( 0, objToScroll.offsetTop );
				
				glb_oNavList.sAnchorInFirstPage = null;
			}
			else
				window.frames(0).window.scrollTo( 0, 0);			

		
			// esta funcao destrava o html contido na janela modal
			lockControlsInModalWin(false);
			
			removeCurrentPosInNavList();
			
			return;
		}	

		// verifica se a navegacao a ser feita e um anchor automatico
		// ou seja, colocado por carregamento de pagina, se for deleta		
		if ( glb_oNavList.currentInList()[3] == 1)
		{
			removeCurrentPosInNavList();
			
			v0 = glb_oNavList.currentInList()[0];
			v1 = glb_oNavList.currentInList()[1];
			v2 = glb_oNavList.currentInList()[2];
		
			if ( (v0 == null) && (v1 == null) && (v2 == null) )
			{
				window.scrollTo( 0, 0);
				
				// esta funcao destrava o html contido na janela modal
				lockControlsInModalWin(false);
				
				return;
			}	
		}
		
		nDocCurrentID = window.frames(0).glb_nTermoAcordoID;
		
		v0 = glb_oNavList.currentInList()[0];
		v1 = glb_oNavList.currentInList()[1];
		v2 = glb_oNavList.currentInList()[2];
		v3 = (nDocCurrentID == glb_oNavList.currentInList()[0]);
		
		if ( glb_oNavList.qtyElemsInList() > 0 )
		{
			if (modalWinIsLocked())
			{
				lockControlsInModalWin(false);
				relockInterface = true;
			}
					
			removeCurrentPosInNavList();
					
			if ( relockInterface )
				lockControlsInModalWin(true);	
		}	
		
		glb_oNavList.nDirection = -1;
				
		loadPageFromServerInFrame( v0, v1, v2, v3 );
	}        
    // 2. O usuario clicou o botao Fechar
    else if (ctl.id == btnCanc.id )
    {
		sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, dataToReturn() );    
	}        
}

/********************************************************************
Retorno de dados para o form ao fechar a modal
********************************************************************/
function dataToReturn()
{
	return null;
}

/********************************************************************
Mostra ou esconde o frame que carrega o documento
********************************************************************/
function showDocumentFrame(action)
{
	var coll, i;
	
	// todos os iframes
    coll = document.all.tags('IFRAME');
    
    for (i=0;i<coll.length; i++)
    {
		if ( (coll.item(i).id).toUpperCase() == 'FRAMEDOCUMENTO' )
		{
			oFrame = coll.item(i);
			
			with (oFrame.style)
			{
				if ( action )
					visibility = 'inherit';
				else	
					visibility = 'hidden';
			}
			
			break;
        }
    }
}

/********************************************************************
Grava na pilha de acoes do objeto que controla a navegacao

Parametros:
	nTermoAcordoID - ID do documento carregado
	sAnchorID - ID do anchor executado
	nTipoDocumento - Tipo do documento a ser carregado:
					< 0 -> Estudo
					null -> Atual
					> 0 -> Revisao
	nAuto - 1 ou 0 . Informa se uma navegacao para anchor foi executada
	        por automaticamente (apos carregar uma pagina)
	        ou por uma acao do usuario.
********************************************************************/
function saveCurrentPosInNavList(nTermoAcordoID, sAnchorID, nTipoDocumento, nAuto)
{
	if ((sAnchorID == null) || (sAnchorID == ''))
		sAnchorID = '__0';
		
	if ( nAuto == null )	
		nAuto = 0;
	
	if ( glb_oNavList.nDirection >= 0 )
		glb_oNavList.addAndMakeCurrentInList( new Array(nTermoAcordoID, sAnchorID, nTipoDocumento, nAuto),
		             'btnVoltarStatusAndHint()' );
}

/********************************************************************
Remove na pilha de acoes do objeto que controla a navegacao.
E necessario sempre um registro null, null, null, 0 na pilha
********************************************************************/
function removeCurrentPosInNavList()
{
	if ( glb_oNavList.qtyElemsInList() == 0 )
	{
		glb_oNavList.addAndMakeCurrentInList( new Array(null, null, null, 0) );
		return null;
	}	

	glb_oNavList.removeCurrentInList('btnVoltarStatusAndHint()');
	
	// a pilha nao pode ficar zerada
	if ( glb_oNavList.qtyElemsInList() == 0 )
		glb_oNavList.addAndMakeCurrentInList( new Array(null, null, null, 0) );
	
	return null;	
}

/********************************************************************
Habilita/Desabilita e controle o hint do botao btnVoltar
********************************************************************/
function btnVoltarStatusAndHint()
{
	var nBtnCounter = 1;

	btnVoltar.disabled = true;
	btnVoltar.title = '';

	for (i=0; i<glb_oNavList.qtyElemsInList(); i++)
	{
		if ( (glb_oNavList.aList[i][0] == null) && (glb_oNavList.aList[i][1] == null) )
		{
			btnVoltar.disabled = false;
			continue;
		}	
		
		if (glb_oNavList.aList[i][3] == 0)
		{
		    btnVoltar.disabled = false;

			nBtnCounter++;
			selIdiomaID.disabled = true;

			secText(glb_sTitulo, 1);
		}	
	}
	
	if ( nBtnCounter >= 1 )
		btnVoltar.title = (nBtnCounter).toString() + ' retorno' + (nBtnCounter > 1 ? 's' : '') + ' ' +
			'pendente' + (nBtnCounter > 1 ? 's' : '');
	
	if (btnVoltar.disabled)
	{
	    btnVoltar.title = '';
	    selIdiomaID.disabled = false;
	    secText(glb_sTitulo, 1);
	}
}
