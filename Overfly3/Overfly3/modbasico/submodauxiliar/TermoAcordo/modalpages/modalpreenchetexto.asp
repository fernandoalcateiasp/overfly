<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalpreenchetextoHtml" name="modalpreenchetextoHtml">

<head>

<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modbasico/submodauxiliar/TermoAcordo/modalpages/modalpreenchetexto.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
'Script de variaveis globais

Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
Response.Write vbcrlf
Response.Write vbcrlf

Dim i, btnOKState, dso, fieldName, gridID, colText, windowTitle

btnOKState = "D"  'desabilitado
dso = ""
fieldName = ""
gridID = ""
colText = 0
windowTitle = ""

For i = 1 To Request.QueryString("btnOKState").Count
    btnOKState = Request.QueryString("btnOKState")(i)
Next

For i = 1 To Request.QueryString("dso").Count
    dso = Request.QueryString("dso")(i)
Next

For i = 1 To Request.QueryString("fieldName").Count
    fieldName = Request.QueryString("fieldName")(i)
Next

For i = 1 To Request.QueryString("gridID").Count
    gridID = Request.QueryString("gridID")(i)
Next

For i = 1 To Request.QueryString("colText").Count
    colText = Request.QueryString("colText")(i)
Next

For i = 1 To Request.QueryString("windowTitle").Count
    windowTitle = Request.QueryString("windowTitle")(i)
Next


If (btnOKState = "D") Then
    Response.Write "var glb_btnOKState = 'D';"
Else
    Response.Write "var glb_btnOKState = 'H';"
End If

Response.Write "var glb_dso = " & Chr(39) & dso & Chr(39) & ";"

Response.Write "var glb_fieldName = " & Chr(39) & fieldName & Chr(39) & ";"

Response.Write "var glb_gridID = " & Chr(39) & gridID & Chr(39) & ";"

Response.Write "var glb_colText = " & CStr(colText) & ";"

Response.Write "var glb_windowTitle = "  & Chr(39) & windowTitle & Chr(39) & ";"

Response.Write vbcrlf
Response.Write vbcrlf

Response.Write "</script>"
Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">
<!--

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // Movimenta a modal alguns pixels mais para baixo
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        frameRect[1] += 40;
        moveFrameInHtmlTop(getExtFrameID(window), frameRect);
    }
    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    var elem = document.getElementById('modalpreenchetextoBody');

    with (elem)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    if (! txtTextoFromGrid.readOnly )
        txtTextoFromGrid.focus();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    var sTexto = '';

    // texto da secao01
    secText(glb_windowTitle, 1);

    // ajusta elementos da janela
    var elem;
    var tempWidth;
    var tempHeight;

    var modWidth = 0;
    var modHeight = 0;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect) {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }

    // ajusta o divTexto
    elem = window.document.getElementById('divTexto');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        // left = parseInt(document.getElementById('divUF').style.left) + parseInt(document.getElementById('divUF').style.width) + ELEM_GAP;
        left = ELEM_GAP;
        // top = parseInt(document.getElementById('divUF').style.top);
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
        width = modWidth - ((2 * ELEM_GAP) + 3);
        tempWidth = parseInt(width);
        height = modHeight - (parseInt(top) + ((5 * ELEM_GAP) + 5));
        tempHeight = height;
        
    }

    elem = document.getElementById('txtTextoFromGrid');
    with (elem.style) {
        left = 0;
        top = 0;
        width = tempWidth;
        height = tempHeight;
    }

    sTexto = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                               glb_gridID + '.TextMatrix(' + glb_gridID + '.Row,' +
                               glb_colText + ')');

    if (sTexto == null)
        sTexto = '';

    btnOK.disabled = (glb_btnOKState == 'D');

    txtTextoFromGrid.maxLength = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,
                              glb_dso + '.recordset(' + '\'' + glb_fieldName + '\'' + ').definedSize');
    txtTextoFromGrid.readOnly = btnOK.disabled;

    //sTexto = ReplaceText(sTexto, "'", "\'")
    
    txtTextoFromGrid.value = sTexto;
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    // ctl.id retorna o id do botao clicado (OK ou Cancelar)
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    txtTextoFromGrid.value = replaceStr(txtTextoFromGrid.value, "'", '<aspas>');
    txtTextoFromGrid.value = replaceStr(txtTextoFromGrid.value, '<aspas>', "''");

    if (ctl.id == btnOK.id) 
    {
        // 1. O usuario clicou o botao OK
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_I' , txtTextoFromGrid.value);
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_I', null );    
}
//-->
</script>

</head>

<body id="modalpreenchetextoBody" name="modalpreenchetextoBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
    
     <div id="divTexto" name="divTexto" class="divGeneral">
        <textarea id="txtTextoFromGrid" name="txtTextoFromGrid" VIEWASTEXT LANGUAGE="javascript" onfocus="return __selFieldContent(this)"></textarea>
    </div>    

    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Cancelar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        

</body>

</html>
