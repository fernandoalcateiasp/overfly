/********************************************************************
pesqlist.js

Library javascript de funcoes do pesqlist do form tiposaux
********************************************************************/
var nEmpresaID = getCurrEmpresaData();

// VARIAVEIS GLOBAIS ************************************************
// Dados da listagem da pesquisa 
var dsoListData01 = new CDatatransport("dsoListData01");
// Dados dos combos de contexto e filtros
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
// Dados dos proprietarios para o pesqlist .URL
var dsoPropsPL = new CDatatransport("dsoPropsPL");
// Dados para verificar se existe Termo de Acordo
var dsoTermoAcordo = new CDatatransport('dsoTermoAcordo');
 
// FINAL DE VARIAVEIS GLOBAIS ***************************************
var glb_idiomaPesq;
/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    pesqlistIsVisibleAndUnlocked()
    btnBarClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    specialClauseOfResearch()

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

function window_onload()
{
    windowOnLoad_1stPart();
    
    //@@ Ordem e titulos das colunas do grid de pesquisa
    glb_COLPESQORDER = new Array('ID', 'Est', 'Termo', 'Texto', 'Observa��o', 'EstadoID');

    windowOnLoad_2ndPart();
}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
- Avancar, retroagir, incluir, alterar, estado, 
excluir, OK, cancelar refresh, anterior e proximo. 
(Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) 
{
    ;
}


/********************************************************************
Funcao disparada pelo frame work.
O pesqlist esta visivel e ativo
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function pesqlistIsVisibleAndUnlocked() 
{
    showBtnsEspecControlBar('sup', false, [0, 0, 0, 0, 1]);
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Texto']);

    fg.ColHidden(fg.Cols - 1) = true;

    if ((fg.Rows > 1) && (fg.Row > 0))
        setupEspecBtnsControlBar('sup', 'DDDH');
    else
        setupEspecBtnsControlBar('sup', 'DDDD');        
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
    if (btnClicked == 4) 
    {
        openModalTermosdeAcordo();
    }
}
/*********************************************************************
Funcao do programador, mostra os termos de acordo do registro
formatados
*********************************************************************/
function openModalTermosdeAcordo() 
{
    var nTermoAcordoID = parseFloat(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TermoAcordoID')));
    
    dsoTermoAcordo.SQL = 'DECLARE @TermoAcordo BIT ' +
		               'IF(SELECT COUNT(TextoTermoAcordoID) AS Qtde ' +
				            'FROM TermoAcordo_Texto WITH(NOLOCK) ' +
				            'WHERE TermoAcordoID = ' + nTermoAcordoID + ') > 0 ' +
		               'BEGIN ' +
				            'SET @TermoAcordo = 1 ' +
		               'END ' +
		               'ELSE ' +
		               'BEGIN ' +
				            'SET @TermoAcordo = 0 ' +
		               'END ' +
		               'SELECT @TermoAcordo AS TermoAcordo';

    setConnection(dsoTermoAcordo);
    dsoTermoAcordo.ondatasetcomplete = openModalTermosdeAcordo_DSC;
    dsoTermoAcordo.refresh();
}

/*********************************************************************
Funcao do programador, mostra os termos de acordo do registro
formatados
*********************************************************************/
function openModalTermosdeAcordo_DSC() 
{
    var nIdiomaID = nEmpresaID[7];
    var nTermoAcordoID = parseFloat(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TermoAcordoID')));
    var ItemTermoAcordoID = parseFloat(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ItemId')));
    var nEstadoID = parseFloat(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Estado')));

    var nTermoAcordoVerifica = dsoTermoAcordo.recordset['TermoAcordo'].value;
        glb_idiomaPesq = nIdiomaID; 

    if (nTermoAcordoVerifica == 1) 
    {
        window.top.openModalTermoAcordo('S', '', nTermoAcordoID, nIdiomaID, 'T', ItemTermoAcordoID);
    }
    else 
    {
        if (window.top.overflyGen.Alert('N�o existe texto de termo de acordo cadastrado') == 0)
            return null;
    }
}
/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALTERMOACORDOHTML') 
    {
        if (param1 == 'OK') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Pede string complementar de pesquisa para forms que tenham este caso.

Parametro:
nenhum

Retorno:
a string ou null se nao tem
********************************************************************/
function specialClauseOfResearch()
{

    //@@ da automacao -> retorno padrao
    return null;
}