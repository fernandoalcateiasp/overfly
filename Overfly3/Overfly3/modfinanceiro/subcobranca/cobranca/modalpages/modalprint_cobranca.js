/********************************************************************
modalprint_pedidos.js

Library javascript para o modalprint.asp
Pedidos 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Dados de impressao matricial .SQL 
var dsoPrint01 = new CDatatransport("dsoPrint01");
// dso usado para envia e-mails 
var dsoEMail = new CDatatransport("dsoEMail");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    setupPage()
    loadDataAndTreatInterface()
    loadReportsInList()
    linkDivsAndReports()
    invertChkBox()
    chkBox_Clicked()
    selReports_Change()
    btnOK_Status()
    btnOK_Clicked()
    showDivByReportID()
    getCurrDivReportAttr()
    noRights()
    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
	glb_bUseProgressBar = true;
	asort(glb_aReportsTimeExecution, 0);

    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
        
    // ajusta o body do html
    with (modalprintBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
       
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();
    
    if ( selReports.disabled == false )
        selReports.focus();
    else if (btnCanc.disabled == false)
        btnCanc.focus();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
    {
		pb_StopProgressBar(true);
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
	}        
        
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01
    secText('Relat�rios', 1);

    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var vGap, hGap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;
    
    // ajusta o paragrafo relatorios
    lblReports.innerText = translateTerm('Relat�rio', null);
    with (lblReports.style)
    {
        backgroundColor = 'transparent';
        color = 'black';
        fontSize = '8pt';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP;
        height = 16;
        width = (lblReports.innerText).length * FONT_WIDTH;
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    // ajusta combo de relatorios
    with (selReports.style)
    {
        fontSize = '10pt';
        left = ELEM_GAP;
        top = vGap;
        width = widthFree - (2 * ELEM_GAP);
        height = parseInt(selReports.currentStyle.height, 10);
        vGap = parseInt(top, 10) + parseInt(height, 10);
    }

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();
    
    // O estado do botao btnOK
    btnOK_Status();
    
    // O usuario tem direito ou nao
    noRights();
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // Por default selReports botao OK vem travados
    selReports.disabled = true;
    btnOK.disabled = true;
    
    selReports.onchange = selReports_Change;
    
    // carrega a lista de relatorios
    loadReportsInList();
    
    // linka os divs nos relatorios
    linkDivsAndReports();
    
    // restringe digitacao em campos numericos
    setMaskAndLengthToInputs();

    // mostra o div correspondente ao relatorio
    showDivByReportID();
}

/********************************************************************
Carrega a lista de relatorios
********************************************************************/
function loadReportsInList()
{
    var i;
    
    if ( glb_arrayReports != null )
    {
        for ( i=0; i<glb_arrayReports.length; i++)    
        {
            var oOption = document.createElement("OPTION");
            oOption.text = glb_arrayReports[i][0];
            oOption.value = glb_arrayReports[i][1];
            selReports.add(oOption);
            
            if ( glb_arrayReports[i][2] )
                selReports.selectedIndex = i;
        }
        
        selReports.disabled = false;
    }
}

/********************************************************************
Linka os divs nos relatorios
********************************************************************/
function linkDivsAndReports()
{
}

/********************************************************************
Inverte o check de um checkbox associado a um label
Funcao copiada da lib js_interfacex.js, nao linkada neste form
********************************************************************/
function invertChkBox()
{
    var ctl = this.nextSibling;
    
    if ( ctl.disabled == true )
        return;

    ctl.checked = !ctl.checked;
    
    // O estado do botao btnOK
    btnOK_Status();
    
    window.focus();
    if ( ctl.disabled == false )
        ctl.focus();
    
    return true;
}

/********************************************************************
Usuario trocou o relatorio selecionado no combo de relatorios
********************************************************************/
function selReports_Change()
{
    // mostra o div correspondente ao relatorio
    showDivByReportID();
    
    // status do botao btnOK;
    btnOK_Status();

    window.focus();
    if ( this.disabled == false )
        this.focus();
    
    return true;
}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver um relatorio selecionado e
pelo menos um checkbox checado. Alem disto
o div visivel deve ser coerente com o relatorio selecionado
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;
    var i, elem;
    
    if ( selReports.selectedIndex != -1 )
    {
        // Remessa
        if (selReports.value == 40201)
            btnOKStatus = false;
        // Retorno
        else if (selReports.value == 40202)
            btnOKStatus = false;
    }
    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    if ( (selReports.value == 40201) && (glb_sCaller == 'S') )
        relatorioRemessa();
    else if ( (selReports.value == 40202) && (glb_sCaller == 'S') )
        relatorioRetorno();
    else        
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller , null);
}

/********************************************************************
Mostra o div correspondente ao relatorio selecionado
********************************************************************/
function showDivByReportID()
{
    var i, coll, attr, currRep;
    
    attr = null;
    currRep = 0;
    
    currRep = selReports.value;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( attr == currRep )
                coll[i].style.visibility = 'visible';
            else
                coll[i].style.visibility = 'hidden';    
        }
    }
}

/********************************************************************
Retorna o id de relatorio associado ao correspondente
div de relatorio visivel
********************************************************************/
function getCurrDivReportAttr()
{
    var i, coll, attr, retVal;
    
    attr = null;
    retVal = 0;
    
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
        {
            if ( coll[i].style.visibility == 'visible' )
            {
                retVal = attr;
                break;
            }    
        }
    }
    return retVal;    
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs()
{
    
}

/********************************************************************
Usuario nao tem direito a nenhum relatorio
********************************************************************/
function noRights()
{
    if ( selReports.options.length != 0 )
        return null;

    var frameRect = new Array();
    var i, j, elem, elem1, modWidth, modHeight;
    var topFree;
    var selReportsFloor;
    var coll, attr;
    
    // esconde todos os divs
    coll = window.document.getElementsByTagName('DIV');
    
    for ( i=0; i<coll.length; i++ )
    {
        attr = coll[i].getAttribute('report', 1);
        
        // nao faz nada se nao e div de relatorio
        if ( attr == null )
            continue;
        else
            coll[i].style.visibility = 'hidden';    
    }
    
    // desabilita o combo de relatorios
    selReports.disabled = true;
    
    // desabilita o botao OK
    btnOK.disabled = true;
        
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
        modWidth = frameRect[2];
    
    // descontar a altura dos botoes OK e Cancela
    modHeight = parseInt(btnOK.style.top, 10);
       
    elem = document.getElementById('selReports');
    
    // inicio da altura livre (desconta o combo de relatorios)
    topFree = parseInt(elem.currentStyle.top, 10) + parseInt(elem.currentStyle.height, 10);
        
    // a altura livre    
    modHeight -= topFree;
        
    elem = document.createElement('P');
    elem.name = '__lbl';
    elem.className = 'paraNormal';
    elem.innerText = 'Relat�rios n�o liberados';
    elem.style.height = '10pt';
    elem.style.width = (elem.innerText).length * FONT_WIDTH;
    elem.style.color = 'black';
    elem.style.backgroundColor = 'transparent';
    elem.style.fontSize = '10pt'; 
    elem.style.textAlign = 'center';
    elem.style.cursor = 'default';
    
    // acrescenta o elemento
    window.document.body.appendChild(elem);
        
    elem.style.top = topFree + (modHeight / 2) - (parseInt(elem.style.height) / 2);
        
    elem.style.left = modWidth / 2 - parseInt(elem.style.width) / 2;
        
    return null;
}

function relatorioRemessa()
{

    var strParameters = "RelatorioID=" + selReports.value + "&glb_nCobrancaID=" + glb_nCobrancaID + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modfinanceiro/subcobranca/cobranca/serverside/Reports_cobranca.aspx?' + strParameters;
}

function relatorioRetorno()
{
    var strParameters = "RelatorioID=" + selReports.value + "&glb_nCobrancaID=" + glb_nCobrancaID + "&glb_sEmpresaFantasia=" + glb_sEmpresaFantasia;

    pb_StartProgressBar(glb_aReportsTimeExecution[aseek(glb_aReportsTimeExecution, selReports.value, 0)][1]);
    lockControlsInModalWin(true);
    window.document.onreadystatechange = reports_onreadystatechange;
    window.document.location = SYS_PAGESURLROOT + '/modfinanceiro/subcobranca/cobranca/serverside/Reports_cobranca.aspx?' + strParameters;
}
