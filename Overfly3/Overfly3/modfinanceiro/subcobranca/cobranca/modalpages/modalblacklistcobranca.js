/*****************************************************************************************************
modalblacklistcobranca.js
modalblacklistcobranca.asp
/*****************************************************************************************************

/*****************************************************************************************************
Declara��o de vari�veis globais.
******************************************************************************************************/
var dsoSearch = new CDatatransport('dsoSearch');
var dsoBlackListVerify = new CDatatransport('dsoBlackListVerify');
var dsoBlackList = new CDatatransport('dsoBlackList');
var dsoBlackListBancos = new CDatatransport('dsoBlackListBancos');
var dsoOperation = new CDatatransport('dsoOperation');
var aIdsOfClients = new Array();
var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024 * 1;

/*****************************************************************************************************
Faz a configura��o inicial do html.
******************************************************************************************************/
function window_onload() {
    window_onload_1stPart();

    setupPage();

    var elem = document.getElementById('modalblacklistcobrancaBody');
    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    showExtFrame(window, true);
    txtPessoa.focus();
    selClientes.disabled = btnIncluir.disabled = btnExcluir.disabled = btnAlterar.disabled = btnGravar.disabled = true;
}

/*****************************************************************************************************
Configura��o dos elementos da p�gina.
******************************************************************************************************/
function setupPage() {

    secText('Black List Cobran�a', 1);
    var elem;

    // Ajuste da div de pessoa.
    elem = window.document.getElementById('divPessoa');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10) + ELEM_GAP;
        width = (40 * FONT_WIDTH) + 24 + 20 + 42;
        height = 45;
    }

    //  Ajuste dos elementos.
    adjustElementsInForm([['lblPessoa', 'txtPessoa', 20, 1, -10, -10],
    ['lblClientes', 'selClientes', 20, 1, 90, 1]],
        null, null, true);

    //Ajuste dos bot�es.
    var buttonsArray = [btnPesquisar, btnIncluir, btnExcluir, btnAlterar, btnGravar];
    var buttonOfArray;
    var topButtonBase = parseInt(txtPessoa.currentStyle.top, 9);
    var leftButtonBase = parseInt(txtPessoa.currentStyle.left, 10) + parseInt(txtPessoa.currentStyle.width, 10) + ELEM_GAP;
    var widthButtonBase = (FONT_WIDTH * 10);

    for (var i = 0; i < buttonsArray.length; i++) {

        buttonOfArray = buttonsArray[i];

        with (buttonOfArray.style) {
            top = topButtonBase;
            left = leftButtonBase;
            width = widthButtonBase;
        }

        leftButtonBase = leftButtonBase + (buttonOfArray.id == 'btnPesquisar' ? 260 : 88);
    }

    // Ajuste da div do grid.
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divPessoa').style.top) + parseInt(document.getElementById('divPessoa').style.height) + ELEM_GAP;
        width = 775;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 50;
    }

    //Ajuste do grid.
    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }


    // Ajuste da div do grid Bancos.
    elem = window.document.getElementById('divBancos');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divPessoa').style.top) + parseInt(document.getElementById('divPessoa').style.height) + ELEM_GAP + 200;
        width = 775;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 20;
    }

    //Ajuste do grid Bancos.
    elem = document.getElementById('fgBancos');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divBancos').style.width);
        height = parseInt(document.getElementById('divBancos').style.height);
    }

    //Configura��o dos grid.
    startGridInterface(fg);
    startGridInterface(fgBancos);

    headerGrid(fg, ['ID',
        'Nome',
        'Est',
        'Inclusao'], []);

    headerGrid(fgBancos, ['Codigo',
        'Banco',
        'Bloq'], []);

    fg.Redraw = fgBancos.Redraw = 2;
    txtPessoa.onkeypress = txtPessoa_onKeyPress;
}

/*****************************************************************************************************
Pesquisa tamb�m ao utilizar a tecla "Enter".
******************************************************************************************************/
function txtPessoa_onKeyPress() {

    if (event.keyCode == 13) {
        lockControlsInModalWin(true);
        makeSearch(txtPessoa.value);
    }
}

/*****************************************************************************************************
Pesquisa dos bancos do cliente que for selecionado no grid de clientes.
******************************************************************************************************/
function fg_EnterCell_Prg() {

    // Se possuir linhas no grid ativa controles referentes e preenche grid de bancos.
    if (fg.Rows > 1) {
        btnExcluir.disabled = btnAlterar.disabled = false;
        fillModalGridBlackListBancos(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ID')));
    } else {
        btnExcluir.disabled = btnAlterar.disabled = true;
        fgBancos.Rows = 1;
        fgBancos.AutoSizeMode = 0;
        fgBancos.AutoSize(0, fgBancos.Cols - 1);
    }
}

/*****************************************************************************************************
Chamadas de fun��es nos eventos de click dos bot�es.
******************************************************************************************************/
function btn_onclick(ctl) {

    window.focus();
    if (ctl.id == btnOK.id)
        btnOK.focus();
    else if (ctl.id == btnCanc.id)
        btnCanc.focus();

    lockControlsInModalWin(true);

    // Bot�o pesquisar.
    if (ctl.id == btnPesquisar.id)
        makeSearch(txtPessoa.value);
    // Bot�o Cancelar.
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
    // Bot�o Incluir.
    else if (ctl.id == btnIncluir.id) {
        fillModalGridBlackListVerify(selClientes.value);
        // Bot�o Excluir.
    } else if (ctl.id == btnExcluir.id) {
        sendDataToServer(fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ID')), 2);
        // Bot�o Alterar.
    } else if (ctl.id == btnAlterar.id) {
        changeMode();
        // Bot�o Gravar.
    } else if ((ctl.id == btnGravar.id) || (ctl.id == btnOK.id)) {
        saveDataInGrid();
    }
}

/*****************************************************************************************************
Pesquisa utilizando o argumento como par�metro.
******************************************************************************************************/
function makeSearch(searchKey) {

    setConnection(dsoSearch);
    var where;

    // Se o argumento for um n�mero faz a pesquisa utlizando ID, sen�o � utilizado Fantasia ou Nome.
    if (isNaN(searchKey)) {
        where = 'AND ((a.Nome LIKE \'' + searchKey + '%\')' + ' OR (a.Fantasia LIKE \'' + searchKey + '%\')) ';
    } else {
        where = 'AND (a.PessoaID LIKE \'' + searchKey + '%\') ';
    }

    // Clientes ativos que n�o sejam empresas da Alcateia nem colaboradores.
    var sql = 'SELECT DISTINCT TOP 20 a.PessoaID AS fldID, a.Fantasia AS fldName ' +
        'FROM Pessoas a WITH(NOLOCK) ' +
        'INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.SujeitoID = a.PessoaID) ' +
        'WHERE (a.EstadoID = 2) ' +
        'AND (a.ClassificacaoID NOT IN (56, 57)) ' +
        'AND (b.EstadoID = 2) ' +
        'AND (b.TipoRelacaoID = 21) ' + where;

    dsoSearch.SQL = sql;
    dsoSearch.ondatasetcomplete = makeSearch_DSC;
    dsoSearch.refresh();

}

/*****************************************************************************************************
Preenchimento do combo com o retorno da pesquisa.
******************************************************************************************************/
function makeSearch_DSC() {

    clearComboEx(['selClientes']);

    while (!dsoSearch.recordset.EOF) {
        var oOption = document.createElement("OPTION");
        oOption.text = '(' + dsoSearch.recordset['fldID'].value + ') ' + dsoSearch.recordset['fldName'].value;
        oOption.value = dsoSearch.recordset['fldID'].value;
        selClientes.add(oOption);
        dsoSearch.recordset.MoveNext();
    }

    lockControlsInModalWin(false);

    if (selClientes.length >= 1) {
        selClientes.disabled = btnIncluir.disabled = false;
        selClientes.focus();
    }
    else {
        selClientes.disabled = btnIncluir.disabled = true;
        txtPessoa.focus();
    }

}

/*****************************************************************************************************
Defini��o do DSO de verifica��o de exist�ncia do cliente.
******************************************************************************************************/
function fillModalGridBlackListVerify(clientID) {

    setConnection(dsoBlackListVerify);

    // Clientes que estejam na BlackList de Cobran�a.
    var sql = 'SELECT a.dtBlackList AS Inclusao, b.PessoaID AS ID, b.Fantasia AS Nome, c.RecursoAbreviado AS Est ' +
        'FROM CobrancaBlackList a WITH(NOLOCK) ' +
        'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.PessoaID) ' +
        'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = b.EstadoID)' +
        'WHERE a.PessoaID = ' + clientID;

    dsoBlackListVerify.SQL = sql;
    dsoBlackListVerify.ondatasetcomplete = fillModalGridBlackListVerify_DSC;
    dsoBlackListVerify.Refresh();

}

/*****************************************************************************************************
Verifica��o da exist�ncia do cliente.
******************************************************************************************************/
function fillModalGridBlackListVerify_DSC() {

    // Se esse cliente n�o estiver na BlackList retorna solicita��o para inclu�-lo.
    if (dsoBlackListVerify.recordset.aRecords.length == 0) {

        sendDataToServer(selClientes.value, 1);

    } else {

        // Se o cliente estiver na BlackList adiciona no array e preenche modal com os seus respectivos dados.
        manipulateIDs(selClientes.value, 'add');
        fillModalGridBlackList();
    }
}

/*****************************************************************************************************
Defini��o do DSO de BlackList.
******************************************************************************************************/
function fillModalGridBlackList() {

    setConnection(dsoBlackList);

    var sIdsOfClients;

    // Loop que percorre todos os IDs dos arrays de clientes.
    if (aIdsOfClients.length == 0) {
        sIdsOfClients = 0;
    } else {
        for (var i = 0; i < aIdsOfClients.length; i++) {
            if (i == 0) {
                sIdsOfClients = aIdsOfClients[i];
            } else {
                sIdsOfClients += ',' + aIdsOfClients[i];
            }
        }
    }

    // Seleciona os dados dos clientes selecionados que j� est�o na BlackList por ordem alfab�tica.
    var sql = 'SELECT a.dtBlackList AS Inclusao, b.PessoaID AS ID, b.Fantasia AS Nome, c.RecursoAbreviado AS Est ' +
        'FROM CobrancaBlackList a WITH(NOLOCK) ' +
        'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.PessoaID) ' +
        'INNER JOIN Recursos c WITH(NOLOCK) ON (c.RecursoID = b.EstadoID)' +
        'WHERE a.PessoaID IN (' + sIdsOfClients + ') ' +
        'ORDER BY b.Fantasia';

    dsoBlackList.SQL = sql;
    dsoBlackList.ondatasetcomplete = fillModalGridBlackList_DSC;
    dsoBlackList.Refresh();


}

/*****************************************************************************************************
Preenchimento do grid de BlackList.
******************************************************************************************************/
function fillModalGridBlackList_DSC() {

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    headerGrid(fg, ['ID',
        'Nome',
        'Est',
        'Inclusao'], []);

    fillGridMask(fg, dsoBlackList, ['ID',
        'Nome',
        'Est',
        'Inclusao'],
        ['', '', '', ''],
        ['', '', '', dTFormat + ' hh:mm:ss']);

    alignColsInGrid(fg, []);
    fg.Editable = false;
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    lockControlsInModalWin(false);

    // Ao preencher o grid de BlackList lista tamb�m o de bancos respectivos do cliente.
    fg_EnterCell_Prg();

}

/*****************************************************************************************************
A��es no banco de dados.
clientID - ID do cliente que ser� feito a a��o.
operation - Opera��o que ser� feita. Podendo ser tr�s: 
"1" � feito a inclus�o do ID na BlackList.
"2" � feito a exclus�o.
"" Quando n�o for passado nenhum par�metro para essa fun��o e o array de bancos n�o estiver vazio
� feito a altera��o dos bloqueios dos bancos para o cliente selecionado.
******************************************************************************************************/
function sendDataToServer(clientID, operation) {

    var strPars = new String();
    setConnection(dsoOperation);

    if ((operation == 1) || (operation == 2)) {

        strPars += '?nTipoOperacao=' + operation;
        strPars += '&nPessoaID=' + clientID;
        strPars += '&nUsuarioID=' + glb_USERID;

        // Opera��o 1. Inclus�o - Pergunta se realmente deseja inserir o cliente na BlackList.
        if (operation == 1) {

            var _returnMessage = window.top.overflyGen.Confirm('Cliente ID ' + clientID + ' n�o est� na BlackList. Deseja inclu�-lo?');

            if (_returnMessage != 1) {
                lockControlsInModalWin(false);
                return;
            }

            // Inclus�o do ID do cliente no array de clientes.
            manipulateIDs(selClientes.value, 'add');
        }

        // Opera��o 2. Exclus�o - Pergunta se realmente deseja excluir o cliente da BlackList.
        else if (operation == 2) {

            var _returnMessage = window.top.overflyGen.Confirm('Tem certeza que deseja excluir o Cliente ID ' + clientID + ' da BlackList?');

            if (_returnMessage != 1) {
                lockControlsInModalWin(false);
                return;
            }

            // Exclus�o do ID do cliente no array de clientes.
            manipulateIDs(clientID, 'remove');
        }

        dsoOperation.URL = SYS_ASPURLROOT + '/modfinanceiro/subcobranca/cobranca/serverside/blacklistcobranca.aspx' + strPars;
        dsoOperation.ondatasetcomplete = fillModalGridBlackList;
        dsoOperation.refresh();

    // Se n�o for passado nenhum par�metro para essa fun��o utiliza o array de altera��o de bloqueios de bancos.
    } else if (glb_aSendDataToServer.length > 0) {

        if (glb_nTimerSendDataToServer != null) {
            window.clearInterval(glb_nTimerSendDataToServer);
            glb_nTimerSendDataToServer = null;
        }

        try {
            if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
                dsoOperation.URL = SYS_ASPURLROOT + '/modfinanceiro/subcobranca/cobranca/serverside/blacklistcobranca.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer];
                dsoOperation.ondatasetcomplete = sendDataToServer_DSC;
                dsoOperation.refresh();
            }
            else {
                lockControlsInModalWin(false);
            }
        }
        catch (e) {
            if (window.top.overflyGen.Alert('Erro. Tente novamente.') == 0)
                return null;
            lockControlsInModalWin(false);
        }
    } else {
        return null;
        lockControlsInModalWin(false);
    }

}

/*****************************************************************************************************
Defini��o do DSO de bancos.
******************************************************************************************************/
function fillModalGridBlackListBancos(clientID) {

    setConnection(dsoBlackListBancos);

    // Todos os bancos que foram inseridos na tabela do cliente selecionado.
    var sql = 'SELECT b.CobBancoID, c.Codigo, c.Banco, b.Bloqueado AS Bloq ' +
        'FROM CobrancaBlackList a WITH(NOLOCK) ' +
        'INNER JOIN CobrancaBlackList_Bancos b WITH(NOLOCK) ON(b.CobrancaBlackListID = a.CobrancaBlackListID) ' +
        'INNER JOIN Bancos c WITH(NOLOCK) ON(c.BancoID = b.BancoID) ' +
        'WHERE(a.PessoaID = ' + clientID + ')';


    dsoBlackListBancos.SQL = sql;
    dsoBlackListBancos.ondatasetcomplete = fillModalGridBlackListBancos_DSC;
    dsoBlackListBancos.Refresh();

}

/*****************************************************************************************************
Preenchimento do grid de bancos.
******************************************************************************************************/
function fillModalGridBlackListBancos_DSC() {

    headerGrid(fgBancos, ['Codigo',
        'Banco',
        'Bloq',
        'CobBancoID'], [3]);


    fillGridMask(fgBancos, dsoBlackListBancos, ['Codigo*',
        'Banco*',
        'Bloq',
        'CobBancoID'],
        ['', '', '', ''],
        ['', '', '', '']);

    // Pinta de cinza.
    for (var i = 0; i < fgBancos.Cols; i++) {
        for (var j = 1; j < fgBancos.Rows; j++) {
            fgBancos.Cell(6, j, i, j, i) = 0XDCDCDC;
        }
    }


    lockControlsInModalWin(false);
    fgBancos.Editable = false;
    fgBancos.AutoSizeMode = 0;
    fgBancos.AutoSize(0, fgBancos.Cols - 1);

}

/*****************************************************************************************************
Manipula��o do array.
******************************************************************************************************/
function manipulateIDs(value, method) {
    // Adiciona
    if (method == 'add') {
        // Percorre o array inteiro antes de incluir.
        for (var i = 0; i < aIdsOfClients.length; i++) {
            // Se o valor j� estiver no array retorna.
            if (aIdsOfClients[i] == value) {
                return;
            }
        }
        // Adiciona valor no array.
        aIdsOfClients.push(value);

        // Exclui.
    } else if (method == 'remove') {
        for (var i = 0; i < aIdsOfClients.length; i++) {
            if (aIdsOfClients[i] == value) {
                // Exclui valor do array.
                aIdsOfClients.splice(i, 1);
            }
        }
    }
}

/*****************************************************************************************************
Entra/sai do do modo de edi��o.
******************************************************************************************************/
function changeMode() {

    // Entra em modo de edi��o.
    if (btnAlterar.value == 'Alterar') {
        if (fg.Rows > 1) {

            //Pinta todos as c�lulas do grid de BlackList de cinza.
            for (var i = 0; i < fg.Cols; i++) {
                for (var j = 1; j < fg.Rows; j++) {
                    fg.Cell(6, j, i, j, i) = 0XDCDCDC;
                }
            }

            //Pinta as c�lulas das colunas de Bloqueado no grid de Bancos de branco.
            for (var i = 1; i < fgBancos.Rows; i++) {
                fgBancos.Cell(6, i, getColIndexByColKey(fgBancos, 'Bloq'), i, getColIndexByColKey(fgBancos, 'Bloq')) = 0xFFFFFF;
            }

            lockControlsInModalWin(false);
            // Deixa o grid de BlackList desbilitado.
            fg.Enabled = false;
            // Deixa o grid de bancos edit�vel.
            fgBancos.Editable = 1;
            // Altera controles.
            txtPessoa.disabled = selClientes.disabled = btnIncluir.disabled = btnExcluir.disabled = btnPesquisar.disabled = true;
            btnGravar.disabled = btnOK.disabled = false;
            btnAlterar.value = 'Voltar';

        }

        // Sai do modo de edi��o.
    } else {

        // Altera controles.
        lockControlsInModalWin(false);
        txtPessoa.disabled = selClientes.disabled = btnIncluir.disabled = btnExcluir.disabled = btnPesquisar.disabled = false;
        btnGravar.disabled = btnOK.disabled = true;
        btnAlterar.value = 'Alterar';
        fillModalGridBlackList();
    }
}

/*****************************************************************************************************
Salva os dados do grid e envia para o servidor.
******************************************************************************************************/
function saveDataInGrid() {

    var strPars = new String();
    var i = 0;
    var nDataLen = 0;
    var nBytesAcum = 0;

    lockControlsInModalWin(true);
    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    // Percorre todas as linhas do grid de bancos.
    for (i = 1; i < fgBancos.Rows; i++) {

        nBytesAcum = strPars.length;

        if ((nBytesAcum >= glb_nMaxStringSize) || (strPars == '')) {
            nBytesAcum = 0;
            if (strPars != '') {
                glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                strPars = '';
                nDataLen = 0;
            }
        }

        nDataLen++;

        strPars += (strPars == '' ? '?' : '&') + 'nCobBancoID=' + getCellValueByColKey(fgBancos, 'CobBancoID', i);
        strPars += '&sBloq=' + (getCellValueByColKey(fgBancos, 'Bloq', i) == 0 ? 0 : 1);
    }

    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
    else {
        return null;
    }

    sendDataToServer();

}

/*****************************************************************************************************
Retorno do envio dos dados do grid para o servidor.
******************************************************************************************************/
function sendDataToServer_DSC() {
    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer();', INTERVAL_BATCH_SAVE, 'JavaScript');

    if (!(dsoOperation.recordset.BOF && dsoOperation.recordset.EOF)) {
        dsoOperation.recordset.MoveFirst();
        if (dsoOperation.recordset['Resultado'].value = 0) {
            if (window.top.overflyGen.Alert(dsoOperation.recordset['Resultado'].value) == 0)
                return null;
        }
        else {
            window.top.overflyGen.Alert('Altera��es feitas.');
            changeMode();
        }

    }

}