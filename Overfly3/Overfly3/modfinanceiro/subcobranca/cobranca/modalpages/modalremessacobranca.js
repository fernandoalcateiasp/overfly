/********************************************************************
modalbancos.js

Library javascript para o modalbancos.asp
Pessoas 
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// Dso generico para banco de dados
var dsoGen01 = new CDatatransport("dsoGen01");

var glb_PassedOneInDblClick = false;
var glb_EnableChkOK = true;

var frameRemessa;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************
RELACAO DAS FUNCOES:
    
EVENTOS DOS OBJETOS DE IMPRESSAO:

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
// imagens das lupas
var glb_LUPA_IMAGES = new Array();
glb_LUPA_IMAGES[0] = new Image();
glb_LUPA_IMAGES[1] = new Image();

// carrega as imagens de lupa
glb_LUPA_IMAGES[0].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find.gif';
glb_LUPA_IMAGES[1].src = SYS_PAGESURLROOT + '/images/btnsforms1/btn_find_dis.gif';
// %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();   
       
    // final de carregamento e visibilidade de janela
    // foi movido daqui

    // Inicia o carregamento do grid
    fillModalGridRemessa(false);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
    window.focus();
    if (ctl.id == btnOK.id )
        btnOK.focus();
    else if (ctl.id == btnCanc.id )
        btnCanc.focus();
    
    // esta funcao trava o html contido na janela modal
    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id )
    {
        // 1. O usuario clicou o botao OK
        btnOK_Clicked();
    }
    // 2. O usuario clicou o botao Cancela
    else if (ctl.id == btnCanc.id )
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );    
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // texto da secao01    
    secText('Remessa de Cobran�a', 1);

    // carrega dados na interface e configura travamento de seus elementos
    loadDataAndTreatInterface();

    // O estado do botao btnOK
    btnOK_Status();

    chkEmpresa.onclick = chkEmpresa_onclick;
}

/********************************************************************
Carrega dados nos elementos da interface e trava/destrava controles
********************************************************************/
function loadDataAndTreatInterface()
{
    // ajusta elementos da janela
    var frameRect, modWidth, modHeight;
    var topFree, widthFree, heightFree; 
    var frameBorder = 6;
    var x_gap, y_gap;

    // largura e altura da janela modal
    frameRect = getRectFrameInHtmlTop(getExtFrameID(window));
    if (frameRect)
    {
        modWidth = frameRect[2];
        modHeight = frameRect[3];
    }
    
    // inicio da altura livre
    topFree = parseInt(divMod01.currentStyle.top, 10) + parseInt(divMod01.currentStyle.height, 10);
    
    // altura livre da janela modal (descontando barra azul e botoes OK/Cancel)
    heightFree = parseInt(btnOK.currentStyle.top, 10) - topFree;    
    
    // largura livre
    widthFree = modWidth - frameBorder;

    elem = window.document.getElementById('divEmpresa');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP - 8;
        top = topFree + 10;
        width = widthFree - 2 * ELEM_GAP;
        height = heightFree - 5 * ELEM_GAP;
    }

    adjustElementsInForm([['lblEmpresa', 'chkEmpresa', 4.2, 1, 0, -17]], null, null, true);

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = topFree + ELEM_GAP + 25;
        width = widthFree - 2 * ELEM_GAP;    
        height = heightFree - 5 * ELEM_GAP;
    }
    
    elem = document.getElementById('fg');
    with (elem.style)
    {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = parseInt(document.getElementById('divFG').currentStyle.height);
    }

    // ajusta o paragrafo e o combo de arquivos
    elem = lblArquivos;
    with (elem.style)
    {
        visibility = 'hidden';
        fontSize = '8pt';
        textAlign = 'left';
        width = FONT_WIDTH * (elem.innerText).length;
        height = 16;
        left = ELEM_GAP;
        top = parseInt(divFG.currentStyle.top, 10);
        x_gap = parseInt(left, 10);
        y_gap = parseInt(top, 10) + parseInt(height, 10);
    }
    
    elem = selArquivos;
    with (elem.style)
    {
        visibility = 'hidden';
        fontSize = '10pt';
        width = parseInt(document.getElementById('divFG').currentStyle.width);
        height = 24;
        left = x_gap;
        top = y_gap;
    }

    // ajusta a mensagem de falta de elementos
    elem = lblMessage;
    // remessa
    elem.innerText = 'Nenhum t�tulo dispon�vel para cobran�a.';
        
    with (elem.style)
    {
        visibility = 'hidden';
        fontSize = '10pt';
        textAlign = 'center';
        width = FONT_WIDTH * (elem.innerText).length;
        height = 24;
        left = (widthFree - parseInt(width, 10)) / 2;
        //top = parseInt(divFG.currentStyle.top, 10) + (parseInt(divFG.currentStyle.height, 10) - parseInt(height, 10)) / 2;
        top = topFree + ((heightFree - parseInt(height, 10)) / 2);
    }

    startGridInterface(fg);
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    
    // Por default o botao OK vem travado
    btnOK.disabled = true;
        
    // restringe digitacao em campos numericos
    // e da outras providencias
    setMaskAndLengthToInputs();

}

/********************************************************************
Status do botao OK - habilitado ou desabilitado

Habilita se tiver documento digitado
********************************************************************/
function btnOK_Status()
{
    var btnOKStatus = true;

    if ( fg.Rows > 1 )
        btnOKStatus = false;

    btnOK.disabled = btnOKStatus;
}

/********************************************************************
Usuario clicou o botao OK
********************************************************************/
function btnOK_Clicked()
{
    var chkEmpresa = document.getElementById('chkEmpresa').checked;
    //Quantidade de linhas no grid
    var aLength = dsoGen01.recordset.aRecords.length;
    //Variavel para saber quantos checkbox's est�o checados
    var checked = 0;
    var strParams = '';

    //Cria uma string com dados para procedure
    for (var i = 2; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0) {
            strParams = strParams + dsoGen01.recordset.aRecords[i-2][1] + ',';
            checked++;
        }
    }
    // Remove a ultima virgula
    strParams = strParams.substring(0, (strParams.length - 1));

    if (!checked) {
        window.top.overflyGen.Alert('Por favor, selecione pelo menos um campo.');
        lockControlsInModalWin(false);
        return;
    }

    //paramtros para enviar ao aspx
    var strParameters = 'nData=' + strParams + '&aLength=' + aLength + '&chkEmpresa=' + (chkEmpresa == 0 ? false : true) + '&iUser=' + glb_USERID + '&nCobrancaID=0';

    //envia requisi��o aspx
    frameRemessa = document.getElementById("frmRemessa");
    frameRemessa.contentWindow.document.onreadystatechange = remessa_onreadystatechange;
    frameRemessa.contentWindow.location = SYS_PAGESURLROOT + '/modfinanceiro/subcobranca/cobranca/serverside/RemessaCobranca.aspx?' + strParameters;
}

function remessa_onreadystatechange() {
    var chkEmpresa = document.getElementById('chkEmpresa').checked;

    if ((frameRemessa.contentWindow.document.readyState == 'loaded') ||
        (frameRemessa.contentWindow.document.readyState == 'interactive') ||
        (frameRemessa.contentWindow.document.readyState == 'complete')) {

        fillModalGridRemessa(chkEmpresa);
        frameRemessa.contentWindow.document.onreadystatechange = null;
        frameRemessa.contentWindow.location = null;
    }
}

/********************************************************************
Restringe quantidade de caracteres digitados, coloca
mascara para digitacao em campos numericos,
seleciona conteudo do campo quando em foco etc
********************************************************************/
function setMaskAndLengthToInputs()
{
    ;    
}

/********************************************************************
Preenche o grid para a Remessa
********************************************************************/
function fillModalGridRemessa(chkEmpresa)
{
    selArquivos.style.visibility = 'hidden';
    lblArquivos.style.visibility = 'hidden';

    setConnection(dsoGen01);

    if (chkEmpresa) {
        dsoGen01.SQL = 'SELECT ' +
                            'CONVERT(bit,0) AS OK, b.RelPesContaID, a.EmpresaID, g.Fantasia, e.BancoID AS BancoID, e.Codigo + \'-\' + d.Fantasia AS Codigo, STR(COUNT(e.Codigo),5,0) AS QuantidadeTitulos, ' +
                            'CONVERT(NUMERIC (11,2), STR(SUM(a.Valor),11,2)) AS TotalTitulos, f.Layout, ' +
                            'CASE WHEN (SELECT COUNT(1) FROM CobrancaRemessaProcessa aa WHERE b.RelPesContaID = aa.RelPesContaID AND aa.dtProcessamento IS NULL) >= 1 THEN \'Aguardar processamento\' ELSE \'\' END AS Status ' +
                        'FROM ' +
                            'Financeiro a WITH(NOLOCK) ' +
                                'INNER JOIN RelacoesPessoas_Contas b WITH(NOLOCK) ON (a.RelPesContaID = b.RelPesContaID) ' +
                                'INNER JOIN RelacoesPessoas c WITH(NOLOCK) ON (b.RelacaoID = c.RelacaoID) ' +
                                'INNER JOIN Pessoas d WITH(NOLOCK) ON (c.ObjetoID = d.PessoaID) ' +
                                'INNER JOIN Bancos e WITH(NOLOCK) ON (d.BancoID = e.BancoID) ' +
                                'INNER JOIN RelacoesPesRec_Cobranca f WITH(NOLOCK) ON (a.RelPesContaID = f.RelPesContaID) ' +
                                'INNER JOIN Pessoas g WITH(NOLOCK) ON (a.EmpresaID = g.PessoaID) ' +
                        'WHERE ' +
                            'a.EstadoID >= 41 ' +
                            'AND a.EstadoID < 48 ' +
                            'AND a.TipoFinanceiroID=1002 ' +
                            'AND a.ComandoID IS NOT NULL ' +
                            'AND ((f.RelPesContaID = 1 AND f.EstadoID IN (2,3)) OR (f.RelPesContaID <> 1 AND f.EstadoID = 2)) ' +
                            'AND f.Layout = (CASE WHEN (LEN(a.NossoNumero) = 17 AND f.RelPesContaID IN (1, 71)) THEN  2 ELSE 1 END) ' +
                        'GROUP BY a.EmpresaID, g.Fantasia, e.Codigo, e.BancoID, b.RelPesContaID, f.Layout, d.Fantasia ' +
                        'ORDER BY a.EmpresaID, g.Fantasia, e.Codigo, e.BancoID, b.RelPesContaID ';
    }
    else {
        dsoGen01.SQL = 'SELECT ' +
                            'CONVERT(bit,0) AS OK, e.BancoID AS BancoID, e.Codigo + \'-\' + d.Fantasia AS Codigo, STR(COUNT(e.Codigo),5,0) AS QuantidadeTitulos, CONVERT(NUMERIC (11,2), ' +
                            'STR(SUM(a.Valor),11,2)) AS TotalTitulos, ' +
                            'CASE WHEN (SELECT COUNT(1) FROM CobrancaRemessaProcessa aa WHERE e.BancoID = aa.BancoID AND aa.dtProcessamento IS NULL) >= 1 THEN \'Aguardar processamento\' ELSE \'\' END AS Status ' +
                        'FROM ' +
                            'Financeiro a WITH(NOLOCK) ' +
                                'INNER JOIN RelacoesPessoas_Contas b WITH(NOLOCK) ON (a.RelPesContaID = b.RelPesContaID) ' +
                                'INNER JOIN RelacoesPessoas c WITH(NOLOCK) ON (b.RelacaoID = c.RelacaoID) ' +
                                'INNER JOIN Pessoas d WITH(NOLOCK) ON (c.ObjetoID = d.PessoaID) ' +
                                'INNER JOIN Bancos e WITH(NOLOCK) ON (d.BancoID = e.BancoID) ' +
                                'INNER JOIN RelacoesPesRec_Cobranca f WITH(NOLOCK) ON (a.RelPesContaID = f.RelPesContaID) ' +
                        'WHERE ' +
                            'a.EstadoID >= 41 ' +
                            'AND a.EstadoID < 48 ' +
                            'AND a.TipoFinanceiroID=1002 ' +
                            'AND a.ComandoID IS NOT NULL ' +
                            'AND ((f.RelPesContaID = 1 AND f.EstadoID IN (2,3)) OR (f.RelPesContaID <> 1 AND f.EstadoID = 2)) ' +
                            'AND f.Layout = (CASE WHEN (LEN(a.NossoNumero) = 17 AND f.RelPesContaID IN (1, 71)) THEN  2 ELSE 1 END) ' +
                         'GROUP BY e.Codigo, e.BancoID, d.Fantasia ' +
                         'ORDER BY  e.Codigo, e.BancoID';
    }
    
    dsoGen01.ondatasetcomplete = fillModalGridRemessa_DSC;
    dsoGen01.refresh();
}

function fillModalGridRemessa_DSC()
{
    var chkEmpresa = document.getElementById('chkEmpresa').checked;
    var i;

    if ( !(dsoGen01.recordset.BOF && dsoGen01.recordset.EOF) )    
    {
        lblMessage.style.visibility = 'hidden';
        startGridInterface(fg);
        fg.FrozenCols = 0;
        
        if (chkEmpresa) {
            headerGrid(fg, ['Empresa', 'EmpresaID', 'BancoID', 'Banco', 'OK', 'Titulos', 'Total', 'Layout', 'RelPesContaID', 'Status'], [1, 2, 7, 8]);
            fillGridMask(fg, dsoGen01, ['Fantasia*', 'EmpresaID*', 'BancoID*', 'Codigo*', 'OK', 'QuantidadeTitulos*', 'TotalTitulos*', 'Layout*', 'RelPesContaID*', 'Status*'], ['', '', '', '', '', '', '', ''], ['', '', '', '', '', '', '###,###,###.00', '', '']);
            gridHasTotalLine(fg, 'Totais ', 0xC0C0C0, null, true, [[5, '#########', 'S'], [6, '###,###,###,###.00', 'S']]);
        }
        else {
            headerGrid(fg, ['Banco', 'OK', 'Titulos', 'Total', 'BancoID', 'Status'], [4]);
            fillGridMask(fg, dsoGen01, ['Codigo*', 'OK', 'QuantidadeTitulos*', 'TotalTitulos*', 'BancoID*', 'Status*'], ['', '', '', '', ''], ['', '', '', '###,###,###.00', '']);
            gridHasTotalLine(fg, 'Totais ', 0xC0C0C0, null, true, [[2, '#########', 'S'], [3, '###,###,###,###.00', 'S']]);
        }

        fg.Editable = true;
    }
    else
    {
        divFG.style.visibility = 'hidden';
        lblMessage.style.visibility = 'visible';
        document.getElementById('divEmpresa').style.visibility = 'hidden';
    }

    // O estado do botao btnOK
    btnOK_Status();
    
    showModalAfterDataLoad();
}

function chkEmpresa_onclick() {
    var chkEmpresa = document.getElementById('chkEmpresa').checked;

    lockControlsInModalWin(true);

    fillModalGridRemessa(chkEmpresa);    
}

function js_BeforeRowColChangeRemessaCobranca(grid, OldRow, OldCol, NewRow, NewCol, Cancel) {
    //Forca celula read-only    
    if (!glb_GridIsBuilding) {
        if (glb_FreezeRolColChangeEvents)
            return true;

        if (currCellIsLocked(grid, NewRow, NewCol)) {
            glb_validRow = OldRow;
            glb_validCol = OldCol;
        }
        else
            js_fg_BeforeRowColChange(grid, OldRow, OldCol, NewRow, NewCol, Cancel);
    }
}

function js_fg_modalremessacobrancaDblClick(grid, Row, Col) {

    if (fg.Col != getColIndexByColKey(fg, 'OK')) {
        return;
    }
    glb_PassedOneInDblClick = true;

    if (fg.Col == getColIndexByColKey(fg, 'OK')) {
        for (i = 2; i < fg.Rows; i++)
            fg.TextMatrix(i, getColIndexByColKey(fg, 'OK')) = glb_EnableChkOK;
        glb_EnableChkOK = !glb_EnableChkOK;
    }
}

function currCellIsLocked(grid, nRow, nCol) {
    var bRetVal = false;

    if (nRow <= 1)
        return true;

    if (getColIndexByColKey(grid, 'PermiteEditar') < 0)
        return bRetVal;

    bRetVal = ((grid.ValueMatrix(nRow, getColIndexByColKey(grid, 'PermiteEditar')) == 0) && (nCol == getColIndexByColKey(grid, 'OK')));

    return bRetVal;
}

/********************************************************************
Mostra janela apos carregar dados
********************************************************************/
function showModalAfterDataLoad()
{
    // ajusta o body do html
    with (modalremessacobrancaBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
    window.focus();

    if (btnCanc.disabled == false)
        btnCanc.focus();

    // coloca foco no grid
    if ( fg.runtimeStyle.visibility == 'visible' )
        fg.focus();

    lockControlsInModalWin(false);
}