/********************************************************************
superioresp.js

Library javascript de funcoes para abertura de objetos rds dos sups.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:
    setSqlToDso(dso,nID,sCompleteFunction,lByProprietario)
    setup_dsoSup01FromPesq(dso)

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
nID                        -> id do usuario logado se registro novo
                           -> ou id do registro
sCompleteFunction          -> funcao de ondatasetcomplete a ser disparada na volta do servidor
lByProprietario(opcional)  -> se e um novo registro lByProprietario e true, pq qdo e um novo
                              registro o filtro deve ser por proprietarioID e nao pelo identity
                              do registro

Retorno:
nulo
********************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister)
{
    var sSQL;

    //@@
    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 *, ' +
               'CONVERT(VARCHAR, dtGravacao, '+DATE_SQL_PARAM+') as V_dtGravacao, ' +
               'ValorTotalTitulos = dbo.fn_Numero_Formata((CASE TipoCobrancaID WHEN 1091 THEN (SELECT ISNULL(SUM(b.Valor),0) ' +
                   'FROM Cobranca_Detalhes a WITH(NOLOCK), Financeiro b WITH(NOLOCK) ' +
                   'WHERE a.CobrancaID=Cobranca.CobrancaID AND a.FinanceiroID=b.FinanceiroID) ' +
                   'WHEN 1092 THEN (SELECT ISNULL(SUM(a.ValorTitulo),0) ' +
                        'FROM Cobranca_Detalhes a WITH(NOLOCK) ' +
                        'WHERE a.CobrancaID=Cobranca.CobrancaID) END), 2, 1, 101), ' +
               'TotalTitulos = (SELECT COUNT(*) FROM Cobranca_Detalhes a WITH(NOLOCK) ' +
               'WHERE a.CobrancaID=Cobranca.CobrancaID), ' +
			   'BancoID = (SELECT TOP 1 c.BancoID FROM RelacoesPessoas_Contas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), Pessoas c WITH(NOLOCK) WHERE (a.RelPesContaID = Cobranca.RelPesContaID AND a.RelacaoID = b.RelacaoID AND b.ObjetoID = c.PessoaID)), ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Cobranca.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM Cobranca  ' +
               'WHERE ProprietarioID = '+ nID + 'ORDER BY CobrancaID DESC';

    else
        sSQL = 'SELECT *, ' + 
               'CONVERT(VARCHAR, dtGravacao, '+DATE_SQL_PARAM+') as V_dtGravacao, ' +
               'ValorTotalTitulos = dbo.fn_Numero_Formata((CASE TipoCobrancaID WHEN 1091 THEN (SELECT ISNULL(SUM(b.Valor),0) ' +
               'FROM Cobranca_Detalhes a WITH(NOLOCK), Financeiro b WITH(NOLOCK) ' +
               'WHERE a.CobrancaID=Cobranca.CobrancaID AND a.FinanceiroID=b.FinanceiroID) ' +
               'WHEN 1092 THEN (SELECT ISNULL(SUM(a.ValorTitulo),0) ' +
               'FROM Cobranca_Detalhes a WITH(NOLOCK) ' +
               'WHERE a.CobrancaID=Cobranca.CobrancaID) END),2,1,101), ' +
               'TotalTitulos = (SELECT COUNT(*) FROM Cobranca_Detalhes a WITH(NOLOCK) ' +
               'WHERE a.CobrancaID=Cobranca.CobrancaID), ' +
			   'BancoID = (SELECT TOP 1 c.BancoID FROM RelacoesPessoas_Contas a WITH(NOLOCK), RelacoesPessoas b WITH(NOLOCK), Pessoas c WITH(NOLOCK) WHERE (a.RelPesContaID = Cobranca.RelPesContaID AND a.RelacaoID = b.RelacaoID AND b.ObjetoID = c.PessoaID)), ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nID + ' AND ObjetoID=Cobranca.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END), ' +
               'Prop1 = CASE ProprietarioID ' +
               'WHEN ' + nUserID + ' THEN 1 ' +
               'ELSE 0 ' +
               'END, ' +
               'Prop2 = (CASE WHEN ProprietarioID= ' + nUserID + ' THEN 1 ' +
               'WHEN AlternativoID= ' + nUserID + ' THEN 1 ' +
               'WHEN (SELECT COUNT (*) FROM RelacoesPessoas WITH(NOLOCK) ' + 
               'WHERE EstadoID=2 AND TipoRelacaoID=34 AND SujeitoID= ' + nUserID + ' AND ObjetoID=Cobranca.AlternativoID)>0 THEN 1 ' +
               'ELSE 0 ' +
               'END) ' +
               'FROM Cobranca WITH(NOLOCK) ' +
               'WHERE CobrancaID = ' + nID + ' ORDER BY CobrancaID DESC';

    setConnection(dso);
    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();
    
    return null;
}

/********************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function setup_dsoSup01FromPesq(dso)
{
    setConnection(dso);
    
    var sql;
    
    sql = 'SELECT *, ' +
          'CONVERT(VARCHAR, dtGravacao, '+DATE_SQL_PARAM+') as V_dtGravacao, ' +
          'ValorTotalTitulos = dbo.fn_Numero_Formata((CASE TipoCobrancaID WHEN 1091 THEN (SELECT ISNULL(SUM(b.Valor),0) ' +
          'FROM Cobranca_Detalhes a, Financeiro b ' +
          'WHERE a.CobrancaID=Cobranca.CobrancaID AND a.FinanceiroID=b.FinanceiroID) ' +
          'WHEN 1092 THEN (SELECT ISNULL(SUM(a.ValorTitulo),0) ' +
          'FROM Cobranca_Detalhes a ' +
          'WHERE a.CobrancaID=Cobranca.CobrancaID) END),2,1,101), ' +
          'TotalTitulos = (SELECT COUNT(*) FROM Cobranca_Detalhes a ' +
          'WHERE a.CobrancaID=Cobranca.CobrancaID), ' +
		  'BancoID = (SELECT TOP 1 c.BancoID FROM RelacoesPessoas_Contas a, RelacoesPessoas b, Pessoas c WHERE (a.RelPesContaID = Cobranca.RelPesContaID AND a.RelacaoID = b.RelacaoID AND b.ObjetoID = c.PessoaID)), ' +
          '0 AS Prop1, 0 AS Prop2 ' +
          'FROM Cobranca WHERE CobrancaID = 0';
    
    dso.SQL = sql;          
}              
