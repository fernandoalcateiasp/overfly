
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
'Arquivo pesqlistsvr.asp de todos os forms
'Para cada form e necessario acrescentar um novo case onde assinalado
'por //@@

On Error Resume Next
Response.ContentType = "text/xml"

Dim i
Dim fldF
Dim rsSPCommand

Dim nCobrancaID
Dim errorOcurr

Dim rsNew  
Dim rsERROR

Dim sMsgErro

nCobrancaID = 0
sMsgErro = ""

For i = 1 To Request.QueryString("nCobrancaID").Count    
    nCobrancaID = Request.QueryString("nCobrancaID")(i)
Next

Set rsSPCommand = Server.CreateObject("ADODB.Command")
  
With rsSPCommand
	.CommandTimeout = 60 * 25
    .ActiveConnection = strConn
    .CommandText = "sp_Cobranca_BaixaAutomatica"
    .CommandType = adCmdStoredProc
    .Parameters.Append( .CreateParameter("@CobrancaID", adInteger, adParamInput, 10 , CLng(nCobrancaID)) )
    .Parameters.Append( .CreateParameter("@Erro",adVarChar, adParamOutput, 1024) )
    .Execute
    
    sMsgErro = rsSPCommand.Parameters("@Erro").Value
End With

errorOcurr = False
For Each rsERROR In rsSPCommand.ActiveConnection.Errors
    errorOcurr = True
Next

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer

rsNew.Fields.Append "fldError", adVarChar, 100, adFldUpdatable
  
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText

'se deu erro

rsNew.AddNew

If (errorOcurr) Then
    rsNew.Fields("fldError").Value = "Erro ao processar solicitação, tente novamente"
ElseIf (sMsgErro <> "") Then
    rsNew.Fields("fldError").Value = sMsgErro
Else
    rsNew.Fields("fldError").Value = ""
End If

rsNew.Update
'retorna recordset ao cliente
rsNew.Save Response, adPersistXML

Set rsSPCommand = Nothing
  
rsNew.Close
Set rsNew = Nothing

%>
