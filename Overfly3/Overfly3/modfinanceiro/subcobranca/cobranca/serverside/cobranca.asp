
<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<%
On Error Resume Next
Response.ContentType = "text/xml"

Dim rsNew
Dim nCobrancaID
Dim strSQL
Dim nRecsAffected
Dim rsCommand
Dim i
  
sFldKey = ""
nOrder = ""
strSQL = ""
nCobrancaID = 0

Set rsCommand = Server.CreateObject("ADODB.Command")

rsCommand.CommandTimeout = 60 * 10

For i = 1 To Request.QueryString("nCobrancaID").Count    
    nCobrancaID = Request.QueryString("nCobrancaID")(i)
Next

strSQL = "EXEC sp_Cobranca_Detalhes " & nCobrancaID 

If (strSQL <> "") Then
    rsCommand.ActiveConnection = strConn
    rsCommand.CommandText = strSQL
    rsCommand.CommandType = adCmdText
    rsCommand.Execute nRecsAffected
End If

Set rsCommand = Nothing

Set rsNew = Server.CreateObject("ADODB.Recordset")
rsNew.CursorLocation = adUseServer
rsNew.Fields.Append "fldresp", adVarChar, 50, adFldUpdatable
rsNew.Open , , adOpenKeyset, adLockOptimistic, adCmdText
rsNew.AddNew
rsNew.Fields("fldresp").Value = CStr(nRecsAffected)
rsNew.Update
rsNew.Save Response, adPersistXML
rsNew.Close
Set rsNew = Nothing
%>

