﻿using java.lang;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSData;

namespace Overfly3.modfinanceiro.subcobranca.cobranca.serverside
{
    public partial class blacklistcobranca : System.Web.UI.OverflyPage
    {

        protected static Integer zero = new Integer(0);
        protected static Integer[] zeroArray = new Integer[0];

        protected Integer tipoOperacao;
        protected Integer nTipoOperacao
        {
            get { return tipoOperacao; }
            set { tipoOperacao = value != null ? value : zero; }
        }

        protected Integer pessoaID;
        protected Integer nPessoaID
        {
            get { return pessoaID; }
            set { pessoaID = value != null ? value : zero; }
        }


        protected Integer usuarioID;
        protected Integer nUsuarioID
        {
            get { return usuarioID; }
            set { usuarioID = value != null ? value : zero; }
        }

        protected Integer[] cobBancoID;

        protected Integer[] nCobBancoID
        {
            get { return cobBancoID; }
            set { cobBancoID = value != null ? value : zeroArray; }
        }

        protected string[] bloq;
        protected String[] sBloq
        {
            get { return bloq; }
            set { bloq = value; }
        }

        protected Integer dataLen;
        protected Integer nDataLen
        {
            get { return dataLen; }
            set { dataLen = value != null ? value : zero; }
        }


        protected string returnStringSQL(Integer _tipoOperacao)
        {
            string sql = "";

            // 1 - Inclusão. 2 - Exclusão. 3 - Atualização dos bloqueios dos bancos.
            switch (_tipoOperacao == null ? 3 : _tipoOperacao.intValue())
            {
                case 1:
                    sql = "INSERT INTO CobrancaBlackList (PessoaID, UsuarioID, dtBlackList)" +
                    "SELECT " + pessoaID.ToString() + ", " + usuarioID.ToString() + ", GETDATE()";
                    break;
                case 2:
                    sql = "DELETE FROM CobrancaBlackList WHERE PessoaID = " + pessoaID;
                    break;
                case 3:
                    for (int i = 0; i < dataLen.intValue(); i++)
                    {
                        sql += " UPDATE CobrancaBlackList_Bancos SET Bloqueado = " + bloq[i] + " WHERE (CobBancoID = " + cobBancoID[i] + ")";
                    }
                    break;
                default:
                    break;
            }

            return sql;
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT " + DataInterfaceObj.ExecuteSQLCommand(returnStringSQL(tipoOperacao)).ToString() + " as Resultado"));
        }

    }
}