using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modfinanceiro.subcobranca.cobranca.serverside
{
	public partial class processaarqret : System.Web.UI.OverflyPage
	{
		private Integer cobrancaID;
		protected Integer nCobrancaID
		{
			get { return cobrancaID; }
			set { cobrancaID = value; }
		}

		protected string CobrancaBaixaAutomatica()
		{
			ProcedureParameters[] param = new ProcedureParameters[]
				{
					new ProcedureParameters("@CobrancaID", SqlDbType.Int, nCobrancaID.intValue(), ParameterDirection.Input),
					new ProcedureParameters("@Erro", SqlDbType.VarChar, DBNull.Value, ParameterDirection.InputOutput, 1024)
				};

			DataInterfaceObj.execNonQueryProcedure("sp_Cobranca_BaixaAutomatica", param);

			string result = (param[1].Data != DBNull.Value) ? (string) param[1].Data : "";

			return result;
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			string msg = CobrancaBaixaAutomatica();

			if (msg != "")
			{
				WriteResultXML(DataInterfaceObj.getRemoteData("select '" + msg + "' as fldError"));
			}
			else
			{
				WriteResultXML(DataInterfaceObj.getRemoteData("select '" + msg + "' as fldError"));
			}
		}
	}
}
