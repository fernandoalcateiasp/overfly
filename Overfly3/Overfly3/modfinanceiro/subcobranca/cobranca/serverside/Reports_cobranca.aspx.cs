﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;

namespace Overfly3.PrintJet
{
    public partial class Reports_cobranca : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        //private int RelatorioID;
        private int mCobrancaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nCobrancaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string nomeRel = Request.Params["relatorioID"].ToString();
                //RelatorioID = Convert.ToInt32(Request.Params["relatorioID"].ToString());

                switch (nomeRel)
                {
                    // Remessa de Cobrança
                    case "40201":
                        remessaCobranca();
                        break;

                    case "40202":
                        retornoCobranca();
                        break;
                }
            }
        }

        public void remessaCobranca()
        {
            // Excel
            int Formato = 2;
            string Title = "Remessa de Cobrança #"+mCobrancaID;
            string EmpresaNome = mEmpresaFantasia;
            //mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["EmpresaID"]);
            string strSQL = "SELECT b.SequencialRegistro AS Seql, b.NossoNumero AS [Nosso Número], b.Carteira AS Cart, d.Comando AS Com, b.Instrucao1 AS Inst1, b.Instrucao2 AS Inst2, " +
                              "e.FinanceiroID AS Financeiro, f.RecursoAbreviado AS Est, e.PedidoID AS [Número], e.Duplicata, g.Fantasia AS Cliente, CONVERT(VARCHAR(10),e.dtEmissao,103) AS [Emissão], h.ItemAbreviado AS Forma, " +
                              "e.PrazoPagamento AS Prazo, CONVERT(VARCHAR(10),e.dtVencimento,103) AS Vencimento, e.Valor " +
                              "FROM Cobranca a WITH(NOLOCK) " +
                                "INNER JOIN Cobranca_Detalhes b WITH(NOLOCK) ON (a.CobrancaID=b.CobrancaID) " +
                                "INNER JOIN RelacoesPessoas_Contas i WITH(NOLOCK) ON (a.RelPesContaID = i.RelPesContaID) " +
                                "INNER JOIN RelacoesPessoas j WITH(NOLOCK) ON (i.RelacaoID = j.RelacaoID) " +
                                "INNER JOIN Pessoas l WITH(NOLOCK) ON (j.ObjetoID = l.PessoaID) " +
                                "INNER JOIN Bancos c WITH(NOLOCK) ON (l.BancoID=c.BancoID) " +
                                "INNER JOIN Bancos_Comandos d WITH(NOLOCK) ON (c.BancoID=d.BancoID AND b.ComandoID=d.ComandoID) " +
                                "INNER JOIN Financeiro e WITH(NOLOCK) ON (b.FinanceiroID=e.FinanceiroID) " +
                                "LEFT OUTER JOIN TiposAuxiliares_Itens h WITH(NOLOCK) ON (e.FormaPagamentoID=h.ItemID) " +
                                "INNER JOIN Recursos f WITH(NOLOCK) ON (e.EstadoID=f.RecursoID) " +
                                "INNER JOIN Pessoas g WITH(NOLOCK) ON (e.PessoaID=g.PessoaID) " +
                              "WHERE (a.CobrancaID= " + mCobrancaID + ")" +
                              "ORDER BY b.SequencialRegistro";
            var Header_Body = (Formato == 1 ? "Header" : "Body");
            //var posY = (Formato == 1 ? "0" : "0.004");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", EmpresaNome, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "7.13834", "0.0", "11", "#0113a0", "B", Header_Body, "6.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.013834", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "10.0", "0.0", "11", "black", "B", Header_Body, "50.0", "Horas");
                Relatorio.CriarObjLabelData("Now", "", "", "16.795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.6795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");


                //Relatorio.CriarObjLabel("Fixo", "", Title, "1", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "80", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                //Relatorio.CriarObjTabela("0.00", "0.00001", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }

        public void retornoCobranca()
        {
            // Excel
            int Formato = 2;
            string Title = "Retorno de Cobrança #" + mCobrancaID;
            string EmpresaNome = mEmpresaFantasia;
            //mEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["EmpresaID"]);
            string strSQL = "SELECT b.SequencialRegistro AS Seql, b.NossoNumero AS [Nosso Número], f.Nome AS Banco, f.Agencia AS [Agência], " +
                                "b.Carteira AS Cart, g.Modalidade AS [Mod], i.ItemMasculino AS Modalidade, h.Modalidade AS [M-A], j.ItemMasculino AS [Modalidade Anterior], " +
                                "c.Comando AS Com, m.ItemMasculino AS Comando, b.Motivo, CONVERT(VARCHAR(4), b.OcorrenciaID) + (CASE b.Processado WHEN 1 THEN " + "\'" + " X" + "\'" + " ELSE SPACE(0) END) AS Ocor, " +
                                "b.FinanceiroID AS Financeiro, l.NotaFiscal AS [Nota Fiscal], CONVERT(VARCHAR(10),b.dtVencimento,103) AS Vencimento, b.ValorTitulo AS [Valor Título], CONVERT(VARCHAR(10),b.dtEntrada,103) AS [Data Entrada], " +
                                "b.ValorTarifa AS [Valor Tarifa], b.OutrasDespesas AS [Outras Despesas], b.JurosDesconto AS [Juros Desconto], b.IOFDesconto AS [IOF Desconto], " +
                                "b.ValorAbatimento AS [Valor Abatimento], b.DescontoConcedido AS [Desconto Concedido], b.ValorRecebido AS [Valor Recebido], " +
                                "b.JurosMora AS [Juros Mora], b.OutrosRecebimentos AS [Outros Recebimentos], b.AbatimentoNaoAproveitado AS [Abatimento não Aproveitado] , " +
                                "b.ValorLancamento AS [Valor Lançamento] " +
                                "FROM Cobranca a WITH(NOLOCK) " +
                                    "INNER JOIN Cobranca_Detalhes b WITH(NOLOCK) ON a.CobrancaID=b.CobrancaID " +
                                    "INNER JOIN RelacoesPessoas_Contas d WITH(NOLOCK) ON a.RelPesContaID = d.RelPesContaID " +
                                    "INNER JOIN RelacoesPessoas e WITH(NOLOCK) ON e.RelacaoID = d.RelacaoID " +
                                    "INNER JOIN Bancos_Comandos c WITH(NOLOCK) ON (b.ComandoID = c.ComandoID AND b.Comando=c.Comando) " +
                                    "INNER JOIN Pessoas f WITH(NOLOCK) ON e.ObjetoID=f.PessoaID AND f.BancoID=c.BancoID  " +
                                    "LEFT OUTER JOIN Bancos_Modalidades g WITH(NOLOCK) ON b.ModalidadeID=g.ModalidadeID AND g.BancoID=c.BancoID " +
                                    "LEFT OUTER JOIN Bancos_Modalidades h WITH(NOLOCK) ON b.ModalidadeAnteriorID=h.ModalidadeID AND h.BancoID=c.BancoID " +
                                    "LEFT JOIN TiposAuxiliares_Itens i WITH(NOLOCK) ON g.ModalidadeID = i.ItemID AND i.TipoID = 813 " +
                                    "LEFT JOIN TiposAuxiliares_Itens j WITH(NOLOCK) ON h.ModalidadeID = j.ItemID AND j.TipoID = 813 " +
                                    "LEFT JOIN Financeiro k WITH(NOLOCK) ON b.FinanceiroID = k.FinanceiroID " +
                                    "LEFT JOIN NotasFiscais l WITH(NOLOCK) ON k.PedidoID = l.PedidoID AND l.EstadoID=67 " +            
                                    "LEFT JOIN TiposAuxiliares_Itens m WITH(NOLOCK) ON b.ComandoID = m.ItemID AND m.TipoID = 810 " +
                                "WHERE a.CobrancaID = " + mCobrancaID + " " +
                                "ORDER BY b.SequencialRegistro";
            var Header_Body = (Formato == 1 ? "Header" : "Body");
            //var posY = (Formato == 1 ? "0" : "0.004");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", EmpresaNome, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "7.13834", "0.0", "11", "#0113a0", "B", Header_Body, "6.51332", "");
                //Relatorio.CriarObjLabel("Fixo", "", Title, "1.013834", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "10.0", "0.0", "11", "black", "B", Header_Body, "50.0", "Horas");
                Relatorio.CriarObjLabelData("Now", "", "", "16.795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");
                //Relatorio.CriarObjLabelData("Now", "", "", "1.6795", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");


                //Relatorio.CriarObjLabel("Fixo", "", Title, "1", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                //Relatorio.CriarObjLabelData("Now", "", "", "80", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.2", "Query1", true, true, true);
                //Relatorio.CriarObjTabela("0.00", "0.00001", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }

            Relatorio.CriarPDF_Excel(Title, Formato);
        }
    }
}
