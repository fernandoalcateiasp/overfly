using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modfinanceiro.subcobranca.cobranca.serverside
{
	public partial class RemessaCobranca : System.Web.UI.OverflyPage
	{

        private string sData = Convert.ToString(HttpContext.Current.Request.Params["nData"]);
        private bool chkEmpresa = Convert.ToBoolean(HttpContext.Current.Request.Params["chkEmpresa"]);
        private int aLength = Convert.ToInt32(HttpContext.Current.Request.Params["aLength"]);
        private int iUser = Convert.ToInt32(HttpContext.Current.Request.Params["iUser"]);
        private int nCobrancaID = Convert.ToInt32(HttpContext.Current.Request.Params["nCobrancaID"]);

        protected override void PageLoad(object sender, EventArgs e)
        {
            string sMensagem = "NULL";
            if (nCobrancaID != 0)
            {
                string strSQL = "UPDATE CobrancaRemessaProcessa SET dtProcessamento = NULL, UsuarioID = " + iUser + " WHERE CobrancaID = " + nCobrancaID;
                DataInterfaceObj.ExecuteSQLCommand(strSQL);
            }
            else
            {
                string sBanco = "NULL";
                string sRelPesConta = "NULL";

                if (chkEmpresa)
                    sRelPesConta = sData;
                else
                    sBanco = sData;

                ProcedureParameters[] procParams = new ProcedureParameters[8];
                procParams[0] = new ProcedureParameters("@EmpresaID", System.Data.SqlDbType.Int, DBNull.Value);

                if (sRelPesConta == "NULL")
                    procParams[1] = new ProcedureParameters("@RelPesContas", SqlDbType.VarChar, DBNull.Value);
                else
                    procParams[1] = new ProcedureParameters("@RelPesContas", SqlDbType.VarChar, sRelPesConta.ToString());

                if (sBanco == "NULL")
                    procParams[2] = new ProcedureParameters("@Bancos", SqlDbType.VarChar, DBNull.Value);
                else
                    procParams[2] = new ProcedureParameters("@Bancos", SqlDbType.VarChar, sBanco.ToString());

                procParams[3] = new ProcedureParameters("@Usuarios", SqlDbType.Int, iUser);

                procParams[4] = new ProcedureParameters("@Layout", System.Data.SqlDbType.Int, DBNull.Value);

                procParams[5] = new ProcedureParameters("@Controle", System.Data.SqlDbType.Int, 1);

                procParams[6] = new ProcedureParameters("@CobrancaRemessaProcessaID", System.Data.SqlDbType.Int, DBNull.Value);

                procParams[7] = new ProcedureParameters("@Mensagem", System.Data.SqlDbType.VarChar, DBNull.Value, ParameterDirection.Output, 8000);

                DataInterfaceObj.execNonQueryProcedure(
                    "sp_CobrancaRemessa_Gerador",
                    procParams);

                sMensagem = ((procParams[7].Data != DBNull.Value) ? (string)procParams[7].Data : null);

                if (sMensagem != null)
                {
                    HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert(\"Favor selecionar apenas itens que n�o est�o em procesamento.\");window.parent.lockControlsInModalWin(false);</script>");
                    return;
                }
            }
            WriteResultXML(DataInterfaceObj.getRemoteData("select '" + sMensagem + "' as fldresp"));
        }
	}
}
