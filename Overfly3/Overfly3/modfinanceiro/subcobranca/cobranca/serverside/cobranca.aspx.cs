using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modfinanceiro.subcobranca.cobranca.serverside
{
	public partial class cobranca : System.Web.UI.OverflyPage
	{
		private Integer cobrancaID;
		protected Integer nCobrancaID
		{
			get { return cobrancaID; }
			set { cobrancaID = value; }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			int fldresp = DataInterfaceObj.ExecuteSQLCommand("EXEC sp_Cobranca_Detalhes " + nCobrancaID);

			WriteResultXML(DataInterfaceObj.getRemoteData("select " + fldresp + " as fldresp"));
		}
	}
}
