/********************************************************************
inferioresp.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.CobrancaID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Cobranca a WHERE '+
        sFiltro + 'a.CobrancaID = '+idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28201) // Remessa
    {
        dso.SQL = 'SELECT a.* FROM Cobranca_Detalhes a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.CobrancaID = '+idToFind + ' ' +
        'ORDER BY a.SequencialRegistro';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28202) // Retorno
    {
        var nBancoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['BancoID'].value");
        
        dso.SQL = 'SELECT a.*, c.Modalidade AS Modalidade, d.Modalidade AS ModalidadeAnterior, e.ItemMasculino AS Ocorrencia ' +
            'FROM Cobranca_Detalhes a WITH(NOLOCK) ' +
                'LEFT OUTER JOIN Bancos_Modalidades c WITH(NOLOCK) ON (a.ModalidadeID=c.ModalidadeID AND c.BancoID=' + nBancoID + ') ' +
                'LEFT OUTER JOIN Bancos_Modalidades d WITH(NOLOCK) ON (a.ModalidadeAnteriorID=d.ModalidadeID AND d.BancoID=' + nBancoID + ') ' +
                'LEFT JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON a.OcorrenciaID = e.ItemID ' + 
            'WHERE ' + sFiltro + 'a.CobrancaID = ' + idToFind + ' ' +
            'ORDER BY a.SequencialRegistro';
        return 'dso01Grid_DSC';
    }
    if (folderID == 28050) // Resumo
    {
        dso.SQL = 'SELECT a.CobrancaID, ' +
                        'dbo.fn_Cobranca_Resumo(a.CobrancaID, 1) AS ValorCobranca, ' +
                        'dbo.fn_Cobranca_Resumo(a.CobrancaID, 2) AS ContaCorrente, ' +
                        'dbo.fn_Cobranca_Resumo(a.CobrancaID, 3) AS ContaVinculada, ' +
                        'dbo.fn_Cobranca_Resumo(a.CobrancaID, 4) AS ValorExtrato, ' +
		                'dbo.fn_Cobranca_Resumo(a.CobrancaID, 5) AS ContaNaoIdentificada ' +
                    'FROM Cobranca a WITH(NOLOCK) WHERE ' + sFiltro + 'a.CobrancaID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
       
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }

    // Remessa
    else if ( pastaID == 28201 )
    {
        var nBancoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL ,"dsoSup01.recordset['BancoID'].value");

        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ComandoID, Comando ' +
                      'FROM Bancos_Comandos WITH(NOLOCK) ' +
                      'WHERE BancoID = ' + nBancoID + ' ' +
                      'ORDER BY Comando';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT a.CobDetalheID,c.RecursoAbreviado,b.PedidoID,b.Duplicata,d.Fantasia, ' +
                      'b.dtEmissao, e.ItemAbreviado, b.PrazoPagamento, b.dtVencimento, f.SimboloMoeda, b.Valor ' +
                      'FROM Cobranca_Detalhes a WITH(NOLOCK), Financeiro b WITH(NOLOCK), Recursos c WITH(NOLOCK), Pessoas d WITH(NOLOCK), TiposAuxiliares_Itens e WITH(NOLOCK), Conceitos f WITH(NOLOCK) ' +
                      'WHERE a.CobrancaID= ' + nRegistroID + ' AND a.FinanceiroID=b.FinanceiroID AND b.EstadoID=c.RecursoID ' +
                      'AND b.PessoaID=d.PessoaID AND b.FormaPagamentoID=e.ItemID ' +
                      'AND b.MoedaID=f.ConceitoID';
        }
    }        
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(28050);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 28050); //Resumo

    //Remessa
    if ( nTipoRegistroID == 1091 )
	{
        vPastaName = window.top.getPastaName(28201);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 28201); 
    }
    //Retorno
    else if ( nTipoRegistroID == 1092 )
	{
        vPastaName = window.top.getPastaName(28202);
	    if (vPastaName != '')
	    	addOptionToSelInControlBar('inf', 1, vPastaName, 28202); 
    }

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 28201) // Remessa
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1], ['CobrancaID', registroID]);
        
        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Remessa
    else if (folderID == 28201)
    {
        headerGrid(fg,['_calc_hidden_0',
					   'Sequencial',
                       'Nosso N�mero',
                       'Cart',
                       'Com',
                       'Inst1',
                       'Inst2',
                       'Financeiro',
                       'Est',
                       'N�mero',
                       'Duplicata',
                       'Pessoa',
                       'Emissao',
                       'Forma',
                       'Prazo',
                       'Vencimento',
                       'Moeda',
                       'Valor',
                       'CobDetalheID'], [0, 18]);

        glb_aCelHint = [[0,3,'Carteira de cobran�a'],
                        [0,4,'Comando de cobran�a']];
                       
        fillGridMask(fg,currDSO,['_calc_hidden_0',
								 'SequencialRegistro',
                                 'NossoNumero',
                                 'Carteira',
                                 'ComandoID',
                                 'Instrucao1',
                                 'Instrucao2',
                                 'FinanceiroID',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^RecursoAbreviado',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^PedidoID',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^Duplicata',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^Fantasia',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^dtEmissao',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^ItemAbreviado',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^PrazoPagamento',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^dtVencimento',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^SimboloMoeda',
                                 '^CobDetalheID^dso01GridLkp^CobDetalheID^Valor',
                                 'CobDetalheID'],
                                 ['','','','','','','','','','','','','99/99/9999','','','99/99/9999','','',''],
                                 ['','','','','','','','','','','','',dTFormat,'','',dTFormat,'','###,###,###.00','']);
        fg.FrozenCols = 1;

		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1,'######','C'], 
			[17,'###,###,###.00','S']]);
			
		if (fg.Rows > 1)
			fg.TextMatrix(1, getColIndexByColKey(fg, 'NossoNumero')) = 'Totais';
    }    
    // Retorno
    else if (folderID == 28202)
    {
        headerGrid(fg,['_calc_hidden_0',
					   'Sequencial',
                       'Nosso N�mero',
                       'Ag�ncia',
                       'Cart',
                       'Mod',
                       'M-A',
                       'Com',
                       'Mot',
                       'ID',
                       'Ocorr�ncia',
                       'OK',
                       'Financeiro',
                       'Vencimento',
                       'Valor T�tulo',
                       'Data Entrada',
                       'Data Cr�dito',
                       'Valor Tarifa',
                       'Outras Despesas',
                       'Juros Descontos',
                       'IOF Desconto',
                       'Valor Abatimento',
                       'Desconto Concedido',
                       'Valor Recebido',
                       'Juros Mora',
                       'Outros Recebimentos',
                       'Abatimento N�o Aprov',
                       'Valor Lancamento',
                       'CobDetalheID'], [0,28]);

        glb_aCelHint = [[0,4,'Carteira de cobran�a'],
						[0,5,'Modalidade de cobran�a'],
                        [0,6,'Modalidade de cobran�a anterior'],
                        [0,7,'Comando de cobran�a'],
                        [0,8,'Motivo'],
                        [0,9,'Ocorr�ncia de processamento']];

        fillGridMask(fg,currDSO,['_calc_hidden_0',
                                 'SequencialRegistro',
                                 'NossoNumero',
                                 'AgenciaCobradora',
                                 'Carteira',
								 'Modalidade',
								 'ModalidadeAnterior',
                                 'Comando',
                                 'Motivo',
                                 'OcorrenciaID',
                                 'Ocorrencia',
                                 'Processado',
                                 'FinanceiroID',
                                 'dtVencimento',
                                 'ValorTitulo',
                                 'dtEntrada',
                                 'dtCredito',
                                 'ValorTarifa',
                                 'OutrasDespesas',
                                 'JurosDesconto',
                                 'IOFDesconto',
                                 'ValorAbatimento',
                                 'DescontoConcedido',
                                 'ValorRecebido',
                                 'JurosMora',
                                 'OutrosRecebimentos',
                                 'AbatimentoNaoAproveitado',
                                 'ValorLancamento',
                                 'CobDetalheID'],
                                 ['','','','','','','','','','','','','','','','','','','','','','','','','','','','',''],
                                 ['','','','','','','','','','','','','',
                                 dTFormat,
                                 '##,###,###,###.00',
                                 dTFormat,
                                 dTFormat,
                                 '##,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '##,###,###,###.00',
                                 '']);
        fg.FrozenCols = 1;
        
		gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[1,'######','C'], 
			[14,'###,###,###.00','S'],			
			[17,'###,###,###.00','S'],
			[18,'###,###,###.00','S'],
			[19,'###,###,###.00','S'],
			[20,'###,###,###.00','S'],
			[21,'###,###,###.00','S'],
			[22,'###,###,###.00','S'],
			[23,'###,###,###.00','S'],
			[24,'###,###,###.00','S'],
			[25,'###,###,###.00','S'],
			[26,'###,###,###.00','S'],
			[27,'###,###,###.00','S']]);

        alignColsInGrid(fg, [1, 3, 4, 7, 8, 11,14, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27]);
		if (fg.Rows > 1)
			fg.TextMatrix(1, getColIndexByColKey(fg, 'NossoNumero')) = 'Totais';
    }    
}
