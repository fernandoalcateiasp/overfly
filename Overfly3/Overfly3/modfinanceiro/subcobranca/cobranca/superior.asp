<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="cobrancasup01Html" name="cobrancasup01Html">
	<head>
	<title></title>

	<%
		'Links de estilo, bibliotecas da automacao e especificas
		Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
	    
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
	    
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcobranca/cobranca/superior.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcobranca/cobranca/superioresp.js" & Chr(34) & "></script>" & vbCrLf
	%>


	<script ID="wndJSProc" LANGUAGE="javascript">
	<!--

	//-->
	</script>
	</head>

	<!-- //@@ -->
	<body id="cobrancasup01Body" name="cobrancasup01Body" LANGUAGE="javascript" onload="return window_onload()">
	        
		<!-- Controle para simular tab na gravacao -->
		<input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
	    
		<!-- //@@ Os divs sao definidos de acordo com o form -->
		<!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
		<!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->
	    
		<!-- Div principal superior -->
		<div id="divSup01_01" name="divSup01_01" class="divExtern">
			<!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->
			<p class="lblGeneral" id="lblRegistroID">ID</p>
			<input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">
			<p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
			<input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" name="txtEstadoID" class="fldGeneral">        
	        
			<p id="lblRelPesContaID" name="lblRelPesContaID" class="lblGeneral">Banco</p>
			<select id="selRelPesContaID" name="selRelPesContaID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="RelPesContaID"></select>    
			<p id="lblTipoRegistroID" name="lblTipoRegistroID" class="lblGeneral">Tipo</p>
			<select id="selTipoRegistroID" name="selTipoRegistroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="TipoCobrancaID"></select>    
			<p id="lbldtGravacao" name="lbldtGravacao" class="lblGeneral">Data</p>
			<input type="text" id="txtdtGravacao" name="txtdtGravacao" DATASRC="#dsoSup01" DATAFLD="V_dtGravacao" class="fldGeneral">
			<p id="lblSequencial" name="lblSequencial" class="lblGeneral">Sequencial</p>
			<input type="text" id="txtSequencial" name="txtSequencial" DATASRC="#dsoSup01" DATAFLD="Sequencial" class="fldGeneral" title="N�mero sequencial de cobran�a">
			<p id="lblArquivo" name="lblArquivo" class="lblGeneral">Arquivo</p>
			<input type="text" id="txtArquivo" name="txtArquivo" DATASRC="#dsoSup01" DATAFLD="Arquivo" class="fldGeneral">
			<p id="lblTitulos" name="lblTitulos" class="lblGeneral">T�tulos</p>
			<input type="text" id="Text1" name="txtTitulos" DATASRC="#dsoSup01" DATAFLD="TotalTitulos" class="fldGeneral">
			<p id="lblLayout" name="lblLayout" class="lblGeneral">Layout</p>
			<input type="text" id="txtLayout" name="txtLayout" DATASRC="#dsoSup01" DATAFLD="Layout" class="fldGeneral">
			<p id="lblTotal" name="lblTotal" class="lblGeneral">Total</p>
			<input type="text" id="txtTotal" name="txtTotal" DATASRC="#dsoSup01" DATAFLD="ValorTotalTitulos" class="fldGeneral">
			<p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
			<input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral">
		</div>

        <iframe id="frmRemessa" name="frmRemessa" style="display:none"  frameborder="no"></iframe>
	</body>
</html>
