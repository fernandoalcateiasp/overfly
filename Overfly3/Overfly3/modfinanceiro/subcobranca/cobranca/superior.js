/********************************************************************
superior.js

Library javascript para o tiposrelsup01.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_localTimerInt = null;
// Variavel de controle de inclus�o
var glb_insertCobranca = false;

// Dados do registro corrente .SQL 
var dsoSup01 = new CDatatransport("dsoSup01");
// Descricao do estado atual do registro .SQL 
var dsoStateMachine = new CDatatransport("dsoStateMachine");
// Dados dos combos estaticos .URL 
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// Processa Retorno da Cobranca .URL 
var dsoRetornoCobranca = new CDatatransport("dsoRetornoCobranca");
// //@@ Os dsos sao definidos de acordo com o form 
var dsoCobranca = new CDatatransport("dsoCobranca");
var dsoVerificacao = new CDatatransport("dsoVerificacao");
// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

FUNCOES DA AUTOMACAO:
    putSpecialAttributesInControls()
    prgServerSup(btnClicked)
    prgInterfaceSup(btnClicked)
    optChangedInCmb(cmb)
    btnLupaClicked(btnClicked)
    finalOfSupCascade(btnClicked)
    btnBarClicked(controlBar, btnClicked)
    btnBarNotEspecClicked(controlBar, btnClicked)
    modalInformForm(idElement, param1, param2)
    supInitEditMode()
    formFinishLoad()

FUNCOES DA MAQUINA DE ESTADO:
    stateMachOpened( currEstadoID )
    stateMachBtnOK( currEstadoID, newEstadoID )
    stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
    
FUNCOES ESPECIFICAS DO FORM:    

FUNCOES DO CARRIER:
    carrierArrived(idElement, idBrowser, param1, param2)
    carrierReturning(idElement, idBrowser, param1, param2)

********************************************************************/

/********************************************************************

// IMPLEMENTACAO DAS FUNCOES

//@@ todas as funcoes abaixo sao particulares de cada form
// conforme o form algumas funcoes devem ser removidas
// e outras serao acrescentadas
********************************************************************/

function window_onload()
{
    //@@ Iniciado carregamento de form, primeira funcao controladora
    initInterfaceLoad(window.top.formName, 0X7);
    
    //@@ array bidimensional de combos ids e campo indice na tabela
    // identificador do combo. O array deve ser null se nao tem combos estaticos
    glb_aStaticCombos = ([['selRelPesContaID', '1'],
                          ['selTipoRegistroID', '2']]);

    windowOnLoad_1stPart();
        
    //@@ Path dos arquivos inf e pesqlist
    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modfinanceiro/subcobranca/cobranca/inferior.asp',
                              SYS_PAGESURLROOT + '/modfinanceiro/subcobranca/cobranca/pesquisa.asp');                          
        
    //@@ Linka os divs com os correspondentes subforms
    // criando o atributo sfsGrupo nos divs.
    // Os infs nao tem div principal e todos os divs sao secundarios.
    // Parametros:
    // 1o par: id do div principal ou null se nao tem div principal
    // 2opar: array de divs ids dos divs secundarios
    // ou null se nao tem divs secundarios
    // 3o par: array de xxx_ID que identifica o grupo contido no div
    // ou null se o form nao tem div secundarios.
    // Esta funcao tageia o div principal e preenche array
    // dos divs secundarios do arquivo guardando na variavel
    // global do arquivo: glb_aDIVS
    linkDivsAndSubForms('divSup01_01', null, null);
    
    windowOnLoad_2ndPart();
    
    //@@ Guarda o nome do campo na tabela que contem o id do registro corrente no sup
    glb_sFldIDName = 'CobrancaID';
    
    //@@ Guarda o id do controle (combo) que contem os tipos de registros
    // se nao e aplicavel no form, fazer glb_sCtlTipoRegistroID = '';
    glb_sCtlTipoRegistroID = 'selTipoRegistroID';

    //@@ Guarda o nome do campo na tabela que contem o tipo do registro corrente no sup
    // se nao e aplicavel no form, fazer glb_sFldTipoRegistroName = ''
    glb_sFldTipoRegistroName = 'TipoCobrancaID';
}

function setupPage()
{
    //@@ Ajusta os divs e seus elementos
    adjustDivs('sup',[[1,'divSup01_01']]);
    
    //@@ *** Div principal superior divSup01_01 ***/
    adjustElementsInForm([['lblRegistroID','txtRegistroID',10,1],
                          ['lblEstadoID','txtEstadoID',2,1],
                          ['lblRelPesContaID','selRelPesContaID',12,1],
                          ['lblTipoRegistroID','selTipoRegistroID',10,1],
                          ['lbldtGravacao','txtdtGravacao',10,1],
                          ['lblSequencial','txtSequencial',7,1],
                          ['lblArquivo','txtArquivo',12,1],
                          ['lblTitulos', 'txtTitulos', 4, 1],
                          ['lblLayout', 'txtLayout', 4, 1],
                          ['lblTotal','txtTotal',12,1,-5],
                          ['lblObservacao','txtObservacao',30,2]], null, null, true);
}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2)
{
    if (param1 == 'SHOWCOBRANCA')
    {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if ( __currFormState() != 0 )
            return null;
            
        // param2 - traz array:
            // o id da empresa do form que mandou
            // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();
        
        if ( param2[0] != empresa[0])
            return null;

        window.top.focus();
        
        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);
        
        return new Array(getHtmlId(), null, null);          
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2)
{

    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************

/********************************************************************
Funcao disparada pelo frame work.
Coloca atributos em campos para serem manobrados graficamente
(esconde/mostra e habilita desabilita) pelo framework.

Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function putSpecialAttributesInControls()
{
    //@@
    // trava o combo de tipo de pessoa

    // Tabela de atributos especiais:
    // 1. fnShowHide - seta funcao do programador que o framework executa
    //                 ao mostrar/esconder o controle.
    // 2. prog_SHHD  - (true) retira do frame work a operacao de mostrar/esconder
    //                 habiitar/desabilitar o controle e transfere para
    //                 o programador. False ou null devolve para o framework.
    
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares de servidor
do e preenchimentos de combos do sup, quando o usuario clica
um botao da barra do sup.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgServerSup(btnClicked)
{
    //@@
    
    
	// Mover esta funcao para os finais de retornos de operacoes no
	// banco de dados
	finalOfSupCascade(btnClicked);
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao tem que ser chamada na ultima (ou ultimas) funcoes de
retorno de dados do servidor, feitas pelo programador.
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function finalOfSupCascade(btnClicked)
{
    //@@
    

    // Nao mexer - Inicio de automacao ==============================
    // Invoca a funcao lockAndOrSvrInf_SYS() do detailinf.js
    // para as operacoes de bancos de dados da automacao.
	sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
	// Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao faz a chamada de todas as operacoes particulares
do form, necessarias apos um changeDiv. NAO PODE CONTER NENHUMA
OPERACAO DE BANCO DE DADOS.
           
Parametros: 
ultimo botao clicado na barra de botoes superior

Retorno:
nenhum
********************************************************************/
function prgInterfaceSup(btnClicked)
{
    //@@
    // Chama funcoes particulares do form que forem necessarias
    // depois de trocar o div

    setReadOnlyFields();

    // Nao mexer - Inicio de automacao ==============================
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Esta funcao recebe todos os onchange de combos do form e executa as
funcoes apropriadas definidas pelo programador e aqui invocadas. 
           
Parametros: 
nenhum

Retorno:
nenhum
********************************************************************/
function optChangedInCmb(cmb)
{
    var cmbID = cmb.id;
    
    //@@ Os if e os else if abaixo sao particulares de cada form
    // Troca a interface do sup em funcao de item selecionado
    // no combo de tipo.
    // Se for necessario ao programador, repetir o if abaixo
    // definido, antes deste comentario.
    // Nao mexer - Inicio de automacao ==============================
    if ( cmbID == 'selTipoRegistroID' )
        adjustSupInterface();
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara logo apos abrir a maquina de estado. 
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
nenhum
********************************************************************/
function stateMachOpened( currEstadoID )
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou OK na maquina de estado
           
Parametros: 
currEstadoID    - EstadoID do registro

Retorno:
false - automacao prossegue
true  - automacao para        
       
Nota:
Se o programador retornar diferente de null nesta funcao,
o sistema permanecera travado, ao final do seu codigo o programador
dever� chamar a seguinte funcao da automacao:
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function stateMachBtnOK( currEstadoID, newEstadoID )
{
    glb_localTimerInt = window.setInterval(
            'processBtnOKInStateMach(' + currEstadoID + ', ' + newEstadoID + ')',
            30, 'JavaScript');

    // @@ padrao do frame work e retornar false
    // return true para a automacao
    return true;
}

/********************************************************************
Funcao disparada pelo frame work.
Dispara se o usuario trocou o estado.
Dispara logo apos fechar a maquina de estado. 
           
Parametros: 
oldEstadoID     - previo EstadoID do registro
currEstadoID    - novo EstadoID do registro
bSavedID        - o novo estadoID foi gravado no banco (true/false)

Retorno:
nenhum
********************************************************************/
function stateMachClosed( oldEstadoID, currEstadoID, bSavedID )
{
    var currInfFolder = sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'keepCurrFolder()');
    
    if ( (currInfFolder == 28202) && (oldEstadoID == 64) && (currEstadoID == 66) )
        sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'__btn_REFR(' + '\'' + 'inf' + '\'' + ')');
}

/********************************************************************
Funcao disparada pelo frame work.
Propaga clique e botao de lupa que nao seja proprietario e
alternativo.
           
Parametros: 
btnClicked      - referencia ao botao de lupa clicado

Retorno:
nenhum
********************************************************************/
function btnLupaClicked(btnClicked)
{

}

/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou botao especifico da barra.
           
Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - 1, 2, 3, ou 4

Retorno:
nenhum
********************************************************************/
function btnBarClicked(controlBar, btnClicked)
{
	var contexto = 0;
	var sAnchor;
	contexto = getCmbCurrDataInControlBar('sup', 1);

    if ( controlBar == 'SUP' )
    {
    	// Documentos
    	if (btnClicked == 1) {
    		__openModalDocumentos(controlBar, dsoSup01.recordset.Fields[0].value);
    	}
    	// usuario clicou botao imprimir
        if ( btnClicked == 2 )
            openModalPrint();                
		// Procedimento
		else if ( btnClicked == 3 )
		{
			if (contexto[1] == 9211)
				sAnchor = '212111';
			else				
				sAnchor = '212112';

    		window.top.openModalControleDocumento('S', '', 910, null, sAnchor, 'T');
		}
    }
}
/********************************************************************
Funcao disparada pelo frame work.
Usuario clicou um botao nao especifico do control bar.

Parametros: 
controlBar      - 'SUP' ou 'INF'
btnClicked      - Um dos seguintes botoes:
                - Avancar, retroagir, incluir, alterar, estado, 
                  excluir, OK, cancelar refresh, anterior e proximo. 
                  (Testar a string que vem).

Retornos:
null            - a automacao prossegue. Qualquer outro retorno a
                  automacao nao prossegue.
********************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked)
{
    // Para prosseguir a automacao retornar null
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
Propagada por janela modal que abriu ou fechou.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function modalInformForm(idElement, param1, param2)
{
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML')
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    // Modal de impressao
    else if ( idElement.toUpperCase() == 'MODALPRINTHTML' )    
    {
        if ( param1 == 'OK' )                
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');
           
            // nao mexer
            return 0;
        }
        else if (param1 == 'CANCEL')
        {
            // esta funcao fecha a janela modal e destrava a interface
            restoreInterfaceFromModal();    
            // escreve na barra de status
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            // nao mexer
            return 0;
        }    
    }
    // Modal de documentos
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
    	if (param1 == 'OK') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    	else if (param1 == 'CANCEL') {
    		// esta funcao fecha a janela modal e destrava a interface
    		restoreInterfaceFromModal();
    		// escreve na barra de status
    		writeInStatusBar('child', 'cellMode', 'Detalhe');

    		// nao mexer
    		return 0;
    	}
    }
}

/********************************************************************
Funcao disparada pelo frame work.
Informa que o sup esta em modo de edicao e a interface ja esta
destravada.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function supInitEditMode()
{
    setReadOnlyFields();

    // chamada abaixo necessaria para prosseguir a automacao
    // mover para final de operacoes de servidor se for o caso
    supInitEditMode_Continue();
}

/********************************************************************
Funcao disparada pelo frame work.
Form carregou e vai mostrar a interface.

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function formFinishLoad()
{
    // seta botao de impressao
    //especBtnIsPrintBtn('sup', 1);

    // mostra quatro botoes especificos desabilitados
    showBtnsEspecControlBar('sup', true, [1,1,1,0]);
    // os hints dos botoes especificos
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Remessa']);
}

/********************************************************************
Funcao do programador
    Seta os campos read-only
           
Parametros: 
Nenhum

Retorno:
Nenhum
********************************************************************/
function setReadOnlyFields()
{
    // campos read-only
    txtTotal.disabled = true;
    txtTitulos.disabled = true;
    txtLayout.disabled = true;
    selRelPesContaID.disabled = true;
    selTipoRegistroID.disabled = true;
}

/********************************************************************
Funcao criada pelo programador.
Abre janela modal de impressao

Parametro:
nenhum

Retorno:
nenhum
********************************************************************/
function openModalPrint()
{
    var htmlPath;
    var strPars = new String();
    var empresaData = getCurrEmpresaData();
    var empresaID = empresaData[0];
    var empresaFantasia = empresaData[3];
    var contexto = getCmbCurrDataInControlBar('sup', 1);
    var userID = getCurrUserID();
    var nCobrancaID = dsoSup01.recordset['CobrancaID'].value;
    
    // mandar os parametros para o servidor
    // o primeiro parametro e obrigatorio
    
    // sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    strPars = '?sCaller=' + escape('S');

    // parametros opcionais
    strPars += '&nEmpresaID=' + escape(empresaID);
    strPars += '&sEmpresaFantasia=' + escape(empresaFantasia);       
    strPars += '&nContextoID=' + escape(contexto[1]);       
    strPars += '&nUserID=' + escape(userID);       
    strPars += '&nCobrancaID=' + escape(nCobrancaID);       

    // carregar modal - faz operacao de banco no carregamento
    htmlPath = SYS_ASPURLROOT + '/modfinanceiro/subcobranca/cobranca/modalpages/modalprint.asp'+strPars;

    showModalWin(htmlPath, new Array(346,234));
}

/********************************************************************
Funcao criada pelo programador.
Programador terminou de processar o clique do botao OK na maquina de estado
           
Parametros: 
nenhum

Retorno:
irrelevante
       
Nota:
Chama a funcao
stateMachSupExec(action)
onde action = 'OK' - o novo estado sera gravado
              'CANC' - a gravacao sera cancelada
para prosseguir a automacao
********************************************************************/
function processBtnOKInStateMach(currEstadoID, newEstadoID)
{
    if ( glb_localTimerInt != null )
    {
        window.clearInterval(glb_localTimerInt);
        glb_localTimerInt = null;
    }

    // gerar arquivo de remessa de cobranca
    if ((dsoSup01.recordset['TipoCobrancaID'].value == 1091) && ((currEstadoID == 61) && (newEstadoID == 62))) {

        var strParameters = 'nData=0&aLength=0&chkEmpresa=false';

        strParameters += '&iUser=' + glb_USERID + '&nCobrancaID=' + dsoSup01.recordset['CobrancaID'].value;

        //envia requisi��o aspx
        frameRemessa = document.getElementById("frmRemessa");
        frameRemessa.contentWindow.document.onreadystatechange = remessa_onreadystatechange;
        frameRemessa.contentWindow.location = SYS_PAGESURLROOT + '/modfinanceiro/subcobranca/cobranca/serverside/RemessaCobranca.aspx?' + strParameters;
    }

    // processar retornos de cobrancas
    // Estado Recebido->Processado
    else if ((dsoSup01.recordset['TipoCobrancaID'].value == 1092) && ((currEstadoID == 64) && (newEstadoID == 66)))
    {
        //Se der erro por algum motivo, n�o deixa avan�ar
        if (txtTitulos.value == "0") {
            alert("Arquivo com erro, verificar pasta de erros do banco!");
            stateMachSupExec('CANC');
        }
        else {
            verifyCobrancaInServer(currEstadoID, newEstadoID);
        }
        // processaArquivoRetorno();
    }
    // prossegue a automacao
    else 
        stateMachSupExec('OK');
}

function remessa_onreadystatechange() {
    if ((frameRemessa.contentWindow.document.readyState == 'loaded') ||
        (frameRemessa.contentWindow.document.readyState == 'interactive') ||
        (frameRemessa.contentWindow.document.readyState == 'complete')) {

        frameRemessa.contentWindow.document.onreadystatechange = null;
        frameRemessa.contentWindow.location = null;
        stateMachSupExec('OK');
    }
}

function processaArquivoRetorno()
{
    var strPars;
    strPars = '?nCobrancaID=' + escape(dsoSup01.recordset['CobrancaID'].value);
    
    dsoRetornoCobranca.URL = SYS_ASPURLROOT + '/modfinanceiro/subcobranca/cobranca/serverside/processaarqret.aspx' + strPars;
    dsoRetornoCobranca.ondatasetcomplete = processaArquivoRetorno_DSC;
    dsoRetornoCobranca.Refresh();
}

function processaArquivoRetorno_DSC()
{
    var sErrorMsg = dsoRetornoCobranca.recordset['fldError'].value;
    var folderChanged = false;
    
    if ( sErrorMsg != '')
    {
        stateMachSupExec('CANC');

        folderChanged = selOptByValueOfSelInControlBar('inf', 1, 28202);
                
        if (folderChanged)
        {
            sendJSMessage('PESQLIST_HTML', JS_DATAINFORM, EXECEVAL,
                          'fillCmbsFiltInf_DSC(' + 28202 + ')');
            
            var filterID = 41077;
            
            if ( selOptByValueOfSelInControlBar('inf', 2, filterID) )
            {
                sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'glb_NovaCobrancaRetorno = false');
                sendJSMessage('INF_HTML', JS_DATAINFORM, EXECEVAL,'__btn_REFR(' + '\'' + 'inf' + '\'' + ')');
            }
        }    
    }
    else
        stateMachSupExec('OK');
}

/********************************************************************
Verificacoes do Financeiro
********************************************************************/
function verifyCobrancaInServer(currEstadoID, newEstadoID)
{
    var nCobrancaID = dsoSup01.recordset['CobrancaID'].value;
    var strPars = new String();

    strPars = '?nCobrancaID=' + escape(nCobrancaID);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);
    strPars += '&nUsuarioID=' + escape(getCurrUserID());

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modfinanceiro/subcobranca/cobranca/serverside/verificacaocobranca.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyCobrancaInServer_DSC;
    dsoVerificacao.refresh();
}

/********************************************************************
Retorno do servidor - Verificacoes do Financeiro
********************************************************************/
function verifyCobrancaInServer_DSC()
{
    var sMensagem = (dsoVerificacao.recordset.Fields['Mensagem'].value == null ? '' : dsoVerificacao.recordset.Fields['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset.Fields['Resultado'].value == null ? 0 : dsoVerificacao.recordset.Fields['Resultado'].value);

    if (nResultado == 1)
    {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 1)
            processaArquivoRetorno();
        else 
            stateMachSupExec('CANC');
    }

    else if (nResultado == 2)
    {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        processaArquivoRetorno();
    }
    else
    {
        if (sMensagem != '')
        {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('CANC');
    }
}