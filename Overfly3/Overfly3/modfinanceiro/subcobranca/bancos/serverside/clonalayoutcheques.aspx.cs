using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modfinanceiro.subcobranca.bancosEx.serverside
{
	public partial class clonalayoutcheques : System.Web.UI.OverflyPage
	{
		private Integer bancoID;
		private Integer bancoCloneID;
		
		protected Integer nBancoID {
			set { bancoID = value != null ? value : new Integer(0); }
		}
		protected Integer nBancoCloneID {
			set { bancoCloneID = value != null ? value : new Integer(0); }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			// Roda a procedure sp_Banco_ClonaLayoutCheque.
			ProcedureParameters[] procparam = new ProcedureParameters[2];

			procparam[0] = new ProcedureParameters("@BancoID", SqlDbType.Int, bancoID.intValue());
			procparam[1] = new ProcedureParameters("@BancoCloneID", SqlDbType.Int, bancoCloneID.intValue());

			DataInterfaceObj.execNonQueryProcedure("sp_Banco_ClonaLayoutCheque", procparam);
			
			// Gera o resultado.
			WriteResultXML(DataInterfaceObj.getRemoteData("select null as fldResponse"));
		}
	}
}
