using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modfinanceiro.subcobranca.bancosEx.serverside
{
	public partial class fillcmbsmodal : System.Web.UI.OverflyPage
	{
		private Integer bancoID;
		private Integer folderID;

		protected Integer nBancoID
		{
			set { bancoID = value != null ? value : new Integer(0); }
		}
		protected Integer nFolderID
		{
			set { folderID = value != null ? value : new Integer(0); }
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
			string sql = "";
			
			// folderID == 28231 // Layout CNAB
			// folderID == 28232) // Comandos CNAB
			// folderID == 28233) // Layout de Cheques
			if(folderID.intValue() == 28231 || folderID.intValue() == 28232) {
				sql += "SELECT a.BancoID as fldID, a.Banco as fldName " +
						 "FROM Bancos a " +
						 "WHERE a.BancoID IN (SELECT BancoID FROM Bancos_Layout) AND a.BancoID <> " + bancoID;
			} else if(folderID.intValue() == 28233) {
				sql += "SELECT a.BancoID as fldID, a.Banco as fldName " +
						 "FROM Bancos a " +
						 "WHERE a.BancoID IN (SELECT BancoID FROM Bancos_Cheques) AND a.BancoID <> " + bancoID;
			}
			
			WriteResultXML(DataInterfaceObj.getRemoteData(sql));
		}
	}
}
