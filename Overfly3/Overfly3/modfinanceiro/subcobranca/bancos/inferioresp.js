/********************************************************************
especificinf.js

Library javascript de funcoes para abertura de objetos rds dos infs e
preenchimento do combo1 de pastas do control bar inferior.
Especifica para o form de Tipos de Relacoes
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var glb_nFormID = window.top.formID;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

/********************************************************************

INDICE DAS FUNCOES:

    setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
    setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
    fillCmbPastasInCtlBarInf(nTipoRegistroID)    
    criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
    constructGridBody(folderID, currDSO)    

********************************************************************/

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe,
vai ao banco de dados no servidor e chama a funcao de ondatasetcomplete de acordo 
com o parametro recebido.
           
Parametros: 
dso                        -> dso a ser aberto
folderID                   -> id da pasta corrente no control bar inferior
idToFind                   -> id do registro corrente no sup
sFiltro                    -> string do filtro corrente no control bar inferior
nTipoRegistroID            -> tipo do registro corrente
Retorno:
nulo
********************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID)
{
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    //@@
    if ( (folderID == 20008) || (folderID == 20009) ) // Prop/Altern ou Observacoes
    {
        dso.SQL = 'SELECT a.BancoID,a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Bancos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.BancoID = '+idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) // LOG
    {
        dso.SQL = 'SELECT *, dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso ' + 
                  'FROM LOGs a WITH(NOLOCK) ' + 
                  'WHERE ' + sFiltro + ' a.RegistroID = '+ idToFind + ' ' +
                  'AND a.FormID=' + glb_nFormID + ' ' +
                  'ORDER BY a.SubFormID ASC, a.Data DESC';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28231) // Layout CNAB
    {
        dso.SQL = 'SELECT a.* FROM Bancos_Layout a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.BancoID = ' + idToFind + ' ' +
		'ORDER BY a.Layout, a.Retorno, a.TipoRegistro, a.Posicao ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28232) // Comandos CNAB
    {
        dso.SQL = 'SELECT a.* FROM Bancos_Comandos a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.BancoID = ' + idToFind + ' ' +
        'ORDER BY a.ComandoID, a.Comando';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28233) // Layout de Cheques
    {
        dso.SQL = 'SELECT a.* FROM Bancos_Cheques a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.BancoID = ' + idToFind + ' ' +
        'ORDER BY a.Linha, a.Coluna ';
        return 'dso01Grid_DSC';
    }
    else if (folderID == 28235) // Layout de Cobrancas Parametros
    {
        dso.SQL = 'SELECT a.* FROM Bancos_LayoutParametros a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.BancoID = ' + idToFind + ' ' +
        'ORDER BY a.BanLayoutParamID ';
        return 'dso01Grid_DSC';
    }        
    else if (folderID == 28234) // Modalidades de cobranca
    {
        dso.SQL = 'SELECT a.* FROM Bancos_Modalidades a WITH(NOLOCK) WHERE ' +
        sFiltro + 'a.BancoID = ' + idToFind + ' ' +
        'ORDER BY a.ModalidadeID ';
        return 'dso01Grid_DSC';
    }
}

/********************************************************************
Monta string de select para combo dentro de grid

Parametros:
dso                 - referencia ao dso corrente
pastaID             - pasta corrente
nRegistroID         - id do registro corrente
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID)
{
    setConnection(dso);
       
    // LOG
    if ( pastaID == 20010 )
    {
        if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if ( dso.id == 'dso02GridLkp' )
        {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                      'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if ( dso.id == 'dso03GridLkp' )
        {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if ( dso.id == 'dso04GridLkp' )
        {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                      'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                      'WHERE (a.FormID=' + glb_nFormID + ' ' +
                      'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    // Layout CNAB
    else if ( pastaID == 28231 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT 1 AS fldIndex, 0 AS fldID, ' + '\'' + 'Header' + '\'' + ' AS fldName ' +
                      'UNION ALL ' +
                      'SELECT 2 AS fldIndex, 1 AS fldID, ' + '\'' + 'Detalhe' + '\'' + ' AS fldName ' +
                      'UNION ALL ' +
                      'SELECT 3 AS fldIndex, 2 AS fldID, ' + '\'' + 'Trailer' + '\'' + ' AS fldName ' +
                      'ORDER BY fldIndex';
        }
    }
    // Comandos CNAB
    else if ( pastaID == 28232 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE TipoID = 810 ' +
                      'ORDER BY ItemMasculino';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT b.ComandoID, ' +
            	      'Tipo = CASE WHEN a.Filtro LIKE ' + '\'' + '%(1091)%' + '\'' + ' THEN ' + '\'' + 'Remessa' + '\'' + ' ' + 
            	      'WHEN a.Filtro LIKE ' + '\'' + '%(1092)%' + '\'' + ' THEN ' + '\'' + 'Retorno' + '\'' + ' ' +
            		  'ELSE SPACE(0) END ' +
                      'FROM TiposAuxiliares_Itens a WITH(NOLOCK), Bancos_Comandos b WITH(NOLOCK) ' +
                      'WHERE (b.ComandoID = a.ItemID AND b.BancoID =' + nRegistroID + ') ';
        }
        else if ( dso.id == 'dsoStateMachineLkp' )
        {
            dso.SQL = 'SELECT a.RecursoID, a.RecursoAbreviado AS Estado ' +
                      'FROM Recursos a WITH(NOLOCK), Bancos_Comandos b WITH(NOLOCK) ' +
                      'WHERE b.EstadoID = a.RecursoID AND b.BancoID =' + nRegistroID;
        }
    }
    // Layout de Cheques
    else if ( pastaID == 28233 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID = 808 ' +
                      'ORDER BY Ordem';
        }
	}    
    // Modalidades de cobranca
    else if ( pastaID == 28234 )
    {
        if ( dso.id == 'dsoCmb01Grid_01' )
        {
            dso.SQL = 'SELECT ItemID, ItemAbreviado ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID = 813 ' +
                      'ORDER BY Ordem';
        }
        else if ( dso.id == 'dso01GridLkp' )
        {
            dso.SQL = 'SELECT ItemID,ItemMasculino ' +
                      'FROM TiposAuxiliares_Itens WITH(NOLOCK) ' +
                      'WHERE EstadoID=2 AND TipoID = 813 ' +
                      'ORDER BY Ordem';
        }
        else if ( dso.id == 'dsoStateMachineLkp' )
        {
            dso.SQL = 'SELECT DISTINCT a.RecursoID, a.RecursoAbreviado AS Estado ' +
                      'FROM Recursos a WITH(NOLOCK), Bancos_Modalidades b WITH(NOLOCK) ' +
                      'WHERE b.EstadoID = a.RecursoID AND b.BancoID =' + nRegistroID;
        }
	}    
}

/********************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior

Parametros:
nTipoRegistroID     - tipo do registro corrente

Retorno:
nenhum
********************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID)
{
    var i=0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

	cleanupSelInControlBar('inf',1);

	vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20008); //Observa��es

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20009); //Proprietario

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 20010); //LOG

    vPastaName = window.top.getPastaName(28233);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28233); // Layout Cheques

    vPastaName = window.top.getPastaName(28235);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28235); // Layout de Cobranca Parametros
		
    vPastaName = window.top.getPastaName(28231);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28231); // Layout CNAB

    vPastaName = window.top.getPastaName(28232);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28232); // Comandos CNAB
 	
    vPastaName = window.top.getPastaName(28234);
    if (vPastaName != '')
		addOptionToSelInControlBar('inf', 1, vPastaName, 28234); // Modalidades de Cobranca

    // Nao mexer - Inicio de automacao ============================== 	
	// Tenta selecionar novamente a pasta anterior selecionada no combo
	// Se nao mais existe a pasta anteriormente selecionada no combo
	// seleciona a pasta default
	
	stripFoldersByRight(currSelection);
        
    return true;
    // Final de Nao mexer - Inicio de automacao =====================
}

/********************************************************************
Funcao disparada pelo frame work.
Tratar consistencia dos dados inseridos pelo usuario aqui.
           
Parametros: 
btnClicked      - controlbar e botao clicado pelo usuario
folderID        - id da pasta selecionada
tipoCtlrsPasta  - 'GRID' a pasta e um grid/ 'CTRLS' a pasta nao e
                  um grid
currDSO         - referencia dso corrente
registroID      - id do registro corrente no sup

Retorno:
true se os dados estao ok, caso contrario false.
********************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID)
{
    //@@ return true se tudo ok, caso contrario return false;
 
    var currLine = 0;
    
    // A pasta e grid    
    if (tipoCtlrsPasta == 'GRID') // se � grid
    {
        if (folderID == 28231) // Layout CNAB
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[2,4,5,6], ['BancoID', registroID]);
        else if (folderID == 28232) // Comandos CNAB 
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[3,4], ['BancoID', registroID]);
        else if (folderID == 28233) // Layout Cheque 
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1,2,3,4,5,6], ['BancoID', registroID]);
        else if (folderID == 28234) // Comandos CNAB 
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0], ['BancoID', registroID]);
        else if (folderID == 28235) // Layout de Cobrancas Parametros
            currLine = criticLineGridAndFillDsoEx(fg, currDSO,keepCurrLineInGrid(),[0,1], ['BancoID', registroID]);

        if (currLine < 0)
            return false;
    }   
    
    return true;
}

/********************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work
           
Parametros: 
folderID        - id da pasta corrente
currDSO         - referencia ao dso corrente

Retorno:
nenhum
********************************************************************/
function constructGridBody(folderID, currDSO)
{
    var dTFormat = '';
    
    if ( DATE_FORMAT == "DD/MM/YYYY" )
        dTFormat = 'dd/mm/yyyy';
    else if ( DATE_FORMAT == "MM/DD/YYYY" )
        dTFormat = 'mm/dd/yyyy';
    
    fg.FrozenCols = 0;
    
    if (folderID == 20010) // LOG
    {
        headerGrid(fg,['SubForm',
                       'Data           Hora     ',
                       'Colaborador',
                       'Evento',
                       'Est',
                       'Motivo',
                       'LOGID'], [6]);
                       
        fillGridMask(fg,currDSO,['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
                                 'DataFuso',
                                 '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
                                 '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
                                 '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
                                 'Motivo',
                                 'LOGID'],
                                 ['','99/99/9999','','','','',''],
                                 ['',dTFormat + ' hh:mm:ss','','','','','']);

        // Merge de Colunas
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    // Layout CNAB
    else if (folderID == 28231)
    {
        headerGrid(fg, ['Layout',
                       'Retorno',
                       'TipoRegistro',
                       'Constante',
                       'Campo',
                       'Posi��o',
                       'Tamanho',
                       'Decimal/Replicar',
                       'Conteudo',
                       'Express�o',
                       'BanLayoutID'], [10]);

        fillGridMask(fg, currDSO, ['Layout',
                                 'Retorno',
                                 'TipoRegistro',
                                 'Constante',
                                 'Campo',
                                 'Posicao',
                                 'Tamanho',
                                 'DecimalReplicar',
                                 'Conteudo',
                                 'Expressao',
                                 'BanLayoutID']
                                 ,['999','','','','','999','999','','','','']);
        
        alignColsInGrid(fg,[]);                           

    }    
    // Comandos CNAB
    else if (folderID == 28232)
    {
        headerGrid(fg, ['Tipo',
                       'ID',
                       'Est',
                       'Comando',
                       'C�digo',
                       'Observa��o',
                       'BanComandoID'], [6]);

        fillGridMask(fg, currDSO, ['^ComandoID^dso01GridLkp^ComandoID^Tipo*',
                                 '^ComandoID^dso01GridLkp^ComandoID^ComandoID*',
                                 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*', 
                                 'ComandoID',
                                 'Comando',
                                 'Observacao',
                                 'BanComandoID']
                                 ,['','','','','','','']);

        alignColsInGrid(fg,[1,4]);                           
        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }    
    // Layout Cheque
    else if (folderID == 28233)
    {
        headerGrid(fg,['Campo',
                       'Linha',
                       'Coluna',
                       'Tamanho',
                       'Tamanho da Fonte',
                       'Negrito',
                       'Caixa Alta',
                       'BanComandoID'], [7]);

        fillGridMask(fg,currDSO,['TipoCampoID',
								 'Linha',
                                 'Coluna',
                                 'Tamanho',
                                 'TamanhoFonte',
                                 'Negrito',
                                 'CaixaAlta',
                                 'BanChequeID']
                                 ,['','99','999','999','99', '','','']);
        with (fg) {
            ColDataType(6) = 11;
            ColDataType(5) = 11;
        }        
        
        alignColsInGrid(fg,[1,2,3,4,5,6]);
        fg.MergeCells = 6;
        fg.MergeCol(-1) = true;
    }
    // Layout de Cobranca Parametros
    else if (folderID == 28235)
    {
        headerGrid(fg,['Layout',
                       'Linha Iden Ag�ncia Conta',
                       'Linha Iden Layout',
                       'Coluna Iden Layout',
                       'Express�o Iden Layout',
                       'BanLayoutParmID'], [5]);
                       
        glb_aCelHint = [[0,0,'Tipo do Layout'],
                        [0,1,'Linha de identifica��o da ag�ncia e conta'],
                        [0,2,'Linha de identifica��o do tipo do layout'],
                        [0,3,'Coluna de identifica��o do tipo do layout'],
                        [0,4,'Express�o de identifica��o do tipo do layout']];                       

        fillGridMask(fg,currDSO,['Layout',
								 'LinhaIdentificacaoAgenciaConta',
                                 'LinhaIdentificacaoLayout',
                                 'ColunaIdentificacaoLayout',
                                 'ExpressaoIdentificacaoLayout',
                                 'BanLayoutParamID']
                                 ,['999','999','999','999','','']);

        alignColsInGrid(fg,[0,1,2,3]);
        //fg.MergeCells = 4;
        //fg.MergeCol(-1) = true;
    }
    // Modalidades de Cobranca
    else if (folderID == 28234)
    {
        headerGrid(fg,['ID',
                       'Est',
                       'Modalidade',
                       'C�digo',
                       'Observa��o',
                       'BanModalidadeID'], [5]);

        fillGridMask(fg,currDSO,['ModalidadeID',
								 '^EstadoID^dsoStateMachineLkp^RecursoID^Estado*', 
                                 '^ModalidadeID^dso01GridLkp^ItemID^ItemMasculino*',
                                 'Modalidade',
                                 'Observacao',
                                 'BanModalidadeID']
                                 ,['','','','','','']);

        //alignColsInGrid(fg,[1,2,3]);
        //fg.MergeCells = 4;
        //fg.MergeCol(-1) = true;
    }
}
