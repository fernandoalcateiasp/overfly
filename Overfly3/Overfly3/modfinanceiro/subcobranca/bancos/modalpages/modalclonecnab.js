/********************************************************************
modalclonecnab.js

Library javascript para o modalclonecnab.js
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
// Dados dos combos estaticos .URL
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
// FINAL DE VARIAVEIS GLOBAIS *************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload()
{
    window_onload_1stPart();
    
    // parametrizacao dos atributos especiais desta modal
    setModalAttributes();
        
    // ajusta o body do html
    with (modalclonecnabBody)
    {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }
    
    setupPage();
        
	// inicia o carregamento de combos desta modal
	startLocalCmbs();
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage()
{
    // Caption da janela modal
    secText('Clonar Layout', 1);
        
	var tempLeft = ELEM_GAP;
    var elem;
    
    // Ajusta o divPesquisa
    with (divPesquisa.style)
    {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 24;
        width = 410;
        height = 102;
    }
    
    adjustElementsInForm([['lblBancoID','selBancoID',23,1]],null,null,true);
    
    // reajusta label e combo selBancoID
    with (lblBancoID.style)
    {
        left = parseInt(btnOK.currentStyle.left);
    }    
    with (selBancoID.style)
    {
        left = parseInt(btnOK.currentStyle.left);
        width = ( parseInt(btnCanc.currentStyle.left) +
                  parseInt(btnCanc.currentStyle.width) -
                  parseInt(btnOK.currentStyle.left) );
    }
    
    //por padrao entra desabilitado
    selBancoID.disabled = true;
}

/********************************************************************
Seleciona conteudo de um controle quando o mesmo recebe foco
********************************************************************/
function selTextInControl()
{
    this.select();
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl)
{
   lockControlsInModalWin(true);

    if ( ctl.id == 'btnOK' )
    {
        sendJSMessage( getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_' + glb_sCaller, null);
    }
    else if ( ctl.id == 'btnCanc' )
    {
        sendJSMessage( getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null );
    }    

}
    
/********************************************************************
Esta funcao faz a parametrizacao dos atributos especiais desta modal

Parametros:
nenhum

Retornos:
nenhum
********************************************************************/
function setModalAttributes()
{
}

function startLocalCmbs()
{
    var strPas = new String();
    strPas = '?nBancoID='+escape(glb_nBancoID);
    strPas += '&nFolderID='+escape(glb_nFolderID);
    
    dsoEstaticCmbs.URL = SYS_ASPURLROOT + '/modfinanceiro/subcobranca/bancos/serverside/fillcmbsmodal.asp'+strPas;
    dsoEstaticCmbs.ondatasetcomplete = dsoEstaticCmbs_DSC;
    dsoEstaticCmbs.Refresh();
}

function dsoEstaticCmbs_DSC() {
    var optionStr, optionValue;
    var oldDataSrc;
    var oldDataFld;
    var aCmbsDynamics = [selBancoID];
    var nIndice = 0;
    
    clearComboEx(['selBancoID']);
    
    if (!((dsoEstaticCmbs.recordset.BOF)&&(dsoEstaticCmbs.recordset.EOF)))
        dsoEstaticCmbs.recordset.MoveFirst();
    else
    {
        // mostra a janela modal com o arquivo carregado
        showExtFrame(window, true);
    
	    return null;
    }    

    while (!dsoEstaticCmbs.recordset.EOF )
    {
        optionStr = dsoEstaticCmbs.recordset['fldName'].value;
		optionValue = dsoEstaticCmbs.recordset['fldID'].value;

        var oOption = document.createElement("OPTION");
        oOption.text = optionStr;
        oOption.value = optionValue;
        aCmbsDynamics[nIndice].add(oOption);

        dsoEstaticCmbs.recordset.MoveNext();
    }

    if ( selBancoID.options.length > 0 )
        selBancoID.disabled = false;
    else
        selBancoID.disabled = true;    

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
    
  	// coloca foco
	selBancoID.focus();

	return null;
}
