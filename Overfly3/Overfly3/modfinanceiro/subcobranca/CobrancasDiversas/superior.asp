<%@  language="VBSCRIPT" enablesessionstate="False" %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<!-- //@@ ID e name do arquivo -->
<html id="cobrancasup01Html" name="cobrancasup01Html">
<head>
    <title></title>

    <%
		'Links de estilo, bibliotecas da automacao e especificas
		Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
	    
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
	    
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcobranca/CobrancasDiversas/superior.js" & Chr(34) & "></script>" & vbCrLf
		Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcobranca/CobrancasDiversas/superioresp.js" & Chr(34) & "></script>" & vbCrLf
    %>


    <script id="wndJSProc" language="javascript">
	<!--

    //-->
    </script>
</head>

<!-- //@@ -->
<body id="cobrancasup01Body" name="cobrancasup01Body" language="javascript" onload="return window_onload()">

    <!-- Controle para simular tab na gravacao -->
    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">

    <!-- //@@ Os divs sao definidos de acordo com o form -->
    <!-- //@@ Todos os controles select cujo onchange e invocado convergem para -->
    <!-- a funcao optChangedInCmb(cmb), que esta no sup01.js" -->

    <!-- Div principal superior -->
    <div id="divSup01_01" name="divSup01_01" class="divExtern">
        <!-- Os dois controles RegistroID e EstadoID abaixo sao comuns a todos os forms -->

        <p class="lblGeneral" id="lblRegistroID">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">

        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" datasrc="#dsoStateMachine" datafld="Estado" name="txtEstadoID" class="fldGeneral">

        <p id="lblSelPrestadorID" name="lblSelPrestadorID" class="lblGeneral">Prestador</p>
        <select id="selPrestadorID" name="selPrestadorID" class="fldGeneral" datasrc="#dsoSup01" datafld="PrestadorID"></select>

        <p id="lbldtGravacao" name="lbldtGravacao" class="lblGeneral">Data</p>
        <input type="text" id="txtdtGravacao" name="txtdtGravacao" datasrc="#dsoSup01" datafld="V_dtGravacao" class="fldGeneral">

        <p id="lblNomeArquivo" name="lblNomeArquivo" class="lblGeneral">Nome Arquivo</p>
        <input type="text" id="txtNomeArquivo" name="txtNomeArquivo" datasrc="#dsoSup01" datafld="NomeArquivo" class="fldGeneral">

        <p id="lblRepasseIR" name="lblRepasseIR" class="lblGeneral">Repasse IR</p>
        <input type="text" id="txtRepasseIR" name="txtRepasseIR" datasrc="#dsoSup01" datafld="RepasseIR" class="fldGeneral">

        <p id="lblValorCreditadoTotal" name="lblValorCreditadoTotal" class="lblGeneral">Valor Creditado</p>
        <input type="text" id="txtValorCreditadoTotal" name="txtValorCreditadoTotal" datasrc="#dsoSup01" datafld="ValorCreditadoTotal" class="fldGeneral">

        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observação</p>
        <input type="text" id="txtObservacao" name="txtObservacao" datasrc="#dsoSup01" datafld="Observacao" class="fldGeneral">
    </div>
</body>
</html>
