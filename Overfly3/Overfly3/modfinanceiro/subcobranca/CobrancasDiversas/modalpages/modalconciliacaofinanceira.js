/********************************************************************
modalconciliacaofinanceira.js

Library javascript para o modalconciliacaofinanceira.asp
********************************************************************/

// VARIAVEIS GLOBAIS ************************************************
var dsoCiclos = new CDatatransport("dsoCiclos");
var dsoValoresLocalizar = new CDatatransport("dsoValoresLocalizar");
var dsoCobrancasDetalhes = new CDatatransport("dsoCobrancasDetalhes");
var dsoGravacao = new CDatatransport("dsoGravacao");

var glb_aSendDataToServer = new Array();
var glb_nPointToaSendDataToServer = 0;
var glb_nTimerSendDataToServer = null;
var glb_nMaxStringSize = 1024;

// FINAL DE VARIAVEIS GLOBAIS ***************************************

// IMPLEMENTACAO DAS FUNCOES

/********************************************************************
Configura o html
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    // configuracao inicial do html
    setupPage();

    // ajusta o body do html
    var elem = document.getElementById('modalconciliacaofinanceiraHtmlBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    modalFrame = getFrameInHtmlTop('frameModal');
    modalFrame.style.top = 27;
    //modalFrame.style.left = 0;

    // mostra a janela modal com o arquivo carregado
    showExtFrame(window, true);
}

/********************************************************************
Configuracao inicial do html
********************************************************************/
function setupPage() {
    // texto da secao01
    secText('Concilia��o Financeira', 1);

    var modalFrame = getFrameInHtmlTop('frameModal');

    // ajusta elementos da janela
    var elem;

    // Label Servi�o
    elem = window.document.getElementById('lblServico');
    with (elem.style) {
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height) + ELEM_GAP;
    }

    // Combo Servi�o
    elem = window.document.getElementById('selServico');
    with (elem.style) {
        left = document.getElementById('lblServico').offsetLeft;
        top = document.getElementById('lblServico').offsetTop + document.getElementById('lblServico').offsetHeight;
        width = 100;
    }

    // Div de Controles do servi�o Concilia��o
    elem = window.document.getElementById('divControlesConciliacao');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = document.getElementById('selServico').offsetLeft + document.getElementById('selServico').offsetWidth + ELEM_GAP;
        top = document.getElementById('lblServico').offsetTop;
        width = parseInt(modalFrame.style.width) - document.getElementById('selServico').offsetWidth - (ELEM_GAP * 2) - 14;
        height = 40;
    }

    // Ajusta elementos do divControlesConcilia��o
    adjustElementsInForm([['lblProcesso', 'selProcesso', 17, 1, -10, -7],
                          ['lblEmpresa', 'selEmpresa', 17, 1],
                          ['lblDataInicio', 'txtDataInicio', 11, 1],
                          ['lblDataFim', 'txtDataFim', 11, 1],
                          ['lblSaldo', 'txtSaldo', 12, 1]],
                          null, null, true);

    txtDataInicio.maxLength = 10;
    txtDataInicio.onkeypress = verifyDateTimeNotLinked;
    txtDataFim.maxLength = 10;
    txtDataFim.onkeypress = verifyDateTimeNotLinked;

    txtSaldo.readOnly = true;

    var buttonsHeight = 24;
    var buttonsWidth = 80;

    // Posicionamento btnListar - alinhado � direita
    elem = window.document.getElementById('btnListar');
    with (elem.style) {
        height = buttonsHeight;
        width = buttonsWidth;
        left = document.getElementById('divControlesConciliacao').offsetWidth - buttonsWidth;
        top = document.getElementById('txtDataFim').offsetTop;
    }

    // Posicionamento btnAssociar
    elem = window.document.getElementById('btnAssociar');
    with (elem.style) {
        height = buttonsHeight;
        width = buttonsWidth;
        left = document.getElementById('btnListar').offsetLeft - buttonsWidth - (ELEM_GAP / 2);
        top = document.getElementById('txtDataFim').offsetTop;
    }
    /*
    // Posicionamento btnDissociar
    elem = window.document.getElementById('btnDissociar');
    with (elem.style) {
        height = buttonsHeight;
        width = buttonsWidth;
        left = document.getElementById('btnAssociar').offsetLeft - buttonsWidth - (ELEM_GAP / 2);
        top = document.getElementById('txtDataFim').offsetTop;
    }
    */
    // Div dos grids
    elem = window.document.getElementById('divGrids');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = document.getElementById('selServico').offsetLeft;
        top = document.getElementById('selServico').offsetTop + document.getElementById('selServico').offsetHeight + ELEM_GAP;
        width = parseInt(modalFrame.style.width) - ELEM_GAP - 14;
        height = 474;
    }

    // ajusta o divFG
    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = 0;
        width = document.getElementById('divGrids').offsetWidth;
        height = 150;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }

    startGridInterface(fg);
    fg.Cols = 0;
    fg.Cols = 1;
    fg.ColWidth(0) = parseInt(divFG.currentStyle.width, 10) * 18;
    fg.Redraw = 2;

    // ajusta o divFG2
    elem = window.document.getElementById('divFG2');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = 0;
        top = document.getElementById('divFG').offsetTop + document.getElementById('divFG').offsetHeight + ELEM_GAP;
        width = document.getElementById('divGrids').offsetWidth;
        height = document.getElementById('divGrids').offsetHeight - document.getElementById('divFG').offsetHeight - ELEM_GAP;
    }

    elem = document.getElementById('fg2');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG2').style.width);
        height = parseInt(document.getElementById('divFG2').style.height);
    }

    startGridInterface(fg2);
    fg2.Cols = 0;
    fg2.Cols = 1;
    fg2.ColWidth(0) = parseInt(divFG2.currentStyle.width, 10) * 18;
    fg2.Redraw = 2;

    // Oculta bot�es OK e Cancelar
    btnOK.style.visibility = 'hidden';
    btnCanc.style.visibility = 'hidden';

    // Carrega ciclos de apura��o dos marketplaces
    lockControlsInModalWin(true);

    setConnection(dsoCiclos);

    dsoCiclos.URL = SYS_ASPURLROOT + '/modfinanceiro/subcobranca/CobrancasDiversas/serverside/ciclosfinanceiros.aspx';
    dsoCiclos.ondatasetcomplete = dsoCiclos_DSC;
    dsoCiclos.refresh();

    //selProcesso.selectedIndex = 3;
    //selEmpresa.selectedIndex = 1;
    //txtDataInicio.value = '26/10/2016';
    //txtDataFim.value = '30/11/2016';
    //listarValoresLocalizar();
}

function dsoCiclos_DSC() {
    lockControlsInModalWin(false);
}

/********************************************************************
Clique botao OK ou Cancela
********************************************************************/
function btn_onclick(ctl) {
    // ctl.id retorna o id do botao clicado

    if (ctl.id == btnListar.id)
        listarValoresLocalizar();
    else if (ctl.id == btnAssociar.id)
        associarFinanceiros();
    /*else if (ctl.id == btnDissociar.id) {
        var estadoVL = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Estado*'))

        if (estadoVL == "F")
            estornaVL();
        else
            dissociarFinanceiros();
    }*/
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_' + glb_sCaller, null);
}

/********************************************************************
Carrega grid de Valores a localizar
********************************************************************/
function listarValoresLocalizar() {

    lockControlsInModalWin(true);

    var empresaID = selEmpresa.value;
    var pessoaID = selProcesso.value;

    // Tempor�rio: Pessoa Juridica da Cnova foi alterada durante o processo.
    if (pessoaID == '312097') {
        pessoaID = '312097 OR a.PessoaID = 312096';
    }
    else if (pessoaID == '67788') {
        pessoaID = '67788 OR a.PessoaID = 238356';
    }
    else if (pessoaID == '71002') {
        pessoaID = '71002 OR a.PessoaID = 360341';
    }

    setConnection(dsoValoresLocalizar);

    dsoValoresLocalizar.SQL = 'SELECT ValorID, d.RecursoAbreviado AS Estado, b.Fantasia AS Empresa, c.Fantasia AS Pessoa, a.dtEmissao, a.dtApropriacao, a.Identificador, ' +
                                    'ISNULL(dbo.fn_ValorLocalizar_Posicao(a.ValorID, 5), 0) AS SaldoFinanceiro, a.Valor, CONVERT(BIT, 0) AS OK ' +
                                'FROM ValoresLocalizar a WITH(NOLOCK) ' +
                                    'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.EmpresaID) ' +
                                    'INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.PessoaID) ' +
                                    'INNER JOIN Recursos d WITH(NOLOCK) ON (d.RecursoID = a.EstadoID) ' +
                                'WHERE ((a.EstadoID IN (1, 41)) AND (a.PessoaID = ' + pessoaID + ') /*AND (a.EmpresaID = ' + empresaID + ')*/) ' +
                                'ORDER BY a.dtEmissao';

    dsoValoresLocalizar.ondatasetcomplete = dsoValoresLocalizar_DSC;
    dsoValoresLocalizar.refresh();
}

function dsoValoresLocalizar_DSC()
{
    startGridInterface(fg);

    glb_GridIsBuilding = true;

    fg.Editable = true;

    headerGrid(fg, ['ID',
                    'Est',
                    'Empresa',
                    'Pessoa',
                    'Emiss�o',
                    'Apropria��o',
                    'Identificador',
                    'Saldo',
                    'Valor',
                    'OK'], []);

    fillGridMask(fg, dsoValoresLocalizar, ['ValorID*',
                                'Estado*',
                                'Empresa*',
                                'Pessoa*',
                                'dtEmissao*',
                                'dtApropriacao*',
                                'Identificador*',
                                'SaldoFinanceiro*',
                                'Valor*',
                                'OK'], 
                                ['', '', '', '', '', '', '', '999999999999.99', '999999999999.99', ''],
                                ['', '', '', '', '', '', '', '###,###,###,###.00', '###,###,###,###.00', '']);

    alignColsInGrid(fg, [7, 8]);

    //fg.FontSize = '8';
    fg.AutoSizeMode = 0;
    fg.AutoSize(0, fg.Cols - 1);
    fg.Redraw = 2;

    glb_GridIsBuilding = false;

    fg2.Rows = 1;

    lockControlsInModalWin(false);
}

/********************************************************************
Carrega grid de Cobran�as Diversas
********************************************************************/
function listarCobrancasDiversas() {

    lockControlsInModalWin(true);

    var dtInicio = putDateInMMDDYYYY(txtDataInicio.value);
    var dtFim = putDateInMMDDYYYY(txtDataFim.value);

    if ((dtInicio == "") || (dtFim == "")) {
        dtInicio = "NULL";
        dtFim = "NULL";

        //if (window.top.overflyGen.Alert('Datas n�o preenchidas') == 0)
        //    return null;

        //lockControlsInModalWin(false);
        //return;
    }
    else {
        dtInicio = "'" + dtInicio + "'";
        dtFim = "'" + dtFim + " 23:59:59'";
    }   
        

    var parceiroID = selProcesso.value;
    var programaMarketingID = selProcesso.options[selProcesso.selectedIndex].getAttribute("ProgramaMarketingID", 1);
    var empresaID = selEmpresa.value;
    var valorID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ValorID*'));

    if (parceiroID == "67788") {
        parceiroID = "67788, 238356";
    }
    else if (parceiroID == "312097") {
        parceiroID = "312097,312096";
    }

    setConnection(dsoCobrancasDetalhes);

    dsoCobrancasDetalhes.SQL =
        "SELECT  b.CobrancaDiversaID, b.CobDetalheID, b.TipoOcorrenciaID, e.ItemMasculino AS Ocorrencia, " +
		        "b.Estorno, b.dtOcorrencia, b.dtCiclo, b.PedidoPrestador, b.Processado, c.PedidoID, d.FinanceiroID, " +
                "CONVERT(NUMERIC(11,2), ((CASE WHEN b.TipoOcorrenciaID = 1440 AND a.PrestadorID = 348658 " +
									        "THEN b.Valor + (SELECT aa.Valor " +
														        "FROM CobrancaDiversas_Detalhes aa WITH(NOLOCK) " +
														        "WHERE ((aa.Estorno = b.Estorno) AND ((aa.CobDetalheReferenciaID = b.CobDetalheID) " +
                                                                    "OR ((aa.PedidoPrestador = b.PedidoPrestador) AND (a.PrestadorID = 355248))) AND (aa.TipoOcorrenciaID = 1441))) " +
									        "ELSE b.Valor END) * (CASE WHEN b.TipoOcorrenciaID = 1442 THEN 1 ELSE e.Numero END) * (CASE WHEN Estorno = 0 THEN 1 ELSE -1 END))) AS Valor, " +
		        "f.ItemAbreviado AS FormaPagamento, b.Loja, b.ValorID, CONVERT(BIT, 0) AS OK, " +
		        "(CASE WHEN b.TipoOcorrenciaID = 1446 THEN 1 ELSE 0 END) AS PedidoPrestadorOrdem " +
            "FROM CobrancaDiversas a WITH(NOLOCK) " +
                "INNER JOIN CobrancaDiversas_Detalhes b WITH(NOLOCK) ON (b.CobrancaDiversaID = a.CobrancaDiversaID) " +
                "LEFT JOIN Pedidos c WITH(NOLOCK) ON((c.SeuPedido LIKE '%' + b.PedidoPrestador + '%') AND (c.ParceiroID IN (" + parceiroID + ")) AND (c.EstadoID > 26) " +
                    "AND (((c.TipoPedidoID = 602) AND (b.Estorno = 0)) OR ((c.TipoPedidoID = 601) AND (b.Estorno = 1)))) " +
                //"LEFT JOIN Pedidos c WITH(NOLOCK) ON ((c.SeuPedido LIKE '%' + b.PedidoPrestador + '%') AND (c.ParceiroID IN (" + parceiroID + ")) AND (c.PedidoID > 26)) " +
                "LEFT JOIN Financeiro d WITH(NOLOCK) ON ((d.PedidoID = c.PedidoID) AND (d.EstadoID < 48) AND (d.ParceiroID IN (" + parceiroID + "))) " +
                "INNER JOIN TiposAuxiliares_Itens e WITH(NOLOCK) ON (e.ItemID = b.TipoOcorrenciaID) " +
                "LEFT JOIN TiposAuxiliares_Itens f WITH(NOLOCK) ON (f.ItemID = b.FormaPagamentoID) " +
            "WHERE ((a.EmpresaID = " + empresaID + ") AND (a.PrestadorID = " + programaMarketingID + ") /*AND (a.EstadoID = 8)*/ " +
                "AND ((a.PrestadorID <> 348658) OR ((a.PrestadorID = 348658) AND (b.TipoOcorrenciaID NOT IN (1442, 1443, 1444)))) " +
                "AND ((b.dtCiclo BETWEEN " + dtInicio + " AND " + dtFim + ") OR (b.ValorID = " + valorID + "))) " +
            "ORDER BY Sequencial, dtOcorrencia, PedidoPrestadorOrdem, PedidoPrestador, TipoOcorrenciaID, Estorno";

        //"UNION ALL " +
        //"SELECT a.CobrancaDiversaID, NULL AS CobDetalheID, 1446 AS TipoOcorrenciaID, dbo.fn_TipoAuxiliar_Item(1446, NULL) AS Ocorrencia, 0 AS Estorno, MAX(b.dtOcorrencia) AS dtOcorrencia, " +
        //        "MAX(b.dtCiclo) AS dtCiclo, NULL AS PedidoPrestador, NULL AS PedidoID, NULL AS FinanceiroID, a.RepasseIR, NULL AS FormaPagamento, NULL AS Loja, NULL AS ValorID, CONVERT(BIT, 0) AS OK, " +
        //        "1 AS PedidoPrestadorOrdem " +
        //    "FROM CobrancaDiversas a WITH(NOLOCK) " +
        //        "INNER JOIN CobrancaDiversas_Detalhes b WITH(NOLOCK) ON (b.CobrancaDiversaID = a.CobrancaDiversaID) " +
        //    "WHERE ((a.EmpresaID = " + empresaID + ") AND (a.PrestadorID = " + programaMarketingID + ") " +
        //        "AND (b.dtCiclo BETWEEN " + dtInicio + " AND " + dtFim + ")) " +
        //    "GROUP BY a.CobrancaDiversaID, a.RepasseIR " +
        //    "ORDER BY dtOcorrencia, PedidoPrestadorOrdem, PedidoPrestador, Valor, TipoOcorrenciaID";

    dsoCobrancasDetalhes.ondatasetcomplete = dsoCobrancasDetalhes_DSC;
    dsoCobrancasDetalhes.refresh();
}

function dsoCobrancasDetalhes_DSC() {
    startGridInterface(fg2);

    glb_GridIsBuilding = true;

    fg2.Editable = true;

    headerGrid(fg2, ['CobrancaID',                  // 00
                    'Ocorr�ncia',                   // 01
                    'Estorno',                      // 02
                    'Data',                         // 03
                    'Ciclo',                        // 04
                    'Pedido Prestador',             // 05
                    'PedidoID',                     // 06
                    'FinanceiroID',                 // 07
                    'Valor',                        // 08
                    //'Forma',
                    //'Loja',
                    'Processado',                   // 09
                    'ValorID',                      // 10
                    'OK',                           // 11
                    'TipoOcorrenciaID',             // 12
                    'CobDetalheID'], [12, 13]);     // 13

    fillGridMask(fg2, dsoCobrancasDetalhes, ['CobrancaDiversaID*',      // 00
                                            'Ocorrencia*',              // 01
                                            'Estorno*',                 // 02
                                            'dtOcorrencia*',            // 03
                                            'dtCiclo*',                 // 04
                                            'PedidoPrestador*',         // 05
                                            'PedidoID*',                // 06
                                            'FinanceiroID*',            // 07
                                            'Valor*',                   // 08
                                            //'FormaPagamento*',
                                            //'Loja*',
                                            'Processado*',              // 09
                                            'ValorID*',                 // 10
                                            'OK',                       // 11
                                            'TipoOcorrenciaID',         // 12
                                            'CobDetalheID'],            // 13
                                            ['', '', '', '', '', '', '', '', '999999999999.99', '', '', '', '', ''],
                                            ['', '', '', '', '', '', '', '', '###,###,###,###.00', '', '', '', '', '']);

    alignColsInGrid(fg2, [0, 5, 6, 7, 8, 10]);

    gridHasTotalLine(fg2, 'Totais', 0xC0C0C0, null, true, [[1, '######', 'C'],
                                                           [8, '###,###,###,###.00', 'S']]);

    var nColValor = getColIndexByColKey(fg, 'Valor*');
    var nVermelhoFonte = 0x0000FF;
    var valorCobrancaDiversaAcumulado = 0;
    var valorVL = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Valor*'));
    var valorExcedido = false;

    // Seleciona registros at� atingir valor do VL
    for (var i = 2; i < fg2.Rows; i++) {

        var valorCobrancaDiversa = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Valor*'));
        var ValorID = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'ValorID*'));
        var Processado = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Processado*'));
        
        if (Processado  == 0)
            fg2.TextMatrix(i, getColIndexByColKey(fg2, 'OK')) = 1;

        /*
        if (ValorID == 0) {
            // Seleciona Cobran�as at� atingir valor do VL
            valorCobrancaDiversaAcumulado += (valorCobrancaDiversa == "" ? 0 : valorCobrancaDiversa);

            if ((valorCobrancaDiversaAcumulado <= valorVL) && (!valorExcedido))
                fg2.TextMatrix(i, getColIndexByColKey(fg2, 'OK')) = 1;
            else
                valorExcedido = true;
        }
        */

        // Pinta fonte do grid de vermelho quando valor � negativo
        if (valorCobrancaDiversa < 0) {
            fg2.Select(i, nColValor, i, nColValor);
            fg2.FillStyle = 1;
            fg2.CellForeColor = nVermelhoFonte;
        }
    }

    atualizaSaldo();

    //fg.FontSize = '8';
    fg2.AutoSizeMode = 0;
    fg2.AutoSize(0, fg2.Cols - 1);
    fg2.Redraw = 2;

    glb_GridIsBuilding = false;

    lockControlsInModalWin(false);

    if (fg2.Rows > 1)
        fg2.Row = 2;

    fg2.focus();
}

/********************************************************************
Atualiza txtSaldo com (Soma das cobran�as diversas - VL selecionado)
********************************************************************/
function atualizaSaldo() {

    if (fg2.Rows < 2) {
        txtSaldo.value = '';
        return;
    }

    //var valorVL = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Valor*'));
    var valorCobrancaDiversaSelecionado = 0;

    // Soma Cobran�as diversas selecionadas
    for (var i = 2; i < fg2.Rows; i++) {
        var Ok = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'OK'));

        if (Ok != 0 ) {
            var valorCobrancaDiversa = fg2.ValueMatrix(i, getColIndexByColKey(fg2, 'Valor*'));
            valorCobrancaDiversaSelecionado += (valorCobrancaDiversa == "" ? 0 : valorCobrancaDiversa);
        }
    }

    //txtSaldo.value = roundNumber((valorVL - valorCobrancaDiversaSelecionado), 2);
    //txtSaldo.value = padNumReturningStr((valorVL - valorCobrancaDiversaSelecionado), 2);

    //var saldo = valorVL - valorCobrancaDiversaSelecionado;
    var saldo = valorCobrancaDiversaSelecionado;

    txtSaldo.value = saldo.toFixed(2);
}

/********************************************************************
Associa financeiros das cobran�as diversas conciliadas ao VL
********************************************************************/
function associarFinanceiros() {
    lockControlsInModalWin(true);

    // Se n�o passou nas verifica��es, retorna e libera controles
    if (!(verificaCobrancasDiversas())) {
        lockControlsInModalWin(false);
        return;
    }

    var strPars = new String();
    var nDataLen = 0;
    var nBytesAcum = 0;
    var nValorID = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'ValorID*'));

    glb_aSendDataToServer = [];
    glb_nPointToaSendDataToServer = 0;

    // Loop nos registros de cobran�as diretas - financeiros (exceto linha de Comiss�o (1441) e RepasseIR (1446))
    for (var i = 2; i < fg2.Rows; i++) {
        var nTipoOcorrenciaID = getCellValueByColKey(fg2, 'TipoOcorrenciaID', i);

        //var nProcesso = selProcesso.options[selProcesso.selectedIndex].getAttribute("ProgramaMarketingID", 1);

        //if ((getCellValueByColKey(fg2, 'OK', i) != 0)
        //        && (((nTipoOcorrenciaID != 1441) && (nProcesso != 355248)) || (nProcesso == 355248))) {

        if ((getCellValueByColKey(fg2, 'OK', i) != 0) && (nTipoOcorrenciaID != 1441)) {
           
            nBytesAcum = strPars.length;

            // Se excedeu o tamanho de parametros para enviar ao servidor - garda em array lote para enviar ao servidor
            if (nBytesAcum >= glb_nMaxStringSize) {
                nBytesAcum = 0;

                if (strPars != '') {
                    glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);
                    strPars = '';
                    nDataLen = 0;
                }
            }

            // Se strPars est� limpa, iniciou um novo lote de envio ao servidor - coloca parametro do usu�rio
            if (strPars == '') {
                strPars += '?nUserID=' + escape(glb_USERID);
                strPars += '&bDissocia=0';
                strPars += '&nValorID=' + escape(nValorID);
            }

            var nValorBaixa = null;

            // Repasse IR
            if (nTipoOcorrenciaID == 1446) {
                nValorBaixa = parseFloat(getCellValueByColKey(fg2, 'Valor*', i).toString().replace(',', '.'));
            }
            else
                nValorBaixa = buscaValorBaixa(i);

            strPars += '&nCobDetalheID=' + escape(getCellValueByColKey(fg2, 'CobDetalheID', i));
            strPars += '&nTipoOcorrenciaID=' + escape(getCellValueByColKey(fg2, 'TipoOcorrenciaID', i));
            strPars += '&nFinanceiroID=' + escape(getCellValueByColKey(fg2, 'FinanceiroID*', i));
            strPars += '&nValorBaixa=' + nValorBaixa;

            nDataLen++;
        }
    }

    // Adiciona ultimo lote vez do loop no array para envio ao servidor
    if (nDataLen > 0)
        glb_aSendDataToServer[glb_aSendDataToServer.length] = strPars + '&nDataLen=' + escape(nDataLen);

    // Envia dados ao aservidor
    sendDataToServer();
}

/********************************************************************
Envia dados para servidor
********************************************************************/
function sendDataToServer() {
    if (glb_nTimerSendDataToServer != null) {
        window.clearInterval(glb_nTimerSendDataToServer);
        glb_nTimerSendDataToServer = null;
    }

    try {
        if (glb_nPointToaSendDataToServer < glb_aSendDataToServer.length) {
            var strPars = ((glb_aSendDataToServer.length == (glb_nPointToaSendDataToServer + 1)) ? "&bUltimo=1" : "&bUltimo=0");

            dsoGravacao.URL = SYS_ASPURLROOT + '/modfinanceiro/subcobranca/CobrancasDiversas/serverside/associafinanceiros.aspx' + glb_aSendDataToServer[glb_nPointToaSendDataToServer] + strPars;
            dsoGravacao.ondatasetcomplete = sendDataToServer_DSC;
            dsoGravacao.refresh();
        }
        else {
            lockControlsInModalWin(false);

            if (window.top.overflyGen.Alert('Cobran�as associadas com sucesso.') == 0)
                return null;

            listarCobrancasDiversas();
        }
    }
    catch (e) {
        if (window.top.overflyGen.Alert('Opera��o n�o autorizada') == 0)
            return null;

        lockControlsInModalWin(false);
        glb_OcorrenciasTimerInt = window.setInterval('listarCobrancasDiversas()', 10, 'JavaScript');
    }
}

function sendDataToServer_DSC() {
    if (!(dsoGravacao.recordset.BOF && dsoGravacao.recordset.EOF)) {
        dsoGravacao.recordset.MoveFirst();

        if ((dsoGravacao.recordset['Resultado'].value != null) && (dsoGravacao.recordset['Resultado'].value != '')) {
            if (window.top.overflyGen.Alert(dsoGravacao.recordset['Resultado'].value) == 0)
                return null;

            glb_OcorrenciasTimerInt = window.setInterval('listarCobrancasDiversas()', 10, 'JavaScript');

            return null;
        }
    }

    glb_nPointToaSendDataToServer++;
    glb_nTimerSendDataToServer = window.setInterval('sendDataToServer()', INTERVAL_BATCH_SAVE, 'JavaScript');
}

/********************************************************************
Calcula valor parcial de baixa de cada financeiro: Valor total financeiro - Comiss�o
Retorno: Valor de baixa j� calculado
********************************************************************/
function buscaValorBaixa(Row) {
    var sPedidoPrestador = getCellValueByColKey(fg2, 'PedidoPrestador*', Row);
    var nFinanceiroID = getCellValueByColKey(fg2, 'FinanceiroID*', Row);
    var nValorFinanceiro = parseFloat(getCellValueByColKey(fg2, 'Valor*', Row).toString().replace(',', '.'));
    var nValorComissaoAcumulado = 0;
    var sPedidoPrestadorAnterior = '';

    for (var i = 2; i < fg2.Rows; i++) {
        var nTipoOcorrenciaID = getCellValueByColKey(fg2, 'TipoOcorrenciaID', i);

        // Comiss�o
        if (nTipoOcorrenciaID == 1441) {
            var sPedidoPrestadorComissao = getCellValueByColKey(fg2, 'PedidoPrestador*', i);
            var nFinanceiroComissaoID = getCellValueByColKey(fg2, 'FinanceiroID*', i);
            var nValorComissao = parseFloat(getCellValueByColKey(fg2, 'Valor*', i).replace(',', '.'));

            if ((sPedidoPrestador == sPedidoPrestadorComissao) && (nFinanceiroID = nFinanceiroComissaoID) && (Math.abs(nValorComissao) > 0) && (sPedidoPrestador != sPedidoPrestadorAnterior)) {
                nValorComissaoAcumulado += nValorComissao;
                sPedidoPrestadorAnterior = sPedidoPrestadorComissao;
            }
        }
    }

    return nValorFinanceiro + nValorComissaoAcumulado;
}

/********************************************************************
Valida associa��o das cobran�as (financeiros) ao VL
Retorno: true  - passou em todas as verifica��es
         false - n�o passou em todas as verifica��es, n�o faz associa��o
********************************************************************/
function verificaCobrancasDiversas() {

    // Se n�o preencheu grids para associar
    if ((fg.Rows < 1) || (fg2.Rows < 2)) {
        if (window.top.overflyGen.Alert('Selecione registros para associar.') == 0)
            return null;

        lockControlsInModalWin(false);
        return false;
    }

    // Verifica saldo entre VL e Cobran�as selecionados
    if (txtSaldo.value != 0) {
        //if (window.top.overflyGen.Alert('Cobran�as selecionadas n�o correspondem ao valor do VL.') == 0)
        //    return null;

        var msg = 'Cobran�as selecionadas n�o correspondem ao valor do VL.\n' +
                  'Uma vez associados os financeiros o processo de pagamento estar� iniciado.\n\nDeseja continuar?';

        var _retMsg = window.top.overflyGen.Confirm(msg);

        if ((_retMsg == 0) || (_retMsg == 2))
            return false;
    }

    var bValorLocalizarOK = false;

    // Loop no grid de Valores a Localizar
    for (var i = 1; i < fg.Rows; i++) {
        if (getCellValueByColKey(fg, 'OK', i) != 0)
            bValorLocalizarOK = true;
    }

    // Verifica se selecionou algum VL para associa��o
    if (!bValorLocalizarOK) {
        if (window.top.overflyGen.Alert('Selecione um Valor a Localizar para associar.') == 0)
            return null;

        return false;
    }

    // Loop no grid de Cobran�as Diversas
    for (var i = 2; i < fg2.Rows; i++) {
        if ((getCellValueByColKey(fg2, 'OK', i) != 0) && (getCellValueByColKey(fg2, 'TipoOcorrenciaID', i) != 1446)) {

            // Verifica se algum dos itens selecionados n�o possui
            if (getCellValueByColKey(fg2, 'FinanceiroID*', i) == '') {
                if (window.top.overflyGen.Alert('Cobran�as selecionadas n�o possuem um FinanceiroID encontrado.') == 0)
                    return null;

                lockControlsInModalWin(false);
                return false;
            }
        }
    }

    return true;
}

/********************************************************************
Fun��es de eventos dos grids
********************************************************************/
function js_fg_modalConciliacaoFinanceira_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol) {

    if (OldRow != NewRow) {

        // Limpa grid de Cobran�as Detalhes
        fg2.Rows = 1;
        atualizaSaldo();

        /*
        var dtApropriacaoVL = null;

        dtApropriacaoVL = fg.TextMatrix(NewRow, getColIndexByColKey(fg, 'dtApropriacao*'));

        if (dtApropriacaoVL != null) {
            var dia = new Date(putDateInMMDDYYYY(dtApropriacaoVL)).getDate();
            var pessoaID = selProcesso.options[selProcesso.selectedIndex].getAttribute("ProgramaMarketingID", 1);
            var diaInicio = null;
            var diaFim = null;
            var cicloAtual = true;

            dsoCiclos.recordset.setFilter('PessoaID=' + pessoaID);
            dsoCiclos.recordset.moveFirst();

            while (!dsoCiclos.recordset.EOF) {
                diaInicio = dsoCiclos.recordset['DiaInicio'].value;
                diaFim = dsoCiclos.recordset['DiaFim'].value;

                if ((dia >= diaInicio) && (dia <= diaFim)) {
                    if (cicloAtual)
                    {
                        dia = diaInicio - 1;
                        cicloAtual = false;
                        dsoCiclos.recordset.moveFirst();
                        continue;
                    }

                    break;
                }

                dsoCiclos.recordset.moveNext();
            }

            var a = 1;
        }
        */
    }

    //js_fg_AfterRowColChange(fg, OldRow, OldCol, NewRow, NewCol);
}

function js_fg_modalConciliacaoFinanceira_DblClick(fg, Row, Col) {

    // Lista Cobran�as Diversas
    listarCobrancasDiversas();
}

function js_fg2_modalConciliacaoFinanceira_DblClick(fg2, Row, Col) {

    // Marca OK em todas as linhas
    if (fg2.Rows > 1) {
        var nCobrancaDiversaOK = getCellValueByColKey(fg2, 'OK', 2);

        if ((Row == 1) && (Col == getColIndexByColKey(fg2, 'OK'))) {

            for (var i = 2; i < fg2.Rows; i++) {
                fg2.TextMatrix(i, getColIndexByColKey(fg2, 'OK')) = (nCobrancaDiversaOK != 0 ? 0 : 1);
            }

            atualizaSaldo();
        }
    }
}

function js_fg2_modalConciliacaoFinanceira_AfterEdit(fg2, Row, Col) {
    var nColOK = getColIndexByColKey(fg2, 'OK');

    if ((Col == nColOK) && (Row > 1)) {
        atualizaSaldo();
        
        var Processado = fg2.ValueMatrix(Row, getColIndexByColKey(fg2, 'Processado*'));
        
        if (Processado != 0)
            fg2.TextMatrix(Row, Col) = 0;
    }
}