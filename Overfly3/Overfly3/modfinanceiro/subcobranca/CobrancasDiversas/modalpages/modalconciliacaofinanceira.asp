<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    'Dados fornecidos pelo OverflySrvCfg.dll
    Dim objSvrCfg
    'String de conexao definida pelo dll pois o include de conexao
    'neste caso nao pode ser definido dinamicamente
    Dim strConn, pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")

    strConn = objSvrCfg.DataBaseStrConn(Application("appName"))
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="modalconciliacaofinanceiraHtml" name="modalconciliacaofinanceiraHtml">
<head>
<title></title>

<%
    'Links de estilo, bibliotecas da automacao e especificas
    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcobranca/CobrancasDiversas/modalpages/modalconciliacaofinanceira.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modalgen/commonmodal.js" & Chr(34) & "></script>" & vbCrLf
                                                                                                                
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcobranca/CobrancasDiversas/modalpages/modalconciliacaofinanceira.js" & Chr(34) & "></script>" & vbCrLf
%>

<%
    'Script de variaveis globais

    Dim i, sCaller, glb_sUltimaPesquisa

    sCaller = ""

    'sCaller - quem chamou a modal (PL - pesqlist, S - sup, I - inf)
    For i = 1 To Request.QueryString("sCaller").Count    
        sCaller = Request.QueryString("sCaller")(i)
    Next

    If ( sCaller <> "" ) Then
        sCaller = UCase(sCaller)
    End If

    Response.Write "<script ID=" & Chr(34) & "serverSideVars"  & Chr(34) & " LANGUAGE=" & Chr(34) & "javascript" & Chr(34) & ">"
    Response.Write vbcrlf
    Response.Write vbcrlf

    'Necessario para compatibilidade da automacao
    Response.Write "var glb_USERID = 0;"
    Response.Write vbcrlf
    Response.Write vbcrlf

    Response.Write "var glb_sCaller = " & Chr(39) & sCaller & Chr(39) & ";"
    Response.Write vbcrlf

    Response.Write "</script>"
    Response.Write vbcrlf
%>

<script ID="wndJSProc" LANGUAGE="javascript">

</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
    if (glb_GridIsBuilding)
        return;

    js_fg_modalConciliacaoFinanceira_AfterRowColChange(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
    js_fg_modalConciliacaoFinanceira_DblClick(fg, fg.Row, fg.Col);
</SCRIPT>


<!-- Eventos para grid de Cobran�as Diversas -->
<SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=DblClick>
    js_fg2_modalConciliacaoFinanceira_DblClick(fg2, fg2.Row, fg2.Col);
</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg2 EVENT=AfterEdit>
    js_fg2_modalConciliacaoFinanceira_AfterEdit(fg2, arguments[0], arguments[1])
 </SCRIPT>

</head>

<body id="modalConciliacaoFinanceiraHtmlBody" name="modalConciliacaoFinanceiraHtmlBody" LANGUAGE="javascript" onload="return window_onload()">

    <p id="lblServico" name="lblServico" class="lblGeneral">Servi�o</p>
    <select id="selServico" name="selServico" class="fldGeneral" LANGUAGE=javascript>
        <option value="0">Concilia��o</option>
    </select>
    
    <!-- Controles para servi�o Concilia��o --> 
    <div id="divControlesConciliacao" name="divControlesConciliacao" class="divGeneral">
        <%
            ' Declara vari�veis para uso no preenchimento dos combos
            Dim strSQL, rsData
            Set rsData = Server.CreateObject("ADODB.Recordset")
        %>

        <p id="lblProcesso" name="lblProcesso" class="lblGeneral">Processo</p>
        <select id="selProcesso" name="selProcesso" class="fldGeneral" LANGUAGE=javascript>
        <%
            strSQL =    "SELECT DISTINCT b.SujeitoID AS fldID, b.ObjetoID AS ProgramaMarketingID, a.Fantasia AS fldName " & _
                            "FROM Pessoas a WITH(NOLOCK) " & _
                                "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.ObjetoID = a.PessoaID) " & _
                            "WHERE ((a.TipoPessoaID = 55) AND (a.EstadoID = 2) " & _
                                "AND (dbo.fn_TagValor(a.Observacoes, 'ProgramaMarketing' , '<', ',', 1) LIKE 'MARKETPLACE') " & _
                                "AND (b.TipoRelacaoID = 36) AND (b.EstadoID = 2)) " & _
                        "UNION ALL " & _
                        "SELECT DISTINCT a.ObjetoID AS fldID, b.ObjetoID AS ProgramaMarketingID, c.Fantasia AS fldName " & _
                            "FROM RelacoesPessoas a WITH(NOLOCK) " & _
                                "INNER JOIN RelacoesPesRec b WITH(NOLOCK) ON (b.SujeitoID = a.SujeitoID) " & _
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.ObjetoID) " & _
                            "WHERE (a.EstadoID = 2) AND (a.TipoRelacaoID = 21) AND (b.EstadoID = 2) AND (b.TipoRelacaoID = 12) " & _
                                "AND (dbo.fn_TagValor(c.Observacoes, 'AdquirenteFinanceiro' , '<', ',', 1) LIKE '1')  " & _
                        "ORDER BY fldName"

	        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	If (Not (rsData.BOF AND rsData.EOF) ) Then
        		While Not (rsData.EOF)
        		    Response.Write("<option value='" & rsData.Fields("fldID").Value & "' ")
                    Response.Write("ProgramaMarketingID='" & rsData.Fields("ProgramaMarketingID").Value & "'> ")
        			Response.Write(rsData.Fields("fldName").Value & "</option>" )
        			rsData.MoveNext()
        		WEnd
        	End If

        	rsData.Close
        %>
        </select>
        
        <p id="lblEmpresa" name="lblEmpresa" class="lblGeneral">Empresa</p>
        <select id="selEmpresa" name="selEmpresa" class="fldGeneral" LANGUAGE=javascript>
        <%
            strSQL =    "SELECT DISTINCT b.SujeitoID AS fldID, d.Fantasia AS fldName " & _
                            "FROM Pessoas a WITH(NOLOCK) " & _
                                "INNER JOIN RelacoesPessoas b WITH(NOLOCK) ON (b.ObjetoID = a.PessoaID) " & _
                                "INNER JOIN RelacoesPessoas c WITH(NOLOCK) ON (c.ObjetoID = a.PessoaID) " & _
                                "INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = b.SujeitoID) " & _
                            "WHERE ((a.TipoPessoaID = 55) AND (a.EstadoID = 2) " & _
                                "AND (dbo.fn_TagValor(a.Observacoes, 'ProgramaMarketing' , '<', ',', 1) LIKE 'MARKETPLACE') " & _
                                "AND (b.TipoRelacaoID = 35) AND (b.EstadoID = 2) " & _
                                "AND (c.TipoRelacaoID = 36) AND (c.EstadoID = 2)) " & _
                        "UNION " & _
                        "SELECT DISTINCT d.PessoaID AS fldID, d.Fantasia AS fldName " & _
                            "FROM RelacoesPessoas a WITH(NOLOCK) " & _
                                "INNER JOIN RelacoesPesRec b WITH(NOLOCK) ON (b.SujeitoID = a.SujeitoID) " & _
                                "INNER JOIN Pessoas c WITH(NOLOCK) ON (c.PessoaID = a.ObjetoID) " & _
                                "INNER JOIN Pessoas d WITH(NOLOCK) ON (d.PessoaID = a.SujeitoID) " & _
                            "WHERE (a.EstadoID = 2) AND (a.TipoRelacaoID = 21) AND (b.EstadoID = 2) AND (b.TipoRelacaoID = 12) " & _
                                "AND (dbo.fn_TagValor(c.Observacoes, 'AdquirenteFinanceiro' , '<', ',', 1) LIKE '1') " & _
                        "ORDER BY fldName "

	        rsData.Open strSQL, strConn, adOpenForwardOnly, adLockReadOnly, adCmdText

        	If (Not (rsData.BOF AND rsData.EOF) ) Then
        		While Not (rsData.EOF)
        		    Response.Write("<option value='" & rsData.Fields("fldID").Value & "'>")
        			Response.Write(rsData.Fields("fldName").Value & "</option>" )
        			rsData.MoveNext()
        		WEnd
        	End If

        	rsData.Close
        %>
        </select>

        <p id="lblDataInicio" name="lblDataInicio" title="Data inicial do ciclo de faturamento" class="lblGeneral">Data In�cio</p>
        <input type="text" id="txtDataInicio" name="txtDataInicio" title="Data inicial do ciclo de faturamento" class="fldGeneral" LANGUAGE="javascript">
        <p id="lblDataFim" name="lblDataFim" title="Data inicial do ciclo de faturamento" class="lblGeneral">Data Fim</p>
        <input type="text" id="txtDataFim" name="txtDataFim" title="Data inicial do ciclo de faturamento" class="fldGeneral" LANGUAGE="javascript">
        <p id="lblSaldo" name="lblSaldo" class="lblGeneral">Saldo</p>
        <input type="text" id="txtSaldo" name="txtSaldo" title="Valor do Valor a Localizar - Cobran�as Diversas selecionadas" class="fldGeneral" LANGUAGE="javascript">
        <!--<input type="button" id="btnDissociar" name="btnDissociar" value="Dissociar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">-->
        <input type="button" id="btnAssociar" name="btnAssociar" value="Associar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
        <input type="button" id="btnListar" name="btnListar" value="Listar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    </div>

    <!-- Grids -->
    <div id="divGrids" name="divGrids" class="divGeneral">
        <!-- Div de Valores a Localizar -->
        <div id="divFG" name="divFG" class="divGeneral">
            <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT></object>
        </div>
        
        <!-- Div de Cobran�as Diversas -->
        <div id="divFG2" name="divFG2" class="divGeneral">
            <object CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg2" name="fg2" VIEWASTEXT></object>
        </div>
    </div>
    
    <input type="button" id="btnOK" name="btnOK" value="OK" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">
    <input type="button" id="btnCanc" name="btnCanc" value="Fechar" LANGUAGE="javascript" onclick="return btn_onclick(this)" class="btns">        
    
    <!-- Automa��o -->
    <div id="divMod01" name="divMod01" class="divGeneral">
        <p id="paraMod01" name="paraMod01" class="paraNormal"></p>
    </div>    
</body>
</html>
