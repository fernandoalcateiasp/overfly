using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modfinanceiro.subcobranca.CobrancasDiversas.serverside
{
    public partial class associafinanceiros : System.Web.UI.OverflyPage
	{
        private Integer UserId;
        public Integer nUserID
        {
            get { return UserId; }
            set { UserId = value; }
        }

        private Integer ValorID;
        public Integer nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }

        private Integer[] CobDetalheID;
        public Integer[] nCobDetalheID
        {
            get { return CobDetalheID; }
            set { CobDetalheID = value; }
        }

        private Integer[] TipoOcorrenciaID;
        public Integer[] nTipoOcorrenciaID
        {
            get { return TipoOcorrenciaID; }
            set { TipoOcorrenciaID = value; }
        }

        private Integer[] FinanceiroID;
        public Integer[] nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }

        private string[] ValorBaixa;
        public string[] nValorBaixa
        {
            get { return ValorBaixa; }
            set { ValorBaixa = value; }
        }
        
        private Integer DataLen;
        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }

        private string Ultimo;
        public string bUltimo
        {
            get { return Ultimo; }
            set { Ultimo = value; }
        }

        private string AssociaFinanceiros()
        {
            string Resultado = "";

            for (int i = 0; i < DataLen.intValue(); i++)
            {
                // Se n�o � Repasse IR
                if (Convert.ToInt32(TipoOcorrenciaID[i]) != 1446)
                {
                    ProcedureParameters[] procParams = new ProcedureParameters[9];

                    procParams[0] = new ProcedureParameters(
                        "@ValorID",
                        System.Data.SqlDbType.Int,
                        ValorID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(ValorID.ToString()));

                    procParams[1] = new ProcedureParameters(
                        "@FinanceiroID",
                        System.Data.SqlDbType.Int,
                        FinanceiroID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID[i]));

                    procParams[2] = new ProcedureParameters(
                        "@Valor",
                        System.Data.SqlDbType.Money,
                        ValorBaixa.Length == 0 ? DBNull.Value : (Object)double.Parse(ValorBaixa[i]));

                    procParams[3] = new ProcedureParameters(
                        "@ValorEncargos",
                        System.Data.SqlDbType.Decimal,
                        DBNull.Value);

                    procParams[4] = new ProcedureParameters(
                        "@OK",
                        System.Data.SqlDbType.Bit, true);

                    procParams[5] = new ProcedureParameters(
                        "@Observacao",
                        System.Data.SqlDbType.VarChar, DBNull.Value, 30);

                    procParams[6] = new ProcedureParameters(
                        "@UsuarioID",
                        System.Data.SqlDbType.Int,
                        UserId.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UserId.ToString()));

                    procParams[7] = new ProcedureParameters(
                        "@Incluir",
                        System.Data.SqlDbType.Bit, true);

                    procParams[8] = new ProcedureParameters(
                       "@Resultado",
                       System.Data.SqlDbType.VarChar,
                       DBNull.Value,
                       ParameterDirection.InputOutput);

                    procParams[8].Length = 8000;

                    DataInterfaceObj.execNonQueryProcedure("sp_ValorLocalizar_AssociarFinanceiro", procParams);

                    if (procParams[8].Data != DBNull.Value)
                        Resultado += procParams[8].Data.ToString() + "\r \n";
                    else
                    {
                        int fldresp;

                        string sql = "DECLARE @ValFinanceiroID INT " +
                                     "SELECT @ValFinanceiroID = ValFinanceiroID " +
                                        "FROM ValoresLocalizar_Financeiros WITH(NOLOCK) " +
                                        "WHERE ((ValorID = " + ValorID.ToString() + ") AND (FinanceiroID = " + FinanceiroID[i].ToString() + ")) " +
                                     "UPDATE CobrancaDiversas_Detalhes " +
                                        "SET ValorID = " + ValorID.ToString() + ", ValFinanceiroID = @ValFinanceiroID, Processado = 1 " +
                                        "WHERE CobDetalheID = " + CobDetalheID[i].ToString();

                        fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

                        if (fldresp < 0)
                            Resultado = "Erro ao atualizar cobran�a diversa.";
                    } 
                }
                else
                {
                    int fldresp;

                    string sql = "UPDATE CobrancaDiversas_Detalhes SET /*ValorID = " + ValorID.ToString() + ",*/ Processado = 1 WHERE CobDetalheID = " + CobDetalheID[i].ToString();

                    fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

                    if (fldresp < 0)
                        Resultado = "Erro ao atualizar cobran�a diversa de Repasse IR.";
                }
            }

            // No ultimo lote de atualiza��es do banco, atualiza ValorID das cobranssas relacionadas entre elas 
            if (bUltimo == "1") {
                int fldresp;

                string sql = "UPDATE c " +
                                "SET c.ValorID = a.ValorID, c.ValFinanceiroID = a.ValFinanceiroID, c.Processado = a.Processado " +
                                "FROM CobrancaDiversas_Detalhes a " +
                            		"INNER JOIN CobrancaDiversas b ON (b.CobrancaDiversaID = a.CobrancaDiversaID) " +
                            		"INNER JOIN CobrancaDiversas_Detalhes c ON ((c.Estorno = a.Estorno) " +
                            			"AND ((c.CobDetalheReferenciaID = a.CobDetalheID) " +
                            				// Para B2W a liga��o entre comiss�o e recebimento � pelo PedidoPrestador
                            				"OR ((c.PedidoPrestador = a.PedidoPrestador) AND (b.PrestadorID = 355248)))) " +
                                "WHERE ((a.ValorID = " + ValorID.ToString() + ") AND ((c.ValorID IS NULL) OR (c.ValFinanceiroID IS NULL))) ";

                fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);

                if (fldresp < 0)
                    Resultado = "Erro ao atualizar cobran�a diversa relacionadas.";
            }

            return Resultado;
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            string Resultado = AssociaFinanceiros();

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + Resultado + "' as Resultado "));
		}
	}
}
