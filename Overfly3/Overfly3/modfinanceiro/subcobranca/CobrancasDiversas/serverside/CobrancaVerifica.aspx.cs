﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using WSData;

namespace Overfly3.modfinanceiro.subcobranca.CobrancasDiversas.serverside
{
    public partial class CobrancaVerifica : System.Web.UI.OverflyPage
    {
        private string CobrancaDiversaID = "";
        private string currEstadoId = "";
        private string newEstadoId = "";
        private string UsuarioID = "";
        string sql = "";
        string Resultado = "";
        string Mensagem = "";

        private string response = "";

        protected string nCobrancaDiversaID
        {
            set { CobrancaDiversaID = value != null ? value : ""; }
        }

        protected string nCurrEstadoID
        {
            set { currEstadoId = value != null ? value : ""; }
        }
        protected string nNewEstadoID
        {
            set { newEstadoId = value != null ? value : ""; }
        }

        protected string nUsuarioID
        {
            set { UsuarioID = value != null ? value : ""; }
        }

        protected void VerificaCObranca()
        {
            // Executa procedure sp_Atendimento_Realiza
            ProcedureParameters[] aParams = new ProcedureParameters[6];

            aParams[0] = new ProcedureParameters(
                "@CobrancaDiversaID",
                System.Data.SqlDbType.Int,
                CobrancaDiversaID == null || CobrancaDiversaID.Length == 0 ?
                    System.DBNull.Value :
                    (Object)int.Parse(CobrancaDiversaID));

            aParams[1] = new ProcedureParameters(
                "@EstadoDeID",
                System.Data.SqlDbType.Int,
                currEstadoId == null || currEstadoId.Length == 0 ?
                    System.DBNull.Value :
                    (Object)int.Parse(currEstadoId));

            aParams[2] = new ProcedureParameters(
                "@EstadoParaID",
                System.Data.SqlDbType.Int,
                newEstadoId == null || newEstadoId.Length == 0 ?
                    System.DBNull.Value :
                    (Object)int.Parse(newEstadoId));

            aParams[3] = new ProcedureParameters(
             "@UsuarioID",
             System.Data.SqlDbType.Int,
             UsuarioID == null || UsuarioID.Length == 0 ?
                 System.DBNull.Value : (Object)int.Parse(UsuarioID));

            aParams[4] = new ProcedureParameters(
                      "@Resultado",
                      System.Data.SqlDbType.Int,
                      DBNull.Value,
                      ParameterDirection.InputOutput);

            aParams[5] = new ProcedureParameters(
                "@Mensagem",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            aParams[5].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure(
                "sp_CobrancaDiversas_Verifica",
                aParams);

            if (aParams[4].Data != DBNull.Value)
                Resultado += aParams[4].Data.ToString();

            if (aParams[5].Data != DBNull.Value)
                Mensagem += aParams[5].Data.ToString();

            sql += (sql == "" ? "SELECT" : "UNION SELECT") + " " +
                (Resultado != null ? ("'" + Resultado + "' as Resultado, ") : " ") +
                (Mensagem.ToString() != null ? ("'" + Mensagem + "' as Mensagem ") : " ");
            //(ValorId.ToString() != null ? ("' " + ValorId + "' as ValorID ") : " ");
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            VerificaCObranca();
            WriteResultXML(
                          DataInterfaceObj.getRemoteData(sql));
        }
    }
}