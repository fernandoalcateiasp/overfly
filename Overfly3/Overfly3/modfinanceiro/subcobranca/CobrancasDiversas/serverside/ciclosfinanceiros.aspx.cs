using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modfinanceiro.subcobranca.CobrancasDiversas.serverside
{
    public partial class ciclosfinanceiros : System.Web.UI.OverflyPage
	{
        protected DataSet execProcedure()
        {
            string sql = "EXEC sp_Marketplace_CicloFinanceiro";

            return DataInterfaceObj.getRemoteData(sql);
        }

		protected override void PageLoad(object sender, EventArgs e)
		{
            WriteResultXML(execProcedure());
		}
	}
}
