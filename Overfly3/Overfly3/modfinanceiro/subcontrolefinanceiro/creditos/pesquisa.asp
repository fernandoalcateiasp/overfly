<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="creditospesqlistHtml" name="creditospesqlistHtml">

<head>

<title></title>

<%

    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_pesqlist.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/creditos/pesqlist.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">


</script>

<script LANGUAGE="javascript" FOR="txtArgumento" EVENT="onkeydown">

    txtArgumento_onkeydown();

</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
fg_KeyPress(arguments[0]);
</SCRIPT>

<script LANGUAGE="javascript" FOR="fg" EVENT="DblClick">
fg_DblClick();
</script>

<script LANGUAGE="javascript" FOR="fg" EVENT="BeforeSort">
fg_BeforeSort();
</script>

<script LANGUAGE="javascript" FOR="fg" EVENT="AfterSort">
fg_AfterSort();
</script>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
    js_fg_MouseMove(fg, arguments[0], arguments[1], arguments[2], arguments[3]);
</SCRIPT>

<SCRIPT LANGUAGE="javascript" FOR="fg" EVENT="AfterRowColChange">
js_fg_AfterRowColChangePesqList (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
</SCRIPT>

</head>

<body id="creditospesqlistBody" name="creditospesqlistBody" LANGUAGE="javascript" onload="return window_onload()">
    <div id="divSup01_01" name="divSup01_01" class="divExtern">

        <p id="lblRefrInf" name="lblRefrInf" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkRefrInf)" title="Refresh">Ref</p>
        <input type="checkbox" id="chkRefrInf" name="chkRefrInf" class="fldGeneral" title="Refresh"></input>
            
        <p id="lblOrdem" name="lblOrdem" class="lblGeneral" LANGUAGE=javascript onclick="return invertChkBox(chkOrdem)" title="Inverter ordem?">Inv</p>
        <input type="checkbox" id="chkOrdem" name="chkOrdem" class="fldGeneral" title="Inverter ordem?"></input>
                
        <p id="lblRegistrosVencidos" name="lblRegistrosVencidos" class="lblGeneral" LANGUAGE=javascript title="Registros vencidos?">Venc</p>
        <input type="checkbox" id="chkRegistrosVencidos" name="chkRegistrosVencidos" class="fldGeneral" title="Registros vencidos?"></input>    

        <p id="lblProprietariosPL" name="lblProprietariosPL" class="lblGeneral" LANGUAGE=javascript>Proprietário</p>
        <select id="selProprietariosPL" name="selProprietariosPL" class="fldGeneral"></select>

        <p id="lblRegistros" name="lblRegistros" class="lblGeneral">Registros</p>
        <select id="selRegistros" name="selRegistros" class="fldGeneral">
            <option value=100>100</option>
            <option value=200>200</option>
            <option value=300>300</option>
        </select>
                
        <p id="lblPesquisa" name="lblPesquisa" class="lblGeneral">Chave de Pesquisa</p>
        <select id="selPesquisa" name="selPesquisa" class="fldGeneral">
            <option id="NaCreditoID" name="NaCreditoID" value="aCreditoID">ID</option>
            <option id="NaParceiroID" name="NaParceiroID" value="aParceiroID">ParceiroID</option>
            <option id="VbFantasia" name="VcFantasia" value="bFantasia">Parceiro</option>
        </select>

        <p id="lblArgumento" name="lblArgumento" class="lblGeneral">Argumento</p>
        <input type="text" id="txtArgumento" name="txtArgumento" class="fldGeneral" LANGUAGE="javascript"></input>
                
        <p id="lblFiltro" name="lblFiltro" class="lblGeneral">Filtro</p>
        <input type="text" id="txtFiltro" name="txtFiltro" class="fldGeneral"></input>

    </div>
        
    <div id="divSup01_02" name="divSup01_02" class="divExtern" LANGUAGE=javascript>
        <OBJECT CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT></OBJECT>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>    

</body>

</html>
