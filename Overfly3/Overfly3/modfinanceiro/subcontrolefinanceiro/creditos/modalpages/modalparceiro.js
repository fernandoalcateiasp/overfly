/*****************************************************************************************************
Declara��o de vari�veis globais.
******************************************************************************************************/
var dsoPesq = new CDatatransport("dsoPesq");

/********************************************************************
WindowOnLoad.
********************************************************************/
function window_onload() {
    window_onload_1stPart();

    setupPage();

    var elem = document.getElementById('modalparceiroBody');

    with (elem) {
        style.backgroundColor = 'transparent';
        scroll = 'no';
        style.visibility = 'visible';
    }

    showExtFrame(window, true);

    if (document.getElementById('txtParceiro').disabled == false)
        txtParceiro.focus();
}

/********************************************************************
Configuracao inicial do html.
********************************************************************/
function setupPage() {

    secText('Selecionar Parceiro', 1);

    var elem;
    var temp;

    elem = window.document.getElementById('divParceiro');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divMod01').style.height, 10);
        width = (30 * FONT_WIDTH) + 24 + 20 + 42;
        temp = parseInt(width);
        height = 40;

    }

    adjustElementsInForm([['lblChavePesquisa', 'selChavePesquisa', 12, 1, -10],
    ['lblParceiro', 'txtParceiro', 25, 1, -3]],
        null, null, true);

    btnFindPesquisa.disabled = true;
    with (btnFindPesquisa.style) {
        top = parseInt(txtParceiro.style.top, 10);
        left = parseInt(txtParceiro.style.left, 10) + parseInt(txtParceiro.style.width, 10) + 2;
        width = 80;
        height = 24;
    }

    elem = window.document.getElementById('divFG');
    with (elem.style) {
        border = 'none';
        backgroundColor = 'transparent';
        left = ELEM_GAP;
        top = parseInt(document.getElementById('divParceiro').style.top) + parseInt(document.getElementById('divParceiro').style.height) + (ELEM_GAP * 2);
        width = temp + 221;
        height = (MAX_FRAMEHEIGHT_OLD / 2) - (ELEM_GAP * 12) + 4;
    }

    elem = document.getElementById('fg');
    with (elem.style) {
        left = 0;
        top = 0;
        width = parseInt(document.getElementById('divFG').style.width);
        height = parseInt(document.getElementById('divFG').style.height);
    }

    startGridInterface(fg);

    headerGrid(fg, ['Nome',
        'ID',
        'Fantasia',
        'Documento',
        'Cidade',
        'UF',
        'Pa�s',
        'Parceiro'], [2, 7]);

    fg.Redraw = 2;

    var aOptions2 = [[0, 'ID'],
    [1, 'Nome'],
    [2, 'Fantasia'],
    [3, 'Documento']];

    for (i = 0; i <= 3; i++) {
        var oOption = document.createElement("OPTION");
        oOption.value = aOptions2[i][0];
        oOption.text = aOptions2[i][1];
        selChavePesquisa.add(oOption);
    }

    txtParceiro.onkeypress = txtParceiro_onKeyPress;
}

/********************************************************************
Realiza pesquisa ao clicar em enter.
********************************************************************/
function txtParceiro_onKeyPress() {
    if (event.keyCode == 13)
        btnFindPesquisa_onclick();
}

/********************************************************************
Altera estado do bot�o.
********************************************************************/
function txtParceiro_ondigit(ctl) {
    changeBtnState(ctl.value);
}

/********************************************************************
Realiza a pesquisa.
********************************************************************/
function btnFindPesquisa_onclick(ctl) {
    txtParceiro.value = trimStr(txtParceiro.value);

    changeBtnState(txtParceiro.value);

    if (btnFindPesquisa.disabled)
        return;

    startPesq(txtParceiro.value);
}

/********************************************************************
Clique com o bot�o OK ao dar double click.
********************************************************************/
function fg_DblClick() {
    if ((fg.Row >= 1) && (btnOK.disabled == false))
        btn_onclick(btnOK);
}

/********************************************************************
Clique botao OK ou Cancela.
********************************************************************/
function btn_onclick(ctl) {

    lockControlsInModalWin(true);

    if (ctl.id == btnOK.id) {
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'OK_CALLFORM_S', new Array(
            fg.TextMatrix(fg.Row, 2), fg.TextMatrix(fg.Row, 1), 1,
            fg.TextMatrix(fg.Row, 7)));
    }
    else if (ctl.id == btnCanc.id)
        sendJSMessage(getHtmlId(), JS_DATAINFORM, 'CANCEL_CALLFORM_S', null);

}

/********************************************************************
Monta dso da pesquisa.
********************************************************************/
function startPesq(strPesquisa) {
    lockControlsInModalWin(true);
    var strSQL = '';
    var strSQLSelect = '';
    var strSQLFrom = '';
    var strSQLWhere = '';
    var strSQLOrder = '';

    var sChavePesquisa = '';
    var sPesquisa = '';

    if (selChavePesquisa.value == 0) {

        if (isNaN(strPesquisa)) {
            if (window.top.overflyGen.Alert('Informe um n�mero para pesquisar por ID.') == 0)
                return null;

            lockControlsInModalWin(false);
            return false;
        }

        sChavePesquisa = 'Pessoas.Nome';
        sPesquisa = 'AND Pessoas.PessoaID';
    }
    else if (selChavePesquisa.value == 1) {
        sChavePesquisa = 'Pessoas.Nome';
        sPesquisa = ' AND Pessoas.Nome';
    }

    else if (selChavePesquisa.value == 2) {
        sChavePesquisa = 'Pessoas.Fantasia';
        sPesquisa = ' AND Pessoas.Fantasia';
    }
    else if (selChavePesquisa.value == 3) {
        sPesquisa = 'AND Documentos.Numero';
    }

    strSQLSelect = 'SELECT TOP 100 ' + sChavePesquisa + ' AS fldName, Pessoas.PessoaID AS fldID, Pessoas.Fantasia AS Fantasia, Documentos.Numero AS Numero, ' +
        'Cidades.Localidade AS Cidade, UFs.CodigoLocalidade2 AS UF, Paises.CodigoLocalidade2 AS Pais, ' +
        '(CASE WHEN (SELECT COUNT (Pessoas1.PessoaID) ' +
        'FROM RelacoesPessoas RelPessoas1 WITH (NOLOCK), RelacoesPessoas RelPessoas2 WITH (NOLOCK), Pessoas Pessoas1 WITH (NOLOCK) ' +
        'WHERE (Pessoas.PessoaID = RelPessoas1.SujeitoID AND RelPessoas1.TipoRelacaoID = 21 AND ' +
        'RelPessoas1.EstadoID = 2 AND RelPessoas1.ObjetoID = RelPessoas2.SujeitoID AND ' +
        'RelPessoas2.TipoRelacaoID = 21 AND RelPessoas2.EstadoID = 2 AND RelPessoas2.ObjetoID = ' + glb_nEmpresaID + ' AND ' +
        'RelPessoas2.SujeitoID = Pessoas1.PessoaID AND Pessoas1.EstadoID = 2) ) >0 THEN 2 ELSE 1 END ) AS Parceiro ';

    strSQLFrom = 'FROM RelacoesPessoas RelPessoas WITH (NOLOCK) ' +
        'INNER JOIN Pessoas WITH (NOLOCK) ON (RelPessoas.SujeitoID = Pessoas.PessoaID) ' +
        'INNER JOIN Pessoas_Documentos Documentos WITH (NOLOCK) ON (Pessoas.PessoaID = Documentos.PessoaID) ' +
        'INNER JOIN Pessoas_Enderecos Enderecos WITH (NOLOCK) ON (Pessoas.PessoaID = Enderecos.PessoaID) ' +
        'LEFT OUTER JOIN Localidades Cidades WITH (NOLOCK) ON (Enderecos.CidadeID = Cidades.LocalidadeID) ' +
        'LEFT OUTER JOIN Localidades UFs WITH (NOLOCK) ON (Enderecos.UFID = UFs.LocalidadeID) ' +
        'LEFT OUTER JOIN Localidades Paises WITH (NOLOCK) ON (Enderecos.PaisID = Paises.LocalidadeID) ';

    strSQLWhere = 'WHERE (RelPessoas.ObjetoID = ' + glb_nEmpresaID + ' AND RelPessoas.TipoRelacaoID = 21 AND RelPessoas.EstadoID = 2 AND ' +
        'Pessoas.EstadoID = 2 AND ' +
        '((Documentos.TipoDocumentoID = 101 OR Documentos.TipoDocumentoID = 111)) AND (Enderecos.Ordem = 1)) ' +
        sPesquisa + ' >= ' + '\'' + strPesquisa + '\'' + ' ';

    strSQLOrder = ((selChavePesquisa.value == 0) ? 'ORDER BY Pessoas.PessoaID' : 'ORDER BY fldName');

    strSQL = strSQLSelect + strSQLFrom + strSQLWhere + strSQLOrder;

    setConnection(dsoPesq);
    dsoPesq.SQL = strSQL;
    dsoPesq.ondatasetcomplete = dsopesq_DSC;
    dsoPesq.Refresh();
}

/********************************************************************
Retorno da dso da Pesquisa e constru��o do grid.
********************************************************************/
function dsopesq_DSC() {
    var sChavePesquisa = 'Nome';
    if (selChavePesquisa.value == 1)
        sChavePesquisa = 'Fantasia';

    startGridInterface(fg);
    fg.FrozenCols = 0;

    headerGrid(fg, [sChavePesquisa,
        'ID',
        'Fantasia',
        'Documento',
        'Cidade',
        'UF',
        'Pa�s',
        'Parceiro'], [2, 7]);

    fillGridMask(fg, dsoPesq, ['fldName',
        'fldID',
        'Fantasia',
        'Numero',
        'Cidade',
        'UF',
        'Pais',
        'Parceiro'], ['', '', '', '', '', '', '', '']);
    fg.FrozenCols = 1;

    lockControlsInModalWin(false);

    if (fg.Rows > 1) {
        btnOK.disabled = false;
        window.focus();
        fg.focus();
    }
    else {
        btnOK.disabled = true;
        txtParceiro.focus();
    }
}

/********************************************************************
Clique com o bot�o OK ao clicar em enter.
********************************************************************/
function fg_KeyPress(KeyAscii) {
    if ((KeyAscii == 13) && (fg.Row > 0)) {
        btn_onclick(btnOK);
    }
}
