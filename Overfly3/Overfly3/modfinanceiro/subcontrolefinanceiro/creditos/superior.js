/*****************************************************************************************************
Declara��o de vari�veis globais.
******************************************************************************************************/
var dsoSup01 = new CDatatransport("dsoSup01");
var dsoCmbDynamic01 = new CDatatransport("dsoCmbDynamic01");
var dsoCmbDynamic02 = new CDatatransport("dsoCmbDynamic02");
var dsoCmbDynamic03 = new CDatatransport("dsoCmbDynamic03");
var dsoCmbDynamic04 = new CDatatransport("dsoCmbDynamic04");
var dsoStateMachine = new CDatatransport("dsoStateMachine");
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
var dsoVerificacao = new CDatatransport("dsoVerificacao");
var dsoVerificacaoInclusao = new CDatatransport("dsoVerificacaoInclusao");
var dsoDadosPessoa = new CDatatransport("dsoDadosPessoa");
var glb_CounterCmbsDynamics = 0;
var glb_nUserID = glb_USERID;
var glb_EmpresaID = getCurrEmpresaData();
var glb_sMensagemErro = '';

/*****************************************************************************************************
WindowOnLoad.
******************************************************************************************************/
function window_onload() {

    initInterfaceLoad(window.top.formName, 0X7);

    glb_aStaticCombos = ([['selSeguradoraID', '1']]);

    windowOnLoad_1stPart();

    glb_aFilePath = new Array(SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/creditos/inferior.asp',
        SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/creditos/pesquisa.asp');

    linkDivsAndSubForms('divSup01_01', null, null);

    windowOnLoad_2ndPart();

    glb_sFldIDName = 'CreditoID';

    glb_sCtlTipoRegistroID = '';

    glb_sFldTipoRegistroName = '';
}   

/*****************************************************************************************************
Configura��o do HTML da p�gina.
******************************************************************************************************/
function setupPage() {

    adjustDivs('sup', [[1, 'divSup01_01']]);

    adjustElementsInForm([['lblRegistroID', 'txtRegistroID', 10, 1],
    ['lblEstadoID', 'txtEstadoID', 2, 1],
    ['lblParceiroID', 'selParceiroID', 20, 1],
    ['btnFindParceiro', 'btn', 21, 1],
    ['lbldtSolicitacao', 'txtdtSolicitacao', 20, 1],
    ['lblSolicitanteID', 'selSolicitanteID', 20, 1],
    ['lblPrioridade', 'chkPrioridade', 3, 1],
    ['lblPaisID', 'selPaisID', 15, 2],
    ['lblSeguradoraID', 'selSeguradoraID', 20, 2],
    ['lblMoedaID', 'selMoedaID', 10, 2],
    ['lblValorSolicitado', 'txtValorSolicitado', 15, 2],
    ['lblPotencialCredito', 'txtPotencialCredito', 15, 2],
    ['lblValorConcedido', 'txtValorConcedido', 15, 2],
    ['lbldtConcessao', 'txtdtConcessao', 15, 3],
    ['lbldtInicio', 'txtdtInicio', 15, 3],
    ['lbldtFim', 'txtdtFim', 15, 3],
    ['lbldtCancelamento', 'txtdtCancelamento', 15, 3],
    ['lblObservacao', 'txtObservacao', 50, 4]], null, null, true);

}

// CARRIER **********************************************************

/********************************************************************
DISPARAR UM CARRIER:
Usar a seguinte funcao:

sendJSCarrier(getHtmlId(),param1, param2);

Onde:
getHtmlId() - fixo, remete o id do html que disparou o carrier.
param1      - qualquer coisa. De uso do programador.
param2      - qualquer coisa. De uso do programador.

********************************************************************/

/********************************************************************
Funcao disparada pelo frame work.
CARRIER CHEGANDO NO ARQUIVO.
Executa quando um carrier e disparado por outro form.

Parametros: 
idElement   - id do html que disparou o carrier
idBrowser   - id do browser que disparou o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Se intercepta sempre retornar o seguinte array:
array[0] = getHtmlId() ou null
array[1] = qualquer coisa ou null
array[2] = qualquer coisa ou null
Se nao intercepta retornar null
********************************************************************/
function carrierArrived(idElement, idBrowser, param1, param2) {
    if (param1 == 'SHOWCREDITO') {
        // Da automacao, deve constar de todos os ifs do carrierArrived
        if (__currFormState() != 0)
            return null;

        // param2 - traz array:
        // o id da empresa do form que mandou
        // o id do registro a abrir
        // No sendJSMessage abaixo:
        // param2[1] = null significa que nao deve paginar
        // param2[2] = null significa que nao ha array de ids

        var empresa = getCurrEmpresaData();

        if (!(param2[0] == empresa[0]))
            param2[0] = empresa[0];

        // Exclusivo para exibir detalhe em form
        showDetailByCarrier(param2[1]);

        window.top.focus();

        return new Array(getHtmlId(), null, null);
    }
    // Programador alterar conforme necessario
    return null;
}

/********************************************************************
Funcao disparada pelo frame work.
CARRIER RETORNANDO AO ARQUIVO QUE O DISPAROU.
Executa quando um carrier e disparado por este form e apos executar
nos demais forms abertos, retorna a este form.

Parametros: 
idElement   - string id do html que interceptou o carrier
idBrowser   - string id do browser que contem o form que interceptou
              o carrier
param1      - parametro um trazido pelo carrier
param2      - parametro dois trazido pelo carrier

Retorno:
Sempre retorna null
********************************************************************/
function carrierReturning(idElement, idBrowser, param1, param2) {
    // Nao mexer - Inicio de automacao ==============================
    return null;
    // Final de Nao mexer - Inicio de automacao =====================
}

// FINAL DE CARRIER *************************************************


/*****************************************************************************************************
Configura��o de controles.
******************************************************************************************************/
function putSpecialAttributesInControls() {

    // Altera a cor dos controles para verde.
    lblParceiroID.style.color = lblSeguradoraID.style.color = lblPotencialCredito.style.color = lbldtInicio.style.color =
    lblValorSolicitado.style.color = lbldtFim.style.color = lblObservacao.style.color = 'green';

    // Altera a cor dos controles para azul.
    lblValorConcedido.style.color = 'blue';

    // Desabilita controles.
    txtdtSolicitacao.disabled = txtdtConcessao.disabled = txtdtCancelamento.disabled =
    selSolicitanteID.disabled = selParceiroID.disabled = selPaisID.disabled = selMoedaID.disabled = true;
}

/*****************************************************************************************************
Preenche combos e ajusta os labels.
******************************************************************************************************/
function prgServerSup(btnClicked) {

    glb_BtnFromFramWork = btnClicked;
    startDynamicCmbs();
    finalOfSupCascade(glb_BtnFromFramWork);
}

/*****************************************************************************************************
Clique na lupa.
******************************************************************************************************/
function btnLupaClicked(btnClicked) {
    if (btnClicked.id == 'btnFindParceiro')
        openModalParceiro();
}

/*****************************************************************************************************
Abre modalparceiro.
******************************************************************************************************/
function openModalParceiro() {
    var htmlPath;
    var strPars = new Array();
    var nCurrEmpresa = glb_EmpresaID[0];
    var nContextoID = getCmbCurrDataInControlBar('sup', 1);

    strPars = '?nEmpresaID=' + escape(nCurrEmpresa);
    strPars += '&nContextoID=' + escape(nContextoID[1]);

    htmlPath = SYS_PAGESURLROOT + '/modfinanceiro/subcontrolefinanceiro/creditos/modalpages/modalparceiro.asp' + strPars;
    showModalWin(htmlPath, new Array(571, 284));
}

/*****************************************************************************************************
Insere ID na Label.
******************************************************************************************************/
function adjustLabelsCombos(bPaging) {

    if (bPaging) {
        setLabelOfControl(lblParceiroID, dsoSup01.recordset['ParceiroID'].value);
        setLabelOfControl(lblSolicitanteID, dsoSup01.recordset['SolicitanteID'].value);
        setLabelOfControl(lblSeguradoraID, dsoSup01.recordset['SeguradoraID'].value);
    } else {
        setLabelOfControl(lblParceiroID, selParceiroID.value);
        setLabelOfControl(lblSolicitanteID, selSolicitanteID.value);
        setLabelOfControl(lblSeguradoraID, selSeguradoraID.value);
    }

}

/*****************************************************************************************************
Preenche o combo de parceiro.
******************************************************************************************************/
function fillComboParceiro(aParceiro) {
    clearComboEx(['selParceiroID']);
    var oldDataSrc = selParceiroID.dataSrc;
    var oldDataFld = selParceiroID.dataFld;
    selParceiroID.dataSrc = '';
    selParceiroID.dataFld = '';
    var oOption = document.createElement("OPTION");
    oOption.text = aParceiro[0];
    oOption.value = aParceiro[1];
    selParceiroID.add(oOption);
    selParceiroID.dataSrc = oldDataSrc;
    selParceiroID.dataFld = oldDataFld;

    if (selParceiroID.options.length != 0) {
        setValueInControlLinked(selParceiroID, dsoSup01);
        adjustLabelsCombos(true);
    }
    else
        restoreInterfaceFromModal();
}

/*****************************************************************************************************
Monta DSO do combo de solicitante e parceiro.
******************************************************************************************************/
function startDynamicCmbs() {

    glb_CounterCmbsDynamics = 4;

    setConnection(dsoCmbDynamic01);

    dsoCmbDynamic01.SQL = 'SELECT PessoaID as fldID,Fantasia as fldName ' +
        'FROM Pessoas WITH(NOLOCK) ' +
        'WHERE PessoaID = ' + dsoSup01.recordset['SolicitanteID'].value;
    dsoCmbDynamic01.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic01.Refresh();

    setConnection(dsoCmbDynamic02);

    dsoCmbDynamic02.SQL = 'SELECT PessoaID as fldID,Fantasia as fldName ' +
        'FROM Pessoas WITH(NOLOCK) ' +
        'WHERE PessoaID = ' + dsoSup01.recordset['ParceiroID'].value;
    dsoCmbDynamic02.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic02.Refresh();

    setConnection(dsoCmbDynamic03);

    dsoCmbDynamic03.SQL = 'SELECT LocalidadeID as fldID,Localidade as fldName ' +
        'FROM Localidades WITH(NOLOCK) ' +
        'WHERE LocalidadeID = ' + dsoSup01.recordset['PaisID'].value;
    dsoCmbDynamic03.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic03.Refresh();


    setConnection(dsoCmbDynamic04);

    dsoCmbDynamic04.SQL = 'SELECT ConceitoID as fldID,SimboloMoeda as fldName ' +
        'FROM Conceitos WITH(NOLOCK) ' +
        'WHERE ConceitoID = ' + dsoSup01.recordset['MoedaID'].value;
    dsoCmbDynamic04.ondatasetcomplete = dsoCmbDynamic_DSC;
    dsoCmbDynamic04.Refresh();

}

/*****************************************************************************************************
Retorna o DSO dos combos.
******************************************************************************************************/
function dsoCmbDynamic_DSC() {
    var optionStr, optionValue;
    var aCmbsDynamics = [selSolicitanteID, selParceiroID, selPaisID, selMoedaID];
    var aDSOsDynamics = [dsoCmbDynamic01, dsoCmbDynamic02, dsoCmbDynamic03, dsoCmbDynamic04];
    var oldDataSrc;
    var oldDataFld;
    var oOption;
    var i;
    var lFirstRecord;
    var nQtdCmbs = 4;

    clearComboEx(['selSolicitanteID', 'selParceiroID', 'selPaisID', 'selMoedaID']);

    glb_CounterCmbsDynamics--;

    if (glb_CounterCmbsDynamics == 0) {
        for (i = 0; i < nQtdCmbs; i++) {
            oldDataSrc = aCmbsDynamics[i].dataSrc;
            oldDataFld = aCmbsDynamics[i].dataFld;
            aCmbsDynamics[i].dataSrc = '';
            aCmbsDynamics[i].dataFld = '';
            lFirstRecord = true;
            while (!aDSOsDynamics[i].recordset.EOF) {
                optionStr = aDSOsDynamics[i].recordset['fldName'].value;
                optionValue = aDSOsDynamics[i].recordset['fldID'].value;
                var oOption = document.createElement("OPTION");
                oOption.text = optionStr;
                oOption.value = optionValue;

                aCmbsDynamics[i].add(oOption);
                aDSOsDynamics[i].recordset.MoveNext();
                lFirstRecord = false;
            }
            aCmbsDynamics[i].dataSrc = oldDataSrc;
            aCmbsDynamics[i].dataFld = oldDataFld;
        }

        adjustLabelsCombos(true);
        finalOfSupCascade(glb_BtnFromFramWork);
    }
    return null;
}

/*****************************************************************************************************
Recebe todos os onchange de combos.
 ******************************************************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    verifyCreditoAlreadyExists();
    adjustLabelsCombos(false);
}

/*****************************************************************************************************
Propagada por janela modal que abriu ou fechou.
 ******************************************************************************************************/
function modalInformForm(idElement, param1, param2) {
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {

            restoreInterfaceFromModal();

            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {

            restoreInterfaceFromModal();

            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {

            restoreInterfaceFromModal();

            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
        else if (param1 == 'CANCEL') {

            restoreInterfaceFromModal();

            writeInStatusBar('child', 'cellMode', 'Detalhe');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {

            restoreInterfaceFromModal();

            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {

            restoreInterfaceFromModal();

            writeInStatusBar('child', 'cellMode', 'Detalhe');


            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALPARCEIROHTML') {
        if (param1 == 'OK') {
            fillComboParceiro(param2);
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            verifyCreditoAlreadyExists();
            return 0;
        }
        else if (param1 == 'CANCEL') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
    else if (idElement.toUpperCase() == 'MODALSOCIOSHTML') {
        if (param1 == 'OK') {
            fillComboParceiro(param2);
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            verifyCreditoAlreadyExists();
            return 0;
        }
        else if (param1 == 'CANCEL') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Altera��o');
            return 0;
        }
    }
}

/*****************************************************************************************************
Esta funcao faz a chamada de todas as operacoes particulares.
 ******************************************************************************************************/
function prgInterfaceSup(btnClicked) {
    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDCHANGEDIVSUP', btnClicked);
}

/*****************************************************************************************************
Esta funcao tem que ser chamada nas �ltimas funcoes de retorno de dados do servidor, feitas pelo programador.
 ******************************************************************************************************/
function finalOfSupCascade(btnClicked) {

    sendJSMessage(getHtmlId(), JS_INFSYS, 'ENDPROGSERVERSUP', btnClicked);
}

/*****************************************************************************************************
Informa que o sup esta em modo de edicao e a interface ja esta destravada.
 ******************************************************************************************************/
function supInitEditMode() {
    controlReadOnlyFields(true);
    supInitEditMode_Continue();
}

/*****************************************************************************************************
Usuario clicou um botao nao especifico do control bar.
 ******************************************************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {

    if (btnClicked == 'SUPOK')
    {

        if ((dsoSup01.recordset[glb_sFldIDName].value == null))
        {
            if (glb_sMensagemErro == '') {
                dsoSup01.recordset['SolicitanteID'].value = glb_nUserID;
                dsoSup01.recordset['PaisID'].value = glb_EmpresaID[1];
                dsoSup01.recordset['MoedaID'].value = glb_EmpresaID[9];
                startDynamicCmbs();
            }
            else {
                if (window.top.overflyGen.Alert(glb_sMensagemErro) == 0)
                    return null;

                return false;
            }
        }

    }
    else if (btnClicked == 'SUPALT')
    {
        var nEstadoID = dsoSup01.recordset['EstadoID'].value;

        if ((nEstadoID == 83) || (nEstadoID == 7) || (nEstadoID == 161) || (nEstadoID == 5))
        {
            if (window.top.overflyGen.Alert('Nesse estado n�o � permitido realizar altera��es.') == 0)
                return null;

            return false; 
        }
    }
    else if ((btnClicked == 'SUPINCL') || (btnClicked == 'SUPCANC'))
    {
        controlReadOnlyFields(false);
    }

    return null;
}

/*****************************************************************************************************
Usuario clicou botao especifico da barra.
 ******************************************************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    if ((controlBar.toUpperCase() == 'SUP') && (btnClicked == 4) && (dsoSup01.recordset[glb_sFldIDName].value != null)) {
        // Manda o id da pessoa a detalhar 
        sendJSCarrier(getHtmlId(), 'SHOWPESSOA', new Array(glb_EmpresaID[0], selParceiroID.value));
    }
    else if (btnClicked == 3) {
        window.top.openModalControleDocumento('S', '', 720001, null, '22', 'T');
    }
    else if ((controlBar.toUpperCase() == 'SUP') && (btnClicked == 5) && (dsoSup01.recordset[glb_sFldIDName].value != null)) {
        window.top.openModalHTML(dsoSup01.recordset[glb_sFldIDName].value, 'Resumo do Cr�dito', 'S', 12);
    }
}

/*****************************************************************************************************
Form carregou e vai mostrar a interface.
 ******************************************************************************************************/
function formFinishLoad() {
    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Pessoa', 'Resumo']);
}

/*****************************************************************************************************
Dispara logo apos abrir a maquina de estado.
 ******************************************************************************************************/
function stateMachOpened(currEstadoID) {

}

/*****************************************************************************************************
Usuario clicou OK na maquina de estado.
 ******************************************************************************************************/
function stateMachBtnOK(currEstadoID, newEstadoID) {
    verifyCreditosInServer(currEstadoID, newEstadoID);
    return true;
}

/*****************************************************************************************************
Dispara se o usuario trocou o estado.
 ******************************************************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID, bSavedID) {

}

/*****************************************************************************************************
Realiza verifica��o de cr�dito.
 ******************************************************************************************************/
function verifyCreditosInServer(currEstadoID, newEstadoID) {
    var nCreditoID = dsoSup01.recordset['CreditoID'].value;
    var strPars = new String();

    strPars = '?nCreditoID=' + escape(nCreditoID);
    strPars += '&nEstadoDeID=' + escape(currEstadoID);
    strPars += '&nEstadoParaID=' + escape(newEstadoID);

    dsoVerificacao.URL = SYS_ASPURLROOT + '/modfinanceiro/subcontrolefinanceiro/creditos/serverside/verificacaocredito.aspx' + strPars;
    dsoVerificacao.ondatasetcomplete = verifyCreditosInServer_DSC;
    dsoVerificacao.refresh();
}

/*****************************************************************************************************
Retorno da verifica��o de cr�dito.
 ******************************************************************************************************/
function verifyCreditosInServer_DSC() {

    var sMensagem = (dsoVerificacao.recordset['Mensagem'].value == null ? '' : dsoVerificacao.recordset['Mensagem'].value);
    var nResultado = (dsoVerificacao.recordset['Resultado'].value == null ? 0 : dsoVerificacao.recordset['Resultado'].value);

    if (nResultado == 0) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }

        stateMachSupExec('OK');
    }
    else if (nResultado == 1) {
        var _retConf = window.top.overflyGen.Confirm(sMensagem);

        if (_retConf == 0)
            return null;
        else if (_retConf == 1)
            stateMachSupExec('OK');
        else
            stateMachSupExec('CANC');
    }
    else if (nResultado == 2) {
        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;
        }
        stateMachSupExec('CANC');
    }
}    

/*****************************************************************************************************
Verifica se j� existe cr�dito deste parceiro.
 ******************************************************************************************************/
function verifyCreditoAlreadyExists() {

    var nParceiroID = selParceiroID.value;
    var nSeguradoraID = selSeguradoraID.value;
    var nValorSolicitado = txtValorSolicitado.value;
    var nValorConcedido = txtValorConcedido.value;

    if (nParceiroID == '') {
        return false;
    }

    if ((nSeguradoraID == '') || (nSeguradoraID == 0)) {
        nSeguradoraID = 'NULL';
    }

    if (nValorSolicitado == '') {
        nValorSolicitado = '0';
    }

    if (nValorConcedido == '') {
        nValorConcedido = '0';
    }


    setConnection(dsoVerificacaoInclusao);

    dsoVerificacaoInclusao.SQL = 'SELECT a.CreditoID, a.EstadoID, a.ValorSolicitado, a.ValorConcedido ' +
        'FROM Creditos a ' +
        'WHERE (a.ParceiroID = ' + nParceiroID + ' ) ' +
        'AND (a.PaisID = ' + glb_EmpresaID[1]  + ') ' +
        'AND (((' + nValorSolicitado + ' <= ISNULL(a.ValorSolicitado, 0)) AND (a.EstadoID IN (1, 9, 34))) OR ' +
        '((' + nValorConcedido + ' <= ISNULL(a.ValorConcedido, 0)) AND (a.EstadoID = 75))) ' +
        'AND (((a.SeguradoraID IS NULL) AND (' + nSeguradoraID + ' IS NULL)) ' +
        'OR ((a.SeguradoraID IS NOT NULL) AND (' + nSeguradoraID + ' = a.SeguradoraID))) ' +
        'ORDER BY a.CreditoID DESC ';
    dsoVerificacaoInclusao.ondatasetcomplete = verifyCreditoAlreadyExists_DSC;
    dsoVerificacaoInclusao.Refresh();
}

/*****************************************************************************************************
Retorno da verifica��o se j� existe cr�dito deste parceiro.
 ******************************************************************************************************/
function verifyCreditoAlreadyExists_DSC() {
    glb_sMensagemErro = '';

    var nEstadoID;

    while (!dsoVerificacaoInclusao.recordset.EOF) {
        nEstadoID = dsoVerificacaoInclusao.recordset['EstadoID'].value;
        nCreditoID = dsoVerificacaoInclusao.recordset['CreditoID'].value;
        nValorSolicitado = (dsoVerificacaoInclusao.recordset['ValorSolicitado'].value == null ? '0' : dsoVerificacaoInclusao.recordset['ValorSolicitado'].value);
        nValorConcedido = (dsoVerificacaoInclusao.recordset['ValorConcedido'].value == null ? '0' : dsoVerificacaoInclusao.recordset['ValorConcedido'].value);

        if (glb_sMensagemErro != '')
            glb_sMensagemErro += '\n';

        if (glb_sMensagemErro == '')
            glb_sMensagemErro += 'Os seguintes cr�ditos j� foram concedidos ou solicitados: \n';

        if (((nEstadoID == 1) || (nEstadoID == 9) || (nEstadoID == 34))) {
            glb_sMensagemErro += 'ID: ' + nCreditoID + '. Valor Solicitado: ' + nValorSolicitado + '.';
        }

        if (nEstadoID == 75) {
            glb_sMensagemErro += 'ID: ' + nCreditoID + '. Valor Concedido: ' + nValorConcedido + '.';
        }

        dsoVerificacaoInclusao.recordset.MoveNext();
    }

}

/*****************************************************************************************************
Controla campos disabilitados.
 ******************************************************************************************************/
function controlReadOnlyFields(bAlteracao) {

    if (bAlteracao)
    {
        var nEstadoID = dsoSup01.recordset['EstadoID'].value;

        if (nEstadoID == 75)
        {
            selParceiroID.disabled = true;
            lockBtnLupa(btnFindParceiro, true);
            selSeguradoraID.disabled = true;
            txtdtInicio.disabled = true;
        }
    }
    else
    {
        selParceiroID.disabled = false;
        lockBtnLupa(btnFindParceiro, false);
        selSeguradoraID.disabled = false;
        txtdtInicio.disabled = false;
    }

    return null;
}
