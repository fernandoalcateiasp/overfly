<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="creditosinf01Html" name="creditosinf01Html">

<head>

<title></title>

<%

    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf

    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_gridsex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailinf.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/creditos/inferior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/creditos/inferioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">

</SCRIPT>

<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPress>
 fg_KeyPress(arguments[0]);
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=KeyPressEdit>
 js_fg_KeyPressEdit (arguments[0], arguments[1], arguments[2]);
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=EnterCell>
 js_fg_EnterCell (fg);
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterRowColChange>
js_fg_AfterRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseUp>
 fg_MouseUp();
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseDown>
 fg_MouseDown();
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeEdit>
 fg_BeforeEdit();
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=DblClick>
 fg_DblClick();
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ChangeEdit>
 fg_ChangeEdit();
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=ValidateEdit>
 fg_ValidateEdit();
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=BeforeRowColChange>
 js_fg_BeforeRowColChange (fg, arguments[0], arguments[1], arguments[2], arguments[3], arguments[4]);
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=MouseMove>
 js_fg_MouseMove (fg, arguments[0], arguments[1], arguments[2], arguments[3]);
</SCRIPT>
<SCRIPT LANGUAGE=javascript FOR=fg EVENT=AfterEdit>
js_Creditos_AfterEdit(fg.Row, fg.Col);
</SCRIPT>

</head>

<body id="creditosinf01Body" name="creditosinf01Body" LANGUAGE="javascript" onload="return window_onload()">

    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
    <div id="divInf01_01" name="divInf01_01" class="divExtern">
        <textarea id="txtObservacoes" name="txtObservacoes" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Observacoes"></textarea>
    </div>
    
    <div id="divInf01_02" name="divInf01_02" class="divExtern">
        <p  id="lblProprietario" name="lblProprietario" class="lblGeneral">Propriet�rio</p>
        <select id="selProprietario" name="selProprietario" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Proprietarioid"></select>

	    <input type="image" id="btnFindProprietario" name="btnFindProprietario" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">
        <p id="lblAlternativo" name="lblAlternativo" class="lblGeneral">Alternativo</p>
        <select id="selAlternativo" name="selAlternativo" class="fldGeneral" DATASRC="#dso01JoinSup" DATAFLD="Alternativoid"></select>

	    <input type="image" id="btnFindAlternativo" name="btnFindAlternativo" class="fldGeneral" title="Preencher Combo" WIDTH="24" HEIGHT="23">
    </div>

    <div id="divInf01_03" name="divInf01_03" class="divExtern">
        <p  id="lblFiliais" name="lblFiliais" class="lblGeneral">Filiais</p>
		<input type="text" id="txtFiliais" name="txtFiliais" DATASRC="#dso01JoinSup" DATAFLD="Filiais" class="fldGeneral"></input>

        <p  id="lblConsulta1" name="lblConsulta1" class="lblGeneral">Consulta 1</p>
		<input type="text" id="txtConsulta1" name="txtConsulta1" DATASRC="#dso01JoinSup" DATAFLD="Consulta1" class="fldGeneral"></input>

        <p  id="lblConsulta2" name="lblConsulta2" class="lblGeneral">Consulta 2</p>
		<input type="text" id="txtConsulta2" name="txtConsulta2" DATASRC="#dso01JoinSup" DATAFLD="Consulta2" class="fldGeneral"></input>

        <p  id="lblPontualidade" name="lblPontualidade" class="lblGeneral">Pontualidade (%)</p>
		<input type="text" id="txtPontualidade" name="txtPontualidade" DATASRC="#dso01JoinSup" DATAFLD="Pontualidade" class="fldGeneral"></input>

        <p  id="lblValorUltimaCompra" name="lblValorUltimaCompra" class="lblGeneral">Valor �ltima Compra</p>
		<input type="text" id="txtValorUltimaCompra" name="txtValorUltimaCompra" DATASRC="#dso01JoinSup" DATAFLD="ValorUltimaCompra" class="fldGeneral"></input>

        <p  id="lbldtUltimaCompra" name="lbldtUltimaCompra" class="lblGeneral">Data �ltima Compra</p>
		<input type="text" id="txtdtUltimaCompra" name="txtdtUltimaCompra" DATASRC="#dso01JoinSup" DATAFLD="V_dtUltimaCompra" class="fldGeneral"></input>

        <p  id="lblProtestos" name="lblProtestos" class="lblGeneral">Protestos</p>
		<input type="text" id="txtProtestos" name="txtProtestos" DATASRC="#dso01JoinSup" DATAFLD="Protestos" class="fldGeneral"></input>

        <p  id="lblValorProtestos" name="lblValorProtestos" class="lblGeneral">Valor Protestos</p>
		<input type="text" id="txtValorProtestos" name="txtValorProtestos" DATASRC="#dso01JoinSup" DATAFLD="ValorProtestos" class="fldGeneral"></input>

        <p  id="lblAcoesJudiciais" name="lblAcoesJudiciais" class="lblGeneral">A��es Judiciais</p>
		<input type="text" id="txtAcoesJudiciais" name="txtAcoesJudiciais" DATASRC="#dso01JoinSup" DATAFLD="AcoesJudiciais" class="fldGeneral"></input>

        <p  id="lblValorAcoesJudiciais" name="lblValorAcoesJudiciais" class="lblGeneral">Valor A��es Judiciais</p>
		<input type="text" id="txtValorAcoesJudiciais" name="txtValorAcoesJudiciais" DATASRC="#dso01JoinSup" DATAFLD="ValorAcoesJudiciais" class="fldGeneral"></input>
    </div>

    <div id="divInf01_04" name="divInf01_04" class="divExtern">
        <OBJECT CLASSID="clsid:D76D712E-4A96-11D3-BD95-D296DC2DD072" id="fg" name="fg" VIEWASTEXT>
        </OBJECT>
        <img id="hr_L_FGBorder" name="hr_L_FGBorder" class="lblGeneral"></img>
        <img id="hr_R_FGBorder" name="hr_R_FGBorder" class="lblGeneral"></img>
        <img id="hr_B_FGBorder" name="hr_B_FGBorder" class="lblGeneral"></img>
    </div>

</body>

</html>
