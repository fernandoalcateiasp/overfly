/*****************************************************************************************************
Declara��o de vari�veis globais.
 ******************************************************************************************************/
var glb_nFormID = window.top.formID;

/*****************************************************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe.
 ******************************************************************************************************/
function setSqlToCurrDso(dso, folderID, idToFind, sFiltro, nTipoRegistroID) {
    var nEmpresaData = getCurrEmpresaData();

    setConnection(dso);

    if ((folderID == 20008) || (folderID == 20009)) {
        dso.SQL = 'SELECT a.CreditoID, a.ProprietarioID,a.AlternativoID,a.Observacoes FROM Creditos a WITH(NOLOCK) WHERE ' +
            sFiltro + 'a.CreditoID = ' + idToFind;
        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 20010) {
        dso.SQL = 'SELECT a.LOGID, a.FormID, a.SubFormID, a.RegistroID, a.Data, a.UsuarioID, a.EventoID, a.EstadoID, ISNULL(a.Motivo, SPACE(1)) AS Motivo, ' +
            'dbo.fn_Data_Fuso(a.Data, NULL, ' + nEmpresaData[2] + ') AS DataFuso, b.Campo, (CASE a.EventoID WHEN 28 THEN SPACE(1) ELSE b.Conteudo END) AS Conteudo, ' +
            'dbo.fn_Log_Mudanca(b.LogDetalheID) AS Mudanca ' +
            'FROM LOGs a WITH(NOLOCK) ' +
            'LEFT OUTER JOIN LOGs_Detalhes b WITH(NOLOCK) ON b.LOGID=a.LOGID ' +
            'WHERE ' + sFiltro + ' a.RegistroID = ' + idToFind + ' AND a.FormID=' + glb_nFormID + ' ' +
            'ORDER BY a.SubFormID, a.Data DESC';
        return 'dso01Grid' + '_DSC';
    }
    else if (folderID == 29093) {
        dso.SQL = 'SELECT a.* FROM Creditos_Garantias a WITH(NOLOCK) ' +
            'WHERE ' + sFiltro + ' a.CreditoID = ' + idToFind;

        return 'dso01Grid_DSC';
    }
    else if (folderID == 31123) {
        dso.SQL = 'SELECT a.*, CONVERT(VARCHAR,a.dtUltimaCompra, 103) AS V_dtUltimaCompra ' +
            'FROM Creditos a WITH(NOLOCK) ' +
            'WHERE ' + sFiltro + ' a.CreditoID = ' + idToFind;

        return 'dso01JoinSup_DSC';
    }
    else if (folderID == 40226) {
        dso.SQL = 'SELECT TOP 100 a.CreditoID AS [ID], ' +
            'b.RecursoAbreviado AS Estado, ' +
            'e.CodigoLocalidade3 AS Pais,  ' +
            'd.Fantasia AS Seguradora,  ' +
            'a.dtSolicitacao,  ' +
            'd.Fantasia AS Solicitante,  ' +
            'a.dtConcessao,  ' +
            'a.dtFim, ' +
            'f.SimboloMoeda AS Moeda, ' +
            'a.ValorSolicitado, ' +
            'a.PotencialCredito, ' +
            'a.ValorConcedido, ' +
            'a.Observacao, ' +
            'CONVERT(VARCHAR(50), SPACE(0)) AS fldError ' +
            'FROM Creditos a WITH(NOLOCK) ' +
            'INNER JOIN Recursos b WITH(NOLOCK) ON(b.RecursoID = a.EstadoID) ' +
            'INNER JOIN Pessoas c WITH(NOLOCK) ON(c.PessoaID = a.SolicitanteID) ' +
            'LEFT JOIN Pessoas d WITH(NOLOCK) ON(d.PessoaID = a.SeguradoraID) ' +
            'INNER JOIN Localidades e WITH(NOLOCK) ON(e.LocalidadeID = a.PaisID) ' +
            'INNER JOIN Conceitos f WITH(NOLOCK) ON(f.ConceitoID = a.MoedaID) ' +
            'WHERE(a.CreditoID <> ' + idToFind + ') AND (a.ParceiroID = ' + sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'ParceiroID\'].value') + ') ' +
            'ORDER BY a.CreditoID DESC';

        return 'dso01Grid_DSC';
    }

}

/*****************************************************************************************************
Monta string de select para combo dentro de grid.
 ******************************************************************************************************/
function setSqlToDsoCmbOrLkpGrid(dso, pastaID, nRegistroID, nTipoRegistroID) {
    var nEmpresaData = getCurrEmpresaData();
    setConnection(dso);
    var nEmpresaAlternativaID = 0;

    if (nEmpresaData[0] == 2)
        nEmpresaAlternativaID = 4;
    else
        nEmpresaAlternativaID = nEmpresaData[0];

    if (pastaID == 20010) {
        if (dso.id == 'dso01GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoFantasia ' +
                'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                'WHERE (a.FormID=' + glb_nFormID + ' ' +
                'AND a.RegistroID = ' + nRegistroID + ' AND a.SubFormID = b.RecursoID)';
        }
        else if (dso.id == 'dso02GridLkp') {
            dso.SQL = 'SELECT b.PessoaID, b.Fantasia ' +
                'FROM LOGs a WITH(NOLOCK), Pessoas b WITH(NOLOCK) ' +
                'WHERE (a.FormID=' + glb_nFormID + ' ' +
                'AND a.RegistroID = ' + nRegistroID + ' AND a.UsuarioID = b.PessoaID)';
        }
        else if (dso.id == 'dso03GridLkp') {
            dso.SQL = 'SELECT b.ItemID, b.ItemAbreviado ' +
                'FROM LOGs a WITH(NOLOCK), TiposAuxiliares_Itens b WITH(NOLOCK) ' +
                'WHERE (a.FormID=' + glb_nFormID + ' ' +
                'AND a.RegistroID = ' + nRegistroID + ' AND a.EventoID = b.ItemID)';
        }
        else if (dso.id == 'dso04GridLkp') {
            dso.SQL = 'SELECT b.RecursoID, b.RecursoAbreviado ' +
                'FROM LOGs a WITH(NOLOCK), Recursos b WITH(NOLOCK) ' +
                'WHERE (a.FormID=' + glb_nFormID + ' ' +
                'AND a.RegistroID = ' + nRegistroID + ' AND a.EstadoID = b.RecursoID)';
        }
    }
    else if (pastaID == 29093) {
        if (dso.id == 'dsoCmb01Grid_01') {
            dso.SQL = 'SELECT a.ItemID, a.ItemMasculino ' +
                'FROM TiposAuxiliares_Itens a WITH(NOLOCK) ' +
                'WHERE (a.TipoID = 16 AND a.EstadoID = 2) ' +
                'ORDER BY a.Ordem';
        }
    }

}

/*****************************************************************************************************
Esta funcao preenche o combo de pastas do controlbar inferior.
 ******************************************************************************************************/
function fillCmbPastasInCtlBarInf(nTipoRegistroID) {
    var i = 0;
    var currSelection = getCmbCurrDataInControlBar('inf', 1);
    var vPastaName;

    cleanupSelInControlBar('inf', 1);

    vPastaName = window.top.getPastaName(20008);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20008);

    vPastaName = window.top.getPastaName(20009);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20009);

    vPastaName = window.top.getPastaName(20010);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 20010);

    vPastaName = window.top.getPastaName(29093);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 29093);

    vPastaName = window.top.getPastaName(31123);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 31123);

    vPastaName = window.top.getPastaName(40226);
    if (vPastaName != '')
        addOptionToSelInControlBar('inf', 1, vPastaName, 40226);

    stripFoldersByRight(currSelection);

    return true;

}

/*****************************************************************************************************
Tratar consistencia dos dados inseridos pelo usuario aqui.
 ******************************************************************************************************/
function criticDataForSave(btnClicked, folderID, tipoCtlrsPasta, currDSO, registroID) {

    if (tipoCtlrsPasta == 'GRID')
    {
        if (folderID == 29093) {

            var sMensagem = '';
            var nTipo = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'TipoGarantiaID'));
            var sGarantia = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'Garantia'));
            var sFiadorAvalista = fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'FiadorAvalista'));
            var nValor = fg.ValueMatrix(fg.Row, getColIndexByColKey(fg, 'Valor'));
            var dtInicio = trimStr(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtInicio')));
            var dtFim = trimStr(fg.TextMatrix(fg.Row, getColIndexByColKey(fg, 'dtFim')));

            if (nTipo == 0)
                sMensagem += 'Informe o Tipo.';

            if (sGarantia == '') {
                if (sMensagem != '')
                    sMensagem += '\n';

                sMensagem += 'Informe a Garantia.';
            }

            if (sFiadorAvalista == '') {
                if (sMensagem != '')
                    sMensagem += '\n';

                sMensagem += 'Informe o Fiador/Avalista.';
            }

            if (nValor == 0) {
                if (sMensagem != '')
                    sMensagem += '\n';

                sMensagem += 'Informe o Valor.';
            }

            if (dtInicio == '') {
                if (sMensagem != '')
                    sMensagem += '\n';

                sMensagem += 'Informe a Data in�cio.';
            }

            if (dtFim == '') {
                if (sMensagem != '')
                    sMensagem += '\n';

                sMensagem += 'Informe a Data Fim.';
            }

            if ((putDateInYYYYMMDD(dtInicio) > putDateInYYYYMMDD(dtFim)) && (chkDataEx(dtInicio) && chkDataEx(dtFim))) {
                if (sMensagem != '')
                    sMensagem += '\n';

                sMensagem += 'A data de in�cio n�o pode ser maior que a data fim.';
            }

            if (sMensagem != '') {

                if (window.top.overflyGen.Alert(sMensagem) == 0)
                    return null;

                currLine = -1;
            }
            else {
                currLine = criticLineGridAndFillDsoEx(fg, currDSO, keepCurrLineInGrid(), [0, 1, 2, 3, 4, 5], ['CreditoID', registroID]);
            }
        }

        if (currLine < 0)
            return false;
    }
    else if (folderID == 31123) {

        var sMensagem = '';
        var dtUltimaCompra = putDateInYYYYMMDD(txtdtUltimaCompra.value);
        var dtHoje = putDateInYYYYMMDD(getCurrDate());

        if (dtUltimaCompra.indexOf(':') > 0) {
            sMensagem += 'A data de �ltima compra cont�m caracteres inv�lidos.';
        }

        if (dtUltimaCompra > dtHoje) {
            if (sMensagem != '')
                sMensagem += '\n';

            sMensagem += 'A data de �ltima compra n�o pode ser maior que a data atual.';
        }

        if (sMensagem != '') {
            if (window.top.overflyGen.Alert(sMensagem) == 0)
                return null;

            return false;
        }

    }


    return true;
}

/*****************************************************************************************************
Constroi o corpo de um grid. Os combos sao construidos pelo frame work.
 ******************************************************************************************************/
function constructGridBody(folderID, currDSO) {
    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    fg.FrozenCols = 0;

    if (folderID == 20010) {

        headerGrid(fg, ['SubForm',
            'Data           Hora     ',
            'Colaborador',
            'Evento',
            'Est',
            'Motivo',
            'Campo',
            'De',
            'Para',
            'LOGID'], [9]);

        fillGridMask(fg, currDSO, ['^SubFormID^dso01GridLkp^RecursoID^RecursoFantasia',
            'DataFuso',
            '^UsuarioID^dso02GridLkp^PessoaID^Fantasia',
            '^EventoID^dso03GridLkp^ItemID^ItemAbreviado',
            '^EstadoID^dso04GridLkp^RecursoID^RecursoAbreviado',
            'Motivo',
            'Campo',
            'Conteudo',
            'Mudanca',
            'LOGID'],
            ['', '99/99/9999', '', '', '', '', '', '', '', ''],
            ['', dTFormat + ' hh:mm:ss', '', '', '', '', '', '', '', '']);

        alignColsInGrid(fg, [9]);

        fg.MergeCells = 4;
        fg.MergeCol(-1) = true;
    }
    else if (folderID == 29093) {

        headerGrid(fg, ['Tipo',
            'Garantia',
            'Fiador/Avalista',
            'Valor',
            'Inicio',
            'Fim',
            'Observacao',
            'CredGarantiaID'], [7]);

        fillGridMask(fg, currDSO, ['TipoGarantiaID',
            'Garantia',
            'FiadorAvalista',
            'Valor',
            'dtInicio',
            'dtFim',
            'Observacao',
            'CredGarantiaID'],
            ['', '', '', '999999999.99', '99/99/9999', '99/99/9999', '', ''],
            ['', '', '', '###,###,###.00', dTFormat, dTFormat, '', '']);

        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
        fg.Redraw = 2;
    } else if (folderID == 40226) {

        headerGrid(fg, ['ID',
            'Est',
            'Pa�s',
            'Seguradora',
            'Solicita��o',
            'Solicitante',
            'Concess�o',
            'Fim',
            'Moeda',
            'Valor Solicitado',
            'Potencial Credito',
            'Valor Concedido',
            'Observa��o'], []);

        fillGridMask(fg, currDSO, ['ID',
            'Estado',
            'Pais',
            'Seguradora',
            'dtSolicitacao',
            'Solicitante',
            'dtConcessao',
            'dtFim',
            'Moeda',
            'ValorSolicitado',
            'PotencialCredito',
            'ValorConcedido',
            'Observacao'],
            ['', '', '', '', '', '', '', '', '', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', ''],
            ['', '', '', '', '', '', '', '', '', '###,###,###,###.00', '###,###,###,###.00', '###,###,###,###.00', '']);

        gridHasTotalLine(fg, 'Totais', 0xC0C0C0, null, true, [[9, '###,###,###,###.00', 'S'], [11, '###,###,###,###.00', 'S']]);

        fg.FrozenCols = 1;
        fg.AutoSizeMode = 0;
        fg.AutoSize(0, fg.Cols - 1);
        fg.Redraw = 2;
    }

}

/*****************************************************************************************************
Controi a data atual.
 ******************************************************************************************************/
function getCurrDate() {
    var d, s = "";
    d = new Date();

    if (DATE_SQL_PARAM == 103) {
        s += padL(d.getDate().toString(), 2, '0') + "/";
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
    }
    else {
        s += padL((d.getMonth() + 1).toString(), 2, '0') + "/";
        s += padL(d.getDate().toString(), 2, '0') + "/";
    }

    s += d.getYear() + " ";

    return (s);
}
