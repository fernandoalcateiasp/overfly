<%@ LANGUAGE=VBSCRIPT EnableSessionState=False %>

<%
    Option Explicit
    Response.Expires = 0
    Dim objSvrCfg
    Dim pagesURLRoot
    
    Set objSvrCfg = Server.CreateObject("OverflySvrCfg.OverflyMTS")
    
    pagesURLRoot = objSvrCfg.PagesURLRoot(Application("appName"))
    
    Set objSvrCfg = Nothing
%>

<html id="creditossup01Html" name="creditossup01Html">

<head>

<title></title>

<%

    Response.Write "<LINK REL=" & Chr(34) & "stylesheet" & Chr(34) & " HREF=" & Chr(34) & pagesURLRoot & "/clientside/form.css" & Chr(34) & "type=" & Chr(34) & "text/css" & Chr(34) & ">" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/Defines.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordsetParser.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransportSystem.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFieldStructure.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CField.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CFields.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CRecordset.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CDatatransport.js" & Chr(34) & "></script>" & vbCrLf
	Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/overflyRDSClient/CReturnParam.js" & Chr(34) & "></script>" & vbCrLf
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_sysbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_constants.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_htmlbase.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_interface.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_interfaceex.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_statusbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_fastbuttonsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_controlsbar.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_rdsfacil.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_strings.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/js_modalwin.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_detailsup.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_common.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/system/formlibs/js_formscontrolsbar.js" & Chr(34) & "></script>" & vbCrLf                
    
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/creditos/superior.js" & Chr(34) & "></script>" & vbCrLf
    Response.Write "<script LANGUAGE=" & Chr(34) & "JavaScript" & Chr(34) & " SRC=" & Chr(34) & pagesURLRoot & "/modfinanceiro/subcontrolefinanceiro/creditos/superioresp.js" & Chr(34) & "></script>" & vbCrLf
%>

<script ID="wndJSProc" LANGUAGE="javascript">

</script>

</head>

<body id="creditossup01Body" name="creditossup01Body" LANGUAGE="javascript" onload="return window_onload()">

    <input type="text" id="txtNulo" name="txtNulo" class="fldGeneral">
    
        <div id="divSup01_01" name="divSup01_01" class="divExtern">

        <p id="lblRegistroID" name="lblRegistroID" class="lblGeneral">ID</p>
        <input type="text" id="txtRegistroID" name="txtRegistroID" class="fldGeneral">

        <p id="lblEstadoID" name="lblEstadoID" class="lblGeneral">Est</p>
        <input type="text" id="txtEstadoID" name="txtEstadoID" DATASRC="#dsoStateMachine" DATAFLD="Estado" class="fldGeneral">



        <p id="lblParceiroID" name="lblParceiroID" class="lblGeneral">Parceiro</p>
        <select id="selParceiroID" name="selParceiroID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="ParceiroID"></select>
        <input type="image" id="btnFindParceiro" name="btnFindParceiro" class="fldGeneral" title="Preencher Combo">

        <p id="lbldtSolicitacao" name="lbldtSolicitacao" class="lblGeneral">Data Solicita��o</p>
        <input type="text" id="txtdtSolicitacao" name="txtdtSolicitacao" DATASRC="#dsoSup01" DATAFLD="V_dtSolicitacao" class="fldGeneral">

        <p id="lblSolicitanteID" name="lblSolicitanteID" class="lblGeneral">Solicitante</p>
        <select id="selSolicitanteID" name="selSolicitanteID" class="fldGeneral"></select>

        <p id="lblPrioridade" name="lblPrioridade" class="lblGeneral">Pri</p>
		<input type="checkbox" id="chkPrioridade" name="chkPrioridade" DATASRC="#dsoSup01" DATAFLD="Prioridade" class="fldGeneral" Title="Solicita��o com prioridade?">

        <p id="lblPaisID" name="lblPaisID" class="lblGeneral">Pa�s</p>
        <select id="selPaisID" name="selPaisID" class="fldGeneral"></select>

        <p id="lblSeguradoraID" name="lblSeguradoraID" class="lblGeneral">Seguradora</p>
        <select id="selSeguradoraID" name="selSeguradoraID" class="fldGeneral" DATASRC="#dsoSup01" DATAFLD="SeguradoraID"></select>

        <p id="lblMoedaID" name="lblMoedaID" class="lblGeneral">Moeda</p>
        <select id="selMoedaID" name="selMoedaID" class="fldGeneral"></select>

        <p id="lblValorSolicitado" name="lblValorSolicitado" class="lblGeneral">Valor Solicitado</p>
        <input type="text" id="txtValorSolicitado" name="txtValorSolicitado" onchange="verifyCreditoAlreadyExists()" DATASRC="#dsoSup01" DATAFLD="ValorSolicitado" class="fldGeneral">

        <p id="lblPotencialCredito" name="lblPotencialCredito" class="lblGeneral">Potencial de Cr�dito</p>
        <input type="text" id="txtPotencialCredito" name="txtPotencialCredito" DATASRC="#dsoSup01" DATAFLD="PotencialCredito" class="fldGeneral">

        <p id="lblValorConcedido" name="lblValorConcedido" class="lblGeneral">Valor Concedido</p>
        <input type="text" id="txtValorConcedido" name="txtValorConcedido" onchange="return verifyCreditoAlreadyExists()" DATASRC="#dsoSup01" DATAFLD="ValorConcedido" class="fldGeneral">



        <p id="lbldtConcessao" name="lbldtConcessao" class="lblGeneral">Data Concess�o</p>
        <input type="text" id="txtdtConcessao" name="txtdtConcessao" DATASRC="#dsoSup01" DATAFLD="V_dtConcessao" class="fldGeneral">

        <p id="lbldtInicio" name="lbldtInicio" class="lblGeneral">Data In�cio</p>
        <input type="text" id="txtdtInicio" name="txtdtInicio" DATASRC="#dsoSup01" DATAFLD="V_dtInicio" class="fldGeneral">

        <p id="lbldtFim" name="lbldtFim" class="lblGeneral">Data Fim</p>
        <input type="text" id="txtdtFim" name="txtdtFim" DATASRC="#dsoSup01" DATAFLD="V_dtFim" class="fldGeneral">

        <p id="lbldtCancelamento" name="lbldtCancelamento" class="lblGeneral">Data Cancelamento</p>
        <input type="text" id="txtdtCancelamento" name="txtdtCancelamento" DATASRC="#dsoSup01" DATAFLD="V_dtCancelamento" class="fldGeneral">

        <p id="lblObservacao" name="lblObservacao" class="lblGeneral">Observa��o</p>
        <input type="text" id="txtObservacao" name="txtObservacao" DATASRC="#dsoSup01" DATAFLD="Observacao" class="fldGeneral">

    </div>
    
</body>

</html>
