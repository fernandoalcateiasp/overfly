using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using WSData;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.creditos.serverside
{
	public partial class verificacaocredito : System.Web.UI.OverflyPage
	{
        private int response;
        private string mensagem;

		private Integer creditoID;
		protected Integer nCreditoID
		{
			get { return creditoID; }
			set { creditoID = value; }
		}

		private Integer estadoDeID;
		protected Integer nEstadoDeID
		{
			get { return estadoDeID; }
			set { estadoDeID = value; }
		}

		private Integer estadoParaID;
		protected Integer nEstadoParaID
		{
			get { return estadoParaID; }
			set { estadoParaID = value; }
		}

		protected void CreditoVerifica()
		{
			ProcedureParameters[] param = new ProcedureParameters[5];

            param[0] = new ProcedureParameters(
                "@CreditoID", 
                SqlDbType.Int, 
                nCreditoID.intValue());

            param[1] = new ProcedureParameters(
                "@EstadoDeID", 
                SqlDbType.Int, 
                nEstadoDeID.intValue());
				
            param[2] = new ProcedureParameters(
                "@EstadoParaID", 
                SqlDbType.Int, 
                nEstadoParaID.intValue());

            param[3] = new ProcedureParameters(
                "@Resultado", 
                SqlDbType.Int, 
                DBNull.Value, 
                ParameterDirection.InputOutput);
			    
            param[4] = new ProcedureParameters(
                "@Mensagem", 
                SqlDbType.VarChar, 
                DBNull.Value, 
                ParameterDirection.InputOutput);

			param[4].Length = 8000;
			
            DataInterfaceObj.execNonQueryProcedure("sp_Credito_Verifica", param);
            
            // Obt�m o resultado da execu��o.
            response = int.Parse(param[3].Data.ToString());
            mensagem = param[4].Data.ToString();
		}

		protected override void PageLoad(object sender, EventArgs e)
		{
            CreditoVerifica();

            WriteResultXML(
                DataInterfaceObj.getRemoteData("select " + response + " as Resultado, '" + mensagem + "' as Mensagem")
            );

		}
	}
}
