/*****************************************************************************************************
Declara��o de vari�veis globais.
******************************************************************************************************/
var glb_EmpresaData = null;
var __inFTimer = null;
var dso01JoinSup = new CDatatransport("dso01JoinSup");
var dsoEstaticCmbs = new CDatatransport("dsoEstaticCmbs");
var dsoDynamicCmbs = new CDatatransport("dsoDynamicCmbs");
var dsoStateMachineLkp = new CDatatransport("dsoStateMachineLkp");
var dsoFiltroInf = new CDatatransport("dsoFiltroInf");
var dsoCmbsPropAlt = new CDatatransport("dsoCmbsPropAlt");
var dso01Grid = new CDatatransport("dso01Grid");
var dso01GridLkp = new CDatatransport("dso01GridLkp");
var dso02GridLkp = new CDatatransport("dso02GridLkp");
var dso03GridLkp = new CDatatransport("dso03GridLkp");
var dso04GridLkp = new CDatatransport("dso04GridLkp");
var dsoCmb01Grid_01 = new CDatatransport("dsoCmb01Grid_01");

/*****************************************************************************************************
WindowOnLoad.
******************************************************************************************************/
function window_onload() {

    dealWithGrid_Load();
    windowOnLoad_1stPart();
    glb_aStaticCombos = null;

    glb_aGridStaticCombos = [[20010, [[0, 'dso01GridLkp', '', ''],
    [0, 'dso02GridLkp', '', ''],
    [0, 'dso03GridLkp', '', ''],
    [0, 'dso04GridLkp', '', '']]],
    [29093, [[0, 'dsoCmb01Grid_01', 'ItemMasculino', 'ItemID']]]];

    linkDivsAndSubForms(null, ['divInf01_01',
        'divInf01_02',
        'divInf01_03',
        'divInf01_04'],
        [20008, 20009, 31123, [20010, 29093, 40226]]);

    windowOnLoad_2ndPart();

}

/*****************************************************************************************************
Ajusta interface da pagina
******************************************************************************************************/
function setupPage() {

    adjustElementsInForm([['lblFiliais', 'txtFiliais', 9, 1],
    ['lblConsulta1', 'txtConsulta1', 9, 1],
    ['lblConsulta2', 'txtConsulta2', 9, 1],
    ['lblPontualidade', 'txtPontualidade', 9, 1],
    ['lblValorUltimaCompra', 'txtValorUltimaCompra', 15, 1, -15],
    ['lbldtUltimaCompra', 'txtdtUltimaCompra', 15, 1],
    ['lblProtestos', 'txtProtestos', 9, 2],
    ['lblValorProtestos', 'txtValorProtestos', 12, 2],
    ['lblAcoesJudiciais', 'txtAcoesJudiciais', 12, 2],
    ['lblValorAcoesJudiciais', 'txtValorAcoesJudiciais', 15, 2]], null, 'INF');

    txtFiliais.setAttribute('minMax', new Array(0, 99999), 1);
    txtConsulta1.setAttribute('minMax', new Array(0, 99999), 1);
    txtConsulta2.setAttribute('minMax', new Array(0, 99999), 1);
    txtPontualidade.setAttribute('minMax', new Array(0, 100), 1);
    txtValorUltimaCompra.setAttribute('minMax', new Array(0, 9999999999.99), 1);
    txtProtestos.setAttribute('minMax', new Array(0, 99999), 1);
    txtValorProtestos.setAttribute('minMax', new Array(0, 9999999999.99), 1);
    txtAcoesJudiciais.setAttribute('minMax', new Array(0, 99999), 1);
    txtValorAcoesJudiciais.setAttribute('minMax', new Array(0, 9999999999.99), 1);

    glb_EmpresaData = getCurrEmpresaData();
}

/*****************************************************************************************************
Recebe todos os onchange de combos.
******************************************************************************************************/
function optChangedInCmb(cmb) {
    var cmbID = cmb.id;

    if (cmbID == 'selProprietario' || cmbID == 'selAlternativo')
        setPropAlt();
}

/*****************************************************************************************************
Faz a chamada de todas as operacoes particulares de servidor.
******************************************************************************************************/
function prgServerInf(btnClicked) {
    finalOfInfCascade(btnClicked);
}

/*****************************************************************************************************
Faz a chamada de todas as operacoes particularesdo inf.
******************************************************************************************************/
function prgInterfaceInf(btnClicked, pastaID, dso) {
    showBtnsEspecControlBar('inf', false, [1, 1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('inf', ['', '', '', '', '', '']);

    if (pastaID == 23328) {
        showBtnsEspecControlBar('inf', true, [0, 0, 0]);
        tipsBtnsEspecControlBar('inf', ['', '', '']);
    }

    sendJSMessage(getHtmlId(), JS_INFSYS, 'FINISHCHANGEDIV', btnClicked);
}

/*****************************************************************************************************
Troca de Interface.
******************************************************************************************************/
function finalOfInfCascade(btnClicked) {
    if ((btnClicked.toUpperCase()).indexOf('SUP') >= 0)

        sendJSMessage(getHtmlId(), JS_SUPSYS, 'CHANGEDIV', btnClicked);
    else

        sendJSMessage(getHtmlId(), JS_INFSYS, 'CHANGEDIV', btnClicked);

}

/*****************************************************************************************************
Propagada por janela modal que abriu ou fechou.
******************************************************************************************************/
function modalInformForm(idElement, param1, param2) {

    if (keepCurrFolder() == 23102) {
        if (idElement.toUpperCase() == 'MODALPRODUTOSHTML') {
            if (param1 == 'OK') {

                writeInStatusBar('child', 'cellMode', 'Altera��o');
            }
            else if (param1 == 'CANCEL') {

                restoreInterfaceFromModal();

                writeInStatusBar('child', 'cellMode', 'Altera��o');
            }

            __inFTimer = window.setInterval('forceInfRefr()', 50, 'JavaScript');
            return null;
        }
    }
}

/*****************************************************************************************************
Trata os controls bar sup e inf.
******************************************************************************************************/
function treatsControlsBars(btnClicked, folderID, tipoCtlrsPasta, dsoID) {
    setupEspecBtnsControlBar('sup', 'HDHHHD');
}

/*****************************************************************************************************
Clique na lupa.
******************************************************************************************************/
function btnLupaClicked(btnClicked) {
    ;
}

/*****************************************************************************************************
Usuario inseriu uma linha nova no grid.
******************************************************************************************************/
function addedLineInGrid(folderID, grid, nLineInGrid, dso) {
    ;
}

/*****************************************************************************************************
Dispara logo apos abrir a maquina de estado.
******************************************************************************************************/
function stateMachOpened(currEstadoID) {

}

/*****************************************************************************************************
Dispara se o usuario trocou o estado.
******************************************************************************************************/
function stateMachClosed(oldEstadoID, currEstadoID) {

}

/*****************************************************************************************************
Usuario clicou botao especifico da barra.
******************************************************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    ;
}

/*****************************************************************************************************
Usuario clicou um botao nao especifico do control bar.
******************************************************************************************************/
function btnBarNotEspecClicked(controlBar, btnClicked) {
    return null;
}

/*****************************************************************************************************
For�a refresh.
******************************************************************************************************/
function forceInfRefr() {
    if (__inFTimer != null) {
        window.clearInterval(__inFTimer);
        __inFTimer = null;
    }

    __btn_REFR('inf');
}

/*****************************************************************************************************
Usuario clicou o botao de alteracao em uma pasta de grid.
******************************************************************************************************/
function btnAltPressedInGrid(folderID) {
    ;
}

/*****************************************************************************************************
Usuario clicou o botao de alteracao em uma pasta que nao e grid.
******************************************************************************************************/
function btnAltPressedInNotGrid(folderID) {
    ;
}

/*****************************************************************************************************
Usuario selecionou uma pasta no control bar inferior.
******************************************************************************************************/
function fillCmbFiltsInfAutomatic(folderID) {
    return true;
}

/*****************************************************************************************************
Usuario selecionou uma pasta no control bar inferior.
******************************************************************************************************/
function selPastaChanged(optText, optValue, optIndex, nQtyGridsInFolder) {
    return null;
}

/*****************************************************************************************************
Setar os botoes da barra infeior, em funcao dos direitos do usuario logado.
******************************************************************************************************/
function frameIsAboutToApplyRights(folderID) {

    var nEstadoID = sendJSMessage('SUP_HTML', JS_DATAINFORM, EXECEVAL, 'dsoSup01.recordset[\'EstadoID\'].value');

    if (folderID == 40226) {

        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = true;
    }
    else if ((folderID == 29093) && ((nEstadoID == 83) || (nEstadoID == 7) || (nEstadoID == 161) || (nEstadoID == 5)))
    {
        glb_BtnsIncAltEstExcl[0] = true;
        glb_BtnsIncAltEstExcl[1] = true;
        glb_BtnsIncAltEstExcl[2] = true;
        glb_BtnsIncAltEstExcl[3] = null;
    }
    else {

        glb_BtnsIncAltEstExcl[0] = null;
        glb_BtnsIncAltEstExcl[1] = null;
        glb_BtnsIncAltEstExcl[2] = null;
        glb_BtnsIncAltEstExcl[3] = null;
    }

    return null;
}

/*****************************************************************************************************
Ajusta graficamente elementos de divs que nao estao no padrao do framework.
******************************************************************************************************/
function adjustElementsInDivsNonSTD() {
    ;
}

/********************************************************************
Coloca atributos em campos para serem manobrados graficamente.
********************************************************************/
function putSpecialAttributesInControls() {
    ;
}

function fg_AfterRowColChange_Prg() {
}
function fg_DblClick_Prg() {
    // Historico
    if (keepCurrFolder() == 40226)
    {
        var nCreditoID = getCellValueByColKey(fg, 'ID', fg.Row);

        //sendJSCarrier(getHtmlId(), 'TELEFONIA_DIAL', new Array(sDDD, sNumero, nPessoaID));
        sendJSCarrier(getHtmlId(), 'SHOWCREDITO', new Array(glb_EmpresaData[0], nCreditoID));
    }
}
function fg_ChangeEdit_Prg() {
}
function fg_ValidateEdit_Prg() {
}
function fg_BeforeRowColChange_Prg() {
}
function fg_EnterCell_Prg() {
}
function fg_MouseUp_Prg() {
}
function fg_MouseDown_Prg() {
}
function fg_BeforeEdit_Prg() {
}
function fg_BeforeRowColChange_Prg() {

}

function js_Creditos_AfterEdit(Row, Col) {
    if (fg.Editable) {
        if (keepCurrFolder() == 29093) {
            if (Col == getColIndexByColKey(fg, 'Valor')) {
                fg.TextMatrix(Row, Col) = treatNumericCell(fg.TextMatrix(Row, Col));
            }
        }
    }
}
