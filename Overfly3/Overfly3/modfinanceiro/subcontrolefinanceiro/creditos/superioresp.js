/*****************************************************************************************************
Esta funcao seta o sql do dso para buscar o registro a ser mostrado no detalhe.
 ******************************************************************************************************/
function setSqlToDso(dso, nID, sCompleteFunction, newRegister) {
    var sSQL;

    var nUserID = getCurrUserID();

    if (newRegister == true)
        sSQL = 'SELECT TOP 1 a.*, ' +
            'CONVERT(VARCHAR, a.dtSolicitacao, ' + DATE_SQL_PARAM + ') as V_dtSolicitacao, ' +
            'CONVERT(VARCHAR, a.dtConcessao, ' + DATE_SQL_PARAM + ') as V_dtConcessao, ' +
            'CONVERT(VARCHAR, a.dtInicio, ' + DATE_SQL_PARAM + ') as V_dtInicio, ' +
            'CONVERT(VARCHAR, a.dtFim, ' + DATE_SQL_PARAM + ') as V_dtFim, ' +
            'CONVERT(VARCHAR, a.dtCancelamento, ' + DATE_SQL_PARAM + ') as V_dtCancelamento, ' +
            'b.TipoPessoaID, ' + 
            'Prop1 = CASE a.ProprietarioID ' +
            'WHEN ' + nID + ' THEN 1 ' +
            'ELSE 0 ' +
            'END, ' +
            'Prop2 = (CASE WHEN a.ProprietarioID= ' + nID + ' THEN 1 ' +
            'WHEN a.AlternativoID= ' + nID + ' THEN 1 ' +
            'WHEN (SELECT COUNT (*) FROM RelacoesPessoas b WITH(NOLOCK) ' +
            'WHERE b.EstadoID=2 AND b.TipoRelacaoID=34 AND b.SujeitoID= ' + nID + ' AND b.ObjetoID=a.AlternativoID)>0 THEN 1 ' +
            'ELSE 0 ' +
            'END) ' +
            'FROM Creditos a WITH(NOLOCK) ' +
                'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ParceiroID) ' +
            'WHERE a.SolicitanteID = ' + nID + ' ORDER BY CreditoID DESC';
    else
        sSQL = 'SELECT a.*, ' +
            'CONVERT(VARCHAR, a.dtSolicitacao, ' + DATE_SQL_PARAM + ') as V_dtSolicitacao, ' +
            'CONVERT(VARCHAR, a.dtConcessao, ' + DATE_SQL_PARAM + ') as V_dtConcessao, ' +
            'CONVERT(VARCHAR, a.dtInicio, ' + DATE_SQL_PARAM + ') as V_dtInicio, ' +
            'CONVERT(VARCHAR, a.dtFim, ' + DATE_SQL_PARAM + ') as V_dtFim, ' +
            'CONVERT(VARCHAR, a.dtCancelamento, ' + DATE_SQL_PARAM + ') as V_dtCancelamento, ' +
            'b.TipoPessoaID, ' + 
            'Prop1 = CASE a.ProprietarioID ' +
            'WHEN ' + nUserID + ' THEN 1 ' +
            'ELSE 0 ' +
            'END, ' +
            'Prop2 = (CASE WHEN a.ProprietarioID= ' + nUserID + ' THEN 1 ' +
            'WHEN a.AlternativoID= ' + nUserID + ' THEN 1 ' +
            'WHEN (SELECT COUNT (*) FROM RelacoesPessoas b WITH(NOLOCK) ' +
            'WHERE b.EstadoID=2 AND b.TipoRelacaoID=34 AND b.SujeitoID= ' + nUserID + ' AND b.ObjetoID=a.AlternativoID)>0 THEN 1 ' +
            'ELSE 0 ' +
            'END) ' +
            'FROM Creditos a WITH(NOLOCK) ' +
                'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ParceiroID) ' +
            'WHERE a.CreditoID = ' + nID + ' ORDER BY CreditoID DESC';

    setConnection(dso);

    dso.SQL = sSQL;
    dso.ondatasetcomplete = eval(sCompleteFunction);
    dso.refresh();

    return null;
}

/*****************************************************************************************************
Esta funcao configura o dsoSup01 para inclusao de novo registro.
 ******************************************************************************************************/
function setup_dsoSup01FromPesq(dso) {
    setConnection(dso);

    var sql;

    var nUserID = getCurrUserID();


    sql = 'SELECT a.*, ' +
        'CONVERT(VARCHAR, a.dtSolicitacao, ' + DATE_SQL_PARAM + ') as V_dtSolicitacao, ' +
        'CONVERT(VARCHAR, a.dtConcessao, ' + DATE_SQL_PARAM + ') as V_dtConcessao, ' +
        'CONVERT(VARCHAR, a.dtInicio, ' + DATE_SQL_PARAM + ') as V_dtInicio, ' +
        'CONVERT(VARCHAR, a.dtFim, ' + DATE_SQL_PARAM + ') as V_dtFim, ' +
        'CONVERT(VARCHAR, a.dtCancelamento, ' + DATE_SQL_PARAM + ') as V_dtCancelamento, ' +
        'b.TipoPessoaID, ' + 
        'Prop1 = CASE a.ProprietarioID ' +
        'WHEN ' + nUserID + ' THEN 1 ' +
        'ELSE 0 ' +
        'END, ' +
        'Prop2 = (CASE WHEN a.ProprietarioID= ' + nUserID + ' THEN 1 ' +
        'WHEN a.AlternativoID= ' + nUserID + ' THEN 1 ' +
        'WHEN (SELECT COUNT (*) FROM RelacoesPessoas b WITH(NOLOCK) ' +
        'WHERE b.EstadoID=2 AND b.TipoRelacaoID=34 AND b.SujeitoID= ' + nUserID + ' AND b.ObjetoID=a.AlternativoID)>0 THEN 1 ' +
        'ELSE 0 ' +
        'END) ' +
        'FROM Creditos a WITH(NOLOCK) ' +
            'INNER JOIN Pessoas b WITH(NOLOCK) ON (b.PessoaID = a.ParceiroID) ' +
        'WHERE a.CreditoID = 0 ORDER BY CreditoID DESC';

    dso.SQL = sql;
}