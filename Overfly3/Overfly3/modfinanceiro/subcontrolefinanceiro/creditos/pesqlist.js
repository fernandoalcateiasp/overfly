/*****************************************************************************************************
Declara��o de vari�veis globais.
******************************************************************************************************/
var dsoListData01 = new CDatatransport("dsoListData01");
var dsoCmbsContFilt = new CDatatransport("dsoCmbsContFilt");
var dsoPropsPL = new CDatatransport("dsoPropsPL");

/*****************************************************************************************************
Window onload
******************************************************************************************************/
function window_onload() {
    windowOnLoad_1stPart();

    glb_COLPESQORDER = new Array('ID', 'Est', 'Parceiro', 'Pa�s', 'Seguradora', 'Solicita��o', 'Solicitante', 'Pri', 'Concess�o', 'Fim', 'Moeda',
        'Valor Solicitado', 'Potencial Credito', 'Valor Concedido', 'Observa��o', 'PessoaID', 'TipoPessoaID');

    var dTFormat = '';

    if (DATE_FORMAT == "DD/MM/YYYY")
        dTFormat = 'dd/mm/yyyy';
    else if (DATE_FORMAT == "MM/DD/YYYY")
        dTFormat = 'mm/dd/yyyy';

    glb_aCOLPESQFORMAT = new Array('', '', '', '', '', dTFormat, '', '', dTFormat, dTFormat, '', '###,###,###.00', '###,###,###.00', '###,###,###.00', '', '', '');

    windowOnLoad_2ndPart();
}

/*****************************************************************************************************
O pesqlist esta visivel e ativo
******************************************************************************************************/
function pesqlistIsVisibleAndUnlocked() {

    if (fg.Cols > 0) {
        fg.ColHidden(getColIndexByColKey(fg, 'PessoaID')) = true;
        fg.ColHidden(getColIndexByColKey(fg, 'TipoPessoaID')) = true;
    }

    showBtnsEspecControlBar('sup', true, [1, 1, 1, 1, 1]);
    tipsBtnsEspecControlBar('sup', ['Documentos', 'Relat�rios', 'Procedimento', 'Detalhar Pessoa', 'Resumo']);
    setupEspecBtnsControlBar('sup', 'HDHDHD');

    if (fg.Cols > 2)
        fg.ColAlignment(10) = 1;
}

/*****************************************************************************************************
Usuario clicou botao especifico da barra.
******************************************************************************************************/
function btnBarClicked(controlBar, btnClicked) {
    if (btnClicked == 1) {
        if (fg.Rows > 1) {
            __openModalDocumentos(controlBar, fg.TextMatrix(fg.Row, 0));
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
    else if (btnClicked == 3) {
        window.top.openModalControleDocumento('PL', '', 720001, null, '22', 'T');
    }
    else if (btnClicked == 5) {
        if (fg.Rows > 1) {
            window.top.openModalHTML(getCellValueByColKey(fg, 'ID', fg.Row), 'Resumo do Cr�dito', 'PL', 12);
        }
        else {
            window.top.overflyGen.Alert('Selecione um registro.');
        }
    }
}

/*****************************************************************************************************
Propagada por janela modal que abriu ou fechou.
******************************************************************************************************/
function modalInformForm(idElement, param1, param2) {
    if (idElement.toUpperCase() == 'MODALCONTROLEDOCUMENTOHTML') {
        if (param1 == 'OK') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
        else if (param1 == 'CANCEL') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Listagem');
            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALPRINTHTML') {
        if (param1 == 'OK') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALDOCUMENTOSHTML') {
        if (param1 == 'OK') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
    if (idElement.toUpperCase() == 'MODALOPENHTML') {
        if (param1 == 'OK') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
        else if (param1 == 'CANCEL') {
            restoreInterfaceFromModal();
            writeInStatusBar('child', 'cellMode', 'Detalhe');

            return 0;
        }
    }
}

/*****************************************************************************************************
Pede string complementar de pesquisa para forms que tenham este caso.
******************************************************************************************************/
function specialClauseOfResearch() {
    return null;
}