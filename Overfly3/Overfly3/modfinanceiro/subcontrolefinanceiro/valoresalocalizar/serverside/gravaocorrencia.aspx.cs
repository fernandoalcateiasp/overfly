﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class gravaocorrencia : System.Web.UI.OverflyPage
    {
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private string[] Delete;

        public string[] nDelete
        {
            get { return Delete; }
            set
            {
                Delete = value;

                for (int i = 0; i < Delete.Length; i++)
                {
                    if (Delete[i] == null || Delete[i] == "")
                        Delete[i] = string.Empty;
                }
            }
        }
        private string[] Valor;

        public string[] nValor
        {
            get { return Valor; }
            set { Valor = value; }
        }
        private string[] OK;

        public string[] bOK
        {
            get { return OK; }
            set { OK = value; }
        }
        private string[] Observacao;

        public string[] sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }
        private Integer[] ValFinanceiroID;

        public Integer[] nValFinanceiroID
        {
            get { return ValFinanceiroID; }
            set { ValFinanceiroID = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            string sql = "";
            string erros = "";

            try
            {
                for (int i = 0; i < DataLen.intValue(); i++)
                {
                    if (Delete[0].ToString() == "1")
                    {
                        sql = "DELETE ValoresLocalizar_Financeiros WHERE ValFinanceiroID =" + ValFinanceiroID[i];

                    }
                    if (Delete[0].ToString() == "0")
                    {
                        sql = "UPDATE ValoresLocalizar_Financeiros SET OK =" + OK[i] + ", Valor=" + Valor[i] + ", Observacao = '" + Observacao[i] + "'" +
                                    " WHERE ValFinanceiroID =" + ValFinanceiroID[i];
                    }
                    DataInterfaceObj.ExecuteSQLCommand(sql);
                }
            }
            catch (System.Exception exception)
            {

                //erros += "#ERROR#OverflyPage (" + this.GetType().Name + "):" + exception.Message + " \r\n ";
                erros += exception.Message + " \r\n ";
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + erros + "' as Mensagem "));
        }
    }
}