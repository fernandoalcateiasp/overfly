﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class geradepositobancario : System.Web.UI.OverflyPage
    {

        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private Integer EmpresaID;

        public Integer nEmpresaID
        {
            get { return EmpresaID; }
            set { EmpresaID = value; }
        }
        private Integer RelPesContaID;

        public Integer nRelPesContaID
        {
            get { return RelPesContaID; }
            set { RelPesContaID = value; }
        }
        private Integer MoedaID;

        public Integer nMoedaID
        {
            get { return MoedaID; }
            set { MoedaID = value; }
        }
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer[] ValorID;

        public Integer[] nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }
        private Integer FormCallerID;

        public Integer nFormCallerID
        {
            get { return FormCallerID; }
            set { FormCallerID = value; }
        }
        private Integer DepositoID;

        public Integer nDepositoID
        {
            get { return DepositoID; }
            set { DepositoID = value; }
        }


        protected override void PageLoad(object sender, EventArgs e)
        {
            string sResultado = "";
            string Deposito = "";

            if (DepositoID.intValue() == 0)
            {

                ProcedureParameters[] procParamsLocalizar = new ProcedureParameters[7];

                procParamsLocalizar[0] = new ProcedureParameters(
                    "@EmpresaID",
                    System.Data.SqlDbType.Int,
                    EmpresaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(EmpresaID));

                procParamsLocalizar[1] = new ProcedureParameters(
                    "@dtData",
                    System.Data.SqlDbType.DateTime,
                    DateTime.Now);

                procParamsLocalizar[2] = new ProcedureParameters(
                   "@RelPesContaID",
                   System.Data.SqlDbType.Int,
                    RelPesContaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(RelPesContaID));

                procParamsLocalizar[3] = new ProcedureParameters(
                   "@MoedaID",
                   System.Data.SqlDbType.Int,
                   MoedaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(MoedaID));

                procParamsLocalizar[4] = new ProcedureParameters(
                   "@UsuarioID",
                   System.Data.SqlDbType.Int,
                   UserID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UserID.ToString()));

                procParamsLocalizar[5] = new ProcedureParameters(
                   "@DepositoID",
                   System.Data.SqlDbType.Int,
                   DBNull.Value,
                  ParameterDirection.InputOutput);

                procParamsLocalizar[6] = new ProcedureParameters(
                   "@Resultado",
                   System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.InputOutput);
                procParamsLocalizar[6].Length = 8000;


                DataInterfaceObj.execNonQueryProcedure("sp_DepositoBancario_Gerador", procParamsLocalizar);

                if (procParamsLocalizar[6].Data != DBNull.Value)
                    sResultado = procParamsLocalizar[6].Data.ToString();


                if (procParamsLocalizar[5].Data != DBNull.Value)
                    Deposito = procParamsLocalizar[5].Data.ToString();

            }

            if (Deposito.ToString() != "0")
            {
                for (int i = 0; i < DataLen.intValue(); i++)
                {
                    ProcedureParameters[] procParamsLocalizar = new ProcedureParameters[3];

                    procParamsLocalizar[0] = new ProcedureParameters(   
                        "@DepositoID",
                        System.Data.SqlDbType.Int,
                        Deposito != null ? DBNull.Value : (Object)Deposito);

                    procParamsLocalizar[1] = new ProcedureParameters(
                        "@ValorID",
                        System.Data.SqlDbType.Int,
                        ValorID.Length > 0 ? (Object)Convert.ToInt32(ValorID[i]) : DBNull.Value);

                    procParamsLocalizar[2] = new ProcedureParameters(
                       "@Resultado",
                       System.Data.SqlDbType.VarChar,
                      DBNull.Value,
                      ParameterDirection.InputOutput);
                    procParamsLocalizar[2].Length = 8000;


                    DataInterfaceObj.execNonQueryProcedure("sp_DepositoBancarioValores_Gerador", procParamsLocalizar);

                    if (procParamsLocalizar[2].Data != DBNull.Value)
                        sResultado = procParamsLocalizar[2].Data.ToString();

                }
            }

            WriteResultXML(DataInterfaceObj.getRemoteData(
            "select '" + sResultado + "' as Resultado, " +
                            Deposito + " as DepositoID"));
        }
    }
}