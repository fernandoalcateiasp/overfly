﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class confirmarfinanceiros : System.Web.UI.OverflyPage
    {

        private Integer UserID;

        public Integer nUserID
        {
            get { return UserID; }
            set { UserID = value; }
        }
        private Integer Confirmar;

        public Integer nConfirmar
        {
            get { return Confirmar; }
            set { Confirmar = value; }
        }
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer[] ValorID;

        public Integer[] nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }
        private Integer[] ValFinanceiroID;

        public Integer[] nValFinanceiroID
        {
            get { return ValFinanceiroID; }
            set { ValFinanceiroID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            int fldresp = 0;
            string sql = "";

            for (int i = 0; i < DataLen.intValue(); i++)
            {

                if (Confirmar.intValue() == 1)
                    sql = "UPDATE ValoresLocalizar_Financeiros SET OK=1 " +
                            "WHERE ValFinanceiroID = " + ValFinanceiroID[i];
                else
                    sql = "UPDATE ValoresLocalizar SET EstadoID=41, UsuarioID = " + (UserID) + " " +
                            "WHERE (ValorID = " + ValorID[i] + " AND EstadoID = 1 AND " +
                                "dbo.fn_ValorLocalizar_Verifica(ValorID, EstadoID, 41, GETDATE()) IS NULL)";

                fldresp = DataInterfaceObj.ExecuteSQLCommand(sql);


            }
            WriteResultXML(DataInterfaceObj.getRemoteData(
               "select '" + fldresp + "' as fldresp "));
        }
    }
}