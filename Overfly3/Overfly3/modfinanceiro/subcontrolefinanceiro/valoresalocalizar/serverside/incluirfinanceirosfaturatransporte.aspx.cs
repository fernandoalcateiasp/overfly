﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class incluirfinanceirosfaturatransporte : System.Web.UI.OverflyPage
    {
        private Integer ValorID;

        public Integer nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }
        private Integer FaturaID;

        public Integer nFaturaID
        {
            get { return FaturaID; }
            set { FaturaID = value; }
        } 
        
        
        protected override void PageLoad(object sender, EventArgs e)
        {
            string Resultado = "";
            ProcedureParameters[] procParams = new ProcedureParameters[3];

            procParams[0] = new ProcedureParameters(
            "@ValorID",
            System.Data.SqlDbType.Int,
            ValorID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(ValorID));

            procParams[1] = new ProcedureParameters(
            "@FaturaID",
            System.Data.SqlDbType.Int,
            FaturaID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(FaturaID));

            procParams[2] = new ProcedureParameters(
            "@Resultado",
            System.Data.SqlDbType.VarChar,
            DBNull.Value,
            ParameterDirection.InputOutput);
            procParams[2].Length = 8000;


            DataInterfaceObj.execNonQueryProcedure("sp_ValorLocalizar_FaturaTransporte", procParams);

            if (procParams[2].Data != DBNull.Value)
                Resultado += procParams[2].Data.ToString() + "\r \n";



            WriteResultXML(DataInterfaceObj.getRemoteData("SELECT " + Resultado + " AS Resultado"));

        }
    }
}