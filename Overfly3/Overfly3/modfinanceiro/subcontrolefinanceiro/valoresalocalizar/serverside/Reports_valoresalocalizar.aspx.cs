﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.Reporting.WebForms;
using System.Data;
using System.Configuration;
using OVERFLYSVRCFGLib;
using System.Data.SqlClient;
using System.IO;
using System.Diagnostics;
using WSData;
using System.Globalization;

namespace Overfly3.PrintJet
{
    public partial class Reports_valoresalocalizar : System.Web.UI.Page
    {
        ClsReport Relatorio = new ClsReport();

        public string[,] arrSqlQuery;
        public string[,] arrIndexKey;
        private int nEmpresaID = Convert.ToInt32(HttpContext.Current.Request.Params["glb_nEmpresaID"]);
        private string mEmpresaFantasia = Convert.ToString(HttpContext.Current.Request.Params["glb_sEmpresaFantasia"]);
        private int sLinguaLogada = Convert.ToInt32(HttpContext.Current.Request.Params["nEmpresaLogadaIdioma"]);
        private string nEmpresaNome = Convert.ToString(HttpContext.Current.Request.Params["nEmpresaNome"]);
        protected dataInterface DataInterfaceObj = new dataInterface(System.Configuration.ConfigurationManager.AppSettings["application"]);
        private int formato = Convert.ToInt32(HttpContext.Current.Request.Params["Formato"]);
        private string nContextoID = Convert.ToString(HttpContext.Current.Request.Params["nContextoID"]);
        private string sDataEmissaoIni = Convert.ToString(HttpContext.Current.Request.Params["sDataEmissaoIni"]);
        private string sDataEmissaoFim = Convert.ToString(HttpContext.Current.Request.Params["sDataEmissaoFim"]);
        private string chkApropriacao = Convert.ToString(HttpContext.Current.Request.Params["chkApropriacao"]);
        private string selFormaPagamentoID = Convert.ToString(HttpContext.Current.Request.Params["selFormaPagamentoID"]);
        private string txtFiltro = Convert.ToString(HttpContext.Current.Request.Params["txtFiltro"]);
        private string chkOcorrencias = Convert.ToString(HttpContext.Current.Request.Params["chkOcorrencias"]);
        private string sEstados = Convert.ToString(HttpContext.Current.Request.Params["sEstados"]);

        protected void Page_Load(object sender, EventArgs e)
        {

            if (!IsPostBack)
            {
                string sRelatorioID = Request.Params["RelatorioID"].ToString();

                switch (sRelatorioID)
                {
                    case "40191":
                        relatorioValoresLocalizar();
                        break;

                    case "40192":
                        relatorioCheque();
                        break;
                }

            }
        }

        public void relatorioValoresLocalizar()
        {
            // Excel
            int Formato = 2;
            string Title = "";
            string sFieldData;
            string nFiltroID = "";

            if (chkApropriacao == "1")
                sFieldData = "dtApropriacao";
            else
                sFieldData = "dtEmissao";

            if (nContextoID == "9131")
            {
                nFiltroID = "1001";
                Title = "Valores a Localizar - Pagamentos";
            }

            if (nContextoID == "9132")
            {
                nFiltroID = "1002";
                Title = "Valores a Localizar - Recebimentos";
            }

            string strSQL = " SELECT " +
                                " ValoresLocalizar.ValorID AS [ID], Estados.RecursoAbreviado AS [Estado], FormasPagamento.ItemAbreviado AS [Forma], Pessoas.Fantasia AS [Pessoa], " +
                                " ValoresLocalizar.Emitente, ValoresLocalizar.dtEmissao AS [Emissão], ValoresLocalizar.dtApropriacao AS [Apropriação], ValoresLocalizar.BancoAgencia, " +
                                " ValoresLocalizar.NumeroDocumento AS [Numero], Moedas.SimboloMoeda AS [Moeda], ValoresLocalizar.Valor, " +
                                " dbo.fn_ValorLocalizar_Posicao(ValoresLocalizar.ValorID, 4) AS [SaldoFinanceiro], dbo.fn_ValorLocalizar_Posicao(ValoresLocalizar.ValorID, 2) AS [SaldoOcorrencia], " +
                                " Colaboradores.Fantasia AS [Colaborador], ValoresLocalizar.Observacao, 1 AS [_Registro] " +
                            " FROM " +
                                " ValoresLocalizar ValoresLocalizar WITH(NOLOCK) " +
                                    " LEFT OUTER JOIN Pessoas WITH(NOLOCK) ON(ValoresLocalizar.PessoaID = Pessoas.PessoaID) " +
                                    " INNER JOIN TiposAuxiliares_Itens FormasPagamento WITH(NOLOCK) ON(ValoresLocalizar.FormaPagamentoID = FormasPagamento.ItemID) " +
                                    " INNER JOIN Recursos Estados WITH(NOLOCK) ON(ValoresLocalizar.EstadoID = Estados.RecursoID) " +
                                    " INNER JOIN Conceitos Moedas WITH(NOLOCK) ON(ValoresLocalizar.MoedaID = Moedas.ConceitoID) " +
                                    " INNER JOIN Pessoas Colaboradores WITH(NOLOCK) ON(Colaboradores.PessoaID = ValoresLocalizar.ProprietarioID) " +
                            " WHERE " +
                                " (ValoresLocalizar.EmpresaID = " + nEmpresaID + ") " +
                            " AND (ValoresLocalizar.TipoValorID = " + nFiltroID + ") ";

            if (sDataEmissaoIni != "")
                strSQL += " AND ValoresLocalizar." + sFieldData + " >= " + sDataEmissaoIni;

            if (sDataEmissaoFim != "")
                strSQL += " AND ValoresLocalizar." + sFieldData + " <= " + sDataEmissaoFim;

            if (selFormaPagamentoID != "0")
                strSQL += " AND (ValoresLocalizar.FormaPagamentoID= " + selFormaPagamentoID + ") ";

            if (txtFiltro != "NULL")
                strSQL += " AND " + txtFiltro;

            if (sEstados != "")
                strSQL += sEstados;

            strSQL += " ORDER BY ValoresLocalizar." + sFieldData + ", ValoresLocalizar.BancoAgencia, Numero";


            var Header_Body = (Formato == 1 ? "Header" : "Body");

            arrSqlQuery = new string[,] { { "Query1", strSQL } };

            string[,] arrIndexKey = new string[,] { { "" } };

            if (Formato == 1)
            {
                ;
            }

            else if (Formato == 2) // Se Excel
            {
                Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Excel");

                Relatorio.CriarObjLabel("Fixo", "", mEmpresaFantasia, "0.001", "0.0", "11", "#0113a0", "B", Header_Body, "4.51332", "");
                Relatorio.CriarObjLabel("Fixo", "", Title, "7.13834", "0.0", "11", "#0113a0", "B", Header_Body, "10.51332", "");
                Relatorio.CriarObjLabelData("Now", "", "", "20.0", "0.0", "11", "black", "B", Header_Body, "3.72187", "Horas");

                //Cria Tabela           
                Relatorio.CriarObjTabela("0.00", "0.6", "Query1", true, true, true);
                Relatorio.TabelaEnd();
            }
            Relatorio.CriarPDF_Excel(Title, Formato);
        }

        public void relatorioCheque()
        {
            string Title = "Cheques";
            int Formato = 1;
            int nValorID = Convert.ToInt32(HttpContext.Current.Request.Params["nValorID"]);
            bool chkCheckSummary = Convert.ToBoolean(HttpContext.Current.Request.Params["chkCheckSummary"]);
            string nFonteSummary = Convert.ToString(HttpContext.Current.Request.Params["nFonteSummary"]);
            string txtNumero = Convert.ToString(HttpContext.Current.Request.Params["txtNumero"]);

            Double dValor = Convert.ToDouble(HttpContext.Current.Request.Params["1081"]);
            string sExtenso1 = Convert.ToString(HttpContext.Current.Request.Params["1082"]);
            string sExtenso2 = Convert.ToString(HttpContext.Current.Request.Params["1083"]);
            string sNominal = Convert.ToString(HttpContext.Current.Request.Params["1084"]);
            string sCidade = Convert.ToString(HttpContext.Current.Request.Params["1085"]);
            string sDia = Convert.ToString(HttpContext.Current.Request.Params["1086"]);
            string sMes = Convert.ToString(HttpContext.Current.Request.Params["1087"]);
            string sAno = Convert.ToString(HttpContext.Current.Request.Params["1088"]);
            string sHistorico = Convert.ToString(HttpContext.Current.Request.Params["1089"]);

            //Define formato de data de acordo com a lingua logada
            string param = "dd/MM/yyy";
            int DATE_SQL_PARAM = 103;
            string sCulture = "pt-BR";

            if (sLinguaLogada != 246)
            {
                param = "MM/dd/yyy";
                DATE_SQL_PARAM = 101;
                sCulture = "en-US";
            }

            string sValor = string.Format(CultureInfo.GetCultureInfo(sCulture), "{0:N}", dValor);

            //Recebe data atual para colocar no titulo do Excel
            DateTime DataHoje = DateTime.Now;
            string _data = DataHoje.ToString(param);

            param = "<Format>" + param + "</Format>";

            string strSQL = "SELECT " +
                                "e.TipoCampoID, e.Linha, e.Coluna, e.Tamanho, e.TamanhoFonte, e.Negrito, e.CaixaAlta " +
                            "FROM " +
                                "ValoresLocalizar a WITH(NOLOCK) " +
                                    "INNER JOIN Pessoas b WITH(NOLOCK) ON(a.EmpresaID = b.PessoaID) " +
                                    "INNER JOIN Pessoas_Enderecos c WITH(NOLOCK) ON(b.PessoaID = c.PessoaID) " +
                                    "INNER JOIN Bancos d WITH(NOLOCK) ON(LEFT(a.BancoAgencia, 3) = d.Codigo) " +
                                    "INNER JOIN Bancos_Cheques e WITH(NOLOCK) ON(d.BancoID = e.BancoID) " +
                            "WHERE " +
                                "a.ValorID = " + nValorID +
                            "AND a.EmpresaID = b.PessoaID AND c.Ordem = 1 AND c.EndFaturamento = 1 AND c.PaisID = d.PaisID AND d.EstadoID = 2 " +
                            "ORDER BY e.TipoCampoID ";

            string strSQLImage = "SELECT '' AS Vazio";

            if (chkCheckSummary)
            {
                string strSQL2 = "SELECT " +
                                    "dbo.fn_Caracter_RemoveEspecial(b.Nome) AS Pessoa, a.ValorID, ISNULL(CONVERT(VARCHAR(10), a.dtEmissao, " + DATE_SQL_PARAM + "), SPACE(0)) AS dtEmissao, " +
                                    "ISNULL(dbo.fn_Caracter_RemoveEspecial(dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 5)), SPACE(0)) AS Banco, " +
                                    "ISNULL(dbo.fn_ContaBancaria_Nome(a.RelPesContaID, 4), SPACE(0)) AS Conta, ISNULL(a.NumeroDocumento, SPACE(0)) AS Cheque, " +
                                    "ISNULL(dbo.fn_Numero_Formata(a.Valor, 2, 1, " + DATE_SQL_PARAM + "), SPACE(0)) AS Valor " +
                                "FROM " +
                                    "ValoresLocalizar a WITH(NOLOCK) " +
                                        "INNER JOIN Pessoas b WITH(NOLOCK) ON(b.PessoaID = a.PessoaID) " +
                                "WHERE " +
                                    " a.ValorID = " + nValorID;

                string strSQL3 = "SELECT " +
                                    "ISNULL(a.FinanceiroID, 0) AS FinanceiroID, ISNULL(b.PedidoID, 0) AS PedidoID, ISNULL(b.Duplicata, SPACE(0)) AS Duplicata, " +
                                    "ISNULL(CONVERT(VARCHAR(10), b.dtVencimento, " + DATE_SQL_PARAM + "), SPACE(0)) AS dtVencimento, " +
                                    "ISNULL(dbo.fn_Numero_Formata(b.Valor, 2, 1, " + DATE_SQL_PARAM + "), SPACE(0)) AS OriginalAmmount, " +
                                    "ISNULL(dbo.fn_Numero_Formata(dbo.fn_Financeiro_Posicao(b.FinanceiroID, 9, NULL, GETDATE(), NULL, NULL), 2, 1, " + DATE_SQL_PARAM + "), SPACE(0)) AS BalanceDue, " +
                                    "ISNULL(dbo.fn_Numero_Formata(a.Valor, 2, 1, " + DATE_SQL_PARAM + "), SPACE(0)) AS Payment " +
                                "FROM " +
                                    "ValoresLocalizar_Financeiros a WITH(NOLOCK) " +
                                        "INNER JOIN Financeiro b WITH(NOLOCK) ON(b.FinanceiroID = a.FinanceiroID) " +
                                "WHERE a.ValorID = " + nValorID +
                                "ORDER BY a.FinanceiroID";

                arrSqlQuery = new string[,] { { "Query1", strSQL }, { "Query2", strSQL2 }, { "Query3", strSQL3 }, { "QueryImage", strSQLImage } };
            }
            else
                arrSqlQuery = new string[,] { { "Query1", strSQL }, { "QueryImage", strSQLImage } };

            Relatorio.CriarRelatório(Title.ToString(), arrSqlQuery, "BRA", "Portrait", "1.8");

            DataSet dsFin = new DataSet();

            dsFin = (DataSet)HttpContext.Current.Session["Query"];
            if (dsFin.Tables["Query1"].Rows.Count == 0)
            {
                HttpContext.Current.Response.Write("<script type=\"text/javascript\"> alert('Nenhum registro encontrado.');</script>");
                return;
            }
            else
            {
                var linhaSQL = 0;

                Relatorio.CriarObjImage("Query", "QueryImage", "Vazio", "0", "0", "Header", "0", "0");
                Relatorio.CriarObjLabel("Fixo", "", "*" + sValor + "*", ((Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), dsFin.Tables["Query1"].Rows[linhaSQL]["TamanhoFonte"].ToString(), "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), "");
                Relatorio.CriarObjLabel("Fixo", "", sExtenso1, (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL += 1]["Coluna"]) / 10).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), dsFin.Tables["Query1"].Rows[linhaSQL]["TamanhoFonte"].ToString(), "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), "");

                if (sExtenso2.Length != 0)
                    Relatorio.CriarObjLabel("Fixo", "", sExtenso2, (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL += 1]["Coluna"]) / 10).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), "8", "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), "");
                else
                    linhaSQL += 1;

                Relatorio.CriarObjLabel("Fixo", "", sNominal, ((Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL += 1]["Coluna"]) / 10) + 0.3).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), dsFin.Tables["Query1"].Rows[linhaSQL]["TamanhoFonte"].ToString(), "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - ((Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10) + 0.3)).ToString(), "");

                if (sCidade != null)
                    Relatorio.CriarObjLabel("Fixo", "", sCidade, (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL += 1]["Coluna"]) / 10).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), dsFin.Tables["Query1"].Rows[linhaSQL]["TamanhoFonte"].ToString(), "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), "");

                Relatorio.CriarObjLabel("Fixo", "", sDia, ((Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL += 1]["Coluna"]) / 10)).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), dsFin.Tables["Query1"].Rows[linhaSQL]["TamanhoFonte"].ToString(), "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), "");
                Relatorio.CriarObjLabel("Fixo", "", sMes, ((Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL += 1]["Coluna"]) / 10)).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), dsFin.Tables["Query1"].Rows[linhaSQL]["TamanhoFonte"].ToString(), "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), "");
                Relatorio.CriarObjLabel("Fixo", "", sAno, ((Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL += 1]["Coluna"]) / 10)).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), dsFin.Tables["Query1"].Rows[linhaSQL]["TamanhoFonte"].ToString(), "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), "");
                Relatorio.CriarObjLabel("Fixo", "", sHistorico, (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL += 1]["Coluna"]) / 10).ToString(), (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10).ToString(), dsFin.Tables["Query1"].Rows[linhaSQL]["TamanhoFonte"].ToString(), "black", (dsFin.Tables["Query1"].Rows[linhaSQL]["Negrito"].ToString() == "True" ? "B" : "L"), "Body", (21 - (Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Coluna"]) / 10)).ToString(), "");

                double linhaSumario = ((Convert.ToDouble(dsFin.Tables["Query1"].Rows[linhaSQL]["Linha"]) / 10) + 3.2);

                if (chkCheckSummary && dsFin.Tables["Query2"].Rows.Count != 0 && dsFin.Tables["Query3"].Rows.Count != 0)
                {
                    string sPessoa = "";
                    if (nContextoID == "9131")
                        sPessoa = dsFin.Tables["Query2"].Rows[0]["Pessoa"].ToString();
                    //Recebimentos	
                    else if (nContextoID == "9132")
                        sPessoa = nEmpresaNome;

                    string sHeader1 = "ID " + dsFin.Tables["Query2"].Rows[0]["ValorID"] + "    " + dsFin.Tables["Query2"].Rows[0]["dtEmissao"] + "    " + dsFin.Tables["Query2"].Rows[0]["Banco"] + " " +
                                      dsFin.Tables["Query2"].Rows[0]["Conta"] + "    " + "Check # " + txtNumero + "    " + "$ " + dsFin.Tables["Query2"].Rows[0]["Valor"];

                    Relatorio.CriarObjLabel("Fixo", "", sPessoa, "1", linhaSumario.ToString(), nFonteSummary, "black", "L", "Body", "20");
                    Relatorio.CriarObjLabel("Fixo", "", sHeader1, "1", (linhaSumario + 1).ToString(), nFonteSummary, "black", "L", "Body", "20");

                    //Tabela 1 - Sumário
                    Relatorio.CriarObjTabela("0", (Convert.ToDouble(linhaSumario) + 2).ToString(), "Query3", false, false, true);

                    Relatorio.CriarObjColunaNaTabela("Financ", "FinanceiroID", false, "2.6", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "FinanceiroID", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Order", "PedidoID", true, "Order", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "PedidoID", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Invoice", "Duplicata", true, "Invoice", "Default", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Date", "dtVencimento", true, "Date", "Default", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtVencimento", "DetailGroup", Fonte: nFonteSummary, Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("Original Amt", "OriginalAmmount", true, "Original Amt", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "OriginalAmmount", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Balance Due", "BalanceDue", true, "Balance Due", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "BalanceDue", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Payment", "Payment", true, "Payment", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "Payment", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.TabelaEnd();


                    //Tabela 2 - Sumário
                    double nTop = ((Math.Max(55 - ((dsFin.Tables["Query3"].Rows.Count - 1) * 5), 10)) / 10);
                    nTop = nTop + linhaSumario;

                    Relatorio.CriarObjLabel("Fixo", "", sPessoa, "1", (nTop + 3).ToString(), nFonteSummary, "black", "L", "Body", "20");
                    Relatorio.CriarObjLabel("Fixo", "", sHeader1, "1", (nTop + 4).ToString(), nFonteSummary, "black", "L", "Body", "20");

                    Relatorio.CriarObjTabela("0", (nTop + 5).ToString(), "Query3", false, false, false);

                    Relatorio.CriarObjColunaNaTabela("Financ", "FinanceiroID", false, "2.6", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "FinanceiroID", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Order", "PedidoID", true, "Order", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "PedidoID", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Invoice", "Duplicata", true, "Invoice", "Default", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "Duplicata", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Date", "dtVencimento", true, "Date", "Default", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "dtVencimento", "DetailGroup", Fonte: nFonteSummary, Decimais: param);

                    Relatorio.CriarObjColunaNaTabela("Original Amt", "OriginalAmmount", true, "Original Amt", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "OriginalAmmount", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Balance Due", "BalanceDue", true, "Balance Due", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "BalanceDue", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.CriarObjColunaNaTabela("Payment", "Payment", true, "Payment", "Right", nFonteSummary, "0.6");
                    Relatorio.CriarObjCelulaInColuna("Query", "Payment", "DetailGroup", Fonte: nFonteSummary);

                    Relatorio.TabelaEnd();
                }

                //Relatorio.CriarPDF_Excel("Cheques", Formato);
                Relatorio.Imprimir("Cheques");
            }
        }
    }
}
