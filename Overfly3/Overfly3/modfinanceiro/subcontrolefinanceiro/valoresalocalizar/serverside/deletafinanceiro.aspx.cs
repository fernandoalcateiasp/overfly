﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class deletafinanceiro : System.Web.UI.OverflyPage
    {
        private Integer[] ValFinanceiroID;

        public Integer[] nValFinanceiroID
        {
            get { return ValFinanceiroID; }
            set { ValFinanceiroID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            for (int i = 0; i < ValFinanceiroID.Length; i++)
            {
                DataInterfaceObj.getRemoteData("DELETE ValoresLocalizar_Financeiros " +
                                                    "WHERE ValFinanceiroID =" + (ValFinanceiroID[i]));
            }
            WriteResultXML(DataInterfaceObj.getRemoteData(
              "select '' as Resultado "));
        }
    }
}