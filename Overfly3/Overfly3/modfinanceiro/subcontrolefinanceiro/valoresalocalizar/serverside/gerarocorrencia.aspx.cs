﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizarEx.serverside
{
    public partial class gerarocorrencia : System.Web.UI.OverflyPage
    {
        private Integer UserId;

        public Integer nUserID
        {
            get { return UserId; }
            set { UserId = value; }
        }
        private Integer DataLen;

        public Integer nDataLen
        {
            get { return DataLen; }
            set { DataLen = value; }
        }
        private Integer[] ValorID;

        public Integer[] nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }

        protected override void PageLoad(object sender, EventArgs e)
        {
            string Resultado = "";
            for (int i = 0; i < DataLen.intValue(); i++)
            {
                ProcedureParameters[] procParamsLocalizar = new ProcedureParameters[3];

                procParamsLocalizar[0] = new ProcedureParameters(
                    "@ValorID",
                    System.Data.SqlDbType.Int,
                    ValorID.Length == 0 ? DBNull.Value : (Object)Convert.ToInt32(ValorID[i]));

                procParamsLocalizar[1] = new ProcedureParameters(
                    "@UsuarioID",
                    System.Data.SqlDbType.Int,
                    UserId.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UserId.ToString()));

                procParamsLocalizar[2] = new ProcedureParameters(
                   "@Resultado",
                   System.Data.SqlDbType.VarChar,
                  DBNull.Value,
                  ParameterDirection.InputOutput);
                procParamsLocalizar[2].Length = 8000;


                DataInterfaceObj.execNonQueryProcedure("sp_ValorLocalizar_Fechamento", procParamsLocalizar);

                if (procParamsLocalizar[2].Data != DBNull.Value)
                    Resultado += procParamsLocalizar[2].Data.ToString() + "\r \n";

            }


            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + Resultado + "' as Resultado "));

        }
    }
}