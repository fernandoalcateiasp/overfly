﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using java.lang;
using WSData;

namespace Overfly3.modfinanceiro.subcontrolefinanceiro.valoresalocalizar.serverside
{
    public partial class EstornarFinanceiro : System.Web.UI.OverflyPage
    {

        private string MensagemErro;

        private Integer FinanceiroID;

        public Integer nFinanceiroID
        {
            get { return FinanceiroID; }
            set { FinanceiroID = value; }
        }
        private Integer ValorID;

        public Integer nValorID
        {
            get { return ValorID; }
            set { ValorID = value; }
        }
        private Integer UsuarioID;

        public Integer nUsuarioID
        {
            get { return UsuarioID; }
            set { UsuarioID = value; }
        }
        private string Observacao;

        public string sObservacao
        {
            get { return Observacao; }
            set { Observacao = value; }
        }


        public void EstornarFinanceiroS()
        {

            ProcedureParameters[] procParam = new ProcedureParameters[5];

            procParam[0] = new ProcedureParameters(
                "@FinanceiroID",
                System.Data.SqlDbType.Int,
                FinanceiroID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(FinanceiroID.ToString()));

            procParam[1] = new ProcedureParameters(
               "@ValorID",
               System.Data.SqlDbType.Int,
               ValorID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(ValorID.ToString()));

            procParam[2] = new ProcedureParameters(
                "@UsuarioID",
                System.Data.SqlDbType.Int,
                UsuarioID.intValue() == 0 ? DBNull.Value : (Object)Convert.ToInt32(UsuarioID.ToString()));

            procParam[3] = new ProcedureParameters(
                "@Observacoes",
                System.Data.SqlDbType.VarChar,
                Observacao == null ? DBNull.Value : (Object)Observacao.ToString());

            procParam[4] = new ProcedureParameters(
                "@MensagemErro",
                System.Data.SqlDbType.VarChar,
                DBNull.Value,
                ParameterDirection.InputOutput);
            procParam[4].Length = 8000;

            DataInterfaceObj.execNonQueryProcedure("sp_Financeiro_Estornar", procParam);

            if (procParam[4].Data != DBNull.Value)
                MensagemErro = procParam[4].Data.ToString();
        }
        protected override void PageLoad(object sender, EventArgs e)
        {
            EstornarFinanceiroS();
            WriteResultXML(DataInterfaceObj.getRemoteData(
                "select '" + MensagemErro + "' as MensagemErro "));

        }
    }
}